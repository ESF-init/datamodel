SPOOL VSTAT_1_035.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.035', 'JGI', 'Added VIEW CORRESPONDINGSHIFTS');

CREATE OR REPLACE FORCE VIEW CORRESPONDINGSHIFTS
(
   COPILOTSHIFTID,
   PROXSHIFTID,
   DEBTORNO,
   LOCATIONNO,
   DIFFERENCE,
   COILOTSHIFTBEGIN,
   PROXSHIFTBEGIN
)
AS
   SELECT a.shiftid AS copilotshiftid,
          b.shiftid AS proxshiftid,
          A.DEBTORNO,
          A.LOCATIONNO,
          (b.shiftbegin - a.shiftbegin) * 60 * 60 * 24 AS DIFFERENCE,
          a.shiftbegin AS COILOTSHIFTBEGIN,
          b.shiftbegin AS PROXSHIFTBEGIN
     FROM stat_rm_shift a,
          (SELECT *
             FROM stat_rm_shift
            WHERE STAT_RM_SHIFT.DEVICETYPE = 70) b
    WHERE     A.DEBTORNO = B.DEBTORNO
          AND A.LOCATIONNO = B.LOCATIONNO
          AND a.DEVICETYPE = 120
          AND (b.shiftbegin - a.shiftbegin) * 60 * 60 * 24 <= 10
          AND (b.shiftbegin - a.shiftbegin) * 60 * 60 * 24 >= 0
--and a.shiftid not in (select exportreference from EXP_EXPORTTRACKING);
;


COMMIT;

PROMPT Done!

SPOOL OFF