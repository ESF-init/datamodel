SPOOL VSTAT_1_034.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.034', 'JGI', 'Added attribute DEPOTCODE and handling');

ALTER TABLE VSTAT.STAT_RM_EXT_TRANSACTION
 ADD (DEPOTCODE  VARCHAR2(10));

CREATE OR REPLACE FUNCTION EXTRACT_DEPOTCODE (RUNNINGBOARD IN VARCHAR2)
   RETURN VARCHAR2
IS
   returnValue      VARCHAR2(10);
BEGIN
   returnValue := '-';
   IF( RUNNINGBOARD IS NOT NULL )
   THEN 
     returnValue := SUBSTR(RUNNINGBOARD,1,2);
   END IF;
   RETURN returnValue;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN '-';      -- Fehler abfangen und alternativen Wert zur�ckgeben
   WHEN OTHERS
   THEN
   Return SQLERRM ; -- Fehlermeldung als Ergebnis zur�ckgeben oder
END EXTRACT_DEPOTCODE;
/

CREATE OR REPLACE PROCEDURE VSTAT.VASA_STAT_RM_EXT_TRANSACTION
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);

  -- Because its not to much data we refresh the whole table
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'rm_ext_transaction', 'STAT_RM_EXT_TRANSACTION',
               (SELECT COUNT(*) FROM STAT_RM_EXT_TRANSACTION), 'values before update');
                 
  INSERT INTO STAT_RM_EXT_TRANSACTION 
            (TRANSACTIONID,NOTE,LATITUDE,LONGITUDE,CODE,ROUTECODE,TICKETREFERENCE,TRIPCODE,TRIPPURPOSE,DEPOTCODE)  
     (SELECT TRANSACTIONID,NOTE,LATITUDE,LONGITUDE,CODE,ROUTECODE,TICKETREFERENCE,TRIPCODE,TRIPPURPOSE,EXTRACT_DEPOTCODE(ROUTECODE)
       FROM RM_EXT_TRANSACTION@vario_productive WHERE TRANSACTIONID IN (SELECT TRANSACTIONID FROM STAT_RM_TRANSACTIONS));
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'rm_ext_transaction', 'STAT_RM_EXT_TRANSACTION',
               (SELECT COUNT(*) FROM STAT_RM_EXT_TRANSACTION), 'values after update');  
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
  
EXCEPTION
  WHEN OTHERS
  THEN
    EXECUTE IMMEDIATE ('ROLLBACK');
    err_num := SQLCODE;
    err_msg := TO_CHAR (err_num) || '-' || SQLERRM;
    INSERT INTO system_log (text, action, actionresult, username)
         VALUES (err_msg, 'Error', err_num, $$plsql_unit);
    EXECUTE IMMEDIATE ('COMMIT');
    RAISE;
  -- exception handlers and block end here
END VASA_STAT_RM_EXT_TRANSACTION;
/

COMMIT;

PROMPT Done!

SPOOL OFF