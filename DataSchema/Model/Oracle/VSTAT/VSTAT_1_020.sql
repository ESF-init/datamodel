SPOOL VSTAT_1_020.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.020', 'JGI', 'Added update for STAT_TM_TICKETTOGROUP');

--- New table VASA_STAT_TM_TICKETCATEGORY

CREATE TABLE STAT_TM_TICKETTOGROUP
(
  TICKETID       NUMBER(10),
  TICKETGROUPID  NUMBER(10)
);


--- Adjust update procedure

CREATE OR REPLACE PROCEDURE VASA_STAT_TM_TICKETTOGROUP
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);

  -- Because its not to much data we refresh the whole table
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'STAT_TM_TICKETTOGROUP', 'STAT_TM_TICKETTOGROUP',
               (SELECT COUNT(*) FROM STAT_TM_TICKETTOGROUP), 'values before update');  
  DELETE FROM STAT_TM_TICKETTOGROUP;
  INSERT INTO STAT_TM_TICKETTOGROUP 
     (TICKETID,TICKETGROUPID)
      (SELECT TICKETID,TICKETGROUPID
       FROM TM_TICKETTOGROUP@vario_productive);
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'STAT_TM_TICKETTOGROUP', 'STAT_TM_TICKETTOGROUP',
               (SELECT COUNT(*) FROM STAT_TM_TICKETTOGROUP), 'values after update');  
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
  
EXCEPTION
  WHEN OTHERS
  THEN
    EXECUTE IMMEDIATE ('ROLLBACK');
    err_num := SQLCODE;
    err_msg := TO_CHAR (err_num) || '-' || SQLERRM;
    INSERT INTO system_log (text, action, actionresult, username)
         VALUES (err_msg, 'Error', err_num, $$plsql_unit);
    EXECUTE IMMEDIATE ('COMMIT');
    RAISE;
  -- exception handlers and block end here
END VASA_STAT_TM_TICKETTOGROUP;
/

CREATE OR REPLACE PROCEDURE vasa_refresh_OneToOneTables
IS
BEGIN
   INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);
  -- *************************   
      vasa_stat_SL_autoloadSetting;
      vasa_stat_SL_product;
      vasa_stat_SL_paymentoption;
      vasa_stat_SL_contract;
      vasa_stat_SL_order;
      vasa_stat_SL_orderdetail;
      vasa_stat_SL_organization;
      vasa_stat_SL_address;
      vasa_stat_SL_person;      
      vasa_stat_SL_payment;
      vasa_um_device;
      vasa_um_unit;
      VASA_RM_DEVICEPAYMENTMETHOD;
      VASA_TM_TARIF;
      VASA_STAT_TM_TICKETGROUP;
      VASA_STAT_TM_TICKETTOGROUP;
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
END vasa_refresh_OneToOneTables;
/

--- Correct old data
--- This statement is not active as it should not be run automatically

COMMIT;

PROMPT Done!

SPOOL OFF

