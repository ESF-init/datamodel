SPOOL VSTAT_1_036.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.036', 'JGI', 'Added MOBSTATODOMETERID to table STAT_MOBSTAT_ODOMETER');

ALTER TABLE STAT_MOBSTAT_ODOMETER ADD (MOBSTATODOMETERID  NUMBER(10));

CREATE OR REPLACE PROCEDURE VASA_STAT_MOBSTAT_ODOMETER
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
-- *************************
   INSERT INTO system_log
               (action, username
               )
        VALUES ('Start', $$plsql_unit
               );

   BEGIN
-- *************************
-- Count how many values are copied

      -- Count how many values are copied
      INSERT INTO system_log
                  (action, username, content, tablename, actionresult, text)
           VALUES ('Count', $$plsql_unit, 'Odometer', 'MOBSTAT_VARIO_ODOMETER',
                   (SELECT count(*)
            FROM MOBSTAT_VARIO_ODOMETER 
           WHERE NOT EXISTS (SELECT *  FROM STAT_MOBSTAT_ODOMETER b WHERE b.OPD_DATE = MOBSTAT_VARIO_ODOMETER.OPD_DATE AND b.VEHICLE_ID = MOBSTAT_VARIO_ODOMETER.VEHICLE_ID and b.ACT_TIME = MOBSTAT_VARIO_ODOMETER.ACT_TIME)),
                   'Values, which need to be imported ');

  

------------------------------------------------------------------
      INSERT INTO STAT_MOBSTAT_ODOMETER
                  (OPD_DATE,VEHICLE_ID,METERS,ACT_TIME,REASON,METERS_ABSOLUTE,COPILOT_RUNTIME,GPS_LONGITUDE,GPS_LATITUDE,GPS_ALTITUDE,MOBSTATODOMETERID)
         (SELECT OPD_DATE,VEHICLE_ID,METERS,ACT_TIME,REASON,METERS_ABSOLUTE,COPILOT_RUNTIME,GPS_LONGITUDE,GPS_LATITUDE,GPS_ALTITUDE,GETDATAID()
            FROM MOBSTAT_VARIO_ODOMETER 
           WHERE NOT EXISTS (SELECT *  FROM STAT_MOBSTAT_ODOMETER b WHERE b.OPD_DATE = MOBSTAT_VARIO_ODOMETER.OPD_DATE AND b.VEHICLE_ID = MOBSTAT_VARIO_ODOMETER.VEHICLE_ID and b.ACT_TIME = MOBSTAT_VARIO_ODOMETER.ACT_TIME));

      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Finished', $$plsql_unit
                  );

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log
                     (text, action, actionresult, username
                     )
              VALUES (err_msg, 'Error', err_num, $$plsql_unit
                     );

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;                               -- exception handlers and block end here
END VASA_STAT_MOBSTAT_ODOMETER;
/


COMMIT;

-- Update old data

-- UPDATE STAT_MOBSTAT_ODOMETER SET MOBSTATODOMETERID = getdataid() WHERE MOBSTATODOMETERID IS NULL;

PROMPT Done!

SPOOL OFF