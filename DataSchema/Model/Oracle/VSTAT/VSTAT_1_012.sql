SPOOL VSTAT_1_012.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.012', 'JGI', 'Added table STAT_TM_TICKETCATEGORY');


--- Add table STAT_INSPECTOR_LOGON

CREATE TABLE STAT_TM_TICKETCATEGORY
(
  TICKETCATEGORYID  NUMBER(10),
  NUMBER_           NUMBER(10),
  NAME              VARCHAR2(200 BYTE),
  DESCRIPTION       VARCHAR2(200 BYTE),
  CLIENTID          NUMBER(10)
);         

--- Adjust update procedure

CREATE OR REPLACE PROCEDURE VASA_STAT_TM_TICKETCATEGORY
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
-- *************************
      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Start', $$plsql_unit
                  ); 


-- *************************
-- Seeing there are not many depots (< 100.000) we refresh the whole tables
      DELETE FROM STAT_TM_TICKETCATEGORY;

-- Copy all depots
      INSERT INTO STAT_TM_TICKETCATEGORY
                  (TICKETCATEGORYID,NUMBER_,NAME,DESCRIPTION,CLIENTID )
         (SELECT TICKETCATEGORYID,NUMBER_,NAME,DESCRIPTION,CLIENTID 
            FROM TM_TICKETCATEGORY@vario_productive);

-- Count how many values are copied
      INSERT INTO system_log
                  (text, action, content, tablename,
                   actionresult, username
                  )
           VALUES ('Ready to import values', 'Count', 'Depots', 'VARIO_DEPOT',
                   (SELECT COUNT (*)
                      FROM STAT_TM_TICKETCATEGORY), $$plsql_unit
                  );

-- *************************
      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Finished', $$plsql_unit
                  );

-- *************************
      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log
                     (text, action, actionresult, username
                     )
              VALUES (err_msg, 'Error', err_num, $$plsql_unit
                     );

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;                               -- exception handlers and block end here
END VASA_STAT_TM_TICKETCATEGORY;
/

CREATE OR REPLACE PROCEDURE VASA_REFRESH
IS
   err_num   NUMBER;

   err_msg   VARCHAR2 (200);
   v_lockTimeFrame number := 1440/1440;
   v_runID NUMBER(18);
   v_runCnt NUMBER;
BEGIN
-- *************************
   INSERT INTO system_log (action, username, text)
        VALUES ('Start', $$plsql_unit, '**** Masterscipt started ****');

--------------------------------------------------------------------------------
-- Initialise protection against multiple executing
--------------------------------------------------------------------------------  
   SELECT NVL(max(runID),1) INTO v_runID FROM sys_runstat;
   v_runID := v_runID + 1;        
   BEGIN 
     SELECT count(*) INTO v_runCnt FROM sys_runstat 
      WHERE runName = $$plsql_unit
        AND lockTime > sysdate;
     IF v_runCnt > 0 THEN
       INSERT INTO sys_runstat(runID, runName, startTime, endTime, result)
         VALUES (v_runID, $$plsql_unit, sysdate, sysdate, 'detected running instance and stopped');     
       RETURN;
     ELSE
       INSERT INTO sys_runstat(runID, runName, startTime, lockTime)
         VALUES (v_runID, $$plsql_unit, sysdate, sysdate+v_lockTimeFrame);     
     END IF;     
   END;
   
--------------------------------------------------------------------------------
-- VASA_preparations: Job to prepare several values for other jobs
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
    vasa_preparations;
   END;

--------------------------------------------------------------------------------
-- Jobs to update the 1to1-tables
-- at the very beginning because they are independent from others and may be referred from others
-- Requires other job to run:    none
-- Job type: full refresh
--------------------------------------------------------------------------------
   BEGIN
     vasa_refresh_OneToOneTables;
   END;
   
--------------------------------------------------------------------------------
-- vasa_stat_sl_photograph: Job to prepare STAT_Photograph tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
     vasa_stat_sl_photograph;
   END;
   
--------------------------------------------------------------------------------
-- VASA_CONTRACTS: Job to prepare STAT_Contracts tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
     vasa_contracts;
   END;

--------------------------------------------------------------------------------
-- VASA_DEBTOR: Job to prepare STAT_dm_debtor tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_debtor;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_FARESTAGES: Job the prepare the STAT_TM_FARESTAGENAMES
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_tm_farestages;
   END;

--------------------------------------------------------------------------------
-- VASA_CARDS: Job to prepare STAT_CARD tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_cards;
   END;

--------------------------------------------------------------------------------
-- VASA_STAT_RM_SHIFT: Job the prepare the STAT_RM_SHIFT
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update
--------------------------------------------------------------------------------
   BEGIN
      vasa_stat_rm_shift;
   END;

--------------------------------------------------------------------------------
-- VASA_STAT_RM_TRANSACTIONS: Job the prepare the STAT_RM_TRASNACTIONS
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_TM_FARESTAGES (for the function to get the farestage names)
--------------------------------------------------------------------------------
   BEGIN
     vasa_stat_rm_transactions;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_TABLES: Job the prepare all the STAT_TM_* tables
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_STAT_RM_TRANSACTIONS (to get all the used ticketids)
--------------------------------------------------------------------------------
   BEGIN
     vasa_tm_tables;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_TABLES: Job the prepare all the STAT_TM_* tables
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_STAT_RM_TRANSACTIONS (to get all the used line and linenames)
--------------------------------------------------------------------------------
   BEGIN
      vasa_line;
   END;

--------------------------------------------------------------------------------
-- vasa_postprocessing: Job for cleaning up
-- Requires other job to run:    none
-- Job type: Clening job for other processes
--------------------------------------------------------------------------------
   BEGIN
      vasa_postprocessing;
   END;

--------------------------------------------------------------------------------
-- vasa_depot: Job to update master data DEPOT
-- Requires other job to run:    none
-- Job type: Full table refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_depot;
   END;

--------------------------------------------------------------------------------
-- vasa_card: Job to update master data CARDACTIONREQUEST
-- Requires other job to run:    none
-- Job type: Full table refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_stat_pp_cardactionrequest;
   END;

--------------------------------------------------------------------------------
-- vasa_ACCOUNTENTRYID: Job to update the accountentries of the live db
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_accountentryid;
   END;

--------------------------------------------------------------------------------
-- VASA_DM_CARDS: Job to update the DM_CARDs of the live db
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_dm_cards;
   END;

--------------------------------------------------------------------------------
-- VASA_STOP: Job to update the STAT_VARIO_STOP
-- Requires other job to run:    none
-- Job type: full refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_stop;
   END;

--------------------------------------------------------------------------------
-- VASA_PP_PRODUCTONCARD: Job to update the stat_pp_productoncard
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_pp_productoncard;
   END;

--------------------------------------------------------------------------------
-- VASA_RM_BalancePoint: Job to decide whether and in case create a new balance point
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_RM_BalancePoint;
   END;
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: UPdates the inspector logons
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_STAT_INSPECTOR_LOGON;
   END;   
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: Updates the STAT_MOBSTAT_TRIPS table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_STAT_MOBSTAT_TRIPS;
   END;   
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: Updates the STAT_RM_EXTERNALDATA table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------   
   BEGIN
      VASA_STAT_RM_EXTERNALDATA;
   END;
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: Updates the STAT_TM_TICKETCATEGORY table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------   
   BEGIN
      VASA_STAT_TM_TICKETCATEGORY;
   END;
   
   
   
--------------------------------------------------------------------------------
-- VASA_CLEAN_PREVIOUSIMPORTED: Job to clean values which are stored for a reimport and statisc overview
--                              A parameter in this procedure describes how many days are kept in the sysstem (Standard:30)
-- Requires other job to run:    none
-- Job type: Cleaning procedure
--------------------------------------------------------------------------------
   /*BEGIN
      --vasa_clean_previousimported; Is started in a second script once a day
   END;*/
   
--------------------------------------------------------------------------------
-- Finalise protection against multiple executing (unlock)
--------------------------------------------------------------------------------  
   BEGIN
     UPDATE sys_runstat 
        SET lockTime = NULL, endTime = sysdate, result = 'finished'
      WHERE runID = v_runID;
   END;    

   INSERT INTO system_log (action, username, text)
        VALUES ('Finished', $$plsql_unit, '*** Masterscript finished ***');

   EXECUTE IMMEDIATE ('COMMIT');
END vasa_refresh;
/




--- Correct old data
--- This statement is not active as it should not be run automatically


COMMIT;

PROMPT Done!

SPOOL OFF
