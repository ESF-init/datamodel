SPOOL VSTAT_1_008.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.008', 'JGI', 'Added table STAT_MOBSTAT_TRIPS');


--- Add table STAT_INSPECTOR_LOGON

CREATE TABLE STAT_MOBSTAT_TRIPS
(
  STARTTIME_TRIP_HHMMSS    VARCHAR2(4000 BYTE),
  ENDTIME_TRIP_HHMMSS      VARCHAR2(4000 BYTE),
  DRIVER_LOGONTIME_HHMMSS  VARCHAR2(4000 BYTE),
  SEQNO                    NUMBER,
  OPERATINGDAY_DATE        DATE,
  STARTTIME_TRIP_RAW       NUMBER,
  ENDTIME_TRIP_RAW         NUMBER,
  STARTTIME_BLOCK_RAW      NUMBER,
  RUNNINGBOARD             VARCHAR2(4000 BYTE),
  DEBTORNO                 NUMBER,
  VEHICLENO                VARCHAR2(4000 BYTE),
  DRIVER_LOGONTIME_RAW     NUMBER,
  SERVICE_TYPE             VARCHAR2(256 BYTE)   DEFAULT NULL,
  DIRECTION                VARCHAR2(256 BYTE)   DEFAULT 'none',
  PATTERN                  VARCHAR2(4000 BYTE)
);

--- Adjust update procedure

CREATE OR REPLACE PROCEDURE VASA_STAT_MOBSTAT_TRIPS
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
-- *************************
   INSERT INTO system_log
               (action, username
               )
        VALUES ('Start', $$plsql_unit
               );

   BEGIN
-- *************************
-- Count how many values are copied

      -- Count how many values are copied
      INSERT INTO system_log
                  (action, username, content, tablename, actionresult, text)
           VALUES ('Count', $$plsql_unit, 'Shift', 'POST_SHIFTID',
                   (SELECT COUNT (UNIQUE (event_no)) FROM MOBSTAT_VARIO_TRIPS
                     WHERE event_no NOT IN (SELECT SEQNO FROM STAT_MOBSTAT_TRIPS)),
                   'Values, which need to be imported ');

  

------------------------------------------------------------------
      INSERT INTO STAT_MOBSTAT_TRIPS
                  (STARTTIME_TRIP_HHMMSS,ENDTIME_TRIP_HHMMSS,DRIVER_LOGONTIME_HHMMSS,SEQNO,OPERATINGDAY_DATE,STARTTIME_TRIP_RAW,ENDTIME_TRIP_RAW,STARTTIME_BLOCK_RAW,RUNNINGBOARD,DEBTORNO,VEHICLENO,DRIVER_LOGONTIME_RAW,SERVICE_TYPE,DIRECTION,PATTERN)
         (SELECT ACT_DEP_TIME_TRIP_HHMMSS,ACT_END_TIME_TRIP_HHMMSS,ACT_DEP_TIME_DRVR_HHMMSS,EVENT_NO,OPD_DATE,ACT_DEP_TIME_TRIP,ACT_END_TIME_TRIP,ACT_DEP_TIME_BLOCK,BLOCK_CODE,DRIVER_ID,VEHICLE_NAME,ACT_DEP_TIME_DRVR,TRIP_ROLE,PATTERN_DIRECTION,PATTERN
            FROM MOBSTAT_VARIO_TRIPS
           WHERE  event_no NOT IN (SELECT seqno FROM STAT_MOBSTAT_TRIPS));

      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Finished', $$plsql_unit
                  );

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log
                     (text, action, actionresult, username
                     )
              VALUES (err_msg, 'Error', err_num, $$plsql_unit
                     );

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;                               -- exception handlers and block end here
END VASA_STAT_MOBSTAT_TRIPS;
/

CREATE OR REPLACE PROCEDURE VASA_REFRESH
IS
   err_num   NUMBER;

   err_msg   VARCHAR2 (200);
   v_lockTimeFrame number := 1440/1440;
   v_runID NUMBER(18);
   v_runCnt NUMBER;
BEGIN
-- *************************
   INSERT INTO system_log (action, username, text)
        VALUES ('Start', $$plsql_unit, '**** Masterscipt started ****');

--------------------------------------------------------------------------------
-- Initialise protection against multiple executing
--------------------------------------------------------------------------------  
   SELECT NVL(max(runID),1) INTO v_runID FROM sys_runstat;
   v_runID := v_runID + 1;        
   BEGIN 
     SELECT count(*) INTO v_runCnt FROM sys_runstat 
      WHERE runName = $$plsql_unit
        AND lockTime > sysdate;
     IF v_runCnt > 0 THEN
       INSERT INTO sys_runstat(runID, runName, startTime, endTime, result)
         VALUES (v_runID, $$plsql_unit, sysdate, sysdate, 'detected running instance and stopped');     
       RETURN;
     ELSE
       INSERT INTO sys_runstat(runID, runName, startTime, lockTime)
         VALUES (v_runID, $$plsql_unit, sysdate, sysdate+v_lockTimeFrame);     
     END IF;     
   END;
   
--------------------------------------------------------------------------------
-- VASA_preparations: Job to prepare several values for other jobs
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
    vasa_preparations;
   END;

--------------------------------------------------------------------------------
-- Jobs to update the 1to1-tables
-- at the very beginning because they are independent from others and may be referred from others
-- Requires other job to run:    none
-- Job type: full refresh
--------------------------------------------------------------------------------
   BEGIN
     vasa_refresh_OneToOneTables;
   END;
   
--------------------------------------------------------------------------------
-- vasa_stat_sl_photograph: Job to prepare STAT_Photograph tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
     vasa_stat_sl_photograph;
   END;
   
--------------------------------------------------------------------------------
-- VASA_CONTRACTS: Job to prepare STAT_Contracts tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
     vasa_contracts;
   END;

--------------------------------------------------------------------------------
-- VASA_DEBTOR: Job to prepare STAT_dm_debtor tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_debtor;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_FARESTAGES: Job the prepare the STAT_TM_FARESTAGENAMES
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_tm_farestages;
   END;

--------------------------------------------------------------------------------
-- VASA_CARDS: Job to prepare STAT_CARD tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_cards;
   END;

--------------------------------------------------------------------------------
-- VASA_STAT_RM_SHIFT: Job the prepare the STAT_RM_SHIFT
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update
--------------------------------------------------------------------------------
   BEGIN
      vasa_stat_rm_shift;
   END;

--------------------------------------------------------------------------------
-- VASA_STAT_RM_TRANSACTIONS: Job the prepare the STAT_RM_TRASNACTIONS
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_TM_FARESTAGES (for the function to get the farestage names)
--------------------------------------------------------------------------------
   BEGIN
     vasa_stat_rm_transactions;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_TABLES: Job the prepare all the STAT_TM_* tables
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_STAT_RM_TRANSACTIONS (to get all the used ticketids)
--------------------------------------------------------------------------------
   BEGIN
     vasa_tm_tables;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_TABLES: Job the prepare all the STAT_TM_* tables
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_STAT_RM_TRANSACTIONS (to get all the used line and linenames)
--------------------------------------------------------------------------------
   BEGIN
      vasa_line;
   END;

--------------------------------------------------------------------------------
-- vasa_postprocessing: Job for cleaning up
-- Requires other job to run:    none
-- Job type: Clening job for other processes
--------------------------------------------------------------------------------
   BEGIN
      vasa_postprocessing;
   END;

--------------------------------------------------------------------------------
-- vasa_depot: Job to update master data DEPOT
-- Requires other job to run:    none
-- Job type: Full table refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_depot;
   END;

--------------------------------------------------------------------------------
-- vasa_card: Job to update master data CARDACTIONREQUEST
-- Requires other job to run:    none
-- Job type: Full table refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_stat_pp_cardactionrequest;
   END;

--------------------------------------------------------------------------------
-- vasa_ACCOUNTENTRYID: Job to update the accountentries of the live db
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_accountentryid;
   END;

--------------------------------------------------------------------------------
-- VASA_DM_CARDS: Job to update the DM_CARDs of the live db
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_dm_cards;
   END;

--------------------------------------------------------------------------------
-- VASA_STOP: Job to update the STAT_VARIO_STOP
-- Requires other job to run:    none
-- Job type: full refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_stop;
   END;

--------------------------------------------------------------------------------
-- VASA_PP_PRODUCTONCARD: Job to update the stat_pp_productoncard
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_pp_productoncard;
   END;

--------------------------------------------------------------------------------
-- VASA_RM_BalancePoint: Job to decide whether and in case create a new balance point
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_RM_BalancePoint;
   END;
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: UPdates the inspector logons
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_STAT_INSPECTOR_LOGON;
   END;   
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: Updates the STAT_MOBSTAT_TRIPS table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_STAT_MOBSTAT_TRIPS;
   END;   
   
--------------------------------------------------------------------------------
-- VASA_CLEAN_PREVIOUSIMPORTED: Job to clean values which are stored for a reimport and statisc overview
--                              A parameter in this procedure describes how many days are kept in the sysstem (Standard:30)
-- Requires other job to run:    none
-- Job type: Cleaning procedure
--------------------------------------------------------------------------------
   /*BEGIN
      --vasa_clean_previousimported; Is started in a second script once a day
   END;*/
   
--------------------------------------------------------------------------------
-- Finalise protection against multiple executing (unlock)
--------------------------------------------------------------------------------  
   BEGIN
     UPDATE sys_runstat 
        SET lockTime = NULL, endTime = sysdate, result = 'finished'
      WHERE runID = v_runID;
   END;    

   INSERT INTO system_log (action, username, text)
        VALUES ('Finished', $$plsql_unit, '*** Masterscript finished ***');

   EXECUTE IMMEDIATE ('COMMIT');
END vasa_refresh;
/




--- Correct old data
--- This statement is not active as it should not be run automatically


COMMIT;

PROMPT Done!

SPOOL OFF
