SPOOL VSTAT_1_006.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.006', 'JGI', 'Added attributes to STAT_RM_SHIFT');


--- Extend table with column EXTERNALNAME

ALTER TABLE STAT_RM_SHIFT
 ADD (DEBITACCOUNT  NUMBER(10));


--- Adjust update procedure

CREATE OR REPLACE PROCEDURE VASA_STAT_RM_SHIFT
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
-- *************************
   INSERT INTO system_log
               (action, username
               )
        VALUES ('Start', $$plsql_unit
               );

   BEGIN
-- *************************
-- Count how many values are copied
      INSERT INTO system_log
                  (action, username, content, tablename,
                   actionresult, text
                  )
           VALUES ('Count', $$plsql_unit, 'Shift', 'STAT_RM_SHIFT',
                   (SELECT COUNT (*)
                      FROM post_shiftid), 'Ready to import values'
                  );

      -- Count how many values are copied
      INSERT INTO system_log
                  (action, username, content, tablename,
                   actionresult,
                   text
                  )
           VALUES ('Count', $$plsql_unit, 'Shift', 'POST_SHIFTID',
                   (SELECT COUNT (UNIQUE (shiftid))
                      FROM stat_rm_shift
                     WHERE stat_rm_shift.shiftid IN (SELECT shiftid
                                                       FROM post_shiftid)),
                   'Values already imported which need to be updated (reimport)'
                  );

      DELETE FROM stat_rm_shift
            WHERE stat_rm_shift.shiftid IN (SELECT shiftid
                                              FROM post_shiftid);

------------------------------------------------------------------
      INSERT INTO stat_rm_shift
                  (shiftid, shiftstateid, shiftbegin, shiftend, cardno,
                   debtorno, locationno, deviceno, devicetype, lineno,
                   importdate, exportdate, exportstate, routeno,
                   firstticketno, lastticketno, salesvolume, cashreceipt,
                   isdst, hashvalue, clientid, balanceno, shiftnumber,
                   shiftcounter, depositnumber, logoffdebtor,
                   cancellationcount, cancellationamount,debitaccount)
         (SELECT rm_shift.shiftid, rm_shift.shiftstateid, rm_shift.shiftbegin,
                 rm_shift.shiftend, rm_shift.cardno, rm_shift.debtorno,
                 rm_shift.locationno, rm_shift.deviceno, rm_shift.devicetype,
                 rm_shift.lineno, rm_shift.importdate, rm_shift.exportdate,
                 rm_shift.exportstate, rm_shift.routeno,
                 rm_shift.firstticketno, rm_shift.lastticketno,
                 rm_shift.salesvolume, rm_shift.cashreceipt, rm_shift.isdst,
                 rm_shift.hashvalue, rm_shift.clientid, rm_shift.balanceno,
                 rm_shift.shiftnumber, rm_shift.shiftcounter,
                 rm_shift.depositnumber, rm_shift.logoffdebtor,
                 rm_shift.cancellationcount, rm_shift.cancellationamount, rm_shift.debitaccount
            FROM post_shiftid, rm_shift@vario_productive
           WHERE post_shiftid.shiftid = rm_shift.shiftid);

      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Finished', $$plsql_unit
                  );

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log
                     (text, action, actionresult, username
                     )
              VALUES (err_msg, 'Error', err_num, $$plsql_unit
                     );

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;                               -- exception handlers and block end here
END vasa_stat_rm_shift;
/



--- Correct old data
--- This statement is not active as it should not be run automatically

update stat_rm_shift a set a.debitaccount = (select b.debitaccount from rm_shift@vario_productive b where b.shiftid = a.shiftid ) 
where a.shiftid in (select shiftid from rm_shift@vario_productive);

COMMIT;

PROMPT Done!

SPOOL OFF
