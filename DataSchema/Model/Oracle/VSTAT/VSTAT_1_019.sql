SPOOL VSTAT_1_019.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.019', 'JGI', 'Added update for STAT_TM_TICKETGROUP');

--- New table VASA_STAT_TM_TICKETCATEGORY

CREATE TABLE STAT_TM_TICKETGROUP
(
  TICKETGROUPID   NUMBER(10),
  TARIFFID        NUMBER(10)                    NOT NULL,
  EXTERNALNUMBER  NUMBER(10),
  DESCRIPTION     VARCHAR2(100 BYTE)
);


--- Adjust update procedure

CREATE OR REPLACE PROCEDURE VASA_STAT_TM_TICKETGROUP
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);

  -- Because its not to much data we refresh the whole table
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'STAT_TM_TICKETGROUP', 'STAT_TM_TICKETGROUP',
               (SELECT COUNT(*) FROM STAT_TM_TICKETGROUP), 'values before update');  
  DELETE FROM STAT_TM_TICKETGROUP;
  INSERT INTO STAT_TM_TICKETGROUP 
     (TICKETGROUPID,TARIFFID,EXTERNALNUMBER,DESCRIPTION)
      (SELECT TICKETGROUPID,TARIFFID,EXTERNALNUMBER,DESCRIPTION
       FROM TM_TICKETGROUP@vario_productive);
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'STAT_TM_TICKETGROUP', 'STAT_TM_TICKETGROUP',
               (SELECT COUNT(*) FROM STAT_TM_TICKETGROUP), 'values after update');  
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
  
EXCEPTION
  WHEN OTHERS
  THEN
    EXECUTE IMMEDIATE ('ROLLBACK');
    err_num := SQLCODE;
    err_msg := TO_CHAR (err_num) || '-' || SQLERRM;
    INSERT INTO system_log (text, action, actionresult, username)
         VALUES (err_msg, 'Error', err_num, $$plsql_unit);
    EXECUTE IMMEDIATE ('COMMIT');
    RAISE;
  -- exception handlers and block end here
END VASA_STAT_TM_TICKETGROUP;
/

CREATE OR REPLACE PROCEDURE vasa_refresh_OneToOneTables
IS
BEGIN
   INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);
  -- *************************   
      vasa_stat_SL_autoloadSetting;
      vasa_stat_SL_product;
      vasa_stat_SL_paymentoption;
      vasa_stat_SL_contract;
      vasa_stat_SL_order;
      vasa_stat_SL_orderdetail;
      vasa_stat_SL_organization;
      vasa_stat_SL_address;
      vasa_stat_SL_person;      
      vasa_stat_SL_payment;
      vasa_um_device;
      vasa_um_unit;
      VASA_RM_DEVICEPAYMENTMETHOD;
      VASA_TM_TARIF;
      VASA_STAT_TM_TICKETGROUP;
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
END vasa_refresh_OneToOneTables;
/

--- Correct old data
--- This statement is not active as it should not be run automatically

COMMIT;

PROMPT Done!

SPOOL OFF

