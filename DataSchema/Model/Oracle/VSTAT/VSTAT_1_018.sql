SPOOL VSTAT_1_018.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.018', 'JGI', 'Added update for TM_TARIF');

--- New table VASA_STAT_TM_TICKETCATEGORY

CREATE TABLE VSTAT_TM_TARIF
(
  VALIDFROM          DATE                       NOT NULL,
  TARIFID            NUMBER(10)                 NOT NULL,
  VALIDUNTIL         DATE,
  VERSION            NUMBER(10)                 NOT NULL,
  STATE              NUMBER(3)                  NOT NULL,
  CLIENTID           NUMBER(10),
  DEVICECLASSID      NUMBER(10),
  DESCRIPTION        VARCHAR2(200 BYTE),
  NETID              NUMBER(10),
  RELEASECOUNTER     NUMBER(10),
  OWNERCLIENTID      NUMBER(10),
  EXTERNALVERSION    VARCHAR2(10 BYTE),
  BASETARIFFID       NUMBER(10),
  MATRIXTARIFFID     NUMBER(10),
  EXPORTSTATE        NUMBER(10),
  SUBRELEASECOUNTER  NUMBER(18)                 DEFAULT 1                     NOT NULL,
  VALIDFROMTIME      NUMBER(9)
);

--- Adjust update procedure

CREATE OR REPLACE PROCEDURE VASA_TM_TARIF
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);

  -- Because its not to much data we refresh the whole table
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'VSTAT_TM_TARIF', 'VSTAT_TM_TARIF',
               (SELECT COUNT(*) FROM VSTAT_TM_TARIF), 'values before update');  
  DELETE FROM VSTAT_TM_TARIF;
  INSERT INTO VSTAT_TM_TARIF 
     (VALIDFROM,TARIFID,VALIDUNTIL,VERSION,STATE,CLIENTID,DEVICECLASSID,DESCRIPTION,NETID,RELEASECOUNTER,OWNERCLIENTID,EXTERNALVERSION,BASETARIFFID,MATRIXTARIFFID,EXPORTSTATE,SUBRELEASECOUNTER,VALIDFROMTIME)
      (SELECT VALIDFROM,TARIFID,VALIDUNTIL,VERSION,STATE,CLIENTID,DEVICECLASSID,DESCRIPTION,NETID,RELEASECOUNTER,OWNERCLIENTID,EXTERNALVERSION,BASETARIFFID,MATRIXTARIFFID,EXPORTSTATE,SUBRELEASECOUNTER,VALIDFROMTIME
       FROM TM_TARIF@vario_productive);
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'VSTAT_TM_TARIF', 'VSTAT_TM_TARIF',
               (SELECT COUNT(*) FROM VSTAT_TM_TARIF), 'values after update');  
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
  
EXCEPTION
  WHEN OTHERS
  THEN
    EXECUTE IMMEDIATE ('ROLLBACK');
    err_num := SQLCODE;
    err_msg := TO_CHAR (err_num) || '-' || SQLERRM;
    INSERT INTO system_log (text, action, actionresult, username)
         VALUES (err_msg, 'Error', err_num, $$plsql_unit);
    EXECUTE IMMEDIATE ('COMMIT');
    RAISE;
  -- exception handlers and block end here
END VASA_TM_TARIF;
/

CREATE OR REPLACE PROCEDURE vasa_refresh_OneToOneTables
IS
BEGIN
   INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);
  -- *************************   
      vasa_stat_SL_autoloadSetting;
      vasa_stat_SL_product;
      vasa_stat_SL_paymentoption;
      vasa_stat_SL_contract;
      vasa_stat_SL_order;
      vasa_stat_SL_orderdetail;
      vasa_stat_SL_organization;
      vasa_stat_SL_address;
      vasa_stat_SL_person;      
      vasa_stat_SL_payment;
      vasa_um_device;
      vasa_um_unit;
      VASA_RM_DEVICEPAYMENTMETHOD;
      VASA_TM_TARIF;
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
END vasa_refresh_OneToOneTables;
/

--- Correct old data
--- This statement is not active as it should not be run automatically

COMMIT;

PROMPT Done!

SPOOL OFF

