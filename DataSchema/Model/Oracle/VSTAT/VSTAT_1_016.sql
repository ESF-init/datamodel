SPOOL VSTAT_1_016.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.016', 'JGI', 'Added attributes FROMFARESTAGE,TOFARESTAGE to table STAT_RM_TRANSACTIONS');

--- New function

CREATE OR REPLACE FUNCTION getRegionByTariffZone (
   fromtarifzome   NUMBER,
   linename      VARCHAR,
   tariffid      NUMBER
)
   RETURN VARCHAR
IS
   region          VARCHAR (100);
   NO_DATA_FOUND   EXCEPTION;
BEGIN
   --v_number := to_number(str);
   SELECT a.shortname
     INTO region
     FROM tm_farestage@vario_productive a, tm_farematrix@vario_productive b
    WHERE a.farestagelistid = b.farestagelistid 
      AND a.number_ = fromtarifzome
      AND b.tarifid = tariffid
      AND b.NAME = linename;

   RETURN region;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN '-';
   WHEN OTHERS
   THEN
      RETURN NULL;
END getRegionByTariffZone;
/

--- Adjust update procedure

--- Adjust update procedure

CREATE OR REPLACE PROCEDURE VASA_STAT_RM_TRANSACTIONS
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
-- *************************
      INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);

--------------------------------------------------------------------------------
-- zu bearbeitende IDS aus StatistikDB l�schen
-- *************************
-- Count how many values are copied
      INSERT INTO system_log
                  (action, username, content, tablename, actionresult, text)
           VALUES ('Count', $$plsql_unit, 'Shift', 'POST_SHIFTID',
                   (SELECT COUNT (*) FROM post_shiftid), 'Ready to import values');

      -- Count how many values are copied
      INSERT INTO system_log
                  (action, username, content, tablename, actionresult, text)
           VALUES ('Count', $$plsql_unit, 'Shift', 'POST_SHIFTID',
                   (SELECT COUNT (UNIQUE (shiftid)) FROM stat_rm_transactions
                     WHERE stat_rm_transactions.shiftid IN (SELECT shiftid FROM post_shiftid)),
                   'Values already imported which need to be updated (reimport)');

      DECLARE
         v_cntLeft     NUMBER;
         v_stepCnt     NUMBER;
         v_cntDone     NUMBER;
      BEGIN
        v_stepCnt := 1000;
        v_CntDone := 0;
        SELECT count(*) INTO v_cntLeft FROM post_shiftid;

        WHILE (v_cntLeft > 0) LOOP
        -- ********** Now the work begins *********
        DELETE FROM stat_rm_transactions
            WHERE stat_rm_transactions.shiftid IN 
              (SELECT shiftid FROM 
                (SELECT shiftid, rownum rn FROM post_shiftid ORDER BY shiftID)
                  WHERE rn >  v_CntDone
                    AND rn <= v_CntDone+v_stepCnt);

        INSERT INTO stat_rm_transactions
                  (accountentryid, allocatedamount, bestpricerunid,
                   cancellationid, cappingreason, cardbalance, cardchargetan,
                   cardcredit, customercardid, cardreftransactionno,
                   cardtransactionno, clearingid, clearingstate, clientid,
                   courseno, devicebookingstate, devicepaymentmethod,
                   deviceno, direction, factor, fromstopno, fromtariffzone,
                   importdatetime, line, linename, maxdistance, noadults,
                   nochildren, nouser1, nouser2, nouser3, nouser4, paycardno,
                   paycardnumber, payprintedcardno, payPrintedCardnoText, price, pricelevel,
                   routeno, saleschannel, serviceid, shiftid, state, stopfrom,
                   stopto, tripdatetime, transactionid, totariffzone,
                   tripserialno, tariffid, typeid, valueofride, viatariffzone,
                   transactiontypetext, cashreceipt, debtorcardno, locationno,
                   debtorno, devicetype, salesvolume, shiftbegin, shiftend,
                   shiftstateid, text, isbooked, devicebookingstatetext,
                   devicebookingstateid, externalnumber, internalnumber,
                   categoryid, name, ticketid, tarifid, validfrom, version,
                   fromfarestage, tofarestage)
         (SELECT rm_transaction.accountentryid,
                 rm_transaction.allocatedamount,
                 rm_transaction.bestpricerunid, rm_transaction.cancellationid,
                 rm_transaction.cappingreason, rm_transaction.cardbalance,
                 rm_transaction.cardchargetan, rm_transaction.cardcredit,
                 rm_transaction.cardid AS customercardid,
                 rm_transaction.cardreftransactionno,
                 rm_transaction.cardtransactionno, rm_transaction.clearingid,
                 rm_transaction.clearingstate, rm_transaction.clientid,
                 rm_transaction.courseno, rm_transaction.devicebookingstate,
                 rm_transaction.devicepaymentmethod, rm_transaction.deviceno,
                 rm_transaction.direction, rm_transaction.factor,
                 rm_transaction.fromstopno, rm_transaction.fromtariffzone,
                 rm_transaction.importdatetime, rm_transaction.line,
                 NVL (rm_transaction.linename, '0') AS linename,
                 rm_transaction.maxdistance, rm_transaction.noadults,
                 rm_transaction.nochildren, rm_transaction.nouser1,
                 rm_transaction.nouser2, rm_transaction.nouser3, rm_transaction.nouser4, 
                 rm_transaction.paycardno, rm_transaction.paycardno AS paycardnumber,
                 get_printedcardnumber_byid (rm_transaction.cardid) AS payprintedcardno,
                 get_printedcardtext_byid (rm_transaction.cardid) AS payPrintedCardnoText,
                 rm_transaction.price, rm_transaction.pricelevel,
                 rm_transaction.routeno, rm_transaction.saleschannel,
                 rm_transaction.serviceid, rm_transaction.shiftid,
                 rm_transaction.state, rm_transaction.stopfrom,
                 rm_transaction.stopto, rm_transaction.tripdatetime,
                 rm_transaction.transactionid, rm_transaction.totariffzone,
                 rm_transaction.tripserialno, rm_transaction.tariffid,
                 rm_transaction.typeid, rm_transaction.valueofride,
                 rm_transaction.viatariffzone,
                 rm_transactiontype.typename AS transactiontypetext,
                 rm_shift.cashreceipt, rm_shift.cardno AS debtorcardno,
                 rm_shift.locationno, rm_shift.debtorno, rm_shift.devicetype,
                 rm_shift.salesvolume, rm_shift.shiftbegin, rm_shift.shiftend,
                 rm_shift.shiftstateid, rm_devicebookingstate.text,  -- text obsolet; replaced by devicebookingstatetext
                 rm_devicebookingstate.isbooked,
                 rm_devicebookingstate.text AS devicebookingstatetext,
                 rm_devicebookingstate.devicebookingstateid,
                 tm_ticket.externalnumber, tm_ticket.internalnumber,
                 tm_ticket.categoryid, tm_ticket.name, tm_ticket.ticketid,
                 tm_tarif.tarifid, tm_tarif.validfrom, tm_tarif.version, -- better tariffVersion
                 getRegionByTariffZone (fromtariffzone,
                                   linename,
                                   tariffid
                                  ) AS fromfarestage,
                 getRegionByTariffZone (totariffzone,
                                   linename,
                                   tariffid
                                  ) AS tofarestage
            FROM rm_transaction@vario_productive,
                 (SELECT shiftid FROM 
                    (SELECT shiftID, rownum rn FROM post_shiftid ORDER BY shiftID)  
                    WHERE rn >  v_CntDone
                      AND rn <= v_CntDone+v_stepCnt) shiftIDs,        -- shiftIDs instead of post_shiftID
                 rm_shift@vario_productive,
                 rm_devicebookingstate@vario_productive,
                 tm_ticket@vario_productive,
                 tm_tarif@vario_productive,
                 rm_transactiontype@vario_productive
           WHERE rm_shift.shiftid = rm_transaction.shiftid
             AND shiftIDs.shiftid = rm_shift.shiftid                -- shiftIDs instead of post_shiftID
             AND rm_devicebookingstate.devicebookingno = rm_transaction.devicebookingstate
             AND tm_tarif.tarifid = tm_ticket.tarifid
             AND tm_tarif.tarifid = rm_transaction.tariffid
             AND tm_ticket.internalnumber = rm_transaction.tikno
             AND rm_transaction.typeid = rm_transactiontype.typeid);

          -- ********** work ends; looporganization again*********
          v_cntLeft := v_cntLeft - v_stepCnt;  
          if (v_cntLeft < 0) then 
            v_stepCnt := v_stepCnt + v_cntLeft;
            v_cntLeft := 0; 
          end if;
          v_cntDone := v_cntDone + v_stepCnt;
                  
          INSERT INTO system_log (action, username, content, text)
            VALUES ('Insert', $$plsql_unit, 'Shift', ' '
                   || to_char(v_stepCnt) || ' shifts imported. '
                   || to_char(v_cntLeft) || ' shifts up to import.');
          COMMIT;        -- to preserve the rollbacksegment
          
        END LOOP;       
      END;                 
-- *************************
      VASA_RM_paymentDetails;        -- add the paymentdetailinfo to the transactions
      --VASA_RM_TA_balanceChange;        -- handle the logicalCardBalanceChanges
-- *************************
      INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
      COMMIT;
   
   EXCEPTION
      WHEN OTHERS
      THEN
        ROLLBACK;
        err_num := SQLCODE;
        err_msg := SUBSTR (SQLERRM, 1, 200);
        INSERT INTO system_log (text, action, actionresult, username)
           VALUES (err_msg, 'Error', err_num, $$plsql_unit);
        COMMIT;
        RAISE; 
   END;                               -- exception handlers and block end here
END vasa_stat_rm_transactions;
/


--- Correct old data
--- This statement is not active as it should not be run automatically

---UPDATE STAT_RM_TRANSACTIONS a set A.FROMFARESTAGE = getRegionByTariffZone (a.fromtariffzone, a.linename, a.tariffid);
---UPDATE STAT_RM_TRANSACTIONS a set A.TOFARESTAGE= getRegionByTariffZone (a.TOtariffzone, a.linename, a.tariffid);

COMMIT;

PROMPT Done!

SPOOL OFF
