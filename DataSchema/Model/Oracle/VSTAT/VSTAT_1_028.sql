SPOOL VSTAT_1_027.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.028', 'JGI', 'Added table VSTAT_ADMIN_ROWCHECK and handling');

----CREATE table

CREATE TABLE VSTAT_ADMIN_ROWCHECK
(
  CREATIONDATE       DATE                       DEFAULT sysdate,
  NUMBERROWS_MOBILE  NUMBER(10),
  NUMBERROWS_VSTAT   NUMBER(10),
  TABLENAME          VARCHAR2(40 BYTE),
  DIFFERENCE         NUMBER(10)
);


--- Update procedures

CREATE OR REPLACE PROCEDURE VSTAT.VASA_REFRESH
IS
   err_num   NUMBER;

   err_msg   VARCHAR2 (200);
   v_lockTimeFrame number := 1440/1440;
   v_runID NUMBER(18);
   v_runCnt NUMBER;
BEGIN
-- *************************
   INSERT INTO system_log (action, username, text)
        VALUES ('Start', $$plsql_unit, '**** Masterscipt started ****');

--------------------------------------------------------------------------------
-- Initialise protection against multiple executing
--------------------------------------------------------------------------------  
   SELECT NVL(max(runID),1) INTO v_runID FROM sys_runstat;
   v_runID := v_runID + 1;        
   BEGIN 
     SELECT count(*) INTO v_runCnt FROM sys_runstat 
      WHERE runName = $$plsql_unit
        AND lockTime > sysdate;
     IF v_runCnt > 0 THEN
       INSERT INTO sys_runstat(runID, runName, startTime, endTime, result)
         VALUES (v_runID, $$plsql_unit, sysdate, sysdate, 'detected running instance and stopped');     
       RETURN;
     ELSE
       INSERT INTO sys_runstat(runID, runName, startTime, lockTime)
         VALUES (v_runID, $$plsql_unit, sysdate, sysdate+v_lockTimeFrame);     
     END IF;     
   END;
   
--------------------------------------------------------------------------------
-- VASA_preparations: Job to prepare several values for other jobs
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
    vasa_preparations;
   END;

--------------------------------------------------------------------------------
-- Jobs to update the 1to1-tables
-- at the very beginning because they are independent from others and may be referred from others
-- Requires other job to run:    none
-- Job type: full refresh
--------------------------------------------------------------------------------
   BEGIN
     vasa_refresh_OneToOneTables;
   END;
   
--------------------------------------------------------------------------------
-- vasa_stat_sl_photograph: Job to prepare STAT_Photograph tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
     vasa_stat_sl_photograph;
   END;
   
--------------------------------------------------------------------------------
-- VASA_CONTRACTS: Job to prepare STAT_Contracts tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
     vasa_contracts;
   END;

--------------------------------------------------------------------------------
-- VASA_DEBTOR: Job to prepare STAT_dm_debtor tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_debtor;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_FARESTAGES: Job the prepare the STAT_TM_FARESTAGENAMES
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_tm_farestages;
   END;

--------------------------------------------------------------------------------
-- VASA_CARDS: Job to prepare STAT_CARD tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_cards;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_TABLES: Job the prepare all the STAT_TM_* tables
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_STAT_RM_TRANSACTIONS (to get all the used line and linenames)
--------------------------------------------------------------------------------
   BEGIN
      vasa_line;
   END;

--------------------------------------------------------------------------------
-- vasa_depot: Job to update master data DEPOT
-- Requires other job to run:    none
-- Job type: Full table refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_depot;
   END;

--------------------------------------------------------------------------------
-- VASA_DM_CARDS: Job to update the DM_CARDs of the live db
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_dm_cards;
   END;

--------------------------------------------------------------------------------
-- VASA_STAT_RM_SHIFT: Job the prepare the STAT_RM_SHIFT
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update
--------------------------------------------------------------------------------
   BEGIN
      vasa_stat_rm_shift;
   END;

--------------------------------------------------------------------------------
-- VASA_STAT_RM_TRANSACTIONS: Job the prepare the STAT_RM_TRASNACTIONS
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_TM_FARESTAGES (for the function to get the farestage names)
--------------------------------------------------------------------------------
   BEGIN
     vasa_stat_rm_transactions;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_TABLES: Job the prepare all the STAT_TM_* tables
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_STAT_RM_TRANSACTIONS (to get all the used ticketids)
--------------------------------------------------------------------------------
   BEGIN
     vasa_tm_tables;
   END;
   
--------------------------------------------------------------------------------
-- vasa_postprocessing: Job for cleaning up
-- Requires other job to run:    none
-- Job type: Clening job for other processes
--------------------------------------------------------------------------------
   BEGIN
      vasa_postprocessing;
   END;

--------------------------------------------------------------------------------
-- vasa_card: Job to update master data CARDACTIONREQUEST
-- Requires other job to run:    none
-- Job type: Full table refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_stat_pp_cardactionrequest;
   END;

--------------------------------------------------------------------------------
-- vasa_ACCOUNTENTRYID: Job to update the accountentries of the live db
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_accountentryid;
   END;

--------------------------------------------------------------------------------
-- VASA_STOP: Job to update the STAT_VARIO_STOP
-- Requires other job to run:    none
-- Job type: full refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_stop;
   END;

--------------------------------------------------------------------------------
-- VASA_PP_PRODUCTONCARD: Job to update the stat_pp_productoncard
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_pp_productoncard;
   END;

--------------------------------------------------------------------------------
-- VASA_RM_BalancePoint: Job to decide whether and in case create a new balance point
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_RM_BalancePoint;
   END;
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: UPdates the inspector logons
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_STAT_INSPECTOR_LOGON;
   END;   
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: Updates the STAT_MOBSTAT_TRIPS table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   --BEGIN
   --   VASA_STAT_MOBSTAT_TRIPS;
   --END;   
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: Updates the STAT_RM_EXTERNALDATA table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------   
   BEGIN
      VASA_STAT_RM_EXTERNALDATA;
   END;
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: Updates the STAT_TM_TICKETCATEGORY table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------   
   BEGIN
      VASA_STAT_TM_TICKETCATEGORY;
   END;

--------------------------------------------------------------------------------
-- VASA_REFRESH_SL
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------   
   BEGIN
      VASA_REFRESH_SL;
   END;
   
   BEGIN
      UPD_VSTAT_ADMIN_ROWCHECK;
   END;
   
--------------------------------------------------------------------------------
-- VASA_CLEAN_PREVIOUSIMPORTED: Job to clean values which are stored for a reimport and statisc overview
--                              A parameter in this procedure describes how many days are kept in the sysstem (Standard:30)
-- Requires other job to run:    none
-- Job type: Cleaning procedure
--------------------------------------------------------------------------------
   /*BEGIN
      --vasa_clean_previousimported; Is started in a second script once a day
   END;*/
   
--------------------------------------------------------------------------------
-- Finalise protection against multiple executing (unlock)
--------------------------------------------------------------------------------  
   BEGIN
     UPDATE sys_runstat 
        SET lockTime = NULL, endTime = sysdate, result = 'finished'
      WHERE runID = v_runID;
   END;    

   INSERT INTO system_log (action, username, text)
        VALUES ('Finished', $$plsql_unit, '*** Masterscript finished ***');

   EXECUTE IMMEDIATE ('COMMIT');
END vasa_refresh;
/



--- Correct old data
--- This statement is not active as it should not be run automatically

---update stat_mobstat_trips set depotcode = substr(runningboard,1,2);
 
COMMIT;

PROMPT Done!

SPOOL OFF



