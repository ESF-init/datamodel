SPOOL VSTAT_1_014.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.014', 'EPA', 'Added EXP_* tables');



CREATE TABLE EXP_EXPORTTRACKING (
	EXPORTTRACKINGID	NUMBER(18)							NOT NULL,
	EXPORTREFERENCE		NUMBER(18)							NOT NULL,
	EXPORTTYPE			NUMBER(9)							NOT NULL,
	EXPORTDATE			DATE			DEFAULT sysdate		NOT NULL,
	CONSTRAINT PK_EXPORTTRACKINGID PRIMARY KEY (EXPORTTRACKINGID)
)
NOCOMPRESS;

COMMENT ON TABLE 	EXP_EXPORTTRACKING 					IS 'Table to keep tracking of exported elements to EP Morris ESB interface.';
COMMENT ON COLUMN 	EXP_EXPORTTRACKING.EXPORTTRACKINGID IS 'ID of the tracking entry.';
COMMENT ON COLUMN 	EXP_EXPORTTRACKING.EXPORTREFERENCE 	IS 'ID of the tracked element, e.g. SHIFT_ID for an exported shift.';
COMMENT ON COLUMN 	EXP_EXPORTTRACKING.EXPORTTYPE 		IS 'Type of exported element, e.g. a shift or trip element.';
COMMENT ON COLUMN	EXP_EXPORTTRACKING.EXPORTDATE		IS 'Date time of the export of the element.';


CREATE TABLE EXP_EXPORTTRACKINGTYPE (
	ENUMERATIONVALUE	NUMBER(9)							NOT NULL,
	LITERAL				NVARCHAR2(50)						NOT NULL,
	DESCRIPTION			NVARCHAR2(50)						NOT NULL,
	CONSTRAINT PK_EXPORTTRACKINGTYPEID PRIMARY KEY (ENUMERATIONVALUE)
)
NOCOMPRESS;

COMMENT ON TABLE	EXP_EXPORTTRACKINGTYPE						IS 'Enumeration table of the possible exports to EP Morris ESB interface.';
COMMENT ON COLUMN	EXP_EXPORTTRACKINGTYPE.ENUMERATIONVALUE		IS 'Enumeration value of the export type.';
COMMENT ON COLUMN	EXP_EXPORTTRACKINGTYPE.LITERAL				IS 'Short description of the export type.';
COMMENT ON COLUMN	EXP_EXPORTTRACKINGTYPE.DESCRIPTION			IS 'Long description of the export type.';


CREATE INDEX IDX_EXPORTTRACKINGREFERENCE ON EXP_EXPORTTRACKING
(EXPORTREFERENCE);

CREATE BITMAP INDEX IDX_EXPORTTYPE ON EXP_EXPORTTRACKING
(EXPORTTYPE)
NOLOGGING
COMPUTE STATISTICS;


INSERT 
INTO EXP_EXPORTTRACKINGTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
VALUES (1, 'Shift', 'ID of exported shift.');

INSERT 
INTO EXP_EXPORTTRACKINGTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
VALUES (2, 'Transaction', 'ID of exported transaction.');

INSERT 
INTO EXP_EXPORTTRACKINGTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
VALUES (3, 'Trip', 'ID of exported trip.');


CREATE OR REPLACE TRIGGER TRG_EXPORTID 
BEFORE INSERT ON EXP_EXPORTTRACKING
FOR EACH ROW
BEGIN
  SELECT DATAID.NEXTVAL
  INTO   :new.EXPORTTRACKINGID
  FROM   dual;
END;
/

CREATE OR REPLACE FORCE VIEW VIEW_EXPORTSHIFTVIEW(  SHIFTID,
                                                    SHIFTSTATEID, 
                                                    SHIFTBEGIN, 
                                                    SHIFTEND, 
                                                    CARDNO, 
                                                    DEBTORNO, 
                                                    LOCATIONNO, 
                                                    DEVICENO, 
                                                    DEVICETYPE, 
                                                    LINENO, 
                                                    IMPORTDATE, 
                                                    EXPORTDATE, 
                                                    EXPORTSTATE, 
                                                    ROUTENO, 
                                                    FIRSTTICKETNO, 
                                                    LASTTICKETNO, 
                                                    SALESVOLUME, 
                                                    CASHRECEIPT, 
                                                    ISDST, 
                                                    HASHVALUE, 
                                                    CLIENTID, 
                                                    BALANCENO, 
                                                    PRICEREDUCTION, 
                                                    SETTLEMENTID, 
                                                    SHIFTNUMBER, 
                                                    SHIFTCOUNTER, 
                                                    DEPOSITNUMBER, 
                                                    LOGOFFDEBTOR, 
                                                    CANCELLATIONCOUNT, 
                                                    CANCELLATIONAMOUNT, 
                                                    DEBITACCOUNT)
AS
SELECT s.SHIFTID, s.SHIFTSTATEID, s.SHIFTBEGIN, s.SHIFTEND, s.CARDNO, s.DEBTORNO, 
s.LOCATIONNO, s.DEVICENO, s.DEVICETYPE, s.LINENO, s.IMPORTDATE, s.EXPORTDATE, 
s.EXPORTSTATE, s.ROUTENO, s.FIRSTTICKETNO, s.LASTTICKETNO, s.SALESVOLUME, 
s.CASHRECEIPT, s.ISDST, s.HASHVALUE, s.CLIENTID, s.BALANCENO, s.PRICEREDUCTION, 
s.SETTLEMENTID, s.SHIFTNUMBER, s.SHIFTCOUNTER, s.DEPOSITNUMBER, s.LOGOFFDEBTOR, 
s.CANCELLATIONCOUNT, s.CANCELLATIONAMOUNT, s.DEBITACCOUNT
FROM STAT_RM_SHIFT s
LEFT JOIN (SELECT * FROM EXP_EXPORTTRACKING WHERE EXP_EXPORTTRACKING.EXPORTTYPE = 1) e
ON s.SHIFTID = e.EXPORTREFERENCE
WHERE e.EXPORTREFERENCE IS NULL;


COMMIT;

PROMPT Done!

SPOOL OFF
