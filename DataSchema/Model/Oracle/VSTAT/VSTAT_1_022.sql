SPOOL VSTAT_1_022.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.022', 'JGI', 'Added odometer handling');

--- New SYNONYM MOBSTAT_VARIO_ODOMETER 

CREATE OR REPLACE PUBLIC SYNONYM MOBSTAT_VARIO_ODOMETER FOR NXWM_EXTERNAL.DAT_TD_VEHICLE_ODOMETER;

--- New TABLE STAT_MOBSTAT_ODOMETER 

CREATE TABLE STAT_MOBSTAT_ODOMETER
(
  OPD_DATE         DATE,
  VEHICLE_ID       NUMBER,
  METERS           NUMBER,
  ACT_TIME         NUMBER,
  REASON           NUMBER,
  METERS_ABSOLUTE  NUMBER,
  COPILOT_RUNTIME  NUMBER,
  GPS_LONGITUDE    NUMBER,
  GPS_LATITUDE     NUMBER,
  GPS_ALTITUDE     NUMBER
);

--- Adjust update procedure

CREATE OR REPLACE PROCEDURE VASA_STAT_MOBSTAT_ODOMETER
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
-- *************************
   INSERT INTO system_log
               (action, username
               )
        VALUES ('Start', $$plsql_unit
               );

   BEGIN
-- *************************
-- Count how many values are copied

      -- Count how many values are copied
      INSERT INTO system_log
                  (action, username, content, tablename, actionresult, text)
           VALUES ('Count', $$plsql_unit, 'Odometer', 'MOBSTAT_VARIO_ODOMETER',
                   (SELECT count(*)
            FROM MOBSTAT_VARIO_ODOMETER 
           WHERE NOT EXISTS (SELECT *  FROM STAT_MOBSTAT_ODOMETER b WHERE b.OPD_DATE = MOBSTAT_VARIO_ODOMETER.OPD_DATE AND b.VEHICLE_ID = MOBSTAT_VARIO_ODOMETER.VEHICLE_ID and b.ACT_TIME = MOBSTAT_VARIO_ODOMETER.ACT_TIME)),
                   'Values, which need to be imported ');

  

------------------------------------------------------------------
      INSERT INTO STAT_MOBSTAT_ODOMETER
                  (OPD_DATE,VEHICLE_ID,METERS,ACT_TIME,REASON,METERS_ABSOLUTE,COPILOT_RUNTIME,GPS_LONGITUDE,GPS_LATITUDE,GPS_ALTITUDE)
         (SELECT OPD_DATE,VEHICLE_ID,METERS,ACT_TIME,REASON,METERS_ABSOLUTE,COPILOT_RUNTIME,GPS_LONGITUDE,GPS_LATITUDE,GPS_ALTITUDE
            FROM MOBSTAT_VARIO_ODOMETER 
           WHERE NOT EXISTS (SELECT *  FROM STAT_MOBSTAT_ODOMETER b WHERE b.OPD_DATE = MOBSTAT_VARIO_ODOMETER.OPD_DATE AND b.VEHICLE_ID = MOBSTAT_VARIO_ODOMETER.VEHICLE_ID and b.ACT_TIME = MOBSTAT_VARIO_ODOMETER.ACT_TIME));

      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Finished', $$plsql_unit
                  );

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log
                     (text, action, actionresult, username
                     )
              VALUES (err_msg, 'Error', err_num, $$plsql_unit
                     );

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;                               -- exception handlers and block end here
END VASA_STAT_MOBSTAT_ODOMETER;
/



--- Correct old data
--- This statement is not active as it should not be run automatically

COMMIT;

PROMPT Done!

SPOOL OFF


