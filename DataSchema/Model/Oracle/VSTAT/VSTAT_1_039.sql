﻿SPOOL VSTAT_1_038.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.039', 'JGI', 'Added ticket names for EMV');

CREATE OR REPLACE PROCEDURE VASA_SL_TRANSACTIONJOURNAL
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
      -- *************************
      INSERT INTO system_log (action, username)
           VALUES ('Start', $$plsql_unit);

      -- *************************
      UpdateDataRows ('SL_TRANSACTIONJOURNAL');

      -- *************************
      INSERT INTO system_log (action,
                              username,
                              content,
                              tablename,
                              actionresult,
                              text)
              VALUES (
                        'Count',
                        $$plsql_unit,
                        'TRANSACTIONJOURNAL',
                        'VSTAT_SL_TRANSACTIONJOURNAL',
                        (SELECT COUNT (*)
                           FROM VSTAT_SL_TRANSACTIONJOURNAL
                          WHERE transactionjournalid IN (SELECT UNIQUE
                                                                (datarowid)
                                                           FROM post_sl
                                                          WHERE TABLENAME =
                                                                   'SL_TRANSACTIONJOURNAL')),
                        'Values already imported which need to be updated (reimport)');

      EXECUTE IMMEDIATE ('COMMIT');

      -- *************************
      -- Start importing: First we delete all values which are going to be reimported
      DELETE FROM VSTAT_SL_TRANSACTIONJOURNAL
            WHERE transactionjournalid IN (SELECT datarowid
                                             FROM post_sl
                                            WHERE TABLENAME =
                                                     'SL_TRANSACTIONJOURNAL');

      -- Receive all values from the live db
      INSERT INTO vstat_SL_TRANSACTIONJOURNAL (TRANSACTIONJOURNALID,
                                               SALEID,
                                               TRANSACTIONID,
                                               DEVICETIME,
                                               BOARDINGTIME,
                                               SHIFTBEGIN,
                                               OPERATORID,
                                               CLIENTID,
                                               LINE,
                                               COURSENUMBER,
                                               ROUTENUMBER,
                                               ROUTECODE,
                                               DIRECTION,
                                               ZONE,
                                               SALESCHANNELID,
                                               DEVICENUMBER,
                                               VEHICLENUMBER,
                                               MOUNTINGPLATENUMBER,
                                               DEBTORNUMBER,
                                               GEOLOCATIONLONGITUDE,
                                               GEOLOCATIONLATITUDE,
                                               STOPNUMBER,
                                               TRANSITACCOUNTID,
                                               FAREMEDIAID,
                                               FAREMEDIATYPE,
                                               PRODUCTID,
                                               PRODUCTID2,
                                               TICKETID,
                                               FAREAMOUNT,
                                               CUSTOMERGROUP,
                                               TRANSACTIONTYPE,
                                               TRANSACTIONNUMBER,
                                               RESULTTYPE,
                                               ORIGINALONLINERESULTTYPE,
                                               ORIGINALDEVICERESULTTYPE,
                                               FILLLEVEL,
                                               PURSEBALANCE,
                                               PURSEBALANCE2,
                                               PURSECREDIT,
                                               PURSECREDIT2,
                                               GROUPSIZE,
                                               VALIDFROM,
                                               VALIDTO,
                                               DURATION,
                                               TARIFFDATE,
                                               TARIFFVERSION,
                                               TICKETINTERNALNUMBER,
                                               TRIPTICKETINTERNALNUMBER,
                                               WHITELISTVERSION,
                                               WHITELISTVERSIONCREATED,
                                               CANCELLATIONREFERENCE,
                                               CANCELLATIONREFERENCEGUID,
                                               COMPLETIONSTATE,
                                               RESPONSESTATE,
                                               INSERTTIME,
                                               LASTUSER,
                                               LASTMODIFIED,
                                               TRANSACTIONCOUNTER,
                                               PROPERTIES,
                                               TRIPCOUNTER,
                                               CREDITCARDAUTHORIZATIONID,
                                               SMARTTAPID,
                                               SMARTTAPCOUNTER,
                                               RESULTSOURCE,
                                               TRANSACTIONTIME,
                                               ROUNDTRIPTIME,
                                               JOURNEYSTARTTIME)
         (SELECT TRANSACTIONJOURNALID,
                 SALEID,
                 TRANSACTIONID,
                 DEVICETIME,
                 BOARDINGTIME,
                 SHIFTBEGIN,
                 OPERATORID,
                 CLIENTID,
                 LINE,
                 COURSENUMBER,
                 ROUTENUMBER,
                 ROUTECODE,
                 DIRECTION,
                 ZONE,
                 SALESCHANNELID,
                 DEVICENUMBER,
                 VEHICLENUMBER,
                 MOUNTINGPLATENUMBER,
                 DEBTORNUMBER,
                 GEOLOCATIONLONGITUDE,
                 GEOLOCATIONLATITUDE,
                 STOPNUMBER,
                 TRANSITACCOUNTID,
                 FAREMEDIAID,
                 FAREMEDIATYPE,
                 PRODUCTID,
                 PRODUCTID2,
                 TICKETID,
                 FAREAMOUNT,
                 CUSTOMERGROUP,
                 TRANSACTIONTYPE,
                 TRANSACTIONNUMBER,
                 RESULTTYPE,
                 ORIGINALONLINERESULTTYPE,
                 ORIGINALDEVICERESULTTYPE,
                 FILLLEVEL,
                 PURSEBALANCE,
                 PURSEBALANCE2,
                 PURSECREDIT,
                 PURSECREDIT2,
                 GROUPSIZE,
                 VALIDFROM,
                 VALIDTO,
                 DURATION,
                 TARIFFDATE,
                 TARIFFVERSION,
                 TICKETINTERNALNUMBER,
                 TRIPTICKETINTERNALNUMBER,
                 WHITELISTVERSION,
                 WHITELISTVERSIONCREATED,
                 CANCELLATIONREFERENCE,
                 CANCELLATIONREFERENCEGUID,
                 COMPLETIONSTATE,
                 RESPONSESTATE,
                 INSERTTIME,
                 LASTUSER,
                 LASTMODIFIED,
                 TRANSACTIONCOUNTER,
                 PROPERTIES,
                 TRIPCOUNTER,
                 CREDITCARDAUTHORIZATIONID,
                 SMARTTAPID,
                 SMARTTAPCOUNTER,
                 RESULTSOURCE,
                 TRANSACTIONTIME,
                 ROUNDTRIPTIME,
                 JOURNEYSTARTTIME
            FROM sl_transactionjournal@vario_productive t
           WHERE t.transactionjournalid IN (SELECT DATAROWID
                                              FROM POST_SL
                                             WHERE TABLENAME =
                                                      'SL_TRANSACTIONJOURNAL'));

      EXECUTE IMMEDIATE ('COMMIT');

      -- Save the imported values in the table system_previous_imported which keeps
      -- all the imported IDs for a certain time range (standard 7 days)
      INSERT INTO system_previous_imported (value_id, id_name, importdate)
         (SELECT post_sl.datarowid, 'TRANSACTIONJOURNALID', SYSDATE
            FROM post_sl
           WHERE TABLENAME = 'SL_TRANSACTIONJOURNAL');

      EXECUTE IMMEDIATE ('COMMIT');

      -- Delte the ID's from the live db which were just imported
      DELETE FROM STAT_POSTPROCESSOR_SL@vario_productive
            WHERE datarowid IN (SELECT datarowid
                                  FROM post_sl
                                 WHERE TABLENAME = 'SL_TRANSACTIONJOURNAL');

      -- Delete the POST table
      --DELETE FROM post_sl
      --      WHERE TABLENAME = 'SL_TRANSACTIONJOURNAL';

      INSERT INTO system_log (action, username)
           VALUES ('Finished', $$plsql_unit);

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log (text,
                                 action,
                                 actionresult,
                                 username)
              VALUES (err_msg,
                      'Error',
                      err_num,
                      $$plsql_unit);

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;
-- exception handlers and block end here
END VASA_SL_TRANSACTIONJOURNAL;
/

CREATE OR REPLACE PROCEDURE VASA_TM_TABLES
IS
    err_num number;
    err_msg VARCHAR2(200);
BEGIN

  BEGIN
-- *************************
      INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);
-- *************************
  -- Make sure we don't have any double entries in POST_TICKETID. Therewith it is possible to enter ticketid's into
  -- POST_TICKETID by hand.
  DELETE FROM POST_TICKETID WHERE TICKETID IN
  (SELECT unique(TICKETID) FROM STAT_RM_TRANSACTIONS, POST_SHIFTID
   WHERE POST_SHIFTID.SHIFTID = STAT_RM_TRANSACTIONS.SHIFTID);
 
  DELETE FROM POST_TICKETID WHERE TICKETID IN
  (SELECT unique(TICKETID) FROM VSTAT_SL_TRANSACTIONJOURNAL
   WHERE transactionjournalid IN (SELECT datarowid
                                             FROM post_sl
                                            WHERE TABLENAME =
                                                     'SL_TRANSACTIONJOURNAL'));
  
  -- Updating all new ticketid's which were sold since the last update
    INSERT INTO POST_TICKETID (TICKETID)
    (SELECT unique(TICKETID) FROM STAT_RM_TRANSACTIONS, POST_SHIFTID
      WHERE POST_SHIFTID.SHIFTID = STAT_RM_TRANSACTIONS.SHIFTID);
      
    INSERT INTO POST_TICKETID (TICKETID)
    (SELECT unique(TICKETID) FROM VSTAT_SL_TRANSACTIONJOURNAL
      WHERE transactionjournalid IN (SELECT datarowid
                                             FROM post_sl
                                            WHERE TABLENAME =
                                                     'SL_TRANSACTIONJOURNAL')
    AND TICKETID NOT IN (select ticketid from STAT_TM_TIK_ATTRVALUES));

    INSERT INTO system_log (action, username, content, tablename, actionresult, text)
           VALUES ('Count', $$plsql_unit, 'Tariff Tables', 'POST_TICKETID',
                   (SELECT COUNT (*) FROM POST_TICKETID), 'Ready to import values');
    COMMIT;
--------------------------------------------------------------------------------
-- Before  we start inserting information into stat_tm_tik_attrvalues we delete the tickets
-- which were used in the latest transactions.
-- Q: Why are we not deleting the ticketids in POST_TICKETID if we already have them in STAT_TM_TIK_ATTRVALUES?
-- A: In unlikely case attribute values of the tickets have changed. In this case the values will be updated as well
-- Well knowing we are not updating all tickets, we might miss updates which are done on old tickets. In case they are
-- never sold again, these values won't be updated here. In praxis, this never happend bevore. If so, use FIX_TM_TABLES
  DELETE FROM STAT_TM_TIK_ATTRVALUES WHERE ticketid in (SELECT TICKETID FROM POST_TICKETID);

    INSERT INTO stat_tm_tik_attrvalues (ticketid, clientid, serviceid, printtext, name, tickettype, internalnumber, externalnumber, fixprice, externalname, categoryid, externalname2, description)
      SELECT tik.ticketid, tf.clientid, tik.serviceid, tik.printtext,
              tik.name, tik.tickettype, tik.internalnumber, tik.externalnumber, tik.price AS fixprice, tik.externalname, tik.categoryid, tik.externalname2, tik.description
        FROM tm_ticket@vario_productive tik,
             tm_tarif@vario_productive tf,
             post_ticketid tikid
       WHERE tik.tarifID = tf.tarifID
         AND tik.ticketID = tikid.ticketID;
   
-- Table should be exported in its entirty every day 
   UPDATE STAT_TM_TIK_ATTRVALUES set vasaexportstate = 0, vasaexportdate = null;

-- Only update the latest tickets. This means ticketids out of POST_TICKETID
    UPDATE  /*+ NO_CPU_COSTING */ STAT_TM_TIK_ATTRVALUES
      SET (tarifarea, tarifarea_NO) =
             (SELECT view_tm_tik_attrvalue.avname, view_tm_tik_attrvalue.avno
                FROM view_tm_tik_attrvalue
                WHERE view_tm_tik_attrvalue.ticketid = stat_tm_tik_attrvalues.ticketid
                 AND view_tm_tik_attrvalue.attribclassid = 1)
      WHERE STAT_TM_TIK_ATTRVALUES.ticketid in (SELECT TICKETID FROM post_ticketid);

    UPDATE  /*+ NO_CPU_COSTING */ stat_tm_tik_attrvalues
      SET (tiktype, tiktype_NO) =
             (SELECT view_tm_tik_attrvalue.avname, view_tm_tik_attrvalue.avno
                FROM view_tm_tik_attrvalue
               WHERE view_tm_tik_attrvalue.ticketid =
                                               stat_tm_tik_attrvalues.ticketid
                 AND view_tm_tik_attrvalue.attribclassid = 3
                 )
      WHERE STAT_TM_TIK_ATTRVALUES.ticketid in (SELECT TICKETID FROM post_ticketid);

    UPDATE  /*+ NO_CPU_COSTING */ stat_tm_tik_attrvalues
      SET (farestage, farestage_NO) =
             (SELECT view_tm_tik_attrvalue.avname, view_tm_tik_attrvalue.avno
                FROM view_tm_tik_attrvalue
               WHERE view_tm_tik_attrvalue.ticketid =
                                               stat_tm_tik_attrvalues.ticketid
                 AND view_tm_tik_attrvalue.attribclassid = 7
                 )
      WHERE STAT_TM_TIK_ATTRVALUES.ticketid in (SELECT TICKETID FROM post_ticketid);

    UPDATE  /*+ NO_CPU_COSTING */ stat_tm_tik_attrvalues
      SET (tikclass, tikclass_NO) =
             (SELECT view_tm_tik_attrvalue.avname, view_tm_tik_attrvalue.avno
                 FROM view_tm_tik_attrvalue
               WHERE view_tm_tik_attrvalue.ticketid =
                                               stat_tm_tik_attrvalues.ticketid
                 AND view_tm_tik_attrvalue.attribclassid = 14
                 )
      WHERE STAT_TM_TIK_ATTRVALUES.ticketid in (SELECT TICKETID FROM post_ticketid);

    UPDATE  /*+ NO_CPU_COSTING */ stat_tm_tik_attrvalues
      SET (product, product_NO) =
             (SELECT view_tm_tik_attrvalue.avname, view_tm_tik_attrvalue.avno
                FROM view_tm_tik_attrvalue
               WHERE view_tm_tik_attrvalue.ticketid =
                                               stat_tm_tik_attrvalues.ticketid
                 AND view_tm_tik_attrvalue.attribclassid = 19
                 )
      WHERE STAT_TM_TIK_ATTRVALUES.ticketid in (SELECT TICKETID FROM post_ticketid);


--------------------------------------------------------------------------------
    DELETE FROM STAT_TM_ATTRIBUTETARIFFAREA;

    INSERT INTO stat_tm_attributetariffarea
               (number_, NAME, stat_tm_attributetariffarea.clientid)
      (SELECT UNIQUE (tarifarea_NO),
                     tarifarea AS NAME,
                     stat_tm_tik_attrvalues.CLIENTID
                FROM stat_tm_tik_attrvalues
                 );

--------------------------------------------------------------------------------
    DELETE FROM STAT_TM_ATTRIBUTETICKETTYPE;

    INSERT INTO stat_tm_attributetickettype
               (number_, NAME, stat_tm_attributetickettype.clientid)
      (SELECT UNIQUE (tiktype_NO),
                     tiktype AS NAME,
                     stat_tm_tik_attrvalues.CLIENTID
                FROM stat_tm_tik_attrvalues
                 );

--------------------------------------------------------------------------------
    DELETE FROM STAT_TM_ATTRIBUTEFARESTAGE;

    INSERT INTO stat_tm_attributefarestage
               (number_, NAME, stat_tm_attributefarestage.clientid)
          (SELECT UNIQUE (farestage_NO),
                     farestage AS NAME,
                     stat_tm_tik_attrvalues.CLIENTID
                FROM stat_tm_tik_attrvalues
                 );


--------------------------------------------------------------------------------
    DELETE FROM STAT_TM_ATTRIBUTECLASS;

    INSERT INTO stat_tm_attributeclass
               (number_, NAME, stat_tm_attributeclass.clientid)
      (SELECT UNIQUE (tikclass_NO),
                     tikclass AS NAME,
                     stat_tm_tik_attrvalues.CLIENTID
                FROM stat_tm_tik_attrvalues
                 );

--------------------------------------------------------------------------------
    DELETE FROM STAT_TM_ATTRIBUTEPRODUCT;

    INSERT INTO STAT_TM_ATTRIBUTEPRODUCT
               (number_, NAME, stat_tm_ATTRIBUTEPRODUCT.clientid)
      (SELECT UNIQUE (product_NO),
                     product AS NAME,
                     stat_tm_tik_attrvalues.CLIENTID
                FROM stat_tm_tik_attrvalues
                 );

    COMMIT;

    INSERT INTO system_previous_imported(value_id,id_name,importdate)
     (SELECT post_ticketid.ticketid,'TICKETID',sysdate FROM POST_TICKETID);

    DELETE FROM POST_TICKETID;
    COMMIT;

-- *************************
    INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
    COMMIT;  

   EXCEPTION
      WHEN OTHERS
      THEN
        EXECUTE IMMEDIATE ('ROLLBACK');
        err_num := SQLCODE;
        err_msg := TO_CHAR (err_num) || '-' || SQLERRM;
        INSERT INTO system_log (text, action, actionresult, username)
           VALUES (err_msg, 'Error', err_num, $$plsql_unit);
        COMMIT;
        RAISE;
   END;                               -- exception handlers and block end here

END VASA_TM_TABLES;
/

CREATE OR REPLACE PROCEDURE VASA_REFRESH_SL
IS
   
BEGIN

--------------------------------------------------------------------------------
-- Jobs to update the SL-tables
--------------------------------------------------------------------------------
 BEGIN
    VASA_SL_CARDS;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
     VASA_SL_CREDITCARDAUTHOR;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_CREDITCARDAUTHORNLOG;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_CREDITCARDTERMINAL;
   END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_FAREPAYMENTERROR;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_PAYMENTJOURNAL;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_SALE;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_TRANSACTIONJOURNAL;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_WHITELIST;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_WHITELISTJOURNAL;
 END;
   
--------------------------------------------------------------------------------


END VASA_REFRESH_SL;
/


CREATE OR REPLACE PROCEDURE VASA_REFRESH
IS
   err_num   NUMBER;

   err_msg   VARCHAR2 (200);
   v_lockTimeFrame number := 1440/1440;
   v_runID NUMBER(18);
   v_runCnt NUMBER;
BEGIN
-- *************************
   INSERT INTO system_log (action, username, text)
        VALUES ('Start', $$plsql_unit, '**** Masterscipt started ****');

--------------------------------------------------------------------------------
-- Initialise protection against multiple executing
--------------------------------------------------------------------------------  
   SELECT NVL(max(runID),1) INTO v_runID FROM sys_runstat;
   v_runID := v_runID + 1;        
   BEGIN 
     SELECT count(*) INTO v_runCnt FROM sys_runstat 
      WHERE runName = $$plsql_unit
        AND lockTime > sysdate;
     IF v_runCnt > 0 THEN
       INSERT INTO sys_runstat(runID, runName, startTime, endTime, result)
         VALUES (v_runID, $$plsql_unit, sysdate, sysdate, 'detected running instance and stopped');     
       RETURN;
     ELSE
       INSERT INTO sys_runstat(runID, runName, startTime, lockTime)
         VALUES (v_runID, $$plsql_unit, sysdate, sysdate+v_lockTimeFrame);     
     END IF;     
   END;
   
--------------------------------------------------------------------------------
-- VASA_preparations: Job to prepare several values for other jobs
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
    vasa_preparations;
   END;

--------------------------------------------------------------------------------
-- Jobs to update the 1to1-tables
-- at the very beginning because they are independent from others and may be referred from others
-- Requires other job to run:    none
-- Job type: full refresh
--------------------------------------------------------------------------------
   BEGIN
     vasa_refresh_OneToOneTables;
   END;
   
--------------------------------------------------------------------------------
-- vasa_stat_sl_photograph: Job to prepare STAT_Photograph tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
     vasa_stat_sl_photograph;
   END;
   
--------------------------------------------------------------------------------
-- VASA_CONTRACTS: Job to prepare STAT_Contracts tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
     vasa_contracts;
   END;

--------------------------------------------------------------------------------
-- VASA_DEBTOR: Job to prepare STAT_dm_debtor tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_debtor;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_FARESTAGES: Job the prepare the STAT_TM_FARESTAGENAMES
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_tm_farestages;
   END;

--------------------------------------------------------------------------------
-- VASA_CARDS: Job to prepare STAT_CARD tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   --BEGIN
   --   vasa_cards;
   --END;

--------------------------------------------------------------------------------
-- vasa_depot: Job to update master data DEPOT
-- Requires other job to run:    none
-- Job type: Full table refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_depot;
   END;

--------------------------------------------------------------------------------
-- VASA_DM_CARDS: Job to update the DM_CARDs of the live db
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_dm_cards;
   END;

--------------------------------------------------------------------------------
-- VASA_STAT_RM_SHIFT: Job the prepare the STAT_RM_SHIFT
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update
--------------------------------------------------------------------------------
   BEGIN
      vasa_stat_rm_shift;
   END;

   
--------------------------------------------------------------------------------
-- vasa_card: Job to update master data CARDACTIONREQUEST
-- Requires other job to run:    none
-- Job type: Full table refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_stat_pp_cardactionrequest;
   END;

--------------------------------------------------------------------------------
-- vasa_ACCOUNTENTRYID: Job to update the accountentries of the live db
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_accountentryid;
   END;

--------------------------------------------------------------------------------
-- VASA_STOP: Job to update the STAT_VARIO_STOP
-- Requires other job to run:    none
-- Job type: full refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_stop;
   END;

--------------------------------------------------------------------------------
-- VASA_PP_PRODUCTONCARD: Job to update the stat_pp_productoncard
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_pp_productoncard;
   END;

--------------------------------------------------------------------------------
-- VASA_RM_BalancePoint: Job to decide whether and in case create a new balance point
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_RM_BalancePoint;
   END;
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: UPdates the inspector logons
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_STAT_INSPECTOR_LOGON;
   END;   
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: Updates the STAT_MOBSTAT_TRIPS table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   --BEGIN
   --   VASA_STAT_MOBSTAT_TRIPS;
   --END;   
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: Updates the STAT_TM_TICKETCATEGORY table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------   
   BEGIN
      VASA_STAT_TM_TICKETCATEGORY;
   END;

--------------------------------------------------------------------------------
-- VASA_STAT_RM_TRANSACTIONS: Job the prepare the STAT_RM_TRASNACTIONS
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_TM_FARESTAGES (for the function to get the farestage names)
--------------------------------------------------------------------------------
   BEGIN
     vasa_stat_rm_transactions;
   END;

--------------------------------------------------------------------------------
-- VASA_STAT_RM_EXTERNALDATA: Updates the STAT_RM_EXTERNALDATA table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------   
   BEGIN
      VASA_STAT_RM_EXTERNALDATA;
   END;
   
--------------------------------------------------------------------------------
-- VASA_STAT_RM_EXT_TRANSACTION: Updates the STAT_RM_EXT_TRANSACTION table
-- Requires other job to run:    vasa_stat_rm_transactions
-- Job type: non refresh
--------------------------------------------------------------------------------   
   BEGIN
      VASA_STAT_RM_EXT_TRANSACTION;
   END;
   
--------------------------------------------------------------------------------
-- VASA_TM_TABLES: Job the prepare all the STAT_TM_* tables
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_STAT_RM_TRANSACTIONS (to get all the used line and linenames)
--------------------------------------------------------------------------------
   BEGIN
      vasa_line;
   END;

--------------------------------------------------------------------------------
-- VASA_REFRESH_SL
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------   
   BEGIN
      VASA_REFRESH_SL;
   END;
   
   BEGIN
      UPD_VSTAT_ADMIN_ROWCHECK;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_TABLES: Job the prepare all the STAT_TM_* tables
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_STAT_RM_TRANSACTIONS (to get all the used ticketids)
--------------------------------------------------------------------------------
   BEGIN
     vasa_tm_tables;
   END;
   
--------------------------------------------------------------------------------
-- vasa_postprocessing: Job for cleaning up
-- Requires other job to run:    none
-- Job type: Clening job for other processes
--------------------------------------------------------------------------------
   BEGIN
      vasa_postprocessing;
   END;
   
--------------------------------------------------------------------------------
-- VASA_CLEAN_PREVIOUSIMPORTED: Job to clean values which are stored for a reimport and statisc overview
--                              A parameter in this procedure describes how many days are kept in the sysstem (Standard:30)
-- Requires other job to run:    none
-- Job type: Cleaning procedure
--------------------------------------------------------------------------------
   /*BEGIN
      --vasa_clean_previousimported; Is started in a second script once a day
   END;*/
   
--------------------------------------------------------------------------------
-- Finalise protection against multiple executing (unlock)
--------------------------------------------------------------------------------  
   BEGIN
     UPDATE sys_runstat 
        SET lockTime = NULL, endTime = sysdate, result = 'finished'
      WHERE runID = v_runID;
   END;    

   INSERT INTO system_log (action, username, text)
        VALUES ('Finished', $$plsql_unit, '*** Masterscript finished ***');

   EXECUTE IMMEDIATE ('COMMIT');
END vasa_refresh;
/

CREATE OR REPLACE PROCEDURE VSTAT.VASA_POSTPROCESSING
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
-- *************************
      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Start', $$plsql_unit
                  );

-- *************************
      INSERT INTO system_log
                  (action, username, content, tablename,
                   actionresult
                  )
           VALUES ('Delete', $$plsql_unit, 'Shift', 'POST_SHIFTID',
                   (SELECT COUNT (*)
                      FROM post_shiftid)
                  );

      EXECUTE IMMEDIATE ('COMMIT');

      DELETE FROM stat_shiftpostprocessor@vario_productive
            WHERE shiftid IN (SELECT shiftid
                                FROM post_shiftid);

      INSERT INTO system_previous_imported
                  (value_id, id_name, importdate)
         (SELECT shiftid, 'SHIFTID', SYSDATE
            FROM post_shiftid);

      -- The deleting of the shift IDs after all jobs are finished has the follwing advantage:
      -- Shiftids from the productive db can easily been updated into the table POST_SHIFTID.
      -- When VASA_REFRESH job is running the next time, these values will also been taken to update all the
      -- tables in the statistic db which are using the SHIFTIDS for an incremental update (e.g. VASA_STA_RM_TRANSACTIONS,
      -- VASA_TM_TABLES, VASA_LINE, e.g.).
      DELETE FROM post_shiftid;
      
      DELETE FROM POST_SL;

      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Finished', $$plsql_unit
                  );

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log
                     (text, action, actionresult, username
                     )
              VALUES (err_msg, 'Error', err_num, $$plsql_unit
                     );

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;                               -- exception handlers and block end here
END vasa_postprocessing;
/





PROMPT Done!

SPOOL OFF