SPOOL VSTAT_1_001.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.001', 'JGI', 'Added update for RM_DEVICEPAYMENTMETHOD');

CREATE OR REPLACE PROCEDURE VASA_RM_DEVICEPAYMENTMETHOD
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);

  -- Because its not to much data we refresh the whole table
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'device paymentmethods', 'RM_DEVICEPAYMENTMETHOD',
               (SELECT COUNT(*) FROM RM_DEVICEPAYMENTMETHOD), 'values before update');  
  DELETE FROM RM_DEVICEPAYMENTMETHOD;
  INSERT INTO RM_DEVICEPAYMENTMETHOD 
      (DEVICEPAYMENTMETHODID,DESCRIPTION,VISIBLE,EVENDMASK,EXTERNALNUMBER)     
      (SELECT DEVICEPAYMENTMETHODID,DESCRIPTION,VISIBLE,EVENDMASK,EXTERNALNUMBER
       FROM RM_DEVICEPAYMENTMETHOD@vario_productive);
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'device paymentmethods', 'RM_DEVICEPAYMENTMETHOD',
               (SELECT COUNT(*) FROM RM_DEVICEPAYMENTMETHOD), 'values after update');  
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
  
EXCEPTION
  WHEN OTHERS
  THEN
    EXECUTE IMMEDIATE ('ROLLBACK');
    err_num := SQLCODE;
    err_msg := TO_CHAR (err_num) || '-' || SQLERRM;
    INSERT INTO system_log (text, action, actionresult, username)
         VALUES (err_msg, 'Error', err_num, $$plsql_unit);
    EXECUTE IMMEDIATE ('COMMIT');
    RAISE;
  -- exception handlers and block end here
END VASA_RM_DEVICEPAYMENTMETHOD;
/


CREATE OR REPLACE PROCEDURE vasa_refresh_OneToOneTables
IS
BEGIN
   INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);
  -- *************************   
      vasa_stat_SL_autoloadSetting;
      vasa_stat_SL_product;
      vasa_stat_SL_paymentoption;
      vasa_stat_SL_contract;
      vasa_stat_SL_order;
      vasa_stat_SL_orderdetail;
      vasa_stat_SL_organization;
      vasa_stat_SL_address;
      vasa_stat_SL_person;      
      vasa_stat_SL_payment;
      vasa_um_device;
      vasa_um_unit;
      VASA_RM_DEVICEPAYMENTMETHOD;
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
END vasa_refresh_OneToOneTables;
/



COMMIT;

PROMPT Done!

SPOOL OFF
