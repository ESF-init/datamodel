SPOOL VSTAT_1_017.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.017', 'JGI', 'Added update for TM_TICKETCATEGORY');


--- New table VASA_STAT_TM_TICKETCATEGORY

CREATE TABLE VASA_TM_TICKETCATEGORY
(
  TICKETCATEGORYID  NUMBER(10),
  NUMBER_           NUMBER(10),
  NAME              VARCHAR2(200 BYTE),
  DESCRIPTION       VARCHAR2(200 BYTE),
  CLIENTID          NUMBER(10)
);

--- Adjust update procedure

CREATE OR REPLACE PROCEDURE VASA_STAT_TM_TICKETCATEGORY
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);

  -- Because its not to much data we refresh the whole table
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'units', 'UM_unit',
               (SELECT COUNT(*) FROM VASA_TM_TICKETCATEGORY), 'values before update');  
  DELETE FROM VASA_TM_TICKETCATEGORY;
  INSERT INTO VASA_TM_TICKETCATEGORY 
            (TICKETCATEGORYID, NUMBER_, NAME, description, CLIENTID)  
     (SELECT TICKETCATEGORYID, NUMBER_, NAME, description, CLIENTID
       FROM TM_TICKETCATEGORY@vario_productive);
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'ticket categories', 'VASA_TM_TICKETCATEGORY',
               (SELECT COUNT(*) FROM VASA_TM_TICKETCATEGORY), 'values after update');  
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
  
EXCEPTION
  WHEN OTHERS
  THEN
    EXECUTE IMMEDIATE ('ROLLBACK');
    err_num := SQLCODE;
    err_msg := TO_CHAR (err_num) || '-' || SQLERRM;
    INSERT INTO system_log (text, action, actionresult, username)
         VALUES (err_msg, 'Error', err_num, $$plsql_unit);
    EXECUTE IMMEDIATE ('COMMIT');
    RAISE;
  -- exception handlers and block end here
END VASA_STAT_TM_TICKETCATEGORY;
/

CREATE OR REPLACE PROCEDURE vasa_refresh_OneToOneTables
IS
BEGIN
   INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);
  -- *************************   
      vasa_stat_SL_autoloadSetting;
      vasa_stat_SL_product;
      vasa_stat_SL_paymentoption;
      vasa_stat_SL_contract;
      vasa_stat_SL_order;
      vasa_stat_SL_orderdetail;
      vasa_stat_SL_organization;
      vasa_stat_SL_address;
      vasa_stat_SL_person;      
      vasa_stat_SL_payment;
      vasa_um_device;
      vasa_um_unit;
      VASA_RM_DEVICEPAYMENTMETHOD;
	  VASA_STAT_TM_TICKETCATEGORY;
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
END vasa_refresh_OneToOneTables;
/



--- Correct old data
--- This statement is not active as it should not be run automatically


COMMIT;

PROMPT Done!

SPOOL OFF
