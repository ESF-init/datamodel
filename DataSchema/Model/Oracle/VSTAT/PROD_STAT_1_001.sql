
CREATE OR REPLACE TRIGGER "TRG_RM_ShiftPostProcessor_DEL" 
AFTER DELETE ON RM_shift
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE 
 nID NUMBER(18);
 nShiftStateID NUMBER(2);
BEGIN
    nID := :OLD.shiftID;
    nShiftStateID := :OLD.shiftStateID;
    DELETE FROM stat_shiftPostProcessor WHERE shiftID = nID;
    IF NOT nShiftStateID IN (40,41) THEN
        Insert into stat_shiftPostProcessor (shiftID) Values (nID);
    END IF;
END;
/

CREATE OR REPLACE TRIGGER "TRG_RM_ShiftPostProcessor_UPD" 
AFTER INSERT OR UPDATE ON RM_shift
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE 
 nID NUMBER(18);
 nShiftStateID NUMBER(2);
BEGIN
    nID := :NEW.shiftID;
    nShiftStateID := :NEW.shiftStateID;
    DELETE FROM stat_shiftPostProcessor WHERE shiftID = nID;
    IF NOT nShiftStateID IN (40,41) THEN
        Insert into stat_shiftPostProcessor (shiftID) Values (nID);
    END IF;
END;
/
