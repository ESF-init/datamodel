SPOOL VSTAT_1_024.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.024', 'JGI', 'Added tables for EMV');

--- New tables

CREATE TABLE POST_SL
(
  DATAROWID  NUMBER(18),
  TABLENAME  VARCHAR2(100 BYTE)
);

CREATE UNIQUE INDEX PK_POST_SL ON POST_SL
(DATAROWID, TABLENAME)
LOGGING
NOPARALLEL;

ALTER TABLE POST_SL ADD (
  CONSTRAINT PK_POST_SL
  PRIMARY KEY
  (DATAROWID, TABLENAME)
  USING INDEX PK_POST_SL
  ENABLE VALIDATE);


CREATE TABLE VSTAT_SL_CARD
(
  CARDID                NUMBER(18)              NOT NULL,
  CARDPHYSICALDETAILID  NUMBER(18)              NOT NULL,
  CARDHOLDERID          NUMBER(18),
  INVENTORYSTATE        NUMBER(9)               NOT NULL,
  BATCHNUMBER           NVARCHAR2(50)           NOT NULL,
  SERIALNUMBER          NVARCHAR2(100)          NOT NULL,
  PRINTEDNUMBER         NVARCHAR2(100)          NOT NULL,
  SECURITYCODE          NVARCHAR2(3),
  STATE                 NUMBER(9)               NOT NULL,
  IMPORTED              DATE                    DEFAULT sysdate               NOT NULL,
  LASTUSED              DATE                    DEFAULT sysdate               NOT NULL,
  EXPIRATION            DATE                    DEFAULT TO_TIMESTAMP('31.12.9999 23:59:59', 'DD.MM.YYYY HH24:MI:SS') NOT NULL,
  BLOCKED               DATE                    DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  BLOCKINGREASON        NUMBER(9)               NOT NULL,
  BLOCKINGCOMMENT       NVARCHAR2(250),
  STORAGELOCATIONID     NUMBER(18)              NOT NULL,
  ISSHARED              NUMBER(1)               DEFAULT 0                     NOT NULL,
  ISVOICEENABLED        NUMBER(1)               DEFAULT 0                     NOT NULL,
  ISTRAINING            NUMBER(1)               DEFAULT 0                     NOT NULL,
  ISPRODUCTPOOL         NUMBER(1)               DEFAULT 0                     NOT NULL,
  PURSEBALANCE          NUMBER(9)               DEFAULT 0                     NOT NULL,
  LOADCOUNTER           NUMBER(9)               DEFAULT 0                     NOT NULL,
  CHARGECOUNTER         NUMBER(9)               DEFAULT 0                     NOT NULL,
  CARDTYPE              NUMBER(18)              DEFAULT 0                     NOT NULL,
  USERGROUPID           NUMBER(9),
  SUBSIDYLIMIT          NUMBER(9),
  FAREMEDIAID           NVARCHAR2(100),
  SHORTCODE             NVARCHAR2(20),
  REVISION              NUMBER(9),
  DESCRIPTION           NVARCHAR2(40),
  PARTICIPANTID         NUMBER(18),
  SEQUENTIALNUMBER      NVARCHAR2(20),
  REPLACEDCARDID        NUMBER(18),
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,
  CLIENTID              NUMBER(18),
  NUMBEROFPRINTS        NUMBER(18),
  PRINTEDDATE           DATE
);

CREATE TABLE VSTAT_SL_CREDITCARDAUTHOR
(
  CREDITCARDAUTHORIZATIONID  NUMBER(18)         NOT NULL,
  CARDID                     NUMBER(18)         NOT NULL,
  STATUS                     NUMBER(9)          DEFAULT 0                     NOT NULL,
  LOCALDATETIME              DATE               NOT NULL,
  TRANSACTIONDATETIME        DATE               NOT NULL,
  EXTERNALAUTHID             NVARCHAR2(25),
  SYSTEMTRACEAUDITNUMBER     NVARCHAR2(25)      NOT NULL,
  RESPONSECODE               NVARCHAR2(25)      NOT NULL,
  AMOUNT                     NUMBER(9)          NOT NULL,
  PURSEAMOUNT                NUMBER(9)          NOT NULL,
  TOKEN                      NVARCHAR2(50)      NOT NULL,
  CARDTYPE                   NVARCHAR2(15)      NOT NULL,
  EMVDATA                    NVARCHAR2(1000),
  COMPLETIONDATETIME         DATE               NOT NULL,
  ORDERNUMBER                NVARCHAR2(15)      NOT NULL,
  ACI                        NVARCHAR2(1),
  TRANSACTIONIDENTIFIER      NVARCHAR2(20),
  BANKNETDATA                NVARCHAR2(13),
  CREDITCARDTERMINALID       NUMBER(18)         NOT NULL,
  LASTUSER                   NVARCHAR2(50)      DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED               DATE               DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER         INTEGER            NOT NULL,
  DEVICETYPEINDICATOR        NUMBER(9),
  EXTERNALCARDREFERENCE      VARCHAR2(50 BYTE),
  EXTERNALCARDHASH           VARCHAR2(50 BYTE),
  LASTEMVDATAUPDATE          DATE,
  CREATIONDATE               DATE,
  ACCOUNTSTATE               NUMBER(9),
  AUTHORIZATIONTYPE          NUMBER(9),
  ISARQCREQUESTED            NUMBER(1),
  COMPLETIONTYPE             NUMBER(9),
  RETRYCOUNTDOWN             NUMBER(9),
  RISKFLAGS                  NUMBER(9)          DEFAULT 0                     NOT NULL,
  CARDSEQUENCENUMBER         NUMBER(9),
  ENCRYPTEDDATA              VARCHAR2(1000 BYTE),
  CARDLEVELRESULT            NVARCHAR2(2)
);

COMMENT ON COLUMN VSTAT_SL_CREDITCARDAUTHOR.CARDSEQUENCENUMBER IS 'The Card Sequence Number is a number used to differentiate cards that have the same
primary account number (PAN).';

COMMENT ON COLUMN VSTAT_SL_CREDITCARDAUTHOR.ENCRYPTEDDATA IS 'The ENCRYPTEDDATA column is used to store encrypted creditcard/debitcard data to re-process requests when enryption is needed';

COMMENT ON COLUMN VSTAT_SL_CREDITCARDAUTHOR.CARDLEVELRESULT IS 'A value returned by Visa, to designate the type of card product used to process the transaction.';

CREATE TABLE VSTAT_SL_CREDITCARDAUTHORLOG
(
  CREDITCARDAUTHORIZATIONLOGID  NUMBER(18)      NOT NULL,
  CREDITCARDAUTHORIZATIONID     NUMBER(18)      NOT NULL,
  INSERTTIME                    DATE            NOT NULL,
  REQUEST                       NCLOB,
  RESPONSE                      NCLOB
)
LOB (REQUEST) STORE AS (
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          64K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                  FLASH_CACHE      DEFAULT
                  CELL_FLASH_CACHE DEFAULT
                 ))
LOB (RESPONSE) STORE AS (
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          64K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                  FLASH_CACHE      DEFAULT
                  CELL_FLASH_CACHE DEFAULT
                 ));


CREATE TABLE VSTAT_SL_CREDITCARDTERMINAL
(
  CREDITCARDTERMINALID  NUMBER(18)              NOT NULL,
  EXTERNALTERMINALID    NVARCHAR2(128)          NOT NULL,
  DATAWIREID            NVARCHAR2(128),
  DEVICENUMBER          NUMBER(9),
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,
  CREDITCARDMERCHANTID  NUMBER(18)              NOT NULL
);

COMMENT ON COLUMN VSTAT_SL_CREDITCARDTERMINAL.CREDITCARDMERCHANTID IS 'Merchant this terminal belongs to';

CREATE TABLE VSTAT_SL_FAREPAYMENTERROR
(
  FAREPAYMENTERRORID  NUMBER(18)                NOT NULL,
  FAREPAYMENTREQUEST  NCLOB                     NOT NULL,
  ERROR               NCLOB                     NOT NULL,
  LASTUSER            NVARCHAR2(50)             DEFAULT 'SYS'                 NOT NULL,
  TRANSACTIONCOUNTER  INTEGER                   NOT NULL,
  LASTMODIFIED        DATE                      DEFAULT sysdate               NOT NULL,
  TRANSACTIONID       VARCHAR2(40 BYTE)
)
LOB (FAREPAYMENTREQUEST) STORE AS (
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          64K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                  FLASH_CACHE      DEFAULT
                  CELL_FLASH_CACHE DEFAULT
                 ))
LOB (ERROR) STORE AS (
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          64K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                  FLASH_CACHE      DEFAULT
                  CELL_FLASH_CACHE DEFAULT
                 ));


CREATE TABLE VSTAT_SL_PAYMENTJOURNAL
(
  PAYMENTJOURNALID      NUMBER(18)              NOT NULL,
  TRANSACTIONJOURNALID  NUMBER(18),
  SALEID                NUMBER(18),
  PAYMENTTYPE           NUMBER(9)               DEFAULT 0                     NOT NULL,
  CREDITCARDNETWORK     NUMBER(9),
  AMOUNT                NUMBER(9)               DEFAULT 0                     NOT NULL,
  CODE                  NVARCHAR2(200),
  CONFIRMED             NUMBER(1)               DEFAULT 0                     NOT NULL,
  EXECUTIONTIME         DATE                    DEFAULT SYSDATE,
  EXECUTIONRESULT       NVARCHAR2(150),
  STATE                 NUMBER(9),
  TOKEN                 NVARCHAR2(125),
  ISPRETAX              NUMBER(1)               DEFAULT 0                     NOT NULL,
  REFERENCENUMBER       NVARCHAR2(200),
  PAYMENTOPTIONID       NUMBER(18),
  MASKEDPAN             NVARCHAR2(50),
  PROVIDERREFERENCE     NVARCHAR2(50),
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,
  RECONCILIATIONSTATE   NUMBER(9)               DEFAULT 0                     NOT NULL,
  CARDID                NUMBER(18),
  ENCRYPTEDDATATYPE     NUMBER(9),
  PERSONID              NUMBER(18),
  ENCRYPTEDDATA         NCLOB
)
LOB (ENCRYPTEDDATA) STORE AS (
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          64K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                  FLASH_CACHE      DEFAULT
                  CELL_FLASH_CACHE DEFAULT
                 ));

COMMENT ON COLUMN VSTAT_SL_PAYMENTJOURNAL.CARDID IS 'Reference to card for stored value payments.';

COMMENT ON COLUMN VSTAT_SL_PAYMENTJOURNAL.ENCRYPTEDDATATYPE IS 'Encrypted payment data type.';

COMMENT ON COLUMN VSTAT_SL_PAYMENTJOURNAL.PERSONID IS 'Invoice data for this payment.';


CREATE TABLE VSTAT_SL_SALE
(
  SALEID                   NUMBER(18)           NOT NULL,
  SALETRANSACTIONID        NVARCHAR2(40)        NOT NULL,
  SALETYPE                 NUMBER(9)            NOT NULL,
  SALEDATE                 DATE                 DEFAULT SYSDATE               NOT NULL,
  CLIENTID                 NUMBER(18)           NOT NULL,
  SALESCHANNELID           NUMBER(18)           NOT NULL,
  ORDERID                  NUMBER(18),
  CONTRACTID               NUMBER(18),
  LOCATIONNUMBER           NUMBER(9),
  DEVICENUMBER             NUMBER(9),
  SALESPERSONNUMBER        NUMBER(9),
  MERCHANTNUMBER           NUMBER(9),
  RECEIPTREFERENCE         NVARCHAR2(40),
  NETWORKREFERENCE         NVARCHAR2(40),
  EXTERNALORDERNUMBER      NVARCHAR2(50),
  CANCELLATIONREFERENCEID  NUMBER(18),
  REFUNDREFERENCEID        NUMBER(18),
  ISCLOSED                 NUMBER(1)            DEFAULT 0,
  ISORGANIZATIONAL         NUMBER(1)            DEFAULT 0                     NOT NULL,
  LASTUSER                 NVARCHAR2(50)        DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED             DATE                 DEFAULT SYSDATE               NOT NULL,
  TRANSACTIONCOUNTER       INTEGER              NOT NULL
);


CREATE TABLE VSTAT_SL_TRANSACTIONJOURNAL
(
  TRANSACTIONJOURNALID       NUMBER(18)         NOT NULL,
  SALEID                     NUMBER(18),
  TRANSACTIONID              NVARCHAR2(40)      NOT NULL,
  DEVICETIME                 DATE               NOT NULL,
  BOARDINGTIME               DATE,
  SHIFTBEGIN                 DATE,
  OPERATORID                 NUMBER(10),
  CLIENTID                   NUMBER(10)         NOT NULL,
  LINE                       NVARCHAR2(20),
  COURSENUMBER               NUMBER(9),
  ROUTENUMBER                NUMBER(9),
  ROUTECODE                  NVARCHAR2(60),
  DIRECTION                  NUMBER(9),
  ZONE                       NUMBER(9),
  SALESCHANNELID             NUMBER(18)         NOT NULL,
  DEVICENUMBER               NUMBER(9),
  VEHICLENUMBER              NUMBER(9),
  MOUNTINGPLATENUMBER        NUMBER(9),
  DEBTORNUMBER               NUMBER(9)          DEFAULT 0                     NOT NULL,
  GEOLOCATIONLONGITUDE       NUMBER(9),
  GEOLOCATIONLATITUDE        NUMBER(9),
  STOPNUMBER                 NUMBER(9),
  TRANSITACCOUNTID           NUMBER(18),
  FAREMEDIAID                NVARCHAR2(100),
  FAREMEDIATYPE              NUMBER(9)          DEFAULT 0                     NOT NULL,
  PRODUCTID                  NUMBER(18),
  PRODUCTID2                 NUMBER(18),
  TICKETID                   NUMBER(18),
  FAREAMOUNT                 NUMBER(9)          DEFAULT 0                     NOT NULL,
  CUSTOMERGROUP              NUMBER(9)          DEFAULT 0                     NOT NULL,
  TRANSACTIONTYPE            NUMBER(9)          NOT NULL,
  TRANSACTIONNUMBER          NUMBER(9)          DEFAULT 0                     NOT NULL,
  RESULTTYPE                 NUMBER(9)          DEFAULT 0                     NOT NULL,
  ORIGINALONLINERESULTTYPE   NUMBER(9),
  ORIGINALDEVICERESULTTYPE   NUMBER(9),
  FILLLEVEL                  NUMBER(9),
  PURSEBALANCE               NUMBER(9),
  PURSEBALANCE2              NUMBER(9),
  PURSECREDIT                NUMBER(9),
  PURSECREDIT2               NUMBER(9),
  GROUPSIZE                  NUMBER(9),
  VALIDFROM                  DATE,
  VALIDTO                    DATE,
  DURATION                   NUMBER(18)         DEFAULT 0                     NOT NULL,
  TARIFFDATE                 DATE               NOT NULL,
  TARIFFVERSION              NUMBER(9)          DEFAULT 0                     NOT NULL,
  TICKETINTERNALNUMBER       NUMBER(9),
  TRIPTICKETINTERNALNUMBER   NUMBER(9),
  WHITELISTVERSION           NUMBER(18),
  WHITELISTVERSIONCREATED    NUMBER(18),
  CANCELLATIONREFERENCE      NUMBER(18),
  CANCELLATIONREFERENCEGUID  NVARCHAR2(40),
  COMPLETIONSTATE            NUMBER(9)          DEFAULT 0                     NOT NULL,
  RESPONSESTATE              NUMBER(9)          DEFAULT 0                     NOT NULL,
  INSERTTIME                 TIMESTAMP(6)       DEFAULT SYSTIMESTAMP,
  LASTUSER                   NVARCHAR2(50)      DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED               DATE               DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER         INTEGER            NOT NULL,
  PROPERTIES                 NUMBER(9)          DEFAULT 0,
  TRIPCOUNTER                NUMBER(9),
  CREDITCARDAUTHORIZATIONID  NUMBER(18),
  SMARTTAPID                 NVARCHAR2(50),
  SMARTTAPCOUNTER            NVARCHAR2(25),
  RESULTSOURCE               NUMBER(9),
  TRANSACTIONTIME            NUMBER(9),
  ROUNDTRIPTIME              NUMBER(9),
  JOURNEYSTARTTIME           DATE
);

COMMENT ON COLUMN VSTAT_SL_TRANSACTIONJOURNAL.SMARTTAPID IS 'Google smart tap id.';

COMMENT ON COLUMN VSTAT_SL_TRANSACTIONJOURNAL.SMARTTAPCOUNTER IS 'Google smart tap counter.';

COMMENT ON COLUMN VSTAT_SL_TRANSACTIONJOURNAL.RESULTSOURCE IS 'Source of the validation result. 0=OVS, 1=CLient.';

COMMENT ON COLUMN VSTAT_SL_TRANSACTIONJOURNAL.TRANSACTIONTIME IS 'Processing time of the transaction on the client.';

COMMENT ON COLUMN VSTAT_SL_TRANSACTIONJOURNAL.ROUNDTRIPTIME IS 'Round trip time from sending the request to the OVS to recieveing the result.';

COMMENT ON COLUMN VSTAT_SL_TRANSACTIONJOURNAL.JOURNEYSTARTTIME IS 'Start time of the journey. This won''t be updated for transfers.';


CREATE TABLE VSTAT_SL_WHITELIST
(
  WHITELISTID               NUMBER(18)          NOT NULL,
  CARDID                    NUMBER(18)          NOT NULL,
  BYTEREPRESENTATION        NVARCHAR2(2000)     NOT NULL,
  LASTUSER                  NVARCHAR2(50)       DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED              DATE                DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER        INTEGER             NOT NULL,
  FAREMEDIAID               NVARCHAR2(100)      NOT NULL,
  FAREMEDIATYPE             NUMBER(9)           NOT NULL,
  INITIATORTRANSACTIONTIME  DATE,
  INSERTTIME                TIMESTAMP(6)        DEFAULT SYSTIMESTAMP
);

CREATE TABLE VSTAT_SL_WHITELISTJOURNAL
(
  WHITELISTJOURNALID        NUMBER(18)          NOT NULL,
  CARDID                    NUMBER(18)          NOT NULL,
  BYTEREPRESENTATION        NVARCHAR2(2000)     NOT NULL,
  LASTUSER                  NVARCHAR2(50)       DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED              DATE                DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER        INTEGER             NOT NULL,
  FAREMEDIAID               NVARCHAR2(100)      NOT NULL,
  FAREMEDIATYPE             NUMBER(9)           NOT NULL,
  INITIATORTRANSACTIONTIME  DATE,
  INSERTTIME                TIMESTAMP(6)        DEFAULT SYSTIMESTAMP
);

ALTER TABLE SYSTEM_PREVIOUS_IMPORTED
MODIFY(ID_NAME VARCHAR2(100 ));

--- Update procedures

--- Correct old data
--- This statement is not active as it should not be run automatically
 
COMMIT;

PROMPT Done!

SPOOL OFF


