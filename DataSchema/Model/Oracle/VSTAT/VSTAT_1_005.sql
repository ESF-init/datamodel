SPOOL VSTAT_1_005.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.005', 'JED', 'Added EXTERNALNAME2 for STAT_TM_TIK_ATTRVALUES');


--- Extend table with column EXTERNALNAME

ALTER TABLE STAT_TM_TIK_ATTRVALUES
 ADD (EXTERNALNAME2  VARCHAR2(100));

--- Adjust update procedure

CREATE OR REPLACE PROCEDURE VASA_TM_TABLES
IS
    err_num number;
    err_msg VARCHAR2(200);
BEGIN

  BEGIN
-- *************************
      INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);
-- *************************
  -- Make sure we don't have any double entries in POST_TICKETID. Therewith it is possible to enter ticketid's into
  -- POST_TICKETID by hand.
  DELETE FROM POST_TICKETID WHERE TICKETID IN
  (SELECT unique(TICKETID) FROM STAT_RM_TRANSACTIONS, POST_SHIFTID
   WHERE POST_SHIFTID.SHIFTID = STAT_RM_TRANSACTIONS.SHIFTID);

  -- Updating all new ticketid's which were sold since the last update
    INSERT INTO POST_TICKETID (TICKETID)
    (SELECT unique(TICKETID) FROM STAT_RM_TRANSACTIONS, POST_SHIFTID
      WHERE POST_SHIFTID.SHIFTID = STAT_RM_TRANSACTIONS.SHIFTID);

    INSERT INTO system_log (action, username, content, tablename, actionresult, text)
           VALUES ('Count', $$plsql_unit, 'Tariff Tables', 'POST_TICKETID',
                   (SELECT COUNT (*) FROM POST_TICKETID), 'Ready to import values');
    COMMIT;
--------------------------------------------------------------------------------
-- Before  we start inserting information into stat_tm_tik_attrvalues we delete the tickets
-- which were used in the latest transactions.
-- Q: Why are we not deleting the ticketids in POST_TICKETID if we already have them in STAT_TM_TIK_ATTRVALUES?
-- A: In unlikely case attribute values of the tickets have changed. In this case the values will be updated as well
-- Well knowing we are not updating all tickets, we might miss updates which are done on old tickets. In case they are
-- never sold again, these values won't be updated here. In praxis, this never happend bevore. If so, use FIX_TM_TABLES
  DELETE FROM STAT_TM_TIK_ATTRVALUES WHERE ticketid in (SELECT TICKETID FROM POST_TICKETID);

    INSERT INTO stat_tm_tik_attrvalues (ticketid, clientid, serviceid, printtext, name, tickettype, internalnumber, externalnumber, fixprice, externalname, categoryid, externalname2)
      SELECT tik.ticketid, tf.clientid, tik.serviceid, tik.printtext,
              tik.name, tik.tickettype, tik.internalnumber, tik.externalnumber, tik.price AS fixprice, tik.externalname, tik.categoryid, tik.externalname2
        FROM tm_ticket@vario_productive tik,
             tm_tarif@vario_productive tf,
             post_ticketid tikid
       WHERE tik.tarifID = tf.tarifID
         AND tik.ticketID = tikid.ticketID;

-- Only update the latest tickets. This means ticketids out of POST_TICKETID
    UPDATE  /*+ NO_CPU_COSTING */ STAT_TM_TIK_ATTRVALUES
      SET (tarifarea, tarifarea_NO) =
             (SELECT view_tm_tik_attrvalue.avname, view_tm_tik_attrvalue.avno
                FROM view_tm_tik_attrvalue
                WHERE view_tm_tik_attrvalue.ticketid = stat_tm_tik_attrvalues.ticketid
                 AND view_tm_tik_attrvalue.attribclassid = 1)
      WHERE STAT_TM_TIK_ATTRVALUES.ticketid in (SELECT TICKETID FROM post_ticketid);

    UPDATE  /*+ NO_CPU_COSTING */ stat_tm_tik_attrvalues
      SET (tiktype, tiktype_NO) =
             (SELECT view_tm_tik_attrvalue.avname, view_tm_tik_attrvalue.avno
                FROM view_tm_tik_attrvalue
               WHERE view_tm_tik_attrvalue.ticketid =
                                               stat_tm_tik_attrvalues.ticketid
                 AND view_tm_tik_attrvalue.attribclassid = 3
                 )
      WHERE STAT_TM_TIK_ATTRVALUES.ticketid in (SELECT TICKETID FROM post_ticketid);

    UPDATE  /*+ NO_CPU_COSTING */ stat_tm_tik_attrvalues
      SET (farestage, farestage_NO) =
             (SELECT view_tm_tik_attrvalue.avname, view_tm_tik_attrvalue.avno
                FROM view_tm_tik_attrvalue
               WHERE view_tm_tik_attrvalue.ticketid =
                                               stat_tm_tik_attrvalues.ticketid
                 AND view_tm_tik_attrvalue.attribclassid = 7
                 )
      WHERE STAT_TM_TIK_ATTRVALUES.ticketid in (SELECT TICKETID FROM post_ticketid);

    UPDATE  /*+ NO_CPU_COSTING */ stat_tm_tik_attrvalues
      SET (tikclass, tikclass_NO) =
             (SELECT view_tm_tik_attrvalue.avname, view_tm_tik_attrvalue.avno
                 FROM view_tm_tik_attrvalue
               WHERE view_tm_tik_attrvalue.ticketid =
                                               stat_tm_tik_attrvalues.ticketid
                 AND view_tm_tik_attrvalue.attribclassid = 14
                 )
      WHERE STAT_TM_TIK_ATTRVALUES.ticketid in (SELECT TICKETID FROM post_ticketid);

    UPDATE  /*+ NO_CPU_COSTING */ stat_tm_tik_attrvalues
      SET (product, product_NO) =
             (SELECT view_tm_tik_attrvalue.avname, view_tm_tik_attrvalue.avno
                FROM view_tm_tik_attrvalue
               WHERE view_tm_tik_attrvalue.ticketid =
                                               stat_tm_tik_attrvalues.ticketid
                 AND view_tm_tik_attrvalue.attribclassid = 19
                 )
      WHERE STAT_TM_TIK_ATTRVALUES.ticketid in (SELECT TICKETID FROM post_ticketid);


--------------------------------------------------------------------------------
    DELETE FROM STAT_TM_ATTRIBUTETARIFFAREA;

    INSERT INTO stat_tm_attributetariffarea
               (number_, NAME, stat_tm_attributetariffarea.clientid)
      (SELECT UNIQUE (tarifarea_NO),
                     tarifarea AS NAME,
                     stat_tm_tik_attrvalues.CLIENTID
                FROM stat_tm_tik_attrvalues
                 );

--------------------------------------------------------------------------------
    DELETE FROM STAT_TM_ATTRIBUTETICKETTYPE;

    INSERT INTO stat_tm_attributetickettype
               (number_, NAME, stat_tm_attributetickettype.clientid)
      (SELECT UNIQUE (tiktype_NO),
                     tiktype AS NAME,
                     stat_tm_tik_attrvalues.CLIENTID
                FROM stat_tm_tik_attrvalues
                 );

--------------------------------------------------------------------------------
    DELETE FROM STAT_TM_ATTRIBUTEFARESTAGE;

    INSERT INTO stat_tm_attributefarestage
               (number_, NAME, stat_tm_attributefarestage.clientid)
          (SELECT UNIQUE (farestage_NO),
                     farestage AS NAME,
                     stat_tm_tik_attrvalues.CLIENTID
                FROM stat_tm_tik_attrvalues
                 );


--------------------------------------------------------------------------------
    DELETE FROM STAT_TM_ATTRIBUTECLASS;

    INSERT INTO stat_tm_attributeclass
               (number_, NAME, stat_tm_attributeclass.clientid)
      (SELECT UNIQUE (tikclass_NO),
                     tikclass AS NAME,
                     stat_tm_tik_attrvalues.CLIENTID
                FROM stat_tm_tik_attrvalues
                 );

--------------------------------------------------------------------------------
    DELETE FROM STAT_TM_ATTRIBUTEPRODUCT;

    INSERT INTO STAT_TM_ATTRIBUTEPRODUCT
               (number_, NAME, stat_tm_ATTRIBUTEPRODUCT.clientid)
      (SELECT UNIQUE (product_NO),
                     product AS NAME,
                     stat_tm_tik_attrvalues.CLIENTID
                FROM stat_tm_tik_attrvalues
                 );

    COMMIT;

    INSERT INTO system_previous_imported(value_id,id_name,importdate)
     (SELECT post_ticketid.ticketid,'TICKETID',sysdate FROM POST_TICKETID);

    DELETE FROM POST_TICKETID;
    COMMIT;

-- *************************
    INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
    COMMIT;  

   EXCEPTION
      WHEN OTHERS
      THEN
        EXECUTE IMMEDIATE ('ROLLBACK');
        err_num := SQLCODE;
        err_msg := TO_CHAR (err_num) || '-' || SQLERRM;
        INSERT INTO system_log (text, action, actionresult, username)
           VALUES (err_msg, 'Error', err_num, $$plsql_unit);
        COMMIT;
        RAISE;
   END;                               -- exception handlers and block end here

END VASA_TM_TABLES;
/

--- Correct old data
--- This statement is not active as it should not be run automatically

--- UPDATE STAT_TM_TIK_ATTRVALUES a SET a.EXTERNALNAME2 = (select b.externalname2 from tm_ticket@vario_productive b where b.ticketid = a.ticketid);   



COMMIT;

PROMPT Done!

SPOOL OFF
