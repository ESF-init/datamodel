SPOOL VSTAT_1_025.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.025', 'JGI', 'UPdate procedures for EMV added');

--- Update procedures

CREATE OR REPLACE PROCEDURE UpdateDataRows (T_NAME IN VARCHAR)
    IS
BEGIN
   -- Delete all cardid's  from the productive db which are going to be imported anyway
      DELETE FROM STAT_POSTPROCESSOR_SL@vario_productive
            WHERE datarowid IN (SELECT datarowid
                               FROM post_sl where tablename = T_NAME)
            AND tablename = T_NAME;

-- Get the IDs from the productive DB
      INSERT INTO post_sl
         SELECT UNIQUE (datarowid), tablename
                  FROM STAT_POSTPROCESSOR_SL@vario_productive where tablename = T_NAME;

      INSERT INTO system_log
                  (action, username, content, tablename,
                   actionresult, text
                  )
           VALUES ('Count', $$plsql_unit, T_NAME, 'POST_SL',
                   (SELECT COUNT (*)
                      FROM post_sl WHERE TABLENAME = T_NAME), 'Ready to import values'
                  );

      EXECUTE IMMEDIATE ('COMMIT');
END UpdateDataRows;
/

/* Formatted on 07/03/2018 13:24:46 (QP5 v5.269.14213.34769) */
CREATE OR REPLACE PROCEDURE VASA_SL_CARDS
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
      -- *************************
      INSERT INTO system_log (action, username)
           VALUES ('Start', $$plsql_unit);

      -- *************************
      UpdateDataRows ('SL_CARD');

      -- *************************
      INSERT INTO system_log (action,
                              username,
                              content,
                              tablename,
                              actionresult,
                              text)
              VALUES (
                        'Count',
                        $$plsql_unit,
                        'CARD',
                        'SL_CARD',
                        (SELECT COUNT (*)
                           FROM VSTAT_SL_CARD
                          WHERE CARDid IN (SELECT UNIQUE
                                                                     (datarowid)
                                                                FROM post_sl
                                                               WHERE TABLENAME =
                                                                        'SL_CARD')),
                        'Values already imported which need to be updated (reimport)');

      EXECUTE IMMEDIATE ('COMMIT');

      -- *************************
      -- Start importing: First we delete all values which are going to be reimported
      DELETE FROM VSTAT_SL_CARD
            WHERE CARDid IN (SELECT datarowid
                                                  FROM post_sl
                                                 WHERE TABLENAME = 'SL_CARD');

      -- Receive all values from the live db
      INSERT INTO VSTAT_SL_CARD (CARDID                
  ,CARDPHYSICALDETAILID
  ,CARDHOLDERID        
  ,INVENTORYSTATE      
  ,BATCHNUMBER         
  ,SERIALNUMBER        
  ,PRINTEDNUMBER       
  ,SECURITYCODE        
  ,STATE               
  ,IMPORTED            
  ,LASTUSED            
  ,EXPIRATION          
  ,BLOCKED             
  ,BLOCKINGREASON      
  ,BLOCKINGCOMMENT     
  ,STORAGELOCATIONID   
  ,ISSHARED            
  ,ISVOICEENABLED      
  ,ISTRAINING          
  ,ISPRODUCTPOOL       
  ,PURSEBALANCE        
  ,LOADCOUNTER         
  ,CHARGECOUNTER       
  ,CARDTYPE            
  ,USERGROUPID         
  ,SUBSIDYLIMIT        
  ,FAREMEDIAID         
  ,SHORTCODE           
  ,REVISION            
  ,DESCRIPTION         
  ,PARTICIPANTID       
  ,SEQUENTIALNUMBER    
  ,REPLACEDCARDID      
  ,LASTUSER            
  ,LASTMODIFIED        
  ,TRANSACTIONCOUNTER  
  ,CLIENTID            
  ,NUMBEROFPRINTS      
  ,PRINTEDDATE         )
         (SELECT CARDID                
  ,CARDPHYSICALDETAILID
  ,CARDHOLDERID        
  ,INVENTORYSTATE      
  ,BATCHNUMBER         
  ,SERIALNUMBER        
  ,PRINTEDNUMBER       
  ,SECURITYCODE        
  ,STATE               
  ,IMPORTED            
  ,LASTUSED            
  ,EXPIRATION          
  ,BLOCKED             
  ,BLOCKINGREASON      
  ,BLOCKINGCOMMENT     
  ,STORAGELOCATIONID   
  ,ISSHARED            
  ,ISVOICEENABLED      
  ,ISTRAINING          
  ,ISPRODUCTPOOL       
  ,PURSEBALANCE        
  ,LOADCOUNTER         
  ,CHARGECOUNTER       
  ,CARDTYPE            
  ,USERGROUPID         
  ,SUBSIDYLIMIT        
  ,FAREMEDIAID         
  ,SHORTCODE           
  ,REVISION            
  ,DESCRIPTION         
  ,PARTICIPANTID       
  ,SEQUENTIALNUMBER    
  ,REPLACEDCARDID      
  ,LASTUSER            
  ,LASTMODIFIED        
  ,TRANSACTIONCOUNTER  
  ,CLIENTID            
  ,NUMBEROFPRINTS      
  ,PRINTEDDATE         
            FROM SL_CARD@vario_productive t
           WHERE t.CARDid IN (SELECT DATAROWID
                                FROM POST_SL
                               WHERE TABLENAME = 'SL_CARD'));

      EXECUTE IMMEDIATE ('COMMIT');

      -- Save the imported values in the table system_previous_imported which keeps
      -- all the imported IDs for a certain time range (standard 7 days)
      INSERT INTO system_previous_imported (value_id, id_name, importdate)
         (SELECT post_sl.datarowid, 'CARDID', SYSDATE
            FROM post_sl
           WHERE TABLENAME = 'SL_CARD');

      EXECUTE IMMEDIATE ('COMMIT');

      -- Delte the ID's from the live db which were just imported
      DELETE FROM STAT_POSTPROCESSOR_SL@vario_productive
            WHERE datarowid IN (SELECT datarowid
                                  FROM post_sl
                                 WHERE TABLENAME = 'SL_CARD');

      -- Delete the POST table
      DELETE FROM post_sl
            WHERE TABLENAME = 'SL_CARD';

      INSERT INTO system_log (action, username)
           VALUES ('Finished', $$plsql_unit);

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log (text,
                                 action,
                                 actionresult,
                                 username)
              VALUES (err_msg,
                      'Error',
                      err_num,
                      $$plsql_unit);

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;
-- exception handlers and block end here
END VASA_SL_CARDS;
/

/* Formatted on 07/03/2018 11:38:09 (QP5 v5.269.14213.34769) */
CREATE OR REPLACE PROCEDURE VASA_SL_CREDITCARDAUTHOR
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
      -- *************************
      INSERT INTO system_log (action, username)
           VALUES ('Start', $$plsql_unit);

      -- *************************
      UpdateDataRows ('SL_CREDITCARDAUTHORIZATION');

      -- *************************
      INSERT INTO system_log (action,
                              username,
                              content,
                              tablename,
                              actionresult,
                              text)
              VALUES (
                        'Count',
                        $$plsql_unit,
                        'CREDITCARDAUTHORIZATION',
                        'VSTAT_SL_CREDITCARDAUTHORLOG',
                        (SELECT COUNT (*)
                           FROM VSTAT_SL_CREDITCARDAUTHOR
                          WHERE CREDITCARDAUTHORIZATIONid IN (SELECT UNIQUE
                                                                        (datarowid)
                                                                   FROM post_sl
                                                                  WHERE TABLENAME =
                                                                           'SL_CREDITCARDAUTHORIZATION')),
                        'Values already imported which need to be updated (reimport)');

      EXECUTE IMMEDIATE ('COMMIT');

      -- *************************
      -- Start importing: First we delete all values which are going to be reimported
      DELETE FROM VSTAT_SL_CREDITCARDAUTHOR
            WHERE CREDITCARDAUTHORIZATIONid IN (SELECT datarowid
                                                     FROM post_sl
                                                    WHERE TABLENAME =
                                                             'SL_CREDITCARDAUTHORIZATION');

      -- Receive all values from the live db
      INSERT INTO VSTAT_SL_CREDITCARDAUTHOR (CREDITCARDAUTHORIZATIONID  
  ,CARDID                   
  ,STATUS                   
  ,LOCALDATETIME            
  ,TRANSACTIONDATETIME      
  ,EXTERNALAUTHID           
  ,SYSTEMTRACEAUDITNUMBER   
  ,RESPONSECODE             
  ,AMOUNT                   
  ,PURSEAMOUNT              
  ,TOKEN                    
  ,CARDTYPE                 
  ,EMVDATA                  
  ,COMPLETIONDATETIME       
  ,ORDERNUMBER              
  ,ACI                      
  ,TRANSACTIONIDENTIFIER    
  ,BANKNETDATA              
  ,CREDITCARDTERMINALID     
  ,LASTUSER                 
  ,LASTMODIFIED             
  ,TRANSACTIONCOUNTER       
  ,DEVICETYPEINDICATOR      
  ,EXTERNALCARDREFERENCE    
  ,EXTERNALCARDHASH         
  ,LASTEMVDATAUPDATE        
  ,CREATIONDATE             
  ,ACCOUNTSTATE             
  ,AUTHORIZATIONTYPE        
  ,ISARQCREQUESTED          
  ,COMPLETIONTYPE           
  ,RETRYCOUNTDOWN           
  ,RISKFLAGS                
  ,CARDSEQUENCENUMBER       
  ,ENCRYPTEDDATA            
  ,CARDLEVELRESULT)
         (SELECT CREDITCARDAUTHORIZATIONID  
  ,CARDID                   
  ,STATUS                   
  ,LOCALDATETIME            
  ,TRANSACTIONDATETIME      
  ,EXTERNALAUTHID           
  ,SYSTEMTRACEAUDITNUMBER   
  ,RESPONSECODE             
  ,AMOUNT                   
  ,PURSEAMOUNT              
  ,TOKEN                    
  ,CARDTYPE                 
  ,EMVDATA                  
  ,COMPLETIONDATETIME       
  ,ORDERNUMBER              
  ,ACI                      
  ,TRANSACTIONIDENTIFIER    
  ,BANKNETDATA              
  ,CREDITCARDTERMINALID     
  ,LASTUSER                 
  ,LASTMODIFIED             
  ,TRANSACTIONCOUNTER       
  ,DEVICETYPEINDICATOR      
  ,EXTERNALCARDREFERENCE    
  ,EXTERNALCARDHASH         
  ,LASTEMVDATAUPDATE        
  ,CREATIONDATE             
  ,ACCOUNTSTATE             
  ,AUTHORIZATIONTYPE        
  ,ISARQCREQUESTED          
  ,COMPLETIONTYPE           
  ,RETRYCOUNTDOWN           
  ,RISKFLAGS                
  ,CARDSEQUENCENUMBER       
  ,ENCRYPTEDDATA            
  ,CARDLEVELRESULT          
            FROM SL_CREDITCARDAUTHORIZATION@vario_productive t
           WHERE t.CREDITCARDAUTHORIZATIONid IN (SELECT DATAROWID
                                                      FROM POST_SL
                                                     WHERE TABLENAME =
                                                              'SL_CREDITCARDAUTHORIZATION'));

      EXECUTE IMMEDIATE ('COMMIT');

      -- Save the imported values in the table system_previous_imported which keeps
      -- all the imported IDs for a certain time range (standard 7 days)
      INSERT INTO system_previous_imported (value_id, id_name, importdate)
         (SELECT post_sl.datarowid, 'CREDITCARDAUTHORIZATIONID', SYSDATE
            FROM post_sl
           WHERE TABLENAME = 'SL_CREDITCARDAUTHORIZATION');

      EXECUTE IMMEDIATE ('COMMIT');

      -- Delte the ID's from the live db which were just imported
      DELETE FROM STAT_POSTPROCESSOR_SL@vario_productive
            WHERE datarowid IN (SELECT datarowid
                                  FROM post_sl
                                 WHERE TABLENAME =
                                          'SL_CREDITCARDAUTHORIZATION');

      -- Delete the POST table
      DELETE FROM post_sl
            WHERE TABLENAME = 'SL_CREDITCARDAUTHORIZATION';

      INSERT INTO system_log (action, username)
           VALUES ('Finished', $$plsql_unit);

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log (text,
                                 action,
                                 actionresult,
                                 username)
              VALUES (err_msg,
                      'Error',
                      err_num,
                      $$plsql_unit);

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;
-- exception handlers and block end here
END VASA_SL_CREDITCARDAUTHOR;
/
/* Formatted on 07/03/2018 11:38:09 (QP5 v5.269.14213.34769) */
CREATE OR REPLACE PROCEDURE VASA_SL_CREDITCARDAUTHORNLOG
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
      -- *************************
      INSERT INTO system_log (action, username)
           VALUES ('Start', $$plsql_unit);

      -- *************************
      UpdateDataRows ('SL_CREDITCARDAUTHORIZATIONLOG');

      -- *************************
      INSERT INTO system_log (action,
                              username,
                              content,
                              tablename,
                              actionresult,
                              text)
              VALUES (
                        'Count',
                        $$plsql_unit,
                        'CREDITCARDAUTHORIZATIONLOG',
                        'VSTAT_SL_CREDITCARDAUTHORILOG',
                        (SELECT COUNT (*)
                           FROM VSTAT_SL_CREDITCARDAUTHORLOG
                          WHERE CREDITCARDAUTHORIZATIONLOGid IN (SELECT UNIQUE
                                                                        (datarowid)
                                                                   FROM post_sl
                                                                  WHERE TABLENAME =
                                                                           'SL_CREDITCARDAUTHORIZATIONLOG')),
                        'Values already imported which need to be updated (reimport)');

      EXECUTE IMMEDIATE ('COMMIT');

      -- *************************
      -- Start importing: First we delete all values which are going to be reimported
      DELETE FROM VSTAT_SL_CREDITCARDAUTHORLOG
            WHERE CREDITCARDAUTHORIZATIONLOGid IN (SELECT datarowid
                                                     FROM post_sl
                                                    WHERE TABLENAME =
                                                             'SL_CREDITCARDAUTHORIZATIONLOG');

      -- Receive all values from the live db
      INSERT INTO VSTAT_SL_CREDITCARDAUTHORLOG (CREDITCARDAUTHORIZATIONLOGID,
                                                CREDITCARDAUTHORIZATIONID,
                                                INSERTTIME,
                                                REQUEST,
                                                RESPONSE)
         (SELECT CREDITCARDAUTHORIZATIONLOGID,
                 CREDITCARDAUTHORIZATIONID,
                 INSERTTIME,
                 REQUEST,
                 RESPONSE
            FROM sl_CREDITCARDAUTHORIZATIONLOG@vario_productive t
           WHERE t.CREDITCARDAUTHORIZATIONLOGid IN (SELECT DATAROWID
                                                      FROM POST_SL
                                                     WHERE TABLENAME =
                                                              'SL_CREDITCARDAUTHORIZATIONLOG'));

      EXECUTE IMMEDIATE ('COMMIT');

      -- Save the imported values in the table system_previous_imported which keeps
      -- all the imported IDs for a certain time range (standard 7 days)
      INSERT INTO system_previous_imported (value_id, id_name, importdate)
         (SELECT post_sl.datarowid, 'CREDITCARDAUTHORIZATIONLOGID', SYSDATE
            FROM post_sl
           WHERE TABLENAME = 'SL_CREDITCARDAUTHORIZATIONLOG');

      EXECUTE IMMEDIATE ('COMMIT');

      -- Delte the ID's from the live db which were just imported
      DELETE FROM STAT_POSTPROCESSOR_SL@vario_productive
            WHERE datarowid IN (SELECT datarowid
                                  FROM post_sl
                                 WHERE TABLENAME =
                                          'SL_CREDITCARDAUTHORIZATIONLOG');

      -- Delete the POST table
      DELETE FROM post_sl
            WHERE TABLENAME = 'SL_CREDITCARDAUTHORIZATIONLOG';

      INSERT INTO system_log (action, username)
           VALUES ('Finished', $$plsql_unit);

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log (text,
                                 action,
                                 actionresult,
                                 username)
              VALUES (err_msg,
                      'Error',
                      err_num,
                      $$plsql_unit);

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;
-- exception handlers and block end here
END VASA_SL_CREDITCARDAUTHORNLOG;
/
/* Formatted on 07/03/2018 10:36:49 (QP5 v5.269.14213.34769) */
CREATE OR REPLACE PROCEDURE VASA_SL_CREDITCARDTERMINAL
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
      -- *************************
      INSERT INTO system_log (action, username)
           VALUES ('Start', $$plsql_unit);

      -- *************************
      UpdateDataRows ('SL_CREDITCARDTERMINAL');

      -- *************************
      INSERT INTO system_log (action,
                              username,
                              content,
                              tablename,
                              actionresult,
                              text)
              VALUES (
                        'Count',
                        $$plsql_unit,
                        'CREDITCARDTERMINAL',
                        'VSTAT_SL_CREDITCARDTERMINAL',
                        (SELECT COUNT (*)
                           FROM VSTAT_SL_CREDITCARDTERMINAL
                          WHERE CREDITCARDTERMINALid IN (SELECT UNIQUE
                                                                (datarowid)
                                                           FROM post_sl
                                                          WHERE TABLENAME =
                                                                   'SL_CREDITCARDTERMINAL')),
                        'Values already imported which need to be updated (reimport)');

      EXECUTE IMMEDIATE ('COMMIT');

      -- *************************
      -- Start importing: First we delete all values which are going to be reimported
      DELETE FROM VSTAT_SL_CREDITCARDTERMINAL
            WHERE CREDITCARDTERMINALid IN (SELECT datarowid
                                             FROM post_sl
                                            WHERE TABLENAME =
                                                     'SL_CREDITCARDTERMINAL');

      -- Receive all values from the live db
      INSERT INTO vstat_SL_CREDITCARDTERMINAL (CREDITCARDTERMINALID,
                                               EXTERNALTERMINALID,
                                               DATAWIREID,
                                               DEVICENUMBER,
                                               LASTUSER,
                                               LASTMODIFIED,
                                               TRANSACTIONCOUNTER,
                                               CREDITCARDMERCHANTID)
         (SELECT CREDITCARDTERMINALID,
                 EXTERNALTERMINALID,
                 DATAWIREID,
                 DEVICENUMBER,
                 LASTUSER,
                 LASTMODIFIED,
                 TRANSACTIONCOUNTER,
                 CREDITCARDMERCHANTID
            FROM sl_CREDITCARDTERMINAL@vario_productive t
           WHERE t.CREDITCARDTERMINALid IN (SELECT DATAROWID
                                              FROM POST_SL
                                             WHERE TABLENAME =
                                                      'SL_CREDITCARDTERMINAL'));

      EXECUTE IMMEDIATE ('COMMIT');

      -- Save the imported values in the table system_previous_imported which keeps
      -- all the imported IDs for a certain time range (standard 7 days)
      INSERT INTO system_previous_imported (value_id, id_name, importdate)
         (SELECT post_sl.datarowid, 'CREDITCARDTERMINALID', SYSDATE
            FROM post_sl
           WHERE TABLENAME = 'SL_CREDITCARDTERMINAL');

      EXECUTE IMMEDIATE ('COMMIT');

      -- Delte the ID's from the live db which were just imported
      DELETE FROM STAT_POSTPROCESSOR_SL@vario_productive
            WHERE datarowid IN (SELECT datarowid
                                  FROM post_sl
                                 WHERE TABLENAME = 'SL_CREDITCARDTERMINAL');

      -- Delete the POST table
      DELETE FROM post_sl
            WHERE TABLENAME = 'SL_CREDITCARDTERMINAL';

      INSERT INTO system_log (action, username)
           VALUES ('Finished', $$plsql_unit);

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log (text,
                                 action,
                                 actionresult,
                                 username)
              VALUES (err_msg,
                      'Error',
                      err_num,
                      $$plsql_unit);

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;
-- exception handlers and block end here
END VASA_SL_CREDITCARDTERMINAL;
/
/* Formatted on 07/03/2018 10:31:40 (QP5 v5.269.14213.34769) */
CREATE OR REPLACE PROCEDURE VASA_SL_FAREPAYMENTERROR
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
      -- *************************
      INSERT INTO system_log (action, username)
           VALUES ('Start', $$plsql_unit);

      -- *************************
      UpdateDataRows ('SL_FAREPAYMENTERROR');

      -- *************************
      INSERT INTO system_log (action,
                              username,
                              content,
                              tablename,
                              actionresult,
                              text)
              VALUES (
                        'Count',
                        $$plsql_unit,
                        'FAREPAYMENTERROR',
                        'VSTAT_SL_FAREPAYMENTERROR',
                        (SELECT COUNT (*)
                           FROM VSTAT_SL_FAREPAYMENTERROR
                          WHERE FAREPAYMENTERRORid IN (SELECT UNIQUE
                                                              (datarowid)
                                                         FROM post_sl
                                                        WHERE TABLENAME =
                                                                 'SL_FAREPAYMENTERROR')),
                        'Values already imported which need to be updated (reimport)');

      EXECUTE IMMEDIATE ('COMMIT');

      -- *************************
      -- Start importing: First we delete all values which are going to be reimported
      DELETE FROM VSTAT_SL_FAREPAYMENTERROR
            WHERE FAREPAYMENTERRORid IN (SELECT datarowid
                                           FROM post_sl
                                          WHERE TABLENAME =
                                                   'SL_FAREPAYMENTERROR');

      -- Receive all values from the live db
      INSERT INTO vstat_SL_FAREPAYMENTERROR (FAREPAYMENTERRORID,
                                             FAREPAYMENTREQUEST,
                                             ERROR,
                                             LASTUSER,
                                             TRANSACTIONCOUNTER,
                                             LASTMODIFIED,
                                             TRANSACTIONID)
         (SELECT FAREPAYMENTERRORID,
                 FAREPAYMENTREQUEST,
                 ERROR,
                 LASTUSER,
                 TRANSACTIONCOUNTER,
                 LASTMODIFIED,
                 TRANSACTIONID
            FROM sl_FAREPAYMENTERROR@vario_productive t
           WHERE t.FAREPAYMENTERRORid IN (SELECT DATAROWID
                                            FROM POST_SL
                                           WHERE TABLENAME =
                                                    'SL_FAREPAYMENTERROR'));

      EXECUTE IMMEDIATE ('COMMIT');

      -- Save the imported values in the table system_previous_imported which keeps
      -- all the imported IDs for a certain time range (standard 7 days)
      INSERT INTO system_previous_imported (value_id, id_name, importdate)
         (SELECT post_sl.datarowid, 'FAREPAYMENTERRORID', SYSDATE
            FROM post_sl
           WHERE TABLENAME = 'SL_FAREPAYMENTERROR');

      EXECUTE IMMEDIATE ('COMMIT');

      -- Delte the ID's from the live db which were just imported
      DELETE FROM STAT_POSTPROCESSOR_SL@vario_productive
            WHERE datarowid IN (SELECT datarowid
                                  FROM post_sl
                                 WHERE TABLENAME = 'SL_FAREPAYMENTERROR');

      -- Delete the POST table
      DELETE FROM post_sl
            WHERE TABLENAME = 'SL_FAREPAYMENTERROR';

      INSERT INTO system_log (action, username)
           VALUES ('Finished', $$plsql_unit);

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log (text,
                                 action,
                                 actionresult,
                                 username)
              VALUES (err_msg,
                      'Error',
                      err_num,
                      $$plsql_unit);

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;
-- exception handlers and block end here
END VASA_SL_FAREPAYMENTERROR;
/
/* Formatted on 07/03/2018 10:23:33 (QP5 v5.269.14213.34769) */
CREATE OR REPLACE PROCEDURE VASA_SL_PAYMENTJOURNAL
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
      -- *************************
      INSERT INTO system_log (action, username)
           VALUES ('Start', $$plsql_unit);

      -- *************************
      UpdateDataRows ('SL_PAYMENTJOURNAL');

      -- *************************
      INSERT INTO system_log (action,
                              username,
                              content,
                              tablename,
                              actionresult,
                              text)
              VALUES (
                        'Count',
                        $$plsql_unit,
                        'PAYMENTJOURNAL',
                        'VSTAT_SL_PAYMENTJOURNAL',
                        (SELECT COUNT (*)
                           FROM VSTAT_SL_PAYMENTJOURNAL
                          WHERE PAYMENTJOURNALid IN (SELECT UNIQUE
                                                            (datarowid)
                                                       FROM post_sl
                                                      WHERE TABLENAME =
                                                               'SL_PAYMENTJOURNAL')),
                        'Values already imported which need to be updated (reimport)');

      EXECUTE IMMEDIATE ('COMMIT');

      -- *************************
      -- Start importing: First we delete all values which are going to be reimported
      DELETE FROM VSTAT_SL_PAYMENTJOURNAL
            WHERE PAYMENTJOURNALid IN (SELECT datarowid
                                         FROM post_sl
                                        WHERE TABLENAME = 'SL_PAYMENTJOURNAL');

      -- Receive all values from the live db
      INSERT INTO vstat_SL_PAYMENTJOURNAL (PAYMENTJOURNALID,
                                           TRANSACTIONJOURNALID,
                                           SALEID,
                                           PAYMENTTYPE,
                                           CREDITCARDNETWORK,
                                           AMOUNT,
                                           CODE,
                                           CONFIRMED,
                                           EXECUTIONTIME,
                                           EXECUTIONRESULT,
                                           STATE,
                                           TOKEN,
                                           ISPRETAX,
                                           REFERENCENUMBER,
                                           PAYMENTOPTIONID,
                                           MASKEDPAN,
                                           PROVIDERREFERENCE,
                                           LASTUSER,
                                           LASTMODIFIED,
                                           TRANSACTIONCOUNTER,
                                           RECONCILIATIONSTATE,
                                           CARDID,
                                           ENCRYPTEDDATATYPE,
                                           PERSONID,
                                           ENCRYPTEDDATA)
         (SELECT PAYMENTJOURNALID,
                 TRANSACTIONJOURNALID,
                 SALEID,
                 PAYMENTTYPE,
                 CREDITCARDNETWORK,
                 AMOUNT,
                 CODE,
                 CONFIRMED,
                 EXECUTIONTIME,
                 EXECUTIONRESULT,
                 STATE,
                 TOKEN,
                 ISPRETAX,
                 REFERENCENUMBER,
                 PAYMENTOPTIONID,
                 MASKEDPAN,
                 PROVIDERREFERENCE,
                 LASTUSER,
                 LASTMODIFIED,
                 TRANSACTIONCOUNTER,
                 RECONCILIATIONSTATE,
                 CARDID,
                 ENCRYPTEDDATATYPE,
                 PERSONID,
                 ENCRYPTEDDATA
            FROM sl_PAYMENTJOURNAL@vario_productive t
           WHERE t.PAYMENTJOURNALid IN (SELECT DATAROWID
                                          FROM POST_SL
                                         WHERE TABLENAME =
                                                  'SL_PAYMENTJOURNAL'));

      EXECUTE IMMEDIATE ('COMMIT');

      -- Save the imported values in the table system_previous_imported which keeps
      -- all the imported IDs for a certain time range (standard 7 days)
      INSERT INTO system_previous_imported (value_id, id_name, importdate)
         (SELECT post_sl.datarowid, 'PAYMENTJOURNALID', SYSDATE
            FROM post_sl
           WHERE TABLENAME = 'SL_PAYMENTJOURNAL');

      EXECUTE IMMEDIATE ('COMMIT');

      -- Delte the ID's from the live db which were just imported
      DELETE FROM STAT_POSTPROCESSOR_SL@vario_productive
            WHERE datarowid IN (SELECT datarowid
                                  FROM post_sl
                                 WHERE TABLENAME = 'SL_PAYMENTJOURNAL');

      -- Delete the POST table
      DELETE FROM post_sl
            WHERE TABLENAME = 'SL_PAYMENTJOURNAL';

      INSERT INTO system_log (action, username)
           VALUES ('Finished', $$plsql_unit);

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log (text,
                                 action,
                                 actionresult,
                                 username)
              VALUES (err_msg,
                      'Error',
                      err_num,
                      $$plsql_unit);

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;
-- exception handlers and block end here
END VASA_SL_PAYMENTJOURNAL;
/
/* Formatted on 07/03/2018 10:14:31 (QP5 v5.269.14213.34769) */
CREATE OR REPLACE PROCEDURE VASA_SL_SALE
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
      -- *************************
      INSERT INTO system_log (action, username)
           VALUES ('Start', $$plsql_unit);

      -- *************************
      UpdateDataRows ('SL_SALE');

      -- *************************
      INSERT INTO system_log (action,
                              username,
                              content,
                              tablename,
                              actionresult,
                              text)
              VALUES (
                        'Count',
                        $$plsql_unit,
                        'SALE',
                        'VSTAT_SL_SALE',
                        (SELECT COUNT (*)
                           FROM VSTAT_SL_SALE
                          WHERE SALEid IN (SELECT UNIQUE
                                                                (datarowid)
                                                           FROM post_sl
                                                          WHERE TABLENAME =
                                                                   'SL_SALE')),
                        'Values already imported which need to be updated (reimport)');

      EXECUTE IMMEDIATE ('COMMIT');

      -- *************************
      -- Start importing: First we delete all values which are going to be reimported
      DELETE FROM VSTAT_SL_SALE
            WHERE SALEid IN (SELECT datarowid
                                             FROM post_sl
                                            WHERE TABLENAME =
                                                     'SL_SALE');

      -- Receive all values from the live db
      INSERT INTO vstat_SL_SALE (SALEID                   
  ,SALETRANSACTIONID      
  ,SALETYPE               
  ,SALEDATE               
  ,CLIENTID               
  ,SALESCHANNELID         
  ,ORDERID                
  ,CONTRACTID             
  ,LOCATIONNUMBER         
  ,DEVICENUMBER           
  ,SALESPERSONNUMBER      
  ,MERCHANTNUMBER         
  ,RECEIPTREFERENCE       
  ,NETWORKREFERENCE       
  ,EXTERNALORDERNUMBER    
  ,CANCELLATIONREFERENCEID
  ,REFUNDREFERENCEID      
  ,ISCLOSED               
  ,ISORGANIZATIONAL       
  ,LASTUSER               
  ,LASTMODIFIED           
  ,TRANSACTIONCOUNTER)
         (SELECT SALEID                   
  ,SALETRANSACTIONID      
  ,SALETYPE               
  ,SALEDATE               
  ,CLIENTID               
  ,SALESCHANNELID         
  ,ORDERID                
  ,CONTRACTID             
  ,LOCATIONNUMBER         
  ,DEVICENUMBER           
  ,SALESPERSONNUMBER      
  ,MERCHANTNUMBER         
  ,RECEIPTREFERENCE       
  ,NETWORKREFERENCE       
  ,EXTERNALORDERNUMBER    
  ,CANCELLATIONREFERENCEID
  ,REFUNDREFERENCEID      
  ,ISCLOSED               
  ,ISORGANIZATIONAL       
  ,LASTUSER               
  ,LASTMODIFIED           
  ,TRANSACTIONCOUNTER     
            FROM sl_SALE@vario_productive t
           WHERE t.SALEid IN (SELECT DATAROWID
                                              FROM POST_SL
                                             WHERE TABLENAME =
                                                      'SL_SALE'));

      EXECUTE IMMEDIATE ('COMMIT');

      -- Save the imported values in the table system_previous_imported which keeps
      -- all the imported IDs for a certain time range (standard 7 days)
      INSERT INTO system_previous_imported (value_id, id_name, importdate)
         (SELECT post_sl.datarowid, 'SALEID', SYSDATE
            FROM post_sl
           WHERE TABLENAME = 'SL_SALE');

      EXECUTE IMMEDIATE ('COMMIT');

      -- Delte the ID's from the live db which were just imported
      DELETE FROM STAT_POSTPROCESSOR_SL@vario_productive
            WHERE datarowid IN (SELECT datarowid
                                  FROM post_sl
                                 WHERE TABLENAME = 'SL_SALE');

      -- Delete the POST table
      DELETE FROM post_sl
            WHERE TABLENAME = 'SL_SALE';

      INSERT INTO system_log (action, username)
           VALUES ('Finished', $$plsql_unit);

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log (text,
                                 action,
                                 actionresult,
                                 username)
              VALUES (err_msg,
                      'Error',
                      err_num,
                      $$plsql_unit);

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;
-- exception handlers and block end here
END VASA_SL_SALE;
/

/* Formatted on 07/03/2018 10:14:31 (QP5 v5.269.14213.34769) */
CREATE OR REPLACE PROCEDURE VASA_SL_TRANSACTIONJOURNAL
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
      -- *************************
      INSERT INTO system_log (action, username)
           VALUES ('Start', $$plsql_unit);

      -- *************************
      UpdateDataRows ('SL_TRANSACTIONJOURNAL');

      -- *************************
      INSERT INTO system_log (action,
                              username,
                              content,
                              tablename,
                              actionresult,
                              text)
              VALUES (
                        'Count',
                        $$plsql_unit,
                        'TRANSACTIONJOURNAL',
                        'VSTAT_SL_TRANSACTIONJOURNAL',
                        (SELECT COUNT (*)
                           FROM VSTAT_SL_TRANSACTIONJOURNAL
                          WHERE transactionjournalid IN (SELECT UNIQUE
                                                                (datarowid)
                                                           FROM post_sl
                                                          WHERE TABLENAME =
                                                                   'SL_TRANSACTIONJOURNAL')),
                        'Values already imported which need to be updated (reimport)');

      EXECUTE IMMEDIATE ('COMMIT');

      -- *************************
      -- Start importing: First we delete all values which are going to be reimported
      DELETE FROM VSTAT_SL_TRANSACTIONJOURNAL
            WHERE transactionjournalid IN (SELECT datarowid
                                             FROM post_sl
                                            WHERE TABLENAME =
                                                     'SL_TRANSACTIONJOURNAL');

      -- Receive all values from the live db
      INSERT INTO vstat_SL_TRANSACTIONJOURNAL (TRANSACTIONJOURNALID,
                                               SALEID,
                                               TRANSACTIONID,
                                               DEVICETIME,
                                               BOARDINGTIME,
                                               SHIFTBEGIN,
                                               OPERATORID,
                                               CLIENTID,
                                               LINE,
                                               COURSENUMBER,
                                               ROUTENUMBER,
                                               ROUTECODE,
                                               DIRECTION,
                                               ZONE,
                                               SALESCHANNELID,
                                               DEVICENUMBER,
                                               VEHICLENUMBER,
                                               MOUNTINGPLATENUMBER,
                                               DEBTORNUMBER,
                                               GEOLOCATIONLONGITUDE,
                                               GEOLOCATIONLATITUDE,
                                               STOPNUMBER,
                                               TRANSITACCOUNTID,
                                               FAREMEDIAID,
                                               FAREMEDIATYPE,
                                               PRODUCTID,
                                               PRODUCTID2,
                                               TICKETID,
                                               FAREAMOUNT,
                                               CUSTOMERGROUP,
                                               TRANSACTIONTYPE,
                                               TRANSACTIONNUMBER,
                                               RESULTTYPE,
                                               ORIGINALONLINERESULTTYPE,
                                               ORIGINALDEVICERESULTTYPE,
                                               FILLLEVEL,
                                               PURSEBALANCE,
                                               PURSEBALANCE2,
                                               PURSECREDIT,
                                               PURSECREDIT2,
                                               GROUPSIZE,
                                               VALIDFROM,
                                               VALIDTO,
                                               DURATION,
                                               TARIFFDATE,
                                               TARIFFVERSION,
                                               TICKETINTERNALNUMBER,
                                               TRIPTICKETINTERNALNUMBER,
                                               WHITELISTVERSION,
                                               WHITELISTVERSIONCREATED,
                                               CANCELLATIONREFERENCE,
                                               CANCELLATIONREFERENCEGUID,
                                               COMPLETIONSTATE,
                                               RESPONSESTATE,
                                               INSERTTIME,
                                               LASTUSER,
                                               LASTMODIFIED,
                                               TRANSACTIONCOUNTER,
                                               PROPERTIES,
                                               TRIPCOUNTER,
                                               CREDITCARDAUTHORIZATIONID,
                                               SMARTTAPID,
                                               SMARTTAPCOUNTER,
                                               RESULTSOURCE,
                                               TRANSACTIONTIME,
                                               ROUNDTRIPTIME,
                                               JOURNEYSTARTTIME)
         (SELECT TRANSACTIONJOURNALID,
                 SALEID,
                 TRANSACTIONID,
                 DEVICETIME,
                 BOARDINGTIME,
                 SHIFTBEGIN,
                 OPERATORID,
                 CLIENTID,
                 LINE,
                 COURSENUMBER,
                 ROUTENUMBER,
                 ROUTECODE,
                 DIRECTION,
                 ZONE,
                 SALESCHANNELID,
                 DEVICENUMBER,
                 VEHICLENUMBER,
                 MOUNTINGPLATENUMBER,
                 DEBTORNUMBER,
                 GEOLOCATIONLONGITUDE,
                 GEOLOCATIONLATITUDE,
                 STOPNUMBER,
                 TRANSITACCOUNTID,
                 FAREMEDIAID,
                 FAREMEDIATYPE,
                 PRODUCTID,
                 PRODUCTID2,
                 TICKETID,
                 FAREAMOUNT,
                 CUSTOMERGROUP,
                 TRANSACTIONTYPE,
                 TRANSACTIONNUMBER,
                 RESULTTYPE,
                 ORIGINALONLINERESULTTYPE,
                 ORIGINALDEVICERESULTTYPE,
                 FILLLEVEL,
                 PURSEBALANCE,
                 PURSEBALANCE2,
                 PURSECREDIT,
                 PURSECREDIT2,
                 GROUPSIZE,
                 VALIDFROM,
                 VALIDTO,
                 DURATION,
                 TARIFFDATE,
                 TARIFFVERSION,
                 TICKETINTERNALNUMBER,
                 TRIPTICKETINTERNALNUMBER,
                 WHITELISTVERSION,
                 WHITELISTVERSIONCREATED,
                 CANCELLATIONREFERENCE,
                 CANCELLATIONREFERENCEGUID,
                 COMPLETIONSTATE,
                 RESPONSESTATE,
                 INSERTTIME,
                 LASTUSER,
                 LASTMODIFIED,
                 TRANSACTIONCOUNTER,
                 PROPERTIES,
                 TRIPCOUNTER,
                 CREDITCARDAUTHORIZATIONID,
                 SMARTTAPID,
                 SMARTTAPCOUNTER,
                 RESULTSOURCE,
                 TRANSACTIONTIME,
                 ROUNDTRIPTIME,
                 JOURNEYSTARTTIME
            FROM sl_transactionjournal@vario_productive t
           WHERE t.transactionjournalid IN (SELECT DATAROWID
                                              FROM POST_SL
                                             WHERE TABLENAME =
                                                      'SL_TRANSACTIONJOURNAL'));

      EXECUTE IMMEDIATE ('COMMIT');

      -- Save the imported values in the table system_previous_imported which keeps
      -- all the imported IDs for a certain time range (standard 7 days)
      INSERT INTO system_previous_imported (value_id, id_name, importdate)
         (SELECT post_sl.datarowid, 'TRANSACTIONJOURNALID', SYSDATE
            FROM post_sl
           WHERE TABLENAME = 'SL_TRANSACTIONJOURNAL');

      EXECUTE IMMEDIATE ('COMMIT');

      -- Delte the ID's from the live db which were just imported
      DELETE FROM STAT_POSTPROCESSOR_SL@vario_productive
            WHERE datarowid IN (SELECT datarowid
                                  FROM post_sl
                                 WHERE TABLENAME = 'SL_TRANSACTIONJOURNAL');

      -- Delete the POST table
      DELETE FROM post_sl
            WHERE TABLENAME = 'SL_TRANSACTIONJOURNAL';

      INSERT INTO system_log (action, username)
           VALUES ('Finished', $$plsql_unit);

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log (text,
                                 action,
                                 actionresult,
                                 username)
              VALUES (err_msg,
                      'Error',
                      err_num,
                      $$plsql_unit);

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;
-- exception handlers and block end here
END VASA_SL_TRANSACTIONJOURNAL;
/


CREATE OR REPLACE PROCEDURE VASA_SL_WHITELIST
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
-- *************************
      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Start', $$plsql_unit
                  );

-- *************************
     UpdateDataRows ('SL_WHITELIST');

-- *************************
      INSERT INTO system_log
                  (action, username, content, tablename,
                   actionresult,
                   text
                  )
           VALUES ('Count', $$plsql_unit, 'Whitelist', 'VSTAT_SL_WHITELIST',
                   (SELECT COUNT (*)
                      FROM VSTAT_SL_WHITELIST
                     WHERE whitelistid IN (SELECT UNIQUE (datarowid)
                                               FROM post_sl WHERE TABLENAME = 'SL_WHITELIST')),
                   'Values already imported which need to be updated (reimport)'
                  );

      EXECUTE IMMEDIATE ('COMMIT');

-- *************************
-- Start importing: First we delete all values which are going to be reimported
      DELETE FROM VSTAT_SL_WHITELIST
            WHERE whitelistid IN (SELECT datarowid FROM post_sl WHERE TABLENAME = 'SL_WHITELIST');

-- Receive all values from the live db
      INSERT INTO vstat_sl_whitelist        -- columns without tablename are always from sl_card
                  (WHITELISTID,CARDID,BYTEREPRESENTATION,LASTUSER,LASTMODIFIED,TRANSACTIONCOUNTER,FAREMEDIAID,FAREMEDIATYPE,INITIATORTRANSACTIONTIME,INSERTTIME
                  )
         (SELECT WHITELISTID,CARDID,BYTEREPRESENTATION,LASTUSER,LASTMODIFIED,TRANSACTIONCOUNTER,FAREMEDIAID,FAREMEDIATYPE,INITIATORTRANSACTIONTIME,INSERTTIME
            FROM sl_whitelist@vario_productive whitelist 
           WHERE whitelist.whitelistid IN (SELECT DATAROWID FROM POST_SL WHERE TABLENAME = 'SL_WHITELIST'));

      EXECUTE IMMEDIATE ('COMMIT');

      -- Save the imported values in the table system_previous_imported which keeps
      -- all the imported IDs for a certain time range (standard 7 days)
      INSERT INTO system_previous_imported
                  (value_id, id_name, importdate)
         (SELECT post_sl.datarowid, 'WHITELISTID', SYSDATE
            FROM post_sl WHERE TABLENAME = 'SL_WHITELIST');

      EXECUTE IMMEDIATE ('COMMIT');

-- Delte the ID's from the live db which were just imported
      DELETE FROM STAT_POSTPROCESSOR_SL@vario_productive
            WHERE datarowid IN (SELECT datarowid
                               FROM post_sl WHERE TABLENAME = 'SL_WHITELIST');

-- Delete the POST table
      DELETE FROM post_sl WHERE TABLENAME = 'SL_WHITELIST';

      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Finished', $$plsql_unit
                  );

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log
                     (text, action, actionresult, username
                     )
              VALUES (err_msg, 'Error', err_num, $$plsql_unit
                     );

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;
-- exception handlers and block end here
END VASA_SL_WHITELIST;
/

CREATE OR REPLACE PROCEDURE VASA_SL_WHITELISTJOURNAL
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
   BEGIN
-- *************************
      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Start', $$plsql_unit
                  );

-- *************************
     UpdateDataRows ('SL_WHITELISTJOURNAL');

-- *************************
      INSERT INTO system_log
                  (action, username, content, tablename,
                   actionresult,
                   text
                  )
           VALUES ('Count', $$plsql_unit, 'Whitelistjournal', 'VSTAT_SL_WHITELISTJOURNAL',
                   (SELECT COUNT (*)
                      FROM VSTAT_SL_WHITELISTJOURNAL
                     WHERE whitelistjournalid IN (SELECT UNIQUE (datarowid)
                                               FROM post_sl WHERE TABLENAME = 'SL_WHITELISTJOURNAL')),
                   'Values already imported which need to be updated (reimport)'
                  );

      EXECUTE IMMEDIATE ('COMMIT');

-- *************************
-- Start importing: First we delete all values which are going to be reimported
      DELETE FROM VSTAT_SL_WHITELISTJOURNAL
            WHERE whitelistjournalid IN (SELECT datarowid FROM post_sl WHERE TABLENAME = 'SL_WHITELISTJOURNAL');

-- Receive all values from the live db
      INSERT INTO vstat_SL_WHITELISTJOURNAL     
      
      (WHITELISTJOURNALID,CARDID,BYTEREPRESENTATION,LASTUSER,LASTMODIFIED,TRANSACTIONCOUNTER,FAREMEDIAID,FAREMEDIATYPE,INITIATORTRANSACTIONTIME,INSERTTIME)
         (SELECT WHITELISTJOURNALID,CARDID,BYTEREPRESENTATION,LASTUSER,LASTMODIFIED,TRANSACTIONCOUNTER,FAREMEDIAID,FAREMEDIATYPE,INITIATORTRANSACTIONTIME,INSERTTIME
            FROM sl_whitelistjournal@vario_productive w
           WHERE w.whitelistjournalid IN (SELECT DATAROWID FROM POST_SL WHERE TABLENAME = 'SL_WHITELISTJOURNAL'));

      EXECUTE IMMEDIATE ('COMMIT');

      -- Save the imported values in the table system_previous_imported which keeps
      -- all the imported IDs for a certain time range (standard 7 days)
      INSERT INTO system_previous_imported
                  (value_id, id_name, importdate)
         (SELECT post_sl.datarowid, 'WHITELISTJOURNALID', SYSDATE
            FROM post_sl WHERE TABLENAME = 'SL_WHITELISTJOURNAL');

      EXECUTE IMMEDIATE ('COMMIT');

-- Delte the ID's from the live db which were just imported
      DELETE FROM STAT_POSTPROCESSOR_SL@vario_productive
            WHERE datarowid IN (SELECT datarowid
                               FROM post_sl WHERE TABLENAME = 'SL_WHITELISTJOURNAL');

-- Delete the POST table
      DELETE FROM post_sl WHERE TABLENAME = 'SL_WHITELISTJOURNAL';

      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Finished', $$plsql_unit
                  );

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log
                     (text, action, actionresult, username
                     )
              VALUES (err_msg, 'Error', err_num, $$plsql_unit
                     );

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;
-- exception handlers and block end here
END VASA_SL_WHITELISTJOURNAL;
/

CREATE OR REPLACE PROCEDURE VASA_REFRESH_SL
IS
   
BEGIN

--------------------------------------------------------------------------------
-- Jobs to update the SL-tables
--------------------------------------------------------------------------------
 BEGIN
    VASA_SL_CARDS;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
     VASA_SL_CREDITCARDAUTHOR;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_CREDITCARDAUTHORNLOG;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_CREDITCARDTERMINAL;
   END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_FAREPAYMENTERROR;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_PAYMENTJOURNAL;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_SALE;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_TRANSACTIONJOURNAL;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_WHITELIST;
 END;
   
--------------------------------------------------------------------------------
 BEGIN
      VASA_SL_WHITELISTJOURNAL;
 END;
   
--------------------------------------------------------------------------------

END VASA_REFRESH_SL;
/

CREATE OR REPLACE PROCEDURE VASA_REFRESH
IS
   err_num   NUMBER;

   err_msg   VARCHAR2 (200);
   v_lockTimeFrame number := 1440/1440;
   v_runID NUMBER(18);
   v_runCnt NUMBER;
BEGIN
-- *************************
   INSERT INTO system_log (action, username, text)
        VALUES ('Start', $$plsql_unit, '**** Masterscipt started ****');

--------------------------------------------------------------------------------
-- Initialise protection against multiple executing
--------------------------------------------------------------------------------  
   SELECT NVL(max(runID),1) INTO v_runID FROM sys_runstat;
   v_runID := v_runID + 1;        
   BEGIN 
     SELECT count(*) INTO v_runCnt FROM sys_runstat 
      WHERE runName = $$plsql_unit
        AND lockTime > sysdate;
     IF v_runCnt > 0 THEN
       INSERT INTO sys_runstat(runID, runName, startTime, endTime, result)
         VALUES (v_runID, $$plsql_unit, sysdate, sysdate, 'detected running instance and stopped');     
       RETURN;
     ELSE
       INSERT INTO sys_runstat(runID, runName, startTime, lockTime)
         VALUES (v_runID, $$plsql_unit, sysdate, sysdate+v_lockTimeFrame);     
     END IF;     
   END;
   
--------------------------------------------------------------------------------
-- VASA_preparations: Job to prepare several values for other jobs
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
    vasa_preparations;
   END;

--------------------------------------------------------------------------------
-- Jobs to update the 1to1-tables
-- at the very beginning because they are independent from others and may be referred from others
-- Requires other job to run:    none
-- Job type: full refresh
--------------------------------------------------------------------------------
   BEGIN
     vasa_refresh_OneToOneTables;
   END;
   
--------------------------------------------------------------------------------
-- vasa_stat_sl_photograph: Job to prepare STAT_Photograph tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
     vasa_stat_sl_photograph;
   END;
   
--------------------------------------------------------------------------------
-- VASA_CONTRACTS: Job to prepare STAT_Contracts tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
     vasa_contracts;
   END;

--------------------------------------------------------------------------------
-- VASA_DEBTOR: Job to prepare STAT_dm_debtor tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_debtor;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_FARESTAGES: Job the prepare the STAT_TM_FARESTAGENAMES
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_tm_farestages;
   END;

--------------------------------------------------------------------------------
-- VASA_CARDS: Job to prepare STAT_CARD tables
-- Requires other job to run:     none
--------------------------------------------------------------------------------
   BEGIN
      vasa_cards;
   END;

--------------------------------------------------------------------------------
-- VASA_STAT_RM_SHIFT: Job the prepare the STAT_RM_SHIFT
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update
--------------------------------------------------------------------------------
   BEGIN
      vasa_stat_rm_shift;
   END;

--------------------------------------------------------------------------------
-- VASA_STAT_RM_TRANSACTIONS: Job the prepare the STAT_RM_TRASNACTIONS
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_TM_FARESTAGES (for the function to get the farestage names)
--------------------------------------------------------------------------------
   BEGIN
     vasa_stat_rm_transactions;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_TABLES: Job the prepare all the STAT_TM_* tables
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_STAT_RM_TRANSACTIONS (to get all the used ticketids)
--------------------------------------------------------------------------------
   BEGIN
     vasa_tm_tables;
   END;

--------------------------------------------------------------------------------
-- VASA_TM_TABLES: Job the prepare all the STAT_TM_* tables
-- Requires other job to run:
--      - VASA_preparations (to get the latest shiftids for an incremntal update)
--      - VASA_STAT_RM_TRANSACTIONS (to get all the used line and linenames)
--------------------------------------------------------------------------------
   BEGIN
      vasa_line;
   END;

--------------------------------------------------------------------------------
-- vasa_postprocessing: Job for cleaning up
-- Requires other job to run:    none
-- Job type: Clening job for other processes
--------------------------------------------------------------------------------
   BEGIN
      vasa_postprocessing;
   END;

--------------------------------------------------------------------------------
-- vasa_depot: Job to update master data DEPOT
-- Requires other job to run:    none
-- Job type: Full table refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_depot;
   END;

--------------------------------------------------------------------------------
-- vasa_card: Job to update master data CARDACTIONREQUEST
-- Requires other job to run:    none
-- Job type: Full table refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_stat_pp_cardactionrequest;
   END;

--------------------------------------------------------------------------------
-- vasa_ACCOUNTENTRYID: Job to update the accountentries of the live db
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_accountentryid;
   END;

--------------------------------------------------------------------------------
-- VASA_DM_CARDS: Job to update the DM_CARDs of the live db
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_dm_cards;
   END;

--------------------------------------------------------------------------------
-- VASA_STOP: Job to update the STAT_VARIO_STOP
-- Requires other job to run:    none
-- Job type: full refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_stop;
   END;

--------------------------------------------------------------------------------
-- VASA_PP_PRODUCTONCARD: Job to update the stat_pp_productoncard
-- Requires other job to run:    none
-- Job type: Incremental refresh
--------------------------------------------------------------------------------
   BEGIN
      vasa_pp_productoncard;
   END;

--------------------------------------------------------------------------------
-- VASA_RM_BalancePoint: Job to decide whether and in case create a new balance point
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_RM_BalancePoint;
   END;
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: UPdates the inspector logons
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_STAT_INSPECTOR_LOGON;
   END;   
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: Updates the STAT_MOBSTAT_TRIPS table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------
   BEGIN
      VASA_STAT_MOBSTAT_TRIPS;
   END;   
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: Updates the STAT_RM_EXTERNALDATA table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------   
   BEGIN
      VASA_STAT_RM_EXTERNALDATA;
   END;
   
--------------------------------------------------------------------------------
-- VASA_STAT_INSPECTOR_LOGON: Updates the STAT_TM_TICKETCATEGORY table
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------   
   BEGIN
      VASA_STAT_TM_TICKETCATEGORY;
   END;

--------------------------------------------------------------------------------
-- VASA_REFRESH_SL
-- Requires other job to run:    none
-- Job type: non refresh
--------------------------------------------------------------------------------   
   BEGIN
      VASA_REFRESH_SL;
   END;
   
   
   
--------------------------------------------------------------------------------
-- VASA_CLEAN_PREVIOUSIMPORTED: Job to clean values which are stored for a reimport and statisc overview
--                              A parameter in this procedure describes how many days are kept in the sysstem (Standard:30)
-- Requires other job to run:    none
-- Job type: Cleaning procedure
--------------------------------------------------------------------------------
   /*BEGIN
      --vasa_clean_previousimported; Is started in a second script once a day
   END;*/
   
--------------------------------------------------------------------------------
-- Finalise protection against multiple executing (unlock)
--------------------------------------------------------------------------------  
   BEGIN
     UPDATE sys_runstat 
        SET lockTime = NULL, endTime = sysdate, result = 'finished'
      WHERE runID = v_runID;
   END;    

   INSERT INTO system_log (action, username, text)
        VALUES ('Finished', $$plsql_unit, '*** Masterscript finished ***');

   EXECUTE IMMEDIATE ('COMMIT');
END vasa_refresh;
/



--- Correct old data
--- This statement is not active as it should not be run automatically
 
COMMIT;

PROMPT Done!

SPOOL OFF



