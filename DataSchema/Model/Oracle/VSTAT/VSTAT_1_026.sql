SPOOL VSTAT_1_026.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.026', 'JGI', 'Added column SHIFTBEGIN to table STAT_MOBSTAT_TRIPS');

----Alter table

ALTER TABLE STAT_MOBSTAT_TRIPS
 ADD (SHIFTBEGIN  DATE);
 
----Adding function to convert date values

CREATE OR REPLACE FUNCTION ConvertDate(secSinceMidnight IN NUMBER, operating_day DATE)
 RETURN DATE
IS
  dateValue DATE;
  secValue NUMBER;
BEGIN
   secValue := secSinceMidnight;
   IF( secSinceMidnight > 86400 )
   THEN
       secValue := secSinceMidnight - 86400;
       dateValue := TO_DATE( to_char(operating_day, 'DD.MM.YYYY') || ' ' || to_char(trunc(sysdate) + ( secValue / ( 60 * 60 * 24 ) ), 'HH24:MI:SS'), 'DD.MM.YYYY HH24:MI:SS') + 1;
   ELSE
       dateValue := TO_DATE( to_char(operating_day, 'DD.MM.YYYY') || ' ' || to_char(trunc(sysdate) + ( secValue / ( 60 * 60 * 24 ) ), 'HH24:MI:SS'), 'DD.MM.YYYY HH24:MI:SS');
   END IF;    
   RETURN dateValue;
END;
/


--- Update procedures

CREATE OR REPLACE PROCEDURE VASA_STAT_MOBSTAT_TRIPS
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
-- *************************
   INSERT INTO system_log
               (action, username
               )
        VALUES ('Start', $$plsql_unit
               );

   BEGIN
-- *************************
-- Count how many values are copied

      -- Count how many values are copied
      INSERT INTO system_log
                  (action, username, content, tablename, actionresult, text)
           VALUES ('Count', $$plsql_unit, 'Shift', 'POST_SHIFTID',
                   (SELECT COUNT (UNIQUE (event_no)) FROM MOBSTAT_VARIO_TRIPS
                     WHERE event_no NOT IN (SELECT SEQNO FROM STAT_MOBSTAT_TRIPS)),
                   'Values, which need to be imported ');

  

------------------------------------------------------------------
      INSERT INTO STAT_MOBSTAT_TRIPS
                  (STARTTIME_TRIP_HHMMSS,ENDTIME_TRIP_HHMMSS,DRIVER_LOGONTIME_HHMMSS,SEQNO,OPERATINGDAY_DATE,STARTTIME_TRIP_RAW,ENDTIME_TRIP_RAW,STARTTIME_BLOCK_RAW,RUNNINGBOARD,DEBTORNO,VEHICLENO,DRIVER_LOGONTIME_RAW,SERVICE_TYPE,DIRECTION,PATTERN,TRIP_NAME,LOCATIONNO,SHIFTBEGIN)
         (SELECT ACT_DEP_TIME_TRIP_HHMMSS,ACT_END_TIME_TRIP_HHMMSS,ACT_DEP_TIME_DRVR_HHMMSS,EVENT_NO,OPD_DATE,ACT_DEP_TIME_TRIP,ACT_END_TIME_TRIP,ACT_DEP_TIME_BLOCK,BLOCK_CODE,DRIVER_ID,VEHICLE_NAME,ACT_DEP_TIME_DRVR,TRIP_ROLE,PATTERN_DIRECTION,PATTERN,TRIP_NAME,VEHICLE_NAME,ConvertDate(driver_logontime_raw, operatingday_date)
            FROM view_stat_mobstat_trips
           WHERE  event_no NOT IN (SELECT seqno FROM STAT_MOBSTAT_TRIPS));

      INSERT INTO system_log
                  (action, username
                  )
           VALUES ('Finished', $$plsql_unit
                  );

      EXECUTE IMMEDIATE ('COMMIT');
   EXCEPTION
      WHEN OTHERS
      THEN
         EXECUTE IMMEDIATE ('ROLLBACK');

         err_num := SQLCODE;
         err_msg := TO_CHAR (err_num) || '-' || SQLERRM;

         INSERT INTO system_log
                     (text, action, actionresult, username
                     )
              VALUES (err_msg, 'Error', err_num, $$plsql_unit
                     );

         EXECUTE IMMEDIATE ('COMMIT');

         RAISE;
   END;                               -- exception handlers and block end here
END VASA_STAT_MOBSTAT_TRIPS;
/


--- Correct old data
--- This statement is not active as it should not be run automatically

---update stat_mobstat_trips set shiftbegin = ConvertDate(driver_logontime_raw, operatingday_date);
 
COMMIT;

PROMPT Done!

SPOOL OFF



