SPOOL VSTAT_1_031.log

INSERT INTO vstat_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '1.031', 'JGI', 'Added more updates for look-up tables');

--- New view view_stat_mobstat_trips 


--- Adjust update procedure

CREATE OR REPLACE PROCEDURE VASA_VARIO_DEVICECLASS
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);

  -- Because its not to much data we refresh the whole table
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'deviceclasses', 'VARIO_DEVICECLASS',
               (SELECT COUNT(*) FROM VARIO_DEVICECLASS), 'values before update');  
  DELETE FROM VARIO_DEVICECLASS;
  INSERT INTO VARIO_DEVICECLASS 
            (DEVICECLASSID,NAME,SHORTNAME,SALEDEVICE,CREATELS,TYPEOFUNITID,IMAGEFILE,GENERALACCOUNT,VISIBLE,IMSAPPLICATIONNAME,LOCKABLE,RELEASEVISIBILITY,RELEASENAME,TMVISIBILITY,CREATETARIFFARCHIVE,PARAMETERRELEASEVISIBILITY,UNZIPENABLED)  
     (SELECT DEVICECLASSID,NAME,SHORTNAME,SALEDEVICE,CREATELS,TYPEOFUNITID,IMAGEFILE,GENERALACCOUNT,VISIBLE,IMSAPPLICATIONNAME,LOCKABLE,RELEASEVISIBILITY,RELEASENAME,TMVISIBILITY,CREATETARIFFARCHIVE,PARAMETERRELEASEVISIBILITY,UNZIPENABLED
       FROM VARIO_DEVICECLASS@vario_productive);
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'deviceclasses', 'VARIO_DEVICECLASS',
               (SELECT COUNT(*) FROM VARIO_DEVICECLASS), 'values after update');  
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
  
EXCEPTION
  WHEN OTHERS
  THEN
    EXECUTE IMMEDIATE ('ROLLBACK');
    err_num := SQLCODE;
    err_msg := TO_CHAR (err_num) || '-' || SQLERRM;
    INSERT INTO system_log (text, action, actionresult, username)
         VALUES (err_msg, 'Error', err_num, $$plsql_unit);
    EXECUTE IMMEDIATE ('COMMIT');
    RAISE;
  -- exception handlers and block end here
END VASA_VARIO_DEVICECLASS;
/

CREATE OR REPLACE PROCEDURE VASA_TM_TICKETTYPE
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);

  -- Because its not to much data we refresh the whole table
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'tickettypes', 'TM_TICKETTYPE',
               (SELECT COUNT(*) FROM TM_TICKETTYPE), 'values before update');  
  DELETE FROM TM_TICKETTYPE;
  INSERT INTO TM_TICKETTYPE 
            (TICKETTYPEID,NAME,DESCRIPTION,EVENDTYPEID,EVENDBALANCING,EVENDPAYMASK,TRANSACTIONTYPEID,ENUMERATIONVALUE)  
     (SELECT TICKETTYPEID,NAME,DESCRIPTION,EVENDTYPEID,EVENDBALANCING,EVENDPAYMASK,TRANSACTIONTYPEID,ENUMERATIONVALUE
       FROM TM_TICKETTYPE@vario_productive);
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'tickettypes', 'TM_TICKETTYPE',
               (SELECT COUNT(*) FROM TM_TICKETTYPE), 'values after update');  
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
  
EXCEPTION
  WHEN OTHERS
  THEN
    EXECUTE IMMEDIATE ('ROLLBACK');
    err_num := SQLCODE;
    err_msg := TO_CHAR (err_num) || '-' || SQLERRM;
    INSERT INTO system_log (text, action, actionresult, username)
         VALUES (err_msg, 'Error', err_num, $$plsql_unit);
    EXECUTE IMMEDIATE ('COMMIT');
    RAISE;
  -- exception handlers and block end here
END VASA_TM_TICKETTYPE;
/

CREATE OR REPLACE PROCEDURE VASA_RM_SHIFTSTATE
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);

  -- Because its not to much data we refresh the whole table
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'shiftstates', 'RM_SHIFTSTATE',
               (SELECT COUNT(*) FROM RM_SHIFTSTATE), 'values before update');  
  DELETE FROM RM_SHIFTSTATE;
  INSERT INTO RM_SHIFTSTATE  
            (SHIFTSTATEID,BOOK,STATENAME,VISIBLE)  
     (SELECT SHIFTSTATEID,BOOK,STATENAME,VISIBLE
       FROM RM_SHIFTSTATE@vario_productive);
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'shiftstates', 'RM_SHIFTSTATE',
               (SELECT COUNT(*) FROM RM_SHIFTSTATE), 'values after update');  
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
  
EXCEPTION
  WHEN OTHERS
  THEN
    EXECUTE IMMEDIATE ('ROLLBACK');
    err_num := SQLCODE;
    err_msg := TO_CHAR (err_num) || '-' || SQLERRM;
    INSERT INTO system_log (text, action, actionresult, username)
         VALUES (err_msg, 'Error', err_num, $$plsql_unit);
    EXECUTE IMMEDIATE ('COMMIT');
    RAISE;
  -- exception handlers and block end here
END VASA_RM_SHIFTSTATE;
/


CREATE OR REPLACE PROCEDURE VASA_RM_DEVICEBOOKINGSTATE
IS
   err_num   NUMBER;
   err_msg   VARCHAR2 (200);
BEGIN
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);

  -- Because its not to much data we refresh the whole table
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'devicebookingstates', 'RM_DEVICEBOOKINGSTATE',
               (SELECT COUNT(*) FROM RM_DEVICEBOOKINGSTATE), 'values before update');  
  DELETE FROM RM_DEVICEBOOKINGSTATE;
  INSERT INTO RM_DEVICEBOOKINGSTATE 
            (DEVICEBOOKINGSTATEID,TEXT,DEVICEBOOKINGNO,ISBOOKED)  
     (SELECT DEVICEBOOKINGSTATEID,TEXT,DEVICEBOOKINGNO,ISBOOKED
       FROM RM_DEVICEBOOKINGSTATE@vario_productive);
  INSERT INTO system_log (action, username, content, tablename, actionresult, text)
       VALUES ('Count', $$plsql_unit, 'devicebookingstates', 'RM_DEVICEBOOKINGSTATE',
               (SELECT COUNT(*) FROM RM_DEVICEBOOKINGSTATE), 'values after update');  
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
  
EXCEPTION
  WHEN OTHERS
  THEN
    EXECUTE IMMEDIATE ('ROLLBACK');
    err_num := SQLCODE;
    err_msg := TO_CHAR (err_num) || '-' || SQLERRM;
    INSERT INTO system_log (text, action, actionresult, username)
         VALUES (err_msg, 'Error', err_num, $$plsql_unit);
    EXECUTE IMMEDIATE ('COMMIT');
    RAISE;
  -- exception handlers and block end here
END VASA_RM_DEVICEBOOKINGSTATE;
/

CREATE OR REPLACE PROCEDURE vasa_refresh_OneToOneTables
IS
BEGIN
   INSERT INTO system_log (action, username) VALUES ('Start', $$plsql_unit);
  -- *************************   
      vasa_stat_SL_autoloadSetting;
      vasa_stat_SL_product;
      vasa_stat_SL_paymentoption;
      vasa_stat_SL_contract;
      vasa_stat_SL_order;
      vasa_stat_SL_orderdetail;
      vasa_stat_SL_organization;
      vasa_stat_SL_address;
      vasa_stat_SL_person;      
      vasa_stat_SL_payment;
      vasa_um_device;
      vasa_um_unit;
      VASA_RM_DEVICEPAYMENTMETHOD;
      VASA_TM_TARIF;
      VASA_STAT_TM_TICKETGROUP;
      VASA_STAT_TM_TICKETTOGROUP;
      VASA_RM_TRANSACTIONTYPE;
      VASA_VARIO_DEVICECLASS;
      VASA_TM_TICKETTYPE;
      VASA_RM_SHIFTSTATE;
      VASA_RM_DEVICEBOOKINGSTATE;
  -- *************************
  INSERT INTO system_log (action, username) VALUES ('Finished', $$plsql_unit);
  EXECUTE IMMEDIATE ('COMMIT');
END vasa_refresh_OneToOneTables;
/

--- Correct old data
--- This statement is not active as it should not be run automatically

COMMIT;

PROMPT Done!

SPOOL OFF