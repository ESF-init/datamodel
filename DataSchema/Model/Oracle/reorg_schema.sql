set heading off
set feedback off

-- Protokolltabellen
var ts_idx varchar2(20)
var ts_protocol varchar2(20)
var db_schema varchar2(20)

-- EXAMPLE:
--define ts_prot = MOBILE_PROT
--exec :ts_idx := 'MOBILE_IDX'
--exec :ts_protocol := 'MOBILE_PROT'
--exec :db_schema := 'SCHEMA_TEST'

-- PLEASE USE CAPITAL LETTERS!!!!!
define ts_prot = <add prot tablespace>
exec :ts_idx := '<add index tablespace>'
exec :ts_protocol := '<add protocol tablespace>'
exec :db_schema := '<add db_schema>'


ALTER TABLE PROTOCOL MOVE TABLESPACE &ts_prot;
ALTER TABLE PROT_ACTION MOVE TABLESPACE &ts_prot;
ALTER TABLE PROT_FCT_GROUP MOVE TABLESPACE &ts_prot;
ALTER TABLE PROT_FUNCTION MOVE TABLESPACE &ts_prot;
ALTER TABLE PROT_MESSAGE MOVE TABLESPACE &ts_prot;
ALTER TABLE PROT_NOTIFICATION MOVE TABLESPACE &ts_prot;
ALTER TABLE PROT_TO_NOTIFICATION MOVE TABLESPACE &ts_prot;


spool rebuildIndex.sql
set heading off
set feedback off
/* Formatted on 27/03/2017 14:04:23 (QP5 v5.269.14213.34769) */
SELECT    'ALTER INDEX '
       || UPPER (INDEX_NAME)
       || ' REBUILD TABLESPACE '
       || :ts_idx
       || ';'
  FROM USER_INDEXES
 WHERE     TABLE_OWNER = UPPER ( :db_schema)
       AND INDEX_TYPE <> 'LOB'
       AND UPPER (TABLE_NAME) NOT IN ('PROTOCOL',
                                      'PROT_ACTION',
                                      'PROT_FCT_GROUP',
                                      'PROT_FUNCTION',
                                      'PROT_MESSAGE',
                                      'PROT_NOTIFICATION',
                                      'PROT_TO_NOTIFICATION') AND UPPER(TABLESPACE_NAME) <> :ts_idx;
spool off;									  
@@rebuildIndex.sql  

spool rebuildprotIndex.sql
set heading off
set feedback off                                      
/* Formatted on 27/03/2017 14:04:23 (QP5 v5.269.14213.34769) */
SELECT    'ALTER INDEX '
       || UPPER (INDEX_NAME)
       || ' REBUILD TABLESPACE '
       || :ts_protocol
       || ';'
  FROM USER_INDEXES
 WHERE     TABLE_OWNER = UPPER ( :db_schema)
       AND INDEX_TYPE <> 'LOB'
       AND UPPER (TABLE_NAME) IN ('PROTOCOL',
                                      'PROT_ACTION',
                                      'PROT_FCT_GROUP',
                                      'PROT_FUNCTION',
                                      'PROT_MESSAGE',
                                      'PROT_NOTIFICATION',
                                      'PROT_TO_NOTIFICATION') AND UPPER(TABLESPACE_NAME) <> :ts_protocol;
spool off;
@@rebuildprotIndex.sql    

EXEC DBMS_UTILITY.compile_schema(schema => :db_schema);

exit;

