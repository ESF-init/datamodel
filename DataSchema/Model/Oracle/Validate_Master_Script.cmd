REM *************************************************************************           
REM --                                                                                  
REM --  Create user                                                                     
REM --                                                                                  
REM --  MOBILEvario                                                                     
REM --                                                                                  
REM -------------------------------------------------------------------------           
REM --                                                                                  
REM --  Execute the user-script                                                         
REM --                                                                                  
SET ORACLE_SID=MOBILE 
SQLPLUS /NOLOG @.\Validate_Master_Script.sql > Validate_Master_Script.log                                   

SQLPLUS schema_test/fvs@MOBILE.SRVSENECTUS @.\Create_Vario_Schema.sql 
                                                                                        
PAUSE                                                                                   
