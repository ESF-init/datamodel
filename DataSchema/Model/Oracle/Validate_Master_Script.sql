  
SET ECHO ON                                                                   
                         .                                                                             
CONNECT SYS/SYSINIT@MOBILE.SRVSENECTUS AS SYSDBA; 

DROP USER schema_test CASCADE;
                                              
.                                                                             
CREATE USER schema_test                                                        
  IDENTIFIED BY         fvs                                          
  DEFAULT TABLESPACE    MOBILE_DAT                                             
  TEMPORARY TABLESPACE  MOBILE_TMP                                             
  PROFILE               DEFAULT;                                              
.                                                                             
GRANT CREATE SESSION TO schema_test; 
GRANT CREATE TABLE TO schema_test; 
GRANT CREATE VIEW TO schema_test;
GRANT CREATE SEQUENCE TO schema_test; 
GRANT CREATE TRIGGER TO schema_test; 
GRANT CREATE SYNONYM TO schema_test;
GRANT CREATE PROCEDURE TO schema_test;
GRANT CREATE MATERIALIZED VIEW TO schema_test;
GRANT CREATE TYPE TO schema_test;


ALTER USER schema_test QUOTA 1G ON MOBILE_DAT; 

                                                                                                                                     
.                                                                             
EXIT;                                                                         
