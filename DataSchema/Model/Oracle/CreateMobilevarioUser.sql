SET ECHO ON                                                                   
.                                                                             
CONNECT SYS/SYSINIT@BTSW11.SRVGERAS AS SYSDBA; 

DROP USER mobilevario CASCADE;
                                              
.                                                                             
CREATE USER mobilevario
  IDENTIFIED BY         fvs                                          
  DEFAULT TABLESPACE    MOBILE_DAT                                             
  TEMPORARY TABLESPACE  MOBILE_TMP                                             
  PROFILE               DEFAULT;                                              
.                                                                             
GRANT CREATE SESSION TO mobilevario; 
GRANT CREATE TABLE TO mobilevario; 
GRANT CREATE VIEW TO mobilevario;
GRANT CREATE SEQUENCE TO mobilevario; 
GRANT CREATE TRIGGER TO mobilevario; 
GRANT CREATE SYNONYM TO mobilevario;
GRANT CREATE PROCEDURE TO mobilevario;
GRANT CREATE MATERIALIZED VIEW TO mobilevario;
GRANT CREATE TYPE TO mobilevario;


ALTER USER mobilevario QUOTA 1G ON MOBILE_DAT; 

                                                                                                                                     
.                                                                             
EXIT;                                                                         
