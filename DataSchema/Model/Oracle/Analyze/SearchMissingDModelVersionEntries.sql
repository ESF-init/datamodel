-- This statement is searching for missing vario_dmodellversion entries.


select 
    r_start "Start of missing versions", --Start of missing version number range
    r_stop "End of missing versions", --End of missing version number range
    r_stop - r_start + 1 "Count of missing versions" --Count of missing version scripts
from 
(
    with DModellVersions as
    (
        select to_number(replace(dmodellno, '.', '')) pn 
        from vario_dmodellversion 
		where REGEXP_LIKE(dmodellno, '^[0-9.]*$') --filter version numbers with . and Numbers
    )
    select 
        m.pn + 1 as r_start,
        (select min(pn) - 1 from DModellVersions x where x.pn > m.pn) as r_stop
    from DModellVersions m
    left outer join DModellVersions r on m.pn = r.pn - 1
    where r.pn is null
)
where r_stop is not null
order by r_start desc;