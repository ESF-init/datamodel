--Is the SALESVOLUME equal to the transactions?

select s.shiftid, s.debtorno, t.EXPECTEDAMOUNT, s.SALESVOLUME from rm_shift s
join (
select t.shiftid SHIFTID, sum(t.PRICE) EXPECTEDAMOUNT 
from rm_transaction t
join rm_devicebookingstate dbs on t.devicebookingstate = dbs.DEVICEBOOKINGNO
where t.Cancellationid = 0
and dbs.isbooked != 0
and t.typeid != 1
group by t.shiftid
) t on s.shiftid = t.shiftid
where t.expectedamount != s.SALESVOLUME
and shiftstateid != 30
order by s.SHIFTBEGIN desc;