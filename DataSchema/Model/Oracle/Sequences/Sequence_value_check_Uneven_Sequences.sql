-- FOR USE IN REPLICATED (2 SERVER) ENVIRONMENT !!! --
-- FOR UNEVEN PK SEQUENCES SERVER --
SET serveroutput ON
DECLARE

	cnt NUMBER:=0;
	a   VARCHAR2(50);
	b   VARCHAR2(50);
	fixit VARCHAR2(4000):='fix script'||chr(13);
  
	type t_seq_rec IS
	  record
	  (
		seqname  VARCHAR2(50),
		tblname  VARCHAR2(50),
		tblcolpk VARCHAR2(50) );
	
	type t_seq_tab IS
		TABLE OF t_seq_rec INDEX BY pls_integer;
	
	seq_tab t_seq_tab;
	
	CURSOR getseq IS
		SELECT sequence_name 
		FROM   user_sequences
		WHERE  sequence_name not in ('SL_ATTRIBUTEVALUETOPERSON_SEQ','DATAID','SEQ_PROTOCOLID','SEQ_SM_CHILDPARENTCOMPRULEID',
            'SEQ_SM_DASHBOARD','SEQ_SM_IMSJOB','SL_CARDHOLDER_SEQ','SL_CONFIGURATION_SEQ','SL_CREDITCARDAUTHLOG_SEQ',
            'SL_CUSTOMERACCTIFICATION_SEQ','SL_DEVICECOMMHISTORY_SEQ','SL_DEVICEREADERRESOURCE_SEQ','SL_RULEVIOLTOTRANS_SEQ');

	CURSOR gettabandcol (c_seq VARCHAR2) IS
		SELECT acc.table_name,acc.column_name
		FROM   all_constraints ac,
		       all_cons_columns acc
		WHERE ac.constraint_type   = 'P'
		  AND ac.constraint_name   = acc.constraint_name
		  AND ac.owner             = acc.owner
		  AND acc.owner            ='FVS'
		  AND acc.table_name       = (SELECT 
										CASE
											WHEN substr(sequence_name,instr(sequence_name,'_',-1)) = '_SEQ' THEN substr(sequence_name,1,instr(sequence_name,'_',-1)-1)
											WHEN substr(sequence_name,1,4) = 'SEQ_' THEN substr(sequence_name,instr(sequence_name,'_',1)+1)
											ELSE sequence_name
										END AS seqname
										FROM user_sequences
										WHERE sequence_name=c_seq)
		ORDER BY acc.table_name,acc.position;
	
	PROCEDURE generate_report IS
		seq_idx pls_integer;
		sql_qry VARCHAR2(150);
    sql_subQuery  VARCHAR2(1000);
		val1 NUMBER;
		inc1 NUMBER;
		val2 NUMBER;
		jmp1 NUMBER;
    v_seq varchar2(50);
    v_tab varchar2(50);
    v_col varchar2(50);
	BEGIN
		seq_idx := seq_tab.first;
		LOOP
			EXIT WHEN seq_idx IS NULL;
			
						
			-- grab the current value and increment size for the sequence
			v_seq:=seq_tab(seq_idx).seqname;
			sql_qry:= 'SELECT last_number,increment_by FROM user_sequences WHERE sequence_name = '''|| v_seq ||'''';
			--DBMS_OUTPUT.PUT_LINE(chr(9)||sql_qry);
			EXECUTE IMMEDIATE sql_qry INTO val1,inc1;
			--DBMS_OUTPUT.PUT_LINE (chr(9)||'value is: '||val1||' increment_by: '||inc1);			
			
			-- grab max value from primary key
 			v_tab:=seq_tab(seq_idx).tblname;
			v_col:=seq_tab(seq_idx).tblcolpk;
			sql_qry:= '(SELECT nvl(max('||v_col||'),0) FROM '|| v_tab ||' WHERE  MOD('||v_col||',2) <> 0)' ; 
			--	sql_qry:= 'SELECT nvl(max('||v_col||'),0) FROM '|| v_tab ||'' ;     
 	
			--DBMS_OUTPUT.PUT_LINE(chr(9)||sql_qry);
      
			EXECUTE IMMEDIATE sql_qry INTO val2;
			--DBMS_OUTPUT.PUT_LINE (chr(9)||'max value is: '||val2);		
			
			-- run tests
			if val1 <= val2 then 
				-- print out info
				dbms_output.put_line('Sequence = ' || seq_tab(seq_idx).seqname || ' table name = ' ||seq_tab(seq_idx).tblname || ' primary key = ' || seq_tab(seq_idx).tblcolpk );
				DBMS_OUTPUT.PUT_LINE (chr(9)||'ERROR:  Sequence value ('||val1||') is lower than primary key value('||val2||')' );
				DBMS_OUTPUT.PUT_LINE (chr(9)||'INFO: ' || v_seq || ' NEXTVAL:' || val1 || 'INC:' || inc1 || 'Highest uneven PK:'|| val2 ||'');				
				DBMS_OUTPUT.PUT_LINE('----');
				-- add fix script here
				sql_qry:= 'select '||val2||'-'||val1||'+20 from dual';
				--DBMS_OUTPUT.PUT_LINE(chr(9)||sql_qry);
				EXECUTE IMMEDIATE sql_qry INTO jmp1;				
				fixit:=fixit||'alter sequence '||v_seq||' increment by '|| jmp1 ||';'||chr(13);
				-- then reset back to standard increment after the jump!!!
				fixit:=fixit||'alter sequence '||v_seq||' increment by 2;'||chr(13);
			else
     --  if MOD(val1, 2) <> 0 then
        --  DBMS_OUTPUT.PUT_LINE (chr(9)||'WARNING: NexValue is not even. It is ('||val1||') :' || v_seq ||'');
      if MOD(val1, 2) = 0 then
          DBMS_OUTPUT.PUT_LINE (chr(9)||'WARNING: NexValue is even. It is ('||val1||') :' || v_seq ||'');
      end if;
      if inc1 <> 2 then
        DBMS_OUTPUT.PUT_LINE (chr(9)||'WARNING: Increment value is not 2. It is ('||inc1||') :' || v_seq ||'');
        -- add fix script here
        fixit:=fixit||'alter sequence '||v_seq||' increment by 2'||chr(13);
      
      else
        null;
        -- for debugging purposes
        DBMS_OUTPUT.PUT_LINE (chr(9)||'ALL GOOD:' || v_seq || ' NEXTVAL:' || val1 || '   INC:' || inc1 || '   highest uneven PK:'|| val2 ||'');					
      end if;	
       
			end if;

			seq_idx := seq_tab.next(seq_idx);
		END LOOP;
		-- print out fix script
		DBMS_OUTPUT.PUT_LINE(fixit);
	
	END;

BEGIN
	FOR R IN getseq
	LOOP
		cnt:=cnt+1;
		seq_tab(cnt).seqname := R.sequence_name;
		--dbms_output.put_line(chr(9)||'passing in '||seq_tab(cnt).seqname);
		OPEN gettabandcol(seq_tab(cnt).seqname);
		LOOP
			FETCH gettabandcol INTO a,b;
			EXIT WHEN gettabandcol%notfound;
			seq_tab(cnt).tblname  := a;
			seq_tab(cnt).tblcolpk := b;
		END LOOP;
		CLOSE gettabandcol;
	END LOOP;
	
	-- add the non-linear matches!  i.e. sl_whitelistjournal uses the sl_whitelist_seq sequence
	-- add the following four lines for each new entry. Must have only a SINGLE column for primary key!
	-- SL_WHITELISTJOURNAL
	cnt:=cnt+1;
	seq_tab(cnt).seqname  := 'SL_WHITELIST_SEQ';
	seq_tab(cnt).tblname  := 'SL_WHITELISTJOURNAL';
	seq_tab(cnt).tblcolpk := 'WHITELISTJOURNALID';
	

	generate_report;
  
END;