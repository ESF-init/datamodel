-- ==========================================================================================================================================
-- THIS SCRIPT IS NOT TESTABLE! Autocommit with drop and create sequence!! ==================================================================
-- ==========================================================================================================================================

-- This script resets all sales data in the database
-- Useful when the customer goes live and the test data have to be removed


-- ==========================================================================================================================================
-- ===== Non-SL-Tables ======================================================================================================================
-- ==========================================================================================================================================

-- DM_DEBTOR
update dm_debtor set accountbalance = 0;
delete from dm_debtor where debtorno in (0); --delete only preselected debtors 

delete from user_list where userid in (0); --delete only preselected users

delete from user_ismember isMember where not exists(select * from user_list where userid = isMember.userid);


-- MAX_ENTRYNO
update max_entryno
set maxentryno = 1;

-- RM_TRANSACTION
Delete from RM_TRANSACTION;

-- RM_BINARYDATA
Delete from RM_BINARYDATA;

-- VARIO_NUMBER_RANGE
Delete from VARIO_NUMBER_RANGE;

-- PP_ACCOUNTENTRY
Delete from PP_ACCOUNTENTRY;

-- DM_CARDHISTORY
Delete from DM_CARDHISTORY;

-- DM_CARDCLEARINGHISTORY
Delete from DM_CARDCLEARINGHISTORY;

-- PP_ACCOUNT
Delete from PP_ACCOUNT;

-- VARIO_SETTLEMENT
Delete from VARIO_SETTLEMENT;

-- PP_ACCOUNTBALANCE
Delete from PP_ACCOUNTBALANCE;

-- RM_DUTYBALANCE
Delete from RM_DUTYBALANCE;

-- RM_EXPORT
Delete from RM_EXPORT;

-- ROE_BUSINESSTRANSACTION
Delete from ROE_BUSINESSTRANSACTION;

-- RM_SHIFTFILELOCATION
Delete from RM_SHIFTFILELOCATION;

-- RM_SHIFT
Delete from RM_SHIFT;

-- FILELOCATION
Delete from FILELOCATION;

-- PROT_MESSAGE
TRUNCATE TABLE PROT_MESSAGE;

-- PROT_ACCOUNTEVENT
Delete from PROT_ACCOUNTEVENT;

-- PROT_ACCOUNTEVENTHEADER
Delete from PROT_ACCOUNTEVENTHEADER;

-- PROTOCOL
Delete from PROTOCOL;

-- RM_CDSPAYMENT
Delete from RM_CDSPAYMENT;

-- AM_COMPONENTCLEARING
Delete from AM_COMPONENTCLEARING;

-- AM_CASHSERVICEBALANCE
Delete from AM_CASHSERVICEBALANCE;

-- AM_CASHSERVICEDATA
Delete from AM_CASHSERVICEDATA;

-- AM_FILELOCATION
Delete from AM_FILELOCATION;

-- AM_COMPONENTEXCHANGE
Delete from AM_COMPONENTEXCHANGE;

-- AM_CASHSERVICE
Delete from AM_CASHSERVICE;

-- AM_COMPONENTDATA
Delete from AM_COMPONENTDATA;

-- AM_COMPONENTSTATE
Delete from AM_COMPONENTSTATE;

-- AM_COMPONENTFILLING
Delete from AM_COMPONENTFILLING;

-- AM_COMPONENT
Delete from AM_COMPONENT;

-- AM_AUTOMAT
Delete from AM_AUTOMAT;

-- FILEERRORS
Delete from FILEERRORS;

-- FILELOCATION
Delete from FILELOCATION;

-- RM_SHIFTINVENTORY
Delete from RM_SHIFTINVENTORY;



-- ==========================================================================================================================================
-- ===== SL-Tables ==========================================================================================================================
-- ==========================================================================================================================================

UPDATE SL_NUMBERGROUP SET CURRENTVALUE = STARTVALUE , TRANSACTIONCOUNTER = TRANSACTIONCOUNTER+1;

UPDATE SL_CONTRACT SET ORGANIZATIONID = NULL , TRANSACTIONCOUNTER = TRANSACTIONCOUNTER+1 WHERE ORGANIZATIONID IS NOT NULL;


-- SL_FAREPAYMENTERROR
Delete from SL_FAREPAYMENTERROR;
Drop Sequence SL_FAREPAYMENTERROR_SEQ;
CREATE SEQUENCE SL_FAREPAYMENTERROR_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- ACC_SALESREVENUE
Delete from ACC_SALESREVENUE;
Drop Sequence ACC_SALESREVENUE_SEQ;
CREATE SEQUENCE ACC_SALESREVENUE_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- ACC_REVENUERECOGNITION
Delete from ACC_REVENUERECOGNITION;
Drop Sequence ACC_REVENUERECOGNITION_SEQ;
CREATE SEQUENCE ACC_REVENUERECOGNITION_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- ACC_PAYMENTRECOGNITION
Delete from ACC_PAYMENTRECOGNITION;
Drop Sequence ACC_PAYMENTRECOGNITION_SEQ;
CREATE SEQUENCE ACC_PAYMENTRECOGNITION_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- ACC_PAYMENTRECONCILIATION
Delete from ACC_PAYMENTRECONCILIATION;
Drop Sequence ACC_PAYMENTRECONCILIATION_SEQ;
CREATE SEQUENCE ACC_PAYMENTRECONCILIATION_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- ACC_BANKSTATEMENT
Delete from ACC_BANKSTATEMENT;
Drop Sequence ACC_BANKSTATEMENT_SEQ;
CREATE SEQUENCE ACC_BANKSTATEMENT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- ACC_REVENUESETTLEMENT
Delete from ACC_REVENUESETTLEMENT;
Drop Sequence ACC_REVENUESETTLEMENT_SEQ;
CREATE SEQUENCE ACC_REVENUESETTLEMENT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- ACC_CLOSEOUTPERIOD
Delete from ACC_CLOSEOUTPERIOD;
Drop Sequence ACC_CLOSEOUTPERIOD_SEQ;
CREATE SEQUENCE ACC_CLOSEOUTPERIOD_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- ACC_SETTLEDREVENUE
Delete from ACC_SETTLEDREVENUE;

-- SL_CAPPINGJOURNAL
Delete from SL_CAPPINGJOURNAL;
Drop Sequence SL_CAPPINGJOURNAL_SEQ;
CREATE SEQUENCE SL_CAPPINGJOURNAL_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_PAYMENTJOURNAL
Delete from SL_PAYMENTJOURNAL;
Drop Sequence SL_PAYMENTJOURNAL_SEQ;
CREATE SEQUENCE SL_PAYMENTJOURNAL_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_ORDERDETAILTOPERSON
Delete from SL_ORDERDETAILTOPERSON;

-- SL_RULEVIOLATIONTOTRANSACTION
Delete from SL_RULEVIOLATIONTOTRANSACTION;

-- SL_RULEVIOLATION
Delete from SL_RULEVIOLATION;
Drop Sequence SL_RULEVIOLATION_SEQ;
CREATE SEQUENCE SL_RULEVIOLATION_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_CARDTORULEVIOLATION
Delete from SL_CARDTORULEVIOLATION;
Drop Sequence SL_CARDTORULEVIOLATION_SEQ;
CREATE SEQUENCE SL_CARDTORULEVIOLATION_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_PAYMENTTOCOMPONENT
Delete from SL_PAYMENTTOCOMPONENT;

-- SL_PAYMENTJOURNALTOCOMPONENT
Delete from SL_PAYMENTJOURNALTOCOMPONENT;

-- SL_ORGANIZATIONPARTICIPANT
Delete from SL_ORGANIZATIONPARTICIPANT;
Drop Sequence SL_ORGANIZATIONPARTICIPANT_SEQ;
CREATE SEQUENCE SL_ORGANIZATIONPARTICIPANT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_TRANSACTIONJOURNAL
Delete from SL_TRANSACTIONJOURNAL;
Drop Sequence SL_TRANSACTIONJOURNAL_SEQ;
CREATE SEQUENCE SL_TRANSACTIONJOURNAL_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_WHITELIST
Delete from SL_WHITELIST;
Drop Sequence SL_WHITELIST_SEQ;
CREATE SEQUENCE SL_WHITELIST_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_WHITELISTJOURNAL
Delete from SL_WHITELISTJOURNAL;

-- SL_AUTOLOADSETTING
Delete from SL_AUTOLOADSETTING;
Drop Sequence SL_AUTOLOADSETTING_SEQ;
CREATE SEQUENCE SL_AUTOLOADSETTING_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_ORDERDETAIL
Delete from SL_ORDERDETAIL;
Drop Sequence SL_ORDERDETAIL_SEQ;
CREATE SEQUENCE SL_ORDERDETAIL_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_ORDERHISTORY
Delete from SL_ORDERHISTORY;
Drop Sequence SL_ORDERHISTORY_SEQ;
CREATE SEQUENCE SL_ORDERHISTORY_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_SALE
Delete from SL_SALE;
Drop Sequence SL_SALE_SEQ;
CREATE SEQUENCE SL_SALE_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_WORKITEMHISTORY
Delete from SL_WORKITEMHISTORY;
Drop Sequence SL_WORKITEMHISTORY_SEQ;
CREATE SEQUENCE SL_WORKITEMHISTORY_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_PRODUCT
Delete from SL_PRODUCT;
Drop Sequence SL_PRODUCT_SEQ;
CREATE SEQUENCE SL_PRODUCT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_VERIFICATIONATTEMPT
Delete from SL_VERIFICATIONATTEMPT;
Drop Sequence SL_VERIFICATIONATTEMPT_SEQ;
CREATE SEQUENCE SL_VERIFICATIONATTEMPT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_TRANSACTIONTOINSPECTION
Delete from SL_TRANSACTIONTOINSPECTION;

-- SL_WORKITEM
Delete from SL_WORKITEM;
Drop Sequence SL_WORKITEM_SEQ;
CREATE SEQUENCE SL_WORKITEM_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_SECURITYRESPONSE
Delete from SL_SECURITYRESPONSE;
Drop Sequence SL_SECURITYRESPONSE_SEQ;
CREATE SEQUENCE SL_SECURITYRESPONSE_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_PRODUCTSUBSIDY
Delete from SL_PRODUCTSUBSIDY;
Drop Sequence SL_PRODUCTSUBSIDY_SEQ;
CREATE SEQUENCE SL_PRODUCTSUBSIDY_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_PRICEADJUSTMENT
Delete from SL_PRICEADJUSTMENT;
Drop Sequence SL_PRICEADJUSTMENT_SEQ;
CREATE SEQUENCE SL_PRICEADJUSTMENT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_CARDEVENT
Delete from SL_CARDEVENT;
Drop Sequence SL_CARDEVENT_SEQ;
CREATE SEQUENCE SL_CARDEVENT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_CARDTOCONTRACT
Delete from SL_CARDTOCONTRACT;

-- SL_SECURITYRESPONSE_ENC
Delete from SL_SECURITYRESPONSE_ENC;
Drop Sequence SL_SECURITYRESPONSE_ENC_SEQ;
CREATE SEQUENCE SL_SECURITYRESPONSE_ENC_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_CARDSTOCKTRANSFER
Delete from SL_CARDSTOCKTRANSFER;

-- SL_CREDITCARDAUTHORIZATION
Delete from SL_CREDITCARDAUTHORIZATION;
Drop Sequence SL_CREDITCARDAUTHORIZATION_SEQ;
CREATE SEQUENCE SL_CREDITCARDAUTHORIZATION_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_SURVEYANSWER
Delete from SL_SURVEYANSWER;

-- SL_ORDER
Delete from SL_ORDER;
Drop Sequence SL_ORDER_SEQ;
CREATE SEQUENCE SL_ORDER_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

DELETE from SL_CUSTOMERACCTOVERATTEMPTTYPE;

-- SL_CUSTOMERACCOUNT
Delete from SL_CUSTOMERACCOUNT;
Drop Sequence SL_CUSTOMERACCOUNT_SEQ;
CREATE SEQUENCE SL_CUSTOMERACCOUNT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_ORGANIZATION
Delete from SL_ORGANIZATION;
Drop Sequence SL_ORGANIZATION_SEQ;
CREATE SEQUENCE SL_ORGANIZATION_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_PHOTOGRAPH
Delete from SL_PHOTOGRAPH;
Drop Sequence SL_PHOTOGRAPH_SEQ;
CREATE SEQUENCE SL_PHOTOGRAPH_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_STOCKTRANSFER
Delete from SL_STOCKTRANSFER;
Drop Sequence SL_STOCKTRANSFER_SEQ;
CREATE SEQUENCE SL_STOCKTRANSFER_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_CARD
Delete from SL_CARD;
Drop Sequence SL_CARD_SEQ;
CREATE SEQUENCE SL_CARD_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_CREDITCARDTERMINAL
Delete from SL_CREDITCARDTERMINAL;
Drop Sequence SL_CREDITCARDTERMINAL_SEQ;
CREATE SEQUENCE SL_CREDITCARDTERMINAL_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_SURVEYRESPONSE
Delete from SL_SURVEYRESPONSE;
Drop Sequence SL_SURVEYRESPONSE_SEQ;
CREATE SEQUENCE SL_SURVEYRESPONSE_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_SURVEYCHOICE
Delete from SL_SURVEYCHOICE;
Drop Sequence SL_SURVEYCHOICE_SEQ;
CREATE SEQUENCE SL_SURVEYCHOICE_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_PAYMENTOPTION
Delete from SL_PAYMENTOPTION;
Drop Sequence SL_PAYMENTOPTION_SEQ;
CREATE SEQUENCE SL_PAYMENTOPTION_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_CONTRACTHISTORY
Delete from SL_CONTRACTHISTORY;
Drop Sequence SL_CONTRACTHISTORY_SEQ;
CREATE SEQUENCE SL_CONTRACTHISTORY_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_AUTOLOADTOPAYMENTOPTION
Delete from SL_AUTOLOADTOPAYMENTOPTION;
Drop Sequence SL_AUTOLOADTOPAYMENTOPTION_SEQ;
CREATE SEQUENCE SL_AUTOLOADTOPAYMENTOPTION_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_NOTIFICATIONMESSAGEOWNER
Delete from SL_NOTIFICATIONMESSAGEOWNER;

-- SL_CONTRACTADDRESS
Delete from SL_CONTRACTADDRESS;

-- SL_PERSONADDRESS
Delete from SL_PERSONADDRESS;

-- SL_PERSON
Delete from SL_PERSON;
Drop Sequence SL_PERSON_SEQ;
CREATE SEQUENCE SL_PERSON_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_PERSON_ENC
Delete from SL_PERSON_ENC;
Drop Sequence SL_PERSON_ENC_SEQ;
CREATE SEQUENCE SL_PERSON_ENC_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_STORAGELOCATION
Delete from SL_STORAGELOCATION;
Drop Sequence SL_STORAGELOCATION_SEQ;
CREATE SEQUENCE SL_STORAGELOCATION_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_SURVEYQUESTION
Delete from SL_SURVEYQUESTION;
Drop Sequence SL_SURVEYQUESTION_SEQ;
CREATE SEQUENCE SL_SURVEYQUESTION_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_SURVEYDESCRIPTION
Delete from SL_SURVEYDESCRIPTION;

-- SL_CONTRACT
Delete from SL_CONTRACT;
Drop Sequence SL_CONTRACT_SEQ;
CREATE SEQUENCE SL_CONTRACT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_NOTIFICATIONMESSAGE
Delete from SL_NOTIFICATIONMESSAGE;
Drop Sequence SL_NOTIFICATIONMESSAGE_SEQ;
CREATE SEQUENCE SL_NOTIFICATIONMESSAGE_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_ADDRESS
Delete from SL_ADDRESS;
Drop Sequence SL_ADDRESS_SEQ;
CREATE SEQUENCE SL_ADDRESS_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_CONTRACTTOPAYMENTMETHOD
Delete from SL_CONTRACTTOPAYMENTMETHOD;

-- SL_SURVEY
Delete from SL_SURVEY;
Drop Sequence SL_SURVEY_SEQ;
CREATE SEQUENCE SL_SURVEY_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_JOBBULKIMPORT
Delete from SL_JOBBULKIMPORT;
Drop Sequence SL_JOBBULKIMPORT_SEQ;
CREATE SEQUENCE SL_JOBBULKIMPORT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_JOBCARDIMPORT
Delete from SL_JOBCARDIMPORT;
Drop Sequence SL_JOBCARDIMPORT_SEQ;
CREATE SEQUENCE SL_JOBCARDIMPORT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_JOB
Delete from SL_JOB;
Drop Sequence SL_JOB_SEQ;
CREATE SEQUENCE SL_JOB_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_DEVICEREADERKEYTOCERT
Delete from SL_DEVICEREADERKEYTOCERT;
Drop Sequence SL_DEVICEREADERKEYTOCERT_SEQ;
CREATE SEQUENCE SL_DEVICEREADERKEYTOCERT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_DEVICEREADERTOCERT
Delete from SL_DEVICEREADERTOCERT;
Drop Sequence SL_DEVICEREADERTOCERT_SEQ;
CREATE SEQUENCE SL_DEVICEREADERTOCERT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_DEVICECOMMUNICATIONHISTORY
Delete from SL_DEVICECOMMUNICATIONHISTORY;

-- SL_DEVICEREADERJOB
--Delete from SL_DEVICEREADERJOB;
--Drop Sequence SL_DEVICEREADERJOB_SEQ;
--CREATE SEQUENCE SL_DEVICEREADERJOB_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_DEVICEREADERKEY
--Delete from SL_DEVICEREADERKEY;
--Drop Sequence SL_DEVICEREADERKEY_SEQ;
--CREATE SEQUENCE SL_DEVICEREADERKEY_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_DEVICEREADER
--Delete from SL_DEVICEREADER;
--Drop Sequence SL_DEVICEREADER_SEQ;
--CREATE SEQUENCE SL_DEVICEREADER_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- PERF_PERFORMANCE
TRUNCATE TABLE PERF_PERFORMANCE;
Drop Sequence PERF_PERFORMANCE_SEQ;
CREATE SEQUENCE PERF_PERFORMANCE_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- VAS_REPORTJOBRESULT
Delete from VAS_REPORTJOBRESULT;
Drop Sequence VAS_REPORTJOBRESULT_SEQ;
CREATE SEQUENCE VAS_REPORTJOBRESULT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- VAS_REPORTJOB
Delete from VAS_REPORTJOB;
Drop Sequence VAS_REPORTJOB_SEQ;
CREATE SEQUENCE VAS_REPORTJOB_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- VAS_FILTERVALUE
Delete from VAS_FILTERVALUE;
Drop Sequence VAS_FILTERVALUE_SEQ;
CREATE SEQUENCE VAS_FILTERVALUE_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- VAS_FILTERVALUESET
Delete from VAS_FILTERVALUESET;
Drop Sequence VAS_FILTERVALUESET_SEQ;
CREATE SEQUENCE VAS_FILTERVALUESET_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_MERCHANT
Delete from SL_MERCHANT where MERCHANTID <> 2;
Drop Sequence SL_MERCHANT_SEQ;
CREATE SEQUENCE SL_MERCHANT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_MESSAGE
Delete from SL_MESSAGE;
Drop Sequence SL_MESSAGE_SEQ;
CREATE SEQUENCE SL_MESSAGE_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_PAYMENT
Delete from SL_PAYMENT;
Drop Sequence SL_PAYMENT_SEQ;
CREATE SEQUENCE SL_PAYMENT_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- SL_AUDITREGISTER
Delete from SL_AUDITREGISTER;
Drop Sequence SL_AUDITREGISTER_SEQ;
CREATE SEQUENCE SL_AUDITREGISTER_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- PERF_AGGREGATION
Delete from PERF_AGGREGATION;
Drop Sequence PERF_AGGREGATION_SEQ;
CREATE SEQUENCE PERF_AGGREGATION_SEQ INCREMENT BY 1 NOMINVALUE NOMAXVALUE CACHE 20 NOCYCLE NOORDER;

-- ==========================================================================================================================================
-- ===== COMMIT =============================================================================================================================
-- ==========================================================================================================================================

COMMIT; -- THIS SCRIPT IS NOT TESTABLE! Autocommit with drop and create sequence!!


-- ==========================================================================================================================================
-- ===== Check tables with content ==========================================================================================================
-- ==========================================================================================================================================

select
   table_name,
   to_number(extractvalue(xmltype(dbms_xmlgen.getxml('select count(*) c from '||table_name)),'/ROWSET/ROW/C')) countOfRows
from user_tables
where table_name not like '%SPX%'
order by countOfRows desc, table_name;


