--!!!!!!ACHTUNG ES WERDEN ALLE ENTITLEMENTS GELÖSCHT UNABHÄNGING OB ABO ODER NICHT.!!!!!! 
--!!!!!!FALLS DAS WO ANDERS GENUTZT WIRD MUSS DAS ANGEPASST WERDEN!!!!!!

--DELETE TABLE CONTACTPERSONIDs
drop table contactpersonids;
--DELETE TABLE PERSON
drop table person;
--DROP TABLE CONTRACTADDRESSIDS
drop table contractaddressids;
--DELETE TABLE
drop table customerpersonids;
--DELETE TABLE
drop table BANKCONNECTIONDATATIDs;
--DELETE TABLE CARDHOLDERADDRESSIDS
drop table cardholderaddressids;
--DELETE TABLE BANKADDRESSIDS
drop table bankaddressids;
--DELETE TABLE ACCOUNTOWNERIDs
drop table accountownerids;
--DELETE TABLE CONTACTPERSONADDRESSIds
drop table contactpersonaddress;
--DELETE TABLE CUSTOMADDRESSIDs
drop table customeraddressids;
--DROP TABLE CTC
drop table ctc;
--DROP TABLE ORGANIZATION
drop table organization;
--CREATE TEMPORARY TABLES
create table BANKCONNECTIONDATATIDs as select bankconnectiondataid from sl_paymentoption where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4);
create table person as select personid, contractid, alternativeadressid, addressid from sl_person;
create table accountownerids as select accountownerid from sl_bankconnectiondata where bankconnectiondataid  in (select bankconnectiondataid from BANKCONNECTIONDATATIDs);
create table bankaddressids as select addressid, personid  from sl_personaddress where personid in (select accountownerid from accountownerids);
create table customerPERSONIDs as select personid from sl_customeraccount c where c.CONTRACTID in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4); 
create table customeraddressids as select addressid, personid from sl_personaddress;
create table ctc as select cardid, contractid from sl_cardtocontract;
create table cardholderaddressids as select personid, addressid from sl_personaddress where personid in (select personid from sl_person 
where personid in (select cardholderid from sl_card card where card.cardid in (select cardid from ctc where contractid in (select contractid from sl_contract where contracttype = 4))) );
create  table contactpersonids as select contactpersonid from sl_organization o where o.ORGANIZATIONID in (select con.ORGANIZATIONID from sl_contract  con 
where con.CONTRACTTYPE = 4);
create global temporary table contractaddressids as select addressid from sl_contractaddress  where contractid in (select contractid from sl_contract where contracttype = 4);
create global temporary table contactpersonaddress as select personid, addressid from sl_personaddress where personid in (select contactpersonid from contactpersonids);
create table organization as select organizationid from sl_organization where organizationid in (select organizationid from sl_contract where contracttype = 4);

--DELETE CARDEVENTS
delete  from sl_cardevent cardevent where cardevent.CARDID in (select card.cardid from sl_card card 
where card.CARDID in (select cardid from sl_cardtocontract where contractid in (select contractid from sl_contract where contracttype = 4)));

--DELETE DUNNINGTOPOSTING
delete from sl_dunningtoposting where postingid in (select postingid from sl_posting where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4));
--DELETE DUNNINGTOINVOICE
delete from sl_dunningtoinvoice where dunningprocessid in (select dunningprocessid from sl_dunningprocess where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4));
--DELETE DUNNINGTOPRODUCT
delete from sl_dunningtoproduct where dunningprocessid in (select dunningprocessid from sl_dunningprocess where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4));
--DELETE DUNNINGPROCESS
delete from sl_dunningprocess where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4);

--DELETE POSTINGS
delete from sl_posting where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4);

--DELETE PRODUCTTERMINATION
delete from sl_producttermination where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4);
--DELETE SUBSIDYTRANAACTION
delete from sl_subsidytransaction
where productid in
(select productid from sl_product where cardid in (select cardid from sl_card card
 where card.cardid in (select cardid from sl_cardtocontract where contractid in (select contractid from sl_contract where contracttype = 4))));
 delete from sl_subsidytransaction where productid in 
 (select productid from sl_product where cardid in(select cardid from sl_card where cardholderid in 
 (select personid from sl_person where contractid in (select contractid from sl_contract where contracttype = 4))));
--DELETE PRODUCTRELATION
delete from sl_productrelation where productid in
(select productid from sl_product where cardid in (select cardid from sl_card card
 where card.cardid in (select cardid from sl_cardtocontract where contractid in (select contractid from sl_contract where contracttype = 4))));
 delete from sl_productrelation where productid in 
 (select productid from  sl_product where cardid in(select cardid from sl_card 
 where cardholderid in (select personid from sl_person where contractid in (select contractid from sl_contract where contracttype = 4))));
--DELETE ORDERDETAILTOPERSON
delete from sl_orderdetailtoperson otop where orderdetailid in (
select orderdetailid from sl_orderdetail where orderid in (select orderid from sl_order 
where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4)))
;
--DELETE ORDERDETAILPERSON PART2
delete from sl_orderdetailtoperson otop where orderdetailid in (
select orderdetailid from sl_orderdetail 
where productid in (select productid  from sl_product 
where cardid in(select cardid from sl_card where cardholderid in (select personid from sl_person 
where contractid in (select contractid from sl_contract where contracttype = 4)))));

--DELETE ORDERDETAILS
delete from sl_orderdetail 
where orderid in (select orderid from sl_order 
where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4)) 
;
--DELETE ORDERDETAILS PART2
delete from sl_orderdetail 
where productid in (select productid  from sl_product 
where cardid in(select cardid from sl_card where cardholderid in (select personid from sl_person 
where contractid in (select contractid from sl_contract where contracttype = 4))));

--delete orderhistory
delete from sl_orderhistory where orderid in (select orderid from sl_order 
where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4)
);

--DELETE ORDER
delete  from sl_order 
where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4)
;

--delete productentitlement
delete  from sl_entitlementtoproduct where productid in (select productid from sl_product
where cardid in
(select cardid from sl_card card where card.cardid in (select cardid from sl_cardtocontract where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4))));

-- delete entitlements
delete from sl_entitlementtoproduct;
delete   from sl_entitlement;

--delete referencedproduct
delete from sl_product rp where rp.REFERENCEDPRODUCTID in (select p.productid  from sl_product p
where p.cardid in
(select cardid from sl_card card where card.cardid in (select cardid from sl_cardtocontract where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4))));

--delete ORDERDETAILTOPERSON
delete  from SL_ORDERDETAILTOPERSON where ORDERDETAILID in (select ORDERDETAILID from SL_ORDERDETAIL where productid in (select productid from sl_product
where cardid in
(select cardid from sl_card card where card.cardid in (select cardid from sl_cardtocontract where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4)))));

--delete orderdetailproduct
delete  from SL_ORDERDETAIL where productid in (select productid from sl_product
where cardid in
(select cardid from sl_card card where card.cardid in (select cardid from sl_cardtocontract where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4))));

--DELETE POSTINGS
delete from SL_POSTING
where productid in(select productid from sl_product
where cardid in
(select cardid from sl_card card where card.cardid in (select cardid from sl_cardtocontract where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4))));

--DELETE PRODUCT
delete from sl_product
where cardid in
(select cardid from sl_card card where card.cardid in (select cardid from sl_cardtocontract where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4)));

--DELETE PRODUCT PART2
delete from sl_product where cardid in(select cardid from sl_card where cardholderid in (select personid from sl_person where contractid in (select contractid from sl_contract where contracttype = 4)));
delete from sl_product where cardid in (select cardid from sl_card  where REPLACEDCARDID  in (select cardid from sl_card where cardholderid in (select personid from sl_person where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4))));

--DELETE PAYMENTOPTION
delete from sl_paymentoption where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4);

--DELETE BANKCONNECTIONDATA
delete from sl_bankconnectiondata where bankconnectiondataid  in (select bankconnectiondataid from BANKCONNECTIONDATATIDs);
--DELETE CUSTOMERACCOUNT
delete from sl_customeraccount where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4);
--DELETE ADDRESS
delete from sl_personaddress where personid  in (select accountownerid from accountownerids);
delete from sl_address where addressid in (select addressid from bankaddressids);
delete from sl_person where personid in(select accountownerid from accountownerids);
--DELETE ENTITLEMENTS
delete from sl_ENTITLEMENT where personid in (select personid from sl_person where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4));

--DELETE SL_PHOTOGRAPH
delete from sl_photograph where personid in (select personid from customerPERSONIDs);

--DELETE ADDRESS
delete from sl_personaddress where personid in (select personid from customerPERSONIDs);
delete from sl_address where addressid in (select addressid from customeraddressids where personid in (select personid from customerpersonids));

--DELETE CUSTOMERPERSON
delete from sl_person p where p.PERSONID in (select personid from customerPERSONIDs);

--DELETE SL_PHOTOGRAPH
delete from sl_photograph where personid in (select personid from sl_person where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4));
--DELETE PERSONADDRESS
delete from sl_personaddress a where personid in (select personid from sl_person where contractid in (select con.CONTRACTID from sl_contract  con where con.CONTRACTTYPE = 4));

--DELETE CONTRACTHISTORY
delete from sl_contracthistory where contractid in (select con.contractid from sl_contract con 
where con.CONTRACTTYPE = 4);
--DELETE SL_COMMENT
delete from sl_comment where contractid in (select con.contractid from sl_contract con 
where con.CONTRACTTYPE = 4);

--DELETE INOVOICEENTRY
delete from sl_invoiceentry where invoiceid in (select invoiceid from sl_invoice where contractid in (select contractid from sl_contract where contracttype = 4));
--DELETE INVOICE
delete from sl_invoice where contractid in (select con.CONTRACTID from sl_contract  con where con.CONTRACTTYPE = 4);

--DELETE CONTRACTADDRESS
delete from sl_contractaddress where addressid in (select addressid from contractaddressids);
--DELETE ADDRESS
delete from sl_address where addressid in (select addressid from contractaddressids); 

--DELETE CARD
delete  from sl_cardevent cardevent where cardevent.CARDID in (select cardid from sl_card card where card.REPLACEDCARDID in (select cardid from sl_card card where cardid in( select cardid from sl_card where cardholderid in (select personid from sl_person where contractid in (select contractid from sl_contract where contracttype = 4)))));
delete from sl_cardevent cardevent where cardevent.CARDID in (select cardid from sl_card card where cardid in( select cardid from sl_card where cardholderid in (select personid from sl_person where contractid in (select contractid from sl_contract where contracttype = 4))) );
delete from sl_cardtocontract where contractid in (select contractid from sl_contract where contracttype = 4);
delete from sl_cardtocontract where cardid in (select cardid from sl_product where cardid in
(select cardid from sl_card where cardholderid in (select personid from sl_person where contractid in (select contractid from sl_contract where contracttype = 4))));
delete from sl_cardtocontract where cardid in (select cardid from sl_card card where cardid in
( select cardid from sl_card where cardholderid in 
(select personid from sl_person where contractid in (select contractid from sl_contract where contracttype = 4))));
delete from sl_card card where card.REPLACEDCARDID in (select cardid from ctc where contractid in (select contractid from sl_contract where contracttype = 4));
delete from sl_card card where card.cardid in (select cardid from ctc where contractid in (select contractid from sl_contract where contracttype = 4));
delete from sl_card card where card.REPLACEDCARDID in (select cardid from sl_card card where cardid in( select cardid from sl_card where cardholderid in (select personid from sl_person where contractid in (select contractid from sl_contract where contracttype = 4))));
delete from sl_card card where cardid in( select cardid from sl_card where cardholderid in (select personid from sl_person where contractid in (select contractid from sl_contract where contracttype = 4)));
delete from sl_photograph where personid in (select personid from cardholderaddressids);
delete from sl_personaddress where addressid in (select addressid from  cardholderaddressids );

-- delete SL_CARDHOLDERORGANIZATION
delete from SL_CARDHOLDERORGANIZATION where cardholderid in (select personid from sl_person where contractid in (select CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4));

delete from sl_person where personid in (select personid from cardholderaddressids);
delete from sl_address where addressid in (select addressid from cardholderaddressids);

--DELETE ORGANIZATIONACCOUNT
delete from sl_organization where organizationid in (select con.ORGANIZATIONID from sl_contract  con 
where con.CONTRACTTYPE = 4 and con.ORGANIZATIONID	is null);
--DELETE SL_PHOTOGRAPH
delete from sl_photograph where personid in (select contactpersonid from contactpersonids);

delete from sl_personaddress where addressid in (select addressid from contactpersonaddress);
delete from sl_address where addressid in (select addressid from contactpersonaddress);
delete from sl_bankconnectiondata  where accountownerid in (select personid from sl_person where personid in (select contactpersonid from contactpersonids));

--DELETE CARDS BY CARDHOLDERID
delete from sl_card card where card.REPLACEDCARDID  in (select cardid from sl_card where cardholderid in (select personid from sl_person where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4)));
delete from sl_card where cardholderid in (select personid from sl_person where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4));


--DELETE PERSONS
delete from sl_person where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4);
--DELETE ADDRESS
delete from sl_address where addressid in (select addressid from person where contractid in (select contractid from sl_contract where contracttype = 4 ));
delete from sl_address where addressid in (select alternativeadressid from person where contractid in (select contractid from sl_contract where contracttype = 4 ));
delete from sl_contractaddress where contractid in (select con.CONTRACTID from sl_contract  con where con.CONTRACTTYPE = 4);

--DELETE CONTRACTTERMINATION
delete from SL_CONTRACTTERMINATION where contractid in (select con.CONTRACTID from sl_contract  con 
where con.CONTRACTTYPE = 4);

--DELETE CONTRACTS
delete from sl_contract con where con.contracttype = 4 and con.ORGANIZATIONID is not null;

--DELETE CONTRACTS
delete from sl_contract con where con.contracttype = 4 and con.ORGANIZATIONID is null;
--DELETE ORGANIZATIONACCOUNT
delete from sl_organization where organizationid in (select organizationid from organization);

--DELETE ORGANITATIONADDRESS
delete from sl_personaddress where personid in (select personid from sl_person where personid in (select contactpersonid from contactpersonids));
--DELETE ORGANIZATIONCONTACTPERSON
delete from sl_person where personid in (select contactpersonid from contactpersonids);