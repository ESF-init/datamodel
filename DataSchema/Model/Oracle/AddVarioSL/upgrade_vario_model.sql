SPOOL upgrade_vario.txt

/* ACHTUNG: DBMS-Output aktivieren! */

/* INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
   VALUES (sysdate, '1.445', 'TSC', 'Changes from SL script on non-SL tables moved to vario script.');*/

-- =======[ Changes on existing tables ]===========================================================================

/* USER_LANGUAGE.LANGUAGEID ist NUMBER(5,0), in USER_LIST hatte die Spalte aber nur NUMBER(2,0).
   Dasselbe gilt f�r CLIENTID, die unter VARIO_CLIENT NUMBER(10,0) ist.  */
ALTER TABLE USER_LIST
MODIFY (
	LANGUAGEID NUMBER(10,0),
	CLIENTID NUMBER(10, 0),
	USERSHORTNAME VARCHAR2(50)
);

/* Auch hier war die CLIENTID spalte nur NUMBER(2,0), w�hrend sie in in VARIO_CLIENT NUMBER(10,0) ist. */
ALTER TABLE USER_GROUP
MODIFY (
	CLIENTID NUMBER(10, 0)
);

/* RESOURCEID konnte nicht referenziert werden, weil nur VARCHAR2 und die Datentypen deshalb nicht zusammenpassten. */
ALTER TABLE USER_RESOURCE
MODIFY (
	RESOURCEID NVARCHAR2(512),
	RESOURCEGROUP NVARCHAR2(20),
	RESOURCENAME NVARCHAR2(50),
	RESOURCEDESCR NVARCHAR2(125)
);

/* RESOURCEID L�nge passend zur PK Spalte in USER_RESOURCE. */
ALTER TABLE USER_GROUPRIGHT
MODIFY (
  RESOURCEID VARCHAR2(512)
);

/* Anpassung fuer den Verweis von SL_CARD.CARDTYPE. */
ALTER TABLE VARIO_TYPEOFCARD
MODIFY (
	TYPEOFCARDID NUMBER(18, 0)
);

/* Angleichung Datentypen. */
ALTER TABLE TM_TICKETTYPE
MODIFY (
	TICKETTYPEID NUMBER(18, 0)
);

/* Angleichung Datentypen. */
ALTER TABLE PP_TITLE
MODIFY (
	CODENO NUMBER(9,0)
);

/* Angleichung Datentypen. */
ALTER TABLE PP_CAUSEOFCARDLOCKING
MODIFY (
	CODENO NUMBER(9,0)
);

/* Angleichung Datentypen. */
ALTER TABLE PP_CARDSTATUS
MODIFY (
	CODENO NUMBER(9,0)
);

/* Kompatibilitaet zu SL Enumerationen. */
ALTER TABLE PP_TITLE
ADD (
	LITERAL NVARCHAR2(50)
);

/* Kompatibilitaet zu SL Enumerationen. */
ALTER TABLE PP_CAUSEOFCARDLOCKING
ADD (
	LITERAL NVARCHAR2(50)
);

/* Kompatibilitaet zu SL Enumerationen. */
ALTER TABLE PP_CARDSTATUS
ADD (
	LITERAL NVARCHAR2(50)
);

/* Angleichung Datentypen. */
ALTER TABLE PP_CARDACTIONREQUESTTYPE
MODIFY (
	REQUESTTYPEID NUMBER (9, 0)
);

/* Angleichung Datentypen. */
ALTER TABLE PP_CARDACTIONREQUEST
MODIFY (
	ID NUMBER(18, 0),
	CLIENTID NUMBER(18, 0) DEFAULT 0,
	CLEARINGID NUMBER(18, 0) DEFAULT 0,
	REQUESTTYPE NUMBER (9, 0),
	VALID NUMBER(10,0) DEFAULT 1 --Datentyp in LLBLGen anpassen Boolean
);

ALTER TABLE PP_CARDACTIONREQUEST
MODIFY (
	VALID NUMBER(10,0) DEFAULT 1 
);

/* The column must be hold numbers up to 999. */
ALTER TABLE VARIO_TYPEOFCARD
MODIFY(
	CODENO NUMBER(3,0)
);

/* Standardwert ergaenzt. (The column is needed in classic vario.) */
ALTER TABLE VARIO_PHYSICALCARDTYPE 
MODIFY (
	CATEGORY NUMBER(9, 0) DEFAULT 0
);

/* Angleichung Datentypen. */
ALTER TABLE vario_settlement
MODIFY (
	STATE NUMBER(9,0) 
);

/* NOT-NULL-Constraint ergaenzt. */
ALTER TABLE vario_typeofsettlement
MODIFY (
	TYPEOFSETTLEMENTID NUMBER(10,0) NOT NULL --Datentyp in LLBLGen anpassen Int32
);

/* Nur als Erinnerung, keine Aenderungen. */
ALTER TABLE pp_accountentry
MODIFY (
	CLIENTID NUMBER (22,2), --Datentyp in LLBLGen anpassen Int64
	ACCOUNTTYPE NUMBER(10,0) --Datentyp in LLBLGen anpassen Int32
);

/* Angleichung Datentypen und Standardwerte ergaenzt. 
	 Alle NUMBER(10,0) enstprechen der original Precision aus dem Master-Script f�r vario. */
ALTER TABLE DM_DEBTOR
MODIFY (
	TITLEID NUMBER(10,0) DEFAULT 0, --Datentyp in LLBLGen anpassen Int32
	TRAININGLEVEL NUMBER(1,0) DEFAULT 0,
	SERVICELEVEL NUMBER(9,0) DEFAULT 0,
	MINALLOWEDACCOUNTBALANCE NUMBER(10,0) DEFAULT -100000, --Datentyp in LLBLGen anpassen Int32
	MAXDAYSSINCELASTCLEARANCE NUMBER(10,0) DEFAULT 30, --Datentyp in LLBLGen anpassen Int32
	VALIDTO DATE DEFAULT TO_TIMESTAMP('31.12.9999 23:59:59', 'DD.MM.YYYY HH24:MI:SS'),
	LASTBALANCEDATE DATE DEFAULT TO_TIMESTAMP('01.01.2000 00:00:00', 'DD.MM.YYYY HH24:MI:SS'),
	DEBITBALANCE NUMBER(10,0) DEFAULT 0, --Datentyp in LLBLGen anpassen Int32
	ACCOUNTBALANCE NUMBER(10,0) DEFAULT 0 --Datentyp in LLBLGen anpassen Int32
);

UPDATE DM_DEBTOR SET TITLEID = -1 WHERE TITLEID IS NULL;
UPDATE DM_DEBTOR SET DEBITBALANCE = 0 WHERE DEBITBALANCE IS NULL;

/* Constraint ergaenzt. */
ALTER TABLE DM_DEBTOR
MODIFY (
	TITLEID NUMBER(10,0) DEFAULT 0 
);

/* Constraint ergaenzt. */
ALTER TABLE DM_DEBTOR
MODIFY (
	TRAININGLEVEL NUMBER(1,0) DEFAULT 0 
);

/* Constraint ergaenzt. */
ALTER TABLE DM_DEBTOR
MODIFY (
	SERVICELEVEL NUMBER(9,0) DEFAULT 0 
);

/* Constraint ergaenzt. */
ALTER TABLE DM_DEBTOR
MODIFY (
	MINALLOWEDACCOUNTBALANCE NUMBER(10,0) DEFAULT -100000 
);

/* Constraint ergaenzt. */
ALTER TABLE DM_DEBTOR
MODIFY (
	MAXDAYSSINCELASTCLEARANCE NUMBER(10,0) DEFAULT 30 
);

/* Constraint ergaenzt. */
ALTER TABLE DM_DEBTOR
MODIFY (
	VALIDTO DATE DEFAULT TO_TIMESTAMP('31.12.9999 23:59:59', 'DD.MM.YYYY HH24:MI:SS') 
);

/* Constraint ergaenzt. */
ALTER TABLE DM_DEBTOR
MODIFY (
	LASTBALANCEDATE DATE DEFAULT TO_TIMESTAMP('01.01.2000 00:00:00', 'DD.MM.YYYY HH24:MI:SS') 
);

/* Constraint ergaenzt. */
ALTER TABLE DM_DEBTOR
MODIFY (
	DEBITBALANCE NUMBER(10,0) DEFAULT 0 
);
 
/* Constraint ergaenzt. */
ALTER TABLE DM_DEBTOR
MODIFY (
	ACCOUNTBALANCE NUMBER(10,0) DEFAULT 0 
);

/* Ersetze unbenannte Unique-Constraint auf ID-Spalte durch Primary-Key constraint. */
DECLARE
  user_string VARCHAR2(100);
  constraint_name_string VARCHAR2(100);
  column_name_string VARCHAR2(100);
  sql_string VARCHAR2(500);
  table_name_string VARCHAR2(100) := 'PP_ACCOUNTENTRY';
BEGIN
	-- Aktueller User.
  select user into user_string
  from dual;

	-- Name der Unique-Constraint raussuchen.
  SELECT CONSTRAINT_NAME into constraint_name_string
  FROM all_constraints
  WHERE table_name = table_name_string
  AND constraint_type = 'U'
  AND owner = user_string;
  
	-- Spaltenname raussuchen fuer welche die Unique-Constraint gilt.
  select column_name into column_name_string
  from user_cons_columns
  where table_name = table_name_string
  and CONSTRAINT_NAME = constraint_name_string
  and owner = user_string;
  
  dbms_output.put_line('Unique constraint found with name ' || constraint_name_string || ' found on column ' || column_name_string || ' for user ' || user_string);
  
	-- FKs fuer die Spalte auflisten.
  for i in (
    select table_name, constraint_name
    from all_constraints
    where r_owner = user_string
    and constraint_type = 'R'
    and status = 'ENABLED'
    and r_constraint_name = constraint_name_string)
  loop
    dbms_output.put_line('WARNING: FK constraint ' || i.constraint_name || ' on ' || i.table_name || ' will be dropped too!');
  end loop i;

	-- Alte Unique-Constraint entfernen aber index behalten.
  sql_string := 'alter table ' || table_name_string || ' drop constraint ' || constraint_name_string || ' cascade keep index';
  dbms_output.put_line('Executing: ' || sql_string);
  EXECUTE IMMEDIATE sql_string;
  
  -- PK-Constraint mit altem index anlegen.
  sql_string := 'alter table ' || table_name_string || ' add constraint PK_' || table_name_string || ' primary key (' || column_name_string || ') using index';
  dbms_output.put_line('Executing: ' || sql_string);
  EXECUTE IMMEDIATE sql_string;
  
  dbms_output.put_line('Dropped constraint ' || constraint_name_string);
	
	EXCEPTION
		WHEN others THEN
			dbms_output.put_line('EXCEPTION: No unique constraint found.');
END;
/


/* Ersetze unbenannte Unique-Constraint auf ID-Spalte durch Primary-Key constraint. */
DECLARE
  user_string VARCHAR2(100);
  constraint_name_string VARCHAR2(100);
  column_name_string VARCHAR2(100);
  sql_string VARCHAR2(500);
  table_name_string VARCHAR2(100) := 'PP_ACCOUNTPOSTINGKEY';
BEGIN
	-- Aktueller User.
  select user into user_string
  from dual;

	-- Name der Unique-Constraint raussuchen.
  SELECT CONSTRAINT_NAME into constraint_name_string
  FROM all_constraints
  WHERE table_name = table_name_string
  AND constraint_type = 'U'
  AND owner = user_string;
  
	-- Spaltenname raussuchen fuer welche die Unique-Constraint gilt.
  select column_name into column_name_string
  from user_cons_columns
  where table_name = table_name_string
  and CONSTRAINT_NAME = constraint_name_string
  and owner = user_string;
  
  dbms_output.put_line('Unique constraint found with name ' || constraint_name_string || ' found on column ' || column_name_string || ' for user ' || user_string);
  
	-- FKs fuer die Spalte auflisten.
  for i in (
    select table_name, constraint_name
    from all_constraints
    where r_owner = user_string
    and constraint_type = 'R'
    and status = 'ENABLED'
    and r_constraint_name = constraint_name_string)
  loop
    dbms_output.put_line('WARNING: FK constraint ' || i.constraint_name || ' on ' || i.table_name || ' will be dropped too!');
  end loop i;

	-- Alte Unique-Constraint entfernen aber index behalten.
  sql_string := 'alter table ' || table_name_string || ' drop constraint ' || constraint_name_string || ' cascade keep index';
  dbms_output.put_line('Executing: ' || sql_string);
  EXECUTE IMMEDIATE sql_string;
  
  -- PK-Constraint mit altem index anlegen.
  sql_string := 'alter table ' || table_name_string || ' add constraint PK_' || table_name_string || ' primary key (' || column_name_string || ') using index';
  dbms_output.put_line('Executing: ' || sql_string);
  EXECUTE IMMEDIATE sql_string;
  
  dbms_output.put_line('Dropped constraint ' || constraint_name_string);
	
	EXCEPTION
		WHEN others THEN
			dbms_output.put_line('EXCEPTION: No unique constraint found.');
END;
/

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

/* Bei Stammdatenversorgung wird die Spalte ENUMERATIONVALUE ansonsten nicht beruecksichtigt. */
CREATE OR REPLACE TRIGGER TICKETTYPE_BRI 
BEFORE INSERT ON TM_TICKETTYPE 
FOR EACH ROW 
  BEGIN
	:new.EnumerationValue := :new.TicketTypeID;
  END;
/

/* Bei Stammdatenversorgung wird die Spalte LITERAL ansonsten nicht beruecksichtigt. */
DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('PP_TITLE', 'PP_CAUSEOFCARDLOCKING', 'PP_CARDSTATUS');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
		BEGIN
			dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
			
			sql_string := '';
			sql_string := 
				'create or replace trigger ' || table_names(table_name) || '_BRI' ||
				' before insert on ' || table_names(table_name) || 
				' for each row begin :new.literal := :new.description; end;';
			EXECUTE IMMEDIATE sql_string;

			dbms_output.put_line('Done!');
		EXCEPTION
			WHEN others THEN
				dbms_output.put_line('Error creating trigger: ' || sqlerrm);
		END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Fertig!

SPOOL off