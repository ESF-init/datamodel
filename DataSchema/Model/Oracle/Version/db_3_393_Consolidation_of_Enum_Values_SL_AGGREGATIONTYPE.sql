SPOOL db_3_393.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.393', 'MKD', 'Consolidation of values in SL_AGGREGATIONTYPE with values in AggregationType enum');

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

Delete From SL_AGGREGATIONTYPE
Where LITERAL NOT IN ('Raw', 'Year', 'Month', 'Day', 'Hour');

update SL_AGGREGATIONTYPE
set ENUMERATIONVALUE = 1
where LITERAL = 'Year';

update SL_AGGREGATIONTYPE
set ENUMERATIONVALUE = 4
where LITERAL = 'Hour';

update SL_AGGREGATIONTYPE
set ENUMERATIONVALUE = 2
where LITERAL = 'Month';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
