SPOOL db_3_030.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.030', 'LDO', 'Tables and columns required for configuring the Vario messaging system.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_EMAILEVENT ADD
(
    MESSAGETYPE     Number(9)
);

COMMENT ON COLUMN SL_EMAILEVENT.MESSAGETYPE IS 'Configures the default message type to be sent for this message event. Can be overwritten for any customer by SL_ACCOUNTMESSAGESETTINGS.';

ALTER TABLE SL_MESSAGE ADD
(
    EMAILEVENTID        Number(18),
    INTERNALDATA        Blob,
    CONSTRAINT FK_MESSAGE_EMAILEVENT FOREIGN KEY (EMAILEVENTID) REFERENCES SL_EMAILEVENT (EMAILEVENTID)
);

COMMENT ON COLUMN SL_MESSAGE.EMAILEVENTID IS 'ID of the message event for which the message was configured.';
COMMENT ON COLUMN SL_MESSAGE.INTERNALDATA IS 'Used for multi message prevention. Content dependend on message event.'; 

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_ACCOUNTMESSAGESETTING
(
    CustomerAccountId   Number(18)  NOT NULL,
    EmailEventId        Number(18)  NOT NULL,
    MessageType         Number(9),
    IsActive            Number(1)   DEFAULT 0 NOT NULL,
    LASTUSER            VARCHAR2(50  Byte) DEFAULT 'SYS' NOT NULL,
    LASTMODIFIED        DATE        DEFAULT sysdate NOT NULL,	
	TRANSACTIONCOUNTER  INTEGER     NOT NULL,
    CONSTRAINT PK_ACCOUNTMESSAGESETTING PRIMARY KEY (CUSTOMERACCOUNTID, EMAILEVENTID),
    CONSTRAINT FK_ACCOUNTMESSAGESET_ACCOUNT FOREIGN KEY (CUSTOMERACCOUNTID) REFERENCES SL_CUSTOMERACCOUNT (CUSTOMERACCOUNTID),
    CONSTRAINT FK_ACCOUNTMESSAGESET_EMAIL FOREIGN KEY (EMAILEVENTID) REFERENCES SL_EMAILEVENT (EMAILEVENTID)
    );
    
COMMENT ON TABLE SL_ACCOUNTMESSAGESETTING IS 'Configures what kind of message is sent to a customer in case of specific message events.';
COMMENT ON COLUMN SL_ACCOUNTMESSAGESETTING.CUSTOMERACCOUNTID IS 'ID of the customer for which an event message is configured.';
COMMENT ON COLUMN SL_ACCOUNTMESSAGESETTING.EMAILEVENTID IS 'ID of the message event which is configured.';
COMMENT ON COLUMN SL_ACCOUNTMESSAGESETTING.MESSAGETYPE IS 'Type of message which is to be sent.';
COMMENT ON COLUMN SL_ACCOUNTMESSAGESETTING.ISACTIVE IS 'Indicates whether a message is to be sent.';
COMMENT ON COLUMN SL_ACCOUNTMESSAGESETTING.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_ACCOUNTMESSAGESETTING.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(7) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_ACCOUNTMESSAGESETTING');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
