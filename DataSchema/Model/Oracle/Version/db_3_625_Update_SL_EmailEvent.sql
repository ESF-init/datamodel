SPOOL db_3_625.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.625', 'WBO', 'Updated SL_Emailevent');


-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Autoload Transferred', 'AutoloadTransferred', 1, 'AutoloadMailMerge');

COMMIT;

PROMPT Done!

SPOOL OFF;
