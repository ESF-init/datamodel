SPOOL db_3_365.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.365', 'TIR', 'Add Column ExpiresAfterDays to UM_Archivejob');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE UM_ARCHIVEJOB
ADD ExpiresAfterDays NUMBER(10);

COMMENT ON COLUMN UM_ARCHIVEJOB.ExpiresAfterDays IS 'Indicates after how many days this archivejob should expire.'; 

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
