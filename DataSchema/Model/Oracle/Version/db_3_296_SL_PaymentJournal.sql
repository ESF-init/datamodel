SPOOL db_3_296.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.296', 'DST', 'Added unique constraint to SL_PaymentJournal.WalletTransactionID.');

alter table sl_paymentjournal add CONSTRAINT uk_paymentjournal_walletid UNIQUE (WalletTransactionID);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
