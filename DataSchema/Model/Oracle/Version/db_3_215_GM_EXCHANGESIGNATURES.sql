SPOOL db_3_215.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.215', 'LWU', 'Added table EXCHANGESIGNATURES(Image of signature) to Database.');


-- =======[ Adding new Table ]===========================================================================

CREATE TABLE GM_EXCHANGESIGNATURES
(
  SIGNATUREID    NUMBER,
  SIGNATUREBLOB  LONG RAW
);


CREATE UNIQUE INDEX GM_EXCHANGESIGNATURES_PK ON GM_EXCHANGESIGNATURES(SIGNATUREID);

ALTER TABLE GM_EXCHANGESIGNATURES ADD (
  CONSTRAINT GM_EXCHANGESIGNATURES_PK
  PRIMARY KEY
  (SIGNATUREID)
  USING INDEX GM_EXCHANGESIGNATURES_PK
  ENABLE VALIDATE);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;