SPOOL db_3_486.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.486', 'JED', 'Added LastRun and Contraint to VARIO_APPLICATIONVERSION. Updated comments.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE VARIO_APPLICATIONVERSION ADD LASTRUN DATE NULL; 

ALTER TABLE VARIO_APPLICATIONVERSION ADD CONSTRAINT UC_APPLICATIONVERSION UNIQUE (APPLICATIONID, VERSION, WORKSTATION);

COMMENT ON COLUMN VARIO_APPLICATIONVERSION.LASTRUN IS 'Last reported execution of the application on this workstation with this version.';

--comment update/improvement
COMMENT ON TABLE VARIO_APPLICATIONVERSION is '';
COMMENT ON COLUMN VARIO_APPLICATIONVERSION.APPLICATIONDATE IS '';
COMMENT ON COLUMN VARIO_APPLICATIONVERSION.VERSION IS '';

COMMENT ON TABLE VARIO_APPLICATIONVERSION is 'Tracking of installed application versions. ';
COMMENT ON COLUMN VARIO_APPLICATIONVERSION.APPLICATIONDATE IS 'First reported execution of the application on this workstation with this version.';
COMMENT ON COLUMN VARIO_APPLICATIONVERSION.VERSION IS 'Normalized version string of the application (each digit is padded to expand 3 characters to improve sorting. Version "1.2.3.4" is stored as "001.002.003.004")';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
