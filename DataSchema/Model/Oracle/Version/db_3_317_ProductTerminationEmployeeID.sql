SPOOL db_3_317.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.317', 'BVI', 'Modified Foreign key SL_ProductTermination.EmployeeID.');

alter table SL_PRODUCTTERMINATION drop constraint FK_PRODUCTTERMINATION_PERSON;

alter table SL_PRODUCTTERMINATION add constraint FK_PRODUCTTERMINATION_User foreign key (EMPLOYEEID) references User_List (Userid);

COMMENT ON COLUMN SL_PRODUCTTERMINATION.EMPLOYEEID is 'References User_List.UserID';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
