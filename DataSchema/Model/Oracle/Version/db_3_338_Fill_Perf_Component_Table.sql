SPOOL db_3_338.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.338', 'MKD', 'Adding the performance components ');


-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

MERGE INTO PERF_COMPONENT p
USING (WITH
     Components (COMPONENTID, NAME) AS (
                Select 1, 'Whitelist' FROM DUAL
        UNION   Select 2, 'FarePayment' FROM DUAL
        UNION   Select 3, 'Api' FROM DUAL
        UNION   Select 4, 'FareInspection' FROM DUAL
     )
     Select COMPONENTID, NAME FROM Components) c ON (p.COMPONENTID = c.COMPONENTID)
WHEN NOT MATCHED THEN INSERT (p.COMPONENTID, p.NAME) VALUES (c.COMPONENTID, c.NAME)
WHEN MATCHED THEN UPDATE SET p.NAME = c.NAME;


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
