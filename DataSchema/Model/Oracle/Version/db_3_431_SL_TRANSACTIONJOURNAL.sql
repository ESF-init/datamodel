SPOOL db_3_431.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.431', 'flf', 'New columns in SL_TRANSACTIONJOURNAL');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (APPLIEDPASSTICKETID  NUMBER(18));

COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.APPLIEDPASSTICKETID IS 'Ticketid of the pass which was applied to achieve a trip. Either single trip or transfer.';

ALTER TABLE SL_TRANSACTIONJOURNAL ADD 
CONSTRAINT SL_TRANSACT_APPRODUCT
 FOREIGN KEY (APPLIEDPASSPRODUCTID)
 REFERENCES SL_PRODUCT (PRODUCTID);

 ALTER TABLE SL_TRANSACTIONJOURNAL ADD 
CONSTRAINT SL_TRANSACT_APPTICKET
 FOREIGN KEY (APPLIEDPASSTICKETID)
 REFERENCES TM_TICKET (TICKETID);


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
