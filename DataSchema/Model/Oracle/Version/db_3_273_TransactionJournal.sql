SPOOL db_3_273.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.273', 'FLF', 'Added fare scale name and trip value to SL_TRANSACTIONJOURNAL');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (TRIPVALUE  NUMBER(9));

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (FARESCALENAME  NVARCHAR2(100));


COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.TRIPVALUE IS 'Accumulated value of the trip';
COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.FARESCALENAME IS 'Name of the fare scale (from tariff) for the transaction.';


COMMIT;

PROMPT Done!

SPOOL OFF;
