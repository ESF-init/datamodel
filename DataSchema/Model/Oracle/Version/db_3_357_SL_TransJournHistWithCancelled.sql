SPOOL db_3_357.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.357', 'DST', 'Added SalesPersonNumber to SL_TransJournHistWithCancelled.');

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW SL_TRANSJOURNHISTWITHCANCELLED
AS
with
trans AS (
  SELECT *
  FROM sl_transactionjournal
  WHERE
    --transactionjournalid NOT IN ( SELECT cancellationreference FROM sl_transactionjournal can WHERE cancellationreference IS NOT NULL) AND
    transactiontype IN (
      66,   -- Boarding
      84,   -- Transfer
      85,   -- Use
      99,   -- Charge
      108,  -- Load
      261,  -- ClientFare
      262,  -- OpenLoopVirtualCharge
      7000, -- Inspection
      9001, -- FareMediaSale
      9003, -- Refund
      9006, -- Adjustment
      9007, -- DormancyFee
      9009, -- BalanceTransfer
      9010, -- ProductTransfer
      9012, -- StoredValuePayment
      9013  -- StoredValueRefund
))
SELECT 
  trans.transactionid,
  trans.operatorid,
  trans.saleschannelid,
  CASE WHEN trans.transactiontype = 66 AND trans.validto IS NOT NULL THEN CAST (9011 /* ProductActivation */ AS NUMBER (9)) ELSE trans.transactiontype END transactiontype,
  trans.devicetime timestamp,
  trans.ticketinternalnumber ticketexternalnumber,
  trans.resulttype result,
  CASE 
    WHEN trans.resulttype = 0 /* Ok */
       THEN CASE WHEN trans.transactiontype = 99 /*charge*/ AND prod.tickettype = 201 /*pursepretax*/ THEN CAST (NULL AS NUMBER (9)) ELSE trans.pursecredit END
       /* PreTax charge does not get written in Product2, workaround */
       ELSE CAST (0 AS NUMBER (9)) END 
  pursecredit,
  CASE WHEN trans.resulttype = 0 /* Ok */
       THEN CASE WHEN trans.transactiontype = 99 /*charge*/ AND prod.tickettype = 201 /*pursepretax*/ THEN CAST (NULL AS NUMBER (9)) ELSE trans.pursebalance END
       /* PreTax charge does not get written in Product2, workaround */
       ELSE CAST (0 AS NUMBER (9)) END
  pursebalance,
  CASE WHEN trans.resulttype = 0 /* Ok */
       THEN CASE WHEN     trans.transactiontype = 99 /*charge*/ AND prod.tickettype = 201 /*pursepretax*/ THEN trans.pursecredit ELSE trans.pursecredit2 END
       /* PreTax charge does not get written in Product2, workaround */
       ELSE CAST (0 AS NUMBER (9)) END
  pursecreditpretax,
  CASE WHEN trans.resulttype = 0 /* Ok */
       THEN CASE WHEN trans.transactiontype = 99 /*charge*/ AND prod.tickettype = 201 /*pursepretax*/ THEN trans.pursebalance ELSE trans.pursebalance2 END
       ELSE CAST (0 AS NUMBER (9)) END
  pursebalancepretax,
  trans.fareamount price,
  CASE
    WHEN trans.validfrom IS NULL AND trans.validto IS NULL AND trans.DURATION > 0 THEN trans.devicetime
    ELSE trans.validfrom END 
  validfrom, /* Transfers don't have ValidFrom and ValidTo set, workaround */
  CASE
    WHEN trans.validto IS NULL AND trans.validfrom IS NOT NULL THEN trans.validfrom + trans.DURATION / (24 * 60 * 60)
      /* Boardings don't have ValidTo set, workaround */
    WHEN trans.validfrom IS NULL AND trans.validto IS NULL AND trans.DURATION > 0 THEN trans.devicetime + trans.DURATION / (24 * 60 * 60)
               /* Transfers don't have ValidFrom and ValidTo set, workaround */
    ELSE trans.validto
    END
  validto,
  trans.groupsize numberofpersons,
  trans.externaltransactionidentifier,
  STOP.stopno,
  STOP.stopcode,
  STOP.stopname,
  line.lineno,
  trans.line linecode,
  line.linename,
  trans.vehiclenumber,
  trans.devicenumber,
  device.NAME devicename,
  device.description devicedescription,
  cap.cappingname appliedcapping,
  ticket.NAME ticketname,
  trans.inserttime,
  trans.transitaccountid,
  CJOURNAL.transactionid cancellationreference,
  CASE WHEN CJOURNAL.transactionid IS NULL THEN 0 ELSE 1 END hascancellationreference,
  tokencard.serialnumber tokencardserialnumber,
  trans.debtornumber salespersonnumber
FROM trans
     LEFT JOIN tm_ticket ticket ON ticket.ticketid = trans.ticketid
     LEFT JOIN tm_tarif tariff on tariff.tarifid = ticket.tarifid
     LEFT JOIN vario_stop STOP ON STOP.netid = tariff.netid AND STOP.stopno = trans.stopnumber
     LEFT JOIN vario_line line ON line.netid = tariff.netid AND line.linecode = trans.line
     LEFT JOIN um_device device ON device.deviceno = trans.devicenumber AND device.deviceclassid = trans.saleschannelid
     LEFT JOIN sl_product prod ON prod.productid = trans.productid
     LEFT JOIN sl_cappingjournal cappingjournal
         ON cappingjournal.transactionjournalid = trans.transactionjournalid AND cappingjournal.state IN (3, /* applied */ 2 /* partiallyapplied */)
     LEFT JOIN tm_rule_capping cap ON cap.potnumber = cappingjournal.potid AND cap.tariffid = tariff.tarifid
     LEFT JOIN sl_transactionjournal cjournal ON trans.transactionjournalid = CJOURNAL.CANCELLATIONREFERENCE
     LEFT JOIN sl_card tokencard ON trans.tokencardid = tokencard.cardid;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
