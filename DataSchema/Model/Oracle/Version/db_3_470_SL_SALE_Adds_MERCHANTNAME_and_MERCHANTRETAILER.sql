SPOOL db_3_470.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.470', 'dby', 'Adds MerchantName and MerchantRetailer to SL_SALE');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_SALE ADD MerchantName NVARCHAR2(50) NULL;
COMMENT ON COLUMN SL_SALE.MerchantName IS 'Merchant Name included in POS object of sale request';

ALTER TABLE SL_SALE ADD MerchantRetailer NVARCHAR2(50) NULL;
COMMENT ON COLUMN SL_SALE.MerchantRetailer IS 'Merchant Retailer included in POS object of sale request';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
