SPOOL db_3_582.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.582', 'SOE', 'Refactored SL_NotificationMessageOwner.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_NOTIFICATIONMESSAGEOWNER ADD NOTIFICATIONMESSAGEOWNERID NUMBER(18 , 0);

-- =======[ Sequences ]===========================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
     'SL_NOTIFICATIONMESSAGEOWN');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data Migration ]=================================================================================================
-- insert data into new notification message owner id column

UPDATE SL_NOTIFICATIONMESSAGEOWNER SET NOTIFICATIONMESSAGEOWNERID = SL_NOTIFICATIONMESSAGEOWN_SEQ.nextval
  WHERE NOTIFICATIONMESSAGEOWNERID IS NULL;
     
-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_NOTIFICATIONMESSAGEOWNER MODIFY NOTIFICATIONMESSAGEOWNERID not null;
ALTER TABLE SL_NOTIFICATIONMESSAGEOWNER DROP CONSTRAINT PK_NotificationMessageOwner;
ALTER TABLE SL_NOTIFICATIONMESSAGEOWNER ADD CONSTRAINT UC_NOTIFICATIONMESSAGEOWNER UNIQUE (CONTRACTID, NOTIFICATIONMESSAGEID, CUSTOMERACCOUNTID);
ALTER TABLE SL_NOTIFICATIONMESSAGEOWNER ADD CONSTRAINT PK_NOTIFICATIONMESSAGEOWNER PRIMARY KEY (NOTIFICATIONMESSAGEOWNERID);
COMMENT ON COLUMN SL_NOTIFICATIONMESSAGEOWNER.NOTIFICATIONMESSAGEOWNERID IS 'Unique identifier which serves as primary key.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
