SPOOL db_3_106.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.106', 'DST', 'Added new column CardSequenceNumber to SL_CreditCardAuthorization');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_creditcardauthorization add cardsequencenumber number(9);
comment on column sl_creditcardauthorization.cardsequencenumber is 'The Card Sequence Number is a number used to differentiate cards that have the same
primary account number (PAN).';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
