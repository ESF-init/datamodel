SPOOL db_3_129.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.129', 'AMA', 'Update Unit Number fields');

-- Start adding schema changes here


-- =======[ Changes on existing tables ]===========================================================================

alter table um_unit
modify
(
	UNITNO	NUMBER(18),
	VISIBLEUNITNO	NUMBER(18)
);


---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
