SPOOL db_3_171.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.171', 'SLR', 'Add Column BRTYPE9ID to  TM_TICKET');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE TM_TICKET
ADD ( BRTYPE9ID  NUMBER(18) );

COMMENT ON COLUMN TM_TICKET.BRTYPE9ID IS 'Field for Business rule type reference';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
