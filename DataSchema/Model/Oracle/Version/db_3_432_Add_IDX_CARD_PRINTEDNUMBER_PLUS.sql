SPOOL db_3_432.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.432', 'LJU', 'Add IDX_CARD_PRINTEDNUMBER_PLUS');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================
CREATE INDEX IDX_CARD_PRINTEDNUMBER_PLUS ON SL_CARD
(PRINTEDNUMBER, ISPRODUCTPOOL, CARDID, DESCRIPTION);

---End adding schema changes 

-- =======[ Commit ]===============================================================================================
COMMIT;

PROMPT Done!

SPOOL OFF;
