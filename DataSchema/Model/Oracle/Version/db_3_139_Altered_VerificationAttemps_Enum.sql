SPOOL db_3_139.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.139', 'EPA', 'Added "Password" enum to SL_VERIFICATIONATTEMPTTYPE.');

-- =======[ Changes to Existing Tables ]===========================================================================

INSERT INTO SL_VERIFICATIONATTEMPTTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (3, 'Password', 'Password verification attempt.');
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;