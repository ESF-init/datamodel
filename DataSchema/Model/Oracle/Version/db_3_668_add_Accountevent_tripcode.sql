SPOOL db_3_668.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.668', 'ULB', 'Add Tripcode to Prot_Accountevent');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE PROT_ACCOUNTEVENT ADD (TripCode VARCHAR2(20 BYTE));

COMMENT ON COLUMN PROT_ACCOUNTEVENT.TripCode IS 'Contains the current tripcode';
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
