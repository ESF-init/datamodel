SPOOL db_3_032.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.032', 'DST', 'New table SL_CreditCardMerchant');

-- =======[ New Tables ]===========================================================================================

create table sl_creditcardmerchant
(
    creditcardmerchantid number(18)         not null,
    externalmerchantid   nvarchar2(128)     not null,
    name                 nvarchar2(128),
    lastuser             varchar2(50  byte) default 'SYS' not null,
    lastmodified         date               default sysdate not null,	
    transactioncounter   integer            not null
);

alter table sl_creditcardmerchant
   add constraint pk_creditcardmerchant
   primary key (creditcardmerchantid);

create sequence sl_creditcardmerchant_seq
       increment by 1
       nominvalue
       nomaxvalue
       cache 20
       nocycle
       noorder;

create or replace trigger sl_creditcardmerchant_bri before insert on sl_creditcardmerchant for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/

create or replace trigger sl_creditcardmerchant_bru before update on sl_creditcardmerchant for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

comment on table sl_creditcardmerchant IS 'Defines a merchant account used for credit card processing.';
comment on column sl_creditcardmerchant.creditcardmerchantid IS 'ID of the merchant account.';
comment on column sl_creditcardmerchant.externalmerchantid IS 'External ID of this merchant (MID)';
comment on column sl_creditcardmerchant.name IS 'Optional name of the merchant';

comment on column sl_creditcardmerchant.lastmodified IS 'Technical field: date time of the last change to this dataset';
comment on column sl_creditcardmerchant.transactioncounter IS 'Technical field: counter to prevent concurrent changes to this dataset';
comment on column sl_creditcardmerchant.lastuser IS 'Technical field: last (system) user that changed this dataset';

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_creditcardauthorization modify externalauthid null;

alter table sl_creditcardterminal add CreditCardMerchantID Number(18) NOT NULL;
comment on column sl_creditcardterminal.CreditCardMerchantID IS 'Merchant this terminal belongs to';

alter table sl_creditcardterminal
  add constraint fk_creditcardterm_merchant foreign key (creditcardmerchantid)
  references sl_creditcardmerchant (creditcardmerchantid);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
