SPOOL db_3_371.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.371', 'DST', 'Added CreditCardAuthorizationID to SL_Sale.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_Sale ADD CreditCardAuthorizationID NUMBER(18);
ALTER TABLE SL_Sale ADD CONSTRAINT FK_Sale_CreditCardAuth FOREIGN KEY (CreditCardAuthorizationID) REFERENCES SL_CreditCardAuthorization (CreditCardAuthorizationID);
create index idx_sale_creditcardauth on sl_sale (creditcardauthorizationid);
COMMENT ON COLUMN SL_SALE.CreditCardAuthorizationID is 'Optional reference to authorization. References SL_CreditCardAuthorization.CreditCardAuthorizationID.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
