SPOOL db_3_263.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.263', 'FLF', 'Added SL_ATTRIBUTE, SL_ATTRIBUTEVALUE, SL_ATTRIBUTEVALUETOPERSON');

CREATE TABLE SL_ATTRIBUTE
(
  ATTRIBUTEID       NUMBER(18)                  NOT NULL,
  ATTRIBUTENAME     VARCHAR2(300)               NOT NULL,
  DESCRIPTION       VARCHAR2(300),
  ISMULTIVALUE      NUMBER(1)                   NOT NULL,
  ATTRIBUTECLASSID  NUMBER(9)                   NOT NULL,
  LASTUSER                       NVARCHAR2(50)         DEFAULT 'SYS',
  LASTMODIFIED                   DATE                  DEFAULT sysdate,
  TRANSACTIONCOUNTER             INTEGER               NOT NULL
);

COMMENT ON COLUMN SL_ATTRIBUTE.ATTRIBUTEID IS 'Unique identifier of the attribute';
COMMENT ON COLUMN SL_ATTRIBUTE.ATTRIBUTENAME IS 'Name of the attribute.';
COMMENT ON COLUMN SL_ATTRIBUTE.DESCRIPTION IS 'Description of the attribute.';
COMMENT ON COLUMN SL_ATTRIBUTE.ISMULTIVALUE IS 'Defines if multi selection is allowed for the attribute.';
COMMENT ON COLUMN SL_ATTRIBUTE.ATTRIBUTECLASSID IS 'Identifies the class of the attribute. Should indicate which tables this attribute can be bound to. Eg.SL_PERSON.';

ALTER TABLE SL_ATTRIBUTE ADD (
  CONSTRAINT SL_ATTRIBUTE_PK
  PRIMARY KEY
  (ATTRIBUTEID)
  ENABLE VALIDATE);


CREATE TABLE SL_ATTRIBUTEVALUE
(
  ATTRIBUTEVALUEID  NUMBER(18)                  NOT NULL,
  ATTRIBUTEID       NUMBER(18)                  NOT NULL,
  VALUE             VARCHAR2(2000)              NOT NULL,
  ORDERNUMBER       NUMBER(9)                   NOT NULL,
  LASTUSER                       NVARCHAR2(50)         DEFAULT 'SYS',
  LASTMODIFIED                   DATE                  DEFAULT sysdate,
  TRANSACTIONCOUNTER             INTEGER               NOT NULL
);

COMMENT ON COLUMN SL_ATTRIBUTEVALUE.ATTRIBUTEVALUEID IS 'Unique identifier.';
COMMENT ON COLUMN SL_ATTRIBUTEVALUE.ATTRIBUTEID IS 'Identifier of the attribute the value is assigned to.';
COMMENT ON COLUMN SL_ATTRIBUTEVALUE.VALUE IS 'Actual value of the attribute.';
COMMENT ON COLUMN SL_ATTRIBUTEVALUE.ORDERNUMBER IS 'Number to order values of one attribute.';


ALTER TABLE SL_ATTRIBUTEVALUE ADD (
  CONSTRAINT SL_ATTRIBUTEVALUE_PK
  PRIMARY KEY
  (ATTRIBUTEVALUEID)
  ENABLE VALIDATE);
  
ALTER TABLE SL_ATTRIBUTEVALUE
ADD CONSTRAINT SL_ATTRIBUTEVALUE_R01 
  FOREIGN KEY (ATTRIBUTEID) 
  REFERENCES SL_ATTRIBUTE (ATTRIBUTEID);

  
CREATE TABLE SL_ATTRIBUTEVALUETOPERSON
(
  PERSONID          NUMBER(18)                  NOT NULL,
  ATTRIBUTEVALUEID  NUMBER(18)                  NOT NULL,
  LASTUSER                       NVARCHAR2(50)         DEFAULT 'SYS',
  LASTMODIFIED                   DATE                  DEFAULT sysdate,
  TRANSACTIONCOUNTER             INTEGER               NOT NULL
);

COMMENT ON COLUMN SL_ATTRIBUTEVALUETOPERSON.PERSONID IS 'References the person the attribute value is assigned to.';
COMMENT ON COLUMN SL_ATTRIBUTEVALUETOPERSON.ATTRIBUTEVALUEID IS 'References the attribute value the person is assigned to.';


ALTER TABLE SL_ATTRIBUTEVALUETOPERSON
 ADD CONSTRAINT SL_ATTRIBUTEVALUETOPERSON_PK
  PRIMARY KEY
  (ATTRIBUTEVALUEID, PERSONID);


ALTER TABLE SL_ATTRIBUTEVALUETOPERSON
 ADD CONSTRAINT SL_ATTRIBUTEVALUETOPERSON_R01 
  FOREIGN KEY (PERSONID) 
  REFERENCES SL_PERSON (PERSONID);

ALTER TABLE SL_ATTRIBUTEVALUETOPERSON
 ADD CONSTRAINT SL_ATTRIBUTEVALUETOPERSON_R02 
  FOREIGN KEY (ATTRIBUTEVALUEID) 
  REFERENCES SL_ATTRIBUTEVALUE (ATTRIBUTEVALUEID);
  
  
-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(3) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_ATTRIBUTE', 'SL_ATTRIBUTEVALUE', 'SL_ATTRIBUTEVALUETOPERSON');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(3) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_ATTRIBUTE', 'SL_ATTRIBUTEVALUE', 'SL_ATTRIBUTEVALUETOPERSON');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/



---End adding schema changes 
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
