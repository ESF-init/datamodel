SPOOL db_3_165.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.165', 'APD', 'Creation of SL_comment and SL_commentpriority');

CREATE TABLE SL_COMMENTPRIORITY
(
  ENUMERATIONVALUE  NUMBER(9,0)                NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL
);

COMMENT ON TABLE SL_COMMENTPRIORITY IS 'Priority of a comment';

COMMENT ON COLUMN SL_COMMENTPRIORITY.ENUMERATIONVALUE IS 'Unique ID of the comment priorities';
COMMENT ON COLUMN SL_COMMENTPRIORITY.LITERAL IS 'Literal for comment priorities';
COMMENT ON COLUMN SL_COMMENTPRIORITY.DESCRIPTION IS 'Description for comment priorities';

CREATE TABLE SL_COMMENT
(
  COMMENTID           NUMBER(18,0)              NOT NULL,
  CONTRACTID          NUMBER(18,0)              NOT NULL,
  TEXT                NVARCHAR2(512)            NOT NULL,
  PRIORITY            NUMBER(9,0)               DEFAULT 0                     NOT NULL,
  LASTUSER            NVARCHAR2(50)             DEFAULT 'SYS',
  LASTMODIFIED        DATE                      DEFAULT sysdate,
  TRANSACTIONCOUNTER  INTEGER              NOT NULL
);

ALTER TABLE sl_comment add constraint pk_comment primary key (commentid);
alter table sl_comment add constraint fk_comment_contract foreign key (contractid) references sl_contract(contractid);

COMMENT ON TABLE SL_COMMENT IS 'Table of comments that can be created for a contract';

COMMENT ON COLUMN SL_COMMENT.COMMENTID IS 'Unique ID of a comment';
COMMENT ON COLUMN SL_COMMENT.CONTRACTID IS 'REFERENCES to SL_CommentPriority';
COMMENT ON COLUMN SL_COMMENT.TEXT IS 'Textual description of the comment';
COMMENT ON COLUMN SL_COMMENT.PRIORITY IS 'Priority of the comment';
COMMENT ON COLUMN SL_COMMENT.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_COMMENT.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_COMMENT.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';


-- =======[ Sequences ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_COMMENT');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_COMMENT');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

COMMIT;

PROMPT Done!

SPOOL OFF;
