SPOOL db_3_592.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.592', 'ARH', 'Updated Paratransit tables');

-- =======[ Changes on existing tables ]===========================================================================
INSERT INTO SL_PARATRANSPAYMENTTYPE(ENUMERATIONVALUE, DESCRIPTION) VALUES (0, 'No payment type');

COMMENT ON TABLE SL_PARATRANSJOURNAL is 'It holds all transactional information about a paratransit trip';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.PARATRANSJOURNALID IS 'Unique identifier of SL_PARATRANSJOURNAL';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.PARATRANSJOURNALTYPE IS 'Link with SL_PARATRANSJOURNALTYPE. Is required to model the different use cases in a single transaction table';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.PARATRANSPAYMENTTYPE IS 'Payment types : this field only needs to be set for a boarding transaction. NULL for all subsequent transactions ';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.DEVICETIME IS 'Date and time of the transaction';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.OPERATORID IS 'Driver ID that is logged into the application';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.OPERATORCOMPANYID IS 'Cab Company or HandiVan identifier';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.VEHICLENUMBER IS 'Identifies the vehicle. This number is not configured in MOBILEvario but is a meaningful identifier for the service provider (e.g. 3rd Party Cab Company)';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.EXTERNALTRIPID IS 'ID of the trip #, entered by the driver. This number is used to reconcile data between trip reports, invoices and the Trapeze system.';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.EXTERNALRIDERID IS 'ID that identifies the passenger in the trapeze system. INIT will store that ID in the cardholder record of the fare collection system TransitAccount.';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.FARETRANSACTIONID IS 'ID that was generated to perform the fare collection transaction. This is used to tie the two records (paratransit and fare collection) together. Null for Eligibility Center rides.';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.FAREMETER IS 'The end value of the fare meter in a taxi ride scenario. This field must only be set when the transaction type is “EndRide”. For HandiVan scenarios, the value can be NULL.';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.LATITUDE IS 'GPS coordinates.';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.LONGITUDE IS 'GPS coordinates.';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.ODOMETER IS 'Current odometer value. Can be empty for No Show and Cancellation transactions.';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.SHAREDRIDEID IS 'The ID of an existing shared ride. This field can be left empty, if no shared ride is present.';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.LASTMODIFIER IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_PARATRANSJOURNAL.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';

COMMENT ON TABLE SL_PARATRANSJOURNALTYPE is 'The SL_PARATRANSJOURNALTYPE table is required to model the different use cases in a single transaction table (SL_PARATRANSJOURNAL).';
COMMENT ON COLUMN SL_PARATRANSJOURNALTYPE.ENUMERATIONVALUE IS 'Unique identifier of SL_PARATRANSJOURNALTYPE';
COMMENT ON COLUMN SL_PARATRANSJOURNALTYPE.DESCRIPTION IS 'Description of the journal type';

COMMENT ON TABLE SL_PARATRANSPAYMENTTYPE is 'List the different payment types for a paratransit trip.';
COMMENT ON COLUMN SL_PARATRANSPAYMENTTYPE.ENUMERATIONVALUE IS 'Unique identifier of SL_PARATRANSPAYMENTTYPE';
COMMENT ON COLUMN SL_PARATRANSPAYMENTTYPE.DESCRIPTION IS 'Description of the payment type';

COMMENT ON TABLE SL_PARATRANSATTRIBUTE is 'The SL_PARATRANSATTRIBUTE table stores all additional information captured for a paratranst trip such as accessories';
COMMENT ON COLUMN SL_PARATRANSATTRIBUTE.PARATRANSATTRIBUTEID IS 'SL_PARATRANSATTRIBUTE SL_PARATRANSATTRIBUTETYPE';
COMMENT ON COLUMN SL_PARATRANSATTRIBUTE.PARATRANSATTRIBUTETYPEID IS 'Link with SL_PARATRANSATTRIBUTETYPE.';
COMMENT ON COLUMN SL_PARATRANSATTRIBUTE.AMOUNT IS 'Amount/quantity';
COMMENT ON COLUMN SL_PARATRANSATTRIBUTE.EXTERNALTRIPID IS 'ID that identifies the passenger in the trapeze system.';
COMMENT ON COLUMN SL_PARATRANSATTRIBUTE.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_PARATRANSATTRIBUTE.LASTMODIFIER IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_PARATRANSATTRIBUTE.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';

COMMENT ON TABLE SL_PARATRANSATTRIBUTETYPE is 'A static table for the configured attributes and their costs needed fr paratransit trips.';
COMMENT ON COLUMN SL_PARATRANSATTRIBUTETYPE.ENUMERATIONVALUE IS 'Unique identifier of SL_PARATRANSATTRIBUTETYPE';
COMMENT ON COLUMN SL_PARATRANSATTRIBUTETYPE.DESCRIPTION IS 'Description of the attribute.';
COMMENT ON COLUMN SL_PARATRANSATTRIBUTETYPE.COST IS 'Cost of an attribute.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
