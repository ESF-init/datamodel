SPOOL db_3_090.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.090', 'SOE', 'Updated View SL_INVOICE_VIEW');

-- =======[ Views ]===========================================================================

CREATE OR REPLACE FORCE VIEW SL_INVOICE_VIEW
(
    SCHOOLYEARID,
    CONTRACTID,
    ORGANIZATIONNAME,
    ORGANIZATIONNUMBER,
    INVOICEENTRYAMOUNT,
    CREATIONDATE,
    FROMDATE,
    INVOICEENTRYCOUNT,
    INVOICEID,
    INVOICENUMBER,
    INVOICETYPE,
    TODATE,
    ACTIVECARDCOUNT,
    BLOCKEDCARDCOUNT,
    REPLACEDCARDCOUNT,
    PRINTEDCARDCOUNT,
    NOTPRINTEDCARDCOUNT
    )
AS
 
SELECT 
i.schoolyearid, 
co.contractid, 
o.NAME, 
o.organizationnumber,
i.invoiceentryamount, 
i.creationdate, 
i.fromdate,
i.invoiceentrycount, 
i.invoiceid, 
i.invoicenumber, 
i.invoicetype,
i.todate, 
count(decode(crd.state, 1, 1, null)) ActiveCardCount, 
count(decode(crd.state, 2, 1, null)) BlockedCardCount,
count(decode(crd.state, 3, 1, null)) ReplacedCardCount, 
count(decode(numberofprints, null, 1, null)) PrintedCardCount,
count(decode(numberofprints, null, null, 1)) NotPrintedCardCount
FROM sl_invoice i 
INNER JOIN sl_contract co ON i.contractid = co.contractid
INNER JOIN sl_organization o ON o.organizationid = co.organizationid
INNER JOIN sl_invoiceentry ie ON i.invoiceid = ie.invoiceid
INNER JOIN sl_posting frst ON ie.invoiceentryid = frst.invoiceentryid
INNER JOIN sl_product prod ON frst.productid = prod.productid
INNER JOIN sl_card crd ON crd.cardid = prod.cardid
group by i.schoolyearid, co.contractid, o.NAME, o.organizationnumber,
          i.invoiceentryamount, i.creationdate, i.fromdate,
          i.invoiceentrycount, i.invoiceid, i.invoicenumber, i.invoicetype,
          i.todate
order by invoiceid, contractid;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
