SPOOL db_3_094.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.094', 'SOE', 'added printedDate Card');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_card add printedDate date null ;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;