SPOOL db_3_050.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.050', 'BAW', 'Added new column to recognized load and use view to indicate cancelled taps. New view for optimizied sale export.');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================
CREATE OR REPLACE VIEW ACC_RECOGNIZEDLOADANDUSE 
(AMOUNT, BASEFARE,  CREDITACCOUNTNUMBER, CUSTOMERGROUP, DEBITACCOUNTNUMBER,
LINEGROUPID, POSTINGDATE, POSTINGREFERENCE, PREMIUM, REVENUERECOGNITIONID,
REVENUESETTLEMENTID, TICKETNUMBER, OPERATORID, CLOSEOUTPERIODID, PERIODFROM,
PERIODTO, CLOSEOUTTYPE, STATE, CANCELLATIONREFERENCE, CANCELLATIONREFERENCEGUID,
CLIENTID, DEVICETIME, FAREAMOUNT, LINE, TRANSACTIONOPERATORID,
PRODUCTID, PURSEBALANCE, PURSECREDIT, RESULTTYPE, SALESCHANNELID,
TARIFFDATE, TARIFFVERSION, TICKETID, TICKETINTERNALNUMBER, TRANSACTIONJOURNALID,
TRANSACTIONTYPE, TRIPTICKETINTERNALNUMBER, CANCELLEDTAP)
AS
SELECT 
  RR.AMOUNT,
  RR.BASEFARE,
  RR.CREDITACCOUNTNUMBER,
  RR.CUSTOMERGROUP,
  RR.DEBITACCOUNTNUMBER,
  RR.LINEGROUPID,
  RR.POSTINGDATE,
  RR.POSTINGREFERENCE,
  RR.PREMIUM,
  RR.REVENUERECOGNITIONID,
  RR.REVENUESETTLEMENTID,
  RR.TICKETNUMBER,
  RR.OPERATORID,
  CP.CLOSEOUTPERIODID,
  CP.PERIODFROM,
  CP.PERIODTO,
  CP.CLOSEOUTTYPE,
  CP.STATE,
  TJ1.CANCELLATIONREFERENCE,
  TJ1.CANCELLATIONREFERENCEGUID,
  TJ1.CLIENTID,
  TJ1.DEVICETIME,
  TJ1.FAREAMOUNT,
  TJ1.LINE,
  TJ1.OPERATORID AS TRANSACTIONOPERATORID,
  TJ1.PRODUCTID,
  TJ1.PURSEBALANCE,
  TJ1.PURSECREDIT,
  TJ1.RESULTTYPE,
  TJ1.SALESCHANNELID,
  TJ1.TARIFFDATE,
  TJ1.TARIFFVERSION,
  TJ1.TICKETID,
  TJ1.TICKETINTERNALNUMBER,
  TJ1.TRANSACTIONJOURNALID,
  TJ1.TRANSACTIONTYPE,
  TJ1.TRIPTICKETINTERNALNUMBER,
  CAST (DECODE (TJ1.CANCELLATIONREFERENCEGUID, NULL, 0, 1) AS NUMBER(9)) AS CANCELLEDTAP
FROM (SL_TRANSACTIONJOURNAL TJ1 INNER JOIN ACC_REVENUERECOGNITION RR ON TJ1.TRANSACTIONJOURNALID = RR.TRANSACTIONJOURNALID)
  INNER JOIN ACC_CLOSEOUTPERIOD CP ON CP.CLOSEOUTPERIODID = RR.CLOSEOUTPERIODID
WHERE TJ1.TRANSACTIONTYPE = 108 OR TJ1.TRANSACTIONTYPE = 85 OR (TJ1.TRANSACTIONTYPE = 66 AND TJ1.VALIDTO IS NOT NULL);

CREATE OR REPLACE VIEW ACC_RECOGNIZEDSALETRANSACTIONS
(CLOSEOUTPERIODID, PERIODFROM, PERIODTO, CLOSEOUTDATE, STATE, CLOSEOUTTYPE,
SALESREVENUEID, POSTINGDATE, POSTINGREFERENCE, CREDITACCOUNTNUMBER,
DEBITACCOUNTNUMBER, AMOUNT, TRANSACTIONJOURNALID, TRANSACTIONID, LINE, COURSENUMBER,
ROUTENUMBER, ROUTECODE, DIRECTION, ZONE, VEHICLENUMBER,
MOUNTINGPLATENUMBER, DEBTORNUMBER, STOPNUMBER, TRANSITACCOUNTID, FAREMEDIAID,
FAREMEDIATYPE, PRODUCTID, TICKETID, FAREAMOUNT, CUSTOMERGROUP, TRANSACTIONTYPE,
RESULTTYPE, FILLLEVEL, PURSEBALANCE, PURSECREDIT, GROUPSIZE, VALIDFROM,
VALIDTO, DURATION, TARIFFDATE, TARIFFVERSION, TICKETINTERNALNUMBER,
CANCELLATIONREFERENCE, CANCELLATIONREFERENCEGUID, TRIPTICKETINTERNALNUMBER,
PRODUCTID2, PURSEBALANCE2, PURSECREDIT2, SALEID, SALETRANSACTIONID,
SALETYPE, SALEDATE, CLIENTID, SALESCHANNELID, LOCATIONNUMBER, DEVICENUMBER,
SALESPERSONNUMBER, MERCHANTNUMBER, RECEIPTREFERENCE, NETWORKREFERENCE,
SALECANCELLATIONREFERENCEID, ISCLOSED, ORDERID, EXTERNALORDERNUMBER,
CONTRACTID, REFUNDREFERENCEID, ISORGANIZATIONAL)
AS
SELECT 
CP.CLOSEOUTPERIODID,  
CP.PERIODFROM, 
CP.PERIODTO,
CP.CLOSEOUTDATE,
CP.STATE,
CP.CLOSEOUTTYPE,
SR.SALESREVENUEID,
SR.POSTINGDATE,
SR.POSTINGREFERENCE,
SR.CREDITACCOUNTNUMBER,
SR.DEBITACCOUNTNUMBER,
SR.AMOUNT,
TJ.TRANSACTIONJOURNALID,
TJ.TRANSACTIONID,
TJ.LINE,
TJ.COURSENUMBER,
TJ.ROUTENUMBER,
TJ.ROUTECODE,
TJ.DIRECTION,
TJ.ZONE,
TJ.VEHICLENUMBER,
TJ.MOUNTINGPLATENUMBER,
TJ.DEBTORNUMBER,
TJ.STOPNUMBER,
TJ.TRANSITACCOUNTID,
TJ.FAREMEDIAID,
TJ.FAREMEDIATYPE,
TJ.PRODUCTID,
TJ.TICKETID,
TJ.FAREAMOUNT,
TJ.CUSTOMERGROUP,
TJ.TRANSACTIONTYPE,
TJ.RESULTTYPE,
TJ.FILLLEVEL,
TJ.PURSEBALANCE,
TJ.PURSECREDIT,
TJ.GROUPSIZE,
TJ.VALIDFROM,
TJ.VALIDTO,
TJ.DURATION,
TJ.TARIFFDATE,
TJ.TARIFFVERSION,
TJ.TICKETINTERNALNUMBER,
TJ.CANCELLATIONREFERENCE,
TJ.CANCELLATIONREFERENCEGUID,
TJ.TRIPTICKETINTERNALNUMBER,
TJ.PRODUCTID2,
TJ.PURSEBALANCE2,
TJ.PURSECREDIT2,
S.SALEID,
S.SALETRANSACTIONID,
S.SALETYPE,
S.SALEDATE,
S.CLIENTID,
S.SALESCHANNELID,
S.LOCATIONNUMBER,
S.DEVICENUMBER,
S.SALESPERSONNUMBER,
S.MERCHANTNUMBER,
S.RECEIPTREFERENCE,
S.NETWORKREFERENCE,
S.CANCELLATIONREFERENCEID AS SALECANCELLATIONREFERENCEID,
S.ISCLOSED,
S.ORDERID,
S.EXTERNALORDERNUMBER,
S.CONTRACTID,
S.REFUNDREFERENCEID,
S.ISORGANIZATIONAL
FROM ACC_CLOSEOUTPERIOD CP, ACC_SALESREVENUE SR, SL_TRANSACTIONJOURNAL TJ, SL_SALE S
WHERE CP.CLOSEOUTPERIODID = SR.CLOSEOUTPERIODID AND 
    SR.TRANSACTIONJOURNALID = TJ.TRANSACTIONJOURNALID AND
    TJ.SALEID = S.SALEID;
	
-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
