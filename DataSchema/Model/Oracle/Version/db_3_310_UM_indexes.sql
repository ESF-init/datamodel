SPOOL db_3_310.log;

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.310', 'MKK', 'New indexes needed for better performance of Loadingstatistics');

-- =======[ Changes to Existing Tables ]===========================================================================

prompt CREATE INDEX UM_LOADINGSTAT_ARCHIVEID_DEVICEID ON UM_LOADINGSTATISTICS
CREATE INDEX UM_LS_ARCHIVEID_DEVICEID ON UM_LOADINGSTATISTICS
(deviceid, archiveid)
;

prompt CREATE INDEX UM_DEVICE_UNITID ON UM_DEVICE
CREATE INDEX UM_DEVICE_UNITID ON UM_DEVICE
(unitid)
;


PROMPT Done!

SPOOL OFF;
