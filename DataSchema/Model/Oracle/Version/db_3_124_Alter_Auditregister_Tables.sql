SPOOL db_3_124.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.124', 'EPA', 'Altered audit register tables.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
DROP TRIGGER SL_AUDITREGISTERVALUE_BRI;
DROP TRIGGER SL_AUDITREGISTERVALUE_BRU;
DROP SEQUENCE SL_AUDITREGISTERVALUE_SEQ;
DROP TABLE SL_AUDITREGISTERVALUE;

DROP TABLE SL_AUDITREGISTERVALUETYPE;
DROP TABLE SL_AUDITREGISTERSUBSETTYPE;

ALTER TABLE SL_AUDITREGISTER
ADD (VALIDATIONS NUMBER(18,0),
RIDERCLASSCOUNTS NUMBER(18,0),
APPROVEDTRANSACTIONS NUMBER(18,0),
DENIEDTRANSACTIONS NUMBER(18,0),
READFAILURES NUMBER(18,0),
VALIDATEDFAREPRODUCTS NUMBER(18,0),
DEDUCTEDFAREPRODUCTS NUMBER(18,0),
NEWFAREMEDIA NUMBER(18,0),
FAREPRODUCTSALES NUMBER(18,0),
STOREDVALUELOADED NUMBER(18,0),
ACCOUNTINQUIRIES NUMBER(18,0),
CASHTRANSACTIONS NUMBER(18,0),
CARDTRANSACTIONS NUMBER(18,0),
TRANSACTIONS NUMBER(18,0),
TOTALCOUNTS NUMBER(18,0),
TOTALVALUES NUMBER(18,0),
SUCCESSDATETIME DATE);

COMMENT ON COLUMN SL_AUDITREGISTER.VALIDATIONS is 'Total validation counts';
COMMENT ON COLUMN SL_AUDITREGISTER.RIDERCLASSCOUNTS is 'Rider class counts';
COMMENT ON COLUMN SL_AUDITREGISTER.APPROVEDTRANSACTIONS is 'Count of approved transactions';
COMMENT ON COLUMN SL_AUDITREGISTER.DENIEDTRANSACTIONS is 'Count of denied transactions';
COMMENT ON COLUMN SL_AUDITREGISTER.READFAILURES is 'Count of read failures';
COMMENT ON COLUMN SL_AUDITREGISTER.VALIDATEDFAREPRODUCTS is 'Fare products validated';
COMMENT ON COLUMN SL_AUDITREGISTER.DEDUCTEDFAREPRODUCTS is 'Fare value deducted';
COMMENT ON COLUMN SL_AUDITREGISTER.NEWFAREMEDIA is 'New fare media.';
COMMENT ON COLUMN SL_AUDITREGISTER.FAREPRODUCTSALES is 'Fare product sold';
COMMENT ON COLUMN SL_AUDITREGISTER.STOREDVALUELOADED is 'Stored value loaded';
COMMENT ON COLUMN SL_AUDITREGISTER.ACCOUNTINQUIRIES is 'Account inquiries';
COMMENT ON COLUMN SL_AUDITREGISTER.CASHTRANSACTIONS is 'Cash transactions by amount and denomination';
COMMENT ON COLUMN SL_AUDITREGISTER.CARDTRANSACTIONS is 'Credit and debit card transactions by amount';
COMMENT ON COLUMN SL_AUDITREGISTER.TRANSACTIONS is 'Count of approved and denied transactions';
COMMENT ON COLUMN SL_AUDITREGISTER.TOTALCOUNTS is 'The total count of all transactions completed since data was last uploaded to the back office';
COMMENT ON COLUMN SL_AUDITREGISTER.TOTALVALUES is 'The total value of all transactions completed since data was last uploaded to the back office';
COMMENT ON COLUMN SL_AUDITREGISTER.SUCCESSDATETIME is 'The date and time of the last successful data upload to the back office';

-- =======[ New Tables ]===========================================================================================

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
