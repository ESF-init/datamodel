SPOOL db_3_467.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.467', 'MFA', 'Alter table SL_TRANSACTIONJOURNAL, add new column CARDHOLDERID.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_TRANSACTIONJOURNAL ADD CARDHOLDERID NUMBER(18) REFERENCES SL_PERSON(PERSONID);
COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.CARDHOLDERID IS 'The cardholder id for the transaction.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
