SPOOL db_3_622.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.622', 'AWH', 'New fields CASHRETURN and CASHRECEIVED to SL_PaymentJournal');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PAYMENTJOURNAL
ADD  CASHRETURN NUMBER(9) NULL;
COMMENT ON COLUMN SL_PAYMENTJOURNAL.CASHRETURN IS 'The amount of cash being returned to a customer when a payment is made.'; 

ALTER TABLE SL_PAYMENTJOURNAL
ADD  CASHRECEIVED NUMBER(9) NULL;
COMMENT ON COLUMN SL_PAYMENTJOURNAL.CASHRECEIVED IS 'The amount of cash being received from a customer when a payment is made.'; 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
