SPOOL db_3_254.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.254', 'FLF', 'Adding tariffid index to  TM_AREALISTELEMENTGROUP');

-- =======[ Changes to Existing Tables ]===========================================================================

CREATE INDEX IDX_AREALISTELEMENTGROUP ON TM_AREALISTELEMENTGROUP (TARIFFID);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
