SPOOL db_3_332.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.332', 'EPA', 'Added view ACC_BANKSTATEMENTVERIFICATION');

-- =======[ Views ]================================================================================================
CREATE OR REPLACE FORCE VIEW ACC_BANKSTATEMENTVERIFICATION
(
    BANKSTATEMENTID,
    PAYMENTJOURNALID,
    SALEID,
    MATCHEDSTATEMENTS,
    AMOUNT,
    PAYMENTREFERENCE, 
    TRANSACTIONDATE,
    BANKSTATEMENTSTATE,
    PAYMENTSTATE)
AS SELECT 
        j.BANKSTATEMENTID as BANKSTATEMENTID,
        j.PAYMENTJOURNALID as PAYMENTJOURNALID,
        j.SALEID as SALEID,
        (case when j.PAYMENTJOURNALID is null or j.saleid is null then 0 else COUNT end) as MATCHEDSTATEMENTS, 
        bs.AMOUNT as AMOUNT,
        bs.PAYMENTREFERENCE as PAYMENTREFERENCE, 
        bs.TRANSACTIONDATE	as TRANSACTIONDATE,
        bs.STATE as BANKSTATEMENTSTATE,
        pj.STATE as PAYMENTSTATE
    from (
       SELECT 
            b.BANKSTATEMENTID as BANKSTATEMENTID, 
            count(b.bankstatementid) as COUNT, 
            min(p.PAYMENTJOURNALID) as PAYMENTJOURNALID, 
            min(s.SALEID) as SALEID
        FROM ACC_BANKSTATEMENT b
        left JOIN SL_PAYMENTJOURNAL p ON p.Amount = b.Amount and p.Code = b.PaymentReference and p.state = 1
        left JOIN SL_SALE s ON s.SALEID = p.SALEID and (
            (s.SALESCHANNELID = 70 and trunc(b.TRANSACTIONDATE) = trunc(p.executiontime)) OR
            (s.SALESCHANNELID <> 70 and trunc(b.TRANSACTIONDATE + (1/24*2.997)) = trunc(p.executiontime)))    
        group by b.BANKSTATEMENTID
    ) j
    left join SL_PAYMENTJOURNAL pj on pj.PAYMENTJOURNALID = j.PAYMENTJOURNALID
    left join ACC_BANKSTATEMENT bs on bs.BANKSTATEMENTID = j.BANKSTATEMENTID;

COMMENT ON COLUMN ACC_BANKSTATEMENTVERIFICATION.BANKSTATEMENTID IS 'Reference to ACC_BANKSTATEMENT.';
COMMENT ON COLUMN ACC_BANKSTATEMENTVERIFICATION.PAYMENTJOURNALID IS 'Reference to SL_PAYMENTJOURNAL for the corresponding bank statement.';
COMMENT ON COLUMN ACC_BANKSTATEMENTVERIFICATION.SALEID IS 'Reference to SL_SALE for the corresponding payment.';
COMMENT ON COLUMN ACC_BANKSTATEMENTVERIFICATION.MATCHEDSTATEMENTS IS 'Amount of matches between the bank statement and payment journals.';
COMMENT ON COLUMN ACC_BANKSTATEMENTVERIFICATION.PAYMENTREFERENCE IS 'Payment reference for the payment transaction.';
COMMENT ON COLUMN ACC_BANKSTATEMENTVERIFICATION.TRANSACTIONDATE IS 'Timestamp of the transaction.';
COMMENT ON COLUMN ACC_BANKSTATEMENTVERIFICATION.BANKSTATEMENTSTATE IS 'State of the bank statement data set.';
COMMENT ON COLUMN ACC_BANKSTATEMENTVERIFICATION.PAYMENTSTATE IS 'State of the payment journal data set.';


-- =======[ Commit ]===============================================================================================
COMMIT;
PROMPT Done!
SPOOL OFF;
