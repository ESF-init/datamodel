SPOOL db_3_450.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.450', 'bts', 'Alter  SL_PRODUCTTERMINATION_add_Terminationstatus');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_PRODUCTTERMINATION
 ADD (Terminationstatus  NUMBER(9) NOT NULL);

ALTER TABLE SL_PRODUCTTERMINATION
MODIFY(TERMINATIONSTATUS  DEFAULT 0);

COMMENT ON COLUMN SL_PRODUCTTERMINATION.Terminationstatus IS 'Describes the status of a termination: final, withdrawnor or pending';
---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
