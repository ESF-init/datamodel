SPOOL db_3_616.log;

-----------------------------------------------------------------------------
--Version 3.616
-----------------------------------------------------------------------------
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '3.616', 'SOE', 'Updated SL_EmailEvent');


-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

DELETE FROM SL_Emailevent WHERE Eventname = 'OrderCreatedIncludingOrderState';

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'SaleReceipt','Sale Receipt', -1, 'SaleMailMerge', 75);

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'UnusedPassExpiry','Unused Pass Expiry', -1, 'PassExpiryMailMerge', 76);

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'OrderConfirmed_OrderState_7','OrderConfirmed for order state On Hold', -1, 'OrderMailMerge', 77);

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'OrderConfirmed_OrderState_8','OrderConfirmed for order state Verification required', -1, 'OrderMailMerge', 78);

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'OrderConfirmed_OrderState_9','OrderConfirmed for order state Manual verification', -1, 'OrderMailMerge', 79);

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'OrderConfirmed_OrderState_4','OrderConfirmed for order state Open', -1, 'OrderMailMerge', 80);

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'OrderConfirmed_OrderState_5','OrderConfirmed for order state Failed', -1, 'OrderMailMerge', 81);

COMMIT;

PROMPT Done!

SPOOL OFF;
