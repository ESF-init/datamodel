SPOOL db_3_396.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.396', 'ARH', 'Protocol_Deprecate_and_Add_Column');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE PROTOCOL ADD HOSTNAME VARCHAR2(50 BYTE);

COMMENT ON COLUMN PROTOCOL.HOSTNAME IS 'The name of the device, for most devices this is the Windows name.';

COMMENT ON COLUMN PROTOCOL.DEVICENO IS 'DEPRECATED';

COMMIT;

PROMPT Done!

SPOOL OFF;