SPOOL db_3_437.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.437', 'BVI', 'New columns for VAS-tables.');

Alter Table vas_reportjob add (EmailRecipient nvarchar2(250));
COMMENT ON COLUMN vas_reportjob.EmailRecipient IS 'Email addresses for email notifications when the job ran.';

Alter Table vas_reportjob add (EnableNotification number(1));
COMMENT ON COLUMN vas_reportjob.EnableNotification IS 'Activate notifications when the job ran.';

Alter Table vas_reportjob add (UserID number(10));
COMMENT ON COLUMN vas_reportjob.UserID IS 'References User_List.UserID. The user who has created the job.';

Alter Table vas_reportjob add (CreationTime Date);
COMMENT ON COLUMN vas_reportjob.CreationTime IS 'Creation time of the job.';

Alter Table vas_reportjob add CONSTRAINT FK_ReportJob_User FOREIGN KEY (UserID) REFERENCES User_List(UserID);


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
