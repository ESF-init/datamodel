SPOOL db_3_637.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.637', 'SOE', 'SmartcardNegativeBalance inserted in SL_EmailEvent');

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'SmartcardNegativeBalance','SmartcardNegativeBalance', -1, 'SmartcardMailMerge', 97);

COMMIT;

PROMPT Done!

SPOOL OFF;
