SPOOL db_3_057.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.057', 'EPA', 'Add ISBLOCKED to SL_PAYMENTOPTION');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_PAYMENTOPTION ADD (ISBLOCKED NUMBER(1) DEFAULT 0 NOT NULL);
-- =======[ New Tables ]===========================================================================================
COMMENT ON COLUMN SL_PAYMENTOPTION.ISBLOCKED IS 'Represents the blocking state of the payment option.';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
