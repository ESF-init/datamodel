SPOOL db_3_563.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.563', 'FRD', 'Adding column for rule result details to CH_VALIDATIONTRANSACTION.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE CH_VALIDATIONTRANSACTION ADD VALIDATION_RESULT_DETAIL CLOB;

COMMENT ON COLUMN CH_VALIDATIONTRANSACTION.VALIDATION_RESULT_DETAIL  IS 'Validation Rules Result Details';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
