SPOOL db_3_323.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.323', 'BVI', 'Added PostingKeyType.');

-- =======[ Create+Edit ]===============================================================================================

create table SL_PostingKeyType (
	PostingKeyTypeID  	NUMBER(18)                NOT NULL,
	Name				NVARCHAR2(125)			  NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_PostingKeyType PRIMARY KEY (PostingKeyTypeID)
);

create sequence SL_PostingKeyType_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

CREATE OR REPLACE TRIGGER SL_PostingKeyType_BRI before insert on SL_PostingKeyType for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_PostingKeyType_BRU before update on SL_PostingKeyType for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

alter table SL_PostingKey add PostingKeyTypeID NUMBER(18);

alter table SL_PostingKey add CONSTRAINT FK_PostingKey_PostingKeyType FOREIGN KEY (PostingKeyTypeID) REFERENCES SL_Organization(OrganizationID);

-- =======[ Comments ]===============================================================================================

COMMENT ON TABLE SL_PostingKeyType is 'Another category to group postings.';
COMMENT ON COLUMN SL_PostingKeyType.LASTMODIFIED is 'Technical field: date time of the last change to this dataset.';
COMMENT ON COLUMN SL_PostingKeyType.LASTUSER is 'Technical field: last (system) user that changed this dataset.';
COMMENT ON COLUMN SL_PostingKeyType.TRANSACTIONCOUNTER is 'Technical field: counter to prevent concurrent changes to this dataset.';
COMMENT ON COLUMN SL_PostingKeyType.Name is 'A name for the posting key type..';
COMMENT ON COLUMN SL_PostingKeyType.PostingKeyTypeID is 'Unqiue identifier for a posting key type.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
