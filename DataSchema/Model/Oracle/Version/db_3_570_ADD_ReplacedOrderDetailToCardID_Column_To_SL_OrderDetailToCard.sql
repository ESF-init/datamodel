SPOOL db_3_570.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.570', 'EMN', 'Adding ReplacedOrderDetailToCardID column to SL_OrderDetailToCard');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_ORDERDETAILTOCARD ADD ReplacedOrderDetailToCardID NUMBER(18) NULL;
COMMENT ON COLUMN SL_ORDERDETAILTOCARD.ReplacedOrderDetailToCardID IS 'References the OrderDetailToCardId this replaced';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
