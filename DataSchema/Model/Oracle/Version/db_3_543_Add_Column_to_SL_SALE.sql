SPOOL db_3_543.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.543', 'MFA', 'Add SalesPersonEmail column into SL_SALE and alter view');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_SALE ADD SALESPERSONEMAIL NVARCHAR2(512) NULL;
COMMENT ON COLUMN SL_SALE.SALESPERSONEMAIL IS 'Sales person email address';

-- =======[ Views ]================================================================================================

 CREATE OR REPLACE FORCE VIEW "VIEW_SL_SALEHISTORY" ("SALEID", "SALETRANSACTIONID", "RECEIPTREFERENCE", "SALESCHANNELID", "SALETYPE", "LOCATIONNUMBER", "DEVICENUMBER", "SALESPERSONNUMBER", "MERCHANTNUMBER", "NETWORKREFERENCE", "SALEDATE", "OPERATORID", "ACCOUNTID", "EXTERNALORDERNUMBER", "ORDERID", "NOTES", "STATE", "ORDERNUMBER", "ISCANCELED", "AMOUNT", "LASTCUSTOMER", "SALESPERSONEMAIL") AS 
  select
    sl_sale.saleid,
    sl_sale.saletransactionid,
    sl_sale.receiptreference,
    sl_sale.saleschannelid,
    sl_sale.saletype,
    sl_sale.locationnumber,
    sl_sale.devicenumber,
    sl_sale.salespersonnumber,
    sl_sale.merchantnumber,
    sl_sale.networkreference,
    sl_sale.saledate,
    sl_sale.clientid   as operatorid,
    sl_sale.contractid as accountid,
    sl_sale.externalordernumber,
    sl_sale.orderid,
    sl_sale.notes,
    sl_order.state,
    sl_order.ordernumber,
    case when sl_sale.saleid not in (select can.cancellationreferenceid from sl_sale can where can.cancellationreferenceid is not null) then 0 else 1 end iscanceled,
    (select sum(sl_paymentjournal.amount) from sl_paymentjournal where sl_paymentjournal.saleid = sl_sale.saleid) amount,
    c.lastcustomer,
    sl_sale.salespersonemail
  from sl_sale
        left outer join sl_order on sl_sale.orderid = sl_order.orderid
        left outer join ( select h.lastcustomer, h.orderid, row_number() over (partition by h.orderid order by h.lastmodified) as rn from sl_orderhistory h ) c on c.orderid = sl_sale.orderid and rn = 1
;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
