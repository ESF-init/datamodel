SPOOL db_3_636.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.636', 'JSB', 'Add activation date to views and materialized views and set materialized views to NOLOGGING');

-- =======[ Views ]================================================================================================
-- DROP MATERIALIZED VIEWS --
DROP MATERIALIZED VIEW VIEW_UNIT_DEVICE_LOADINGSTAT;
DROP MATERIALIZED VIEW VIEW_UM_CURRELEASES;
DROP MATERIALIZED VIEW VIEW_UM_CURRELEASLOADINGSTAT;

-- CREATE VIEW VIEW_UM_CURRELEASARCHIVE
CREATE OR REPLACE FORCE VIEW view_um_curreleasarchives (archiveid,
                                                        archivejobid,
                                                        archivedistributiontaskid,
                                                        deviceclassid,
                                                        filename,
                                                        title,
                                                        clientid,
                                                        archivetype,
                                                        deviceclassshort,
                                                        started,
                                                        useall,
                                                        archiveactivationdate
                                                       )
AS
   SELECT DISTINCT a.archiveid, j.archivejobid, dt.archivedistributiontaskid,
                   j.deviceclassid, a.filename, j.filename AS title,
                   j.clientid, t.typename AS archivetype,
                   c.shortname AS deviceclassshort, dt.started, j.useall,
                   a.activationdate as archiveactivationdate
              FROM um_currentdistributedarchive cur INNER JOIN um_archive a
                   ON a.archiveid = cur.archiveid
                   INNER JOIN um_archivejob j ON j.archiveid = a.archiveid
                   INNER JOIN um_archivedistributiontask dt
                   ON dt.archivejobid = j.archivejobid
                   INNER JOIN um_typeofarchive t
                   ON a.typeofarchiveid = t.typeofarchiveid
                   INNER JOIN vario_deviceclass c
                   ON j.deviceclassid = c.deviceclassid;

-- CREATE VIEW VIEW_LOADINGSTATISTIC_ARCHIVE                
CREATE OR REPLACE FORCE VIEW view_loadingstatistic_archive (loadingstatisticsid,
                                                            deviceid,
                                                            deviceclassid,
                                                            archiveid,
                                                            loadedfrom,
                                                            loadeduntil,
                                                            typeofarchiveid,
                                                            filename,
                                                            filedate,
                                                            VERSION,
                                                            lsstate,
                                                            archiveactivationdate
                                                           )
AS
   SELECT um_loadingstatistics.loadingstatisticsid,
          um_loadingstatistics.deviceid, um_archive.deviceclassid,
          um_loadingstatistics.archiveid, um_loadingstatistics.loadedfrom,
          um_loadingstatistics.loadeduntil, um_archive.typeofarchiveid,
          um_archive.filename, um_archive.filedate, um_archive.VERSION,
          um_loadingstatistics.lsstate, um_archive.activationdate as archiveactivationdate
     FROM um_archive RIGHT OUTER JOIN um_loadingstatistics
          ON um_archive.archiveid = um_loadingstatistics.archiveid;

-- CREATE MATERIALIZED VIEW VIEW_UNIT_DEVICE_LOADINGSTAT
CREATE MATERIALIZED VIEW VIEW_UNIT_DEVICE_LOADINGSTAT ("CLIENTID", "UNITID", "UNITNO", "VISIBLEUNITNO", "UNITNAME", "UNITDESCRIPTION", "LICENSE", "LASTWLANCONNECTION", "LASTGPRSCONNECTION", "DEPOTID", "DEPOTNAME", "DEVICEID", "DEVICENO", "DEVICECLASSID", "DEVICECLASS", "DEVICENAME", "DEVICEDESCRIPTION", "MOUNTINGPLATE", "LASTDATA", "INVENTORYNUMBER", "EXTERNALDEVICENO", "ARCHIVEID", "FILENAME", "VERSION", "FILEDATE", "TYPENAME", "LOADINGSTATISTICSID", "LOADEDFROM", "LOADEDUNTIL", "STATEID", "LSSTATE", "ARCHIVEACTIVATIONDATE")
SEGMENT CREATION IMMEDIATE
NOCOMPRESS 
NOLOGGING
BUILD IMMEDIATE
USING INDEX 
REFRESH COMPLETE ON DEMAND START WITH sysdate+0 NEXT (sysdate+5/1440)
USING DEFAULT LOCAL ROLLBACK SEGMENT
USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
AS SELECT u.clientid, u.unitid, u.unitno, u.visibleunitno, u.unitname,
       u.description AS unitdescription, u.license, u.lastwlanconnection,
       u.lastgprsconnection, v.depotid, v.depotname, d.deviceid, d.deviceno,
       dc.deviceclassid, dc.shortname AS deviceclass, d.NAME AS devicename,
       d.description AS devicedescription, d.mountingplate, d.lastdata,
       d.inventorynumber, d.externaldeviceno, a.archiveid, a.filename,
       a.VERSION, a.filedate, t.typename, l.loadingstatisticsid, l.loadedfrom,
       l.loadeduntil, ls.stateid, ls.description AS lsstate, a.activationdate as archiveactivationdate
  FROM um_unit u,
       vario_depot v,
       um_device d,
       vario_deviceclass dc,
       um_loadingstatistics l,
       um_archive a,
       um_typeofarchive t,
       um_lsstate ls
 WHERE u.typeofunitid = 3
   AND u.unitstate = 1
   AND v.depotid = u.depotid
   AND d.unitid(+) = u.unitid
   AND dc.deviceclassid(+) = d.deviceclassid
   AND l.deviceid(+) = d.deviceid
   AND a.archiveid(+) = l.archiveid
   AND t.typeofarchiveid(+) = a.typeofarchiveid
   AND ls.stateid(+) = l.lsstate;

-- CREATE MATERIALIZED VIEW VIEW_UM_CURRELEASLOADINGSTAT   
CREATE MATERIALIZED VIEW VIEW_UM_CURRELEASLOADINGSTAT ("ARCHIVEID", "DEVICEID", "LSSTATE", "LOADEDFROM")
NOCACHE
NOLOGGING
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND START WITH sysdate+0 NEXT (sysdate+5/1440)
USING DEFAULT LOCAL ROLLBACK SEGMENT
USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
AS select distinct r.ARCHIVEID, l.deviceid as deviceid, l.lsstate, l.LOADEDFROM from view_loadingstat_overview l, VIEW_UM_CurReleasArchives r 
  where r.archiveid=l.archiveid;

-- CREATE MATERIALIZED VIEW VIEW_UM_CURRELEASES
CREATE MATERIALIZED VIEW VIEW_UM_CURRELEASES ("ARCHIVEID", "ARCHIVEJOBID", "ARCHIVEDISTRIBUTIONTASKID", "FILENAME", "TITLE", "CLIENTID", "ARCHIVETYPE", "DEVICECLASSID", "DEVICECLASSSHORT", "STARTED", "USEALL", "ARCHIVEACTIVATIONDATE","TARGETFTPSERVER", "LOADEDFTPSERVER", "TARGETUNITS", "TARGETDEVICES", "LOADEDDEVICES")
SEGMENT CREATION IMMEDIATE
NOCOMPRESS 
NOLOGGING
BUILD IMMEDIATE
USING INDEX 
REFRESH COMPLETE ON DEMAND START WITH sysdate+0 NEXT (sysdate+10/1440)
USING DEFAULT LOCAL ROLLBACK SEGMENT
USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
AS 
SELECT DISTINCT cur.archiveid, cur.archivejobid,
                cur.archivedistributiontaskid, cur.filename, cur.title,
                cur.clientid, cur.archivetype, cur.deviceclassid,
                cur.deviceclassshort, cur.started, cur.useall,cur.archiveactivationdate,
                td.targetftpserver, td.loadedftpserver, u.targetunits,
                CAST (NVL (cu.targetdevices, '0') AS NUMBER (8, 0)
                     ) AS targetdevices,
                CAST (NVL (cu.loadeddevices, '0') AS NUMBER (8, 0)
                     ) AS loadeddevices
           FROM view_um_curreleasarchives cur,
                (SELECT   archivedistributiontaskid,
                          CAST
                             (COUNT (ftpservername) AS NUMBER (8, 0)
                             ) AS targetftpserver,
                          CAST
                             (SUM (DECODE (lsstate,
                                           0, 1,
                                           1, 1,
                                           9, 1,
                                           10, 1,
                                           11, 1,
                                           0
                                          )
                                  ) AS NUMBER (8, 0)
                             ) AS loadedftpserver
                     FROM view_um_taskdistributedtoftp
                 GROUP BY archivedistributiontaskid) td,
                (SELECT   archivejobid,
                          CAST (COUNT (unitid) AS NUMBER (8, 0))
                                                               AS targetunits
                     FROM um_archivejobdistributedtounit
                 GROUP BY archivejobid) u,
                view_um_curreleascountdevices cu
          WHERE cur.archivedistributiontaskid = td.archivedistributiontaskid
            AND cur.archivejobid = u.archivejobid
            AND cur.archivedistributiontaskid = cu.archivedistributiontaskid(+);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;