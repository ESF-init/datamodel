SPOOL db_3_638.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.638', 'FLF', 'Add new field DEVICECLASSID and comment to SL_CREDITCARDTERMINAL');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CREDITCARDTERMINAL
 ADD (DEVICECLASSID  NUMBER(18));


COMMENT ON COLUMN SL_CREDITCARDTERMINAL.DEVICECLASSID IS 'Deviceclass/Saleschannel of the credit card terminal.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
