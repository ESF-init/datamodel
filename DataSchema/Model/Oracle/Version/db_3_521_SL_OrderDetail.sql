SPOOL db_3_521.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.521', 'DST', 'Fix SL_OrderDetail');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_ORDERDETAIL RENAME COLUMN CARDPRINTINGTYPEID TO CARDPRINTINGTYPE;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
