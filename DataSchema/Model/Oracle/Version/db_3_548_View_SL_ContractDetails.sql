SPOOL db_3_548.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.548', 'BVI', 'Modified VIEW_SL_CONTRACTDETAILS');

-- =======[ Changes to Existing Views ]===========================================================================
CREATE OR REPLACE VIEW VIEW_SL_CONTRACTDETAILS
AS
    SELECT *
      FROM (WITH
                validContractAddress
                AS
                    (SELECT *
                       FROM (SELECT FIRST_VALUE (cAddress_sub.AddressID)
                                        OVER (
                                            PARTITION BY cAddress_sub.ContractID
                                            ORDER BY a_sub.ValidFrom DESC)
                                        newestID,
                                    a_sub.*,
                                    cAddress_sub.ContractID
                               FROM sl_address  a_sub
                                    JOIN sl_contractaddress cAddress_sub
                                        ON a_sub.AddressID =
                                           cAddress_sub.AddressID
                              WHERE     (   a_sub.ValidFrom IS NULL
                                         OR a_sub.ValidFrom <= SYSDATE)
                                    AND (   a_sub.ValidUntil IS NULL
                                         OR a_sub.ValidUntil >= SYSDATE)
                                    AND (cAddress_sub.AddressType = 5))
                      WHERE newestID = AddressID --Ensure there is maximum one address for each contract
                                                ),
                newestBankConnectionData
                AS
                    (SELECT *
                       FROM (SELECT FIRST_VALUE (bcd.BankConnectionDataID)
                                        OVER (PARTITION BY po.ContractID
                                              ORDER BY bcd.CreationDate DESC)
                                        newestID,
                                    po.contractID
                                        contractID,
                                    bcd.*
                               FROM sl_paymentoption  po
                                    JOIN sl_bankconnectiondata bcd
                                        ON po.BankConnectionDataID =
                                           bcd.BankConnectionDataID
                              WHERE     (   bcd.ValidFrom IS NULL
                                         OR bcd.ValidFrom <= SYSDATE)
                                    AND (   bcd.ValidUntil IS NULL
                                         OR bcd.ValidUntil >= SYSDATE)
                                    AND po.PAYMENTMETHOD = 269)
                      WHERE BankConnectionDataID = newestID --Ensure there is maximum one BankConnectionData for each contract
                                                           ),
                newestInvoices
                AS
                    (SELECT *
                       FROM (SELECT FIRST_VALUE (i_sub.INVOICEID)
                                        OVER (PARTITION BY ContractID
                                              ORDER BY CreationDate DESC)
                                        newestID,
                                    i_sub.*
                               FROM sl_invoice i_sub
                              WHERE i_sub.InvoiceType = 6)
                      WHERE newestID = InvoiceID --Ensure there is maximum one Invoice for each contract
                                                ),
                customerAccount
                AS
                    (SELECT *
                       FROM (SELECT FIRST_VALUE (
                                        customerAccount.customerAccountID)
                                        OVER (
                                            PARTITION BY customerAccount.ContractID
                                            ORDER BY
                                                customerAccount.CustomerAccountID DESC)
                                        oldestID,
                                    customerAccount.*
                               FROM sl_customeraccount customerAccount)
                      WHERE customerAccountID = oldestID --Ensure there is maximum one CustomerAccount for each contract
                                                        ),
                newestComment
                AS
                    (SELECT commentSub.*,
                            commentPriority.Literal     CommentPriorityName
                       FROM (SELECT FIRST_VALUE (CommentID)
                                        OVER (PARTITION BY ContractID
                                              ORDER BY CreationDate DESC)
                                        newestID,
                                    sl_comment.*
                               FROM sl_comment) commentSub
                            LEFT JOIN sl_commentpriority commentPriority
                                ON commentSub.priority =
                                   commentPriority.EnumerationValue
                      WHERE newestID = CommentID --Ensure there is maximum one comment for each contract
                                                ),
                currentDunningData
                AS
                    (SELECT *
                       FROM (SELECT FIRST_VALUE (i_sub.InvoiceID)
                                        OVER (
                                            PARTITION BY dp_sub.ContractID
                                            ORDER BY i_sub.CreationDate DESC)
                                        newestID,
                                    i_sub.InvoiceID
                                        InvoiceID,
                                    dp_sub.ContractID,
                                    i_sub.CreationDate,
                                    i_sub.InvoiceNumber,
                                    dp_sub.PROCESSNUMBER,
                                    dlevel_sub.NAME
                               FROM sl_dunningprocess  dp_sub
                                    JOIN sl_dunningtoinvoice dti_sub
                                        ON dp_sub.DUNNINGPROCESSID =
                                           dti_sub.DUNNINGPROCESSID
                                    JOIN sl_invoice i_sub
                                        ON i_sub.invoiceid =
                                           dti_sub.invoiceid
                                    JOIN sl_dunninglevel dlevel_sub
                                        ON dti_sub.DUNNINGLEVELID =
                                           dlevel_sub.dunninglevelid
                              WHERE dp_sub.IsClosed = 0)
                      WHERE InvoiceID = newestID --Ensure there is maximum one dunning data for each contract
                                                ),
                productTermination
                AS
                    (SELECT productTerminationSub.*,
                            productTerminationType.Name
                                ProductTerminationTypeName
                       FROM (SELECT FIRST_VALUE (
                                        sl_productTermination.ProductTerminationId)
                                        OVER (
                                            PARTITION BY sl_productTermination.ContractId
                                            ORDER BY
                                                sl_productTermination.DemandDate DESC)
                                        newestID,
                                    sl_productTermination.*
                               FROM sl_productTermination)
                            productTerminationSub
                            JOIN
                            sl_ProductTerminationType productTerminationType
                                ON productTerminationSub.ProductTerminationTypeId =
                                   productTerminationType.ProductTerminationTypeId
                      WHERE ProductTerminationId = newestID --Ensure there is maximum one product termination for each contract
                                                           ),
                AccountBalanceToday
                AS
                    (  SELECT ContractID, SUM (postingamount) PostingSum
                         FROM sl_posting
                        WHERE TRUNC (valuedatetime) <= TRUNC (SYSDATE)
                     GROUP BY contractid),
                AccountBalanceEndOfLastMonth
                AS
                    (  SELECT ContractID, SUM (postingamount) PostingSum
                         FROM sl_posting
                        WHERE TRUNC (valuedatetime) < TRUNC (SYSDATE, 'MONTH')
                     GROUP BY contractid),
                AccountBalanceStartOfLastMonth
                AS
                    (  SELECT ContractID, SUM (postingamount) PostingSum
                         FROM sl_posting
                        WHERE TRUNC (valuedatetime) <
                              ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -1)
                     GROUP BY contractid),
                PostingSumCurrentMonth
                AS
                    (  SELECT ContractID, SUM (postingamount) PostingSum
                         FROM sl_posting
                        WHERE     TRUNC (valuedatetime) >=
                                  TRUNC (SYSDATE, 'MONTH')
                              AND TRUNC (valuedatetime) <
                                  ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), 1)
                     GROUP BY contractid),
                PostingSumLastMonth
                AS
                    (  SELECT ContractID, SUM (postingamount) PostingSum
                         FROM sl_posting
                        WHERE     TRUNC (valuedatetime) >=
                                  ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -1)
                              AND TRUNC (valuedatetime) <
                                  TRUNC (SYSDATE, 'MONTH')
                     GROUP BY contractid)
            SELECT contract.ContractID
                       ContractID,
                   contract.OrganizationID
                       OrganizationID,
                   contract.State
                       State,
                   contract.ContractNumber
                       ContractNumber,
                   contract.ValidFrom
                       ValidFrom,
                   contract.ValidTo
                       ValidTo,
                   contract.ClientID
                       ClientID,
                   contract.PaymentModalityID
                       PaymentModalityID,
                   contract.LastUser
                       LastUser,
                   contract.LastModified
                       LastModified,
                   contract.TransactionCounter
                       TransactionCounter,
                   contract.ClientAuthenticationToken
                       ClientAuthenticationToken,
                   contract.AccountingModeID
                       AccountingModeID,
                   contract.ContractType
                       ContractType,
                   contract.AdditionalText
                       AdditionalText,
                   (CASE
                        WHEN contract.OrganizationID > 0
                        THEN
                            p_OrganizationContactPerson.FirstName
                        ELSE
                            p_Contractor.FirstName
                    END)
                       AS PersonFirstName,
                   (CASE
                        WHEN contract.OrganizationID > 0
                        THEN
                            p_OrganizationContactPerson.MiddleName
                        ELSE
                            p_Contractor.MiddleName
                    END)
                       AS PersonMiddleName,
                   (CASE
                        WHEN contract.OrganizationID > 0
                        THEN
                            p_OrganizationContactPerson.LastName
                        ELSE
                            p_Contractor.LastName
                    END)
                       AS PersonLastName,
                   (CASE
                        WHEN contract.OrganizationID > 0
                        THEN
                            p_OrganizationContactPerson.DateOfBirth
                        ELSE
                            p_Contractor.DateOfBirth
                    END)
                       AS PersonDateOfBirth,
                   (CASE
                        WHEN contract.OrganizationID > 0
                        THEN
                            p_OrganizationContactPerson.Gender
                        ELSE
                            p_Contractor.Gender
                    END)
                       AS PersonGender,
                   (CASE
                        WHEN contract.OrganizationID > 0
                        THEN
                            p_OrganizationContactPerson.Salutation
                        ELSE
                            p_Contractor.Salutation
                    END)
                       AS PersonSalutation,
                   (CASE
                        WHEN contract.OrganizationID > 0
                        THEN
                            p_OrganizationContactPerson.Title
                        ELSE
                            p_Contractor.Title
                    END)
                       AS PersonTitle,
                   (CASE
                        WHEN contract.OrganizationID > 0
                        THEN
                            p_OrganizationContactPerson.PhoneNumber
                        ELSE
                            p_Contractor.PhoneNumber
                    END)
                       AS PersonPhoneNumber,
                   (CASE
                        WHEN contract.OrganizationID > 0
                        THEN
                            p_OrganizationContactPerson.PersonId
                        ELSE
                            p_Contractor.PersonId
                    END)
                       AS PersonPersonId,
                   (CASE
                        WHEN contract.OrganizationID > 0
                        THEN
                            p_OrganizationContactPerson.CellPhoneNumber
                        ELSE
                            p_Contractor.CellPhoneNumber
                    END)
                       AS PersonCellPhoneNumber,
                   (CASE
                        WHEN contract.OrganizationID > 0
                        THEN
                            p_OrganizationContactPerson.Email
                        ELSE
                            p_Contractor.Email
                    END)
                       AS PersonEmail,
                   (CASE
                        WHEN contract.OrganizationID > 0
                        THEN
                            p_OrganizationContactPerson.FaxNumber
                        ELSE
                            p_Contractor.FaxNumber
                    END)
                       AS PersonFaxNumber,
                   validContractAddress.ADDRESSID
                       ADDRESSID,
                   validContractAddress.Street
                       AddressStreet,
                   validContractAddress.POSTALCODE
                       AddressPostalCode,
                   validContractAddress.CITY
                       ADDRESSCity,
                   validContractAddress.Country
                       ADDRESSCountry,
                   validContractAddress.Region
                       ADDRESSRegion,
                   validContractAddress.ADDRESSEE
                       ADDRESSAddressee,
                   validContractAddress.ADDRESSFIELD1
                       ADDRESSAddressField1,
                   validContractAddress.ADDRESSFIELD2
                       ADDRESSAddressField2,
                   validContractAddress.STREETNUMBER
                       ADDRESSSTREETNUMBER,
                   organization.Name
                       OrganizationName,
                   organization.Abbreviation
                       OrganizationAbbreviation,
                   organization.Description
                       OrganizationDescription,
                   organization.OrganizationNumber
                       OrganizationNumber,
                   organization.ExternalNumber
                       OrganizationExternalNumber,
                   newestBankConnectionData.IBAN
                       BankConnectionDataIBAN,
                   newestBankConnectionData.BIC
                       BankConnectionDataBIC,
                   newestBankConnectionData.LASTUSAGE
                       BankConnectionDataLastUsage,
                   newestInvoices.INVOICENUMBER
                       LastInvoiceNumber,
                   newestInvoices.CREATIONDATE
                       LastInvoiceCreationDate,
                   newestComment.TEXT
                       CommentText,
                   newestComment.Priority
                       CommentPriorityNumber,
                   newestComment.CommentPriorityName,
                   paymentModality.DEVICEPAYMENTMETHOD
                       PaymentModalityPaymentMethodID,
                   devicePaymentMethod.Description
                       PaymentModalityPaymentMethod,
                   currentDunningData.CreationDate
                       DunningCreationDate,
                   currentDunningData.InvoiceNumber
                       DunningInvoiceNumber,
                   currentDunningData.ProcessNumber
                       DunningProcessNumber,
                   currentDunningData.Name
                       DunningLevelName,
                   productTermination.ProductId
                       ProductTerminationProductId,
                   productTermination.TerminationDate
                       ProductTerminationDate,
                   productTermination.DemandDate
                       ProductTerminationDemandDate,
                   productTermination.ProductTerminationTypeName,
                   NVL (accountBalanceToday.POSTINGSUM, 0)
                       AccountBalanceToday,
                   NVL (accountBalanceEndLastMonth.POSTINGSUM, 0)
                       AccountBalanceEndLastMonth,
                   NVL (accountBalanceStartLastMonth.POSTINGSUM, 0)
                       AccountBalanceStartLastMonth,
                   NVL (postingSumCurrentMonth.POSTINGSUM, 0)
                       PostingAmountSumCurrentMonth,
                   NVL (postingSumLastMonth.POSTINGSUM, 0)
                       PostingAmountSumLastMonth
              FROM sl_contract  contract
                   LEFT JOIN customerAccount
                       ON customerAccount.ContractID = contract.ContractID
                   LEFT JOIN sl_person p_Contractor
                       ON p_Contractor.PersonID = customerAccount.PersonID
                   LEFT JOIN sl_Organization organization
                       ON organization.OrganizationID =
                          contract.OrganizationID
                   LEFT JOIN sl_person p_OrganizationContactPerson
                       ON p_OrganizationContactPerson.PersonID =
                          organization.CONTACTPERSONID
                   LEFT JOIN validContractAddress
                       ON validContractAddress.ContractID =
                          contract.ContractID
                   LEFT JOIN newestBankConnectionData
                       ON newestBankConnectionData.ContractID =
                          contract.ContractID
                   LEFT JOIN newestInvoices
                       ON newestInvoices.contractid = contract.contractID
                   LEFT JOIN newestComment
                       ON newestComment.ContractID = contract.CONTRACTID
                   LEFT JOIN sl_paymentmodality paymentModality
                       ON contract.paymentmodalityid =
                          paymentModality.paymentmodalityid
                   LEFT JOIN rm_devicepaymentmethod devicePaymentMethod
                       ON paymentModality.DEVICEPAYMENTMETHOD =
                          devicePaymentMethod.DEVICEPAYMENTMETHODID
                   LEFT JOIN currentDunningData
                       ON currentDunningData.ContractID = contract.ContractID
                   LEFT JOIN productTermination
                       ON productTermination.ContractID = contract.ContractID
                   LEFT JOIN AccountBalanceToday accountBalanceToday
                       ON accountBalanceToday.ContractID =
                          contract.ContractID
                   LEFT JOIN
                   AccountBalanceEndOfLastMonth accountBalanceEndLastMonth
                       ON accountBalanceEndLastMonth.ContractID =
                          contract.ContractID
                   LEFT JOIN
                   AccountBalanceStartOfLastMonth
                   accountBalanceStartLastMonth
                       ON accountBalanceStartLastMonth.ContractID =
                          contract.ContractID
                   LEFT JOIN PostingSumCurrentMonth postingSumCurrentMonth
                       ON postingSumCurrentMonth.ContractID =
                          contract.ContractID
                   LEFT JOIN PostingSumLastMonth postingSumLastMonth
                       ON postingSumLastMonth.ContractID =
                          contract.ContractID);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;