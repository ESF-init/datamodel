SPOOL db_3_399.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.399', 'ULB', 'Increased columnsize AM_COMPONENT.COMPONENTNO');


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE AM_COMPONENT MODIFY(COMPONENTNO NUMBER(14));


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
