SPOOL db_3_356.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.356', 'HNI', 'Added new columns to SL_BookingItems');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
COMMENT ON COLUMN SL_BOOKINGITEm.PURPOSEREFERENCE IS 'NOT USED';

ALTER TABLE SL_BOOKINGITEM
 ADD (PRODUCTNAME  NVARCHAR2(50));

 COMMENT ON COLUMN SL_BOOKINGITEM.PRODUCTNAME IS 'Name of the product, service or ticket used in this RegioMove trip.';

ALTER TABLE SL_BOOKINGITEM
 ADD (PRODUCTID  NVARCHAR2(50));

 COMMENT ON COLUMN SL_BOOKINGITEM.PRODUCTID IS 'External identifier of the product, service or ticket used in this RegioMove';

ALTER TABLE SL_BOOKINGITEM
 ADD (PAYMENTREFERENCE  NVARCHAR2(50));

 COMMENT ON COLUMN SL_BOOKINGITEM.PAYMENTREFERENCE IS 'External identifier of a Logapay payment process.';
---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
