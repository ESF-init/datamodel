SPOOL db_3_152.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.152', 'STV', 'Creation of SL_EntitlementType, SL_Entitlement');

CREATE TABLE SL_EntitlementType
(
    EntitlementTypeID                           NUMBER(18,0)            NOT NULL,
	Name	                                 	NVARCHAR2(100)          NOT NULL,    	
	Description                                 NVARCHAR2(250)          NULL,
	ApplicationScope							NVARCHAR2(100)			NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL	 
);

COMMENT ON TABLE SL_EntitlementType is 'Types of entitlements';
COMMENT ON COLUMN SL_EntitlementType.Name is 'Unique identtifier for entitlement types';
COMMENT ON COLUMN SL_EntitlementType.Description is 'Description for entitlement types';
COMMENT ON COLUMN SL_EntitlementType.ApplicationScope is 'Scope of where the entitlement is used';

CREATE TABLE SL_Entitlement
(
    EntitlementID                               NUMBER(18,0)            NOT NULL,
	PersonID	                                NUMBER(18,0)            NOT NULL,	
	EntitlementTypeID                           NUMBER(18,0)            NOT NULL,	
	ValidFrom									DATE,
	ValidTo										DATE,
	Document									NVARCHAR2(250)          NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL
);

COMMENT ON TABLE SL_Entitlement is 'The actuel entitlements';
COMMENT ON COLUMN SL_Entitlement.PersonID is 'REFERENCES the table SL_PERSON';
COMMENT ON COLUMN SL_Entitlement.EntitlementTypeID is 'REFERENCES the table SL_EntitlementType';

CREATE TABLE SL_EntitlementToProduct
(
	EntitlementID                               NUMBER(18,0)            NOT NULL,
	ProductID									NUMBER(18,0)            NOT NULL
);

COMMENT ON TABLE SL_Entitlement is 'Mapping for the entilements and products';
   
alter table SL_EntitlementType add CONSTRAINT PK_EntitlementType PRIMARY KEY (EntitlementTypeID);

alter table SL_Entitlement add CONSTRAINT PK_Entitlement PRIMARY KEY (EntitlementID);
alter table SL_Entitlement add CONSTRAINT FK_Entitlement_EntitlementType FOREIGN KEY (EntitlementTypeID) REFERENCES SL_EntitlementType (EntitlementTypeID);
alter table SL_Entitlement add CONSTRAINT FK_Entitlement_Person FOREIGN KEY (PersonID) REFERENCES SL_PERSON (PersonID);

ALTER TABLE SL_EntitlementToProduct ADD (
  CONSTRAINT PK_EntitlementToProduct
  PRIMARY KEY (ProductID,EntitlementID));

alter table SL_EntitlementToProduct add CONSTRAINT FK_EntitlementToProduct FOREIGN KEY (ProductID) REFERENCES SL_Product (ProductID);
alter table SL_EntitlementToProduct add CONSTRAINT FK_ProductToEntitlement FOREIGN KEY (EntitlementID) REFERENCES SL_Entitlement (EntitlementID);

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_Entitlement', 'SL_EntitlementType');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ MODIFY existing tables ]==============================================================================================


-- =======[ Trigger ]==============================================================================================
CREATE OR REPLACE TRIGGER SL_Entitlement_BRI before insert on SL_Entitlement for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_Entitlement_BRU before update on SL_Entitlement for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
