SPOOL db_3_223.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.223', 'TIR', 'Creating view VIEW_IC_INSTALLED_VERSION.');

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW VIEW_IC_INSTALLED_VERSION ("DEVICENO", "DEVICECLASSID", "UNITNO", "FILENAME", "TYPENAME", "LOADEDFROM", "STATUS", "VERSION", "DATUM", "TYPEOFARCHIVEID", "CLIENTID", "LOADINGSTATE") AS 
  SELECT d.deviceno, d.deviceclassid, u.unitno, a.filename, t.typename,
          l.loadedfrom, 'Pending' AS status,
          TO_NUMBER (SUBSTR (REGEXP_SUBSTR (a.filename, 'V\d{1,5}'), 2)
                    ) AS VERSION,
          TO_DATE (SUBSTR (REGEXP_SUBSTR (a.filename, 'F\d{8}'), 2),
                   'YYYYMMDD'
                  ) AS datum,
          t.typeofarchiveid, u.clientid, 1 AS loadingstate
     FROM um_device d INNER JOIN um_unit u
          ON u.unitid = d.unitid AND u.typeofunitid = 3
          INNER JOIN um_currentdistributedarchive c ON d.unitid = c.unitid
          INNER JOIN um_archive a
          ON a.archiveid = c.archiveid AND a.deviceclassid = d.deviceclassid
          INNER JOIN um_typeofarchive t ON a.typeofarchiveid =
                                                             t.typeofarchiveid
          LEFT OUTER JOIN um_loadingstatistics l
          ON c.archiveid = l.archiveid AND d.deviceid = l.deviceid
    WHERE l.loadingstatisticsid IS NULL OR l.lsstate < 0
   UNION
   SELECT d.deviceno, d.deviceclassid, u.unitno, a.filename, t.typename,
          l.loadedfrom AS "loaded from", 'Loaded' AS status,
          TO_NUMBER (SUBSTR (REGEXP_SUBSTR (a.filename, 'V\d{1,5}'), 2)
                    ) AS VERSION,
          TO_DATE (SUBSTR (REGEXP_SUBSTR (a.filename, 'F\d{8}'), 2),
                   'YYYYMMDD'
                  ) AS datum,
          t.typeofarchiveid, u.clientid, l.lsstate AS loadingstate
     FROM um_device d INNER JOIN um_unit u
          ON u.unitid = d.unitid AND u.typeofunitid = 3
          INNER JOIN um_loadingstatistics l ON d.deviceid = l.deviceid
          INNER JOIN um_archive a
          ON l.archiveid = a.archiveid AND a.deviceclassid = d.deviceclassid
          INNER JOIN um_typeofarchive t ON a.typeofarchiveid =
                                                             t.typeofarchiveid
    WHERE (l.loadeduntil IS NULL AND l.loadedfrom >= d.lastdata)
       OR (l.loadeduntil >= d.lastdata);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;