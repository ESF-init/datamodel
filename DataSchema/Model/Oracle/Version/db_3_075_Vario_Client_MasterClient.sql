SPOOL db_3_075.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.075', 'BVI', 'Added MasterClientID to Vario_Client.');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

alter table Vario_Client add MasterClientID number(10);

COMMENT ON COLUMN Vario_Client.MasterClientID IS 'Defining a master client for a sub client.';

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Sequence ]---------------------------------------------------------------------------------------------

-- =======[ Trigger ]==============================================================================================

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
