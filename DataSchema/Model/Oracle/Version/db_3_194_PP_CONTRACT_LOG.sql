SPOOL db_3_194.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.194', 'JGI', 'Added new table PP_CONTRACT_LOG');

-- =======[ Changes to Existing Tables ]===========================================================================
CREATE TABLE PP_CONTRACT_LOG
(
  LOGID     NUMBER(10) NOT NULL,
  CLIENTID  NUMBER(10),
  LOGDATE   DATE  DEFAULT sysdate,
  LOGTEXT   VARCHAR2(1000)
);

COMMENT ON COLUMN PP_CONTRACT_LOG.LOGID IS 'Primary key for table PP_CONTRACT_LOG';
COMMENT ON COLUMN PP_CONTRACT_LOG.CLIENTID IS 'Id for tennant';
COMMENT ON COLUMN PP_CONTRACT_LOG.LOGDATE IS 'Date and time for the log entry';
COMMENT ON COLUMN PP_CONTRACT_LOG.LOGTEXT IS 'Text of the log message';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
