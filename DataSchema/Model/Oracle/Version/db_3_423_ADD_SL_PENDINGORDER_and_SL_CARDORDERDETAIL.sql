SPOOL db_3_423.log;
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.423', 'EMN', 'ADD SL_PENDINGORDER_and_SL_CARDORDERDETAIL');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_PENDINGORDER
(
    PENDINGORDERID              NUMBER(18),
    ORDERID         	        NUMBER(18),
    CONTRACTID                  NUMBER(18),
    SESSIONID           	    NVARCHAR2(100),
    ISPROCESSED                 NUMBER(1),
    PAYMENTSUCCESS              NUMBER(1),
    ORDERCOMPLETE              	NUMBER(1),
    ERRORMEASSGE                NVARCHAR2(100),
    LASTUSER                	NVARCHAR2(50)        DEFAULT 'SYS',
    LASTMODIFIED            	DATE                 DEFAULT sysdate,
    TRANSACTIONCOUNTER      	INTEGER,
    DATAROWCREATIONDATE     	DATE                 DEFAULT sysdate
);

COMMENT ON TABLE  SL_PENDINGORDER IS 'The table holds pending order data related to payments and order completion.';
COMMENT ON COLUMN SL_PENDINGORDER.PENDINGORDERID IS 'Id of the pending order information.';
COMMENT ON COLUMN SL_PENDINGORDER.ORDERID IS 'Id of the parent order.';
COMMENT ON COLUMN SL_PENDINGORDER.CONTRACTID IS 'Id of the customer contract.';
COMMENT ON COLUMN SL_PENDINGORDER.SESSIONID IS 'Id of the payment session created by paymark.';
COMMENT ON COLUMN SL_PENDINGORDER.ISPROCESSED IS 'Indicates if the pending order has been processed by the order processing service.';
COMMENT ON COLUMN SL_PENDINGORDER.PAYMENTSUCCESS IS 'Indicates if the payment was executed successfully.';
COMMENT ON COLUMN SL_PENDINGORDER.ORDERCOMPLETE IS 'Indicates if pending order was completed successfully.';
COMMENT ON COLUMN SL_PENDINGORDER.ERRORMEASSGE IS 'Error message if the pending order failed in the order processing service.';
COMMENT ON COLUMN SL_PENDINGORDER.LASTUSER IS 'Technical field: last (system) user that changed this dataset.';
COMMENT ON COLUMN SL_PENDINGORDER.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset.';
COMMENT ON COLUMN SL_PENDINGORDER.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset.';
COMMENT ON COLUMN SL_PENDINGORDER.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

CREATE UNIQUE INDEX PK_PENDINGORDER ON SL_PENDINGORDER(PENDINGORDERID);
CREATE UNIQUE INDEX UK_PENDINGORDER ON SL_PENDINGORDER(ORDERID);

ALTER TABLE SL_PENDINGORDER ADD CONSTRAINT PK_PENDINGORDER PRIMARY KEY (PENDINGORDERID);

CREATE TABLE SL_CARDORDERDETAIL
(
    CARDORDERDETAILID          NUMBER(18),
    PENDINGORDERID              NUMBER(18),
    CARDHOLDERID                NUMBER(18),
    ORDERDETAILID               NUMBER(18),
    REPLACEDCARDID              NUMBER(18)           DEFAULT NULL,
    CLIENTID                    NUMBER(18)           DEFAULT NULL,
    LASTUSER                	NVARCHAR2(50)        DEFAULT 'SYS',
    LASTMODIFIED            	DATE                 DEFAULT sysdate,
    TRANSACTIONCOUNTER      	INTEGER,
    DATAROWCREATIONDATE     	DATE                 DEFAULT sysdate
);

CREATE UNIQUE INDEX PK_CARDORDERDETAIL ON SL_CARDORDERDETAIL(CARDORDERDETAILID);

ALTER TABLE SL_CARDORDERDETAIL ADD CONSTRAINT PK_CARDORDERDETAIL PRIMARY KEY (CARDORDERDETAILID);
ALTER TABLE SL_CARDORDERDETAIL ADD CONSTRAINT FK_CARDORDERDETAIL_PENDORDER FOREIGN KEY (PENDINGORDERID) REFERENCES SL_PENDINGORDER (PENDINGORDERID) ON DELETE CASCADE;

-- -------[ Triggers ]-----------------------------------------------------------------------------------------
CREATE OR REPLACE TRIGGER SL_PENDINGORDER_BRI before insert on SL_PENDINGORDER for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_PENDINGORDER_BRU before update on SL_PENDINGORDER for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_CARDORDERDETAIL_BRI before insert on SL_CARDORDERDETAIL for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_CARDORDERDETAIL_BRU before update on SL_CARDORDERDETAIL for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Sequences ]============================================================================================
CREATE SEQUENCE SL_PENDINGORDER_SEQ START WITH 1
                                                  MAXVALUE 9999999999999999999999999999
                                                  MINVALUE 1
                                                  NOCYCLE
                                                  CACHE 20
                                                  NOORDER;
                                                  
CREATE SEQUENCE SL_CARDORDERDETAIL_SEQ START WITH 1
                                                  MAXVALUE 9999999999999999999999999999
                                                  MINVALUE 1
                                                  NOCYCLE
                                                  CACHE 20
                                                  NOORDER;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
