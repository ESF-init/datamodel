SPOOL db_3_481.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.481', 'SVR', 'Alter table SL_CONFIGURATIONDEFINITION, add foreign key to VARIO_CLIENT.CLIENTID.');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table SL_CONFIGURATIONDEFINITION add constraint FK_CONFDEFINITION_CLIENT foreign key (OWNERCLIENTID) references VARIO_CLIENT (CLIENTID);

COMMENT ON COLUMN SL_CONFIGURATIONDEFINITION.OWNERCLIENTID is 'Client value can only be changed by a user with the same clientId (ownerClientId == clientId) or if ownerClientId is 0 (default value).References VARIO_CLIENT.CLIENTID.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
