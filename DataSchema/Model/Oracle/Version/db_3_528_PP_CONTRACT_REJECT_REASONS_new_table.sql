SPOOL db_3_528.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.528', 'ARH', 'New table PP_CONTRACT_REJECT_REASONS');

-- Start adding schema changes here
-- =======[ New Tables ]===========================================================================================

CREATE TABLE PP_CONTRACT_REJECT_REASONS
(
	CONTRACTREJECTREASONID  NUMBER(10)            NOT NULL,
	DESCRIPTION             VARCHAR2(50),
	VISIBLE                 NUMBER(1)             DEFAULT 1
);

COMMENT ON TABLE PP_CONTRACT_REJECT_REASONS IS 'The table used to store the reject reason.';
COMMENT ON COLUMN PP_CONTRACT_REJECT_REASONS.CONTRACTREJECTREASONID IS 'Primary key.';
COMMENT ON COLUMN PP_CONTRACT_REJECT_REASONS.DESCRIPTION IS 'Description';
COMMENT ON COLUMN PP_CONTRACT_REJECT_REASONS.VISIBLE IS 'Visible ?';

ALTER TABLE PP_CONTRACT_REJECT_REASONS ADD (CONSTRAINT PP_CONTRACT_REJECT_REASONS_PK PRIMARY KEY (CONTRACTREJECTREASONID) ENABLE VALIDATE);

ALTER TABLE PP_CONTRACT ADD (REJECTREASONID  NUMBER(10));
ALTER TABLE PP_CONTRACT ADD (ONLINECREATIONDATE  DATE);
COMMENT ON COLUMN PP_CONTRACT.REJECTREASONID IS 'Links with PP_CONTRACT_REJECT_REASONS.';
COMMENT ON COLUMN PP_CONTRACT.ONLINECREATIONDATE IS 'Creation date for online contract.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
