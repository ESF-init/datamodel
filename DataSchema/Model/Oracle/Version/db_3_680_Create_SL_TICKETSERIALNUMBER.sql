SPOOL db_3_680.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.680', 'DHN', 'Add SL_TICKETSERIALNUMBER and SL_TICKETSERIALNUMBERSTATUS');

CREATE TABLE SL_TICKETSERIALNUMBER
(
  TICKETSERIALNUMBERID  NUMBER(18)                          NOT NULL,
  SERIALNUMBER          NUMBER(18)                          NOT NULL,
  TICKETTYPEID 			NUMBER(18)                          NULL,
  TICKETID   			NUMBER(10)                          NULL,  
  STATUS                NUMBER(9)                           NOT NULL,
  SOLDBY                NVARCHAR2(50)                       NULL,
  SOLDDATE              DATE                                NULL,
  PRODUCTID             NUMBER(18)                          NULL,
  LASTUSER				NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
  LASTMODIFIED			DATE			DEFAULT sysdate		NOT NULL,
  TRANSACTIONCOUNTER 	INTEGER								NOT NULL
);

ALTER TABLE SL_TICKETSERIALNUMBER ADD CONSTRAINT PK_TICKETSERIALNUMBER PRIMARY KEY (TICKETSERIALNUMBERID);
ALTER TABLE SL_TICKETSERIALNUMBER ADD CONSTRAINT FK_TICKETSERIALNUMBER_TTID FOREIGN KEY (TICKETTYPEID) REFERENCES TM_TICKETTYPE (TICKETTYPEID);
ALTER TABLE SL_TICKETSERIALNUMBER ADD CONSTRAINT FK_TICKETSERIALNUMBER_TICKETID FOREIGN KEY (TICKETID) REFERENCES TM_TICKET (TICKETID);
ALTER TABLE SL_TICKETSERIALNUMBER ADD CONSTRAINT FK_TICKETSERIALNUMBER_PID FOREIGN KEY (PRODUCTID) REFERENCES SL_PRODUCT (PRODUCTID);


COMMENT ON TABLE SL_TICKETSERIALNUMBER IS 'Represents numbers used for serialized tickets (SL_TICKETSERIALNUMBER).';
COMMENT ON COLUMN SL_TICKETSERIALNUMBER.TICKETSERIALNUMBERID IS 'Unique ID of TICKETSERIALNUMBER';
COMMENT ON COLUMN SL_TICKETSERIALNUMBER.SERIALNUMBER IS 'The number of a serialized ticket';
COMMENT ON COLUMN SL_TICKETSERIALNUMBER.TICKETTYPEID IS 'The ticket type assigned to the number';
COMMENT ON COLUMN SL_TICKETSERIALNUMBER.TICKETID IS 'The ticket assigned to the number';
COMMENT ON COLUMN SL_TICKETSERIALNUMBER.STATUS IS 'Status of the serial number: a value from table SL_TICKETSERIALNUMBERSTATUS';
COMMENT ON COLUMN SL_TICKETSERIALNUMBER.SOLDBY IS 'The user that sold the serialized product';
COMMENT ON COLUMN SL_TICKETSERIALNUMBER.SOLDDATE IS 'The date of the selling';
COMMENT ON COLUMN SL_TICKETSERIALNUMBER.PRODUCTID IS 'The ID of the product sold with the serial number';
COMMENT ON COLUMN SL_TICKETSERIALNUMBER.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_TICKETSERIALNUMBER.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_TICKETSERIALNUMBER.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';


CREATE TABLE SL_TICKETSERIALNUMBERSTATUS
(
    ENUMERATIONVALUE NUMBER(9)     not null
        constraint PK_TICKETSERIALNUMBERSTATUS
            primary key,
    LITERAL          NVARCHAR2(50) not null,
    DESCRIPTION      NVARCHAR2(50) not null
);
COMMENT ON TABLE SL_TICKETSERIALNUMBERSTATUS is 'Enumeration of ticket serial number status, known to the system.';
COMMENT ON COLUMN SL_TICKETSERIALNUMBERSTATUS.ENUMERATIONVALUE is 'Unique ID of the status type';
COMMENT ON COLUMN SL_TICKETSERIALNUMBERSTATUS.LITERAL is 'Name of the status type; used internally.';
COMMENT ON COLUMN SL_TICKETSERIALNUMBERSTATUS.DESCRIPTION is 'Descripton of the status type. This is used in the UI to display the value.';

---- =======[ Functions and Data Types ]=============================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_TICKETSERIALNUMBERSTATUS (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (0, 'Inactive', 'Ticket serial number is inactive');
INSERT INTO SL_TICKETSERIALNUMBERSTATUS (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (1, 'Active', 'Ticket serial number is active');
INSERT INTO SL_TICKETSERIALNUMBERSTATUS (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (2, 'Sold', 'Ticket serial number is sold');
INSERT INTO SL_TICKETSERIALNUMBERSTATUS (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (3, 'Expired', 'Ticket serial number is expired');

-- =======[ Sequences ]============================================================================================
create sequence SL_TICKETSERIALNUMBER_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================
CREATE OR REPLACE TRIGGER SL_TICKETSERIALNUMBER_BRI before insert on SL_TICKETSERIALNUMBER for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_TICKETSERIALNUMBER_BRU before update on SL_TICKETSERIALNUMBER for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/


COMMIT;

PROMPT Done!

SPOOL OFF;
