SPOOL db_3_067.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.067', 'SLR', 'added columns Block* SL_PAYMENTOPTION; added views');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PAYMENTOPTION
ADD (BLOCKINGCOMMENT NVARCHAR2(250 ));

ALTER TABLE SL_PAYMENTOPTION
ADD (BLOCKINGREASON NUMBER(9) DEFAULT 0 NOT NULL);

ALTER TABLE SL_PAYMENTOPTION
ADD (BLOCKINGDATE DATE);

COMMENT ON COLUMN SL_PAYMENTOPTION.BLOCKINGCOMMENT IS 'Optional comment for blocking.';
COMMENT ON COLUMN SL_PAYMENTOPTION.BLOCKINGREASON IS 'Blocking reason.';
COMMENT ON COLUMN SL_PAYMENTOPTION.BLOCKINGDATE IS 'Date of blocking.';


ALTER TABLE SL_RULEVIOLATION
  ADD (PAYMENTTOKEN NVARCHAR2(50));

COMMENT ON COLUMN SL_RULEVIOLATION.PAYMENTTOKEN IS 'Payment token of related rule violation.';


ALTER TABLE SL_CARDTORULEVIOLATION
ADD (AGGREGATIONFROM DATE DEFAULT SYSDATE NOT NULL);

ALTER TABLE SL_CARDTORULEVIOLATION
ADD (AGGREGATIONTO DATE DEFAULT SYSDATE NOT NULL);

ALTER TABLE SL_CARDTORULEVIOLATION
  DROP CONSTRAINT FK_CARDTORULEVIO_RULEVIOLATION;


ALTER TABLE SL_CARDTORULEVIOLATION
Modify RULEVIOLATIONID NUMBER(18) NULL;

ALTER TABLE SL_CARDTORULEVIOLATION
  ADD (RULEVIOLATIONCOUNTER NUMBER(18) DEFAULT 0 NOT NULL);
  
COMMENT ON COLUMN SL_CARDTORULEVIOLATION.AGGREGATIONFROM IS 'Beginning aggregation date.';
COMMENT ON COLUMN SL_CARDTORULEVIOLATION.AGGREGATIONTO IS 'Ending aggregation date.';
COMMENT ON COLUMN SL_CARDTORULEVIOLATION.RULEVIOLATIONCOUNTER IS 'Total count of rule violations found for card within time period.';
COMMENT ON COLUMN SL_CARDTORULEVIOLATION.RULEVIOLATIONID IS 'Obsolet column.';

-- =======[ New Tables ]===========================================================================================


-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================


CREATE OR REPLACE FORCE VIEW view_ruleviolation_details (ruleviolationid,
                                                             ruleviolationtype,
                                                             violationvalue,
                                                             MESSAGE,
                                                             executiontime,
                                                             ruleviolationtext,
                                                             ruleviolationprovider,
                                                             isresolved,
                                                             aggregationfrom,
                                                             aggregationto,
                                                             ruleviolationsourcetype,
                                                             cardid,
                                                             serialnumber,
                                                             printednumber,
                                                             cardstate,
                                                             cardlastused,
                                                             cardexpiration,
                                                             cardisblocked,
                                                             cardblockingreason,
                                                             cardblockingcomment,
                                                             cardtype,
                                                             faremediaid,
                                                             productid,
                                                             ticketid,
                                                             tickettype,
                                                             validfrom,
                                                             validto,
                                                             productiscanceled,
                                                             price,
                                                             productisblocked,
                                                             productblockingreason,
                                                             productblockingdate,
                                                             productcancellationdate,
                                                             paymentoptionid,
                                                             contractid,
                                                             paymentmethod,
                                                             paymentoptiondesc,
                                                             bankingservicetoken,
                                                             paymentoptionexpiry,
                                                             paymentoptioncreditcardnet,
                                                             paymentoptionpriority,
                                                             paymentoptionname,
                                                             istemporary,
                                                             paymentoptionisretired,
                                                             bankconnectiondataid,
                                                             prospectivepaymentoptionid,
                                                             paymentoptionisblocked
                                                            )
AS
   SELECT sl_ruleviolation."RULEVIOLATIONID",
          sl_ruleviolation."RULEVIOLATIONTYPE",
          sl_ruleviolation."VIOLATIONVALUE", sl_ruleviolation."MESSAGE",
          sl_ruleviolation."EXECUTIONTIME",
          sl_ruleviolation."TEXT" AS ruleviolationtext,
          sl_ruleviolation."RULEVIOLATIONPROVIDER",
          sl_ruleviolation."ISRESOLVED", sl_ruleviolation."AGGREGATIONFROM",
          sl_ruleviolation."AGGREGATIONTO",
          sl_ruleviolation."RULEVIOLATIONSOURCETYPE", sl_card."CARDID",
          sl_card."SERIALNUMBER", sl_card."PRINTEDNUMBER",
          sl_card."STATE" AS cardstate, sl_card."LASTUSED" AS cardlastused,
          sl_card."EXPIRATION" AS cardexpiration,
          sl_card."BLOCKED" AS cardisblocked,
          sl_card."BLOCKINGREASON" AS cardblockingreason,
          sl_card."BLOCKINGCOMMENT" AS cardblockingcomment,
          sl_card."CARDTYPE", sl_card."FAREMEDIAID", sl_product."PRODUCTID",
          sl_product."TICKETID", sl_product."TICKETTYPE",
          sl_product."VALIDFROM", sl_product."VALIDTO",
          sl_product."ISCANCELED" AS productiscanceled, sl_product."PRICE",
          sl_product."ISBLOCKED" AS productisblocked,
          sl_product."BLOCKINGREASON" AS productblockingreason,
          sl_product."BLOCKINGDATE" AS productblockingdate,
          sl_product."CANCELLATIONDATE" AS productcancellationdate,
          sl_paymentoption."PAYMENTOPTIONID", sl_paymentoption."CONTRACTID",
          sl_paymentoption."PAYMENTMETHOD",
          sl_paymentoption."DESCRIPTION" AS paymentoptiondesc,
          sl_paymentoption."BANKINGSERVICETOKEN",
          sl_paymentoption."EXPIRY" AS paymentoptionexpiry,
          sl_paymentoption."CREDITCARDNETWORK" AS paymentoptioncreditcardnet,
          sl_paymentoption."PRIORITY" AS paymentoptionpriority,
          sl_paymentoption."NAME" AS paymentoptionname,
          sl_paymentoption."ISTEMPORARY",
          sl_paymentoption."ISRETIRED" AS paymentoptionisretired,
          sl_paymentoption."BANKCONNECTIONDATAID",
          sl_paymentoption."PROSPECTIVEPAYMENTOPTIONID",
          sl_paymentoption."ISBLOCKED" AS paymentoptionisblocked
     FROM sl_ruleviolation LEFT OUTER JOIN sl_card
          ON sl_card.cardid = sl_ruleviolation.cardid
          LEFT OUTER JOIN sl_product
          ON sl_product.productid = sl_ruleviolation.productid
          LEFT OUTER JOIN sl_paymentoption
          ON sl_paymentoption.paymentoptionid =
                                              sl_ruleviolation.paymentoptionid
          ;


CREATE OR REPLACE FORCE VIEW view_sl_paymentjournal (paymentjournalid,
                                                         paymjourntransjourn,
                                                         paymentjournalsaleid,
                                                         paymenttype,
                                                         paymentjournalcreditcardnet,
                                                         amount,
                                                         code,
                                                         confirmed,
                                                         executiontime,
                                                         executionresult,
                                                         state,
                                                         token,
                                                         ispretax,
                                                         referencenumber,
                                                         paymentoptionid,
                                                         maskedpan,
                                                         providerreference,
                                                         reconciliationstate,
                                                         paymentoptioncontractid,
                                                         paymentmethod,
                                                         paymentoptiondesc,
                                                         bankingservicetoken,
                                                         paymentoptionexpiry,
                                                         paymentoptioncreditcardnet,
                                                         paymentoptionpriority,
                                                         paymentoptionname,
                                                         istemporary,
                                                         paymentoptionisretired,
                                                         bankconnectiondataid,
                                                         prospectivepaymentoptionid,
                                                         paymentoptionisblocked,
                                                         transactionjournalid,
                                                         transactionid,
                                                         devicetime,
                                                         boardingtime,
                                                         operatorid,
                                                         transactionjournalclientid,
                                                         line,
                                                         coursenumber,
                                                         routenumber,
                                                         routecode,
                                                         direction,
                                                         ZONE,
                                                         transactionjournalsaleschannel,
                                                         devicenumber,
                                                         vehiclenumber,
                                                         mountingplatenumber,
                                                         debtornumber,
                                                         geolocationlongitude,
                                                         geolocationlatitude,
                                                         stopnumber,
                                                         transitaccountid,
                                                         faremediaid,
                                                         faremediatype,
                                                         productid,
                                                         ticketid,
                                                         fareamount,
                                                         customergroup,
                                                         transactiontype,
                                                         transactionnumber,
                                                         resulttype,
                                                         filllevel,
                                                         pursebalance,
                                                         pursecredit,
                                                         groupsize,
                                                         validfrom,
                                                         validto,
                                                         DURATION,
                                                         tariffdate,
                                                         tariffversion,
                                                         ticketinternalnumber,
                                                         whitelistversion,
                                                         cancellationreference,
                                                         cancellationreferenceguid,
                                                         whitelistversioncreated,
                                                         properties,
                                                         transactionjournalsaleid,
                                                         tripticketinternalnumber,
                                                         shiftbegin,
                                                         completionstate,
                                                         responsestate,
                                                         productid2,
                                                         pursebalance2,
                                                         pursecredit2,
                                                         creditcardauthorizationid,
                                                         tripcounter,
                                                         inserttime,
                                                         saletransactionid,
                                                         saletype,
                                                         saledate,
                                                         saleclientid,
                                                         salesaleschannel,
                                                         locationnumber,
                                                         saledevicenumber,
                                                         salespersonnumber,
                                                         merchantnumber,
                                                         receiptreference,
                                                         networkreference,
                                                         cancellationreferenceid,
                                                         isclosed,
                                                         orderid,
                                                         externalordernumber,
                                                         salecontractid,
                                                         refundreferenceid,
                                                         isorganizational
                                                        )
AS
   SELECT sl_paymentjournal."PAYMENTJOURNALID",
          sl_paymentjournal."TRANSACTIONJOURNALID" AS paymjourntransjourn,
          sl_paymentjournal."SALEID" AS paymentjournalsaleid,
          sl_paymentjournal."PAYMENTTYPE",
          sl_paymentjournal."CREDITCARDNETWORK"
                                               AS paymentjournalcreditcardnet,
          sl_paymentjournal."AMOUNT", sl_paymentjournal."CODE",
          sl_paymentjournal."CONFIRMED", sl_paymentjournal."EXECUTIONTIME",
          sl_paymentjournal."EXECUTIONRESULT", sl_paymentjournal."STATE",
          sl_paymentjournal."TOKEN", sl_paymentjournal."ISPRETAX",
          sl_paymentjournal."REFERENCENUMBER",
          sl_paymentjournal."PAYMENTOPTIONID", sl_paymentjournal."MASKEDPAN",
          sl_paymentjournal."PROVIDERREFERENCE",
          sl_paymentjournal."RECONCILIATIONSTATE",
          sl_paymentoption."CONTRACTID" AS paymentoptioncontractid,
          sl_paymentoption."PAYMENTMETHOD",
          sl_paymentoption."DESCRIPTION" AS paymentoptiondesc,
          sl_paymentoption."BANKINGSERVICETOKEN",
          sl_paymentoption."EXPIRY" AS paymentoptionexpiry,
          sl_paymentoption."CREDITCARDNETWORK" AS paymentoptioncreditcardnet,
          sl_paymentoption."PRIORITY" AS paymentoptionpriority,
          sl_paymentoption."NAME" AS paymentoptionname,
          sl_paymentoption."ISTEMPORARY",
          sl_paymentoption."ISRETIRED" AS paymentoptionisretired,
          sl_paymentoption."BANKCONNECTIONDATAID",
          sl_paymentoption."PROSPECTIVEPAYMENTOPTIONID",
          sl_paymentoption."ISBLOCKED" AS paymentoptionisblocked,
          sl_transactionjournal.transactionjournalid,
          sl_transactionjournal.transactionid,
          sl_transactionjournal.devicetime,
          sl_transactionjournal.boardingtime,
          sl_transactionjournal.operatorid,
          sl_transactionjournal.clientid AS transactionjournalclientid,
          sl_transactionjournal.line, sl_transactionjournal.coursenumber,
          sl_transactionjournal.routenumber, sl_transactionjournal.routecode,
          sl_transactionjournal.direction, sl_transactionjournal.ZONE,
          sl_transactionjournal.saleschannelid
                                            AS transactionjournalsaleschannel,
          sl_transactionjournal.devicenumber,
          sl_transactionjournal.vehiclenumber,
          sl_transactionjournal.mountingplatenumber,
          sl_transactionjournal.debtornumber,
          sl_transactionjournal.geolocationlongitude,
          sl_transactionjournal.geolocationlatitude,
          sl_transactionjournal.stopnumber,
          sl_transactionjournal.transitaccountid,
          sl_transactionjournal.faremediaid,
          DECODE (sl_transactionjournal.faremediatype,
                  215, 'ClosedLoop',
                  217, 'OpenLoop',
                  sl_transactionjournal.faremediatype
                 ) AS faremediatype,
          sl_transactionjournal.productid, sl_transactionjournal.ticketid,
          sl_transactionjournal.fareamount,
          DECODE (sl_transactionjournal.customergroup,
                  1, 'Adult',
                  2, 'Youth',
                  3, 'Honored Citizen',
                  4, 'Paratransit',
                  sl_transactionjournal.customergroup
                 ) AS customergroup,
          sl_transactionjournal.transactiontype,
          sl_transactionjournal.transactionnumber,
          sl_transactionjournal.resulttype, sl_transactionjournal.filllevel,
          sl_transactionjournal.pursebalance,
          sl_transactionjournal.pursecredit, sl_transactionjournal.groupsize,
          sl_transactionjournal.validfrom, sl_transactionjournal.validto,
          sl_transactionjournal.DURATION, sl_transactionjournal.tariffdate,
          sl_transactionjournal.tariffversion,
          sl_transactionjournal.ticketinternalnumber,
          sl_transactionjournal.whitelistversion,
          sl_transactionjournal.cancellationreference,
          sl_transactionjournal.cancellationreferenceguid,
          sl_transactionjournal.whitelistversioncreated,
          sl_transactionjournal.properties,
          sl_transactionjournal.saleid AS transactionjournalsaleid,
          sl_transactionjournal.tripticketinternalnumber,
          sl_transactionjournal.shiftbegin,
          DECODE (sl_transactionjournal.completionstate,
                  0, 'Pending',
                  1, 'Online',
                  2, 'Offline',
                  3, 'Error',
                  sl_transactionjournal.completionstate
                 ) AS completionstate,
          sl_transactionjournal.responsestate,
          sl_transactionjournal.productid2,
          sl_transactionjournal.pursebalance2,
          sl_transactionjournal.pursecredit2,
          sl_transactionjournal.creditcardauthorizationid,
          sl_transactionjournal.tripcounter, sl_transactionjournal.inserttime,
          sl_sale.saletransactionid, sl_sale.saletype, sl_sale.saledate,
          sl_sale.clientid AS saleclientid,
          sl_sale.saleschannelid AS salesaleschannel, sl_sale.locationnumber,
          sl_sale.devicenumber AS saledevicenumber, sl_sale.salespersonnumber,
          sl_sale.merchantnumber, sl_sale.receiptreference,
          sl_sale.networkreference, sl_sale.cancellationreferenceid,
          sl_sale.isclosed, NVL (sl_sale.orderid, -999) AS orderid,
          sl_sale.externalordernumber,
          NVL (sl_sale.contractid, -999) AS salecontractid,
          sl_sale.refundreferenceid, sl_sale.isorganizational
     FROM sl_paymentjournal LEFT OUTER JOIN sl_paymentoption
          ON sl_paymentoption.paymentoptionid =
                                             sl_paymentjournal.paymentoptionid
          LEFT OUTER JOIN sl_transactionjournal
          ON sl_transactionjournal.transactionjournalid =
                                        sl_paymentjournal.transactionjournalid
          LEFT OUTER JOIN sl_sale ON sl_sale.saleid = sl_paymentjournal.saleid
          ;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
