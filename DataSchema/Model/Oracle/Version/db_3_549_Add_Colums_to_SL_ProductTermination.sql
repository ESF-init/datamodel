SPOOL db_3_549.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.549', 'BTS', 'Adding columns MONTHINUSE, CAPREACHED, REFERENCEDTICKETID to SL_PRODUCTTERMINATION.');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_PRODUCTTERMINATION
 ADD (PROCESSINGCOST  NUMBER);


ALTER TABLE SL_PRODUCTTERMINATION
 ADD (MONTHINUSE  NUMBER);


ALTER TABLE SL_PRODUCTTERMINATION
 ADD (CAPREACHED  NUMBER(1));

Update SL_PRODUCTTERMINATION set transactioncounter = transactioncounter+1, CAPREACHED =0;
 
ALTER Table SL_PRODUCTTERMINATION MODIFY CAPREACHED  NUMBER(1) Default(0);
 
ALTER TABLE SL_PRODUCTTERMINATION
 ADD (REFERENCEDTICKETID  NUMBER(18));

ALTER Table SL_PRODUCTTERMINATION MODIFY CAPREACHED  NUMBER(1) Default(0);

COMMENT ON COLUMN SL_PRODUCTTERMINATION.PROCESSINGCOST IS 'Termination processing costs';


COMMENT ON COLUMN SL_PRODUCTTERMINATION.MONTHINUSE IS 'Total month in use of a product until termination date';


COMMENT ON COLUMN SL_PRODUCTTERMINATION.CAPREACHED IS 'Cap reached. Best price will be invoiced';


COMMENT ON COLUMN SL_PRODUCTTERMINATION.REFERENCEDTICKETID IS 'Reference ticket for difference cost calculation ';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
