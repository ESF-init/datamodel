SPOOL db_3_623.log;


------------------------------------------------------------------------------
--Version 3.623
------------------------------------------------------------------------------
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.623', 'SOE', 'FareCategoryChanged inserted in SL_EmailEvent');


-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------


INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'FareCategoryChanged','Notification of passenger type change', -1, 'SmartcardMailMerge', 84);


COMMIT;

PROMPT Done!

SPOOL OFF;
