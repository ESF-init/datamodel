SPOOL db_3_493.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.493', 'ARH', 'Added BookingTypeTime to sl_bookingitem');


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_BOOKINGITEM
 ADD (BOOKINGTYPETIME  DATE);

 COMMENT ON COLUMN SL_BOOKINGITEM.BOOKINGTYPETIME IS 'Timestamp for each BookingType update.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
