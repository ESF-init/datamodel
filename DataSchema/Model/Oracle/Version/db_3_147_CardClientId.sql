SPOOL db_3_147.log;

INSERT INTO VARIO_DMODELLVERSION(RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION) 
VALUES (sysdate, '3.147', 'MIW', 'Add CardIssuerId to RM_TRANSACTION');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE RM_TRANSACTION ADD CardIssuerId Number(18);

COMMENT ON COLUMN RM_TRANSACTION.CardIssuerId IS 'Represents the client id of the issuer of the smartcard that was used in the transaction. Optional reference to VARIO_CLIENT.CLIENTID.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
