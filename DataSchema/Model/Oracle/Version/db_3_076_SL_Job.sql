SPOOL db_3_076.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.076', 'DST', 'Added column data to Job');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_job add data nclob;
comment on column sl_job.data is 'JobType related data.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
