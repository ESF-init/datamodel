SPOOL db_3_641.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.641', 'EMN', 'rename enumerations in sl_mediasaleprecondition');

-- =======[ Changes to Existing Tables ]===========================================================================

UPDATE SL_MEDIASALEPRECONDITION
   SET LITERAL = 'CompanyLogoSupported',
    DESCRIPTION = 'CompanyLogoSupported'
WHERE ENUMERATIONVALUE = 8;

UPDATE SL_MEDIASALEPRECONDITION
   SET LITERAL = 'NameRequiredCompanyLogoSupported',
    DESCRIPTION = 'NameRequiredCompanyLogoSupported'
WHERE ENUMERATIONVALUE = 9;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
