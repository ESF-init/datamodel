SPOOL db_3_558.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.558', 'SOE', 'Add enumeration value to SL_MESSAGETYPE');

-- =======[ Data ]==================================================================================================

INSERT INTO SL_MESSAGETYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
VALUES
(2, 'Push', 'Push');



-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
