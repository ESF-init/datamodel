SPOOL db_3_527.log;
------------------------------------------------------------------------------
--Version 3.527
------------------------------------------------------------------------------
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.527', 'MEA', 'Add CM_CRYPTOGRAMCERTIFICATE Table and CM_CRYPTOGRAMCERTTYPE');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE CM_CRYPTOGRAMCERTIFICATE
(
	ID										NUMBER(18),
    CHR                                     NVARCHAR2(30)                NOT NULL,  
    keyType            						NUMBER(9)                    NOT NULL,
    CAR                                     NVARCHAR2(30)                NOT NULL,
    RawData            						NVARCHAR2(2000)              NOT NULL 
);
ALTER TABLE CM_CRYPTOGRAMCERTIFICATE ADD (  CONSTRAINT CM_CRYPTOGRAMCERTIFICATE_PK PRIMARY KEY (ID));
CREATE INDEX IDX_CRYPTOGRAMCERTIFICATE_CHR on CM_CRYPTOGRAMCERTIFICATE (CHR);

COMMENT ON TABLE  CM_CRYPTOGRAMCERTIFICATE IS         'Table containing Cryptogram certificates.';
COMMENT ON COLUMN CM_CRYPTOGRAMCERTIFICATE.keyType IS 'Cryptogram Certificate Type see CM_CRYPTOGRAMCERTTYPE';
COMMENT ON COLUMN CM_CRYPTOGRAMCERTIFICATE.CAR IS     'CAR Certification Authority Reference.';
COMMENT ON COLUMN CM_CRYPTOGRAMCERTIFICATE.CHR IS     'CHR Certificate Holder Reference.';



-- =======[ New Tables ]===========================================================================================
CREATE TABLE CM_CRYPTOGRAMCERTTYPE
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL
);


ALTER TABLE CM_CRYPTOGRAMCERTTYPE ADD (  CONSTRAINT PK_CM_CRYPTOGRAMCERTTYPE PRIMARY KEY (ENUMERATIONVALUE)    USING INDEX );

COMMENT ON TABLE  CM_CRYPTOGRAMCERTIFICATE IS         'Table containing Cryptogram certificates.';
COMMENT ON COLUMN CM_CRYPTOGRAMCERTIFICATE.keyType IS 'Cryptogram Certificate Type see CM_CRYPTOGRAMCERTTYPE';
COMMENT ON COLUMN CM_CRYPTOGRAMCERTIFICATE.CAR IS     'CAR Certification Authority Reference.';
COMMENT ON COLUMN CM_CRYPTOGRAMCERTIFICATE.CHR IS     'CHR Certificate Holder Reference.';

Insert into CM_CRYPTOGRAMCERTTYPE  (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values   (0, 'None', 'Not used');
Insert into CM_CRYPTOGRAMCERTTYPE   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values   (1, 'NMSAMAuthorizationkeys', 'Card and SAM Authorization keys PKCS#1');
Insert into CM_CRYPTOGRAMCERTTYPE   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values   (2, 'SAMAuthorizationkeys', 'SAM Authorization keys SO-9796-2');
Insert into CM_CRYPTOGRAMCERTTYPE   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values   (3, 'SAMEncryptionKey', 'SAM Encryption Key');
Insert into CM_CRYPTOGRAMCERTTYPE   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values   (4, 'SAMSignatureKey', 'SAM Signature Key');
Insert into CM_CRYPTOGRAMCERTTYPE   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values   (5, 'KeymanagementActivationkey', 'Keymanagement Activation key');
Insert into CM_CRYPTOGRAMCERTTYPE   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values   (6, 'KeymanagementOrganizationSignatureKey', 'Keymanagement Organization Signature Key');
 Insert into CM_CRYPTOGRAMCERTTYPE   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values   (7, 'CACertificate', 'CA Certificate');
Commit;
PROMPT Done!

SPOOL OFF;