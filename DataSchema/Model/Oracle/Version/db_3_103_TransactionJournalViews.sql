SPOOL db_3_103.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.103', 'DST', 'Refresh for VIEW_SL_TRANSACTIONJOURNAL and added new view SL_TRANSACTIONJOURNALHISTORY');

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW VIEW_SL_TRANSACTIONJOURNAL
(
  TRANSACTIONJOURNALID,
  TRANSACTIONID,
  DEVICETIME,
  BOARDINGTIME,
  OPERATORID,
  CLIENTID,
  LINE,
  COURSENUMBER,
  ROUTENUMBER,
  ROUTECODE,
  DIRECTION,
  ZONE,
  SALESCHANNELID,
  DEVICENUMBER,
  VEHICLENUMBER,
  MOUNTINGPLATENUMBER,
  DEBTORNUMBER,
  GEOLOCATIONLONGITUDE,
  GEOLOCATIONLATITUDE,
  STOPNUMBER,
  TRANSITACCOUNTID,
  FAREMEDIAID,
  FAREMEDIATYPE,
  PRODUCTID,
  TICKETID,
  FAREAMOUNT,
  CUSTOMERGROUP,
  TRANSACTIONTYPE,
  TRANSACTIONNUMBER,
  RESULTTYPE,
  FILLLEVEL,
  PURSEBALANCE,
  PURSECREDIT,
  GROUPSIZE,
  VALIDFROM,
  VALIDTO,
  DURATION,
  TARIFFDATE,
  TARIFFVERSION,
  TICKETINTERNALNUMBER,
  WHITELISTVERSION,
  CANCELLATIONREFERENCE,
  CANCELLATIONREFERENCEGUID,
  LASTUSER,
  LASTMODIFIED,
  TRANSACTIONCOUNTER,
  WHITELISTVERSIONCREATED,
  PROPERTIES,
  SALEID,
  TRIPTICKETINTERNALNUMBER,
  TICKETNAME,
  SHIFTBEGIN,
  COMPLETIONSTATE,
  RESPONSESTATE,
  PRODUCTID2,
  PURSEBALANCE2,
  PURSECREDIT2,
  CREDITCARDAUTHORIZATIONID,
  TRIPCOUNTER,
  INSERTTIME,
  ISVOICEENABLED,
  ISTRAINING
)
AS
with
tariff as (
select tarifid tariffid, netid from (
select tarifid, netid, max(version), max(validfrom) from tm_tarif
group by tarifid, netid having tarifid in (select tariffid from tm_tariff_release where 
  releasetype = decode((select 1 from sl_configurationdefinition where name = 'UseTestTariffArchive' and lower(defaultvalue) like 'f%'), 1, 2, 1) /* returns 1 (testrelease) if UseTestTariffArchive is set to true, otherwise 2 (productiverelease) */
  and deviceclassid = 70)
order by max(validfrom) desc, max(version) desc
) where rownum = 1)
SELECT sl_transactionjournal.transactionjournalid,
          sl_transactionjournal.transactionid,
          sl_transactionjournal.devicetime,
          sl_transactionjournal.boardingtime,
          sl_transactionjournal.operatorid, sl_transactionjournal.clientid,
          sl_transactionjournal.line, sl_transactionjournal.coursenumber,
          sl_transactionjournal.routenumber, sl_transactionjournal.routecode,
          sl_transactionjournal.direction, sl_transactionjournal.ZONE,
          sl_transactionjournal.saleschannelid,
          sl_transactionjournal.devicenumber,
          sl_transactionjournal.vehiclenumber,
          sl_transactionjournal.mountingplatenumber,
          sl_transactionjournal.debtornumber,
          sl_transactionjournal.geolocationlongitude,
          sl_transactionjournal.geolocationlatitude,
          sl_transactionjournal.stopnumber,
          sl_transactionjournal.transitaccountid,
          sl_transactionjournal.faremediaid,
          DECODE (sl_transactionjournal.faremediatype,
                  215, 'ClosedLoop',
                  217, 'OpenLoop',
                  220, 'Barcode',
                  224, 'VirtualCardGoogle',
                  225, 'VirtualCardApple',
                  226, 'ExternalUidCard',
                  227, 'ExternalBarcode',
                  sl_transactionjournal.faremediatype
                 ) AS faremediatype,
          sl_transactionjournal.productid, sl_transactionjournal.ticketid,
          sl_transactionjournal.fareamount,
          NVL(tm_attributevalue.name, sl_transactionjournal.customergroup) AS customergroup,
          sl_transactionjournal.transactiontype,
          sl_transactionjournal.transactionnumber,
          sl_transactionjournal.resulttype, sl_transactionjournal.filllevel,
          sl_transactionjournal.pursebalance,
          sl_transactionjournal.pursecredit, sl_transactionjournal.groupsize,
          sl_transactionjournal.validfrom, sl_transactionjournal.validto,
          sl_transactionjournal.DURATION, sl_transactionjournal.tariffdate,
          sl_transactionjournal.tariffversion,
          sl_transactionjournal.ticketinternalnumber,
          sl_transactionjournal.whitelistversion,
          sl_transactionjournal.cancellationreference,
          sl_transactionjournal.cancellationreferenceguid,
          sl_transactionjournal.lastuser, sl_transactionjournal.lastmodified,
          sl_transactionjournal.transactioncounter,
          sl_transactionjournal.whitelistversioncreated,
          sl_transactionjournal.properties, sl_transactionjournal.saleid,
          sl_transactionjournal.tripticketinternalnumber,
          tm_ticket.NAME AS ticketname, sl_transactionjournal.shiftbegin,
          DECODE (sl_transactionjournal.completionstate,
                  0, 'Pending',
                  1, 'Online',
                  2, 'Offline',
                  3, 'Error',
                  sl_transactionjournal.completionstate
                 ) AS completionstate,
          sl_transactionjournal.responsestate,
          sl_transactionjournal.productid2,
          sl_transactionjournal.pursebalance2,
          sl_transactionjournal.pursecredit2,
          sl_transactionjournal.creditcardauthorizationid,
          sl_transactionjournal.tripcounter, sl_transactionjournal.inserttime,
          sl_card.isvoiceenabled, sl_card.istraining
     FROM sl_transactionjournal 
     INNER JOIN tm_ticket
          ON (sl_transactionjournal.ticketid = tm_ticket.ticketid)
     LEFT OUTER JOIN sl_card
          ON sl_transactionjournal.transitaccountid = sl_card.cardid
     left join tm_attribute on tm_attribute.tarifid in (select tariffid from tariff) and tm_attribute.attribclassid=14
     left join tm_attributevalue on tm_attributevalue.attributeid = tm_attribute.attributeid and tm_attributevalue.number_ = sl_transactionjournal.customergroup
;


CREATE OR REPLACE VIEW SL_TRANSACTIONJOURNALHISTORY
(
  transactionid,
  operatorid,
  saleschannelid,
  transactiontype,
  timestamp,
  ticketexternalnumber,
  result,
  pursecredit,
  pursebalance,
  pursecreditpretax,
  pursebalancepretax,
  duration,
  validfrom,
  validto, 
  numberofpersons,
  stopno,
  stopname,
  lineno,
  linename,
  vehiclenumber,
  devicenumber,
  devicename,
  devicedescription,
  appliedcapping,
  producttickettype,
  ticketname,
  transitaccountid
)
AS
with
tariff as (
select tarifid tariffid, netid from (
select tarifid, netid, max(version), max(validfrom) from tm_tarif
group by tarifid, netid having tarifid in (select tariffid from tm_tariff_release where 
  releasetype = decode((select 1 from sl_configurationdefinition where name = 'UseTestTariffArchive' and lower(defaultvalue) like 'f%'), 1, 2, 1) /* returns 1 (testrelease) if UseTestTariffArchive is set to true, otherwise 2 (productiverelease) */
  and deviceclassid = 70)
order by max(validfrom) desc, max(version) desc
) where rownum = 1),
trans as (
select * 
from sl_transactionjournal
where
transactionjournalid not in (select cancellationreference from sl_transactionjournal can where cancellationreference is not null) and
transactiontype in (
66, -- Boarding
84, -- Transfer
85, -- Use
99, -- Charge
108, -- Load
261, -- ClientFare
262, -- OpenLoopVirtualCharge
7000, -- Inspection
9001, -- FareMediaSale
9003, -- Refund
9006, -- Adjustment
9007, -- DormancyFee
9009, -- BalanceTransfer
9010, -- ProductTransfer
9012, -- StoredValuePayment
9013 -- StoredValueRefund
))
select 
  trans.transactionid,
  trans.operatorid,
  trans.saleschannelid,
  case when trans.transactiontype = 66 and trans.validto is not null then 9011 /* ProductActivation */ else trans.transactiontype end transactiontype,
  trans.devicetime timestamp,
  trans.ticketinternalnumber ticketexternalnumber,
  trans.resulttype result,
  case 
    when trans.resulttype = 0 /* Ok */ 
      then case when trans.transactiontype = 99 /*charge*/ and prod.tickettype = 201 /*pursepretax*/ then null else trans.pursecredit end /* PreTax charge does not get written in Product2, workaround */
    else 0 end pursecredit,
  case when trans.resulttype = 0 /* Ok */ 
    then case when trans.transactiontype = 99 /*charge*/ and prod.tickettype = 201 /*pursepretax*/ then null else trans.pursebalance end /* PreTax charge does not get written in Product2, workaround */
    else 0 end pursebalance,
  case when trans.resulttype = 0 /* Ok */ 
    then case when trans.transactiontype = 99 /*charge*/ and prod.tickettype = 201 /*pursepretax*/ then trans.pursecredit else trans.pursecredit2 end /* PreTax charge does not get written in Product2, workaround */
    else 0 end pursecreditpretax,
  case when trans.resulttype = 0 /* Ok */
    then case when trans.transactiontype = 99 /*charge*/ and prod.tickettype = 201 /*pursepretax*/ then trans.pursebalance else trans.pursebalance2 end else 0 end pursebalancepretax,
  trans.duration,
  case
    when trans.validfrom is null and trans.validto is null and trans.duration > 0 then trans.devicetime 
    else trans.validfrom end validfrom, /* Transfers don't have ValidFrom and ValidTo set, workaround */
  case 
    when trans.validto is null and trans.validfrom is not null then trans.validfrom+trans.duration/(24*60*60) /* Boardings don't have ValidTo set, workaround */
    when trans.validfrom is null and trans.validto is null and trans.duration > 0 then trans.devicetime+trans.duration/(24*60*60) /* Transfers don't have ValidFrom and ValidTo set, workaround */
    else trans.validto end validto, 
  trans.groupsize numberofpersons,
  stop.stopno,
  stop.stopname,
  line.lineno,
  line.linename,
  trans.vehiclenumber,
  trans.devicenumber,
  device.name devicename,
  device.description devicedescription,
  cap.cappingname appliedcapping,
  prod.tickettype producttickettype,
  ticket.name ticketname,
  trans.transitaccountid
from trans
left join tariff on 1=1 -- tariff only contains one row
left join tm_ticket ticket on ticket.internalnumber = trans.ticketinternalnumber and tariff.tariffid = ticket.tarifid
left join vario_stop stop on stop.netid = tariff.netid and stop.stopno = trans.stopnumber
left join vario_line line on line.netid = tariff.netid and line.lineno = trans.line
left join um_device device on device.deviceno = trans.devicenumber
left join sl_product prod on prod.productid = trans.productid
left join sl_cappingjournal cappingjournal on cappingjournal.transactionjournalid = trans.transactionjournalid and cappingjournal.state in ( 3, /* applied */ 2 /* partiallyapplied */ )
left join TM_RULE_CAPPING cap on cap.potnumber = cappingjournal.potid and cap.tariffid in (select tariffid from tariff)
;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
