SPOOL db_3_266.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.266', 'STV', 'VdvEntitlementData Column Binary Data containg the Vdv product specifc static part for the Vdv Entitlement');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TRIGGER SL_PRODUCT_BRU DISABLE;
ALTER TABLE SL_PRODUCT ADD (VdvEntitlementData BLOB default empty_blob());
ALTER TRIGGER SL_PRODUCT_BRU ENABLE;

-- =======[ New Tables ]===========================================================================================

COMMENT ON COLUMN SL_PRODUCT.VdvEntitlementData IS 'Binary Data containg the Vdv product specifc static part for the Vdv Entitlement';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
