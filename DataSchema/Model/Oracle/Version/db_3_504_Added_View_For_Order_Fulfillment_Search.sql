SPOOL db_3_504.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.504', 'EMN', 'SL_ORDERSEARCHVIEW : Orders with customer info');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

--COMMENT ON TABLE XXX is '';

--COMMENT ON COLUMN XXX.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
--COMMENT ON COLUMN XXX.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
--COMMENT ON COLUMN XXX.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================
CREATE OR REPLACE FORCE VIEW SL_ORDERSEARCHVIEW
(
    ORDERID,
    ORDERNUMBER,
    STATE,
    ORDERDATE,
    PRIORITY,
    PROCESSINGENDDATE,
    PRODUCTS,
    PRINTEDNUMBER,
    NAME,
    ADDRESSFIELD1,
    ADDRESSFIELD2,
    CITY,
    REGION,
    POSTALCODE,
    PAYMENTSTATE
)
AS
        SELECT o."ORDERID",
           o."ORDERNUMBER",
           o."STATE",
           o."ORDERDATE",
           o."PRIORITY",
           od."PROCESSINGENDDATE",
           po."PRODUCTS",
           cards."PRINTEDNUMBER",
           (CASE WHEN so.name is not null then so.name
                WHEN sca.customeraccountid is not null then spe.firstname || ' ' || spe.lastname
                WHEN cards.cardholderid is not null then sp2.firstname || ' ' || sp2.lastname
                end)as "NAME",
           sa."ADDRESSFIELD1",
           sa."ADDRESSFIELD2",
           sa."CITY",
           sa."REGION",
           sa."POSTALCODE",
           cards.state as "PAYMENTSTATE"
    from sl_order o 
        join ( select sod.orderid as orderid, listagg(tt.name, ',') within group (order by tt.name) 
            as products from sl_orderdetail sod
            join sl_product sp on sp.productid = sod.productid 
            join tm_ticket tt on tt.ticketid = sp.ticketid
            group by sod.orderid) po on po.orderid = o.orderid  
        join sl_orderdetail od on od.ORDERID = o.ORDERID
        join sl_address sa on o.shippingaddressid = sa.addressid
    	left join sl_contract sc on sc.contractid = o.contractid
    	left join sl_organization so on so.organizationid = sc.organizationid
    	left join sl_customeraccount sca on sca.contractid = sc.contractid
    	left join sl_person spe on spe.personid = sca.personid
        left join (select sale.orderid, card.printednumber, card.cardholderid, pj.state from sl_sale sale 
            join sl_paymentjournal pj on pj.saleid = sale.saleid
            join sl_transactionjournal tj on tj.SALEID = sale.SALEID
            join sl_card card on card.FAREMEDIAID = tj.FAREMEDIAID
            where sale.orderid is not null) cards on cards.orderid = o.orderid
        left join sl_person sp2 on sp2.PERSONID = cards.CARDHOLDERID
    group by o.orderid,o.ordernumber, o.STATE, o.ORDERDATE, o.priority, od.PROCESSINGENDDATE,
        po.products, cards.PRINTEDNUMBER,
        (CASE WHEN so.name is not null then so.name
              WHEN sca.customeraccountid is not null then spe.firstname || ' ' || spe.lastname
              WHEN cards.cardholderid is not null then sp2.firstname || ' ' || sp2.lastname
         end), sa.ADDRESSFIELD1 , sa.ADDRESSFIELD2,  sa.City,  sa.REGION, sa.POSTALCODE, cards.state;
-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
