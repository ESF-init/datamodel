SPOOL db_3_019.log

INSERT INTO vario_dmodellversion (rundate,dmodellno,responsible,annotation)
VALUES (SYSDATE,'3.019','FLF','Added TM_FARESTAGE.EXTERNALNUMBER and TM_BUSINESSRULECLAUSE.ACCUMULATE');


---Start adding schema changes here

ALTER TABLE TM_FARESTAGE
 ADD (EXTERNALNUMBER  NUMBER(10));

 ALTER TABLE TM_BUSINESSRULECLAUSE
 ADD (ACCUMULATE  NUMBER(1)                         DEFAULT 0                     NOT NULL);

---End adding schema changes



COMMIT;

PROMPT Done!

SPOOL OFF