SPOOL db_3_482.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.482', 'STV', 'New_SL_CHANGEVOUCHER_View');

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW SL_CHANGEVOUCHER
(
    CHANGEVOUCHERID,
    DEBTOR,
    DEBTORNAME,
    DEVICENUMBER,
    ISSUEDATE,
    LOCATIONNUMBER,
    LOCATION,
    REFERENCE,
    ISREDEEMED,
    REDEEMDATE,
    REDEEMBY,
    REDEEMEDONDEVICE,
    SALESTRANSACTIONID,
    PRODUCTID,
    FAREAMOUNT
)
AS
        SELECT    t.TransactionJournalID,
                d.DEBTORNO,
                d.NAME,
                s.DEVICENUMBER,
                t.DEVICETIME,
                st.STOPNO,
                st.STOPNAME,
                s.RECEIPTREFERENCE,
                (CASE WHEN refund.TRANSACTIONJOURNALID is not null THEN 1 ELSE 0 END),
                refundSale.SALEDATE,
                refund.DEBTORNUMBER,
                refund.DEVICENUMBER,
                s.SALETRANSACTIONID,
                p.PRODUCTID,
                p.PRICE
      FROM SL_TRANSACTIONJOURNAL t
        LEFT JOIN (SELECT * FROM SL_TRANSACTIONJOURNAL r                 
                   WHERE r.TRANSACTIONTYPE = 9003 ) refund -- REFUND
        ON   refund.PRODUCTID = t.PRODUCTID
      LEFT JOIN SL_SALE s ON s.SALEID = t.SALEID
      LEFT JOIN SL_PRODUCT p ON p.PRODUCTID = t.PRODUCTID
      LEFT JOIN DM_DEBTOR d ON d.DEBTORNO = t.DEBTORNUMBER
      LEFT JOIN VARIO_STOP st ON st.STOPNO = t.STOPNUMBER
      LEFT JOIN SL_SALE refundSale ON refundSale.SALEID = refund.SALEID
      WHERE t.TRANSACTIONTYPE = 9015 --CHANGE_VOUCHER
;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
