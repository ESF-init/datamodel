SPOOL db_3_487.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.487', 'INIT\STV', 'IORCA-207 - Added table and columns for participant groups.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE sl_organizationparticipant 
	ADD (participantgroupid NUMBER(18, 0) NULL);
    
COMMENT ON COLUMN sl_organizationparticipant.participantgroupid IS 'Foreign key, ID of the participant group the participant belongs to.';

-- =======[ New Tables ]===========================================================================================

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE sl_participantgroup
(
	contractid NUMBER(18, 0) NOT NULL, 
	groupname NVARCHAR2(125) NOT NULL, 
	groupdescription NVARCHAR2(512) NULL,
	participantgroupid NUMBER(18, 0) NOT NULL,
	lastuser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	lastmodified DATE DEFAULT sysdate NOT NULL,
	transactioncounter INTEGER NOT NULL
);

COMMENT ON TABLE  sl_participantgroup IS 'The table is used for grouping participants to simplify bulk import and sale processes.';
COMMENT ON COLUMN sl_participantgroup.contractid IS 'Foreign key, ID of the institution contract the participant group belongs to.';
COMMENT ON COLUMN sl_participantgroup.groupname IS 'Name of the participant group, cf. unique constraint name + contract ID.';
COMMENT ON COLUMN sl_participantgroup.participantgroupid IS 'Primary key, ID of the participant group.';
COMMENT ON COLUMN sl_participantgroup.lastuser IS 'Technical field: last (system) user that changed this record.';
COMMENT ON COLUMN sl_participantgroup.lastmodified IS 'Technical field: date time of the last change to this record.';
COMMENT ON COLUMN sl_participantgroup.transactioncounter IS 'Technical field: used for concurrency checks to support concurrent editing of the same record.';

ALTER TABLE sl_participantgroup
	ADD CONSTRAINT pk_participantgroup PRIMARY KEY
	( 
		participantgroupid
	);

ALTER TABLE sl_participantgroup
	ADD CONSTRAINT uk_namecontractid UNIQUE
	(
		groupname, contractid
	);
    
ALTER TABLE sl_participantgroup
	ADD CONSTRAINT fk_participantgrp_contract FOREIGN KEY
	(
		contractid
	)
	REFERENCES sl_contract
	(
		contractid
	);

ALTER TABLE sl_organizationparticipant
	ADD CONSTRAINT fk_toparticipantgroup FOREIGN KEY
	(
		participantgroupid
	)
	REFERENCES sl_participantgroup
	(
		participantgroupid
	);

-- =======[ Sequences ]============================================================================================

CREATE SEQUENCE sl_participantgroup_seq INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================

CREATE OR REPLACE TRIGGER sl_participantgroup_bri
    BEFORE INSERT ON sl_participantgroup 
    FOR EACH ROW BEGIN 
        :NEW.transactioncounter := dbms_utility.get_time; 
    END;
/

CREATE OR REPLACE TRIGGER sl_participantgroup_bru
	BEFORE UPDATE ON sl_participantgroup 
    FOR EACH ROW BEGIN
        IF (:NEW.transactioncounter != :OLD.transactioncounter + 1) 
            THEN raise_application_error( -20000, 'Concurrency Failure' ); 
        END IF; 
        :NEW.transactioncounter := dbms_utility.get_time; 
    END;
/

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
