SPOOL db_3_361.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.361', 'LEK', 'Added RegioMove Columns');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_BOOKINGITEM
ADD (BOOKINGOPEN NUMBER(1));

COMMENT ON COLUMN SL_BOOKINGITEM.BOOKINGOPEN IS 'Indicates whether the booking item is still open or closed.';

ALTER TABLE SL_MOBILITYPROVIDER
ADD (MOBILITYPROVIDERNUMBER NUMBER(18));

COMMENT ON COLUMN SL_MOBILITYPROVIDER.MOBILITYPROVIDERNUMBER IS 'The unique and specific number of the mobility provider.'; 

ALTER TABLE SL_MOBILITYPROVIDER
ADD (DRIVINGLICENCEREQUIRED NUMBER(1));

COMMENT ON COLUMN SL_MOBILITYPROVIDER.DRIVINGLICENCEREQUIRED IS 'Indicates whether a driving licence is required for using the mobility provider.';

ALTER TABLE SL_MOBILITYPROVIDER
ADD (FIXEDPRICEPROVIDER NUMBER(1));

COMMENT ON COLUMN SL_MOBILITYPROVIDER.FIXEDPRICEPROVIDER IS 'Indicates whether the mobility provider has fixed prices.';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
