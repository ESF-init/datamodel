INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.484', 'BVI', 'Printer enums.');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_PrinterType
(
  EnumerationValue  NUMBER(9)           NOT NULL,
  Literal           NVARCHAR2(50)       NOT NULL,
  Description       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_PrinterType PRIMARY KEY (EnumerationValue)
);
COMMENT ON TABLE SL_PrinterType IS 'Enumeration values for SL_Printer.PrinterType.';
COMMENT ON COLUMN SL_PrinterType.EnumerationValue IS 'Type identifier.';
COMMENT ON COLUMN SL_PrinterType.Literal IS 'Type literal.';
COMMENT ON COLUMN SL_PrinterType.Description IS 'Type description.';

INSERT ALL
	INTO SL_PrinterType (EnumerationValue, Literal, Description) VALUES (0, 'Unknown', 'Unknown')
	INTO SL_PrinterType (EnumerationValue, Literal, Description) VALUES (1, 'BulkPrinter', 'Bulk printer')
	INTO SL_PrinterType (EnumerationValue, Literal, Description) VALUES (2, 'ReceiptPrinter', 'Receipt printer')
	INTO SL_PrinterType (EnumerationValue, Literal, Description) VALUES (3, 'LetterPrinter', 'Letter printer')
SELECT * FROM DUAL;


CREATE TABLE SL_PrinterState
(
  EnumerationValue  NUMBER(9)           NOT NULL,
  Literal           NVARCHAR2(50)       NOT NULL,
  Description       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_PrinterState PRIMARY KEY (EnumerationValue)
);
COMMENT ON TABLE SL_PrinterState IS 'High level state for printers.';
COMMENT ON COLUMN SL_PrinterState.EnumerationValue IS 'Type identifier.';
COMMENT ON COLUMN SL_PrinterState.Literal IS 'Type literal.';
COMMENT ON COLUMN SL_PrinterState.Description IS 'Type description.';


INSERT ALL
	INTO SL_PrinterState (EnumerationValue, Literal, Description) VALUES (0, 'Unknown', 'Unknown')
	INTO SL_PrinterState (EnumerationValue, Literal, Description) VALUES (1, 'Online', 'Online')
	INTO SL_PrinterState (EnumerationValue, Literal, Description) VALUES (2, 'Offline', 'Offline')
	INTO SL_PrinterState (EnumerationValue, Literal, Description) VALUES (3, 'Busy', 'Busy')
SELECT * FROM DUAL;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;