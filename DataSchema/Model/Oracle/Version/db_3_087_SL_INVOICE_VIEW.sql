SPOOL db_3_087.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.087', 'SOE', 'Updated View SL_INVOICE_VIEW');

-- =======[ Views ]===========================================================================

CREATE OR REPLACE FORCE VIEW SL_INVOICE_VIEW
(
SCHOOLYEARID,
CONTRACTID,
NAME,
ORGANIZATIONNUMBER,
INVOICEENTRYAMOUNT,
CREATIONDATE,
FROMDATE,
INVOICEENTRYCOUNT,
INVOICEID,
INVOICENUMBER,
INVOICETYPE,
TODATE,
CARDSTATE,
NUMBEROFPRINTS
)
AS
SELECT 
i.SCHOOLYEARID,
co.CONTRACTID,
o.NAME,
o.ORGANIZATIONNUMBER,
i.INVOICEENTRYAMOUNT,
i.CREATIONDATE,
i.FROMDATE,
i.INVOICEENTRYCOUNT,
i.INVOICEID,
i.INVOICENUMBER,
i.INVOICETYPE,
i.TODATE,
crd.STATE,
crd.NUMBEROFPRINTS
FROM sl_invoice  i
LEFT JOIN sl_contract co ON i.CONTRACTID = co.CONTRACTID
INNER JOIN sl_organization o ON o.ORGANIZATIONID = co.ORGANIZATIONID
INNER JOIN sl_invoiceentry ie on i.INVOICEID = ie.INVOICEID
INNER JOIN (SELECT * FROM (SELECT p.*, ROW_NUMBER()
             OVER(PARTITION BY p.postingId ORDER BY p.postingId) AS Seq
            FROM  sl_posting p) a
            WHERE  Seq = 1) frst on ie.INVOICEENTRYID = frst.INVOICEENTRYID
INNER JOIN sl_product prod on frst.PRODUCTID = prod.PRODUCTID
INNER JOIN sl_card crd on crd.CARDID = prod.CARDID ;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
