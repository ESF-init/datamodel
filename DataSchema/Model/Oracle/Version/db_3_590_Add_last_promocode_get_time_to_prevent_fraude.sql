SPOOL db_3_590.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.590', 'STV', 'Add_last_promocode_get_time_to_prevent_fraude');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PROMOCODE ADD LASTGETTIME DATE NULL;

COMMIT;

PROMPT Done!

SPOOL OFF;
