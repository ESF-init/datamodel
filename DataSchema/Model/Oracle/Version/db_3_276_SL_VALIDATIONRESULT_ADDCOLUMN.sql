SPOOL db_3_276.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.276', 'PCO', 'Clearing House Associating the Validation Results with the rule run');

-- =======[ Changes on existing tables ]===========================================================================
ALTER TABLE SL_VALIDATIONRESULT
  ADD VALIDATIONRUNRULEID NUMBER(18) NOT NULL;
  
COMMENT ON COLUMN SL_VALIDATIONRESULT.VALIDATIONRUNRULEID IS 'Refernece to the validation run for this rule. SL_VALIDATIONRUNRULE.VALIDATIONRUNRULEID';

ALTER TABLE SL_VALIDATIONRESULT
    ADD (CONSTRAINT FK_VALRESULT_RUNRULEID FOREIGN KEY (VALIDATIONRUNRULEID) REFERENCES SL_VALIDATIONRUNRULE(VALIDATIONRUNRULEID));
    

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================
-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL off; 
