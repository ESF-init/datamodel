SPOOL db_3_674.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.674', 'ARH', 'Add HomeZoneEmails to SL_EmailEvent');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================



INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'RegiomoveHomeZoneWillExpire','Warn of the upcoming expiration of HomeZone', 0, 'HomeZoneExpirationMailMerge', 103);

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'RegiomoveHomeZoneIsExpired','Warn of the expiration of the HomeZone', 0, 'HomeZoneExpirationMailMerge', 104);



---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
