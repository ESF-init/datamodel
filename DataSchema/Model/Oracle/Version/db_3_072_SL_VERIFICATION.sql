SPOOL db_3_072.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.072', 'EPA', 'Added SL_CUSTOMER ACCOUNT verification tables and rows');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_VERIFICATIONATTEMPTTYPE
(
  ENUMERATIONVALUE		NUMBER(9) 			NOT NULL,
  LITERAL				NVARCHAR2(50) 		NOT NULL,
  DESCRIPTION			NVARCHAR2(50) 		NOT NULL,
  CONSTRAINT PK_VERIFICATIONATTEMPTTYPE PRIMARY KEY (ENUMERATIONVALUE)
);

COMMENT ON TABLE SL_VERIFICATIONATTEMPTTYPE IS 'Enumeration table for account verification types';
COMMENT ON COLUMN SL_VERIFICATIONATTEMPTTYPE.ENUMERATIONVALUE IS 'Primary key and enum value for verification type.';
COMMENT ON COLUMN SL_VERIFICATIONATTEMPTTYPE.LITERAL IS 'Short description of verification type.';
COMMENT ON COLUMN SL_VERIFICATIONATTEMPTTYPE.DESCRIPTION IS 'Long description of verification type.';


CREATE TABLE SL_VERIFICATIONATTEMPT
(
  VERIFICATIONID    	NUMBER(18) 						NOT NULL, 
  VERIFICATIONTIME		DATE	 						NOT NULL,
  CUSTOMERACCOUNTID 	NUMBER(18) 						NOT NULL,
  VERIFICATIONTYPE		NUMBER(9) 						NOT NULL,
  LASTUSER            	NVARCHAR2(50) 	DEFAULT 'SYS'   NOT NULL,
  LASTMODIFIED        	DATE          	DEFAULT sysdate NOT NULL,
  TRANSACTIONCOUNTER  	INTEGER                         NOT NULL,
  CONSTRAINT PK_VERIFICATIONATTEMPT PRIMARY KEY (VERIFICATIONID),
  CONSTRAINT FK_CUSTOMERACCOUNT FOREIGN KEY (CUSTOMERACCOUNTID) REFERENCES SL_CUSTOMERACCOUNT(CUSTOMERACCOUNTID)
);

COMMENT ON TABLE SL_VERIFICATIONATTEMPT IS 'Verification attempt history of users to establish a account blocking on failed attempts.';
COMMENT ON COLUMN SL_VERIFICATIONATTEMPT.VERIFICATIONID IS 'Unique identifier of the verification entry.';
COMMENT ON COLUMN SL_VERIFICATIONATTEMPT.VERIFICATIONTIME IS 'Timestamp of the verification attempt.';
COMMENT ON COLUMN SL_VERIFICATIONATTEMPT.CUSTOMERACCOUNTID IS 'Customer account of the verification attempt.';
COMMENT ON COLUMN SL_VERIFICATIONATTEMPT.VERIFICATIONTYPE IS 'Type of verification process that has been used in the attempt.';


-- -------[ Enumerations ]-----------------------------------------------------------------------------------------
INSERT ALL 
	INTO SL_VERIFICATIONATTEMPTTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (0, 'Unknown', 'Unknown type of verification attempt.')
	INTO SL_VERIFICATIONATTEMPTTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (1, 'Phone', 'Phone PIN verification attempt.')
	INTO SL_VERIFICATIONATTEMPTTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (2, 'SecurityQuestion', 'Security question verification attempt.')
SELECT * FROM DUAL;

-- -------[ Sequence ]---------------------------------------------------------------------------------------------
DECLARE
  TYPE table_names_array IS VARRAY(4) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_VERIFICATIONATTEMPT');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(6) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_VERIFICATIONATTEMPT');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
