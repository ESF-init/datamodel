SPOOL db_3_490.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.490', 'ULB', 'CreditCardPaymenthistory');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE RM_PAYMENT
MODIFY(RECEIPTNUMBER VARCHAR2(40 BYTE));

CREATE INDEX IDX_RM_TX_Shoppingcart ON RM_TRANSACTION
(SHOPPINGCART)
LOGGING
NOPARALLEL;

-- =======[ New Tables ]===========================================================================================
CREATE TABLE RM_CreditCardPaymentHistory
(
  CREDITCARDTRANSACTIONID      NUMBER(10),
  SHOPPINGCART        NUMBER(10)               NOT NULL,
  ISSUER               VARCHAR2(40 BYTE),
  MASKEDPAN            VARCHAR2(40 BYTE),
  TERMINALID           VARCHAR2(40 BYTE),
  REFERENCENO          VARCHAR2(40 BYTE),
  AUTHCODE             VARCHAR2(40 BYTE),
  AMOUNT               NUMBER(9),
  STATUS               VARCHAR2(40 BYTE),
  ACCOUNTTYPE          VARCHAR2(40 BYTE),
  shiftId               NUMBER(10)               NOT NULL,
  transactiontime   Date
)
;


CREATE UNIQUE INDEX RM_CREDITCARDTRANSACTIONS_PK ON RM_CreditCardPaymentHistory
(CREDITCARDTRANSACTIONID);


ALTER TABLE RM_CreditCardPaymentHistory ADD (
  CONSTRAINT RM_CREDITCARDTRANSACTIONS_PK
 PRIMARY KEY
 (CREDITCARDTRANSACTIONID)
    USING INDEX 
);

ALTER TABLE RM_CreditCardPaymentHistory ADD (
  CONSTRAINT FK_RM_CCPayment_History_TX 
 FOREIGN KEY (shiftid) 
 REFERENCES RM_SHIFT (shiftid)
    ON DELETE CASCADE);
	
COMMENT ON TABLE RM_CreditCardPaymentHistory IS 'Contains the details of a payment.';

COMMENT ON COLUMN RM_CreditCardPaymentHistory.SHOPPINGCART IS 'NO of the shoppingcart referenced in RM_TRANSACTION';

COMMENT ON COLUMN RM_CreditCardPaymentHistory.ISSUER IS 'Bank or credit union that issued the payment card.';

COMMENT ON COLUMN RM_CreditCardPaymentHistory.MASKEDPAN IS 'Masked primary account number.';

COMMENT ON COLUMN RM_CreditCardPaymentHistory.TERMINALID IS 'ID of the terminal.';

COMMENT ON COLUMN RM_CreditCardPaymentHistory.REFERENCENO IS 'Reference number.';

COMMENT ON COLUMN RM_CreditCardPaymentHistory.AUTHCODE IS 'Authorization Code';

COMMENT ON COLUMN RM_CreditCardPaymentHistory.AMOUNT IS 'Mode of validation';

COMMENT ON COLUMN RM_CreditCardPaymentHistory.STATUS IS 'Status of each transaction (AUTHORIZED,COMMITED,APPROVED,VOIDED,...  ) ';

COMMENT ON COLUMN RM_CreditCardPaymentHistory.ACCOUNTTYPE IS 'Type of account (Credit/Debit)';

COMMENT ON COLUMN RM_CreditCardPaymentHistory.SHIFTID IS 'Reference to rm_shift';

COMMENT ON COLUMN RM_CreditCardPaymentHistory.TRANSACTIONTIME IS 'Date/Time of the transaction';


-- =======[ Views ]================================================================================================
CREATE OR REPLACE VIEW View_RM_CreditCardPayments
as
select 
RM_SHIFT.CLIENTID,
RM_SHIFT.SHIFTID,
RM_SHIFT.SHIFTBEGIN,
RM_SHIFT.LOCATIONNO,
RM_SHIFT.DEBTORNO,
RM_SHIFT.SHIFTSTATEID,
RM_CreditCardPaymentHistory.CREDITCARDTRANSACTIONID,
RM_CreditCardPaymentHistory.ACCOUNTTYPE,
RM_CreditCardPaymentHistory.ISSUER,
RM_CreditCardPaymentHistory.MASKEDPAN,
RM_CreditCardPaymentHistory.TERMINALID,
RM_CreditCardPaymentHistory.REFERENCENO,
RM_CreditCardPaymentHistory.AUTHCODE,
RM_CreditCardPaymentHistory.AMOUNT,
RM_CreditCardPaymentHistory.STATUS,
RM_CREDITCARDPAYMENTHistory.TRANSACTIONTIME,
RM_CREDITCARDPAYMENTHistory.SHOPPINGCART
from RM_CreditCardPaymentHistory,rm_shift
where RM_CreditCardPaymentHistory.shiftid = rm_shift.shiftid 
order by RM_SHIFT.SHIFTBEGIN, RM_CREDITCARDPAYMENTHistory.TRANSACTIONTIME, RM_CREDITCARDPAYMENTHistory.CREDITCARDTRANSACTIONID;

create or replace view view_RM_shoppingcartpayments
as
select sum( decode (rm_transaction.devicepaymentmethod,255, RM_PAYMENT.AMOUNT,rm_transaction.price)) as amount,
decode (rm_transaction.devicepaymentmethod,255, RM_PAYMENT.devicepaymentmethodid ,rm_transaction.devicepaymentmethod) as Paymentmethod,
rm_transaction.shoppingcart,
rm_transaction.shiftid ,
rm_devicebookingstate.isbooked
from rm_transaction,rm_payment,rm_devicebookingstate
where rm_transaction.transactionid = rm_payment.transactionid(+)
AND rm_devicebookingstate.devicebookingno = rm_transaction.devicebookingstate
and rm_transaction.shoppingcart <> 0
group by   decode (rm_transaction.devicepaymentmethod,255, RM_PAYMENT.devicepaymentmethodid,rm_transaction.devicepaymentmethod),
shoppingcart,shiftid ,rm_devicebookingstate.isbooked;
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
