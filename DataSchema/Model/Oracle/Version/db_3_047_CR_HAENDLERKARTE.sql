SPOOL db_3_047.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.047', 'JGI', 'Increased column TERMINAL_ID to NUMBER(10)');

-- Start adding schema changes here

ALTER TABLE CR_HAENDLERKARTEN MODIFY(TERMINAL_ID NUMBER(10));

-- =======[ New Tables ]===========================================================================================

--COMMENT ON TABLE XXX is '';

--COMMENT ON COLUMN XXX.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
--COMMENT ON COLUMN XXX.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
--COMMENT ON COLUMN XXX.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
