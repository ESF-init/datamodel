SPOOL db_3_327.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.327', 'JGI', 'New procedure P$CLOSEPROTOCOL');

-- =======[ Functions and Data Types ]=============================================================================
CREATE OR REPLACE PROCEDURE P$CLOSEPROTOCOL
(
   p_nPROTID IN NUMBER
)
IS

BEGIN
  
   UPDATE PROTOCOL a SET A.PROTPRIO = (SELECT MAX(B.PROTMESSAGEPRIO) FROM PROT_MESSAGE b WHERE B.PROTID = A.PROTID) WHERE A.PROTID = p_nPROTID;
   UPDATE PROTOCOL a SET A.PROTNUMBEROFMESSAGE = (SELECT COUNT(*) FROM PROT_MESSAGE b WHERE B.PROTID = A.PROTID) WHERE A.PROTID = p_nPROTID;
   --COMMIT;

END P$CLOSEPROTOCOL;
/

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================
-- =======[ Data ]==================================================================================================
-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL off; 
