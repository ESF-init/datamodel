SPOOL db_3_453.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.453', 'OMA', 'Addition of images to Contract Entities, logos for Institution Accounts.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
--Test this again on db
ALTER TABLE SL_PHOTOGRAPH
ADD (CONTRACTID NUMBER(18) NULL);

ALTER TABLE SL_PHOTOGRAPH ADD (
  CONSTRAINT FK_PHOTOGRAPH_CONTRACT 
  FOREIGN KEY (CONTRACTID) 
  REFERENCES SL_CONTRACT (CONTRACTID)
  ENABLE VALIDATE);
  
  ---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;