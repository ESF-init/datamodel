SPOOL db_3_027.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.027', 'MIW', 'Creation of SL_VirtualCard');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_VIRTUALCARD
(
    VIRTUALCARDID       NUMBER(18,0)    NOT NULL,
    CARDID              NUMBER(18,0)    NOT NULL,
    EXTERNALDEVICEID    NVARCHAR2(512),
    EXTERNALAPPID       NVARCHAR2(512),
    EXTERNALWALLETID    NVARCHAR2(512),
    HASLINKEDDEVICE     NUMBER(1,0),
    HASUSERS            NUMBER(1,0),
    TAPCOUNTER          NUMBER(9,0),    
    SYNCHRONIZED        DATE,
    LASTUSER            VARCHAR2(50 Byte)   DEFAULT 'SYS' NOT NULL,
    LASTMODIFIED        DATE            DEFAULT SYSDATE NOT NULL,
    TRANSACTIONCOUNTER  INTEGER         NOT NULL
);

ALTER TABLE SL_VIRTUALCARD ADD CONSTRAINT PK_VIRTUALCARD PRIMARY KEY (VIRTUALCARDID);
ALTER TABLE SL_VIRTUALCARD ADD CONSTRAINT FK_VIRTUALCARD_CARD FOREIGN KEY (CARDID) REFERENCES SL_CARD (CARDID);

COMMENT ON TABLE SL_VIRTUALCARD is 'Table containing additional information about virtual cards which are implemented on mobile devices. Main data about the card is stored in SL_CARD';
COMMENT ON COLUMN SL_VIRTUALCARD.VIRTUALCARDID is 'Unique identifier of the virtual card';
COMMENT ON COLUMN SL_VIRTUALCARD.CARDID IS 'ID of the card';
COMMENT ON COLUMN SL_VIRTUALCARD.EXTERNALDEVICEID is 'Identifier of the mobile device. Used by Google wallet';
COMMENT ON COLUMN SL_VIRTUALCARD.EXTERNALAPPID is 'Identifier of the application that registers the virtual card on a mobile device. Used by app provider';
COMMENT ON COLUMN SL_VIRTUALCARD.EXTERNALWALLETID is 'Identifier of the wallet object. In case of Google it represents the Loyalty Object ID';
COMMENT ON COLUMN SL_VIRTUALCARD.HASLINKEDDEVICE is 'Indicates if the virtual card is registed to a device. Used by Google wallet';
COMMENT ON COLUMN SL_VIRTUALCARD.HASUSERS is 'Indicates if the virtual card is registed to a device. Used by Google wallet';
COMMENT ON COLUMN SL_VIRTUALCARD.TAPCOUNTER is 'Tap counter reported by the mobile device. Used by Google wallet';
COMMENT ON COLUMN SL_VIRTUALCARD.SYNCHRONIZED is 'Date of the last synchronization to wallet backend system';

COMMENT ON COLUMN SL_VIRTUALCARD.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_VIRTUALCARD.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_VIRTUALCARD.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
     'SL_VIRTUALCARD');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_VIRTUALCARD');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
