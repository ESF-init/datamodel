SPOOL db_3_577.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.577', 'SLA', 'Add TaxRate, TaxAmount to SL_TransactionJournal and TaxLocationCode, TaxTicketId to SL_Product');


-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_TRANSACTIONJOURNAL ADD TAXRATE NUMBER(10,3) NULL;

COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.TAXRATE IS 'Tax rate number';

ALTER TABLE SL_TRANSACTIONJOURNAL
ADD TAXAMOUNT Number(9) NULL;

COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.TAXAMOUNT IS 'Tax amount number';

ALTER TABLE SL_PRODUCT
ADD TAXLOCATIONCODE Number(10) NULL;

COMMENT ON COLUMN SL_PRODUCT.TAXLOCATIONCODE  IS 'Tax location code';

ALTER TABLE SL_PRODUCT
ADD TAXTICKETID Number(18) NULL;

COMMENT ON COLUMN SL_PRODUCT.TAXTICKETID IS 'Tax ticket id';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;


