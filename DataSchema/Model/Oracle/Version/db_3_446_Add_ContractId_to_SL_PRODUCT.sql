SPOOL db_3_446.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.446', 'dby', 'adds CONTRACTID column to SL_PRODUCT');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PRODUCT
 ADD (CONTRACTID  NUMBER(18));
 COMMENT ON COLUMN SL_PRODUCT.CONTRACTID IS 'Id of contract associated with a product, used for updating pass sales that have an account';
 
ALTER TABLE SL_PRODUCT ADD 
CONSTRAINT FK_PRODUCT_CONTRACTID
 FOREIGN KEY (CONTRACTID)
 REFERENCES SL_CONTRACT (CONTRACTID);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
