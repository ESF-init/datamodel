SPOOL db_3_192.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.192', 'ULB', 'Additional Columns for VDV_TLVDATA');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE VDV_TLVDATA
 ADD (BerechtigungOrgId  NUMBER(10));

ALTER TABLE VDV_TLVDATA
 ADD (ProduktOrgId  NUMBER(10));

ALTER TABLE VDV_TLVDATA
 ADD (AppinstanzOrgId  NUMBER(10));

ALTER TABLE VDV_TLVDATA
 ADD (Ortid  NUMBER(10));

ALTER TABLE VDV_TLVDATA
 ADD (terminalid  NUMBER(10));

 COMMENT ON COLUMN VDV_TLVDATA.BerechtigungOrgId IS 'Contains the permission VDV-Organisation ID';
 COMMENT ON COLUMN VDV_TLVDATA.ProduktOrgId IS 'Contains the product VDV-Organisation ID';
 COMMENT ON COLUMN VDV_TLVDATA.AppinstanzOrgId IS 'Contains the application VDV-Organisation ID';
 COMMENT ON COLUMN VDV_TLVDATA.Ortid IS 'References VDV_TRANSAKTIONSORT.ID';
 COMMENT ON COLUMN VDV_TLVDATA.terminalid IS 'References VDV_TERMINAL.ID';

ALTER TABLE VDV_TLVDATA
 ADD CONSTRAINT fk_vdv_TLVdata_Ort 
 FOREIGN KEY (Ortid) 
 REFERENCES VDV_TRANSAKTIONSORT (ID);

ALTER TABLE VDV_TLVDATA
 ADD CONSTRAINT fk_vdv_TLVdata_terminal 
 FOREIGN KEY (terminalid) 
 REFERENCES VDV_TERMINAL (ID);

-- Foreignkey missing - so we have to cleanup before we  can add it
delete from VDV_TLVDATA where txeid not in (select txeid from VDV_TXTRANSACTION);

ALTER TABLE VDV_TLVDATA
 ADD FOREIGN KEY (TXEID) 
 REFERENCES VDV_TXTRANSACTION (TXEID)
    ON DELETE CASCADE;
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
