SPOOL db_3_536.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.536', 'FLF', 'Added indexes to SL_TRANSACTIONJOURNAL');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

CREATE INDEX IDX_TXJOURNAL_APPLIEDPASSPROD ON SL_TRANSACTIONJOURNAL
(APPLIEDPASSPRODUCTID);

CREATE INDEX IDX_TXJOURNAL_CARDHOLDER ON SL_TRANSACTIONJOURNAL
(CARDHOLDERID);


---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
