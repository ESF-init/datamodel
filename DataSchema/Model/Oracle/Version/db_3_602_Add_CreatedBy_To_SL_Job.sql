SPOOL db_3_602.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.602', 'DST', 'Add CreatedBy to SL_Job');

ALTER TABLE SL_JOB ADD CreatedBy NVARCHAR2(512);
COMMENT ON COLUMN SL_JOB.CreatedBy IS 'User that created the Job.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
