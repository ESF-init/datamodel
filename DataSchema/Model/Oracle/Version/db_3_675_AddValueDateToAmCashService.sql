SPOOL db_3_675.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.675', 'JED', 'Add column ValueDate to AM_CashServiceData');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE AM_CASHSERVICEDATA
ADD VALUEDATE DATE DEFAULT NULL;

COMMENT ON COLUMN AM_CASHSERVICEDATA.VALUEDATE IS 'Indicates when the value was recorded, or null if unknown.'; 

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
