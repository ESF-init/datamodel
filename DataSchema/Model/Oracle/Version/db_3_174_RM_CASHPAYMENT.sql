SPOOL db_3_174.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.174', 'UB', 'RM_CDSPayment and RM_CASHPAYMENT');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE RM_CDSPAYMENT ADD 
(
   REFERENCETEXT      VARCHAR2(256 BYTE)
)
;


-- =======[ New Tables ]===========================================================================================

CREATE OR REPLACE SYNONYM RM_CASHPAYMENT FOR RM_CDSPAYMENT;

--COMMENT ON TABLE XXX is '';

COMMENT ON COLUMN RM_CDSPAYMENT.REFERENCETEXT IS 'Contains a reference text for the payment.';
--COMMENT ON COLUMN XXX.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
--COMMENT ON COLUMN XXX.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
