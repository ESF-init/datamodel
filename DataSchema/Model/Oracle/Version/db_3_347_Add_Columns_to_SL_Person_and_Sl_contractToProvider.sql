SPOOL db_3_347.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.347', 'ARH', 'Add Columns to SL_PERSON and SL_CONTRACTTOPROVIDER');

-- =======[ Changes to Existing Tables ]===========================================================================

-- SL_PERSON
ALTER TABLE SL_PERSON ADD HASDRIVINGLICENSE 	NUMBER(1)         DEFAULT 0                     NOT NULL;

COMMENT ON COLUMN SL_PERSON.HASDRIVINGLICENSE  IS 'Flag to set if the person has a driving license.';

-- SL_CONTRACTTOPROVIDER
ALTER TABLE SL_CONTRACTTOPROVIDER ADD HASSUBSCRIPTION 	NUMBER(1)         DEFAULT 0                     NOT NULL;
ALTER TABLE SL_CONTRACTTOPROVIDER ADD SUBSCRIPTIONREFERENCE 	NVARCHAR2(30);
ALTER TABLE SL_CONTRACTTOPROVIDER ADD SUBSCRIPTIONVALIDUNTIL 	DATE                    DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL;

COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.HASSUBSCRIPTION  IS 'Flag to set if the person has a subscription.';
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.SUBSCRIPTIONREFERENCE  IS 'Subscription reference by the mobility provider';
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.SUBSCRIPTIONVALIDUNTIL  IS 'The subscription is valid until';


COMMIT;

PROMPT Done!

SPOOL OFF;
