SPOOL db_3_524.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.524', 'EMN', 'Adding Primary key Id to OrderDetailToCard');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_ORDERDETAILTOCARD DROP CONSTRAINT PK_OrderDetailToCard;
DROP INDEX UC_ORDERDETAILTOCARD;

ALTER TABLE SL_ORDERDETAILTOCARD ADD CONSTRAINT UC_OrderDetailToCard UNIQUE (ORDERDETAILID, CARDID, JOBID);
ALTER TABLE SL_ORDERDETAILTOCARD ADD ORDERDETAILTOCARDID NUMBER(18) NOT NULL;
ALTER TABLE SL_ORDERDETAILTOCARD ADD CONSTRAINT PK_ORDERDETAILTOCARD PRIMARY KEY (ORDERDETAILTOCARDID);
COMMENT ON COLUMN SL_ORDERDETAILTOCARD.ORDERDETAILTOCARDID IS 'Unique identifier, which serves as primary key';

-- =======[ Sequences ]===========================================================================================


create sequence SL_ORDERDETAILTOCARD_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
