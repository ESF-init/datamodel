SPOOL db_3_178.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.178', 'SLR', 'updated field length of Payment Token in table RuleViolation.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_RULEVIOLATION
MODIFY PAYMENTTOKEN NVARCHAR2(125);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
