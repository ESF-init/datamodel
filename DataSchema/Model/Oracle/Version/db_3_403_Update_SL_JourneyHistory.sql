SPOOL db_3_403.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.403', 'OMA', 'Update to TransactionJourneyHistory');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

--COMMENT ON TABLE XXX is '';

--COMMENT ON COLUMN XXX.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
--COMMENT ON COLUMN XXX.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
--COMMENT ON COLUMN XXX.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================
DROP VIEW SL_TRANSACTIONJOURNEYHISTORY;

/* Formatted on 06/12/2019 14:32:04 (QP5 v5.300) */
CREATE OR REPLACE FORCE VIEW SL_TRANSACTIONJOURNEYHISTORY
(
    TRANSACTIONID,
    CARDID,
    TRIPDATETIME,
    TYPEID,
    TYPENAME,
    TICKETNUMBER,
    TICKETNAME,
    FROMSTOPNUMBER,
    FROMSTOPNAME,
    TOSTOPNUMBER,
    TOSTOPNAME,
    PRICE,
    CARDBALANCE,
    CARDTRANSACTIONNO,
    CARDREFTRANSACTIONNO,
    TRIPSERIALNO,
    JOURNEYLEGCOUNTER
)
AS
    SELECT DISTINCT TRANSACTIONID,
           tr.CARDID,
           TRIPDATETIME,
           tr.TYPEID,
           CASE
               WHEN tr.TYPEID = 501 THEN 'Journey'
               WHEN tr.TYPEID = 15 THEN 'Journey'
               ELSE TYPENAME
           END
               AS TypeName,
           TikNo,
           tik.Name,
           FROMSTOPNO,
           fromStop.stoplongname,
           tostopno,
           tostopname,
           tr.Price,
           CardBalance,
           tr.cardtransactionno,
           tr.cardreftransactionno,
           tripserialno,
           tr.journeylegcounter
      FROM RM_TRANSACTION  tr
           JOIN rm_devicebookingstate
               ON tr.devicebookingstate =
                      rm_devicebookingstate.devicebookingno
           JOIN rm_shift ON tr.shiftid = rm_shift.shiftid
           JOIN rm_transactiontype tt ON tr.typeid = tt.typeid
           JOIN tm_ticket tik
               ON tr.TARIFFID = tik.tarifid AND tr.tikno = tik.internalnumber
           JOIN tm_tarif ON tik.TARIFID = tm_tarif.TARIFID
           JOIN vario_net ON tm_tarif.NETID = vario_net.NETID 
           LEFT JOIN vario_stop fromStop
               ON     vario_net.netid = fromStop.netid
                  AND tr.fromstopno = fromStop.stopno
           LEFT JOIN (
           SELECT CardID, JourneyLegCounter, CardTransactionNo, innerTR.CardRefTransactionNo, fromstopno as tostopno, innerStop.stoplongname as tostopname
            FROM RM_TRANSACTION innerTR
             JOIN tm_ticket innerTik
               ON innerTR.TARIFFID = innerTik.tarifid AND innerTR.tikno = innerTik.internalnumber
             JOIN tm_tarif ON innerTik.TARIFID = tm_tarif.TARIFID
             JOIN vario_net ON tm_tarif.NETID = vario_net.NETID 
             LEFT JOIN vario_stop innerStop
               ON     vario_net.netid = innerStop.netid
                  AND innerTR.FromStopno = innerStop.stopno
                  ) toStop
            ON toStop.CardID = tr.CARDID AND toStop.JOURNEYLEGCOUNTER = tr.JOURNEYLEGCOUNTER
            AND tr.CARDTRANSACTIONNO != toStop.CARDTRANSACTIONNO
            AND toStop.Cardreftransactionno != 0
            AND (toStop.CARDREFTRANSACTIONNO = tr.CARDTRANSACTIONNO OR toStop.CARDREFTRANSACTIONNO = tr.CARDREFTRANSACTIONNO)
     WHERE     rm_shift.shiftstateid < 30
           AND tr.cancellationid = 0
           AND rm_devicebookingstate.isbooked = 1
           AND tr.typeid NOT IN (5, 502);

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
