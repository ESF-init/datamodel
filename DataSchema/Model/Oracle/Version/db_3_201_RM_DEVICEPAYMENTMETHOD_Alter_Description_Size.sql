SPOOL db_3_201.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.201', 'EPA', 'Alter description row size from 20 chars to 40 chars.');


-- =======[ Changes to Existing Tables ]===========================================================================

alter table RM_DEVICEPAYMENTMETHOD modify description VARCHAR2(40);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;