SPOOL db_3_359.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.359', 'MKD', 'Refactored audit registers. Unpivoting the registers into new table');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

create table SL_AUDITREGISTER_TEMP as
    SELECT *
    FROM SL_AUDITREGISTER;

Drop Table SL_AUDITREGISTER;

-- =======[ New Tables ]===========================================================================================
CREATE TABLE SL_AUDITREGISTER
(
    AUDITREGISTERID            NUMBER(18,0) NOT NULL,
    SHIFTID                    VARCHAR2(32) NOT NULL,
    REPORTDATETIME            DATE DEFAULT sysdate NOT NULL,
    LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
    LastModified DATE DEFAULT sysdate NOT NULL, 
    TransactionCounter INTEGER NOT NULL,
    CONSTRAINT PK_AUDITREGISTERID PRIMARY KEY (AUDITREGISTERID),
    CONSTRAINT FK_AUDITREGISTER_SHIFTID FOREIGN KEY (SHIFTID) REFERENCES RM_SHIFTINVENTORY(HASHVALUE),
    CONSTRAINT UK_AUDITREGISTER_SHIFTID UNIQUE (SHIFTID)
);

COMMENT ON TABLE SL_AUDITREGISTER is 'Table to gather statistical informations about reporting source. See SL_AUDITREGISTERVALUE for audit register values.';
COMMENT ON COLUMN SL_AUDITREGISTER.AUDITREGISTERID is 'Unique ID of entry.';
COMMENT ON COLUMN SL_AUDITREGISTER.SHIFTID is 'Shift id of reporting source.';
COMMENT ON COLUMN SL_AUDITREGISTER.REPORTDATETIME is 'Time and date of reported incident.';
COMMENT ON COLUMN SL_AUDITREGISTER.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_AUDITREGISTER.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_AUDITREGISTER.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

CREATE TABLE SL_AUDITREGISTERVALUE
(
    AUDITREGISTERVALUEID    NUMBER(18,0) NOT NULL,
    AUDITREGISTERID            NUMBER(18,0) NOT NULL,
    REGISTERVALUETYPE        NUMBER(9,0) NOT NULL,
    REGISTERVALUE             VARCHAR2(50) NOT NULL,
    LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
    LastModified DATE DEFAULT sysdate NOT NULL, 
    TransactionCounter INTEGER NOT NULL,
    CONSTRAINT PK_AUDITREGISTERVALUEID 
        PRIMARY KEY (AUDITREGISTERVALUEID),
    CONSTRAINT FK_AUDITREGISTERID 
        FOREIGN KEY (AUDITREGISTERID) 
        REFERENCES SL_AUDITREGISTER(AUDITREGISTERID)
        ON DELETE CASCADE
);    

COMMENT ON TABLE SL_AUDITREGISTERVALUE is 'Statistics audit values. See SL_AUDITREGISTER for reporting source.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUE.AUDITREGISTERVALUEID is 'Unique ID of entry.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUE.AUDITREGISTERID is 'ID of reporting source.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUE.REGISTERVALUETYPE is 'Type of audit entry. See SL_AUDITREGISTERVALUETYPE enumeration for valid types.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUE.REGISTERVALUE is 'Value of audit entry.';


CREATE TABLE SL_AUDITREGISTERVALUETYPE
(
    ENUMERATIONVALUE        NUMBER(9,0) NOT NULL,
    LITERAL                    NVARCHAR2(50) NOT NULL,
    DESCRIPTION                NVARCHAR2(250) NOT NULL,
    CONSTRAINT PK_AUDITREGISTERVALUETYPE PRIMARY KEY (ENUMERATIONVALUE)
);

COMMENT ON TABLE SL_AUDITREGISTERVALUETYPE is 'Enumeration of all available audit register types.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUETYPE.ENUMERATIONVALUE is 'Enumeration value of audit register value type.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUETYPE.LITERAL is 'Short name of enumeration value.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUETYPE.DESCRIPTION is 'Detail name of enumeration value.';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (1, 'TotalValidations', 'Total validation counts');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (2, 'RiderClassCount', 'Rider class count');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (3, 'ApprovedTransactionsAbsolute', 'Count of approved transactions (absolute)');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (4, 'DeniedTransactionsAbsolute', 'Count of denied transactions (absolute)');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (5, 'ReadFailuresAbsolute', 'Count of read failures');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (6, 'ValidatedFareProducts', 'Fare products validated');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (7, 'DeductedFareValues', 'Fare values deducted');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (8, 'NewFareMediaAbsolute', 'New fare media issued');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (9, 'FareProductSalesAbsolute', 'Fare products sold');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (10, 'StoredValueLoadedAbsolute', 'Stored value loaded (absolute)');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (11, 'AccountInquiries', 'Account inquiries');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (12, 'CashTransactions', 'Cash transactions by amount and denomination (relative value, amount per shift)');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (1314, 'CardTransactions', 'Credit (13) and debit (14) card transactions by amount');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (15, 'ApprovedAndDeniedTransactionsAbsolute', 'Count of approved and denied transactions');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (161, 'AllTransactionsCount', 'The total count of all transactions completed since data was last uploaded to the back office');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (162, 'AllTransactionsValue', 'The total value of all transactions completed since data was last uploaded to the back office');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (17, 'SuccessDateTime', 'The date and time of the last successful data upload to the back office');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (18, 'ApprovedTransactionsRelative', 'Count of approved transactions since data was last uploaded');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (19, 'DeniedTransactionsRelative', 'Count of denied transactions since data was last uploaded');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (20, 'ReadFailuresRelative', 'Count of read failures since data was last uploaded');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (21, 'NewFareMediaRelative', 'New fare media issued since data was las uploaded');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (22, 'FareProductSalesRelative', 'Fare products sold since data was last uploaded');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (23, 'StoredValueLoadedRelative', 'Stored values loaded since data was last uploaded');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (24, 'ApprovedAndDeniedTransactionsRelative', 'Count of approved and denied transactions since data was last uploaded');

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_AUDITREGISTERVALUE');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_AUDITREGISTER', 'SL_AUDITREGISTERVALUE');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

---End adding schema changes 

-- =======[ Data Migration ]=================================================================================================

-- insert data into new audit register table
INSERT INTO SL_AUDITREGISTER
  SELECT AUDITREGISTERID,
         SHIFTID,
         REPORTDATETIME,
         LastUser,
         LastModified,
         TransactionCounter
FROM (
  --doing subselect with OVER PARTITON BY ... to get only the latest audit register when there are multiple with the same shift id => shift id is unique
         SELECT AUDITREGISTERID,
                First_Value(AUDITREGISTERID) OVER (PARTITION BY SHIFTID order by REPORTDATETIME desc) FirstID,
                SHIFTID,
                REPORTDATETIME,
                LastUser,
                LastModified,
                TransactionCounter
         FROM SL_AUDITREGISTER_TEMP
     )
WHERE AUDITREGISTERID = FirstID;

-- unpivoting audit register value columns from old audit register table and inserting into new value table
INSERT INTO SL_AUDITREGISTERVALUE
WITH OldAudits AS (
   SELECT  AUDITREGISTERID,
           --convert all audit register value columns to varchar so that they have the same datatype
           TO_CHAR(VALIDATIONS) as VALIDATIONS,
           TO_CHAR(RIDERCLASSCOUNTS) as RIDERCLASSCOUNTS,
           TO_CHAR(APPROVEDTRANSACTIONS) as APPROVEDTRANSACTIONS,
           TO_CHAR(DENIEDTRANSACTIONS) as DENIEDTRANSACTIONS,
           TO_CHAR(READFAILURES) as READFAILURES,
           TO_CHAR(VALIDATEDFAREPRODUCTS) as VALIDATEDFAREPRODUCTS,
           TO_CHAR(DEDUCTEDFAREPRODUCTS) as DEDUCTEDFAREPRODUCTS,
           TO_CHAR(NEWFAREMEDIA) as NEWFAREMEDIA,
           TO_CHAR(FAREPRODUCTSALES) as FAREPRODUCTSALES,
           TO_CHAR(STOREDVALUELOADED) as STOREDVALUELOADED,
           TO_CHAR(ACCOUNTINQUIRIES) as ACCOUNTINQUIRIES,
           TO_CHAR(CASHTRANSACTIONS) as CASHTRANSACTIONS,
           TO_CHAR(CARDTRANSACTIONS) as CARDTRANSACTIONS,
           TO_CHAR(TRANSACTIONS) as TRANSACTIONS,
           TO_CHAR(TOTALCOUNTS) as TOTALCOUNTS,
           TO_CHAR(TOTALVALUES) as TOTALVALUES,
           TO_CHAR(SUCCESSDATETIME, 'YYYY-MM-DD"T"HH24:MI:SS.FF3"Z"') as SUCCESSDATETIME, --datetime pattern in UTC following ISO 8601 (similar to "o" pattern .net)
           LASTUSER,
           LASTMODIFIED,
           TRANSACTIONCOUNTER,
           SHIFTID
  FROM SL_AUDITREGISTER_TEMP
  WHERE AUDITREGISTERID IN (
      --doing subselect with OVER PARTITON BY ... to get only the latest audit register when there are multiple with the same shift id => shift id is unique
      SELECT First_Value(AUDITREGISTERID) OVER (PARTITION BY SHIFTID order by REPORTDATETIME desc) FirstID
      FROM SL_AUDITREGISTER_TEMP
      )
)
SELECT
       SL_AUDITREGISTERVALUE_SEQ.nextval AUDITREGISTERVALUEID,
       AUDITREGISTERID,
       REGISTERVALUETYPE,
       REGISTERVALUE,
       LASTUSER,
       LASTMODIFIED,
       TRANSACTIONCOUNTER
FROM OldAudits
unpivot
(
  RegisterValue
    for RegisterValueType in (
        VALIDATIONS as 1,
        RIDERCLASSCOUNTS as 2,
        APPROVEDTRANSACTIONS as 19,
        DENIEDTRANSACTIONS as 20,
        READFAILURES as 21,
        VALIDATEDFAREPRODUCTS as 6,
        DEDUCTEDFAREPRODUCTS as 7,
        NEWFAREMEDIA as 22,
        FAREPRODUCTSALES as 23,
        STOREDVALUELOADED as 24,
        ACCOUNTINQUIRIES as 11,
        CASHTRANSACTIONS as 12,
        CARDTRANSACTIONS as 1314,
        TRANSACTIONS as 25,
        TOTALCOUNTS as 161,
        TOTALVALUES as 162,
        SUCCESSDATETIME as 17
        )
);

DROP TABLE SL_AUDITREGISTER_TEMP;
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
