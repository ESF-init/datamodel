SPOOL db_3_202.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.202', 'LWU', 'Added UNITID to GM_GOODSEXCHANGE.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE GM_GOODSEXCHANGE
ADD UNITID NUMBER (10);

-- =======[ Commit ]===============================================================================================

COMMENT ON COLUMN GM_GOODSEXCHANGE.UNITID IS 'Reference to UM_UNIT';
COMMIT;

PROMPT Done!

SPOOL OFF;