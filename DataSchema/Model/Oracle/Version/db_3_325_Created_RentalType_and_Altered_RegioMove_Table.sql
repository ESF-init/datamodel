SPOOL db_3_325.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.325', 'HNI', 'Created RentalType and Altered RegioMove Table');

-- Start adding schema changes here

-- CREATE SL_RENTALTYPE
CREATE TABLE SL_RENTALTYPE
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL
);

COMMENT ON TABLE SL_RENTALTYPE IS 'Enumeration of all valid rental types in the system.';
COMMENT ON COLUMN SL_RENTALTYPE.ENUMERATIONVALUE IS 'Unique id of the rental type';
COMMENT ON COLUMN SL_RENTALTYPE.LITERAL IS 'Name of the rental type; used internally';
COMMENT ON COLUMN SL_RENTALTYPE.DESCRIPTION IS 'Description of the rental type. Used in the UI to represent the rental type';

INSERT INTO SL_RENTALTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (0, 'Inactive', 'Rental is inactive.');
INSERT INTO SL_RENTALTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (1, 'Active', 'Rental is still active.');
INSERT INTO SL_RENTALTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (2, 'Paused', 'Rental is paused.');
INSERT INTO SL_RENTALTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (3, 'Returned', 'Rental is finished.');

ALTER TABLE SL_RENTALTYPE
 ADD CONSTRAINT PK_RENTALTYPE
  PRIMARY KEY
  (ENUMERATIONVALUE)
  ENABLE VALIDATE;

-- =======[ Changes to Existing Tables ]===========================================================================

-- SL_BOOKINGITEM

ALTER TABLE SL_BOOKINGITEM ADD RENTALTYPE NUMBER(9);
COMMENT ON COLUMN SL_BOOKINGITEM.RENTALTYPE IS 'Rental state of the booking item. References SL_RENTALTYPE.ENUMERATIONVALUE.';

-- SL_BOKINGTYPE

UPDATE SL_BOOKINGTYPE
SET 
    LITERAL = 'PriceRequest',
    Description = 'Default value on creation and price request.'
WHERE 
    ENUMERATIONVALUE = 0;    
UPDATE SL_BOOKINGTYPE
SET 
    LITERAL = 'Active',
    Description = 'Booking is still Active.'
WHERE 
    ENUMERATIONVALUE = 1;
UPDATE SL_BOOKINGTYPE
SET 
    LITERAL = 'Pending',
    Description = 'Waiting for an answer.'
WHERE 
    ENUMERATIONVALUE = 2;   
UPDATE SL_BOOKINGTYPE
SET 
    LITERAL = 'Cleared',
    Description = 'Booking is completed.'
WHERE 
    ENUMERATIONVALUE = 3;   
UPDATE SL_BOOKINGTYPE
SET 
    LITERAL = 'Error',
    Description = 'Error on request.'
WHERE 
    ENUMERATIONVALUE = 4;   


COMMIT;

PROMPT Done!

SPOOL OFF;
