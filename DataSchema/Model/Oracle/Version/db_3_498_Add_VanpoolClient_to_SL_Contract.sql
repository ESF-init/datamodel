SPOOL db_3_498.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.498', 'MMB', 'add VanpoolClientID to SL_CONTRACT');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_CONTRACT ADD VANPOOLCLIENTID NUMBER(10); 
ALTER TABLE SL_CONTRACT ADD CONSTRAINT FK_CONTRACT_VANPOOLCLIENT FOREIGN KEY (VANPOOLCLIENTID) REFERENCES VARIO_CLIENT(CLIENTID);

COMMENT ON COLUMN SL_CONTRACT.VANPOOLCLIENTID IS 'Reference of the vario client id';



-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;

