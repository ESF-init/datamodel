SPOOL db_3_197.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.197', 'JSB', 'Change default value of column UM_DEVICE.LASTDATA to to_timestamp');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE UM_DEVICE MODIFY (LASTDATA DEFAULT TO_TIMESTAMP('01.01.0001', 'DD/MM/YYYY'));

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
