SPOOL db_3_002.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.002', 'ULB', 'Added contracttype to SL_ACCOUNTINGMODE');

ALTER TABLE SL_ACCOUNTINGMODE
 ADD (contracttype  NUMBER(9)                       DEFAULT 0                     NOT NULL);
COMMIT;

PROMPT Done!

SPOOL OFF
