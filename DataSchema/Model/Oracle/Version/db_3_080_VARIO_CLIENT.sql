SPOOL db_3_080.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.080', 'SLR', 'Added column to VARIO_CLIENT.');

-- =======[ New Tables ]===========================================================================================

alter table VARIO_CLIENT add DefaultLineNoBundle NVARCHAR2(100);

COMMENT ON COLUMN VARIO_CLIENT.DefaultLineNoBundle IS 'Contains a default definition of line no bundle used by sales export.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
