SPOOL db_3_220.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.220', 'PCO', 'Recreating SL_TRANSACTIONHISTORY view.');

DROP VIEW SL_TRANSACTIONHISTORY;

CREATE OR REPLACE FORCE VIEW SL_TRANSACTIONHISTORY
(
    CARDID,
    CARDTRANSACTIONNO,
    CARDBALANCE,
    CARDCREDIT,
    CLIENTID,
    DEVICEBOOKINGSTATE,
    DISTANCE,
    EXTTIKNAME,
    FARESTAGE,
    FACTOR,
    FROMTARIFFZONE,
    LINENAME,
    PAYCARDNO,
    PRICE,
    ROUTENO,
    TRANSACTIONID,
    TRIPDATETIME,
    IMPORTDATETIME,
    TYPEID,
    TYPENAME,
    VALUEOFRIDE,
    FROMSTOPNO,
    STOPCODE,
    DIRECTION,
    TICKETNAME,
    DEVICENO,
    LOCATIONNO,
    STOPNAME,
    VALIDFROM,
    VALIDUNTIL,
    DEVICEPAYMENTMETHOD,
    DEBTORNO,
    REMAININGRIDES,
    ORGANIZATIONID,
    COMPANYNAME,
    SALESCHANNEL
)
AS
      SELECT DISTINCT rm_transaction.cardid,
                      rm_transaction.CARDTRANSACTIONNO,
                      rm_transaction.cardbalance,
                      rm_transaction.cardcredit,
                      rm_transaction.clientid,
                      rm_transaction.devicebookingstate,
                      rm_transaction.distance,
                      rm_transaction.exttikname,
                      rm_transaction.farestage,
                      rm_transaction.factor,
                      rm_transaction.fromtariffzone,
                      rm_transaction.linename,
                      rm_transaction.paycardno,
                      rm_transaction.price,
                      rm_transaction.routeno,
                      rm_transaction.transactionid,
                      rm_transaction.tripdatetime,
                      rm_transaction.importdatetime,
                      rm_transaction.typeid,
                      rm_transactiontype.typename,
                      rm_transaction.valueofride,
                      rm_transaction.fromstopno,
                      vario_stop.stopcode,
                      rm_transaction.direction,
                      tm_ticket.NAME AS ticketname,
                      rm_transaction.deviceno,
                      rm_shift.locationno,
                      vario_stop.stopname,
                      rm_transaction.validfrom,
                      rm_transaction.validuntil,
                      rm_transaction.devicepaymentmethod,
                      rm_shift.debtorno,
                      rm_transaction.remainingrides,
                      sl_product.organizationid,
                      vario_client.COMPANYNAME,
                      vario_deviceclass.NAME
        FROM rm_transaction
             JOIN rm_devicebookingstate
                 ON rm_transaction.devicebookingstate =
                        rm_devicebookingstate.devicebookingno
             JOIN rm_transactiontype
                 ON rm_transaction.typeid = rm_transactiontype.typeid
             JOIN rm_shift ON rm_transaction.shiftid = rm_shift.shiftid
             JOIN tm_ticket
                 ON     rm_transaction.TARIFFID = tm_ticket.tarifid
                    AND rm_transaction.tikno = tm_ticket.internalnumber
             JOIN tm_tarif ON tm_ticket.TARIFID = tm_tarif.TARIFID
             JOIN vario_net ON tm_tarif.NETID = vario_net.NETID
             JOIN sl_card ON rm_transaction.cardid = sl_card.cardid
             JOIN vario_client
                 ON rm_transaction.CLIENTID = vario_client.clientid
             JOIN vario_deviceclass
                 ON rm_transaction.SALESCHANNEL =
                        vario_deviceclass.DEVICECLASSID
             LEFT JOIN sl_product
                 ON     rm_transaction.cardid = SL_PRODUCT.CARDID
                    AND (   RM_TRANSACTION.TICKETINSTANCEID =
                                SL_PRODUCT.INSTANCENUMBER
                         OR rm_transaction.ticketinstanceid = 0)
             LEFT JOIN vario_stop
                 ON     vario_net.netid = vario_stop.netid
                    AND rm_transaction.fromstopno = vario_stop.stopno
       WHERE     rm_shift.shiftstateid < 30
             AND rm_transaction.cancellationid = 0
             AND rm_devicebookingstate.isbooked = 1
    ORDER BY rm_transaction.cardid ASC, rm_transaction.tripdatetime DESC;