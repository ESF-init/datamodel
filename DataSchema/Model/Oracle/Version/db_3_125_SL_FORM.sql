SPOOL db_3_125.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.125', 'SLR', 'Alter SL_FORM table');


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_FORM ADD DOCUMENTTYPE NUMBER(9,0);
ALTER TABLE SL_FORM ADD POSTINGSCOPE NUMBER(9,0);

COMMENT ON COLUMN SL_FORM.DOCUMENTTYPE IS 'Optional reference to enum SL_DOCUMENTTYPE.';
COMMENT ON COLUMN SL_FORM.POSTINGSCOPE IS 'Optional reference to enum SL_POSTINGSCOPE.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
