SPOOL db_3_282.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.282', 'HNI', 'Added missing table and column comments');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
COMMENT ON TABLE SL_EntitlementToProduct is 'Mapping for the entilements and products';
COMMENT ON TABLE TM_TICKET_EXPORTINFO is 'Assignment table from TM_TICKET to TM_EXPORTINFO (m:n). Defines one or more export information for each ticket.';
COMMENT ON TABLE GM_DEBTORTOKEN IS 'BSAG: MDE-API: Table to log login of Debtors to Service';
COMMENT ON TABLE GM_EXCHANGESIGNATURES IS 'BSAG: MDE-API: BlobFiles for signature on issuing Paperrolls';

COMMENT ON COLUMN GM_GOODSEXCHANGE.ADDRESSEE is 'Technical field: addressee of the booking transaction.';
COMMENT ON COLUMN PP_BILL.BUSINESSVOLUMELAST3MONTH is 'Turnover amount of the last 3 month prior to this invoice.';
COMMENT ON COLUMN PP_CARD.CARDLOAD is 'Number of loaded products an the smartcard.';
COMMENT ON COLUMN SL_AUDITREGISTER.SHIFTID is 'Shift id of reporting source.';
COMMENT ON COLUMN SL_PAYMENTJOURNAL.TRANSACTIONJOURNALID is 'Deprecated.';
COMMENT ON COLUMN SL_PRODUCTRELATION.FARESTAGE is 'the selected fare stage of the relation.';
COMMENT ON COLUMN SL_WORKITEM.TYPEDISCRIMINATOR is 'Technical field that allows to select work items of a sepcial type.';
COMMENT ON COLUMN VARIO_NET.TIMETABLEVERSION is 'Version number of a time table.';
COMMENT ON COLUMN DM_DEBTORLOGINTODEVICE.CASHNO IS 'Number of the Cash (non referential)';
COMMENT ON COLUMN DM_DEBTORLOGINTODEVICE.DEVICENO IS 'Number of the Device (non referential)';
COMMENT ON COLUMN DM_DEBTORLOGINTODEVICE.TRAININGFLAG IS 'identifies if the entry is a training entry';
COMMENT ON COLUMN DM_DEBTORLOGINTODEVICE.SALESCHANNEL IS 'Number of the Saleschannel (non referential)';
COMMENT ON COLUMN DM_DEBTORLOGINTODEVICE.DEBTORNO IS 'Number of the Debtor (non referential)';
COMMENT ON COLUMN DM_DEBTORLOGINTODEVICE.USERSHORTNAME IS 'Shortname of the User (non referential)';
COMMENT ON COLUMN DM_DEBTORLOGINTODEVICE.INSERTDATETIME IS 'log the time the entry was entered';
COMMENT ON COLUMN DM_DEBTORLOGINTODEVICE.KEYID IS 'Unique Identifier for table';
COMMENT ON COLUMN DM_DEBTORLOGINTODEVICE.LOCATIONNO IS 'Number of the Location (non referntial)';
COMMENT ON COLUMN GM_DEBTORTOKEN.TOKEN IS 'generated unique Token for the bsag webservice to call Request';
COMMENT ON COLUMN GM_DEBTORTOKEN.USERID IS 'Vario User that was used in the process of logging into the Webservice';
COMMENT ON COLUMN GM_DEBTORTOKEN.CREATIONDATE IS 'Creationdate of the entry';
COMMENT ON COLUMN GM_EXCHANGESIGNATURES.SIGNATUREID IS 'Unique Identifier for table';
COMMENT ON COLUMN GM_EXCHANGESIGNATURES.SIGNATUREBLOB IS 'the signature picture as blob';
COMMENT ON COLUMN SL_DUNNINGTOINVOICE.LASTUSER is ' Shortcut of the last user of the table column.';
COMMENT ON COLUMN SL_DUNNINGTOINVOICE.LASTMODIFIED is ' Date of the last use.';
COMMENT ON COLUMN SL_DUNNINGTOINVOICE.Description is 'Possible description.';
COMMENT ON COLUMN SL_DUNNINGTOINVOICE.DUNNINGPROCESSID is 'Reference to dunning process  in SL_DUNNINGPROCESS.';
COMMENT ON COLUMN SL_DUNNINGTOINVOICE.TRANSACTIONCOUNTER is 'Transaction counter';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.MANUALLYBOOKED is 'A manually debited return debit note.';
COMMENT ON COLUMN GM_GOODSEXCHANGE.SIGNATUREID IS 'the Id of an signature';
COMMENT ON COLUMN SL_DUNNINGPROCESS.DESCRIPTION IS 'Description of a dunning process';
COMMENT ON COLUMN SL_ATTRIBUTE.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_ATTRIBUTE.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_ATTRIBUTE.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_ATTRIBUTEVALUE.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_ATTRIBUTEVALUE.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_ATTRIBUTEVALUE.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_ATTRIBUTEVALUETOPERSON.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_ATTRIBUTEVALUETOPERSON.TRANSACTIONCOUNTER IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_ATTRIBUTEVALUETOPERSON.LASTMODIFIED IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN UM_DEVICEREPAIRHISTORY.DESCRIPTION IS 'Description of repair history ';
COMMENT ON COLUMN UM_DEVICEREPAIRHISTORY.DEVICEID IS 'Id of the device ';
COMMENT ON COLUMN UM_DEVICEREPAIRHISTORY.DEVICEREPAIRHISTORYID IS 'Unique indentifier of the table  ';
COMMENT ON COLUMN PROCCESSEDFILES.DEVICEID IS 'Id of the device ';
COMMENT ON COLUMN PROCCESSEDFILES.FILENAME IS 'Name of the processed file';
---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
