SPOOL db_3_427.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.427', 'BTS', 'ALTER_TABEL SL_CARDHOLDERORGANIZATION');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_CARDHOLDERORGANIZATION
 ADD CONSTRAINT SL_CARDHOLDERORGANIZATION_R02 
  FOREIGN KEY (ORGANIZATIONID) 
  REFERENCES SL_ORGANIZATION (ORGANIZATIONID)
  ENABLE VALIDATE;
  
 ALTER TABLE SL_CARDHOLDERORGANIZATION
 ADD CONSTRAINT SL_CARDHOLDERORGANIZATION_R01 
  FOREIGN KEY (CARDHOLDERID) 
  REFERENCES SL_PERSON (PERSONID)
  ENABLE VALIDATE;

-- =======[ TRIGGER ]===========================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_CARDHOLDERORGANIZATION');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
