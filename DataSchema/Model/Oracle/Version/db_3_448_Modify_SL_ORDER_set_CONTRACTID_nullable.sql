SPOOL db_3_448.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.448', 'mfa', 'Modify SL_ORDER set CONTRACTID nullable');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_ORDER MODIFY CONTRACTID NULL;

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
