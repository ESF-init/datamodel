SPOOL db_3_669.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.669', 'MFA', 'View for getting pass revenue recognitions for accounting.');

-- Start adding schema changes here

-- =======[ Views ]================================================================================================
CREATE OR REPLACE FORCE VIEW "ACC_PASSREVENUERECOGNITIONS"("PASSTICKETID", "PASSTRANSITACCOUNTID", "PASSREVENUESETTLEMENTID", "TAPREVENUESETTLEMENTID", "TAPREVENUERECOGNITIONID", 
    "TAPREVENUERECOGNITIONAMOUNT", "TAPTRANSACTIONJOURNALID", "TAPTRANSACTIONTYPE", "TAPBOARDINGGUID", "TAPPURSECREDIT", "TAPORIGINALSINGLEFARE", "TAPBASEFARE", "TAPOPERATORID",
    "TAPTICKETNUMBER", "TAPAPPLIEDPASSTICKETID", "TAPDEVICETIME", "TAPTICKETID", "TAPCANCELLATIONREFERENCEGUID", "TAPCANCELLATIONREFERENCE") AS
select passrr.ticketid, passrr.transitaccountid, passrr.revenuesettlementid, taprr.revenuesettlementid, taprr.revenuerecognitionid, taprr.amount, 
    taprr.transactionjournalid, taptj.transactiontype, taprr.boardingguid, taptj.pursecredit, taptj.originalsinglefare, taprr.basefare, 
    taprr.operatorid, taprr.ticketnumber, taprr.appliedpassticketid, taptj.devicetime, 
    taptj.ticketid, taptj.cancellationreferenceguid, taptj.cancellationreference
from acc_revenuerecognition passrr 
left join acc_revenuerecognition taprr on taprr.appliedpassticketid = passrr.ticketid and taprr.transitaccountid = passrr.transitaccountid
join sl_transactionjournal taptj on taptj.transactionjournalid = taprr.transactionjournalid
where passrr.appliedpassticketid is null and passrr.boardingguid is null and passrr.ticketid is not null 
    and passrr.includeinsettlement = 1 and taprr.includeinsettlement = 1
    and (passrr.revenuesettlementid is null or (passrr.revenuesettlementid is not null and taprr.revenuesettlementid is null));

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
