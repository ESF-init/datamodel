SPOOL db_3_394.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.394', 'MKD', 'Adding table and view for ServerResponseTime aggregation');

-- =======[ New Tables ]===========================================================================================

create table PERF_SrtDataAggregation (
    AggregationID NUMBER (18) PRIMARY KEY,
    AggregationFrom DATE NOT NULL,
    AggregationTo DATE NOT NULL,
    AggregationType  NUMBER(9) NOT NULL,
    PerformanceID NUMBER(18) NOT NULL,
    EventTime TIMESTAMP NOT NULL,
    IndicatorID NUMBER(18) NOT NULL,
    Hostname NVARCHAR2(200),
    SalesChannelID NUMBER(18),
    DeviceNumber NUMBER(9),
    ShiftBegin TIMESTAMP,
    NumberOfAuthorizedTx NUMBER(9),
    Duration NUMBER(9,3),
    Quantity NUMBER(9),
    NumberAboveThreshold NUMBER(9),
    TotalNumber NUMBER(9) NOT NULL
    );

COMMENT ON TABLE PERF_SrtDataAggregation is 'Joined Data from PERF_PERFORMANCE, PERF_ADDITIONALDATA and SL_TRANSACTIONJOURNAL (see View_ServerResponseTimeData) gets aggregated regulary and is saved in this table for usage on the server response time kpi report.';

COMMENT ON COLUMN PERF_SrtDataAggregation.AggregationID IS 'Unique ID.';
COMMENT ON COLUMN PERF_SrtDataAggregation.AggregationFrom IS 'Start timestamp of the aggregation window.';
COMMENT ON COLUMN PERF_SrtDataAggregation.AggregationTo IS 'Start timestamp of the aggregation window.';
COMMENT ON COLUMN PERF_SrtDataAggregation.AggregationType IS 'Type of the aggregation (enum value from SL_AGGREGATIONTYPE).';
COMMENT ON COLUMN PERF_SrtDataAggregation.PerformanceID IS 'Aggrgation (MIN) of PERF_PERFORMANCE.PerformanceId';
COMMENT ON COLUMN PERF_SrtDataAggregation.EventTime IS 'Aggrgation (MIN) of PERF_PERFORMANCE.EventTime';
COMMENT ON COLUMN PERF_SrtDataAggregation.IndicatorID IS 'Group by of PERF_PERFORMANCE.IndicatorId';
COMMENT ON COLUMN PERF_SrtDataAggregation.Hostname IS 'Group by of PERF_ADDITIONALDATA.Hostname';
COMMENT ON COLUMN PERF_SrtDataAggregation.SalesChannelID IS 'Group by of SL_TRANSACTIONJOURNAL.SalesChannelId';
COMMENT ON COLUMN PERF_SrtDataAggregation.DeviceNumber IS 'Group by of SL_TRANSACTIONJOURNAL.DeviceNumber';
COMMENT ON COLUMN PERF_SrtDataAggregation.ShiftBegin IS 'Group by of SL_TRANSACTIONJOURNAL.ShiftBegin';
COMMENT ON COLUMN PERF_SrtDataAggregation.NumberOfAuthorizedTx IS 'Aggrgation (SUM) of SL_TRANSACTIONJOURNAL.RESULTSOURCE';
COMMENT ON COLUMN PERF_SrtDataAggregation.Duration IS 'Aggrgation (AVG) of PERF_PERFORMANCE.Duration';
COMMENT ON COLUMN PERF_SrtDataAggregation.Quantity IS 'Aggrgation (SUM) of PERF_PERFORMANCE.Quantity';
COMMENT ON COLUMN PERF_SrtDataAggregation.NumberAboveThreshold IS 'Aggrgation (SUM) of PERF_PERFORMANCE.Duration > 100';
COMMENT ON COLUMN PERF_SrtDataAggregation.TotalNumber IS 'Total number of aggregate rows for each group.';

-- =======[ Views ]================================================================================================
create or replace view View_ServerResponseTimeData
AS
(select p.PERFORMANCEID PerformanceId,
       p.EVENTTIME,
       p.INDICATORID,
       a.HOSTNAME,
       t.SALESCHANNELID,
       CAST(t.DEVICENUMBER as NUMBER(9)) DeviceNumber,
       t.SHIFTBEGIN,
       CAST(t.RESULTSOURCE as NUMBER(9)) NumberOfAuthorizedTx,
       CAST(p.DURATION as NUMBER(9,3)) Duration,
       CAST(p.QUANTITY as NUMBER(9)) Quantity,
       CAST(
           case when p.DURATION > 150 Then 1 ELSE 0 End
        as NUMBER(9)) NumberAboveThreshold,
       CAST(1 as NUMBER(9)) TotalNumber,
       CAST(0 as NUMBER(1)) IsAggregated,
       CAST(0 as NUMBER(9)) AggregationType
from PERF_PERFORMANCE p inner join PERF_ADDITIONALDATA a on p.PERFORMANCEID = a.PERFORMANCEID
     left outer join SL_TRANSACTIONJOURNAL t on a.TRANSACTIONID = t.TRANSACTIONID
Union
Select PerformanceId,
    EventTime,
    IndicatorID,
    Hostname,
    SalesChannelId,
    DeviceNumber,
    ShiftBegin,
    NumberOfAuthorizedTx,
    Duration,
    Quantity,
    NumberAboveThreshold,
    TotalNumber,
    CAST(1 as NUMBER(1)) IsAggregated,
    AggregationType
from PERF_SRTDataAggregation p);

-- =======[ Sequences ]============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'PERF_SRTDataAggregation');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
