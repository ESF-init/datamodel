SPOOL db_3_489.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.489', 'MEA', 'added table SL_CERTIFICATETYPE');


-- =======[ New Tables ]===========================================================================================
CREATE TABLE SL_CERTIFICATETYPE
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL
);


CREATE UNIQUE INDEX PK_CERTIFICATETYPE ON SL_CERTIFICATETYPE
(ENUMERATIONVALUE);


ALTER TABLE SL_CERTIFICATETYPE ADD (
  CONSTRAINT PK_CERTIFICATETYPE
 PRIMARY KEY
 (ENUMERATIONVALUE)
    USING INDEX );

COMMENT ON TABLE  SL_CERTIFICATETYPE is ' Enumeration containing certificate types used by CM and API';
COMMENT ON COLUMN SL_CERTIFICATETYPE.ENUMERATIONVALUE   IS 'Contains the Enum value';
COMMENT ON COLUMN SL_CERTIFICATETYPE.LITERAL            IS 'Contains the literal';
COMMENT ON COLUMN SL_CERTIFICATETYPE.DESCRIPTION        IS 'Contains the description';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
