SPOOL db_3_491.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.491', 'EMN', 'Add new job type OrderFulfillment');

-- =======[ Enumerations ]================================================================================================
INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (19, 'OrderFulfillment', 'Fulfill order item');


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;