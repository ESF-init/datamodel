SPOOL db_3_400.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.400', 'HNI', 'Update SL_ContractToProvider');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CONTRACTTOPROVIDER
MODIFY SUBSCRIPTIONREFERENCE NVARCHAR2(50);

---End adding schema changes 
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
