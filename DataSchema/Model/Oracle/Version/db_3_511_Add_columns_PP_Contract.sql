SPOOL db_3_511.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.511', 'ARH', 'Add columns in PP_CONTRACT');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE PP_CONTRACT ADD (VERIFICATIONTOKEN  NVARCHAR2(100));
ALTER TABLE PP_CONTRACT ADD (VERIFICATIONTOKENEXPIRYTIME DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL);
ALTER TABLE PP_CONTRACT ADD (ISACTIVATEDWITHEMAIL  NUMBER(1, 0) DEFAULT 0 NOT NULL);

COMMENT ON COLUMN PP_CONTRACT.VERIFICATIONTOKEN IS 'Token for email verification when registering a contract.';
COMMENT ON COLUMN PP_CONTRACT.VERIFICATIONTOKENEXPIRYTIME IS 'Expiry time for the verification token.';
COMMENT ON COLUMN PP_CONTRACT.ISACTIVATEDWITHEMAIL IS 'Flag to know if the contract was activated with an email.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
