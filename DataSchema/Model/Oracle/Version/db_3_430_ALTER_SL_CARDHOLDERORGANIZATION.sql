SPOOL db_3_430.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.430', 'BTS', 'New columns in SL_CARDHOLDERORGANIZATION');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_CARDHOLDERORGANIZATION
 ADD (ValidFrom  DATE);


ALTER TABLE SL_CARDHOLDERORGANIZATION
 ADD (ValidUntil  DATE);


COMMENT ON COLUMN SL_CARDHOLDERORGANIZATION.VALIDFROM IS 'Valid from date.';

COMMENT ON COLUMN SL_CARDHOLDERORGANIZATION.VALIDUNTIL IS 'Valid until .';

-- =======[ New Tables ]===========================================================================================

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
