SPOOL db_3_539.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.539', 'DNG', 'Extend ACC Views with COSTCENTERKEY');

-- =======[ Changes to Existing Tables ]===========================================================================

  CREATE OR REPLACE FORCE VIEW "ACC_REC_LOAD_USE_CML_SETTLED" ("REVENUERECOGNITIONID", "REVENUESETTLEMENTID", "SETTLEMENTTYPE", "AMOUNT", "PREMIUM", "BASEFARE", "TICKETNUMBER", "CUSTOMERGROUP", "LINEGROUPID", "OPERATORID", "CREDITACCOUNTNUMBER", "DEBITACCOUNTNUMBER", "POSTINGDATE", "POSTINGREFERENCE", "INCLUDEINSETTLEMENT", "CLOSEOUTPERIODID", "PERIODFROM", "PERIODTO", "CLOSEOUTTYPE", "STATE", "TRANSACTIONJOURNALID", "TRANSITACCOUNTID", "DEVICETIME", "FAREAMOUNT", "LINE", "TICKETINTERNALNUMBER", "TICKETID", "TRANSACTIONTYPE", "CLIENTID", "TRANSACTIONOPERATORID", "SALESCHANNELID", "RESULTTYPE", "CANCELLATIONREFERENCEGUID", "CANCELLATIONREFERENCE", "PRODUCTID", "PURSEBALANCE", "PURSECREDIT", "TARIFFDATE", "TARIFFVERSION", "TRIPTICKETINTERNALNUMBER", "COSTCENTERKEY", "LASTMODIFIED", "CANCELLEDTAP") AS 
  SELECT rr.revenuerecognitionid, rs.revenuesettlementid, rs.settlementtype,
          rr.amount, rr.premium, rr.basefare, rr.ticketnumber,
          rr.customergroup, rr.linegroupid, rr.operatorid,
          rr.creditaccountnumber, rr.debitaccountnumber, rr.postingdate,
          rr.postingreference, rr.includeinsettlement, cp.closeoutperiodid,
          cp.periodfrom, cp.periodto, cp.closeouttype, cp.state,
          tj1.transactionjournalid, tj1.transitaccountid, tj1.devicetime,
          tj1.fareamount, tj1.line, tj1.ticketinternalnumber, tj1.ticketid,
          tj1.transactiontype, tj1.clientid,
          tj1.operatorid AS transactionoperatorid, tj1.saleschannelid,
          tj1.resulttype, tj1.cancellationreferenceguid,
          tj1.cancellationreference, tj1.productid, tj1.pursebalance,
          tj1.pursecredit, tj1.tariffdate, tj1.tariffversion,
          tj1.tripticketinternalnumber, tj1.costcenterkey,
		  rr.LASTMODIFIED,
          CAST
             (DECODE (tj1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)
             ) AS cancelledtap
     FROM acc_revenuerecognition rr
          LEFT JOIN
          (SELECT   MIN (sr1.revenuesettlementid) AS settlementid,
                    revenuerecognitionid
               FROM acc_settledrevenue sr1 INNER JOIN acc_revenuesettlement s1
                    ON s1.revenuesettlementid = sr1.revenuesettlementid
              WHERE s1.settlementtype = 1                          --CML based
           GROUP BY revenuerecognitionid) sr
          ON sr.revenuerecognitionid = rr.revenuerecognitionid
          LEFT JOIN acc_revenuesettlement rs
          ON rs.revenuesettlementid = sr.settlementid
        AND settlementtype = 1                                     --CML based
          INNER JOIN sl_transactionjournal tj1
          ON tj1.transactionjournalid = rr.transactionjournalid
          INNER JOIN acc_closeoutperiod cp
          ON cp.closeoutperiodid = rr.closeoutperiodid
    WHERE tj1.transactiontype = 108
       OR tj1.transactiontype = 85
       OR (tj1.transactiontype = 66 AND tj1.validto IS NOT NULL);

  CREATE OR REPLACE FORCE VIEW "ACC_RECOGNIZED_TAP_TAB_SETTLED" ("REVENUERECOGNITIONID", "REVENUESETTLEMENTID", "SETTLEMENTTYPE", "AMOUNT", "PREMIUM", "BASEFARE", "TICKETNUMBER", "CUSTOMERGROUP", "LINEGROUPID", "OPERATORID", "CREDITACCOUNTNUMBER", "DEBITACCOUNTNUMBER", "POSTINGDATE", "POSTINGREFERENCE", "INCLUDEINSETTLEMENT", "CLOSEOUTPERIODID", "PERIODFROM", "PERIODTO", "CLOSEOUTTYPE", "STATE", "TRANSACTIONJOURNALID", "TRANSITACCOUNTID", "DEVICETIME", "FAREAMOUNT", "LINE", "TICKETINTERNALNUMBER", "TICKETID", "TRANSACTIONTYPE", "CLIENTID", "TRANSACTIONOPERATORID", "SALESCHANNELID", "RESULTTYPE", "CANCELLATIONREFERENCEGUID", "CANCELLATIONREFERENCE", "PRODUCTID", "PURSEBALANCE", "PURSECREDIT", "TARIFFDATE", "TARIFFVERSION", "TRIPTICKETINTERNALNUMBER", "COSTCENTERKEY", "LASTMODIFIED", "CANCELLEDTAP") AS 
  SELECT rr.revenuerecognitionid, rs.revenuesettlementid, rs.settlementtype,
          rr.amount, rr.premium, rr.basefare, rr.ticketnumber,
          rr.customergroup, rr.linegroupid, rr.operatorid,
          rr.creditaccountnumber, rr.debitaccountnumber, rr.postingdate,
          rr.postingreference, rr.includeinsettlement, cp.closeoutperiodid,
          cp.periodfrom, cp.periodto, cp.closeouttype, cp.state,
          tj1.transactionjournalid, tj1.transitaccountid, tj1.devicetime,
          tj1.fareamount, tj1.line, tj1.ticketinternalnumber, tj1.ticketid,
          tj1.transactiontype, tj1.clientid,
          tj1.operatorid AS transactionoperatorid, tj1.saleschannelid,
          tj1.resulttype, tj1.cancellationreferenceguid,
          tj1.cancellationreference, tj1.productid, tj1.pursebalance,
          tj1.pursecredit, tj1.tariffdate, tj1.tariffversion,
          tj1.tripticketinternalnumber, tj1.costcenterkey,
		  rr.LASTMODIFIED,
          CAST
             (DECODE (tj1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)
             ) AS cancelledtap
     FROM acc_revenuerecognition rr
          LEFT JOIN
          (SELECT   MIN (sr1.revenuesettlementid) AS settlementid,
                    revenuerecognitionid
               FROM acc_settledrevenue sr1 INNER JOIN acc_revenuesettlement s1
                    ON s1.revenuesettlementid = sr1.revenuesettlementid
              WHERE s1.settlementtype = 0                          --TAB based
           GROUP BY revenuerecognitionid) sr
          ON sr.revenuerecognitionid = rr.revenuerecognitionid
          LEFT JOIN acc_revenuesettlement rs
          ON rs.revenuesettlementid = sr.settlementid
        AND settlementtype = 0                                     --TAB based
          INNER JOIN sl_transactionjournal tj1
          ON tj1.transactionjournalid = rr.transactionjournalid
          INNER JOIN acc_closeoutperiod cp
          ON cp.closeoutperiodid = rr.closeoutperiodid
    WHERE tj1.validto IS NULL
      AND (tj1.transactiontype = 66 OR tj1.transactiontype = 84);

  CREATE OR REPLACE FORCE VIEW "ACC_REC_LOAD_USE_TAB_SETTLED" ("REVENUERECOGNITIONID", "REVENUESETTLEMENTID", "SETTLEMENTTYPE", "AMOUNT", "PREMIUM", "BASEFARE", "TICKETNUMBER", "CUSTOMERGROUP", "LINEGROUPID", "OPERATORID", "CREDITACCOUNTNUMBER", "DEBITACCOUNTNUMBER", "POSTINGDATE", "POSTINGREFERENCE", "INCLUDEINSETTLEMENT", "CLOSEOUTPERIODID", "PERIODFROM", "PERIODTO", "CLOSEOUTTYPE", "STATE", "TRANSACTIONJOURNALID", "TRANSITACCOUNTID", "DEVICETIME", "FAREAMOUNT", "LINE", "TICKETINTERNALNUMBER", "TICKETID", "TRANSACTIONTYPE", "CLIENTID", "TRANSACTIONOPERATORID", "SALESCHANNELID", "RESULTTYPE", "CANCELLATIONREFERENCEGUID", "CANCELLATIONREFERENCE", "PRODUCTID", "PURSEBALANCE", "PURSECREDIT", "TARIFFDATE", "TARIFFVERSION", "TRIPTICKETINTERNALNUMBER", "COSTCENTERKEY", "LASTMODIFIED", "CANCELLEDTAP") AS 
  SELECT rr.revenuerecognitionid, rs.revenuesettlementid, rs.settlementtype,
          rr.amount, rr.premium, rr.basefare, rr.ticketnumber,
          rr.customergroup, rr.linegroupid, rr.operatorid,
          rr.creditaccountnumber, rr.debitaccountnumber, rr.postingdate,
          rr.postingreference, rr.includeinsettlement, cp.closeoutperiodid,
          cp.periodfrom, cp.periodto, cp.closeouttype, cp.state,
          tj1.transactionjournalid, tj1.transitaccountid, tj1.devicetime,
          tj1.fareamount, tj1.line, tj1.ticketinternalnumber, tj1.ticketid,
          tj1.transactiontype, tj1.clientid,
          tj1.operatorid AS transactionoperatorid, tj1.saleschannelid,
          tj1.resulttype, tj1.cancellationreferenceguid,
          tj1.cancellationreference, tj1.productid, tj1.pursebalance,
          tj1.pursecredit, tj1.tariffdate, tj1.tariffversion,
          tj1.tripticketinternalnumber, tj1.costcenterkey,
		  rr.LASTMODIFIED,
          CAST
             (DECODE (tj1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)
             ) AS cancelledtap
     FROM acc_revenuerecognition rr
          LEFT JOIN
          (SELECT   MIN (sr1.revenuesettlementid) AS settlementid,
                    revenuerecognitionid
               FROM acc_settledrevenue sr1 INNER JOIN acc_revenuesettlement s1
                    ON s1.revenuesettlementid = sr1.revenuesettlementid
              WHERE s1.settlementtype = 0                          --TAB based
           GROUP BY revenuerecognitionid) sr
          ON sr.revenuerecognitionid = rr.revenuerecognitionid
          LEFT JOIN acc_revenuesettlement rs
          ON rs.revenuesettlementid = sr.settlementid
        AND settlementtype = 0                                     --TAB based
          INNER JOIN sl_transactionjournal tj1
          ON tj1.transactionjournalid = rr.transactionjournalid
          INNER JOIN acc_closeoutperiod cp
          ON cp.closeoutperiodid = rr.closeoutperiodid
    WHERE tj1.transactiontype = 108
       OR tj1.transactiontype = 85
       OR (tj1.transactiontype = 66 AND tj1.validto IS NOT NULL);

  CREATE OR REPLACE FORCE VIEW "ACC_CANCEL_REC_TAP_TAB_SETTLED" ("REVENUERECOGNITIONID", "REVENUESETTLEMENTID", "SETTLEMENTTYPE", "AMOUNT", "PREMIUM", "BASEFARE", "TICKETNUMBER", "CUSTOMERGROUP", "LINEGROUPID", "OPERATORID", "CREDITACCOUNTNUMBER", "DEBITACCOUNTNUMBER", "POSTINGDATE", "POSTINGREFERENCE", "INCLUDEINSETTLEMENT", "CLOSEOUTPERIODID", "PERIODFROM", "PERIODTO", "CLOSEOUTTYPE", "STATE", "TRANSACTIONJOURNALID", "TRANSITACCOUNTID", "DEVICETIME", "FAREAMOUNT", "LINE", "TICKETINTERNALNUMBER", "TICKETID", "TRANSACTIONTYPE", "CLIENTID", "TRANSACTIONOPERATORID", "SALESCHANNELID", "RESULTTYPE", "CANCELLATIONREFERENCEGUID", "CANCELLATIONREFERENCE", "PRODUCTID", "PURSEBALANCE", "PURSECREDIT", "TARIFFDATE", "TARIFFVERSION", "TRIPTICKETINTERNALNUMBER", "COSTCENTERKEY", "LASTMODIFIED") AS 
  SELECT rr.revenuerecognitionid, rs.revenuesettlementid, rs.settlementtype,
          rr.amount, rr.premium, rr.basefare, rr.ticketnumber,
          rr.customergroup, rr.linegroupid, rr.operatorid,
          rr.creditaccountnumber, rr.debitaccountnumber, rr.postingdate,
          rr.postingreference, rr.includeinsettlement, cp.closeoutperiodid,
          cp.periodfrom, cp.periodto, cp.closeouttype, cp.state,
          tj1.transactionjournalid, tj1.transitaccountid, tj1.devicetime,
          tj1.fareamount, tj1.line, tj1.ticketinternalnumber, tj1.ticketid,
          tj1.transactiontype, tj1.clientid,
          tj1.operatorid AS transactionoperatorid, tj1.saleschannelid,
          tj1.resulttype, tj1.cancellationreferenceguid,
          tj1.cancellationreference, tj1.productid, tj1.pursebalance,
          tj1.pursecredit, tj1.tariffdate, tj1.tariffversion,
          tj1.tripticketinternalnumber, tj1.costcenterkey,
		  rr.LASTMODIFIED
     FROM acc_revenuerecognition rr
          LEFT JOIN
          (SELECT   MIN (sr1.revenuesettlementid) AS settlementid,
                    revenuerecognitionid
               FROM acc_settledrevenue sr1 INNER JOIN acc_revenuesettlement s1
                    ON s1.revenuesettlementid = sr1.revenuesettlementid
              WHERE s1.settlementtype = 0                          --TAB based
           GROUP BY revenuerecognitionid) sr
          ON sr.revenuerecognitionid = rr.revenuerecognitionid
          LEFT JOIN acc_revenuesettlement rs
          ON rs.revenuesettlementid = sr.settlementid
        AND settlementtype = 0                                     --TAB based
          INNER JOIN sl_transactionjournal tj1
          ON tj1.transactionjournalid = rr.transactionjournalid
          INNER JOIN acc_closeoutperiod cp
          ON cp.closeoutperiodid = rr.closeoutperiodid
          LEFT JOIN sl_transactionjournal tj2
          ON tj2.transactionjournalid = tj1.cancellationreference
    WHERE tj1.transactiontype = 128
      AND (    tj2.validto IS NULL
           AND (tj2.transactiontype = 66 OR tj2.transactiontype = 84)
          );

  CREATE OR REPLACE FORCE VIEW "ACC_CANCEL_REC_TAP_CML_SETTLED" ("REVENUERECOGNITIONID", "REVENUESETTLEMENTID", "SETTLEMENTTYPE", "AMOUNT", "PREMIUM", "BASEFARE", "TICKETNUMBER", "CUSTOMERGROUP", "LINEGROUPID", "OPERATORID", "CREDITACCOUNTNUMBER", "DEBITACCOUNTNUMBER", "POSTINGDATE", "POSTINGREFERENCE", "INCLUDEINSETTLEMENT", "CLOSEOUTPERIODID", "PERIODFROM", "PERIODTO", "CLOSEOUTTYPE", "STATE", "TRANSACTIONJOURNALID", "TRANSITACCOUNTID", "DEVICETIME", "FAREAMOUNT", "LINE", "TICKETINTERNALNUMBER", "TICKETID", "TRANSACTIONTYPE", "CLIENTID", "TRANSACTIONOPERATORID", "SALESCHANNELID", "RESULTTYPE", "CANCELLATIONREFERENCEGUID", "CANCELLATIONREFERENCE", "PRODUCTID", "PURSEBALANCE", "PURSECREDIT", "TARIFFDATE", "TARIFFVERSION", "TRIPTICKETINTERNALNUMBER", "COSTCENTERKEY", "LASTMODIFIED") AS 
  SELECT rr.revenuerecognitionid, rs.revenuesettlementid, rs.settlementtype,
          rr.amount, rr.premium, rr.basefare, rr.ticketnumber,
          rr.customergroup, rr.linegroupid, rr.operatorid,
          rr.creditaccountnumber, rr.debitaccountnumber, rr.postingdate,
          rr.postingreference, rr.includeinsettlement, cp.closeoutperiodid,
          cp.periodfrom, cp.periodto, cp.closeouttype, cp.state,
          tj1.transactionjournalid, tj1.transitaccountid, tj1.devicetime,
          tj1.fareamount, tj1.line, tj1.ticketinternalnumber, tj1.ticketid,
          tj1.transactiontype, tj1.clientid,
          tj1.operatorid AS transactionoperatorid, tj1.saleschannelid,
          tj1.resulttype, tj1.cancellationreferenceguid,
          tj1.cancellationreference, tj1.productid, tj1.pursebalance,
          tj1.pursecredit, tj1.tariffdate, tj1.tariffversion,
          tj1.tripticketinternalnumber, tj1.costcenterkey,
		  rr.LASTMODIFIED
     FROM acc_revenuerecognition rr
          LEFT JOIN
          (SELECT   MIN (sr1.revenuesettlementid) AS settlementid,
                    revenuerecognitionid
               FROM acc_settledrevenue sr1 INNER JOIN acc_revenuesettlement s1
                    ON s1.revenuesettlementid = sr1.revenuesettlementid
              WHERE s1.settlementtype = 1                          --CML based
           GROUP BY revenuerecognitionid) sr
          ON sr.revenuerecognitionid = rr.revenuerecognitionid
          LEFT JOIN acc_revenuesettlement rs
          ON rs.revenuesettlementid = sr.settlementid
        AND settlementtype = 1                                     --CML based
          INNER JOIN sl_transactionjournal tj1
          ON tj1.transactionjournalid = rr.transactionjournalid
          INNER JOIN acc_closeoutperiod cp
          ON cp.closeoutperiodid = rr.closeoutperiodid
          LEFT JOIN sl_transactionjournal tj2
          ON tj2.transactionjournalid = tj1.cancellationreference
    WHERE tj1.transactiontype = 128
      AND (    tj2.validto IS NULL
           AND (tj2.transactiontype = 66 OR tj2.transactiontype = 84)
          );

ALTER TABLE ACC_REVENUESETTLEMENT ADD (BASEFARE  NUMBER(18, 0) DEFAULT 0 NOT NULL);
ALTER TABLE ACC_REVENUESETTLEMENT ADD (OPERATORID  NUMBER(9, 0));
ALTER TABLE ACC_REVENUESETTLEMENT ADD (PREMIUM  NUMBER(18, 0) DEFAULT 0 NOT NULL);
ALTER TABLE ACC_REVENUESETTLEMENT ADD (TAPSCOUNT  NUMBER(18, 0) DEFAULT 0 NOT NULL);
ALTER TABLE ACC_REVENUESETTLEMENT ADD (TRANSITACCOUNTID  NUMBER(18, 0));
ALTER TABLE ACC_REVENUESETTLEMENT ADD (COSTCENTERKEY  VARCHAR2(100 BYTE));
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
