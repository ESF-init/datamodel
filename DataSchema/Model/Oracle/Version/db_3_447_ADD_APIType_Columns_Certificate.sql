SPOOL db_3_447.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.447', 'MEA', 'Add column APITYPE to SL_CERTIFICATE');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_CERTIFICATE ADD APITYPE NUMBER(1) DEFAULT 0 NOT NULL;


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
