SPOOL db_3_609.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.609', 'MKD', 'Creation of relation table SL_NOTIFICATIONMSGTOCUSTOMER');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_NOTIFICATIONMSGTOCUSTOMER (
    NotificationMessageID NUMBER(18, 0) NOT NULL,
    CustomerAccountID NUMBER(18, 0) NOT NULL,
    IsAcknowledged NUMBER(1, 0) DEFAULT 0 NOT NULL,
    IsDeleted NUMBER(1) DEFAULT 0 NOT NULL,
    CONSTRAINT PK_NOTIFICATIONMSGTOCUSTOMER PRIMARY KEY (NotificationMessageID, CustomerAccountID),
    CONSTRAINT FK_CUSTOMERACCOUNTID FOREIGN KEY (CustomerAccountID) REFERENCES SL_CUSTOMERACCOUNT(CUSTOMERACCOUNTID),
    CONSTRAINT FK_NOTIFICATIONMESSAGEID FOREIGN KEY (NotificationMessageID) REFERENCES SL_NOTIFICATIONMESSAGE(NOTIFICATIONMESSAGEID)
);

COMMENT ON TABLE SL_NOTIFICATIONMSGTOCUSTOMER is 'Contains the relation of customer accounts to messages whith the indicators of acknoledgedment and deletion.';
COMMENT ON COLUMN SL_NOTIFICATIONMSGTOCUSTOMER.ISDELETED IS 'Indicates whether the message has been deleted by the user.';
COMMENT ON COLUMN SL_NOTIFICATIONMSGTOCUSTOMER.CUSTOMERACCOUNTID IS 'The ID of the customer account the message should be shown to. Reference to SL_CUSTOMERACCOUNT.';
COMMENT ON COLUMN SL_NOTIFICATIONMSGTOCUSTOMER.ISACKNOWLEDGED is 'Indicates whether the message has been acknowledged by the user.';
COMMENT ON COLUMN SL_NOTIFICATIONMSGTOCUSTOMER.NOTIFICATIONMESSAGEID is 'The ID of the message to be shown. Reference to SL_NOTIFICATIONMESSAGE';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
