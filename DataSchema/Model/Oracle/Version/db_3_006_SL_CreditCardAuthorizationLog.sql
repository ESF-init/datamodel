SPOOL db_3_006.log

INSERT INTO vario_dmodellversion (rundate,dmodellno,responsible,annotation)
VALUES (SYSDATE,'3.006','FLF','Added table SL_CREDITCARDAUTHORIZATIONLOG and row RISKFLAGS to SL_CREDITCARDAUTHORIZATION');

-- Start adding schema changes here

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

CREATE TABLE SL_CREDITCARDAUTHORIZATIONLOG
(
   CREDITCARDAUTHORIZATIONLOGID   NUMBER (18) NOT NULL,
   CREDITCARDAUTHORIZATIONID      NUMBER (18) NOT NULL,
   INSERTTIME                     DATE NOT NULL,
   REQUEST                        VARCHAR2 (2000) NOT NULL,
   RESPONSE                       VARCHAR2 (2000)
);

ALTER TABLE SL_CREDITCARDAUTHORIZATIONLOG ADD (
  CONSTRAINT PK_CREDITCARDAUTHORIZATIONLOG PRIMARY KEY (CREDITCARDAUTHORIZATIONLOGID));

ALTER TABLE SL_CREDITCARDAUTHORIZATIONLOG
   ADD CONSTRAINT FK_Log_CreditCardAuth FOREIGN KEY
          (CREDITCARDAUTHORIZATIONID)
           REFERENCES SL_CREDITCARDAUTHORIZATION (CREDITCARDAUTHORIZATIONID);

CREATE INDEX IDX_LOGAUTHID ON SL_CREDITCARDAUTHORIZATIONLOG (CREDITCARDAUTHORIZATIONID);

CREATE SEQUENCE SL_CREDITCARDAUTHLOG_SEQ START WITH 1
                                                  MAXVALUE 9999999999999999999999999999
                                                  MINVALUE 1
                                                  NOCYCLE
                                                  CACHE 20
                                                  NOORDER;


-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------


ALTER TABLE SL_CREDITCARDAUTHORIZATION ADD (RISKFLAGS NUMBER(9) DEFAULT 0 NOT NULL );




---End adding schema changes


COMMIT;

PROMPT Done!

SPOOL OFF