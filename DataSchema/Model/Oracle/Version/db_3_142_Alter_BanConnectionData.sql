SPOOL db_3_142.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.142', 'SOE', 'Updated  AccountOwnerID as nullable in sl_BankConnectionData');

-- -------[ Enumerations ]------------------------------------------------------------------------------------------
ALTER TABLE sl_BankConnectionData MODIFY AccountOwnerID Number(18) NULL ;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;