SPOOL db_3_292.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.292', 'HNI', 'Created SL_ATTRIBUTETOMOBILITYPROVIDER and renamed tables ');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

COMMENT ON COLUMN SL_MOBILITYPROVIDER.MOBILITYPROVIDERID IS 'Unique id of a mobility provider.';
COMMENT ON TABLE SL_CONTRACTTOPROVIDER IS 'This table links the contracts and the mobility providers (REGIOMOVE project).';

-- Rename Column 
ALTER TABLE SL_BOOKINGITEM RENAME COLUMN "STATE" TO "BOOKINGTYPE";
COMMENT ON COLUMN SL_BOOKINGITEM.BOOKINGTYPE IS 'State of the booking item. References SL_BOOKINGTYPE.ENUMERATIONVALUE.';

-- Rename Column 
ALTER TABLE SL_CONTRACTTOPROVIDER RENAME COLUMN "STATE" TO "PROVIDERACCOUNTSTATE";
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.PROVIDERACCOUNTSTATE IS 'State of the provider account. References SL_PROVIDERACCOUNTSTATE.ENUMERATIONVALUE.';

-- Rename Table
ALTER TABLE SL_PROVIDERACCOUNT_STATE RENAME TO SL_PROVIDERACCOUNTSTATE;
COMMENT ON TABLE SL_PROVIDERACCOUNTSTATE IS 'Enumeration of SL_CONTRACTTOPROVIDER State.';

-- MODIFICATIONS on SL_BOOKING
ALTER TABLE SL_BOOKING DROP CONSTRAINT FK_BOOKING_CARD;
ALTER TABLE SL_BOOKING RENAME COLUMN CARDHOLDERID TO CARDID;  
ALTER TABLE SL_BOOKING ADD CONSTRAINT FK_BOOKING_CARD FOREIGN KEY (CARDID) REFERENCES SL_CARD (CARDID);
ALTER TABLE
    SL_BOOKING
ADD
    (
		  LASTUSER						NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
		  LASTMODIFIED					DATE			DEFAULT sysdate		NOT NULL,
		  TRANSACTIONCOUNTER			INTEGER								NOT NULL,
		  DATAROWCREATIONDATE			DATE			DEFAULT sysdate
    );
COMMENT ON COLUMN SL_BOOKING.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_BOOKING.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_BOOKING.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_BOOKING.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

-- MODIFICATIONS on SL_BOOKINGITEM
ALTER TABLE
    SL_BOOKINGITEM
ADD
    (
		  LASTUSER						NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
		  LASTMODIFIED					DATE			DEFAULT sysdate		NOT NULL,
		  TRANSACTIONCOUNTER			INTEGER								NOT NULL,
		  DATAROWCREATIONDATE			DATE			DEFAULT sysdate
    );
COMMENT ON COLUMN SL_BOOKINGITEM.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_BOOKINGITEM.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_BOOKINGITEM.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_BOOKINGITEM.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

-- MODIFICATIONS on SL_MOBILITYPROVIDER
ALTER TABLE
    SL_MOBILITYPROVIDER
ADD
    (
		  LASTUSER						NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
		  LASTMODIFIED					DATE			DEFAULT sysdate		NOT NULL,
		  TRANSACTIONCOUNTER			INTEGER								NOT NULL,
		  DATAROWCREATIONDATE			DATE			DEFAULT sysdate
    );
COMMENT ON COLUMN SL_MOBILITYPROVIDER.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_MOBILITYPROVIDER.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_MOBILITYPROVIDER.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_MOBILITYPROVIDER.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

-- MODIFICATIONS on SL_CONTRACTTOPROVIDER
ALTER TABLE
    SL_CONTRACTTOPROVIDER
ADD
    (
		  LASTUSER						NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
		  LASTMODIFIED					DATE			DEFAULT sysdate		NOT NULL,
		  TRANSACTIONCOUNTER			INTEGER								NOT NULL,
		  DATAROWCREATIONDATE			DATE			DEFAULT sysdate
    );
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';


-- =======[ New Tables ]===========================================================================================
CREATE TABLE SL_ATTRIBUTETOMOBILITYPROVIDER
(
  ATTRIBUTEID         NUMBER(9)                 NOT NULL,
  MOBILITYPROVIDERID  NUMBER(9)                 NOT NULL
);

COMMENT ON TABLE SL_ATTRIBUTETOMOBILITYPROVIDER IS 'This table links the attributes and the mobility providers.';
COMMENT ON COLUMN SL_ATTRIBUTETOMOBILITYPROVIDER.ATTRIBUTEID IS 'Unique identifier of the attribute';
COMMENT ON COLUMN SL_ATTRIBUTETOMOBILITYPROVIDER.MOBILITYPROVIDERID IS 'Unique id of the of a mobility provider';

ALTER TABLE SL_ATTRIBUTETOMOBILITYPROVIDER ADD (
  CONSTRAINT FK_ATTRIBUTEPROVIDER_PROVIDER 
  FOREIGN KEY (MOBILITYPROVIDERID) 
  REFERENCES SL_MOBILITYPROVIDER (MOBILITYPROVIDERID)
  ENABLE VALIDATE,
  CONSTRAINT FK_ATTRIBUTEPROVIDER_ATTRIBUTE 
  FOREIGN KEY (ATTRIBUTEID) 
  REFERENCES SL_ATTRIBUTE (ATTRIBUTEID)
  ENABLE VALIDATE);


-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- JED: Removed sequence creation, because sequence was already created in db_3_286_RegioMove.sql

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(4) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_MOBILITYPROVIDER','SL_CONTRACTTOPROVIDER', 'SL_BOOKING','SL_BOOKINGITEM');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
