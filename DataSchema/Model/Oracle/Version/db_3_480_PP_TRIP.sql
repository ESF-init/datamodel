SPOOL db_3_480.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.480', 'JGI', 'New column DUETIME');

DECLARE
    l_nCounter NUMBER(1) := -1;
BEGIN

  select count(*) into l_nCounter  from all_synonyms where synonym_name = 'PP_TRIP' and owner = sys_context( 'userenv', 'current_schema' ) ;
  if l_nCounter > 0 THEN
  
    EXECUTE IMMEDIATE 'DROP SYNONYM PP_TRIP';
  END IF;

  select count(*) into l_nCounter  from all_synonyms where synonym_name = 'PP_TRIP' and owner = sys_context( 'userenv', 'current_schema' );
  if l_nCounter = 0 THEN
  
    EXECUTE IMMEDIATE 'CREATE TABLE PP_TRIP AS SELECT * FROM RM_TRANSACTION WHERE 0=1';
  END IF;
    
  -- =======[ Changes to Existing Tables ]===========================================================================

  EXECUTE IMMEDIATE 'ALTER TABLE PP_TRIP ADD (DUETIME  NUMBER(10))';

  --add comments:

  EXECUTE IMMEDIATE 'COMMENT ON COLUMN PP_TRIP.DUETIME IS ''Validity of ticket in seconds''';

END;
/

COMMIT;

PROMPT Done!

SPOOL OFF;

