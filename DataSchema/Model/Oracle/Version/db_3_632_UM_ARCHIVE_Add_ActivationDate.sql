SPOOL db_3_632.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.632', 'TIR', 'Add column for activation date');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE UM_ARCHIVE ADD ACTIVATIONDATE DATE;

COMMENT ON COLUMN UM_ARCHIVE.ACTIVATIONDATE IS 'Activation date of this archive';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;