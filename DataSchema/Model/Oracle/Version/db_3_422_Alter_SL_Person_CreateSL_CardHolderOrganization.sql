SPOOL db_3_422.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.422', 'BTS', 'Extend Table SL_Person with column JobTitle; Create new Table SL_CardHolderOrganization ');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PERSON
 ADD (JobTitle  VARCHAR2(100));

COMMENT ON COLUMN SL_PERSON.JobTitle IS 'Description of a card holders job title';

-- =======[ New Tables ]===========================================================================================


CREATE TABLE SL_CardHolderOrganization
(
  CardHolderID        NUMBER                    NOT NULL,
  OrganizationID      NUMBER                    NOT NULL,
  Created             DATE                      DEFAULT sysdate               NOT NULL,
  LastModified        DATE                      DEFAULT sysdate               NOT NULL,
  Transactioncounter  INTEGER                   NOT NULL,
  Lastuser            NVARCHAR2(50)             DEFAULT 'SYS'                 NOT NULL
);



ALTER TABLE SL_CardHolderOrganization ADD (
  CONSTRAINT SL_CardHolderOrganization_PK
  PRIMARY KEY
  (OrganizationID)
  ENABLE VALIDATE);

COMMENT ON TABLE SL_CardHolderOrganization is 'Assignment between a ticket holder (student) and his school';

COMMENT ON COLUMN SL_CardHolderOrganization.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_CardHolderOrganization.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_CardHolderOrganization.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_CardHolderOrganization.CardHolderID IS 'Identifies the card holder (student)';
COMMENT ON COLUMN SL_CardHolderOrganization.OrganizationID IS 'Identifies the school (organization) to which the card holder  (student) belongs';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
