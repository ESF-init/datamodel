SPOOL db_3_272.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.272', 'BVI', 'Modified SL_PostingKey. Added addresses for organization.');


-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_postingkey add (
constraint postingKey_ClientID_PostingKey UNIQUE (Clientid, PostingKey)
);


-- =======[ New Tables ]===========================================================================

CREATE TABLE SL_OrganizationAddress (
	ORGANIZATIONID NUMBER(18, 0) NOT NULL, 
	AddressID NUMBER(18, 0) NOT NULL, 
	AddressType NUMBER(9, 0) NOT NULL, 
	Created DATE DEFAULT sysdate NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL,
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_OrgAddress PRIMARY KEY (ORGANIZATIONID, AddressID),
	CONSTRAINT FK_OrgAddress_Org FOREIGN KEY (ORGANIZATIONID) REFERENCES SL_Organization(ORGANIZATIONID),
	CONSTRAINT FK_OrgAddress_Address FOREIGN KEY (AddressID) REFERENCES SL_Address(AddressID)
);

INSERT ALL
    INTO SL_AddressType (EnumerationValue, Literal, Description) VALUES (5, 'Contract', 'Contract Address')
SELECT * FROM DUAL; 

COMMENT ON TABLE SL_OrganizationAddress IS 'N:M Table for addresses of an organization (SL_Organization)';
COMMENT ON COLUMN SL_OrganizationAddress.ORGANIZATIONID IS 'ID of organization (SL_Organization)';
COMMENT ON COLUMN SL_OrganizationAddress.AddressID IS 'ID of address (SL_Address)';
COMMENT ON COLUMN SL_OrganizationAddress.AddressType IS 'Type of the address (SL_AddressType)';
COMMENT ON COLUMN SL_OrganizationAddress.Created IS 'Creation timestamp when inserted into database.';
COMMENT ON COLUMN SL_OrganizationAddress.LastUser IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_OrganizationAddress.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_OrganizationAddress.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;