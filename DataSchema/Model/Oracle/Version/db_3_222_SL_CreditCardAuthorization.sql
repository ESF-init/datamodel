SPOOL db_3_222.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.222', 'FLF', 'Added new column CertificationData to SL_CreditCardAuthorization.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (CertificationData NVARCHAR2(2000));
 
comment on column SL_CREDITCARDAUTHORIZATION.CertificationData is 'Contains data only needed for certication purposes.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
