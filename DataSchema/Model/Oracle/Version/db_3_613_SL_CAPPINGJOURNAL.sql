SPOOL db_3_613.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.613', 'FLF', 'Add new field LASTINCREMENT and comment to SL_CAPPINGJOURNAL');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CAPPINGJOURNAL
 ADD (LASTINCREMENT  DATE);

COMMENT ON COLUMN SL_CAPPINGJOURNAL.LASTINCREMENT IS 'Last date-time SL_CAPPINGJOURNAL.AMOUNT was incremented';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
