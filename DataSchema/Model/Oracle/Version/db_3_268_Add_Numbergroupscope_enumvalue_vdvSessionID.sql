SPOOL db_3_268.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.268', 'STV', 'Add Numbergroupscope enumvalue vdvSessionID');

-- =======[ Changes to Existing Tables ]===========================================================================

insert into sl_numbergroupscope
(EnumerationValue,Literal,description) values (20,'Vdv Session ID','Vdv Session ID for Sending PV requests.');

Insert into SL_NUMBERGROUP
   (NUMBERGROUPID, SCOPE, FORMAT, DESCRIPTION, STARTVALUE, 
    CURRENTVALUE, RESETATENDOFMONTH, RESETATENDOFYEAR)
 Values
   (50, 20, '[00000000000]', 'Vdv Session ID', 1, 
    1, 0, 0);
	
	 
  
COMMIT;

PROMPT Done!

SPOOL OFF;
