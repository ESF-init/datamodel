SPOOL db_3_554.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.554', 'MFA', 'Create new view for sl_creditcardauthorization');

-- Start adding schema changes here

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW "VIEW_SL_CCAUTHORIZATION"("CREDITCARDAUTHORIZATIONID", "CARDID", "PRINTEDNUMBER", "FAREMEDIAID", "FAREMEDIATYPE", "STATUS", "LOCALDATETIME", "TRANSACTIONDATETIME", 
"EXTERNALAUTHID", "SYSTEMTRACEAUDITNUMBER", "RESPONSECODE", "AMOUNT", "TOKEN", "LASTUSER", "LASTMODIFIED", "ACI", "COMPLETIONDATAE", "ORDERNUMBER", "BANKNETDATA", "CARDTYPE", "CREDITCARDTERMINALID",
"PURSEAMOUNT", "DEVICETYPEINDICATOR", "EXTERNALCARDREFERENCE", "LASTEMVDATAUPDATE", "CREATIONDATE", "ACCOUNTSTATE", "AUTHORIZATIONTYPE", "ISARQCREQUESTED", "COMPLETIONTYPE", "RISKFLAGS",
"CARDSEQUENCENUMBER", "CARDLEVELRESULT", "CERTIFICATIONDATA", "TRANSACTIONJOURNALID") AS
select sl_creditcardauthorization.creditcardauthorizationid, sl_creditcardauthorization.cardid, sl_card.printednumber, sl_card.faremediaid, 
    DECODE (sl_card.cardtype,
                215, 'ClosedLoop',
                217, 'OpenLoop',
                220, 'Barcode',
                224, 'VirtualCardGoogle',
                225, 'VirtualCardApple',
                226, 'ExternalUidCard',
                227, 'ExternalBarcode',
                sl_card.cardtype) AS faremediatype,
    DECODE(sl_creditcardauthorization.status,
            -2, 'AuthError',
            -1, 'CompletionError',
            0, 'Open',
            1, 'Completed',
            2, 'Retry',
            3, 'PreCompleted',
            4, 'GoodwillCompletion',
            sl_creditcardauthorization.status) AS status, 
sl_creditcardauthorization.localdatetime, sl_creditcardauthorization.transactiondatetime, sl_creditcardauthorization.externalauthid, sl_creditcardauthorization.systemtraceauditnumber,
sl_creditcardauthorization.responsecode, sl_creditcardauthorization.amount, sl_creditcardauthorization.token, sl_creditcardauthorization.lastuser, sl_creditcardauthorization.lastmodified, 
sl_creditcardauthorization.aci, sl_creditcardauthorization.completiondatetime, sl_creditcardauthorization.ordernumber, sl_creditcardauthorization.banknetdata, sl_creditcardauthorization.cardtype, 
sl_creditcardauthorization.creditcardterminalid, sl_creditcardauthorization.purseamount, sl_creditcardauthorization.devicetypeindicator, sl_creditcardauthorization.externalcardreference, 
sl_creditcardauthorization.lastemvdataupdate, sl_creditcardauthorization.creationdate, sl_creditcardauthorization.accountstate, sl_creditcardauthorization.authorizationtype, 
sl_creditcardauthorization.isarqcrequested, sl_creditcardauthorization.completiontype, sl_creditcardauthorization.riskflags, sl_creditcardauthorization.cardsequencenumber,
sl_creditcardauthorization.cardlevelresult, sl_creditcardauthorization.certificationdata, sl_transactionjournal.transactionjournalid
from sl_creditcardauthorization 
left outer join sl_card on sl_card.cardid = sl_creditcardauthorization.cardid 
left outer join sl_transactionjournal on sl_transactionjournal.creditcardauthorizationid = sl_creditcardauthorization.creditcardauthorizationid;

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
