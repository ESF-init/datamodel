SPOOL db_3_650.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.650', 'FLF', 'Add ALIGHTDURATION to SL_TransactionJournal');


-- Start adding schema changes here
-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (ALIGHTDURATION  NUMBER(18));


COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.ALIGHTDURATION IS 'Alight duration of this transaction in seconds.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;