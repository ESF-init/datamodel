SPOOL db_3_010.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.010', 'MIW', 'Added additional text field to sl_contract');

-- Start adding schema changes here

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_CONTRACT ADD (ADDITIONALTEXT NVARCHAR2(2000));

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
