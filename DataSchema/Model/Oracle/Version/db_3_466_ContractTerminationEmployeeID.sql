SPOOL db_3_466.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.466', 'BVI', 'Modified Foreign key SL_ContractTermination.EmployeeID.');

alter table SL_ContractTermination drop constraint FK_CONTTERMINATION_PERSON;

alter table SL_ContractTermination add constraint FK_CONTRACTTERMINATION_User foreign key (EMPLOYEEID) references User_List (Userid);

COMMENT ON COLUMN SL_ContractTermination.EMPLOYEEID is 'References User_List.UserID';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
