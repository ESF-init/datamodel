SPOOL db_3_281.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.281', 'DST', 'Added SL_ContractLink to support contract hierarchies.');


-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_CONTRACTLINK
(
	SOURCECONTRACTID     NUMBER(18) NOT NULL,
	TARGETCONTRACTID     NUMBER(18) NOT NULL,
  DATAROWCREATIONDATE  DATE DEFAULT SYSDATE
);

ALTER TABLE SL_CONTRACTLINK ADD CONSTRAINT PK_CONTRACTLINK PRIMARY KEY (SOURCECONTRACTID, TARGETCONTRACTID);
ALTER TABLE SL_CONTRACTLINK ADD CONSTRAINT FK_CONTRACTLINK_CONTRACT_SRC FOREIGN KEY (SOURCECONTRACTID) REFERENCES SL_CONTRACT (CONTRACTID);
ALTER TABLE SL_CONTRACTLINK ADD CONSTRAINT FK_CONTRACTLINK_CONTRACT_TRGT FOREIGN KEY (TARGETCONTRACTID) REFERENCES SL_CONTRACT (CONTRACTID);

COMMENT ON TABLE SL_CONTRACTLINK is 'Mapping table between linked contracts. Target contracts are children of the source contract.';
COMMENT ON COLUMN SL_CONTRACTLINK.SOURCECONTRACTID IS 'Unique identifier of the source contract.';
COMMENT ON COLUMN SL_CONTRACTLINK.TARGETCONTRACTID IS 'Unique identifier of the target contract.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
