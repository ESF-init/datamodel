SPOOL db_3_274.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.274', 'PCO', 'Clearing House Daily Validation Run Tables');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================
--SL_VALIDATIONRUN START
CREATE TABLE SL_VALIDATIONRUN
(
  VALIDATIONRUNID     NUMBER(10)                NOT NULL,
  PERIODSTARTDATE     DATE,
  PERIODENDDATE       DATE,
  CREATEDATE          DATE                      DEFAULT sysdate,
  VALIDATIONSTATE     NUMBER(9)                DEFAULT 0  
);

--comments
COMMENT ON TABLE SL_VALIDATIONRUN IS 'Contains information on the validation runs which have been processed. Used for Clearing House';

COMMENT ON COLUMN SL_VALIDATIONRUN.VALIDATIONRUNID IS 'Unique id of the validation run.';
COMMENT ON COLUMN SL_VALIDATIONRUN.PERIODSTARTDATE IS 'Start date of the validation period.';
COMMENT ON COLUMN SL_VALIDATIONRUN.PERIODENDDATE IS 'End date of the validation period.';
COMMENT ON COLUMN SL_VALIDATIONRUN.CREATEDATE IS 'Date when the validation was started.';
COMMENT ON COLUMN SL_VALIDATIONRUN.VALIDATIONSTATE IS 'ID of the state of the validation.';

--keys
ALTER TABLE SL_VALIDATIONRUN ADD (CONSTRAINT PK_VALIDATIONRUN PRIMARY KEY (VALIDATIONRUNID));
--SL_VALIDATIONRUN END

--SL_VALIDATIONSTATUS START

CREATE TABLE SL_VALIDATIONSTATUS
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL
);

COMMENT ON TABLE SL_VALIDATIONSTATUS IS 'Enumeration of different validation states.';

COMMENT ON COLUMN SL_VALIDATIONSTATUS.ENUMERATIONVALUE IS 'Identifying value of the validation state type.';
COMMENT ON COLUMN SL_VALIDATIONSTATUS.LITERAL IS 'Short description of the validation state type.';
COMMENT ON COLUMN SL_VALIDATIONSTATUS.DESCRIPTION IS 'Extended description of the validation state type.';

ALTER TABLE SL_VALIDATIONSTATUS ADD (CONSTRAINT PK_VALIDATIONSTATUS PRIMARY KEY (ENUMERATIONVALUE));

--SL_VALIDATIONSTATUS END

--SL_VALIDATIONRUNRULE START

CREATE TABLE SL_VALIDATIONRUNRULE
(
  VALIDATIONRUNRULEID 	NUMBER(18)  NOT NULL,
  VALIDATIONRUNID		NUMBER(10),
  VALIDATIONRULEID		NUMBER(18)  NOT NULL,	 
  STATUS				NUMBER(9) NOT NULL,  
  STARTDATETIME 		DATE,
  ENDDATETIME 			DATE  
);

COMMENT ON TABLE SL_VALIDATIONRUNRULE IS 'Records what validation rules are in a validation run';

COMMENT ON COLUMN SL_VALIDATIONRUNRULE.VALIDATIONRUNRULEID		IS 'Unique id of the validation rule run';
COMMENT ON COLUMN SL_VALIDATIONRUNRULE.VALIDATIONRUNID			IS 'FK to the validation run this rule is part of FK to SL_VALIDATIONRUN';
COMMENT ON COLUMN SL_VALIDATIONRUNRULE.VALIDATIONRULEID			IS 'ID of rule that is to be run. FK to SL_VALIDATIONRULE';
COMMENT ON COLUMN SL_VALIDATIONRUNRULE.STATUS					IS 'Status of that rules validation. Started / Finished';
COMMENT ON COLUMN SL_VALIDATIONRUNRULE.STARTDATETIME 			IS 'Start date time of validation rule';
COMMENT ON COLUMN SL_VALIDATIONRUNRULE.ENDDATETIME 				IS 'End date time of the validation rule';

ALTER TABLE SL_VALIDATIONRUNRULE ADD (CONSTRAINT PK_VALIDATIONRUNRULE PRIMARY KEY (VALIDATIONRUNRULEID));
ALTER TABLE SL_VALIDATIONRUNRULE ADD (CONSTRAINT FK_VALCLOSE_VALIDATIONRUNID FOREIGN KEY (VALIDATIONRUNID) REFERENCES SL_VALIDATIONRUN(VALIDATIONRUNID));
ALTER TABLE SL_VALIDATIONRUNRULE ADD (CONSTRAINT FK_VALCLOSE_VALIDATIONRULEID FOREIGN KEY (VALIDATIONRULEID) REFERENCES SL_VALIDATIONRULE(VALIDATIONRULEID));
--SL_VALIDATIONRUNRULE START

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(5) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_VALIDATIONRUNRULE','SL_VALIDATIONRUN');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================
INSERT INTO SL_VALIDATIONSTATUS VALUES (1, 'Started', 'Started');
INSERT INTO SL_VALIDATIONSTATUS VALUES (2, 'Finished', 'Finished');
-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL off; 
