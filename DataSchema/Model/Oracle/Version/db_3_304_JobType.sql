SPOOL db_3_304.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.304', 'DST', 'Added missing JobTypes.');

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (3, 'ParticipantImport ', 'Import participants');
INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (4, 'ParticipantUpdate ', 'Update participants');
INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (5, 'ParticipantDelete ', 'Delete participants');
INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (6, 'ThirdPartyFareMediaImport ', 'Import third party fare media');
INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (7, 'BulkSale ', 'Sale of products');
INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (8, 'TransitAccountBlock ', 'Block transit accounts');
INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (9, 'TransitAccountUnblock ', 'Unblock transit accounts');
UPDATE SL_JobType set Literal = 'ProductTransferFromStock', Description =  'Transfer products from product stock' WHERE EnumerationValue = 10;
INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (11, 'TransferProductToStock ', 'Transfer products to product stock');


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
