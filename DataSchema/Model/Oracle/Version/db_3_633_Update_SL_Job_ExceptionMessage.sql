SPOOL db_3_633.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.633', 'EMN', 'Modify exception message size on sl_job');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_JOB MODIFY EXCEPTIONMESSAGE NVARCHAR2(200);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
