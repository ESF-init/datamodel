SPOOL db_3_626.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.626', 'ULB', 'Add Rm_Transaction.FarePaymentTransactionId');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE RM_TRANSACTION
ADD (FarePaymentTransactionID VARCHAR2(40 CHAR));

COMMENT ON COLUMN 
RM_TRANSACTION.FarePaymentTransactionID IS 
'References SL_FarePayment.TransactionID';

ALTER TABLE RM_PAYMENTDETAILS
MODIFY(SEQUENCENO VARCHAR2(80 ));

COMMIT;

PROMPT Done!

SPOOL OFF;

