SPOOL dbsl_3_097.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.097', 'AMA', 'Subscription Accounting');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_POSTINGKEYCATEGORY (
    EnumerationValue NUMBER(9,0)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_POSTINGKEYCATEGORY PRIMARY KEY (EnumerationValue)
);

COMMENT ON TABLE SL_POSTINGKEYCATEGORY IS 'Contains a list of available posting key categories';
COMMENT ON COLUMN SL_POSTINGKEYCATEGORY.EnumerationValue IS 'Unique id of the category';
COMMENT ON COLUMN SL_POSTINGKEYCATEGORY.Literal IS 'Name of the category; used internally';
COMMENT ON COLUMN SL_POSTINGKEYCATEGORY.Description IS 'Category description; used in the UI';

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

ALTER TABLE SL_POSTINGKEY Add Category NUMBER(9) DEFAULT 0 NOT NULL;

COMMENT ON COLUMN SL_POSTINGKEY.Category IS 'The category of the posting key - References SL_POSTINGKEYCATEGORY.ENUMERATIONVALUE';

ALTER TABLE SL_INVOICE Add CLIENTID NUMBER(18);

ALTER TABLE SL_INVOICE
  ADD CONSTRAINT FK_INVOICE_CLIENT FOREIGN KEY (CLIENTID)
  REFERENCES VARIO_CLIENT (CLIENTID);

COMMENT ON COLUMN SL_INVOICE.CLIENTID IS 'The ID of the responsible client on the invoice. Reference to VARIO_CLIENT.CLIENTID';

ALTER TABLE SL_INVOICE Add GeneratedBy NUMBER(9) DEFAULT 0 NOT NULL;

COMMENT ON COLUMN SL_INVOICE.GeneratedBy IS 'The number of the module created the invice';

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
    INTO SL_POSTINGKEYCATEGORY (EnumerationValue, Literal, Description) VALUES (0, 'None', 'None')
    INTO SL_POSTINGKEYCATEGORY (EnumerationValue, Literal, Description) VALUES (1, 'Accounting', 'Accounting')
    INTO SL_POSTINGKEYCATEGORY (EnumerationValue, Literal, Description) VALUES (2, 'Termination', 'Termination')
    INTO SL_POSTINGKEYCATEGORY (EnumerationValue, Literal, Description) VALUES (3, 'Dunning', 'Dunning')
SELECT * FROM DUAL;

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
