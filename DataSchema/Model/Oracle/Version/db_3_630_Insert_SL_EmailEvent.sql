SPOOL db_3_630.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.630', 'SOE', 'New EventTypes inserted in SL_EmailEvent');


-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'RegisterSmartcardFromNotPrimary','RegisterSmartcard From Not Primary', -1, 'SmartcardMailMerge', 86);
INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'RemoveSmartcardFromNotPrimary','RemoveSmartcard From Not Primary', -1, 'SmartcardMailMerge', 87);
INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadCreatedFromNotPrimary','AutoloadCreated From Not Primary', -1, 'AutoloadMailMerge', 88);
INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadConfirmationFromNotPrimary','AutoloadConfirmation From Not Primary', -1, 'AutoloadMailMerge', 89);
INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadUpdatedFromNotPrimary','AutoloadUpdated From Not Primary', -1, 'AutoloadMailMerge', 90);
INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadDeletedFromNotPrimary','AutoloadDeleted From Not Primary', -1, 'AutoloadMailMerge', 91);
INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadSkippedFromNotPrimary','AutoloadSkipped From Not Primary', -1, 'AutoloadMailMerge', 92);
INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadSkippedPassManuallyAddedFromNotPrimary','AutoloadSkippedPassManuallyAdded From Not Primary', -1, 'AutoloadMailMerge', 93);
INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadSuspendedFromNotPrimary','AutoloadSuspended From Not Primary', -1, 'AutoloadMailMerge', 94);
INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadPriceChangeFromNotPrimary','AutoloadPriceChange From Not Primary', -1, 'AutoloadMailMerge', 95);
INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadTransactionFailedFromNotPrimary','AutoloadTransactionFailed From Not Primary', -1, 'AutoloadMailMerge', 96);

COMMIT;

PROMPT Done!

SPOOL OFF;
