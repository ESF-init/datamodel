SPOOL db_3_663.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.663', 'AWH', 'Update SL_NOTIFICATIONMESSAGE, add column HtmlFormat ');



-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_NOTIFICATIONMESSAGE
	ADD HTMLFORMAT NCLOB;
	
COMMENT ON COLUMN
SL_NOTIFICATIONMESSAGE.HTMLFORMAT IS
'The HTML version of the message.';

COMMIT;

PROMPT Done!

SPOOL OFF;
