SPOOL db_3_583.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.583', 'DIS', 'Add ContractId column and Foreign Key to SL_PAYMENTJOURNAL');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PAYMENTJOURNAL
 ADD (CONTRACTID  NUMBER(18));
 COMMENT ON COLUMN SL_PAYMENTJOURNAL.CONTRACTID IS 'Id of contract associated with a payment';

ALTER TABLE SL_PAYMENTJOURNAL ADD 
CONSTRAINT FK_PAYMENTJOURNAL_CONTRACTID
 FOREIGN KEY (CONTRACTID)
 REFERENCES SL_CONTRACT (CONTRACTID);


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
