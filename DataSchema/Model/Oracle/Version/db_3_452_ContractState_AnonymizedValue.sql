SPOOL db_3_452.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.452', 'BVI', 'New contract state Anonymized');

Insert into SL_CONTRACTSTATE
   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values
   (4, 'Anonymized', 'Anonymized');

COMMIT;

PROMPT Done!

SPOOL OFF;
