SPOOL db_3_656.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.656', 'SOE', 'Insert values in SL_DEVICETOKENSTATE.');

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

Insert into SL_DEVICETOKENSTATE  (ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values  (0, 'Inactive', 'Inactive');
Insert into SL_DEVICETOKENSTATE  (ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values  (1, 'Active', 'Active');


PROMPT Done!

SPOOL OFF;
