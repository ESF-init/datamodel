SPOOL db_3_640.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.640', 'BVI', 'Added new columns to SL_Invoicing.');

Alter Table SL_Invoicing add IsPseudoInvoicing Number(1);

Update sl_invoicing set IsPseudoInvoicing = 0, transactioncounter = null;

Alter Table SL_Invoicing modify IsPseudoInvoicing Number(1) default 0 Not null;

COMMENT ON COLUMN SL_Invoicing.IsPseudoInvoicing IS 'Indicates if the invoicing entry was created in pseudo mode.';

Alter Table SL_Invoicing add RawInvoicingResultData NCLOB;
COMMENT ON COLUMN SL_Invoicing.RawInvoicingResultData IS 'Variable data that can be stored here like pseudo invoice results that can not be commited.';

Alter Table SL_Invoicing add InvoicesCount Number(9);
COMMENT ON COLUMN SL_Invoicing.InvoicesCount IS 'Number of invoices created in for this invoicing.';

Alter Table SL_Invoicing add InvoiceTotalAmount Number(18);
COMMENT ON COLUMN SL_Invoicing.InvoiceTotalAmount IS 'Invoicing amount of created invoices for this invoicing.';


COMMIT;

PROMPT Done!

SPOOL OFF;
