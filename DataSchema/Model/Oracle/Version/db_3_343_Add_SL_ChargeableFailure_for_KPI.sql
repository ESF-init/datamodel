SPOOL db_3_343.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.343', 'SEA', 'Added new table SL_ChargeableFailure for defining chargeable failures for the KPI');

-- =======[ New Tables ]===========================================================================================
create table SL_ChargeableFailure (
    ChargeableFailureID number(18) NOT NULL,
	ErrorCode  	Varchar(255)                NOT NULL,
	Error				NVARCHAR2(1000)			  NOT NULL,
	IsChargeable number(1) DEFAULT 1 NOT NULL , 
	ServerityLevel number(9) DEFAULT 0 NOT NULL,
    LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_ChargeableFailure PRIMARY KEY (ChargeableFailureID) 
);

-- =======[ Trigger ]==============================================================================================
create sequence SL_ChargeableFailure_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

CREATE OR REPLACE TRIGGER SL_ChargeableFailure_BRI before insert on SL_ChargeableFailure for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_ChargeableFailure_BRU before update on SL_ChargeableFailure for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Data ]==================================================================================================
INSERT INTO SL_ChargeableFailure(ChargeableFailureID,ErrorCode, Error, ServerityLevel) VALUES (1,'94', 'Smartcard reader error', 1);
INSERT INTO SL_ChargeableFailure(ChargeableFailureID,ErrorCode, Error, ServerityLevel) VALUES (2,'63', 'Data memory near full/full', 1);
INSERT INTO SL_ChargeableFailure(ChargeableFailureID,ErrorCode, Error, ServerityLevel) VALUES (3,'53', 'Internal EEPROM error', 3);
INSERT INTO SL_ChargeableFailure(ChargeableFailureID,ErrorCode, Error, ServerityLevel) VALUES (4,'54', 'External EEPROM error', 3);
INSERT INTO SL_ChargeableFailure(ChargeableFailureID,ErrorCode, Error, ServerityLevel) VALUES (5,'147','Outdated Hotlist', 1);
INSERT INTO SL_ChargeableFailure(ChargeableFailureID,ErrorCode, Error, ServerityLevel) VALUES (6,'37', 'RTC low battery', 3);

-- =======[ Functions and Data Types ]=============================================================================
CREATE OR REPLACE FUNCTION GetMatchingDeviceHistory (p_Messagetime IN DATE, p_deviceid in NUMBER)
    RETURN NUMBER
IS
    tmpVar   NUMBER (10);
BEGIN
    tmpVar := 0;

    SELECT devicehistoryid into tmpVar
      FROM (  SELECT *
                FROM um_devicehistory
               WHERE VALIDFROM < p_Messagetime
               and deviceid = p_deviceid
                    ORDER BY validfrom DESC)
     WHERE ROWNUM = 1; 
     return tmpVar;
EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
        RETURN 0;
    WHEN OTHERS
    THEN
        -- Consider logging the error and then re-raise
        RAISE;
END GetMatchingDeviceHistory;
/

-- =======[ Comments ]===============================================================================================

COMMENT ON TABLE SL_ChargeableFailure is 'Another category to group postings.';
COMMENT ON COLUMN SL_ChargeableFailure.LASTMODIFIED is 'Technical field: date time of the last change to this dataset.';
COMMENT ON COLUMN SL_ChargeableFailure.LASTUSER is 'Technical field: last (system) user that changed this dataset.';
COMMENT ON COLUMN SL_ChargeableFailure.TRANSACTIONCOUNTER is 'Technical field: counter to prevent concurrent changes to this dataset.';
COMMENT ON COLUMN SL_ChargeableFailure.ErrorCode is 'A error code for the chargeable failure..';
COMMENT ON COLUMN SL_ChargeableFailure.Error is 'A name for the posting key type..';
COMMENT ON COLUMN SL_ChargeableFailure.ChargeableFailureID is 'Unqiue identifier for a chargeable failures.';
COMMENT ON COLUMN SL_ChargeableFailure.IsChargeable is 'defines if failure is chargeable.';
COMMENT ON COLUMN SL_ChargeableFailure.ServerityLevel is 'defines the severity level of the error. #1 will be highest. Lower severity level on higher number';

-- =======[ Views ]================================================================================================

--COMMENTED OUT DUE SM TABLES ARE NOT PART OF MOBILEvario VALIDATION SCRIPT

--create or replace view ChargeableFailure
--as
--select  m.MESSAGEID, sc.DEVICECLASSID,dh.deviceid, sc.NAME,sc.SHORTNAME,  m.ERRORCODE, m.MESSAGETIME, dh.UNITID, u.UNITNO,tu.typeofunitid, nvl(tu.description,'Unknown') typeofunit, 
--nvl(cf.error,m.messagetext
--) message,
--nvl(cf.ischargeable, 
--0
--) ischargeable,
--nvl(cf.serveritylevel, 
--0 
--) serveritylevel
--from  sm_component c ,  sm_message m, um_device d, vario_deviceclass sc, um_devicehistory dh, um_unit u, sl_chargeablefailure cf, um_typeofunit tu
--where c.COMPONENTID = m.COMPONENTID
--and  c.SERIALNUMBER = to_char(d.DEVICENO) 
--and d.DEVICECLASSID = sc.DEVICECLASSID
--and dh.DEVICEID = d.DEVICEID
--and GetMatchingDeviceHistory(m.MESSAGETIME, d.DEVICEID) = dh.DEVICEHISTORYID
--and dh.UNITID = u.UNITID(+)
--and m.errorcode = cf.errorcode(+)
--and u.typeofunitid = tu.typeofunitid(+)
--;


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;