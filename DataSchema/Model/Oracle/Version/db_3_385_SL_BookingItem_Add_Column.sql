SPOOL db_3_385.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.385', 'ARH', 'Add_Column_BikeType_To_SL_BOOKINGITEM');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_BOOKINGITEM
ADD BikeType NUMBER(10,0) null;

COMMENT ON COLUMN SL_BOOKINGITEM.BikeType IS 'Bike type if booking item is a bike sharing item.';

COMMIT;

PROMPT Done!

SPOOL OFF;