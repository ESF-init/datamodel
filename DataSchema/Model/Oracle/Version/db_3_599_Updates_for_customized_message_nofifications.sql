SPOOL db_3_599.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.599', 'FRD', 'Updates and additions for customized customer notifications');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_MESSAGEFORMAT
(
    ENUMERATIONVALUE        NUMBER(9,0) NOT NULL,
    DESCRIPTION             NVARCHAR2(50) NOT NULL
);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

INSERT INTO SL_MESSAGEFORMAT(ENUMERATIONVALUE, DESCRIPTION) VALUES (1, 'HTML');
INSERT INTO SL_MESSAGEFORMAT(ENUMERATIONVALUE, DESCRIPTION) VALUES (2, 'Text');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_MESSAGEFORMAT ADD CONSTRAINT PK_MESSAGEFORMAT PRIMARY KEY (ENUMERATIONVALUE);

ALTER TABLE SL_MESSAGETYPE ADD MESSAGEFORMAT NUMBER(9,0);
COMMENT ON COLUMN SL_MESSAGETYPE.MESSAGEFORMAT IS 'How the message is sent, reference to SL_MESSAGEFORMAT';

UPDATE SL_MESSAGETYPE SET MESSAGEFORMAT = 1 WHERE ENUMERATIONVALUE = 0;
UPDATE SL_MESSAGETYPE SET MESSAGEFORMAT = 2 WHERE ENUMERATIONVALUE IN (1,2);

ALTER TABLE SL_EMAILEVENT ADD (ISCONFIGURABLE NUMBER(1) DEFAULT 0 NOT NULL);
COMMENT ON COLUMN SL_EMAILEVENT.ISCONFIGURABLE IS 'Determines if a customer account can override activation of sending notification event.';

ALTER TABLE SL_CUSTOMERACCOUNTNOTIFICATION ADD (DEVICETOKEN  VARCHAR2(255));
COMMENT ON COLUMN SL_CUSTOMERACCOUNTNOTIFICATION.DEVICETOKEN IS 'Token used by the device.';
COMMENT ON COLUMN SL_CUSTOMERACCOUNTNOTIFICATION.SENDTO IS 'Deprecated, not in use.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
