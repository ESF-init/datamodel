SPOOL db_3_190.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.190', 'UB', 'Expand SL_COMMENT TEXT to 2000');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_COMMENT MODIFY TEXT NVARCHAR2 (2000);


---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
