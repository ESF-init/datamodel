SPOOL db_3_245.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.245', 'EPA', 'Added SL_WORKITEMVIEW');

-- Start adding schema changes here

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW SL_WORKITEMVIEW (workitemid,
        workitemsubjectid,
        title,
        memo,
        contractid,
        orderid,
        typediscriminator,
        created,
        createdby,
        executed,
        source,
        state,
        assignee,
        assigned,
        lastuser,
        cardid,
        identificationnumber,
        externalordernumber)
AS select wi.workitemid,
        wi.workitemsubjectid,
        wi.title,
        wi.memo,
        wi.contractid,
        wi.orderid,
        wi.typediscriminator,
        wi.created,
        wi.createdby,
        executed,
        wi.source,
        wi.state,
        wi.assignee,
        wi.assigned,
        wi.lastuser,
        wi.cardid,
        case when wi.typediscriminator = 'OrderWorkItem' then o1.ordernumber
            else (case when wi.typediscriminator = 'ContractWorkItem' then  c.contractnumber
            else cd.printednumber end) end identificationnumber, 
        case when wi.orderid is not null then o1.externalordernumber else null end externalordernumber
from sl_workitem wi
left join (select o.orderid, sa.externalordernumber, o.ordernumber from sl_order o left join sl_sale sa on sa.orderid = o.orderid) o1 on wi.orderid = o1.orderid
left join sl_contract c on wi.contractid = c.contractid
left join sl_card cd on wi.cardid = cd.cardid;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
