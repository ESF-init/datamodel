SPOOL db_3_058.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.058', 'SOE', 'Creation of SL_ORDERHISTORY');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_ORDERHISTORY
(
    ORDERHISTORYID       NUMBER(18,0)    NOT NULL,
    ORDERID              NUMBER(18,0)    NOT NULL,
    MESSAGE    NVARCHAR2(512),
	LASTUSER            VARCHAR2(50 Byte)   DEFAULT 'SYS' NOT NULL,
    LASTMODIFIED        DATE            DEFAULT SYSDATE NOT NULL,
    TRANSACTIONCOUNTER  INTEGER         NOT NULL,
	LASTCUSTOMER         VARCHAR2(50 Byte)   DEFAULT 'SYS' NOT NULL
);

ALTER TABLE SL_ORDERHISTORY ADD CONSTRAINT PK_ORDERHISTORY PRIMARY KEY (ORDERHISTORYID);
ALTER TABLE SL_ORDERHISTORY ADD CONSTRAINT FK_ORDERHISTORY_ORDER FOREIGN KEY (ORDERID) REFERENCES SL_ORDER (ORDERID);


COMMENT ON TABLE SL_ORDERHISTORY is 'Table containing history about table order ';
COMMENT ON COLUMN SL_ORDERHISTORY.ORDERHISTORYID is 'Unique identifier of the order history';
COMMENT ON COLUMN SL_ORDERHISTORY.ORDERID IS 'ID of the order';
COMMENT ON COLUMN SL_ORDERHISTORY.MESSAGE is '';
COMMENT ON COLUMN SL_ORDERHISTORY.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_ORDERHISTORY.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_ORDERHISTORY.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_ORDERHISTORY.LASTCUSTOMER IS 'Technical field: last customer that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
     'SL_ORDERHISTORY');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_ORDERHISTORY');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
