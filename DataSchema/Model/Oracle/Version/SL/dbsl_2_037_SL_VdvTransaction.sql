SPOOL dbsl_2_037.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.037', 'DST', 'SL_VdvTransaction: Added TariffBlock for Ambers');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW SL_VDVTRANSACTION
AS 
SELECT 
SL_Card.CardID,
RM_Transaction.SalesChannel,
RM_Transaction.TripDateTime,
RM_Transaction.Price,
TM_Ticket.Name,
VDV_TxTransaction.TxTyp,
VDV_TxTransaction.TxData,
VDV_TxAber.BerValidBegin,
VDV_TxAber.BerValidEnd,
Vdv_TxAber.BerTariffBlock
FROM SL_Card
JOIN RM_Transaction ON Paycardno = SerialNumber
JOIN TM_Ticket ON RM_Transaction.TariffID = TM_Ticket.TarifID and RM_Transaction.TikNo = TM_Ticket.InternalNumber
JOIN VDV_TxTransaction ON RM_Transaction.TransactionID = VDV_TxTransaction.TransactionID 
LEFT JOIN VDV_TxAber ON VDV_TxTransaction.TxeID = VDV_TxAber.TxeID;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
