SPOOL dbsl_2_058.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.058', 'MIW', 'Added columns to SL_PAYMENTJOURNAL');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_PAYMENTJOURNAL MODIFY (TRANSACTIONJOURNALID NULL);

ALTER TABLE SL_PAYMENTJOURNAL ADD (CreditCardNetwork NUMBER(9));

ALTER TABLE SL_PAYMENTJOURNAL ADD (IsPreTax  NUMBER(1) DEFAULT 0 NOT NULL);

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_CreditCardNetwork (
    EnumerationValue NUMBER(9, 0) NOT NULL, 
    Literal NVARCHAR2(50) NOT NULL, 
    Description NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_CreditCardNetwork PRIMARY KEY (EnumerationValue)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
    INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (0, 'Visa', 'Visa')
    INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (1, 'MasterCard', 'MasterCard')
    INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (2, 'AmericanExpress', 'AmericanExpress')
    INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (3, 'Discover', 'Discover')
SELECT * FROM DUAL; 

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
