SPOOL dbsl_2_091.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.091', 'TSC', 'Customer Demographics.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

/*
CREATE TABLE SL_CustomerQuestion (
	CustomerQuestionID NUMBER(18, 0) NOT NULL,
	Language NVARCHAR2(25) NOT NULL,
	Text NVARCHAR2(2000) NOT NULL,
	HasMultipleAnswers NUMBER(1, 0) DEFAULT 0 NOT NULL,
	IsRetired NUMBER(1, 0) DEFAULT 0 NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,	
	CONSTRAINT PK_CustomerQuestion PRIMARY KEY (CustomerQuestionID)
);

CREATE TABLE SL_CustomerAnswer (
	CustomerAnswerID NUMBER(18, 0) NOT NULL,
	CustomerQuestionID NUMBER(18, 0) NOT NULL,
	Language NVARCHAR2(25) NOT NULL,
	Text NVARCHAR2(2000) NOT NULL,
	HasFreeText NUMBER(1, 0) DEFAULT 0 NOT NULL,
	CustomerFollowUpQuestionID NUMBER(18, 0),
	IsRetired NUMBER(1, 0) DEFAULT 0 NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_CustomerAnswer PRIMARY KEY (CustomerAnswerID),
	CONSTRAINT FK_CustomerAnsw_CustomerQuesti FOREIGN KEY (CustomerQuestionID) REFERENCES SL_CustomerQuestion (CustomerQuestionID),
	CONSTRAINT FK_CustomAnsw_CustomFoUpQuesti FOREIGN KEY (CustomerFollowUpQuestionID) REFERENCES SL_CustomerQuestion (CustomerQuestionID)
);

CREATE TABLE SL_CustomerReply (
	CustomerQuestionID NUMBER(18, 0) NOT NULL,
	CustomerAnswerID NUMBER(18, 0) NOT NULL,
	PersonID NUMBER(18, 0) NOT NULL,
	FreeTextAnswer NVARCHAR2(250),
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_CustomerReply PRIMARY KEY (CustomerQuestionID, CustomerAnswerID, PersonID),
	CONSTRAINT FK_CustomerRepl_CustomerQuesti FOREIGN KEY (CustomerQuestionID) REFERENCES SL_CustomerQuestion (CustomerQuestionID),
	CONSTRAINT FK_CustomerRepl_CustomerAnswer FOREIGN KEY (CustomerAnswerID) REFERENCES SL_CustomerAnswer (CustomerAnswerID),
	CONSTRAINT FK_CustomerReply_Person FOREIGN KEY (PersonID) REFERENCES SL_Person (PersonID)
);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW SL_CustomerDemographic (PersonID, FirstName, LastName, QuestionID, Language, QuestionText, AnswerID, AnswerText, AnswerFollowUpQuestionID, FreeTextAnswer)
AS
  SELECT SL_Person.PersonID,
    SL_Person.FirstName,
    SL_Person.LastName,
    SL_CustomerQuestion.CustomerQuestionID,
    SL_CustomerQuestion.Language,
    SL_CustomerQuestion.Text,
    SL_CustomerAnswer.CustomerAnswerID,
    SL_CustomerAnswer.Text,
    SL_CustomerAnswer.CustomerFollowUpQuestionID,
    SL_CustomerReply.FreeTextAnswer
  FROM ((SL_Person
  INNER JOIN SL_CustomerReply
  ON SL_Person.PersonID = SL_CustomerReply.PersonID)
  INNER JOIN SL_CustomerQuestion
  ON SL_CustomerReply.CustomerQuestionID = SL_CustomerQuestion.CustomerQuestionID)
  INNER JOIN SL_CustomerAnswer
  ON SL_CustomerReply.CustomerAnswerID = SL_CustomerAnswer.CustomerAnswerID
  AND SL_CustomerQuestion.Language     = SL_CustomerAnswer.Language;

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_CustomerQuestion', 'SL_CustomerAnswer');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(3) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_CustomerQuestion', 'SL_CustomerAnswer', 'SL_CustomerReply');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/
*/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
