SPOOL dbsl_2_043.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.043', 'TSC', 'Added view SL_AccountPostingKey to filter values from PP_AccountPostingKey for the DriverWebsite.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW SL_AccountPostingKey ( EntryTypeNo, EntryTypeName, Manual, Export, Sign, Cancelation, DebitEntry, EntryRangeID, TaxRate, BillShown, BillClear, ReturnDebit) 
AS
	SELECT  EntryTypeNo, EntryTypeName, Manual, Export, Sign, Cancelation, DebitEntry, EntryRangeID, TaxRate, BillShown, BillClear, ReturnDebit
	FROM pp_accountpostingkey
	where entrytypeno in (100, 110, 200, 212, 220, 250)
	order by entrytypeno;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
