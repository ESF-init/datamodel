SPOOL dbsl_2_057.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.057', 'MIW', 'New tables for accounting. Part 1.');

-- =======[ New SL Tables ]========================================================================================

CREATE TABLE ACC_SalesRevenue
(
  SalesRevenueID        NUMBER(18)              NOT NULL,
  TransactionJournalID  NUMBER(18)              NOT NULL,
    
  PostingDate           Date                    ,
  PostingReference      NVARCHAR2(40)            ,
  AccountingDay         Date                    NOT NULL,
  
  CreditAccountNumber   NVARCHAR2(40)            NOT NULL,
  DebitAccountNumber    NVARCHAR2(40)            NOT NULL,
  
  Amount                NUMBER(18)              Default 0  NOT NULL,
  
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,
  
  CONSTRAINT PK_SalesRevenue PRIMARY KEY (SalesRevenueID),
  
  CONSTRAINT FK_SalesRev_TransJour FOREIGN KEY (TransactionJournalID) REFERENCES SL_TransactionJournal(TransactionJournalID)
);

CREATE TABLE ACC_PaymentRecognition
(
  PaymentRecognitionID  NUMBER(18)              NOT NULL,
  PaymentJournalID      NUMBER(18)              NOT NULL,
    
  PostingDate           Date                    ,
  PostingReference      NVARCHAR2(40)            ,
  AccountingDay         Date                    NOT NULL,
  
  CreditAccountNumber   NVARCHAR2(40)            NOT NULL,
  DebitAccountNumber    NVARCHAR2(40)            NOT NULL,
  
  Amount                NUMBER(18)              Default 0  NOT NULL,
  
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,
  
  CONSTRAINT PK_PaymentRecognition PRIMARY KEY (PaymentRecognitionID),
  
  CONSTRAINT FK_PaymentRecog_PayJour FOREIGN KEY (PaymentJournalID) REFERENCES SL_PaymentJournal(PaymentJournalID)
);

CREATE TABLE ACC_BankStatement
(
  BankStatementID       NUMBER(18)           NOT NULL,
    
  FileDate              Date                    NOT NULL,
  FileReference         NVARCHAR2(300)           NOT NULL,
  TransactionDate       Date                    NOT NULL,
  
  PaymentReference      NVARCHAR2(300)           NOT NULL,
  Amount                NUMBER(18)              Default 0  NOT NULL,
  
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,

  CONSTRAINT PK_BankStatement PRIMARY KEY (BankStatementID)
);

CREATE TABLE ACC_PaymentReconciliation
(
  PaymentReconciliationID   NUMBER(18)           NOT NULL,
  PaymentJournalID          NUMBER(18)           NOT NULL,
  BankStatementID           NUMBER(18)           NOT NULL,
    
  PostingDate           Date                    ,
  PostingReference      NVARCHAR2(40)            ,
  AccountingDay         Date                    NOT NULL,
  
  CreditAccountNumber   NVARCHAR2(40)            NOT NULL,
  DebitAccountNumber    NVARCHAR2(40)            NOT NULL,
  
  Amount                NUMBER(18)              Default 0  NOT NULL,
  
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,
  
  CONSTRAINT PK_PaymentReconciliation PRIMARY KEY (PaymentReconciliationID),
  
  CONSTRAINT FK_PaymentRecon_PayJour FOREIGN KEY (PaymentJournalID) REFERENCES SL_PaymentJournal(PaymentJournalID),
  CONSTRAINT FK_PaymentRecon_BankState FOREIGN KEY (BankStatementID) REFERENCES ACC_BankStatement(BankStatementID)
);

CREATE TABLE ACC_RevenueSettlement
(
  RevenueSettlementID   NUMBER(18)              NOT NULL,
  TransactionJournalID  NUMBER(18)              NOT NULL,
  ClientID              NUMBER(18)              NOT NULL,
    
  PostingDate           Date                    ,
  PostingReference      NVARCHAR2(40)            ,
  AccountingDay         Date                    NOT NULL,
  
  CreditAccountNumber   NVARCHAR2(40)            NOT NULL,
  DebitAccountNumber    NVARCHAR2(40)            NOT NULL,
  
  Amount                NUMBER(18)              Default 0  NOT NULL,
  
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,
  
  CONSTRAINT PK_RevenueSettlement PRIMARY KEY (RevenueSettlementID),
  
  CONSTRAINT FK_RevSettlement_TransJour FOREIGN KEY (TransactionJournalID) REFERENCES SL_TransactionJournal(TransactionJournalID),
  CONSTRAINT FK_RevSettlement_Client FOREIGN KEY (ClientID) REFERENCES VARIO_CLIENT(ClientID)
);

CREATE TABLE ACC_RevenueRecognition
(
  RevenueRecognitionID  NUMBER(18)              NOT NULL,
  TransactionJournalID  NUMBER(18)              NOT NULL,
  RevenueSettlementID   NUMBER(18)              ,
    
  PostingDate           Date                    ,
  PostingReference      NVARCHAR2(40)            ,
  AccountingDay         Date                    NOT NULL,
  
  CreditAccountNumber   NVARCHAR2(40)            NOT NULL,
  DebitAccountNumber    NVARCHAR2(40)            NOT NULL,
  
  Amount                NUMBER(18)              Default 0  NOT NULL,
  
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,
  
  CONSTRAINT PK_RevenueRecognition PRIMARY KEY (RevenueRecognitionID),
  
  CONSTRAINT FK_RevenueRecog_TransJour FOREIGN KEY (TransactionJournalID) REFERENCES SL_TransactionJournal(TransactionJournalID),
  CONSTRAINT FK_RevenueRecog_RevSettle FOREIGN KEY (RevenueSettlementID) REFERENCES ACC_RevenueSettlement(RevenueSettlementID)
);

CREATE TABLE SL_SALE
(
  SALEID                NUMBER(18)                NOT NULL,
  SALETRANSACTIONID     NVARCHAR2(40)             NOT NULL,
  SALETYPE              NUMBER(9)                 NOT NULL,
  SALEDATE              DATE                      DEFAULT SYSDATE               NOT NULL,
  CLIENTID              NUMBER(18)                NOT NULL,
  SALESCHANNELID        NUMBER(18)                NOT NULL,
  LOCATIONNUMBER        NUMBER(9)                 ,
  DEVICENUMBER          NUMBER(9)                 ,
  SALESPERSONNUMBER     NUMBER(9)                 ,
  MERCHANTNUMBER        NUMBER(9)                 ,
  RECEIPTREFERENCE      NVARCHAR2(40)             ,
  NETWORKREFERENCE      NVARCHAR2(40)             ,
  CANCELLATIONREFERENCEID      NUMBER(18)         ,
  ISCLOSED              NUMBER(1)                 DEFAULT 0,
  LASTUSER              NVARCHAR2(50)             DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE                      DEFAULT SYSDATE               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                   NOT NULL,
  
  CONSTRAINT PK_SALE PRIMARY KEY (SALEID),
  
  CONSTRAINT FK_SALE_CLIENT FOREIGN KEY (CLIENTID) REFERENCES VARIO_CLIENT (CLIENTID),
  CONSTRAINT FK_SALE_SALESCHANNEL FOREIGN KEY (SALESCHANNELID) REFERENCES VARIO_DEVICECLASS (DEVICECLASSID),
  CONSTRAINT FK_SALE_CANCELLATION FOREIGN KEY (CANCELLATIONREFERENCEID) REFERENCES SL_SALE (SALEID)
);
	
-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_SaleType (
    EnumerationValue NUMBER(9, 0) NOT NULL, 
    Literal NVARCHAR2(50) NOT NULL, 
    Description NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_SaleType PRIMARY KEY (EnumerationValue)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_PaymentJournal
    ADD (SaleId Number(18));

ALTER TABLE SL_PaymentJournal
    ADD (CONSTRAINT FK_PAYJOURNAL_SALE FOREIGN KEY (SALEID) REFERENCES SL_SALE (SALEID));
    
ALTER TABLE SL_TransactionJournal
    ADD (SaleId Number(18));
    
ALTER TABLE SL_TransactionJournal
    ADD (CONSTRAINT FK_TRANSJOURNAL_SALE FOREIGN KEY (SALEID) REFERENCES SL_SALE (SALEID));

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
  'ACC_SalesRevenue','ACC_PaymentRecognition','ACC_BankStatement',
  'ACC_PaymentReconciliation','ACC_RevenueSettlement','ACC_RevenueRecognition',
  'SL_SALE' );
   sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
  'ACC_SalesRevenue','ACC_PaymentRecognition','ACC_BankStatement',
  'ACC_PaymentReconciliation','ACC_RevenueSettlement','ACC_RevenueRecognition',
  'SL_SALE'
  );
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
    INTO SL_SaleType (EnumerationValue, Literal, Description) VALUES (0, 'Sale', 'Sale')
    INTO SL_SaleType (EnumerationValue, Literal, Description) VALUES (1, 'Order', 'Order')
    INTO SL_SaleType (EnumerationValue, Literal, Description) VALUES (2, 'Refund', 'Refund')
    INTO SL_SaleType (EnumerationValue, Literal, Description) VALUES (3, 'Cancellation', 'Cancellation')
SELECT * FROM DUAL; 

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
