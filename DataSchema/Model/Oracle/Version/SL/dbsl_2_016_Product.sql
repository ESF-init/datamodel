SPOOL dbsl_2_016.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.016', 'MIW', 'Added product fields');

-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------

ALTER TABLE SL_PRODUCT ADD (RelationFrom  NUMBER(18));

ALTER TABLE SL_PRODUCT ADD (RelationTo  NUMBER(18));

ALTER TABLE SL_PRODUCT ADD (RemainingValue  NUMBER(9));

UPDATE sl_product
SET remainingvalue = 0, transactioncounter = transactioncounter + 1
WHERE remainingvalue IS NULL;

ALTER TABLE sl_product
MODIFY  (
	remainingvalue NUMBER(9) DEFAULT 0 NOT NULL
);

COMMIT;

PROMPT Done!
SPOOL off