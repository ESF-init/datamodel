SPOOL dbsl_2_109.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.109', 'DST', 'Modified table SL_CreditCardAuthorization');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------


-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

ALTER TABLE SL_CreditCardAuthorization ADD (
  ACI NVARCHAR2(1) NULL
);

ALTER TABLE SL_CreditCardAuthorization
ADD (
	CONSTRAINT FK_CreditCardAuth_Card FOREIGN KEY (CardID) REFERENCES SL_Card(CardID)
);

-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off
