SPOOL dbsl_2_072.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.072', 'BAW', 'Accounting and new fields');

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_CUSTOMERACCOUNT ADD(PASSWORDCHANGED DATE);

ALTER TABLE SL_PAYMENTJOURNAL ADD(REFERENCENUMBER NVARCHAR2(200));

ALTER TABLE SL_ORGANIZATION ADD (EXTERNALNUMBER NVARCHAR2(50));

ALTER TABLE ACC_CLOSEOUTPERIOD ADD (STATE NUMBER(9) NOT NULL);

ALTER TABLE ACC_CLOSEOUTPERIOD ADD (CLOSEOUTDATE DATE DEFAULT sysdate NOT NULL);

ALTER TABLE ACC_CLOSEOUTPERIOD ADD (CLOSEOUTTYPE NUMBER(9) NOT NULL);

ALTER TABLE SL_NUMBERGROUP MODIFY(STARTVALUE NUMBER(18));

ALTER TABLE SL_NUMBERGROUP MODIFY(CURRENTVALUE NUMBER(18));

	
-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

Insert into SL_NUMBERGROUPSCOPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values (4, 'SalesReference', 'Reference number for custpmer receipts');

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

Insert into SL_NUMBERGROUP (NUMBERGROUPID, SCOPE, DESCRIPTION, STARTVALUE, CURRENTVALUE, LASTUSER, FORMAT, RESETATENDOFMONTH, RESETATENDOFYEAR)
 Values (SL_NUMBERGROUP_SEQ.nextval, 4, 'Sales reference sequence', 1, 1, 'sys', '[0]', 0, 0);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
