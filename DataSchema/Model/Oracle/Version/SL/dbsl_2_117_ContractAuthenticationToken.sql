SPOOL dbsl_2_117.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.117', 'BVI', 'Contract: AuthenticationToken');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

ALTER TABLE sl_contract ADD (
    ClientAuthenticationToken NVARCHAR2 (120)
);

-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off
