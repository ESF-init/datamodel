SPOOL dbsl_2_118.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.118', 'DST', 'TransactionJournal: Added timestamp InsertTime');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

alter table sl_transactionjournal add InsertTime TimeStamp;

alter table sl_transactionjournal modify inserttime default systimestamp;

-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off
