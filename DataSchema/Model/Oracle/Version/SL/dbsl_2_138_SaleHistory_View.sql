SPOOL dbsl_2_138.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.138', 'SLR', 'Modified SaleHistoryDetail view to get correct product price');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================
CREATE OR REPLACE FORCE VIEW VIEW_SL_SALEHISTORYDETAIL AS
SELECT 
    SL_SALE.SALEID,
    SL_SALE.SALETRANSACTIONID,
    SL_SALE.RECEIPTREFERENCE,
    SL_SALE.SALESCHANNELID,
    SL_SALE.SALETYPE,
    SL_SALE.LOCATIONNUMBER,
    SL_SALE.DEVICENUMBER,
    SL_SALE.SALESPERSONNUMBER,
    SL_SALE.MERCHANTNUMBER,
    SL_SALE.NETWORKREFERENCE,
    SL_SALE.SALEDATE,
    SL_SALE.CLIENTID   AS OPERATORID,
    SL_SALE.CONTRACTID AS ACCOUNTID,
    SL_SALE.EXTERNALORDERNUMBER,
    SL_SALE.ORDERID,
    COALESCE(ORD.OrderTicketName,TR.TransasctionTicketName) As TicketName,
    COALESCE(ORD.OrderTicketID,TR.TransactionTicketId) As TicketID,
    COALESCE(ORD.OrderPrice,TR.TransactionPrice) AS Price,
    ORD.STATE,
    ORD.ORDERNUMBER,
    ORD.OrderProduct As ProductId,
    GREATEST(COALESCE(ORD.OrderQuantity,0), COALESCE(TransactionQuantity,0) ) AS quantity
  FROM SL_SALE  
      LEFT OUTER JOIN      
          (SELECT  
            SL_SALE.SALETRANSACTIONID, 
            tm_ticket.name As TransasctionTicketName,
            TM_TICKET.TICKETID AS TransactionTicketId,
            MAX(SL_TRANSACTIONJOURNAL.FareAmount) As TransactionPrice,
            COUNT(*) AS TransactionQuantity
            FROM (((SL_SALE INNER JOIN SL_TRANSACTIONJOURNAL ON SL_SALE.SALEID= SL_TRANSACTIONJOURNAL.SALEID)
                            INNER JOIN TM_TICKET ON TM_TICKET.TICKETID = SL_TRANSACTIONJOURNAL.TICKETID)
                            INNER JOIN SL_PRODUCT ON SL_PRODUCT.PRODUCTID = SL_TRANSACTIONJOURNAL.PRODUCTID)
            WHERE TM_TICKET.TICKETTYPE != 200 --PURSE
            GROUP BY SL_SALE.ORDERID, TM_TICKET.TICKETID, TM_TICKET.NAME, SL_SALE.SALETRANSACTIONID
           ) TR
        On SL_SALE.SALETRANSACTIONID = TR.SALETRANSACTIONID     
        LEFT OUTER JOIN       
          (SELECT
            SL_SALE.SALEID, 
            SL_SALE.SALETRANSACTIONID, 
            SL_SALE.ORDERID,
            SL_ORDERDETAIL.ORDERDETAILID,
            SL_ORDER.ORDERNUMBER,
            SL_ORDER.STATE,
            SL_ORDERDETAIL.REQUIREDORDERDETAILID,
            CASE REQUIREDORDERDETAILID
                WHEN NULL THEN SL_ORDERDETAIL.Quantity 
                ELSE (SELECT Quantity FROM SL_ORDERDETAIL WHERE SL_ORDER.ORDERID = SL_ORDERDETAIL.ORDERID AND REQUIREDORDERDETAILID IS NULL)
            END OrderQuantity,
            SL_ORDERDETAIL.PRODUCTID AS OrderProduct,
            SL_PRODUCT.PRICE AS OrderPrice,
            TM_TICKET.TicketID AS OrderTicketID,
            TM_TICKET.NAME AS OrderTicketName
            FROM SL_SALE 
                INNER JOIN SL_ORDER ON SL_SALE.ORDERID = SL_ORDER.ORDERID
                left outer join SL_ORDERDETAIL ON SL_ORDER.ORDERID = SL_ORDERDETAIL.ORDERID
                inner JOIN SL_PRODUCT ON SL_PRODUCT.PRODUCTID = SL_ORDERDETAIL.PRODUCTID
                inner Join TM_TICKET ON TM_TICKET.TICKETID = SL_PRODUCT.TICKETID
            WHERE TM_TICKET.TICKETTYPE != 200 --PURSE
            ) ORD 
        On SL_SALE.SALETRANSACTIONID = ORD.SALETRANSACTIONID 
  GROUP BY 
    SL_SALE.SALEID,
    SL_SALE.SALETRANSACTIONID,
    SL_SALE.RECEIPTREFERENCE,
    SL_SALE.SALESCHANNELID,
    SL_SALE.SALETYPE,
    SL_SALE.LOCATIONNUMBER,
    SL_SALE.DEVICENUMBER,
    SL_SALE.SALESPERSONNUMBER,
    SL_SALE.MERCHANTNUMBER,
    SL_SALE.NETWORKREFERENCE,
    SL_SALE.SALEDATE,
    SL_SALE.CLIENTID,
    SL_SALE.CONTRACTID ,
    SL_SALE.EXTERNALORDERNUMBER,
    SL_SALE.ORDERID,
    COALESCE(ORD.OrderTicketID,TR.TransactionTicketId),
    COALESCE(ORD.OrderTicketName,TR.TransasctionTicketName),
    COALESCE(ORD.OrderPrice,TR.TransactionPrice),
    ORD.STATE,
    ORD.ORDERNUMBER,
    ORD.OrderProduct,
    ORD.OrderPrice,
    GREATEST(COALESCE(ORD.OrderQuantity,0), COALESCE(TransactionQuantity,0) );


-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
