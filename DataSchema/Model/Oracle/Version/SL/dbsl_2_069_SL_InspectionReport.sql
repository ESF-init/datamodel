SPOOL dbsl_2_069.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.069', 'MIW', 'New table InspectionReport');

-- =======[ New SL Tables ]========================================================================================

CREATE TABLE SL_InspectionReport (
    InspectionReportID NUMBER(18,0) NOT NULL,
    OperatorId NUMBER(18,0) NOT NULL,
    InspectorNumber NUMBER(9,0) NOT NULL,
    DriverNumber NUMBER(9,0) NOT NULL,
    VehicleNumber NUMBER(9,0) NOT NULL,
    LocationNumber NUMBER(9,0) NOT NULL,
    Direction NUMBER(9,0) DEFAULT 0 NOT NULL,
    Line NVARCHAR2(40) NOT NULL,
    Route NVARCHAR2(40) NOT NULL,
    InspectionStart DATE NOT NULL,
    InspectionEnd DATE NOT NULL,
    InspectionComment NVARCHAR2(1000) NOT NULL,
    RawData NCLOB NOT NULL,
    Report NCLOB,

    LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
    LastModified DATE DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER NOT NULL,    

    CONSTRAINT PK_InspectionReport PRIMARY KEY (InspectionReportID)
);

CREATE TABLE SL_TransactionToInspection (
	InspectionReportID NUMBER(18, 0) NOT NULL, 
	TransactionJournalID NUMBER(18, 0) NOT NULL, 
	CONSTRAINT PK_TransactionToInspection PRIMARY KEY (InspectionReportID, TransactionJournalID),
	CONSTRAINT FK_TransToInsp_Transaction FOREIGN KEY (TransactionJournalID) REFERENCES SL_TransactionJournal(TransactionJournalID),
	CONSTRAINT FK_TransToInsp_Inspection FOREIGN KEY (InspectionReportID) REFERENCES SL_InspectionReport(InspectionReportID)
);
	
-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Changes on existing tables ]===========================================================================
	
-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
  'SL_InspectionReport' );
   sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
	'SL_InspectionReport' 
  );
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
