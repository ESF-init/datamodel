SPOOL dbsl_2_006.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.006', 'DST', 'Created table SL_PaymentProvider');

CREATE TABLE SL_PaymentProvider (
  EnumerationValue NUMBER(9, 0) NOT NULL, 
  Literal NVARCHAR2(50) NOT NULL, 
  Description NVARCHAR2(50) NOT NULL, 
   CONSTRAINT PK_PaymentProvider PRIMARY KEY (EnumerationValue)
);

INSERT ALL
  INTO SL_PaymentProvider(EnumerationValue, Literal, Description)VALUES (0, 'None', 'None')
  INTO SL_PaymentProvider(EnumerationValue, Literal, Description)VALUES (1, 'Digicash', 'Digicash')
  INTO SL_PaymentProvider(EnumerationValue, Literal, Description)VALUES (2, 'Flashiz', 'Flashiz')
  INTO SL_PaymentProvider(EnumerationValue, Literal, Description)VALUES (3, 'InternetSecure', 'InternetSecure')
  INTO SL_PaymentProvider(EnumerationValue, Literal, Description)VALUES (4, 'Saferpay', 'Saferpay')
SELECT * FROM DUAL;

-- -------[ Sequences ]--------------------------------------------------------------------------------------------

-- -------[ Trigger ]----------------------------------------------------------------------------------------------

-- -------[ Data ]-------------------------------------------------------------------------------------------------

COMMIT;

PROMPT Done!
SPOOL off