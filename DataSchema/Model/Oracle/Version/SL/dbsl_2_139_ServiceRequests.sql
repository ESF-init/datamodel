SPOOL dbsl_2_139.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.139', 'TSC', 'Work item overview for service requests.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW SL_WorkItemOverview (WorkItemID, CardID, FareMediaID, PrintedNumber, OrderID, ContractID, ContractNumber, OrganizationID, WorkItemSubjectID, TypeDiscriminator, Title, Memo, Source, State, Assignee, CreatedBy, Created, Assigned, Executed, LastModified) 
AS
	SELECT
		SL_WorkItem.WorkItemID,
		SL_WorkItem.CardID,
		SL_Card.FareMediaID,
		SL_Card.PrintedNumber,
		SL_WorkItem.OrderID,
		SL_WorkItem.ContractID,
		SL_Contract.ContractNumber,
		SL_Contract.OrganizationID,
		SL_WorkItem.WorkItemSubjectID,
		SL_WorkItem.TypeDiscriminator,
		SL_WorkItem.Title,
		SL_WorkItem.Memo,
		SL_WorkItem.Source,
		SL_WorkItem.State,
		SL_WorkItem.Assignee,
		SL_WorkItem.CreatedBy,
		SL_WorkItem.Created,
		SL_WorkItem.Assigned,
		SL_WorkItem.Executed,
		SL_WorkItem.LastModified
	FROM (SL_WorkItem LEFT OUTER JOIN SL_Contract ON SL_WorkItem.ContractID = SL_Contract.ContractID)
    LEFT OUTER JOIN SL_Card ON SL_WorkItem.CardID = SL_Card.CardID;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
