SPOOL dbsl_2_090.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
    VALUES (sysdate, '2.090', 'JED', 'Extended WorkItems for use in Incident Management');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_WORKITEM 
ADD (
    CreatedBy  NVARCHAR2(50),
    Source  NUMBER(9,0),         -- ID to SL_Source
    State  NUMBER(9,0),          -- ID to WL_WorkItemState
    Assignee  NUMBER(18,0),      -- ID to USER_LIST
    CardID  NUMBER(18,0)         -- ID to SL_Card
);

ALTER TABLE SL_WORKITEM 
ADD CONSTRAINT FK_WorkItem_Card FOREIGN KEY (CardID) REFERENCES SL_Card(CardID); 

-- =======[ New SL Tables ]========================================================================================

CREATE TABLE SL_WorkItemHistory (
    WorkItemHistoryID NUMBER(18,0) NOT NULL,
    WorkItemID NUMBER(18,0) NOT NULL,
    Text NVARCHAR2(512) NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
    CONSTRAINT PK_WorkItemHistory PRIMARY KEY (WorkItemHistoryID),
    CONSTRAINT FK_WorkItemHistory_WorkItem FOREIGN KEY (WorkItemID) REFERENCES SL_WorkItem(WorkItemID)
);

CREATE TABLE SL_WorkItemState (
	EnumerationValue NUMBER(9, 0) NOT NULL,
	Literal NVARCHAR2(50) NOT NULL, 
	Description NVARCHAR2(50) NOT NULL, 
	CONSTRAINT PK_WorkItemState PRIMARY KEY (EnumerationValue)
);

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_WorkItemHistory');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_WorkItemHistory');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
	INTO SL_WorkItemState (EnumerationValue, Literal, Description) VALUES (0, 'New', 'New')
	INTO SL_WorkItemState (EnumerationValue, Literal, Description) VALUES (1, 'In Work', 'In Work')
	INTO SL_WorkItemState (EnumerationValue, Literal, Description) VALUES (2, 'Pending', 'Pending')
	INTO SL_WorkItemState (EnumerationValue, Literal, Description) VALUES (3, 'Need Information', 'Need Information')
    INTO SL_WorkItemState (EnumerationValue, Literal, Description) VALUES (4, 'Resolved', 'Resolved')
    INTO SL_WorkItemState (EnumerationValue, Literal, Description) VALUES (5, 'Closed', 'Closed')
SELECT * FROM DUAL;

-- =======[ Commit ]===============================================================================================

--Rollback;
Commit;
