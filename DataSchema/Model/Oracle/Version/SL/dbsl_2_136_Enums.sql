SPOOL dbsl_2_136.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
	VALUES (sysdate, '2.136', 'MIW', 'Enumerations for TransactionType and FarePaymentResultType');

DROP TABLE SL_RESULTTYPE; -- replaced by new table below.

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

CREATE TABLE SL_FAREPAYMENTRESULTTYPE
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_FAREPAYMENTRESULTTYPE PRIMARY KEY (ENUMERATIONVALUE)
);

CREATE TABLE SL_TRANSACTIONTYPE
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_TRANSACTIONTYPE PRIMARY KEY (ENUMERATIONVALUE)
);

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (0, 'Ok', 'no Error')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (1, 'Modify', 'indicates that contents were modified')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (2, 'CardPresent', 'proxcard still present after CardReleased')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (3, 'Unconcerned', 'appl. not responsible for handling transaction')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (4, 'Continue', 'appl. requests further processing')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (5, 'ModifyRescan', 'card modified, rescan required')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (6, 'ResetWarm', 'planned warm reset')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (7, 'CloneCard', 'creates a card from the prestate')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (100, 'ErrorMin', 'real Errors start here...')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (101, 'ErrorController', 'general failure')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (102, 'ErrorInvalidData', 'data corrupt')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (103, 'ErrorInvalidId', 'wrong card-ID')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (104, 'ErrorInvalidCard', 'card corrupt')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (105, 'ErrorCollision', 'more than one valid card at the same time')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (106, 'ErrorInvalidTc', 'new TC smaller than card-TC')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (107, 'ErrorAuthFailed', 'Mifare-authentication failed')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (108, 'ErrorTelegram', 'unknown telegram')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (109, 'ErrorMifarekeys', 'invalid Mifare-Keyset')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (110, 'ErrorCardLost', 'card lost while waiting for host')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (111, 'ErrorAddress', 'address not start of block or out of range')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (112, 'ErrorState', 'Wrong state for this action')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (113, 'ErrorConfiguration', 'configuration Error in controller')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (114, 'ErrorSlotEmpty', 'requested slot not active')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (115, 'ErrorNotSupported', 'Action is not supported')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (116, 'ErrorInvalidFunction', 'function not available')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (117, 'ErrorInitialize', 'initialization failed')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (118, 'ErrorControllerTimeout', 'no response from card or SAM')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (130, 'ErrorRead', 'read Error misc')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (131, 'ErrorReadPermdat', 'read Error permanent data')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (132, 'ErrorReadTc', 'read Error TC')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (133, 'ErrorReadTa', 'read Error TA-block')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (134, 'ErrorReadNoData', 'read Error no data or record available ') 
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (140, 'ErrorWrite', 'write Error misc')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (141, 'ErrorWriteTa', 'write Error TA-block misc')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (142, 'ErrorWriteTaNak', 'write Error TA-block NAK received')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (143, 'ErrorWriteTaCardLost', 'write Error TA-Block card lost')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (150, 'ErrorWriteTc', 'write Error TC misc')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (151, 'ErrorWriteTcNak', 'write Error TC NAK received')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (152, 'ErrorWriteTcCardLost', 'write Error TC card lost, TC not incremented')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (153, 'ErrorWriteTcCardLostCommit', 'write Error TC card lost on commit')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (154, 'ErrorWriteOutOfMemory', 'write Error Card or ProxFile is out of Memory')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (160, 'ErrorReaderOff', 'reader is not turned on yet')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (198, 'ErrorUnplannedReset', 'Unplanned warm reset, possible reader crash')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (199, 'ErrorControllerBusy', 'Controller busy, try again later')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (200, 'ErrorInvalidLength', 'invalid length field in transaction data')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (201, 'ErrorInvalidCompany', 'company id not matching')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (202, 'ErrorBlocked', 'card is blocked (blacklisted)')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (203, 'ErrorInvalidMac', 'MAC checksum not matching')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (204, 'ErrorInvalidKeyid', 'unknown master key id')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (205, 'ErrorValueBlock', 'get or set ValueBlock')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (206, 'ErrorHostNotReady', 'host cannot process card')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (207, 'ErrorInsufficientCredit', 'not enough credit for desired transaction')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (208, 'ErrorHostTimeout', 'timeout host waits for response of controller')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (209, 'ErrorNoLocation', 'missing locating information (line, fare stage)')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (210, 'ErrorNoFarestage', 'no fare stage found in tariff')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (211, 'ErrorNoTicket', 'no ticket found in tariff')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (212, 'ErrorNoFare', 'unable to compute fare from fare stage and ticket')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (213, 'ErrorCiMissing', 'check out fails: no check in found')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (214, 'ErrorCiExpired', 'check out fails: check in is expired')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (215, 'ErrorCiOutside', 'check out fails: check in on other line or route')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (216, 'ErrorCiSameStop', 'check out fails: check in at same stop')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (217, 'ErrorCardExpired', 'card is expired')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (218, 'ErrorInvalidCardtype', 'unknown card type')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (219, 'ErrorAccountRecord', 'cannot initialize accounting record')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (220, 'ErrorAccountWrite', 'cannot write accounting record')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (221, 'ErrorCardChanged', 'card has changed in long transaction')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (222, 'ErrorCiAlreadyDone', ' card is already checked in')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (223, 'ErrorCiWrongVehicle', 'check out fails: check in in other vehicle')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (224, 'ErrorHostInternal', 'internal host Error')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (225, 'ErrorOverflow', 'not enough space on card')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (226, 'ErrorInvalidHostData', 'inconsistent data on card')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (227, 'ErrorInvalidAction', 'invalid action list entry')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (228, 'ErrorNoTripsLeft', 'no more valid trips')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (229, 'ErrorReversal', 'reversal not possible')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (230, 'ErrorActionProcessing', 'reversal not possible')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (231, 'ErrorInvalidZone', 'ticket not valid in current zone')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (232, 'ErrorAlreadyLoggedOn', 'card of the person already logged on')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (233, 'ErrorCardIgnored', 'system currently cannot process this card type')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (234, 'ErrorActivation', 'card not activated')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (235, 'ErrorValidFuture', 'card not yet valid')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (236, 'ErrorUninitialized', 'card or datastructure not initialized ')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (237, 'ErrorManualabort', 'Process aborted (eg by driver) ')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (238, 'ErrorInvalidArray', 'invalid array on card ')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (239, 'ErrorNoApplication', 'missing application on card')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (240, 'ErrorPurseUpperLimit', 'purse upper limit overrun')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (241, 'ErrorActionAlreadyDone', 'already done')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (242, 'ErrorAlreadyActivated', 'already activated')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (243, 'ErrorCardDormant', 'Card is dormant')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (244, 'ErrorInvalidFileid', 'invalid file id')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (245, 'ErrorInvalidTicket', 'invalid ticket')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (246, 'ErrorCryption', 'ErrorCryption')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (247, 'ErrorTicketDuplicate', 'ticket duplicated, ticket already existing')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (248, 'ErrorCardConfiguration', 'Card configuration not appropriate')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (249, 'ErrorSamConfiguration', 'SAM configuration not appropriate')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (250, 'ErrorTooManyPersons', 'too many persons (group size or add. riders)')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (251, 'ErrorTicketExpired', 'ticket expired')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (252, 'ErrorInvalidTime', 'ticket not valid at current time')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (253, 'ErrorInvalidLine', 'ticket not valid on this line')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (254, 'ErrorRuledOut', 'ticket not valid due to business rule')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (255, 'ErrorInvalidOutmask', 'ticket not valid due to outmask condition')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (256, 'ErrorInvalidPaymask', 'ticket not valid due to paymask condition')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (257, 'ErrorInvalidSignature', 'to verify ticket signature')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (258, 'ErrorOffline', 'host is offline (ID based systems)')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (259, 'ErrorNoTicketOnCard', 'No (valid) ticket on card')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (260, 'ErrorCiAlreadyDenied', 'Check in has already been denied')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (261, 'ErrorTrainingMode', 'Live card presented in training mode')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (262, 'ErrorTrainingCard', 'Training card presented (in live mode)')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (300, 'ErrorDevOpen', 'Error opening connection to device')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (301, 'ErrorDevConfig', 'Error loading device specific data')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (302, 'ErrorDevTimeout', 'timeout occured')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (400, 'ErrorIpiInternal', 'internal Error (exception etc.)')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (401, 'ErrorIpiReqInvalid', 'invalid request')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (402, 'ErrorIpiTimeout', 'card request timeout')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (403, 'ErrorIpiBadCardstate', 'card state not matching')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (501, 'ErrorNoChanges', 'ErrorNoChanges')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (502, 'ErrorOutOfEepromError', 'ErrorOutOfEepromError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (503, 'ErrorIllegalCommandCode', 'ErrorIllegalCommandCode')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (504, 'ErrorIntegrityError', 'ErrorIntegrityError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (505, 'ErrorNoSuchKey', 'ErrorNoSuchKey')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (507, 'ErrorPermissionError', 'ErrorPermissionError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (508, 'ErrorParameterError', 'ErrorParameterError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (509, 'ErrorApplicationNotFound', 'ErrorApplicationNotFound')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (510, 'ErrorApplIntegrityError', 'ErrorApplIntegrityError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (511, 'ErrorAuthenticationError', 'ErrorAuthenticationError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (512, 'ErrorAdditionalFrame', 'ErrorAdditionalFrame')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (513, 'ErrorBoundaryError', 'ErrorBoundaryError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (514, 'ErrorPiccIntegrityError', 'ErrorPiccIntegrityError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (515, 'ErrorCommandAborted', 'ErrorCommandAborted')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (516, 'ErrorPiccDisabledError', 'ErrorPiccDisabledError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (517, 'ErrorCountError', 'ErrorCountError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (518, 'ErrorDuplicateError', 'ErrorDuplicateError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (519, 'ErrorEepromError', 'ErrorEepromError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (520, 'ErrorFileNotFound', 'ErrorFileNotFound')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (521, 'ErrorFileIntegrityError', 'ErrorFileIntegrityError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (522, 'ErrorLengthError', 'ErrorLengthError')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (600, 'Error_Vario_No_Tariff', 'tariff not found in Vario DB')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (601, 'Error_Vario_Not_Supported', 'not yet implemented or configured')
	 INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) values (602, 'Error_Vario_Completion', 'Error in the credit card completion process')
SELECT * FROM DUAL; 

INSERT ALL
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (0, 'None', 'Has to be determined by OVS')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (66, 'Boarding', 'Checkin/Checkout with purse')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (84, 'Transfer', 'Transfer from a previous boarding')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (85, 'Use', 'Pass use')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (99, 'Charge', 'Topup')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (108, 'Load', 'New Product')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (128, 'Cancellation', 'Cancellation')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (260, 'TuplaLoad', 'Tupla Load')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (261, 'ClientFare', 'Client Fare')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (262, 'OpenLoopVirtualCharge', 'Credit Card Compensation (Open Loop)')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (7000, 'Inspection', 'Inspection')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (9001, 'FareMediaSale', 'Fare Media Sale')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (9002, 'AddValueAccount', 'Add Value Account')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (9003, 'Refund', 'Refund')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (9004, 'ShopItemSale', 'Shop Item Sale')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (9005, 'SingleTicketSale', 'Single Ticket Sale')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (9006, 'Adjustment', 'Product adjustment')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (9007, 'DormancyFee', 'Fee for deduction on unused cards')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (9008, 'CardReplacement', 'Card Replacement')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (9009, 'BalanceTransfer', 'Balance Transfer')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (9010, 'ProductTransfer', 'Product Transfer')
	INTO SL_TRANSACTIONTYPE (EnumerationValue, Literal, Description) VALUES (777777777, 'Dummy', 'Dummy')
SELECT * FROM DUAL; 

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
