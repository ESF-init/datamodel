SPOOL dbsl_2_143.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
    VALUES (sysdate, '2.143', 'SLR', 'Added and modified enumerations and tables for fraud management');

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_RULEVIOLATIONPROVIDER
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_RULEVIOLATIONPROVIDER PRIMARY KEY (ENUMERATIONVALUE)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE SL_RuleViolationToTransaction
( 
    RuleViolationToTransactionID number(18) not null,
    RuleViolationID       number(18),
    TransactionJournalID  number(18),
    PaymentJournalID      number(18),
    TransactionID         number(18),
	LastUser              NVARCHAR2(50)   DEFAULT 'SYS'                 NOT NULL,
    LastModified          DATE            DEFAULT sysdate               NOT NULL,
    TransactionCounter    INTEGER         NOT NULL,
    CONSTRAINT PK_RuleViolationToTransaction PRIMARY KEY (RuleViolationToTransactionID),
    CONSTRAINT FK_RuleViolToTran_RuleViol FOREIGN KEY (RuleViolationID) REFERENCES SL_RULEVIOLATION(RuleViolationID),
    CONSTRAINT FK_RuleViolToTran_TransJour FOREIGN KEY (TransactionJournalID) REFERENCES SL_TRANSACTIONJOURNAL(TransactionJournalID),
    CONSTRAINT FK_RuleViolToTrans_PaymJour FOREIGN KEY (PaymentJournalID) REFERENCES SL_PAYMENTJOURNAL(PaymentJournalID),
    CONSTRAINT FK_RuleViolToTrans_Trans FOREIGN KEY (TransactionID) REFERENCES RM_TRANSACTION(TransactionID)
);

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_RULEVIOLATION 
DROP CONSTRAINT FK_RULEVIOLATION_TRANSJOUR;

ALTER TABLE SL_RULEVIOLATION
ADD CardID NUMBER(18);

ALTER TABLE SL_RULEVIOLATION
ADD CONSTRAINT FK_RULEVIOLATION_CARD FOREIGN KEY (CardID) REFERENCES SL_CARD(CardId);

ALTER TABLE SL_RULEVIOLATION
ADD Text NVARCHAR2(2000);

ALTER TABLE SL_RULEVIOLATION
ADD RuleViolationProvider NUMBER(9) DEFAULT 0 NOT NULL;

ALTER TABLE SL_RULEVIOLATION
ADD IsResolved NUMBER(1) DEFAULT 0 NOT NULL;

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

CREATE SEQUENCE SL_RuleViolToTrans_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================

CREATE OR REPLACE TRIGGER SL_RuleViolToTrans_BRU before update ON SL_RuleViolationToTransaction for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_RuleViolToTrans_BRI before insert ON SL_RuleViolationToTransaction for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

DELETE FROM SL_RULEVIOLATIONTYPE;

INSERT ALL
    INTO SL_RULEVIOLATIONTYPE (EnumerationValue, Literal, Description) VALUES (0, 'PRODUCT_USAGE_VELOCITY', 'Product Usage Velocity')
    INTO SL_RULEVIOLATIONTYPE (EnumerationValue, Literal, Description) VALUES (1, 'PRODUCT_LOAD_VELOCITY', 'Product Load Velocity')
    INTO SL_RULEVIOLATIONTYPE (EnumerationValue, Literal, Description) VALUES (2, 'TRANSACTION_VELOCITY', 'Transaction Velocity')
    INTO SL_RULEVIOLATIONTYPE (EnumerationValue, Literal, Description) VALUES (3, 'GEOGRAPHICAL_VELOCITY', 'Geographical Velocity')
SELECT * FROM DUAL; 

INSERT ALL
    INTO SL_RULEVIOLATIONPROVIDER (EnumerationValue, Literal, Description) VALUES (0, 'UNKNOWN', 'Unknown')
    INTO SL_RULEVIOLATIONPROVIDER (EnumerationValue, Literal, Description) VALUES (1, 'SL', 'SL')
    INTO SL_RULEVIOLATIONPROVIDER (EnumerationValue, Literal, Description) VALUES (2, 'RM', 'RM')
    INTO SL_RULEVIOLATIONPROVIDER (EnumerationValue, Literal, Description) VALUES (3, 'VDVKA', 'VDVKA')
SELECT * FROM DUAL; 

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
