SPOOL dbsl_2_151.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.151', 'SLR', 'TransactionJournal view added.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW VIEW_SL_TRANSACTIONJOURNAL
AS
   SELECT sl_transactionjournal.transactionjournalid,
          sl_transactionjournal.transactionid,
          sl_transactionjournal.devicetime,
          sl_transactionjournal.boardingtime,
          sl_transactionjournal.operatorid, sl_transactionjournal.clientid,
          sl_transactionjournal.line, sl_transactionjournal.coursenumber,
          sl_transactionjournal.routenumber, sl_transactionjournal.routecode,
          sl_transactionjournal.direction, sl_transactionjournal.ZONE,
          sl_transactionjournal.saleschannelid,
          sl_transactionjournal.devicenumber,
          sl_transactionjournal.vehiclenumber,
          sl_transactionjournal.mountingplatenumber,
          sl_transactionjournal.debtornumber,
          sl_transactionjournal.geolocationlongitude,
          sl_transactionjournal.geolocationlatitude,
          sl_transactionjournal.stopnumber,
          sl_transactionjournal.transitaccountid,
          sl_transactionjournal.faremediaid,
          DECODE (sl_transactionjournal.faremediatype,
                  215, 'ClosedLoop',
                  217, 'OpenLoop',
                  sl_transactionjournal.faremediatype
                 ) AS faremediatype,
          sl_transactionjournal.productid, sl_transactionjournal.ticketid,
          sl_transactionjournal.fareamount,
          DECODE (sl_transactionjournal.customergroup,
                  1, 'Adult',
                  2, 'Youth',
                  3, 'Honored Citizen',
                  4, 'Paratransit',
                  sl_transactionjournal.customergroup
                 ) AS customergroup,
          sl_transactionjournal.transactiontype,
          sl_transactionjournal.transactionnumber,
          sl_transactionjournal.resulttype, sl_transactionjournal.filllevel,
          sl_transactionjournal.pursebalance,
          sl_transactionjournal.pursecredit, sl_transactionjournal.groupsize,
          sl_transactionjournal.validfrom, sl_transactionjournal.validto,
          sl_transactionjournal.DURATION, sl_transactionjournal.tariffdate,
          sl_transactionjournal.tariffversion,
          sl_transactionjournal.ticketinternalnumber,
          sl_transactionjournal.whitelistversion,
          sl_transactionjournal.cancellationreference,
          sl_transactionjournal.cancellationreferenceguid,
          sl_transactionjournal.lastuser, sl_transactionjournal.lastmodified,
          sl_transactionjournal.transactioncounter,
          sl_transactionjournal.whitelistversioncreated,
          sl_transactionjournal.properties, sl_transactionjournal.saleid,
          sl_transactionjournal.tripticketinternalnumber,
          tm_ticket.NAME AS ticketname, sl_transactionjournal.shiftbegin,
          DECODE (sl_transactionjournal.completionstate,
                  0, 'Pending',
                  1, 'Online',
                  2, 'Offline',
                  3, 'Error',
                  sl_transactionjournal.completionstate
                 ) AS completionstate,
          sl_transactionjournal.responsestate,
          sl_transactionjournal.productid2,
          sl_transactionjournal.pursebalance2,
          sl_transactionjournal.pursecredit2,
          sl_transactionjournal.creditcardauthorizationid,
          sl_transactionjournal.tripcounter, sl_transactionjournal.inserttime
     FROM sl_transactionjournal, tm_ticket
    WHERE sl_transactionjournal.ticketid = tm_ticket.ticketid;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
