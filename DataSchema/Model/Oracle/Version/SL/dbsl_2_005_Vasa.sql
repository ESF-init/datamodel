SPOOL dbsl_2_005.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.005', 'DST', 'Created table VAS_FLEXVALUE');

CREATE TABLE VAS_FLEXVALUE
(
   FLEXVALUEID         NUMBER(18)         NOT NULL,
   NAME                NVARCHAR2(50)     NOT NULL,
   CONTENT             NVARCHAR2(2000)   NOT NULL,
   DISPLAYORDER        NUMBER(9)         NOT NULL,
   LASTUSER            NVARCHAR2(50)     DEFAULT 'SYS' NOT NULL,
   LASTMODIFIED        DATE              DEFAULT sysdate NOT NULL,
   TRANSACTIONCOUNTER  NUMBER            NOT NULL,
   CONSTRAINT PK_FlexValue PRIMARY KEY (FlexValueID)
);

-- -------[ Sequences ]--------------------------------------------------------------------------------------------

CREATE SEQUENCE VAS_FLEXVALUE_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;

-- -------[ Trigger ]----------------------------------------------------------------------------------------------
CREATE OR REPLACE TRIGGER VAS_FLEXVALUE_BRI before insert on VAS_FLEXVALUE for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER VAS_FLEXVALUE_BRU before update on VAS_FLEXVALUE for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- -------[ Data ]-------------------------------------------------------------------------------------------------

COMMIT;

PROMPT Done!
SPOOL off