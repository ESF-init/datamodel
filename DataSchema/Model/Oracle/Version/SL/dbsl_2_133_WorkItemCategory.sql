SPOOL dbsl_2_133.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.133', 'TSC', 'Added work item category for service request and incident management.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_WorkItemCategory (
	EnumerationValue NUMBER(9, 0) NOT NULL, 
	Literal NVARCHAR2(50) NOT NULL, 
	Description NVARCHAR2(50) NOT NULL, 
	CONSTRAINT PK_WorkItemCategory PRIMARY KEY (EnumerationValue)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
	INTO SL_WorkItemCategory (EnumerationValue, Literal, Description) VALUES (0, 'Unknown', 'Unknown')
	INTO SL_WorkItemCategory (EnumerationValue, Literal, Description) VALUES (1, 'Card', 'Card')
	INTO SL_WorkItemCategory (EnumerationValue, Literal, Description) VALUES (2, 'Order', 'Order')
	INTO SL_WorkItemCategory (EnumerationValue, Literal, Description) VALUES (3, 'CustomerContract', 'Customer Contract')
	INTO SL_WorkItemCategory (EnumerationValue, Literal, Description) VALUES (4, 'OrganizationContract', 'Organization Contract')
SELECT * FROM DUAL; 

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
