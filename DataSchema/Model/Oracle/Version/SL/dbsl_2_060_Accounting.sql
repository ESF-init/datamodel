SPOOL dbsl_2_060.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.060', 'MIW', 'New tables for accounting. Part 2.');

-- =======[ New SL Tables ]========================================================================================

CREATE TABLE ACC_CloseoutPeriod
(
  CloseoutPeriodID      NUMBER(18)              NOT NULL,
  PeriodFrom            Date                    ,
  PeriodTo              Date                    ,
  
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,
  
  CONSTRAINT PK_CloseoutPeriod PRIMARY KEY (CloseoutPeriodID)
);
	
-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE ACC_SalesRevenue
	DROP COLUMN ACCOUNTINGDAY;

ALTER TABLE ACC_PaymentRecognition
	DROP COLUMN ACCOUNTINGDAY;

ALTER TABLE ACC_PaymentReconciliation
	DROP COLUMN ACCOUNTINGDAY;

ALTER TABLE ACC_RevenueSettlement
	DROP COLUMN ACCOUNTINGDAY;

ALTER TABLE ACC_RevenueRecognition
	DROP COLUMN ACCOUNTINGDAY;

	
ALTER TABLE ACC_SalesRevenue
	ADD (CloseoutPeriodID Number(18));
	
ALTER TABLE ACC_PaymentRecognition
	ADD (CloseoutPeriodID Number(18));
	
ALTER TABLE ACC_PaymentReconciliation
	ADD (CloseoutPeriodID Number(18));

ALTER TABLE ACC_RevenueSettlement
	ADD (CloseoutPeriodID Number(18));

ALTER TABLE ACC_RevenueRecognition
    ADD (CloseoutPeriodID Number(18));

	
ALTER TABLE ACC_SalesRevenue
	ADD (CONSTRAINT FK_SalesRevenue_Closeout FOREIGN KEY (CloseoutPeriodID) REFERENCES ACC_CloseoutPeriod (CloseoutPeriodID));
	
ALTER TABLE ACC_PaymentRecognition
	ADD (CONSTRAINT FK_PaymentRecog_Closeout FOREIGN KEY (CloseoutPeriodID) REFERENCES ACC_CloseoutPeriod (CloseoutPeriodID));
	
ALTER TABLE ACC_PaymentReconciliation
	ADD (CONSTRAINT FK_PaymentRecon_Closeout FOREIGN KEY (CloseoutPeriodID) REFERENCES ACC_CloseoutPeriod (CloseoutPeriodID));

ALTER TABLE ACC_RevenueSettlement
	ADD (CONSTRAINT FK_RevenueSettlement_Closeout FOREIGN KEY (CloseoutPeriodID) REFERENCES ACC_CloseoutPeriod (CloseoutPeriodID));

ALTER TABLE ACC_RevenueRecognition
    ADD (CONSTRAINT FK_RevenueRecog_Closeout FOREIGN KEY (CloseoutPeriodID) REFERENCES ACC_CloseoutPeriod (CloseoutPeriodID));
	
	
ALTER TABLE ACC_Bankstatement
	ADD (State Number(9));
	
-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
  'ACC_CloseoutPeriod' );
   sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
	'ACC_CloseoutPeriod' 
  );
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
