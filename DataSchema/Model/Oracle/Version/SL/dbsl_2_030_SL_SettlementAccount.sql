SPOOL dbsl_2_030.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.030', 'FMT', 'Added SL_SettlementAccount');


-- =======[ New SL Tables ]========================================================================================
CREATE TABLE SL_SETTLEMENTACCOUNT
(
  SETTLEMENTACCOUNTID   NUMBER(18)                NOT NULL,
  NAME                  NVARCHAR2(50),
  DESCRIPTION         NVARCHAR2(200),
  LASTUSER            NVARCHAR2(50)             DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED        DATE                      DEFAULT SYSDATE               NOT NULL,
  TRANSACTIONCOUNTER  INTEGER                   NOT NULL,
  CONSTRAINT PK_SETTLEMENTACCOUNT PRIMARY KEY (SETTLEMENTACCOUNTID)
);


-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_SETTLEMENTSETUP 
 ADD (
	FROMACCOUNTID  NUMBER(18),
        TOACCOUNTID  NUMBER(18),
	CONSTRAINT FK_SETTLEMENTSETUP_FromAccount  FOREIGN KEY (FROMACCOUNTID)  REFERENCES SL_SETTLEMENTACCOUNT (SETTLEMENTACCOUNTID),
	CONSTRAINT FK_SETTLEMENTSETUP_ToAccount  FOREIGN KEY (ToACCOUNTID)  REFERENCES SL_SETTLEMENTACCOUNT (SETTLEMENTACCOUNTID)
 );

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

create sequence SL_SETTLEMENTACCOUNT_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================

 
CREATE OR REPLACE TRIGGER SL_SETTLEMENTACCOUNT_BRI before insert ON SL_SETTLEMENTACCOUNT for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_SETTLEMENTACCOUNT_BRU before update ON SL_SETTLEMENTACCOUNT for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
