SPOOL dbsl_2_056.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.056', 'FLF', 'Added column PROPERTIES to SL_TRANSACTIONJOURNAL ');

-- =======[ Changes on existing tables ]===========================================================================
ALTER TRIGGER SL_TRANSACTIONJOURNAL_BRU DISABLE;
ALTER TABLE SL_TRANSACTIONJOURNAL ADD (PROPERTIES NUMBER(9) DEFAULT 0);
ALTER TRIGGER SL_TRANSACTIONJOURNAL_BRU ENABLE;

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
