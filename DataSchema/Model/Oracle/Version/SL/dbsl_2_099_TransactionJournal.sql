SPOOL dbsl_2_099.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.099', 'FLF', 'Added new columns for multiple purse use');

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (PRODUCTID2  NUMBER(18));

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (PURSEBALANCE2  NUMBER(9));

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (PURSECREDIT2  NUMBER(9));
 
 
-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
