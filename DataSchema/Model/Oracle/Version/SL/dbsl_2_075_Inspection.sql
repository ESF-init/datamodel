SPOOL dbsl_2_075.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.075', 'MIW', 'Added tables and new fields for inspection reports');

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_INSPECTIONRESULT
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL,
  CONSTRAINT PK_INSPECTIONRESULT PRIMARY KEY (ENUMERATIONVALUE)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE SL_INSPECTIONCRITERION
(
  INSPECTIONCRITERIONID	NUMBER(18)                NOT NULL,
  INSPECTIONREPORTID	  NUMBER(18)                NOT NULL,
  CRITERION             NVARCHAR2(200)            NOT NULL,
  CRITERIONCOMMENT      NVARCHAR2(1000),
  INSPECTIONRESULT		  NUMBER(9)                 NOT NULL,
  LASTUSER            	NVARCHAR2(50)             DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED        	DATE                      DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER  	INTEGER                   NOT NULL,
  CONSTRAINT PK_INSPECTIONCRITERION PRIMARY KEY (INSPECTIONCRITERIONID)
);

ALTER TABLE SL_INSPECTIONCRITERION 
ADD (
  CONSTRAINT FK_INSPCRITERION_INSPREPORT FOREIGN KEY (INSPECTIONREPORTID) REFERENCES SL_InspectionReport(InspectionReportID)
);

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_INSPECTIONREPORT MODIFY(INSPECTIONCOMMENT NULL);

ALTER TABLE SL_INSPECTIONREPORT ADD (IsProcessed NUMBER(1) DEFAULT 0 NOT NULL);

ALTER TABLE SL_INSPECTIONREPORT ADD (SUPERVISORCOMMENT NVARCHAR2(1000));

ALTER TABLE SL_PHOTOGRAPH ADD (CAPTION NVARCHAR2(1000));

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

CREATE SEQUENCE SL_INSPECTIONCRITERION_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================

CREATE OR REPLACE TRIGGER SL_INSPECTIONCRITERION_BRU before update ON SL_INSPECTIONCRITERION for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_INSPECTIONCRITERION_BRI before insert ON SL_INSPECTIONCRITERION for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
	INTO SL_INSPECTIONRESULT (EnumerationValue, Literal, Description) VALUES (0, 'NotChecked', 'NotChecked')
	INTO SL_INSPECTIONRESULT (EnumerationValue, Literal, Description) VALUES (1, 'OK', 'OK')
	INTO SL_INSPECTIONRESULT (EnumerationValue, Literal, Description) VALUES (2, 'NotOK', 'NotOK')
SELECT * FROM DUAL; 

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
