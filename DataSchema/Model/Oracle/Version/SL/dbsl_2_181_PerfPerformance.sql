SPOOL dbsl_2_181.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.181', 'SLR', 'Create new table for performance aggregation.');

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_AGGREGATION_FUNCTION
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL,
  CONSTRAINT PK_AGGREGATION_FUNCTION PRIMARY KEY (ENUMERATIONVALUE)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE PERF_AGGREGATION
(
  AGGREGATIONID         NUMBER(18)      NOT NULL,
  AGGREGATIONFROM       DATE            NOT NULL,
  AGGREGATIONTO         DATE            NOT NULL,
  AGGREGATIONFUNCTION   NUMBER(9)       NOT NULL,
  AGGREGATIONVALUE      NUMBER(18,2)    NOT NULL,
  INDICATORID           NUMBER(18)      NOT NULL,
  DESCRIPTION           NVARCHAR2(200)          ,
  LASTUSER              NVARCHAR2(50)   DEFAULT 'SYS'     NOT NULL,
  LASTMODIFIED          DATE            DEFAULT sysdate   NOT NULL,
  TRANSACTIONCOUNTER    INTEGER         NOT NULL,
  
  CONSTRAINT PK_AGGREGATION PRIMARY KEY (AGGREGATIONID),
  CONSTRAINT FK_AGGREGATION_INDICATOR FOREIGN KEY (INDICATORID) REFERENCES PERF_INDICATOR(INDICATORID)
);


-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('PERF_AGGREGATION');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('PERF_AGGREGATION');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (0, 'None', 'None');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (1, 'Count', 'Count');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (2, 'CountDistinct', 'Count Distinct');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (3, 'CountRow', 'Count Row');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (4, 'CountBig', 'Count Big');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (5, 'CountBigDistinct', 'Count Big Distinct');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (6, 'CountBigRow', 'Count Big Row');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (7, 'Avg', 'Average');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (8, 'AvgDistinct', 'Average Distinct');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (9, 'Max', 'Maximum');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (10, 'Min', 'Minimum');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (11, 'Sum', 'Sum');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (12, 'SumDistinct', 'Sum Distinct');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (13, 'StDev', 'Standard Deviation');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (14, 'StDevDistinct', 'Standard Deviation Distinct');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (15, 'Variance', 'Variance');
INSERT INTO SL_AGGREGATION_FUNCTION (EnumerationValue, Literal, Description) VALUES (16, 'VarianceDistinct', 'Variance Distinct');



COMMIT;

PROMPT Done!
SPOOL off