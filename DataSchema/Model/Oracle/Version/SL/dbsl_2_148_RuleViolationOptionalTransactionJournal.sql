SPOOL dbsl_2_148.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.148', 'SLR', 'SL_RuleViolation: Modified TransactionJournalID to DEFAULT NULL');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

ALTER TABLE SL_RULEVIOLATION 
MODIFY (TRANSACTIONJOURNALID NULL);

-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off
