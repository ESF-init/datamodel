SPOOL dbsl_2_166.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.166', 'SMF', 'Added tables for FareEvasionFine');

-- =======[ Changes on existing tables ]===========================================================================

-- Should be written to section 'Master Tables' but must be written here to execute the script as a whole (referenced in existing table SL_INSPECTIONREPORT).
CREATE TABLE SL_FareEvasionInspection (
    FareEvasionInspectionID NUMBER(18,0)                  NOT NULL,
    InspectionNumber        NUMBER(18,0)                  NOT NULL,
    InspectionName          NVARCHAR2(25)                 NOT NULL,
    Description             NVARCHAR2(50)                         ,
    IsControl               NUMBER(1,0)   DEFAULT 0       NOT NULL,
    ClientID                NUMBER(18,0)                  NOT NULL,
    LastUser                NVARCHAR2(50) DEFAULT 'SYS'   NOT NULL,
    LastModified            DATE          DEFAULT sysdate NOT NULL,
    TransactionCounter      INTEGER                       NOT NULL,

    CONSTRAINT PK_FareEvasionInspection PRIMARY KEY (FareEvasionInspectionID),
    CONSTRAINT FK_FareEvasionInsp_Client FOREIGN KEY (ClientID) REFERENCES VARIO_CLIENT(ClientID)
);

ALTER TABLE SL_INSPECTIONREPORT ADD (InspectionCount NUMBER(9,0));
ALTER TABLE SL_INSPECTIONREPORT ADD (FareEvasionInspectionID NUMBER(9,0));
ALTER TABLE SL_INSPECTIONREPORT
 ADD CONSTRAINT FK_InspectionReport_Type FOREIGN KEY (FareEvasionInspectionID) REFERENCES SL_FareEvasionInspection(FareEvasionInspectionID);

ALTER TABLE SL_LOOKUPADDRESS ADD (StreetNumberTo NVARCHAR2(25));

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_FareEvasionState (
    EnumerationValue NUMBER(9,0)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_FareEvasionState PRIMARY KEY (EnumerationValue)
);

CREATE TABLE SL_AddressBookEntryType (
    EnumerationValue NUMBER(9,0)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_OfficeType PRIMARY KEY (EnumerationValue)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE SL_TransportCompany
(
    TransportCompanyID NUMBER(18,0)                  NOT NULL,
    CompanyNumber      NUMBER(18,0)                  NOT NULL,
    CompanyName        NVARCHAR2(25)                 NOT NULL,
    Description        NVARCHAR2(50)                         ,
    ClientID           NUMBER(18,0)                  NOT NULL,
    LastUser           NVARCHAR2(50) DEFAULT 'SYS'   NOT NULL,
    LastModified       DATE          DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER                       NOT NULL,

    CONSTRAINT PK_TransportCompany PRIMARY KEY (TransportCompanyID),
    CONSTRAINT FK_TransCompany_Client FOREIGN KEY (ClientID) REFERENCES VARIO_CLIENT(ClientID)
);


CREATE TABLE SL_IdentificationType
(
    IdentificationTypeID NUMBER(18,0)                  NOT NULL,
    TypeNumber           NUMBER(18,0)                  NOT NULL,
    TypeName             NVARCHAR2(25)                 NOT NULL,
    Description          NVARCHAR2(50)                         ,
    ClientID             NUMBER(18,0)                  NOT NULL,
    LastUser             NVARCHAR2(50) DEFAULT 'SYS'   NOT NULL,
    LastModified         DATE          DEFAULT sysdate NOT NULL,
    TransactionCounter   INTEGER                       NOT NULL,

    CONSTRAINT PK_IdentificationType PRIMARY KEY (IdentificationTypeID),
    CONSTRAINT FK_IdentType_Client FOREIGN KEY (ClientID) REFERENCES VARIO_CLIENT(ClientID)
);

CREATE TABLE SL_FareEvasionBehaviour
(
    FareEvasionBehaviourID NUMBER(18,0)                  NOT NULL,
    BehaviourNumber        NUMBER(18,0)                  NOT NULL,
    BehaviourName          NVARCHAR2(25)                 NOT NULL,
    Description            NVARCHAR2(50)                         ,
    ClientID               NUMBER(18,0)                  NOT NULL,
    LastUser               NVARCHAR2(50) DEFAULT 'SYS'   NOT NULL,
    LastModified           DATE          DEFAULT sysdate NOT NULL,
    TransactionCounter     INTEGER                       NOT NULL,

    CONSTRAINT PK_FareEvasionBehaviour PRIMARY KEY (FareEvasionBehaviourID),
    CONSTRAINT FK_FareEvasionBeh_Client FOREIGN KEY (ClientID) REFERENCES VARIO_CLIENT(ClientID)
);

CREATE TABLE SL_FareChangeCause
(
    FareChangeCauseID  NUMBER(18,0)                  NOT NULL,
    ChangeCauseNumber  NUMBER(18,0)                  NOT NULL,
    ChangeCauseName    NVARCHAR2(25)                 NOT NULL,
    Description        NVARCHAR2(50)                         ,
    ClientID           NUMBER(18,0)                  NOT NULL,
    LastUser           NVARCHAR2(50) DEFAULT 'SYS'   NOT NULL,
    LastModified       DATE          DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER                       NOT NULL,

    CONSTRAINT PK_FareChangeCause PRIMARY KEY (FareChangeCauseID),
    CONSTRAINT FK_FareChangeCause_Client FOREIGN KEY (ClientID) REFERENCES VARIO_CLIENT(ClientID)
);

-- No defaults for this table, must be set in code in this case to find out in code which columns have already been populated by data and which contain defaults.
CREATE TABLE SL_FareEvasionIncident
(
    FareEvasionIncidentID  NUMBER(18,0)                   NOT NULL,
    IncidentNumber         NVARCHAR2(25)                  NOT NULL,
    FareEvasionReasonID    NUMBER(18,0)                           ,
    InspectionTime         DATE                           NOT NULL,
    LineName               NVARCHAR2(125)                         ,
    InspectionStopNumber   NUMBER(18,0)                           ,
    DirectionNumber        NUMBER(18,0)                           ,
    DebtorID               NUMBER(18,0)                           ,
    TransportCompanyID     NUMBER(18,0)                           ,
    PrincipalClaim         NUMBER(9,0)                            ,
    IsPayed                NUMBER(1,0)                    NOT NULL,
    PersonID               NUMBER(18,0)                   NOT NULL,
    GuardianID             NUMBER(18,0)                           ,
    IdentificationNumber   NVARCHAR2(50)                  NOT NULL,
    IdentificationTypeID   NUMBER(9,0)                    NOT NULL,
    Notes                  NVARCHAR2(512)                         ,
    FareEvasionBehaviourID NUMBER(9,0)                           ,
	FareEvasionCategoryID  NUMBER(18,0)                           ,
    IsAddressPoliceChecked NUMBER(1,0)                    NOT NULL,
    IsPenaltyDemanded      NUMBER(1,0)                    NOT NULL,
    PenaltyDemandDate      DATE                           NOT NULL,
    StatusChangeDate       DATE                                   ,
    ReceiptPadNumber       NVARCHAR2(50)                          ,
    PaymentDueDate         DATE                           NOT NULL,
    InsertionTime          DATE                           NOT NULL,
    InsertionUser          NVARCHAR2(50)                  NOT NULL,
    IsManuallyInserted     NUMBER(1,0)                    NOT NULL,
    Status                 NUMBER(9,0)                    NOT NULL, -- ENUM SL_FareEvasionState
    IsAppealed             NUMBER(1,0)                    NOT NULL,
    AppealDate             DATE                           NOT NULL,
    RecurringLinkID        NVARCHAR2(50)                          ,
    BinaryDataID           NUMBER(18,0)                           ,
    ContractID             NUMBER(18,0)                           ,
    FareChangeCauseID      NUMBER(9,0)                            ,
    InspectionReportID     NUMBER(18,0)                           ,
    IsTicketWithdrawn      NUMBER(1,0)                    NOT NULL,
    IsBlackened            NUMBER(1,0)                    NOT NULL,
    CompletedDateTime      DATE                           NOT NULL,
    ClientID               NUMBER(18,0)                   NOT NULL,
    LastUser               NVARCHAR2(50)  DEFAULT 'SYS'   NOT NULL,
    LastModified           DATE           DEFAULT sysdate NOT NULL,
    TransactionCounter     INTEGER                        NOT NULL,

    CONSTRAINT PK_FareEvasionIncident PRIMARY KEY (FareEvasionIncidentID),
    CONSTRAINT FK_FareEvasionInc_Reason     FOREIGN KEY (FareEvasionReasonID) REFERENCES TM_FareEvasionReason(FareEvasionReasonID),
    CONSTRAINT FK_FareEvasionInc_InspRep    FOREIGN KEY (InspectionReportID) REFERENCES SL_InspectionReport(InspectionReportID),
    CONSTRAINT FK_FareEvasionInc_Debtor     FOREIGN KEY (DebtorID) REFERENCES DM_DEBTOR(DebtorID),
    CONSTRAINT FK_FareEvasionInc_TransComp  FOREIGN KEY (TransportCompanyID) REFERENCES SL_TransportCompany(TransportCompanyID),
    CONSTRAINT FK_FareEvasionInc_Person     FOREIGN KEY (PersonID) REFERENCES SL_PERSON(PersonID),
    CONSTRAINT FK_FareEvasionInc_Guardian   FOREIGN KEY (GuardianID) REFERENCES SL_PERSON(PersonID),
    CONSTRAINT FK_FareEvasionInc_IdentType  FOREIGN KEY (IdentificationTypeID) REFERENCES SL_IdentificationType(IdentificationTypeID),
    CONSTRAINT FK_FareEvasionInc_Behaviour  FOREIGN KEY (FareEvasionBehaviourID) REFERENCES SL_FareEvasionBehaviour(FareEvasionBehaviourID),
    CONSTRAINT FK_FareEvasionInc_BinaryData FOREIGN KEY (BinaryDataID) REFERENCES RM_BINARYDATA(ID),
    CONSTRAINT FK_FareEvasionInc_Contract   FOREIGN KEY (ContractID) REFERENCES SL_CONTRACT(ContractID),
    CONSTRAINT FK_FareEvasionInc_ClientID   FOREIGN KEY (ClientID) REFERENCES VARIO_CLIENT(ClientID),
    CONSTRAINT FK_FareEvasionInc_FareChange FOREIGN KEY (FareChangeCauseID) REFERENCES SL_FareChangeCause(FareChangeCauseID),
	CONSTRAINT FK_FareEvasionInc_Category   FOREIGN KEY (FareEvasionCategoryID) REFERENCES TM_FareEvasionCategory(FareEvasionCategoryID),
    CONSTRAINT UK_FareEvasionInc_Number UNIQUE (IncidentNumber)
);

CREATE TABLE SL_AddressBookEntry
(
    AddressBookEntryID NUMBER(18,0)                  NOT NULL,
    EntryType          NUMBER(9,0)   DEFAULT 0       NOT NULL, -- ENUM SL_AddressBookEntryType
    AddressID          NUMBER(18,0)                  NOT NULL,
    ClientID           NUMBER(18,0)                  NOT NULL,
    LastUser           NVARCHAR2(50) DEFAULT 'SYS'   NOT NULL,
    LastModified       DATE          DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER                       NOT NULL,

    CONSTRAINT PK_AddressBookEntry PRIMARY KEY (AddressBookEntryID),
    CONSTRAINT FK_AddressBookEntry_Client  FOREIGN KEY (ClientID) REFERENCES VARIO_CLIENT(ClientID),
    CONSTRAINT FK_AddressBookEntry_Address FOREIGN KEY (AddressID) REFERENCES SL_ADDRESS(AddressID)
);

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(7) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_FareEvasionInspection', 'SL_IdentificationType', 'SL_TransportCompany', 'SL_FareEvasionBehaviour', 'SL_FareChangeCause', 'SL_FareEvasionIncident', 'SL_AddressBookEntry');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(7) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_FareEvasionInspection', 'SL_IdentificationType', 'SL_TransportCompany', 'SL_FareEvasionBehaviour', 'SL_FareChangeCause', 'SL_FareEvasionIncident', 'SL_AddressBookEntry');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_FareEvasionState (EnumerationValue, Literal, Description) VALUES (0, 'Open', 'Offen');
INSERT INTO SL_FareEvasionState (EnumerationValue, Literal, Description) VALUES (1, 'InWork', 'In Bearbeitung');
INSERT INTO SL_FareEvasionState (EnumerationValue, Literal, Description) VALUES (2, 'CompletedPayed', 'Erledigt bezahlt');
INSERT INTO SL_FareEvasionState (EnumerationValue, Literal, Description) VALUES (3, 'CompletedNonPayed', 'Erledigt nicht bezahlt');
INSERT INTO SL_FareEvasionState (EnumerationValue, Literal, Description) VALUES (4, 'InfoLetter', 'Infobrief');
INSERT INTO SL_FareEvasionState (EnumerationValue, Literal, Description) VALUES (5, 'FirstDunningMinor', 'Erstes Mahnverfahren Minderjährige');
INSERT INTO SL_FareEvasionState (EnumerationValue, Literal, Description) VALUES (6, 'SecondDunningMinor', 'Zweites Mahnverfahren Minderjährige');
INSERT INTO SL_FareEvasionState (EnumerationValue, Literal, Description) VALUES (7, 'DunningFullAge', 'Mahnverfahren Erwachsene');
INSERT INTO SL_FareEvasionState (EnumerationValue, Literal, Description) VALUES (8, 'CompletedEncashment', 'Erledigt Inkasso');
INSERT INTO SL_FareEvasionState (EnumerationValue, Literal, Description) VALUES (9, 'Error', 'Fehler');
INSERT INTO SL_FareEvasionState (EnumerationValue, Literal, Description) VALUES (10, 'Inactive', 'Ruhend');

INSERT INTO SL_AddressBookEntryType (EnumerationValue, Literal, Description) VALUES (0, 'Unknown', 'Unbekannt');
INSERT INTO SL_AddressBookEntryType (EnumerationValue, Literal, Description) VALUES (1, 'RegistrationOffice', 'Einwohnermeldeamt');
INSERT INTO SL_AddressBookEntryType (EnumerationValue, Literal, Description) VALUES (2, 'PoliceOffice', 'Polizeidienststelle');

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

--Rollback;
COMMIT;

PROMPT Done!
SPOOL off
