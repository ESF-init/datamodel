SPOOL dbsl_2_163.log

INSERT INTO VARIO_DMODELLVERSION(RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION)
VALUES (sysdate, '2.163', 'MIW', 'Relations for Contract and Device Class to Payment Methods.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

CREATE TABLE SL_CHANNELTOPAYMENTMETHOD
(
  DEVICECLASSID			NUMBER(18) NOT NULL,
  DEVICEPAYMENTMETHODID	NUMBER(18) NOT NULL,
  
  CONSTRAINT PK_DEVICECLASSPAYMENTMETHOD PRIMARY KEY (DEVICECLASSID, DEVICEPAYMENTMETHODID),
  CONSTRAINT FK_CHANTOPAYMETH_DEVICECLASS FOREIGN KEY (DEVICECLASSID) REFERENCES VARIO_DEVICECLASS(DEVICECLASSID),
  CONSTRAINT FK_CHANTOPAYMETH_DEVPAYMETHOD FOREIGN KEY (DEVICEPAYMENTMETHODID) REFERENCES RM_DEVICEPAYMENTMETHOD(DEVICEPAYMENTMETHODID)
);

CREATE TABLE SL_CONTRACTTOPAYMENTMETHOD
(
  CONTRACTID  			NUMBER(18) NOT NULL,
  DEVICEPAYMENTMETHODID	NUMBER(18) NOT NULL,
  
  CONSTRAINT PK_CONTRACTTOPAYMENTMETHOD PRIMARY KEY (CONTRACTID, DEVICEPAYMENTMETHODID),
  CONSTRAINT FK_CONTPAYMETHOD_CONTRACT FOREIGN KEY (CONTRACTID) REFERENCES SL_CONTRACT(CONTRACTID),
  CONSTRAINT FK_CONTPAYMETHOD_DEVPAYMETHOD FOREIGN KEY (DEVICEPAYMENTMETHODID) REFERENCES RM_DEVICEPAYMENTMETHOD(DEVICEPAYMENTMETHODID)
);

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
