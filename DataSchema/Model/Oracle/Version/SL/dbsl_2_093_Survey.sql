SPOOL dbsl_2_093.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.093', 'TSC', 'Customer Survey.');

-- =======[ Changes on existing tables ]===========================================================================

/*
DROP TRIGGER SL_CustomerQuestion_BRI;
DROP TRIGGER SL_CustomerQuestion_BRU;
DROP TRIGGER SL_CustomerAnswer_BRI;
DROP TRIGGER SL_CustomerAnswer_BRU;
DROP TRIGGER SL_CustomerReply_BRI;
DROP TRIGGER SL_CustomerReply_BRU;
DROP TRIGGER SL_CustomerSurvey_BRI;
DROP TRIGGER SL_CustomerSurvey_BRU;

DROP SEQUENCE SL_CustomerQuestion_SEQ;
DROP SEQUENCE SL_CustomerAnswer_SEQ;
DROP SEQUENCE SL_CustomerSurvey_SEQ;

DROP TABLE SL_CustomerQuestion CASCADE CONSTRAINTS PURGE;
DROP TABLE SL_CustomerAnswer CASCADE CONSTRAINTS PURGE;
DROP TABLE SL_CustomerReply CASCADE CONSTRAINTS PURGE;
DROP TABLE SL_CustomerSurvey CASCADE CONSTRAINTS PURGE;

DROP VIEW SL_CustomerDemographic CASCADE CONSTRAINTS;
*/

-- =======[ New SL Tables ]========================================================================================

CREATE TABLE SL_Survey (
	SurveyID NUMBER(18, 0) NOT NULL,
	ValidFrom DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL, 
	ValidTo DATE DEFAULT TO_DATE('31-12-9999 23:59:59', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_Survey PRIMARY KEY (SurveyID)
);

CREATE TABLE SL_SurveyDescription (
	SurveyID NUMBER(18, 0) NOT NULL,
	Language NVARCHAR2(25) NOT NULL,
	Name NVARCHAR2(50) NOT NULL, 
	Description NVARCHAR2(250),
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_SurveyDescription PRIMARY KEY (SurveyID, Language),
	CONSTRAINT FK_SurveyDescription_Survey FOREIGN KEY (SurveyID) REFERENCES SL_Survey(SurveyID)
);

CREATE TABLE SL_SurveyQuestion (
	SurveyQuestionID NUMBER(18, 0) NOT NULL,
	SurveyID NUMBER(18, 0) NOT NULL,
	Language NVARCHAR2(25) NOT NULL,
	Text NVARCHAR2(2000) NOT NULL,
	HasMultipleAnswers NUMBER(1, 0) DEFAULT 0 NOT NULL,
	Rank NUMBER(9, 0) DEFAULT 999999999 NOT NULL,
	IsRetired NUMBER(1, 0) DEFAULT 0 NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,	
	CONSTRAINT PK_SurveyQuestion PRIMARY KEY (SurveyQuestionID),
	CONSTRAINT FK_SurveyQuestion_Survey FOREIGN KEY (SurveyID) REFERENCES SL_Survey(SurveyID)
);

CREATE TABLE SL_SurveyChoice (
	SurveyChoiceID NUMBER(18, 0) NOT NULL,
	SurveyQuestionID NUMBER(18, 0) NOT NULL,
	Language NVARCHAR2(25) NOT NULL,
	Text NVARCHAR2(2000) NOT NULL,
	HasFreeText NUMBER(1, 0) DEFAULT 0 NOT NULL,
	FollowUpQuestionID NUMBER(18, 0),
	IsRetired NUMBER(1, 0) DEFAULT 0 NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_SurveyChoice PRIMARY KEY (SurveyChoiceID),
	CONSTRAINT FK_SurveyChoice_SurveyQuestion FOREIGN KEY (SurveyQuestionID) REFERENCES SL_SurveyQuestion(SurveyQuestionID),
	CONSTRAINT FK_SurveyChoic_FollowUpQuestio FOREIGN KEY (FollowUpQuestionID) REFERENCES SL_SurveyQuestion(SurveyQuestionID)
);

CREATE TABLE SL_SurveyResponse (
	SurveyResponseID NUMBER(18, 0) NOT NULL,
	SurveyID NUMBER(18, 0) NOT NULL,
	PersonID NUMBER(18, 0) NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_SurveyResponse PRIMARY KEY (SurveyResponseID),
	CONSTRAINT FK_SurveyResponse_Survey FOREIGN KEY (SurveyID) REFERENCES SL_Survey(SurveyID),
	CONSTRAINT FK_SurveyResponse_Person FOREIGN KEY (PersonID) REFERENCES SL_Person(PersonID)
);

CREATE TABLE SL_SurveyAnswer (
	SurveyResponseID NUMBER(18, 0) NOT NULL,
	SurveyQuestionID NUMBER(18, 0) NOT NULL,
	SurveyChoiceID NUMBER(18, 0) NOT NULL,
	FreeTextAnswer NVARCHAR2(250),
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_SurveyAnswer PRIMARY KEY (SurveyResponseID, SurveyQuestionID, SurveyChoiceID),
	CONSTRAINT FK_SurveyAnswer_SurveyResponse FOREIGN KEY (SurveyResponseID) REFERENCES SL_SurveyResponse(SurveyResponseID),
	CONSTRAINT FK_SurveyAnswer_SurveyQuestion FOREIGN KEY (SurveyQuestionID) REFERENCES SL_SurveyQuestion(SurveyQuestionID),
	CONSTRAINT FK_SurveyAnswer_SurveyChoice FOREIGN KEY (SurveyChoiceID) REFERENCES SL_SurveyChoice(SurveyChoiceID)
);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW SL_SurveyReply (PersonID, PersonFirstName, PersonLastName, SurveyID, SurveyName, SurveyDescription, QuestionID, QuestionLanguage, QuestionText, ChoiceID, ChoiceText, ChoiceFollowUpQuestionID, ChoiceFreeText)
AS
	SELECT SL_Person.PersonID,
		SL_Person.FirstName,
		SL_Person.LastName,
		SL_Survey.SurveyID,
		SL_SurveyDescription.Name,
		SL_SurveyDescription.Description,
		SL_SurveyQuestion.SurveyQuestionID,
		SL_SurveyQuestion.Language,
		SL_SurveyQuestion.Text,
		SL_SurveyChoice.SurveyChoiceID,
		SL_SurveyChoice.Text,
		SL_SurveyChoice.FollowUpQuestionID,
		SL_SurveyAnswer.FreeTextAnswer
	FROM (((((SL_Person INNER JOIN SL_SurveyResponse ON SL_Person.PersonID = SL_SurveyResponse.PersonID)
		INNER JOIN SL_Survey ON SL_Survey.SurveyID = SL_SurveyResponse.SurveyID)
			INNER JOIN SL_SurveyAnswer ON SL_SurveyAnswer.SurveyResponseID = SL_SurveyResponse.SurveyResponseID)
				INNER JOIN SL_SurveyQuestion ON SL_SurveyQuestion.SurveyQuestionID = SL_SurveyAnswer.SurveyQuestionID)
					INNER JOIN SL_SurveyChoice ON SL_SurveyChoice.SurveyChoiceID = SL_SurveyAnswer.SurveyChoiceID AND SL_SurveyChoice.Language = SL_SurveyQuestion.Language)
						INNER JOIN SL_SurveyDescription ON SL_SurveyDescription.SurveyID = SL_Survey.SurveyID AND SL_SurveyDescription.Language = SL_SurveyQuestion.Language
	ORDER BY SL_Person.PersonID,
		SL_SurveyResponse.SurveyResponseID,
		SL_SurveyResponse.SurveyID,
		SL_SurveyQuestion.Rank,
		SL_SurveyQuestion.SurveyQuestionID;

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(4) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_Survey', 'SL_SurveyQuestion', 'SL_SurveyChoice', 'SL_SurveyResponse');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(6) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_Survey', 'SL_SurveyQuestion', 'SL_SurveyChoice', 'SL_SurveyResponse', 'SL_SurveyDescription', 'SL_SurveyAnswer');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
