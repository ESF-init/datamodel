SPOOL dbsl_2_068.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.068', 'FLF', 'New procedure to perform lock on db level');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================



 --WILL ONLY RUN WITH SUFFCIENT PRIVILEGES --
CREATE OR REPLACE PROCEDURE RequestLock (
   lockhandle              OUT      VARCHAR2,
   retcode                 OUT      NUMBER,
   lockname                IN       VARCHAR2,
   timeoutseconds          IN       NUMBER,
   locknameexpiryseconds   IN       NUMBER
)
IS
BEGIN
   DBMS_LOCK.allocate_unique (lockname, lockhandle, locknameexpiryseconds);
   retcode :=
             DBMS_LOCK.request (lockhandle, DBMS_LOCK.x_mode, timeoutseconds, TRUE);
END;
/
  --WILL ONLY RUN WITH SUFFCIENT PRIVILEGES --


 

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off


COMMIT;

PROMPT Fertig!

SPOOL off