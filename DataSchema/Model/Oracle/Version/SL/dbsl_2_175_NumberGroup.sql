SPOOL dbsl_2_175.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.175', 'SLR', 'Added line no number group for VARIO_LINE');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

INSERT ALL
	INTO SL_NUMBERGROUPSCOPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (7, 'BarCode', 'BarCode Identifier')
	INTO SL_NUMBERGROUPSCOPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (8, 'OrganizationIdentifier', 'Organization Identifier')
	INTO SL_NUMBERGROUPSCOPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (9, 'LineNo', 'Unique Line No for VARIO_LINE')
SELECT * FROM DUAL;

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

--Rollback;
COMMIT;

PROMPT Done!
SPOOL off
