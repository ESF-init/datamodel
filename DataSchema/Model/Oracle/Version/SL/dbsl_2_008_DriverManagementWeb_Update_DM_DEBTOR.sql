SPOOL dbsl_2_008.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.008', 'TSC', 'Update and default values for DM_DEBTOR for TURKU Driver Management Web.');

-- =======[ Changes on existing tables ]===========================================================================

/* Moved to vario script db_1_445_vario_changes_from_variosl_script. */

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW sl_debtoraccountbalance 
(
	accountid, 
	accountnumber, 
	clientid, 
	firstname, 
	lastname, 
	accountbalance, 
	period
) 
AS 
	SELECT
		dm_debtor.debtorid as accountid,
		dm_debtor.debtorno AS accountnumber,
		dm_debtor.clientid,
		dm_debtor.firstname,
		dm_debtor.name,
		CAST (pp_accountbalance.balance AS NUMBER (9, 0)) as accountbalance, 
		pp_accountbalance.month AS period
	FROM dm_debtor, pp_accountbalance
	WHERE dm_debtor.debtorid = pp_accountbalance.contractid
	AND pp_accountbalance.BALANCEID in 
	(
		select pp_accountbalance.BALANCEID
		from pp_accountbalance, 
		(
			select pp_accountbalance.contractid, MAX(pp_accountbalance.MONTH) as month
			from pp_accountbalance
			group by pp_accountbalance.contractid
		) maxMonth
		where pp_accountbalance.contractid = maxMonth.contractid
		and pp_accountbalance.MONTH = maxMonth.month
	)
	ORDER BY dm_debtor.debtorid, pp_accountbalance.MONTH DESC;

CREATE OR REPLACE FORCE VIEW sl_debtorpostings 
(
	accountid, 
	accountnumber, 
	clientid, 
	postingtime, 
	valuedate, 
	amount, 
	postingtext,
	receiptnumber, 
	postingcodenumer, 
	postingcodetext
) 
AS 
	SELECT  
		dm_debtor.debtorid AS accountid,
		dm_debtor.debtorno AS accountnumber,
		dm_debtor.clientid,
		pp_accountentry.bookdatetime AS postingtime, 
		pp_accountentry.valuedatetime AS valuedate, 
		CAST (nvl(pp_accountentry.amount, 0) AS NUMBER (9, 0)) AS amount,
		pp_accountentry.postingtext as postingtext, 
		lpad(to_char(pp_accountentryrange.entryrangeno), 1, '0') || '-' || lpad(to_char(pp_accountentry.entryno), 10, '0') AS receiptnumber, 
		pp_accountpostingkey.entrytypeno AS postingcodenumer,
		pp_accountpostingkey.entrytypename AS postingcodetext
	FROM pp_accountentry, dm_debtor, pp_accountpostingkey, pp_accountentryrange
	WHERE  dm_debtor.debtorid = pp_accountentry.contractid 
	AND pp_accountentry.entrytypeno = pp_accountpostingkey.entrytypeno 
	AND pp_accountpostingkey.entryrangeid = pp_accountentryrange.entryrangeid 
	ORDER BY dm_debtor.debtorno, pp_accountentry.valuedatetime, pp_accountentry.bookdatetime DESC;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

INSERT INTO sl_configuration(configurationid, NAME, SCOPE, configurationvalue, description)
VALUES (sl_configuration_seq.nextval, 'DebtorNumberRangeMultiplier', 'DriverManagement', '10000', '');

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off