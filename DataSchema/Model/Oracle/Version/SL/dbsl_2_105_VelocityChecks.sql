SPOOL dbsl_2_105.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
    VALUES (sysdate, '2.105', 'SLR', 'Added tables for velocity checks');

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_RULEVIOLATIONTYPE
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_RULEVIOLATIONTYPE PRIMARY KEY (ENUMERATIONVALUE)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE SL_RULEVIOLATION
(
  RULEVIOLATIONID       NUMBER(18)      NOT NULL,
  TRANSACTIONJOURNALID  NUMBER(18)      NOT NULL,
  RULEVIOLATIONTYPE     NUMBER(9)       NOT NULL,
  VIOLATIONVALUE        NVARCHAR2(100),
  MESSAGE               NVARCHAR2(2000),
  EXECUTIONTIME         DATE            NOT NULL,
  
  LASTUSER              NVARCHAR2(50)   DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE            DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER         NOT NULL,
  
   CONSTRAINT PK_RULEVIOLATION PRIMARY KEY (RULEVIOLATIONID)
);

ALTER TABLE SL_RULEVIOLATION 
ADD (
   CONSTRAINT FK_RULEVIOLATION_TRANSJOUR FOREIGN KEY (TRANSACTIONJOURNALID) REFERENCES SL_TRANSACTIONJOURNAL(TRANSACTIONJOURNALID)
);

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

CREATE SEQUENCE SL_RULEVIOLATION_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================

CREATE OR REPLACE TRIGGER SL_RULEVIOLATION_BRU before update ON SL_RULEVIOLATION for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_RULEVIOLATION_BRI before insert ON SL_RULEVIOLATION for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
    INTO SL_RULEVIOLATIONTYPE (EnumerationValue, Literal, Description) VALUES (0, 'STORED_VALUE_USAGE_VELOCITY', 'STORED_VALUE_USAGE_VELOCITY')
    INTO SL_RULEVIOLATIONTYPE (EnumerationValue, Literal, Description) VALUES (1, 'STORED_VALUE_LOAD_VELOCITY', 'STORED_VALUE_LOAD_VELOCITY')
    INTO SL_RULEVIOLATIONTYPE (EnumerationValue, Literal, Description) VALUES (2, 'TRANSACTION_VELOCITY', 'TRANSACTION_VELOCITY')
    INTO SL_RULEVIOLATIONTYPE (EnumerationValue, Literal, Description) VALUES (3, 'GEOGRAPHICAL_VELOCITY', 'GEOGRAPHICAL_VELOCITY')
SELECT * FROM DUAL; 

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
