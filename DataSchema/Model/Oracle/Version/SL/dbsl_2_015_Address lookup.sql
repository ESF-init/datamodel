SPOOL dbsl_2_015.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.015', 'MIW', 'Added address lookup table');

-- -------[ Tables ]--------------------------------------------------------------------------------------------

CREATE TABLE SL_LookupAddress (
    LookupAddressID NUMBER(18, 0) NOT NULL, 
    Street NVARCHAR2(125), 
    StreetNumber NVARCHAR2(25), 
    AddressField1 NVARCHAR2(125), 
    AddressField2 NVARCHAR2(125), 
    City NVARCHAR2(125), 
    PostalCode NVARCHAR2(25), 
    Region NVARCHAR2(125), 
    Country NVARCHAR2(125), 
    LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
    LastModified DATE DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER NOT NULL,
    CONSTRAINT PK_LookupAddress PRIMARY KEY (LookupAddressID)
);

-- -------[ Triggers ]--------------------------------------------------------------------------------------------

CREATE OR REPLACE TRIGGER SL_LookupAddress_BRU before update ON SL_LookupAddress for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/


CREATE OR REPLACE TRIGGER SL_LookupAddress_BRI before insert ON SL_LookupAddress for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/

-- -------[ Sequences ]--------------------------------------------------------------------------------------------

CREATE SEQUENCE SL_LookupAddress_SEQ
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 200
  NOORDER;


COMMIT;

PROMPT Done!
SPOOL off