SPOOL dbsl_2_088.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.088', 'BAW', 'Added new order functions, refund, card pool, payment options.');

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_ORGANIZATION ADD (HasProductPool NUMBER(1) DEFAULT 0 NOT NULL);
ALTER TABLE SL_ORGANIZATION ADD (Discount NUMBER(9));

ALTER TABLE SL_CARD ADD (IsProductPool NUMBER(1) DEFAULT 0 NOT NULL);

ALTER TABLE SL_SALE ADD (ExternalOrderNumber NVARCHAR2(50));

ALTER TABLE SL_SALE ADD(ContractID Number(18));
ALTER TABLE SL_SALE ADD CONSTRAINT FK_SALE_CONTRACT FOREIGN KEY (ContractID) REFERENCES SL_CONTRACT (ContractID);

ALTER TABLE SL_SALE ADD(RefundReferenceID Number(18));
ALTER TABLE SL_SALE ADD CONSTRAINT FK_SALE_REFUND FOREIGN KEY (RefundReferenceID) REFERENCES SL_SALE (SALEID);

ALTER TABLE SL_PAYMENTJOURNAL ADD (PAYMENTOPTIONID  NUMBER(18));
ALTER TABLE SL_PAYMENTJOURNAL ADD CONSTRAINT FK_PayJournal_PayOption FOREIGN KEY (PAYMENTOPTIONID) REFERENCES SL_PAYMENTOPTION (PAYMENTOPTIONID);

ALTER TABLE SL_PAYMENTJOURNAL ADD (MaskedPAN  NVARCHAR2(50));

ALTER TABLE SL_PAYMENTJOURNAL ADD (ProviderReference  NVARCHAR2(50));

UPDATE SL_PERSON SET PERSONALASSISTENT=0, TRANSACTIONCOUNTER=TRANSACTIONCOUNTER+1 WHERE PERSONALASSISTENT IS NULL;
ALTER TABLE SL_PERSON MODIFY (PERSONALASSISTENT NUMBER(1) DEFAULT 0 NOT NULL);

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
