SPOOL dbsl_2_162.log

-- IMPORTANT: MOBILEvario DDI needs to be stopped before this script can be run.

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.162', 'DST', 'Added column ResourceID to VAS_Report and VAS_ReportCategory.');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE VAS_Report ADD ResourceID NVARCHAR2(512);
ALTER TABLE VAS_Report
 ADD CONSTRAINT FK_Report_Resource FOREIGN KEY (ResourceID) REFERENCES User_Resource(ResourceID);

ALTER TABLE VAS_ReportCategory ADD ResourceID NVARCHAR2(512);
ALTER TABLE VAS_ReportCategory
 ADD CONSTRAINT FK_ReportCategory_Resource FOREIGN KEY (ResourceID) REFERENCES User_Resource(ResourceID);

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
