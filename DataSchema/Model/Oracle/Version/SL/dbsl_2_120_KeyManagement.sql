SPOOL dbsl_2_120.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.120', 'AMA', 'Added Key Management Stuff');

-- =======[ New Tables ]========================================================================================

CREATE TABLE SL_DEVICEREADER
(
    DEVICEREADERID  		NUMBER(18)  					NOT NULL,		
    NAME            		NVARCHAR2(125),
    DEVICEROLE				NUMBER(9)						DEFAULT 0                     	NOT NULL,
	DEVICESTATUS			NUMBER(9)						DEFAULT 0                     	NOT NULL,
	DEVICEREADERNUMBER		NUMBER(9)              			DEFAULT 0                     	NOT NULL,
	DEVICENO				NUMBER(10),
	KEK						NVARCHAR2(2000)                 NOT NULL,
	DESCRIPTION				NVARCHAR2(512),
	LASTCONNECTIONDATE		DATE                      		DEFAULT sysdate               	NOT NULL,
	LASTUSER				NVARCHAR2(50)             		DEFAULT 'SYS'                 	NOT NULL,
	LASTMODIFIED			DATE                      		DEFAULT sysdate               	NOT NULL,
	TRANSACTIONCOUNTER		NUMBER                   		NOT NULL,
	CONSTRAINT PK_DEVICEREADER PRIMARY KEY (DEVICEREADERID)
);

CREATE INDEX IDX_DEVICEREADER_ROLE_STATUS on SL_DEVICEREADER (DEVICEROLE, DEVICESTATUS) TABLESPACE MOBILE_IDX;
CREATE INDEX IDX_DEVICEREADER_No on SL_DEVICEREADER (DEVICEREADERNUMBER) TABLESPACE MOBILE_IDX;

CREATE TABLE SL_CERTIFICATE
(
    CERTIFICATEID  			NUMBER(18)  				NOT NULL,		
    CERTIFICATESIGNATURE    NVARCHAR2(400)				NOT NULL,
    CERTIFICATE			    NVARCHAR2(2000)				NOT NULL,
	VERSION    				NUMBER(9)					DEFAULT 0                 		NOT NULL,
    CERTIFICATETYPE    		NUMBER(9)					DEFAULT 0                 		NOT NULL,
	ISSUERNAME				NVARCHAR2(255)				NOT NULL,
	SUBJECTNAME				NVARCHAR2(255)				NOT NULL,
	VALIDFROM				DATE              			DEFAULT sysdate                 NOT NULL,
	VALIDTO					DATE                		DEFAULT sysdate                 NOT NULL,
	DESCRIPTION				NVARCHAR2(512),
	CERTIFICATESTATUS		NUMBER(9)              		DEFAULT 0                     	NOT NULL,
	LASTUSER				NVARCHAR2(50)            	DEFAULT 'SYS'                 	NOT NULL,
	LASTMODIFIED			DATE                     	DEFAULT sysdate               	NOT NULL,
	TRANSACTIONCOUNTER		NUMBER                  	NOT NULL,
	CONSTRAINT PK_CERTIFICATE PRIMARY KEY (CERTIFICATEID)
);

CREATE TABLE SL_DEVICEREADERKEY
(
    DEVICEREADERKEYID  			NUMBER(18)  				NOT NULL,		
    DEVICEREADERID  			NUMBER(18)  				NOT NULL,		
    VERSION    					NUMBER(9)					DEFAULT 0                 		NOT NULL,
	VALIDFROM					DATE        	      		DEFAULT sysdate                 NOT NULL,
    KEYTYPE						NUMBER(9)					DEFAULT 0                 		NOT NULL,
    KEYINDEX					NUMBER(9)					DEFAULT 0                 		NOT NULL,
    TB31KEYBLOCK				NVARCHAR2(2000),
	DATASIGNATUREKEY			NVARCHAR2(2000),
	KEYSTATUS					NUMBER(9)              		DEFAULT 0						NOT NULL,
	CREATIONDATE				DATE        	      		DEFAULT sysdate                 NOT NULL,
	INSTALLATIONDATE			DATE        	      		DEFAULT sysdate                 NOT NULL,
	DESCRIPTION					NVARCHAR2(512),
	LASTUSER					NVARCHAR2(50)            	DEFAULT 'SYS'                 	NOT NULL,
	LASTMODIFIED				DATE                     	DEFAULT sysdate               	NOT NULL,
	TRANSACTIONCOUNTER			NUMBER                  	NOT NULL,
	CONSTRAINT PK_DEVICEREADERKEY PRIMARY KEY (DEVICEREADERKEYID),
	
	CONSTRAINT FK_DEVICEKEY_DEVICEREADER
    FOREIGN KEY (DEVICEREADERID)
    REFERENCES SL_DEVICEREADER (DEVICEREADERID)
);

CREATE INDEX IDX_DEVICEREADERKEY_READER on SL_DEVICEREADERKEY (DEVICEREADERID) TABLESPACE MOBILE_IDX;
CREATE INDEX IDX_DEVICEREADERKEY_INDVER on SL_DEVICEREADERKEY (KEYINDEX, VERSION, VALIDFROM) TABLESPACE MOBILE_IDX;


CREATE TABLE SL_DEVICEREADERJOB
(
    DEVICEREADERJOBID  			NUMBER(18)  				NOT NULL,		
    WORKERREADERID  			NUMBER(18),		
    JOBSTATUS    				NUMBER(9)					DEFAULT 0                 		NOT NULL,
    JOBPRIORITY					NUMBER(9)					DEFAULT 0                 		NOT NULL,
    JOBTYPE						NUMBER(9)					DEFAULT 0                 		NOT NULL,
	VALIDFROM					DATE        	      		DEFAULT sysdate                 NOT NULL,
	TIMEOUT						DATE        	      		DEFAULT sysdate                 NOT NULL,
    DEVICEROLE		 			NUMBER(9)					DEFAULT 0                     	NOT NULL,
	TARGETREADERID				NUMBER(18)					DEFAULT 0                     	NOT NULL,
	JOBPARAMETER				NVARCHAR2(512),
	LASTUSER					NVARCHAR2(50)            	DEFAULT 'SYS'                 	NOT NULL,
	LASTMODIFIED				DATE                     	DEFAULT sysdate               	NOT NULL,
	TRANSACTIONCOUNTER			NUMBER                  	NOT NULL,
	CONSTRAINT PK_DEVICEREADERJOB PRIMARY KEY (DEVICEREADERJOBID),
	
	CONSTRAINT FK_DEVRDERJOB_WORKER
    FOREIGN KEY (WORKERREADERID)
    REFERENCES SL_DEVICEREADER (DEVICEREADERID),

	CONSTRAINT FK_DEVRDERJOB_TARGETD
    FOREIGN KEY (TARGETREADERID)
    REFERENCES SL_DEVICEREADER (DEVICEREADERID)
);

CREATE INDEX IDX_DEVICEREADERJOB_DATE on SL_DEVICEREADERJOB (VALIDFROM, TIMEOUT,JOBSTATUS) TABLESPACE MOBILE_IDX;
CREATE INDEX IDX_DEVICEREADERJOB_WORKER on SL_DEVICEREADERJOB (WORKERREADERID) TABLESPACE MOBILE_IDX;
CREATE INDEX IDX_DEVICEREADERJOB_TARGET on SL_DEVICEREADERJOB (TARGETREADERID) TABLESPACE MOBILE_IDX;

CREATE TABLE SL_DEVICECOMMUNICATIONHISTORY
(
    DEVICECOMMUNICATIONHISTORYID  			NVARCHAR2(40)  				NOT NULL,		
    DEVICEREADERID  						NUMBER(18)  				NOT NULL,		
    REQUESTTYPE    							NUMBER(9)					DEFAULT 0                 		NOT NULL,
	REQUESTDATE								DATE        	      		DEFAULT sysdate                 NOT NULL,
    REQUESTDESCRIPTION						NVARCHAR2(2000),
    RESPONSETYPE    						NUMBER(9)					DEFAULT 0                 		NOT NULL,
    RESPONSEDESCRIPTION						NVARCHAR2(2000),
	DESCRIPTION								NVARCHAR2(2000),
	LASTUSER								NVARCHAR2(50)            	DEFAULT 'SYS'                 	NOT NULL,
	LASTMODIFIED							DATE                     	DEFAULT sysdate               	NOT NULL,
	TRANSACTIONCOUNTER						NUMBER                  	NOT NULL,
	CONSTRAINT PK_DEVICECOMMUNICATIONHISTORY PRIMARY KEY (DEVICECOMMUNICATIONHISTORYID),
	
	CONSTRAINT FK_DEVICECOMMHIST_DEVICERDER
    FOREIGN KEY (DEVICEREADERID)
    REFERENCES SL_DEVICEREADER (DEVICEREADERID)
);
CREATE INDEX IDX_DEVICECOMMHIST_REQDATE on SL_DEVICECOMMUNICATIONHISTORY (REQUESTDATE) TABLESPACE MOBILE_IDX;
CREATE INDEX IDX_DEVICECOMMHIST_REQTYPE on SL_DEVICECOMMUNICATIONHISTORY (REQUESTTYPE) TABLESPACE MOBILE_IDX;
CREATE INDEX IDX_DEVICECOMMHIST_DEVDATE on SL_DEVICECOMMUNICATIONHISTORY (DEVICEREADERID,REQUESTDATE) TABLESPACE MOBILE_IDX;

CREATE TABLE SL_DEVICEREADERKEYTOCERT
(
    DEVICEREADERKEYTOCERTIFICATEID  			NUMBER(18)  				NOT NULL,		
    DEVICEREADERKEYID  							NUMBER(18)  				NOT NULL,		
    CERTIFICATEID    							NUMBER(18)					NOT NULL,
	LABEL										NVARCHAR2(255)				DEFAULT ''                     	NOT NULL,
	LASTUSER									NVARCHAR2(50)            	DEFAULT 'SYS'                 	NOT NULL,
	LASTMODIFIED								DATE                     	DEFAULT sysdate               	NOT NULL,
	TRANSACTIONCOUNTER							NUMBER                  	NOT NULL,
	CONSTRAINT PK_DEVICEREADERKEYTOCERT PRIMARY KEY (DEVICEREADERKEYTOCERTIFICATEID),
	
	CONSTRAINT FK_DEVRDERKEYTOCERT_DEV
    FOREIGN KEY (DEVICEREADERKEYID)
    REFERENCES SL_DEVICEREADERKEY (DEVICEREADERKEYID),

	CONSTRAINT FK_DEVRDERKEYTOCERT_CERT
    FOREIGN KEY (CERTIFICATEID)
    REFERENCES SL_CERTIFICATE (CERTIFICATEID)
);

CREATE TABLE SL_DEVICEREADERTOCERT
(
    DEVICEREADERTOCERTIFICATEID  				NUMBER(18)  				NOT NULL,		
    DEVICEREADERID  							NUMBER(18)  				NOT NULL,		
    CERTIFICATEID    							NUMBER(18)					NOT NULL,
	CERTIFICATESTATUS							NUMBER(9)              		DEFAULT 0						NOT NULL,
	LASTUSER									NVARCHAR2(50)            	DEFAULT 'SYS'                 	NOT NULL,
	LASTMODIFIED								DATE                     	DEFAULT sysdate               	NOT NULL,
	TRANSACTIONCOUNTER							NUMBER                  	NOT NULL,
	CONSTRAINT PK_DEVICEREADERTOCERT PRIMARY KEY (DEVICEREADERTOCERTIFICATEID),
	
	CONSTRAINT FK_DEVICEREADERTOCERT_DEV
    FOREIGN KEY (DEVICEREADERID)
    REFERENCES SL_DEVICEREADER (DEVICEREADERID),

	CONSTRAINT FK_DEVICEREADERTOCERT_CERT
    FOREIGN KEY (CERTIFICATEID)
    REFERENCES SL_CERTIFICATE (CERTIFICATEID)
);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

CREATE SEQUENCE SL_DEVICEREADER_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;

CREATE SEQUENCE SL_CERTIFICATE_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;

CREATE SEQUENCE SL_DEVICEREADERKEY_SEQ
	   INCREMENT BY 1
	   NOMINVALUE
	   NOMAXVALUE
	   CACHE 20
	   NOCYCLE
	   NOORDER;
	   
CREATE SEQUENCE SL_DEVICEREADERJOB_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;
	   
CREATE SEQUENCE SL_DEVICECOMMHISTORY_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;
	   
CREATE SEQUENCE SL_DEVICEREADERKEYTOCERT_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;

CREATE SEQUENCE SL_DEVICEREADERTOCERT_SEQ
		INCREMENT BY 1
		NOMINVALUE
		NOMAXVALUE
		CACHE 20
		NOCYCLE
		NOORDER;
		
-- =======[ Trigger ]==============================================================================================
CREATE OR REPLACE TRIGGER SL_DEVICEREADER_BRI before insert on SL_DEVICEREADER for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_DEVICEREADER_BRU before update on SL_DEVICEREADER for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_CERTIFICATE_BRI before insert on SL_CERTIFICATE for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_CERTIFICATE_BRU before update on SL_CERTIFICATE for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_DEVICEREADERKEY_BRI before insert on SL_DEVICEREADERKEY for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_DEVICEREADERKEY_BRU before update on SL_DEVICEREADERKEY for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_DEVICEREADERJOB_BRI before insert on SL_DEVICEREADERJOB for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_DEVICEREADERJOB_BRU before update on SL_DEVICEREADERJOB for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_DEVICECOMMHISTORY_BRI before insert on SL_DEVICECOMMUNICATIONHISTORY for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_DEVICECOMMHISTORY_BRU before update on SL_DEVICECOMMUNICATIONHISTORY for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_DEVICEREADERKEYTOCERT_BRI before insert on SL_DEVICEREADERKEYTOCERT for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_DEVICEREADERKEYTOCERT_BRU before update on SL_DEVICEREADERKEYTOCERT for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_DEVICEREADERTOCERT_BRI before insert on SL_DEVICEREADERTOCERT for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_DEVICEREADERTOCERT_BRU before update on SL_DEVICEREADERTOCERT for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off