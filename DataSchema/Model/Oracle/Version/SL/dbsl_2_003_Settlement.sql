SPOOL dbsl_2_003.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
  VALUES (sysdate, '2.003', 'FMT', 'Settlement');

CREATE TABLE Sl_SettlementSetup (
  SettlementSetupID NUMBER(18, 0) NOT NULL,
  NAME NVARCHAR2(100),
  FormulaString NVARCHAR2(1000),
  Schedulerfc5545 NVARCHAR2(200),
  ValidFrom DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  ValidUntil DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  ReleaseState NUMBER(9,0) NOT NULL,
  LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL,
  LastModified DATE DEFAULT SYSDATE NOT NULL,
  TransactionCounter INTEGER NOT NULL,
  CONSTRAINT PK_SettlementSetup PRIMARY KEY (SettlementSetupID)
);

CREATE TABLE Sl_SettlementReleaseState (
  EnumerationValue NUMBER(9, 0) NOT NULL, 
  Literal NVARCHAR2(50) NOT NULL, 
  Description NVARCHAR2(50) NOT NULL, 
   CONSTRAINT PK_SettlementReleaseState PRIMARY KEY (EnumerationValue)
);

INSERT ALL
	INTO SL_SettlementReleaseState(EnumerationValue, Literal, Description)VALUES (0, 'NotReleased', 'NotReleased')
	INTO SL_SettlementReleaseState(EnumerationValue, Literal, Description)VALUES (1, 'Released', 'Released')
	INTO SL_SettlementReleaseState(EnumerationValue, Literal, Description)VALUES (2, 'Revoked', 'Revoked')
SELECT * FROM DUAL;

CREATE TABLE SL_SettlementQuerySetting (
  SettlementQuerySettingID NUMBER(18, 0) NOT NULL,
  SettlementSetupID NUMBER(18, 0) NOT NULL,
  NAME NVARCHAR2(100),
  AgencyOwningVehicle NUMBER(18, 0),
  Operation  NUMBER(9, 0) NOT NULL,
  StaticValue NUMBER(9,0) NOT NULL,
  Lastuser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL,
  Lastmodified DATE DEFAULT SYSDATE NOT NULL,
  TransactionCounter INTEGER NOT NULL,
  CONSTRAINT PK_SettlementQuerySetting PRIMARY KEY (SettlementQuerySettingID),
  CONSTRAINT FK_QuerySetting_Setup FOREIGN KEY (SettlementSetupID ) REFERENCES SL_SettlementSetup(SettlementSetupID )
);

CREATE TABLE SL_SettlementQuerysetToTic(
    SettlementQuerySettingID NUMBER(18, 0) NOT NULL,
    TicketID NUMBER(18, 0) NOT NULL,
    CONSTRAINT PK_QuerySettingToTicket PRIMARY KEY (SettlementQuerySettingID, TicketID),
    CONSTRAINT FK_QuerySettingToTicket_Query FOREIGN KEY (SettlementQuerySettingID) REFERENCES SL_SettlementQuerySetting (SettlementQuerySettingID),
    CONSTRAINT FK_QuerySettingToTicket_Ticke FOREIGN KEY (TicketID) REFERENCES TM_Ticket(TicketID)
);

CREATE TABLE SL_SettlementOperation (
  EnumerationValue NUMBER(9, 0) NOT NULL, 
  Literal NVARCHAR2(50) NOT NULL, 
  Description NVARCHAR2(50) NOT NULL, 
  CONSTRAINT PK_SettlementOperation PRIMARY KEY (EnumerationValue)
);

INSERT ALL
  INTO SL_SettlementOperation(EnumerationValue, Literal, Description)VALUES (0, 'Static', 'Static')
  INTO SL_SettlementOperation(EnumerationValue, Literal, Description)VALUES (1, 'Transaction_Count', 'Transaction_Count')
  INTO SL_SettlementOperation(EnumerationValue, Literal, Description)VALUES (2, 'Transaction_Average_Price', 'Transaction_Average_Price')
  INTO SL_SettlementOperation(EnumerationValue, Literal, Description)VALUES (3, 'Transaction_Sum_Price', 'Transaction_Sum_Price')
  INTO SL_SettlementOperation(EnumerationValue, Literal, Description)VALUES (4, 'Product_Average_Price', 'Product_Average_Price')
  INTO SL_SettlementOperation(EnumerationValue, Literal, Description)VALUES (5, 'Days_In_Month', 'Days_In_Month')
  INTO SL_SettlementOperation(EnumerationValue, Literal, Description)VALUES (6, 'Business_Days_In_Month', 'Business_Days_In_Month')
SELECT * FROM DUAL;

CREATE TABLE SL_SettlementRun (
  SettlementRunID NUMBER(18, 0) NOT NULL,
  SettlementSetupID NUMBER(18, 0) NOT NULL,
  BeginDate DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  EndDate DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL,
  LastModified DATE DEFAULT SYSDATE NOT NULL,
  TransactionCounter INTEGER NOT NULL,
  CONSTRAINT PK_SettlementRun PRIMARY KEY (SettlementRunID),
  CONSTRAINT FK_SettlementRun_Setup FOREIGN KEY (SettlementSetupID ) REFERENCES SL_SettlementSetup (SettlementSetupID )
);

CREATE TABLE SL_SettlementRunValue (
  SettlementRunValueID NUMBER(18, 0) NOT NULL,
  SettlementRunID NUMBER(18, 0) NOT NULL,
  VALUE BINARY_DOUBLE NOT NULL,
  Calculated DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL,
  LastModified DATE DEFAULT SYSDATE NOT NULL,
  TransactionCounter INTEGER NOT NULL,
  CONSTRAINT PK_SettlementRunValue PRIMARY KEY (SettlementRunValueID ),
  CONSTRAINT FK_SettlementRunValue_Run FOREIGN KEY (SettlementRunID) REFERENCES SL_SettlementRun (SettlementRunID )
);

CREATE TABLE SL_SettlementQueryValue (
  SettlementQueryValueID NUMBER(18, 0) NOT NULL,
  SettlementQuerySettingID NUMBER(18, 0) NOT NULL,
  SettlementRunValueID NUMBER(18, 0) NOT NULL,
  VALUE NUMBER NOT NULL,
  Calculated DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL,
  LastModified DATE DEFAULT SYSDATE NOT NULL,
  TransactionCounter INTEGER NOT NULL,
  CONSTRAINT PK_SettlementQueryValue PRIMARY KEY (SettlementQueryValueID ),
  CONSTRAINT FK_SettlementQueryValue_Run FOREIGN KEY (SettlementRunValueID) REFERENCES SL_SettlementRunValue (SettlementRunValueID ),
  CONSTRAINT FK_SettlementQueryValue_Set FOREIGN KEY (SettlementQuerySettingID) REFERENCES SL_SettlementQuerySetting (SettlementQuerySettingID )
);

CREATE TABLE SL_SettlementRunValueToVario (
  SettlementRunValueID NUMBER(18, 0) NOT NULL,
  VarioSettlementID NUMBER(10, 0) NOT NULL,
  CONSTRAINT PK_VarioSettlementRunValue PRIMARY KEY (SettlementRunValueID, VarioSettlementID),
  CONSTRAINT FK_SettlementRunValue FOREIGN KEY (SettlementRunValueID) REFERENCES SL_SettlementRunValue(SettlementRunValueID),
  CONSTRAINT FK_VarioSettlement FOREIGN KEY (VarioSettlementID) REFERENCES VARIO_SETTLEMENT (ID )
);

DECLARE
   TYPE table_names_array IS VARRAY (5) OF VARCHAR2 (100);

   table_names   table_names_array
      := table_names_array ('SL_SettlementSetup',
                            'SL_SettlementQuerySetting',
                            'SL_SettlementRun',
                            'SL_SettlementQueryValue',
                            'SL_SettlementRunValue'
                           );
   sql_string    VARCHAR2 (500);
BEGIN
   FOR table_name IN table_names.FIRST .. table_names.LAST
   LOOP
      BEGIN
         sql_string :=
               'create sequence '
            || table_names (table_name)
            || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
         DBMS_OUTPUT.put_line (   'Creating sequence for: '
                               || table_names (table_name)
                              );

         EXECUTE IMMEDIATE sql_string;

         DBMS_OUTPUT.put_line ('Done!');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('Error creating sequence: ' || SQLERRM);
      END;
   END LOOP;
END;

DECLARE
   TYPE table_names_array IS VARRAY (5) OF VARCHAR2 (100);

   table_names   table_names_array
      := table_names_array ('SL_SettlementSetup',
                            'SL_SettlementQuerySetting',
                            'SL_SettlementRun',
                            'SL_SettlementQueryValue',
                            'SL_SettlementRunValue'
                           );
   sql_string    VARCHAR2 (500);
BEGIN
   FOR table_name IN table_names.FIRST .. table_names.LAST
   LOOP
      BEGIN
         sql_string := '';
         DBMS_OUTPUT.put_line (   'Creating insert trigger for: '
                               || table_names (table_name)
                              );
         sql_string :=
               'create or replace trigger '
            || table_names (table_name)
            || '_BRI'
            || ' before insert on '
            || table_names (table_name)
            || ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';

         EXECUTE IMMEDIATE sql_string;

         DBMS_OUTPUT.put_line (   'Creating update trigger for: '
                               || table_names (table_name)
                              );
         sql_string :=
               'create or replace trigger '
            || table_names (table_name)
            || '_BRU'
            || ' before update on '
            || table_names (table_name)
            || ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';

         EXECUTE IMMEDIATE sql_string;

         DBMS_OUTPUT.put_line ('Done!');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('Error creating trigger: ' || SQLERRM);
      END;
   END LOOP;
END;

COMMIT;

PROMPT Done!
SPOOL off