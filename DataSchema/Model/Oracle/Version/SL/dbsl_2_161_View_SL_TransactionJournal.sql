SPOOL dbsl_2_161.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.161', 'SLR', 'Added columns IsVoiceEnabled and IsTraining from SL_Card.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW view_sl_transactionjournal (transactionjournalid,
                                                               transactionid,
                                                               devicetime,
                                                               boardingtime,
                                                               operatorid,
                                                               clientid,
                                                               line,
                                                               coursenumber,
                                                               routenumber,
                                                               routecode,
                                                               direction,
                                                               ZONE,
                                                               saleschannelid,
                                                               devicenumber,
                                                               vehiclenumber,
                                                               mountingplatenumber,
                                                               debtornumber,
                                                               geolocationlongitude,
                                                               geolocationlatitude,
                                                               stopnumber,
                                                               transitaccountid,
                                                               faremediaid,
                                                               faremediatype,
                                                               productid,
                                                               ticketid,
                                                               fareamount,
                                                               customergroup,
                                                               transactiontype,
                                                               transactionnumber,
                                                               resulttype,
                                                               filllevel,
                                                               pursebalance,
                                                               pursecredit,
                                                               groupsize,
                                                               validfrom,
                                                               validto,
                                                               DURATION,
                                                               tariffdate,
                                                               tariffversion,
                                                               ticketinternalnumber,
                                                               whitelistversion,
                                                               cancellationreference,
                                                               cancellationreferenceguid,
                                                               lastuser,
                                                               lastmodified,
                                                               transactioncounter,
                                                               whitelistversioncreated,
                                                               properties,
                                                               saleid,
                                                               tripticketinternalnumber,
                                                               ticketname,
                                                               shiftbegin,
                                                               completionstate,
                                                               responsestate,
                                                               productid2,
                                                               pursebalance2,
                                                               pursecredit2,
                                                               creditcardauthorizationid,
                                                               tripcounter,
                                                               inserttime,
                                                               isvoiceenabled,
                                                               istraining
                                                              )
AS
   SELECT sl_transactionjournal.transactionjournalid,
          sl_transactionjournal.transactionid,
          sl_transactionjournal.devicetime,
          sl_transactionjournal.boardingtime,
          sl_transactionjournal.operatorid, sl_transactionjournal.clientid,
          sl_transactionjournal.line, sl_transactionjournal.coursenumber,
          sl_transactionjournal.routenumber, sl_transactionjournal.routecode,
          sl_transactionjournal.direction, sl_transactionjournal.ZONE,
          sl_transactionjournal.saleschannelid,
          sl_transactionjournal.devicenumber,
          sl_transactionjournal.vehiclenumber,
          sl_transactionjournal.mountingplatenumber,
          sl_transactionjournal.debtornumber,
          sl_transactionjournal.geolocationlongitude,
          sl_transactionjournal.geolocationlatitude,
          sl_transactionjournal.stopnumber,
          sl_transactionjournal.transitaccountid,
          sl_transactionjournal.faremediaid,
          DECODE (sl_transactionjournal.faremediatype,
                  215, 'ClosedLoop',
                  217, 'OpenLoop',
                  sl_transactionjournal.faremediatype
                 ) AS faremediatype,
          sl_transactionjournal.productid, sl_transactionjournal.ticketid,
          sl_transactionjournal.fareamount,
          DECODE (sl_transactionjournal.customergroup,
                  1, 'Adult',
                  2, 'Youth',
                  3, 'Honored Citizen',
                  4, 'Paratransit',
                  sl_transactionjournal.customergroup
                 ) AS customergroup,
          sl_transactionjournal.transactiontype,
          sl_transactionjournal.transactionnumber,
          sl_transactionjournal.resulttype, sl_transactionjournal.filllevel,
          sl_transactionjournal.pursebalance,
          sl_transactionjournal.pursecredit, sl_transactionjournal.groupsize,
          sl_transactionjournal.validfrom, sl_transactionjournal.validto,
          sl_transactionjournal.DURATION, sl_transactionjournal.tariffdate,
          sl_transactionjournal.tariffversion,
          sl_transactionjournal.ticketinternalnumber,
          sl_transactionjournal.whitelistversion,
          sl_transactionjournal.cancellationreference,
          sl_transactionjournal.cancellationreferenceguid,
          sl_transactionjournal.lastuser, sl_transactionjournal.lastmodified,
          sl_transactionjournal.transactioncounter,
          sl_transactionjournal.whitelistversioncreated,
          sl_transactionjournal.properties, sl_transactionjournal.saleid,
          sl_transactionjournal.tripticketinternalnumber,
          tm_ticket.NAME AS ticketname, sl_transactionjournal.shiftbegin,
          DECODE (sl_transactionjournal.completionstate,
                  0, 'Pending',
                  1, 'Online',
                  2, 'Offline',
                  3, 'Error',
                  sl_transactionjournal.completionstate
                 ) AS completionstate,
          sl_transactionjournal.responsestate,
          sl_transactionjournal.productid2,
          sl_transactionjournal.pursebalance2,
          sl_transactionjournal.pursecredit2,
          sl_transactionjournal.creditcardauthorizationid,
          sl_transactionjournal.tripcounter, sl_transactionjournal.inserttime,
          sl_card.isvoiceenabled, sl_card.istraining
     FROM sl_transactionjournal INNER JOIN tm_ticket
          ON (sl_transactionjournal.ticketid = tm_ticket.ticketid)
          LEFT OUTER JOIN sl_card
          ON sl_transactionjournal.transitaccountid = sl_card.cardid
          ;


-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
