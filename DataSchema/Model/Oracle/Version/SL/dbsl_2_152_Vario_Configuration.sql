SPOOL dbsl_2_152.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.152', 'BVI', 'Refactoring of user_config and sl_configuration -> new Configuration-Tables');

--Info: This script does have not the default regions, because it is to complex.

-- =======[ Changes on existing tables ]===========================================================================

Alter Table VAS_FilterElement drop Constraint FK_FilterElement_SLConfig;
Alter Table VAS_ReportToClient drop Constraint FK_ReportToClient_Config;

-- =======[ New SL Tables ]========================================================================================

CREATE TABLE SL_ConfigurationDataType
(
  ENUMERATIONVALUE  NUMBER(9, 0)	NOT NULL,
  LITERAL           NVARCHAR2(50)	NOT NULL,
  DESCRIPTION       NVARCHAR2(50)	NOT NULL,
  CONSTRAINT PK_ConfDataTypeID PRIMARY KEY (ENUMERATIONVALUE)
);

Create table SL_ConfigurationDefinition(
    ConfigurationDefinitionID Number(18,0) NOT NULL,
    Name NVARCHAR2(125) NOT NULL,
    GroupKey NVARCHAR2(125) NOT NULL,
    Description NVARCHAR2(250) NOT NULL,
    DataType Number(9) DEFAULT 1 NOT NULL,
    AccessLevel Number(1) DEFAULT 0 NOT NULL,
	IsDefaultConfigurable Number(1) DEFAULT 1 NOT NULL,
    DefaultValue NCLOB,
    Scope NVARCHAR2(255),
    LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
    LastModified DATE DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_ConfDefinition PRIMARY KEY (ConfigurationDefinitionID),
	CONSTRAINT UC_ConfDefinition_01 UNIQUE (Name)
);

Create table SL_ConfigurationOverwrite(
	ConfigurationOverwriteID Number(18,0) NOT NULL,
    ClientID Number(18,0) NOT NULL,
    ConfigurationDefinitionID Number(18,0) NOT NULL,
    Value NCLOB,
    SpecifierType NVARCHAR2(125),
    SpecifierValue NVARCHAR2(125),
    LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
    LastModified DATE DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_ConfOverwrite PRIMARY KEY (ConfigurationOverwriteID),
	CONSTRAINT FK_ConfOverwrite_Definition FOREIGN KEY (ConfigurationDefinitionID) REFERENCES SL_ConfigurationDefinition(ConfigurationDefinitionID),
	CONSTRAINT FK_ConfOverwrite_Client FOREIGN KEY (ClientID) REFERENCES Vario_Client(ClientID),
	CONSTRAINT UC_ConfOverwrite_01 UNIQUE (ClientID, ConfigurationDefinitionID, SpecifierType, SpecifierValue)
);

Create Unique Index UI_ConfigurationOverwrite on SL_ConfigurationOverwrite(ClientID, ConfigurationDefinitionID, upper(SpecifierType), upper(SpecifierValue));

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

Insert Into SL_ConfigurationDataType (EnumerationValue, Literal, Description) values (0, 'Text', 'Text');

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
	'SL_ConfigurationDefinition', 'SL_ConfigurationOverwrite'
  );
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
  
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
	'SL_ConfigurationDefinition', 'SL_ConfigurationOverwrite'
  );
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
  
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
      'create or replace trigger ' || table_names(table_name) || '_BRI' ||
      ' before insert on ' || table_names(table_name) || 
      ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
  
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
      'create or replace trigger ' || table_names(table_name) || '_BRU' ||
      ' before update on ' || table_names(table_name) || 
      ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data Import ]==============================================================================================

Declare -- This imports all configurations from User_Config and from SL_Configuration
    configDefID number(9);
    countExisting number(9);
    
    uc_configID User_Config.configID%type;
    uc_clientID User_Config.ClientID%type;
    uc_configValue User_Config.ConfigValue%type;
    uc_remark User_Config.Remark%type;
    uc_isGlobal User_Config.IsGlobal%type;
    
    slc_configID SL_Configuration.ConfigurationID%type;
    slc_name SL_Configuration.Name%type;
    slc_scope SL_Configuration.Scope%type;
    slc_description SL_Configuration.Description%type;
    slc_value SL_Configuration.ConfigurationValue%type;
    
    slc_lastUser SL_Configuration.LastUser%type;
    
    cdef_nextID SL_ConfigurationDefinition.ConfigurationDefinitionID%type;
    
    CURSOR userConfigGlobals is Select * from User_Config where clientid = 0;
    CURSOR userConfigClients is Select * from User_Config where clientid > 0;
    CURSOR sl_configs is SELECT ConfigurationID, Name, Scope, Description, LastUser, ConfigurationValue from sl_configuration;
BEGIN
    OPEN userConfigGlobals;
    LOOP --selecting from User_Config, that are "defaults" -> clientID = 0
        FETCH userConfigGlobals into uc_clientID, uc_configID, uc_configValue, uc_remark, uc_isGlobal;
        EXIT WHEN userConfigGlobals%notFound;
        
        dbms_output.put_line('First insert: id: ' || uc_configID || ', value: ' || uc_configValue || '.');
        
        IF uc_remark is null THEN  --null value for description is invalid.
          uc_remark := '-';
        END IF;
        
        INSERT INTO SL_ConfigurationDefinition (ConfigurationDefinitionID, Name, GroupKey, Description, DataType, AccessLevel, DefaultValue, Scope)
            VALUES (SL_ConfigurationDefinition_Seq.nextval, uc_configID, 'Vario', uc_remark, 0, 6, uc_configValue, null);
        
    END LOOP;
    CLOSE userConfigGlobals;
    
    OPEN userConfigClients;
    LOOP -- selecting all "overwrites" from User_Config -> clientID > 0
        FETCH userConfigClients into uc_clientID, uc_configID, uc_configValue, uc_remark, uc_isGlobal;
        EXIT WHEN userConfigClients%notFound;
        
        IF uc_remark is null THEN  --null value for description is invalid.
          uc_remark := '-';
        END IF;
        
        Select count(*) into countExisting
        from SL_ConfigurationDefinition cd
        where CD.NAME = uc_configID;
        
        IF countExisting > 0 THEN
            Select cd.ConfigurationDefinitionID into configDefID
            from SL_ConfigurationDefinition cd
            where CD.NAME = uc_configID;
        ELSE
          Select SL_ConfigurationDefinition_Seq.nextval into configDefID from dual;
			
          INSERT INTO SL_ConfigurationDefinition (ConfigurationDefinitionID, Name, GroupKey, Description, DataType, AccessLevel, IsDefaultConfigurable, DefaultValue, Scope)
                VALUES (configDefID, uc_configID, 'Vario', uc_remark, 0, 6, 0, null, null);
        END IF;
        
        Select count(*) into countExisting --check if there is a vario_client with the given clientId
        from VARIO_Client cd
        where CD.ClientID = uc_clientID;
        
        IF countExisting > 0 THEN --There are some invalid configs with invalid clientID. Filtering them here.
			INSERT INTO SL_ConfigurationOverwrite (ConfigurationOverwriteID, ClientID, ConfigurationDefinitionID, Value, SpecifierType, SpecifierValue)
				  VALUES(SL_ConfigurationOverwrite_SEQ.nextval, uc_clientID, configDefID, uc_configValue, '', '');
			dbms_output.put_line('Client type insert: id: ' || uc_configID || ', value: ' || uc_configValue || ', clientID: ' || uc_clientID || '.');
        END IF;
    END LOOP;
    CLOSE userConfigClients;
    
    OPEN sl_configs;
    LOOP -- loop over SL_Configuration
        FETCH sl_configs into slc_configID, slc_name, slc_scope, slc_description, slc_lastUser, slc_value;
        EXIT WHEN sl_configs%notFound;
        
        IF slc_description is null THEN --null value for description is invalid.
            slc_description := '-';
        END IF;
        
        select SL_ConfigurationDefinition_Seq.nextval into cdef_nextID from dual;
        
        IF slc_scope is not null and lower(slc_scope) != lower('Vario') THEN --Configs from User_Config has been overwritten in SL_Configuration with "Vario"-scope. They will be ignored.
            INSERT INTO SL_ConfigurationDefinition (ConfigurationDefinitionID, Name, GroupKey, Description, DataType, AccessLevel, DefaultValue, Scope)
                VALUES (cdef_nextID, slc_name, slc_scope, slc_description, 0, 6, slc_value, null);
                
            Update Vas_FilterElement set ValueListDataSourceID = cdef_nextID, TransactionCounter = TransactionCounter+1 where ValueListDataSourceID = slc_configID;
            Update Vas_ReportToClient set ConfigurationID = cdef_nextID where ConfigurationID = slc_configID;
        END IF;
    END LOOP;
    CLOSE sl_configs;
END;
/

-- =======[ Changes on existing tables ]===========================================================================

Alter Table VAS_FilterElement add Constraint FK_FilterElement_Config Foreign Key (ValueListDataSourceID) REFERENCES SL_ConfigurationDefinition(ConfigurationDefinitionID);
Alter Table VAS_ReportToClient add Constraint FK_ReportToClient_Config Foreign Key (ConfigurationID) REFERENCES SL_ConfigurationDefinition(ConfigurationDefinitionID);

create table user_config_backup as select * from user_config;
create table sl_configuration_backup as select * from sl_configuration;

drop table sl_configuration Cascade constraints;
drop table user_config;

-- =======[ Views ]================================================================================================

Create or replace View user_config as
    --Selection of all overwrites
    select 
      o.ClientID as clientid, 
      d.Name as ConfigID, 
      to_char(o.VALUE) as configValue, 
      d.DESCRIPTION as remark, 
      0 as IsGlobal
    from SL_ConfigurationDefinition d 
    join SL_ConfigurationOverwrite o on d.ConfigurationDefinitionID = o.ConfigurationDefinitionID
    where o.SPECIFIERTYPE is null
      and (dbms_lob.getlength(o.Value) <= 4000 or o.value is null)

    UNION
    
    --Selection of default values/definitions
    select 
      to_number(0) as clientid, 
      d.Name as ConfigID, 
      to_char(defaultvalue) as configValue, 
      d.DESCRIPTION as remark, 
      0 as IsGlobal
    from SL_ConfigurationDefinition d
    where d.IsDefaultConfigurable = 1 and
	(dbms_lob.getlength(d.DefaultValue) <= 4000 or d.defaultvalue is null)
;

Create or replace view sl_configuration as 
  Select 
    ConfigurationDefinitionID as ConfigurationID,
    Name as Name,
    GroupKey as Scope,
    Description as Description,
    DefaultValue as ConfigurationValue,
    LastUser as LastUser,
    LastModified as LastModified,
    TransactionCounter as TransactionCounter
  from SL_ConfigurationDefinition;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
