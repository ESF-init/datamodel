SPOOL dbsl_2_070.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.070', 'DST', 'Made OrganizationType full table from enum');

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Changes on existing tables ]===========================================================================

alter table SL_OrganizationType rename column EnumerationValue to OrganizationTypeID;
alter table SL_OrganizationType modify OrganizationTypeID NUMBER(18);
alter table SL_OrganizationType ADD (
  LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
  LastModified DATE DEFAULT sysdate NOT NULL,
  TransactionCounter INTEGER DEFAULT 0 NOT NULL
);
alter table SL_OrganizationType modify TransactionCounter DEFAULT NULL;

alter table SL_Organization rename column OrganizationType to OrganizationTypeID;
alter table SL_Organization modify OrganizationTypeID NUMBER(18);
alter table SL_Organization add constraint FK_ORG_TYPE FOREIGN KEY (OrganizationTypeID) references SL_OrganizationType(OrganizationTypeID);
	
-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
  'SL_OrganizationType' );
   sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
	'SL_OrganizationType' 
  );
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
