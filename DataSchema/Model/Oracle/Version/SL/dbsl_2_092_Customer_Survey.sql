SPOOL dbsl_2_092.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.092', 'TSC', 'Customer Survey.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

/*
ALTER TABLE SL_CustomerReply DROP PRIMARY KEY DROP INDEX;

CREATE TABLE SL_CustomerSurvey (
	CustomerSurveyID NUMBER(18, 0) NOT NULL,
	Name NVARCHAR2(50) NOT NULL, 
	Description NVARCHAR2(250),
	ValidFrom DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL, 
	ValidTo DATE DEFAULT TO_DATE('31-12-9999 23:59:59', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_CustomerSurvey PRIMARY KEY (CustomerSurveyID)
);

ALTER TABLE SL_CustomerReply
ADD (
	CustomerSurveyID NUMBER(18, 0) NOT NULL
);

ALTER TABLE SL_CustomerReply
ADD (
	CONSTRAINT FK_CustomerRepl_CustomerSurvey FOREIGN KEY (CustomerSurveyID) REFERENCES SL_CustomerSurvey(CustomerSurveyID)
);

ALTER TABLE SL_CustomerReply
ADD (
	CONSTRAINT PK_CustomerReply PRIMARY KEY (CustomerSurveyID, CustomerQuestionID, CustomerAnswerID, PersonID)
);

ALTER TABLE SL_CustomerQuestion 
ADD
(
	CustomerSurveyID NUMBER(18, 0) NOT NULL,
	Rank NUMBER(9, 0) DEFAULT 999999999 NOT NULL
);

ALTER TABLE SL_CustomerQuestion
ADD (
	CONSTRAINT FK_CustomerQuest_CustomerSurve FOREIGN KEY (CustomerSurveyID) REFERENCES SL_CustomerSurvey(CustomerSurveyID)
);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW SL_CustomerDemographic (PersonID, PersonFirstName, PersonLastName, SurveyID, SurveyName, SurveyDescription, QuestionID, QuestionLanguage, QuestionText, AnswerID, AnswerText, AnswerFollowUpQuestionID, AnswerFreeText)
AS
  SELECT SL_Person.PersonID,
    SL_Person.FirstName,
    SL_Person.LastName,
    SL_CustomerSurvey.CustomerSurveyID,
    SL_CustomerSurvey.Name,
    SL_CustomerSurvey.Description,
    SL_CustomerQuestion.CustomerQuestionID,
    SL_CustomerQuestion.Language,
    SL_CustomerQuestion.Text,
    SL_CustomerAnswer.CustomerAnswerID,
    SL_CustomerAnswer.Text,
    SL_CustomerAnswer.CustomerFollowUpQuestionID,
    SL_CustomerReply.FreeTextAnswer
  FROM (((SL_Person
  INNER JOIN SL_CustomerReply
  ON SL_Person.PersonID = SL_CustomerReply.PersonID)
  INNER JOIN SL_CustomerSurvey
  ON SL_CustomerSurvey.CustomerSurveyID = SL_CustomerReply.CustomerSurveyID)
  INNER JOIN SL_CustomerQuestion
  ON SL_CustomerReply.CustomerQuestionID = SL_CustomerQuestion.CustomerQuestionID)
  INNER JOIN SL_CustomerAnswer
  ON SL_CustomerReply.CustomerAnswerID = SL_CustomerAnswer.CustomerAnswerID
  AND SL_CustomerQuestion.Language     = SL_CustomerAnswer.Language
  ORDER BY SL_Person.PersonID,
    SL_CustomerSurvey.CustomerSurveyID,
    SL_CustomerQuestion.Rank,
    SL_CustomerQuestion.CustomerQuestionID;

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_CustomerSurvey');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_CustomerSurvey');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/
*/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
