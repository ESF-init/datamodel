SPOOL dbsl_2_007.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.007', 'MIW', 'New order state');


-- -------[ Sequences ]--------------------------------------------------------------------------------------------

-- -------[ Trigger ]----------------------------------------------------------------------------------------------

-- -------[ Data ]-------------------------------------------------------------------------------------------------

Insert into SL_ORDERSTATE (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	Values (5, 'Failed', 'Failed');

COMMIT;

PROMPT Done!
SPOOL off