SPOOL dbsl_2_121.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.121', 'MIW', 'Changes to accounting tables');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_BANKSTATEMENTRECORDTYPE
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_BANKSTATEMENTRECORDTYPE PRIMARY KEY (ENUMERATIONVALUE)
);

CREATE TABLE SL_BANKSTATEMENTSTATE
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_BANKSTATEMENTSTATE PRIMARY KEY (ENUMERATIONVALUE)
);

CREATE TABLE SL_PAYMENTRECONCILIATIONSTATE
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_PAYMENTRECONCILIATIONSTATE PRIMARY KEY (ENUMERATIONVALUE)
);

CREATE TABLE SL_CLOSEOUTSTATE
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_CLOSEOUTSTATE PRIMARY KEY (ENUMERATIONVALUE)
);

CREATE TABLE SL_CLOSEOUTTYPE
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_CLOSEOUTTYPE PRIMARY KEY (ENUMERATIONVALUE)
);

-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

-- ACC_BANKSTATEMENT --
ALTER TABLE ACC_BANKSTATEMENT MODIFY(STATE DEFAULT 0 NOT NULL );

ALTER TABLE ACC_BANKSTATEMENT ADD (PAYMENTJOURNALID NUMBER(18));

ALTER TABLE ACC_BANKSTATEMENT ADD (RECORDTYPE NUMBER(9) DEFAULT 0 NOT NULL);

ALTER TABLE ACC_BANKSTATEMENT ADD CONSTRAINT FK_BANKSTATEMENT_PAYJOURNAL 
	FOREIGN KEY (PAYMENTJOURNALID) REFERENCES SL_PAYMENTJOURNAL (PAYMENTJOURNALID);

-- ACC_PAYMENTRECONCILIATION --
ALTER TABLE ACC_PAYMENTRECONCILIATION DROP CONSTRAINT FK_PAYMENTRECON_BANKSTATE;

ALTER TABLE ACC_PAYMENTRECONCILIATION DROP COLUMN BANKSTATEMENTID;

ALTER TABLE ACC_PAYMENTRECONCILIATION ADD (RECONCILED DATE DEFAULT sysdate NOT NULL);

-- SL_PAYMENTJOURNAL --
ALTER TABLE SL_PAYMENTJOURNAL ADD (RECONCILIATIONSTATE NUMBER(9) DEFAULT 0 NOT NULL);

 
-- -------[ Data ]--------------------------------------------------------------------------------------------  

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
	INTO SL_BANKSTATEMENTRECORDTYPE (EnumerationValue, Literal, Description) VALUES (0, 'Payment', 'Payment')
	INTO SL_BANKSTATEMENTRECORDTYPE (EnumerationValue, Literal, Description) VALUES (1, 'Chargeback', 'Chargeback')
SELECT * FROM DUAL; 

INSERT ALL
	INTO SL_BANKSTATEMENTSTATE (EnumerationValue, Literal, Description) VALUES (0, 'New', 'New')
	INTO SL_BANKSTATEMENTSTATE (EnumerationValue, Literal, Description) VALUES (1, 'Match', 'Match')
	INTO SL_BANKSTATEMENTSTATE (EnumerationValue, Literal, Description) VALUES (2, 'NoMatch', 'NoMatch')
	INTO SL_BANKSTATEMENTSTATE (EnumerationValue, Literal, Description) VALUES (3, 'Duplicate', 'Duplicate')
SELECT * FROM DUAL; 

INSERT ALL
	INTO SL_PAYMENTRECONCILIATIONSTATE (EnumerationValue, Literal, Description) VALUES (0, 'NotReconciled', 'NotReconciled')
	INTO SL_PAYMENTRECONCILIATIONSTATE (EnumerationValue, Literal, Description) VALUES (1, 'Reconciled', 'Reconciled')
	INTO SL_PAYMENTRECONCILIATIONSTATE (EnumerationValue, Literal, Description) VALUES (2, 'Chargeback', 'Chargeback')
SELECT * FROM DUAL; 

INSERT ALL
	INTO SL_CLOSEOUTSTATE (EnumerationValue, Literal, Description) VALUES (0, 'Started', 'Started')
	INTO SL_CLOSEOUTSTATE (EnumerationValue, Literal, Description) VALUES (1, 'Recognized', 'Recognized')
	INTO SL_CLOSEOUTSTATE (EnumerationValue, Literal, Description) VALUES (2, 'ExportStarted', 'ExportStarted')
	INTO SL_CLOSEOUTSTATE (EnumerationValue, Literal, Description) VALUES (3, 'Exported', 'Exported')
	INTO SL_CLOSEOUTSTATE (EnumerationValue, Literal, Description) VALUES (4, 'Failed', 'Failed')
	INTO SL_CLOSEOUTSTATE (EnumerationValue, Literal, Description) VALUES (5, 'ExportFailed', 'ExportFailed')
SELECT * FROM DUAL; 

INSERT ALL
	INTO SL_CLOSEOUTTYPE (EnumerationValue, Literal, Description) VALUES (0, 'Sale', 'Sale')
	INTO SL_CLOSEOUTTYPE (EnumerationValue, Literal, Description) VALUES (1, 'Payment', 'Payment')
	INTO SL_CLOSEOUTTYPE (EnumerationValue, Literal, Description) VALUES (2, 'PaymentReconciliation', 'PaymentReconciliation')
	INTO SL_CLOSEOUTTYPE (EnumerationValue, Literal, Description) VALUES (3, 'RevenueRecognition', 'RevenueRecognition')
	INTO SL_CLOSEOUTTYPE (EnumerationValue, Literal, Description) VALUES (4, 'Settlement', 'Settlement')
SELECT * FROM DUAL; 

COMMIT;

PROMPT Done!
SPOOL off
