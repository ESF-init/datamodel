-- -------[ Drop Unused Tables, Triggers and Sequences ]-----------------------------------------------------------

DECLARE
  TYPE table_names_array IS VARRAY(3) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
	'SL_EmailMessage', 'SL_MessageTemplate', 'SL_MessageField'); 
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Deleting BRI trigger for: ' || table_names(table_name));
	  sql_string := 'DROP TRIGGER ' || table_names(table_name) || '_BRI';
	  BEGIN
		EXECUTE IMMEDIATE sql_string;
		EXCEPTION WHEN OTHERS THEN 
	  dbms_output.put_line('Error deleting BRI trigger: ' || sqlerrm);
	  END;
	  
	  dbms_output.put_line('Deleting BRU trigger for: ' || table_names(table_name));
	  sql_string := 'DROP TRIGGER ' || table_names(table_name) || '_BRU';
	  BEGIN
		EXECUTE IMMEDIATE sql_string;
		EXCEPTION WHEN OTHERS THEN 
	  dbms_output.put_line('Error deleting BRU trigger: ' || sqlerrm);
	  END;
	  
	  dbms_output.put_line('Deleting sequence for: ' || table_names(table_name));
	  sql_string := 'DROP SEQUENCE ' || table_names(table_name) || '_SEQ';
	  BEGIN
		EXECUTE IMMEDIATE sql_string;
		EXCEPTION WHEN OTHERS THEN 
	  dbms_output.put_line('Error deleting sequence: ' || sqlerrm);
	  END;
	  
	  dbms_output.put_line('Deleting table: ' || table_names(table_name));
	  sql_string := 'DROP TABLE ' || table_names(table_name) || ' CASCADE CONSTRAINTS PURGE';
	  BEGIN
		EXECUTE IMMEDIATE sql_string;
		EXCEPTION WHEN OTHERS THEN 
	  dbms_output.put_line('Error deleting table: ' || sqlerrm);
	  END;
	  
	  dbms_output.put_line('Done!');
	EXCEPTION WHEN OTHERS THEN
	  dbms_output.put_line('Error deleting unused tables: ' || sqlerrm);
	END;
  END LOOP;
END;
/