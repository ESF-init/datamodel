-- WARNING: The SL_Form.SchemaName column values must be set after the script was executed

SPOOL dbsl_2_001.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.001', 'TSC', 'Replaced EmailMessage for use with configurable forms and added EmailEvent. Deleted SL_MessageTemplate and SL_MessageField.');

CREATE TABLE SL_EmailEvent (
	EmailEventID NUMBER(18, 0) NOT NULL, 
	EventName NVARCHAR2(50) NOT NULL,
	Description NVARCHAR2(250),
	IsActive NUMBER(1, 0) DEFAULT 0 NOT NULL,
	ValidFormSchemaName NVARCHAR2(250) NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_EmailEvent PRIMARY KEY (EmailEventID),
	CONSTRAINT UC_EmailEvent_01 UNIQUE (EventName)
);

-- m-zu-n
CREATE TABLE SL_EmailMessage (
	FormID NUMBER(18, 0) NOT NULL, 
	EmailEventID NUMBER(18, 0) NOT NULL, 
	Subject NVARCHAR2(250) NOT NULL, 
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_EmailMessage PRIMARY KEY (FormID, EmailEventID),
	CONSTRAINT FK_EmailMessage_Form FOREIGN KEY (FormID) REFERENCES SL_Form(FormID),
	CONSTRAINT FK_EmailMessage_EmailEvent FOREIGN KEY (EmailEventID) REFERENCES SL_EmailEvent(EmailEventID)
);

-- Alter tables Form, because we need the SchameName to verify it is useable with a specific EmailEvent

ALTER TABLE SL_Form DISABLE ALL TRIGGERS;

ALTER TABLE SL_Form
ADD (
  SchemaName NVARCHAR2(250) 
);

UPDATE SL_Form SET SchemaName = 'none'

ALTER TABLE SL_Form
MODIFY (
  SchemaName NVARCHAR2(250) NOT NULL
);

ALTER TABLE SL_FORM ENABLE  ALL TRIGGERS;

-- -------[ Sequences ]--------------------------------------------------------------------------------------------

DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
	'SL_EmailEvent', 'SL_EmailMessage');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- -------[ Trigger ]----------------------------------------------------------------------------------------------

/* alle update und insert trigger fuer konkurierende Zugriffe */
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
	'SL_EmailEvent', 'SL_EmailMessage');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- -------[ Data ]-------------------------------------------------------------------------------------------------

/* SL_EmailEvent is not an enumeration and uses a sequence, which cannot be used with INSERT ALL. */
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Account Confirmation', 'AccountConfirmation', 1, 'AccountMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Close Account', 'CloseAccount', 1, 'AccountMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Account Update Confirmation', 'UpdateAccount', 1, 'AccountMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Reset Password', 'ResetPassword', 1, 'AccountMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Administrative Account Added', 'AddAdministrativeAccountConfirmation', 1, 'AccountMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Autoload Confirmation', 'AutoloadConfirmation', 1, 'AutoloadMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Autoload Suspended', 'AutoloadSuspended', 0, 'AutoloadMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Autoload Price Change', 'AutoloadPriceChange', 0, 'AutoloadMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Autoload Transaction Failed', 'AutoloadTransactionFailed', 0, 'AutoloadMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Smartcard Registration', 'RegisterSmartcard', 0, 'SmartcardMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Smartcard Removed', 'RemoveSmartcard', 0, 'SmartcardMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Smartcard Blocked', 'BlockSmartcard', 0, 'SmartcardMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Smartcard Unblocked', 'UnblockSmartcard', 0, 'SmartcardMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Smartcard Replaced', 'ReplaceSmartcard', 0, 'SmartcardMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Smartcard Replaced', 'ReplaceSmartcardOrganization', 0, 'SmartcardMailMerge');                                                                         
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Order Confirmation', 'OrderConfirmation', 0, 'OrderMailMerge');
INSERT INTO SL_EmailEvent (EmailEventID, Description, EventName, IsActive, ValidFormSchemaName) VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'Order Payment Result', 'OrderPaymentResult', 1, 'OrderMailMerge');

COMMIT;

PROMPT Done!
SPOOL off