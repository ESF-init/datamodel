SPOOL dbsl_2_115.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.115', 'DST', 'New table SL_CreditCardTerminal');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

CREATE TABLE SL_CreditCardTerminal
(
  CreditCardTerminalID          NUMBER(18)      NOT NULL,
  ExternalTerminalID            NVARCHAR2(128)  NOT NULL,
  DataWireID                    NVARCHAR2(128),
  DeviceNumber                  NUMBER(9),
  LASTUSER                      NVARCHAR2(50)   DEFAULT 'SYS'     NOT NULL,
  LASTMODIFIED                  DATE            DEFAULT sysdate   NOT NULL,
  TRANSACTIONCOUNTER            INTEGER         NOT NULL
);


CREATE OR REPLACE TRIGGER SL_CreditCardTerminal_BRI before insert ON SL_CreditCardTerminal for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_CreditCardTerminal_BRU before update ON SL_CreditCardTerminal for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

ALTER TABLE SL_CreditCardTerminal 
  ADD CONSTRAINT PK_CreditCardTerminal
  PRIMARY KEY (CreditCardTerminalID)
  USING INDEX TABLESPACE MOBILE_IDX;
 
CREATE SEQUENCE SL_CreditCardTerminal_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;


-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

ALTER TABLE SL_CreditCardAuthorization ADD (
  CreditCardTerminalID     NUMBER(18)   NOT NULL
);

ALTER TABLE SL_CreditCardAuthorization
  ADD CONSTRAINT FK_CreditAuth_CreditTerminal
  FOREIGN KEY (CreditCardTerminalID)
  REFERENCES SL_CreditCardTerminal(CreditCardTerminalID);

-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off
