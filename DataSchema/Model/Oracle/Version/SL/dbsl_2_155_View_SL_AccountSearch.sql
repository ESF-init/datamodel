SPOOL dbsl_2_155.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
    VALUES (sysdate, '2.155', 'LDO', 'Added account search view sl_accountsearch.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW SL_ACCOUNTSEARCH (personid,
                                               contractid,
                                               username,
                                               phonepin,
                                               authenticationtoken,
                                               lastname,
                                               firstname,
                                               dateofbirth,
                                               phonenumber,
                                               organizationname,
                                               addressee,
                                               street,
                                               streetnumber,
                                               addressfield1,
                                               addressfield2,
                                               city,
                                               postalcode,
                                               region,
                                               country,
                                               addresstype,
                                               cardprintednumber,
                                               persontype
                                              )
AS
   SELECT CAST (sl_person.personid AS NUMBER (18, 0)),
          sl_customeraccount.contractid, sl_customeraccount.username,
          sl_customeraccount.phonepassword,
          sl_contract.clientauthenticationtoken, sl_person.lastname,
          sl_person.firstname, sl_person.dateofbirth,
          DECODE (sl_person.phonenumber,
                  '0', '',
                  sl_person.phonenumber
                 ) AS phonenumber,
          sl_organization.NAME AS organizationname,
          sl_address.addressee,
          sl_address.street,
          sl_address.streetnumber,
          sl_address.addressfield1,
          sl_address.addressfield2,
          sl_address.city,
          sl_address.postalcode,
          sl_address.region,
          sl_address.country,
          CASE
             WHEN sl_contractaddress.addressid =
                                          sl_address.addressid
                THEN 'Contract'
             WHEN sl_person.addressid = sl_address.addressid
                THEN 'Person'
             ELSE ''
          END AS addresstype,
          sl_card.printednumber AS cardprintednumber,
          CASE
             WHEN (sl_person.personid = 0)
                THEN ''
             WHEN (sl_customeraccount.personid = sl_person.personid
                  )
                THEN 'Contract'
             WHEN (sl_person.personid = sl_card.participantid)
                THEN 'Participant'
             WHEN (sl_person.personid = sl_card.cardholderid)
                THEN 'CardHolder'
             WHEN (sl_person.typediscriminator = 'CardHolder')
                THEN 'Person:CardHolder'
             ELSE 'Person:Person'
          END AS persontype
     FROM sl_person FULL OUTER JOIN sl_customeraccount
          ON sl_person.contractid = sl_customeraccount.contractid
         OR sl_customeraccount.personid = sl_person.personid
          LEFT JOIN sl_contract ON sl_person.contractid =
                                                        sl_contract.contractid
          LEFT JOIN sl_organization
          ON sl_contract.organizationid = sl_organization.organizationid
          LEFT JOIN sl_contractaddress
          ON sl_contract.contractid = sl_contractaddress.addressid
          LEFT JOIN sl_address
          ON sl_contractaddress.addressid = sl_address.addressid
         OR sl_person.addressid = sl_address.addressid
          LEFT JOIN sl_card
          ON sl_person.personid = sl_card.cardholderid
         OR sl_person.personid = sl_card.participantid
          ;
          
-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
