SPOOL dbsl_2_034.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.034', 'DST', 'View SL_OrderWithTotal');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW SL_OrderWithTotal AS 
WITH totals (orderid, total) as (
select orderid, sum(sl_orderdetail.quantity * sl_product.price)
from sl_orderdetail 
join sl_product on sl_product.productid = sl_orderdetail.productid 
group by orderid
)
select sl_order.*, CAST(totals.total AS NUMBER(9)) as total from totals join sl_order ON SL_order.orderid = totals.orderid;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
