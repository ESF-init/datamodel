SPOOL dbsl_2_182.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.182', 'LDO', 'Changed sl_orderwithtotal view: added externalordernumber.');

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW SL_ORDERWITHTOTAL
AS 
WITH totals as (
select orderid as orderid, sum(sl_orderdetail.quantity * sl_product.price) as total
from sl_orderdetail 
join sl_product on sl_product.productid = sl_orderdetail.productid 
group by orderid
)
select sl_order.ORDERID,sl_order.CONTRACTID,sl_order.INVOICEADDRESSID,sl_order.SHIPPINGADDRESSID,sl_order.PAYMENTOPTIONID,sl_order.ORDERSOURCE,sl_order.STATE,sl_order.ORDERNUMBER,sl_order.ORDERDATE,sl_order.FULFILLDATE,sl_order.ISMAIL,sl_order.VOUCHERNUMBER,sl_order.LASTUSER,sl_order.LASTMODIFIED,sl_order.TRANSACTIONCOUNTER, CAST(totals.total AS NUMBER(9)) as total, sl_sale.externalordernumber
from totals join sl_order ON SL_order.orderid = totals.orderid
left join sl_sale on sl_order.orderid = sl_sale.orderid;
    
COMMIT;

PROMPT Done!
SPOOL off