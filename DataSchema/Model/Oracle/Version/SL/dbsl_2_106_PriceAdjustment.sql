SPOOL dbsl_2_106.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.106', 'MIW', 'Added organizational price adjustment');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

CREATE TABLE SL_PriceAdjustment
(
  PriceAdjustmentID     NUMBER(18)      NOT NULL,
  ORGANIZATIONID        NUMBER(18)      NOT NULL,
  TICKETINTERNALNUMBER  NUMBER(9)       NOT NULL,
  PriceAdjustment       NUMBER(9)       DEFAULT 0         NOT NULL,
  ISPERCENTAGE          NUMBER(1)       DEFAULT 0         NOT NULL,
  LASTUSER              NVARCHAR2(50)   DEFAULT 'SYS'     NOT NULL,
  LASTMODIFIED          DATE            DEFAULT sysdate   NOT NULL,
  TRANSACTIONCOUNTER    INTEGER         NOT NULL
);

CREATE UNIQUE INDEX PK_PriceAdjustment ON SL_PriceAdjustment (PriceAdjustmentID);
CREATE UNIQUE INDEX UC_PriceAdjustment_01 ON SL_PriceAdjustment (ORGANIZATIONID, TICKETINTERNALNUMBER);

CREATE OR REPLACE TRIGGER SL_PriceAdjustment_BRI before insert ON SL_PriceAdjustment for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_PriceAdjustment_BRU before update ON SL_PriceAdjustment for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

ALTER TABLE SL_PriceAdjustment ADD (
  CONSTRAINT PK_PriceAdjustment PRIMARY KEY (PriceAdjustmentID),
  CONSTRAINT UC_PriceAdjustment_01 UNIQUE (ORGANIZATIONID, TICKETINTERNALNUMBER));

-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off