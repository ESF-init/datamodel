SPOOL dbsl_2_116.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.116', 'DST', 'CreditCardAuthorization: New column PurseAmount');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

ALTER TABLE SL_CreditCardAuthorization ADD (
    PurseAmount                     NUMBER(9)         NOT NULL
);

-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off
