SPOOL dbsl_2_180.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.180', 'BVI', 'Changed userconfig view.');

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW USER_CONFIG (CLIENTID, CONFIGID, CONFIGVALUE, REMARK, ISGLOBAL) AS 
  select 
      o.ClientID, 
      d.Name, 
      to_char(o.VALUE), 
      d.DESCRIPTION, 
      CAST (0  AS NUMBER(1,0))
    from SL_ConfigurationDefinition d 
    join SL_ConfigurationOverwrite o on d.ConfigurationDefinitionID = o.ConfigurationDefinitionID
    where o.SPECIFIERTYPE is null
      and (dbms_lob.getlength(o.Value) <= 4000 or o.value is null)

    UNION
    
    --Selection of default values/definitions
    select 
      CAST (0 AS NUMBER(18,0)),
      d.Name, 
      to_char(defaultvalue), 
      d.DESCRIPTION, 
      CAST (0  AS NUMBER(1,0))
    from SL_ConfigurationDefinition d
    where d.IsDefaultConfigurable != 0 and
	(dbms_lob.getlength(d.DefaultValue) <= 4000 or d.defaultvalue is null) 
	;
	
COMMIT;

PROMPT Done!
SPOOL off