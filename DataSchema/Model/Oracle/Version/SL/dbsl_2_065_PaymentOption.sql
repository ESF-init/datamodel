SPOOL dbsl_2_065.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.065', 'MIW', 'New funding source fields (PaymentOption and AutoloadSetting)');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_PAYMENTOPTION ADD(CREDITCARDNETWORK NVARCHAR2(20));

ALTER TABLE SL_PAYMENTOPTION ADD (EXPIRY DATE);

ALTER TABLE SL_PAYMENTOPTION ADD(PRIORITY NUMBER(9) DEFAULT 0 NOT NULL);

ALTER TABLE SL_AUTOLOADSETTING ADD (ALTERNATIVEPAYMENTOPTIONID NUMBER(18));

ALTER TABLE SL_AUTOLOADSETTING ADD CONSTRAINT FK_AutoloadSetting_AltPayOpt FOREIGN KEY (ALTERNATIVEPAYMENTOPTIONID) REFERENCES SL_PaymentOption(PaymentOptionID);

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
