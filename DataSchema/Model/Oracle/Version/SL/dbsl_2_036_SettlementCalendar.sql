SPOOL dbsl_2_036.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
  VALUES (sysdate, '2.036', 'FMT', 'Calendar for settlement');

CREATE TABLE Sl_SettlementCalendar (
  SettlementCalendarID NUMBER(18, 0) NOT NULL,
  NAME NVARCHAR2(50) NOT NULL,
  Description NVARCHAR2(250),
  LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL,
  LastModified DATE DEFAULT SYSDATE NOT NULL,
  TransactionCounter INTEGER NOT NULL,
  CONSTRAINT PK_SettlementCalendar PRIMARY KEY (SettlementCalendarID)
);

CREATE TABLE Sl_SettlementHoliday (
  SettlementHolidayID NUMBER(18, 0) NOT NULL,
  SettlementCalendarID NUMBER(18, 0) NOT NULL,
  NAME NVARCHAR2(50) NOT NULL,
  Holiday DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  Recurrence NVARCHAR2(500),
  LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL,
  LastModified DATE DEFAULT SYSDATE NOT NULL,
  TransactionCounter INTEGER NOT NULL,
  CONSTRAINT PK_SettlementHoliday PRIMARY KEY (SettlementHolidayID),
  CONSTRAINT FK_SettlementHoliday_Calendar FOREIGN KEY (SettlementCalendarID ) REFERENCES Sl_SettlementCalendar(SettlementCalendarID )
);

ALTER TABLE SL_SettlementQuerySetting 
 ADD (
	SettlementCalendarID NUMBER(18, 0),
	CONSTRAINT FK_QuerySetting_Calendar  FOREIGN KEY (SettlementCalendarID)  REFERENCES Sl_SettlementCalendar (SettlementCalendarID)
 );

DECLARE
   TYPE table_names_array IS VARRAY (5) OF VARCHAR2 (100);

   table_names   table_names_array
      := table_names_array ('Sl_SettlementCalendar',
                            'Sl_SettlementHoliday');
   sql_string    VARCHAR2 (500);
BEGIN
   FOR table_name IN table_names.FIRST .. table_names.LAST
   LOOP
      BEGIN
         sql_string :=
               'create sequence '
            || table_names (table_name)
            || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
         DBMS_OUTPUT.put_line (   'Creating sequence for: '
                               || table_names (table_name)
                              );

         EXECUTE IMMEDIATE sql_string;

         DBMS_OUTPUT.put_line ('Done!');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('Error creating sequence: ' || SQLERRM);
      END;
   END LOOP;
END;

DECLARE
   TYPE table_names_array IS VARRAY (5) OF VARCHAR2 (100);

   table_names   table_names_array
      := table_names_array ('Sl_SettlementCalendar',
                            'Sl_SettlementHoliday');
   sql_string    VARCHAR2 (500);
BEGIN
   FOR table_name IN table_names.FIRST .. table_names.LAST
   LOOP
      BEGIN
         sql_string := '';
         DBMS_OUTPUT.put_line (   'Creating insert trigger for: '
                               || table_names (table_name)
                              );
         sql_string :=
               'create or replace trigger '
            || table_names (table_name)
            || '_BRI'
            || ' before insert on '
            || table_names (table_name)
            || ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';

         EXECUTE IMMEDIATE sql_string;

         DBMS_OUTPUT.put_line (   'Creating update trigger for: '
                               || table_names (table_name)
                              );
         sql_string :=
               'create or replace trigger '
            || table_names (table_name)
            || '_BRU'
            || ' before update on '
            || table_names (table_name)
            || ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';

         EXECUTE IMMEDIATE sql_string;

         DBMS_OUTPUT.put_line ('Done!');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('Error creating trigger: ' || SQLERRM);
      END;
   END LOOP;
END;

COMMIT;

PROMPT Done!
SPOOL off