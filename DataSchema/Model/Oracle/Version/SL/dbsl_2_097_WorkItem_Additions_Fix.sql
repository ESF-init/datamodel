SPOOL dbsl_2_097.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.097', 'TSC', 'Additions and revisions to dbsl_1_090.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

CREATE TABLE SL_WorkItemSubject (
    WorkItemSubjectID NUMBER(18,0) NOT NULL,
	Text NVARCHAR2(125) NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
    CONSTRAINT PK_WorkItemSubject PRIMARY KEY (WorkItemSubjectID)
);

ALTER TABLE SL_WORKITEM 
ADD (
    WorkItemSubjectID  NUMBER(18,0) -- ID of the user assigned work item subject
);

ALTER TABLE SL_WORKITEM 
ADD CONSTRAINT FK_WorkItem_Subject FOREIGN KEY (WorkItemSubjectID) REFERENCES SL_WorkItemSubject(WorkItemSubjectID); 

ALTER TABLE SL_WORKITEM 
ADD CONSTRAINT FK_WorkItem_Assignee FOREIGN KEY (Assignee) REFERENCES USER_LIST(UserID); 

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_WorkItemSubject');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_WorkItemSubject');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_WorkItemSubject (WorkItemSubjectID, Text) VALUES (SL_WorkItemSubject_SEQ.NEXTVAL, 'Other');  
INSERT INTO SL_WorkItemSubject (WorkItemSubjectID, Text) VALUES (SL_WorkItemSubject_SEQ.NEXTVAL, 'Card lost');
INSERT INTO SL_WorkItemSubject (WorkItemSubjectID, Text) VALUES (SL_WorkItemSubject_SEQ.NEXTVAL, 'Issues with top ups');
INSERT INTO SL_WorkItemSubject (WorkItemSubjectID, Text) VALUES (SL_WorkItemSubject_SEQ.NEXTVAL, 'Issues with card transactions');
INSERT INTO SL_WorkItemSubject (WorkItemSubjectID, Text) VALUES (SL_WorkItemSubject_SEQ.NEXTVAL, 'Issues with contract');
INSERT INTO SL_WorkItemSubject (WorkItemSubjectID, Text) VALUES (SL_WorkItemSubject_SEQ.NEXTVAL, 'Issues with web access'); 
INSERT INTO SL_WorkItemSubject (WorkItemSubjectID, Text) VALUES (SL_WorkItemSubject_SEQ.NEXTVAL, 'Issues with payment method or funding sources');


-- =======[ Commit ]===============================================================================================

Commit;
