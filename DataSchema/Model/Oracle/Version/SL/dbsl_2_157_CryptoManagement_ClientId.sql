SPOOL dbsl_257.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.157', 'SMF', 'Added ClientID and enumerations for CryptoManagement');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE CM_CryptogramArchive
 ADD (ClientID  NUMBER(18,0) NOT NULL);
 
ALTER TABLE CM_CryptogramArchive
 ADD CONSTRAINT FK_CryptogramArchive_ClientID FOREIGN KEY (ClientID) REFERENCES VARIO_CLIENT(ClientID);
 
ALTER TABLE CM_CryptogramArchive
 MODIFY (DataStatus NUMBER(9,0) DEFAULT 1);

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE CM_CryptogramLoadStatus (
    EnumerationValue NUMBER(9,0)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_CryptogramLoadStatus PRIMARY KEY (EnumerationValue)
);

CREATE TABLE CM_CryptogramErrorType (
    EnumerationValue NUMBER(9,0)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_CryptogramErrorType PRIMARY KEY (EnumerationValue)
);

CREATE TABLE CM_SamModuleStatus (
    EnumerationValue NUMBER(9,0)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT CM_SamModuleStatus PRIMARY KEY (EnumerationValue)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
    INTO CM_CryptogramLoadStatus (EnumerationValue, Literal, Description) VALUES (0, 'Imported', 'Imported')
    INTO CM_CryptogramLoadStatus (EnumerationValue, Literal, Description) VALUES (1, 'Loaded', 'Loaded')
    INTO CM_CryptogramLoadStatus (EnumerationValue, Literal, Description) VALUES (2, 'LoadingFailed', 'Loading Failed')
SELECT * FROM DUAL;

INSERT ALL
    INTO CM_CryptogramErrorType (EnumerationValue, Literal, Description) VALUES (0, 'SuccessfullyLoaded', 'Successfully Loaded')
    INTO CM_CryptogramErrorType (EnumerationValue, Literal, Description) VALUES (1, 'CounterTooLow', 'Counter Too Low')
    INTO CM_CryptogramErrorType (EnumerationValue, Literal, Description) VALUES (2, 'CounterTooHigh', 'Counter Too High')
    INTO CM_CryptogramErrorType (EnumerationValue, Literal, Description) VALUES (3, 'CommandoFailed', 'Commando Failed')
    INTO CM_CryptogramErrorType (EnumerationValue, Literal, Description) VALUES (4, 'SignKeyLocked', 'Sign Key Locked')
    INTO CM_CryptogramErrorType (EnumerationValue, Literal, Description) VALUES (5, 'UnspecifiedError', 'Unspecified Error')
SELECT * FROM DUAL;

INSERT ALL
    INTO CM_SamModuleStatus (EnumerationValue, Literal, Description) VALUES (0, 'Inactive', 'SAM module is inactive.')
    INTO CM_SamModuleStatus (EnumerationValue, Literal, Description) VALUES (1, 'Active', 'SAM module is active.')
SELECT * FROM DUAL;

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

--Rollback;
COMMIT;

PROMPT Done!
SPOOL off
