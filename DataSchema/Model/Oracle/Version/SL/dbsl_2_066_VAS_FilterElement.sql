SPOOL dbsl_2_066.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.066', 'BVI', 'New field to define datasource on filter element.');

ALTER TABLE VAS_FILTERELEMENT ADD(
    VALUELISTDATASOURCEID Number(18,0)
);

ALTER TABLE VAS_FilterElement ADD(
     CONSTRAINT FK_FILTERELEMENT_SLCONFIG FOREIGN KEY (VALUELISTDATASOURCEID) REFERENCES SL_CONFIGURATION(CONFIGURATIONID)   
);

COMMIT;

PROMPT Done!
SPOOL off