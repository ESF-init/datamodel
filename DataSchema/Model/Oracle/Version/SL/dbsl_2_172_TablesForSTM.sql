SPOOL dbsl_2_172.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.172', 'FMT', 'Added tables for STM');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_ADDRESS ADD COORDINATEID NUMBER(18);

ALTER TABLE SL_CARD ADD CLIENTID NUMBER(18);
ALTER TABLE SL_CARD ADD NUMBEROFPRINTS NUMBER(18);

ALTER TABLE SL_CARD
  ADD CONSTRAINT FK_CARD_CLIENT
  FOREIGN KEY (CLIENTID)
  REFERENCES VARIO_CLIENT (CLIENTID);

ALTER TABLE SL_CONTRACT ADD ACCOUNTINGMODEID NUMBER(18);
ALTER TABLE SL_CONTRACT ADD CONTRACTTYPE NUMBER(9);

ALTER TABLE SL_ORDER ADD SCHOOLYEARID NUMBER(18);

ALTER TABLE SL_ORDER
  ADD CONSTRAINT FK_ORDER_SCHOOLYEAR
  FOREIGN KEY (SCHOOLYEARID)
  REFERENCES SL_SCHOOLYEAR (SCHOOLYEARID);

ALTER TABLE SL_ORGANIZATION ADD FRAMEORGANIZATIONID NUMBER(18);
ALTER TABLE SL_ORGANIZATION ADD ORDERERNUMBER NVARCHAR2(25);
ALTER TABLE SL_ORGANIZATION ADD ORGANIZATIONNUMBER NVARCHAR2(25);
ALTER TABLE SL_ORGANIZATION ADD PRINTNAME NVARCHAR2(50);
ALTER TABLE SL_ORGANIZATION ADD SUBTYPE NUMBER(9);

ALTER TABLE SL_ORGANIZATION
  ADD CONSTRAINT FK_ORGANIZATION_ORGANIZATION
  FOREIGN KEY (FRAMEORGANIZATIONID)
  REFERENCES SL_ORGANIZATION (ORGANIZATIONID);

ALTER TABLE SL_PERSON ADD ALTERNATIVEADRESSID NUMBER(18);
ALTER TABLE SL_PERSON ADD GRADE NVARCHAR2(25);
ALTER TABLE SL_PERSON ADD SCHOOLYEARID NUMBER(18);

ALTER TABLE SL_PERSON
  ADD CONSTRAINT FK_PERSON_ALTERNATIVEADDRESS
  FOREIGN KEY (ALTERNATIVEADRESSID)
  REFERENCES SL_ADDRESS (ADDRESSID);

ALTER TABLE SL_PERSON
  ADD CONSTRAINT FK_PERSON_SCHOOLYEAR
  FOREIGN KEY (SCHOOLYEARID)
  REFERENCES SL_SCHOOLYEAR (SCHOOLYEARID);
  
ALTER TABLE SL_REFUND ADD ISRELEVANTFORPOSTING NUMBER(1) DEFAULT 0 NOT NULL;

ALTER TABLE VARIO_STOP ADD COORDINATEID NUMBER(18);
CREATE INDEX IDX_STOP_COORDINATE ON VARIO_STOP (COORDINATEID ASC);

ALTER TABLE SL_CONTRACT
  ADD CONSTRAINT FK_CONTRACT_CLIENT
  FOREIGN KEY (CLIENTID)
  REFERENCES VARIO_CLIENT (CLIENTID);

  
-- =======[ New Tables ]========================================================================================

CREATE TABLE SL_COORDINATE
(
  COORDINATEID NUMBER(18) NOT NULL,
  EASTING NUMBER(20,10) NOT NULL,
  NORTHING NUMBER(20,10) NOT NULL,
  ZONENAME VARCHAR2(5 Byte),
  LASTUSER VARCHAR2(50 Byte) DEFAULT 'SYS' NOT NULL,
  LASTMODIFIED DATE DEFAULT sysdate NOT NULL,
  TRANSACTIONCOUNTER NUMBER NOT NULL
);
ALTER TABLE SL_COORDINATE
  ADD CONSTRAINT PK_SL_COORDINATE
  PRIMARY KEY (COORDINATEID);

 ALTER TABLE VARIO_STOP ADD 
  CONSTRAINT FK_STOP_COORDINATE 
 FOREIGN KEY (COORDINATEID) 
 REFERENCES SL_COORDINATE (COORDINATEID);
 
ALTER TABLE SL_ADDRESS
  ADD CONSTRAINT FK_ADDRESS_COORDINATE
  FOREIGN KEY (COORDINATEID)
  REFERENCES SL_COORDINATE (COORDINATEID);
  
CREATE INDEX IDX_COORDINATE_EASTING ON SL_COORDINATE (EASTING ASC);
CREATE INDEX IDX_COORDINATE_NORTHING ON SL_COORDINATE (NORTHING ASC);

CREATE TABLE SL_ACCOUNTINGMODE
(
  ACCOUNTINGMODEID NUMBER(18) NOT NULL,
  ACCOUNTINGMETHOD NUMBER(9) NOT NULL,
  DESCRIPTION NVARCHAR2(125),
  LASTUSER NVARCHAR2(50) DEFAULT 'SYS' NOT NULL,
  LASTMODIFIED DATE DEFAULT sysdate NOT NULL,
  TRANSACTIONCOUNTER NUMBER NOT NULL
);
ALTER TABLE SL_ACCOUNTINGMODE
  ADD CONSTRAINT PK_SL_ACCOUNTINGMODE
  PRIMARY KEY (ACCOUNTINGMODEID);

ALTER TABLE SL_CONTRACT
  ADD CONSTRAINT FK_CONTRACT_ACCOUNTINGMODE
  FOREIGN KEY (ACCOUNTINGMODEID)
  REFERENCES SL_ACCOUNTINGMODE (ACCOUNTINGMODEID);
  

CREATE TABLE SL_PRODUCTRELATION
(
  PRODUCTRELATIONID NUMBER(18) NOT NULL,
  PRODUCTID NUMBER(18) NOT NULL,
  RELATIONFROM NUMBER(18),
  RELATIONTO NUMBER(18),
  FAREMATRIXENTRYPRIORITY NUMBER(18),
  FROMADDRESSID NUMBER(18),
  TOADDRESSID NUMBER(18)
);
ALTER TABLE SL_PRODUCTRELATION
  ADD CONSTRAINT PK_SL_PRODUCTRELATION
  PRIMARY KEY (PRODUCTRELATIONID);  
ALTER TABLE SL_PRODUCTRELATION ADD 
  CONSTRAINT FK_RELATION_PRODUCT 
 FOREIGN KEY (PRODUCTID) 
 REFERENCES SL_PRODUCT (PRODUCTID);
ALTER TABLE SL_PRODUCTRELATION ADD  
  CONSTRAINT FK_RELATIONFROMADDRESS 
 FOREIGN KEY (FROMADDRESSID) 
 REFERENCES SL_ADDRESS (ADDRESSID);
 ALTER TABLE SL_PRODUCTRELATION ADD  
  CONSTRAINT FK_RELATATIONTOADDRESS 
 FOREIGN KEY (TOADDRESSID) 
 REFERENCES SL_ADDRESS (ADDRESSID);
 
-- -------[ Enumerations ]-----------------------------------------------------------------------------------------
CREATE TABLE SL_ORGANIZATIONSUBTYPE
(
  ENUMERATIONVALUE NUMBER(9) NOT NULL,
  LITERAL NVARCHAR2(50) NOT NULL,
  DESCRIPTION NVARCHAR2(50) NOT NULL
);

ALTER TABLE SL_ORGANIZATIONSUBTYPE
  ADD CONSTRAINT PK_SL_ORGANIZATIONSUBTYPE
  PRIMARY KEY (ENUMERATIONVALUE);
  
  
-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW SL_FRAMEWORKORDERDETAILS
(
  ORDERID,
  FRAMEWORKCOUNTRY,
  FRAMEWORKREGION,
  FRAMEWORKSTREET,
  FRAMEWORKSTREETNUMBER,
  SCHOOLNAME,
  SCHOOLORGANIZATIONNUMBER,
  SCHOOLADDRESSEE,
  SCHOOLADDRESSFIELD1,
  SCHOOLADDRESSFIELD2,
  SCHOOLCITY,
  ORDERNUMBER,
  SCHOOLPOSTALCODE,
  SCHOOLCOUNTRY,
  SCHOOLSTREET,
  SCHOOLSTREETNUMBER,
  SCHOOLREGION,
  ORGANIZATIONALIDENTIFIER,
  FIRSTNAME,
  LASTNAME,
  DATEOFBIRTH,
  PRINTEDNUMBER,
  FRAMEWORKNAME,
  TICKETNAME,
  VALIDFROM,
  VALIDTO,
  RELATIONFROM,
  RELATIONTO,
  RELATIONPRIORITY,
  SCHOOLYEARNAME,
  FRAMEWORKCONTRACTID,
  SCHOOLCONTRACTID,
  PERSONID,
  FRAMEWORKORGNAIZATIONNUMBER,
  PRODUCTRELATIONID,
  PRODUCTID,
  TICKETID,
  ORDERDETAILID,
  FRAMEWORKADDRESSEE,
  FRAMEWORKADDRESSFIELD1,
  FRAMEWORKADDRESSFIELD2,
  FRAMEWORKCITY,
  FRAMEWORKPOSTALCODE
)
AS SELECT orderentity.orderid, orderentity.ordernumber, frameworkcontractorg.NAME AS frameworkname, frameworkcontractorg.organizationnumber AS frameworkorgnaizationnumber, frameworkcontractaddress.addressee AS frameworkaddressee, frameworkcontractaddress.addressfield1 AS frameworkaddressfield1, frameworkcontractaddress.addressfield2 AS frameworkaddressfield2, frameworkcontractaddress.city AS frameworkcity, frameworkcontractaddress.postalcode AS frameworkpostalcode, frameworkcontractaddress.country AS frameworkcountry, frameworkcontractaddress.street AS frameworkstreet, frameworkcontractaddress.streetnumber AS frameworkstreetnumber, frameworkcontractaddress.region AS frameworkregion, schoolorg.NAME AS schoolname, schoolorg.organizationnumber AS schoolorganizationnumber, schoolcontractaddress.addressee AS schooladdressee, schoolcontractaddress.addressfield1 AS schooladdressfield1, schoolcontractaddress.addressfield2 AS schooladdressfield2, schoolcontractaddress.city AS schoolcity, schoolcontractaddress.postalcode AS schoolpostalcode, schoolcontractaddress.country AS schoolcountry, schoolcontractaddress.street AS schoolstreet, schoolcontractaddress.streetnumber AS schoolstreetnumber, schoolcontractaddress.region AS schoolregion, cardholder.organizationalidentifier, cardholder.firstname, cardholder.lastname, cardholder.dateofbirth, card.printednumber, ticket.NAME AS ticketname, product.validfrom, product.validto, relation.relationfrom, relation.relationto, relation.farematrixentrypriority, schoolyear.schoolyearname, frameworkcontract.contractid AS frameworkcontractid, schoolcontract.contractid AS schoolcontractid, cardholder.personid, relation.productrelationid, product.productid, product.ticketid, orderdetail.orderdetailid FROM sl_order orderentity, sl_contract frameworkcontract, sl_orderdetail orderdetail, sl_organization frameworkcontractorg, sl_address frameworkcontractaddress, sl_contractaddress frameworkcta, sl_product product, tm_ticket ticket, sl_productrelation relation, sl_card card, sl_person cardholder, sl_contract schoolcontract, sl_organization schoolorg, sl_address schoolcontractaddress, sl_contractaddress schoolcta, sl_schoolyear schoolyear WHERE orderentity.contractid = frameworkcontract.contractid AND frameworkcontract.organizationid = frameworkcontractorg.organizationid AND frameworkcontractaddress.addressid = frameworkcta.addressid AND frameworkcta.isretired = 0 AND frameworkcta.addresstype = 0 AND frameworkcta.contractid = frameworkcontract.contractid AND orderdetail.orderid = orderentity.orderid AND product.productid = orderdetail.productid AND product.productid = relation.productid AND product.tickettype <> 103 AND product.ticketid = ticket.ticketid AND product.cardid = card.cardid AND card.cardholderid = cardholder.personid AND cardholder.contractid = schoolcontract.contractid AND schoolorg.organizationid = schoolcontract.organizationid AND schoolcta.contractid = schoolcontract.contractid AND schoolcta.addressid = schoolcontractaddress.addressid AND schoolcta.isretired = 0 AND schoolcta.addresstype = 0 AND cardholder.schoolyearid = schoolyear.schoolyearid;

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_COORDINATE', 'SL_ACCOUNTINGMODE', 'SL_PRODUCTRELATION');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_COORDINATE', 'SL_ACCOUNTINGMODE');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------



Insert into SL_ORGANIZATIONSUBTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (0, '-', '-');
Insert into SL_ORGANIZATIONSUBTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (1, 'Grundschule', 'Grundschule');
Insert into SL_ORGANIZATIONSUBTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (2, 'Hauptschule', 'Hauptschule');
Insert into SL_ORGANIZATIONSUBTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (3, 'Realschule', 'Realschule');
Insert into SL_ORGANIZATIONSUBTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (4, 'Gymnasium', 'Gymnasium');
Insert into SL_ORGANIZATIONSUBTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (5, 'Gesamtschule', 'Gesamtschule');
Insert into SL_ORGANIZATIONSUBTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (6, 'Berufsschule', 'Berufsschule');
Insert into SL_ORGANIZATIONSUBTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (7, 'Förderschule', 'Förderschule');
Insert into SL_ORGANIZATIONSUBTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (8, 'Kindergarten', 'Kindergarten');
Insert into SL_ORGANIZATIONSUBTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (9, 'Privatschule', 'Privatschule');
Insert into SL_ORGANIZATIONSUBTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (10, 'Sekundarschule', 'Sekundarschule');

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

--Rollback;
COMMIT;

PROMPT Done!
SPOOL off
