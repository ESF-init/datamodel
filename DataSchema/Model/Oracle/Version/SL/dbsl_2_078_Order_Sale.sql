SPOOL dbsl_2_078.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.078', 'BAW', 'Added relation SL_SALE, added fields to ACC_REVENUESETTLEMENT');

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_SALE ADD (OrderID NUMBER(18));

ALTER TABLE SL_SALE ADD CONSTRAINT FK_SALE_ORDER FOREIGN KEY (ORDERID) REFERENCES SL_ORDER (ORDERID);

ALTER TABLE ACC_REVENUESETTLEMENT ADD (CUSTOMERGROUP NUMBER(9));

ALTER TABLE ACC_REVENUESETTLEMENT ADD (LINEGROUPID NUMBER(18));

ALTER TABLE ACC_REVENUESETTLEMENT ADD (TICKETNUMBER NUMBER(9));

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW ACC_RECOGNIZEDPAYMENT
AS 
  SELECT 
    SL_SALE.SALESCHANNELID, 
    SL_SALE.MERCHANTNUMBER,
	SL_SALE.SALETYPE,
    SL_PAYMENTJOURNAL.PAYMENTTYPE, 
    ACC_PAYMENTRECOGNITION.*
  FROM SL_SALE, SL_PAYMENTJOURNAL, ACC_PAYMENTRECOGNITION
  WHERE SL_SALE.SALEID = SL_PAYMENTJOURNAL.SALEID
    AND SL_PAYMENTJOURNAL.PAYMENTJOURNALID = ACC_PAYMENTRECOGNITION.PAYMENTJOURNALID
  ORDER BY SL_SALE.SALESCHANNELID, SL_SALE.MERCHANTNUMBER, SL_PAYMENTJOURNAL.PAYMENTTYPE;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
