SPOOL dbsl_2_027.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
	VALUES (sysdate, '2.027', 'DST', 'Made PaymentOptionID nullable in SL_AutoloadSetting');

ALTER TABLE SL_AUTOLOADSETTING
MODIFY PAYMENTOPTIONID NULL;
 
COMMIT;

PROMPT Done!
SPOOL off
