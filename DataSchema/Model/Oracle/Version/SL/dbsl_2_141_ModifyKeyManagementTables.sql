SPOOL dbsl_2_141.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.141', 'AMA', 'Key Management: Modify tables');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

ALTER TABLE SL_CERTIFICATE ADD (LABEL    NVARCHAR2(100) );

ALTER TABLE SL_DEVICEREADER ADD (DEVICECLASSID	NUMBER(10));

ALTER TABLE SL_DEVICEREADERKEY RENAME COLUMN TB31KEYBLOCK to KEYBLOCK;

-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off