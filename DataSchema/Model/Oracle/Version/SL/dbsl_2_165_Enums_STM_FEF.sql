SPOOL dbsl_2_165.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.165', 'SMF', 'Enumerations for student management and fare evasion fine.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_ContractType (
    EnumerationValue NUMBER(9,0)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_ContractType PRIMARY KEY (EnumerationValue)
);

CREATE TABLE SL_AccountingMethod (
    EnumerationValue NUMBER(9,0)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_AccountingMethod PRIMARY KEY (EnumerationValue)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_ContractType (EnumerationValue, Literal, Description) VALUES (0, 'Unknown', 'Unknown');
INSERT INTO SL_ContractType (EnumerationValue, Literal, Description) VALUES (1, 'CustomerContract', 'CustomerContract');
INSERT INTO SL_ContractType (EnumerationValue, Literal, Description) VALUES (2, 'SchoolContract', 'SchoolContract');
INSERT INTO SL_ContractType (EnumerationValue, Literal, Description) VALUES (3, 'FrameworkContract', 'FrameworkContract');
INSERT INTO SL_ContractType (EnumerationValue, Literal, Description) VALUES (4, 'AbonementContract', 'AbonementContract');
INSERT INTO SL_ContractType (EnumerationValue, Literal, Description) VALUES (5, 'FareEvasionFineContract', 'FareEvasionFineContract');

INSERT INTO SL_AccountingMethod (EnumerationValue, Literal, Description) VALUES (1, 'Partial_2_Final_1', '2 Abschlag + Endabrechnung');
INSERT INTO SL_AccountingMethod (EnumerationValue, Literal, Description) VALUES (2, 'Partial_N_Final_1', 'N Abschlag+Endabrechnung');
INSERT INTO SL_AccountingMethod (EnumerationValue, Literal, Description) VALUES (3, 'Partial_0_Exact_N_Final_1', 'N Spitz+Endabrechnung');
INSERT INTO SL_AccountingMethod (EnumerationValue, Literal, Description) VALUES (4, 'Instalment', 'Ratenzahlung');

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

--Rollback;
COMMIT;

PROMPT Done!
SPOOL off
