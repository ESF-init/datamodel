SPOOL dbsl_2_042.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.042', 'MIW', 'SL_PERSON added new fields');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_PERSON
 ADD (PERSONALASSISTENT  NUMBER(1));

ALTER TABLE SL_PERSON
 ADD (IDENTIFIER  NVARCHAR2(20));

ALTER TABLE SL_PERSON
 ADD (GENDER  NUMBER(1));

-- =======[ New SL Tables ]========================================================================================

CREATE TABLE SL_Gender (
	EnumerationValue NUMBER(9, 0) NOT NULL,
	Literal NVARCHAR2(50) NOT NULL, 
	Description NVARCHAR2(50) NOT NULL, 
	CONSTRAINT PK_Gender PRIMARY KEY (EnumerationValue)
);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

INSERT ALL
  INTO SL_Gender(EnumerationValue, Literal, Description)VALUES (0, 'Unknown', 'Unknown')
  INTO SL_Gender(EnumerationValue, Literal, Description)VALUES (1, 'Male', 'Male')
  INTO SL_Gender(EnumerationValue, Literal, Description)VALUES (2, 'Female', 'Female')
SELECT * FROM DUAL;

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
