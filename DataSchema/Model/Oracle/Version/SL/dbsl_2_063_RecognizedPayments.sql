SPOOL dbsl_2_063.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.063', 'BAW', 'Created view for accounting payment recognition.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW ACC_RecognizedPayment
AS 
  SELECT 
    sl_sale.saleschannelid, 
    sl_sale.merchantnumber, 
    sl_paymentjournal.paymenttype, 
    acc_paymentrecognition.*
  FROM sl_sale, sl_paymentjournal, acc_paymentrecognition
  WHERE sl_sale.saleid = sl_paymentjournal.saleid
    AND sl_paymentjournal.paymentjournalid = acc_paymentrecognition.paymentjournalid
  ORDER BY sl_sale.saleschannelid, sl_sale.merchantnumber, sl_paymentjournal.paymenttype;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
