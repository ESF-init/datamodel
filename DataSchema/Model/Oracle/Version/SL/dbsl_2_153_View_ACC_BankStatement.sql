SPOOL dbsl_2_153.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.153', 'SLR', 'Added reference to payment journal.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW VIEW_ACC_BANKSTATEMENT
AS
   SELECT ACC_BANKSTATEMENT.BANKSTATEMENTID, ACC_BANKSTATEMENT.FILEDATE,
          ACC_BANKSTATEMENT.FILEREFERENCE, ACC_BANKSTATEMENT.TRANSACTIONDATE,
          ACC_BANKSTATEMENT.PAYMENTREFERENCE,
          ACC_BANKSTATEMENT.AMOUNT AS BANKSTATEMENTAMOUNT,
          NVL (ACC_BANKSTATEMENT.STATE, -999) AS BANKSTATEMENTSTATE,
          ACC_BANKSTATEMENT.LASTUSER AS BANKSTATEMENTLASTUSER,
          ACC_BANKSTATEMENT.LASTMODIFIED AS BANKSTATEMENTLASTMODIFIED,
          ACC_BANKSTATEMENT.TRANSACTIONCOUNTER AS BANKTRANSACTIONCOUNTER,
          NVL (ACC_BANKSTATEMENT.RECORDTYPE, -999) AS RECORDTYPE,
          SL_PAYMENTJOURNAL.PAYMENTJOURNALID,
          SL_PAYMENTJOURNAL.TRANSACTIONJOURNALID,
          NVL (SL_PAYMENTJOURNAL.PAYMENTTYPE, -999) AS PAYMENTTYPE,
          SL_PAYMENTJOURNAL.AMOUNT, SL_PAYMENTJOURNAL.CODE,
          SL_PAYMENTJOURNAL.CONFIRMED, SL_PAYMENTJOURNAL.EXECUTIONTIME,
          DECODE (SL_PAYMENTJOURNAL.EXECUTIONRESULT,
                  100, 'SOK',
                  101, 'DMISSINGFIELD',
                  102, 'DINVALIDDATA',
                  104, 'DDUPLICATE',
                  110, 'SPARTIALAPPROVAL',
                  150, 'ESYSTEM',
                  151, 'ETIMEOUT',
                  152, 'ETIMEOUT',
                  200, 'DAVSNO',
                  201, 'DCALL',
                  202, 'DCARDEXPIRED',
                  203, 'DCARDREFUSED',
                  204, 'DCARDREFUSED',
                  205, 'DCARDREFUSED',
                  207, 'DCARDREFUSED',
                  208, 'DCARDREFUSED',
                  209, 'DCARDREFUSED',
                  210, 'DCARDREFUSED',
                  211, 'DCARDREFUSED',
                  220, 'DCHECKREFUSED',
                  221, 'DCHECKREFUSED',
                  222, 'DCHECKREFUSED',
                  230, 'DCV',
                  231, 'DINVALIDDATA',
                  232, 'DINVALIDDATA',
                  233, 'DINVALIDDATA',
                  234, 'DINVALIDDATA',
                  235, 'DINVALIDDATA',
                  236, 'DINVALIDDATA',
                  237, 'DINVALIDDATA',
                  238, 'DINVALIDDATA',
                  239, 'DINVALIDDATA',
                  240, 'DINVALIDDATA',
                  241, 'DINVALIDDATA',
                  242, 'DNOAUTH',
                  243, 'DINVALIDDATA',
                  246, 'DNOTVOIDABLE',
                  247, 'DINVALIDDATA',
                  248, 'DBOLETODECLINED',
                  250, 'ETIMEOUT',
                  251, 'DCARDREFUSED',
                  254, 'DINVALIDDATA',
                  400, 'DSCORE',
                  450, 'DINVALIDADDRESS',
                  451, 'DINVALIDADDRESS',
                  452, 'DINVALIDADDRESS',
                  453, 'DINVALIDADDRESS',
                  454, 'DINVALIDADDRESS',
                  455, 'DINVALIDADDRESS',
                  456, 'DINVALIDADDRESS',
                  457, 'DINVALIDADDRESS',
                  458, 'addressIDADDRESS',
                  459, 'DINVALIDADDRESS',
                  460, 'DINVALIDADDRESS',
                  461, 'DINVALIDADDRESS',
                  475, 'DAUTHENTICATE',
                  476, 'DAUTHENTICATIONFAILED',
                  480, 'DREVIEW',
                  481, 'DREJECT',
                  520, 'DSETTINGS',
                  700, 'DRESTRICTED',
                  701, 'DRESTRICTED',
                  702, 'DRESTRICTED',
                  703, 'DRESTRICTED',
                  SL_PAYMENTJOURNAL.EXECUTIONRESULT
                 ) AS EXECUTIONRESULT,
          NVL (SL_PAYMENTJOURNAL.STATE, -999) AS STATE,
          SL_PAYMENTJOURNAL.LASTUSER, SL_PAYMENTJOURNAL.LASTMODIFIED,
          SL_PAYMENTJOURNAL.TRANSACTIONCOUNTER, SL_PAYMENTJOURNAL.TOKEN,
          SL_PAYMENTJOURNAL.SALEID,
          NVL (SL_PAYMENTJOURNAL.CREDITCARDNETWORK,
               -999) AS CREDITCARDNETWORK, SL_PAYMENTJOURNAL.ISPRETAX,
          SL_PAYMENTJOURNAL.REFERENCENUMBER,
          NVL (SL_PAYMENTJOURNAL.PAYMENTOPTIONID, -999) AS PAYMENTOPTIONID,
          SL_PAYMENTJOURNAL.MASKEDPAN, SL_PAYMENTJOURNAL.PROVIDERREFERENCE,
          NVL (SL_PAYMENTJOURNAL.RECONCILIATIONSTATE,
               -999
              ) AS RECONCILIATIONSTATE
     FROM ACC_BANKSTATEMENT LEFT JOIN SL_PAYMENTJOURNAL
          ON SL_PAYMENTJOURNAL.PAYMENTJOURNALID = ACC_BANKSTATEMENT.PAYMENTJOURNALID;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
