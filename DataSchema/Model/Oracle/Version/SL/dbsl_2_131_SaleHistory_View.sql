SPOOL dbsl_2_131.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.131', 'SLR', 'SL_SALEHISTORY view created.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW VIEW_SL_SALEHISTORY AS
select SL_SALE.SALETRANSACTIONID,
    SL_SALE.RECEIPTREFERENCE,
    SL_SALE.SALESCHANNELID,
    SL_SALE.LOCATIONNUMBER,
    SL_SALE.DEVICENUMBER,
    SL_SALE.SALESPERSONNUMBER,
    SL_SALE.MERCHANTNUMBER,
    SL_SALE.NETWORKREFERENCE,
    SL_SALE.SALEDATE,
    SL_SALE.CLIENTID   as OPERATORID,
    SL_SALE.CONTRACTID as ACCOUNTID,
    SL_SALE.EXTERNALORDERNUMBER,
    SL_SALE.ORDERID,
    tr.TICKETNAME,
    tr.TICKETID,
    ord.STATE,
    ord.ORDERNUMBER,
    GREATEST(COALESCE(ord.OrderQuantity,0), COALESCE(TransactionQuantity,0) ) AS quantity
  from SL_SALE  
      INNER JOIN      
          (select  
            SL_SALE.SALETRANSACTIONID, 
            tm_ticket.name As TicketName,
            TM_TICKET.TICKETID,
            COUNT(*) AS TransactionQuantity
            from ((SL_SALE inner join SL_TRANSACTIONJOURNAL on SL_SALE.SALEID= SL_TRANSACTIONJOURNAL.SALEID)
                            inner join TM_TICKET on TM_TICKET.TICKETID = SL_TRANSACTIONJOURNAL.TICKETID)
            WHERE TM_TICKET.TICKETTYPE != 200 --PURSE
            GROUP BY SL_SALE.ORDERID, TM_TICKET.TICKETID, TM_TICKET.NAME, SL_SALE.SALETRANSACTIONID
           ) tr
        On SL_SALE.SALETRANSACTIONID = tr.SALETRANSACTIONID
        left outer join 
          (select
            SL_SALE.SALEID, 
            SL_SALE.SALETRANSACTIONID, 
            SL_SALE.ORDERID,
            SL_ORDERDETAIL.ORDERDETAILID,
            SL_ORDER.ORDERNUMBER,
            SL_ORDER.STATE,
            CASE REQUIREDORDERDETAILID
                WHEN NULL THEN SL_ORDERDETAIL.Quantity 
                ELSE (SELECT Quantity FROM SL_ORDERDETAIL WHERE SL_ORDER.ORDERID = SL_ORDERDETAIL.ORDERID AND REQUIREDORDERDETAILID IS NULL)
            END OrderQuantity 
            FROM SL_SALE 
                INNER JOIN SL_ORDER on SL_SALE.ORDERID = SL_ORDER.ORDERID
                left outer join SL_ORDERDETAIL on SL_ORDER.ORDERID = SL_ORDERDETAIL.ORDERID
            ) ord
        On SL_SALE.SALETRANSACTIONID = ord.SALETRANSACTIONID 
  group by SL_SALE.SALETRANSACTIONID,
    SL_SALE.RECEIPTREFERENCE,
    SL_SALE.SALESCHANNELID,
    SL_SALE.LOCATIONNUMBER,
    SL_SALE.DEVICENUMBER,
    SL_SALE.SALESPERSONNUMBER,
    SL_SALE.MERCHANTNUMBER,
    SL_SALE.NETWORKREFERENCE,
    SL_SALE.SALEDATE,
    SL_SALE.CLIENTID,
    SL_SALE.CONTRACTID ,
    SL_SALE.EXTERNALORDERNUMBER,
    SL_SALE.ORDERID,
    tr.TICKETID,
    tr.TICKETNAME,
    ord.STATE,
    ord.ORDERNUMBER,
    GREATEST(COALESCE(ord.OrderQuantity,0), COALESCE(TransactionQuantity,0) );


-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
