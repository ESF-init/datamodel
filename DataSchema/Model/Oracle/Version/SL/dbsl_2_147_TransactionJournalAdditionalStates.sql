SPOOL dbsl_2_147.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.147', 'FLF', 'TransactionJournal: Added ORIGINALDEVICERESULTTYPE and ORIGINALONLINERESULTTYPE');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (ORIGINALONLINERESULTTYPE  NUMBER(9));

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (ORIGINALDEVICERESULTTYPE  NUMBER(9));

-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off
