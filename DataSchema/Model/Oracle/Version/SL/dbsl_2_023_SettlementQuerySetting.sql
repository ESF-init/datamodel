SPOOL dbsl_2_023.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
	VALUES (sysdate, '2.023', 'FMT', 'Changed field StaticValue in SettlementQuerySetting');

ALTER TABLE SL_SettlementQuerySetting 
	MODIFY StaticValue Number;

COMMIT;

PROMPT Done!
SPOOL off
