SPOOL dbsl_2_017.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.017', 'BVI', 'Added columns to view sl_transactionhistory');

-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------
DROP VIEW SL_TRANSACTIONHISTORY;

/* Formatted on 2014/07/23 10:02 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW sl_transactionhistory (cardid,
                                                        cardbalance,
                                                        cardcredit,
                                                        clientid,
                                                        devicebookingstate,
                                                        distance,
                                                        exttikname,
                                                        farestage,
                                                        factor,
                                                        fromtariffzone,
                                                        linename,
                                                        paycardno,
                                                        price,
                                                        routeno,
                                                        transactionid,
                                                        tripdatetime,
                                                        typeid,
                                                        valueofride,
                                                        fromstopno,
                                                        direction,
                                                        ticketname,
                                                        deviceno,
                                                        locationno
                                                       )
AS
   SELECT rm_transaction.cardid, rm_transaction.cardbalance,
          rm_transaction.cardcredit, rm_transaction.clientid,
          rm_transaction.devicebookingstate, rm_transaction.distance,
          rm_transaction.exttikname, rm_transaction.farestage,
          rm_transaction.factor, rm_transaction.fromtariffzone,
          rm_transaction.linename, rm_transaction.paycardno,
          rm_transaction.price, rm_transaction.routeno,
          rm_transaction.transactionid, rm_transaction.tripdatetime,
          rm_transaction.typeid, rm_transaction.valueofride,
          rm_transaction.fromstopno, rm_transaction.direction,
          tm_ticket.NAME AS ticketname,
          RM_TRANSACTION.DEVICENO, RM_SHIFT.LOCATIONNO
     FROM rm_devicebookingstate,
          rm_shift,
          rm_transaction,
          rm_transactiontype,
          tm_ticket
    WHERE rm_shift.shiftid = rm_transaction.shiftid
      AND rm_shift.shiftstateid < 30
      AND rm_transaction.cancellationid = 0
      AND rm_transaction.typeid = rm_transactiontype.typeid
      AND rm_devicebookingstate.devicebookingno =
                                             rm_transaction.devicebookingstate
      AND rm_devicebookingstate.isbooked = 1
      AND tm_ticket.tarifid = rm_transaction.tariffid
      AND tm_ticket.internalnumber = rm_transaction.tikno;


COMMIT;

PROMPT Done!
SPOOL off