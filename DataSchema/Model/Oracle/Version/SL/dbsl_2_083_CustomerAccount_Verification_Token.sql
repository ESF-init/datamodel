SPOOL dbsl_2_083.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.083', 'TSC', 'Added columns for account verification to customer account.');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_CustomerAccount
ADD(
  VerificationToken NVARCHAR2(100),
  VerificationTokenExpiryTime DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  IsPasswordTemporary NUMBER(1, 0) DEFAULT 0 NOT NULL
);

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
