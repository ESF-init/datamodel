SPOOL dbsl_2_021.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
	VALUES (sysdate, '2.021', 'DST', 'Added comment columns to SL_PERSON');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

ALTER TABLE SL_PERSON
	ADD (COMMENTTEXT NVARCHAR2(2000),
	COMMENTALARM NUMBER(1) DEFAULT 0 NOT NULL
);

COMMIT;

PROMPT Done!
SPOOL off
