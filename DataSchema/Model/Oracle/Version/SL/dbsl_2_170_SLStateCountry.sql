SPOOL dbsl_2_170.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.170', 'BVI', 'Added country and state tables.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE SL_Country
(
    CountryID           NUMBER(18,0)      NOT NULL,
    NAME                VARCHAR2(200)     NOT NULL,
    Code                VARCHAR2(20)     NOT NULL,
    DialingPrefix       VARCHAR2(20),
    LastUser            NVARCHAR2(50)  DEFAULT 'SYS'   NOT NULL,
    LastModified        DATE           DEFAULT sysdate NOT NULL,
    TransactionCounter  INTEGER                        NOT NULL,
    CONSTRAINT PK_Country PRIMARY KEY (CountryID)
);

CREATE TABLE SL_StateOfCountry
(
    StateOfCountryID    NUMBER(18,0)      NOT NULL,
    CountryID           NUMBER(18,0),
    Name                VARCHAR2(200)     NOT NULL,
    ShortName           VARCHAR2(20)     NOT NULL,
    ISOCode             VARCHAR2(20)     NOT NULL,
    LastUser            NVARCHAR2(50)  DEFAULT 'SYS'   NOT NULL,
    LastModified        DATE           DEFAULT sysdate NOT NULL,
    TransactionCounter  INTEGER                        NOT NULL,
    CONSTRAINT PK_StateOfCountry PRIMARY KEY (StateOfCountryID),
    CONSTRAINT FK_StateOfCountry_Country    FOREIGN KEY (CountryID)      REFERENCES SL_Country(CountryID)
);

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(3) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_Country', 'SL_StateOfCountry');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(3) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_Country', 'SL_StateOfCountry');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

--Rollback;
COMMIT;

PROMPT Done!
SPOOL off
