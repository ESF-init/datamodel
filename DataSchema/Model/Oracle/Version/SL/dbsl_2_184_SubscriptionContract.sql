SPOOL dbsl_2_184.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.184', 'BVI', 'First changes arroung contract for subscription management.');

-- =======[ Changes on existing tables ]===========================================================================
-- =======[ New SL Tables ]========================================================================================

CREATE TABLE SL_BankConnectionData(
	BankConnectionDataID NUMBER (18,0) NOT NULL,
	AccountOwnerID NUMBER (18,0) NOT NULL,
	IBAN NVARCHAR2(34) NOT NULL,
	BIC NVARCHAR2(11),
	ValidFrom DATE,
	ValidUntil DATE,
	CreationDate DATE DEFAULT sysdate NOT NULL,
	MandateReference NVARCHAR2(35) NOT NULL,
	Description	NVARCHAR2(512),
	LastUsage DATE,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
    LastModified DATE DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_BankConnection PRIMARY KEY (BankConnectionDataID),
	CONSTRAINT FK_BankConnection_Person FOREIGN KEY (AccountOwnerID) REFERENCES SL_Person(PersonID)
);

ALTER TABLE SL_PaymentOption 
ADD BankConnectionDataID NUMBER(18,0);

ALTER TABLE SL_PaymentOption 
ADD ProspectivePaymentOptionID NUMBER(18,0);

ALTER TABLE SL_PaymentOption 
ADD CONSTRAINT FK_PaymentOption_PaymentOption FOREIGN KEY (ProspectivePaymentOptionID) REFERENCES SL_PaymentOption(PaymentOptionID);

ALTER TABLE SL_PaymentOption 
ADD CONSTRAINT FK_PaymentOption_BankConnect FOREIGN KEY (BankConnectionDataID) REFERENCES SL_BankConnectionData(BankConnectionDataID);

ALTER TABLE PP_CREDIT_SCREENING 
ADD BankConnectionDataID NUMBER(18,0);

ALTER TABLE PP_CREDIT_SCREENING 
ADD CONSTRAINT FK_CreditScreening_BankData FOREIGN KEY (BankConnectionDataID) REFERENCES SL_BankConnectionData(BankConnectionDataID);

CREATE TABLE SL_PaymentModality(
	PaymentModalityID NUMBER (18,0) NOT NULL,
	DevicePaymentMethod NUMBER (9,0) NOT NULL,
	Frequency  NUMBER (9,0) NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
    LastModified DATE DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_PaymentModality PRIMARY KEY (PaymentModalityID)
);

ALTER TABLE SL_Contract
ADD PaymentModalityID NUMBER (18,0);

ALTER TABLE SL_Contract 
ADD CONSTRAINT FK_Contract_PaymentModality FOREIGN KEY (PaymentModalityID) REFERENCES SL_PaymentModality(PaymentModalityID);

ALTER TABLE SL_ADDRESS 
ADD ValidFrom DATE;

ALTER TABLE SL_ADDRESS 
modify ValidFrom DATE DEFAULT sysdate;

ALTER TABLE SL_ADDRESS 
ADD ValidUntil DATE;

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(3) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
	'SL_PaymentModality', 'SL_BankConnectionData'
  );
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
  
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(3) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
	'SL_PaymentModality', 'SL_BankConnectionData'
  );
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
  
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
      'create or replace trigger ' || table_names(table_name) || '_BRI' ||
      ' before insert on ' || table_names(table_name) || 
      ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
  
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
      'create or replace trigger ' || table_names(table_name) || '_BRU' ||
      ' before update on ' || table_names(table_name) || 
      ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off;
