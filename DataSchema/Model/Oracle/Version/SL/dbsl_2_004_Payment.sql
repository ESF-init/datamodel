SPOOL dbsl_2_004.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
	VALUES (sysdate, '2.004', 'MIW', 'Moved sl_payment and sl_photograph to RAW and added fields to sl_payment.');

ALTER TABLE sl_photograph MOVE TABLESPACE MOBILE_RAW;
ALTER INDEX PK_PHOTOGRAPH REBUILD;
ALTER INDEX UK_PHOTOGRAPH_01 REBUILD;

ALTER TABLE SL_PAYMENT MOVE TABLESPACE MOBILE_RAW;
ALTER INDEX PK_PAYMENT REBUILD;

ALTER TABLE SL_PAYMENT MODIFY(EXECUTIONRESULT NVARCHAR2(150));

ALTER TABLE SL_PAYMENT ADD (ReceiptNumber NVARCHAR2(50));
ALTER TABLE SL_PAYMENT ADD (TransactionOrderNumber NVARCHAR2(50));
ALTER TABLE SL_PAYMENT ADD (FullResult CLOB);

ALTER TABLE SL_ORDERDETAIL ADD (IsProcessed NUMBER(1) DEFAULT 0 NOT NULL);

-- -------[ Sequences ]--------------------------------------------------------------------------------------------

-- -------[ Trigger ]----------------------------------------------------------------------------------------------

-- -------[ Data ]-------------------------------------------------------------------------------------------------

COMMIT;

PROMPT Done!
SPOOL off