SPOOL dbsl_2_002.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.002', 'TSC', 'Removed CardToPerson and added column CardHolderID to Card. One card should only have one card holder.');

-- Delete current card holder

DELETE
FROM SL_CardToPerson;

DROP TABLE SL_CardToPerson CASCADE CONSTRAINTS PURGE;

-- Update card

ALTER TABLE SL_Card DISABLE ALL TRIGGERS;

ALTER TABLE SL_Card
ADD (
	CardHolderID NUMBER(18, 0), 
	CONSTRAINT FK_Card_Person FOREIGN KEY (CardHolderID) REFERENCES SL_Person(PersonID)
);

ALTER TABLE SL_Card ENABLE  ALL TRIGGERS;

-- -------[ Sequences ]--------------------------------------------------------------------------------------------

-- -------[ Trigger ]----------------------------------------------------------------------------------------------

-- -------[ Data ]-------------------------------------------------------------------------------------------------

COMMIT;

PROMPT Done!
SPOOL off