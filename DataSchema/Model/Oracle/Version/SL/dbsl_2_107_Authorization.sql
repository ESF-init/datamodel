SPOOL dbsl_2_107.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.107', 'DST', 'New table SL_CreditCardAuthorization');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

CREATE TABLE SL_CreditCardAuthorization
(
  CreditCardAuthorizationID     NUMBER(18)      NOT NULL,
  CardID                        NUMBER(18)      NOT NULL,
  Status                        NUMBER(9)       DEFAULT 0         NOT NULL,
  LocalDateTime                 DATE            NOT NULL,
  TransactionDateTime           DATE            NOT NULL,
  ExternalAuthID                NVARCHAR2(25)   NOT NULL,
  SystemTraceAuditNumber        NVARCHAR2(25)   NOT NULL,
  ResponseCode                  NVARCHAR2(25)   NOT NULL,
  Amount                        NUMBER(9)       NOT NULL,
  Token                         NVARCHAR2(50)   NOT NULL,
  EmvData                       NVARCHAR2(1000) NOT NULL,
  LASTUSER                      NVARCHAR2(50)   DEFAULT 'SYS'     NOT NULL,
  LASTMODIFIED                  DATE            DEFAULT sysdate   NOT NULL,
  TRANSACTIONCOUNTER            INTEGER         NOT NULL
);

CREATE UNIQUE INDEX PK_CreditCardAuthorization ON SL_CreditCardAuthorization  (CreditCardAuthorizationID);

CREATE OR REPLACE TRIGGER SL_CreditCardAuthorization_BRI before insert ON SL_CreditCardAuthorization for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_CreditCardAuthorization_BRU before update ON SL_CreditCardAuthorization for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

ALTER TABLE SL_CreditCardAuthorization ADD (
  CONSTRAINT PK_CreditCardAuthorization PRIMARY KEY (CreditCardAuthorizationID)
);

CREATE SEQUENCE SL_CreditCardAuthorization_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;


-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

ALTER TABLE SL_TransactionJournal ADD (
  CreditCardAuthorizationID     NUMBER(18)
);

ALTER TABLE SL_TransactionJournal
ADD (
	CONSTRAINT FK_TransJournal_CreditCardAuth FOREIGN KEY (CreditCardAuthorizationID) REFERENCES SL_CreditCardAuthorization(CreditCardAuthorizationID)
);
-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off
