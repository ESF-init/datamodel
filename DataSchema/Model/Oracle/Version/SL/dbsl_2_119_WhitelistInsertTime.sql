SPOOL dbsl_2_119.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.119', 'DST', 'Whitelist: Added timestamp InsertTime');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

alter table sl_Whitelist add InsertTime TimeStamp;

alter table sl_Whitelist modify inserttime default systimestamp;

alter table sl_WhitelistJournal add InsertTime TimeStamp;

alter table sl_WhitelistJournal modify inserttime default systimestamp;

-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off
