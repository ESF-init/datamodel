SPOOL dbsl_2_110.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '2.110', 'DST', 'Modified table SL_CreditCardAuthorization');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------


-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

ALTER TABLE SL_CreditCardAuthorization ADD (
  CardType                NVARCHAR2(10)    NOT NULL,
  CompletionDateTime      DATE            NOT NULL,
  OrderNumber             NVARCHAR2(15)   NOT NULL,
  TransactionIdentifier   NVARCHAR2(20)   NULL,
  BankNetData             NVARCHAR2(13)   NULL
);

-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off
