SPOOL dbsl_2_167.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.167', 'SLR', 'Added table SL_Posting* and SL_INVOICE* for FareEvasionFine and STM');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_INVOICETYPE (
    EnumerationValue NUMBER(9)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_INVOICETYPE PRIMARY KEY (ENUMERATIONVALUE)
);

CREATE TABLE SL_POSTINGSCOPE
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL,
  CONSTRAINT PK_POSTINGSCOPE PRIMARY KEY (ENUMERATIONVALUE)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE SL_SCHOOLYEAR
(
  SCHOOLYEARID              NUMBER(18)                                                                          NOT NULL,
  CLIENTID                  NUMBER(18)          DEFAULT 0                                                       NOT NULL,
  ISCURRENTSCHOOLYEAR       NUMBER(1)                                                                                   ,
  SCHOOLYEARNAME            NVARCHAR2(50)                                                                               ,
  DESCRIPTION               NVARCHAR2(125)                                                                              ,
  STARTDATE                 DATE                DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  ENDDATE                   DATE                DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  NUMBEROFMONTHS            NUMBER(18)                                                                          NOT NULL,
  NUMBEROFACCOUNTINGMONTHS  NUMBER(18)                                                                          NOT NULL,
  LASTUSER                  NVARCHAR2(50)       DEFAULT 'SYS'                                                   NOT NULL,
  LASTMODIFIED              DATE                DEFAULT sysdate                                                 NOT NULL,
  TRANSACTIONCOUNTER        INTEGER                                                                             NOT NULL,

  CONSTRAINT PK_SCHOOLYEAR PRIMARY KEY (SCHOOLYEARID),
  CONSTRAINT FK_SCHOOLYEAR_CLIENT FOREIGN KEY (CLIENTID) REFERENCES VARIO_CLIENT (CLIENTID)
);

CREATE TABLE SL_INVOICE
(
  INVOICEID           NUMBER(18)                                                                    NOT NULL,
  CONTRACTID          NUMBER(18)                                                                    NOT NULL,
  INVOICEENTRYCOUNT   NUMBER(18)                                                                    NOT NULL,
  INVOICEENTRYAMOUNT  NUMBER(18)                                                                    NOT NULL,
  INVOICETYPE         NUMBER(9)                                                                     NOT NULL,
  INVOICENUMBER       NVARCHAR2(50)                                                                 NOT NULL,
  CREATIONDATE        DATE          DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  FROMDATE            DATE          DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  TODATE              DATE          DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  DUEDATE             DATE          DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  LASTUSER            NVARCHAR2(50) DEFAULT 'SYS'                                                   NOT NULL,
  LASTMODIFIED        DATE          DEFAULT sysdate                                                 NOT NULL,
  TRANSACTIONCOUNTER  INTEGER                                                                       NOT NULL,
  SCHOOLYEARID        NUMBER(18)                                                                            ,
  
  CONSTRAINT PK_INVOICE PRIMARY KEY (INVOICEID),
  CONSTRAINT FK_INVOICE_CONTRACT   FOREIGN KEY (CONTRACTID)   REFERENCES SL_CONTRACT (CONTRACTID),
  CONSTRAINT FK_INVOICE_SCHOOLYEAR FOREIGN KEY (SCHOOLYEARID) REFERENCES SL_SCHOOLYEAR (SCHOOLYEARID)
);

CREATE TABLE SL_INVOICEENTRY
(
  INVOICEENTRYID      NUMBER(18)                NOT NULL,
  AMOUNT              NUMBER(18)                NOT NULL,
  POSTINGCOUNT        NUMBER(18)                NOT NULL,
  LASTUSER            NVARCHAR2(50)             DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED        DATE                      DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER  INTEGER                   NOT NULL,
  INVOICEENTRYNUMBER  NUMBER(18),
  INVOICEID           NUMBER(18),
  
  CONSTRAINT PK_INVOICEENTRY PRIMARY KEY (INVOICEENTRYID),
  CONSTRAINT FK_SLINVOICEENTRY_SLINVOICE FOREIGN KEY (INVOICEID) REFERENCES SL_INVOICE (INVOICEID)
);

CREATE TABLE SL_POSTINGKEY
(
  POSTINGKEYID        NUMBER(18)                     NOT NULL,
  CLIENTID            NUMBER(18)    DEFAULT 0        NOT NULL,
  POSTINGSCOPE        NUMBER(9)                      NOT NULL,
  POSTINGKEY          NUMBER(18)                     NOT NULL,
  DESCRIPTION         NVARCHAR2(125)                         ,
  ISCREDIT            NUMBER(1)                      NOT NULL,
  ISMANUALLYBOOKABLE  NUMBER(1)                      NOT NULL,
  LASTUSER            NVARCHAR2(50) DEFAULT 'SYS'    NOT NULL,
  LASTMODIFIED        DATE          DEFAULT sysdate  NOT NULL,
  TRANSACTIONCOUNTER  INTEGER                        NOT NULL,

  CONSTRAINT PK_POSTINGKEY PRIMARY KEY (POSTINGKEYID),
  CONSTRAINT FK_POSTINGKEY_Client FOREIGN KEY (ClientID) REFERENCES VARIO_CLIENT(ClientID)
);

CREATE TABLE SL_POSTING
(
  POSTINGID           NUMBER(18)                                                                     NOT NULL,
  CONTRACTID          NUMBER(18)                                                                     NOT NULL,
  USERID              NUMBER(18)                                                                     NOT NULL,
  INVOICEENTRYID      NUMBER(18)                                                                             ,
  POSTINGKEYID        NUMBER(18)                                                                     NOT NULL,
  VALUEDATETIME       DATE           DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  DESCRIPTION         NVARCHAR2(125)                                                                         ,
  POSTINGAMOUNT       NUMBER(18)                                                                     NOT NULL,
  POSTINGDATE         DATE           DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
  PRODUCTID           NUMBER(18)                                                                             ,
  LASTUSER            NVARCHAR2(50)  DEFAULT 'SYS'                                                   NOT NULL,
  LASTMODIFIED        DATE           DEFAULT sysdate                                                 NOT NULL,
  TRANSACTIONCOUNTER  INTEGER                                                                        NOT NULL,
  RECEIPTNUMBER       NUMBER(18),
  
  CONSTRAINT PK_POSTING PRIMARY KEY (POSTINGID),
  CONSTRAINT FK_POSTING_USERLIST     FOREIGN KEY (USERID)         REFERENCES USER_LIST (USERID),
  CONSTRAINT FK_POSTING_POSTINGKEY   FOREIGN KEY (POSTINGKEYID)   REFERENCES SL_POSTINGKEY (POSTINGKEYID),
  CONSTRAINT FK_POSTING_CONTRACT     FOREIGN KEY (CONTRACTID)     REFERENCES SL_CONTRACT (CONTRACTID),
  CONSTRAINT FK_POSTING_PRODUCT      FOREIGN KEY (PRODUCTID)      REFERENCES SL_PRODUCT (PRODUCTID),
  CONSTRAINT FK_POSTING_INVOICEENTRY FOREIGN KEY (INVOICEENTRYID) REFERENCES SL_INVOICEENTRY (INVOICEENTRYID)
);

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_SCHOOLYEAR', 'SL_INVOICE', 'SL_INVOICEENTRY', 'SL_POSTINGKEY', 'SL_POSTING');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(90) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_SCHOOLYEAR', 'SL_INVOICE', 'SL_INVOICEENTRY', 'SL_POSTINGKEY', 'SL_POSTING');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_POSTINGSCOPE (EnumerationValue, Literal, Description) VALUES (0, 'Undefined', '-');
INSERT INTO SL_POSTINGSCOPE (EnumerationValue, Literal, Description) VALUES (1, 'STM', 'Schülerverwaltung');
INSERT INTO SL_POSTINGSCOPE (EnumerationValue, Literal, Description) VALUES (2, 'ABO', 'Abonnement');
INSERT INTO SL_POSTINGSCOPE (EnumerationValue, Literal, Description) VALUES (3, 'EBE', 'Erhöhtes Beförderungsentgelt');

INSERT INTO SL_INVOICETYPE (EnumerationValue, Literal, Description) VALUES (1, 'Partial', 'Abschlagsrechnung');
INSERT INTO SL_INVOICETYPE (EnumerationValue, Literal, Description) VALUES (2, 'Final', 'Endabrechnung');
INSERT INTO SL_INVOICETYPE (EnumerationValue, Literal, Description) VALUES (3, 'Exact', 'Spitzabrechnung');

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

--Rollback;
COMMIT;

PROMPT Done!
SPOOL off
