SPOOL dbsl_2_183.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.183', 'FLF', 'CreditCardAuthorization: Added CreditCall columns');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (EXTERNALCARDREFERENCE  VARCHAR2(50));

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (EXTERNALCARDHASH  VARCHAR2(50));

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (LASTEMVDATAUPDATE  DATE);

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (CREATIONDATE  DATE);

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (ACCOUNTSTATE  NUMBER(9));

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (AUTHORIZATIONTYPE  NUMBER(9));

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (ISARQCREQUESTED  NUMBER(1));

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (COMPLETIONTYPE  NUMBER(9));

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (RETRYCOUNTDOWN  NUMBER(9));

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off;
