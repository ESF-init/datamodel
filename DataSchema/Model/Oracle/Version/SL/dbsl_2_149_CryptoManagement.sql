SPOOL dbsl_2_149.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.149', 'SMF', 'Added Crypto Management');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE CM_Resource
(
    ResourceID         NUMBER(18,0)                   NOT NULL,
    BinaryData         BLOB                           NOT NULL,
    HashValue          NVARCHAR2(250)                 NOT NULL,
    DataType           NUMBER(9,0)    DEFAULT 0       NOT NULL,
    LastUser           NVARCHAR2(50)  DEFAULT 'SYS'   NOT NULL,
    LastModified       DATE           DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER                        NOT NULL,
    CONSTRAINT PK_Resource PRIMARY KEY (ResourceID),
    CONSTRAINT UK_Resource_HashValue UNIQUE (HashValue)
);

CREATE TABLE CM_CryptogramArchive
(
    CryptogramArchiveID   NUMBER(18,0)                                                                   NOT NULL,
    CryptogramArchiveName NVARCHAR2(50)                                                                  NOT NULL,
    Description           NVARCHAR2(250)                                                                         ,
    IsReleased            NUMBER(1,0)    DEFAULT 0                                                       NOT NULL,
    IsArchived            NUMBER(1,0)    DEFAULT 0                                                       NOT NULL,
    ReleaseDate           DATE           DEFAULT TO_DATE('31.12.9999 23:59:59', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
    ArchiveDate           DATE           DEFAULT TO_DATE('31.12.9999 23:59:59', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
    DataStatus            NUMBER(9,0)    DEFAULT 0                                                       NOT NULL,
    LastUser              NVARCHAR2(50)  DEFAULT 'SYS'                                                   NOT NULL,
    LastModified          DATE           DEFAULT sysdate                                                 NOT NULL,
    TransactionCounter    INTEGER                                                                        NOT NULL,
    CONSTRAINT PK_CryptogramArchive PRIMARY KEY (CryptogramArchiveID),
    CONSTRAINT UK_CryptogramArchive_Name UNIQUE (CryptogramArchiveName)
);

CREATE TABLE CM_Content
(
    ContentID                 NUMBER(18,0)                   NOT NULL,
    ShortSamNumber            NVARCHAR2(50)                  NOT NULL,
    KeyLoadCounter            NUMBER(9,0)    DEFAULT 0       NOT NULL,
    CryptogramResourceID      NUMBER(18,0)                   NOT NULL,
    SignCertificateResourceID NUMBER(18,0)                   NOT NULL,
    SubCertificateResourceID  NUMBER(18,0)                   NOT NULL,
    CryptogramFileName        NVARCHAR2(512)                 NOT NULL,
    SignCertificateFileName   NVARCHAR2(512)                 NOT NULL,
    SubCertificateFileName    NVARCHAR2(512)                 NOT NULL,
    LoadStatus                NUMBER(9,0)    DEFAULT 0       NOT NULL,
    ErrorType                 NUMBER(9,0)    DEFAULT 0       NOT NULL,
    CryptogramArchiveID       NUMBER(18,0)                   NOT NULL,
    LastUser                  NVARCHAR2(50)  DEFAULT 'SYS'   NOT NULL,
    LastModified              DATE           DEFAULT sysdate NOT NULL,
    TransactionCounter        INTEGER                        NOT NULL,
    CONSTRAINT PK_Content PRIMARY KEY (ContentID),
    CONSTRAINT FK_Content_Resource_Cry      FOREIGN KEY (CryptogramResourceID)      REFERENCES CM_Resource(ResourceID),
    CONSTRAINT FK_Content_Resource_Sig      FOREIGN KEY (SignCertificateResourceID) REFERENCES CM_Resource(ResourceID),
    CONSTRAINT FK_Content_Resource_Sub      FOREIGN KEY (SubCertificateResourceID)  REFERENCES CM_Resource(ResourceID),
    CONSTRAINT FK_Content_CryptogramArchive FOREIGN KEY (CryptogramArchiveID)       REFERENCES CM_CryptogramArchive(CryptogramArchiveID),
    CONSTRAINT UK_Content_ShortSam_KeyLoad UNIQUE (ShortSamNumber, KeyLoadCounter)
);

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(3) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'CM_Resource', 'CM_Content', 'CM_CryptogramArchive');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(3) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'CM_Resource', 'CM_Content', 'CM_CryptogramArchive');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

--Rollback;
COMMIT;

PROMPT Done!
SPOOL off
