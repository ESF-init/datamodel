SPOOL dbsl_2_081.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.081', 'DST', 'Added Parameter->Unit support');

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Changes on existing tables ]===========================================================================

alter table SL_PARAMETERVALUE modify UnitCollectionID null;
alter table SL_PARAMETERVALUE add UnitID Number(10,0);
ALTER TABLE SL_PARAMETERVALUE
  ADD CONSTRAINT FK_PARAMETERVAL_UNIT FOREIGN KEY (UnitID)
  REFERENCES UM_Unit (UnitID);
 
alter table sl_parametervalue add ParameterArchiveID Number(18,0);
ALTER TABLE SL_PARAMETERVALUE
  ADD CONSTRAINT FK_PARAMETERVAL_ARCHIVE FOREIGN KEY (ParameterArchiveID)
  REFERENCES SL_ParameterArchive (ParameterArchiveID);

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW SL_ParameterArchiveResult AS 
with params as (
SELECT    CAST (NVL (um_unit.unitno, -1) AS NUMBER (10, 0)) UnitNumber, um_unit.unitname,
          sl_parameter.parameternumber, sl_parameter.description ParameterDescription,
          sl_parameter.defaultvalue, sl_parametervalue.parametervalue,
          sl_parametervalue.mountingplateindex, sl_unitcollection.NAME unitcollectionname,
          NVL(sl_parametervalue.parameterarchiveid, sl_unitcollection.parameterarchiveid) parameterarchiveid,
          sl_parametervalue.deviceclassid,
          -- use ROW_NUMBER to give priority to unit dependent parameters over unit collection based ones
          ROW_NUMBER() OVER (partition by NVL(sl_parametervalue.parameterarchiveid, sl_unitcollection.parameterarchiveid), um_unit.unitid, sl_parametervalue.Parameterid, sl_parametervalue.mountingplateindex, sl_parametervalue.deviceclassid order by sl_parametervalue.unitid) rn 
FROM sl_parametervalue
JOIN sl_parameter ON sl_parametervalue.parameterid = sl_parameter.parameterid
LEFT JOIN sl_unitcollection ON sl_unitcollection.unitcollectionid = sl_parametervalue.unitcollectionid
LEFT JOIN sl_unitcollectiontounit ON sl_parametervalue.unitcollectionid = sl_unitcollectiontounit.unitcollectionid
LEFT JOIN um_unit ON sl_unitcollectiontounit.unitid = um_unit.unitid OR sl_parametervalue.unitid = um_unit.unitid
WHERE (isglobal = 1 OR unitno IS NOT NULL)
)
select UNITNUMBER,
  UNITNAME,
  PARAMETERNUMBER,
  PARAMETERDESCRIPTION,
  DEFAULTVALUE,
  PARAMETERVALUE,
  MOUNTINGPLATEINDEX,
  UNITCOLLECTIONNAME,
  PARAMETERARCHIVEID,
  DEVICECLASSID
 from params
 where rn = 1;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
