SPOOL dbsl_2_135.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.135', 'SLR', 'Added SaleType and SaleId attribute to SaleHistory view');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW VIEW_SL_SALEHISTORY AS
SELECT 
    SL_SALE.SALEID,
    SL_SALE.SALETRANSACTIONID,
    SL_SALE.RECEIPTREFERENCE,
    SL_SALE.SALESCHANNELID,
    SL_SALE.SALETYPE,
    SL_SALE.LOCATIONNUMBER,
    SL_SALE.DEVICENUMBER,
    SL_SALE.SALESPERSONNUMBER,
    SL_SALE.MERCHANTNUMBER,
    SL_SALE.NETWORKREFERENCE,
    SL_SALE.SALEDATE,
    SL_SALE.CLIENTID   AS OPERATORID,
    SL_SALE.CONTRACTID AS ACCOUNTID,
    SL_SALE.EXTERNALORDERNUMBER,
    SL_SALE.ORDERID,
    TR.TICKETNAME,
    TR.TICKETID,
    ORD.STATE,
    ORD.ORDERNUMBER,
    GREATEST(COALESCE(ORD.ORDERQUANTITY,0), COALESCE(TRANSACTIONQUANTITY,0) ) AS QUANTITY
  FROM SL_SALE  
      INNER JOIN      
          (SELECT  
            SL_SALE.SALETRANSACTIONID, 
            TM_TICKET.NAME AS TICKETNAME,
            TM_TICKET.TICKETID,
            COUNT(*) AS TRANSACTIONQUANTITY
            FROM ((SL_SALE INNER JOIN SL_TRANSACTIONJOURNAL ON SL_SALE.SALEID= SL_TRANSACTIONJOURNAL.SALEID)
                            INNER JOIN TM_TICKET ON TM_TICKET.TICKETID = SL_TRANSACTIONJOURNAL.TICKETID)
            WHERE TM_TICKET.TICKETTYPE != 200 --PURSE
            GROUP BY SL_SALE.ORDERID, TM_TICKET.TICKETID, TM_TICKET.NAME, SL_SALE.SALETRANSACTIONID
           ) TR
        ON SL_SALE.SALETRANSACTIONID = TR.SALETRANSACTIONID
        LEFT OUTER JOIN 
          (SELECT
            SL_SALE.SALEID, 
            SL_SALE.SALETRANSACTIONID, 
            SL_SALE.ORDERID,
            SL_ORDERDETAIL.ORDERDETAILID,
            SL_ORDER.ORDERNUMBER,
            SL_ORDER.STATE,
            CASE REQUIREDORDERDETAILID
                WHEN NULL THEN SL_ORDERDETAIL.QUANTITY 
                ELSE (SELECT QUANTITY FROM SL_ORDERDETAIL WHERE SL_ORDER.ORDERID = SL_ORDERDETAIL.ORDERID AND REQUIREDORDERDETAILID IS NULL)
            END ORDERQUANTITY 
            FROM SL_SALE 
                INNER JOIN SL_ORDER ON SL_SALE.ORDERID = SL_ORDER.ORDERID
                LEFT OUTER JOIN SL_ORDERDETAIL ON SL_ORDER.ORDERID = SL_ORDERDETAIL.ORDERID
            ) ORD
        ON SL_SALE.SALETRANSACTIONID = ORD.SALETRANSACTIONID 
  GROUP BY 
    SL_SALE.SALEID,
    SL_SALE.SALETRANSACTIONID,
    SL_SALE.RECEIPTREFERENCE,
    SL_SALE.SALESCHANNELID,
    SL_SALE.SALETYPE,
    SL_SALE.LOCATIONNUMBER,
    SL_SALE.DEVICENUMBER,
    SL_SALE.SALESPERSONNUMBER,
    SL_SALE.MERCHANTNUMBER,
    SL_SALE.NETWORKREFERENCE,
    SL_SALE.SALEDATE,
    SL_SALE.CLIENTID,
    SL_SALE.CONTRACTID ,
    SL_SALE.EXTERNALORDERNUMBER,
    SL_SALE.ORDERID,
    TR.TICKETID,
    TR.TICKETNAME,
    ORD.STATE,
    ORD.ORDERNUMBER,
    GREATEST(COALESCE(ORD.ORDERQUANTITY,0), COALESCE(TRANSACTIONQUANTITY,0) );


-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
