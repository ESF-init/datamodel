SPOOL dbsl_2_011.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.011', 'DST', 'SL_CustomerAccount: Phonepassword nullable');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_CUSTOMERACCOUNT MODIFY PHONEPASSWORD NULL;


COMMIT;

PROMPT Done!
SPOOL off