SPOOL dbsl_2_040.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.040', 'DST', 'Added FaremediaID and ShortCode to SL_CARD, Municipality, UserGroupExpiry and UserGroup to SL_Person and FillLevel to SL_Product');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_CARD
 ADD (FAREMEDIAID  NVARCHAR2(100));

ALTER TABLE SL_CARD
 ADD (SHORTCODE  NVARCHAR2(20));

ALTER TABLE SL_PERSON
 ADD (MUNICIPALITY  NVARCHAR2(100));

ALTER TABLE SL_PERSON
 ADD (USERGROUPEXPIRY  DATE);

ALTER TABLE SL_PERSON
 ADD (USERGROUP  NUMBER(9));

ALTER TABLE SL_PRODUCT
 ADD (FILLLEVEL  NUMBER(9));

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
