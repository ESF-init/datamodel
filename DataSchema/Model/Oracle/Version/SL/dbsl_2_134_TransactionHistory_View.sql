SPOOL dbsl_2_134.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.134', 'DST', 'Modified TransactionHistory view to remove duplicates');

-- was previously 1.131 (duplicate number)

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW SL_TRANSACTIONHISTORY
(
  CARDID,
  CARDBALANCE,
  CARDCREDIT,
  CLIENTID,
  DEVICEBOOKINGSTATE,
  DISTANCE,
  EXTTIKNAME,
  FARESTAGE,
  FACTOR,
  FROMTARIFFZONE,
  LINENAME,
  PAYCARDNO,
  PRICE,
  ROUTENO,
  TRANSACTIONID,
  TRIPDATETIME,
  TYPEID,
  TYPENAME,
  VALUEOFRIDE,
  FROMSTOPNO,
  STOPCODE,
  DIRECTION,
  TICKETNAME,
  DEVICENO,
  LOCATIONNO,
  STOPNAME,
  VALIDFROM,
  VALIDUNTIL,
  DEVICEPAYMENTMETHOD,
  DEBTORNO,
  REMAININGRIDES,
  ORGANIZATIONID
)
AS 
SELECT  distinct rm_transaction.cardid, rm_transaction.cardbalance,
            rm_transaction.cardcredit, rm_transaction.clientid,
            rm_transaction.devicebookingstate, rm_transaction.distance,
            rm_transaction.exttikname, rm_transaction.farestage,
            rm_transaction.factor, rm_transaction.fromtariffzone,
            rm_transaction.linename, rm_transaction.paycardno,
            rm_transaction.price, rm_transaction.routeno,
            rm_transaction.transactionid, rm_transaction.tripdatetime,
            rm_transaction.typeid, rm_transactiontype.typename,
            rm_transaction.valueofride, rm_transaction.fromstopno,
            vario_stop.stopcode, rm_transaction.direction,
            tm_ticket.NAME AS ticketname, rm_transaction.deviceno,
            rm_shift.locationno, vario_stop.stopname,
            rm_transaction.validfrom, rm_transaction.validuntil,
            rm_transaction.devicepaymentmethod, rm_shift.debtorno,
            rm_transaction.remainingrides, sl_product.organizationid
      FROM rm_transaction
      join rm_devicebookingstate on rm_transaction.devicebookingstate = rm_devicebookingstate.devicebookingno
      join rm_transactiontype on rm_transaction.typeid = rm_transactiontype.typeid
      join rm_shift on rm_transaction.shiftid = rm_shift.shiftid
      join tm_ticket on rm_transaction.TARIFFID = tm_ticket.tarifid AND rm_transaction.tikno = tm_ticket.internalnumber
      join tm_tarif on tm_ticket.TARIFID = tm_tarif.TARIFID
      join vario_net on tm_tarif.NETID = vario_net.NETID
      join sl_card on rm_transaction.cardid = sl_card.cardid
      left join sl_product on rm_transaction.cardid = SL_PRODUCT.CARDID and (RM_TRANSACTION.TICKETINSTANCEID = SL_PRODUCT.INSTANCENUMBER or rm_transaction.ticketinstanceid=0)
      left join vario_stop on vario_net.netid = vario_stop.netid and rm_transaction.fromstopno = vario_stop.stopno
      WHERE 
        rm_shift.shiftstateid < 30
        AND rm_transaction.cancellationid = 0
        AND rm_devicebookingstate.isbooked = 1
   ORDER BY rm_transaction.cardid ASC, rm_transaction.tripdatetime DESC;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
