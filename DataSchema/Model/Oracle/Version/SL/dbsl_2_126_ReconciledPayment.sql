SPOOL dbsl_2_126.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.126', 'BAW', 'Additional view for reconciled payments.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW acc_reconciledpayment (saleschannelid, merchantnumber, saletype, paymenttype, paymentreconciliationid, paymentjournalid, postingdate, postingreference, creditaccountnumber, debitaccountnumber, amount, lastuser, lastmodified, transactioncounter, closeoutperiodid, reconciled)
AS
  SELECT sl_sale.saleschannelid,
    sl_sale.merchantnumber,
    sl_sale.saletype,
    sl_paymentjournal.paymenttype,
    acc_paymentreconciliation.paymentreconciliationid,
    acc_paymentreconciliation.paymentjournalid,
    acc_paymentreconciliation.postingdate,
    acc_paymentreconciliation.postingreference,
    acc_paymentreconciliation.creditaccountnumber,
    acc_paymentreconciliation.debitaccountnumber,
    acc_paymentreconciliation.amount,
    acc_paymentreconciliation.lastuser,
    acc_paymentreconciliation.lastmodified,
    acc_paymentreconciliation.transactioncounter,
    acc_paymentreconciliation.closeoutperiodid,
    acc_paymentreconciliation.reconciled
  FROM sl_sale,
    sl_paymentjournal,
    acc_paymentreconciliation
  WHERE sl_sale.saleid                   = sl_paymentjournal.saleid
  AND sl_paymentjournal.paymentjournalid = acc_paymentreconciliation.paymentjournalid
  ORDER BY sl_sale.saleschannelid,
    sl_sale.merchantnumber,
    sl_paymentjournal.paymenttype;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
