SPOOL dbsl_2_174.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.174', 'BVI', 'Added new column to configuration definition. Changed userconfig view.');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_ConfigurationDefinition
ADD IsDefaultConfigurable Number(1);

Update SL_ConfigurationDefinition
SET IsDefaultConfigurable = 1, transactioncounter = transactioncounter + 1;

ALTER TABLE SL_ConfigurationDefinition
MODIFY IsDefaultConfigurable Number(1) DEFAULT 1 NOT NULL;

BEGIN
	EXECUTE IMMEDIATE 'create table user_config_backup as select * from user_config'; --equivalent to "create table if not exists"
EXCEPTION
     WHEN OTHERS THEN
      IF SQLCODE != -955 THEN
         RAISE;
      END IF;
End;
/

BEGIN
	EXECUTE IMMEDIATE 'create table sl_configuration_backup as select * from sl_configuration'; --equivalent to  "create table if not exists"
EXCEPTION
     WHEN OTHERS THEN
      IF SQLCODE != -955 THEN
         RAISE;
      END IF;
End;
/

BEGIN
	EXECUTE IMMEDIATE 'drop table sl_configuration'; --equivalent to "drop table if exists"
EXCEPTION
     WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
End;
/

BEGIN
	EXECUTE IMMEDIATE 'drop table user_config'; --equivalent to "drop table if exists"
EXCEPTION
     WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
End;
/

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW USER_CONFIG (CLIENTID, CONFIGID, CONFIGVALUE, REMARK, ISGLOBAL) AS 
  select 
      o.ClientID, 
      d.Name, 
      to_char(o.VALUE), 
      d.DESCRIPTION, 
      CAST (0  AS NUMBER(1,0))
    from SL_ConfigurationDefinition d 
    join SL_ConfigurationOverwrite o on d.ConfigurationDefinitionID = o.ConfigurationDefinitionID
    where o.SPECIFIERTYPE is null
      and (dbms_lob.getlength(o.Value) <= 4000 or o.value is null)

    UNION
    
    --Selection of default values/definitions
    select 
      CAST (0 AS NUMBER(18,0)),
      d.Name, 
      to_char(defaultvalue), 
      d.DESCRIPTION, 
      CAST (0  AS NUMBER(1,0))
    from SL_ConfigurationDefinition d
    where d.IsDefaultConfigurable = 1 and
	(dbms_lob.getlength(d.DefaultValue) <= 4000 or d.defaultvalue is null) 
	;
	
Create or replace view sl_configuration as 
  Select 
    ConfigurationDefinitionID as ConfigurationID,
    Name as Name,
    GroupKey as Scope,
    Description as Description,
    DefaultValue as ConfigurationValue,
    LastUser as LastUser,
    LastModified as LastModified,
    TransactionCounter as TransactionCounter
  from SL_ConfigurationDefinition;
  
-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]=================================================================================================

update sl_configurationDefinition d
set d.IsDefaultConfigurable = 0, d.transactioncounter = d.transactioncounter+1
where d.DefaultValue is null;

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
