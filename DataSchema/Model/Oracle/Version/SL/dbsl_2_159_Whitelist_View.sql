SPOOL dbsl_2_159.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.159', 'SLR', 'Whitelist view added.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================


/* Formatted on 2016/10/10 13:23 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW view_sl_whitelistjournal 
AS
   SELECT sl_whitelist.whitelistid, sl_whitelist.cardid,
          sl_whitelist.byterepresentation, sl_whitelist.faremediaid,
          NVL (sl_whitelist.faremediatype, 0) AS cardtype,
          sl_whitelist.initiatortransactiontime
     FROM sl_whitelist;



-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
