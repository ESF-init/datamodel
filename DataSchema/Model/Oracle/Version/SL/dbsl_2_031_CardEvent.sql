SPOOL dbsl_2_031.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.031', 'FMT', 'Added new enumeration values to SL_EventType');

INSERT ALL
	INTO SL_CardEventType (EnumerationValue, Literal, Description) VALUES (10, 'ServiceComment', 'ServiceComment')
	INTO SL_CardEventType (EnumerationValue, Literal, Description) VALUES (11, 'ProductModification', 'ProductModification')
SELECT * FROM DUAL; 

COMMIT;
