SPOOL dbsl_2_085.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.085', 'TSC', 'Multiple Shipping Addresses per Contract.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

CREATE TABLE SL_ContractAddress (
	ContractID NUMBER(18, 0) NOT NULL, 
	AddressID NUMBER(18, 0) NOT NULL, 
	AddressType NUMBER(9, 0) NOT NULL, 
	Created DATE DEFAULT sysdate NOT NULL,
	IsRetired NUMBER(1) DEFAULT 0 NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL,
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_ContractAddress PRIMARY KEY (ContractID, AddressID),
	CONSTRAINT FK_ContractAddress_Contract FOREIGN KEY (ContractID) REFERENCES SL_Contract(ContractID),
	CONSTRAINT FK_ContractAddress_Address FOREIGN KEY (AddressID) REFERENCES SL_Address(AddressID)
);

CREATE TABLE SL_AddressType (
	EnumerationValue NUMBER(9, 0) NOT NULL,
	Literal NVARCHAR2(50) NOT NULL, 
	Description NVARCHAR2(50) NOT NULL, 
	CONSTRAINT PK_AddressType PRIMARY KEY (EnumerationValue)
);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_ContractAddress');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
    INTO SL_AddressType (EnumerationValue, Literal, Description) VALUES (0, 'Invoice', 'Invoice Address')
    INTO SL_AddressType (EnumerationValue, Literal, Description) VALUES (1, 'Shipping', 'Shipping Address')
SELECT * FROM DUAL; 

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- Move invoice address references
INSERT INTO SL_ContractAddress ( ContractID, AddressID, AddressType, Created, IsRetired )
SELECT  SL_Contract.ContractID, SL_Address.AddressID, 0, SL_Address.LastModified, 0
FROM SL_Contract, SL_Address
WHERE SL_Contract.InvoiceAddressID = SL_Address.AddressID;

-- Move shipping address references
INSERT INTO SL_ContractAddress ( ContractID, AddressID, AddressType, Created, IsRetired )
SELECT  SL_Contract.ContractID, SL_Address.AddressID, 1, SL_Address.LastModified, 0
FROM SL_Contract, SL_Address
WHERE SL_Contract.ShippingAddressID = SL_Address.AddressID;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
