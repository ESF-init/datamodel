SPOOL dbsl_2_012.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.012', 'MIW', 'SL_PERSON: added Autenticated');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_PERSON ADD (Authenticated NUMBER(1,0) DEFAULT 0 NOT NULL);

COMMIT;

PROMPT Done!
SPOOL off