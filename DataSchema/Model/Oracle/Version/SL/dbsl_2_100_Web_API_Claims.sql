SPOOL dbsl_2_100.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.100', 'TSC', 'Web API Claims.');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE USER_RESOURCE
MODIFY (
  RESOURCEID NVARCHAR2(512),
  RESOURCEDESCR NVARCHAR2(125)
);

ALTER TABLE USER_GROUPRIGHT
MODIFY (
  RESOURCEID NVARCHAR2(512)
);

ALTER TABLE SL_CLAIM
MODIFY (
  RESOURCEID NVARCHAR2(512)
);

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW SL_UserRight ( CLIENTID, USERSHORTNAME, COMPANYNAME, RESOURCEID, LEVELOFRIGHT )
AS
SELECT DISTINCT UG.CLIENTID,
	UL.USERSHORTNAME,
	VC.COMPANYNAME,
	UGR.RESOURCEID,
	CAST (bitoragg(UGR.LEVELOFRIGHT) AS NUMBER(1,0))
  FROM (((USER_LIST UL
  INNER JOIN USER_ISMEMBER UI
  ON UL.USERID = UI.USERID)
  INNER JOIN USER_GROUP UG
  ON UI.USERGROUPID = UG.USERGROUPID)
  INNER JOIN USER_GROUPRIGHT UGR
  ON UG.USERGROUPID = UGR.USERGROUPID)
  INNER JOIN VARIO_CLIENT VC
  ON UG.CLIENTID = VC.CLIENTID
  GROUP BY UG.CLIENTID,
	UL.USERSHORTNAME,
	VC.COMPANYNAME,
	UGR.RESOURCEID
  ORDER BY UG.CLIENTID,
	UL.USERSHORTNAME,
	VC.COMPANYNAME,
	UGR.RESOURCEID;
	
CREATE OR REPLACE FORCE VIEW SL_USERCLAIM (CLIENTID, USERSHORTNAME, RESOURCEID, URI) 
AS 
  SELECT DISTINCT UG.CLIENTID,
	UL.USERSHORTNAME,
	UGR.RESOURCEID,
	CL.URI
  FROM (((USER_LIST UL
  INNER JOIN USER_ISMEMBER UI
  ON UL.USERID = UI.USERID)
  INNER JOIN USER_GROUP UG
  ON UI.USERGROUPID = UG.USERGROUPID)
  INNER JOIN USER_GROUPRIGHT UGR
  ON UG.USERGROUPID = UGR.USERGROUPID)
  INNER JOIN SL_CLAIM CL
  ON CL.RESOURCEID = UGR.RESOURCEID
  ORDER BY UG.CLIENTID,
	UL.USERSHORTNAME,
	UGR.RESOURCEID,
	CL.URI;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

Commit;
