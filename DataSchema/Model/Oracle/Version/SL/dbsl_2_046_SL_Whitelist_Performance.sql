SPOOL dbsl_2_046.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '2.046', 'DST', 'Whitelist refactoring + Performance');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_Whitelist
DROP (ValidFrom);
ALTER TABLE SL_Whitelist
DROP (ValidTo);
ALTER TABLE SL_Whitelist
DROP (IncrementNumber);
ALTER TABLE SL_Whitelist
DROP (CardSerialNumber);

ALTER TABLE SL_Whitelist
ADD (FareMediaID     NVARCHAR2(40)      NOT NULL,
     FareMediaType   NUMBER(9)          NOT NULL);

ALTER TABLE SL_Whitelist MODIFY ByteRepresentation NVARCHAR2(2000);

-- =======[ New SL Tables ]========================================================================================

CREATE TABLE SL_WHITELISTJOURNAL
(
   WHITELISTJOURNALID  NUMBER(18)        NOT NULL,
   CARDID              NUMBER(18)        NOT NULL,
   BYTEREPRESENTATION  NVARCHAR2(2000)   NOT NULL,
   LASTUSER            NVARCHAR2(50)     DEFAULT 'SYS' NOT NULL,
   LASTMODIFIED        DATE              DEFAULT sysdate NOT NULL,
   TRANSACTIONCOUNTER  INTEGER           NOT NULL,
   FAREMEDIAID         NVARCHAR2(40)     NOT NULL,
   FAREMEDIATYPE       NUMBER(9)         NOT NULL,
   CONSTRAINT PK_WHITELISTJOURNAL PRIMARY KEY (WHITELISTJOURNALID),
   CONSTRAINT FK_WHITELISTJOURNAL_CARD FOREIGN KEY (CARDID) REFERENCES SL_CARD (CARDID)
);

CREATE TABLE SL_FarePaymentError
(
    FarePaymentErrorID   NUMBER(18)        NOT NULL,
    FarePaymentRequest   NCLOB             NOT NULL,
    Error                NCLOB             NOT NULL,
    LastModified         DATE              DEFAULT sysdate NOT NULL,

    CONSTRAINT PK_FarePaymentError PRIMARY KEY (FarePaymentErrorID)
);

CREATE TABLE SL_RESULTTYPE
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL,
  CONSTRAINT PK_RESULTTYPE PRIMARY KEY (ENUMERATIONVALUE)
);

CREATE TABLE SL_TRANSACTIONJOURNAL
(
  TRANSACTIONJOURNALID  NUMBER(18)              NOT NULL,
  TRANSACTIONID         NVARCHAR2(40)           NOT NULL,
  DEVICETIME            DATE                    NOT NULL,
  BOARDINGTIME          DATE                    ,
  OPERATORID            NUMBER(10)              ,
  CLIENTID              NUMBER(10)              NOT NULL,
  LINE                  NVARCHAR2(20)           ,
  COURSENUMBER          NUMBER(9)               ,
  ROUTENUMBER           NUMBER(9)               ,
  ROUTECODE             NVARCHAR2(20)           ,
  DIRECTION             NUMBER(9)               ,
  ZONE                  NUMBER(9)               ,
  SALESCHANNELID        NUMBER(18)              NOT NULL,
  DEVICENUMBER          NUMBER(9)               ,
  VEHICLENUMBER         NUMBER(9)               ,
  MOUNTINGPLATENUMBER   NUMBER(9)               ,
  DEBTORNUMBER          NUMBER(9)               DEFAULT 0   NOT NULL,
  GEOLOCATIONLONGITUDE  NUMBER(9)               ,
  GEOLOCATIONLATITUDE   NUMBER(9)               ,
  STOPNUMBER            NUMBER(9)               ,

  TRANSITACCOUNTID      NUMBER(18)              ,
  FAREMEDIAID           NVARCHAR2(40)           ,
  FAREMEDIATYPE         NUMBER(9)               DEFAULT 0   NOT NULL,

  PRODUCTID             NUMBER(18)              ,
  TICKETID              NUMBER(18)              NOT NULL,
  FAREAMOUNT            NUMBER(9)               DEFAULT 0   NOT NULL,
  CUSTOMERGROUP         NUMBER(9)               DEFAULT 0   NOT NULL,
  TRANSACTIONTYPE       NUMBER(9)               NOT NULL,
  TRANSACTIONNUMBER     NUMBER(9)               DEFAULT 0   NOT NULL,
  RESULTTYPE            NUMBER(9)               DEFAULT 0   NOT NULL,  
  
  FILLLEVEL             NUMBER(9)               , 
  PURSEBALANCE          NUMBER(9)               ,
  PurseCredit           NUMBER(9)               ,
  GROUPSIZE             NUMBER(9)               ,
  VALIDFROM             DATE                    ,
  VALIDTO               DATE                    ,
  DURATION              NUMBER(9)               DEFAULT 0   NOT NULL,

  TARIFFDATE            DATE                    NOT NULL,
  TARIFFVERSION         NUMBER(9)               DEFAULT 0   NOT NULL,
  TICKETINTERNALNUMBER  NUMBER(9)               DEFAULT 0   NOT NULL,

  WHITELISTVERSION      NUMBER(18)              ,

  CANCELLATIONREFERENCE NUMBER(18)              ,
  CANCELLATIONREFERENCEGUID  NVARCHAR2(40),
  
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'    NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate  NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,
  
  CONSTRAINT PK_TRANSACTIONJOURNAL PRIMARY KEY (TRANSACTIONJOURNALID),
  CONSTRAINT FK_TRANSJOURNAL_CLIENT FOREIGN KEY (CLIENTID) REFERENCES VARIO_CLIENT(CLIENTID),
  CONSTRAINT FK_TRANSJOURNAL_SALESCHANNEL FOREIGN KEY (SALESCHANNELID) REFERENCES VARIO_DEVICECLASS(DEVICECLASSID),
  CONSTRAINT FK_TRANSJOURNAL_PRODUCT FOREIGN KEY (PRODUCTID) REFERENCES SL_PRODUCT(PRODUCTID),
  CONSTRAINT FK_TRANSJOURNAL_CARD FOREIGN KEY (TRANSITACCOUNTID) REFERENCES SL_CARD(CARDID),
  CONSTRAINT FK_TRANSJOURNAL_TICKET FOREIGN KEY (TICKETID) REFERENCES TM_TICKET(TICKETID),
  CONSTRAINT FK_TRANSJOURNAL_CANCELLATION FOREIGN KEY (CANCELLATIONREFERENCE) REFERENCES SL_TRANSACTIONJOURNAL(TRANSACTIONJOURNALID)
);

CREATE TABLE SL_CAPPINGJOURNAL
(
  CAPPINGJOURNALID      NUMBER(18)              NOT NULL,
  PRODUCTID             NUMBER(18)              ,
  TRANSACTIONJOURNALID  NUMBER(18)              NOT NULL,

  POTID                 NUMBER(9)               DEFAULT 0   NOT NULL,
  VALIDFROM             DATE                    NOT NULL,
  AMOUNT                NUMBER(9)               DEFAULT 0   NOT NULL,
  MISSINGAMOUNT         NUMBER(9)               DEFAULT 0   NOT NULL,
  
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,
  
  CONSTRAINT PK_CAPPINGJOURNAL PRIMARY KEY (CAPPINGJOURNALID),
  CONSTRAINT FK_CAPJOURNAL_TRANSJOURNAL FOREIGN KEY (TRANSACTIONJOURNALID) REFERENCES SL_TRANSACTIONJOURNAL (TRANSACTIONJOURNALID)
);

CREATE TABLE SL_PAYMENTJOURNAL
(
  PAYMENTJOURNALID      NUMBER(18)              NOT NULL,
  TRANSACTIONJOURNALID  NUMBER(18)              NOT NULL,

  PAYMENTTYPE           NUMBER(9)               DEFAULT 0   NOT NULL,
  AMOUNT                NUMBER(9)               DEFAULT 0   NOT NULL,
  CODE                  NVARCHAR2(200)          ,
  CONFIRMED             NUMBER(1)               DEFAULT 0   NOT NULL,
  EXECUTIONTIME         DATE                    DEFAULT SYSDATE NOT NULL,
  EXECUTIONRESULT       NVARCHAR2(150)          NOT NULL,
  STATE                 NUMBER(9)               NOT NULL,
  
  LASTUSER              NVARCHAR2(50)           DEFAULT 'SYS'      NOT NULL,
  LASTMODIFIED          DATE                    DEFAULT sysdate    NOT NULL,
  TRANSACTIONCOUNTER    INTEGER                 NOT NULL,
  
  CONSTRAINT PK_PAYMENTJOURNAL PRIMARY KEY (PAYMENTJOURNALID),
  CONSTRAINT FK_PAYJOURNAL_TRANSJOURNAL FOREIGN KEY (TRANSACTIONJOURNALID) REFERENCES SL_TRANSACTIONJOURNAL (TRANSACTIONJOURNALID)
);

CREATE TABLE PERF_Component
(
  ComponentID           NUMBER(18)              NOT NULL,
  Name                  NVARCHAR2(200)          NOT NULL,

  CONSTRAINT PK_Component PRIMARY KEY (ComponentID)
);

CREATE TABLE PERF_Indicator
(
  IndicatorID           NUMBER(18)              NOT NULL,
  Name                  NVARCHAR2(200)          NOT NULL,
  ComponentID           NUMBER(18)              NOT NULL,

  CONSTRAINT PK_Indicator PRIMARY KEY (IndicatorID),
  CONSTRAINT FK_Performance_Component FOREIGN KEY (ComponentID) REFERENCES PERF_Component (ComponentID)
);

CREATE TABLE PERF_Performance
(
  PERFORMANCEID         NUMBER(18)              NOT NULL,
  INDICATORID           NUMBER(18)              NOT NULL,
  EVENTTIME             TIMESTAMP               DEFAULT sysdate NOT NULL,
  DURATION              NUMBER(9)               ,
  QUANTITY              NUMBER(9)               ,
  DESCRIPTION           NVARCHAR2(200)          ,
  AGGREGATION           NUMBER(9)               DEFAULT 0,
  LASTMODIFIED          DATE                    DEFAULT sysdate NOT NULL,
  
  CONSTRAINT PK_PERFORMANCE PRIMARY KEY (PERFORMANCEID),
  CONSTRAINT FK_Performance_Indicator FOREIGN KEY (IndicatorID) REFERENCES PERF_Indicator (IndicatorID)
);


CREATE TABLE SL_AGGREGATIONTYPE
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL,

  CONSTRAINT PK_AggregationType PRIMARY KEY (EnumerationValue)
);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

Insert into SL_AGGREGATIONTYPE  (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values  (0, 'Raw', 'Raw');
Insert into SL_AGGREGATIONTYPE  (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values  (1, 'Minute', 'Minute');
Insert into SL_AGGREGATIONTYPE  (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values  (2, 'Hour', 'Hour');
Insert into SL_AGGREGATIONTYPE  (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values  (3, 'Day', 'Day');
Insert into SL_AGGREGATIONTYPE  (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values  (4, 'Week', 'Week');
Insert into SL_AGGREGATIONTYPE  (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values  (5, 'Month', 'Month');
Insert into SL_AGGREGATIONTYPE  (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values  (6, 'Quarter', 'Quarter');
Insert into SL_AGGREGATIONTYPE  (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values  (7, 'Year', 'Year');

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
   TYPE table_names_array IS VARRAY (9) OF VARCHAR2 (100);

   table_names   table_names_array
      := table_names_array ('SL_TRANSACTIONJOURNAL','SL_CAPPINGJOURNAL','SL_PAYMENTJOURNAL',
                            'SL_FAREPAYMENTERROR', 'PERF_Performance');
   sql_string    VARCHAR2 (500);
BEGIN
   FOR table_name IN table_names.FIRST .. table_names.LAST
   LOOP
      BEGIN
         sql_string :=
               'create sequence '
            || table_names (table_name)
            || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
         DBMS_OUTPUT.put_line (   'Creating sequence for: '
                               || table_names (table_name)
                              );

         EXECUTE IMMEDIATE sql_string;

         DBMS_OUTPUT.put_line ('Done!');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('Error creating sequence: ' || SQLERRM);
      END;
   END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
   TYPE table_names_array IS VARRAY (5) OF VARCHAR2 (100);

   table_names   table_names_array
        := table_names_array ('SL_TRANSACTIONJOURNAL','SL_CAPPINGJOURNAL','SL_PAYMENTJOURNAL');
   sql_string    VARCHAR2 (500);
BEGIN
   FOR table_name IN table_names.FIRST .. table_names.LAST
   LOOP
      BEGIN
         sql_string := '';
         DBMS_OUTPUT.put_line (   'Creating insert trigger for: '
                               || table_names (table_name)
                              );
         sql_string :=
               'create or replace trigger '
            || table_names (table_name)
            || '_BRI'
            || ' before insert on '
            || table_names (table_name)
            || ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';

         EXECUTE IMMEDIATE sql_string;

         DBMS_OUTPUT.put_line (   'Creating update trigger for: '
                               || table_names (table_name)
                              );
         sql_string :=
               'create or replace trigger '
            || table_names (table_name)
            || '_BRU'
            || ' before update on '
            || table_names (table_name)
            || ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';

         EXECUTE IMMEDIATE sql_string;

         DBMS_OUTPUT.put_line ('Done!');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('Error creating trigger: ' || SQLERRM);
      END;
   END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
