SPOOL dbsl_2_026.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
	VALUES (sysdate, '2.026', 'FMT', 'Added FK ralation for OrganizationID on SL_Product');

ALTER TABLE SL_PRODUCT ADD (
CONSTRAINT FK_PRODUCT_ORGANIZATION
FOREIGN KEY (ORGANIZATIONID)
REFERENCES SL_ORGANIZATION (ORGANIZATIONID));
 
COMMIT;

PROMPT Done!
SPOOL off
