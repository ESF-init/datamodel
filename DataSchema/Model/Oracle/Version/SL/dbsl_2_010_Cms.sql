SPOOL dbsl_2_010.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.010', 'DST', 'Changed content in cms to LONG');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE CMS_CONTENTITEM RENAME TO CMS_CONTENTITEM_OLD;

CREATE TABLE CMS_CONTENTITEM
(
   CONTENTITEMID       NUMBER(18)        NOT NULL,
   PAGECONTENTID       NUMBER(18)        NOT NULL,
   SEQUENCENUMBER      NUMBER(9)         NOT NULL,
   RELEVANT            NUMBER(1)         DEFAULT 0 NOT NULL,
   CAPTION             NVARCHAR2(125),
   CONTENT             NCLOB   NOT NULL,
   IMAGE               BLOB,
   LANGUAGE            NVARCHAR2(50),
   LASTUSER            NVARCHAR2(50)     DEFAULT 'SYS' NOT NULL,
   LASTMODIFIED        DATE              DEFAULT sysdate NOT NULL,
   TRANSACTIONCOUNTER  NUMBER            NOT NULL
)
TABLESPACE MOBILE_RAW;

--manually copy data from old to new
throw new sqlexception;

DROP TABLE CMS_CONTENTITEM_OLD;

ALTER TABLE CMS_CONTENTITEM
   ADD CONSTRAINT PK_CONTENTITEM
   PRIMARY KEY (CONTENTITEMID);

ALTER TABLE CMS_CONTENTITEM
  ADD CONSTRAINT FK_CONTENTITEM_PAGECONTENT FOREIGN KEY (PAGECONTENTID)
  REFERENCES CMS_PAGECONTENT (PAGECONTENTID);
  
CREATE OR REPLACE TRIGGER CMS_ContentItem_BRI before insert on CMS_ContentItem for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER CMS_ContentItem_BRU before update on CMS_ContentItem for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

UPDATE CMS_CONTENTTYPE
   SET LITERAL = N'Page',
       DESCRIPTION = N'Page'
WHERE ENUMERATIONVALUE = 0;
UPDATE CMS_CONTENTTYPE
   SET LITERAL = N'Item',
       DESCRIPTION = N'Item'
WHERE ENUMERATIONVALUE = 1;
UPDATE CMS_CONTENTTYPE
   SET LITERAL = N'List',
       DESCRIPTION = N'List'
WHERE ENUMERATIONVALUE = 2;


COMMIT;

PROMPT Done!
SPOOL off