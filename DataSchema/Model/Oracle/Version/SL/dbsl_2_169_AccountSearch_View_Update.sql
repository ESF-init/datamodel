SPOOL dbsl_2_169.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
    VALUES (sysdate, '2.169', 'LDO', 'Updated account search view sl_accountsearch.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

DROP VIEW SL_ACCOUNTSEARCH;

/* Formatted on 2017/01/04 11:32 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW SL_ACCOUNTSEARCH (personid,
                                                      contractid,
                                                      username,
                                                      phonepin,
                                                      accountstate,
                                                      contractnumber,
                                                      authenticationtoken,
                                                      lastname,
                                                      firstname,
                                                      dateofbirth,
                                                      email,
                                                      phonenumber,
                                                      persontype,
                                                      organizationname,
                                                      addressee,
                                                      street,
                                                      streetnumber,
                                                      addressfield1,
                                                      addressfield2,
                                                      city,
                                                      postalcode,
                                                      region,
                                                      country,
                                                      addresstype,
                                                      cardprintednumber
                                                     )
AS
   SELECT sl_person.personid AS personid,
          sl_contract.contractid AS contractid,
          sl_customeraccount.username AS username,
          sl_customeraccount.phonepassword AS phonepin,
          sl_customeraccount.state AS accountstate,
          sl_contract.contractnumber AS contractnumber,
          sl_contract.clientauthenticationtoken AS authenticationtoken,
          sl_person.lastname AS lastname, sl_person.firstname AS firstname,
          sl_person.dateofbirth AS dateofbirth, sl_person.email AS email,
          sl_person.phonenumber AS phonenumber,
          'OrganizationAccountContact' AS persontype,
          sl_organization.NAME AS organizationname,
          sl_address.addressee AS addressee, sl_address.street AS street,
          sl_address.streetnumber AS streetnumber,
          sl_address.addressfield1 AS addressfield1,
          sl_address.addressfield2 AS addressfield2, sl_address.city AS city,
          sl_address.postalcode AS postalcode, sl_address.region AS region,
          sl_address.country AS country, '' AS addresstype,
          CAST (NULL AS NVARCHAR2 (100)) AS cardprintednumber
     FROM sl_person
          INNER JOIN
          (sl_contract LEFT JOIN sl_organization
          ON sl_contract.organizationid = sl_organization.organizationid
          LEFT OUTER JOIN sl_customeraccount
          ON sl_customeraccount.personid = sl_organization.contactpersonid)
          ON sl_person.personid = sl_organization.contactpersonid
          LEFT JOIN sl_address ON sl_address.addressid = sl_person.addressid
    WHERE sl_person.typediscriminator = 'Person'
   UNION
/* Accounts */
   SELECT sl_person.personid AS personid,
          sl_contract.contractid AS contractid,
          sl_customeraccount.username AS username,
          sl_customeraccount.phonepassword AS phonepin,
          sl_customeraccount.state AS accountstate,
          sl_contract.contractnumber AS contractnumber,
          sl_contract.clientauthenticationtoken AS authenticationtoken,
          sl_person.lastname AS lastname, sl_person.firstname AS firstname,
          sl_person.dateofbirth AS dateofbirth, sl_person.email AS email,
          sl_person.phonenumber AS phonenumber,
          (CASE
              WHEN (sl_contract.organizationid IS NULL)
                 THEN 'CustomerAccount'
              ELSE 'OrganizationAccount'
           END
          ) AS persontype,
          sl_organization.NAME AS organizationname,
          sl_address.addressee AS addressee, sl_address.street AS street,
          sl_address.streetnumber AS streetnumber,
          sl_address.addressfield1 AS addressfield1,
          sl_address.addressfield2 AS addressfield2, sl_address.city AS city,
          sl_address.postalcode AS postalcode, sl_address.region AS region,
          sl_address.country AS country, '' AS addresstype,
          CAST (NULL AS NVARCHAR2 (100)) AS cardprintednumber
     FROM sl_person
          INNER JOIN
          (sl_contract LEFT JOIN sl_organization
          ON sl_contract.organizationid = sl_organization.organizationid
          JOIN sl_customeraccount
          ON sl_customeraccount.contractid = sl_contract.contractid)
          ON sl_customeraccount.personid = sl_person.personid
          LEFT JOIN sl_address ON sl_address.addressid = sl_person.addressid
    WHERE sl_person.typediscriminator = 'Person'
      AND (   sl_contract.organizationid IS NULL
           OR sl_person.personid != sl_organization.contactpersonid
          )
   UNION
/* Cardholders with Contract */
   SELECT sl_person.personid AS personid,
          sl_contract.contractid AS contractid,
          CAST (NULL AS NVARCHAR2 (512)) AS username,
          CAST (NULL AS NVARCHAR2 (50)) AS phonepin,
          CAST (NULL AS NUMBER (9)) AS accountstate,
          sl_contract.contractnumber AS contractnumber,
          sl_contract.clientauthenticationtoken AS authenticationtoken,
          sl_person.lastname AS lastname, sl_person.firstname AS firstname,
          sl_person.dateofbirth AS dateofbirth, sl_person.email AS email,
          sl_person.phonenumber AS phonenumber,
          (CASE
              WHEN (sl_card.cardholderid = sl_person.personid)
                 THEN 'CardHolder'
              ELSE 'Participant'
           END
          ) AS persontype,
          sl_organization.NAME AS organizationname,
          sl_address.addressee AS addressee, sl_address.street AS street,
          sl_address.streetnumber AS streetnumber,
          sl_address.addressfield1 AS addressfield1,
          sl_address.addressfield2 AS addressfield2, sl_address.city AS city,
          sl_address.postalcode AS postalcode, sl_address.region AS region,
          sl_address.country AS country, '' AS addresstype,
          sl_card.printednumber AS cardprintednumber
     FROM sl_person
          INNER JOIN
          (sl_contract LEFT JOIN sl_organization
          ON sl_contract.organizationid = sl_organization.organizationid)
          ON sl_person.contractid = sl_contract.contractid
          LEFT JOIN sl_address ON sl_address.addressid = sl_person.addressid
          LEFT JOIN sl_card
          ON sl_card.cardholderid = sl_person.personid
         OR sl_card.participantid = sl_person.personid
    WHERE sl_person.typediscriminator = 'CardHolder'
      AND sl_person.contractid IS NOT NULL
   UNION
/* Cardholders without Contract */
   SELECT sl_person.personid AS personid,
          sl_contract.contractid AS contractid,
          CAST (NULL AS NVARCHAR2 (512)) AS username,
          CAST (NULL AS NVARCHAR2 (50)) AS phonepin,
          CAST (NULL AS NUMBER (9)) AS accountstate,
          sl_contract.contractnumber AS contractnumber,
          sl_contract.clientauthenticationtoken AS authenticationtoken,
          sl_person.lastname AS lastname, sl_person.firstname AS firstname,
          sl_person.dateofbirth AS dateofbirth, sl_person.email AS email,
          sl_person.phonenumber AS phonenumber,
          (CASE
              WHEN (sl_card.cardholderid = sl_person.personid)
                 THEN 'CardHolder'
              ELSE 'Participant'
           END
          ) AS persontype,
          sl_organization.NAME AS organizationname,
          sl_address.addressee AS addressee, sl_address.street AS street,
          sl_address.streetnumber AS streetnumber,
          sl_address.addressfield1 AS addressfield1,
          sl_address.addressfield2 AS addressfield2, sl_address.city AS city,
          sl_address.postalcode AS postalcode, sl_address.region AS region,
          sl_address.country AS country, '' AS addresstype,
          sl_card.printednumber AS cardprintednumber
     FROM sl_person
          LEFT JOIN
          (sl_card LEFT JOIN sl_cardtocontract
          ON sl_card.cardid = sl_cardtocontract.cardid
          LEFT JOIN sl_contract
          ON sl_cardtocontract.contractid = sl_contract.contractid
          LEFT JOIN sl_organization
          ON sl_contract.organizationid = sl_organization.organizationid)
          ON sl_card.cardholderid = sl_person.personid
          LEFT JOIN sl_address ON sl_address.addressid = sl_person.addressid
    WHERE sl_person.typediscriminator = 'CardHolder'
      AND sl_person.contractid IS NULL;



          
-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
