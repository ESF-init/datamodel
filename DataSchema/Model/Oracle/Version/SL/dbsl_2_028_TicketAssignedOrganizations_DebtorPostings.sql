SPOOL dbsl_2_028.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.028', 'TSC', 'Created view TicketAssignedOrganizations and renamed DebtorPostings.PostingCodeNumer to .PostingCodeNumber.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW SL_TicketAssignedOrganizations ( TARIFFID, TICKETID, TICKETTYPE, ORGANIZATIONID, ORGANIZATIONNAME, ORGANIZATIONABBREVIATION, ORGANIZATIONDESCRIPTION,
	SHIPPINGADDRESSSTREET, SHIPPINGADDRESSSTREETNUMBER, SHIPPINGADDRESSADDRESSFIELD1, SHIPPINGADDRESSADDRESSFIELD2, SHIPPINGADDRESSCITY, SHIPPINGADDRESSPOSTALCODE,
	SHIPPINGADDRESSREGION, SHIPPINGADDRESSCOUNTRY, CONTACTFIRSTNAME, CONTACTLASTNAME, CONTACTPHONENUMBER, CONTACTEMAIL
)
AS 
	SELECT 
		TM_TICKET.TARIFID AS TARIFFID,
		TM_TICKET.TICKETID AS TICKETID,
		TM_TICKET.TICKETTYPE AS TICKETTYPE,
		SL_ORGANIZATION.ORGANIZATIONID AS ORGANIZATIONID,
		SL_ORGANIZATION.NAME AS ORGANIZATIONNAME,
		SL_ORGANIZATION.ABBREVIATION AS ORGANIZATIONABBREVIATION,
		SL_ORGANIZATION.DESCRIPTION AS ORGANIZATIONDESCRIPTION,
		SL_ADDRESS.STREET AS SHIPPINGADDRESSSTREET,
		SL_ADDRESS.STREETNUMBER AS SHIPPINGADDRESSSTREETNUMBER,
		SL_ADDRESS.ADDRESSFIELD1 AS SHIPPINGADDRESSADDRESSFIELD1,
		SL_ADDRESS.ADDRESSFIELD2 AS SHIPPINGADDRESSADDRESSFIELD2,
		SL_ADDRESS.CITY AS SHIPPINGADDRESSCITY,
		SL_ADDRESS.POSTALCODE AS SHIPPINGADDRESSPOSTALCODE,
		SL_ADDRESS.REGION AS SHIPPINGADDRESSREGION,
		SL_ADDRESS.COUNTRY AS SHIPPINGADDRESSCOUNTRY,
		SL_PERSON.FIRSTNAME AS CONTACTFIRSTNAME,
		SL_PERSON.LASTNAME AS CONTACTLASTNAME,
		SL_PERSON.PHONENUMBER AS CONTACTPHONENUMBER,
		SL_PERSON.EMAIL AS CONTACTEMAIL
  FROM 
	TM_TICKET, TM_TICKET_ORGANIZATION, SL_ORGANIZATION, SL_CONTRACT, SL_ADDRESS, SL_PERSON
  WHERE TM_TICKET.TICKETID = TM_TICKET_ORGANIZATION.TICKETID(+)
	AND TM_TICKET_ORGANIZATION.ORGANIZATIONID = SL_ORGANIZATION.ORGANIZATIONID
	AND SL_ORGANIZATION.ORGANIZATIONID = SL_CONTRACT.ORGANIZATIONID(+)
	AND SL_CONTRACT.SHIPPINGADDRESSID = SL_ADDRESS.ADDRESSID(+)
	AND SL_ORGANIZATION.CONTACTPERSONID = SL_PERSON.PERSONID(+);

CREATE OR REPLACE FORCE VIEW SL_DebtorPostings ( AccountID, AccountNumber, ClientID, PostingTime, ValueDate, Amount, PostingText, ReceiptNumber, PostingCodeNumber, PostingCodeText ) 
AS 
	SELECT  
		dm_debtor.debtorid AS accountid,
		dm_debtor.debtorno AS accountnumber,
		dm_debtor.clientid,
		pp_accountentry.bookdatetime AS postingtime, 
		pp_accountentry.valuedatetime AS valuedate, 
		CAST (nvl(pp_accountentry.amount, 0) AS NUMBER (9, 0)) AS amount,
		pp_accountentry.postingtext as postingtext, 
		lpad(to_char(pp_accountentryrange.entryrangeno), 1, '0') || '-' || lpad(to_char(pp_accountentry.entryno), 10, '0') AS receiptnumber, 
		pp_accountpostingkey.entrytypeno AS postingcodenumer,
		pp_accountpostingkey.entrytypename AS postingcodetext
	FROM 
		pp_accountentry, dm_debtor, pp_accountpostingkey, pp_accountentryrange
	WHERE  dm_debtor.debtorid = pp_accountentry.contractid 
		AND pp_accountentry.entrytypeno = pp_accountpostingkey.entrytypeno 
		AND pp_accountpostingkey.entryrangeid = pp_accountentryrange.entryrangeid 
	ORDER BY dm_debtor.debtorno, pp_accountentry.valuedatetime, pp_accountentry.bookdatetime DESC;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

insert into sl_claim(claimid, resourceid, uri, description, lastuser)
values (sl_claim_seq.nextval, 'CUSTOMERCARDMANAGEMENT', 'http://variosl.init-ka.de/2012/07/Contracts/IProductService/GetPickupLocations', 'Get Organizations assigned to Tickets.', 'INIT\SYS');

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off