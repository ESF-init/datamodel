SPOOL dbsl_2_022.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
	VALUES (sysdate, '2.022', 'DST', 'Added Invoice to payment providers');

INSERT INTO SL_PAYMENTPROVIDER
(
  ENUMERATIONVALUE,
  LITERAL,
  DESCRIPTION
)
VALUES
(
  5,
  N'Invoice',
  N'Invoice'
);

COMMIT;

PROMPT Done!
SPOOL off
