SPOOL dbsl_2_102.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.102', 'BAW', 'Added field for accounting processes.');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE ACC_REVENUERECOGNITION ADD (
  OperatorID NUMBER(9)
);

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW ACC_RECOGNIZEDTAPCANCELLATION
AS 
SELECT 
  RR.AMOUNT,
  RR.BASEFARE,
  RR.CREDITACCOUNTNUMBER,
  RR.CUSTOMERGROUP,
  RR.DEBITACCOUNTNUMBER,
  RR.LINEGROUPID,
  RR.POSTINGDATE ,
  RR.POSTINGREFERENCE,
  RR.PREMIUM,
  RR.REVENUERECOGNITIONID,
  RR.REVENUESETTLEMENTID,
  RR.TICKETNUMBER,
  RR.OPERATORID,
  CP.CLOSEOUTPERIODID,
  CP.PERIODFROM,
  CP.PERIODTO,
  CP.CLOSEOUTTYPE,
  CP.STATE,
  TJ1.CANCELLATIONREFERENCE,
  TJ1.CANCELLATIONREFERENCEGUID,
  TJ1.CLIENTID,
  TJ1.DEVICETIME,
  TJ1.FAREAMOUNT,
  TJ1.LINE,
  TJ1.OPERATORID AS TRANSACTIONOPERATORID,
  TJ1.PRODUCTID,
  TJ1.PURSEBALANCE,
  TJ1.PURSECREDIT,
  TJ1.RESULTTYPE,
  TJ1.SALESCHANNELID,
  TJ1.TARIFFDATE,
  TJ1.TARIFFVERSION,
  TJ1.TICKETID,
  TJ1.TICKETINTERNALNUMBER,
  TJ1.TRANSACTIONJOURNALID,
  TJ1.TRANSACTIONTYPE,
  TJ1.TRIPTICKETINTERNALNUMBER
FROM ((SL_TRANSACTIONJOURNAL TJ1 INNER JOIN ACC_REVENUERECOGNITION RR ON TJ1.TRANSACTIONJOURNALID = RR.TRANSACTIONJOURNALID)
  INNER JOIN ACC_CLOSEOUTPERIOD CP ON CP.CLOSEOUTPERIODID = RR.CLOSEOUTPERIODID)
    LEFT JOIN SL_TRANSACTIONJOURNAL TJ2 ON TJ2.TRANSACTIONJOURNALID = TJ1.CANCELLATIONREFERENCE
WHERE TJ1.TRANSACTIONTYPE = 128 AND (TJ2.TRANSACTIONTYPE = 66 OR TJ2.TRANSACTIONTYPE = 84);

CREATE OR REPLACE FORCE VIEW ACC_RECOGNIZEDTAP
AS 
SELECT 
  RR.AMOUNT,
  RR.BASEFARE,
  RR.CREDITACCOUNTNUMBER,
  RR.CUSTOMERGROUP,
  RR.DEBITACCOUNTNUMBER,
  RR.LINEGROUPID,
  RR.POSTINGDATE,
  RR.POSTINGREFERENCE,
  RR.PREMIUM,
  RR.REVENUERECOGNITIONID,
  RR.REVENUESETTLEMENTID,
  RR.TICKETNUMBER,
  RR.OPERATORID,
  CP.CLOSEOUTPERIODID,
  CP.PERIODFROM,
  CP.PERIODTO,
  CP.CLOSEOUTTYPE,
  CP.STATE,
  TJ1.CANCELLATIONREFERENCE,
  TJ1.CANCELLATIONREFERENCEGUID,
  TJ1.CLIENTID,
  TJ1.DEVICETIME,
  TJ1.FAREAMOUNT,
  TJ1.LINE,
  TJ1.OPERATORID AS TRANSACTIONOPERATORID,
  TJ1.PRODUCTID,
  TJ1.PURSEBALANCE,
  TJ1.PURSECREDIT,
  TJ1.RESULTTYPE,
  TJ1.SALESCHANNELID,
  TJ1.TARIFFDATE,
  TJ1.TARIFFVERSION,
  TJ1.TICKETID,
  TJ1.TICKETINTERNALNUMBER,
  TJ1.TRANSACTIONJOURNALID,
  TJ1.TRANSACTIONTYPE,
  TJ1.TRIPTICKETINTERNALNUMBER 
FROM ((SL_TRANSACTIONJOURNAL TJ1 INNER JOIN ACC_REVENUERECOGNITION RR ON TJ1.TRANSACTIONJOURNALID = RR.TRANSACTIONJOURNALID)
  INNER JOIN ACC_CLOSEOUTPERIOD CP ON CP.CLOSEOUTPERIODID = RR.CLOSEOUTPERIODID)
WHERE TJ1.TRANSACTIONTYPE = 66 OR TJ1.TRANSACTIONTYPE = 84;

CREATE OR REPLACE FORCE VIEW ACC_RECOGNIZEDLOADANDUSE
AS 
SELECT 
  RR.AMOUNT,
  RR.BASEFARE,
  RR.CREDITACCOUNTNUMBER,
  RR.CUSTOMERGROUP,
  RR.DEBITACCOUNTNUMBER,
  RR.LINEGROUPID,
  RR.POSTINGDATE,
  RR.POSTINGREFERENCE,
  RR.PREMIUM,
  RR.REVENUERECOGNITIONID,
  RR.REVENUESETTLEMENTID,
  RR.TICKETNUMBER,
  RR.OPERATORID,
  CP.CLOSEOUTPERIODID,
  CP.PERIODFROM,
  CP.PERIODTO,
  CP.CLOSEOUTTYPE,
  CP.STATE,
  TJ1.CANCELLATIONREFERENCE,
  TJ1.CANCELLATIONREFERENCEGUID,
  TJ1.CLIENTID,
  TJ1.DEVICETIME,
  TJ1.FAREAMOUNT,
  TJ1.LINE,
  TJ1.OPERATORID AS TRANSACTIONOPERATORID,
  TJ1.PRODUCTID,
  TJ1.PURSEBALANCE,
  TJ1.PURSECREDIT,
  TJ1.RESULTTYPE,
  TJ1.SALESCHANNELID,
  TJ1.TARIFFDATE,
  TJ1.TARIFFVERSION,
  TJ1.TICKETID,
  TJ1.TICKETINTERNALNUMBER,
  TJ1.TRANSACTIONJOURNALID,
  TJ1.TRANSACTIONTYPE,
  TJ1.TRIPTICKETINTERNALNUMBER 
FROM (SL_TRANSACTIONJOURNAL TJ1 INNER JOIN ACC_REVENUERECOGNITION RR ON TJ1.TRANSACTIONJOURNALID = RR.TRANSACTIONJOURNALID)
  INNER JOIN ACC_CLOSEOUTPERIOD CP ON CP.CLOSEOUTPERIODID = RR.CLOSEOUTPERIODID
WHERE TJ1.TRANSACTIONTYPE = 108 OR TJ1.TRANSACTIONTYPE = 85;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

Commit;
