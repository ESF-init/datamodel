SPOOL dbsl_2_144.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.144', 'JED', 'Added Vouchers.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE SL_Voucher (
	VoucherID NUMBER(18, 0) NOT NULL,
	VoucherCode NVARCHAR2(25) NOT NULL,
	CreatedOn DATE NOT NULL,
	CreatedBy NVARCHAR2(50) NOT NULL,
	
	TicketID NUMBER(18, 0) NOT NULL,
	TicketValidFrom DATE NULL, 
	TicketValidTo DATE NULL,
	
	RedeemableFrom DATE NOT NULL,
	RedeemableTo DATE NOT NULL,
	RedeemedOn DATE NULL,
	RedeemedBy NVARCHAR2(50) NULL,
	RedeemedForCardID NUMBER(18, 0) NULL,
	
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL,
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_Voucher PRIMARY KEY (VoucherID),
	CONSTRAINT UC_VoucherCode_01 UNIQUE (VoucherCode),
	CONSTRAINT FK_Voucher_Ticket FOREIGN KEY (TicketID) REFERENCES TM_Ticket(TicketID),
	CONSTRAINT FK_Voucher_Card FOREIGN KEY (RedeemedForCardID) REFERENCES SL_Card(CardID)
);

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_Voucher');
   sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/
-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_Voucher');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/
-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
