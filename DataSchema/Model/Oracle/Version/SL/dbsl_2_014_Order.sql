SPOOL dbsl_2_014.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.014', 'MIW', 'Added IsMail and VoucherNumber to Order');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_ORDER ADD (ISMAIL  NUMBER(1) DEFAULT 1 NOT NULL);
ALTER TABLE SL_ORDER ADD (VOUCHERNUMBER  NVARCHAR2(50));

COMMIT;

PROMPT Done!
SPOOL off