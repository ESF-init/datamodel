SPOOL dbsl_2_024.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
	VALUES (sysdate, '2.024', 'FMT', 'Added OrganizationID to SL_Product');

ALTER TABLE SL_PRODUCT
 ADD (ORGANIZATIONID NUMBER(18));

COMMIT;

PROMPT Done!
SPOOL off
