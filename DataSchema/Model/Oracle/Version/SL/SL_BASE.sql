/*
    Adds SL_BASE to USER_RESOURCE, when not alredy there.
    Adds SL_BASE to each user_group, which has the right CSW_EXECUTE.
*/

--set serveroutput on size 1000000;
set serveroutput on
SPOOL SL_BASE.log

DECLARE
    resourceToAdd CONSTANT NVARCHAR2(512) := 'SL_BASE';
    resourcesRequired CONSTANT NVARCHAR2(512) := 'CSW_EXECUTE';

    numOfExisting NUMBER;
    ugr_UserGroupID USER_GROUP.USERGROUPID%type;
    ugr_UserGroupName USER_GROUP.USERGROUPNAME%type;
    ugr_UserGroupClient USER_GROUP.CLIENTID%type;
    
    CURSOR ugr IS 
        select G.USERGROUPID, G.USERGROUPNAME, G.CLIENTID
        from user_groupright gr, user_group g
        where GR.USERGROUPID = G.USERGROUPID
          and GR.RESOURCEID = resourcesRequired
          and GR.USERGROUPID in
            (select distinct usergroupid
             from user_groupright
             where resourceid != resourceToAdd)
        order by G.CLIENTID;
BEGIN
    select count(*) into numOfExisting from user_resource where resourceid = resourceToAdd;
    IF numOfExisting > 0 THEN
        dbms_output.put_line('Nothing to add. Resource ' || resourceToAdd ||' already in USER_RESOURCE.');
    ELSE 
        /* insert new right into USER_RESOURCE */
        insert into user_resource (RESOURCEID, LANGUAGEID, RESOURCEGROUP, RESOURCENAME, RESOURCEDESCR)
                           --values (resourceToAdd, 1, 'Central', 'Basic SL Services', 'Enumeration + Configuration Services');
						   values (resourceToAdd, 1, 'Zentrale', 'SL Basisdienste', 'Konfiguration und Enumerationen.');
        dbms_output.put_line('Added ' || resourceToAdd || ' into USER_RESOURCE.');
                           
        /* assign new right to all user groups, who have the required user right */
        OPEN ugr;
        LOOP
            FETCH ugr into ugr_UserGroupID, ugr_UserGroupName, ugr_UserGroupClient;
            EXIT WHEN ugr%notFound;
    
            insert into user_groupright (USERGROUPRIGHTID, USERGROUPID, RESOURCEID, LEVELOFRIGHT)
                                 values (GetDataId(), ugr_UserGroupID, resourceToAdd, 6);
            dbms_output.put_line('Added ' || resourceToAdd || ' to user group ' || ugr_UserGroupName || ' (Client ' || ugr_UserGroupClient || ')');
        END LOOP;
        CLOSE ugr;
    END IF;
END;
/

COMMIT;
SPOOL off;