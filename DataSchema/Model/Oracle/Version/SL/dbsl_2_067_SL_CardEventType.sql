SPOOL dbsl_2_067.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
  VALUES (sysdate, '2.067', 'AMA', 'New card event.');

INSERT INTO SL_CARDEVENTTYPE
(
  ENUMERATIONVALUE,
  LITERAL,
  DESCRIPTION
)
VALUES
(
  14,
  N'Autoload',
  N'Autoload'
);

COMMIT;

PROMPT Done!
SPOOL off
