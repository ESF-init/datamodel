SPOOL dbsl_2_103.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '2.103', 'BAW', 'New payment states.');

Insert into SL_PAYMENTSTATE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) values (5,'Voided', 'Voided');
Insert into SL_PAYMENTSTATE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) values (6,'Refunded', 'Refunded');

COMMIT;
