SPOOL dbsl_2_132.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.132', 'MIW', 'Fill gap');

-- someone made a mistake and the script number 1.132 was lost

COMMIT;

PROMPT Done!
SPOOL off
