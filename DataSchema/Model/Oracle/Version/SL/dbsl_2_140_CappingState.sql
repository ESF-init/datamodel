SPOOL dbsl_2_140.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.140', 'DST', 'SL_CappingJournal: CappingState');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

ALTER TABLE SL_CappingJournal
ADD (State NUMBER(9) DEFAULT 0 NOT NULL);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_CAPPINGSTATE
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_CAPPINGSTATE PRIMARY KEY (ENUMERATIONVALUE)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
	INTO SL_CAPPINGSTATE (EnumerationValue, Literal, Description) VALUES (0, 'Unknown', 'Capping state unknown')
	INTO SL_CAPPINGSTATE (EnumerationValue, Literal, Description) VALUES (1, 'NotApplied', 'The capping was filled, but not used')
	INTO SL_CAPPINGSTATE (EnumerationValue, Literal, Description) VALUES (2, 'PartiallyApplied', 'The capping reduced the price and got filled')
	INTO SL_CAPPINGSTATE (EnumerationValue, Literal, Description) VALUES (3, 'Applied', 'The capping was used (full before the transaction)')
	INTO SL_CAPPINGSTATE (EnumerationValue, Literal, Description) VALUES (4, 'OutOfScope', 'The capping was not used in this transaction')
SELECT * FROM DUAL; 

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
