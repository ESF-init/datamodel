SPOOL dbsl_2_178.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '2.178', 'DST', 'Accounting view added column.');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW ACC_RECOGNIZEDPAYMENT
AS 
  SELECT 
    SL_SALE.SALESCHANNELID, 
    SL_SALE.MERCHANTNUMBER,
    SL_SALE.SALETYPE,
    SL_SALE.ISORGANIZATIONAL,
    SL_SALE.CLIENTID,
    SL_PAYMENTJOURNAL.PAYMENTTYPE, 
    ACC_PAYMENTRECOGNITION.*
  FROM SL_SALE, SL_PAYMENTJOURNAL, ACC_PAYMENTRECOGNITION
  WHERE SL_SALE.SALEID = SL_PAYMENTJOURNAL.SALEID
    AND SL_PAYMENTJOURNAL.PAYMENTJOURNALID = ACC_PAYMENTRECOGNITION.PAYMENTJOURNALID
  ORDER BY SL_SALE.SALESCHANNELID, SL_SALE.MERCHANTNUMBER, SL_PAYMENTJOURNAL.PAYMENTTYPE;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off;
