SPOOL db_3_203.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.203', 'LWU', 'Added function GetTypeOfUnit.');


-- =======[ Changes to Existing Tables ]===========================================================================

CREATE OR REPLACE FUNCTION GetTypeOfUnit
(
 pUNITID IN NUMBER
)
RETURN VARCHAR IS
tmpVar VARCHAR(100);
no_data_found exception;
  BEGIN
   SELECT Description INTO tmpVar FROM UM_TYPEOFUNIT
   WHERE TYPEOFUNITID = (SELECT TYPEOFUNITID from UM_UNIT where UNITID = pUNITID);
   return tmpVar;
    EXCEPTION
     WHEN NO_DATA_FOUND THEN
       NULL;
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END GetTypeOfUnit;
/


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;