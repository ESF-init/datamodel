SPOOL db_3_298.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.298', 'BVI', 'New column RelationType on SL_ProductRelation.');

alter table sl_productrelation 
add RelationType Number(9) default 0;

COMMENT ON COLUMN sl_productrelation.RelationType IS 'Defines the tpye of the relation to a product (Default 0, Alternative 1, ...)';

COMMIT;

PROMPT Done!

SPOOL OFF;
