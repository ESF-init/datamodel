SPOOL db_3_255.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.255', 'MEA', 'IssuerId to PP_CARDACTIONATTRIBUTE Added.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE PP_CARDACTIONATTRIBUTE
ADD IssuerId NUMBER (18);

-- =======[ Commit ]===============================================================================================

COMMENT ON COLUMN PP_CARDACTIONATTRIBUTE.IssuerId IS 'Operator that issued that card and whom the card belongs to.';
COMMIT;

PROMPT Done!

SPOOL OFF;