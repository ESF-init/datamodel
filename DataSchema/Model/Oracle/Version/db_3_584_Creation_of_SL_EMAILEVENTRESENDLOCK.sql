SPOOL db_3_584.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.584', 'SOE', 'Creation of SL_EMAILEVENTRESENDLOCK');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_EMAILEVENTRESENDLOCK
(
    EMAILEVENTRESENDLOCKID   NUMBER(18,0)    NOT NULL,
    EVENTTYPE               NUMBER(9)    NOT NULL,
    NOTIFICATIONDATE        DATE    NOT NULL,
    MESSAGETYPE             NUMBER(9)    NOT NULL,
    CARDID                  NUMBER(18,0),
    PERSONID                NUMBER(18,0),
    PRODUCTID               NUMBER(18,0),
    AUTOLOADSETTINGID       NUMBER(18,0)
);

-- =======[ Changes on existing tables ]===========================================================================
ALTER TABLE SL_EMAILEVENTRESENDLOCK ADD CONSTRAINT PK_EMAILEVENTRESENDLOCK PRIMARY KEY (EMAILEVENTRESENDLOCKID);
ALTER TABLE SL_EMAILEVENTRESENDLOCK ADD CONSTRAINT FK_EMAILEVENTRESENDLK_CARD FOREIGN KEY (CARDID) REFERENCES SL_CARD (CARDID);
ALTER TABLE SL_EMAILEVENTRESENDLOCK ADD CONSTRAINT FK_EMAILEVENTRESENDLK_PERSON FOREIGN KEY (PERSONID) REFERENCES SL_PERSON (PERSONID);
ALTER TABLE SL_EMAILEVENTRESENDLOCK ADD CONSTRAINT FK_EMAILEVENTRESENDLK_PRODUCT FOREIGN KEY (PRODUCTID) REFERENCES SL_PRODUCT (PRODUCTID);
ALTER TABLE SL_EMAILEVENTRESENDLOCK ADD CONSTRAINT FK_EMAILEVENTRESENDLK_AUTOLOAD FOREIGN KEY (AUTOLOADSETTINGID) REFERENCES SL_AUTOLOADSETTING (AUTOLOADSETTINGID);


COMMENT ON TABLE SL_EMAILEVENTRESENDLOCK is 'Table tracks when a pass/cardholder/card/autoload got an event notification. This acts as a lock for the windows service which sends the notifications. A resend is only possible if the entry in this table for a specific event and foreign key is cleared.';
COMMENT ON COLUMN SL_EMAILEVENTRESENDLOCK.EMAILEVENTRESENDLOCKID is 'Unique identifier of the resend lock';
COMMENT ON COLUMN SL_EMAILEVENTRESENDLOCK.EVENTTYPE IS 'The type of notification';
COMMENT ON COLUMN SL_EMAILEVENTRESENDLOCK.NOTIFICATIONDATE is 'Date when the pass/cardholder/card/autoload has been notified.';
COMMENT ON COLUMN SL_EMAILEVENTRESENDLOCK.MESSAGETYPE is 'The type of the message. Email, Text, or Push';
COMMENT ON COLUMN SL_EMAILEVENTRESENDLOCK.CARDID is 'References SL_CARD.CARDID.';
COMMENT ON COLUMN SL_EMAILEVENTRESENDLOCK.PERSONID is 'References SL_PERSON.PERSONID.';
COMMENT ON COLUMN SL_EMAILEVENTRESENDLOCK.PRODUCTID is 'References SL_PRODUCT.PRODUCTID.';
COMMENT ON COLUMN SL_EMAILEVENTRESENDLOCK.AUTOLOADSETTINGID is 'References SL_AUTOLOADSETTING.AUTOLOADSETTINGID.';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
     'SL_EMAILEVENTRESENDLOCK');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
