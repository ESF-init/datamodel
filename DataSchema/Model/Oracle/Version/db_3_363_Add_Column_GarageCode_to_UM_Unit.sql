SPOOL db_3_363.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.363', 'TIR', 'Added Column GarageCode');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE UM_UNIT
ADD GARAGECODE VARCHAR2(100 BYTE);

COMMENT ON COLUMN UM_UNIT.GARAGECODE IS 'The garagecode information from mobileplan for this unit.'; 

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
