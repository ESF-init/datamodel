SPOOL db_3_406.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.406', 'OMA', 'Update to TransactionJourneyHistory, cancellations included');

-- Start adding schema changes here
-- =======[ Views ]================================================================================================
/* Formatted on 09/12/2019 16:07:28 (QP5 v5.300) */
CREATE OR REPLACE FORCE VIEW SL_TRANSACTIONJOURNEYHISTORY
(
    TRANSACTIONID,
    CARDID,
    TRIPDATETIME,
    TYPEID,
    TYPENAME,
    TICKETNUMBER,
    TICKETNAME,
    FROMSTOPNUMBER,
    FROMSTOPNAME,
    TOSTOPNUMBER,
    TOSTOPNAME,
    PRICE,
    CARDBALANCE,
    CARDTRANSACTIONNO,
    CARDREFTRANSACTIONNO,
    TRIPSERIALNO,
    JOURNEYLEGCOUNTER
)
AS
        SELECT DISTINCT
           tr.TRANSACTIONID,
           tr.CARDID,
           tr.TRIPDATETIME,
           tr.TYPEID,
           CASE WHEN tr.TYPEID = 501 THEN 'Journey' ELSE TYPENAME END AS TypeName,
           TikNo,
           CASE WHEN tik.PrintText != '' THEN tik.PrintText 
            WHEN tik.Description != '' THEN tik.Description 
            ELSE tik.Name END as "Name",
           fromstopno,
           fromStop.stoplongname,
           toStopNo,
           toStopName,
           toStop.Price + toStop.PriceCorrection as Price,
           toStop.CardBalance AS CardBalance,
           tr.cardtransactionno,
           tr.cardreftransactionno,
           tr.tripserialno,
           tr.journeylegcounter
      FROM RM_TRANSACTION  tr
           JOIN rm_devicebookingstate
               ON tr.devicebookingstate =
                      rm_devicebookingstate.devicebookingno
           JOIN rm_shift ON tr.shiftid = rm_shift.shiftid
           JOIN rm_transactiontype tt ON tr.typeid = tt.typeid
           JOIN tm_ticket tik
               ON tr.TARIFFID = tik.tarifid AND tr.tikno = tik.internalnumber
           JOIN tm_tarif ON tik.TARIFID = tm_tarif.TARIFID
           JOIN vario_net ON tm_tarif.NETID = vario_net.NETID
           LEFT JOIN vario_stop fromStop
               ON     vario_net.netid = fromStop.netid
                  AND tr.fromstopno = fromStop.stopno
           LEFT JOIN
           (SELECT TRANSACTIONID,
                   trans.CARDID,
                   TRIPDATETIME,
                   TYPEID,
                   FROMSTOPNO             AS toStopNo,
                   varioStop.stoplongname AS toStopName,
                   trans.Price,
                   trans.PriceCorrection,
                   trans.CardBalance,
                   trans.cardtransactionno,
                   trans.cardreftransactionno,
                   trans.tripserialno,
                   trans.journeylegcounter
              FROM RM_TRANSACTION  trans
                   INNER JOIN
                   (  SELECT CARDID,
                             cardreftransactionno,
                             MAX (cardtransactionno)
                                 AS cardtransactionno
                        FROM RM_TRANSACTION
                       WHERE cardreftransactionno > 0
                    GROUP BY CARDID, cardreftransactionno) maxT
                       ON     trans.cardid = maxT.cardid
                          AND trans.cardreftransactionno =
                                  maxT.cardreftransactionno
                          AND trans.cardtransactionno =
                                  maxT.cardtransactionno
                   JOIN tm_ticket tik
                       ON     trans.TARIFFID = tik.tarifid
                          AND trans.tikno = tik.internalnumber
                   JOIN tm_tarif ON tik.TARIFID = tm_tarif.TARIFID
                   JOIN vario_net ON tm_tarif.NETID = vario_net.NETID
                   LEFT JOIN vario_stop varioStop
                       ON     vario_net.netid = varioStop.netid
                          AND trans.fromstopno = varioStop.stopno) toStop
               ON     tr.cardid = toStop.cardid
                  AND tr.cardtransactionno = toStop.cardreftransactionno
     WHERE     rm_shift.shiftstateid < 30
           AND rm_devicebookingstate.isbooked = 1
           AND tr.typeid NOT IN (5, 15, 502);

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
