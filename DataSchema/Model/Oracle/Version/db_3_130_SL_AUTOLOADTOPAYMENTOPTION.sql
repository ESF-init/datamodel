SPOOL db_3_130.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.130', 'EPA', 'SL_AUTOLOADTOPAYMENTOPTION');

-- Start adding schema changes here


-- =======[ New Tables ]===========================================================================================
CREATE TABLE SL_AUTOLOADTOPAYMENTOPTION
(
	AUTOLOADTOPAYMENTOPTIONID		NUMBER(18,0) NOT NULL,
	AUTOLOADSETTINGID				NUMBER(18,0) NOT NULL,
	PAYMENTOPTIONID					NUMBER(18,0) NOT NULL,
    BACKUPPAYMENTOPTIONID            NUMBER(18,0),
    AMOUNT                            NUMBER(18,0) NOT NULL,
    LastUser                         NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
    LastModified                     DATE DEFAULT sysdate NOT NULL, 
    TransactionCounter                 INTEGER NOT NULL,
    CONSTRAINT PK_AUTOLOADTOPAMENTOPTIONID PRIMARY KEY (AUTOLOADTOPAYMENTOPTIONID),
    CONSTRAINT FK_AUTOLOADSETTINGID FOREIGN KEY (AUTOLOADSETTINGID) REFERENCES SL_AUTOLOADSETTING(AUTOLOADSETTINGID),
    CONSTRAINT FK_PAYMENTOPTIONID FOREIGN KEY (PAYMENTOPTIONID) REFERENCES SL_PAYMENTOPTION(PAYMENTOPTIONID),
    CONSTRAINT FK_BACKUPPAYMENTOPTIONID FOREIGN KEY (BACKUPPAYMENTOPTIONID) REFERENCES SL_PAYMENTOPTION(PAYMENTOPTIONID)
);

COMMENT ON TABLE SL_AUTOLOADTOPAYMENTOPTION is 'M to N relationship between autoloads and paymentoptions. Autoloads can have more than one funding source.';
COMMENT ON COLUMN SL_AUTOLOADTOPAYMENTOPTION.AUTOLOADTOPAYMENTOPTIONID is 'Primary key.';
COMMENT ON COLUMN SL_AUTOLOADTOPAYMENTOPTION.AUTOLOADSETTINGID is 'Mandatory foreign key to autoload entry.';
COMMENT ON COLUMN SL_AUTOLOADTOPAYMENTOPTION.PAYMENTOPTIONID is 'Mandatory foreign key to paymentoption.';
COMMENT ON COLUMN SL_AUTOLOADTOPAYMENTOPTION.BACKUPPAYMENTOPTIONID is 'Optional foreign key to a backup paymentoption if first paymentoption fails.';
COMMENT ON COLUMN SL_AUTOLOADTOPAYMENTOPTION.AMOUNT is '(Partial) amount of the whole autoload setting amount that will be paid by the given payment option.';
COMMENT ON COLUMN SL_AUTOLOADTOPAYMENTOPTION.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_AUTOLOADTOPAYMENTOPTION.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_AUTOLOADTOPAYMENTOPTION.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_AUTOLOADTOPAYMENTOPTION');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_AUTOLOADTOPAYMENTOPTION');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
