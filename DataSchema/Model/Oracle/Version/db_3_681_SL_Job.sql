SPOOL db_3_681.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.681', 'DST', 'Add HasInputFile and HasOutputFile to SL_Job');

ALTER TABLE SL_JOB ADD HASINPUTFILE NUMBER(1) DEFAULT 0 NOT NULL;
ALTER TABLE SL_JOB ADD HASOUTPUTFILE NUMBER(1) DEFAULT 0 NOT NULL;

COMMENT ON COLUMN SL_JOB.HASINPUTFILE IS 'Defines if the job has an input file';
COMMENT ON COLUMN SL_JOB.HASOUTPUTFILE IS 'Defines if the job has an output file';

COMMIT;

PROMPT Done!

SPOOL OFF;
