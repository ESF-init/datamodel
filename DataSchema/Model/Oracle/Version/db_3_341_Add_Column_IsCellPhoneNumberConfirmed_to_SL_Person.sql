SPOOL db_3_341.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.341', 'ARH', 'Add Column ISCELLPHONENUMBERCONFIRMED to SL_PERSON');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PERSON ADD ISCELLPHONENUMBERCONFIRMED 	NUMBER(1)         DEFAULT 0                     NOT NULL;

COMMENT ON COLUMN SL_PERSON.ISCELLPHONENUMBERCONFIRMED  IS 'Flag to set if the person cell phone number has been confirmed';

COMMIT;

PROMPT Done!

SPOOL OFF;
