SPOOL db_3_161.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.161', 'STV', 'Add Column External Note to FareEvasionIncident');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_FAREEVASIONINCIDENT
ADD ( EXTERNALNOTE  NVARCHAR2(512) );

COMMENT ON COLUMN SL_FAREEVASIONINCIDENT.EXTERNALNOTE IS 'Field for external note to appear on certain forms';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
