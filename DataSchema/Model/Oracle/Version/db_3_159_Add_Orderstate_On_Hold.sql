SPOOL db_3_159.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.159', 'EPA', 'Add order state on hold.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
Insert into SL_ORDERSTATE
   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values
   (7, 'On Hold', 'On Hold');

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
