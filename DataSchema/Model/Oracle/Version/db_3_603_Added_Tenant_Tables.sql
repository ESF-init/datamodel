SPOOL db_3_603.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.603', 'HNI', 'New Tenant Tables');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================
CREATE TABLE SL_TenantState (
	EnumerationValue NUMBER(9) NOT NULL,
	Literal NVARCHAR2(50) NOT NULL, 
	Description NVARCHAR2(50) NOT NULL, 
	CONSTRAINT PK_TenantState PRIMARY KEY (EnumerationValue)
);

COMMENT ON TABLE SL_TenantState is 'State of the Tenant';

COMMENT ON COLUMN SL_TenantState.EnumerationValue IS 'Unique ID of the Tenant state';
COMMENT ON COLUMN SL_TenantState.Literal IS 'Name of the Tenant state; used internally.';
COMMENT ON COLUMN SL_TenantState.Description IS 'Description of the Tenant state. This is used in the UI to diplay the value.';

CREATE TABLE SL_Tenant (
	TenantID NUMBER(18) NOT NULL,
	TenantName NVARCHAR2(50),
	State NUMBER(9) NOT NULL,
	CONSTRAINT PK_Tenant PRIMARY KEY (TenantID)
);

COMMENT ON TABLE SL_Tenant is 'A Tenant created by the OnBoarding platform';

COMMENT ON COLUMN SL_Tenant.TenantID IS 'Unique identifier of the Tenant';
COMMENT ON COLUMN SL_Tenant.TenantName IS 'Name of the Tenant';
COMMENT ON COLUMN SL_Tenant.State IS 'References SL_TenantState';

CREATE TABLE SL_TenantPerson(
TenantPersonID NUMBER(18) NOT NULL,
TenantID NUMBER(18),
UserName NVARCHAR2(50) NOT NULL,
Password NVARCHAR2(50) NOT NULL,
FirstName NVARCHAR2(50),
LastName NVARCHAR2(50),
CellPhoneNumber NVARCHAR2(50),
Email NVARCHAR2(50),
FaxNumber NVARCHAR2(50),
LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
LastModified DATE DEFAULT sysdate NOT NULL, 
TransactionCounter INTEGER NOT NULL,
CONSTRAINT PK_TenantPerson PRIMARY KEY (TenantPersonID),
CONSTRAINT FK_TenantPerson_Tenant FOREIGN KEY (TenantID) REFERENCES SL_TENANT(TenantID)
);

COMMENT ON TABLE SL_TenantPerson is 'Customer account for contact persons of a Tenant';

COMMENT ON COLUMN SL_TenantPerson.TenantPersonID IS 'Unique identifier of the customer account';

COMMENT ON COLUMN SL_TenantPerson.TenantID IS 'References SL_Tenant';
COMMENT ON COLUMN SL_TenantPerson.UserName IS 'Username of the account';
COMMENT ON COLUMN SL_TenantPerson.Password IS 'Hashed password of the account';
COMMENT ON COLUMN SL_TenantPerson.FirstName IS 'First name of the person';
COMMENT ON COLUMN SL_TenantPerson.LastName IS 'Last name of the person';
COMMENT ON COLUMN SL_TenantPerson.CellPhoneNumber IS 'Cellphone number of the person';
COMMENT ON COLUMN SL_TenantPerson.EMAIL IS 'Email of the person';
COMMENT ON COLUMN SL_TenantPerson.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_TenantPerson.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_TenantPerson.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';


CREATE TABLE SL_TenantHistory(
TenantHistoryID NUMBER(18) NOT NULL,
TenantID NUMBER(18) NOT NULL,
TenantPersonID  NUMBER(18),
Message NVARCHAR2(1000) NOT NULL, 
UserName NVARCHAR2(50) NOT NULL, 
LogCreationDate DATE DEFAULT sysdate NOT NULL, 
CONSTRAINT PK_TenantHistory PRIMARY KEY (TenantHistoryID),
CONSTRAINT FK_TenantHistory_Tenant FOREIGN KEY (TenantID) REFERENCES SL_Tenant(TenantID),
CONSTRAINT FK_TenantHistory_TenantPerson FOREIGN KEY (TenantPersonID) REFERENCES SL_TenantPerson(TenantPersonID)
);

COMMENT ON TABLE SL_TenantHistory is 'History of Tenant actions';

COMMENT ON COLUMN SL_TenantHistory.TenantHistoryID IS 'Unique identifier of a history entry';
COMMENT ON COLUMN SL_TenantHistory.TenantID IS 'References SL_Tenant';
COMMENT ON COLUMN SL_TenantHistory.TenantPersonID IS 'References SL_TenantPerson';
COMMENT ON COLUMN SL_TenantHistory.Message IS 'Message containing information about the action';
COMMENT ON COLUMN SL_TenantHistory.UserName IS 'User that created that history entry';
COMMENT ON COLUMN SL_TenantHistory.LogCreationDate IS 'Date time when the record was added';
-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;