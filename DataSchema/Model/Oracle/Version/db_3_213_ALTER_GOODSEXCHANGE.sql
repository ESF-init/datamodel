SPOOL db_3_213.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.213', 'LWU', 'Added SIGNATUREID to GM_GOODSEXCHANGE.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE GM_GOODSEXCHANGE
ADD SIGNATUREID NUMBER (10);

-- =======[ Commit ]===============================================================================================

COMMENT ON COLUMN GM_GOODSEXCHANGE.SIGNATUREID IS 'Reference to GM_EXCHANGESIGNATURES';
COMMIT;

PROMPT Done!

SPOOL OFF;