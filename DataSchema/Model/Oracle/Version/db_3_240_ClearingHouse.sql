SPOOL db_3_240.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.240', 'FRD', 'ClearingHouse extensions for new RITS ClearingHouse');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================


-- =======[ New Tables ]===========================================================================================


CREATE TABLE CH_TRANSACTIONADDITIONAL
(
  TRANSACTIONADDITIONALID	NUMBER(18,0)						NOT NULL,
  TRANSACTIONID				NUMBER(10)							NOT NULL,
  CLEARINGCLASSIFICATION	NUMBER(9)							NOT NULL,
  CORRECTIONDATE			DATE,
  CORRECTEDBY				NVARCHAR2(50),
  RESPONSIBLECLIENTID		NUMBER(10),
  LASTUSER					NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
  LASTMODIFIED				DATE			DEFAULT sysdate		NOT NULL,
  TRANSACTIONCOUNTER		INTEGER								NOT NULL,
  DATAROWCREATIONDATE		DATE			DEFAULT sysdate
);

COMMENT ON TABLE CH_TRANSACTIONADDITIONAL is 'The table collects additional clearing information for each transaction.';
COMMENT ON COLUMN CH_TRANSACTIONADDITIONAL.TRANSACTIONADDITIONALID IS 'Id of the additional transaction information.';
COMMENT ON COLUMN CH_TRANSACTIONADDITIONAL.TRANSACTIONID IS 'Id of the transaction for which additional information is saved (references RM_TRANSACTION).';
COMMENT ON COLUMN CH_TRANSACTIONADDITIONAL.CLEARINGCLASSIFICATION IS 'Current classification of the transaction (values taken from CH_CLEARINGCLASSIFICATION).';
COMMENT ON COLUMN CH_TRANSACTIONADDITIONAL.CORRECTIONDATE IS 'Date at which a former bad classified transaction was corrected for clearing.';
COMMENT ON COLUMN CH_TRANSACTIONADDITIONAL.CORRECTEDBY IS 'Owner who corrected a former bad classified transaction for clearing.';
COMMENT ON COLUMN CH_TRANSACTIONADDITIONAL.RESPONSIBLECLIENTID IS 'Id of the responsible client, derived at clearing time from the VARIO_RESPONSIBILITY table.';
COMMENT ON COLUMN CH_TRANSACTIONADDITIONAL.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN CH_TRANSACTIONADDITIONAL.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN CH_TRANSACTIONADDITIONAL.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN CH_TRANSACTIONADDITIONAL.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

ALTER TABLE CH_TRANSACTIONADDITIONAL ADD (CONSTRAINT PK_TRANSACTIONADDITIONAL PRIMARY KEY (TRANSACTIONADDITIONALID));
ALTER TABLE CH_TRANSACTIONADDITIONAL ADD (CONSTRAINT FK_TRANSADD_TRANSACTIONID FOREIGN KEY (TRANSACTIONID) REFERENCES RM_TRANSACTION(TRANSACTIONID));
ALTER TABLE CH_TRANSACTIONADDITIONAL ADD (CONSTRAINT UK_TRANSADD_TRANSACTIONID UNIQUE (TRANSACTIONID));



CREATE TABLE CH_CLEARINGTRANSACTION
(
  CLEARINGTRANSACTIONID		NUMBER(18,0)						NOT NULL,
  CLEARINGID				NUMBER(10)							NOT NULL,
  TRANSACTIONID				NUMBER(10)							NOT NULL,
  CLEARINGCLASSIFICATION	NUMBER(9)							NOT NULL,
  LASTUSER					NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
  LASTMODIFIED				DATE			DEFAULT sysdate		NOT NULL,
  TRANSACTIONCOUNTER		INTEGER								NOT NULL,
  DATAROWCREATIONDATE		DATE			DEFAULT sysdate
);

COMMENT ON TABLE CH_CLEARINGTRANSACTION is 'Correlation between clearings and transactions incorporated in the clearing.';
COMMENT ON COLUMN CH_CLEARINGTRANSACTION.CLEARINGTRANSACTIONID IS 'Id of the clearing transaction correlation.';
COMMENT ON COLUMN CH_CLEARINGTRANSACTION.CLEARINGID IS 'Id of the clearing which incorporated the transaction (references CH_CLEARING).';
COMMENT ON COLUMN CH_CLEARINGTRANSACTION.TRANSACTIONID IS 'Id of the transaction which is incorporated in the clearing (references RM_TRANSACTION).';
COMMENT ON COLUMN CH_CLEARINGTRANSACTION.CLEARINGCLASSIFICATION IS 'Classification of the transaction in the corresponding clearing (values taken from CH_CLEARINGCLASSIFICATION).';
COMMENT ON COLUMN CH_CLEARINGTRANSACTION.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN CH_CLEARINGTRANSACTION.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN CH_CLEARINGTRANSACTION.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN CH_CLEARINGTRANSACTION.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

ALTER TABLE CH_CLEARINGTRANSACTION ADD (CONSTRAINT PK_CLEARINGTRANSACTION PRIMARY KEY (CLEARINGTRANSACTIONID));
-- CH_Clearing.ClearingId has no primary key defined on, so it is not possible to add a foreign key.
-- ALTER TABLE CH_CLEARINGTRANSACTION ADD (CONSTRAINT FK_CLEARINGTRANS_CLEARINGID FOREIGN KEY (CLEARINGID) REFERENCES CH_CLEARING(CLEARINGID));
ALTER TABLE CH_CLEARINGTRANSACTION ADD (CONSTRAINT FK_CLEARINGTRANS_TRANSACTIONID FOREIGN KEY (TRANSACTIONID) REFERENCES RM_TRANSACTION(TRANSACTIONID));


----------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE CH_CLEARINGCLASSIFICATION
(
  ENUMERATIONVALUE  	NUMBER(9)                   		NOT NULL,
  LITERAL           	NVARCHAR2(50)               		NOT NULL,
  DESCRIPTION       	NVARCHAR2(50)               		NOT NULL,
  DATAROWCREATIONDATE	DATE			DEFAULT sysdate
);

COMMENT ON TABLE CH_CLEARINGCLASSIFICATION IS 'This table contains a list of clearing classification values.';
COMMENT ON COLUMN CH_CLEARINGCLASSIFICATION.ENUMERATIONVALUE IS 'Unique id of the clearing classification value.';
COMMENT ON COLUMN CH_CLEARINGCLASSIFICATION.LITERAL 		  IS 'Name of the clearing classification value. Used internally.';
COMMENT ON COLUMN CH_CLEARINGCLASSIFICATION.DESCRIPTION 	  IS 'Description of the clearing classification value. Used in the UI.';
COMMENT ON COLUMN CH_CLEARINGCLASSIFICATION.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

ALTER TABLE CH_CLEARINGCLASSIFICATION ADD (CONSTRAINT PK_CLEARINGCLASSIFICATION PRIMARY KEY (ENUMERATIONVALUE));


-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(5) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'CH_CLEARINGTRANSACTION','CH_TRANSACTIONADDITIONAL');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/


-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(5) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'CH_CLEARINGTRANSACTION','CH_TRANSACTIONADDITIONAL');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/


-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO CH_CLEARINGCLASSIFICATION (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (0, 'OK', 'OK');
INSERT INTO CH_CLEARINGCLASSIFICATION (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (1, 'Corrected', 'Corrected');
INSERT INTO CH_CLEARINGCLASSIFICATION (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (10, 'Aborted', 'Bad Aborted Shift');
INSERT INTO CH_CLEARINGCLASSIFICATION (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (11, 'Backup', 'Bad Backup Shift');
INSERT INTO CH_CLEARINGCLASSIFICATION (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (12, 'Training', 'Bad Training Shift');
INSERT INTO CH_CLEARINGCLASSIFICATION (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (20, 'Validation', 'Bad Validation Failed');
INSERT INTO CH_CLEARINGCLASSIFICATION (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (30, 'BookingState', 'Bad Incomplete Booking State');


-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
