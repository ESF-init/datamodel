INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.513', 'EMN', 'Priority enums.');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_Priority
(
  EnumerationValue  NUMBER(9)           NOT NULL,
  Literal           NVARCHAR2(50)       NOT NULL,
  Description       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_Priority PRIMARY KEY (EnumerationValue)
);

COMMENT ON TABLE SL_Priority IS 'Enumeration values for SL_Order.Priority.';
COMMENT ON COLUMN SL_Priority.EnumerationValue IS 'Type identifier.';
COMMENT ON COLUMN SL_Priority.Literal IS 'Type literal.';
COMMENT ON COLUMN SL_Priority.Description IS 'Type description.';

INSERT ALL
	INTO SL_Priority (EnumerationValue, Literal, Description) VALUES (0, 'Unknown', 'Unknown')
	INTO SL_Priority (EnumerationValue, Literal, Description) VALUES (10, 'Lowest', 'Lowest')
	INTO SL_Priority (EnumerationValue, Literal, Description) VALUES (20, 'Low', 'Low')
	INTO SL_Priority (EnumerationValue, Literal, Description) VALUES (30, 'Normal', 'Normal')
	INTO SL_Priority (EnumerationValue, Literal, Description) VALUES (40, 'High', 'High')
	INTO SL_Priority (EnumerationValue, Literal, Description) VALUES (50, 'Urgent', 'Urgent')
	INTO SL_Priority (EnumerationValue, Literal, Description) VALUES (60, 'Immediate', 'Immediate')
SELECT * FROM DUAL;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;