SPOOL db_3_173.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.173', 'EPA', 'Add status user list');


CREATE TABLE USER_STATUS
(
	EnumerationValue  NUMBER(9, 0)			NOT NULL,
	Literal           NVARCHAR2(50)		NOT NULL,
	Description       NVARCHAR2(50)		NOT NULL,
	CONSTRAINT PK_USER_STATUS PRIMARY KEY (EnumerationValue)
);

COMMENT ON TABLE USER_STATUS IS 'Enumeration of USER_LIST status.';
COMMENT ON COLUMN USER_STATUS.EnumerationValue IS 'Enumeration id.';
COMMENT ON COLUMN USER_STATUS.Literal IS 'Enumeration short name.';
COMMENT ON COLUMN USER_STATUS.Description IS 'Enumeration description..';

INSERT INTO USER_STATUS (EnumerationValue, Literal, Description)
VALUES (0, 'Active', 'User account is active.');
INSERT INTO USER_STATUS (EnumerationValue, Literal, Description)
VALUES (1, 'Closed', 'User account is closed.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE USER_LIST ADD (STATUS NUMBER(9) default 0 NOT NULL);

COMMENT ON COLUMN USER_LIST.STATUS IS 'Status of the user list entry, e.g. active or closed.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
