SPOOL db_3_684.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.684', 'DST', 'Add InternalNumber, VanpoolGroupId, Notes and Validity to VIEW_SL_SALEHISTORYDETAIL');

CREATE OR REPLACE VIEW VIEW_SL_SALEHISTORYDETAIL
as
SELECT       s.saleid,
             s.saletransactionid,
             s.receiptreference,
             s.saleschannelid,
             s.saletype,
             s.locationnumber,
             s.devicenumber,
             s.salespersonnumber,
             s.merchantnumber,
             s.networkreference,
             s.saledate,
             s.clientid operatorid,
             s.contractid accountid,
             s.secondarycontractid secondaryaccountid,
             s.externalordernumber,
             s.orderid,
             t.name ticketname,
             p.ticketid,
             t.internalnumber,
             CASE -- for topups, the value is in remainingvalue, not in price
               WHEN p.tickettype = 200 THEN p.remainingvalue
               ELSE p.price
             END price,
             o.state,
             o.ordernumber,
             p.productid,
             NVL(required_od.quantity - required_od.fulfilledquantity, 1) * (od.quantity - od.fulfilledquantity) quantity, -- use the quantity of the parent
             (od.quantity * nvl(required_od.fulfilledquantity, 1)) fulfilledquantity,
             od.orderdetailid,
             od.requiredorderdetailid parentorderdetailid,
             null vanpoolgroupid,
             null notes,
             p.validfrom,
             p.validto
      FROM sl_orderdetail od
        JOIN sl_sale s ON s.orderid = od.orderid
        JOIN sl_product p ON p.productid = od.productid
        JOIN tm_ticket t ON t.ticketid = p.ticketid
        JOIN sl_order o ON o.orderid = s.orderid
        LEFT JOIN sl_orderdetail required_od ON od.requiredorderdetailid = required_od.orderdetailid
      UNION ALL
      -- fulfilled orders / normal sales
      SELECT s.saleid,
             s.saletransactionid,
             s.receiptreference,
             s.saleschannelid,
             s.saletype,
             s.locationnumber,
             s.devicenumber,
             s.salespersonnumber,
             s.merchantnumber,
             s.networkreference,
             s.saledate,
             s.clientid operatorid,
             s.contractid accountid,
             s.secondarycontractid secondaryaccountid,
             s.externalordernumber,
             s.orderid,
             t.name ticketname,
             tj.ticketid,
             t.internalnumber,
             tj.fareamount,
             NULL orderstate,
             NULL ordernumber,
             NULL productid,
             count(*) quantity,
             count(*) fulfilledquantity,
             cast(null as number(18)) orderdetailid,
             cast(null as number(18)) parentorderdetailid,
             tj.VANPOOLGROUPID,
             tj.NOTES,
             tj.validfrom,
             tj.validto
      FROM sl_transactionjournal tj
        JOIN sl_sale s ON s.saleid = tj.saleid
        JOIN tm_ticket t ON t.ticketid = tj.ticketid
		WHERE t.tickettype <> 200 -- Filter out AddValueAccount transactions
        group by s.saleid,
             s.saletransactionid,
             s.receiptreference,
             s.saleschannelid,
             s.saletype,
             s.locationnumber,
             s.devicenumber,
             s.salespersonnumber,
             s.merchantnumber,
             s.networkreference,
             s.saledate,
             s.clientid,
             s.contractid,
             s.secondarycontractid,
             s.externalordernumber,
             s.orderid,
             t.name,
             tj.ticketid,
             t.internalnumber,
             tj.fareamount,
             tj.VANPOOLGROUPID,
             tj.NOTES,
             tj.validfrom,
             tj.validto;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
