SPOOL db_3_169.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.169', 'DST', 'New table SL_CardKey');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_CardKey
(
   CardKeyID                 NUMBER(18)        NOT NULL,
   CardKeyVersion            NUMBER(9)         NOT NULL,  
   SignatureKeyVersion       NUMBER(9)         NOT NULL,
   DataFileVersion           NUMBER(9)         NOT NULL,
   TransactionFileVersion    NUMBER(9)         NOT NULL,
   LASTUSER                  NVARCHAR2(50)     DEFAULT 'SYS' NOT NULL,
   LASTMODIFIED              DATE              DEFAULT sysdate NOT NULL,
   TRANSACTIONCOUNTER        NUMBER            NOT NULL
);

ALTER TABLE SL_CardKey ADD CONSTRAINT PK_CardKey PRIMARY KEY (CardKeyID);

COMMENT ON TABLE SL_CARDKEY IS 'Lists the version of key combinations used for a card';
COMMENT ON COLUMN SL_CARDKEY.CardKeyID IS 'Unique identifier of the card key combination';
COMMENT ON COLUMN SL_CARDKEY.CardKeyVersion IS 'Version of the master key used for writing to the card';
COMMENT ON COLUMN SL_CARDKEY.SignatureKeyVersion IS 'Version of the signature key used to create the signature';
COMMENT ON COLUMN SL_CARDKEY.DataFileVersion IS 'Format version for the data file on the card';
COMMENT ON COLUMN SL_CARDKEY.TransactionFileVersion IS 'Format version for the transaction file on the card';
COMMENT ON COLUMN SL_CARDKEY.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_CARDKEY.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_CARDKEY.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_Card ADD CardKeyID NUMBER(18) DEFAULT NULL;
COMMENT ON COLUMN SL_CARD.CardKeyID IS 'References SL_CardKey.CardKeyID';
ALTER TABLE SL_Card ADD CONSTRAINT FK_Card_CardKey FOREIGN KEY (CardKeyID) REFERENCES SL_CardKey (CardKeyID);

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_CardKey');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_CardKey');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
