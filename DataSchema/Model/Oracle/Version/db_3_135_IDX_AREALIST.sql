SPOOL db_3_135.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.135', 'flf', 'Added index to area list table');

-- =======[ Changes to Existing Tables ]===========================================================================

CREATE INDEX IDX_AREALISTKEY ON TM_AREALIST
(TARIFFID, AREALISTKEY);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
