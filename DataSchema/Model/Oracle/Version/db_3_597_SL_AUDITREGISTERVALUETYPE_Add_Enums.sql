SPOOL db_3_597.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.597', 'WHA', 'Additions to AuditRegisterValueType enum');

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (27, 'ApprovedPaymentsRelative', 'Count of approved payment requests since data was last uploaded')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (28, 'DeclinedPaymentsRelative', 'Count of declined payment requests since data was last uploaded')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (29, 'FailedPaymentsRelative', 'Count of failed payment requests since data was last uploaded')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (30, 'RequestedHotlistWhitelistUpdatesRelative', 'Count of requested hotlist/whitelist updates since data was last uploaded')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (31, 'FailedHotlistWhitelistUpdatesRelative', 'Count of failed hotlist/whitelist updates since data was last uploaded')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (32, 'SuccessfulHotlistWhitelistUpdatesRelative', 'Count of successful hotlist/whitelist updates since data was last uploaded')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (33, 'ACHTransactions', 'ACH (Automated Clearing House) transactions by amount')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (34, 'CheckTransactions', 'Check transactions by amount')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (35, 'ContactlessEMVTransactions', 'Contactless EMV (Europay Mastercard Visa; i.e. ApplePay, GooglePay) transactions by amount')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (36, 'ThirdPartyVoucherTransactions', '3rd party voucher transactions by amount')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (37, 'TotalCashTransactions', 'Total count of cash transactions')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (38, 'TotalCheckTransactions', 'Total count of check transactions')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (39, 'TotalContactlessEMVTransactions', 'Total count of contactless EMV (Europay Mastercard Visa; i.e. ApplePay, GooglePay) transactions')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (40, 'TotalThirdPartyVoucherTransactions', 'Total count of 3rd party voucher transactions')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (41, 'TotalCreditDebitCardTransactions', 'Total count of credit and debit card transactions')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (42, 'TotalACHTransactions', 'Total count of ACH (Automated Clearing House) transactions')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (43, 'TotalGoodwillTransactions', 'Total count of "goodwill" transactions')
	INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION)
	VALUES (44, 'GoodwillTransactions', 'Total amount of "goodwill" transactions')
SELECT * FROM DUAL;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
