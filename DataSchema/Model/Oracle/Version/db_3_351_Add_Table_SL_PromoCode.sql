SPOOL db_3_351.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.351', 'MMB', 'Add table SL_PROMOCODE');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_PROMOCODE
(
    PROMOCODEID				NUMBER(18)    		NOT NULL,
	TICKETID				NUMBER(18)			NOT NULL,
	PROMOCODENUMBER			VARCHAR2(512)	 	NOT NULL,
	BEGINDATE				DATE				NOT NULL,
	ENDDATE					DATE				NOT NULL,
	CAMPAIGNDESCRIPTION		VARCHAR2(512)		NULL,
	CUSTOMERID				VARCHAR2(512)	 	NOT NULL,
	STATUS					NUMBER(1)			NOT NULL,
	CREATIONDATE			DATE				NOT NULL,
	REDEMPTIONDATE			DATE				NOT NULL,
	QUANTITY				NUMBER(1)			NOT NULL,
	DISCOUNT				NUMBER(9)			NOT NULL,
	DISCOUNTTYPE			NUMBER(1)			NOT NULL,
	ORIGIN					VARCHAR2(512)		NULL,
	DESTINATION				VARCHAR2(512)		NULL,	
	LASTUSER            	VARCHAR2(50)   		DEFAULT 'SYS' NOT NULL,
    LASTMODIFIED        	DATE            	DEFAULT SYSDATE NOT NULL,
    TRANSACTIONCOUNTER  	INTEGER         	NOT NULL
);

ALTER TABLE SL_PROMOCODE ADD CONSTRAINT PK_PROMOCODE PRIMARY KEY (PROMOCODEID);
ALTER TABLE SL_PROMOCODE ADD CONSTRAINT FK_PROMOCODE_TICKET FOREIGN KEY (TICKETID) REFERENCES TM_TICKET (TICKETID);


COMMENT ON TABLE SL_PROMOCODE is 'Promotional codes table';
COMMENT ON COLUMN SL_PROMOCODE.PROMOCODEID is 'Unique identifier of the promotional codes';
COMMENT ON COLUMN SL_PROMOCODE.TICKETID is 'References TM_TICKET.TICKETID';
COMMENT ON COLUMN SL_PROMOCODE.PROMOCODENUMBER is 'Promotional code number';
COMMENT ON COLUMN SL_PROMOCODE.BEGINDATE is 'Date when promotional code begins';
COMMENT ON COLUMN SL_PROMOCODE.ENDDATE is 'Date when promotional code ends';
COMMENT ON COLUMN SL_PROMOCODE.CAMPAIGNDESCRIPTION is 'Short description regarding promotion campaign';
COMMENT ON COLUMN SL_PROMOCODE.CUSTOMERID is '';
COMMENT ON COLUMN SL_PROMOCODE.STATUS is 'Status of the promotional code, references SL_PROMOCODESTATUS';
COMMENT ON COLUMN SL_PROMOCODE.CREATIONDATE is 'Date when promotional code is created';
COMMENT ON COLUMN SL_PROMOCODE.REDEMPTIONDATE is 'Date when promotional code is used';
COMMENT ON COLUMN SL_PROMOCODE.QUANTITY is 'Available number of promotional code';
COMMENT ON COLUMN SL_PROMOCODE.DISCOUNT is 'Percentage or total amount to be discounted';
COMMENT ON COLUMN SL_PROMOCODE.DISCOUNTTYPE is 'Type of discount, either value or percentage based';
COMMENT ON COLUMN SL_PROMOCODE.ORIGIN is 'Origin station where promotional code is valid';
COMMENT ON COLUMN SL_PROMOCODE.DESTINATION is 'Destination station where promotional code is valid';
COMMENT ON COLUMN SL_PROMOCODE.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_PROMOCODE.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_PROMOCODE.LASTUSER IS 'Technical field: last (system) user that changed this dataset';



CREATE TABLE SL_PROMOCODESTATUS
(
	ENUMERATIONVALUE		NUMBER(9)    		NOT NULL,
	DESCRIPTION 			VARCHAR2(50)	 	NOT NULL,
	LITERAL					VARCHAR2(50)	 	NOT NULL
);

COMMENT ON TABLE SL_PROMOCODESTATUS is 'Promotional code status table';

INSERT INTO SL_PROMOCODESTATUS (ENUMERATIONVALUE, DESCRIPTION, LITERAL)
VALUES (0, 'Promotional code is active', 'Active');
INSERT INTO SL_PROMOCODESTATUS (ENUMERATIONVALUE, DESCRIPTION, LITERAL)
VALUES (1, 'Promotional code is cancelled', 'Cancelled');
INSERT INTO SL_PROMOCODESTATUS (ENUMERATIONVALUE, DESCRIPTION, LITERAL)
VALUES (2, 'Promotional code is disabled', 'Disabled');


-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_PROMOCODE');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_PROMOCODE');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;