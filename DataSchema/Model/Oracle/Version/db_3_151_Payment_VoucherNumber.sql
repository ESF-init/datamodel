SPOOL db_3_151.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.151', 'ULB', 'Added Receiptnumber to rm_payment');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE RM_PAYMENT
ADD (receiptnumber VARCHAR2(20 BYTE));

COMMENT ON COLUMN 
RM_PAYMENT.receiptnumber IS 
'Contains receiptnumber or vouchernumbers';

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
