SPOOL db_3_418.log;
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.418', 'FRD', 'CH_VALIDATIONTRANSACTION create index');

-- =======[ Changes to Existing Tables ]===========================================================================

CREATE INDEX UK_VALIDATEDTRANSACTION ON CH_VALIDATIONTRANSACTION(TRANSACTIONID);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;