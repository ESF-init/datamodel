SPOOL db_3_307.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '3.307', 'BVI','New AddressType: Organization Location.');


INSERT ALL
    INTO SL_AddressType (EnumerationValue, Literal, Description) VALUES (6, 'OrganizationLocation', 'Organization location')
SELECT * FROM DUAL; 


COMMIT;

PROMPT Done!

SPOOL OFF;