SPOOL db_3_593.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.593', 'DIS', 'Add PaymentProviderReferenceExternal to SL_PAYMENTJOURNAL');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PAYMENTJOURNAL
 ADD (PROVIDERREFERENCEEXTERNAL NVARCHAR2(50));
 COMMENT ON COLUMN SL_PAYMENTJOURNAL.PROVIDERREFERENCEEXTERNAL IS 'Payment Provider Reference External';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;