SPOOL db_3_028.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.028', 'STV', 'Added columns for Subscription Managment to SL_Product');

-- Start adding schema changes here

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_PRODUCT ADD (CLIENTID  NUMBER(10));
ALTER TABLE SL_PRODUCT ADD (REFERENCEDPRODUCTID  NUMBER(18));
ALTER TRIGGER SL_PRODUCT_BRU DISABLE;
ALTER TABLE SL_PRODUCT ADD (ISBLOCKED  NUMBER(1) Default 0);
ALTER TRIGGER SL_PRODUCT_BRU ENABLE;
ALTER TABLE SL_PRODUCT ADD (BLOCKINGREASON  NVARCHAR2(100));
ALTER TABLE SL_PRODUCT ADD (BLOCKINGDATE DATE);
ALTER TABLE SL_PRODUCT ADD (CANCELLATIONDATE DATE);

ALTER TABLE SL_PRODUCT ADD CONSTRAINT FK_PRODUCT_CLIENT FOREIGN KEY (CLIENTID) REFERENCES VARIO_CLIENT (CLIENTID);
ALTER TABLE SL_PRODUCT ADD CONSTRAINT FK_PRODUCT_PRODUCT FOREIGN KEY (REFERENCEDPRODUCTID) REFERENCES SL_PRODUCT (PRODUCTID);

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
