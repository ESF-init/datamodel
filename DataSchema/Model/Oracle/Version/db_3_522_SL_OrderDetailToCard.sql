SPOOL db_3_522.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.522', 'DST', 'Fix SL_OrderDetailToCard');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_OrderDetailToCard ADD CONSTRAINT PK_OrderDetailToCard PRIMARY KEY (ORDERDETAILID, CARDID, JOBID) USING INDEX;


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
