SPOOL db_3_529.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.529', 'MFA', 'Add card association type to SL_ORDERDETAIL');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_ORDERDETAIL ADD CARDASSOCIATIONTYPE NUMBER(9,0);
COMMENT ON COLUMN SL_ORDERDETAIL.CARDASSOCIATIONTYPE IS 'Association type of the card';

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;