SPOOL db_3_055.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.055', 'MRP', 'UM_INVENTORYMANAGEMENT');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================
CREATE TABLE UM_INVENTORYMANAGEMENT
(
  TRANSACTIONID 		NUMBER(10,0),
  FIRSTNAME             VARCHAR2(500 BYTE),
  FAMILYNAME            VARCHAR2(500 BYTE),
  ORGANIZATION          VARCHAR2(500 BYTE),
  LOCATION             	VARCHAR2(500 BYTE),
  TRANSACTIONDATE   	DATE,
  TRANSACTIONTYPE     	VARCHAR2(500 BYTE),
  REASON  				VARCHAR2(500 BYTE),
  DEVICEID 				NUMBER(10,0)  
);


--COMMENT ON TABLE UM_INVENTORYMANAGEMENT is '';
comment on table UM_INVENTORYMANAGEMENT IS 'Defines a inventory transaction used for moving a device.';
comment on column UM_INVENTORYMANAGEMENT.transactionid IS 'Unique id to identify the transaction.';
comment on column UM_INVENTORYMANAGEMENT.firstname IS 'First name of the recipient.';
comment on column UM_INVENTORYMANAGEMENT.familyname IS 'Last name of the recipient.';
comment on column UM_INVENTORYMANAGEMENT.organization IS 'Organisation the recipient belongs to.';
comment on column UM_INVENTORYMANAGEMENT.location IS 'Location where the device goes to or get recieved.';
comment on column UM_INVENTORYMANAGEMENT.transactiondate IS 'Date of the transaction.';
comment on column UM_INVENTORYMANAGEMENT.transactiontype IS 'Defines the type of the transaction.';
comment on column UM_INVENTORYMANAGEMENT.reason IS 'Reason of transaction.';
comment on column UM_INVENTORYMANAGEMENT.deviceid IS 'The affected device of the transaction.';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
