SPOOL db_3_458.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.458', 'SVR', 'Add column ACCSETTLEMENTID to SL_POSTING');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_POSTING ADD (ACCSETTLEMENTID  NUMBER(10));

COMMENT ON COLUMN SL_POSTING.ACCSETTLEMENTID IS 'References VARIO_SETTLEMENT.ID.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;