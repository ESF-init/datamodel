SPOOL db_3_088.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.088', 'BVI', 'Added new column to GM_GoodsExchange');

-- =======[ Changes to Existing Tables ]===========================================================================

Alter Table GM_GoodsExchange Add SourceStockID Number(10);

ALTER TABLE GM_GoodsExchange ADD (
  CONSTRAINT FK_GM_GoodsExchange_SStockID
 FOREIGN KEY (SourceStockID) 
 REFERENCES GM_STOCK (StockID));

-- =======[ Commit ]===============================================================================================


COMMIT;

PROMPT Done!

SPOOL OFF;
