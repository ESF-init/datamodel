SPOOL db_3_257.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.257', 'FRD', 'Classification for RITS ClearingHouse, Update for new Critical classification type');

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO CH_CLEARINGCLASSIFICATION (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (100, 'Critical', 'Critical Rule Failure');

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
