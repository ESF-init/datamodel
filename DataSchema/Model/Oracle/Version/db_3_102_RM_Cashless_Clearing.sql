SPOOL db_3_102.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.102', 'ULB', 'New Table CashlessClearing (GiroGo)');
-- =======[ New Tables ]===========================================================================================
CREATE TABLE RM_CASHLESS_CLEARING
(
  CASHLESSCLEARINGID  NUMBER(18),
  SHIFTID             NUMBER(18),
  SAM                 VARCHAR2(20 BYTE)         NOT NULL,
  SUMSEQUENCE         NUMBER(9)                 NOT NULL,
  SHIFTBEGIN          DATE                      NOT NULL,
  DEVICECLASSID       NUMBER(18)                NOT NULL,
  DEVICENO            NUMBER(10)                NOT NULL,
  STATUS              VARCHAR2(200 BYTE),
  CLEARINGDATE        DATE,
  BINARYDATAID        NUMBER(18),
  RECORDCOUNT         NUMBER(4),
  AMOUNT              NUMBER(10),
  SHIFTNUMBER         NUMBER(10),
  CLIENTID            NUMBER(18)
);

CREATE UNIQUE INDEX RM_CASHLESS_CLEARING_PK ON RM_CASHLESS_CLEARING
(CASHLESSCLEARINGID);


ALTER TABLE RM_CASHLESS_CLEARING ADD (
  CONSTRAINT RM_CASHLESS_CLEARING_PK
 PRIMARY KEY
 (CASHLESSCLEARINGID)
    USING INDEX );

COMMENT ON TABLE RM_CASHLESS_CLEARING is 'Details for clearing of Girogo payments';


COMMENT ON COLUMN RM_CASHLESS_CLEARING.CASHLESSCLEARINGID is 'Unique Id for the table RM_CASHLESS_CLEARING';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.SHIFTID            is 'References RM_Shift.Shiftid (optional)';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.SAM                is 'Contains SAM Number (German Haendlerkarte)';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.SUMSEQUENCE        is 'Contains the sam''s sequence counter';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.SHIFTBEGIN         is 'Shiftbegin Timestamp';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.DEVICECLASSID      is 'References Vario_Devioceclass.deviceclassid';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.DEVICENO           is 'Contains device number';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.STATUS             is 'Contains the status of the clearing process';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.CLEARINGDATE       is 'Timestamp of Clearing';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.BINARYDATAID       is 'References RM_Binarydata.Id the Clearingfile';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.RECORDCOUNT        is 'Number of records in clearingfile';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.AMOUNT             is 'Amount in clearingfile';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.SHIFTNUMBER        is 'Number of the shift';
COMMENT ON COLUMN RM_CASHLESS_CLEARING.CLIENTID           is 'References Vario_Client.clientid';



-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
