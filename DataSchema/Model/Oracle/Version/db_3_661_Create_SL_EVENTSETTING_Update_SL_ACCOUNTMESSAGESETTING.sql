SPOOL db_3_661.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.661', 'DIS', 'Create SL_EVENTSETTING and update SL_ACCOUNTMESSAGESETTING ');

-- =======[ Changes to Existing Tables ]===========================================================================

--Create a trigger to change the null values we could receive for messagetype to 0 (email)
--This trigger prevents old code from failing if inserting a null MessageType (Backwards compatibility)
CREATE OR REPLACE TRIGGER SL_ACCOUNTMESSAGESETTING_BRI 
	before insert 
	on SL_ACCOUNTMESSAGESETTING 
	for each row
begin    
    :new.TransactionCounter := dbms_utility.get_time;
	
	IF :new.messagetype is null
    THEN
        :new.messagetype := 0;
    END IF;
end;
/


ALTER TRIGGER SL_ACCOUNTMESSAGESETTING_BRU DISABLE;

--Set the default messagetype value to 0 (if a accountmessagesetting that doesn't include MessageType is inserted before alter the table the script won't fail)
ALTER TABLE SL_ACCOUNTMESSAGESETTING MODIFY MESSAGETYPE DEFAULT 0;

--Set the null values in messagetype to 0
UPDATE SL_ACCOUNTMESSAGESETTING SET MESSAGETYPE = 0, TransactionCounter = TransactionCounter +1 where MESSAGETYPE is null;

ALTER TABLE SL_ACCOUNTMESSAGESETTING MODIFY MESSAGETYPE NUMBER(9) DEFAULT 0 NOT NULL ;

ALTER TABLE SL_ACCOUNTMESSAGESETTING DROP CONSTRAINT PK_ACCOUNTMESSAGESETTING;

DROP INDEX PK_ACCOUNTMESSAGESETTING;
 
ALTER TABLE SL_ACCOUNTMESSAGESETTING ADD (
  CONSTRAINT PK_ACCOUNTMESSAGESETTING
  PRIMARY KEY
  (CUSTOMERACCOUNTID, EMAILEVENTID, MESSAGETYPE));
  
ALTER TRIGGER SL_ACCOUNTMESSAGESETTING_BRU ENABLE;

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_EVENTSETTING (
	EMAILEVENTID NUMBER(18, 0) NOT NULL, 
	MESSAGETYPEID NUMBER(9) NOT NULL,
	ISACTIVE NUMBER(1, 0) DEFAULT 0 NOT NULL,
	ISCONFIGURABLE NUMBER(1, 0) DEFAULT 0 NOT NULL,
	LASTUSER NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LASTMODIFIED DATE DEFAULT sysdate NOT NULL, 
	TRANSACTIONCOUNTER INTEGER NOT NULL,
	constraint PK_D primary key (EMAILEVENTID, MESSAGETYPEID)
);

ALTER TABLE SL_EVENTSETTING ADD CONSTRAINT SL_EMAILEVENTID_FK FOREIGN KEY (EMAILEVENTID) REFERENCES SL_EMAILEVENT(EMAILEVENTID);

ALTER TABLE SL_EVENTSETTING ADD CONSTRAINT SL_MESSAGETYPEID_FK FOREIGN KEY (MESSAGETYPEID) REFERENCES SL_MESSAGETYPE(ENUMERATIONVALUE);

COMMENT ON TABLE  SL_EVENTSETTING IS 'The table is used for configuring what type of notifications a customer account can receive.';

COMMENT ON COLUMN SL_EVENTSETTING.EMAILEVENTID IS 'ID of the message event which is configured.';
COMMENT ON COLUMN SL_EVENTSETTING.MESSAGETYPEID IS 'Type of message which is to be sent.';
COMMENT ON COLUMN SL_EVENTSETTING.ISACTIVE IS 'Indicates whether a message is to be sent.';
COMMENT ON COLUMN SL_EVENTSETTING.ISCONFIGURABLE IS 'Define if the Event is configurable by the user in SL_ACCOUNTMESSAGESETTING';
COMMENT ON COLUMN SL_EVENTSETTING.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_EVENTSETTING.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_EVENTSETTING.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';


CREATE OR REPLACE TRIGGER SL_EventSetting_BRI before insert ON SL_EventSetting for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_EventSetting_BRU before update ON SL_EventSetting for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
