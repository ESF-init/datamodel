SPOOL db_3_479.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.479', 'JGI', 'New column DUETIME');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE PP_TICKET ADD (DUETIME  NUMBER(10));

--add comments:

COMMENT ON COLUMN PP_TICKET.DUETIME IS 'Validity of ticket in seconds';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;