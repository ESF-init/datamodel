SPOOL db_3_117.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.117', 'AMA', 'SubscriptionManagement - Dunning');

-- Start adding schema changes here


-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================
CREATE TABLE SL_DUNNINGLEVEL (
    DUNNINGLEVELID		  			NUMBER(18)  				NOT NULL,		
	CLIENTID               			NUMBER(18)       			NOT NULL,
	NAME                			NVARCHAR2(250)    			NOT NULL,
	ACTIONID    					NUMBER(9)					DEFAULT 0                 		NOT NULL,
	GraceDays						NUMBER(9)					DEFAULT 0                 		NOT NULL,
	AdditionalToleranceDays			NUMBER(9)					DEFAULT 0                 		NOT NULL,
	FormID               			NUMBER(18)       			NOT NULL,
	IsFirstLevel					NUMBER(1)              		DEFAULT 0                     	NOT NULL,
	NextLevelID               		NUMBER(18),
	LASTUSER						NVARCHAR2(50)            	DEFAULT 'SYS'                 	NOT NULL,
	LASTMODIFIED					DATE                     	DEFAULT sysdate               	NOT NULL,
	TRANSACTIONCOUNTER				NUMBER                  	NOT NULL,
	CONSTRAINT PK_DUNNINGLEVEL PRIMARY KEY (DUNNINGLEVELID),
	CONSTRAINT FK_DUNNINGLEVEL_FORM FOREIGN KEY (FormID) REFERENCES SL_FORM (FORMID),
	CONSTRAINT FK_DUNNINGLEVEL_NEXTLEVEL FOREIGN KEY (NextLevelID) REFERENCES SL_DUNNINGLEVEL (DUNNINGLEVELID)
);

COMMENT ON TABLE SL_DUNNINGLEVEL is 'Represents System Dunning levels.';
COMMENT ON COLUMN SL_DUNNINGLEVEL.DUNNINGLEVELID is 'Unique ID of Dunning Levels';
COMMENT ON COLUMN SL_DUNNINGLEVEL.CLIENTID is 'Represents the owner of the level. References VARIO_CLIENT.CLIENTID';
COMMENT ON COLUMN SL_DUNNINGLEVEL.NAME is 'The dunning level name. Given by the user';
COMMENT ON COLUMN SL_DUNNINGLEVEL.ACTIONID is 'Actions that have to be achieved  for this level. References SL_DUNNINGACTION.EnumerationValue';
COMMENT ON COLUMN SL_DUNNINGLEVEL.GraceDays is 'The buffer days to fulfill this dunning';
COMMENT ON COLUMN SL_DUNNINGLEVEL.AdditionalToleranceDays is 'Additional tolerance days before executing the level';
COMMENT ON COLUMN SL_DUNNINGLEVEL.FormID is 'Represents the used form. References SL_FORM.FORMID';
COMMENT ON COLUMN SL_DUNNINGLEVEL.IsFirstLevel is 'Represents whether the level is the first action in the dunning process.';
COMMENT ON COLUMN SL_DUNNINGLEVEL.NextLevelID is 'Represents the next action in the dunning process. If it is NULL => Last Action';

COMMENT ON COLUMN SL_DUNNINGLEVEL.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_DUNNINGLEVEL.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_DUNNINGLEVEL.LASTUSER IS 'Technical field: last (system) user that changed this dataset';


CREATE TABLE SL_DUNNINGLEVELPOSTINGKEY (
    DUNNINGLEVELPOSTINGKEYID  					NUMBER(18)  				NOT NULL,
    DUNNINGLEVELID		  						NUMBER(18)  				NOT NULL,		
    POSTINGKEYID	  							NUMBER(18)  				NOT NULL,		
	AMOUNT										NUMBER(18)              	DEFAULT 0						NOT NULL,
	INITALDESCRIPTION			                NVARCHAR2(250),
	LASTUSER									NVARCHAR2(50)            	DEFAULT 'SYS'                 	NOT NULL,
	LASTMODIFIED								DATE                     	DEFAULT sysdate               	NOT NULL,
	TRANSACTIONCOUNTER							NUMBER                  	NOT NULL,
	CONSTRAINT FK_DUNNINGLEVEL_POSTINGKEY FOREIGN KEY (DUNNINGLEVELID) REFERENCES SL_DUNNINGLEVEL (DUNNINGLEVELID),
	CONSTRAINT FK_POSTINGKEY_DUNNINGLEVEL FOREIGN KEY (POSTINGKEYID) REFERENCES SL_POSTINGKEY (POSTINGKEYID),
	CONSTRAINT PK_DUNNINGLEVELTOPOSTINGKEY PRIMARY KEY (DUNNINGLEVELPOSTINGKEYID)
);

COMMENT ON TABLE SL_DUNNINGLEVELPOSTINGKEY is 'Represents Posting keys have to be used for dunning level.';
COMMENT ON COLUMN SL_DUNNINGLEVELPOSTINGKEY.DUNNINGLEVELPOSTINGKEYID is 'Unique ID of the Table';
COMMENT ON COLUMN SL_DUNNINGLEVELPOSTINGKEY.DUNNINGLEVELID is 'Dunning Level ID. References SL_DUNNINGLEVEL.DUNNINGLEVELID';
COMMENT ON COLUMN SL_DUNNINGLEVELPOSTINGKEY.POSTINGKEYID is 'Posting Key ID. References SL_POSTINGKEY.POSTINGKEYID';
COMMENT ON COLUMN SL_DUNNINGLEVELPOSTINGKEY.AMOUNT is 'The default amount of the dunning posting';
COMMENT ON COLUMN SL_DUNNINGLEVELPOSTINGKEY.INITALDESCRIPTION is 'The default description of the dunning posting';

COMMENT ON COLUMN SL_DUNNINGLEVELPOSTINGKEY.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_DUNNINGLEVELPOSTINGKEY.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_DUNNINGLEVELPOSTINGKEY.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

CREATE TABLE SL_DUNNINGPROCESS (
    DUNNINGPROCESSID		  					NUMBER(18)  				NOT NULL,
    PROCESSNUMBER		  						NUMBER(18)  				NOT NULL,		
    DUNNINGLEVELID	  							NUMBER(18)  				NOT NULL,		
	CONTRACTID          						NUMBER(18)       			NOT NULL,
	USERID              						NUMBER(18)       			NOT NULL,
	INVOICEID              						NUMBER(18)       			NOT NULL,
	ISCLOSED									NUMBER(1)					DEFAULT 0						NOT NULL,
	CREATIONDATE								DATE                     	DEFAULT sysdate               	NOT NULL,
	LASTUPDATED									DATE                     	DEFAULT sysdate               	NOT NULL,
	EXTERNALID              					NVARCHAR2(250),
	DESCRIPTION         						NVARCHAR2(250),
    PROSPECTIVEDUNNINGID		  				NUMBER(18),
	LASTUSER									NVARCHAR2(50)            	DEFAULT 'SYS'                 	NOT NULL,
	LASTMODIFIED								DATE                     	DEFAULT sysdate               	NOT NULL,
	TRANSACTIONCOUNTER							NUMBER                  	NOT NULL,
	CONSTRAINT FK_DUNNING_DUNNINGLEVEL FOREIGN KEY (DUNNINGLEVELID) REFERENCES SL_DUNNINGLEVEL (DUNNINGLEVELID),
	CONSTRAINT FK_DUNNING_USERLIST FOREIGN KEY (USERID) REFERENCES USER_LIST (USERID),
	CONSTRAINT FK_DUNNING_CONTRACT FOREIGN KEY (CONTRACTID) REFERENCES SL_CONTRACT (CONTRACTID),
	CONSTRAINT FK_DUNNING_INVOICE FOREIGN KEY (INVOICEID) REFERENCES SL_INVOICE (INVOICEID),
	CONSTRAINT FK_DUNNING_DUNNING FOREIGN KEY (PROSPECTIVEDUNNINGID) REFERENCES SL_DUNNINGPROCESS (DUNNINGPROCESSID),
	CONSTRAINT PK_DUNNINGPROCESS PRIMARY KEY (DUNNINGPROCESSID)
);

COMMENT ON TABLE SL_DUNNINGPROCESS is 'Contains information about the applied dunning actions.';
COMMENT ON COLUMN SL_DUNNINGPROCESS.DUNNINGPROCESSID is 'Unique ID of the Dunning Process';
COMMENT ON COLUMN SL_DUNNINGPROCESS.PROCESSNUMBER is 'A number that defines the actions belong to one process';
COMMENT ON COLUMN SL_DUNNINGPROCESS.DUNNINGLEVELID is 'References SL_DUNNINGLEVEL.DUNNINGLEVELID';
COMMENT ON COLUMN SL_DUNNINGPROCESS.CONTRACTID is 'References SL_CONTRACT.CONTRACTID';
COMMENT ON COLUMN SL_DUNNINGPROCESS.USERID is 'The system user that generated the action. References USER_LIST.USERID';
COMMENT ON COLUMN SL_DUNNINGPROCESS.INVOICEID is 'A reference to the invoice caused the dunning. References SL_INVOICE.INVOICEID';
--COMMENT ON COLUMN SL_DUNNINGPROCESS.PRODUCTID is 'References SL_PRODUCT.PRODUCTID';
COMMENT ON COLUMN SL_DUNNINGPROCESS.ISCLOSED is 'Indicates whether the process is closed';
COMMENT ON COLUMN SL_DUNNINGPROCESS.CREATIONDATE is 'The creation date';
COMMENT ON COLUMN SL_DUNNINGPROCESS.LASTUPDATED is 'last changes date';
--COMMENT ON COLUMN SL_DUNNINGPROCESS.DUETODATE is 'Due date of the process action';
                  
COMMENT ON COLUMN SL_DUNNINGPROCESS.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_DUNNINGPROCESS.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_DUNNINGPROCESS.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

CREATE TABLE SL_DUNNINGTOPOSTING (
    DUNNINGPROCESSID		  						NUMBER(18)  				NOT NULL,		
    POSTINGID	  									NUMBER(18)  				NOT NULL,		
	CONSTRAINT FK_DUNNINGPOSTING_POSTING FOREIGN KEY (DUNNINGPROCESSID) REFERENCES SL_DUNNINGPROCESS (DUNNINGPROCESSID),
	CONSTRAINT FK_DUNNINGPOSTING_DUNNING FOREIGN KEY (POSTINGID) REFERENCES SL_POSTING (POSTINGID),
	CONSTRAINT PK_DUNNINGTOPOSTING PRIMARY KEY (DUNNINGPROCESSID, POSTINGID)
);

COMMENT ON TABLE SL_DUNNINGTOPOSTING is 'Contains postings generated for dunning action';
COMMENT ON COLUMN SL_DUNNINGTOPOSTING.DUNNINGPROCESSID is 'References SL_DUNNINGPROCESS.DUNNINGPROCESSID';
COMMENT ON COLUMN SL_DUNNINGTOPOSTING.POSTINGID is 'References SL_POSTING.POSTINGID';

CREATE TABLE SL_DUNNINGTOPRODUCT (
    DUNNINGPROCESSID		  						NUMBER(18)  				NOT NULL,		
    PRODUCTID	  									NUMBER(18)  				NOT NULL,		
	CONSTRAINT FK_DUNNINGPRODUCT_POSTING FOREIGN KEY (DUNNINGPROCESSID) REFERENCES SL_DUNNINGPROCESS (DUNNINGPROCESSID),
	CONSTRAINT FK_DUNNINGPRODUCT_DUNNING FOREIGN KEY (PRODUCTID) REFERENCES SL_PRODUCT (PRODUCTID),
	CONSTRAINT PK_DUNNINGTOPRODUCT PRIMARY KEY (DUNNINGPROCESSID, PRODUCTID)
);

COMMENT ON TABLE SL_DUNNINGTOPRODUCT is 'Contains information about the product the dunning was generated for';
COMMENT ON COLUMN SL_DUNNINGTOPRODUCT.DUNNINGPROCESSID is 'References SL_DUNNINGPROCESS.DUNNINGPROCESSID';
COMMENT ON COLUMN SL_DUNNINGTOPRODUCT.PRODUCTID is 'References SL_PRODUCT.PRODUCTID';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_DUNNINGACTION (
    EnumerationValue NUMBER(9,0)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_DUNNINGACTION PRIMARY KEY (EnumerationValue)
);

COMMENT ON TABLE SL_DUNNINGACTION IS 'Contains a list of available action that have to be execute for dunning actions';
COMMENT ON COLUMN SL_DUNNINGACTION.LITERAL IS 'Name of the action; used internally.';
COMMENT ON COLUMN SL_DUNNINGACTION.ENUMERATIONVALUE IS 'Unique id of the dunning action';
COMMENT ON COLUMN SL_DUNNINGACTION.DESCRIPTION IS 'Dunning action description; used in the UI';


-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

CREATE SEQUENCE SL_DUNNINGLEVEL_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;

CREATE SEQUENCE SL_DUNNINGLEVELPOSTINGKEY_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;
	
CREATE SEQUENCE SL_DUNNINGPROCESS_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;	
-- =======[ Trigger ]==============================================================================================

CREATE OR REPLACE TRIGGER SL_DUNNINGLEVEL_BRI before insert on SL_DUNNINGLEVEL for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_DUNNINGLEVEL_BRU before update on SL_DUNNINGLEVEL for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_DUNNINGLEVELPOSTINGKEY_BRI before insert on SL_DUNNINGLEVELPOSTINGKEY for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_DUNNINGLEVELPOSTINGKEY_BRU before update on SL_DUNNINGLEVELPOSTINGKEY for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_DUNNINGPROCESS_BRI before insert on SL_DUNNINGPROCESS for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_DUNNINGPROCESS_BRU before update on SL_DUNNINGPROCESS for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------
INSERT ALL
    INTO SL_DUNNINGACTION (EnumerationValue, Literal, Description) VALUES (0, 'None', 'None')
    INTO SL_DUNNINGACTION (EnumerationValue, Literal, Description) VALUES (1, 'CloseContract', 'Close Contract')
    INTO SL_DUNNINGACTION (EnumerationValue, Literal, Description) VALUES (2, 'Inkasso', 'Inkasso')
SELECT * FROM DUAL;
-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
