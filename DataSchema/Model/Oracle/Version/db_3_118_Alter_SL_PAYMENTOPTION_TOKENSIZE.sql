SPOOL db_3_118.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.118', 'EPA', 'Alter token size of SL_PAYMENTOPTION.BANKINGSERVICETOKEN to 125');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PAYMENTOPTION MODIFY(BANKINGSERVICETOKEN NVARCHAR2(125));


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
