SPOOL db_3_318.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.318', 'ARH', 'Add Column ReceivedReference to SL_BookingItem');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_BOOKINGITEM ADD RECEIVEDREFERENCE NVARCHAR2(50) ;

COMMENT ON COLUMN SL_BOOKINGITEM.RECEIVEDREFERENCE IS 'Reference of the booking/reservation/rent at the mobility provider.';

COMMIT;

PROMPT Done!

SPOOL OFF;
