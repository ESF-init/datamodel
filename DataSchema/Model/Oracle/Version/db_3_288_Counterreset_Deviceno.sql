SPOOL db_3_288.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.288', 'ULB', 'added CR_HAENDLERKARTEN.Deviceno');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE CR_HAENDLERKARTEN
ADD (Deviceno NUMBER(10));

COMMENT ON COLUMN 
CR_HAENDLERKARTEN.Deviceno IS 
'Number of the device the terminal is connected to.';


COMMIT;

PROMPT Done!

SPOOL OFF;
