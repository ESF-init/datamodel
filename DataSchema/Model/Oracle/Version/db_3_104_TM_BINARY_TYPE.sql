SPOOL db_3_104.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.104', 'SLR', 'Added column to TM_BINARY_TYPE.');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table TM_BINARY_TYPE add ImportFiles NVARCHAR2(250);
alter table TM_BINARY_TYPE add AllowedImportFileExtensions NVARCHAR2(250);

COMMENT ON COLUMN TM_BINARY_TYPE.ImportFiles IS 'Defines all file names to import for binary type.';
COMMENT ON COLUMN TM_BINARY_TYPE.ImportFiles IS 'Defines all file extensions allowed for binary type.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
