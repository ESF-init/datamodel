
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.682', 'BVI', 'Modification of DSGVO Package');


CREATE OR REPLACE package DSGVO is  

PROCEDURE p$AddProtocolMessage
    (
       p_sMessageText           IN VARCHAR,
       p_nClientId              IN NUMBER,
       p_nProtID                IN NUMBER DEFAULT -1
    );

Procedure p$AnonymizeContract
(
   p_nContractNo             IN NUMBER,
   p_nClientId               IN NUMBER
);

Procedure p$RemoveContract
(
   p_nContractID             IN NUMBER,
   p_nClientId               IN NUMBER
);

PROCEDURE p$CLEARDEBTORS
(
   p_nMonths             IN NUMBER,
   p_nClientId           IN NUMBER,
   p_nAnonymizedDebtorNo IN NUMBER
);

PROCEDURE p$SL_ANONYMIZEADDRESS
(
    p_nAddressId               IN NUMBER,
    p_nClientId                IN NUMBER,
    p_nProtId                  IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONBANKCONNECTION
(
    p_nBankConnectionId        IN NUMBER,
    p_nClientId                IN NUMBER,
    p_nProtId                  IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONBANKCONTOPERSON
(
    p_nAccountOwnerId          IN NUMBER,
    p_nClientId                IN NUMBER,
    p_nProtId                  IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONPERSON$PERSONID
(
   p_nPersonId               IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONPERSON$CONTRACTID
(
       p_nContractId             IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONYMIZECONTRACT
(
   p_sContractNo             IN VARCHAR,
   p_nClientId               IN NUMBER
);

PROCEDURE p$SL_ANONYMIZECONTRACTID
(
   p_nContractId             IN NUMBER,
   p_nClientId               IN NUMBER
);

PROCEDURE p$SL_ANONCUSTOMERACOUNT$PERSID
(
   p_nPersonId               IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONCUSTOMERACOUNT$CONID
(
   p_nContractId             IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONCUSTOMERACOUNT$ID
(
   p_nCustomerAccountId      IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONORDER
(
   p_nOrderId             IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONORDERSTOCONTRACT
(
   p_nContractId             IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONORDERDETAIL
(
   p_nOrderDetailId          IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONORDERDETAILTOORDER
(
   p_nOrderId                IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONPHOTOGRAPH
(
   p_nPhotographId               IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONPHOTOSTOPERSON
(
   p_nPersonId               IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONCOMMENTS
(
   p_nContractId             IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONCONTRACTHISTORY
(
   p_nContractHistoryId      IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONHISTORYTOCONTRACT
(
   p_nContractId             IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONENTITLEMENT
(
   p_nEntitlementId          IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONENTITLEMENTOFPERSON
(
   p_nPersonId               IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONENTITLEMENTTOPRODUCT
(
   p_nProductId              IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONPRODUCT
(
   p_nProductId               IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONPRODUCTSTOCARD
(
   p_nCardId                 IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONREFUND
(
   p_nRefundId               IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONORDERHISTORY
(
   p_nOrderhistoryId         IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONHISTORYTOORDER
(
   p_nOrderId                IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONVOUCHER
(
   p_nVoucherId              IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONVOUCHERTOCARD
(
   p_nCardId                 IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONPERSONADDRESS
(
   p_nPersonId               IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONCARD$CARDHOLDER
(
   p_nCardHolderId           IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONORGANIZAION$CONTRACT
(
   p_nContractId             IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONORGANIZATION
(
   p_nOrganizationId         IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

PROCEDURE p$SL_ANONCONADDRESSTOCON
(
   p_nContractId             IN NUMBER,
   p_nClientId               IN NUMBER,
   p_nProtId                 IN NUMBER DEFAULT -1
);

end DSGVO;
/
CREATE OR REPLACE package body DSGVO is         

    l_nProtId            NUMBER           := 0;
    l_sMessageText       VARCHAR2(1800);
    l_sParamDescription  VARCHAR2(25)     := 'DSGVO';
    l_sMinDate           VARCHAR2(25)     := '01.01.1970';
    l_sDSGVOEmail        VARCHAR2(25)     := 'a@b.de';
    l_sIBAN              VARCHAR2(22)     := 'DE12500105170648489890';
    l_sBIC               VARCHAR2(22)     := 'INGDDEFFXXX';
    l_sPostalCode        VARCHAR2(22)     := '76131';
    l_sPhoneFax          VARCHAR2(22)     := '0815';
    
    
    PROCEDURE p$AddProtocolMessage
    (
       p_sMessageText           IN VARCHAR,
       p_nClientId              IN NUMBER,
       p_nProtID                IN NUMBER DEFAULT -1
    )
    IS
      PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
      IF p_nProtId = -1 
        THEN
          l_nProtId := getDataId();
          P_CreateprotocolClient(l_nProtId, 7000, sysdate, 'FVS', 0, p_nClientId);
        ELSE
          l_nProtId := p_nProtId;
      END IF;
      
      P_Writemessage(0, 11, l_nProtId, 0, p_sMessageText, sysdate);
      P$CLOSEPROTOCOL(l_nProtId);
      COMMIT;
    END p$AddProtocolMessage;
                
    PROCEDURE p$AnonymizeContract
    (
       p_nContractNo             IN NUMBER,
       p_nClientId               IN NUMBER
    )
    IS
        l_tParamContractID pp_contract.contractid%Type;

    BEGIN
        
        l_nProtId := getDataID();
        P_CreateprotocolClient(l_nProtId, 7000, sysdate, 'FVS', 0, p_nClientId);
        commit;
        -- Find contract
        select contractid into l_tParamContractID from pp_contract where contractno = p_nContractNo;
        l_sMessageText := 'Vertrag mit ID: ' || l_tParamContractID || ' wird anonymisiert';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
        
        -- Anonymize dependent data
        l_sMessageText := 'Einträge aus der Tabelle PP_ACCOUNT von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
        update pp_account 
          set ACCOUNTHOLDERFIRSTNAME  = l_sParamDescription,
              ACCOUNTHOLDERLASTNAME   = l_sParamDescription,
              BANKACCOUNTNO           = 0,
              BANKNAME                = l_sParamDescription,
              BANKNO                  = 0,
              BIC                     = l_sBIC,
              IBAN                    = l_sIBAN,
              SEPARETCODE             = l_sParamDescription,
              STATENO                 = 2 
          where contractid = l_tParamContractID;

        l_sMessageText := 'Einträge aus der Tabelle PP_ACCOUNTENTRY von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
        update pp_accountEntry
           set PostingText         = l_sParamDescription
         where contractID          = l_tParamContractID;

        l_sMessageText := 'Einträge aus der Tabelle PP_CONTRACTHISTORY von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
        update  pp_contracthistory
            set message = l_sParamDescription
          where contractID = l_tParamContractID;
        
        l_sMessageText := 'Einträge aus der Tabelle PP_SHIPMENTADRESS von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);     
        update pp_shipmentadress  
           set ARISTOCRATICTITLE   = l_sParamDescription,
               CITY                = l_sParamDescription,
               COUNTRYSHORTNAME    = l_sParamDescription,
               FAMILYNAME          = l_sParamDescription,
               FIRSTNAME           = l_sParamDescription,
               ORDERNUMBER         = l_sParamDescription,
               POBOX               = 0,
               POBOXCITY           = l_sParamDescription,
               POSTALCODEPOBOX     = l_sParamDescription,
               POSTALCODESTREET    = l_sParamDescription,
               STREET              = l_sParamDescription,
               STREETNO            = l_sParamDescription,
               TITLEID             = 4   
         where contractID = l_tParamContractID;

        l_sMessageText := 'Einträge aus der Tabelle SAM_SEASONTICKETHOLDER von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
        update sam_seasonticketholder
           set CITY                = l_sParamDescription,
               DATEOFBIRTH         = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               DESCRIPTION         = l_sParamDescription,
               EXPORTSTATE         = 0,
               FIRSTNAME           = l_sParamDescription,
               LASTNAME            = l_sParamDescription,
               NUMBER_             = 0,
               PHONE               = l_sPhoneFax,
               PICTURE             = null,
               POSTALCODE          = l_sParamDescription,
               STREET              = l_sParamDescription 
         where seasonticketholderid in 
                    (
                      select holder.seasonticketholderid from sam_paymentschedulemapping psm
                      join sam_seasonticket st on psm.seasonticketid = st.seasonticketid
                      join sam_seasonticketholder holder on st.holderid = holder.seasonticketholderid
                      where psm.contractid = l_tParamContractID
                     );     
        l_sMessageText := 'Attribute von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
         update pp_contract 
            set ACCOUNTHOLDERFIRSTNAME    = l_sParamDescription,
                ACCOUNTHOLDERLASTNAME     = l_sParamDescription,
                ACCOUNTNO                 = 1,
                ACTIVE                    = -1,
                ADDCOMMENT                = l_sParamDescription,
                ADDITION                  = l_sParamDescription,
                ADDITIONALTEXT            = l_sParamDescription,
                ADDRESS1                  = l_sParamDescription,
                ADDRESS2                  = l_sParamDescription,
                AGENTNAME                 = l_sParamDescription,
                AGENTNO                   = 0,
                ANSWERMARKETINGQUESTION1  = l_sParamDescription,
                ANSWERMARKETINGQUESTION2  = l_sParamDescription,
                ANSWERMARKETINGQUESTION3  = l_sParamDescription,
                ANSWERMARKETINGQUESTION4  = l_sParamDescription,
                ANSWERMARKETINGQUESTION5  = l_sParamDescription,
                ARISTOCRATICTITLE         = 0,
                BANKCODE                  = 0,
                BANKNAME                  = l_sParamDescription,
                BIKEUSER                  = 0,
                BILLLEVELOFDETAIL         = 1,
                BIRTHDAY                  = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
                CAUSEOFCANCELATIONID      = 0,
                CENTRELINKCRN             = l_sParamDescription,
                CITY                      = l_sParamDescription,
                CONCESSIONTYPE            = l_sParamDescription,
                CONSESSIONCARDTYPE        = l_sParamDescription,
                CONTRACTLOCK              = 0,
                CONTRACTLOCKDATE          = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
                CONTRACTNO                = 0,
                CONTRACTSTATUSID          = 7,
                COUNTRY                   = l_sParamDescription,
                COUNTRYSHORTNAME          = l_sParamDescription,
                COUNTY                    = l_sParamDescription,
                DATELASTINTERNETACCESS    = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
                DEBITORNO                 = 0,
                DEPOSITALLOWED            = 0,
                DUNNING_BILL_LOCK         = 0,
                DUNNINGLEVELID            = 0,
                DUNNINGTIMESTAMP          = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
                EMAIL                     = l_sDSGVOEmail,
                ENDDATE                   = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
                EXPORTSTATE               = 0,
                FAMILYNAME                = l_sParamDescription,
                FAX                       = l_sPhoneFax,
                FIRSTNAME                 = l_sParamDescription,
                FON                       = l_sPhoneFax,
                IBAN                      = l_sParamDescription,
                IDNUMBER                  = l_sParamDescription,
                IDNUMBERTYPE              = 0,
                INSTITUTION               = l_sParamDescription,
                MARKETINGACTIVITIES       = 0,
                MOBILEFON                 = l_sPhoneFax,
                NOOFINTERNETACCESS        = 0,
                ONLINEACCOUNTNAME         = l_sParamDescription,
                PAYMENTMETHODID           = 1,
                PIN                       = 0,
                POBOX                     = 0,
                POBOXCITY                 = l_sParamDescription,
                POSTALCODEPOBOX           = l_sPostalCode,
                POSTALCODESTREET          = l_sPostalCode,
                REGISTRATIONCONFIRMED     = 0,
                SECONDSIGNATUREEXISTING   = 0,
                SPAREFIELD1               = l_sParamDescription,
                SPAREFIELD2               = l_sParamDescription,
                SPAREFIELD3               = 0,
                SPAREFIELD4               = 0,
                STARTDATE                 = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
                STREET                    = l_sParamDescription,
                STREETNO                  = l_sParamDescription,
                STUDENTID                 = l_sParamDescription,
                STUDENTSCHOOL             = l_sParamDescription,
                STUDENTYEAR               = l_sParamDescription,
                TELEPHONEPASSWORD         = l_sParamDescription,
                TIMELASTINTERNETACCESS    = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
                TITLEID                   = 4,
                TYPEOFDISPATCHBILLID      = 1,
                TYPEOFDISPATCHCARDID      = 1,
                WHEELCHAIRUSER            = 0,
                YEARLEVEL                 = l_sParamDescription
         where contractid = l_tParamContractID;
         
        P$CLOSEPROTOCOL(l_nProtId);
        
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
         -- Log entry in log table
         l_sMessageText := 'Vertrag: ' || p_nContractNo || ' nicht gefunden';
         P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
         P$CLOSEPROTOCOL(l_nProtId);
       WHEN OTHERS THEN
         -- Log entry in log table
         l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Vertrag: ' || l_tParamContractID;
         P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
         P$CLOSEPROTOCOL(l_nProtId);
             
    END p$AnonymizeContract;
    
    Procedure p$RemoveContract
    (
        p_nContractID             IN NUMBER,
        p_nClientId               IN NUMBER
    )
    IS 
    
    l_nContractID NUMBER := 0;

    CURSOR CONTRACT(pc_nContractID IN NUMBER, pc_nClientId IN NUMBER)
      IS
        (SELECT TRAFFICCOMPANYID, CONTRACTNO, CONTRACTID FROM PP_CONTRACT WHERE CONTRACTID = pc_nContractID AND TRAFFICCOMPANYID = pc_nClientId);
        
    BEGIN
      
      l_nProtId := getDataId();
      P_CreateprotocolClient(l_nProtId, 7000, sysdate, 'FVS', 0, p_nClientId);
      FOR CON IN CONTRACT(p_nContractID, p_nClientId) 
      LOOP
          l_nContractID := CON.CONTRACTID;
          l_sMessageText:= 'Löschen von Vertrag: ' || CON.CONTRACTID;
          P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
          -- Delete Account Info
          l_sMessageText:= 'Löschen der Kontodaten';
          P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
          DELETE FROM VARIO_RETURNDEBIT WHERE CONTRACTID = CON.CONTRACTID;
          DELETE FROM PP_ACCOUNTENTRY WHERE CONTRACTID = CON.CONTRACTID;
          DELETE FROM PP_ACCOUNTBALANCE WHERE CONTRACTID = CON.CONTRACTID;
          DELETE FROM PP_ACCOUNT WHERE CONTRACTID = CON.CONTRACTID;
          -- Delete Dunning Info
          l_sMessageText:= 'Löschen der Mahnungen';
          P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
          DELETE FROM PP_FAILED_FORMULAR WHERE CONTRACTID = CON.CONTRACTID;
          DELETE FROM PP_CONTRACTDUNNINGBLOCKING WHERE CONTRACTID = CON.CONTRACTID;
          DELETE FROM PP_DUNNINGFORMULAR WHERE DUNNINGID IN (SELECT DUNNINGID FROM VARIO_DUNNING WHERE CONTRACTID = CON.CONTRACTID);
          DELETE FROM VARIO_DUNNING WHERE CONTRACTID = CON.CONTRACTID;
          -- Delete Bill Info
          l_sMessageText:= 'Löschen der Rechnungen';
          P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
          DELETE FROM PP_BILLFORMULAR WHERE BILLID IN (SELECT BILLID FROM PP_BILL WHERE CONTRACTID = CON.CONTRACTID);
          DELETE FROM PP_BILL WHERE CONTRACTID = CON.CONTRACTID;
          -- Delete Trips and Tickets
          l_sMessageText:= 'Löschen der Tickets und Fahrten';
          P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
          DELETE FROM PP_VALIDATEDTICKET WHERE VALIDATEDTICKETID IN (SELECT VALIDATEDTICKETID FROM PP_TRIP_VALIDATEDTICKET WHERE TRIPID IN (SELECT TRIPID FROM PP_TRIP WHERE CARDID IN (SELECT CARDID FROM PP_CARD WHERE CONTRACTID = CON.CONTRACTID)));
          DELETE FROM PP_TRIP_VALIDATEDTICKET WHERE TRIPID IN (SELECT TRIPID FROM PP_TRIP WHERE CARDID IN (SELECT CARDID FROM PP_CARD WHERE CONTRACTID = CON.CONTRACTID));
          DELETE FROM PP_TRIP WHERE CARDID IN (SELECT CARDID FROM PP_CARD WHERE CONTRACTID = CON.CONTRACTID);
          -- Delete Cards
          l_sMessageText:= 'Löschen der Karten';
          P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
          DELETE FROM PP_CARDSTATUSHISTORY WHERE CARDID IN (SELECT CARDID FROM PP_CARD WHERE CONTRACTID = CON.CONTRACTID);
          DELETE FROM PP_CARD WHERE CONTRACTID = CON.CONTRACTID;
          -- Delete Contract
          l_sMessageText:= 'Löschen der Vertragsdaten';
          P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
          DELETE FROM PP_MISSING_BILLS WHERE CONTRACTID = CON.CONTRACTID;
          DELETE FROM PP_SHIPMENTADRESS WHERE CONTRACTID = CON.CONTRACTID;
          DELETE FROM PP_CONTRACTHISTORY WHERE CONTRACTID = CON.CONTRACTID;
          DELETE FROM PP_CONTRACT WHERE CONTRACTID = CON.CONTRACTID;
       
          
      END LOOP;
      
      P$CLOSEPROTOCOL(l_nProtId);
      
      
      EXCEPTION
       WHEN NO_DATA_FOUND THEN
         -- Log entry in log table
         l_sMessageText := 'Vertrag: ' || p_nContractID || ' nicht gefunden';
         P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
         P$CLOSEPROTOCOL(l_nProtId);
       WHEN OTHERS THEN
         -- Log entry in log table
         l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Löschen von Vertrag: ' || l_nContractID;
         P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
         P$CLOSEPROTOCOL(l_nProtId);
         
    END p$RemoveContract; 
    
    PROCEDURE p$CLEARDEBTORS
    (
       p_nMonths             IN NUMBER,
       p_nClientId           IN NUMBER,
       p_nAnonymizedDebtorNo IN NUMBER
    )
    IS

    l_nDebtorID NUMBER := -1;
      
    CURSOR debtors ( pc_nMonths NUMBER)
      IS
        (SELECT debtorid, debtorno FROM dm_debtor WHERE EXITDATE IS NOT NULL AND EXITDATE < ADD_MONTHS(sysdate, pc_nMonths * -1) );
      
    BEGIN

      ---GM_DEBTORTOKEN
    ---GM_DEVICELOCKS
    ---GM_GOODSEXCHANGE
    ---GM_STOCK

    --POS_POINTOFSALE
    --PP_ACCOUNT
    --PP_ACCOUNTBALANCE
    --PP_ACCOUNTENTRY

    --DM_QUALIFICATION
    --DM_DEBTORLOGINTODEVICE
    --DM_DEBTORFINGERPRINT
    --DM_CARD_TO_DEBTOR
    ---DM_DEBTOR
      -- Open new log file in the database
      l_nProtId := getDataId();
      P_CreateprotocolClient(l_nProtId, 7000, sysdate, 'FVS', 0, p_nClientId);
      FOR debtor IN debtors(p_nMonths) LOOP
        
        l_nDebtorID := debtor.debtorid;
        -- Release debtor from debtor card
        l_sMessageText := 'Karten zu Debtor mit ID: ' || debtor.debtorid || ' gelöscht';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
        
        -- Anonymize shifts
        l_sMessageText := 'Schchten von Debtor mit ID: ' || debtor.debtorid || ' anonymisiert';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
        
        --Delete debtor dependent data
        l_sMessageText := 'Einträge aus der Tabelle GM_DEBTORTOKEN von Debtor mit ID: ' || debtor.debtorid || ' gelöscht';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
        --DELETE FROM GM_DEBTORTOKEN WHERE userid = debtor.debtorid;
        
        l_sMessageText := 'Einträge aus der Tabelle GM_DEVICELOCKS von Debtor mit ID: ' || debtor.debtorid || ' gelöscht';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
        --DELETE FROM GM_DEVICELOCKS WHERE debtorno = debtor.debtorno;
        
        l_sMessageText := 'Einträge aus der Tabelle GM_GOODSEXCHANGE von Debtor mit ID: ' || debtor.debtorid || ' gelöscht';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
        --DELETE FROM GM_GOODSEXCHANGE WHERE debtorid = debtor.debtorid;
        
        l_sMessageText := 'Einträge aus der Tabelle GM_STOCK von Debtor mit ID: ' || debtor.debtorid || ' gelöscht';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
        --DELETE FROM GM_STOCK WHERE debtorid = debtor.debtorid;
        
        -- Delete debtor
        l_sMessageText := 'Debtor mit ID: ' || debtor.debtorid || ' gelöscht';
        P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
        --DELETE FROM DM_DEBTOR WHERE debtorid = debtor.debtorid;
        -- Close log file in the database
        P$CLOSEPROTOCOL(l_nProtId);
        
        
      END LOOP;
      -- Close log file in the database
      P$CLOSEPROTOCOL(l_nProtId);
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
         -- Log entry in log table
         l_sMessageText := 'Debtor nicht gefunden';
         P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
         P$CLOSEPROTOCOL(l_nProtId);
       WHEN OTHERS THEN
         -- Log entry in log table
         l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Löschen von Vertrag: ' || l_nDebtorID;
         P_Writemessage(0, 11, l_nProtId, 0, l_sMessageText, sysdate);
         P$CLOSEPROTOCOL(l_nProtId);
         
    END p$CLEARDEBTORS;
    
    PROCEDURE p$SL_ANONYMIZEADDRESS
    (
       p_nAddressId               IN NUMBER,
       p_nClientId                IN NUMBER,
       p_nProtId                  IN NUMBER DEFAULT -1
    )
    IS
    
     l_tParamPrimaryKeyID sl_address.addressid%Type;

    BEGIN
        
        -- Find contract
        select addressid into l_tParamPrimaryKeyID from sl_address where addressid = p_nAddressId; 
        l_sMessageText := 'Addresse mit ID: ' || p_nAddressId  || ' wird anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
       
        update sl_address          
           set ADDRESSEE = l_sParamDescription,
               STREET = l_sParamDescription,
               STREETNUMBER = l_sParamDescription,
               ADDRESSFIELD1 = l_sParamDescription,
               ADDRESSFIELD2 = l_sParamDescription,
               --CITY = l_sParamDescription, will not be changed (Subscription SDN requirement)
               POSTALCODE = l_sPostalCode,
               REGION = l_sParamDescription,
               COUNTRY = l_sParamDescription,
               LASTUSER = l_sParamDescription,
               LASTMODIFIED = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1,
               DESCRIPTION = l_sParamDescription   
        where addressid = l_tParamPrimaryKeyID;
        
        
         
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
         -- Log entry in log table
         l_sMessageText := 'Adresse: ' || p_nAddressId || ' nicht gefunden';
         p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
       WHEN OTHERS THEN
         -- Log entry in log table
         l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Adresse: ' || p_nAddressId;
         p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
              
       
    END p$SL_ANONYMIZEADDRESS;
    
    
    PROCEDURE p$SL_ANONBANKCONNECTION
    (
       p_nBankConnectionId        IN NUMBER,
       p_nClientId                IN NUMBER,
       p_nProtId                  IN NUMBER DEFAULT -1
    )  
    IS
    
     l_tParamPrimaryKeyID SL_BANKCONNECTIONDATA.BANKCONNECTIONDATAID%Type;

    BEGIN
        
        -- Find contract
        select BANKCONNECTIONDATAID into l_tParamPrimaryKeyID from SL_BANKCONNECTIONDATA where BANKCONNECTIONDATAID =  p_nBankConnectionId; 
        l_sMessageText := 'Bankverbindung mit ID: ' || l_tParamPrimaryKeyID  || ' wird anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
        
        UPDATE SL_PAYMENTOPTION SET BANKCONNECTIONDATAID = null, paymentmethod = 0, transactioncounter = transactioncounter + 1 where BANKCONNECTIONDATAID =l_tParamPrimaryKeyID;
        UPDATE SL_ADDRESSBOOKENTRY SET BANKCONNECTIONDATAID  = null, transactioncounter = transactioncounter + 1 where BANKCONNECTIONDATAID = l_tParamPrimaryKeyID;
        UPDATE PP_CREDIT_SCREENING SET BANKCONNECTIONDATAID = null where BANKCONNECTIONDATAID =l_tParamPrimaryKeyID;
        DELETE FROM SL_BANKCONNECTIONDATA where BANKCONNECTIONDATAID = l_tParamPrimaryKeyID;
        /*update SL_BANKCONNECTIONDATA          
           set IBAN = l_sIBAN,
               BIC = l_sBIC,
               VALIDFROM  = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               VALIDUNTIL  = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               CREATIONDATE  = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               MANDATEREFERENCE = l_sParamDescription,
               DESCRIPTION = l_sParamDescription,
               LASTUSAGE  = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               LASTUSER = l_sParamDescription,
               LASTMODIFIED  = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1,
               FREQUENCYTYPE = 0,
               OWNERTYPE = 0
        where BANKCONNECTIONDATAID = l_tParamPrimaryKeyID;
        */
        
         
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
         -- Log entry in log table
         l_sMessageText := 'Bankverbindung zu Person: ' ||  p_nBankConnectionId || ' nicht gefunden';
         p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
       WHEN OTHERS THEN
         -- Log entry in log table
         l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Bankverbindung: ' || l_tParamPrimaryKeyID;
         p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
              
       
    END p$SL_ANONBANKCONNECTION;
    
    
    PROCEDURE p$SL_ANONBANKCONTOPERSON
    (
       p_nAccountOwnerId          IN NUMBER,
       p_nClientId                IN NUMBER,
       p_nProtId                  IN NUMBER DEFAULT -1
    )
    IS
       
       Cursor c1 (c_nBankCnnectionId NUMBER) is (select BANKCONNECTIONDATAID from SL_BANKCONNECTIONDATA WHERE ACCOUNTOWNERID =  c_nBankCnnectionId);
    BEGIN
       
        FOR nBankConnection IN c1(p_nAccountOwnerId) LOOP
          p$SL_ANONBANKCONNECTION(nBankConnection.BANKCONNECTIONDATAID, p_nClientId, p_nProtId);
        END LOOP;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Keine Bankverbindung zu Person : ' || p_nAccountOwnerId || ' gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Bankverbindungen zu Person: ' || p_nAccountOwnerId;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONBANKCONTOPERSON; 
    
    PROCEDURE p$SL_ANONPERSONADDRESS
    (
       p_nPersonId               IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
    
    l_tParamPrimaryKeyID sl_person.personid%Type;
     
    Cursor c1 (c_nPersonId NUMBER) is (select PERSONID, ADDRESSID from SL_PERSONADDRESS WHERE PERSONID =  c_nPersonId);

    BEGIN
    
        FOR oObject IN c1(p_nPersonId) LOOP
        
        -- SL_ADDRESS anonymisieren
        l_tParamPrimaryKeyID := oObject.PERSONID;
        l_sMessageText := 'Einträge aus der Tabelle SL_ADDRESS von PersonAddress mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
        p$SL_ANONYMIZEADDRESS(oObject.ADDRESSID,p_nClientId,l_nProtId);
       
       
        update sl_personaddress 
           set LASTUSER = l_sParamDescription,
               LASTMODIFIED = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1
        where personid = l_tParamPrimaryKeyID;
        
    END LOOP;     
        
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
         -- Log entry in log table
         l_sMessageText := 'Person: ' || p_nPersonId || ' nicht gefunden';
         p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
       WHEN OTHERS THEN
         -- Log entry in log table
         l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Person: ' || p_nPersonId;
         p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
       
    END p$SL_ANONPERSONADDRESS;  
       
    
    PROCEDURE p$SL_ANONPERSON$PERSONID
    (
       p_nPersonId               IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
    
     l_tParamPrimaryKeyID sl_person.personid%Type;
     l_addressid NUMBER := 0;
     l_alternativeAddressid NUMBER := 0;

    BEGIN
    
        -- Find contract
        select personid, addressid, alternativeadressid into l_tParamPrimaryKeyID, l_addressid, l_alternativeAddressid from sl_person where personid = p_nPersonId; 
        l_sMessageText := 'Person mit ID: ' || p_nPersonId  || ' wird anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
        
        -- Anonymize dependent data
        l_sMessageText := 'Einträge aus der Tabelle SL_ADDRESS von Person mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
        p$SL_ANONYMIZEADDRESS(l_addressid,p_nClientId,l_nProtId);
        p$SL_ANONYMIZEADDRESS(l_alternativeAddressid,p_nClientId,l_nProtId);
        --p$SL_ANONCUSTOMERACOUNT$ID(p_nPersonId, p_nClientId, l_nProtId );
        
        l_sMessageText := 'Einträge aus der Tabelle SL_BANKCONNECTIONDATA von Person mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
        p$SL_ANONBANKCONTOPERSON(p_nPersonId, p_nClientId, l_nProtId);
        
        l_sMessageText := 'Einträge aus der Tabelle SL_ENTITLEMENT von Person mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
        p$SL_ANONENTITLEMENTOFPERSON(p_nPersonId, p_nClientId, l_nProtId);
        
        l_sMessageText := 'Einträge aus der Tabelle SL_PHOTOGRAPH von Person mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
        p$SL_ANONPHOTOSTOPERSON(p_nPersonId, p_nClientId, l_nProtId);
        
        l_sMessageText := 'Einträge aus der Tabelle SL_PERSONADDRESS von Person mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
        p$SL_ANONPERSONADDRESS(p_nPersonId, p_nClientId, l_nProtId);
        
        l_sMessageText := 'Einträge aus der Tabelle SL_CARD von Person mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
        p$SL_ANONCARD$CARDHOLDER(p_nPersonId, p_nClientId, l_nProtId);
       
	    DELETE FROM sl_cardholderorganization where CardHolderID = l_tParamPrimaryKeyID;
		
        update sl_person p
           set p.TITLE = 4,
               p.FIRSTNAME = l_sParamDescription,
               p.MIDDLENAME = l_sParamDescription,
               p.LASTNAME = l_sParamDescription,
               p.DATEOFBIRTH = to_date(('01.01.' || to_char(EXTRACT(YEAR FROM p.DateOfBirth))), 'DD.MM.YYYY'),
               p.EMAIL = l_sDSGVOEmail,
               p.PHONENUMBER = l_sPhoneFax,
               p.CELLPHONENUMBER = l_sPhoneFax,
               p.FAXNUMBER = l_sPhoneFax,
               p.ORGANIZATIONALIDENTIFIER = l_sParamDescription,
               p.AUTHENTICATED = 0,
               p.COMMENTTEXT = l_sParamDescription,
               p.COMMENTALARM = 0,
               p.MUNICIPALITY = l_sParamDescription,
               p.USERGROUPEXPIRY = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               p.USERGROUP = 0,
               p.PERSONALASSISTENT = 0,
               p.IDENTIFIER= l_sParamDescription,
               p.GENDER = 0,
               p.ISEMAILCONFIRMED = 0,
               p.LASTUSER = l_sParamDescription,
               p.LASTMODIFIED = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               p.TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1,
               p.GRADE = l_sParamDescription,
               p.COMMENTTEXT2 = l_sParamDescription,
               p.COMMENTTEXT3 = l_sParamDescription,
               p.COMMENTTEXT4 = l_sParamDescription,
               p.COMMENTTEXT5 = l_sParamDescription,
               p.ISCELLPHONENUMBERCONFIRMED = 0,
               p.HASDRIVINGLICENSE = 0      
        where p.personid = l_tParamPrimaryKeyID;
         
        
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
         -- Log entry in log table
         l_sMessageText := 'Person: ' || p_nPersonId || ' nicht gefunden';
         p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
       WHEN OTHERS THEN
         -- Log entry in log table
         l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Person: ' || p_nPersonId;
         p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
       
    END p$SL_ANONPERSON$PERSONID;
    
    PROCEDURE p$SL_ANONPERSON$CONTRACTID
    (
       p_nContractId             IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
    
    l_tParamPrimaryKeyID sl_person.personid%Type;
     
    Cursor c1 (c_nContractId NUMBER) is (select PERSONID from SL_PERSON WHERE CONTRACTID =  c_nContractId);

    BEGIN
      
      FOR oObject IN c1(p_nContractId) LOOP
        l_tParamPrimaryKeyID := oObject.PERSONID;
        p$SL_ANONPERSON$PERSONID(l_tParamPrimaryKeyID, p_nClientId , p_nProtId);
      END LOOP;
      EXCEPTION
       WHEN NO_DATA_FOUND THEN
         -- Log entry in log table
         l_sMessageText := 'Keine Person zu Vertrag: ' || p_nContractId || ' gefunden';
         p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
       WHEN OTHERS THEN
         -- Log entry in log table
         l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Person zu Vertrag: ' || p_nContractId;
         p$AddProtocolMessage(l_sMessageText, p_nClientId, p_nProtId);
    END p$SL_ANONPERSON$CONTRACTID;
    
    PROCEDURE p$SL_ANONYMIZECONTRACT
    (
       p_sContractNo             IN VARCHAR,
       p_nClientId               IN NUMBER
    )
    IS
    
    l_tParamContractID pp_contract.contractid%Type;
    

    BEGIN
        
        l_nProtId := getDataId();
        P_CreateprotocolClient(l_nProtId, 7000, sysdate, 'FVS', 0, p_nClientId);
        commit;
        -- Find contract
        select contractid into l_tParamContractID from sl_contract where contractnumber = p_sContractNo;
        l_sMessageText := 'Vertrag mit ID: ' || l_tParamContractID || ' wird anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
        
         -- Anonymize dependent data
        l_sMessageText := 'Einträge aus der Tabelle SL_PERSON von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
        p$SL_ANONPERSON$CONTRACTID(l_tParamContractID, p_nClientId, l_nProtId );
        l_sMessageText := 'Einträge aus der Tabelle SL_CUSTOMERACCOUNT von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
        p$SL_ANONCUSTOMERACOUNT$CONID(l_tParamContractID, p_nClientId, l_nProtId );
        l_sMessageText := 'Einträge aus der Tabelle SL_ORDER von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
        p$SL_ANONORDERSTOCONTRACT(l_tParamContractID, p_nClientId, l_nProtId );
        l_sMessageText := 'Einträge aus der Tabelle SL_COMMENT von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
        p$SL_ANONCOMMENTS(l_tParamContractID, p_nClientId, l_nProtId );
        l_sMessageText := 'Einträge aus der Tabelle SL_CONTRACTHISTORY von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
        p$SL_ANONHISTORYTOCONTRACT(l_tParamContractID, p_nClientId, l_nProtId );
        l_sMessageText := 'Einträge aus der Tabelle SL_ORGANIZATION von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
        p$SL_ANONORGANIZAION$CONTRACT(l_tParamContractID, p_nClientId, l_nProtId );
        l_sMessageText := 'Einträge aus der Tabelle SL_CONTRACTADDRESS->SL_ADDRESS von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
        p$SL_ANONCONADDRESSTOCON(l_tParamContractID, p_nClientId, l_nProtId );
        
  
        l_sMessageText := 'Attribute von Vertrag mit ID: ' || l_tParamContractID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
         update   sl_contract 
            set   --CONTRACTNUMBER = 0,
                  VALIDFROM = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
                  VALIDTO = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),                  
                  LASTUSER =  l_sParamDescription,
                  LASTMODIFIED =  TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
                  CLIENTAUTHENTICATIONTOKEN =  l_sParamDescription,         
                  ADDITIONALTEXT = l_sParamDescription,
				  State = 4, --Anonymized
                  TRANSACTIONCOUNTER = TRANSACTIONCOUNTER  + 1               
            where contractid = l_tParamContractID;
         
        
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
         -- Log entry in log table
         l_sMessageText := 'Vertrag: ' || p_sContractNo || ' nicht gefunden';
         p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
       WHEN OTHERS THEN
         -- Log entry in log table
         l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Vertrag: ' || l_tParamContractID;
         p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
       
    END p$SL_ANONYMIZECONTRACT;
    
    PROCEDURE p$SL_ANONYMIZECONTRACTID
    (
       p_nContractId             IN NUMBER,
       p_nClientId               IN NUMBER
    )
    IS
    
    l_tParamContractNo sl_contract.CONTRACTNUMBER%Type;
    

    BEGIN
        
        -- Find contract
        select SL_CONTRACT.CONTRACTNUMBER into l_tParamContractNo from sl_contract where contractid = p_nContractId;
        p$SL_ANONYMIZECONTRACT(l_tParamContractNo, p_nClientId);
        
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
         RETURN;
       WHEN OTHERS THEN
         RAISE;
       
    END p$SL_ANONYMIZECONTRACTID;
    
    PROCEDURE p$SL_ANONCUSTOMERACOUNT$PERSID
    (
       p_nPersonId               IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1 
    )
    IS
      l_tParamPrimaryKeyID SL_CUSTOMERACCOUNT.CUSTOMERACCOUNTID%Type;
    BEGIN
       select CUSTOMERACCOUNTID into l_tParamPrimaryKeyID from SL_CUSTOMERACCOUNT where PERSONID = p_nPersonId;
       p$SL_ANONCUSTOMERACOUNT$ID(l_tParamPrimaryKeyID,  p_nClientId, p_nProtId); 
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
         -- Log entry in log table
         l_sMessageText := 'KundenAccount zu PersonId: ' || p_nPersonId || ' nicht gefunden';
         p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
       WHEN OTHERS THEN
         -- Log entry in log table
         l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von KundenAccount: ' || l_tParamPrimaryKeyID;
         p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
    
    END p$SL_ANONCUSTOMERACOUNT$PERSID;

    PROCEDURE p$SL_ANONCUSTOMERACOUNT$CONID
    (
       p_nContractId             IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
      l_tParamPrimaryKeyID SL_CUSTOMERACCOUNT.CUSTOMERACCOUNTID%Type;
    BEGIN
       select CUSTOMERACCOUNTID into l_tParamPrimaryKeyID from SL_CUSTOMERACCOUNT where CONTRACTID = p_nContractId;
       p$SL_ANONCUSTOMERACOUNT$ID(l_tParamPrimaryKeyID,  p_nClientId, p_nProtId); 
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
         -- Log entry in log table
         l_sMessageText := 'KundenAccount zu Vertrag: ' || p_nContractId || ' nicht gefunden';
         p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
       WHEN OTHERS THEN
         -- Log entry in log table
         l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von KundenAccount: ' || l_tParamPrimaryKeyID;
         p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
    END p$SL_ANONCUSTOMERACOUNT$CONID;

    PROCEDURE p$SL_ANONCUSTOMERACOUNT$ID
    (
       p_nCustomerAccountId      IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_CUSTOMERACCOUNT.CUSTOMERACCOUNTID%Type;
       l_tParamPersonPrimaryKeyID SL_PERSON.PERSONID%Type;
    BEGIN
       
       -- Find contract
       select CUSTOMERACCOUNTID, PERSONID into l_tParamPrimaryKeyID, l_tParamPersonPrimaryKeyID from SL_CUSTOMERACCOUNT where CUSTOMERACCOUNTID = p_nCustomerAccountId;
       
        l_sMessageText := 'Einträge aus der Tabelle SL_PERSON von KundenAccount mit ID: ' || p_nCustomerAccountId  || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
        p$SL_ANONPERSON$PERSONID(l_tParamPersonPrimaryKeyID, p_nClientId, l_nProtId );
       
       l_sMessageText := 'KudenAccount mit ID: ' || l_tParamPrimaryKeyID || ' wird anonymisiert';
       p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
       
       
       update SL_CUSTOMERACCOUNT 
          set  USERNAME = l_sParamDescription,
               PASSWORD = l_sParamDescription,
               PHONEPASSWORD = l_sParamDescription,
               LASTUSER = l_sParamDescription,
               LASTMODIFIED =  TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               PASSWORDCHANGED =  TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               VERIFICATIONTOKEN = l_sParamDescription,
               VERIFICATIONTOKENEXPIRYTIME =  TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               ISPASSWORDTEMPORARY = 0,
               TRANSACTIONCOUNTER = TRANSACTIONCOUNTER  + 1               
         where CUSTOMERACCOUNTID = l_tParamPrimaryKeyID;
         
         
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Kundenaccount: ' || p_nCustomerAccountId || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von KundenAccount: ' || p_nCustomerAccountId;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONCUSTOMERACOUNT$ID;
    
    PROCEDURE p$SL_ANONORDER
    (
       p_nOrderId                IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_ORDER.ORDERID%Type;
       l_tParamInvoicePrimaryKeyID SL_ADDRESS.ADDRESSID%Type;
       l_tParamShippingPrimaryKeyID SL_ADDRESS.ADDRESSID%Type;
       
    BEGIN
       
       -- Find contract
       select ORDERID, INVOICEADDRESSID, SHIPPINGADDRESSID  into l_tParamPrimaryKeyID, l_tParamInvoicePrimaryKeyID, l_tParamShippingPrimaryKeyID from SL_ORDER where ORDERID = p_nOrderId;
       l_sMessageText := 'Order mit ID: ' || l_tParamPrimaryKeyID || ' wird anonymisiert';
       p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
       
       -- Anonymize dependent data
       l_sMessageText := 'Einträge aus der Tabelle SL_ADDRESS von Order mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
       p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
       p$SL_ANONYMIZEADDRESS(l_tParamInvoicePrimaryKeyID,p_nClientId,l_nProtId);
       p$SL_ANONYMIZEADDRESS(l_tParamShippingPrimaryKeyID,p_nClientId,l_nProtId);
       
       l_sMessageText := 'Einträge aus der Tabelle SL_ORDERDETAIL von Order mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
       p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
       p$SL_ANONORDERDETAILTOORDER(l_tParamPrimaryKeyID,p_nClientId,l_nProtId);
       
       l_sMessageText := 'Einträge aus der Tabelle SL_ORDERHISTORY von Order mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
       p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
       p$SL_ANONHISTORYTOORDER(l_tParamPrimaryKeyID,p_nClientId,l_nProtId);
      
       
       update SL_ORDER 
          set  ORDERNUMBER = l_sParamDescription,
               FULFILLDATE = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               ISMAIL = 1,
               VOUCHERNUMBER = l_sParamDescription,
               LASTUSER = l_sParamDescription,
               LASTMODIFIED = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               TRANSACTIONCOUNTER = TRANSACTIONCOUNTER  + 1               
         where ORDERID = l_tParamPrimaryKeyID;
         
         
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Order: ' || p_nOrderId  || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Order: ' || p_nOrderId ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONORDER;

    PROCEDURE p$SL_ANONORDERSTOCONTRACT
    (
       p_nContractId             IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_ORDER.ORDERID%Type;
       
       Cursor c1 (c_nOrderId NUMBER) is (select orderid from SL_ORDER WHERE CONTRACTID =  c_nOrderId);
    BEGIN
       
        FOR nOrder IN c1(p_nContractId) LOOP
          p$SL_ANONORDER(nOrder.orderid, p_nClientId, p_nProtId);
        END LOOP;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Keine Order zu Vertrag : ' || p_nContractId  || ' gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Orders zu Vertrag: ' || p_nContractId;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONORDERSTOCONTRACT;

    PROCEDURE p$SL_ANONORDERDETAIL
    (
       p_nOrderDetailId          IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_ORDERDETAIL.ORDERDETAILID%Type;
       l_tParamReqOrderPrimaryKeyID SL_ORDERDETAIL.ORDERDETAILID%Type;
       l_tParamProductPrimaryKeyID SL_PRODUCT.PRODUCTID%Type;
       l_tParamPaymentPrimaryKeyID SL_PAYMENT.PAYMENTID%Type;
       
    BEGIN
       
       -- Find contract
       select ORDERDETAILID, PRODUCTID, REQUIREDORDERDETAILID, PAYMENTID  into l_tParamPrimaryKeyID,  l_tParamProductPrimaryKeyID, l_tParamReqOrderPrimaryKeyID, l_tParamPaymentPrimaryKeyID from SL_ORDERDETAIL where ORDERDETAILID = p_nOrderDetailId;
       l_sMessageText := 'Orderdetail mit ID: ' || l_tParamPrimaryKeyID || ' wird anonymisiert';
       p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
       
       update SL_ORDERDETAIL 
          set  LASTMODIFIED = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               LASTUSER = l_sParamDescription,
               TRANSACTIONCOUNTER = TRANSACTIONCOUNTER  + 1               
         where ORDERDETAILID = l_tParamPrimaryKeyID;
         
         
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Orderdetail: ' || p_nOrderDetailId  || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Orderdetail: ' || l_tParamPrimaryKeyID ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONORDERDETAIL;
    
    PROCEDURE p$SL_ANONORDERDETAILTOORDER
    (
       p_nOrderId                IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_ORDER.ORDERID%Type;
       
       Cursor c1 (c_nOrderId NUMBER) is (select OrderDetailid from SL_ORDERDetail WHERE ORDERID =  c_nOrderId);
       
    BEGIN
       
       -- Find contract
       select ORDERID into l_tParamPrimaryKeyID from SL_ORDER where ORDERID = p_nOrderId;
       
        FOR nOrderDetail IN c1(l_tParamPrimaryKeyID) LOOP
           p$SL_ANONORDERDETAIL(nOrderDetail.orderdetailid, p_nClientId, p_nProtId);
        END LOOP;
      
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Orderdetail zu Order: ' || p_nOrderId  || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Orderdetail: ' || l_tParamPrimaryKeyID ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONORDERDETAILTOORDER;
    
    PROCEDURE p$SL_ANONPHOTOGRAPH
    (
        p_nPhotographId           IN NUMBER,
        p_nClientId               IN NUMBER,
        p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_PHOTOGRAPH.PHOTOGRAPHID%Type;
       
    BEGIN
       
       -- Find photograph
       select PHOTOGRAPHID into l_tParamPrimaryKeyID from SL_PHOTOGRAPH where PHOTOGRAPHID = p_nPhotographId;
       
       UPDATE SL_PHOTOGRAPH SET
         PHOTOFILE = empty_blob(),
         CAPTION = l_sParamDescription,
         LASTUSER = l_sParamDescription,
         LASTMODIFIED  = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
         TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1
       WHERE PHOTOGRAPHID  = l_tParamPrimaryKeyID;
       
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Photo mit id: ' || p_nPhotographId  || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
          WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Photo: ' || l_tParamPrimaryKeyID ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONPHOTOGRAPH;
    
    PROCEDURE p$SL_ANONPHOTOSTOPERSON
    (
       p_nPersonId               IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_PHOTOGRAPH.PHOTOGRAPHID%Type;
       
       Cursor c1 (c_nPhotoId NUMBER) is (select PHOTOGRAPHID from SL_PHOTOGRAPH WHERE PERSONID =  c_nPhotoId);
    BEGIN
       
        FOR oOBject IN c1(p_nPersonId ) LOOP
          p$SL_ANONPHOTOGRAPH(oOBject.PHOTOGRAPHID, p_nClientId, p_nProtId);
        END LOOP;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Keine Photos zu Person : ' || p_nPersonId  || ' gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Photos zu Person: ' || p_nPersonId;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONPHOTOSTOPERSON;
    
    PROCEDURE p$SL_ANONCOMMENTS
    (
        p_nContractId             IN NUMBER,
        p_nClientId               IN NUMBER,
        p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_COMMENT.COMMENTID%Type;
       Cursor c1 (c_nContractId NUMBER) is (select COMMENTID from SL_COMMENT WHERE CONTRACTID =  c_nContractId);
    BEGIN
       
       FOR oOBject IN c1(p_nContractId) LOOP
           -- Log entry in log table
           l_tParamPrimaryKeyID := oOBject.COMMENTID;
           l_sMessageText := 'Kommentar zu Vertrag mit id: ' || oOBject.COMMENTID  || ' anonymisiert';
           p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           UPDATE SL_COMMENT SET
              TEXT  = l_sParamDescription,
              LASTUSER  = l_sParamDescription,
              LASTMODIFIED = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
              TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1
           WHERE COMMENTID  = oOBject.COMMENTID;
       END LOOP;
       
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Kommentar zu Vertrag mit id: ' || p_nContractId  || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
          WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Kommentar: ' || l_tParamPrimaryKeyID ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONCOMMENTS;

    PROCEDURE p$SL_ANONCONTRACTHISTORY
    (
        p_nContractHistoryId      IN NUMBER,
        p_nClientId               IN NUMBER,
        p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_CONTRACTHISTORY.CONTRACTHISTORYID%Type;
       
    BEGIN
       
       -- Find 
       select CONTRACTHISTORYID into l_tParamPrimaryKeyID from SL_CONTRACTHISTORY where CONTRACTHISTORYID = p_nContractHistoryId;
       l_sMessageText := 'Vertragshistorie mit id: ' || p_nContractHistoryId   || ' wird anonymisiert';
       p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
       UPDATE SL_CONTRACTHISTORY SET
          TEXT  = l_sParamDescription,
          LASTUSER  = l_sParamDescription,
          LASTMODIFIED = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
          TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1
       WHERE CONTRACTHISTORYID  = l_tParamPrimaryKeyID;
       
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Vertragshistorie mit id: ' || p_nContractHistoryId  || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
          WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Vertragshistorie: ' || l_tParamPrimaryKeyID ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONCONTRACTHISTORY;
    

    PROCEDURE p$SL_ANONHISTORYTOCONTRACT
    (
        p_nContractId             IN NUMBER,
        p_nClientId               IN NUMBER,
        p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_CONTRACTHISTORY.CONTRACTHISTORYID%Type;
       
       Cursor c1 (c_nContractId NUMBER) is (select CONTRACTHISTORYID from SL_CONTRACTHISTORY WHERE CONTRACTID =  c_nContractId);
    BEGIN
       
        FOR oOBject IN c1(p_nContractId) LOOP
          p$SL_ANONCONTRACTHISTORY(oOBject.CONTRACTHISTORYID, p_nClientId, p_nProtId);
        END LOOP;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Keine Vertragshistorie zu Vertrag: ' || p_nContractId  || ' gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Vertrgashistorie zu Vertrag: ' || p_nContractId;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONHISTORYTOCONTRACT;
    
    PROCEDURE p$SL_ANONENTITLEMENT
    (
       p_nEntitlementId          IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_ENTITLEMENT.ENTITLEMENTID%Type;
       
    BEGIN
       
       -- Find 
       select ENTITLEMENTID into l_tParamPrimaryKeyID from SL_ENTITLEMENT where ENTITLEMENTID = p_nEntitlementId;
       
       UPDATE SL_ENTITLEMENT SET
          VALIDFROM     = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
            VALIDTO     = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
           DOCUMENT     = l_sParamDescription,
           LASTUSER     = l_sParamDescription,
           LASTMODIFIED = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
           TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1
       WHERE ENTITLEMENTID  = l_tParamPrimaryKeyID;
       
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Entitlement id: ' || p_nEntitlementId  || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
          WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Entitlement: ' || l_tParamPrimaryKeyID ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONENTITLEMENT;
    
    PROCEDURE p$SL_ANONENTITLEMENTOFPERSON
    (
       p_nPersonId               IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_ENTITLEMENT.ENTITLEMENTID%Type;
       
       Cursor c1 (c_nPersonId NUMBER) is (select ENTITLEMENTID from SL_ENTITLEMENT WHERE PERSONID =  c_nPersonId);
       
    BEGIN
       
       -- Find 
       --select ENTITLEMENTID into l_tParamPrimaryKeyID from SL_ENTITLEMENT where PERSONID = p_nPersonId;
       FOR oOBject IN c1(p_nPersonId) 
       LOOP
            -- Log entry in log table
           l_tParamPrimaryKeyID := oOBject.ENTITLEMENTID;
           l_sMessageText := 'Entitlement id: ' || oOBject.ENTITLEMENTID  || ' anonymisiert';
           p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           UPDATE SL_ENTITLEMENT SET
              VALIDFROM     = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
                VALIDTO     = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               DOCUMENT     = l_sParamDescription,
               LASTUSER     = l_sParamDescription,
               LASTMODIFIED = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1
           WHERE ENTITLEMENTID  = oOBject.ENTITLEMENTID;
       END LOOP;
       
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Entitlement zu Peron mit id: ' || p_nPersonId  || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
          WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Entitlement: ' || l_tParamPrimaryKeyID ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONENTITLEMENTOFPERSON;
    
    PROCEDURE p$SL_ANONPRODUCT
    (
        p_nProductId              IN NUMBER,
        p_nClientId               IN NUMBER,
        p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_PRODUCT.PRODUCTID%Type;
       l_tParamREfundPrimaryKeyID SL_REFUND.REFUNDID%Type;
       
    BEGIN
       
       -- Find 
       select PRODUCTID, REFUNDID into l_tParamPrimaryKeyID, l_tParamREfundPrimaryKeyID from SL_PRODUCT where PRODUCTID = p_nProductId;
       
        -- Anonymize dependent data
       l_sMessageText := 'Einträge aus der Tabelle SL_REFUNDS von Produkt mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
       p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
       p$SL_ANONREFUND(l_tParamREfundPrimaryKeyID,p_nClientId,p_nProtId);
       
       UPDATE SL_PRODUCT SET
             TICKETTYPE          = 0,
             VALIDFROM           = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
             VALIDTO             = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
             ISPAID              = 0,
             ISCLEARED           = 0,
             ISCANCELED          = 0,
             ISLOADED            = 0,
             VALIDATEWHENSELLING = 0,
             PRICE               = 0,
             INSTANCENUMBER      = 0,
             RELATIONFROM        = 0,
             RELATIONTO          = 0,
             REMAININGVALUE      = 0,
             FILLLEVEL           = 0,
             GROUPSIZE           = 0,
             EXPIRY              = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
             TRIPSERIALNUMBER    = 0,
             LASTUSER            = l_sParamDescription,
             LASTMODIFIED        = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
             ISBLOCKED           = 0,
             BLOCKINGREASON      = l_sParamDescription,
             BLOCKINGDATE        = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
             CANCELLATIONDATE    = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
             BLOCKINGREASONENUM  = 0,
             DISTANCE            = 0,
             VDVENTNUMBER        = 0,
             MINIMUMTERM         = 0,
             VDVENTITLEMENTDATA  = empty_blob(),
             TRANSACTIONCOUNTER  = TRANSACTIONCOUNTER + 1
       WHERE PRODUCTID  = l_tParamPrimaryKeyID;
       
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Produkt mit id: ' || p_nProductId  || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
          WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Produkt: ' || l_tParamPrimaryKeyID ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONPRODUCT;
    

    PROCEDURE p$SL_ANONPRODUCTSTOCARD
    (
        p_nCardId                 IN NUMBER,
        p_nClientId               IN NUMBER,
        p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_CARD.CARDID%Type;
       
       Cursor c1 (c_nCardId NUMBER) is (select PRODUCTID from SL_PRODUCT WHERE CARDID =  c_nCardId);
    BEGIN
       
        FOR oOBject IN c1(p_nCardId) LOOP
          p$SL_ANONPRODUCT(oOBject.PRODUCTID, p_nClientId, p_nProtId);
        END LOOP;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Keine Produkte zu Karte: ' || p_nCardId  || ' gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Produkten zu Karte: ' || p_nCardId;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONPRODUCTSTOCARD;
    
    PROCEDURE p$SL_ANONREFUND
    (
        p_nRefundId               IN NUMBER,
        p_nClientId               IN NUMBER,
        p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_REFUND.REFUNDID%Type;
       
    BEGIN
       
       -- Find 
       select REFUNDID into l_tParamPrimaryKeyID from SL_REFUND where REFUNDID = p_nRefundId;
       
       UPDATE SL_REFUND SET
          REFUNDCOMMENT     = l_sParamDescription,         
          LASTUSER          = l_sParamDescription,
          LASTMODIFIED      = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
          TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1
       WHERE REFUNDID  = l_tParamPrimaryKeyID;
       
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Refund mit id: ' || p_nRefundId  || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
          WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Refund: ' || l_tParamPrimaryKeyID ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONREFUND;

    PROCEDURE p$SL_ANONORDERHISTORY
    (
        p_nOrderhistoryId         IN NUMBER,
        p_nClientId               IN NUMBER,
        p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_ORDERHISTORY.ORDERHISTORYID%Type;
       
    BEGIN
       
       -- Find 
       select ORDERHISTORYID into l_tParamPrimaryKeyID from SL_ORDERHISTORY where ORDERHISTORYID = p_nOrderhistoryId;
       
       UPDATE SL_ORDERHISTORY SET
           MESSAGE          = l_sParamDescription,         
          LASTUSER          = l_sParamDescription,
          LASTMODIFIED      = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
          TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1
       WHERE ORDERHISTORYID  = l_tParamPrimaryKeyID;
       
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Orderhistorie mit id: ' || p_nOrderhistoryId  || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
          WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Orderhistorie: ' || l_tParamPrimaryKeyID ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONORDERHISTORY;
    

    PROCEDURE p$SL_ANONHISTORYTOORDER
    (
        p_nOrderId                IN NUMBER,
        p_nClientId               IN NUMBER,
        p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_ORDER.ORDERID%Type;
       
       Cursor c1 (c_nOrderId NUMBER) is (select ORDERHISTORYID from SL_ORDERHISTORY WHERE ORDERID =  c_nOrderId);
    BEGIN
       
        FOR oOBject IN c1(p_nOrderId) LOOP
          p$SL_ANONORDERHISTORY(oOBject.ORDERHISTORYID, p_nClientId, p_nProtId);
        END LOOP;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Keine Orderhistorie zu Order: ' || p_nOrderId  || ' gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Orderhistorie zu Order: ' || p_nOrderId;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONHISTORYTOORDER;

    PROCEDURE p$SL_ANONENTITLEMENTTOPRODUCT
    (
        p_nProductId              IN NUMBER,
        p_nClientId               IN NUMBER,
        p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS
       l_tParamPrimaryKeyID SL_PRODUCT.PRODUCTID%Type;
       
       Cursor c1 (c_nProductId NUMBER) is (select ENTITLEMENTID from SL_ENTITLEMENTTOPRODUCT WHERE PRODUCTID =  c_nProductId);
    BEGIN
       
        FOR oOBject IN c1(p_nProductId) LOOP
          p$SL_ANONENTITLEMENT(oOBject.ENTITLEMENTID, p_nClientId, p_nProtId);
        END LOOP;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Keine Entitlements zu Produkt: ' || p_nProductId  || ' gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Entitlements zu Produkt: ' || p_nProductId;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONENTITLEMENTTOPRODUCT;
    
    PROCEDURE p$SL_ANONVOUCHER
    (
        p_nVoucherId              IN NUMBER,
        p_nClientId               IN NUMBER,
        p_nProtId                 IN NUMBER DEFAULT -1
    )
     IS
       l_tParamPrimaryKeyID SL_VOUCHER.VOUCHERID%Type;
       
    BEGIN
       
       -- Find 
       select VOUCHERID into l_tParamPrimaryKeyID from SL_VOUCHER where VOUCHERID = p_nVoucherId;
       
       UPDATE SL_VOUCHER SET
         
          VOUCHERCODE          = l_sParamDescription,
          CREATEDON            = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
          CREATEDBY            = l_sParamDescription, 
          TICKETVALIDFROM      = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
          TICKETVALIDTO        = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
          REDEEMABLEFROM       = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
          REDEEMABLETO         = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
          REDEEMEDON           = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
          REDEEMEDBY           = l_sParamDescription,
          LASTUSER             = l_sParamDescription,
          LASTMODIFIED         = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
          TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1
       WHERE VOUCHERID  = l_tParamPrimaryKeyID;
       
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Voucher mit id: ' || p_nVoucherId  || ' nicht gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
          WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Voucher: ' || l_tParamPrimaryKeyID ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONVOUCHER;

    PROCEDURE p$SL_ANONVOUCHERTOCARD
    (
        p_nCardId                 IN NUMBER,
        p_nClientId               IN NUMBER,
        p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS 
       Cursor c1 (c_nCardId NUMBER) is (select VOUCHERID from SL_VOUCHER WHERE REDEEMEDFORCARDID =  p_nCardId);
    BEGIN
       
        FOR oOBject IN c1(p_nCardId) LOOP
          p$SL_ANONVOUCHER(oOBject.VOUCHERID, p_nClientId, p_nProtId);
        END LOOP;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Kein Voucher zu Kartet: ' || p_nCardId || ' gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Voucher zu Karte: ' || p_nCardId;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONVOUCHERTOCARD;
    
    PROCEDURE p$SL_ANONCARD$CARDHOLDER
    (
       p_nCardHolderId           IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS 
       Cursor c1 (c_nCardHolderId NUMBER) is (select CARDID from SL_CARD WHERE CARDHOLDERID =  c_nCardHolderId);
    BEGIN
       
        FOR oOBject IN c1(p_nCardHolderId) LOOP
          UPDATE SL_CARD SET BLOCKINGCOMMENT = l_sParamDescription, transactioncounter = transactioncounter + 1 WHERE CARDID = oOBject.CARDID;
        END LOOP;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Kein Karte zum Karteninhaber: ' || p_nCardHolderId || ' gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Karte zu Karteninhaber: ' || p_nCardHolderId;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONCARD$CARDHOLDER;
    
    PROCEDURE p$SL_ANONORGANIZAION$CONTRACT
    (
       p_nContractId             IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS 
       
    l_nNumberOFOrganizations NUMBER := 0;
    l_tParamOrganPrimaryKeyID SL_ORGANIZATION.ORGANIZATIONID%Type;
       
    BEGIN
    
           SELECT NVL(ORGANIZATIONID,0) INTO l_tParamOrganPrimaryKeyID FROM SL_CONTRACT WHERE CONTRACTID = p_nContractId ; 
           
           SELECT COUNT(NVL(ORGANIZATIONID,0)) INTO  l_nNumberOFOrganizations FROM SL_CONTRACT WHERE ORGANIZATIONID = l_tParamOrganPrimaryKeyID;
           
           IF l_nNumberOFOrganizations > 1 
           THEN
              UPDATE SL_CONTRACT SET ORGANIZATIONID = null, TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1 WHERE CONTRACTID = p_nContractId;
           ELSE
              p$SL_ANONORGANIZATION(l_tParamOrganPrimaryKeyID,p_nClientId,p_nProtId);
           END IF;
       
           EXCEPTION
               WHEN NO_DATA_FOUND THEN
                 -- Log entry in log table
                 l_sMessageText := 'Kein Organisation Zu Vertrag mit ID: ' || p_nContractId  || ' gefunden';
                 p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
               WHEN OTHERS THEN
                 -- Log entry in log table
                 l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Organisationen zu Vertrag mit ID: ' || p_nContractId ;
                 p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONORGANIZAION$CONTRACT;
    
    PROCEDURE p$SL_ANONORGANIZATION
    (
       p_nOrganizationId         IN NUMBER,
       p_nClientId               IN NUMBER,
       p_nProtId                 IN NUMBER DEFAULT -1
    )
    IS 
       
    l_tParamPrimaryKeyID SL_ORGANIZATION.ORGANIZATIONID%Type;
    l_tParamFOPrimaryKeyID SL_ORGANIZATION.ORGANIZATIONID%Type;
    l_tParamPersonPrimaryKeyID SL_PERSON.PERSONID%Type;
       
    BEGIN
    
       SELECT ORGANIZATIONID, CONTACTPERSONID, FRAMEORGANIZATIONID INTO l_tParamPrimaryKeyID, l_tParamPersonPrimaryKeyID, l_tParamFOPrimaryKeyID FROM SL_ORGANIZATION WHERE ORGANIZATIONID = p_nOrganizationId; 
       
        -- Anonymize dependent data
        l_sMessageText := 'Einträge aus der Tabelle SL_PERSON von Organisation mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
        p$SL_ANONPERSON$PERSONID(l_tParamPersonPrimaryKeyID, p_nClientId, l_nProtId );
        
        -- Anonymize dependent data
        l_sMessageText := 'Einträge aus der Tabelle SL_ORGANIZATION von Organisation mit ID: ' || l_tParamPrimaryKeyID || ' werden anonymisiert';
        p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
        p$SL_ANONORGANIZATION(l_tParamFOPrimaryKeyID, p_nClientId, l_nProtId );
         
        UPDATE SL_ORGANIZATION
           SET NAME                 = l_sParamDescription,
               ABBREVIATION         = l_sParamDescription,
               DESCRIPTION          = l_sParamDescription,
               EXTERNALNUMBER       = l_sParamDescription,
               LASTUSER             = l_sParamDescription,
               LASTMODIFIED         = TO_DATE(l_sMinDate, 'DD.MM.YYYY'),
               TRANSACTIONCOUNTER   = TRANSACTIONCOUNTER + 1,
               ORGANIZATIONNUMBER   = 0,
               PRINTNAME            = l_sParamDescription,
               SUBTYPE              = 0
        WHERE ORGANIZATIONID = l_tParamPrimaryKeyID;
        
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Kein Organisation mit ID: ' || p_nOrganizationId  || ' gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Organisation mit ID: ' || l_tParamPrimaryKeyID ;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONORGANIZATION;
    
    PROCEDURE p$SL_ANONCONADDRESSTOCON 
    (
      p_nContractId             IN NUMBER,
      p_nClientId               IN NUMBER,
      p_nProtId                 IN NUMBER DEFAULT -1
    )
     IS 
       Cursor c1 (c_nContractId NUMBER) is (select ADDRESSID from SL_CONTRACTADDRESS WHERE CONTRACTID =  c_nContractId);
    BEGIN
       
        FOR oOBject IN c1(p_nContractId) LOOP
          p$SL_ANONYMIZEADDRESS(oOBject.ADDRESSID, p_nClientId, p_nProtId);
        END LOOP;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
             -- Log entry in log table
             l_sMessageText := 'Keine Adressen zu Vertrag: ' ||  p_nContractId || ' gefunden';
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
           WHEN OTHERS THEN
             -- Log entry in log table
             l_sMessageText := 'Fehlercode: ' || SQLCODE || ' beim Anonymisieren von Adressen zu Vertrag: ' ||  p_nContractId;
             p$AddProtocolMessage(l_sMessageText, p_nClientId, l_nProtId);
      
    END p$SL_ANONCONADDRESSTOCON ;    

    
End DSGVO;
/


COMMIT;

PROMPT Done!

SPOOL OFF;
SPOOL db_3_682.log;
