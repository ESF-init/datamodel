SPOOL db_3_588.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.588', 'ARH', 'Added Paratransit tables');

CREATE TABLE SL_PARATRANSJOURNAL
(
    PARATRANSJOURNALID   				NUMBER(18,0)    NOT NULL,
	PARATRANSJOURNALTYPE 				NUMBER(9,0) NOT NULL,
	PARATRANSPAYMENTTYPE 				NUMBER(9,0) ,
	DEVICETIME  						DATE    NOT NULL,
	OPERATORID 							NUMBER(18,0),
	OPERATORCOMPANYID 					NUMBER(18,0),
	VEHICLENUMBER 						NUMBER(18,0),
	EXTERNALTRIPID 						VARCHAR2(50),
	EXTERNALRIDERID 					VARCHAR2(50),
	FARETRANSACTIONID 					NVARCHAR2(50),
	FAREMETER 							NUMBER(18,0),
	LATITUDE 							NUMBER(20,10),
	LONGITUDE 							NUMBER(20,10),
	ODOMETER 							NUMBER(18,0),
	SHAREDRIDEID 						VARCHAR2(50),
	LASTUSER NVARCHAR2(50) 				DEFAULT 'SYS' NOT NULL, 
    LASTMODIFIER 						DATE DEFAULT sysdate NOT NULL, 
    TRANSACTIONCOUNTER 					INTEGER NOT NULL,
	CONSTRAINT PK_PARATRANSJOURNAL	 	PRIMARY KEY (PARATRANSJOURNALID)
);

CREATE TABLE SL_PARATRANSJOURNALTYPE
(
    ENUMERATIONVALUE        			NUMBER(9,0) NOT NULL,
    DESCRIPTION                    		NVARCHAR2(50) NOT NULL,
    CONSTRAINT PK_PARATRANSJOURNALTYPE 	PRIMARY KEY (ENUMERATIONVALUE)
);

CREATE TABLE SL_PARATRANSPAYMENTTYPE
(
    ENUMERATIONVALUE        			NUMBER(9,0) NOT NULL,
    DESCRIPTION                    		VARCHAR2(50) NOT NULL,
    CONSTRAINT PK_PARATRANSITPAYMENTTYPE PRIMARY KEY (ENUMERATIONVALUE)
);

CREATE TABLE SL_PARATRANSATTRIBUTE
(
    PARATRANSATTRIBUTEID   					NUMBER(18,0)    NOT NULL,
	PARATRANSATTRIBUTETYPEID   				NUMBER(18,0)    NOT NULL,
	AMOUNT 									NUMBER(18,0),
	EXTERNALTRIPID 							VARCHAR2(50),
	LASTUSER NVARCHAR2(50) 					DEFAULT 'SYS' NOT NULL, 
    LASTMODIFIER 							DATE DEFAULT sysdate NOT NULL, 
    TRANSACTIONCOUNTER 						INTEGER NOT NULL,
	CONSTRAINT PK_PARATRANSATTRIBUTE	 	PRIMARY KEY (PARATRANSATTRIBUTEID)
);

CREATE TABLE SL_PARATRANSATTRIBUTETYPE
(
    ENUMERATIONVALUE   						NUMBER(18,0)    NOT NULL,
	DESCRIPTION 							VARCHAR2(50),
	COST 									NUMBER(18,0),
	CONSTRAINT PK_PARATRANSATTRIBUTETYPE 	PRIMARY KEY (ENUMERATIONVALUE)
);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

INSERT INTO SL_PARATRANSJOURNALTYPE(ENUMERATIONVALUE, DESCRIPTION) VALUES (1, 'Boarding');
INSERT INTO SL_PARATRANSJOURNALTYPE(ENUMERATIONVALUE, DESCRIPTION) VALUES (2, 'PCA boarding');
INSERT INTO SL_PARATRANSJOURNALTYPE(ENUMERATIONVALUE, DESCRIPTION) VALUES (3, 'End ride');
INSERT INTO SL_PARATRANSJOURNALTYPE(ENUMERATIONVALUE, DESCRIPTION) VALUES (4, 'No show');
INSERT INTO SL_PARATRANSJOURNALTYPE(ENUMERATIONVALUE, DESCRIPTION) VALUES (5, 'Cancellation');

INSERT INTO SL_PARATRANSPAYMENTTYPE(ENUMERATIONVALUE, DESCRIPTION) VALUES (1, 'Fare transaction');
INSERT INTO SL_PARATRANSPAYMENTTYPE(ENUMERATIONVALUE, DESCRIPTION) VALUES (2, 'Cash');
INSERT INTO SL_PARATRANSPAYMENTTYPE(ENUMERATIONVALUE, DESCRIPTION) VALUES (3, 'No funds collected');
INSERT INTO SL_PARATRANSPAYMENTTYPE(ENUMERATIONVALUE, DESCRIPTION) VALUES (4, 'Eligibility center ride');

INSERT INTO SL_PARATRANSATTRIBUTETYPE(ENUMERATIONVALUE, DESCRIPTION, COST) VALUES (1, 'Wheelchair', 0);
INSERT INTO SL_PARATRANSATTRIBUTETYPE(ENUMERATIONVALUE, DESCRIPTION, COST) VALUES (2, 'PersonalCareAssistant',0);
INSERT INTO SL_PARATRANSATTRIBUTETYPE(ENUMERATIONVALUE, DESCRIPTION, COST) VALUES (3, 'Companion',0);
INSERT INTO SL_PARATRANSATTRIBUTETYPE(ENUMERATIONVALUE, DESCRIPTION, COST) VALUES (4, 'ServiceAnimal',0);

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_PARATRANSJOURNAL ADD CONSTRAINT FK_PARATRANSJOURNAL_TYPE FOREIGN KEY (PARATRANSJOURNALTYPE) REFERENCES SL_PARATRANSJOURNALTYPE (ENUMERATIONVALUE);
ALTER TABLE SL_PARATRANSATTRIBUTE ADD CONSTRAINT FK_PARATRANSATTRIBUTE_TYPE FOREIGN KEY (PARATRANSATTRIBUTETYPEID) REFERENCES SL_PARATRANSATTRIBUTETYPE (ENUMERATIONVALUE);

-- =======[ Sequences ]============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_PARATRANSJOURNAL', 'SL_PARATRANSATTRIBUTE');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_PARATRANSJOURNAL', 'SL_PARATRANSATTRIBUTE');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
