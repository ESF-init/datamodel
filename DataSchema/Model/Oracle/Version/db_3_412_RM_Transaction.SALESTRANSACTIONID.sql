SPOOL db_3_412.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.412', 'ULB', 'Add RM_Transaction.SALESTRANSACTIONID');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE RM_TRANSACTION
ADD (SALESTRANSACTIONID VARCHAR2(40 BYTE));

-- =======[ New Tables ]===========================================================================================
COMMENT ON COLUMN RM_TRANSACTION.SALESTRANSACTIONID IS 'References SL_Sale.SaleTransactionId';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
