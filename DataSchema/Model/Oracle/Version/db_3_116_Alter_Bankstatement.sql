SPOOL db_3_116.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.116', 'EPA', 'Alter bank statement table');


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE ACC_BANKSTATEMENT ADD AUTHORIZATIONCODE NVARCHAR2(50);
ALTER TABLE ACC_BANKSTATEMENT ADD CARDBRANDTYPE NUMBER(9,0);
ALTER TABLE ACC_BANKSTATEMENT ADD DISPUTEDATE DATE;
ALTER TABLE ACC_BANKSTATEMENT ADD CHARGEBACKCODE NUMBER(9,0);
ALTER TABLE ACC_BANKSTATEMENT ADD CHARGEBACKDESCRIPTION NVARCHAR2(50);

COMMENT ON COLUMN ACC_BANKSTATEMENT.AUTHORIZATIONCODE IS 'Optional authorization code of the bank statement. Not all statements have got a authorization code.';
COMMENT ON COLUMN ACC_BANKSTATEMENT.CARDBRANDTYPE IS 'Optional card brand type of the bank statement. Not all statements have got a card brand type element.';
COMMENT ON COLUMN ACC_BANKSTATEMENT.DISPUTEDATE IS 'Optional dispute date of the bank statement. Not all statements have got a dispute date.';
COMMENT ON COLUMN ACC_BANKSTATEMENT.CHARGEBACKCODE IS 'Optional charge back code of the bank statement. Only charge back statements have got a chargeback code.';
COMMENT ON COLUMN ACC_BANKSTATEMENT.CHARGEBACKDESCRIPTION IS 'Optional description text of chargarback code.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
