SPOOL db_3.604.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.604', 'LGA', 'New field DataProtectionNoticeAccepted in SL_PERSON.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PERSON
ADD  DataProtectionNoticeAccepted DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL;
COMMENT ON COLUMN SL_Person.DataProtectionNoticeAccepted IS 'Date when the DataProtectionNotice was accepted.'; 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off