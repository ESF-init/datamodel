SPOOL db_3_262.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.262', 'TSC', 'Added DM_Debtor.LockerNumber for IVHH-470.');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE DM_Debtor ADD (LockerNumber VARCHAR2(50));

COMMENT ON COLUMN DM_Debtor.LockerNumber IS 'Number of a locker used by the debtor.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
