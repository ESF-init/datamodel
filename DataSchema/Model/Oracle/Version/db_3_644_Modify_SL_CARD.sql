SPOOL db_3_644.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.644', 'FLF', 'Increased size of SL_CARD.PAYMENTACCOUNTREFERENCE');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CARD
MODIFY(PAYMENTACCOUNTREFERENCE NVARCHAR2(256));

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;