SPOOL db_3_162.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.162', 'DST', 'Made DateOfBirth nullable in SL_OrganizationParticipant');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_OrganizationParticipant MODIFY DateOfBirth DEFAULT NULL NULL;

COMMENT ON COLUMN SL_OrganizationParticipant.CARDID IS 'References SL_CARD.CARDID';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
