SPOOL db_3_389.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.389', 'ARH', 'Add Columns AcceptTermsAndConditions/AcceptNewsletter in SL_Person and SL_ContractToProvider');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CONTRACTTOPROVIDER ADD ACCEPTTERMSANDCONDITIONS DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL;
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.ACCEPTTERMSANDCONDITIONS IS 'The date the end user accepted the terms and conditions of the mobility provider.';

ALTER TABLE SL_PERSON ADD ACCEPTTERMSANDCONDITIONS DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL;
COMMENT ON COLUMN SL_PERSON.ACCEPTTERMSANDCONDITIONS IS 'The date the end user accepted the terms and conditions of the appplication.';

ALTER TABLE SL_PERSON ADD ISNEWSLETTERACCEPTED NUMBER(1) DEFAULT 0 NOT NULL;
COMMENT ON COLUMN SL_PERSON.ISNEWSLETTERACCEPTED IS 'Flag - True if the person agrees to receive newsletters.';


COMMIT;

PROMPT Done!

SPOOL OFF;