SPOOL db_3_277.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.277', 'MMB', 'Creation of SL_DOCUMENTHISTORY');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_DOCUMENTHISTORY
(
    DOCUMENTHISTORYID       NUMBER(18)    NOT NULL,
	DOCUMENTID				NUMBER(18)	NOT NULL,
    DOCUMENTNAME    		VARCHAR2(512) 	NOT NULL, 
	DOCUMENTPATH			VARCHAR2(512) 	NOT NULL,
	DOCUMENTTYPE			NUMBER(18) 		NOT NULL,
	DOCUMENTCONTENT			CLOB			NOT NULL,
	DOCUMENTURL				VARCHAR2(2000)	NOT NULL,
	VERSION					VARCHAR2(50),
	TEXT					VARCHAR2(2000),
	INSTANCECASENUMBER		VARCHAR2(50),
	TSTAMP					VARCHAR2(50),
	PROVIDERTYPE			NUMBER(18) 		NOT NULL,
	LASTUSER            	VARCHAR2(50)   	DEFAULT 'SYS' NOT NULL,
    LASTMODIFIED        	DATE            DEFAULT SYSDATE NOT NULL,
    TRANSACTIONCOUNTER  	INTEGER         NOT NULL
);

ALTER TABLE SL_DOCUMENTHISTORY ADD CONSTRAINT PK_DOCUMENTHISTORY PRIMARY KEY (DOCUMENTHISTORYID);


COMMENT ON TABLE SL_DOCUMENTHISTORY is 'Table containing logged documents in ELO';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.DOCUMENTHISTORYID is 'Unique identifier of the document history';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.DOCUMENTID is 'Identifier of the document stored in ELO';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.DOCUMENTNAME is 'Name of the stored document';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.DOCUMENTPATH is 'Path of the stored document';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.DOCUMENTTYPE is 'Type of the stored document';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.DOCUMENTCONTENT is 'Binary content of the stored document';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.DOCUMENTURL is 'Url of the document stored in ELO';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.VERSION is 'Version number of the document';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.TEXT is 'Textual description of the event';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.INSTANCECASENUMBER is 'Contract number';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.TSTAMP is 'Time stamp provided by the document';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.PROVIDERTYPE is 'Type of the document management provider';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_DOCUMENTHISTORY.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
     'SL_DOCUMENTHISTORY');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_DOCUMENTHISTORY');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;