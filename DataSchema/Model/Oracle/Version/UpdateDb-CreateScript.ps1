﻿<#
.SYNOPSIS
Usually located in O:\Main\Source\VarioSL\WindowsServices\Tests\VarioSL.Services.WindowsServices.Tests\Docker
This script creates a single update script for a targeted database of all sql scripts that have a higher version number. 
The script will be put out in the same folder, its name is UpdateDb.sql
Check the parameters with get-help -detailed for more.
.PARAMETER server
The server connection link as it is typed into sqlplus. This should be in the format username/password@host:port
.PARAMETER sqlLoc
The absolute path to the database update sqls and UpdateDb-getVersion.sql. Do not include a trailing \ By default this uses O:\Main\DataSchema\Model\Oracle\Version
#>
param (
	[string]$server = "fvs/fvs@127.0.0.1:1521",
	[string]$sqlLoc = "O:\Main\DataSchema\Model\Oracle\Version"
)
$checkVersion = $FALSE
While($checkVersion -eq $FALSE){
	echo "Checking version. Ctrl-C to cancel if this takes too long, the database probably isn't working"
	$versionString = Invoke-Expression ("echo exit | sqlplus -s $server @"+$sqlLoc+'\UpdateDb-GetVersion.sql') | Out-String
	$checkVersion = $versionString -match "(?<content>\d+\.\d+)\W*$"
}
$startFrom = $matches['content']
echo "Current Db Version: $startFrom"
'' | Set-Content -Path '.\UpdateDb.sql'
Get-ChildItem $sqlLoc -Filter db_3_*.sql | 
Foreach-Object {
    $result = $_.FullName -match "db_(?<content>\d+_\d+)_.*"
    If ($result -eq $TRUE){
		$result = $matches['content'] -replace "_", "."
        If ($result -gt $startFrom) {
			echo "Loading: $_"			
			Get-Content -Path $_.FullName | Add-Content -Path '.\UpdateDb.sql'                   
        }        
    }
}
echo "File .\UpdateDb.sql is ready"
