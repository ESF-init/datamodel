SPOOL db_3_677.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.677', 'MKG', 'Change SL_JOB EXCEPTIONMESSAGE to nclob.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_JOB ADD (EXCEPTIONMESSAGENCLOB  NCLOB);

UPDATE SL_JOB SET EXCEPTIONMESSAGENCLOB=EXCEPTIONMESSAGE, TRANSACTIONCOUNTER=TRANSACTIONCOUNTER+1;

ALTER TABLE SL_JOB DROP COLUMN EXCEPTIONMESSAGE;

ALTER TABLE SL_JOB RENAME COLUMN EXCEPTIONMESSAGENCLOB TO EXCEPTIONMESSAGE;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
