SPOOL db_3_354.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.354', 'STV', 'Add_Column_WrongPinAttempts_to_SL_CARD');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CARD ADD ( WRONGPINATTEMPT NUMBER(8,0) DEFAULT(0) NOT NULL );

-- =======[ New Tables ]===========================================================================================

COMMENT ON COLUMN SL_CARD.WRONGPINATTEMPT IS 'Number of failed pin entry attempts';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
