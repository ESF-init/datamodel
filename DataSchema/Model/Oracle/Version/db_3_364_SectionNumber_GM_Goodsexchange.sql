SPOOL db_3_364.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.364', 'LWU', 'Add SECTIONNUMBER to table GM_GOODSEXCHANGE.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE GM_GOODSEXCHANGE ADD SECTIONNUMBER NUMBER(10);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
