SPOOL db_3_256.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.256', 'ULB', 'Added table AM_CashContenthistory');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE rm_shift
 ADD (cashcontentbegin  NUMBER(10));

ALTER TABLE rm_shift
 ADD (cashcontentend  NUMBER(10));

COMMENT ON COLUMN rm_shift.cashcontentbegin IS 'TVM cash content at shiftbegin';
COMMENT ON COLUMN rm_shift.cashcontentend IS 'TVM cashcontent at shiftend ';
-- =======[ New Tables ]===========================================================================================
CREATE TABLE AM_CASHCONTENTHISTORY
(
  CASHCONTENTHISTORYID  NUMBER(18)              NOT NULL,
  CLIENTID  NUMBER(18)              NOT NULL,
  DEVICENO              NUMBER(10)              NOT NULL,
  DEVICECLASSID         NUMBER(18)              NOT NULL,
  VALUEDATE             DATE                    NOT NULL,
  TRIGGERID             NUMBER(10)              NOT NULL,
  STATUSID              NUMBER(10)              NOT NULL,
  STATUSTEXT            VARCHAR2(255 BYTE),
  SHIFTID               NUMBER(18),
  TRANSACTIONID         NUMBER(18),
  CASHAMOUNT            NUMBER(10)              NOT NULL,
  ExpectedCashChange    NUMBER(10)              NOT NULL,
  CashChange            NUMBER(10)              NOT NULL
)
LOGGING
NOCOMPRESS
NOCACHE
NOPARALLEL
NOMONITORING;


ALTER TABLE am_cashcontenthistory ADD (
  CONSTRAINT am_cashcontenthistory_pk
 PRIMARY KEY
 (cashcontenthistoryid));

COMMENT ON TABLE AM_CASHCONTENTHISTORY is 'Contains all TVM cash content messages';

COMMENT ON COLUMN AM_CASHCONTENTHISTORY.CASHCONTENTHISTORYID IS 'Primary Key';
COMMENT ON COLUMN AM_CASHCONTENTHISTORY.CLIENTID IS 'References Vario_Client.Clientid ';
COMMENT ON COLUMN AM_CASHCONTENTHISTORY.DEVICENO IS 'Device/TVM serial number';
COMMENT ON COLUMN AM_CASHCONTENTHISTORY.DEVICECLASSID IS 'References Vario_Deviceclass.deviceclassid';
COMMENT ON COLUMN AM_CASHCONTENTHISTORY.VALUEDATE IS 'The records timestamp';
COMMENT ON COLUMN AM_CASHCONTENTHISTORY.TRIGGERID IS 'Reason why this record was created (e.g. sale, Shiftbegin,...';
COMMENT ON COLUMN AM_CASHCONTENTHISTORY.STATUSID IS 'Status number, 0=OK, all others=NOK';
COMMENT ON COLUMN AM_CASHCONTENTHISTORY.STATUSTEXT IS 'Status decription ';
COMMENT ON COLUMN AM_CASHCONTENTHISTORY.SHIFTID IS 'References RM_Shift.ShiftId 0...1 ';
COMMENT ON COLUMN AM_CASHCONTENTHISTORY.TRANSACTIONID IS 'References RM_Transaction.TransactionId 0...1 ';
COMMENT ON COLUMN AM_CASHCONTENTHISTORY.CASHAMOUNT IS 'Current cash in the TVM';
COMMENT ON COLUMN AM_CASHCONTENTHISTORY.ExpectedCashChange IS 'Desired value of cash change';
COMMENT ON COLUMN AM_CASHCONTENTHISTORY.CashChange IS 'Real value of cash change';


-- =======[ Functions and Data Types ]=============================================================================
CREATE OR REPLACE FUNCTION getshoppingcartamount (
   shift   IN   NUMBER,
   cart    IN   NUMBER
)
   RETURN NUMBER
IS
   amount   NUMBER;
BEGIN
   amount := 0;

   SELECT NVL (SUM (rm_transaction.price), 0)
     INTO amount
     FROM rm_transaction, rm_devicebookingstate
    WHERE shiftid = shift
      AND shoppingcart = cart
      AND rm_devicebookingstate.devicebookingno =
                                             rm_transaction.devicebookingstate
      AND rm_transaction.devicepaymentmethod in(0,7)
      AND rm_transaction.cancellationid = 0
      AND rm_devicebookingstate.isbooked <> 0;

   RETURN amount;
EXCEPTION
   WHEN OTHERS
   THEN
      -- Consider logging the error and then re-raise
      RAISE;
END getshoppingcartamount;
/

-- =======[ Views ]================================================================================================
CREATE OR REPLACE FORCE VIEW view_am_cashcontenthistory 
AS
   SELECT a.cashcontenthistoryid, a.shiftid, a.transactionid, a.clientid,
          a.deviceno, a.deviceclassid, a.valuedate, a.triggerid, a.statusid,
          a.statustext, a.cashamount, a.expectedcashchange, a.cashchange,
          s.shiftbegin, s.shiftend, t.shoppingcart,
          DECODE
              (NVL (t.shoppingcart, 0),
               0, t.price,
               getshoppingcartamount (t.shiftid, t.shoppingcart)
              ) AS shoppingcartcashprice
     FROM am_cashcontenthistory a, rm_transaction t, rm_shift s
    WHERE a.deviceno = s.deviceno(+)
      AND a.deviceclassid = s.devicetype(+)
      AND a.shiftid = s.shiftid(+)
      AND a.transactionid = t.transactionid(+);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
