SPOOL db_3_488.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.488', 'ARH', 'add new columns to PP_MATRIXENTRY');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE PP_MATRIXENTRY
 ADD (VIAID  NUMBER(10));

ALTER TABLE PP_MATRIXENTRY
 ADD (DESCRIPTION  VARCHAR2(100));

ALTER TABLE PP_MATRIXENTRY
 ADD (ZONEORDER  VARCHAR2(100));

ALTER TABLE PP_MATRIXENTRY
 ADD (DIRECTPURCHASE  NUMBER(1));
 

COMMENT ON COLUMN PP_MATRIXENTRY.VIAID IS 'Via identifier';
COMMENT ON COLUMN PP_MATRIXENTRY.DESCRIPTION IS 'Description';
COMMENT ON COLUMN PP_MATRIXENTRY.ZONEORDER IS 'List of zones';
COMMENT ON COLUMN PP_MATRIXENTRY.DIRECTPURCHASE IS 'Direct purchase ?';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;