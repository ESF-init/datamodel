SPOOL db_3_679.log;

------------------------------------------------------------------------------
--Version 3.679
------------------------------------------------------------------------------

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.679', 'SOE', 'Add new column in SL_EMAILMESSAGE');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_EMAILMESSAGE ADD( MESSAGETYPE Number(9) NULL);
ALTER TABLE SL_EMAILMESSAGE ADD CONSTRAINT FK_MESSAGETYPE_MESSAGETYPE FOREIGN KEY (MESSAGETYPE) REFERENCES SL_MESSAGETYPE(ENUMERATIONVALUE);
COMMENT ON COLUMN SL_EMAILMESSAGE.MESSAGETYPE IS 'Configures the default message type to be sent for this message.';

COMMENT ON COLUMN SL_MESSAGETYPE.MESSAGEFORMAT IS 'Obsolet column.';

COMMIT;

PROMPT Done!

SPOOL OFF;
