SPOOL db_3_416.log;
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.416', 'FRD', 'CH_VALIDATIONTRANSACTION Resolved Type Change');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE CH_VALIDATIONTRANSACTION MODIFY RESOLVED DATE   DEFAULT NULL;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;