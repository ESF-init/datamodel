SPOOL db_3_353.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.353', 'STV', 'Add colum to Table SL_TICKETASSIGNMENT');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_TICKETASSIGNMENT ADD 
(LASTTRANSACTION DATE NULL,
 PERIODICALLOWANCE INT NOT NULL,
 PERIODICTYPEID NUMBER(18,0) NOT NULL);
 
COMMENT ON COLUMN SL_TICKETASSIGNMENT.LASTTRANSACTION IS 'The date of the last sale of this ticket assignment';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.PERIODICALLOWANCE IS 'Restricts the sale of a ticket to once a day/week/month';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.PERIODICTYPEID IS 'References TM_RULE_TYPE';

ALTER TABLE SL_TICKETASSIGNMENT ADD CONSTRAINT FK_TICKETASSIGNEMENT_RULETYPE FOREIGN KEY (PERIODICTYPEID) REFERENCES TM_RULE_TYPE (RULETYPEID);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
