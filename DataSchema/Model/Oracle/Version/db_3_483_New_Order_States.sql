SPOOL db_3_483.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.483', 'EMN', 'Add 4 new states to sl_orderstate');

-- =======[ Enumerations ]================================================================================================
INSERT INTO SL_ORDERSTATE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (1, 'Assigned', 'Assigned');
INSERT INTO SL_ORDERSTATE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (8, 'Verfication Required', 'Verification Required');
INSERT INTO SL_ORDERSTATE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (9, 'Manual Verification', 'Manual Verification');
INSERT INTO SL_ORDERSTATE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (10, 'Closed', 'Closed');


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;