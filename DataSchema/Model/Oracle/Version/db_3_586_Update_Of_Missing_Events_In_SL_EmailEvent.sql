SPOOL db_3_586.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.586', 'MKD', 'All events in SL_EmailEvent which have been missing in script 3.557 are updated with the correct EnumerationValue. Also added unique contstraint in SL_EMAILEVENT.');

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

UPDATE SL_Emailevent SET EnumerationValue = 48, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AccountActivation';
UPDATE SL_Emailevent SET EnumerationValue = 49, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AddCorporateCashValueAutoloadFailed';
UPDATE SL_Emailevent SET EnumerationValue = 50, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AddCorporatePassAutoloadFailed';
UPDATE SL_Emailevent SET EnumerationValue = 51, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AutoloadPayment';
UPDATE SL_Emailevent SET EnumerationValue = 52, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AutoRenewExecution';
UPDATE SL_Emailevent SET EnumerationValue = 53, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AutoRenewSetup';
UPDATE SL_Emailevent SET EnumerationValue = 54, transactioncounter = transactioncounter + 1 WHERE Eventname = 'BillingMail';
UPDATE SL_Emailevent SET EnumerationValue = 55, transactioncounter = transactioncounter + 1 WHERE Eventname = 'BillingMailKVV';
UPDATE SL_Emailevent SET EnumerationValue = 56, transactioncounter = transactioncounter + 1 WHERE Eventname = 'BobAppContractActivation';
UPDATE SL_Emailevent SET EnumerationValue = 57, transactioncounter = transactioncounter + 1 WHERE Eventname = 'BobAppContractUpdate';
UPDATE SL_Emailevent SET EnumerationValue = 58, transactioncounter = transactioncounter + 1 WHERE Eventname = 'BobAppContractVerification';
UPDATE SL_Emailevent SET EnumerationValue = 59, transactioncounter = transactioncounter + 1 WHERE Eventname = 'BobAppRejectedContractActivation';
UPDATE SL_Emailevent SET EnumerationValue = 60, transactioncounter = transactioncounter + 1 WHERE Eventname = 'BobAppVirtualCardRegister';
UPDATE SL_Emailevent SET EnumerationValue = 61, transactioncounter = transactioncounter + 1 WHERE Eventname = 'ClientAuthenticationRequest';
UPDATE SL_Emailevent SET EnumerationValue = 62, transactioncounter = transactioncounter + 1 WHERE Eventname = 'DeclineCorporateAccount';
UPDATE SL_Emailevent SET EnumerationValue = 63, transactioncounter = transactioncounter + 1 WHERE Eventname = 'NegativeBalance';
UPDATE SL_Emailevent SET EnumerationValue = 64, transactioncounter = transactioncounter + 1 WHERE Eventname = 'RegistrationWithPasswordCorporate';
UPDATE SL_Emailevent SET EnumerationValue = 65, transactioncounter = transactioncounter + 1 WHERE Eventname = 'RemoveCorporateCashValueAutoloadFailed';
UPDATE SL_Emailevent SET EnumerationValue = 66, transactioncounter = transactioncounter + 1 WHERE Eventname = 'RemoveCorporatePassAutoloadFailed';
UPDATE SL_Emailevent SET EnumerationValue = 67, transactioncounter = transactioncounter + 1 WHERE Eventname = 'ReportJobExecuted';
UPDATE SL_Emailevent SET EnumerationValue = 68, transactioncounter = transactioncounter + 1 WHERE Eventname = 'ResetPasswordConfirm';
UPDATE SL_Emailevent SET EnumerationValue = 69, transactioncounter = transactioncounter + 1 WHERE Eventname = 'ShareCard';
UPDATE SL_Emailevent SET EnumerationValue = 70, transactioncounter = transactioncounter + 1 WHERE Eventname = 'SmartcardExpiration12Months';
UPDATE SL_Emailevent SET EnumerationValue = 71, transactioncounter = transactioncounter + 1 WHERE Eventname = 'SmartcardExpiration1Month';
UPDATE SL_Emailevent SET EnumerationValue = 72, transactioncounter = transactioncounter + 1 WHERE Eventname = 'TelephonePinChanged';
UPDATE SL_Emailevent SET EnumerationValue = 73, transactioncounter = transactioncounter + 1 WHERE Eventname = 'TransferOfFunds';
UPDATE SL_Emailevent SET EnumerationValue = 74, transactioncounter = transactioncounter + 1 WHERE Eventname = 'UsernameChanged';

-- =======[ Changes on existing tables ]=========================================

ALTER TABLE SL_EMAILEVENT ADD (CONSTRAINT UC_EmailEvent_02 UNIQUE (ENUMERATIONVALUE));

COMMIT;

PROMPT Done!

SPOOL OFF;
