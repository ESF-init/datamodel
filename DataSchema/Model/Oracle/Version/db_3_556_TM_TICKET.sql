SPOOL db_3_556.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.556', 'FLF', 'Add new field BRTYPE10ID and comment to TM_TICKET');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE TM_TICKET
 ADD (BRTYPE10ID  NUMBER(18));


COMMENT ON COLUMN TM_TICKET.BRTYPE10ID IS 'Reference TM_BUSINESSRULE.BUSINESSRULEID. Defines the business rule 10, which belongs to ticket.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
