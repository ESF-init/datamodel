SPOOL db_3_150.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.150', 'DST', 'Change EncryptedData to NCLOB.');

-- =======[ MODIFY existing tables ]==============================================================================================

ALTER TABLE SL_PaymentJournal
 ADD (ENCRYPTEDDATANCLOB  NCLOB);

UPDATE SL_PaymentJournal SET ENCRYPTEDDATANCLOB=ENCRYPTEDDATA, TRANSACTIONCOUNTER = TRANSACTIONCOUNTER+1 where ENCRYPTEDDATA is not null;

ALTER TABLE SL_PaymentJournal DROP COLUMN ENCRYPTEDDATA;

ALTER TABLE SL_PaymentJournal
RENAME COLUMN ENCRYPTEDDATANCLOB TO ENCRYPTEDDATA;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
