SPOOL db_3_207.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.207', 'DST', 'Virtual card notification changes.');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_ProvisioningState
(
  CorrelationID       NVARCHAR2(25)             NOT NULL,
  AccountHash         VARCHAR2(64)              NOT NULL,
  LastModified        DATE                      DEFAULT sysdate
);

ALTER TABLE SL_ProvisioningState ADD CONSTRAINT PK_ProvisioningState PRIMARY KEY (CorrelationID);

COMMENT ON TABLE SL_ProvisioningState is 'Helper table to store the account hash during the Apple provisioning process.';
COMMENT ON COLUMN SL_ProvisioningState.CorrelationID IS 'Primary key identifying this provisioning process, assigned by Mastercard.';
COMMENT ON COLUMN SL_ProvisioningState.AccountHash IS 'SHA-256 hash over the iCloud account.';
COMMENT ON COLUMN SL_ProvisioningState.LastModified IS 'Technical field: date time of the last change to this dataset';

CREATE TABLE SL_NotificationDevice
(
  NotificationDeviceID           NUMBER(18,0)                                NOT NULL,
  DeviceID                       VARCHAR2(64)                                NOT NULL,
  PushToken                      VARCHAR2(64)                                NOT NULL,
  AuthenticationToken            VARCHAR2(64)                                NOT NULL,
  LASTUSER                       NVARCHAR2(50)         DEFAULT 'SYS',
  LASTMODIFIED                   DATE                  DEFAULT sysdate,
  TRANSACTIONCOUNTER             INTEGER                                     NOT NULL
);

ALTER TABLE SL_NotificationDevice add constraint PK_NotificationDevice PRIMARY KEY (NotificationDeviceID);

COMMENT ON TABLE SL_NotificationDevice IS 'Lists iOS devices for push notification purposes.';
COMMENT ON COLUMN SL_NotificationDevice.NotificationDeviceID IS 'Unique identifier of a notification device.';
COMMENT ON COLUMN SL_NotificationDevice.DeviceID IS 'External identifier of the device.';
COMMENT ON COLUMN SL_NotificationDevice.PushToken IS 'External token.';
COMMENT ON COLUMN SL_NotificationDevice.AuthenticationToken IS 'INIT generated token.';
COMMENT ON COLUMN SL_NotificationDevice.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_NotificationDevice.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_NotificationDevice.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_VirtualCard ADD NotificationDeviceID NUMBER(18);
ALTER TABLE SL_VirtualCard ADD CONSTRAINT FK_VirtualCard_NotificationDev FOREIGN KEY (NotificationDeviceID) REFERENCES SL_NotificationDevice (NotificationDeviceID);
COMMENT ON COLUMN SL_VirtualCard.NotificationDeviceID IS 'Reference to NotificationDevice for push notifications.';

ALTER TABLE SL_TransactionJournal ADD TokenCardID NUMBER(18);
ALTER TABLE SL_TransactionJournal ADD CONSTRAINT FK_TransJournal_Card_Token FOREIGN KEY (TokenCardID) REFERENCES SL_Card (CardID);
COMMENT ON COLUMN SL_TransactionJournal.TokenCardID IS 'Reference to SL_Card, identifying the token card for tokenized taps.';

ALTER TABLE SL_TransactionJournal ADD ExternalTransactionIdentifier VARCHAR2(64);
COMMENT ON COLUMN SL_TransactionJournal.ExternalTransactionIdentifier IS 'Identifier for external transaction reconciliation.';

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_NotificationDevice');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_NotificationDevice');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
