SPOOL db_1_212.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('29.07.2011', 'DD.MM.YYYY'), '1.212', 'FLF','TM_PREDEFINEDKEYS');


ALTER TABLE TM_PREDEFINEDKEYS
 ADD (    TARIFFKEY    NUMBER(1));
 
 

COMMIT;

PROMPT Fertig!

SPOOL off
