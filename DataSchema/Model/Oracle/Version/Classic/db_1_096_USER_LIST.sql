spool db_1_096.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '22.01.2010', 'DD.MM.YYYY'), '1.096', 'HSS', 'Spalte PASSWORDCHANGED in Tabelle USER_LIST erweitert');

--**************************************
ALTER TABLE USER_LIST add (PASSWORDCHANGED DATE);

--*****************************************

COMMIT;

PROMPT Fertig!

spool off
