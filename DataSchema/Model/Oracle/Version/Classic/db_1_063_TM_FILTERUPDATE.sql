spool db_1_063.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '01.07.2009', 'DD.MM.YYYY'), '1.063', 'HSS', 'TM Update with new Matrix and Ticket Filter');

rem new column in tm_farematrixentry
ALTER TABLE tm_farematrixentry
ADD (
   linefilterattributeid  NUMBER(10)
);

ALTER TABLE tm_farematrixentry
ADD (
   serviceallocationid    NUMBER(10)
);


rem new column in tm_attribute
ALTER TABLE tm_attribute
ADD (
    showintickets      NUMBER(1)
);

ALTER TABLE tm_attribute
ADD (
    hasmultiplevalues  NUMBER(1)
);

rem set standardvalue in tm_attribute
UPDATE tm_attribute
   SET showintickets = 1;

rem set standardvalue in tm_attribute
UPDATE tm_attribute
   SET hasmultiplevalues = 0;


commit;

PROMPT Fertig!

spool off