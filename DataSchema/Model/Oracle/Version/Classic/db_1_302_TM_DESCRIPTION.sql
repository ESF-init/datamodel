SPOOL db_1_302.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('29.11.2012', 'DD.MM.YYYY'), '1.302', 'FLF','TM Description');

ALTER TABLE TM_RULE_PERIOD ADD  DESCRIPTION       VARCHAR2(1000 BYTE);
 
COMMIT;

PROMPT Fertig!

SPOOL off