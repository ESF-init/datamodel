SPOOL db_1_261.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('14.05.2012', 'DD.MM.YYYY'), '1.261', 'KKV ','Added communityid to SAM_ORDERER');

ALTER TABLE SAM_ORDERER
ADD (COMMUNITYID NUMBER(10));

COMMIT;

PROMPT Done!

SPOOL off