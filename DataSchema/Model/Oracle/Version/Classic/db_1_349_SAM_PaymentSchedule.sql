SPOOL db_1_349.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.349', 'KKV','Added IsAnnuallyRepeatable to SAM_PAYMENTSCHEDULE');

 ALTER TABLE SAM_PAYMENTSCHEDULE	ADD	ISANNUALLYREPEATABLE NUMBER(1);
 
COMMIT;

PROMPT Fertig!

SPOOL off