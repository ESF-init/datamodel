SPOOL db_1_316.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.316', 'OZS','New table RM_CDSPAYMENT');

CREATE TABLE RM_CDSPAYMENT
(
  ID                 NUMBER(10),
  DEBTORID           NUMBER(10)                 DEFAULT 0                     NOT NULL,
  TRANSACTIONNUMBER  NUMBER(10),
  MACHINENO          NUMBER(10),
  VALUEDATE          DATE,
  AMOUNT             NUMBER(10),
  DUTYBALANCEID      NUMBER(10)                 DEFAULT 0                     NOT NULL
);


CREATE INDEX IDX_CDSPAYMENT_DUTYBALANCEID ON RM_CDSPAYMENT
(DUTYBALANCEID);


CREATE INDEX IDX_CDSPAYMENT_DEBTORID ON RM_CDSPAYMENT
(DEBTORID);


ALTER TABLE RM_CDSPAYMENT ADD (
  PRIMARY KEY
 (ID)
    USING INDEX );

COMMIT;

PROMPT Fertig!

SPOOL off
