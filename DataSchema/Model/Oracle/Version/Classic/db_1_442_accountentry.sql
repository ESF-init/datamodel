SPOOL db_1_442.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.442', 'ULB','Added accSettlementid to PP_ACCOUNTENTRY.');

ALTER TABLE PP_ACCOUNTENTRY
ADD (accSettlementid NUMBER(10));

ALTER TABLE am_component
ADD (STOCKWARNING NUMBER(3));

COMMIT;

PROMPT Fertig!

SPOOL off
