SPOOL db_1_378.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.378', 'FLF','Added columns to table tm_vdv_product');


ALTER TABLE TM_VDV_PRODUCT ADD  ISSUEMODESE        NUMBER(18)           DEFAULT 0                      NOT NULL;
ALTER TABLE TM_VDV_PRODUCT ADD  ISSUEMODENM        NUMBER(18)           DEFAULT 0                      NOT NULL;
ALTER TABLE TM_VDV_PRODUCT ADD  PRIORITYMODE        NUMBER(18)           DEFAULT 0                      NOT NULL;
 


 
COMMIT;

PROMPT Fertig!

SPOOL off