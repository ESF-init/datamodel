SPOOL db_1_255.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('13.04.2012', 'DD.MM.YYYY'), '1.255', 'ULB ','Add table PP_PAYMENTMETHODEXTERNAL');

ALTER TABLE ROE_BOOKINGKEY
MODIFY(CLIENTID NUMBER(10));

ALTER TABLE ROE_Businesstransaction
MODIFY(CLIENTID NUMBER(10));

ALTER TABLE ROE_costlocation
MODIFY(CLIENTID NUMBER(10));

ALTER TABLE ROE_incomingtype
MODIFY(CLIENTID NUMBER(10));

ALTER TABLE ROE_ticketspecies
MODIFY(CLIENTID NUMBER(10));



CREATE TABLE PP_PAYMENTMETHODEXTERNAL
(
  ID               NUMBER(10),
  PAYMENTMETHODID  NUMBER(10),
  EXTERNALCODENO   NUMBER(10),
  CLIENTID         NUMBER(10),
  LOCKED           NUMBER(1)
);


CREATE UNIQUE INDEX PK_PP_PAYMENTMETHODEXTERNAL ON PP_PAYMENTMETHODEXTERNAL
(ID);


ALTER TABLE PP_PAYMENTMETHODEXTERNAL ADD (
  CONSTRAINT FK_PAYMENTMETHOD_EXTERNAL 
 FOREIGN KEY (PAYMENTMETHODID) 
 REFERENCES PP_PAYMENTMETHOD (PAYMENTMETHODID)
    ON DELETE CASCADE);



COMMIT;

PROMPT Done!

SPOOL off