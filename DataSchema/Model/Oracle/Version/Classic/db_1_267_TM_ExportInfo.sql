SPOOL db_1_267.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('15.06.2012', 'DD.MM.YYYY'), '1.267', 'FLF','TM_ExportInfo');

CREATE TABLE TM_EXPORTINFO
(
  EXPORTID    NUMBER(10)                        NOT NULL,
  EXPORTNAME  VARCHAR2(250 BYTE)                NOT NULL
);

ALTER TABLE TM_EXPORTINFO ADD (
  CONSTRAINT TM_EXPORT_PK
 PRIMARY KEY
 (EXPORTID));

COMMIT;


PROMPT Fertig!

SPOOL off
