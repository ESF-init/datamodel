spool db_1_113.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '18.03.2010', 'DD.MM.YYYY'), '1.113', 'JGI', 'New functions for BOB');

CREATE OR REPLACE PROCEDURE UPDATE_DPB 
IS
BEGIN
  DBMS_SNAPSHOT.REFRESH(
    LIST                 => 'DUNNING_POSITIVE_BOOKINGS'
   ,PUSH_DEFERRED_RPC    => TRUE
   ,REFRESH_AFTER_ERRORS => FALSE
   ,PURGE_OPTION         => 1
   ,PARALLELISM          => 0
   ,ATOMIC_REFRESH       => TRUE);
END;
/


CREATE OR REPLACE PROCEDURE UPDATE_VNA 
IS
BEGIN
  DBMS_SNAPSHOT.REFRESH(
    LIST                 => 'VARIO_NEGATIVE_ACCOUNT'
   ,PUSH_DEFERRED_RPC    => TRUE
   ,REFRESH_AFTER_ERRORS => FALSE
   ,PURGE_OPTION         => 1
   ,PARALLELISM          => 0
   ,ATOMIC_REFRESH       => TRUE);
END;
/

COMMIT;

PROMPT Fertig!

spool off
