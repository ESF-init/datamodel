SPOOL db_1_481.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.481', 'SLR', 'New table TM_KEY_SELECTION_MODE');

CREATE TABLE TM_KEY_SELECTION_MODE
(
  KEYSELECTIONMODEID   NUMBER(18)           NOT NULL,
  KEYSELECTIONMODENAME NVARCHAR2(50)        NOT NULL,
  DESCRIPTION          NVARCHAR2(50)
);


CREATE UNIQUE INDEX TM_KEY_SELECTION_MODE_PK ON TM_KEY_SELECTION_MODE
(KEYSELECTIONMODEID);


ALTER TABLE TM_KEY_SELECTION_MODE ADD (
  CONSTRAINT PK_KEY_SELECTION_MODE
 PRIMARY KEY
 (KEYSELECTIONMODEID));

ALTER TABLE TM_BUSINESSRULERESULT
 ADD (KEYSELECTIONMODEID  NUMBER(18)            DEFAULT 0                     NOT NULL);


 
COMMIT;

PROMPT Fertig!

SPOOL off