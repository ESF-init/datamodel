SPOOL db_1_392.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.392', 'FLF','Added table tm_binarydata and tm_ticketbinarydata');


CREATE TABLE TM_BINARYDATA
(
  BINARYDATAID  NUMBER(18)                      NOT NULL,
  BINARYNAME    VARCHAR2(200 BYTE)              NOT NULL,
  TARIFFID      NUMBER(18)                      NOT NULL,
  BINARYDATA    BLOB                            NOT NULL,
  BINARYTYPE    NUMBER(9)                       NOT NULL
);


ALTER TABLE TM_BINARYDATA ADD (
  PRIMARY KEY
 (BINARYDATAID));

 
 CREATE TABLE TM_TICKET_BINARYDATA
(
  TICKETID      NUMBER(18)                      NOT NULL,
  BINARYDATAID  NUMBER(18)                      NOT NULL
);


ALTER TABLE TM_TICKET_BINARYDATA ADD (
  PRIMARY KEY
 (TICKETID, BINARYDATAID));
 
COMMIT;

PROMPT Fertig!

SPOOL off