SPOOL db_1_405.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.405', 'OZS','Added new columns to dm_cardclearinghistory');

ALTER TABLE DM_CARDCLEARINGHISTORY
ADD (IMPORTDATETIME DATE);
 
CREATE OR REPLACE TRIGGER TRG_DM_CARDCLEARINGHISTORY_NEW
BEFORE INSERT ON DM_CARDCLEARINGHISTORY
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
BEGIN
  :new.importdatetime := sysdate;
END ;
/

COMMIT;

PROMPT Fertig!

SPOOL off