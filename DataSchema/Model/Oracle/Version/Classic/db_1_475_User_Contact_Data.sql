SPOOL db_1_475.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.475', 'TSC','Added contact data fields to user list for MVG.');

ALTER TABLE USER_LIST
ADD (
	BUSINESS_EMAIL NVARCHAR2(50)
);

ALTER TABLE USER_LIST
ADD (
	BUSINESS_PHONENO NVARCHAR2(30)
);

ALTER TABLE USER_LIST
ADD (
	DIVISION NVARCHAR2(25)
);

ALTER TABLE USER_LIST
ADD (
    BUSINESS_FAXNO NVARCHAR2(25)
);

COMMIT;

PROMPT Fertig!

SPOOL off