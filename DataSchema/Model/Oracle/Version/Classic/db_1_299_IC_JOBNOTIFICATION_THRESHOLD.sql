SPOOL db_1_299.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('09.11.2012', 'DD.MM.YYYY'), '1.299', 'MKK','IC UM_JOBNOTIFICATION column THRESHOLD added');

ALTER TABLE UM_JOBNOTIFICATION ADD THRESHOLD NUMBER (10) DEFAULT 0;
 
COMMIT;

PROMPT Fertig!

SPOOL off