SPOOL db_1_183.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('18.03.2011', 'DD.MM.YYYY'), '1.183', 'FLF','TM OrderNumber');


ALTER TABLE TM_FARESTAGE
 ADD (  ORDERNUMBER      NUMBER(10));



PROMPT Fertig!

SPOOL off
