SPOOL db_1_418.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.418', 'FLF', 'Changed TM_SYSTEMFIELD field NAME from VARCHAR2(100 BYTE) to VARCHAR2(300 BYTE)');

ALTER TABLE TM_SYSTEMFIELD
MODIFY(NAME VARCHAR2(300 BYTE));


 
COMMIT;

PROMPT Fertig!

SPOOL off