SPOOL db_1_271.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('10.07.2012', 'DD.MM.YYYY'), '1.271', 'FLF','TM_GUIDEF created.');

CREATE TABLE TM_GUIDEF
(
  GUIDEFID       NUMBER(10)                     NOT NULL,
  TARIFFID       NUMBER(10)                     NOT NULL,
  DEVICECLASSID  NUMBER(10)                     NOT NULL,
  GUIDATA_XSD    CLOB                           NOT NULL,
  GUIDEF_XML     CLOB                           NOT NULL,
  GUIDATA_XML    CLOB                           NOT NULL,
  VERSION        VARCHAR2(50 CHAR)              NOT NULL,
  ACTIVE         NUMBER(1)                      NOT NULL,
  NAME           VARCHAR2(500 CHAR)             NOT NULL,
  DESCRIPTION    VARCHAR2(500 CHAR),
  SEQUENCE       NUMBER(10)                     NOT NULL,
  PANELID        NUMBER(10)                     NOT NULL
);


ALTER TABLE TM_GUIDEF ADD (
  CONSTRAINT TM_GUIDEF_PK
 PRIMARY KEY
 (GUIDEFID));

ALTER TABLE TM_GUIDEF ADD (
  CONSTRAINT TM_GUIDEF_R01 
 FOREIGN KEY (TARIFFID) 
 REFERENCES TM_TARIF (TARIFID));


COMMIT;


PROMPT Fertig!

SPOOL off
