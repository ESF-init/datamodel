
spool db_1_034.LOG

PROMPT Neue ATTRIBUTE f�r RM_TRANSACTION

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_DATE( '24.02.2009', 'DD.MM.YYYY'), '1.034', 'HSS', 'Neue Attribute f�r MATERIALIZED VIEWS STAT_DESTINATION_TRIANGLE und STAT_RM_TRANSACTIONS');

-- *************************

PROMPT STAT_RM_TRANSACTIONS
DROP MATERIALIZED VIEW STAT_RM_TRANSACTIONS;

CREATE OR REPLACE FUNCTION Getfarestagenamefrommatview
( TARRIF_ID IN NUMBER,
  SERVICENAME IN VARCHAR,
  FARESTAGENO IN NUMBER
)
RETURN VARCHAR IS
tmpVar VARCHAR(50);
BEGIN
   tmpVar := '';
   FOR rec IN
       (SELECT FARESTAGE_NAME FROM MVIEW_TM_FARESTAGENAMES WHERE TARIFID = TARRIF_ID AND   FAReSTAGE_NUMBER=FARESTAGENO)
   LOOP
       tmpVar := rec.FARESTAGE_NAME;
    EXIT;
   END LOOP;
   RETURN tmpVar;
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
       RETURN '-';
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END Getfarestagenamefrommatview;
/

CREATE MATERIALIZED VIEW STAT_RM_TRANSACTIONS 
TABLESPACE VARIO_DAT
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT 
transactionid, RM_TRANSACTION.clientid, cardid, 
   RM_TRANSACTION.deviceno, saleschannel, tripdatetime, 
   noadults, nochildren, fromstopno, 
   fromtariffzone, totariffzone, viatariffzone, 
   pricelevel, RM_TRANSACTION.serviceid, RM_TRANSACTION.state, 
   line, accountentryid, tripserialno, 
   bestpricerunid, importdatetime, nouser1, 
   nouser2, nouser3, nouser4, 
   factor, RM_TRANSACTION.shiftid, tariffid, 
   RM_TRANSACTION.typeid,typename AS transactiontypetext,RM_TRANSACTION.price, valueofride, 
   cancellationid, devicepaymentmethod, NVL(linename,'0') AS linename, RM_TRANSACTION.routeno, 
   cardtransactionno, cardreftransactionno, cardbalance, 
   cardchargetan, cardcredit, devicebookingstate, 
   paycardno, RM_SHIFT.cardno,RM_SHIFT.debtorno,RM_SHIFT.shiftbegin , 
   isbooked,text AS devicebookingstatetext , 
   RM_TRANSACTION.STOPFROM AS stopfrom,
   RM_TRANSACTION.STOPTO AS stopto,
   TM_TICKET.externalnumber, TM_TICKET.internalnumber,  TM_TICKET.categoryid,   TM_TICKET.NAME, 
   TM_TICKET.ticketid, TM_TARIF.tarifid,RM_DEVICEBOOKINGSTATE.devicebookingstateid, 
   TM_TARIF.validfrom,TM_TARIF.VERSION,RM_SHIFT.shiftstateid, 
   Getfarestagenamefrommatview (tariffid, linename, fromtariffzone) AS fromfarestage, 
   Getfarestagenamefrommatview (tariffid, linename, totariffzone) AS tofarestage 
FROM RM_TRANSACTION,RM_SHIFT, RM_DEVICEBOOKINGSTATE ,TM_TICKET, TM_TARIF, RM_TRANSACTIONTYPE 
WHERE RM_SHIFT.shiftid = RM_TRANSACTION.shiftid 
AND RM_DEVICEBOOKINGSTATE.devicebookingno = RM_TRANSACTION.devicebookingstate 
AND TM_TARIF.tarifid = TM_TICKET.tarifid 
AND TM_TARIF.tarifid = RM_TRANSACTION.tariffid 
AND TM_TICKET.internalnumber = RM_TRANSACTION.tikno 
AND RM_TRANSACTION.typeid = RM_TRANSACTIONTYPE.typeid;




PROMPT CREATE MATERIALIZED VIEW STAT_DESTINATION_TRIANGLE 
DROP MATERIALIZED VIEW STAT_DESTINATION_TRIANGLE;

CREATE MATERIALIZED VIEW  STAT_DESTINATION_TRIANGLE 
TABLESPACE VARIO_DAT
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT transactionid, tripdatetime, clientid, startzone, 
          FROMFARESTAGE AS startzonename, destzone, 
          TOFARESTAGE AS destzonename, ticketid, 
          revenue / 100 , debtorno,
    stopfrom, stopto
    FROM 
   (SELECT transactionid, tripdatetime, clientid, 
   fromtariffzone AS startZone, 
    totariffzone AS destZone, 
 FROMFARESTAGE, 
 TOFARESTAGE, 
    ticketid, 
 linename, 
  tariffid, 
  stopfrom, stopto,
          price  AS revenue, debtorno 
     FROM STAT_RM_TRANSACTIONS 
    WHERE (fromtariffzone >= totariffzone) 
      AND (cancellationid = 0) 
      AND (isbooked = 1) 
      AND (shiftstateid < 10) 
      AND (typeid NOT IN (1, 2, 3)) 
UNION 
   SELECT transactionid, tripdatetime, clientid, 
   totariffzone AS startZone, 
    fromtariffzone AS destZone, 
  TOFARESTAGE, 
  FROMFARESTAGE, 
    ticketid, 
 linename, 
 tariffid, 
 stopfrom, stopto,
         price AS revenue, debtorno 
     FROM STAT_RM_TRANSACTIONS 
    WHERE (fromtariffzone < totariffzone) 
      AND (cancellationid = 0) 
      AND (isbooked = 1) 
      AND (shiftstateid < 10) 
      AND (typeid NOT IN (1, 2, 3)));


-- *************************
COMMIT;

PROMPT Fertig!

spool OFF


