SPOOL db_1_336.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.336', 'MKK','RTA UnzipAtDest flag added to UM_ARCHIVEJOB');

ALTER TABLE UM_ARCHIVEJOB ADD (UNZIPATDEST NUMBER(1));

COMMIT;

PROMPT Fertig!

SPOOL off