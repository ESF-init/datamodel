SPOOL db_1_385.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.385', 'ULS','New column for statistics in SAM_ADDTRANSACTIONINFO');

ALTER TABLE sam_addtransactioninfo
ADD (distance NUMBER(10,3));

ALTER TABLE sam_addtransactioninfo
ADD (distanceattributeid NUMBER(10));

ALTER TABLE sam_addtransactioninfo
ADD (tariffattributeid NUMBER(10));
 
COMMIT;

PROMPT Fertig!

SPOOL off