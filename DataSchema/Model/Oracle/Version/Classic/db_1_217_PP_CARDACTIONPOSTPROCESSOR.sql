SPOOL db_1_217.log


INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('02.09.2011', 'DD.MM.YYYY'), '1.217', 'HSS','Card action request for post processes');
     
     

CREATE TABLE PP_CARDACTIONPOSTPROCESSOR
(
  CARDACTIONREQUESTID NUMBER(10)                           NOT NULL
);


COMMIT ;

PROMPT Fertig!

SPOOL off