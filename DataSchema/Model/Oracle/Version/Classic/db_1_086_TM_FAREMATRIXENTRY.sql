spool db_1_086.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '05.11.2009', 'DD.MM.YYYY'), '1.086', 'FLF', 'Neues Attribute DIRECTIONATTRIBUTEID in der Tabelle TM_FAREMATRIXENTRY hinzugefügt');

--**************************************


ALTER TABLE TM_FAREMATRIXENTRY
ADD DIRECTIONATTRIBUTEID NUMBER(10);


--*****************************************

COMMIT;

PROMPT Fertig!

spool off





