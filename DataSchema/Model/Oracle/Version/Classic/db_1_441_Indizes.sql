SPOOL db_1_441.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.441', 'ULB','Several new tables, indexes and foreign keys.');

CREATE TABLE DM_PAPERROLLUSAGE
(
  USAGEID       NUMBER(10),
  CLIENTID      NUMBER(10),
  FIRSTSHIFTID  NUMBER(10),
  LASTSHIFTID   NUMBER(10),
  FIRSTSEEN     DATE,
  LASTSEEN      DATE,
  ROLLNO        NUMBER(10),
  FIRSTBARCODE  NUMBER(4),
  LASTBARCODE   NUMBER(4)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DM_PAPERROLLUSAGE_PK ON DM_PAPERROLLUSAGE
(USAGEID)
NOLOGGING
NOPARALLEL;


ALTER TABLE DM_PAPERROLLUSAGE ADD (
  CONSTRAINT DM_PAPERROLLUSAGE_PK
 PRIMARY KEY
 (USAGEID));

 


CREATE TABLE CR_HAENDLERKARTEN
(
  CRID                       NUMBER(10),
  HAENDLERKARTENNUMMER       VARCHAR2(20 BYTE),
  DATUMZEIT                  DATE,
  FSEQ                       NUMBER(6),
  ANZAHL_ENTKOPPELTE         NUMBER(6),
  WERT_ENTKOPPELTE           NUMBER(8),
  WERT_GEKOPPELTE            NUMBER(8),
  MAX_ANZAHL_ENTKOPPELTE     NUMBER(8),
  MAX_WERT_EIN_ENTKOPPELTER  NUMBER(8),
  MAX_WERT_ALLE_ENTKOPPELTE  NUMBER(8),
  KONFIGURATIONSBYTE         NUMBER(3),
  HAENDLERBANKVERBINDUNG     VARCHAR2(20 BYTE),
  SSEQ                       NUMBER(10),
  HSEQ                       NUMBER(10),
  VERFALLSDATUM              DATE,
  AKTIVIERUNGSDATUM          DATE,
  WAEHRUNGSKENNZEICHEN       VARCHAR2(3 BYTE),
  TERMINAL_ID                NUMBER(8),
  TERMINAL_ZULASSUNGSNUMMER  NUMBER(10),
  OLD_SSEQ                   NUMBER(6),
  VWNR                       NUMBER(4)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

ALTER TABLE CR_HAENDLERKARTEN ADD (
  CONSTRAINT CR_HAENDLERKARTEN_PK
 PRIMARY KEY
 (CRID));



-- ============ User  ========================
ALTER TABLE Vario_settlement ADD (
  FOREIGN KEY (userid) 
 REFERENCES USER_list (userid));
 
 
ALTER TABLE GM_GOODSEXCHANGE ADD (
  FOREIGN KEY (userid) 
 REFERENCES USER_list (userid));
 
 
-- ============ Accounting  ======================== 
 ALTER TABLE ROE_BOOKINGSUMMARY ADD (
  FOREIGN KEY (userid) 
 REFERENCES USER_list (userid));
 
 CREATE INDEX idx_accentry_userid ON PP_ACCOUNTENTRY
(USERID)
NOLOGGING
NOPARALLEL;
 
-- ============  Protocol ========================
CREATE INDEX idx_protctdate ON protocol
(protfctdate)
NOLOGGING
NOPARALLEL;

CREATE INDEX idx_protusername ON protocol
(protusername)
NOLOGGING
NOPARALLEL;

CREATE INDEX idx_protprotprio ON protocol
(protprio)
NOLOGGING
NOPARALLEL;

CREATE INDEX idx_protclientid ON protocol
(clientid)
NOLOGGING
NOPARALLEL;


-- ============  RM* ========================
CREATE INDEX idx_transaction_shiftid ON rm_transaction
(shiftid)
NOLOGGING
NOPARALLEL;


 CREATE INDEX IDX_RM_BINARYDATA_TRANSID ON RM_BINARYDATA
(TRANSACTIONID)
NOLOGGING
NOPARALLEL;

CREATE INDEX IDX_RM_EXTERNALDATA_TRANSID ON RM_EXTERNALDATA
(TRANSACTIONID)
NOLOGGING
NOPARALLEL;



CREATE INDEX idx_exttransaction_tranid ON RM_EXT_TRANSACTION
(transactionid)
NOLOGGING
NOPARALLEL;

CREATE INDEX idx_shift_balanceno ON rm_shift
(balanceno)
NOLOGGING
NOPARALLEL;

CREATE INDEX IDX_SHIFT_settlementid ON RM_SHIFT
(settlementid)
NOLOGGING
NOPARALLEL;

CREATE INDEX idx_RM_BINARY ON RM_BINARYDATA
(DATATYPE, HASHVALUE)
LOGGING
NOPARALLEL;

-- ============  File ========================
CREATE INDEX idx_fileerrordate ON FILEERRORS
(ERRORDATE)
LOGGING
NOPARALLEL;


COMMIT;

PROMPT Fertig!

SPOOL off
