SPOOL db_1_300.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('27.11.2012', 'DD.MM.YYYY'), '1.300', 'FLF','TM TE');

CREATE TABLE TE_BUTTONTYPE
(
  TYPENAME      VARCHAR2(20 BYTE),
  BUTTONTYPEID  NUMBER(18)
);


CREATE TABLE TE_EXTRA
(
  EXTRANAME  VARCHAR2(200 BYTE),
  EXTRAID    NUMBER(18)
);


CREATE TABLE TE_LAYOUT
(
  TARIFID         NUMBER(10),
  VWNR            NUMBER(4),
  TIKNR           NUMBER(4),
  PAGE            VARCHAR2(6 BYTE),
  POSITION        VARCHAR2(6 BYTE),
  BUTTONTEXT      VARCHAR2(100 BYTE),
  SYMBOL          VARCHAR2(100 BYTE),
  TADATUM         NUMBER(7),
  TAVERSION       NUMBER(2),
  AUTOPREISSTUFE  NUMBER(1)                     DEFAULT 0,
  EXTRA           VARCHAR2(200 BYTE),
  BUTTONTYPE      VARCHAR2(20 BYTE)             DEFAULT '-'                   NOT NULL,
  ERRMSG          VARCHAR2(200 BYTE),
  LAYOUTID        NUMBER(18),
  BUTTONTYPEID    NUMBER(18),
  EXTRAID         NUMBER(18),
  POSITIONID      NUMBER(18),
  SYMBOLID        NUMBER(18),
  TICKETID        NUMBER(18)
);


CREATE TABLE TE_POSITIONS
(
  PAGE        VARCHAR2(6 BYTE),
  POSITION    VARCHAR2(6 BYTE),
  POSITIONID  NUMBER(18)
);


CREATE TABLE TE_SYMBOL
(
  SYMBOLNAME  VARCHAR2(256 BYTE),
  SYMBOLID    NUMBER(18)
);


CREATE OR REPLACE TRIGGER TRIGGER_TE_LAYOUT_CHECK
BEFORE INSERT OR UPDATE ON TE_LAYOUT
FOR EACH ROW
DECLARE
  tikcnt number(9);
  msg te_layout.errmsg%TYPE;
BEGIN
  msg := '';
  IF (:new.position LIKE '%0') THEN
    /* Ueberschrift: */
    IF (:new.tiknr <> 0) THEN
      msg := msg||'Ticket in Ueberschrift unzulaessig! ';
    END IF;
    IF (:new.symbol <> '-') THEN
      msg := msg||'Symbol in Ueberschrift unzulaessig! ';
    END IF;
    IF (:new.extra <> '-') THEN
      msg := msg||'Extra in Ueberschrift unzulaessig! ';
    END IF;
    IF (:new.autopreisstufe > 0) THEN
      msg := msg||'AutoPS in Ueberschrift unzulaessig! ';
    END IF;
    IF (:new.buttontype <> '-') THEN
      msg := msg||'Buttontyp in Ueberschrift unzulaessig! ';
    END IF;
  END IF;
      IF (:new.autopreisstufe > 0) THEN
        msg := msg||'AutoPS ohne Ticket unzulaessig! ';
      END IF;
  :new.errmsg := msg;
END;
/

ALTER TABLE TE_BUTTONTYPE ADD (
  PRIMARY KEY
 (BUTTONTYPEID));

ALTER TABLE TE_EXTRA ADD (
  CONSTRAINT TE_EXTRA_PK
 PRIMARY KEY
 (EXTRAID));

ALTER TABLE TE_LAYOUT ADD (
  CONSTRAINT TE_LAYOUT_PK
 PRIMARY KEY
 (LAYOUTID));

ALTER TABLE TE_POSITIONS ADD (
  CONSTRAINT TE_POSITIONS_PK
 PRIMARY KEY
 (POSITIONID));

ALTER TABLE TE_SYMBOL ADD (
  CONSTRAINT TE_SYMBOL_PK
 PRIMARY KEY
 (SYMBOLID));



COMMIT;

PROMPT Fertig!

SPOOL off