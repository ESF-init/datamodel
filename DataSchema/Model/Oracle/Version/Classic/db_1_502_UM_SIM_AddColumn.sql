SPOOL db_1_502.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.502', 'MRP', 'Extend table UM_SIM with column IPADDRESS');

alter table UM_SIM add ("IPADDRESS" VARCHAR2(39 CHAR));

COMMIT;

PROMPT Fertig!

SPOOL off