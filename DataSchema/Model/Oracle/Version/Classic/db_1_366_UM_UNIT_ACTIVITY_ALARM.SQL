SPOOL db_1_366.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate,'1.366', 'MKK','EXECUTION_TIME added to UM_UNIT_ACTIVITY_ALARM');

ALTER TABLE UM_UNIT_ACTIVITY_ALARM ADD EXECUTION_TIME DATE;

COMMIT;
