SPOOL db_1_359.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.359', 'OZS','Renamed VDV_TXEBER to VDV_TXTRANSACTION');

ALTER TABLE vdv_txeber RENAME TO vdv_txtransaction;

CREATE OR REPLACE SYNONYM VDV_TXEBER FOR VDV_TXTRANSACTION;

COMMIT;

PROMPT Fertig!

SPOOL off