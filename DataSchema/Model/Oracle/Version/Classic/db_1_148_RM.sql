SPOOL db_1_148.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('08.10.2010', 'DD.MM.YYYY'), '1.148', 'ULB','Erweiterungen Cash Management and ROE');

ALTER TABLE ROE_BUSINESSTRANSACTION
 ADD (Locked  NUMBER(1));

ALTER TABLE VARIO_CASH
 ADD (MinInvoiceNo  NUMBER(10)                      DEFAULT 1);

ALTER TABLE VARIO_CASH
 ADD (MaxInvoiceNo  NUMBER(10)                      DEFAULT 2000000000);
	
COMMIT ;

PROMPT Fertig!

SPOOL off
