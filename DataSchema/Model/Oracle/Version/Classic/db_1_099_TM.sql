spool db_1_099.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '04.02.2010', 'DD.MM.YYYY'), '1.099', 'FLF', 'some new fields / Adapt Stored Procedures');

--**************************************

ALTER TABLE TM_FARETABLEENTRY
 ADD (VALIDITYPERIOD  NUMBER(10));
 
 ALTER TABLE TM_ROUTE
 ADD (BOARDINGINFO VARCHAR2(100));
 
 ALTER TABLE TM_ROUTE
 ADD (DESTINATIONINFO VARCHAR2(100));

 
 
 
 CREATE OR REPLACE PROCEDURE insert_choice (
   choice      IN   NUMBER,
   tarif       IN   NUMBER,
   ticket      IN   NUMBER,
   value_      IN   VARCHAR,
   text        IN   VARCHAR,
   distance    IN   NUMBER,
   routename   IN   NUMBER
)
IS
BEGIN
   INSERT INTO tm_choice
               (choiceid, tarifid, ticketid, valuestring, textstring,
                distanceattributeid, routenameid
               )
        VALUES (choice, tarif, ticket, value_, text,
                distance, routename
               );
END insert_choice;
/

CREATE OR REPLACE PROCEDURE insert_farematrixentry (
   farematrixentry       IN   NUMBER,
   boarding              IN   NUMBER,
   destination           IN   NUMBER,
   fare_                 IN   NUMBER,
   farematrix            IN   NUMBER,
   distance_             IN   NUMBER,                              /*(10,3)*/
   via                   IN   NUMBER,
   realdistance_         IN   NUMBER,                               /*(10,3)*/
   averagedistance_      IN   NUMBER,                               /*(10,3)*/
   tariffattribute       IN   NUMBER,
   distanceattribute     IN   NUMBER,
   description_          IN   VARCHAR,
   priority_             IN   NUMBER,
   shortname_            IN   VARCHAR,
   longname_             IN   VARCHAR,
   linefilterattribute   IN   NUMBER,
   serviceallocation     IN   NUMBER,
   routename             IN   NUMBER,
   directionattribute    IN   NUMBER
)
IS
BEGIN
   INSERT INTO tm_farematrixentry
               (farematrixentryid, boardingid, destinationid, fare,
                farematrixid, distance, viaid, realdistance,
                averagedistance, tariffattributeid, distanceattributeid,
                description, priority, shortname, longname,
                linefilterattributeid, serviceallocationid, routenameid,
                directionattributeid
               )
        VALUES (farematrixentry, boarding, destination, fare_,
                farematrix, distance_, via, realdistance_,
                averagedistance_, tariffattribute, distanceattribute,
                description_, priority_, shortname_, longname_,
                linefilterattribute, serviceallocation, routename,
                directionattribute
               );
END insert_farematrixentry;
/



CREATE OR REPLACE PROCEDURE insert_route (
   route                  IN   NUMBER,
   routename              IN   NUMBER,
   routetext_             IN   VARCHAR,
   tarif                  IN   NUMBER,
   exchangeable_          IN   NUMBER,
   printtext1             IN   NUMBER,
   printtext2             IN   NUMBER,
   printtext3             IN   NUMBER,
   tariffattribute        IN   NUMBER,
   ticketgroupattribute   IN   NUMBER,
   boardinginf			  IN   VARCHAR,
   destinationinf		  IN   VARCHAR
)
IS
BEGIN
   INSERT INTO tm_route
               (routeid, routenameid, routetext, tarifid, exchangeable,
                printtext1id, printtext2id, printtext3id, tariffattributeid,
                ticketgroupattributeid, boardinginfo, destinationinfo
               )
        VALUES (route, routename, routetext_, tarif, exchangeable_,
                printtext1, printtext2, printtext3, tariffattribute,
                ticketgroupattribute, boardinginf, destinationinf
               );
END insert_route;
/
 
 
--*****************************************

COMMIT;

PROMPT Fertig!

spool off
