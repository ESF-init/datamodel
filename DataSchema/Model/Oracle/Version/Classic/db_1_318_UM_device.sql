SPOOL db_1_318.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.318', 'ULB','Add column (ExternalDeviceNo  to UM_Device');

ALTER TABLE UM_DEVICE ADD (ExternalDeviceNo VARCHAR2(50));

COMMIT;


PROMPT Fertig!

SPOOL off
