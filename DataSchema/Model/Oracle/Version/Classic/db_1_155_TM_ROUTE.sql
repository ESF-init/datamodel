SPOOL db_1_155.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('24.11.2010', 'DD.MM.YYYY'), '1.155', 'FLF','TM_ROUTE corrections');


ALTER TABLE tm_route
 ADD ( ticketgroupattributeid  NUMBER(10));

ALTER TABLE tm_route
 ADD (textfixed               NUMBER(1));

ALTER  TABLE tm_route
 ADD (routename               VARCHAR2(100 BYTE));

ALTER  TABLE tm_route
 ADD (ticketcategoryid        NUMBER(10));



COMMIT ;

PROMPT Fertig!

SPOOL off
