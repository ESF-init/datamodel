SPOOL db_1_445.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.445', 'ULB','Added column deposit to table PP_CARDACTIONATTRIBUTE');

ALTER TABLE PP_CARDACTIONATTRIBUTE
ADD (DEPOSIT NUMBER(10) 
	  DEFAULT 0);

COMMIT;

PROMPT Fertig!

SPOOL off