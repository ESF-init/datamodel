SPOOL db_1_367.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.367', 'FLF','Added col ENUMERATIONVALUE to table tm_tickettype');

ALTER TABLE TM_TICKETTYPE ADD  ENUMERATIONVALUE  NUMBER(10);
 
COMMIT;

PROMPT Fertig!

SPOOL off