
spool db_1_008.log

PROMPT Erweitere Tabelle RM_Settlement 

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '12.10.2008', 'DD.MM.YYYY'), '1.008', 'ULB', 'RM_Settlement');
-- *************************
ALTER TABLE RM_MONTHLYSETTLEMENT
 ADD (depotid  NUMBER(10));
 ALTER TABLE RM_MONTHLYSETTLEMENT
 ADD (clientid  NUMBER(10));
 
PROMPT Erweitere Tabelle PP_Accountentry
 ALTER TABLE PP_ACCOUNTENTRY
 ADD (exportstate  NUMBER(3));



-- *************************
commit;

PROMPT Fertig!

spool off