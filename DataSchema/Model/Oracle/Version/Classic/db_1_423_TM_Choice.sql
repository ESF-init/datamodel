SPOOL db_1_423.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.423', 'FLF', 'TM Kurzwahl');

ALTER TABLE TM_CHOICE
 ADD (AREALISTKEY  NUMBER(18));


ALTER TABLE TM_CHOICE
 ADD (AREALISTKEYBACK  NUMBER(18));

ALTER TABLE TM_CHOICE
 ADD (DIRECTIONATTRIBUTEVALUEID  NUMBER(18));
 
COMMIT;

PROMPT Fertig!

SPOOL off