spool db_1_105.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '15.02.2010', 'DD.MM.YYYY'), '1.105', 'JGi', 'Added materialized views for BOB');

CREATE MATERIALIZED VIEW DUNNING_POSITIVE_BOOKINGS 
TABLESPACE DATA
NOCACHE
LOGGING
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
AS 
select amount, contractid, valuedatetime  FROM PP_ACCOUNTENTRY,PP_ACCOUNTPOSTINGKEY WHERE PP_ACCOUNTPOSTINGKEY.ENTRYTYPENO=PP_ACCOUNTENTRY.ENTRYTYPENO 
AND PP_ACCOUNTPOSTINGKEY.SIGN >0 AND PP_ACCOUNTENTRY.CANCELATIONENTRYID = 0 
AND PP_ACCOUNTENTRY.BILLID = 0 AND PP_ACCOUNTENTRY.CLEARED = 0;

CREATE INDEX DUNNING_POSITIVE_BOOKINGS_01 ON DUNNING_POSITIVE_BOOKINGS
(VALUEDATETIME, CONTRACTID)
LOGGING
TABLESPACE IDX
NOPARALLEL;

CREATE MATERIALIZED VIEW VARIO_NEGATIVE_ACCOUNT 
TABLESPACE DATA
NOCACHE
LOGGING
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT b.MONTH, b.CONTRACTID, b.BALANCEID, 
       b.BALANCE, c.PAYMENTMETHODID, c.CONTRACTNO, c.FIRSTNAME, c.FAMILYNAME, c.BIRTHDAY, d.DESCRIPTION AS PAYMENTDESCRIPTION, 
      c.DUNNINGLEVELID, e.DESCRIPTION AS DUNNINGLEVELDESCRIPTION, c.DUNNINGTIMESTAMP, 
      (c.STREET || ' ' || c.STREETNO || '    ' || c.POSTALCODESTREET || ' ' || c.CITY) AS ADDRESS, c.TRAFFICCOMPANYID 
FROM PP_ACCOUNTBALANCE b, PP_CONTRACT c, PP_PAYMENTMETHOD d, VARIO_DUNNINGLEVEL e 
WHERE b.CONTRACTID = c.CONTRACTID 
  AND c.PAYMENTMETHODID = d.PAYMENTMETHODID 
  AND c.DUNNINGLEVELID = e.DUNNINGLEVELID 
  AND b.BALANCE < 0 
  AND b.MONTH = 
  (SELECT MAX(a.MONTH) FROM PP_ACCOUNTBALANCE a where a.CONTRACTID = b.CONTRACTID );

CREATE INDEX VARIO_NEGATIVE_ACCOUNT_01 ON VARIO_NEGATIVE_ACCOUNT
(TRAFFICCOMPANYID, DUNNINGLEVELID, FAMILYNAME)
LOGGING
TABLESPACE IDX
NOPARALLEL;

CREATE MATERIALIZED VIEW VARIO_NEGATIVE_ACCOUNT_MAT 
TABLESPACE IDX
NOCACHE
LOGGING
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT b.MONTH, b.CONTRACTID, b.BALANCEID,
       b.BALANCE
FROM PP_ACCOUNTBALANCE b
WHERE b.BALANCE < 0
  AND b.MONTH =
  (SELECT MAX(a.MONTH) FROM PP_ACCOUNTBALANCE a where a.CONTRACTID = b.CONTRACTID );

CREATE INDEX I_01 ON VARIO_NEGATIVE_ACCOUNT_MAT
(MONTH, BALANCEID)
LOGGING
TABLESPACE IDX
NOPARALLEL;

CREATE MATERIALIZED VIEW VIEW_FIRSTTRIP 
TABLESPACE DATA
NOCACHE
LOGGING
NOPARALLEL
BUILD IMMEDIATE
USING INDEX
            TABLESPACE DATA
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT   PP_TRIP_VALIDATEDTICKET.validatedticketid,
         MIN (PP_TRIP.tripdatetime) firsttripdatetime
    FROM PP_TRIP, PP_TRIP_VALIDATEDTICKET
   WHERE ((PP_TRIP.tripid = PP_TRIP_VALIDATEDTICKET.tripid))
GROUP BY PP_TRIP_VALIDATEDTICKET.validatedticketid;

-- Note: Index I_SNAP$_VIEW_FIRSTTRIP will be created automatically 
--       by Oracle with the associated materialized view.


CREATE MATERIALIZED VIEW VIEW_SAPPT_EXPORTDATA 
TABLESPACE DATA
NOCACHE
LOGGING
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
AS 
(SELECT validatedticketid, billingdate, ticketnumber, price, longname,
           exportname1 as exportname, paymentmethodid, clientid, firsttrip
      FROM (SELECT pp_validatedticket.validatedticketid,
                   pp_validatedticket.billingdate, pp_ticket.ticketnumber,
                   pp_validatedticket.price, pp_ticket.longname,
                   pp_ticket.exportname1, pp_contract.paymentmethodid,
                   pp_client.clientid
              FROM pp_validatedticket,
                   pp_ticket,
                   pp_card,
                   pp_contract,
                   pp_client
             WHERE pp_validatedticket.ticketid = pp_ticket.ticketid
               AND pp_validatedticket.bestprice = 1
               AND pp_validatedticket.cardid = pp_card.cardid
               AND pp_card.contractid = pp_contract.contractid
               AND pp_contract.trafficcompanyid = pp_client.clientid
               AND exportdate IS NULL) tikdata,
           (SELECT   MIN (tripdatetime) firsttrip,
                     pp_trip_validatedticket.validatedticketid triptikid
                FROM pp_trip, pp_trip_validatedticket
               WHERE pp_trip.tripid = pp_trip_validatedticket.tripid
            GROUP BY pp_trip_validatedticket.validatedticketid) tripdata
     WHERE tikdata.validatedticketid = tripdata.triptikid);

CREATE INDEX VIEW_SAPPT_EXPORTDATA_01 ON VIEW_SAPPT_EXPORTDATA
(FIRSTTRIP, CLIENTID)
LOGGING
TABLESPACE IDX
NOPARALLEL;


COMMIT;

PROMPT Fertig!

spool off
