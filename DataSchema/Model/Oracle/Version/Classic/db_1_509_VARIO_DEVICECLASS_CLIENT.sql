SPOOL db_1_509.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.509', 'FLF/ES', 'Add VARIO_DEVICECLASS_CLIENT_DEF and VARIO_DEVICECLASS_CLIENT');
	
CREATE TABLE VARIO_DEVICECLASS_CLIENT_DEF
(
  DEVICECLASSID  			  NUMBER(10)                     NOT NULL,
  CLIENTID       			  NUMBER(10)                     NOT NULL,
  GENERALACCOUNT              NUMBER(10),
  VISIBLE                     NUMBER(1),
  RELEASEVISIBILITY           NUMBER(1),
  TMVISIBILITY                NUMBER(1),
  CREATETARIFFARCHIVE         NUMBER(1),
  PARAMETERRELEASEVISIBILITY  NUMBER(1),
  CREATELINEDEFDAT            NUMBER(1)     
);

ALTER TABLE VARIO_DEVICECLASS_CLIENT_DEF ADD (
  CONSTRAINT VARIO_DEVICECLASS_TO_CLIENT_PK PRIMARY KEY (DEVICECLASSID, CLIENTID)
);

CREATE OR REPLACE VIEW VARIO_DEVICECLASS_CLIENT
AS
   SELECT d.deviceclassID,
          dc.clientID,
          d.name,
          d.shortname,
          d.saleDevice,
          d.createLS,
          d.TypeOfUnitID,
          d.ImageFile,
          NVL (dc.generalAccount, d.generalAccount) AS generalAccount,
          NVL (dc.visible, d.visible) AS visible,
          d.ImsApplicationName,
          d.lockable,
          NVL (dc.releaseVisibility, d.releaseVisibility) AS releaseVisibility,
          d.releaseName,
          NVL (dc.tmVisibility, d.tmVisibility) AS tmVisibility,
          NVL (dc.createTariffArchive, d.createTariffArchive) AS createTariffArchive,
          NVL (dc.parameterReleaseVisibility, d.parameterReleaseVisibility) AS parameterReleaseVisibility,
          d.unzipEnabled,
          NVL (dc.createLinedefDat, d.createLinedefDat) AS createLinedefDat
     FROM vario_deviceclass d, VARIO_DEVICECLASS_CLIENT_DEF dc
    WHERE d.deviceclassID = dc.deviceclassID;
 
INSERT INTO VARIO_DEVICECLASS_CLIENT_DEF (deviceClassID, clientID)
SELECT d.deviceClassID, c.clientID
  FROM vario_deviceclass d, vario_client c;

COMMIT;

PROMPT Fertig!

SPOOL off