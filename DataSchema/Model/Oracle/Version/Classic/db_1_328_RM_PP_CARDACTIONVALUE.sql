SPOOL db_1_328.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.328', 'OZS','Erweiterung um USERGROUP');

ALTER TABLE PP_CARDACTIONATTRIBUTE 
    ADD (USERGROUP NUMBER(10));



COMMIT;

PROMPT Fertig!

SPOOL off