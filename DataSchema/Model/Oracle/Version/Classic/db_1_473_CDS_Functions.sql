SPOOL db_1_473.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.473', 'JGI','New functions for CDS export');



CREATE OR REPLACE FUNCTION cdsBelegNr(
    text    PP_ACCOUNTENTRY.postingtext%TYPE,
    butypNr    PP_ACCOUNTENTRY.ENTRYTYPENO%TYPE
)
RETURN NUMBER
IS
    posBelegnr NUMBER;
BEGIN
    IF buTypNr!=200 THEN
        return -1;
    END IF;
    IF UPPER(substr(text,0,3))='NR:' THEN
        posBelegNr:=INSTR(upper(text),'-')+1;
    ELSIF UPPER(substr(text,0,7))='CDS-NR:' THEN
        posBelegNr:=INSTR(upper(text),'BELEGNR: ')+9;
    ELSE
        posBelegNr:=1;
    END IF;
    --return to_number(substr(text,posBelegNr,10));
    return to_number(substr(text, posBelegNr));
END; 
/


CREATE OR REPLACE FUNCTION cdsNr(
    text    PP_ACCOUNTENTRY.postingtext%TYPE,
    butypNr    PP_ACCOUNTENTRY.ENTRYTYPENO%TYPE
)
RETURN NUMBER
IS
BEGIN
    IF buTypNr!=200 THEN
        return -1;
    END IF;
    IF UPPER(substr(text,0,3))='NR:' THEN
       return to_number(substr(text,5,6));
    ELSIF UPPER(substr(text,0,7))='CDS-NR:' THEN
       return to_number(substr(text,9,6));
    ELSE
        return 0;
    END IF;
END;
/


COMMIT;

PROMPT Fertig!

SPOOL off
