SPOOL db_1_339.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.339', 'MKK','DOT Version must be longer than 20 bytes to import software versions of other suppliers');

alter table um_archive modify version VARCHAR2(50);

COMMIT;

PROMPT Fertig!

SPOOL off