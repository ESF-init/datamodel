SPOOL db_1_415.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.415', 'ES','Added new columns to dm_cardclearinghistory');

ALTER TABLE DM_CARDCLEARINGHISTORY
ADD (PAIDAMOUNT NUMBER(10));
 

COMMIT;

PROMPT Fertig!

SPOOL off