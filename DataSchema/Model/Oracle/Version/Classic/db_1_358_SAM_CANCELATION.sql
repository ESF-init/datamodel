SPOOL db_1_358.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.358', 'KVK','Added ACCOUNTINGDATE to SAM_CANCELATION');

ALTER TABLE SAM_CANCELATION
 ADD (ACCOUNTINGDATE  DATE);
  
COMMIT;

PROMPT Fertig!

SPOOL off