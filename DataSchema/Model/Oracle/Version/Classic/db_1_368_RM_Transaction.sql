SPOOL db_1_368.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.368', 'ULB','Added CODE  to table RM_EXT_TRANSACTION');

ALTER TABLE RM_EXT_TRANSACTION ADD (CODE VARCHAR2(100 byte));

ALTER TABLE VARIO_SETTLEMENT MODIFY(USERID NUMBER(10));
 
COMMIT;

PROMPT Fertig!

SPOOL off