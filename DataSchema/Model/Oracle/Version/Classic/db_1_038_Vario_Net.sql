
spool db_1_038.log

PROMPT Neue Tabelle VARIO_NET

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '10.03.2009', 'DD.MM.YYYY'), '1.038', 'MSC', 'Neue Tabelle VARIO_NET');

-- *************************

CREATE TABLE VARIO_NET
(
  NETID             NUMBER(10)               NOT NULL,
  BASEVERSION       VARCHAR2(100 BYTE),
  VALIDFROM         DATE                        NOT NULL,
  VALIDUNTIL        DATE                        NOT NULL,
  NOTE              VARCHAR2(200 BYTE),
  NETWORKVERSION    VARCHAR2(100 BYTE),
  TIMETABLEVERSION  VARCHAR2(100 BYTE),
  CREATIONDATE      DATE,
  CREATIONTIME      DATE,
  CLIENTID          NUMBER(10)
)
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


CREATE UNIQUE INDEX PK_NETID ON VARIO_NET
(NETID);

ALTER TABLE VARIO_NET ADD (
  CONSTRAINT PK_NETID
 PRIMARY KEY
 (NETID)
    USING INDEX);

-- *************************
commit;

PROMPT Fertig!

spool off


