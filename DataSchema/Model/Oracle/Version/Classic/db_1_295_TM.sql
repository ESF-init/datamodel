SPOOL db_1_295.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('29.10.2012', 'DD.MM.YYYY'), '1.295', 'FLF','TM');

CREATE TABLE TM_TICKET_ORGANIZATION
(
  TICKETID        NUMBER(18)                    NOT NULL,
  ORGANIZATIONID  NUMBER(18)                    NOT NULL
);

CREATE TABLE TM_TICKET_PHYSICALCARDTYPE
(
  TICKETID            NUMBER(18)                NOT NULL,
  PHYSICALCARDTYPEID  NUMBER(18)                NOT NULL
);



COMMIT;


PROMPT Fertig!

SPOOL off