SPOOL db_1_480.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.480', 'ULB','VDV_KeyInfo');

CREATE TABLE VDV_KeyInfo
(
  id              NUMBER(18),
  SamId           NUMBER(18),
  KeyId           NUMBER(5),
  Keyversion      NUMBER(5),
  OrganisationId  NUMBER(10),
  Limit           NUMBER(10),
  Counter         NUMBER(10)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;


ALTER TABLE VDV_KeyInfo ADD (
  CONSTRAINT VDV_KeyInfo_PK
 PRIMARY KEY
 (id));

 
 ALTER TABLE VDV_KeyInfo ADD (
  FOREIGN KEY (SAMID) 
 REFERENCES UM_SAM (SAMID));
 
 
ALTER TABLE VDV_SAMSTATUS
ADD (SAMOPKEYID VARCHAR2(36 ));


 
 
COMMIT;

PROMPT Fertig!

SPOOL off
