SPOOL db_1_199.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
     VALUES (TO_DATE ('18.05.2011', 'DD.MM.YYYY'), '1.199', 'MKK','p_DELETE_UM_UNIT edited');
 
CREATE OR REPLACE PROCEDURE p_DELETE_UM_UNIT(
    nUNITID IN NUMBER )
IS
nGarageId NUMBER :=0;
BEGIN
  SELECT UNITID INTO nGarageId FROM  UM_UNIT where TYPEOFUNITID = 0 AND CLIENTID = (SELECT CLIENTID FROM UM_UNIT WHERE UNITID = nUNITID);
  UPDATE um_device SET unitid = nGarageId, state = 2 WHERE unitid= nUNITID;
  DELETE FROM um_archivejobdistributedtounit WHERE unitid=nUNITID;
  DELETE FROM um_currentdistributedarchive WHERE unitid = nUNITID;
  DELETE FROM um_datatraffic WHERE unitid = nUNITID;
  DELETE FROM um_unit_client_visibility WHERE unitid = nUNITID;
  DELETE FROM um_unitdistributiontask WHERE unitid = nUNITID;
  DELETE FROM um_unittogroup WHERE unitid = nUNITID;
  DELETE FROM um_unit WHERE unitid = nUNITID;
  COMMIT;
END p_DELETE_UM_UNIT;
/
SPOOL off
