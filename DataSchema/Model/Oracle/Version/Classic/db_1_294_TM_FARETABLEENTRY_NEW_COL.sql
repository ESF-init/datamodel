SPOOL db_1_294.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('22.10.2012', 'DD.MM.YYYY'), '1.294', 'FLF','TM_FARETABLEENTRY New Col.');


ALTER TABLE TM_FARETABLEENTRY ADD RULEPERIODID	NUMBER(18);


COMMIT;


PROMPT Fertig!

SPOOL off