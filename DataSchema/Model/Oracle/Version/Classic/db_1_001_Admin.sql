
spool db_1_001.log

PROMPT Hinzufügen der Spalte USERPASSWORD zur Tabelle USER_LIST

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '30.06.2008', 'DD.MM.YYYY'), '1.001', 'ULB', 'Initiales DModell');
-- *************************

ALTER TABLE USER_LIST ADD (USERPASSWORD  VARCHAR2(32));

-- *************************
commit;

PROMPT Fertig!

spool off