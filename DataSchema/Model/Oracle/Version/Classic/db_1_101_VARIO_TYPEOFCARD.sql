spool db_1_100.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '08.02.2010', 'DD.MM.YYYY'), '1.101', 'MSC', 'Column CODENO extended');

ALTER TABLE VARIO_TYPEOFCARD MODIFY(CODENO NUMBER(3));

COMMIT;

PROMPT Fertig!

spool off
