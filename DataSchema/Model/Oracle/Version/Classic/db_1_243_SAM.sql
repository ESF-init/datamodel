SPOOL db_1_243.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('08.02.2012', 'DD.MM.YYYY'), '1.243', 'KKV','Extended SAM_InvoiceEntry, PP_Bill And PP_AccountEntry');


ALTER TABLE SAM_INVOICEENTRY ADD (
	LINE           VARCHAR2(10 BYTE),
	TICKETCOUNT    NUMBER(10));

ALTER TABLE PP_BILL ADD (
	CLEARED        NUMBER(1));

ALTER TABLE PP_ACCOUNTENTRY ADD (
	SEASONTICKETID NUMBER(10));

COMMIT;


PROMPT Fertig!

SPOOL off