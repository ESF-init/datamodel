SPOOL db_1_406.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.406', 'FLF','Added new column INFOTEXT to TM_BUSINESSRULERESULT');

 
 
ALTER TABLE TM_BUSINESSRULERESULT
 ADD (INFOTEXT  VARCHAR2(2000 BYTE));

 
COMMIT;

PROMPT Fertig!

SPOOL off