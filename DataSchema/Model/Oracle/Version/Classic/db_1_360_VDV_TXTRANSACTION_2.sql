SPOOL db_1_360.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.360', 'OZS','Renamed VDV_TXEBER to VDV_TXTRANSACTION - 2');

ALTER INDEX vdv_txeber_pk RENAME TO vdv_txtransaction_pk;

ALTER TABLE vdv_txtransaction RENAME CONSTRAINT VDV_TXEBER_PK TO vdv_txtransaction_pk;

ALTER TABLE vdv_txtransaction RENAME CONSTRAINT VDV_TXEBER_R01 TO vdv_txtransaction_r01;

COMMIT;

PROMPT Fertig!

SPOOL off