SPOOL db_1_400.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.400', 'FLF','Added new columns to tm tables');

 
 
ALTER TABLE TM_RULE_PERIOD
 ADD ( EXTERNALNAME1          VARCHAR2(500 BYTE)); 

ALTER TABLE TM_LOGO
 ADD (OWNERCLIENTID  NUMBER(18));

 


 
COMMIT;

PROMPT Fertig!

SPOOL off