SPOOL db_1_370.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.370', 'FLF','Added table TM_VDV_KEYSET');

CREATE TABLE TM_VDV_KEYSET
(
  KEYSETID         NUMBER(18)                   NOT NULL,
  TARIFFID         NUMBER(18)                   NOT NULL,
  TICKETID         NUMBER(18)                   NOT NULL,
  CLIENTID         NUMBER(18)                   NOT NULL,
  ORGID_KVP        NUMBER(18)                   NOT NULL,
  KEYVERSION_AUTH  NUMBER(18)                   NOT NULL,
  KEYVERSION_PV    NUMBER(18)                   NOT NULL,
  KEYVERSION_KVP   NUMBER(18)                   NOT NULL
);


ALTER TABLE TM_VDV_KEYSET ADD (
  PRIMARY KEY
 (KEYSETID));
 
COMMIT;

PROMPT Fertig!

SPOOL off