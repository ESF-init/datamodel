
spool db_1_029.log

PROMPT �ndern Tabelle UM_ARCHIVEJOB

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '21.01.2009', 'DD.MM.YYYY'), '1.029', 'JGi', 'Neues Attribut FILELOCATION');

-- *************************

ALTER TABLE UM_ARCHIVEJOB 
 ADD (FILELOCATION    VARCHAR2(100));

-- *************************
commit;

PROMPT Fertig!

spool off