SPOOL db_1_266.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('15.06.2012', 'DD.MM.YYYY'), '1.266', 'FLF','TM_MatrixCopy');


ALTER TABLE TM_FAREMATRIX
 ADD (    OWNERCLIENTID    NUMBER(10));
 
ALTER TABLE TM_FARETABLE
 ADD (    OWNERCLIENTID    NUMBER(10));
 
COMMIT;


PROMPT Fertig!

SPOOL off
