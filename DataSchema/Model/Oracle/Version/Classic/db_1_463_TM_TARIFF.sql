SPOOL db_1_463.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.463', 'FLF','Added ownerclient to tm_calendar');

ALTER TABLE TM_CALENDAR
 ADD (OWNERCLIENTID  NUMBER(10));


COMMIT;

PROMPT Fertig!

SPOOL off