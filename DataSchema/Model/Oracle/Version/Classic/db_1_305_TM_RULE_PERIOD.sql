SPOOL db_1_305.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('22.01.2013', 'DD.MM.YYYY'), '1.305', 'FLF','TM Rule Period');

ALTER TABLE TM_RULE_PERIOD ADD  VALIDATIONTYPE    NUMBER(18)  default 0     NOT NULL;

COMMIT;

PROMPT Fertig!

SPOOL off