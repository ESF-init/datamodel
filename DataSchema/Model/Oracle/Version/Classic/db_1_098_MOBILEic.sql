spool db_1_098.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '28.01.2010', 'DD.MM.YYYY'), '1.098', 'MTW', 'Procedure P_GETARCHIVEDISTRIBUTIONSATE updated');

--**************************************

CREATE OR REPLACE PROCEDURE p_getarchivedistributionstate (
   jobid      IN       NUMBER,
   jobstate   OUT      NUMBER
)
IS
   completed                        NUMBER (1, 0);
   countdepotserver                 NUMBER;
   countloadingstatistics           NUMBER;
   taskid                           NUMBER;
   distributiontoalldepotscreated   NUMBER (1, 0);
   starttime                        DATE;
   active                           NUMBER (1, 0);
   client                           NUMBER;
   typeofarchivejob                 NUMBER;
BEGIN
   SELECT clientid
     INTO client
     FROM um_archivejob
    WHERE archivejobid = jobid;

   SELECT completed, starttime, active, typeofarchivejobid
     INTO completed, starttime, active, typeofarchivejob
     FROM um_archivejob
    WHERE archivejobid = jobid;

   IF active = 0
   THEN
      IF starttime > SYSDATE
      THEN
         jobstate := 0;
         RETURN;
      ELSE
         jobstate := 3;
         RETURN;
      END IF;
   END IF;

   IF completed <> 0 OR typeofarchivejob = 3
   THEN
      jobstate := 2;
      RETURN;
   END IF;

   -- Get the number of installed depotserver
   SELECT COUNT (deviceid)
     INTO countdepotserver
     FROM um_device dev, um_unit unit
    WHERE dev.deviceclassid = -1
      AND dev.unitid = unit.unitid
      AND unit.clientid = client;

   --print @CountDepotServer
   distributiontoalldepotscreated := 0;

   --There can be more then 1 DistributionTask. Therefore all Tasks must be checked until one valid task is found,
   --or all tasks are invalid

   --Cursor for all Tasks assigned to the Job with @JobID
   FOR rec IN (SELECT archivedistributiontaskid
                 FROM um_archivedistributiontask
                WHERE (archivejobid = jobid) AND (NOT (started IS NULL)))
   LOOP
      taskid := rec.archivedistributiontaskid;

      -- Check all available tasks

      -- Get the numbers of depotserver loadingstatstics for the current tasks
      SELECT COUNT (um_loadingstatistics.loadingstatisticsid)
        INTO countloadingstatistics
        FROM um_loadingstatistics INNER JOIN um_lstotask
             ON um_loadingstatistics.loadingstatisticsid =
                                               um_lstotask.loadingstatisticsid
             INNER JOIN um_archivedistributiontask
             ON um_lstotask.archivedistributiontaskid =
                          um_archivedistributiontask.archivedistributiontaskid
             INNER JOIN um_device
             ON um_loadingstatistics.deviceid = um_device.deviceid
             INNER JOIN um_unit ON um_device.unitid = um_unit.unitid
       WHERE (um_device.deviceclassid = -1)
         AND (um_archivedistributiontask.archivejobid = jobid)
         AND (um_unit.clientid = client);

      --if there are loadingstatistics for all depotserver, the flag is set to true
      IF countloadingstatistics = countdepotserver
      THEN
         distributiontoalldepotscreated := 1;
      END IF;

      -- if an task has created distributions to all depotservers check the current distribution state
      IF distributiontoalldepotscreated = 1
      THEN
         SELECT COUNT (um_loadingstatistics.loadingstatisticsid)
           INTO countloadingstatistics
           FROM um_lstotask INNER JOIN um_loadingstatistics
                ON um_lstotask.loadingstatisticsid =
                                      um_loadingstatistics.loadingstatisticsid
                INNER JOIN um_device
                ON um_loadingstatistics.deviceid = um_device.deviceid
                INNER JOIN um_unit ON um_device.unitid = um_unit.unitid
          WHERE (um_lstotask.archivedistributiontaskid = taskid)
            AND (um_device.deviceclassid = -1)
            AND (um_loadingstatistics.lsstate = 1)
            AND (um_unit.clientid = client);

         -- if all depotserver has a loadingstatistic with state "Archive available" set state to green,
         -- otherwise, set state to yellow and end the task scan
         IF countloadingstatistics = countdepotserver
         THEN
            UPDATE um_archivejob
               SET completed = 1
             WHERE archivejobid = jobid;

            jobstate := 2;
         END IF;

         IF countloadingstatistics > 0
         THEN
            jobstate := 1;
         END IF;

         IF countloadingstatistics < countdepotserver
         THEN
            jobstate := 1;
         END IF;

         IF countloadingstatistics = 0
         THEN
            jobstate := 3;
         END IF;

         EXIT;
      END IF;
   END LOOP;

   -- if no task can distribute to all depots set state to red.
   IF distributiontoalldepotscreated <> 1
   THEN
      jobstate := 3;
   END IF;
END;
/

--*****************************************

COMMIT;

PROMPT Fertig!

spool off
