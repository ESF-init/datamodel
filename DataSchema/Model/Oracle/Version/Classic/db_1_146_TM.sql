SPOOL db_1_146.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('22.09.2010', 'DD.MM.YYYY'), '1.146', 'FLF','New field in TM_TARIF');



ALTER TABLE tm_tarif
 ADD (
    matrixtariffid  NUMBER(10));


UPDATE tm_tarif set matrixtariffid = tarifid WHERE matrixtariffid is null;
	
COMMIT ;

PROMPT Fertig!

SPOOL off
