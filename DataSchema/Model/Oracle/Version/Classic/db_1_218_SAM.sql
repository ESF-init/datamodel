SPOOL db_1_218.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('07.09.2011', 'DD.MM.YYYY'), '1.218', 'KKV','Change of SAM_PAYMENTSCHEDULE and -DETAIL');

ALTER TABLE SAM_PAYMENTSCHEDULE
	ADD (ISPROLONGABLE NUMBER(1));

ALTER TABLE SAM_PAYMENTSCHEDULEDETAIL
	ADD (PAYERTYPE NUMBER(1));


COMMIT ;

PROMPT Fertig!

SPOOL off
