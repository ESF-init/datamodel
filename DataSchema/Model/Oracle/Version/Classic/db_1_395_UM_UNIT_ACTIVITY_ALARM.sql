SPOOL db_1_395.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate,'1.395', 'MGR','SENDSTATUSMESSAGE added to UM_UNIT_ACTIVITY_ALARM');

ALTER TABLE UM_UNIT_ACTIVITY_ALARM ADD SENDSTATUSMESSAGE NUMBER(1);

COMMIT;
