SPOOL db_1_410.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.410', 'JGI','Added attributes to PP_CONTRACT');

ALTER TABLE PP_CONTRACT
ADD (
  SPAREFIELD3               NUMBER(1) DEFAULT 0,
  SPAREFIELD4               NUMBER(1) DEFAULT 0
);

COMMIT;

PROMPT Fertig!

SPOOL off