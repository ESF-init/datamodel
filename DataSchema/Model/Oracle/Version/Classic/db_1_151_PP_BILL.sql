SPOOL db_1_151.log

INSERT INTO vario_dmodellversion
            (rundate, dmodellno, responsible,
             annotation
            )
     VALUES (TO_DATE ('18.10.2010', 'DD.MM.YYYY'), '1.151', 'JGi',
             'Increased BILLNO in PP_BILL'
            );



ALTER TABLE PP_BILL
 MODIFY (
    BILLNO            NUMBER(10)
	);


COMMIT ;

PROMPT Fertig!

SPOOL off
