SPOOL db_1_425.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.425', 'JUN','TRG_UPDATEVISIBLEUNITNO');


/* Trigger für alte Projekte, in denen Fahrzeuge nicht über Fahrzeugimport angelegt werden */
CREATE OR REPLACE TRIGGER TRG_UPDATEVISIBLEUNITNO
BEFORE INSERT ON UM_UNIT 
FOR EACH ROW
BEGIN
  IF (:NEW.TYPEOFUNITID <> 1 AND :NEW.VISIBLEUNITNO IS NULL)
  THEN
    :NEW.VISIBLEUNITNO := :NEW.UNITNO;
  END IF;
END TRG_UPDATEVISIBLEUNITNO;
/

COMMIT;

PROMPT Fertig!

SPOOL off