SPOOL db_1_498.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.498', 'FLF', 'New Table TM_EXTERNAL_TICKET');

CREATE TABLE TM_EXTERNAL_TICKET
(
  EXTERNALTICKETID      NUMBER(18)              NOT NULL,
  TARIFFID              NUMBER(18)              NOT NULL,
  OPERATORID            NUMBER(18)              NOT NULL,
  FARESTAGEID           NUMBER(18)              NOT NULL,
  EXTERNALTICKETNUMBER  NUMBER(10)              NOT NULL
);

ALTER TABLE TM_EXTERNAL_TICKET ADD (
  PRIMARY KEY
  (EXTERNALTICKETID));

COMMIT;

PROMPT Fertig!

SPOOL off