
spool db_1_013.log

PROMPT Erstellen der  Tabelle VARIO_STOPPOINT 

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '01.12.2008', 'DD.MM.YYYY'), '1.013', 'BTS', 'vario Haltestellenpunkte');

-- *************************


CREATE TABLE VARIO_STOPPOINT
(
  STOPPOINTCODE       VARCHAR2(200 BYTE)        NOT NULL,
  STOPID              NUMBER                    NOT NULL,
  STOPPOINTPOSITION   NUMBER(5),
  STOPPOINTSHORTNAME  VARCHAR2(200 BYTE),
  STOPPOINTLONGNAME   VARCHAR2(200 BYTE),
  TARIFZONEID1        NUMBER,
  TARIFZONEID2        NUMBER,
  TARIFZONEID3        NUMBER,
  TARIFZONEID4        NUMBER,
  CLIENTID            NUMBER,
  VALIDFROM           DATE                      NOT NULL,
  VALIDUNTIL          DATE                      NOT NULL,
  STOPPOINTID         NUMBER
)
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


CREATE UNIQUE INDEX PK_STOPPOINT ON VARIO_STOPPOINT
(STOPPOINTID)
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


ALTER TABLE VARIO_STOPPOINT ADD (
  CONSTRAINT PK_STOPPOINT
 PRIMARY KEY
 (STOPPOINTID)
    USING INDEX 
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));


ALTER TABLE VARIO_STOPPOINT ADD (
  CONSTRAINT FK_STOPPOINT_TARIFZONE 
 FOREIGN KEY (TARIFZONEID1) 
 REFERENCES VARIO_TARIFZONE (TARIFZONEID));

ALTER TABLE VARIO_STOPPOINT ADD (
  CONSTRAINT FK_STOPPOINT_TARIFZONE2 
 FOREIGN KEY (TARIFZONEID2) 
 REFERENCES VARIO_TARIFZONE (TARIFZONEID));

ALTER TABLE VARIO_STOPPOINT ADD (
  CONSTRAINT FK_STOPPOINT_TARIFZONE3 
 FOREIGN KEY (TARIFZONEID3) 
 REFERENCES VARIO_TARIFZONE (TARIFZONEID));

ALTER TABLE VARIO_STOPPOINT ADD (
  CONSTRAINT FK_STOPPOINT_TARIFZONE4 
 FOREIGN KEY (TARIFZONEID4) 
 REFERENCES VARIO_TARIFZONE (TARIFZONEID));

ALTER TABLE VARIO_STOPPOINT ADD (
  CONSTRAINT FK_VARIOSTOPPOINT_VARIO_STOP 
 FOREIGN KEY (STOPID) 
 REFERENCES VARIO_STOP (STOPID));






-- *************************
commit;

PROMPT Fertig!

spool off