SPOOL db_1_386.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
    VALUES (sysdate, '1.386', 'OZS','VDV-KA: Added KEYDERIVEDATALENGTH, KEYDERIVEDATA to VDV_TXTRANSACTION');

ALTER TABLE VDV_TXTRANSACTION ADD 
(
    KEYDERIVEDATALENGTH        NUMBER(10),
    KEYDERIVEDATA              BLOB
)
;
 
COMMIT;

PROMPT Fertig!

SPOOL off