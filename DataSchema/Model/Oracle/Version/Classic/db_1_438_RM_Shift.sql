SPOOL db_1_438.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.438', 'CoS','New column in RM_SHIFT');

ALTER TABLE RM_SHIFT
  add   MountingPlateNo  NUMBER(3);

COMMIT;

PROMPT Fertig!

SPOOL off