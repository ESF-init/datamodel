SPOOL db_1_329.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.329', 'ULB','RM_TRANSACTION_COMMENT renamed to RM_EXT_TRANSACTION');

alter table RM_TRANSACTION_COMMENT add Latitude number(10);

alter table RM_TRANSACTION_COMMENT add Longitude number(10);

alter table RM_TRANSACTION_COMMENT rename to RM_EXT_TRANSACTION;

create or replace  synonym RM_TRANSACTION_COMMENT for RM_EXT_TRANSACTION;



COMMIT;

PROMPT Fertig!

SPOOL off