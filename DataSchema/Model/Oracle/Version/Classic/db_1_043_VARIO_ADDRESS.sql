
spool db_1_043.log

PROMPT neue Attribute in VARIO_ADDRESS

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '10.03.2009', 'DD.MM.YYYY'), '1.043', 'BTS', 'Neue Attribute in VARIO_ADDRESS');

-- *************************

ALTER TABLE VARIO_ADDRESS ADD ( 
 PRIVATEPHONENO                VARCHAR2(20 BYTE));
 
ALTER TABLE VARIO_ADDRESS ADD ( 
 PRIVATEMOBILEPHONENO           VARCHAR2(20 BYTE));

-- *************************
commit;

PROMPT Fertig!

spool off


