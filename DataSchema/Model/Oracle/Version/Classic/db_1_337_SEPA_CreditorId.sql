SPOOL db_1_337.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.337', 'JGI','VARIO_CLIENT: Added creditor identification for SEPA');

ALTER TABLE VARIO_CLIENT ADD (creditorid VARCHAR2(50));

COMMIT;

PROMPT Fertig!

SPOOL off