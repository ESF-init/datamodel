SPOOL db_1_273.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('14.08.2012', 'DD.MM.YYYY'), '1.273', 'DST','Added READONLY flag to TM_ATTRIBUTE.');

ALTER TABLE TM_ATTRIBUTE ADD READONLY NUMBER (1) default 0 NOT NULL;

COMMIT;


PROMPT Fertig!

SPOOL off