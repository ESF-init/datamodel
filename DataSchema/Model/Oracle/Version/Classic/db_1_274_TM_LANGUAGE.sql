SPOOL db_1_274.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('15.08.2012', 'DD.MM.YYYY'), '1.274', 'FLF','TM_LANGUAGE created.');

CREATE TABLE TM_LANGUAGE
(
  LANGUAGEID      NUMBER(10)                    NOT NULL,
  LANGUAGENAME    VARCHAR2(200 BYTE)            NOT NULL,
  LANGUAGESYMBOL  VARCHAR2(50 BYTE)             NOT NULL
);

ALTER TABLE TM_LANGUAGE ADD (
  CONSTRAINT TM_LANGUAGE_PK
 PRIMARY KEY
 (LANGUAGEID));



COMMIT;


PROMPT Fertig!

SPOOL off