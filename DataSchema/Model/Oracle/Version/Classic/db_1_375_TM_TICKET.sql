SPOOL db_1_375.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.375', 'FLF','Added discount column to tm_ticket');



ALTER TABLE TM_TICKET ADD  DISCOUNT        NUMBER(18);

 
COMMIT;

PROMPT Fertig!

SPOOL off