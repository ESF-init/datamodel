SPOOL db_1_389.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.389', 'ULB','Added column clientid to FILELOCATION');

ALTER TABLE FILELOCATION ADD (clientid NUMBER(3) );
COMMIT;

PROMPT Fertig!

SPOOL off