SPOOL db_1_397.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.397', 'JUN','Added column LICENSE to table UM_UNIT');

ALTER TABLE UM_UNIT
ADD (LICENSE VARCHAR2(20 BYTE));


COMMIT;

PROMPT Fertig!

SPOOL off