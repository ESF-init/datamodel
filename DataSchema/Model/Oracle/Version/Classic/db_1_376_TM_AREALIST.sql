SPOOL db_1_376.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.376', 'FLF','Added sequential column to arealist');



ALTER TABLE TM_AREALIST ADD  SEQUENTIAL        NUMBER(1);

 
COMMIT;

PROMPT Fertig!

SPOOL off