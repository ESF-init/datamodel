spool db_1_090.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '27.11.2009', 'DD.MM.YYYY'), '1.090', 'MIW', 'Datenquelle f�r Top-Ups');

--**************************************

ALTER TABLE PP_CARDACTIONREQUEST
 ADD (REQUESTSOURCE  NUMBER(10)  DEFAULT 0);

--*****************************************

COMMIT;

PROMPT Fertig!

spool off
