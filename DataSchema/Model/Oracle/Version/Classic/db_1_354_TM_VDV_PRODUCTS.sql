SPOOL db_1_354.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.354', 'FLF','Created table TM_VDV_PRODUCT');

CREATE TABLE TM_VDV_PRODUCT
(
  VDVPRODUCTID              NUMBER(18)          NOT NULL,
  TARIFFID                  NUMBER(18)          NOT NULL,
  TICKETID                  NUMBER(18)          NOT NULL,
  DISTANCEATTRIBUTEVALUEID  NUMBER(18)          NOT NULL,
  PRODUCTRESPONSIBLE        NUMBER(10)          NOT NULL,
  PRODUCTNUMBER             NUMBER(10)          NOT NULL,
  PASSENGERTYPE             NUMBER(10)          NOT NULL,
  COMPANIONTYPE1            NUMBER(10)          NOT NULL,
  MAXCOMPANIONS1            NUMBER(10)          NOT NULL,
  COMPANIONTYPE2            NUMBER(10)          NOT NULL,
  MAXCOMPANIONS2            NUMBER(10)          NOT NULL,
  TRANSPORTCATEGORY         NUMBER(10)          NOT NULL,
  SERVICECLASS              NUMBER(10)          NOT NULL,
  WRITEPRICE                NUMBER(1)           NOT NULL,
  WRITEVAT                  NUMBER(1)           NOT NULL,
  FARESCALE                 NUMBER(10)          NOT NULL,
  SELLPRODUCTNUMBER         NUMBER(10)          NOT NULL,
  ISSUEBARCODE              NUMBER(1)           NOT NULL,
  ISSUECHIP                 NUMBER(1)           NOT NULL,
  KEYVERSIONPV              NUMBER(10)          NOT NULL,
  KEYVERSIONKVP             NUMBER(10)          NOT NULL,
  KEYVERSIONAUTH            NUMBER(10)          NOT NULL,
  KEYPRODUCTORGID           NUMBER(10)          NOT NULL
);


CREATE UNIQUE INDEX TM_VDV_PRODUCT_PK ON TM_VDV_PRODUCT
(VDVPRODUCTID);


ALTER TABLE TM_VDV_PRODUCT ADD (
  CONSTRAINT TM_VDV_PRODUCT_PK
 PRIMARY KEY
 (VDVPRODUCTID));
 
COMMIT;

PROMPT Fertig!

SPOOL off