SPOOL db_1_244.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('15.02.2012', 'DD.MM.YYYY'), '1.244', 'JGi','Added FIRSTTRIPID to PP_VALIDATEDTICKET');

ALTER TABLE PP_VALIDATEDTICKET
 ADD (FIRSTTRIPID  NUMBER(10));


COMMIT;

PROMPT Fertig!

SPOOL off
