SPOOL db_1_351.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.351', 'JGI','Rebnamed column IBAN to BIC in PP_BANK');

ALTER TABLE PP_BANK RENAME COLUMN IBAN TO BIC;
 
COMMIT;

PROMPT Fertig!

SPOOL off