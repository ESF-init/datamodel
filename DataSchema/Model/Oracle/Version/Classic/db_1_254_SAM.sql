SPOOL db_1_254.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('12.04.2012', 'DD.MM.YYYY'), '1.254', 'KKV','Changes to SAM_TICKET_TO_ACCOUNTENTRY');

ALTER TABLE SAM_TICKET_TO_ACCOUNTENTRY ADD (
	RELATIONID NUMBER(10));

ALTER TABLE SAM_TICKET_TO_ACCOUNTENTRY ADD (
	VIA NUMBER(10));

ALTER TABLE SAM_TICKET_TO_ACCOUNTENTRY ADD (
	FAREMATRIXENTRYID NUMBER(10));

COMMIT;

PROMPT Done!

SPOOL off