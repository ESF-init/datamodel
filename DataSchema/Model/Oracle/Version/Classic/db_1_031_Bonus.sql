
spool db_1_031.log

PROMPT Tabelle f�r Bonus Schema

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '28.01.2009', 'DD.MM.YYYY'), '1.031', 'ULB', 'Tabelle f�r Bonus Schema');

-- *************************


CREATE TABLE VARIO_BONUS
(
  ID               NUMBER(10)                   DEFAULT 0                     NOT NULL,
  TYPID            NUMBER(2)                    DEFAULT 0                     NOT NULL,
  BONUSPERCENTAGE  NUMBER(3)                    DEFAULT 0                     NOT NULL,
  MINAMOUNT        NUMBER(6)                    NOT NULL
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE VARIO_BONUS ADD (
  PRIMARY KEY
 (ID)
    USING INDEX );




-- *************************
commit;

PROMPT Fertig!

spool off


