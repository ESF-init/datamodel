SPOOL db_1_449.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.449', 'BVI', 'Column in AM_COMPONENTCLEARING nullable.');

Alter table AM_COMPONENTCLEARING modify (CASHUNITID null);

COMMIT;

PROMPT Fertig!

SPOOL off