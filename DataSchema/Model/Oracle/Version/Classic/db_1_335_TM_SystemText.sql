SPOOL db_1_335.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.335', 'FLF','System Text');

 ALTER TABLE TM_SYSTEMTEXT 		ADD		  ORIGINALTEXT  VARCHAR2(1000 CHAR);
 
COMMIT;

PROMPT Fertig!

SPOOL off