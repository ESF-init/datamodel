SPOOL db_1_315.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.315', 'FLF','Added MANDATORY field to TM_BusinessRuleTypeToVariable');

 ALTER TABLE TM_BUSINESSRULETYPETOVARIABLE ADD   MANDATORY               NUMBER(1)             DEFAULT 0                     NOT NULL;

COMMIT;

PROMPT Fertig!

SPOOL off