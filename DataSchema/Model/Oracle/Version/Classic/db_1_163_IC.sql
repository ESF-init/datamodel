spool db_1_163.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '10.12.2010', 'DD.MM.YYYY'), '1.163', 'MKK', 'Procedure now calls p_setjobcomplete instead of p_getarchivedistributionstate');

CREATE OR REPLACE PROCEDURE updatejobstate_ddm (
   inclientid   IN   NUMBER,
   infrom       IN   DATE
)
IS
   tmpvar   NUMBER;
/******************************************************************************
   NAME:       UPDATEJOBSTATE_DDM
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        18.10.2010          1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     UPDATEJOBSTATE_DDM
      Sysdate:         18.10.2010
      Date and Time:   18.10.2010, 08:21:45, and 18.10.2010 08:21:45
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
BEGIN
   FOR rec IN (SELECT archivejobid, typeofarchivejobid
                 FROM um_archivejob
                WHERE (jobstate <> 2 OR jobstate IS NULL)
                  AND clientid = inclientid
                  AND starttime > infrom)
   LOOP
      IF rec.typeofarchivejobid = 4
      THEN
         p_getresetdistributionstate (rec.archivejobid, tmpvar);

         UPDATE um_archivejob
            SET jobstate = tmpvar
          WHERE archivejobid = rec.archivejobid;
      ELSE
         p_setjobcomplete (rec.archivejobid);
      END IF;
   END LOOP;
END updatejobstate_ddm;
/
COMMIT;

PROMPT Fertig!

spool off
