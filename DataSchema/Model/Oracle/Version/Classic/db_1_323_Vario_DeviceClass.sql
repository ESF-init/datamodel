SPOOL db_1_323.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.323', 'DST','Add column (ParameterReleaseVisibility  to Vario_DeviceClass)');

ALTER TABLE VARIO_DEVICECLASS ADD (PARAMETERRELEASEVISIBILITY NUMBER(1) DEFAULT 0);

COMMIT;


PROMPT Fertig!

SPOOL off