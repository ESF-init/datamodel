SPOOL db_1_310.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.310', 'FLF','Business Rules');

CREATE TABLE TM_BUSINESSRULE
(
  BUSINESSRULEID      NUMBER(18)                NOT NULL,
  BUSINESSRULENAME    VARCHAR2(250 BYTE)        NOT NULL,
  BUSINESSRULETYPEID  NUMBER(18)                NOT NULL,
  TARIFFID            NUMBER(18)                NOT NULL,
  ISDEFAULT           NUMBER(1)                 NOT NULL,
  READONLY            NUMBER(1)                 NOT NULL
);

ALTER TABLE TM_BUSINESSRULE ADD (
  CONSTRAINT TM_BUSINESSRULE_PK
 PRIMARY KEY
 (BUSINESSRULEID));
 
 
 
CREATE TABLE TM_BUSINESSRULECLAUSE
(
  BUSINESSRULECLAUSEID     NUMBER(18)           NOT NULL,
  BUSINESSRULEID           NUMBER(18)           NOT NULL,
  BUSINESSRULECONDITIONID  NUMBER(18),
  BUSINESSRULERESULTID     NUMBER(18)           NOT NULL,
  ORDERNUMBER              NUMBER(10)           NOT NULL
);

ALTER TABLE TM_BUSINESSRULECLAUSE ADD (
  PRIMARY KEY
 (BUSINESSRULECLAUSEID));
 
 
 
CREATE TABLE TM_BUSINESSRULECONDITION
(
  BUSINESSRULECONDITIONID  NUMBER(18)           NOT NULL,
  BUSINESSRULETYPEID       NUMBER(18)           NOT NULL,
  TARIFFID                 NUMBER(18)           NOT NULL,
  CRITERIA                 CLOB                 NOT NULL,
  CONDITIONNAME            VARCHAR2(250 BYTE)
);

ALTER TABLE TM_BUSINESSRULECONDITION ADD (
  CONSTRAINT TM_BUSINESSRULECONDITION_PK
 PRIMARY KEY
 (BUSINESSRULECONDITIONID));

 

CREATE TABLE TM_BUSINESSRULERESULT
(
  BUSINESSRULERESULTID  NUMBER(18)              NOT NULL,
  BUSINESSRULETYPEID    NUMBER(18)              NOT NULL,
  TARIFFID              NUMBER(18)              NOT NULL,
  VALID                 NUMBER(1)               NOT NULL,
  SUPPLEMENTTICKET      NUMBER(18),
  RESULTNAME            VARCHAR2(250 BYTE),
  SURCHARGE             NUMBER(18)
);

ALTER TABLE TM_BUSINESSRULERESULT ADD (
  CONSTRAINT TM_BUSINESSRULERESULT_PK
 PRIMARY KEY
 (BUSINESSRULERESULTID));



CREATE TABLE TM_BUSINESSRULETYPE
(
  BUSINESSRULETYPENAME         VARCHAR2(250 BYTE) NOT NULL,
  BUSINESSRULETYPEID           NUMBER(18)       NOT NULL,
  BUSINESSRULETYPEGROUPNUMBER  NUMBER(10)       NOT NULL,
  DESCRIPTION                  VARCHAR2(2000 CHAR)
);

ALTER TABLE TM_BUSINESSRULETYPE ADD (
  CONSTRAINT TM_BUSINESSRULETYPE_PK
 PRIMARY KEY
 (BUSINESSRULETYPEID));


 
CREATE TABLE TM_BUSINESSRULETYPETOVARIABLE
(
  BUSINESSRULETYPEID      NUMBER(18)            NOT NULL,
  BUSINESSRULEVARIABLEID  NUMBER(18)            NOT NULL
);

ALTER TABLE TM_BUSINESSRULETYPETOVARIABLE ADD (
  CONSTRAINT TM_BUSINESSRULETYPETOVARIAB_PK
 PRIMARY KEY
 (BUSINESSRULETYPEID, BUSINESSRULEVARIABLEID));
 

 
CREATE TABLE TM_BUSINESSRULEVARIABLE
(
  BUSINESSRULEVARIABLEID      NUMBER(18)        NOT NULL,
  BUSINESSRULEVARIABLENUMBER  NUMBER(10)        NOT NULL,
  BUSINESSRULEVARIABLENAME    VARCHAR2(250 BYTE) NOT NULL,
  DESCRIPTION                 VARCHAR2(2000 CHAR),
  KEYSTRING                   VARCHAR2(200 BYTE) NOT NULL
);

ALTER TABLE TM_BUSINESSRULEVARIABLE ADD (
  CONSTRAINT TM_BUSINESSRULEVARIABLE_PK
 PRIMARY KEY
 (BUSINESSRULEVARIABLEID));
 
 
 
 ALTER TABLE TM_TICKET ADD  BRTYPE1ID  NUMBER(18);
 ALTER TABLE TM_TICKET ADD  BRTYPE2ID  NUMBER(18);
 
COMMIT;

PROMPT Fertig!

SPOOL off