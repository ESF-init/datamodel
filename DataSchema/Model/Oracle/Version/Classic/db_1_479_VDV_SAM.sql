SPOOL db_1_479.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.479', 'ULB','VDV Loadkey + Sam_status');

CREATE TABLE VDV_LoadKeyMessage
(
  messageid       NUMBER(18),
  samid           NUMBER(18),
  LoadKeyCounter  NUMBER(10),
  loadkeynumber   NUMBER(10),
  loadkeystatus   NUMBER(10),
  loadkeyresult   NUMBER(10)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;


ALTER TABLE VDV_LoadKeyMessage ADD (
  CONSTRAINT VDV_LoadKeyMessage_PK
 PRIMARY KEY
 (messageid));

 
 ALTER TABLE VDV_LoadKeyMessage ADD (
  CONSTRAINT UM_loadkey_SAM_FK1 
 FOREIGN KEY (samid) 
 REFERENCES UM_sam (samid));
 
 
 
 
 CREATE TABLE VDV_SamStatus
(
  statusid        NUMBER(18),
  samid           NUMBER(18),
  componentid     NUMBER(18),
  loadkeycounter  NUMBER(10),
  CertEOV         DATE,
  samversion      VARCHAR2(20),
  terminaltype    NUMBER(10),
  terminalno      NUMBER(10),
  terminalorgid   NUMBER(10),
  samseqNo        NUMBER(10)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;


ALTER TABLE VDV_SamStatus ADD (
  CONSTRAINT VDV_SamStatus_PK
 PRIMARY KEY
 (statusid));

ALTER TABLE VDV_SamStatus ADD (
  FOREIGN KEY (samid) 
 REFERENCES UM_SAM (samid));
 
 
 
COMMIT;

PROMPT Fertig!

SPOOL off
