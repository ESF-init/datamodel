
spool db_1_016.log

PROMPT Erstellen Tabelle DM_Qualification

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '10.12.2008', 'DD.MM.YYYY'), '1.016', 'MSC', 'DM_Qualification Tabelle erstellen');

-- *************************


CREATE TABLE DM_QUALIFICATION
(
  QUALIFICATIONID  NUMBER(10)                NOT NULL,
  QUALIFICATION    VARCHAR2(100 BYTE),
  DEBTORID         NUMBER(10)                   NOT NULL
)
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


ALTER TABLE DM_QUALIFICATION ADD (
  PRIMARY KEY
 (QUALIFICATIONID)
    USING INDEX 
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));

-- *************************
commit;

PROMPT Fertig!

spool off