SPOOL db_1_180.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('10.03.2011', 'DD.MM.YYYY'), '1.180', 'HSS','List Layout Type');

ALTER TABLE TM_LISTLAYOUTOBJECT
ADD LISTLAYOUTTYPEID  NUMBER(10);

CREATE TABLE TM_LISTLAYOUTTYPE
(
  LISTLAYOUTID  NUMBER(10)                          NOT NULL,
  NAME          VARCHAR2(50 BYTE),
  NXNAME        VARCHAR2(50 BYTE)
);

PROMPT Fertig!

SPOOL off
