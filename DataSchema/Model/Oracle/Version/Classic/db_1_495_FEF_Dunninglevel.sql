SPOOL db_1_495.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.495', 'SMF', 'Extension for FareEvasionFine implementation.');

ALTER TABLE VARIO_DUNNINGLEVEL ADD (Fee NUMBER(9,0));

COMMIT;

PROMPT Fertig!

SPOOL off