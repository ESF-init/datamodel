
spool db_1_042.log

PROMPT neue Attribute in DM_DEBTOR

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '10.03.2009', 'DD.MM.YYYY'), '1.042', 'BTS', 'Neue Attribute in DM_DEBTOR');

-- *************************

ALTER TABLE DM_DEBTOR ADD ( 
 STAFFIDENTIFIER                VARCHAR2(50 BYTE));
 
 
-- *************************
commit;

PROMPT Fertig!

spool off


