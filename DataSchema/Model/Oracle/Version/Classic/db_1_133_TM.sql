SPOOL db_1_133.log

INSERT INTO vario_dmodellversion
            (rundate, dmodellno, responsible,
             annotation
            )
     VALUES (TO_DATE ('10.08.2010', 'DD.MM.YYYY'), '1.133', 'FLF',
             'New Table vario_operator'
            );



CREATE TABLE vario_operator
(
  operatorid  NUMBER(10),
  number_     NUMBER(10),
  name_       VARCHAR2(50 BYTE),
  clientid    NUMBER(10)                        DEFAULT 0
);


ALTER TABLE vario_operator ADD (
  CONSTRAINT vario_operator_pk
 PRIMARY KEY
 (operatorid)
);



ALTER TABLE tm_external_packeteffort
 ADD (operatorid  NUMBER(10));
 
ALTER TABLE tm_external_packeteffort
 ADD (cardtype NUMBER(10));

COMMIT ;

PROMPT Fertig!

SPOOL off
