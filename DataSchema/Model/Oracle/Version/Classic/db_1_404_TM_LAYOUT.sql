SPOOL db_1_404.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.404', 'FLF','Added new columns to tm tables');

 
 
ALTER TABLE TM_SYSTEMFIELD
 ADD (LAYOUTOBJECTWIDTH_MM  NUMBER(12,2));

 
CREATE TABLE TM_SYSTEMFIELDDYNGRAPHLO
(
  LAYOUTOBJECTID  NUMBER(18)                    NOT NULL,
  LAYOUTID        NUMBER(18)                    NOT NULL,
  ROW_            NUMBER(18)                    NOT NULL,
  COL_            NUMBER(18)                    NOT NULL,
  SYSTEMFIELDID   NUMBER(18)                    NOT NULL,
  ISINVERSE       NUMBER(1)                     NOT NULL,
  WIDTH           NUMBER(2)                     NOT NULL,
  HEIGHT          NUMBER(2)                     NOT NULL
);


CREATE UNIQUE INDEX TM_SYSTEMFIELDDYNGRAPHLO_PK ON TM_SYSTEMFIELDDYNGRAPHLO
(LAYOUTOBJECTID);


ALTER TABLE TM_SYSTEMFIELDDYNGRAPHLO ADD (
  CONSTRAINT TM_SYSTEMFIELDDYNGRAPHLO_PK
 PRIMARY KEY
 (LAYOUTOBJECTID));

 
 
 
COMMIT;

PROMPT Fertig!

SPOOL off