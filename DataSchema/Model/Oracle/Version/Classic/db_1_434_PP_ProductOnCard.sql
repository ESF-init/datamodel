SPOOL db_1_434.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.434', 'FMT','Added expiration to product on card');

ALTER TABLE PP_ProductONCard add(EXPIRY DATE);

COMMIT;

PROMPT Fertig!

SPOOL off