SPOOL db_1_134.log

INSERT INTO vario_dmodellversion
            (rundate, dmodellno, responsible,
             annotation
            )
     VALUES (TO_DATE ('10.08.2010', 'DD.MM.YYYY'), '1.134', 'FLF',
             'New field in TM_EXTERNAL_PACKETEFFORT'
            );



ALTER TABLE tm_external_packeteffort
 ADD (
    SERVICEID  NUMBER(10));



COMMIT ;

PROMPT Fertig!

SPOOL off
