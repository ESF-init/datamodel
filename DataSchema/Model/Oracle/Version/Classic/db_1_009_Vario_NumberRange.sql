
spool db_1_009.log

PROMPT Erweitere Tabelle RM_Settlement 

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '23.10.2008', 'DD.MM.YYYY'), '1.009', 'ULB/WRA', 'vario nummernkreise');

-- *************************

CREATE TABLE VARIO_NUMBER_RANGE
(
	ID	NUMBER(10) NOT NULL,
	CLIENTID	NUMBER(10) DEFAULT -1 NOT NULL,
	DEPOT	NUMBER(10) DEFAULT -1  NOT NULL,
	NAME	VARCHAR2(100) NOT NULL,
	DESCRIPTION	VARCHAR2(100) NOT NULL,
	RTYPE	VARCHAR2(100) NOT NULL,
	RPREFIX	VARCHAR2(100),
	RSTART	NUMBER DEFAULT 1 NOT NULL,
	REND	NUMBER DEFAULT 9999999999 NOT NULL,
	RINCREMENT	NUMBER DEFAULT 1 NOT NULL,
	RNEXT	NUMBER DEFAULT 1 NOT NULL,
	RCYCLE	NUMBER(1) DEFAULT 0  NOT NULL,
	CONSTRAINT PK_NUMBER_RANGE PRIMARY KEY (ID )
);

CREATE UNIQUE INDEX VARIORANGEIDENT
 ON VARIO_NUMBER_RANGE(CLIENTID, DEPOT, NAME)	;



-- *************************
commit;

PROMPT Fertig!

spool off