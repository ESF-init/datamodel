spool db_1_092.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '30.11.2009', 'DD.MM.YYYY'), '1.092', 'FLF', 'Insert Stored Procedures hinzugefügt');

--**************************************

CREATE OR REPLACE PROCEDURE insert_choice (
   choice      IN   NUMBER,
   tarif       IN   NUMBER,
   ticket      IN   NUMBER,
   value_      IN   VARCHAR,
   text        IN   VARCHAR,
   distance    IN   NUMBER,
   routename   IN   VARCHAR
)
IS
BEGIN
   INSERT INTO tm_choice
               (choiceid, tarifid, ticketid, valuestring, textstring,
                distanceattributeid, routenameid
               )
        VALUES (choice, tarif, ticket, value_, text,
                distance, routename
               );
END insert_choice;
/

CREATE OR REPLACE PROCEDURE insert_farematrixentry (
   farematrixentry       IN   NUMBER,
   boarding              IN   NUMBER,
   destination           IN   NUMBER,
   fare_                 IN   NUMBER,
   farematrix            IN   NUMBER,
   distance_             IN   NUMBER,                              /*(10,3)*/
   via                   IN   NUMBER,
   realdistance_         IN   NUMBER,                               /*(10,3)*/
   averagedistance_      IN   NUMBER,                               /*(10,3)*/
   tariffattribute       IN   NUMBER,
   distanceattribute     IN   NUMBER,
   description_          IN   VARCHAR,
   priority_             IN   NUMBER,
   shortname_            IN   VARCHAR,
   longname_             IN   VARCHAR,
   linefilterattribute   IN   NUMBER,
   serviceallocation     IN   NUMBER,
   routename             IN   NUMBER,
   directionattribute    IN   NUMBER
)
IS
BEGIN
   INSERT INTO tm_farematrixentry
               (farematrixentryid, boardingid, destinationid, fare,
                farematrixid, distance, viaid, realdistance,
                averagedistance, tariffattributeid, distanceattributeid,
                description, priority, shortname, longname,
                linefilterattributeid, serviceallocationid, routenameid,
                directionattributeid
               )
        VALUES (farematrixentry, boarding, destination, fare_,
                farematrix, distance_, via, realdistance_,
                averagedistance_, tariffattribute, distanceattribute,
                description_, priority_, shortname_, longname_,
                linefilterattribute, serviceallocation, routename,
                directionattribute
               );
END insert_farematrixentry;
/

CREATE OR REPLACE PROCEDURE insert_farestage (
   farestage          IN   NUMBER,
   farestagelist      IN   NUMBER,
   num_               IN   NUMBER,
   name_              IN   VARCHAR,
   shortname_         IN   VARCHAR,
   parent_            IN   NUMBER,
   selectable_        IN   NUMBER,
   hierarchielevel_   IN   NUMBER,
   farestageclass_    IN   NUMBER
)
IS
BEGIN
   INSERT INTO tm_farestage
               (farestageid, farestagelistid, number_, NAME, shortname,
                parentid, selectable, hierarchielevel, farestageclass
               )
        VALUES (farestage, farestagelist, num_, name_, shortname_,
                parent_, selectable_, hierarchielevel_, farestageclass_
               );
END insert_farestage;
/

CREATE OR REPLACE PROCEDURE insert_farestagealias (
   farestagealias    IN   NUMBER,
   name_             IN   VARCHAR,
   description_      IN   VARCHAR,
   farestage         IN   NUMBER,
   farestageclass_   IN   NUMBER,
   externalnumber_   IN   NUMBER,
   attributevalue    IN   NUMBER
)
IS
BEGIN
   INSERT INTO tm_farestagealias
               (farestagealiasid, NAME, description, farestageid,
                farestageclass, externalnumber, attributevalueid
               )
        VALUES (farestagealias, name_, description_, farestage,
                farestageclass_, externalnumber_, attributevalue
               );
END insert_farestagealias;
/

CREATE OR REPLACE PROCEDURE insert_faretabelentry (
   faretableentry   IN   NUMBER,
   fare1_           IN   NUMBER,
   fare2_           IN   NUMBER,
   faretable        IN   NUMBER,
   attributevalue   IN   NUMBER,
   distance_        IN   NUMBER,
   shortcode_       IN   NUMBER,
   externalname_    IN   VARCHAR
)
IS
BEGIN
   INSERT INTO tm_faretableentry
               (faretableentryid, fare1, fare2, faretableid,
                attributevalueid, distance, shortcode, externalname
               )
        VALUES (faretableentry, fare1_, fare2_, faretable,
                attributevalue, distance_, shortcode_, externalname_
               );
END insert_faretabelentry;
/

CREATE OR REPLACE PROCEDURE insert_printtext (
   printtextid_   IN   NUMBER,
   printtext_     IN   VARCHAR,
   tarif          IN   NUMBER
)
IS
BEGIN
   INSERT INTO tm_printtext
               (printtextid, printtext, tarifid
               )
        VALUES (printtextid_, printtext_, tarif
               );
END insert_printtext;
/

CREATE OR REPLACE PROCEDURE insert_route (
   route                  IN   NUMBER,
   routename              IN   NUMBER,
   routetext_             IN   VARCHAR,
   tarif                  IN   NUMBER,
   exchangeable_          IN   NUMBER,
   printtext1             IN   NUMBER,
   printtext2             IN   NUMBER,
   printtext3             IN   NUMBER,
   tariffattribute        IN   NUMBER,
   ticketgroupattribute   IN   NUMBER
)
IS
BEGIN
   INSERT INTO tm_route
               (routeid, routenameid, routetext, tarifid, exchangeable,
                printtext1id, printtext2id, printtext3id, tariffattributeid,
                ticketgroupattributeid
               )
        VALUES (route, routename, routetext_, tarif, exchangeable_,
                printtext1, printtext2, printtext3, tariffattribute,
                ticketgroupattribute
               );
END insert_route;
/

CREATE OR REPLACE PROCEDURE insert_routename (
   routename      IN   NUMBER,
   num_           IN   NUMBER,
   name_          IN   VARCHAR,
   description_   IN   VARCHAR,
   tarif          IN   NUMBER
)
IS
BEGIN
   INSERT INTO tm_routename
               (routenameid, number_, NAME, description, tarifid
               )
        VALUES (routename, num_, name_, description_, tarif
               );
END insert_routename;
/

CREATE OR REPLACE PROCEDURE insert_serviceallocation (
   serviceallocation   IN   NUMBER,
   name_               IN   VARCHAR,
   description_        IN   VARCHAR,
   tarif               IN   NUMBER
)
IS
BEGIN
   INSERT INTO tm_serviceallocation
               (serviceallocationid, NAME, description, tarifid
               )
        VALUES (serviceallocation, name_, description_, tarif
               );
END insert_serviceallocation;
/


CREATE OR REPLACE PROCEDURE insert_servicepercentage (
   servicepercentage   IN   NUMBER,
   servicename_        IN   VARCHAR,
   service             IN   VARCHAR,
   percentage_         IN   NUMBER,
   serviceallocation   IN   NUMBER
)
IS
BEGIN
   INSERT INTO tm_servicepercentage
               (servicepercentageid, servicename, serviceid, percentage,
                serviceallocationid
               )
        VALUES (servicepercentage, servicename_, service, percentage_,
                serviceallocation
               );
END insert_servicepercentage;
/


--*****************************************

COMMIT;

PROMPT Fertig!

spool off
