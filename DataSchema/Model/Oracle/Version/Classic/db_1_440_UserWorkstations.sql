SPOOL db_1_440.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.440', 'BVI','New tables to define workstations per vario user.');

Create Table User_Workstation (
    WorkstationID NUMBER(18,0) NOT NULL,
    ComputerName NVARCHAR2(15) NOT NULL,
	ClientID NUMBER(18,0) NOT NULL,
    CONSTRAINT PK_UserWS PRIMARY KEY (WorkstationID),
	CONSTRAINT UN_UserWSUniques UNIQUE (ComputerName, ClientID),
	CONSTRAINT FK_UserWS_Client FOREIGN KEY (ClientID) REFERENCES Vario_Client(ClientID)
);

Create Table User_UserToWorkstation (
    WorkstationID NUMBER(18,0) NOT NULL,
    UserID NUMBER(10,0) NOT NULL,
    CONSTRAINT PK_UserToWS PRIMARY KEY (WorkstationID, UserID),
    CONSTRAINT FK_UserToWS_Workstation FOREIGN KEY (WorkstationID) REFERENCES User_Workstation(WorkstationID),
    CONSTRAINT FK_UserToWS_User FOREIGN KEY (UserID) REFERENCES User_List(USERID)
);



COMMIT;

PROMPT Fertig!

SPOOL off