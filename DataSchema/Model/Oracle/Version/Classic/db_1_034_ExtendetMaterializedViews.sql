
spool db_1_034.log

PROMPT Neue Attribute f�r RM_Transaction

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '24.02.2009', 'DD.MM.YYYY'), '1.034', 'HSS', 'Neue Attribute f�r MATERIALIZED VIEWS STAT_DESTINATION_TRIANGLE und STAT_RM_TRANSACTIONS');

-- *************************

PROMPT STAT_RM_TRANSACTIONS
DROP MATERIALIZED VIEW STAT_DESTINATION_TRIANGLE;

CREATE MATERIALIZED VIEW STAT_DESTINATION_TRIANGLE 
TABLESPACE VARIO_STAT
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT transactionid, tripdatetime, clientid, startzone, 
          FROMFARESTAGE AS startzonename, destzone, 
          TOFARESTAGE AS destzonename, ticketid, 
          revenue / 100 , debtorno, 
    stopfrom, stopto,
	typeid
    FROM 
   (SELECT transactionid, tripdatetime, clientid, 
   fromtariffzone AS startZone, 
    totariffzone AS destZone, 
 FROMFARESTAGE, 
 TOFARESTAGE, 
    ticketid, 
 linename, 
  tariffid, 
  stopfrom, stopto, 
  typeid,
          price  AS revenue, debtorno 
     FROM STAT_RM_TRANSACTIONS 
    WHERE (fromtariffzone >= totariffzone) 
      AND (cancellationid = 0) 
      AND (isbooked = 1) 
      AND (shiftstateid < 10) 
      AND (typeid NOT IN (1, 2, 3)) 
UNION 
   SELECT transactionid, tripdatetime, clientid, 
   totariffzone AS startZone, 
    fromtariffzone AS destZone, 
  TOFARESTAGE, 
  FROMFARESTAGE, 
    ticketid, 
 linename, 
 tariffid, 
 stopfrom, stopto,
 typeid, 
         price AS revenue, debtorno 
     FROM STAT_RM_TRANSACTIONS 
    WHERE (fromtariffzone < totariffzone) 
      AND (cancellationid = 0) 
      AND (isbooked = 1) 
      AND (shiftstateid < 10) 
      AND (typeid NOT IN (1, 2, 3)));




PROMPT CREATE MATERIALIZED VIEW STAT_DESTINATION_TRIANGLE 
DROP MATERIALIZED VIEW STAT_DESTINATION_TRIANGLE;

CREATE MATERIALIZED VIEW  STAT_DESTINATION_TRIANGLE 
TABLESPACE VARIO_STAT
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT transactionid, tripdatetime, clientid, startzone, 
          FROMFARESTAGE AS startzonename, destzone, 
          TOFARESTAGE AS destzonename, ticketid, 
          revenue / 100 , debtorno,
    stopfrom, stopto
    FROM 
   (SELECT transactionid, tripdatetime, clientid, 
   fromtariffzone AS startZone, 
    totariffzone AS destZone, 
 FROMFARESTAGE, 
 TOFARESTAGE, 
    ticketid, 
 linename, 
  tariffid, 
  stopfrom, stopto,
          price  AS revenue, debtorno 
     FROM STAT_RM_TRANSACTIONS 
    WHERE (fromtariffzone >= totariffzone) 
      AND (cancellationid = 0) 
      AND (isbooked = 1) 
      AND (shiftstateid < 10) 
      AND (typeid NOT IN (1, 2, 3)) 
UNION 
   SELECT transactionid, tripdatetime, clientid, 
   totariffzone AS startZone, 
    fromtariffzone AS destZone, 
  TOFARESTAGE, 
  FROMFARESTAGE, 
    ticketid, 
 linename, 
 tariffid, 
 stopfrom, stopto,
         price AS revenue, debtorno 
     FROM STAT_RM_TRANSACTIONS 
    WHERE (fromtariffzone < totariffzone) 
      AND (cancellationid = 0) 
      AND (isbooked = 1) 
      AND (shiftstateid < 10) 
      AND (typeid NOT IN (1, 2, 3)));


-- *************************
commit;

PROMPT Fertig!

spool off


