SPOOL db_1_291.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('10.10.2012', 'DD.MM.YYYY'), '1.291', 'FLF','VARIO_PHYSICALCARDTYPE created.');

CREATE TABLE VARIO_PHYSICALCARDTYPE
(
  PHYSICALCARDTYPEID      NUMBER(18)            NOT NULL,
  NAME                    NVARCHAR2(50)         NOT NULL,
  ABBREVIATION            NVARCHAR2(50),
  DESCRIPTION             NVARCHAR2(50)         NOT NULL,
  CATEGORY                NUMBER(9)             NOT NULL,
  MAXIMUMCAPPINGPOTCOUNT  NUMBER(9)             DEFAULT 0                     NOT NULL,
  DEVICEMAPPINGNUMBER     NUMBER(9)             DEFAULT 0                     NOT NULL
);

ALTER TABLE VARIO_PHYSICALCARDTYPE ADD (
  CONSTRAINT PK_PHYSICALCARDTYPE
 PRIMARY KEY
 (PHYSICALCARDTYPEID));


COMMIT;


PROMPT Fertig!

SPOOL off