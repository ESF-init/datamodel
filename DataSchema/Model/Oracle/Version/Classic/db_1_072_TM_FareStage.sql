spool db_1_072.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '23.09.2009', 'DD.MM.YYYY'), '1.072', 'FLF', 'Erweiterung TM_FARESTGAE um HierarchieLevel');

--**************************************

ALTER TABLE TM_FARESTAGE ADD (HIERARCHIELEVEL  NUMBER(10));

--*****************************************

COMMIT;

PROMPT Fertig!

spool off