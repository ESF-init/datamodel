spool db_1_097.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '26.01.2010', 'DD.MM.YYYY'), '1.097', 'JGi', 'Spalte ORDERNUMBER in Tabelle PP_SHIPMENTADRESS erweitert');

--**************************************
ALTER TABLE PP_SHIPMENTADRESS add (ORDERNUMBER VARCHAR2(50));

--*****************************************

COMMIT;

PROMPT Fertig!

spool off
