SPOOL db_1_388.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.388', 'ULB','Added column courseno to RM_EXT_TRANSACTION');

ALTER TABLE RM_EXT_TRANSACTION ADD (routecode varchar(20));
COMMIT;

PROMPT Fertig!

SPOOL off