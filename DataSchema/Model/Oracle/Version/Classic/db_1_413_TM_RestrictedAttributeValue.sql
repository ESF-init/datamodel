SPOOL db_1_413.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.413', 'FLF','Added new column to TM_ATTRIBUTEVALUE');

ALTER TABLE TM_ATTRIBUTEVALUE
 ADD (RESTRICTED  NUMBER(1)                         DEFAULT 0                     NOT NULL);

COMMIT;

PROMPT Fertig!

SPOOL off