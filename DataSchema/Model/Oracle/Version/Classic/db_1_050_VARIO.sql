
spool db_1_050.log


INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '15.04.2009', 'DD.MM.YYYY'), '1.050', 'NM', 'Anpassung des DMODELLS an Stand STBA');




CREATE TABLE RM_BINARYDATA
(
  ID           NUMBER(10)                       NOT NULL,
  LENGTH       NUMBER(10)                       NOT NULL,
  DATATYPE     NUMBER(10)                       NOT NULL,
  DATA         LONG RAW,
  EXPORTSTATE  NUMBER(2)                        DEFAULT 0
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE RM_BINARYDATA ADD (
  PRIMARY KEY
 (ID)
    USING INDEX 
);


CREATE TABLE TM_STOP
(
  STOPID                      NUMBER(22,10)     NOT NULL,
  STOPNO                      NUMBER(22,10)     NOT NULL,
  STOPNAME                    VARCHAR2(100 BYTE),
  FARESTAGENO                 NUMBER(22,10)     NOT NULL,
  CLIENTID                    NUMBER(10)        DEFAULT 0                     NOT NULL,
  DEPOTID                     NUMBER(10)        DEFAULT 0                     NOT NULL,
  CITYDISTRICTNO              NUMBER(10),
  CITYDISTRICTNAME            VARCHAR2(100 BYTE),
  CITYNO                      NUMBER(10),
  CITYNAME                    VARCHAR2(100 BYTE),
  REGIONNO                    NUMBER(10),
  REGIONNAME                  VARCHAR2(100 BYTE),
  REGIONCODE                  VARCHAR2(100 BYTE),
  ADMINISTRATIONDISTRICTNO    NUMBER(10),
  ADMINISTRATIONDISTRICTNAME  VARCHAR2(100 BYTE),
  ADMINISTRATIONDISTRICTCODE  VARCHAR2(100 BYTE),
  FEDERALSTATENO              NUMBER(10),
  FEDERALSTATENAME            VARCHAR2(100 BYTE),
  COUNTRYNO                   NUMBER(10),
  COUNTRYNAME                 VARCHAR2(100 BYTE),
  COUNTRYCODE                 VARCHAR2(100 BYTE),
  VALIDFROM                   DATE,
  STOPCODE                    VARCHAR2(100 BYTE)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE TM_TICKETCANCELLATIONTYPE
(
  NXID                      NUMBER(10),
  NAME                      VARCHAR2(100 BYTE),
  TICKETCANCELLATIONTYPEID  NUMBER(10),
  DESCRIPTION               VARCHAR2(200 BYTE)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE TM_FAREMATRIXENTRY
ADD (
  SHORTNAME            VARCHAR2(100 BYTE)
);
ALTER TABLE TM_FAREMATRIXENTRY
ADD (
  PRIORITY             NUMBER(10)
);
ALTER TABLE TM_FAREMATRIXENTRY
ADD (
  LONGNAME             VARCHAR2(100 BYTE)
);


ALTER TABLE TM_FARETABLEENTRY
ADD (
  SHORTCODE         NUMBER(10)
);

ALTER TABLE TM_FARETABLEENTRY
ADD (
  EXTERNALNAME      VARCHAR2(100 BYTE)
);


ALTER TABLE TM_TARIF
ADD (
  NETID          NUMBER(10)
);


ALTER TABLE VARIO_DEPOT
ADD (
  CUSTOMERNO        VARCHAR2(20 BYTE)           DEFAULT '-'
);


ALTER TABLE TM_TICKET
ADD (
  MINFACTOR                 NUMBER(10)
);
ALTER TABLE TM_TICKET
ADD (
  MAXFACTOR                 NUMBER(10)
);
ALTER TABLE TM_TICKET
ADD (
  EXTERNALNAME              VARCHAR2(100 BYTE)
);
ALTER TABLE TM_TICKET
ADD (
  SHORTCODE                 NUMBER(10)
);
ALTER TABLE TM_TICKET
ADD (
  TICKETCANCELLATIONTYPEID  NUMBER(10)
);





drop table VARIO_SERVICEJOURNEYPATTERN;

drop table VARIO_STOPPOINT;

drop table VARIO_TARIFZONE;



CREATE OR REPLACE FUNCTION bitor (p_dec1 NUMBER,p_dec2 NUMBER)
RETURN NUMBER IS
/******************************************************************************
   NAME:       bitor
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        05.02.2009          1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     bitor
      Sysdate:         05.02.2009
      Date and Time:   05.02.2009, 15:34:29, and 05.02.2009 15:34:29
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
BEGIN
   return p_dec1-bitand(p_dec1,p_dec2)+p_dec2;
END ;
/


commit;

PROMPT Fertig!

spool off


