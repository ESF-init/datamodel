
spool db_1_028.log

PROMPT �ndern Tabelle UM_UNIT 

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '21.01.2009', 'DD.MM.YYYY'), '1.028', 'JGi', 'Defaultwert f�r DEPOTID = 0');

-- *************************

ALTER TABLE UM_UNIT
MODIFY(DEPOTID  DEFAULT 0);


-- *************************
commit;

PROMPT Fertig!

spool off