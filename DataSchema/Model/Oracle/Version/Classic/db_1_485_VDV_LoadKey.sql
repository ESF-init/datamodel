SPOOL db_1_485.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.485', 'ULB','VDV_Loadkey.MessageDate');


ALTER TABLE VDV_LOADKEYMESSAGE
ADD (MessageDate DATE);

 commit;