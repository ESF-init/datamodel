SPOOL db_1_208.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
     VALUES (TO_DATE ('20.07.2011', 'DD.MM.YYYY'), '1.208', 'ULS','Added seasonticketid to pp_card');

ALTER TABLE PP_CARD
 ADD (SEASONTICKETID NUMBER(10));

COMMIT;

SPOOL off
