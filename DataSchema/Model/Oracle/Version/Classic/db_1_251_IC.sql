SPOOL db_1_251.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('22.03.2012', 'DD.MM.YYYY'), '1.251', 'MKK','P_DELETE_UM_DEVICE added');

CREATE OR REPLACE PROCEDURE P_DELETE_UM_DEVICE(
    nDEVICENO IN NUMBER,
    nDEVICECLASSID IN NUMBER )
IS
nDeviceId NUMBER :=-1;
BEGIN
    SELECT DEVICEID INTO nDeviceId FROM UM_DEVICE WHERE DEVICENO = nDEVICENO AND DEVICECLASSID = nDEVICECLASSID; 
    DELETE FROM um_loadingstatistics WHERE deviceid=nDeviceId;
    DELETE FROM UM_LOADINGSTATISTICSERROR WHERE deviceid=nDeviceId; 
    DELETE FROM UM_DATATRAFFIC WHERE deviceid=nDeviceId;    
    DELETE FROM UM_DEVICE_MONITOR WHERE deviceid=nDeviceId;    
    DELETE FROM UM_DEVICEHISTORY WHERE deviceid=nDeviceId;    
    DELETE FROM um_device WHERE deviceid=nDeviceId;
    COMMIT;
END P_DELETE_UM_DEVICE
/
  




COMMIT;

PROMPT Done!

SPOOL off