SPOOL db_1_417.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.417', 'MIW', 'Changed PP_CARDACTIONREQUEST field CARDNO from number(10) to number(18)');

ALTER TABLE PP_CARDACTIONREQUEST
	MODIFY (CARDNO NUMBER(18));
 
COMMIT;

PROMPT Fertig!

SPOOL off