SPOOL db_1_241.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('02.12.2012', 'DD.MM.YYYY'), '1.241', 'TSC','Added table DM_DEBTORFINGERPRINT and extended DM_DEBTOR for DOT.');

/* Columns necessary for timestamps and values exported by Mentzdv. */
ALTER TABLE DM_DEBTOR 
ADD 
(
	EXTERNALDEBTORNO		VARCHAR2(40) DEFAULT '',
	PREFERREDLANGUAGE		VARCHAR2(20) DEFAULT '',
	DEPOTLASTLOGON			DATE,
	VEHICLELASTLOGON		DATE
);

/* Unique constraint to support selection over DEBTORNO and CLIENTID and to finally
constrict that a user can create two debtors with the same number. */
ALTER TABLE DM_DEBTOR
ADD 
(
	CONSTRAINT "UC_DEBTORNO_CLIENTID" UNIQUE (DEBTORNO, CLIENTID)
);

/* PINs exported by Mentzdv have eight digits, table supports only six digits */
ALTER TABLE DM_DEFAULTPIN
MODIFY 
(
	DEFAULTPIN NUMBER(10)
);

/* Table to hold the fingerprints for each debtor */
CREATE TABLE DM_DEBTORFINGERPRINT
(
	FINGERPRINTID			NUMBER(10)  NOT NULL,
	DEBTORID				NUMBER(10)  DEFAULT 0       NOT NULL,
	FINGERPRINTTEMPLATE		BLOB        NOT NULL,
	REGISTRATIONTIME		DATE        DEFAULT sysdate NOT NULL
);

ALTER TABLE DM_DEBTORFINGERPRINT
ADD
(
	CONSTRAINT DM_DEBTOR_FP PRIMARY KEY (FINGERPRINTID)
);

ALTER TABLE DM_DEBTORFINGERPRINT
ADD
(
	CONSTRAINT DM_DEBTOR_FP_FK FOREIGN KEY (DEBTORID) REFERENCES DM_DEBTOR(DEBTORID)
);

--ROLLBACK;
COMMIT;

PROMPT Done!

SPOOL off