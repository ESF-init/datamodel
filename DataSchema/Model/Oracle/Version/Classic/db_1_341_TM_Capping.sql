SPOOL db_1_341.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.341', 'FLF','Modiefied capping to use service id');

 ALTER TABLE TM_RULE_CAPPING 		ADD		    SERVICEID      NUMBER(18)                     DEFAULT 0;
 
 ALTER TABLE TM_RULE_CAPPING
 DROP CONSTRAINT FK_TM_RULECAPPINGTOCALENDAR;

 
COMMIT;

PROMPT Fertig!

SPOOL off