SPOOL db_1_203.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
     VALUES (TO_DATE ('07.06.2011', 'DD.MM.YYYY'), '1.203', 'RHO','Added column SHIFTID to RM_BINARYDATA');

ALTER TABLE RM_BINARYDATA
 ADD (SHIFTID  NUMBER                               DEFAULT 0                     NOT NULL);
