SPOOL db_1_505.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.505', 'FLF', 'Added OFFLINECREDIT col to TM_BUSINESSRULERESULT');

ALTER TABLE TM_BUSINESSRULERESULT
 ADD (OFFLINECREDIT  NUMBER(10));

COMMIT;

PROMPT Fertig!

SPOOL off