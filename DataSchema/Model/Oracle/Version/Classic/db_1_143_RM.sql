SPOOL db_1_143.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('16.09.2010', 'DD.MM.YYYY'), '1.143', 'ULB','Weiteres Attribut f�r Vitales Land');


ALTER TABLE RM_EXTERNALDATA
 ADD (extDataValue1  VARCHAR2(50));


COMMENT ON COLUMN RM_EXTERNALDATA.extDataValue1 IS 'F�r Vitaleslandcard = manuellErzeugt';



COMMIT ;

PROMPT Fertig!

SPOOL off



