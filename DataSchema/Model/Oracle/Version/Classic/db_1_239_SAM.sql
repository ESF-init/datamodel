SPOOL db_1_239.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('30.01.2012', 'DD.MM.YYYY'), '1.239', 'KKV','Added DEBITORNO to PP_CONTRACT');


ALTER TABLE PP_CONTRACT ADD (
	DEBITORNO           NUMBER(10));

COMMIT;


PROMPT Fertig!

SPOOL off