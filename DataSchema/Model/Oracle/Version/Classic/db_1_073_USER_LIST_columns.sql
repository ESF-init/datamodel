spool db_1_073.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '29.09.2009', 'DD.MM.YYYY'), '1.073', 'NM', 'Vorname-Spalte in USER_LIST');

--**************************************

ALTER TABLE USER_LIST
ADD (
  USERFIRSTNAME     VARCHAR2(50 BYTE)
);

--*****************************************

COMMIT;

PROMPT Fertig!

spool off