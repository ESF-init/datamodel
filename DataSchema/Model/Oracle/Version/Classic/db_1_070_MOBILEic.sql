spool db_1_070.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '01.09.2009', 'DD.MM.YYYY'), '1.070', 'JGi', 'Neue Prozeduren f�r MOBILEic');

CREATE OR REPLACE PROCEDURE InsertDepotResources
IS
  cname varchar2(100);
  descr varchar2(100);
BEGIN
   FOR r_loop IN (SELECT depotno, clientid FROM VARIO_DEPOT )
   LOOP
      IF( r_loop.depotno >= 0 ) THEN
      SELECT COMPANYNAME INTO cname FROM VARIO_CLIENT WHERE CLIENTID = r_loop.clientid;
   descr := 'Depot mit Nummer ' || r_loop.depotno || ' von Client ' || cname;
   INSERT INTO USER_RESOURCE (RESOURCEID,RESOURCEGROUP,RESOURCETYPE,LANGUAGEID,RESOURCENAME,RESOURCEDESCR) VALUES ('DEPOT_' || r_loop.depotno || '_CLIENT_' || cname ,'Central',0,1, descr, descr);
      COMMIT;
   END IF;
   END LOOP;
END InsertDepotResources;
/

CREATE OR REPLACE PROCEDURE p_CLEARARCHIVEJOBS
(
 nDAYS  IN NUMBER,
 nTYPEOFJOBID   IN NUMBER
)
IS
BEGIN
  delete from UM_LSERRORTOTASK where ARCHIVEDISTRIBUTIONTASKID IN (select ARCHIVEDISTRIBUTIONTASKID from UM_ARCHIVEDISTRIBUTIONTASK where archivejobid IN (select archivejobid from UM_ARCHIVEJOB where starttime < sysdate - nDAYS and typeofarchivejobid = nTYPEOFJOBID));

  delete from UM_UNITDISTRIBUTIONTASK where ARCHIVEDISTRIBUTIONTASKID IN (select ARCHIVEDISTRIBUTIONTASKID from UM_ARCHIVEDISTRIBUTIONTASK where archivejobid IN (select archivejobid from UM_ARCHIVEJOB where starttime < sysdate - nDAYS and typeofarchivejobid = nTYPEOFJOBID));

  delete from UM_ARCHIVEDISTRIBUTIONTASK where archivejobid IN (select archivejobid from UM_ARCHIVEJOB where starttime < sysdate - nDAYS and typeofarchivejobid = nTYPEOFJOBID);

  delete from UM_ARCHIVEJOBDISTRIBUTEDTOUNIT where archivejobid IN (select archivejobid from UM_ARCHIVEJOB where starttime < sysdate - nDAYS and typeofarchivejobid = nTYPEOFJOBID);

  if nTYPEOFJOBID <> 2 then
     delete from UM_ARCHIVEJOB where starttime < sysdate - nDAYS  and typeofarchivejobid = nTYPEOFJOBID;
  end if;
  commit;

END p_CLEARARCHIVEJOBS;
/

CREATE OR REPLACE PROCEDURE p_dmservicethreadisalive
(
sTHREADNAME IN VARCHAR
)
IS
nCount NUMBER :=0;
BEGIN
 
  select count (*) into nCount from UM_DMSERVICE where THREADNAME = sTHREADNAME;
 IF nCount=0 THEN
  insert into UM_DMSERVICE (DMSERVICEID,THREADNAME,LASTALIVE)
  VALUES (GetDataID(),sTHREADNAME,sysdate);
 ELSE
  update UM_DMSERVICE set LASTALIVE = sysdate WHERE THREADNAME = sTHREADNAME;
END IF;

END p_dmservicethreadisalive;
/

CREATE OR REPLACE PROCEDURE TerminateArchiveJob(  FNAME IN VARCHAR )
IS
BEGIN
   UPDATE UM_ARCHIVEJOB set completed = 1, reminderinfo = null where archivejobid in (select archivejobid from UM_ARCHIVEJOB where archiveid in (select archiveid from UM_ARCHIVE where filename = FNAME));
END;
/

COMMIT;

PROMPT Fertig!

spool off