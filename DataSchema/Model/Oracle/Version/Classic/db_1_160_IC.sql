spool db_1_160.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '02.12.2010', 'DD.MM.YYYY'), '1.160', 'MTW', 'Add column visibleingui to um_typeofarchive');

ALTER TABLE UM_TYPEOFARCHIVE
ADD (VISIBLEINGUI NUMBER(1) DEFAULT 0 NOT NULL);

UPDATE um_typeofarchive
SET visibleingui            = 1
WHERE UPPER(typeshortname) IN ('Y', 'X','P','I','G','S','T','V','Z','C');

COMMIT;

PROMPT Fertig!

spool off
