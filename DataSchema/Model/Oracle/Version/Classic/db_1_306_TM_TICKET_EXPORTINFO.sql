SPOOL db_1_306.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('24.01.2013', 'DD.MM.YYYY'), '1.306', 'FLF','TM Ticket To Exportinfo');

CREATE TABLE TM_TICKET_EXPORTINFO
(
  EXPORTINFOID  NUMBER(18)                      NOT NULL,
  TICKETID      NUMBER(18)                      NOT NULL
);


ALTER TABLE TM_TICKET_EXPORTINFO ADD (
  CONSTRAINT TM_TICKET_EXPORTINFO_PK
 PRIMARY KEY
 (EXPORTINFOID, TICKETID));


COMMIT;

PROMPT Fertig!

SPOOL off