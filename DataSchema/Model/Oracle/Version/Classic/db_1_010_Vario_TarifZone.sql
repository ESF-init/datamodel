
spool db_1_010.log

PROMPT Erstellen Tabelle VARIO_TARIFZONE 

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '01.12.2008', 'DD.MM.YYYY'), '1.010', 'BTS', 'vario Tarifzonen Informationen');

-- *************************



CREATE TABLE VARIO_TARIFZONE
(
  TARIFZONENUMBER  NUMBER,
  TARIFZONECODE    VARCHAR2(200 BYTE),
  CLIENTID         NUMBER,
  VALIDFROM        DATE,
  VALIDUNTIL       DATE,
  TARIFZONEID      NUMBER
)
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


CREATE UNIQUE INDEX PK_VARIO_TARIFZONE ON VARIO_TARIFZONE
(TARIFZONEID)
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


ALTER TABLE VARIO_TARIFZONE ADD (
  CONSTRAINT PK_VARIO_TARIFZONE
 PRIMARY KEY
 (TARIFZONEID)
    USING INDEX 
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));


ALTER TABLE VARIO_TARIFZONE ADD (
  CONSTRAINT FK_TARIFZONE_VARIO_CLIENT 
 FOREIGN KEY (CLIENTID) 
 REFERENCES VARIO_CLIENT (CLIENTID));









-- *************************
commit;

PROMPT Fertig!

spool off