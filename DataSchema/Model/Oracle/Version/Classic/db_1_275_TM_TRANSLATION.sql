SPOOL db_1_275.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('15.08.2012', 'DD.MM.YYYY'), '1.275', 'FLF','TM_TRANSLATION created.');

CREATE TABLE TM_TRANSLATION
(
  TRANSLATIONID  NUMBER(10)                     NOT NULL,
  LANGUAGEID     NUMBER(10)                     NOT NULL,
  TARIFFID       NUMBER(10)                     NOT NULL,
  BOBJECTID      NUMBER(10)                     NOT NULL,
  CLASSLABEL     VARCHAR2(200 BYTE)             NOT NULL,
  PROPERTYLABEL  VARCHAR2(200 BYTE)             NOT NULL,
  TEXT           VARCHAR2(2000 BYTE)            NOT NULL
);

ALTER TABLE TM_TRANSLATION ADD (
  CONSTRAINT TM_TRANSLATION_PK
 PRIMARY KEY
 (TRANSLATIONID));

ALTER TABLE TM_TRANSLATION ADD (
  CONSTRAINT TM_TRANSLATION_R01 
 FOREIGN KEY (LANGUAGEID) 
 REFERENCES TM_LANGUAGE (LANGUAGEID),
  CONSTRAINT TM_TRANSLATION_R02 
 FOREIGN KEY (TARIFFID) 
 REFERENCES TM_TARIF (TARIFID));


COMMIT;


PROMPT Fertig!

SPOOL off