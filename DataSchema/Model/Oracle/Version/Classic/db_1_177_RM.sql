SPOOL db_1_177.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('18.02.2011', 'DD.MM.YYYY'), '1.177', 'ULB','RM_Shift neue Attribute');

ALTER TABLE RM_SHIFT
 ADD (logOffDebtor  NUMBER(10));

ALTER TABLE RM_SHIFT
 ADD (cancellationCount  NUMBER(10));

ALTER TABLE RM_SHIFT
 ADD (CancellationAmount  NUMBER(10));

COMMIT ;

PROMPT Fertig!

SPOOL off
