SPOOL db_1_144.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('21.09.2010', 'DD.MM.YYYY'), '1.144', 'FLF','New field in TM_CALENDAR');



ALTER TABLE tm_calendar
 ADD (
    NUMBER_  NUMBER(10));



COMMIT ;

PROMPT Fertig!

SPOOL off
