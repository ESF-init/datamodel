SPOOL db_1_312.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.312', 'FLF','Vario Device Class');

 ALTER TABLE VARIO_DEVICECLASS ADD  TMVISIBILITY  NUMBER(1)                DEFAULT 0;
 ALTER TABLE VARIO_DEVICECLASS ADD  CREATETARIFFARCHIVE  NUMBER(1)                DEFAULT 0;
 
COMMIT;

PROMPT Fertig!

SPOOL off