
spool db_1_011.log

PROMPT Erstellen Tabelle VARIO_LINE 

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '01.12.2008', 'DD.MM.YYYY'), '1.011', 'BTS', 'vario Linien Informationen');

-- *************************


CREATE TABLE VARIO_LINE
(
  LINECODE    VARCHAR2(200 BYTE)                NOT NULL,
  LINENUMBER  NUMBER(10)                            NOT NULL,
  LINENAME    VARCHAR2(200 BYTE),
  CLIENTID    NUMBER(10),
  VALIDFROM   DATE                              NOT NULL,
  VALIDUNTIL  DATE                              NOT NULL,
  LINEID      NUMBER(10)
)
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


CREATE UNIQUE INDEX PK_VARIO_LINE ON VARIO_LINE
(LINEID)
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


ALTER TABLE VARIO_LINE ADD (
  CONSTRAINT PK_VARIO_LINE
 PRIMARY KEY
 (LINEID)
    USING INDEX 
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));


ALTER TABLE VARIO_LINE ADD (
  CONSTRAINT FK_VARIO_LINE_VARIO_FILE 
 FOREIGN KEY (CLIENTID) 
 REFERENCES VARIO_CLIENT (CLIENTID));



-- *************************
commit;

PROMPT Fertig!

spool off