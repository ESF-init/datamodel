SPOOL db_1_248.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('07.03.2012', 'DD.MM.YYYY'), '1.248', 'KKV','Added columns to SAM master data');

alter table vario_district add (
    STATEID   NUMBER(10));

alter table vario_community add (
    GOVERNMENTDISTRICTID   NUMBER(10));

alter table vario_community add (
    STATEID   NUMBER(10));

COMMIT;

PROMPT Done!

SPOOL off