spool db_1_085.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '03.11.2009', 'DD.MM.YYYY'), '1.085', 'FLF', 'Neue Tabelle TM_CHOICE');

--**************************************

CREATE TABLE TM_CHOICE
(
  CHOICEID             NUMBER(10),
  TARIFID              NUMBER(10),
  TICKETID             NUMBER(10),
  VALUESTRING          VARCHAR2(100 BYTE),
  TEXTSTRING           VARCHAR2(200 BYTE),
  DISTANCEATTRIBUTEID  NUMBER(10),
  ROUTENAMEID          NUMBER(10)
);


ALTER TABLE TM_CHOICE ADD (
  PRIMARY KEY
 (CHOICEID)
    USING INDEX);

--*****************************************

COMMIT;

PROMPT Fertig!

spool off





