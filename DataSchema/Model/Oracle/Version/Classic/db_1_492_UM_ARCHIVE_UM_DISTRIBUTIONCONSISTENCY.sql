SPOOL db_1_492.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.492', 'MRP', 'Columns added to UM_ARCHIVE, UM_DISTRIBUTIONCONSISTENCY');


alter table um_archive add ("CLIENTID" NUMBER(10,0) default 0);

alter table um_archive add ("ISTESTARCHIVE" NUMBER (1,0) default 0);

alter table UM_DISTRIBUTIONCONSISTENCY add ("DEVICECLASSID" NUMBER (10,0) default 0 not null);

COMMIT;

PROMPT Fertig!

SPOOL off