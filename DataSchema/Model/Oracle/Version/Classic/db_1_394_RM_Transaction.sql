SPOOL db_1_394.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.394', 'ULB','Added columns to table RM_TRANSACTION');

ALTER TABLE RM_TRANSACTION
ADD (Remainingrides NUMBER(5));

ALTER TABLE RM_TRANSACTION
ADD (ticketInstanceId NUMBER(10));



 
COMMIT;

PROMPT Fertig!

SPOOL off