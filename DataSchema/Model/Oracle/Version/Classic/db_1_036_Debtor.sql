
spool db_1_036.log

PROMPT neues Attribut Debtor.BALANCINGDEBITOR

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '03.03.2009', 'DD.MM.YYYY'), '1.036', 'ULB', 'neues Attribut Debtor.BALANCINGDEBITOR');

-- *************************
ALTER TABLE DM_DEBTOR ADD ( BALANCINGDEBITOR  NUMBER(10) DEFAULT 0);

-- *************************
commit;

PROMPT Fertig!

spool off


