SPOOL db_1_348.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.348', 'FLF','Special Receipt');

 ALTER TABLE TM_SPECIALRECEIPT 		ADD		  EVENDFTNUMBER  NUMBER(10);
 
COMMIT;

PROMPT Fertig!

SPOOL off