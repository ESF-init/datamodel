SPOOL db_1_399.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.399', 'JUN','Removed unique constraint for column fileextension from table um_distributionconsistency');

 
ALTER TABLE UM_DISTRIBUTIONCONSISTENCY
 DROP UNIQUE(fileextension); 
 

COMMIT;

PROMPT Fertig!

SPOOL off