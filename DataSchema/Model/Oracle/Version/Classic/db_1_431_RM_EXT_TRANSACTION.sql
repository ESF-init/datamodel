SPOOL db_1_431.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.431', 'ULB','Added TicketReference to RM_EXT_TRANSACTION');


ALTER TABLE RM_EXT_TRANSACTION
 ADD (ticketreference  VARCHAR2(100));



COMMIT;

PROMPT Fertig!

SPOOL off