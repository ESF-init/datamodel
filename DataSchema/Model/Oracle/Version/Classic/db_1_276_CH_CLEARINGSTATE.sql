SPOOL db_1_276.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('15.08.2012', 'DD.MM.YYYY'), '1.276', 'MIW','CH_CLEARINGSTATE created.');

-- this table was missing in the master script but might be present in several databases

declare cnt number;

begin
    select count(*) into cnt from user_tables
    where table_name = 'CH_CLEARINGSTATE';
    
    if cnt <= 0 THEN
        begin
            EXECUTE IMMEDIATE 'CREATE TABLE CH_CLEARINGSTATE (CLEARINGSTATEID NUMBER(10) NOT NULL, CLEARINGSTATENAME  VARCHAR2(50 BYTE)  NOT NULL )';
            EXECUTE IMMEDIATE 'Insert into CH_CLEARINGSTATE (CLEARINGSTATEID, CLEARINGSTATENAME) Values (0, ''Started'')';
            EXECUTE IMMEDIATE 'Insert into CH_CLEARINGSTATE (CLEARINGSTATEID, CLEARINGSTATENAME) Values (1, ''Created'')';
            EXECUTE IMMEDIATE 'Insert into CH_CLEARINGSTATE (CLEARINGSTATEID, CLEARINGSTATENAME) Values (2, ''Commited'')';
            COMMIT;
        end;
    end if;
end;

COMMIT;

PROMPT Fertig!

SPOOL off