SPOOL db_1_210.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
     VALUES (TO_DATE ('22.07.2011', 'DD.MM.YYYY'), '1.210', 'MTW','Bugfix in p_cleararchivejobs2');


create or replace
PROCEDURE p_cleararchivejobs2(
    ndays        IN NUMBER,
    ntypeofjobid IN NUMBER )
IS
  counter number;
  
  CURSOR c (ndays IN NUMBER, ntypeofjobid IN NUMBER)
  IS
    (SELECT archivedistributiontaskid
    FROM um_archivedistributiontask
    WHERE archivejobid IN
      (SELECT archivejobid
      FROM um_archivejob
      WHERE starttime        < SYSDATE - ndays
      AND typeofarchivejobid = ntypeofjobid
      )
    );
    
    CURSOR x (ndays IN NUMBER, ntypeofjobid IN NUMBER)
    IS
      (SELECT archivejobid
      FROM um_archivejob
      WHERE starttime        < SYSDATE - ndays
      AND typeofarchivejobid = ntypeofjobid
      );

  BEGIN
  
  counter := 0;
    
    FOR r IN c (ndays, ntypeofjobid)
    LOOP
      DELETE
      FROM um_lserrortotask
      WHERE archivedistributiontaskid = r.archivedistributiontaskid;
      DELETE
      FROM um_unitdistributiontask
      WHERE archivedistributiontaskid = r.archivedistributiontaskid;

      if counter = 2500 then
      COMMIT;
      counter := 0;
      else
      counter := counter+1;
      end if;
    END LOOP;
    
    commit;
    counter:=0;
  
    FOR r IN x (ndays, ntypeofjobid)
    LOOP
      DELETE FROM um_archivedistributiontask WHERE archivejobid = r.archivejobid;

      DELETE
      FROM um_archivejobdistributedtounit
      WHERE archivejobid = r.archivejobid;

      DELETE FROM proccessedfiles WHERE archivejobid=r.archivejobid;

      IF ntypeofjobid <> 2 THEN
        DELETE FROM um_archivejob WHERE archivejobid = r.archivejobid;
      END IF;

      if counter = 25 then
        COMMIT;
        counter := 0;
      else
        counter := counter+1;
      end if;

    END LOOP;

  commit;

  END p_cleararchivejobs2; 
  /


PROMPT Fertig!

SPOOL off
