SPOOL db_1_186.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('25.03.2011', 'DD.MM.YYYY'), '1.186', 'MKK','UM_ARCHIVE EXTERNALVERSION added');

ALTER TABLE UM_ARCHIVE
ADD (EXTERNALVERSION VARCHAR2(20 BYTE));


PROMPT Fertig!

SPOOL off
