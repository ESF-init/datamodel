SPOOL db_1_381.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.381', 'ULB','Added column courseno to RM_SHIFT');

ALTER TABLE RM_SHIFT
ADD (courseno NUMBER(10));
prompt Pruefung ob hierdurch view_rm_transactions ungueltig geworden ist
prompt Falls ja -> Attribut courseno in view_rm_transactions voll qualifizieren
prompt also "courseno" mit "rm_transaction.courseno" ersetzen
select * from view_rm_transactions where 0=1;
COMMIT;

PROMPT Fertig!

SPOOL off