SPOOL db_1_240.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('30.01.2012', 'DD.MM.YYYY'), '1.240', 'HSS','Extend size of DESCRIPTION in TM_TEMPORALTYPE');


ALTER TABLE TM_TEMPORALTYPE MODIFY DESCRIPTION VARCHAR2(300);
COMMIT;


PROMPT Fertig!

SPOOL off