SPOOL db_1_493.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.493', 'FLF', 'Added new table TM_TICKET_PROPERTY');

CREATE TABLE TM_TICKET_PROPERTY
(
  PROPERTYID         NUMBER(10)                   NOT NULL,
  PROPERTYNAME       VARCHAR2(250 BYTE)           NOT NULL,
  NXNAME  VARCHAR2(300)                          NOT NULL
);


ALTER TABLE TM_TICKET_PROPERTY ADD (
  CONSTRAINT TM_TICKET_PROPERTY_PK
  PRIMARY KEY
  (PROPERTYID));
  
  
ALTER TABLE TM_TICKET
 ADD (TICKETPROPERTYID  NUMBER(10));
 
 CREATE INDEX IDX_RM_TICKET ON RM_TRANSACTION
(TARIFFID, TIKNO)
tablespace &ts_index;


COMMIT;

PROMPT Fertig!

SPOOL off