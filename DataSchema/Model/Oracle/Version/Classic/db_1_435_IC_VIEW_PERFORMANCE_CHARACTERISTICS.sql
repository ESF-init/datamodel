SPOOL db_1_435.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.435', 'MKK','Added View VIEW_UM_PERFCHAR');

DROP VIEW VIEW_UM_PERFCHAR;

CREATE OR REPLACE FORCE VIEW VIEW_UM_PERFCHAR (messageindex,
                                                        vehicleno,
                                                        protmessagetext,
                                                        protmessagedate,
                                                        clientid
                                                       )
AS
   SELECT m.protmessageid AS messageindex,
          REGEXP_SUBSTR (p.protusername, '[^V-]+', 1, 2) AS vehicleno,
          m.protmessagetext, m.protmessagedate, p.clientid
     FROM protocol p, prot_message m, prot_function f
    WHERE f.fctgroupid = 2
      AND f.fctid = p.fctid
      AND p.protid = m.protid
      AND m.protactionid = 22301;



PROMPT Fertig!

SPOOL off