spool db_1_075.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '30.09.2009', 'DD.MM.YYYY'), '1.075', 'FLF', 'Spalte ROUTEID zu TM_FAREMATRIXENTRY hinzugefügt');

--**************************************

ALTER TABLE TM_FAREMATRIXENTRY ADD (ROUTENAMEID  NUMBER(10));


--*****************************************

COMMIT;

PROMPT Fertig!

spool off