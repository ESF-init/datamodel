SPOOL db_1_382.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
	VALUES (sysdate, '1.382', 'MIW','Changed RM_TransactionType TypeID from Number(3) to Number(10)');

ALTER TABLE RM_TRANSACTIONTYPE
	MODIFY(TYPEID NUMBER(10));

COMMIT;

PROMPT Fertig!

SPOOL off