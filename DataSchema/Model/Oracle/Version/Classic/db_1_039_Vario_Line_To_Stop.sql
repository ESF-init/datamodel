
spool db_1_039.log

PROMPT Neue Tabelle VARIO_LINE_TO_STOP

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '10.03.2009', 'DD.MM.YYYY'), '1.039', 'MSC', 'Neue Tabelle VARIO_LINE_TO_STOP');

-- *************************

CREATE TABLE VARIO_LINE_TO_STOP
(
  STOPID      NUMBER(10)                     NOT NULL,
  LINEID      NUMBER(10)                     NOT NULL,
  NETID       NUMBER(10)                     NOT NULL,
  SEQUENCENO  NUMBER(10)                     DEFAULT 0
)
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


ALTER TABLE VARIO_LINE_TO_STOP ADD (
  FOREIGN KEY (STOPID) 
 REFERENCES VARIO_STOP (STOPID));

ALTER TABLE VARIO_LINE_TO_STOP ADD (
  FOREIGN KEY (LINEID) 
 REFERENCES VARIO_LINE (LINEID));

ALTER TABLE VARIO_LINE_TO_STOP ADD (
  FOREIGN KEY (NETID) 
 REFERENCES VARIO_NET (NETID));


-- *************************
commit;

PROMPT Fertig!

spool off


