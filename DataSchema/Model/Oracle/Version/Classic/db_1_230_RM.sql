SPOOL db_1_230.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('16.11.2011', 'DD.MM.YYYY'), '1.230', 'ULB','added deduction to RM_TRANSACTION, several indices for view_rm_transaction! ');

	 ALTER TABLE RM_TRANSACTION  ADD (deduction  NUMBER(10));


CREATE INDEX idx_debtorno ON DM_DEBTOR
(DEBTORNO)
NOLOGGING
TABLESPACE VARIO_IDX
NOPARALLEL;


CREATE INDEX idx_ticketinternalno ON tm_ticket
(INTERNALNUMBER)
NOLOGGING
TABLESPACE VARIO_IDX
NOPARALLEL;



CREATE INDEX idx_devicebookingno ON rm_devicebookingstate
(devicebookingno)
NOLOGGING
TABLESPACE VARIO_IDX
NOPARALLEL;

COMMIT ;

PROMPT Fertig!

SPOOL off
