SPOOL db_1_320.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.320', 'FLF','Added RULEPERIODID to TM_TICKET_DEVICECLASS');

 ALTER TABLE TM_TICKET_DEVICECLASS ADD   RULEPERIODID                   NUMBER(18);

COMMIT;

PROMPT Fertig!

SPOOL off