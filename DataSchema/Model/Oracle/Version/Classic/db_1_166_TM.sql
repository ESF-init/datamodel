SPOOL db_1_166.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('17.12.2010', 'DD.MM.YYYY'), '1.166', 'FLF','TM FareMatrixEntry extended');


	 
ALTER  TABLE TM_FAREMATRIXENTRY
 ADD ( USEDAYSATTRIBUTEID     NUMBER(10));

 
CREATE OR REPLACE PROCEDURE insert_farematrixentry (
   farematrixentry       IN   NUMBER,
   boarding              IN   NUMBER,
   destination           IN   NUMBER,
   fare_                 IN   NUMBER,
   farematrix            IN   NUMBER,
   distance_             IN   NUMBER,                               /*(10,3)*/
   via                   IN   NUMBER,
   realdistance_         IN   NUMBER,                               /*(10,3)*/
   averagedistance_      IN   NUMBER,                               /*(10,3)*/
   tariffattribute       IN   NUMBER,
   distanceattribute     IN   NUMBER,
   description_          IN   VARCHAR,
   priority_             IN   NUMBER,
   shortname_            IN   VARCHAR,
   longname_             IN   VARCHAR,
   linefilterattribute   IN   NUMBER,
   serviceallocation     IN   NUMBER,
   routename             IN   NUMBER,
   directionattribute    IN   NUMBER,
   usedaysattribute      IN   NUMBER default null
)
IS
BEGIN
   INSERT INTO tm_farematrixentry
               (farematrixentryid, boardingid, destinationid, fare,
                farematrixid, distance, viaid, realdistance,
                averagedistance, tariffattributeid, distanceattributeid,
                description, priority, shortname, longname,
                linefilterattributeid, serviceallocationid, routenameid,
                directionattributeid, usedaysattributeid
               )
        VALUES (farematrixentry, boarding, destination, fare_,
                farematrix, distance_, via, realdistance_,
                averagedistance_, tariffattribute, distanceattribute,
                description_, priority_, shortname_, longname_,
                linefilterattribute, serviceallocation, routename,
                directionattribute, usedaysattribute
               );
END insert_farematrixentry;
/


COMMIT ;

PROMPT Fertig!

SPOOL off
