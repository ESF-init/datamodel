SPOOL db_1_150.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('18.10.2010', 'DD.MM.YYYY'), '1.150', 'MKK','New Version of PROCEDURE UPDATEJOBSTATE_DDM. Error in WHERE Clause of P_UPDATEDISTRIBUTIONSUCCESS removed');

CREATE OR REPLACE PROCEDURE UPDATEJOBSTATE_DDM 
(
    inClientid      IN       NUMBER,
    inFrom IN DATE
) 
IS
    tmpVar NUMBER;
/******************************************************************************
   NAME:       UPDATEJOBSTATE_DDM
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        18.10.2010          1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     UPDATEJOBSTATE_DDM
      Sysdate:         18.10.2010
      Date and Time:   18.10.2010, 08:21:45, and 18.10.2010 08:21:45
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
BEGIN
   FOR rec IN (SELECT archivejobid, typeofarchivejobid
                 FROM um_archivejob
                WHERE (jobstate <> 2 OR jobstate IS NULL) AND 
                CLIENTID = inClientid AND
                starttime > inFrom)
   LOOP
      IF rec.typeofarchivejobid = 4
      THEN
         p_getresetdistributionstate (rec.archivejobid, tmpvar);
      ELSE
         p_getarchivedistributionstate (rec.archivejobid, tmpvar);
      END IF;

      UPDATE um_archivejob
         SET jobstate = tmpvar
       WHERE archivejobid = rec.archivejobid;
   END LOOP;
END UPDATEJOBSTATE_DDM;
/

CREATE OR REPLACE PROCEDURE P_UPDATEDISTRIBUTIONSUCCESS
(
    nDISTRIBUTIONTASKID IN NUMBER,
    nARCHIVEID IN number
)
IS
nIsAssignedToTask NUMBER;
nUnitId NUMBER;
/******************************************************************************
   nDISTRIBUTIONTASKIDME:       P_UPDATEDISTRIBUTIONSUCCESS
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        30.01.2008          1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object nDISTRIBUTIONTASKIDme:     P_UPDATEDISTRIBUTIONSUCCESS
      Sysdate:         30.01.2008
      Date and Time:   30.01.2008, 14:47:52, and 30.01.2008 14:47:52
      UsernDISTRIBUTIONTASKIDme:         (set in TOAD Options, Procedure Editor)
      Table nDISTRIBUTIONTASKIDme:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
BEGIN
  FOR rec IN 
  (
    SELECT     UM_LOADINGSTATISTICS.LOADINGSTATISTICSID, UM_LOADINGSTATISTICS.DEVICEID, UM_LOADINGSTATISTICS.LSSTATE
    FROM         UM_LOADINGSTATISTICS INNER JOIN
                      UM_DEVICE ON UM_LOADINGSTATISTICS.DEVICEID = UM_DEVICE.DEVICEID
    WHERE     (UM_LOADINGSTATISTICS.ARCHIVEID = nARCHIVEID) AND (UM_DEVICE.DEVICECLASSID <> - 1)
  )
  LOOP
    select count(*) into nIsAssignedTotask 
    from UM_LSTOTASK
    where loadingstatisticsid = rec.loadingstatisticsid
    and ARCHIVEDISTRIBUTIONTASKID = nDISTRIBUTIONTASKID;
    
    
    IF nIsAssignedTotask=0 THEN
      INSERT INTO UM_LSTOTASK
              (LSTOTASKID, ARCHIVEDISTRIBUTIONTASKID, LOADINGSTATISTICSID)
       VALUES (Getdataid(),nDISTRIBUTIONTASKID,rec.LOADINGSTATISTICSID);
    END IF;
    
    IF rec.LSSTATE=1 THEN
       SELECT     UM_UNIT.UNITID into nUnitId
       FROM       UM_UNIT INNER JOIN
                      UM_DEVICE ON UM_UNIT.UNITID = UM_DEVICE.UNITID
       WHERE (UM_DEVICE.DEVICEID = rec.DEVICEID);
       
       UPDATE    UM_UNITDISTRIBUTIONTASK
       SET              COMPLETED = - 1
       WHERE (UM_UNITDISTRIBUTIONTASK.ARCHIVEDISTRIBUTIONTASKID = nDISTRIBUTIONTASKID) 
       AND   (UM_UNITDISTRIBUTIONTASK.UNITID = nUnitId);
    END IF;
  END LOOP;
END P_UPDATEDISTRIBUTIONSUCCESS;
/
	
COMMIT ;

PROMPT Fertig!

SPOOL off
