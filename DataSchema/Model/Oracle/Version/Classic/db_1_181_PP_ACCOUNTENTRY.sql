SPOOL db_1_181.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('14.03.2011', 'DD.MM.YYYY'), '1.181', 'CoS','Export state expanded');

ALTER TABLE PP_ACCOUNTENTRY
MODIFY(EXPORTSTATE NUMBER(10));

PROMPT Fertig!

SPOOL off
