SPOOL db_1_408.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.408', 'FLF','Make combination of attributeid and number_ unique');

prompt Wenn dieses Scipt fehlschlaegt, bitte umgehend #MBAC-FLF (alternativ #MBAC-ULB) informieren

ALTER TABLE TM_ATTRIBUTEVALUE
 ADD CONSTRAINT TM_ATTRIBUTEVALUE_U01
 UNIQUE (ATTRIBUTEID, NUMBER_);

 
 --select * from TM_ATTRIBUTEVALUE where (ATTRIBUTEID, NUMBER_) in 
 -- (select ATTRIBUTEID, NUMBER_  from TM_ATTRIBUTEVALUE
 -- group by  ATTRIBUTEID, NUMBER_
 -- having count(*) >1);

 
COMMIT;

PROMPT Fertig!

SPOOL off