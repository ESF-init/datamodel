spool db_1_081.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '16.10.2009', 'DD.MM.YYYY'), '1.081', 'RHO', 'Neue Spalte in TM_LOGO');

--**************************************

ALTER TABLE tm_logo 
ADD DESCRIPTION  VARCHAR2(200);

--*****************************************

COMMIT;

PROMPT Fertig!

spool off