
spool db_1_017.log

PROMPT Erstellen Tabelle VARIO_Address

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '10.12.2008', 'DD.MM.YYYY'), '1.017', 'MSC', 'VARIO_Address Tabelle erstellen');

-- *************************


CREATE TABLE VARIO_ADDRESS
(
  ADDRESSID   NUMBER(10)                     NOT NULL,
  STREET      VARCHAR2(100 BYTE),
  HOUSENO     VARCHAR2(20 CHAR),
  COUNTRY     VARCHAR2(100 BYTE),
  POSTALCODE  VARCHAR2(20 CHAR),
  CITY        VARCHAR2(100 BYTE)
)
STORAGE    (
            INITIAL          128K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


ALTER TABLE VARIO_ADDRESS ADD (
  PRIMARY KEY
 (ADDRESSID)
    USING INDEX 
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));


-- *************************
commit;

PROMPT Fertig!

spool off