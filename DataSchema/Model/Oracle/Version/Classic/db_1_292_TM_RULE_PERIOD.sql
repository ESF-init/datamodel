SPOOL db_1_292.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('11.10.2012', 'DD.MM.YYYY'), '1.292', 'FLF','TM_RULE_PERIOD created.');


ALTER TABLE TM_TICKET ADD RULEPERIODID	NUMBER(18);

CREATE TABLE TM_RULE_PERIOD
(
  RULEPERIODID   NUMBER(18)                    NOT NULL,
  PERIODSTART     DATE                          NOT NULL,
  PERIODUNIT      NUMBER(18)                    NOT NULL,
  PERIODVALUE     NUMBER(18)                    NOT NULL,
  VALIDTIMEUNIT   NUMBER(18)                    NOT NULL,
  VALIDTIMEVALUE  NUMBER(18)                    NOT NULL,
  EXTTIMEUNIT     NUMBER(18)                    NOT NULL,
  EXTTIMEVALUE    NUMBER(18)                    NOT NULL,
  SELLABLEFROM    DATE                          NOT NULL,
  DEFAULTFROM     DATE                          NOT NULL,
  TARIFFID        NUMBER(18)                    NOT NULL,
  NAME            VARCHAR2(400 BYTE)            NOT NULL
);




ALTER TABLE TM_RULE_PERIOD ADD (
  CONSTRAINT TM_RULE_CAPPING_NU_PK
 PRIMARY KEY
 (RULEPERIODID));

COMMIT;


PROMPT Fertig!

SPOOL off