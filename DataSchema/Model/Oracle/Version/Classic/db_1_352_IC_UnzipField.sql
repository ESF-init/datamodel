SPOOL db_1_352.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.352', 'MTW','UNZIPENABLED added to vario_deviceclass');

alter table vario_deviceclass add UNZIPENABLED NUMBER(1, 0) DEFAULT 0 NOT NULL;

COMMIT;

PROMPT Fertig!

SPOOL off