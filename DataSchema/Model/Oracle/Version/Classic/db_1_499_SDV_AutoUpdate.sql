SPOOL db_1_499.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.499', 'SLR', 'New attribute AutoUpdate in table VARIO_NET');

ALTER TABLE VARIO_NET
 ADD (AUTOUPDATE NUMBER(1) DEFAULT 0);

COMMIT;

PROMPT Fertig!

SPOOL off