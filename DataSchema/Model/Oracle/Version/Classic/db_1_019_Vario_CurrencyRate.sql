
spool db_1_019.log

PROMPT �ndern Tabelle VARIO_CurrencyRate

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '10.12.2008', 'DD.MM.YYYY'), '1.019', 'MSC', 'VARIO_CurrencyRate Default-Value f�r CurrencyID');

-- *************************

UPDATE Vario_CurrencyRate SET CurrencyID = 0 WHERE CurrencyID is null;

ALTER TABLE VARIO_CURRENCYRATE
MODIFY(CURRENCYID  DEFAULT 0);

-- *************************
commit;

PROMPT Fertig!

spool off