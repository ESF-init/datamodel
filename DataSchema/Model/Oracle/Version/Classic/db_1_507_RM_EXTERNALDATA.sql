SPOOL db_1_507.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.507', 'UB', 'New Attributes RM_EXTERNALDATA');



ALTER TABLE RM_EXTERNALDATA ADD 
 (
   EXTMEDIATYPE        NUMBER (10),
   EXTDATACHECKRESULT  NUMBER (10),
   EXTDATASTATE        NUMBER (10),
   PROXERROR           NUMBER (10)   
 );
 /


COMMIT;

PROMPT Fertig!

SPOOL off