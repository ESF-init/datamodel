spool db_1_103.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '15.02.2010', 'DD.MM.YYYY'), '1.103', 'ULB', 'Several changes in RM_transaction');

 alter table rm_transaction
ADD (direction  NUMBER(3));

alter table rm_transaction
ADD (cappingreason  NUMBER(10));

alter table rm_transaction
ADD (maxdistance  NUMBER(10));

alter table rm_transaction
ADD (validfrom  date);

alter table rm_transaction
ADD (validuntil  date);

ALTER TABLE RM_BINARYDATA
 ADD (clientid  NUMBER(10));

ALTER TABLE RM_BINARYDATA
 ADD (hashvalue  VARCHAR2(32));

ALTER TABLE RM_BINARYDATA
 modify (exportstate  NUMBER(10)); 
 
 CREATE TABLE RM_ExternalData
(
  extdataid       NUMBER(10),
  extbrandno      NUMBER(10),
  extpackeno      NUMBER(10),
  extEffortNo     NUMBER(10),
  extCommunityNo  NUMBER(10),
  extCardType  	  NUMBER(10),
  transactionid   NUMBER(10)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;


ALTER TABLE RM_ExternalData ADD (
  CONSTRAINT PK_RM_ExternalData
 PRIMARY KEY
 (extdataid));

 