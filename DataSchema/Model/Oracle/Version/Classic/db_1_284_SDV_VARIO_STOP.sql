SPOOL db_1_284.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('31.08.2012', 'DD.MM.YYYY'), '1.284', 'BTS','Added missing attribute to VARIO_STOP');

ALTER TABLE VARIO_STOP
ADD (STOPNOEXTERN NUMBER(10));

COMMIT;

PROMPT Fertig! 

SPOOL off