
spool db_1_032.log

PROMPT Tabelle f�r Wichtungsfaktor f�r VASA_CR

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '04.02.2009', 'DD.MM.YYYY'), '1.032', 'HSS', 'Tabelle f�r Wichtungsfaktor');

-- *************************

PROMPT CREATE TABLE STAT_WEIGHTING
CREATE TABLE STAT_WEIGHTING
(
  ID                 NUMBER                     NOT NULL,
  CATEGORYNUMBER     NUMBER                     DEFAULT 0,
  WEIGHTINGNAME      VARCHAR2(200 BYTE),
  WEIGHTINGVALUE     NUMBER                     DEFAULT 100                   NOT NULL,
  ORIGINAL_TABLE_ID  NUMBER
)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
COMMENT ON COLUMN STAT_WEIGHTING.CATEGORYNUMBER IS 'Category of this value';
COMMENT ON COLUMN STAT_WEIGHTING.ID IS 'Primary Key';
COMMENT ON COLUMN STAT_WEIGHTING.WEIGHTINGVALUE IS 'Weighting value';
COMMENT ON COLUMN STAT_WEIGHTING.WEIGHTINGNAME IS 'Name of the value (e.g. ticketname)';
COMMENT ON COLUMN STAT_WEIGHTING.ORIGINAL_TABLE_ID IS 'ID from the original table - required, because the values can be from any table depending on the report. The original table is stored in STAT_WEIGHING_CATEGORY.ID_COLUMNNAME';
CREATE UNIQUE INDEX PK_STAT_WEIGHTING ON STAT_WEIGHTING
(ID)
LOGGING
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;
ALTER TABLE STAT_WEIGHTING ADD (
  CONSTRAINT PK_STAT_WEIGHTING
 PRIMARY KEY
 (ID)
    USING INDEX 
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));

PROMPT CREATE TABLE STAT_WEIGHTING_CATEGORY
CREATE TABLE STAT_WEIGHTING_CATEGORY
(
  ID              NUMBER,
  CATEGORYNAME    VARCHAR2(100 BYTE),
  DESCRIPTION     VARCHAR2(1000 BYTE),
  CATEGORYNUMBER  NUMBER                        DEFAULT 0                     NOT NULL,
  FATHERCATEGORY  NUMBER                        DEFAULT 0,
  ID_COLUMN       VARCHAR2(100 BYTE),
  NAME_COLUMN     VARCHAR2(100 BYTE),
  NAME_TABLE      VARCHAR2(100 BYTE),
  STRING_WHERE    VARCHAR2(100 BYTE)
)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
COMMENT ON COLUMN STAT_WEIGHTING_CATEGORY.FATHERCATEGORY IS '0 when Toplevel else number of CATEGORYNUMBER';
COMMENT ON COLUMN STAT_WEIGHTING_CATEGORY.ID_COLUMN IS 'Tablename of the ID column of the table where the values are from';
COMMENT ON COLUMN STAT_WEIGHTING_CATEGORY.NAME_COLUMN IS 'The value table which belongs the the ID_COLUMN';
COMMENT ON COLUMN STAT_WEIGHTING_CATEGORY.NAME_TABLE IS 'Name of the table where this categroy takes the values from';
CREATE UNIQUE INDEX PK_STAT_WEIGHTING_CATEGORY ON STAT_WEIGHTING_CATEGORY
(ID)
LOGGING
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;
ALTER TABLE STAT_WEIGHTING_CATEGORY ADD (
  CONSTRAINT PK_STAT_WEIGHTING_CATEGORY
 PRIMARY KEY
 (ID)
    USING INDEX 
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));

-- *************************
commit;

PROMPT Fertig!

spool off


