SPOOL db_1_215.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('25.08.2011', 'DD.MM.YYYY'), '1.215', 'OZS','STAT_LOOKUP');

--DROP TABLE stat_lookup cascade constraints purge;

CREATE TABLE STAT_LOOKUP
(
  LOOKUPID           NUMBER(10)                 NOT NULL,
  LINEID             NUMBER(10)                 DEFAULT 0                     NOT NULL,
  FARESTAGEID        NUMBER(10)                 DEFAULT 0                     NOT NULL,
  STOPFROMID         NUMBER(10)                 DEFAULT 0                     NOT NULL,
  STOPTOID           NUMBER(10)                 DEFAULT 0                     NOT NULL,
  STOPID             NUMBER(10)                 DEFAULT 0                     NOT NULL,
  FAREMATRIXENTRYID  NUMBER(10)                 DEFAULT 0                     NOT NULL,
  FROMTARIFFZONEID   NUMBER(10)                 DEFAULT 0                     NOT NULL,
  TOTARIFFZONEID     NUMBER(10)                 DEFAULT 0                     NOT NULL,
  VIATARIFFZONEID    NUMBER(10)                 DEFAULT 0                     NOT NULL,
  CLIENTID           NUMBER(10)                 DEFAULT 0                     NOT NULL,
  INSDATE            DATE                       DEFAULT SYSDATE
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE STAT_LOOKUP ADD (
  PRIMARY KEY
 (LOOKUPID)
    USING INDEX 
    TABLESPACE VARIO_IDX);
	 
-- UPDATE rm_transaction SET rm_transaction.salespostprocessingid = 0;

-- INSERT INTO rm_shiftpostprocessor(shiftid)
--     SELECT shiftid FROM rm_shift WHERE shiftid NOT IN (SELECT shiftid from rm_shiftpostprocessor);



COMMIT ;

/

PROMPT Fertig!

SPOOL off
