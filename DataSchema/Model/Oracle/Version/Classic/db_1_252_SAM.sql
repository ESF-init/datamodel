SPOOL db_1_252.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('28.03.2012', 'DD.MM.YYYY'), '1.252', 'KKV','Changes to SAM_TICKET_TO_ACCOUNTENTRY and SAM_INVOICEENTRY');

ALTER TABLE SAM_TICKET_TO_ACCOUNTENTRY ADD (
	TARIFFAREA VARCHAR2(100 BYTE));

ALTER TABLE SAM_TICKET_TO_ACCOUNTENTRY ADD (
	LINENAME VARCHAR2(200 BYTE));

ALTER TABLE SAM_INVOICEENTRY ADD (
	AVERAGEDISTANCE NUMBER(10,3));

ALTER TABLE SAM_INVOICEENTRY ADD (
	MAXIMUMDISTANCE NUMBER(10,3));

ALTER TABLE SAM_INVOICEENTRY ADD (
	USEDAYSHORTNAME VARCHAR2(10 BYTE));

ALTER TABLE SAM_INVOICEENTRY ADD (
	USEDAYDESCRIPTION VARCHAR2(100 BYTE));

ALTER TABLE SAM_INVOICEENTRY ADD(
	TARIFFAREA VARCHAR2(100BYTE));

COMMIT;

PROMPT Done!

SPOOL off