SPOOL db_1_469.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.469', 'DST','Modified CountingAmount and ReadingAmount to long');

ALTER TABLE  AM_ComponentClearing MODIFY 
(
  CountingAmount    NUMBER(18),
  ReadingAmount    NUMBER(18)
);


COMMIT;

PROMPT Fertig!

SPOOL off
