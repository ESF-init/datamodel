SPOOL db_1_487.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.487', 'OZS','Sam_status added lastdata');


ALTER TABLE VDV_SAMSTATUS
 ADD (lastdata  DATE);

commit;