SPOOL db_1_303.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('17.01.2013', 'DD.MM.YYYY'), '1.303', 'FLF','TM Ticket');

ALTER TABLE TM_TICKET ADD  FARESTAGELISTID       NUMBER(10);
 
COMMIT;

PROMPT Fertig!

SPOOL off