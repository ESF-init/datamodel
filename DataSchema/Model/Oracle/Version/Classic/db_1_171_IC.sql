SPOOL db_1_171.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('13.01.2011', 'DD.MM.YYYY'), '1.171', 'MTW','VARIO_DEVICECLASS column LOCKABLE added');

ALTER TABLE VARIO_DEVICECLASS
ADD (LOCKABLE NUMBER(1) DEFAULT 0);
 
ALTER TABLE UM_DEVICE
ADD (LOCKSTATE NUMBER(1) DEFAULT 0);

CREATE OR REPLACE FORCE VIEW view_umdevice_client (deviceid,
                                                        unitid,
                                                        mountingplate,
                                                        deviceclassid,
                                                        deviceno,
                                                        lastdata,
                                                        NAME,
                                                        clientid,
                                                        state,
                                                        inventorynumber,
                                                        lockstate
                                                       )
AS
   SELECT um_device.deviceid, um_device.unitid, um_device.mountingplate,
          um_device.deviceclassid, um_device.deviceno, um_device.lastdata,
          NVL (um_device.NAME, '') AS NAME, um_unit.clientid, um_device.state,
          um_device.inventorynumber, UM_DEVICE.LOCKSTATE
     FROM um_device LEFT OUTER JOIN um_unit ON um_device.unitid =
                                                                um_unit.unitid;
 

 COMMIT ;

PROMPT Fertig!

SPOOL off
