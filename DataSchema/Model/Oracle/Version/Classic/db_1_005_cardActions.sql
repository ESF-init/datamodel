
spool db_1_005.log

PROMPT Benenne PP_chargingrequest  in PP_cardActionrequest um

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '23.09.2008', 'DD.MM.YYYY'), '1.005', 'ULB', 'PP_chargingrequest->cardActionrequest');
-- *************************

ALTER TABLE PP_chargingrequest RENAME TO PP_CARDACTIONREQUEST;
ALTER TABLE PP_chargingrequesttype RENAME TO PP_CARDACTIONREQUESTTYPE;

CREATE OR REPLACE  SYNONYM PP_chargingrequest FOR PP_CARDACTIONREQUEST; 

CREATE OR REPLACE  SYNONYM PP_chargingrequestTYPE FOR PP_CARDACTIONREQUESTTYPE;

CREATE OR REPLACE FUNCTION GetPendingCharges
(
    cardNo1 in NUMBER
)
RETURN NUMBER IS
tmpVar NUMBER;

/******************************************************************************
   NAME:       GetAmountWithPendingCharges
   PURPOSE:    return the CardBalance after the latest transaction + the sum of all pending ChargingRequests

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        11.01.2008   BAN			   1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     GetAmountWithPendingCharges
      Sysdate:         11.01.2008
      Date and Time:   11.01.2008, 12:57:03, and 11.01.2008 12:57:03
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
BEGIN
   tmpVar := 0;
   SELECT 
	   NVL(SUM(CHARGEVALUE+0),0) into tmpVar
	FROM 
		 PP_CARDACTIONREQUEST
	WHERE
		  PP_CARDACTIONREQUEST.CARDNO = cardNo1
	AND   PP_CARDACTIONREQUEST.REQUESTTYPE=1
	AND   PP_CARDACTIONREQUEST.ISPROCESSED=0;
	
	
   RETURN tmpVar;
    EXCEPTION
     WHEN NO_DATA_FOUND THEN
       NULL;
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
	   
END GetPendingCharges;
/

-- *************************
commit;

PROMPT Fertig!

spool off