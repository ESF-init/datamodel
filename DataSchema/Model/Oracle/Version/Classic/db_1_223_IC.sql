SPOOL db_1_223.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
     VALUES (TO_DATE ('29.09.2011', 'DD.MM.YYYY'), '1.223', 'MKK','Add Column REQUESTEDLOCKSTATE');

ALTER TABLE UM_DEVICE ADD REQUESTEDLOCKSTATE NUMBER (1) default 0;

DROP VIEW VIEW_UMDEVICE_CLIENT;

/* Formatted on 2011/09/29 15:47 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW view_umdevice_client (deviceid,
                                                   unitid,
                                                   mountingplate,
                                                   deviceclassid,
                                                   deviceno,
                                                   lastdata,
                                                   NAME,
                                                   clientid,
                                                   state,
                                                   inventorynumber,
                                                   lockstate,
                                                   requestedlockstate
                                                  )
AS
   SELECT um_device.deviceid, um_device.unitid, um_device.mountingplate,
          um_device.deviceclassid, um_device.deviceno, um_device.lastdata,
          NVL (um_device.NAME, '') AS NAME, um_unit.clientid, um_device.state,
          um_device.inventorynumber, um_device.lockstate,
          um_device.requestedlockstate
     FROM um_device LEFT OUTER JOIN um_unit ON um_device.unitid =
                                                                um_unit.unitid
          ;


COMMIT;

PROMPT Fertig!

SPOOL off
