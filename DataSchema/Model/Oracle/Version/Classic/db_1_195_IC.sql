SPOOL db_1_195.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
     VALUES (TO_DATE ('19.04.2011', 'DD.MM.YYYY'), '1.195', 'MTW','p_DELETE_UM_UNIT');

alter table UM_UNIT drop constraint "FK_UM_UNIT_UM_UNIT";

COMMIT;
	 
create or replace
PROCEDURE p_DELETE_UM_UNIT(
    nUNITID IN NUMBER )
IS
BEGIN
  UPDATE um_device SET unitid = 1 WHERE unitid= nUNITID;
  DELETE FROM um_archivejobdistributedtounit WHERE unitid=nUNITID;
  DELETE FROM um_currentdistributedarchive WHERE unitid = nUNITID;
  DELETE FROM um_datatraffic WHERE unitid = nUNITID;
  DELETE FROM um_unit_client_visibility WHERE unitid = nUNITID;
  DELETE FROM um_unitdistributiontask WHERE unitid = nUNITID;
  DELETE FROM um_unittogroup WHERE unitid = nUNITID;
  DELETE FROM um_unit WHERE unitid = nUNITID;
  COMMIT;
END p_DELETE_UM_UNIT;
/
SPOOL off
