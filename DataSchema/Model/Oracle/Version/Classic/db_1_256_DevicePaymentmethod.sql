SPOOL db_1_256.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('18.04.2012', 'DD.MM.YYYY'), '1.256', 'COS ','Add column DEVICE_PAYMENTMETHOD.EXTERNALNUMBER');

ALTER TABLE RM_DEVICEPAYMENTMETHOD
ADD (EXTERNALNUMBER NUMBER(10));

UPDATE RM_DEVICEPAYMENTMETHOD set externalnumber = devicepaymentmethodid where externalnumber is null;
COMMIT;

PROMPT Done!

SPOOL off