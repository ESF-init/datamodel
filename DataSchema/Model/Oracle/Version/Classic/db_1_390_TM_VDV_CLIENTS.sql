SPOOL db_1_390.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.390', 'FLF','Added columns to table tm_vdv_keyset and tm_vdv_product');


ALTER TABLE TM_VDV_PRODUCT
 ADD ( OWNERCLIENTID             NUMBER(18)                    NOT NULL);

ALTER TABLE TM_VDV_KEYSET
 ADD (OWNERCLIENTID    NUMBER(18)); 


 
COMMIT;

PROMPT Fertig!

SPOOL off