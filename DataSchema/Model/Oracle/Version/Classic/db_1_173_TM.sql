SPOOL db_1_173.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('21.01.2011', 'DD.MM.YYYY'), '1.173', 'FLF','TM');


ALTER TABLE TM_FAREMATRIXENTRY
 ADD ( TICKETGROUPID          NUMBER(10));
 
CREATE OR REPLACE PROCEDURE insert_farematrixentry (
   farematrixentry       IN   NUMBER,
   boarding              IN   NUMBER,
   destination           IN   NUMBER,
   fare_                 IN   NUMBER,
   farematrix            IN   NUMBER,
   distance_             IN   NUMBER,                               /*(10,3)*/
   via                   IN   NUMBER,
   realdistance_         IN   NUMBER,                               /*(10,3)*/
   averagedistance_      IN   NUMBER,                               /*(10,3)*/
   tariffattribute       IN   NUMBER,
   distanceattribute     IN   NUMBER,
   description_          IN   VARCHAR,
   priority_             IN   NUMBER,
   shortname_            IN   VARCHAR,
   longname_             IN   VARCHAR,
   linefilterattribute   IN   NUMBER,
   serviceallocation     IN   NUMBER,
   routename             IN   NUMBER,
   directionattribute    IN   NUMBER,
   usedaysattribute      IN   NUMBER default null,
   ticketgroup           IN   NUMBER default null
)
IS
BEGIN
   INSERT INTO tm_farematrixentry
               (farematrixentryid, boardingid, destinationid, fare,
                farematrixid, distance, viaid, realdistance,
                averagedistance, tariffattributeid, distanceattributeid,
                description, priority, shortname, longname,
                linefilterattributeid, serviceallocationid, routenameid,
                directionattributeid, usedaysattributeid, ticketgroupid
               )
        VALUES (farematrixentry, boarding, destination, fare_,
                farematrix, distance_, via, realdistance_,
                averagedistance_, tariffattribute, distanceattribute,
                description_, priority_, shortname_, longname_,
                linefilterattribute, serviceallocation, routename,
                directionattribute, usedaysattribute, ticketgroup
               );
END insert_farematrixentry;
/

 
 
CREATE TABLE TM_TICKETTOGROUP
(
  TICKETID       NUMBER(10),
  TICKETGROUPID  NUMBER(10)
);


CREATE UNIQUE INDEX TM_TICKETTOGROUP_PK ON TM_TICKETTOGROUP
(TICKETID, TICKETGROUPID);


ALTER TABLE TM_TICKETTOGROUP ADD (
  CONSTRAINT TM_TICKETTOGROUP_PK
 PRIMARY KEY
 (TICKETID, TICKETGROUPID)
    USING INDEX);

	

CREATE TABLE TM_TICKETGROUP
(
  TICKETGROUPID   NUMBER(10),
  TARIFFID        NUMBER(10)                    NOT NULL,
  EXTERNALNUMBER  NUMBER(10),
  DESCRIPTION     VARCHAR2(100 BYTE)
);


CREATE UNIQUE INDEX TM_TICKETGROUP_PK ON TM_TICKETGROUP
(TICKETGROUPID);


ALTER TABLE TM_TICKETGROUP ADD (
  CONSTRAINT TM_TICKETGROUP_PK
 PRIMARY KEY
 (TICKETGROUPID));


COMMIT ;

PROMPT Fertig!

SPOOL off
