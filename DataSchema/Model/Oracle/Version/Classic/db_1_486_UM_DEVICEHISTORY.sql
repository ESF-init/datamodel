SPOOL db_1_486.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.486', 'UB','Add column Mountingplate to UM_Devicehistory');

ALTER TABLE UM_DEVICEHISTORY ADD (MOUNTINGPLATE NUMBER(10) DEFAULT 0);

COMMIT;


PROMPT Fertig!

SPOOL off
