spool db_1_093.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '08.12.2009', 'DD.MM.YYYY'), '1.093', 'ULB', 'neues attribut f�r dm_cardclearinghistory');

--**************************************
alter TABLE dm_cardclearinghistory add (balancing           NUMBER(1));

alter TABLE RM_SHIFTFILELOCATION add (fileimportdate           date);

CREATE OR REPLACE VIEW VIEW_RMSHIFT_FILE
(SHIFTID, SHIFTBEGIN, DEBTORNO, DEVICENO, LOCATIONNO, 
 FILENAME, SHORTNAME, NAME, DECRIPTION, IMPORTDATE)
AS 
SELECT rm_shift.shiftid,RM_SHIFT.shiftbegin,debtorno, RM_SHIFT.deviceno, RM_SHIFT.locationno, 
       FILELOCATION.filename, 
    SUBSTR(FILELOCATION.filename, LENGTH(FILELOCATION.filename)-17) AS shortname, 
    FILETYPE.NAME, RM_SHIFTFILELOCATION.decription ,fileimportdate as IMPORTDATE 
  FROM RM_SHIFT, RM_SHIFTFILELOCATION, FILELOCATION, FILETYPE 
 WHERE (    (RM_SHIFT.shiftid = RM_SHIFTFILELOCATION.shiftid) 
        AND (FILETYPE.filetypid = FILELOCATION.typeid) 
        AND (RM_SHIFTFILELOCATION.filelocationid = FILELOCATION.ID) 
       );

CREATE OR REPLACE TRIGGER Trg_RM_Setimportdate
BEFORE INSERT
ON RM_SHIFTFILELOCATION
REFERENCING NEW AS new OLD AS old
FOR EACH ROW
BEGIN
    :new.FILEIMPORTDATE := SYSDATE;
END;
/

Prompt Aktualisiere Importzeiten	   
update RM_SHIFTFILELOCATION set fileimportdate = (select importdate from rm_shift where rm_shift.shiftid = rm_SHIFTFILELOCATION.shiftid )
where fileimportdate is null;

--*****************************************

COMMIT;

PROMPT Fertig!

spool off
