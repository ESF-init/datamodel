SPOOL db_1_156.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('24.11.2010', 'DD.MM.YYYY'), '1.156', 'HSS','TM_LAYOUT added DevMask');


ALTER TABLE tm_layout
 ADD ( DEVMASK 	NUMBER(10));


COMMIT ;

PROMPT Fertig!

SPOOL off
