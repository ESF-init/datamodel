spool db_1_104.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '15.02.2010', 'DD.MM.YYYY'), '1.104', 'HSS', 'Added price2 into the ticket');

--**************************************
alter table TM_TICKET
ADD (PRICE2                    NUMBER(10)          DEFAULT 0);

--*****************************************

COMMIT;

PROMPT Fertig!

spool off
