SPOOL db_1_374.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.374', 'FLF','Added AREALISTKEYBACK col to TM_FAREMATRIXENTRY');



ALTER TABLE TM_FAREMATRIXENTRY ADD  AREALISTKEYBACK        NUMBER(18);

 
COMMIT;

PROMPT Fertig!

SPOOL off