SPOOL db_1_334.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.334', 'FLF','Ticket Info Text');

 ALTER TABLE TM_TICKET 		ADD		  INFOTEXT                   VARCHAR2(2000 BYTE);
 
COMMIT;

PROMPT Fertig!

SPOOL off