SPOOL db_1_321.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.321', 'FLF','Added FORMID to TM_CARDTICKET');

 ALTER TABLE TM_CARDTICKET ADD   FORMID                   NUMBER(18) 	NOT NULL;

COMMIT;

PROMPT Fertig!

SPOOL off