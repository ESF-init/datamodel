SPOOL db_1_350.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.350', 'STAT','Add column OrderNo to STAT_PROPERTYVALUES');

-- new column should be nullable, so no measurements regarding existing data
 ALTER TABLE STAT_PROPERTYVALUES ADD ORDERNO NUMBER(4);
 
COMMIT;

PROMPT Fertig!

SPOOL off