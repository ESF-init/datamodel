SPOOL db_1_345.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.345', 'JGI','SEPA: Add columns to PP_ACCOUNT');

ALTER TABLE PP_ACCOUNT ADD (BIC            VARCHAR2(50),
                           SEPARETCODE    VARCHAR2(10));

COMMIT;

PROMPT Fertig!

SPOOL off