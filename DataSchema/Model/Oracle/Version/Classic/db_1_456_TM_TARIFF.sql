SPOOL db_1_456.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.456', 'FLF','Added valid from time to tariff');

ALTER TABLE TM_TARIF
 ADD (VALIDFROMTIME  NUMBER(9));


COMMIT;

PROMPT Fertig!

SPOOL off