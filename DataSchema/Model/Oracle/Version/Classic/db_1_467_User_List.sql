SPOOL db_1_467.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.467', 'BVI','Modified CreatedUserBy Column in User_List.');

ALTER TABLE USER_LIST MODIFY UserCreatedBy VARCHAR2(50);

COMMIT;

PROMPT Fertig!

SPOOL off