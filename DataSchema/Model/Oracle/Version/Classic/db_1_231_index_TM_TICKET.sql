SPOOL db_1_231.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('17.11.2011', 'DD.MM.YYYY'), '1.231', 'HSS','Added index on TM_TICKET');

	 ALTER TABLE RM_TRANSACTION  ADD (deduction  NUMBER(10));

CREATE INDEX IDX_TM_TICKET_TARIFID ON TM_TICKET
(TARIFID)
NOLOGGING
TABLESPACE VARIO_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;
COMMIT ;

PROMPT Fertig!

SPOOL off
