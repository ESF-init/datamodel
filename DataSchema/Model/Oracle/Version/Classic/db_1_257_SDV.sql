SPOOL db_1_257.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('24.04.2012', 'DD.MM.YYYY'), '1.257', 'BTS ','Add column VARIO_DEPOT.ACCOUNTID2');

ALTER TABLE VARIO_DEPOT
ADD (ACCOUNTID2 NUMBER(10));

COMMIT;

PROMPT Done!

SPOOL off