SPOOL db_1_169.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('03.01.2011', 'DD.MM.YYYY'), '1.169', 'FLF','TM_ATTRIBUTEVALUE');


ALTER TABLE TM_ATTRIBUTEVALUE
 ADD (  LOGO2ID            NUMBER(10));

 
 CREATE INDEX TICKETIDINDEX ON TM_TICKET_SERVICESPERMITTED
(TICKETID);


CREATE INDEX LINEIDINDEX ON TM_TICKET_SERVICESPERMITTED
(LINEID);
 
 
COMMIT ;

PROMPT Fertig!

SPOOL off
