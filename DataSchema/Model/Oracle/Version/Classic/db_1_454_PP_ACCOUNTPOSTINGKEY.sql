SPOOL db_1_454.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.454', 'ULB', 'Added column sortsequence to PP_ACCOUNTPOSTINGKEY');
  

ALTER TABLE PP_ACCOUNTPOSTINGKEY
 ADD (sortsequence NUMBER(2) DEFAULT 0 NOT NULL);
 
COMMIT;

PROMPT Fertig!

SPOOL off