SPOOL db_1_477.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.477', 'MKK','Replace index in table UM_UNITDISTRIBUTIONTASK to increase import speed of vehicle acks');

DROP INDEX IDX_UNITDISTTASK_UNITID;

CREATE INDEX IDX_UNITDISTTASK_UNITID_TSAKID
    ON UM_UNITDISTRIBUTIONTASK (UNITID, ARCHIVEDISTRIBUTIONTASKID);

PROMPT Fertig!

SPOOL off