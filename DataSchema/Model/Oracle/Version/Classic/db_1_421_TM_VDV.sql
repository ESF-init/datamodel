SPOOL db_1_421.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.421', 'FLF', 'VDV Properties');

ALTER TABLE TM_VDV_KEYSET
 ADD (KEYVERSION_PV_SE  NUMBER(18)                  DEFAULT 1                     NOT NULL);
 
 ALTER TABLE TM_VDV_KEYSET
MODIFY(KEYVERSION_PV  DEFAULT 1);

ALTER TABLE TM_BINARYDATA
 ADD (CLIENTID  NUMBER(18)                          DEFAULT -1                    NOT NULL);


COMMIT;

PROMPT Fertig!

SPOOL off