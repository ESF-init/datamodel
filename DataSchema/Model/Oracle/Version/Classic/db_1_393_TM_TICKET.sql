SPOOL db_1_393.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.393', 'FLF','Added columns to table TM_TICKET');


ALTER TABLE TM_TICKET
 ADD (INFOTEXT2  VARCHAR2(2000 BYTE));


 
COMMIT;

PROMPT Fertig!

SPOOL off