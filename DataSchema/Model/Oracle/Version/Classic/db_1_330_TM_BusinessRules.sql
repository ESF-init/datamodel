SPOOL db_1_330.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.330', 'FLF','Business Rules');

 ALTER TABLE TM_BUSINESSRULERESULT 		ADD		TICKET			NUMBER(18);
 
 ALTER TABLE TM_BUSINESSRULEVARIABLE 	ADD		DOTNETTYPE                  VARCHAR2(400 CHAR) DEFAULT 'System.Object' NOT NULL;

COMMIT;

PROMPT Fertig!

SPOOL off