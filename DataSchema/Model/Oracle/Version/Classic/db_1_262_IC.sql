SPOOL db_1_262.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('01.06.2012', 'DD.MM.YYYY'), '1.262', 'MTW ','Added description field to UM_DEVICE');

ALTER TABLE UM_DEVICE
ADD (DESCRIPTION VARCHAR2(512 BYTE));

DROP VIEW VIEW_UMDEVICE_CLIENT;

/* Formatted on 2011/09/29 15:47 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW view_umdevice_client (deviceid,
                                                   unitid,
                                                   mountingplate,
                                                   deviceclassid,
                                                   deviceno,
                                                   lastdata,
                                                   NAME,
                                                   clientid,
                                                   state,
                                                   inventorynumber,
                                                   lockstate,
                                                   requestedlockstate,
                                                   DESCRIPTION
                                                  )
AS
   SELECT um_device.deviceid, um_device.unitid, um_device.mountingplate,
          um_device.deviceclassid, um_device.deviceno, um_device.lastdata,
          NVL (um_device.NAME, '') AS NAME, um_unit.clientid, um_device.state,
          um_device.inventorynumber, um_device.lockstate,
          um_device.requestedlockstate, um_device.DESCRIPTION
     FROM um_device LEFT OUTER JOIN um_unit ON um_device.unitid =
                                                                um_unit.unitid;


COMMIT;

PROMPT Done!

SPOOL off