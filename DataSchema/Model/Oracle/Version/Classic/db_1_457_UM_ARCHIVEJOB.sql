SPOOL db_1_457.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.457', 'JUN','Added column TOSUBTENANTS to UM_ARCHIVEJOB');

ALTER TABLE UM_ARCHIVEJOB
 ADD (TOSUBTENANTS  NUMBER(1) DEFAULT 0);


COMMIT;

PROMPT Fertig!

SPOOL off