SPOOL db_1_281.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('29.08.2012', 'DD.MM.YYYY'), '1.281', 'MTW', 'p_cleararchivejobs2 updated');

create or replace
PROCEDURE      p_cleararchivejobs2(
    ndays        IN NUMBER,
    ntypeofjobid IN NUMBER )
IS
  counter number;
  
  CURSOR c (ndays IN NUMBER, ntypeofjobid IN NUMBER)
  IS
    (SELECT archivedistributiontaskid
    FROM um_archivedistributiontask
    WHERE archivejobid IN
      (SELECT archivejobid
      FROM um_archivejob
      WHERE starttime        < SYSDATE - ndays
      AND typeofarchivejobid = ntypeofjobid
      )
    );
    
    CURSOR x (ndays IN NUMBER, ntypeofjobid IN NUMBER)
    IS
      (SELECT archivejobid, filename
      FROM um_archivejob
      WHERE starttime        < SYSDATE - ndays
      AND typeofarchivejobid = ntypeofjobid
      );

  BEGIN
  
  counter := 0;
    
    FOR r IN c (ndays, ntypeofjobid)
    LOOP
      DELETE
      FROM um_lserrortotask
      WHERE archivedistributiontaskid = r.archivedistributiontaskid;
      DELETE
      FROM um_unitdistributiontask
      WHERE archivedistributiontaskid = r.archivedistributiontaskid;
      DELETE 
      FROM um_lstotask
      WHERE archivedistributiontaskid = r.archivedistributiontaskid;
      
      if counter = 2500 then
      COMMIT;
      counter := 0;
      else
      counter := counter+1;
      end if;
    END LOOP;
    
    commit;
    counter:=0;
    FOR r IN x (ndays, ntypeofjobid)
    LOOP
      DELETE FROM um_archivedistributiontask WHERE archivejobid = r.archivejobid;

      DELETE
      FROM um_archivejobdistributedtounit
      WHERE archivejobid = r.archivejobid;

      DELETE FROM proccessedfiles WHERE filename=r.filename;

      DELETE FROM um_archivejob WHERE archivejobid = r.archivejobid;
      
      if counter = 25 then
        COMMIT;
        counter := 0;
      else
        counter := counter+1;
      end if;

    END LOOP;
  
  commit;
  END p_cleararchivejobs2; 
/

create or replace
PROCEDURE P_CLEAR_LOADINGSTATISTICS 
(
  nDays IN NUMBER  
) AS 

counter number;

CURSOR devices
  IS
    (SELECT deviceid, LASTDATA
      FROM um_device
    );
BEGIN
  counter := 0;

  FOR device IN devices LOOP
    DELETE 
    FROM um_loadingstatistics l 
    WHERE 
      device.deviceid = l.deviceid 
      AND 
      ((l.loadeduntil < device.lastdata AND l.loadeduntil < SYSDATE - ndays)
        OR
        (l.loadeduntil IS NULL AND l.loadedfrom < device.lastdata AND l.loadedfrom < SYSDATE - ndays));
     
      if counter = 500 then
      COMMIT;
      counter := 0;
      else
      counter := counter+1;
      end if;
      
  END LOOP;
  COMMIT;
END P_CLEAR_LOADINGSTATISTICS;
/
COMMIT;

PROMPT Fertig! 

SPOOL off