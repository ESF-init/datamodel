SPOOL db_1_296.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('31.10.2012', 'DD.MM.YYYY'), '1.296', 'FLF','TM added ruleperiodnumber');

ALTER TABLE TM_RULE_PERIOD ADD RULEPERIODNUMBER	NUMBER(18)	NOT NULL;

COMMIT;

PROMPT Fertig!

SPOOL off