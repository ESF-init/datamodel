SPOOL db_1_453.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.453', 'COS', 'Added row MOCAFILESYSTEM');
  
ALTER TABLE VARIO_TYPEOFCARD
ADD (MOCAFILESYSTEM NUMBER(10,0) DEFAULT 0);
 
COMMIT;

PROMPT Fertig!

SPOOL off