SPOOL db_1_325.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.325', 'OZS','Erweiterung von VARIO_STOP um STOPNAMEEXTERN');

ALTER TABLE VARIO_STOP
ADD (STOPNAMEEXTERN VARCHAR2(100 BYTE));


COMMIT;


PROMPT Fertig!

SPOOL off