SPOOL db_1_249.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('12.03.2012', 'DD.MM.YYYY'), '1.249', 'BTS','Created table Vario_Line_To_Depot for SDV');

CREATE TABLE VARIO_LINE_TO_DEPOT
(
  LINENO    NUMBER(22),
  DEPOTID   NUMBER(10),
  CLIENTID  NUMBER(10)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMIT;

PROMPT Done!

SPOOL off