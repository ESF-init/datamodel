SPOOL db_1_207.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
     VALUES (TO_DATE ('08.07.2011', 'DD.MM.YYYY'), '1.207', 'MKK','Added column DEVICEID to UM_DATATRAFFIC;');

ALTER TABLE UM_DATATRAFFIC
    ADD (DEVICEID NUMBER(10));

    
ALTER TABLE UM_DATATRAFFIC ADD (
  FOREIGN KEY (DEVICEID) 
 REFERENCES UM_DEVICE (DEVICEID));

SPOOL off
