spool db_1_094.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '18.12.2009', 'DD.MM.YYYY'), '1.094', 'ULB', 'Neu Dm_Contractor.balancing');

--**************************************
alter TABLE Dm_Contractor add (balancing           NUMBER(1));

--*****************************************

COMMIT;

PROMPT Fertig!

spool off
