SPOOL db_1_372.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.372', 'FLF','Added AREALISTKEYGROUP col to TM_AREALIST');



ALTER TABLE TM_AREALIST ADD  AREALISTKEYGROUP        NUMBER(18)            NOT NULL;

 
COMMIT;

PROMPT Fertig!

SPOOL off