SPOOL db_1_246.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('02.03.2012', 'DD.MM.YYYY'), '1.246', 'TSC','Added table TopUpReasonCodeID for MTS.');

CREATE TABLE PP_TOPUPREASONCODE
(
    TopUpReasonCodeID   NUMBER(18, 0)   NOT NULL, 
    TopUpReasonCode     NVARCHAR2(50)   NOT NULL,
    ShortDescription    NVARCHAR2(50)   NOT NULL, 
    Localization        NVARCHAR2(50)   NOT NULL,
	CONSTRAINT TopUpReasonCode_PK PRIMARY KEY (TopUpReasonCodeID)
);

/*
INSERT ALL
    INTO PP_TOPUPREASONCODE (TopUpReasonCodeID, TopUpReasonCode, ShortDescription, Localization) VALUES (1, '1', 'Correct online top-up failure', 'en')
    INTO PP_TOPUPREASONCODE (TopUpReasonCodeID, TopUpReasonCode, ShortDescription, Localization) VALUES (2, '2', 'Correct top-up error by bus operator', 'en')
    INTO PP_TOPUPREASONCODE (TopUpReasonCodeID, TopUpReasonCode, ShortDescription, Localization) VALUES (3, '3', 'Transfer of credit to a replacement card', 'en')
    INTO PP_TOPUPREASONCODE (TopUpReasonCodeID, TopUpReasonCode, ShortDescription, Localization) VALUES (4, '4', 'Refund of credit due to cardholder request', 'en')
	INTO PP_TOPUPREASONCODE (TopUpReasonCodeID, TopUpReasonCode, ShortDescription, Localization) VALUES (5, '5', 'Refund of credit due to fare overcharging', 'en')
    INTO PP_TOPUPREASONCODE (TopUpReasonCodeID, TopUpReasonCode, ShortDescription, Localization) VALUES (6, '6', 'Top-up card for promotion activities', 'en')
SELECT * FROM DUAL; 
*/

--ROLLBACK;
COMMIT;

PROMPT Done!

SPOOL off