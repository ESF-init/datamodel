SPOOL db_1_496.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.496', 'FLF', 'Extended ETICKDUETIME');

ALTER TABLE TM_ETICKET
MODIFY(ETICKDUETIME NUMBER(6));


COMMIT;

PROMPT Fertig!

SPOOL off