SPOOL db_1_201.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
     VALUES (TO_DATE ('30.05.2011', 'DD.MM.YYYY'), '1.201', 'MKK','edited p_DELETE_UM_UNIT edited');

CREATE OR REPLACE PROCEDURE p_DELETE_UM_UNIT(
    nUNITID IN NUMBER )
IS
nGarageId NUMBER :=-1;
nClientId NUMBER :=0;
nClientGarageCount NUMBER := 0;
BEGIN
    SELECT CLIENTID INTO nClientId FROM UM_UNIT WHERE UNITID = nUNITID; 
    SELECT count(UNITID) INTO nClientGarageCount FROM  UM_UNIT where TYPEOFUNITID = 0 AND CLIENTID = nClientId;
  IF nClientGarageCount = 1 
  THEN
    SELECT UNITID INTO nGarageId FROM  UM_UNIT where TYPEOFUNITID = 0 AND CLIENTID = nClientId;
  ELSE
    nGarageId := 1;
  END IF;
  UPDATE um_device SET unitid = nGarageId, state = 2 WHERE unitid= nUNITID;
  DELETE FROM um_archivejobdistributedtounit WHERE unitid=nUNITID;
  DELETE FROM um_currentdistributedarchive WHERE unitid = nUNITID;
  DELETE FROM um_datatraffic WHERE unitid = nUNITID;
  DELETE FROM um_unit_client_visibility WHERE unitid = nUNITID;
  DELETE FROM um_unitdistributiontask WHERE unitid = nUNITID;
  DELETE FROM um_unittogroup WHERE unitid = nUNITID;
  DELETE FROM um_unit WHERE unitid = nUNITID;
  COMMIT;
END p_DELETE_UM_UNIT;
/

SPOOL off
