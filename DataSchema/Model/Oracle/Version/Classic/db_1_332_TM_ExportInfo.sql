SPOOL db_1_332.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.332', 'FLF','Export Info');

 ALTER TABLE TM_EXPORTINFO 		ADD		ALLOWTESTEXPORT			NUMBER(1)                    DEFAULT 0                     NOT NULL;
 
COMMIT;

PROMPT Fertig!

SPOOL off