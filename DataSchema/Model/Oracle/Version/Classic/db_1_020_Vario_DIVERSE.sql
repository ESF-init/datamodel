
spool db_1_020.log

PROMPT CREATE TABLE TMP
CREATE TABLE TMP
(
  NAME    VARCHAR2(100 BYTE),
  SFNAME  VARCHAR2(100 BYTE)
);

PROMPT CREATE TABLE TM_CONDITIONALSUBPAGELAYOUTOBJ
CREATE TABLE TM_CONDITIONALSUBPAGELAYOUTOBJ
(
  LAYOUTOBJECTID  NUMBER(10)                    NOT NULL,
  ROW_            NUMBER(10,3)                  NOT NULL,
  COLUMN_         NUMBER(10,3)                  NOT NULL,
  LAYOUTID        NUMBER(10)                    NOT NULL,
  CONDITION       VARCHAR2(1000 BYTE),
  PAGEID          NUMBER(10)
)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

PROMPT ALTER TABLE TM_CONDITIONALSUBPAGELAYOUTOBJ
ALTER TABLE TM_CONDITIONALSUBPAGELAYOUTOBJ ADD (
  PRIMARY KEY
 (LAYOUTOBJECTID)
    USING INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));

PROMPT CREATE TABLE TM_FARETABLEENTRY
CREATE TABLE TM_FARETABLEENTRY
(
  FARETABLEENTRYID  NUMBER(10)                  NOT NULL,
  FARE1             NUMBER(10)                  NOT NULL,
  FARE2             NUMBER(10)                  NOT NULL,
  FARETABLEID       NUMBER(10),
  ATTRIBUTEVALUEID  NUMBER(10),
  DISTANCE          NUMBER(10)
)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

PROMPT ALTER TABLE TM_FARETABLEENTRY
ALTER TABLE TM_FARETABLEENTRY ADD (
  PRIMARY KEY
 (FARETABLEENTRYID)
    USING INDEX 
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          384K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));

PROMPT CREATE TABLE RM_DUTYBALANCESTATE
CREATE TABLE RM_DUTYBALANCESTATE
(
  STATEID    NUMBER(2),
  STATETEXT  VARCHAR2(50 BYTE)
)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

PROMPT ALTER TABLE RM_DUTYBALANCESTATE
ALTER TABLE RM_DUTYBALANCESTATE ADD (
  PRIMARY KEY
 (STATEID)
    USING INDEX 
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));

PROMPT CREATE TABLE TM_PANELS
CREATE TABLE TM_PANELS
(
  PANELID      NUMBER(10)                       NOT NULL,
  NUMBER_      NUMBER(10),
  EVENDNUMBER  NUMBER(10),
  DESCRIPTION  VARCHAR2(100 BYTE),
  TARIFID      NUMBER(10),
  NAME         VARCHAR2(100 BYTE)
)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

PROMPT CREATE UNIQUE INDEX TM_PANELS_PK
CREATE UNIQUE INDEX TM_PANELS_PK ON TM_PANELS
(PANELID)
LOGGING
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

PROMPT ALTER TABLE TM_PANELS
ALTER TABLE TM_PANELS ADD (
  CONSTRAINT TM_PANELS_PK
 PRIMARY KEY
 (PANELID)
    USING INDEX 
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));

PROMPT CREATE TABLE RM_PAYMENT
CREATE TABLE RM_PAYMENT
(
  PAYMENTID              NUMBER(10)             NOT NULL,
  TRANSACTIONID          NUMBER(10)             NOT NULL,
  DEVICEPAYMENTMETHODID  NUMBER(10)             NOT NULL,
  AMOUNT                 NUMBER(10)
)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

PROMPT ALTER TABLE RM_PAYMENT
ALTER TABLE RM_PAYMENT ADD (
  PRIMARY KEY
 (PAYMENTID)
    USING INDEX 
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));

PROMPT CREATE TABLE TM_SYSTEMFIELDBARCODELAYOUTOBJ
CREATE TABLE TM_SYSTEMFIELDBARCODELAYOUTOBJ
(
  LAYOUTOBJECTID  NUMBER(10)                    NOT NULL,
  ROW_            NUMBER(10,3)                  NOT NULL,
  COLUMN_         NUMBER(10,3)                  NOT NULL,
  LAYOUTID        NUMBER(10)                    NOT NULL,
  SYSTEMFIELDID   NUMBER(10),
  FIELDWIDTH      NUMBER(10),
  TYPE            NUMBER(10)
)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

PROMPT ALTER TABLE TM_SYSTEMFIELDBARCODELAYOUTOBJ 
ALTER TABLE TM_SYSTEMFIELDBARCODELAYOUTOBJ ADD (
  PRIMARY KEY
 (LAYOUTOBJECTID)
    USING INDEX 
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));

PROMPT CREATE TABLE TM_FARESTAGEALIAS
CREATE TABLE TM_FARESTAGEALIAS
(
  FARESTAGEALIASID  NUMBER(10)                  NOT NULL,
  NAME              VARCHAR2(100 BYTE)          NOT NULL,
  DESCRIPTION       VARCHAR2(200 BYTE),
  FARESTAGEID       NUMBER(10)                  NOT NULL
)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

PROMPT CREATE INDEX TM_FARESTAGE_ID
CREATE INDEX TM_FARESTAGE_ID ON TM_FARESTAGEALIAS
(FARESTAGEID)
LOGGING
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

PROMPT ALTER TABLE TM_FARESTAGEALIAS
ALTER TABLE TM_FARESTAGEALIAS ADD (
  PRIMARY KEY
 (FARESTAGEALIASID)
    USING INDEX 
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));


PROMPT CREATE TABLE TM_FARETABLE
CREATE TABLE TM_FARETABLE
(
  FARETABLEID  NUMBER(10)                       NOT NULL,
  NAME         VARCHAR2(100 BYTE)               NOT NULL,
  DESCRIPTION  VARCHAR2(200 BYTE),
  TARIFID      NUMBER(10)                       NOT NULL
)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

PROMPT ALTER TABLE TM_FARETABLE
ALTER TABLE TM_FARETABLE ADD (
  PRIMARY KEY
 (FARETABLEID)
    USING INDEX 
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));

PROMPT CREATE OR REPLACE FORCE VIEW VIEW_RM_DUTYBALANCE
CREATE OR REPLACE FORCE VIEW VIEW_RM_DUTYBALANCE
(AMOUNT, DEBTORID, REFNO, REQUESTDATE, STATE, 
 CLIENTID)
AS 
SELECT RM_DUTYBALANCE.AMOUNT,RM_DUTYBALANCE.DEBTORID,RM_DUTYBALANCE.REFNO,RM_DUTYBALANCE.REQUESTDATE,RM_DUTYBALANCE.STATE, DM_DEBTOR.CLIENTID 
FROM RM_DUTYBALANCE,DM_DEBTOR 
WHERE RM_DUTYBALANCE.DEBTORID = dm_debtor.debtorid
/


PROMPT CREATE INDEX PK_TICKETID
CREATE INDEX PK_TICKETID ON TM_ATTRIBUTEVALUELIST
(TICKETID)
LOGGING
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '11.12.2008', 'DD.MM.YYYY'), '1.020', 'JGi', 'Diverse Tabellen erstellen');

COMMIT;

PROMPT Fertig!

spool off