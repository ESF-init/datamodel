spool db_1_087.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '05.11.2009', 'DD.MM.YYYY'), '1.087', 'MIW', 'Feld ONLINEACCOUNTNAME von 10 auf 100 vergroessert.');

--**************************************

ALTER TABLE PP_CONTRACT
MODIFY(ONLINEACCOUNTNAME VARCHAR2(100 BYTE));

--*****************************************

COMMIT;

PROMPT Fertig!

spool off
