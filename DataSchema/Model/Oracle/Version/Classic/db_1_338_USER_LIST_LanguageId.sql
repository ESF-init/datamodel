SPOOL db_1_338.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.338', 'MGR','USER_LIST: Modify LanguageId Number(5) length');

ALTER TABLE USER_LIST MODIFY (languageid NUMBER(5));

COMMIT;

PROMPT Fertig!

SPOOL off