spool db_1_161.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '02.12.2010', 'DD.MM.YYYY'), '1.161', 'MKK', 'Procedure p_getarchivedistributionstate improved. Procedure p_SetJobComplete extended by UPDATE um_archivejob');

CREATE OR REPLACE PROCEDURE p_getarchivedistributionstate(
   jobid      IN       NUMBER,
   jobstate   OUT      NUMBER
)
IS
   completed                        NUMBER (1, 0);
   countdepotserver                 NUMBER;
   countload          NUMBER;
   starttime                        DATE;
   active                           NUMBER (1, 0);
   client                           NUMBER;
   typeofarchivejob                 NUMBER;
   archive_id                        NUMBER;
BEGIN


   SELECT completed, starttime, active, typeofarchivejobid, clientid, archiveid
     INTO completed, starttime, active, typeofarchivejob, client, archive_id
     FROM um_archivejob
    WHERE archivejobid = jobid;

   IF active = 0
   THEN
      IF starttime > SYSDATE
      THEN
         jobstate := 0;
         RETURN;
      ELSE
         jobstate := 3;
         RETURN;
      END IF;
   END IF;

   IF completed <> 0 OR typeofarchivejob = 3
   THEN
      jobstate := 2;
      RETURN;
   END IF;


SELECT count(dev.deviceid) into countdepotserver
FROM um_device dev,
  um_unit unit
WHERE dev.deviceclassid = -1
AND unit.unitid         = dev.unitid
AND unit.typeofunitid   = 2
AND (unit.clientid      = client
OR unit.clientid        =0);

SELECT count(l.deviceid) into countload
  FROM um_unit u INNER JOIN um_device d
       ON u.unitid = d.unitid
     AND d.deviceclassid = -1
     AND u.typeofunitid = 2
     AND (u.clientid = client OR u.clientid = 0)
       LEFT OUTER JOIN um_loadingstatistics l ON d.deviceid = l.deviceid AND l.archiveid = archive_id AND L.LOADEDUNTIL is not null AND LSSTATE IN (0,1,9);

   
     IF countdepotserver = countload
   THEN
      jobstate := 2;
   ELSE
      if countload = 0
      THEN
        jobstate := 3;
      ELSE
        jobstate :=1;
   END IF;
   END IF;
END p_getarchivedistributionstate;
/

CREATE OR REPLACE PROCEDURE p_SetJobComplete
(
    JobID number
) IS
NjobState NUMBER;
/******************************************************************************
   NAME:       p_SetJobComplete
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        21.01.2008   BAN       1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     p_SetJobComplete
      Sysdate:         21.01.2008
      Date and Time:   21.01.2008, 13:54:40, and 21.01.2008 13:54:40
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
BEGIN
   NjobState := 0;
   p_getarchivedistributionstate(JobID,NjobState);
   IF NjobState = 2 THEN
        UPDATE UM_ARCHIVEJOB
        SET COMPLETED = 1
        WHERE ARCHIVEJOBID = JobID;
   END IF;
   UPDATE um_archivejob
         SET jobstate = NjobState
       WHERE archivejobid = JobID;
END p_SetJobComplete;
/

COMMIT;

PROMPT Fertig!

spool off
