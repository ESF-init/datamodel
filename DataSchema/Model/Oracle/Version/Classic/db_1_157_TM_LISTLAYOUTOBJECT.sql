SPOOL db_1_157.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('25.11.2010', 'DD.MM.YYYY'), '1.157', 'HSS','New Table TM_LISTLAYOUTOBJECT ');


CREATE TABLE TM_LISTLAYOUTOBJECT
(
  LAYOUTOBJECTID  NUMBER(10)                    NOT NULL,
  ROW_            NUMBER(10,3)                  NOT NULL,
  COLUMN_         NUMBER(10,3)                  NOT NULL,
  LAYOUTID        NUMBER(10)                    NOT NULL,
  PAGEID          NUMBER(10),
  NAME            VARCHAR2(100 BYTE)
);


COMMIT ;

PROMPT Fertig!

SPOOL off
