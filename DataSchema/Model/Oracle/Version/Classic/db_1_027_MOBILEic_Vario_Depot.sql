
spool db_1_027.log

PROMPT �ndern Tabelle VARIO_DEPOT

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '21.01.2009', 'DD.MM.YYYY'), '1.027', 'JGi', 'Neue Attribute CLIENTID und DEPOTNO');

-- *************************

ALTER TABLE VARIO_DEPOT 
 ADD (DEPOTNO    NUMBER(10));

ALTER TABLE VARIO_DEPOT
 ADD (CLIENTID    NUMBER(10));

-- *************************
commit;

PROMPT Fertig!

spool off