SPOOL db_1_411.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.411', 'BTS','Added attributes to SAM_ORDER');

ALTER TABLE SAM_ORDER
ADD (
  INVALID               NUMBER(1)
);

COMMIT;

PROMPT Fertig!

SPOOL off