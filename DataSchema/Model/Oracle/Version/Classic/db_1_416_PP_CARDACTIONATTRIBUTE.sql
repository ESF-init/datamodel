SPOOL db_1_416.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.416', 'ULB','Added new column to PP_CARDACTIONATTRIBUTE');

ALTER TABLE PP_CARDACTIONATTRIBUTE
ADD (PrintedCardNo VARCHAR2(50));
 

COMMIT;

PROMPT Fertig!

SPOOL off