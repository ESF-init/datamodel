SPOOL db_1_149.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('12.10.2010', 'DD.MM.YYYY'), '1.149', 'FLF','New field in TM_EXTERNAL_PACKETEFFORT');



ALTER TABLE TM_EXTERNAL_PACKETEFFORT
 ADD (
    lineid  NUMBER(10));

	
COMMIT ;

PROMPT Fertig!

SPOOL off
