SPOOL db_1_365.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.365', 'FLF','Added col VALIDITYFROMFARETABLE to table tm_rule_period');

 ALTER TABLE TM_RULE_PERIOD ADD  VALIDITYFROMFARETABLE  NUMBER(1)              DEFAULT 0;
 

COMMIT;

PROMPT Fertig!

SPOOL off