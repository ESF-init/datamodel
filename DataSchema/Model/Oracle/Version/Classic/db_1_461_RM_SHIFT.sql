SPOOL db_1_461.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.461', 'ULB','added RM_SHift.Debitaccount');


 ALTER TABLE RM_SHIFT ADD (DebitAccount NUMBER(10));

COMMIT;

PROMPT Fertig!

SPOOL off