SPOOL db_1_347.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.347', 'ULB','added RM_BINARYDATA.transactionid + rm_transaction.outputdevice');

alter table RM_BINARYDATA add (transactionid number(10));
alter table RM_Transaction add (outputdevice number(3));

COMMIT;

PROMPT Fertig!

SPOOL off