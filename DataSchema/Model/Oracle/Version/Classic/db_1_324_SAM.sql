SPOOL db_1_324.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.324', 'ULS','IBAN und BIC f�r SAM_ORDERER');

ALTER TABLE SAM_ORDERER
ADD (IBAN VARCHAR2(34 BYTE));

ALTER TABLE SAM_ORDERER
ADD (BIC VARCHAR2(11 BYTE));


COMMIT;


PROMPT Fertig!

SPOOL off