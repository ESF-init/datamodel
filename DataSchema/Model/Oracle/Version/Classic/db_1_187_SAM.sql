SPOOL db_1_185.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('29.03.2011', 'DD.MM.YYYY'), '1.187', 'KKV','SAM_REPLACEMENT.ACCOUNTINGDATE');

ALTER TABLE SAM_REPLACEMENT
 ADD (ACCOUNTINGDATE DATE);

COMMIT;

PROMPT Fertig!

SPOOL off
