SPOOL db_1_307.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('11.02.2013', 'DD.MM.YYYY'), '1.307', 'FLF','TM delete foreign key');

ALTER TABLE TM_RULE_CAPPING
drop CONSTRAINT FK_RULETOTYPEOFCARD;

ALTER TABLE TM_RULE_CAPPING
drop CONSTRAINT FK_TM_RULECAPPINGTOCALENDAR;


COMMIT;

PROMPT Fertig!

SPOOL off