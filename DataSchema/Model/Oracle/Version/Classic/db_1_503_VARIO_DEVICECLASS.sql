SPOOL db_1_503.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.503', 'SLR', 'Add column (CreateLineDefDat to Vario_DeviceClass)');

ALTER TABLE VARIO_DEVICECLASS ADD (CREATELINEDEFDAT NUMBER(1) DEFAULT 0);

COMMIT;


PROMPT Fertig!

SPOOL off