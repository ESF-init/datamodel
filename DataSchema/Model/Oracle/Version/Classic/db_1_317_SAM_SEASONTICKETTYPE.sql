SPOOL db_1_317.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.317', 'BTS','Add column ISMULTIPLETICKET in SAM_SEASONTICKETTYPE');

ALTER TABLE SAM_SEASONTICKETTYPE
ADD (ISMULTIPLETICKET NUMBER(1));

COMMIT;


PROMPT Fertig!

SPOOL off
