SPOOL db_1_474.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.474', 'FLF','Added ORIGINALIMAGE to TM_LOGO');

ALTER TABLE TM_LOGO
 ADD (ORIGINALIMAGE  BLOB);

COMMIT;

PROMPT Fertig!

SPOOL off