
spool db_1_022.log

PROMPT �ndern Tabelle VARIO_STOP 

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '11.12.2008', 'DD.MM.YYYY'), '1.022', 'JGi', 'Neuer materialized View MVIEW_TM_FARESTAGENAMES');

-- *************************

DROP MATERIALIZED VIEW MVIEW_TM_FARESTAGENAMES;

CREATE MATERIALIZED VIEW MVIEW_TM_FARESTAGENAMES 
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT NVL (TM_ATTRIBUTEVALUE.NAME, '-') service_name,
       TM_FARESTAGE.NAME farestage_name,
       TM_FARESTAGE.number_ farestage_number, TM_FARESTAGELIST.tarifid
  FROM TM_FARESTAGE, TM_FARESTAGELIST, TM_ATTRIBUTEVALUE
 WHERE (    (TM_FARESTAGELIST.farestagelistid = TM_FARESTAGE.farestagelistid
            )
        AND (TM_ATTRIBUTEVALUE.attributevalueid(+) =
                                             TM_FARESTAGELIST.attributevalueid)
       );

-- *************************
commit;

PROMPT Fertig!

spool off