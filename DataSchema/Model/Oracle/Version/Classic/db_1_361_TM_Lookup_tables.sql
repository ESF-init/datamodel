SPOOL db_1_361.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.361', 'ES','Added TM-Lookuptables for use by filtermanager');

PROMPT add lookup tables for use by filtermanager

CREATE TABLE STAT_TM_ATTRIBUTECLASS ( 
  NUMBER_ NUMBER(10), 
  NAME VARCHAR2(100 BYTE), 
  CLIENTID NUMBER(10)
);
CREATE TABLE STAT_TM_ATTRIBUTEFARESTAGE ( 
  NUMBER_ NUMBER(10), 
  NAME VARCHAR2(100 BYTE), 
  CLIENTID NUMBER(10)
);
CREATE TABLE STAT_TM_ATTRIBUTEPRODUCT ( 
  NUMBER_ NUMBER(10), 
  NAME VARCHAR2(100 BYTE), 
  CLIENTID NUMBER(10)
);
CREATE TABLE STAT_TM_ATTRIBUTETARIFFAREA ( 
  NUMBER_ NUMBER(10), 
  NAME VARCHAR2(100 BYTE), 
  CLIENTID NUMBER(10)
);
CREATE TABLE STAT_TM_ATTRIBUTETICKETTYPE ( 
  NUMBER_ NUMBER(10), 
  NAME VARCHAR2(100 BYTE), 
  CLIENTID NUMBER(10)
);

COMMIT;

PROMPT Fertig!

SPOOL off
