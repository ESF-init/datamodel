SPOOL db_1_490.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
	VALUES (sysdate, '1.490', 'DST', 'Added parameter tables to P_DELETE_UM_UNIT');

CREATE OR REPLACE PROCEDURE p_DELETE_UM_UNIT(
    nUNITID IN NUMBER )
IS
nGarageId NUMBER :=-1;
nClientId NUMBER :=0;
nClientGarageCount NUMBER := 0;
BEGIN
    SELECT CLIENTID INTO nClientId FROM UM_UNIT WHERE UNITID = nUNITID; 
    SELECT count(UNITID) INTO nClientGarageCount FROM  UM_UNIT where TYPEOFUNITID = 0 AND CLIENTID = nClientId;
  IF nClientGarageCount = 1 
  THEN
    SELECT UNITID INTO nGarageId FROM  UM_UNIT where TYPEOFUNITID = 0 AND CLIENTID = nClientId;
  ELSE
    nGarageId := 1;
  END IF;
  UPDATE um_device SET unitid = nGarageId, state = 2 WHERE unitid = nUNITID;
  UPDATE um_unit SET unitstate = 3 WHERE unitid = nUNITID;
  COMMIT;
  DELETE FROM um_unittogroup WHERE unitid = nUNITID;
  DELETE FROM um_unit_client_visibility WHERE unitid = nUNITID;
  DELETE FROM um_datatraffic WHERE unitid = nUNITID;
  DELETE FROM um_currentdistributedarchive WHERE unitid = nUNITID;
  DELETE FROM um_unitdistributiontask WHERE unitid = nUNITID;
  DELETE FROM um_archivejobdistributedtounit WHERE unitid = nUNITID;
  BEGIN
   EXECUTE IMMEDIATE 'DELETE FROM sl_parametervalue where unitid = ' || nUNITID;
   EXECUTE IMMEDIATE 'DELETE FROM sl_unitcollectiontounit where unitid = ' || nUNITID;
  EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
  END;
  DELETE FROM um_unit WHERE unitid = nUNITID;
  COMMIT;
END p_DELETE_UM_UNIT;
/
  
COMMIT;

PROMPT Fertig!

SPOOL off
