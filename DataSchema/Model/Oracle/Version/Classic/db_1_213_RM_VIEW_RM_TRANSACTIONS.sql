SPOOL db_1_213.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('03.08.2011', 'DD.MM.YYYY'), '1.213', 'OZS','VIEW_RM_TRANSACTIONS');

-- do not use version scripts to change views --- MIW 2011-08-04


/* CREATE OR REPLACE VIEW VIEW_RM_TRANSACTIONS
(TRANSACTIONID, CLIENTID, CARDID, DEVICENO, SALESCHANNEL, 
 TRIPDATETIME, NOADULTS, NOCHILDREN, FROMSTOPNO, FROMTARIFFZONE, 
 TOTARIFFZONE, VIATARIFFZONE, PRICELEVEL, SERVICEID, STATE, 
 LINE, ACCOUNTENTRYID, TRIPSERIALNO, BESTPRICERUNID, IMPORTDATETIME, 
 NOUSER1, NOUSER2, NOUSER3, NOUSER4, FACTOR, 
 SHIFTID, TARIFFID, TYPEID, TRANSACTIONTYPETEXT, PRICE, 
 VALUEOFRIDE, CANCELLATIONID, DEVICEPAYMENTMETHOD, LINENAME, ROUTENO, 
 CARDTRANSACTIONNO, CARDREFTRANSACTIONNO, CARDBALANCE, CARDCHARGETAN, CARDCREDIT, 
 DEVICEBOOKINGSTATE, PAYCARDNO, CARDNO, DEBTORNO, SHIFTBEGIN, 
 ISBOOKED, DEVICEBOOKINGSTATETEXT, EXTERNALNUMBER, INTERNALNUMBER, CATEGORYID, 
 NAME, TICKETID, TRIPFREQUENCY, TARIFID, DEVICEBOOKINGSTATEID, 
 VALIDFROM, VERSION, SHIFTSTATEID, STOPFROM, STOPTO, 
 FARESTAGE, DISTANCE, EXTTIKNAME, DEPOTID, ERRORCODE, 
 TARIFFNETWORKNO, FAREMATRIXENTRYID, BARCODE, VIANO, REVOKEID, 
 LOCATIONNO, DEVICETYPE, DEBTORNAME, CENTRALCANCELLATION, COURSENO, 
 TRIPMONTH, TARIFFAREA)
AS 
SELECT transactionid, rm_transaction.clientid, cardid,
          rm_transaction.deviceno, saleschannel, tripdatetime, noadults,
          nochildren, fromstopno, fromtariffzone, totariffzone, viatariffzone,
          pricelevel, rm_transaction.serviceid, rm_transaction.state, line,
          accountentryid, tripserialno, bestpricerunid, importdatetime,
          nouser1, nouser2, nouser3, nouser4, factor, rm_transaction.shiftid,
          tariffid, rm_transaction.typeid, typename AS transactiontypetext,
          rm_transaction.price, valueofride, cancellationid,
          devicepaymentmethod, linename, rm_transaction.routeno,
          cardtransactionno, cardreftransactionno, cardbalance, cardchargetan,
          cardcredit, devicebookingstate, paycardno, rm_shift.cardno,
          rm_shift.debtorno, rm_shift.shiftbegin, isbooked,
          rm_devicebookingstate.text AS devicebookingstatetext,
          tm_ticket.externalnumber, tm_ticket.internalnumber,
          tm_ticket.categoryid, tm_ticket.NAME, tm_ticket.ticketid,
          tm_ticket.tripfrequency, tm_tarif.tarifid,
          rm_devicebookingstate.devicebookingstateid, tm_tarif.validfrom,
          tm_tarif.VERSION, rm_shift.shiftstateid, stopfrom, stopto,
          farestage, distance, exttikname, depotid, errorcode,
          tariffnetworkno, farematrixentryid, rm_transaction.barcode, viano,
          rm_transaction.revokeid, rm_shift.locationno, rm_shift.devicetype,
          dm_debtor.NAME AS debtorname,
          DECODE (cancellationid, tripserialno, 1, 0) AS centralcancellation, courseno, 
    TO_CHAR(tripdatetime, 'MM.YYYY') tripmonth, tm_attributevalue.NAME tariffarea
     FROM rm_transaction,
          rm_shift,
          rm_devicebookingstate,
          tm_ticket,
          tm_tarif,
          rm_transactiontype,
          dm_debtor,
    tm_attributevaluelist,
    tm_attributevalue,
    tm_attribute
    WHERE rm_shift.shiftid = rm_transaction.shiftid
      AND rm_devicebookingstate.devicebookingno =
                                             rm_transaction.devicebookingstate
      AND tm_tarif.tarifid = tm_ticket.tarifid
      AND tm_tarif.tarifid = rm_transaction.tariffid
      AND tm_ticket.internalnumber = rm_transaction.tikno
      AND tm_ticket.ticketid = tm_attributevaluelist.ticketid(+)
      AND tm_attributevaluelist.attributevalueid = tm_attributevalue.attributevalueid(+)
      AND tm_attributevalue.attributeid = tm_attribute.attributeid(+)
      AND tm_attribute.attribclassid = 1
      AND rm_transaction.typeid = rm_transactiontype.typeid
      AND rm_shift.debtorno = dm_debtor.debtorno
      AND rm_shift.clientid = dm_debtor.clientid;
 */

COMMIT;

PROMPT Fertig!

SPOOL off
