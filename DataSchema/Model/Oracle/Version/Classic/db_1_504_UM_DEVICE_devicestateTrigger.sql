SPOOL db_1_504.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.504', 'MKK', 'Trigger had to be changed because seting the state of a device which has been assigned to a garage is not possible.');

CREATE OR REPLACE TRIGGER TRG_UPDATEDEVICESTATE
BEFORE UPDATE
OF UNITID
ON UM_DEVICE 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tTYPEOFUNIT NUMBER(10,0);
tOldTYPEOFUNIT NUMBER(10,0);

BEGIN

  SELECT TYPEOFUNITID INTO tTYPEOFUNIT FROM UM_UNIT WHERE UNITID = :NEW.UNITID;  
  SELECT TYPEOFUNITID INTO tOldTYPEOFUNIT FROM UM_UNIT WHERE UNITID = :OLD.UNITID;  
  

  IF tTYPEOFUNIT = 0
  THEN
   -- It is possible that the user changes the state from 2 to 3 in the GUI. 
   IF (tOldTYPEOFUNIT = 0) AND :NEW.STATE = 3
   THEN
    :NEW.STATE := 3;
   ELSE
    :NEW.STATE := 2;
   END IF;
  ELSE
	-- We need to identify if this change has been triggered by PDI or DDM. PDI always changes LASTDATA.
   IF tTYPEOFUNIT = 3 AND :NEW.LASTDATA <> :OLD.LASTDATA
   THEN
   :NEW.STATE := 1;
   
   END IF;

  END IF;
  
END TRG_UPDATEDEVICESTATE;
/

COMMIT;


PROMPT Fertig!

SPOOL off