SPOOL db_1_455.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.455', 'SBE', 'Added column VERSION to UM_SAM');
  
ALTER TABLE  UM_SAM ADD VERSION varchar2(20);
COMMIT;

PROMPT Fertig!

SPOOL off