SPOOL db_1_288.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('25.09.2012', 'DD.MM.YYYY'), '1.288', 'BTS','Added Unique constraint and disabled UNIQUE_NUMBER');

CREATE UNIQUE INDEX U_NUMBER ON SAM_INVOICE
(CLIENTID, NUMBER_)
;

ALTER TABLE SAM_INVOICE ADD (
  CONSTRAINT U_NUMBER
 UNIQUE (CLIENTID, NUMBER_)
    USING INDEX 
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               )
);

  alter table SAM_INVOICE drop CONSTRAINT UNIQUE_NUMBER;

COMMIT;

PROMPT Fertig!

SPOOL off