SPOOL db_1_448.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.448', 'TSC', 'Added column PrintedCardNo and increased column sizes for NCTs DESFire ID-based driver cards.');

ALTER TABLE dm_card
MODIFY (
  cardid NUMBER(18,0)
);

ALTER TABLE dm_card
MODIFY (
  cardno NUMBER(18,0)
);

ALTER TABLE dm_card
ADD (
  printedcardno NVARCHAR2(20)
);

COMMIT;

PROMPT Fertig!

SPOOL off