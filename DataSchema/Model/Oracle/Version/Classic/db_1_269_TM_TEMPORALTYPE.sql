SPOOL db_1_269.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('27.06.2012', 'DD.MM.YYYY'), '1.269', 'HSS','TM_TEMPORALTYPE extended Description column size to 350.');

ALTER TABLE TM_TEMPORALTYPE
MODIFY(DESCRIPTION VARCHAR2(350 BYTE));


COMMIT;


PROMPT Fertig!

SPOOL off
