SPOOL db_1_369.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.369', 'MKK','EXTERNALDEVICENO added to view VIEW_UMDEVICE_CLIENT.');

DROP VIEW VIEW_UMDEVICE_CLIENT;

/* Formatted on 2014/02/18 07:42 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW view_umdevice_client (deviceid,
                                                         unitid,
                                                         mountingplate,
                                                         deviceclassid,
                                                         deviceno,
                                                         lastdata,
                                                         NAME,
                                                         clientid,
                                                         state,
                                                         inventorynumber,
                                                         lockstate,
                                                         requestedlockstate,
                                                         description,
                                                         externaldeviceno
                                                        )
AS
   SELECT um_device.deviceid, um_device.unitid, um_device.mountingplate,
          um_device.deviceclassid, um_device.deviceno, um_device.lastdata,
          NVL (um_device.NAME, '') AS NAME, um_unit.clientid, um_device.state,
          um_device.inventorynumber, um_device.lockstate,
          um_device.requestedlockstate, um_device.description,
          um_device.externaldeviceno
     FROM um_device LEFT OUTER JOIN um_unit ON um_device.unitid = um_unit.unitid;

COMMIT;

PROMPT Fertig!

SPOOL off