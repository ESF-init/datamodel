SPOOL db_1_154.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('11.11.2010', 'DD.MM.YYYY'), '1.154', 'FLF','New field in TM_PANELS');



ALTER TABLE TM_PANELS
 ADD (
    DEVICECLASS  NUMBER(10));

	
COMMIT ;

PROMPT Fertig!

SPOOL off
