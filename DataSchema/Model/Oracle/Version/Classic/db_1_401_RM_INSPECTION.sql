
SPOOL db_1_401.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.401', 'ULB','Added new columns to inspection');

 
 
alter table RM_INSPECTION add Latitude number(10);

alter table RM_INSPECTION add Longitude number(10);

 


 
COMMIT;

PROMPT Fertig!

SPOOL off