SPOOL db_1_353.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.353', 'JGI','new columns in SEPA_MANDATES');

ALTER TABLE SEPA_MANDATES ADD (FIRSTUSED         DATE,
                               LASTUSED          DATE,
                               ISVALID           NUMBER(1));

COMMIT;

PROMPT Fertig!

SPOOL off

