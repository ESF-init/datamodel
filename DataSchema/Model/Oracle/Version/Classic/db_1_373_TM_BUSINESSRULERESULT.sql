SPOOL db_1_373.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.373', 'FLF','Added TRANSFERTIME to TM_BUSINESSRULERESULT');



ALTER TABLE TM_BUSINESSRULERESULT ADD  TRANSFERTIME             NUMBER(18)           DEFAULT 0                     NOT NULL;

 
COMMIT;

PROMPT Fertig!

SPOOL off