SPOOL db_1_314.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.314', 'FLF','Added ShowInGuiXml to attribute');

 ALTER TABLE TM_ATTRIBUTE ADD   SHOWINGUIXML       NUMBER(1)                  DEFAULT 0                     NOT NULL;

COMMIT;

PROMPT Fertig!

SPOOL off