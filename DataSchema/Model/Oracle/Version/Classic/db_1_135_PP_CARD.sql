SPOOL db_1_135.log

INSERT INTO vario_dmodellversion
            (rundate, dmodellno, responsible,
             annotation
            )
     VALUES (TO_DATE ('16.08.2010', 'DD.MM.YYYY'), '1.135', 'HSS',
             'New field and trigger in PP_CARD'
            );



ALTER TABLE PP_CARD
 ADD (
    LASTUSEDDATE            DATE
	);

prompt CREATE TRIGGER CARDLASTUSED
CREATE OR REPLACE TRIGGER CARDLASTUSED
BEFORE UPDATE
OF CARDBALANCE
ON PP_CARD
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
BEGIN
  :New.lastuseddate := sysdate;
END;
/

COMMIT ;

PROMPT Fertig!

SPOOL off
