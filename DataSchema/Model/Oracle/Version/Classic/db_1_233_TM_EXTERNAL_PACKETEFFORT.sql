SPOOL db_1_233.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('06.12.2011', 'DD.MM.YYYY'), '1.233', 'FLF','TM_EXTERNAL_PACKETEFFORT');


ALTER TABLE TM_EXTERNAL_PACKETEFFORT
 ADD (    ISDEFAULT    NUMBER(1));
 
 
 
COMMIT;

PROMPT Fertig!

SPOOL off
