SPOOL db_1_355.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.355', 'JGI','Added IBAN and BIC to VARIO_CLIENT');

ALTER TABLE VARIO_CLIENT
 ADD (IBAN  VARCHAR2(50));

ALTER TABLE VARIO_CLIENT
 ADD (BIC  VARCHAR2(50));

COMMIT;

PROMPT Fertig!

SPOOL off