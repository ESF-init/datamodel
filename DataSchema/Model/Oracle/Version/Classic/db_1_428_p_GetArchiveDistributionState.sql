SPOOL db_1_428.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.428', 'JUN','Changed procedure P_GETARCHIVEDISTRIBUTIONSTATE');

CREATE OR REPLACE PROCEDURE P_GETARCHIVEDISTRIBUTIONSTATE(
    jobid IN NUMBER,
    jobstate OUT NUMBER )
IS
  completed        NUMBER (1, 0);
  countExpected NUMBER;
  countLoaded        NUMBER;
  starttime DATE;
  active              NUMBER (1, 0);
  client              NUMBER;
  typeofarchivejob    NUMBER;
  archive_id          NUMBER;

BEGIN


  SELECT completed,
    starttime,
    active,
    typeofarchivejobid,
    clientid,
    archiveid
  INTO completed,
    starttime,
    active,
    typeofarchivejob,
    client,
    archive_id
  FROM um_archivejob
  WHERE archivejobid  = jobid;
  
  IF active      = 0 THEN
      IF starttime > SYSDATE THEN
        jobstate  := 0;
        RETURN;
      ELSE
        jobstate := 3;
        RETURN;
      END IF;
    END IF;
    IF completed <> 0 THEN
      jobstate   := 2;
      RETURN;
    END IF;
  
  IF typeofarchivejob = 3 THEN
  
    SELECT COUNT(distinct u.unitid)
    INTO countExpected
    FROM um_unitdistributiontask u
    INNER JOIN um_archivedistributiontask a
    ON u.archivedistributiontaskid = a.archivedistributiontaskid
    WHERE a.archivejobid =jobid;
      
    SELECT COUNT(distinct u.unitid)
    INTO countLoaded
    FROM um_unitdistributiontask u
    INNER JOIN um_archivedistributiontask a
    ON u.archivedistributiontaskid = a.archivedistributiontaskid
    WHERE a.archivejobid =jobid
    AND completed = 1 ;
    
  ELSE
    
    SELECT COUNT(dev.deviceid)
    INTO countExpected
    FROM um_device dev,
      um_unit unit
    WHERE dev.deviceclassid = -1
    AND unit.unitid         = dev.unitid
    AND unit.typeofunitid   = 2
    AND (unit.clientid      = client
    OR unit.clientid        =0);

    SELECT COUNT(l.deviceid)
    INTO countLoaded
    FROM um_unit u
    INNER JOIN um_device d
    ON u.unitid         = d.unitid
    AND d.deviceclassid = -1
    AND u.typeofunitid  = 2
    AND (u.clientid     = client
    OR u.clientid       = 0)
    LEFT OUTER JOIN um_loadingstatistics l
    ON d.deviceid       = l.deviceid
    AND l.archiveid     = archive_id
    AND L.LOADEDUNTIL  IS NOT NULL
    AND LSSTATE        IN (0,1,9);
    
  END IF;
  
  IF countExpected = countLoaded THEN
      jobstate   := 2;
    ELSE
      IF countLoaded = 0 THEN
        jobstate            := 3;
      ELSE
        jobstate :=1;
      END IF;
    END IF;
  
END P_GETARCHIVEDISTRIBUTIONSTATE;
/

COMMIT;


PROMPT Fertig!

SPOOL off