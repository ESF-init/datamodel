SPOOL db_1_322.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.322', 'MTW','Updated DB-Views');

 CREATE OR REPLACE FORCE VIEW "VIEW_UMDEVICE_CLIENT" ("DEVICEID", "UNITID", "MOUNTINGPLATE", "DEVICECLASSID", "DEVICENO", "LASTDATA", "NAME", "CLIENTID", "STATE", "INVENTORYNUMBER", "LOCKSTATE", "REQUESTEDLOCKSTATE", "DESCRIPTION", "EXTERNALDEVICENO")
AS
  SELECT um_device.deviceid,
    um_device.unitid,
    um_device.mountingplate,
    um_device.deviceclassid,
    um_device.deviceno,
    um_device.lastdata,
    NVL (um_device.NAME, '') AS NAME,
    um_unit.clientid,
    um_device.state,
    um_device.inventorynumber,
    um_device.lockstate,
    um_device.requestedlockstate,
    um_device.DESCRIPTION,
    um_device.EXTERNALDEVICENO
  FROM um_device
  LEFT OUTER JOIN um_unit
  ON um_device.unitid = um_unit.unitid;
  
CREATE OR REPLACE VIEW VIEW_LOADINGSTAT_OVERVIEW AS
SELECT UM_UNIT.unitid,
  view_loadingstatistic_archive.loadingstatisticsid,
  view_loadingstatistic_archive.deviceid,
  view_loadingstatistic_archive.deviceclassid,
  view_loadingstatistic_archive.archiveid,
  view_loadingstatistic_archive.loadedfrom,
  view_loadingstatistic_archive.loadeduntil,
  view_loadingstatistic_archive.typeofarchiveid,
  view_loadingstatistic_archive.filename,
  view_loadingstatistic_archive.filedate,
  view_loadingstatistic_archive.VERSION,
  view_loadingstatistic_archive.lsstate,
  UM_UNIT.clientid,
  um_device.externaldeviceno
FROM UM_DEVICE
INNER JOIN UM_UNIT
ON UM_DEVICE.unitid = UM_UNIT.unitid
RIGHT OUTER JOIN view_loadingstatistic_archive
ON UM_DEVICE.deviceid = view_loadingstatistic_archive.deviceid;

CREATE OR REPLACE VIEW VIEW_LOADINGSTATERROR_OVERVIEW AS
SELECT UM_LOADINGSTATISTICSERROR.loadingstatisticserrorid,
  UM_LOADINGSTATISTICSERROR.errorid,
  UM_LOADINGSTATISTICSERROR.deviceid,
  UM_DEVICE.deviceclassid,
  UM_DEVICE.unitid,
  UM_LOADINGSTATISTICSERROR.archiveid,
  UM_ARCHIVE.filename,
  UM_ARCHIVE.filedate,
  UM_ARCHIVE.VERSION,
  UM_ARCHIVE.typeofarchiveid,
  um_archivedistributiontask.started,
  um_unit.clientid,
  um_device.externaldeviceno
FROM UM_LOADINGSTATISTICSERROR
LEFT OUTER JOIN UM_DEVICE
ON UM_LOADINGSTATISTICSERROR.deviceid = UM_DEVICE.deviceid
LEFT OUTER JOIN UM_ARCHIVE
ON UM_LOADINGSTATISTICSERROR.archiveid = UM_ARCHIVE.archiveid
LEFT OUTER JOIN um_lserrortotask
ON UM_LOADINGSTATISTICSERROR.LOADINGSTATISTICSERRORID = um_lserrortotask.loadingstatisticserrorid
LEFT OUTER JOIN um_archivedistributiontask
ON um_lserrortotask.archivedistributiontaskid = um_archivedistributiontask.archivedistributiontaskid
LEFT OUTER JOIN um_unit
ON um_device.unitid=um_unit.unitid;

COMMIT;

PROMPT Fertig!

SPOOL off