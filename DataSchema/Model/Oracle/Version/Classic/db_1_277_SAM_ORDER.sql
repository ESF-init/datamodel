SPOOL db_1_277.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('15.08.2012', 'DD.MM.YYYY'), '1.277', 'KKV','Added missing attribute to SAM_ORDERER');

ALTER TABLE SAM_ORDER
ADD (SUCCESSFULPRINTCOUNTER NUMBER(10));

COMMIT;

PROMPT Fertig! 

SPOOL off