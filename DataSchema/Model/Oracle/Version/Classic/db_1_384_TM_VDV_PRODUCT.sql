SPOOL db_1_384.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.384', 'FLF','Added columns to table tm_vdv_product');


ALTER TABLE TM_VDV_PRODUCT
 ADD (PERIODID  NUMBER(10)	DEFAULT 0	NOT NULL);


 
COMMIT;

PROMPT Fertig!

SPOOL off