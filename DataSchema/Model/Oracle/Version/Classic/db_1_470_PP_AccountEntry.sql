SPOOL db_1_470.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.470', 'DST','Modified EntryTypeNo to long');

ALTER TABLE  PP_AccountEntry MODIFY 
(
  EntryTypeNo    NUMBER(10)
);


COMMIT;

PROMPT Fertig!

SPOOL off
