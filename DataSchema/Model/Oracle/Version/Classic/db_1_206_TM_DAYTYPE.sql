SPOOL db_1_206.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('05.07.2011', 'DD.MM.YYYY'), '1.206', 'FLF','TM_Daytype');


ALTER TABLE TM_DAYTYPE
 ADD (  BEGIN3     DATE);
 
 ALTER TABLE TM_DAYTYPE
 ADD (  END3       DATE);
 



PROMPT Fertig!

SPOOL off
