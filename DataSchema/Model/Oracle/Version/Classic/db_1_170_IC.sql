SPOOL db_1_170.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('12.01.2011', 'DD.MM.YYYY'), '1.170', 'MTW','VIEW_LOADINGSTATERROR_OVERVIEW enhanced');

CREATE OR REPLACE FORCE VIEW "VIEW_LOADINGSTATERROR_OVERVIEW" ("LOADINGSTATISTICSERRORID", "ERRORID", "DEVICEID", "DEVICECLASSID", "UNITID", "ARCHIVEID", "FILENAME", "FILEDATE", "VERSION", "TYPEOFARCHIVEID", "STARTED", "CLIENTID")
AS
  SELECT UM_LOADINGSTATISTICSERROR.loadingstatisticserrorid,
    UM_LOADINGSTATISTICSERROR.errorid,
    UM_LOADINGSTATISTICSERROR.deviceid,
    UM_DEVICE.deviceclassid,
    UM_DEVICE.unitid,
    UM_LOADINGSTATISTICSERROR.archiveid,
    UM_ARCHIVE.filename,
    UM_ARCHIVE.filedate,
    UM_ARCHIVE.VERSION,
    UM_ARCHIVE.typeofarchiveid,
    um_archivedistributiontask.started,
    um_unit.clientid
  FROM UM_LOADINGSTATISTICSERROR
  LEFT OUTER JOIN UM_DEVICE
  ON UM_LOADINGSTATISTICSERROR.deviceid = UM_DEVICE.deviceid
  LEFT OUTER JOIN UM_ARCHIVE
  ON UM_LOADINGSTATISTICSERROR.archiveid = UM_ARCHIVE.archiveid
  LEFT OUTER JOIN um_lserrortotask
  ON UM_LOADINGSTATISTICSERROR.LOADINGSTATISTICSERRORID = um_lserrortotask.loadingstatisticserrorid
  LEFT OUTER JOIN um_archivedistributiontask
  ON um_lserrortotask.archivedistributiontaskid = um_archivedistributiontask.archivedistributiontaskid
  LEFT OUTER JOIN um_unit
  ON um_device.unitid=um_unit.unitid; 
 
COMMIT ;

PROMPT Fertig!

SPOOL off
