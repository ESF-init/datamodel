spool db_1_084.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '26.10.2009', 'DD.MM.YYYY'), '1.084', 'ULB', 'Neue Tabelle dm_cardclearinghistory');

--**************************************

CREATE TABLE dm_cardclearinghistory
(
  clearingid        NUMBER(10),
  clearingdatetime  DATE,
  machinename       VARCHAR2(100 BYTE),
  debtorno          NUMBER(10),
  cardno            NUMBER(10),
  amount            NUMBER(10),
  logofferrorcount   NUMBER(4),
  cleared           NUMBER(1),
  cardtan           NUMBER(10)
);


ALTER TABLE dm_cardclearinghistory ADD (
  PRIMARY KEY
 (clearingid)
    USING INDEX );



CREATE OR REPLACE VIEW view_dm_cardclearinghistory
AS
   SELECT clearingid, clearingdatetime, machinename,
          dm_cardclearinghistory.debtorno, dm_cardclearinghistory.cardno,
          amount, logofferrorcount, cleared, cardtan, vario_depot.depotid,
          dm_debtor.NAME || ', ' || dm_debtor.firstname AS NAME
     FROM dm_cardclearinghistory, dm_card, dm_debtor, vario_depot
    WHERE dm_cardclearinghistory.cardno = dm_card.cardno
      AND dm_cardclearinghistory.debtorno = dm_debtor.debtorno
      AND dm_debtor.depotid = vario_depot.depotid;



--*****************************************

COMMIT;

PROMPT Fertig!

spool off





