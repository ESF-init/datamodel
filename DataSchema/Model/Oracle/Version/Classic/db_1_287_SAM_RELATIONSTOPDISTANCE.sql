SPOOL db_1_287.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('24.09.2012', 'DD.MM.YYYY'), '1.287', 'KKV','Added table SAM_RELATIONSTOPDISTANCE');

CREATE TABLE SAM_RELATIONSTOPDISTANCE
(
  ID           NUMBER(10)                       NOT NULL,
  RELATIONID   NUMBER(10)                       NOT NULL,
  FROMSTOPNO   NUMBER(22,10)                    NOT NULL,
  TOSTOPNO     NUMBER(22,10)                    NOT NULL,
  AVDISTANCE   NUMBER(10,3)                     NOT NULL,
  MAXDISTANCE  NUMBER(10,3)                     NOT NULL,
  CLIENTID     NUMBER(10)                       NOT NULL
);

ALTER TABLE SAM_RELATIONSTOPDISTANCE ADD (
  CONSTRAINT PK_SAM_RELATIONSTOPDISTANCE
 PRIMARY KEY
 (ID)); 

COMMIT;

PROMPT Fertig!

SPOOL off