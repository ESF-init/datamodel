SPOOL db_1_340.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.340', 'FLF','Make internal number not nullable');

ALTER TABLE TM_TICKET MODIFY(INTERNALNUMBER  NOT NULL);
 
COMMIT;

PROMPT Fertig!

SPOOL off