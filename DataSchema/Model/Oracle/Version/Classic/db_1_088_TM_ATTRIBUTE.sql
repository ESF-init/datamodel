spool db_1_088.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '09.11.2009', 'DD.MM.YYYY'), '1.088', 'HSS', 'Attribute durch Visible Feld erweitert.');

--**************************************

ALTER TABLE TM_ATTRIBUTE ADD   (VISIBLE NUMBER(1));

--*****************************************

COMMIT;

PROMPT Fertig!

spool off
