SPOOL db_1_379.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.379', 'FLF','Added columns to table tm_vdv_product');


ALTER TABLE TM_VDV_PRODUCT
 ADD (VDVINFOTEXT  VARCHAR2(1000));

 


 
COMMIT;

PROMPT Fertig!

SPOOL off