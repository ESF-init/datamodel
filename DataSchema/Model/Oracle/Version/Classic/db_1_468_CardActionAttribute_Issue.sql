SPOOL db_1_468.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.468', 'UB','Added Issueflag to PP_CARDACTIONATTRIBUTE');

ALTER TABLE  PP_CARDACTIONATTRIBUTE ADD 
(
  ISSUEFLAG    NUMBER(1)
);


COMMIT;

PROMPT Fertig!

SPOOL off