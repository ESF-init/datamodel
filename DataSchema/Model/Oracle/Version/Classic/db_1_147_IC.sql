SPOOL db_1_147.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('07.10.2010', 'DD.MM.YYYY'), '1.147', 'MTW','Vehicle mail support');

ALTER TABLE UM_UNIT
 ADD (NOTIFICATIONADDRESS  VARCHAR2(50));
 
 ALTER TABLE VARIO_DEPOT
 ADD (NOTIFICATIONADDRESS  VARCHAR2(50));
	
COMMIT ;

PROMPT Fertig!

SPOOL off
