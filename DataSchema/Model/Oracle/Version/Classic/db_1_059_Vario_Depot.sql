spool db_1_059.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '17.06.2009', 'DD.MM.YYYY'), '1.059', 'MSC', 'Erweiterung VARIO_DEPOT');

ALTER TABLE VARIO_DEPOT MODIFY(DEPOTNO	DEFAULT 0);
ALTER TABLE VARIO_DEPOT MODIFY(CLIENTID	DEFAULT 0);

ALTER TABLE VARIO_DEPOT ADD(CASHID	NUMBER(10)	DEFAULT 0);
ALTER TABLE VARIO_DEPOT ADD(ADDRESSID	NUMBER(10)	DEFAULT 0);
ALTER TABLE VARIO_DEPOT ADD(ACCOUNTID	NUMBER(10)	DEFAULT 0);

commit;

PROMPT Fertig!

spool off