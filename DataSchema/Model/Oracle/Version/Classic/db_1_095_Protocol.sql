spool db_1_095.log

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '11.01.2010', 'DD.MM.YYYY'), '1.095', 'MSC', 'Spalte ProtUserName in Tabelle Protocol vergroessert');

--**************************************
ALTER TABLE PROTOCOL MODIFY(PROTUSERNAME VARCHAR2(50 BYTE));

--*****************************************

COMMIT;

PROMPT Fertig!

spool off
