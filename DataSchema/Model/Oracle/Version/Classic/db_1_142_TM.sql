SPOOL db_1_142.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('14.09.2010', 'DD.MM.YYYY'), '1.142', 'FLF','New field in TM_TICKET');



ALTER TABLE tm_ticket
 ADD (
    FORMULARNAME  VARCHAR2(200 Byte));



COMMIT ;

PROMPT Fertig!

SPOOL off
