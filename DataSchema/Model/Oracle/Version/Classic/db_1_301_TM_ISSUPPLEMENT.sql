SPOOL db_1_301.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('28.11.2012', 'DD.MM.YYYY'), '1.301', 'FLF','TM Supplement');

ALTER TABLE TM_TICKET ADD  ISSUPPLEMENT               NUMBER(1)          DEFAULT 0                     NOT NULL;
 
COMMIT;

PROMPT Fertig!

SPOOL off