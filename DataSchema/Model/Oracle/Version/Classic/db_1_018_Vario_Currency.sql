
spool db_1_018.log

PROMPT �ndern Tabelle VARIO_Currency

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '10.12.2008', 'DD.MM.YYYY'), '1.018', 'MSC', 'VARIO_Currency zus�tzliches Attribut');

-- *************************

ALTER TABLE VARIO_Currency
 ADD (CURRENCYNO         NUMBER(10)                 NOT NULL);

-- *************************
commit;

PROMPT Fertig!

spool off