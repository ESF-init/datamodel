SPOOL db_1_443.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.443', 'FLF','Added matrix to tm_ticket_deviceclass');

ALTER TABLE TM_TICKET_DEVICECLASS
 ADD (MATRIXID  NUMBER(18));
 

COMMIT;

PROMPT Fertig!

SPOOL off