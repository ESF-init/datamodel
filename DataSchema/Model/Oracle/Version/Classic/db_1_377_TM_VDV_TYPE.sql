SPOOL db_1_377.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.377', 'FLF','Added table tm_vdv_type');

CREATE TABLE TM_VDV_TYPE
(
  TYPEID       NUMBER(18)                       NOT NULL,
  TYPENAME     VARCHAR2(200 BYTE)               NOT NULL,
  DESCRIPTION  VARCHAR2(400 BYTE),
  TYPEGROUPID  NUMBER(18)                       NOT NULL,
  TYPENUMBER   NUMBER(18)                       NOT NULL
);


CREATE UNIQUE INDEX TM_VDV_TYPE_PK ON TM_VDV_TYPE
(TYPEID);


ALTER TABLE TM_VDV_TYPE ADD (
  CONSTRAINT TM_VDV_TYPE_PK
 PRIMARY KEY
 (TYPEID));


 
COMMIT;

PROMPT Fertig!

SPOOL off