SPOOL db_1_285.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('06.09.2012', 'DD.MM.YYYY'), '1.285', 'KKV','Added externalbillno to pp_bill');

ALTER TABLE PP_BILL
ADD (EXTERNALBILLNO NUMBER(10));

COMMIT;

PROMPT Fertig! 

SPOOL off