SPOOL db_1_476.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.476', 'ULB','Added fields to RM_INSPECTION.');

ALTER TABLE RM_INSPECTION
ADD (vehicleno NUMBER(10));

ALTER TABLE RM_INSPECTION
ADD (stopno NUMBER(10));

ALTER TABLE RM_INSPECTION
ADD (lineno NUMBER(10));

ALTER TABLE RM_INSPECTION
ADD (serviceid NUMBER(10));

ALTER TABLE RM_INSPECTION
ADD (creationtime Date);

ALTER TABLE RM_INSPECTION
ADD (barcode varchar2(13));


ALTER TABLE RM_INSPECTION
ADD (tmp varchar2(20));

update rm_inspection set tmP=cardno;

alter table rm_inspection drop column cardno;

alter table rm_inspection rename column tmp to cardno;



COMMIT;

PROMPT Fertig!

SPOOL off