SPOOL db_1_194.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
     VALUES (TO_DATE ('18.04.2011', 'DD.MM.YYYY'), '1.194', 'RHO','Tables for Vario systemMessenger');

CREATE TABLE VARIO_SYSTEMMESSAGE
(
  ID              NUMBER(25),
  MODUL           VARCHAR2(25 BYTE)             NOT NULL,
  MODULVERSION    VARCHAR2(15 BYTE)             NOT NULL,
  FROMDATE        DATE                          NOT NULL,
  TODATE          DATE                          NOT NULL,
  PREVENTAPP      NUMBER(1)                     NOT NULL,
  MESSAGETEXT     VARCHAR2(200 BYTE)            NOT NULL,
  SENDER          VARCHAR2(40 BYTE)             NOT NULL,
  CLIENTID        NUMBER,
  PRIVILEGEDUSER  NUMBER(10),
  MESSAGECLASS    NUMBER(2)                     NOT NULL,
  ALLCLIENTS      NUMBER(1)
);


CREATE TABLE VARIO_SYSTEMMESSAGECLASS
(
  CLASSNUMBER  NUMBER,
  NAME         VARCHAR2(30 BYTE)
);


ALTER TABLE VARIO_SYSTEMMESSAGECLASS ADD (PRIMARY KEY (CLASSNUMBER));

SET DEFINE OFF;
Insert into VARIO_SYSTEMMESSAGECLASS
   (CLASSNUMBER, NAME)
 Values
   (2, 'Warning');
Insert into VARIO_SYSTEMMESSAGECLASS
   (CLASSNUMBER, NAME)
 Values
   (4, 'Exclamation');
Insert into VARIO_SYSTEMMESSAGECLASS
   (CLASSNUMBER, NAME)
 Values
   (5, 'Hand');
Insert into VARIO_SYSTEMMESSAGECLASS
   (CLASSNUMBER, NAME)
 Values
   (3, 'Information');
Insert into VARIO_SYSTEMMESSAGECLASS
   (CLASSNUMBER, NAME)
 Values
   (1, 'Error');

COMMIT;

PROMPT Fertig!

SPOOL off
