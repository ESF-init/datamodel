SPOOL db_1_139.log

INSERT INTO vario_dmodellversion
            (rundate, dmodellno, responsible,
             annotation
            )
     VALUES (TO_DATE ('08.09.2010', 'DD.MM.YYYY'), '1.139', 'FLF',
             'New Table tm_shortdistance'
            );


CREATE TABLE TM_SHORTDISTANCE
(
  SHORTDISTANCEID  NUMBER(10),
  BOARDING         NUMBER(10),
  DESTINATION      NUMBER(10),
  TARIFFID         NUMBER(10)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE TM_SHORTDISTANCE ADD (
  PRIMARY KEY
 (SHORTDISTANCEID));



COMMIT ;

PROMPT Fertig!

SPOOL off
