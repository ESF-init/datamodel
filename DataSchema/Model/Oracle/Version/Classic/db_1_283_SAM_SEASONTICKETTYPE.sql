SPOOL db_1_283.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (TO_DATE ('31.08.2012', 'DD.MM.YYYY'), '1.283', 'BTS','Added missing attribute to SAM_SEASONTICKETTYPE');

ALTER TABLE SAM_SEASONTICKETTYPE
ADD (ISTRANSFERABLE NUMBER(1));

COMMIT;

PROMPT Fertig! 

SPOOL off