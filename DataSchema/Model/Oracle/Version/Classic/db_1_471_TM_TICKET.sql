SPOOL db_1_471.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.471', 'FLF','Added ordernumber to tm_ticket');

ALTER TABLE TM_TICKET
 ADD (ORDERNUMBER  NUMBER(10)                       DEFAULT 1                     NOT NULL);


COMMIT;

PROMPT Fertig!

SPOOL off