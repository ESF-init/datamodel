SPOOL db_1_153.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('22.10.2010', 'DD.MM.YYYY'), '1.153', 'MKK','VIEW_LOADINGSTAT_OVERVIEW column UM_UNIT.unitid added.');

CREATE OR REPLACE FORCE VIEW "VIEW_LOADINGSTAT_OVERVIEW" ("UNITID", "LOADINGSTATISTICSID", "DEVICEID", "DEVICECLASSID", "ARCHIVEID", "LOADEDFROM", "LOADEDUNTIL", "TYPEOFARCHIVEID", "FILENAME", "FILEDATE", "VERSION", "LSSTATE", "CLIENTID")
AS
  SELECT UM_UNIT.unitid,
    view_loadingstatistic_archive.loadingstatisticsid,
    view_loadingstatistic_archive.deviceid,
    view_loadingstatistic_archive.deviceclassid,
    view_loadingstatistic_archive.archiveid,
    view_loadingstatistic_archive.loadedfrom,
    view_loadingstatistic_archive.loadeduntil,
    view_loadingstatistic_archive.typeofarchiveid,
    view_loadingstatistic_archive.filename,
    view_loadingstatistic_archive.filedate,
    view_loadingstatistic_archive.VERSION,
    view_loadingstatistic_archive.lsstate,
	UM_UNIT.clientid
  FROM UM_DEVICE
  INNER JOIN UM_UNIT
  ON UM_DEVICE.unitid = UM_UNIT.unitid
  RIGHT OUTER JOIN view_loadingstatistic_archive
  ON UM_DEVICE.deviceid = view_loadingstatistic_archive.deviceid;
  
  COMMIT ;

PROMPT Fertig!

SPOOL off