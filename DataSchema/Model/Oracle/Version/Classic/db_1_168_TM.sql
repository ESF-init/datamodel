SPOOL db_1_168.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('29.12.2010', 'DD.MM.YYYY'), '1.168', 'FLF','TM_FARETABLEENTRY');


ALTER TABLE TM_FARETABLEENTRY
 ADD ( VALIDITYTEXT      VARCHAR2(100 BYTE));

COMMIT ;

PROMPT Fertig!

SPOOL off
