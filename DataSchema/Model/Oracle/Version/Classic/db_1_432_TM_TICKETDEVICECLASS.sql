SPOOL db_1_432.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.432', 'FLF','Added price to tm_ticket_deviceclass');

ALTER TABLE TM_TICKET_DEVICECLASS
 ADD (PRICE  NUMBER(9));

 
--UPDATE tm_ticket_deviceclass tdc
 --  SET tdc.price = (SELECT price
 --                    FROM tm_ticket tic
 --                    WHERE tic.ticketid = tdc.ticketid);
 

COMMIT;

PROMPT Fertig!

SPOOL off