SPOOL db_1_319.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.319', 'FLF','Added LineNo to TM_BUSINESSRULERESULT');

 ALTER TABLE TM_BUSINESSRULERESULT ADD   LINENO                   NUMBER(18);

COMMIT;

PROMPT Fertig!

SPOOL off