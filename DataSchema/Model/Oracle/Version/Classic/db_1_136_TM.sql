SPOOL db_1_136.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('17.08.2010', 'DD.MM.YYYY'), '1.136', 'FLF','New field in TM_TARIF');



ALTER TABLE tm_tarif
 ADD (
    BASETARIFFID  NUMBER(10));



COMMIT ;

PROMPT Fertig!

SPOOL off
