SPOOL db_1_185.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('23.03.2011', 'DD.MM.YYYY'), '1.185', 'ULS','Relation.Depot');

ALTER TABLE SAM_RELATION
 ADD (DEPOTID NUMBER(10));

COMMIT;

PROMPT Fertig!

SPOOL off
