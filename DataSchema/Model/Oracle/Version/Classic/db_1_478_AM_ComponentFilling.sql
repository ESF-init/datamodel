SPOOL db_1_478.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '1.478', 'BVI','Added two new columns to AM_ComponentFilling.');

Alter Table AM_ComponentFilling
add(
    ISBOOKED Number(1) Default 0 NOT NULL,
    BOOKINGNO NUMBER(10) Default 0 NOT NULL
);

PROMPT Fertig!

SPOOL off