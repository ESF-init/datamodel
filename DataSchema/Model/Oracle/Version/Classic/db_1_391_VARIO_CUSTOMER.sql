/* Formatted on 2014/06/13 14:54 (Formatter Plus v4.8.8) */
SPOOL db_1_391.log

INSERT INTO vario_dmodellversion
            (rundate, dmodellno, responsible,
             annotation
            )
     VALUES (SYSDATE, '1.391', 'ULS',
             'New table vario_customer to integrate vario_custom and sam_orderer'
            );


CREATE TABLE vario_customer
(
  ordererid          NUMBER(10),
  NAME               VARCHAR2(200 BYTE),
  number_            NUMBER(10),
  contactperson      VARCHAR2(50 BYTE),
  phone              VARCHAR2(20 BYTE),
  email              VARCHAR2(50 BYTE),
  fax                VARCHAR2(20 BYTE),
  clientid           NUMBER(10),
  postalcode         VARCHAR2(10 BYTE),
  city               VARCHAR2(100 BYTE),
  street             VARCHAR2(100 BYTE),
  bankaccountnumber  NUMBER(20),
  banknumber         NUMBER(20),
  bankname           VARCHAR2(100 BYTE),
  debitor            NUMBER(20),
  exportstate        NUMBER(10),
  usebankcollection  NUMBER(1),
  communityid        NUMBER(10),
  iban               VARCHAR2(34 BYTE),
  bic                VARCHAR2(11 BYTE),
  SOURCE             NUMBER(1)
);

CREATE UNIQUE INDEX vario_customer_pk ON vario_customer
(ordererid)
NOLOGGING
NOPARALLEL;

ALTER TABLE vario_customer ADD (
  CONSTRAINT vario_customer_pk
 PRIMARY KEY
 (ordererid));



COMMIT ;

PROMPT Fertig!

SPOOL off