
spool db_1_024.log

PROMPT �ndern Tabelle VARIO_STOP 

INSERT INTO VARIO_DMODELLVERSION ( RUNDATE, DMODELLNO, RESPONSIBLE, ANNOTATION ) VALUES ( 
 TO_Date( '12.12.2008', 'DD.MM.YYYY'), '1.024', 'JGi', 'Neues Attribut REPLACECARDNO');

-- *************************

ALTER TABLE PP_CARD 
 ADD (REPLACECARDNO    NUMBER(10));

ALTER TABLE DM_CARD
 ADD (REPLACECARDNO    NUMBER(10));

-- *************************
commit;

PROMPT Fertig!

spool off