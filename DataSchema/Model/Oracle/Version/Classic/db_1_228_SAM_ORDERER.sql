SPOOL db_1_228.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
     VALUES (TO_DATE ('28.10.2011', 'DD.MM.YYYY'), '1.228', 'KKV','Add USEBANKCOLLECTION to SAM_ORDERER');

ALTER TABLE SAM_ORDERER
	ADD (USEBANKCOLLECTION NUMBER(1));

COMMIT;

PROMPT Fertig!

SPOOL off
