SPOOL db_3_449.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.449', 'bts', 'Create SL_TERMINATIONSTATUS');

-- Start adding schema changes here


-- =======[ New Tables ]===========================================================================================
CREATE TABLE SL_TERMINATIONSTATUS
(
    ENUMERATIONVALUE NUMBER(9)     not null
        constraint PK_TERMINATIONSTATUS
            primary key,
    LITERAL          NVARCHAR2(50) not null,
    DESCRIPTION      NVARCHAR2(50) not null
);


COMMENT ON TABLE SL_TERMINATIONSTATUS is 'Enumeration of termination status, known to the system.';
COMMENT ON COLUMN SL_TERMINATIONSTATUS.ENUMERATIONVALUE is 'Unique ID of the termination status';
COMMENT ON COLUMN SL_TERMINATIONSTATUS.LITERAL is 'Name of the termination status; used internally.';
COMMENT ON COLUMN SL_TERMINATIONSTATUS.DESCRIPTION is 'Descripton of the source. This is used in the UI to display the value.';
---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
