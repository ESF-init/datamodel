SPOOL db_3_024.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.024', 'FMT', 'Added view_sl_studentcardprint view');

-- Start adding schema changes here


-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW view_sl_studentcardprint (cardholderid,
                                                       schoolyearname,
                                                       schoolyearid,
                                                       cardholderfirstname,
                                                       cardholderlastname,
                                                       cardholderdateofbirth,
                                                       cardholdergender,
                                                       schoolprintname,
                                                       schoolnumber,
                                                       orderernumber,
                                                       ordererprintname,
													   ordererorganizationid,
                                                       numberofcardprints,
                                                       cardid,
													   cardstate,
													   cardexpiration,
													   cardserialnumber,
                                                       productid,
                                                       ticketinternalnumber,
                                                       ticketname,
													   ticketid,
													   tariffid,
                                                       cardprintednumber,
                                                       startstreet,
                                                       startstreetnumber,
                                                       stopstreet,
                                                       stopstreetnumber,
                                                       relationfrom,
                                                       relationto,
                                                       farematrixentrypriority
                                                      )
AS
   SELECT ch.personid, sy.schoolyearname, sy.schoolyearid, ch.firstname,
          ch.lastname, ch.dateofbirth, ch.gender, schoolorg.printname,
          schoolorg.organizationnumber, ordererorg.organizationnumber,
          ordererorg.printname, ordererorg.organizationid, card.numberofprints, 
          card.cardid, CARD.STATE,CARD.EXPIRATION, card.serialNumber,
          prod.productid, tic.internalnumber, tic.NAME,tic.ticketid,tic.TARIFID , card.printednumber,
          addrstart.street, addrstart.streetnumber, addrstop.street,
          addrstop.streetnumber, rel.relationfrom, rel.relationto,
          rel.farematrixentrypriority
     FROM sl_person ch,
          sl_schoolyear sy,
          sl_contract schoolcnt,
          sl_organization schoolorg,
          sl_organization ordererorg,
          sl_card card,
          sl_product prod,
          tm_ticket tic,
          sl_productrelation rel,
          sl_address addrstart,
          sl_address addrstop
    WHERE ch.typediscriminator = 'CardHolder'
      AND ch.schoolyearid = sy.schoolyearid
      AND ch.contractid = schoolcnt.contractid
      AND schoolcnt.organizationid = schoolorg.organizationid
      AND schoolorg.frameorganizationid = ordererorg.organizationid
      AND card.cardholderid = ch.personid
      AND prod.cardid = card.cardid
      AND prod.tickettype <> 103
      AND prod.ticketid = tic.ticketid
      AND rel.productid = prod.productid
      AND addrstart.addressid = rel.fromaddressid
      AND addrstop.addressid = rel.toaddressid
      AND rel.relationfrom <> 0
      AND rel.relationto <> 0;
COMMIT ;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------
 
-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
