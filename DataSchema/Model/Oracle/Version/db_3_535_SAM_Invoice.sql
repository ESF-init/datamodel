SPOOL db_3_535.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.535', 'BVI', 'Changed datatype of SAM_Invoice.IssuerEmail.');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SAM_INVOICE 
 MODIFY (IssuerEmail  NVARCHAR2(50));

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
