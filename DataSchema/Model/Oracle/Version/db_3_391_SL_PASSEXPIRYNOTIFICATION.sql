SPOOL db_3_391.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.391', 'ARH', 'SL_PASSEXPIRYNOTIFICATION : Pass expiry notification.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================
CREATE TABLE SL_PASSEXPIRYNOTIFICATION
(
    PASSEXPIRYNOTIFICATIONID  	NUMBER(18,0) NOT NULL,
    PRODUCTID                  	NUMBER(18,0) NOT NULL,
    DAYS                  		NUMBER(10,0) NOT NULL,
    NOTIFICATIONDATE        	DATE NOT NULL,
	LastUser                	NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified            	DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter      	INTEGER NOT NULL,
	CONSTRAINT PK_PASSEXPIRYNOTIFICATION PRIMARY KEY (PASSEXPIRYNOTIFICATIONID),
	CONSTRAINT FK_PASSEXPIRYNOTIFICATION FOREIGN KEY (PRODUCTID) REFERENCES SL_PRODUCT(PRODUCTID)
);


COMMENT ON TABLE SL_PASSEXPIRYNOTIFICATION is 'Tracks when a pass got an expiration notification';
COMMENT ON COLUMN SL_PASSEXPIRYNOTIFICATION.PASSEXPIRYNOTIFICATIONID IS 'Primary key.';
COMMENT ON COLUMN SL_PASSEXPIRYNOTIFICATION.PRODUCTID IS 'ID of the product.';
COMMENT ON COLUMN SL_PASSEXPIRYNOTIFICATION.DAYS IS 'Number of days before expiration of the pass.';
COMMENT ON COLUMN SL_PASSEXPIRYNOTIFICATION.NOTIFICATIONDATE IS 'Date when the pass/card holder has been notified. Can be null.';

COMMENT ON COLUMN SL_PASSEXPIRYNOTIFICATION.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_PASSEXPIRYNOTIFICATION.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_PASSEXPIRYNOTIFICATION.LASTUSER IS 'Technical field: last (system) user that changed this dataset';


DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_PASSEXPIRYNOTIFICATION');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_PASSEXPIRYNOTIFICATION');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/


COMMIT;

PROMPT Done!

SPOOL OFF;
