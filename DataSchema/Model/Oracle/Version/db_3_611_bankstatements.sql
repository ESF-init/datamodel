SPOOL db_3_611.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.611', 'EPA', 'Added missing SL_BANKSTATEMENT enumeration values.');

Insert into SL_BANKSTATEMENTSTATE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values  (4, 'MultiMatch', 'More than one payment matches a bank statement.');
Insert into SL_BANKSTATEMENTSTATE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values  (5, 'InvalidPayment', 'Payment has no valid state on MOBILEvario side.');

COMMIT;

PROMPT Done!

SPOOL OFF;
