SPOOL db_3_131.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.131', 'jsb', 'UM_DEVICE.LASTDATA default value');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
alter table um_device modify lastdata default DATE'0001-01-01';

update um_device set lastdata = DATE'0001-01-01' where lastdata is null;

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
