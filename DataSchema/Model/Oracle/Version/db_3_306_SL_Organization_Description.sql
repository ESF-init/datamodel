SPOOL db_3_306.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '3.306', 'BVI','Set SL_Organization.Description to nullable.');


alter table sl_organization modify (Description null);


COMMIT;

PROMPT Done!

SPOOL OFF;