SPOOL db_3_478.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.478', 'STV', 'New_AuditRegsiter_ValueTypes');


-- -------[ Enumerations ]------------------------------------------------------------------------------------------

Insert into SL_AUDITREGISTERVALUETYPE
   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values
   (25, 'CardTransactionsCredit', 'Credit card transactions by amount');
Insert into SL_AUDITREGISTERVALUETYPE
   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values
   (26, 'CardTransactionsDebit', 'Debit card transactions by amount');


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
