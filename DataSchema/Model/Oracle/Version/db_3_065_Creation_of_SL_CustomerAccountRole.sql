SPOOL db_3_065.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.065', 'SOE', 'Creation of SL_CustomerAccountRoles ');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_CUSTOMERACCOUNTROLE
(
    CUSTOMERACCOUNTROLEID       NUMBER(18,0)    NOT NULL,
    NAME    NVARCHAR2(128)  NOT NULL,
    DESCRIPTION    NVARCHAR2(128)  NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_CUSTOMERACCOUNTROLE PRIMARY KEY (CUSTOMERACCOUNTROLEID)
);

alter table SL_CUSTOMERACCOUNT add CUSTOMERACCOUNTROLEID number(18,0);
alter table SL_CUSTOMERACCOUNT add constraint FK_CUSTOMERACCOUNTROLE foreign key (CUSTOMERACCOUNTROLEID) references SL_CUSTOMERACCOUNTROLE (CUSTOMERACCOUNTROLEID);

COMMENT ON TABLE SL_CUSTOMERACCOUNTROLE is 'Table containing look up information about customer account role';
COMMENT ON COLUMN SL_CUSTOMERACCOUNTROLE.CUSTOMERACCOUNTROLEID is 'Unique identifier of the customer account role';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(4) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_CUSTOMERACCOUNTROLE');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(6) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_CUSTOMERACCOUNTROLE');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/
-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
