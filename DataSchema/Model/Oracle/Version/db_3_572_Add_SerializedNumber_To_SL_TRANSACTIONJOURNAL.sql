SPOOL db_3_572.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.572', 'DIS', 'Add SerializedNumber to SL_TRANSACTIONJOURNAL');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

-- ALTER TABLE SL_TRANSACTIONJOURNAL ADD SERIALIZEDNUMBER NVARCHAR2(20) NULL;

-- COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.SERIALIZEDNUMBER IS 'Serialized Number needed in serialized products.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
