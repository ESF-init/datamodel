SPOOL db_3_009.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.009', 'MIW', 'Added description field to sl_address');

-- Start adding schema changes here

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_ADDRESS ADD (Description  NVARCHAR2(500));


---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
