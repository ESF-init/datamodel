SPOOL db_3_261.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.261', 'ULB', 'added PP_CARDACTIONREQUEST.Transactionid');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE PP_CARDACTIONREQUEST
ADD (Transactionid NUMBER(18));

COMMENT ON COLUMN 
PP_CARDACTIONREQUEST.Transactionid IS 
'References RM_Transaction.transactionid 0...1';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
