SPOOL db_3_533.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.533', 'HNI', 'Added Columns to CS_KVVSubscription');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE CS_KVVSUBSCRIPTION
 ADD (FAREZONEFROMDESCRIPTION  VARCHAR2(60));

 COMMENT ON COLUMN CS_KVVSUBSCRIPTION.FAREZONEFROMDESCRIPTION IS 'Description of the starting farezone';

ALTER TABLE CS_KVVSUBSCRIPTION
 ADD (FAREZONETODESCRIPTION  VARCHAR2(60));

 COMMENT ON COLUMN CS_KVVSUBSCRIPTION.FAREZONETODESCRIPTION IS 'Description of the ending farezone';

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
