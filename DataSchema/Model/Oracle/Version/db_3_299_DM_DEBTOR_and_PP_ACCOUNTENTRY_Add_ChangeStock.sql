SPOOL db_3_299.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.299', 'ES', 'Add column changeStock (Wechselgeldbestand) to dm_debtor' );

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE dm_debtor
ADD ( changeStock NUMBER(10) DEFAULT 0 NOT NULL );

COMMENT ON COLUMN dm_debtor.changeStock IS 'Change stock of the debtor (Wechselgeldbestand), as it results from account entries';

-- =======[ Procedures ]=============================================================================

CREATE OR REPLACE PROCEDURE changeStockSynchro ( pClient IN NUMBER )
IS
BEGIN
	UPDATE dm_debtor SET changeStock = 0 WHERE clientID = pClient;
		
	MERGE INTO dm_debtor dt
	USING (
	  SELECT contractID, sum(amount)*(-1) csa
  	    FROM pp_accountentry
	   WHERE entrytypeNo IN (174, 175, 274, 275)		
  	     AND contractID IN ( SELECT debtorID FROM dm_debtor WHERE clientID = pClient )
	   GROUP BY contractID
	   ORDER BY contractID
	  ) sub
	ON ( dt.debtorID = sub.contractID ) 
	WHEN MATCHED THEN UPDATE SET dt.changeStock = sub.csa
	;
END;
/

-- =======[ Trigger ]==============================================================================================

CREATE OR REPLACE TRIGGER TRG_RM_ACCENTRY_CHANGESTOCK
  AFTER INSERT ON pp_accountentry
  REFERENCING NEW AS new OLD AS old
  FOR EACH ROW
  BEGIN
  
    IF ( :new.entrytypeNo IN (174, 175, 274, 275) ) THEN
	  UPDATE dm_debtor SET changeStock = changeStock - :new.amount WHERE debtorID = :new.contractID ;
	END IF;  
	
  END TRG_RM_ACCENTRY_CHANGESTOCK;
  /

ALTER TRIGGER TRG_RM_ACCENTRY_CHANGESTOCK DISABLE;

PROMPT consider to enable the trigger TRG_RM_ACCENTRY_CHANGESTOCK and to run the procedure changeStockSynchro if change stock should be used.

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
