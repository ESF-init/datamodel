SPOOL db_3_294.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.294', 'JGI', 'Added column VISIBLE to UM_TYPEOFUNIT');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table UM_TYPEOFUNIT add VISIBLE NUMBER(10) DEFAULT 0;

comment on column UM_TYPEOFUNIT.VISIBLE is 'Filter to hide unit types from beeing displayed';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
