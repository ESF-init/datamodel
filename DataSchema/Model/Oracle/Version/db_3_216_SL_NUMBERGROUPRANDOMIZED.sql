SPOOL db_3_216.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.216', 'DST', 'Changes to SL_NUMBERGROUPRANDOMIZED.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_NUMBERGROUPRANDOMIZED RENAME COLUMN CARDNUMBER TO VALUE;

CREATE OR REPLACE PROCEDURE GET_SL_NUMBERGROUPRANDOMIZED(NUMBERGROUPSCOPE IN NUMBER, cardNumber OUT NUMBER)
IS
    l_SelectedfromRandom Integer;
BEGIN
    SELECT VALUE into l_SelectedfromRandom from (SELECT VALUE FROM SL_NUMBERGROUPRANDOMIZED WHERE ISUSED = 0 AND NUMBERGROUPSCOPE = NUMBERGROUPSCOPE ORDER BY RANDOMSORTNUMBER) where rownum = 1 FOR UPDATE;
    UPDATE SL_NUMBERGROUPRANDOMIZED SET ISUSED = 1 WHERE VALUE = l_SelectedfromRandom AND NUMBERGROUPSCOPE = NUMBERGROUPSCOPE;
    COMMIT;
    cardNumber := l_SelectedfromRandom;
END;
/
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
