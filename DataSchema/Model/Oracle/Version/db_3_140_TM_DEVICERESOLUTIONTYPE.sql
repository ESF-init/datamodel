SPOOL db_3_140.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.140', 'SLR', 'Added new enumeration for device resolutions.');

-- =======[ Changes to Existing Tables ]===========================================================================

create table TM_DEVICERESOLUTIONTYPE (
    DEVICERESOLUTIONTYPEID number(18),
    DESCRIPTION VARCHAR2(100),
    DEVICERESOLUTIONNUMBER number(9),
    DEVICERESOLUTIONVALUE number(9)
);

comment on table TM_DEVICERESOLUTIONTYPE is 'This table contains an enumeration of available device resolutions in system.';
comment on column TM_DEVICERESOLUTIONTYPE.DEVICERESOLUTIONTYPEID 
is 'Internal identifier.';
comment on column TM_DEVICERESOLUTIONTYPE.DESCRIPTION
is 'Description of the device resolution type shown in application.';
comment on column TM_DEVICERESOLUTIONTYPE.DEVICERESOLUTIONNUMBER
is 'Internal reference to enumeration value.';
comment on column TM_DEVICERESOLUTIONTYPE.DEVICERESOLUTIONVALUE
is 'Defines the resolution of device resolution type in dpi.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;