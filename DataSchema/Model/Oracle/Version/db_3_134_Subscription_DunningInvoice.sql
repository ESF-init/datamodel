SPOOL db_3_134.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.134', 'bvi', 'Added new columns to SL_Invoice, SL_PostingKey, SL_DunningProcess.');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_PostingKey
 ADD 
 (
	IsBillingRelevant  Number(1)
 );
COMMENT ON COLUMN SL_PostingKey.IsBillingRelevant IS 'Should this type of posting be included while creating an invoice for a contract.';

ALTER TABLE SL_Invoice
 ADD 
 (
	IsPaid  Number(1)
 );
COMMENT ON COLUMN SL_Invoice.IsPaid IS 'True if this invoice is paid.';

ALTER TABLE SL_DunningProcess
 ADD 
 (
	DunningInvoiceID  Number(18)
 );
COMMENT ON COLUMN SL_DunningProcess.DunningInvoiceID IS 'Reference to SL_Invoice.';
 
ALTER TABLE SL_DunningProcess
 ADD 
 (
	CONSTRAINT FK_DunningProcessToInvoice FOREIGN KEY (DunningInvoiceID) REFERENCES SL_Invoice(InvoiceID)
 );
 
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
