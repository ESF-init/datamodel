SPOOL db_3_421.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.421', 'flf', 'New columns in SL_TRANSACTIONJOURNAL');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (APPLIEDPASSPRODUCTID  NUMBER(18));

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (ORIGINALSINGLEFARE  NUMBER(9));

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (BLOCKNUMBER  NUMBER(9));

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (CORRECTEDLINE  NVARCHAR2(50));


COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.APPLIEDPASSPRODUCTID IS 'Pass which was applied to achieve a trip. Either single trip or transfer.';

COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.ORIGINALSINGLEFARE IS 'Fare which an original single trip would have been.';

COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.BLOCKNUMBER IS 'Number of the block';

COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.CORRECTEDLINE IS 'Externally corrected line information.';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
