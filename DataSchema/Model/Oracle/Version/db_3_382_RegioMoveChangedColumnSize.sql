SPOOL db_3_382.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.382', 'LEK', 'Changed RegioMove Column');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_BOOKINGITEM
MODIFY (PRODUCTNAME NVARCHAR2(80));

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
