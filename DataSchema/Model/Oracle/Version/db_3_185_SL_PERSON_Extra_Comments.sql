SPOOL db_3_185.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.185', 'JED', 'Added extra commenttext fields in SL_Person, which can be used project specific.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PERSON
ADD 
(
	COMMENTTEXT2	NVARCHAR2(50),	--in NCT: Job Title
	COMMENTTEXT3	NVARCHAR2(50),	--in NCT: Clock Number
	COMMENTTEXT4	NVARCHAR2(50),	--in NCT: Plus Name
	COMMENTTEXT5	NVARCHAR2(50)   --in NCT: Plus Number
);

COMMENT ON COLUMN SL_PERSON.COMMENTTEXT2 IS 'Additional commenttext, that can have project specific meaning.';
COMMENT ON COLUMN SL_PERSON.COMMENTTEXT3 IS 'Additional commenttext, that can have project specific meaning.';
COMMENT ON COLUMN SL_PERSON.COMMENTTEXT4 IS 'Additional commenttext, that can have project specific meaning.';
COMMENT ON COLUMN SL_PERSON.COMMENTTEXT5 IS 'Additional commenttext, that can have project specific meaning.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
