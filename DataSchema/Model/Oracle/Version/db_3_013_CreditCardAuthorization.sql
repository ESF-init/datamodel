SPOOL db_3_013.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.013', 'DST', 'Increased CardType size to 15 in SL_CreditCardAuthorization');


-- =======[ Changes on existing tables ]===========================================================================

alter table sl_creditcardauthorization modify cardtype nvarchar2(15);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
