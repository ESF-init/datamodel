SPOOL db_3_596.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.596', 'MFA', 'New table to store apportionment calculations. New view to pull value pass taps.');

-- Start adding schema changes here

-- =======[ New Tables ]===========================================================================================

CREATE TABLE ACC_APPORTION
(
  APPORTIONID				NUMBER(18,0)	NOT NULL,
  REVENUERECOGNITIONID 		NUMBER(18,0) 	NOT NULL,
  REVENUESETTLEMENTID		NUMBER(18,0)	NULL,
  TRANSACTIONJOURNALID 		NUMBER(18,0) 	NOT NULL,
  CLOSEOUTPERIODID 			NUMBER(18,0)	NULL,
  TICKETID 					NUMBER(18,0)	NULL,
  TICKETNUMBER 				NUMBER(9,0)		NULL,
  BOARDINGGUID 				NVARCHAR2(40)	NULL,
  OPERATORID  				NUMBER(10,0)	NULL,
  BASEFARE 					NUMBER(18,0)	NULL,
  AMOUNT 					NUMBER(18,0)	NULL,
  PREMIUM 					NUMBER(18,0)	NULL,
  APPORTION 				NUMBER(18,0)	NULL,
  LASTUSER                  NVARCHAR2(50)   DEFAULT 'SYS'   NOT NULL, 
  LASTMODIFIED              DATE            DEFAULT sysdate NOT NULL, 
  TRANSACTIONCOUNTER 		INTEGER     	NOT NULL
);

ALTER TABLE ACC_APPORTION ADD CONSTRAINT PK_APPORTION PRIMARY KEY (APPORTIONID);
ALTER TABLE ACC_APPORTION ADD CONSTRAINT FK_APP_REVENUERECOGNITIONID FOREIGN KEY (REVENUERECOGNITIONID) REFERENCES ACC_REVENUERECOGNITION(REVENUERECOGNITIONID);
ALTER TABLE ACC_APPORTION ADD CONSTRAINT FK_APP_TRANSACTIONJOURNALID FOREIGN KEY (TRANSACTIONJOURNALID) REFERENCES SL_TRANSACTIONJOURNAL(TRANSACTIONJOURNALID);
ALTER TABLE ACC_APPORTION ADD CONSTRAINT FK_APP_CLOSEOUTPERIODID FOREIGN KEY (CLOSEOUTPERIODID) REFERENCES ACC_CLOSEOUTPERIOD(CLOSEOUTPERIODID);

COMMENT ON TABLE ACC_APPORTION IS 'Table for storing apportioned amount for easy lookup.';
COMMENT ON COLUMN ACC_APPORTION.APPORTIONID IS 'Primary key for ACC_APPORTION.';
COMMENT ON COLUMN ACC_APPORTION.REVENUERECOGNITIONID IS 'References the ACC_REVENUERECOGNITION table.';
COMMENT ON COLUMN ACC_APPORTION.REVENUESETTLEMENTID IS 'References the ACC_REVENUESETTLEMENT table.';
COMMENT ON COLUMN ACC_APPORTION.TRANSACTIONJOURNALID IS 'References the SL_TRANSACTIONJOURNAL table.';
COMMENT ON COLUMN ACC_APPORTION.CLOSEOUTPERIODID IS 'References the ACC_CLOSEOUTPERIOD table.';
COMMENT ON COLUMN ACC_APPORTION.TICKETID IS 'The ticket id of the fare.';
COMMENT ON COLUMN ACC_APPORTION.TICKETNUMBER IS 'The internal number of the fare.';
COMMENT ON COLUMN ACC_APPORTION.BOARDINGGUID IS 'The trip id.';
COMMENT ON COLUMN ACC_APPORTION.OPERATORID IS 'The operator id.';
COMMENT ON COLUMN ACC_APPORTION.BASEFARE IS 'The base cost of the fare.';
COMMENT ON COLUMN ACC_APPORTION.AMOUNT IS 'The amount actually paid.';
COMMENT ON COLUMN ACC_APPORTION.PREMIUM IS 'The premium amount that is not apportioned.';
COMMENT ON COLUMN ACC_APPORTION.APPORTION IS 'The apportioned amount.';
COMMENT ON COLUMN ACC_APPORTION.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN ACC_APPORTION.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN ACC_APPORTION.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

COMMENT ON COLUMN ACC_REVENUERECOGNITION.REVENUESETTLEMENTID IS 'OBSOLETE.';

-- =======[ Views ]================================================================================================
CREATE OR REPLACE FORCE VIEW "VIEW_SL_APPORTIONMENT"("APPORTIONID", "REVENUERECOGNITIONID", "REVENUESETTLEMENTID", "TRANSACTIONJOURNALID", "CLOSEOUTPERIODID", "TICKETID", "TICKETNUMBER",
    "BOARDINGGUID", "OPERATORID", "COMPANYNAME", "APPORTION", "LINE", "ZONE", "FAREMEDIAID", "FAREMEDIATYPE", "PRODUCTID", "TRANSACTIONTYPE", "TRANSITACCOUNTID") AS
SELECT a.apportionid, a.revenuerecognitionid, a.revenuesettlementid, a.transactionjournalid, a.closeoutperiodid, a.ticketid, a.ticketnumber,
        a.boardingguid, a.operatorid, c.companyname, a.apportion, t.line, t.zone, t.faremediaid, t.faremediatype, t.productid, t.transactiontype, t.transitaccountid
FROM acc_apportion a
JOIN sl_transactionjournal t ON t.transactionjournalid = a.transactionjournalid
JOIN vario_client c ON c.clientid = t.operatorid;

-- =======[ Sequences ]============================================================================================

create sequence ACC_APPORTION_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================
CREATE OR REPLACE TRIGGER ACC_APPORTION_BRI before insert on ACC_APPORTION for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER ACC_APPORTION_BRU before update on ACC_APPORTION for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
