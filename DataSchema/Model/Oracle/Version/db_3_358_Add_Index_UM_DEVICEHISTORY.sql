SPOOL db_3_358.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.358', 'MKK', 'Need index on UM_DEVICEHISTORY. Performance boost during DDM startup.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

prompt CREATE INDEX IDX_DEVICEH_DID_UID ON UM_DEVICEHISTORY ("DEVICEID", "UNITID")
CREATE INDEX IDX_DEVICEH_DID_UID ON UM_DEVICEHISTORY ("DEVICEID", "UNITID")
LOGGING
NOPARALLEL;


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
