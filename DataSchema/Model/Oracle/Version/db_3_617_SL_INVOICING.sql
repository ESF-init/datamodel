SPOOL db_3_617.log;

------------------------------------------------------------------------------
--Version 3.617
------------------------------------------------------------------------------
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.617', 'ARH', 'Add SL_INVOICING');

-- =======[ New Tables ]===========================================================================================
CREATE TABLE SL_INVOICING
(
	INVOICINGID   				NUMBER(18)      NOT NULL,
	CLIENTID        			NUMBER(18)      NOT NULL,
	USERID  					NUMBER(18)       NOT NULL,
	REQUESTTIME       			DATE            DEFAULT sysdate   NOT NULL,
	COMPLETIONTIME       		DATE,
	INVOICINGTYPE				NUMBER(9)       NOT NULL,
	INVOICINGFILTERTYPE			NUMBER(9)       NOT NULL,
	SEPAFILEPATH          		NVARCHAR2(260),
	LASTUSER              		NVARCHAR2(50)   DEFAULT 'SYS'     NOT NULL,
	LASTMODIFIED          		DATE            DEFAULT sysdate   NOT NULL,
	TRANSACTIONCOUNTER    		INTEGER         NOT NULL
);

ALTER TABLE SL_INVOICING ADD CONSTRAINT SL_INVOICING_PK  PRIMARY KEY  (INVOICINGID)  ENABLE VALIDATE;
ALTER TABLE SL_INVOICING ADD CONSTRAINT FK_INVOICING_CLIENT FOREIGN KEY (CLIENTID) REFERENCES VARIO_CLIENT(CLIENTID);
ALTER TABLE SL_INVOICING ADD CONSTRAINT FK_INVOICING_USER FOREIGN KEY (USERID) REFERENCES USER_LIST(USERID);

COMMENT ON TABLE SL_INVOICING is 'Table containing information about an invoicing';
COMMENT ON COLUMN SL_INVOICING.INVOICINGID is 'References/ID of SL_INVOICING';
COMMENT ON COLUMN SL_INVOICING.CLIENTID is 'For which client is this invoicing';
COMMENT ON COLUMN SL_INVOICING.USERID is 'Who runs the invoicing';
COMMENT ON COLUMN SL_INVOICING.REQUESTTIME is 'Begin of the invoicing';
COMMENT ON COLUMN SL_INVOICING.COMPLETIONTIME is 'End of the invoicing';
COMMENT ON COLUMN SL_INVOICING.INVOICINGTYPE is 'Type of the invoicing';
COMMENT ON COLUMN SL_INVOICING.INVOICINGFILTERTYPE is 'Filter type';
COMMENT ON COLUMN SL_INVOICING.SEPAFILEPATH is 'Physical path to the generated sepa file';
COMMENT ON COLUMN SL_INVOICING.LASTUSER is 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_INVOICING.LASTMODIFIED is 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_INVOICING.TRANSACTIONCOUNTER is 'Technical field: counter to prevent concurrent changes to this dataset';

CREATE TABLE SL_INVOICINGTYPE
(
    ENUMERATIONVALUE NUMBER(9)     not null
        constraint PK_INVOICINGTYPE
            primary key,
    LITERAL          NVARCHAR2(50) not null,
    DESCRIPTION      NVARCHAR2(50) not null
);
COMMENT ON TABLE SL_INVOICINGTYPE is 'Enumeration of invoicing type, known to the system.';
COMMENT ON COLUMN SL_INVOICINGTYPE.ENUMERATIONVALUE is 'Unique ID of the invoicing type';
COMMENT ON COLUMN SL_INVOICINGTYPE.LITERAL is 'Name of the invoicing type; used internally.';
COMMENT ON COLUMN SL_INVOICINGTYPE.DESCRIPTION is 'Descripton of the invoicing type. This is used in the UI to display the value.';

CREATE TABLE SL_INVOICINGFILTERTYPE
(
    ENUMERATIONVALUE NUMBER(9)     not null
        constraint PK_INVOICINGFILTERTYPE
            primary key,
    LITERAL          NVARCHAR2(50) not null,
    DESCRIPTION      NVARCHAR2(50) not null
);
COMMENT ON TABLE SL_INVOICINGFILTERTYPE is 'Enumeration of invoicing filter type, known to the system.';
COMMENT ON COLUMN SL_INVOICINGFILTERTYPE.ENUMERATIONVALUE is 'Unique ID of the invoicing filter type';
COMMENT ON COLUMN SL_INVOICINGFILTERTYPE.LITERAL is 'Name of the invoicing filter type; used internally.';
COMMENT ON COLUMN SL_INVOICINGFILTERTYPE.DESCRIPTION is 'Descripton of the invoicing filter type. This is used in the UI to display the value.';

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_INVOICE ADD  INVOICINGID NUMBER(18);
COMMENT ON COLUMN SL_INVOICE.INVOICINGID IS 'References/ID of SL_INVOICING';
ALTER TABLE SL_INVOICE ADD CONSTRAINT FK_SL_INVOICE_INVOICING FOREIGN KEY (INVOICINGID) REFERENCES SL_INVOICING(INVOICINGID);

ALTER TABLE SL_JOB ADD  INVOICINGID NUMBER(18);
COMMENT ON COLUMN SL_JOB.INVOICINGID IS 'References/ID of SL_INVOICING';
ALTER TABLE SL_JOB ADD CONSTRAINT FK_JOB_INVOICING FOREIGN KEY (INVOICINGID) REFERENCES SL_INVOICING(INVOICINGID);

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------
INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (20, 'SumInvoicing', 'Invoicing for SUM');

INSERT INTO SL_INVOICINGTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (0, 'NotDefined', 'Not defined');
INSERT INTO SL_INVOICINGTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (1, 'SumInvoicing', 'Invoicing for SUM');

INSERT INTO SL_INVOICINGFILTERTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (0, 'All', 'All');
INSERT INTO SL_INVOICINGFILTERTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (1, 'Sepa', 'Sepa');
INSERT INTO SL_INVOICINGFILTERTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (2, 'Invoice', 'Invoice/ Not sepa');

-- =======[ Sequences ]============================================================================================

create sequence SL_INVOICING_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================
CREATE OR REPLACE TRIGGER SL_INVOICING_BRI before insert on SL_INVOICING for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_INVOICING_BRU before update on SL_INVOICING for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

COMMIT;

PROMPT Done!

SPOOL OFF;
