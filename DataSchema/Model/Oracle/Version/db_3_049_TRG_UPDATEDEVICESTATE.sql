SPOOL db_3_049.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.049', 'MKK', 'BSAG: It was not possible to change the state in DDM from Active -> Garage -> Out of Service if the user does not store the changes after each step.');

-- Start adding schema changes here
CREATE OR REPLACE TRIGGER TRG_UPDATEDEVICESTATE
BEFORE UPDATE
OF UNITID
ON UM_DEVICE 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tTYPEOFUNIT NUMBER(10,0);
tOldTYPEOFUNIT NUMBER(10,0);

BEGIN

  SELECT TYPEOFUNITID INTO tTYPEOFUNIT FROM UM_UNIT WHERE UNITID = :NEW.UNITID;  
  SELECT TYPEOFUNITID INTO tOldTYPEOFUNIT FROM UM_UNIT WHERE UNITID = :OLD.UNITID;  

  IF tTYPEOFUNIT = 0
  THEN
   -- It is possible that the user changes the state from 2 to 3 in the GUI. 
   IF :NEW.STATE = 3
   THEN
    :NEW.STATE := 3;
   ELSE
   -- Device replaced by PDI or status set in DDM to Garage
    :NEW.STATE := 2;
   END IF;
  ELSE
    -- We need to identify if this change has been triggered by PDI or DDM. PDI always changes LASTDATA.
   IF tTYPEOFUNIT = 3 AND :NEW.LASTDATA <> :OLD.LASTDATA
   THEN
   :NEW.STATE := 1;
   
   END IF;

  END IF;
  
END TRG_UPDATEDEVICESTATE;
/



-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

--COMMENT ON TABLE XXX is '';

--COMMENT ON COLUMN XXX.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
--COMMENT ON COLUMN XXX.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
--COMMENT ON COLUMN XXX.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
