SPOOL db_3_170.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.170', 'STV', 'SL_PRODUCT ');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PRODUCT
      ADD ( PointOfSaleID Number(18) NULL);

ALTER TABLE SL_PRODUCT
ADD CONSTRAINT fk_product_pointOfSale
  FOREIGN KEY (PointOfSaleID)
  REFERENCES POS_POINTOFSALE(PointOfSaleID);	  

COMMENT ON COLUMN SL_PRODUCT.PointOfSaleID IS 'References table POS_POINTOFSALE';

COMMIT;

PROMPT Done!

SPOOL OFF;
