SPOOL db_3_199.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.199', 'SOE', 'Added clientid to sl_form.');


-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_form add clientid number(10);
alter table sl_form add constraint fk_form_client foreign key (clientid) references vario_client (clientid);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;