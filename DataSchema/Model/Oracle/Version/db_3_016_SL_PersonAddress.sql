SPOOL db_3_016.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.016', 'AMA', 'Added SL_PersonAddress table');

-- Start adding schema changes here


-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================
CREATE TABLE SL_PersonAddress (
	PersonID NUMBER(18, 0) NOT NULL, 
	AddressID NUMBER(18, 0) NOT NULL, 
	AddressType NUMBER(9, 0) NOT NULL, 
	Created DATE DEFAULT sysdate NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL,
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_PersonAddress PRIMARY KEY (PersonID, AddressID),
	CONSTRAINT FK_PersonAddress_Person FOREIGN KEY (PersonID) REFERENCES SL_Person(PersonID),
	CONSTRAINT FK_PersonAddress_Address FOREIGN KEY (AddressID) REFERENCES SL_Address(AddressID)
);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------
INSERT ALL
    INTO SL_AddressType (EnumerationValue, Literal, Description) VALUES (2, 'Correspondence', 'Correspondence Address')
    INTO SL_AddressType (EnumerationValue, Literal, Description) VALUES (3, 'Person', 'Person Address')
	INTO SL_AddressType (EnumerationValue, Literal, Description) VALUES (4, 'PersonAlternative', 'Person Alternative Address')
SELECT * FROM DUAL; 
-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
