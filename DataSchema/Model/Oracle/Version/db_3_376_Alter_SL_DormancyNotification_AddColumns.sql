SPOOL db_3_376.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.376', 'MFA', 'Add two columns to SL_DormancyNotification');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_DORMANCYNOTIFICATION ADD (LASTUSED DATE);
COMMENT ON COLUMN SL_DORMANCYNOTIFICATION.LASTUSED IS 'Last used date of the card at point of recond input.';

ALTER TABLE SL_DORMANCYNOTIFICATION ADD (MONTHS NUMBER(9,0));
COMMENT ON COLUMN SL_DORMANCYNOTIFICATION.MONTHS IS 'Number of months for which the warning is sent.';

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
