SPOOL db_3_392.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.392', 'OMA', 'SL_TRANSACTIONJOURNEYHISTORY : Transactions with Journey info.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

--COMMENT ON TABLE XXX is '';

--COMMENT ON COLUMN XXX.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
--COMMENT ON COLUMN XXX.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
--COMMENT ON COLUMN XXX.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================
CREATE OR REPLACE FORCE VIEW SL_TRANSACTIONJOURNEYHISTORY
(
	TRANSACTIONID,
    CARDID,
	TRIPDATETIME,
	TYPEID,
	TYPENAME,
	TICKETNUMBER,
	TICKETNAME,
	FROMSTOPNUMBER,
	FROMSTOPNAME,
	TOSTOPNUMBER,
	TOSTOPNAME,
	PRICE,
	CARDBALANCE,
	CARDTRANSACTIONNO,
	CARDREFTRANSACTIONNO,
	TRIPSERIALNO,
	JOURNEYLEGCOUNTER
)
AS
      SELECT TRANSACTIONID,
	  CARDID,
	  TRIPDATETIME, 
	  tr.TYPEID, 
	  CASE WHEN cardreftransactionno != 0 THEN 'Journey' ELSE TYPENAME END as TypeName,
	  TikNo, 
	  tik.Name, 
	  FROMSTOPNO, 
	  fromStop.stoplongname, 
	  StopTo, 
	  toStop.stoplongname, 
	  tr.Price, 
	  CardBalance, 
	  cardtransactionno, 
	  cardreftransactionno, 
	  tripserialno, 
	  journeylegcounter
	FROM RM_TRANSACTION tr
		JOIN rm_devicebookingstate
			ON tr.devicebookingstate = rm_devicebookingstate.devicebookingno
		JOIN rm_shift 
			ON tr.shiftid = rm_shift.shiftid
		JOIN rm_transactiontype tt
			ON tr.typeid = tt.typeid
		JOIN tm_ticket tik
			ON tr.TARIFFID = tik.tarifid
			AND tr.tikno = tik.internalnumber
		JOIN tm_tarif 
			ON tik.TARIFID = tm_tarif.TARIFID
		JOIN vario_net 
			ON tm_tarif.NETID = vario_net.NETID
		LEFT JOIN vario_stop fromStop
			ON vario_net.netid = fromStop.netid
			AND tr.fromstopno = fromStop.stopno
		LEFT JOIN vario_stop toStop
			ON vario_net.netid = toStop.netid
			AND tr.StopTo = toStop.stopno
	WHERE rm_shift.shiftstateid < 30
		AND tr.cancellationid = 0
		AND rm_devicebookingstate.isbooked = 1
		AND tr.typeid != 5;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
