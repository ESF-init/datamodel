SPOOL db_3_324.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.324', 'BVI', 'Modified PostingKeyType REFERENCES.');

-- =======[ Create+Edit ]===============================================================================================

alter table SL_PostingKey drop CONSTRAINT FK_PostingKey_PostingKeyType;

alter table SL_PostingKey add CONSTRAINT FK_PostingKey_PostingKeyType FOREIGN KEY (PostingKeyTypeID) REFERENCES SL_PostingKeyType(PostingKeyTypeID);


COMMIT;

PROMPT Done!

SPOOL OFF;
