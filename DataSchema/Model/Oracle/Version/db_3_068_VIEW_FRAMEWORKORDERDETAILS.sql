SPOOL db_3_068.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.068', 'FMT', 'Added aditional colum "Farestage" to View');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================
DROP VIEW SL_FRAMEWORKORDERDETAILS;

CREATE OR REPLACE FORCE VIEW sl_frameworkorderdetails (orderid,
                                                           ordernumber,
                                                           frameworkname,
                                                           frameworkorganizationnumber,
                                                           frameworkorgexternalnnumber,
                                                           frameworkaddressee,
                                                           frameworkaddressfield1,
                                                           frameworkaddressfield2,
                                                           frameworkcity,
                                                           frameworkpostalcode,
                                                           frameworkcountry,
                                                           frameworkregion,
                                                           frameworkstreet,
                                                           frameworkstreetnumber,
                                                           schoolname,
                                                           schoolorganizationnumber,
                                                           schooladdressee,
                                                           schooladdressfield1,
                                                           schooladdressfield2,
                                                           schoolcity,
                                                           schoolpostalcode,
                                                           schoolcountry,
                                                           schoolstreet,
                                                           schoolstreetnumber,
                                                           schoolregion,
                                                           organizationalidentifier,
                                                           firstname,
                                                           lastname,
                                                           dateofbirth,
                                                           printednumber,
                                                           ticketname,
                                                           validfrom,
                                                           validto,
                                                           relationfrom,
                                                           relationto,
                                                           relationpriority,
                                                           schoolyearname,
                                                           frameworkcontractid,
                                                           schoolcontractid,
                                                           personid,
                                                           productrelationid,
                                                           productid,
                                                           ticketid,
                                                           orderdetailid,
														   relationfarestage
                                                          )
AS
   SELECT orderentity.orderid, orderentity.ordernumber,
          frameworkcontractorg.NAME AS frameworkname,
          frameworkcontractorg.organizationnumber
                                               AS frameworkorganizationnumber,
          frameworkcontractorg.externalnumber AS frameworkexternalnumber,
          frameworkcontractaddress.addressee AS frameworkaddressee,
          frameworkcontractaddress.addressfield1 AS frameworkaddressfield1,
          frameworkcontractaddress.addressfield2 AS frameworkaddressfield2,
          frameworkcontractaddress.city AS frameworkcity,
          frameworkcontractaddress.postalcode AS frameworkpostalcode,
          frameworkcontractaddress.country AS frameworkcountry,
          frameworkcontractaddress.region AS frameworkregion,
          frameworkcontractaddress.street AS frameworkstreet,
          frameworkcontractaddress.streetnumber AS frameworkstreetnumber,
          schoolorg.NAME AS schoolname,
          schoolorg.organizationnumber AS schoolorganizationnumber,
          schoolcontractaddress.addressee AS schooladdressee,
          schoolcontractaddress.addressfield1 AS schooladdressfield1,
          schoolcontractaddress.addressfield2 AS schooladdressfield2,
          schoolcontractaddress.city AS schoolcity,
          schoolcontractaddress.postalcode AS schoolpostalcode,
          schoolcontractaddress.country AS schoolcountry,
          schoolcontractaddress.street AS schoolstreet,
          schoolcontractaddress.streetnumber AS schoolstreetnumber,
          schoolcontractaddress.region AS schoolregion,
          cardholder.organizationalidentifier, cardholder.firstname,
          cardholder.lastname, cardholder.dateofbirth, card.printednumber,
          ticket.NAME AS ticketname, product.validfrom, product.validto,
          relation.relationfrom, relation.relationto,
          relation.farematrixentrypriority , schoolyear.schoolyearname,
          frameworkcontract.contractid AS frameworkcontractid,
          schoolcontract.contractid AS schoolcontractid, cardholder.personid,
          relation.productrelationid, product.productid, product.ticketid,
          orderdetail.orderdetailid,RELATION.FARESTAGE
     FROM sl_order orderentity,
          sl_contract frameworkcontract,
          sl_orderdetail orderdetail,
          sl_organization frameworkcontractorg,
          sl_address frameworkcontractaddress,
          sl_contractaddress frameworkcta,
          sl_product product,
          tm_ticket ticket,
          sl_productrelation relation,
          sl_card card,
          sl_person cardholder,
          sl_contract schoolcontract,
          sl_organization schoolorg,
          sl_address schoolcontractaddress,
          sl_contractaddress schoolcta,
          sl_schoolyear schoolyear
    WHERE orderentity.contractid = frameworkcontract.contractid
      AND frameworkcontract.organizationid =
                                           frameworkcontractorg.organizationid
      AND frameworkcontractaddress.addressid = frameworkcta.addressid
      AND frameworkcta.isretired = 0
      AND frameworkcta.addresstype = 0
      AND frameworkcta.contractid = frameworkcontract.contractid
      AND orderdetail.orderid = orderentity.orderid
      AND product.productid = orderdetail.productid
      AND product.productid = relation.productid
      AND product.tickettype <> 103
      AND product.ticketid = ticket.ticketid
      AND product.cardid = card.cardid
      AND card.cardholderid = cardholder.personid
      AND cardholder.contractid = schoolcontract.contractid
      AND schoolorg.organizationid = schoolcontract.organizationid
      AND schoolcta.contractid = schoolcontract.contractid
      AND schoolcta.addressid = schoolcontractaddress.addressid
      AND schoolcta.isretired = 0
      AND schoolcta.addresstype = 0
      AND cardholder.schoolyearid = schoolyear.schoolyearid;
-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
