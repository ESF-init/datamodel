SPOOL db_3_078.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.078', 'ULB', 'added table VDV_TXTRANSACTIONTYPE');


-- =======[ New Tables ]===========================================================================================
CREATE TABLE VDV_TXTRANSACTIONTYPE
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL
);


CREATE UNIQUE INDEX PK_VDV_TXTRANSACTIONTYPE ON VDV_TXTRANSACTIONTYPE
(ENUMERATIONVALUE);


ALTER TABLE VDV_TXTRANSACTIONTYPE ADD (
  CONSTRAINT PK_VDV_TXTRANSACTIONTYPE
 PRIMARY KEY
 (ENUMERATIONVALUE)
    USING INDEX );

COMMENT ON TABLE VDV_TXTRANSACTIONTYPE is ' Enumeration containing VDV-Ka transaction types';
COMMENT ON COLUMN VDV_TXTRANSACTIONTYPE.ENUMERATIONVALUE   IS 'Contains the Enum value';
COMMENT ON COLUMN VDV_TXTRANSACTIONTYPE.LITERAL            IS 'Contains the literal';
COMMENT ON COLUMN VDV_TXTRANSACTIONTYPE.DESCRIPTION        IS 'Contains the description';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
