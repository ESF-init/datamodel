SPOOL db_3_621.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.621', 'FRD', 'Order Processing Cleanup Orders');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_ORDERPROCESSINGCLEANUP
(
  ORDERCLEANUPID	  NUMBER(18)                NOT NULL,
  ORDERID     		  NUMBER(18)                NOT NULL,
  ORDERNUMBER     	  NVARCHAR2(50),
  ERRORMESSAGE        NVARCHAR2(256),
  ORDERRESET          NUMBER(1)                 DEFAULT 0,
  TRANSACTIONCOUNTER  INTEGER     				NOT NULL,
  LASTUSER            NVARCHAR2(50)   			DEFAULT 'SYS'     NOT NULL,
  LASTMODIFIED        DATE            			DEFAULT sysdate   NOT NULL
);

--comments
COMMENT ON TABLE SL_ORDERPROCESSINGCLEANUP IS 'Contains information on the failed orders processed by the daily cleanup. Used for Order Processing.';

COMMENT ON COLUMN SL_ORDERPROCESSINGCLEANUP.ORDERCLEANUPID IS 'Unique id of the order cleanup.';
COMMENT ON COLUMN SL_ORDERPROCESSINGCLEANUP.ORDERID IS 'Id of the order.';
COMMENT ON COLUMN SL_ORDERPROCESSINGCLEANUP.ORDERNUMBER IS 'Order number.';
COMMENT ON COLUMN SL_ORDERPROCESSINGCLEANUP.ERRORMESSAGE IS 'Error message associated with order failure.';
COMMENT ON COLUMN SL_ORDERPROCESSINGCLEANUP.ORDERRESET IS 'Determines if a failed order has been reset.';
COMMENT ON COLUMN SL_ORDERPROCESSINGCLEANUP.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';

--keys
ALTER TABLE SL_ORDERPROCESSINGCLEANUP ADD (CONSTRAINT PK_ORDERCLEANUPID PRIMARY KEY (ORDERCLEANUPID));
ALTER TABLE SL_ORDERPROCESSINGCLEANUP ADD (CONSTRAINT FK_ORDER_CLEANUP FOREIGN KEY (ORDERID) REFERENCES SL_ORDER (ORDERID));


-- =======[ Sequences ]============================================================================================

CREATE SEQUENCE SL_ORDERPROCESSINGCLEANUP_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================

CREATE OR REPLACE TRIGGER SL_ORDERPROCESSINGCLEANUP_BRI before insert on SL_ORDERPROCESSINGCLEANUP for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_ORDERPROCESSINGCLEANUP_BRU before update on SL_ORDERPROCESSINGCLEANUP for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
