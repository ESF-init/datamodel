SPOOL db_3_405.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.405', 'MMB', 'Add IsEmailEnabled column to SL_Person');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TRIGGER SL_PERSON_BRU DISABLE;
ALTER TABLE SL_PERSON ADD ISEMAILENABLED NUMBER(1) NULL;
UPDATE SL_PERSON SET ISEMAILENABLED = 1 WHERE ISEMAILENABLED IS NULL;
ALTER TABLE SL_PERSON MODIFY ISEMAILENABLED NOT NULL;
ALTER TABLE SL_PERSON MODIFY ISEMAILENABLED DEFAULT 1;
ALTER TRIGGER SL_PERSON_BRU ENABLE;
COMMENT ON COLUMN SL_PERSON.ISEMAILENABLED IS 'Flag to enable sending out emails.';

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;

