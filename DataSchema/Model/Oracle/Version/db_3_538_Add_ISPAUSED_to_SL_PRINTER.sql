SPOOL db_3_538.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.538', 'EMN', 'Adding column IsPaused to SL_PRINTER.');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TRIGGER SL_PRINTER_BRU DISABLE;
ALTER TABLE SL_PRINTER ADD ISPAUSED NUMBER(1) Default 0;
ALTER TRIGGER SL_PRINTER_BRU ENABLE;
COMMENT ON COLUMN SL_PRINTER.ISPAUSED IS 'Flag for the printer being in a paused state';
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
