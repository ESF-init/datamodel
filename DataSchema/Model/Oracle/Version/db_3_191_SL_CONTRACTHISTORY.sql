SPOOL db_3_191.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.191', 'UB', 'Expand SL_CONTRACTHISTORY TEXT to 2000');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CONTRACTHISTORY MODIFY TEXT NVARCHAR2 (2000);


---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
