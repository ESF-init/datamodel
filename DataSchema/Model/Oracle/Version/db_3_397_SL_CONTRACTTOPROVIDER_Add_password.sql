SPOOL db_3_397.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.397', 'ARH', 'SL_CONTRACTTOPROVIDER : Added new column encryptedpassword');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CONTRACTTOPROVIDER ADD ENCRYPTEDPASSWORD VARCHAR2(50 BYTE);

COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.ENCRYPTEDPASSWORD IS 'Encrypted password needed to connect the client to the mobility provider system.';

COMMIT;

PROMPT Done!

SPOOL OFF;