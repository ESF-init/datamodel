SPOOL db_3_053.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.053', 'SOE', 'added fareMediaTypeId JobBukImport');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_jobbulkimport add fareMediaTypeId number(18,0) null ;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
