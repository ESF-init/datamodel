SPOOL db_3_070.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.070', 'DST', 'Added schoolyearid to sl_orderdetail.');


-- SchoolYearID was in the main schema, but not in any change script, so this might fail.

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_orderdetail add schoolyearid number(18);
alter table sl_orderdetail add constraint fk_orderdetail_schoolyear foreign key (schoolyearid) references sl_schoolyear (schoolyearid);
comment on column sl_orderdetail.schoolyearid is 'Deprecated';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
