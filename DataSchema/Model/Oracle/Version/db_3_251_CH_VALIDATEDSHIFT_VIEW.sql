SPOOL db_3_251.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.251', 'PCO', 'Clearing House View to show validated shifts');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW        CH_VALIDATEDSHIFTS
(
    SHIFTID	
)
AS
      SELECT s.SHIFTID
		FROM RM_SHIFT s
			JOIN SL_VALIDATIONVALUE VV ON TO_NCHAR(s.SHIFTID) = VV.VALUE
			JOIN SL_VALIDATIONRESULT VR ON VV.VALIDATIONRESULTID = VR.VALIDATIONRESULTID
			JOIN SL_VALIDATIONRULE VRU ON VR.VALIDATIONRULEID = VRU.VALIDATIONRULEID
		WHERE VR.ISVALID = 1 AND LOWER(VV.NAME) = 'shiftid'  				
		AND VRU.NAME = 'ShiftCompletenessCheckRule'
		ORDER BY s.SHIFTID;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

PROMPT Done!

SPOOL off; 

