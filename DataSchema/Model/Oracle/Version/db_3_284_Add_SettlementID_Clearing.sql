SPOOL db_3_284.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.284', 'FRD', 'Adding SettlementID to Clearing House to remove need to persist settlement data');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE CH_CLEARING ADD SETTLEMENTID NUMBER(18,0) NOT NULL;

COMMENT ON COLUMN CH_CLEARING.SETTLEMENTID IS 'Referenece to the bi-monthly settlement results.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
