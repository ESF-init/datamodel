SPOOL db_3_157.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.157', 'STV', 'Add default value for blockingreason SL_CARD');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CARD 
MODIFY BlockingReason DEFAULT 0;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
