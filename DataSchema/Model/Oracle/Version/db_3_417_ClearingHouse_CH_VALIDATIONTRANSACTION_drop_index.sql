SPOOL db_3_417.log;
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.417', 'FRD', 'CH_VALIDATIONTRANSACTION drop index');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE CH_VALIDATIONTRANSACTION DROP CONSTRAINT UK_VALIDATEDTRANSACTION;
DROP INDEX UK_VALIDATEDTRANSACTION;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;