SPOOL db_3_629.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.629', 'ULB', 'Add new datatypes to sl_configurationdatatypes');

COMMENT ON COLUMN SL_CONFIGURATIONDEFINITION.ACCESSLEVEL IS 'None = 0, ClientValue_ReadWrite = 1,DefaultValue_ReadWrite = 2,ReadOnly =3,';
-- =======[ Data ]==================================================================================================
update sl_configurationdefinition set accesslevel=1,transactioncounter=transactioncounter+1 where accesslevel=6;
update sl_configurationdefinition set datatype=0,transactioncounter=transactioncounter+1  where datatype=1;

-- -------[ Enumerations ]------------------------------------------------------------------------------------------
Insert into SL_CONFIGURATIONDATATYPE
   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values
   (10, 'XML', 'XML');
Insert into SL_CONFIGURATIONDATATYPE
   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values
   (11, 'Binary', 'Binary');
Insert into SL_CONFIGURATIONDATATYPE
   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values
   (12, 'LargeText', 'LargeText');





COMMIT;

PROMPT Done!

SPOOL OFF;
