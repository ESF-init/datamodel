SPOOL db_3_368.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.368', 'STV', 'Added new column to SL_TICKETASSIGNMENT.IsAutoRenewEnabled');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_TICKETASSIGNMENT
 ADD (IsAutoRenewEnabled  NUMBER(1,0) DEFAULT(0) NOT NULL );
 
COMMENT ON COLUMN SL_TICKETASSIGNMENT.IsAutoRenewEnabled IS 'This flag determens if the autoload process should top up the remainging quantity';

ALTER TABLE SL_TICKETASSIGNMENTSTATUS ADD CONSTRAINT PK_TICKETASSIGNMENTSTATUS PRIMARY KEY (ENUMERATIONVALUE);
ALTER TABLE SL_PROMOCODESTATUS ADD CONSTRAINT PK_PROMOCODESTATUS PRIMARY KEY (ENUMERATIONVALUE);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
