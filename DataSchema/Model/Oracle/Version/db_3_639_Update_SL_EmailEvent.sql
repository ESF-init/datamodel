SPOOL db_3_639.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.639', 'SOE', 'SmartcardFraudulentUse updated in SL_EmailEvent');

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

UPDATE SL_EmailEvent SET EventName = 'SmartcardFraudulentUse',Description = 'SmartcardFraudulentUse',ValidFormSchemaName = 'SmartcardFraudMerge' , TransactionCounter = TransactionCounter +1 where EnumerationValue = 97 ;

COMMIT;

PROMPT Done!

SPOOL OFF;
