SPOOL db_3_653.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.653', 'SOE', 'New EventType AutoloadUpdatedForPass inserted in SL_EmailEvent.');

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadUpdatedForPass','Autoload updated for pass', -1, 'AutoloadMailMerge', 100);
INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadUpdatedForPassFromNotPrimary','Autoload updated for pass from not primary', -1, 'AutoloadMailMerge', 101);

COMMIT;

PROMPT Done!

SPOOL OFF;
