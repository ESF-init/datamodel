SPOOL db_3_221.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.221', 'TSC', 'Maintenance Level as levels instead as flag, done for IBSAG-574.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE dm_debtor
MODIFY (
    maintenancelevel	NUMBER(9,0)
);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
