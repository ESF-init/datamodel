SPOOL db_3_093.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.093', 'EPA', 'Add column BILLHIDDEN to PP_ACCOUNTPOSTINGKEY');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE PP_ACCOUNTPOSTINGKEY ADD (BILLHIDDEN NUMBER(1) DEFAULT 0  NOT NULL);

-- =======[ New Tables ]===========================================================================================

COMMENT ON COLUMN PP_ACCOUNTPOSTINGKEY.BILLHIDDEN IS 'Hides account entries with this posting key on online debtor accounting';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
