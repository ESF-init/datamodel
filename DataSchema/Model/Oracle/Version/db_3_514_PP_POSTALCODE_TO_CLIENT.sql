SPOOL db_3_514.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.514', 'JGI', 'added table PP_POSTALCODE_TO_CLIENT');


-- =======[ New Tables ]===========================================================================================
CREATE TABLE PP_POSTALCODE_TO_CLIENT
(
  POSTALCODETOCLIENTID  NUMBER(10),
  CLIENTID              NUMBER(10),
  ZIP                   NUMBER(10),
  ABOCENTER             VARCHAR2(50),
  ABOCENTER_CODE        VARCHAR2(50),
  NAME                  VARCHAR2(50)
);


COMMENT ON TABLE  PP_POSTALCODE_TO_CLIENT is 'Assigns different postal codes to clients. This is used to dtermine, which postal code belongs to which client, when creating a contract with the BOBAPP';
COMMENT ON COLUMN PP_POSTALCODE_TO_CLIENT.POSTALCODETOCLIENTID   IS 'Unique identifier, which serves as primary key';
COMMENT ON COLUMN PP_POSTALCODE_TO_CLIENT.CLIENTID   IS 'Assigned client';
COMMENT ON COLUMN PP_POSTALCODE_TO_CLIENT.ZIP   IS 'Zip code (Postal code)';
COMMENT ON COLUMN PP_POSTALCODE_TO_CLIENT.ABOCENTER   IS 'Name of the abocenter, which is responsible for the zip code';
COMMENT ON COLUMN PP_POSTALCODE_TO_CLIENT.ABOCENTER_CODE   IS 'Code of the abocenter, which is responsible for the zip code';
COMMENT ON COLUMN PP_POSTALCODE_TO_CLIENT.NAME   IS 'Name of the location';


ALTER TABLE PP_POSTALCODE_TO_CLIENT ADD (
  CONSTRAINT PP_POSTALCODE_TO_CLIENT_PK
  PRIMARY KEY
  (POSTALCODETOCLIENTID)
  ENABLE VALIDATE);
  

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;

