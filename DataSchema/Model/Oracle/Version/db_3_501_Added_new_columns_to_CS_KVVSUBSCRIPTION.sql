SPOOL db_3_501.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.501', 'ARH', 'add new columns to CS_KVVSUBSCRIPTION');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE CS_KVVSUBSCRIPTION
 ADD (PRODUCTID  VARCHAR2(10 BYTE),
 PRODUCTDESCRIPTION VARCHAR2(60 BYTE),
 FAREZONEID VARCHAR2(10 BYTE),
 FAREZONEFROMID NUMBER(10),
 FAREZONETOID NUMBER(10)
 );

COMMENT ON COLUMN CS_KVVSUBSCRIPTION.PRODUCTID IS 'Product ID for the KVV.';
COMMENT ON COLUMN CS_KVVSUBSCRIPTION.PRODUCTDESCRIPTION IS 'Description of the product.';
COMMENT ON COLUMN CS_KVVSUBSCRIPTION.FAREZONEID IS 'Fare zone ID.';
COMMENT ON COLUMN CS_KVVSUBSCRIPTION.FAREZONEFROMID IS 'Fare zone from ID.';
COMMENT ON COLUMN CS_KVVSUBSCRIPTION.FAREZONETOID IS 'Fare zone to ID.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;