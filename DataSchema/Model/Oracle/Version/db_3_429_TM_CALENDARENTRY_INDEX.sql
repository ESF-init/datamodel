SPOOL db_3_429.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.429', 'FLF', 'Added index IDX_CALENDARENTRYDATE to TM_CALENDARENTRY.DATE_ column');

-- =======[ Changes to Existing Tables ]===========================================================================
CREATE INDEX IDX_CALENDARENTRYDATE ON TM_CALENDARENTRY(DATE_);

-- =======[ New Tables ]===========================================================================================

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
