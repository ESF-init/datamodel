SPOOL db_3_052.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.052', 'AMA', 'Parameter Management - Mobile Devices Settings');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE VARIO_DEVICECLASS Add CREATEPARAMETERARCHIVE NUMBER(1) DEFAULT 0  NOT NULL;

ALTER TABLE SL_UNITCOLLECTION ADD (DEVICECLASSID NUMBER(10), IsDefaultForDeviceClass NUMBER(1) DEFAULT 0  NOT NULL);
	
ALTER TABLE SL_UNITCOLLECTION ADD CONSTRAINT FK_UNITCOLLECTION_DEVICE FOREIGN KEY (DEVICECLASSID) REFERENCES VARIO_DEVICECLASS (DEVICECLASSID);

COMMENT ON COLUMN VARIO_DEVICECLASS.CREATEPARAMETERARCHIVE IS 'Presents whether parameter archive should be generated and saved in the file system for the device class';

COMMENT ON COLUMN SL_UNITCOLLECTION.DEVICECLASSID IS 'Presents whether the unit collection is specific for a device class';
COMMENT ON COLUMN SL_UNITCOLLECTION.IsDefaultForDeviceClass IS 'Presents whether the device class unit collection is a default collection';


-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_ParameterArchiveRelease
(
	ParameterArchiveReleaseID	NUMBER(18)							NOT NULL,
	PARAMETERARCHIVEID  		NUMBER(18)							NOT NULL,
	DEVICECLASSID    			NUMBER(18)							NOT NULL,
	RELEASEDATETIME  			DATE								NOT NULL,
	RELEASETYPE      			NUMBER(9)							NOT NULL,
	LASTUSER					NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
	LASTMODIFIED				DATE			DEFAULT sysdate		NOT NULL,
	TRANSACTIONCOUNTER			NUMBER								NOT NULL,
	CONSTRAINT PK_ParameterArchiveRelease PRIMARY KEY (ParameterArchiveReleaseID),
	CONSTRAINT FK_PararmArchRelease_ARCHIVCE FOREIGN KEY (PARAMETERARCHIVEID) REFERENCES SL_PARAMETERARCHIVE (PARAMETERARCHIVEID),
	CONSTRAINT FK_PararmArchRelease_DEVICE FOREIGN KEY (DEVICECLASSID) REFERENCES VARIO_DEVICECLASS (DEVICECLASSID)
);

COMMENT ON TABLE SL_ParameterArchiveRelease is 'Saves information about parameter archive releases for device classes';

COMMENT ON COLUMN SL_ParameterArchiveRelease.ParameterArchiveReleaseID IS 'Unique id of the parameter archive release';
COMMENT ON COLUMN SL_ParameterArchiveRelease.PARAMETERARCHIVEID IS 'Points to the parameter archive - References SL_PARAMETERARCHIVE.PARAMETERARCHIVEID';
COMMENT ON COLUMN SL_ParameterArchiveRelease.DEVICECLASSID IS 'Points to the device class - References VARIO_DEVICECLASS.DEVICECLASSID';
COMMENT ON COLUMN SL_ParameterArchiveRelease.RELEASEDATETIME IS 'Date and time of the release';
COMMENT ON COLUMN SL_ParameterArchiveRelease.RELEASETYPE IS 'Type of release - Test or final';

COMMENT ON COLUMN SL_ParameterArchiveRelease.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_ParameterArchiveRelease.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_ParameterArchiveRelease.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

CREATE SEQUENCE SL_ParameterArchiveRelease_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;

-- =======[ Trigger ]==============================================================================================

CREATE OR REPLACE TRIGGER SL_ParameterArchiveRelease_BRI before insert on SL_ParameterArchiveRelease for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_ParameterArchiveRelease_BRU before update on SL_ParameterArchiveRelease for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/
-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
