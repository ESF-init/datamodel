SPOOL db_3_578.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.578', 'ULB', 'Add AM_Component.PRINTEDCOMPONENTNO ');


ALTER TABLE AM_COMPONENT ADD (PRINTEDCOMPONENTNO VARCHAR2(25 ));

COMMENT ON COLUMN AM_COMPONENT.PRINTEDCOMPONENTNO IS 'Userfriendly Componentnumber, if not available same as serial';

COMMIT;

PROMPT Done!

SPOOL OFF;
