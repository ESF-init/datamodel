SPOOL db_3_443.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.443', 'JGI', 'PP_CARD, added two new columns for BOB App');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE PP_CARD ADD (EXTERNALID VARCHAR2(1000));

ALTER TABLE PP_CARD ADD (EXTERNALNAME VARCHAR2(1000));


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
