SPOOL db_3_224.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.224', 'SOE', 'Added ENTITLEMENTID to SL_PRODUCT.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PRODUCT
ADD ENTITLEMENTID NUMBER (9);

-- =======[ Commit ]===============================================================================================

COMMENT ON COLUMN SL_PRODUCT.ENTITLEMENTID IS 'ENTITLEMENTID from VDV-KA';
COMMIT;

PROMPT Done!

SPOOL OFF;