SPOOL db_3_460.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.460', 'BVI', 'Added sequences and trigger for contract termination.');

-- =======[ Sequences ]===========================================================================================

create sequence SL_CONTRACTTERMINATIONTYPE_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;
create sequence SL_CONTRACTTERMINATION_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

create or replace trigger  SL_CONTRACTTERMINATIONTYPE_BRI
		before insert on SL_CONTRACTTERMINATIONTYPE
		for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;
		/

create or replace trigger  SL_CONTRACTTERMINATION_BRI
		before insert on SL_CONTRACTTERMINATION
		for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;
		/
		
-- =======[ Trigger ]===========================================================================================

				
create or replace trigger SL_CONTRACTTERMINATIONTYPE_BRU
		before update on SL_CONTRACTTERMINATIONTYPE
		for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;		
		/

create or replace trigger SL_CONTRACTTERMINATION_BRU
		before update on SL_CONTRACTTERMINATION
		for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;		
		/		

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
