SPOOL db_3_243.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible,annotation)
VALUES (sysdate, '3.243', 'DST','Modified rownum limits for housekeeping procedure.');

CREATE OR REPLACE PROCEDURE     PERF_HOUSEKEEPING IS
RESULT   NUMBER;
   
BEGIN
   delete from SL_FarePaymentError
       where dbms_lob.compare(Error, 'Info') = 0
        and LastModified < sysdate - 30
        and rownum <= 150000;
        
   RESULT := SQL%ROWCOUNT;
   INSERT INTO system_log
               (systemlogid, systemdate, action, username, content, tablename, actionresult, text)
        VALUES (getdataid (), SYSDATE, 'DELETE', $$plsql_unit, 'Rows deleted', 'SL_FAREPAYMENTERROR', RESULT, 'Housekeeping for SL_FAREPAYMENTERROR');
   COMMIT;
      
   delete
     from SL_WhitelistJournal wj
     where wj.LastModified < sysdate - 90
     and exists (select whitelistjournalid from sl_whitelistjournal where cardid = wj.cardid and lastmodified > wj.lastmodified and lastmodified < sysdate - 90)
     and rownum <= 150000;

   RESULT := SQL%ROWCOUNT;
   INSERT INTO system_log
               (systemlogid, systemdate, action, username, content, tablename, actionresult, text)
        VALUES (getdataid (), SYSDATE, 'DELETE', $$plsql_unit, 'Rows deleted', 'SL_WHITELISTJOURNAL', RESULT, 'Housekeeping for SL_WHITELISTJOURNAL');        
   COMMIT;
   
   delete
      from RM_BinaryData
      where DataType = 5
      and ExportState in (2) -- skipped
      and rownum <= 150000;
    
   RESULT := SQL%ROWCOUNT;
   INSERT INTO system_log
               (systemlogid, systemdate, action, username, content, tablename, actionresult, text)
        VALUES (getdataid (), SYSDATE, 'DELETE', $$plsql_unit, 'Rows deleted', 'RM_BINARYDATA', RESULT, 'Housekeeping for RM_BINARYDATA');        
   COMMIT;

   EXCEPTION
     WHEN NO_DATA_FOUND THEN
       NULL;
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;

END PERF_HOUSEKEEPING; 
/


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
