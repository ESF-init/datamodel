SPOOL db_3_232.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.232', 'LWU', 'Added EXPORTGUID to VDV_TXTRANSACTION.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE VDV_TXTRANSACTION
ADD EXTERNALIDENTIFIER1 VARCHAR(50);
ALTER TABLE VDV_TXTRANSACTION
ADD EXTERNALIDENTIFIER2 VARCHAR(50);

-- =======[ Commit ]===============================================================================================

COMMENT ON COLUMN VDV_TXTRANSACTION.EXTERNALIDENTIFIER1 IS 'GUID Generated for VDV Export';
COMMENT ON COLUMN VDV_TXTRANSACTION.EXTERNALIDENTIFIER2 IS 'GUID Generated for VDV Export';
COMMIT;

PROMPT Done!

SPOOL OFF;