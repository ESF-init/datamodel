SPOOL db_3_546.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.546', 'FLF', 'Add new EMV related fields');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CARD
 ADD (PAYMENTACCOUNTREFERENCE  NVARCHAR2(50));
COMMENT ON COLUMN SL_CARD.PAYMENTACCOUNTREFERENCE IS 'Unique reference over several PANs of the same account.';

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (AMOUNTREFUNDED  NUMBER(9));
COMMENT ON COLUMN SL_CREDITCARDAUTHORIZATION.AMOUNTREFUNDED IS 'Amount already refunded of the current auth.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
