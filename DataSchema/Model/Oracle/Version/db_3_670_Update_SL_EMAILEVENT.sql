SPOOL db_3_670.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.670', 'SLR', 'Adding new columns to define event notification receiver(s).');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_EMAILEVENT ADD SENDTOCARDHOLDER NUMBER(1) DEFAULT 0 NOT NULL;
COMMENT ON COLUMN SL_EMAILEVENT.SENDTOCARDHOLDER IS 'Indicates the event is sent to the email address stored in the cardholder of the card. SL_CARD.PERSON.EMAIL.';

ALTER TABLE SL_EMAILEVENT ADD SENDTOANONYMOUSCUSTOMERACCS NUMBER(1) DEFAULT -1 NOT NULL;
COMMENT ON COLUMN SL_EMAILEVENT.SENDTOANONYMOUSCUSTOMERACCS IS 'Indicates the event is sent to any anonymous customer account (Association Type IS NULL or undefined [0]). SL_CARD.CARDTOCONTRACT.ASSOCIATIONTYPE.';

ALTER TABLE SL_EMAILEVENT ADD SENDTOPRIMARYCUSTOMERACCOUNT NUMBER(1) DEFAULT 0 NOT NULL;
COMMENT ON COLUMN SL_EMAILEVENT.SENDTOPRIMARYCUSTOMERACCOUNT IS 'Indicates the event is sent to customer account with Primary Association Type only. SL_CARD.CARDTOCONTRACT.ASSOCIATIONTYPE.';

ALTER TABLE SL_EMAILEVENT ADD SENDTOLOADONLYCUSTOMERACCOUNTS NUMBER(1) DEFAULT 0 NOT NULL;
COMMENT ON COLUMN SL_EMAILEVENT.SENDTOLOADONLYCUSTOMERACCOUNTS IS 'Indicates the event is sent to any customer account with Load-Only Association Type. SL_CARD.CARDTOCONTRACT.ASSOCIATIONTYPE.';

ALTER TABLE SL_EMAILEVENT ADD SENDTOEXECUTIVECUSTOMERACCOUNT NUMBER(1) DEFAULT 0 NOT NULL;
COMMENT ON COLUMN SL_EMAILEVENT.SENDTOCARDHOLDER IS 'Indicates the event is sent to customer account assigned to the execution process (i.e. registered customer is placing an order).';


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
