SPOOL db_3_384.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.384', 'FRD', 'ClearingHouse Apportionment');


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE CH_APPORTIONMENT
(
	APPORTIONMENTID			NUMBER(18,0)	NOT NULL,
	PERIODSTARTDATE			DATE,
	PERIODENDDATE			DATE,
	CREATIONDATE			DATE			DEFAULT sysdate,
	LASTUSER				NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
	LASTMODIFIED			DATE			DEFAULT sysdate		NOT NULL,
	TRANSACTIONCOUNTER		INTEGER								NOT NULL,
	DATAROWCREATIONDATE		DATE			DEFAULT sysdate
);

COMMENT ON TABLE CH_APPORTIONMENT IS 'Contains information on the apportionment runs which have been processed. This table is used in the ClearingHouse module.';
COMMENT ON COLUMN CH_APPORTIONMENT.APPORTIONMENTID IS 'Id of the apportionment run.';
COMMENT ON COLUMN CH_APPORTIONMENT.PERIODSTARTDATE IS	'Start date of the apportionment period.';
COMMENT ON COLUMN CH_APPORTIONMENT.PERIODENDDATE IS 'End date of the apportionment period.';
COMMENT ON COLUMN CH_APPORTIONMENT.CREATIONDATE IS 'Date when the apportionment was started.';
COMMENT ON COLUMN CH_APPORTIONMENT.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN CH_APPORTIONMENT.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN CH_APPORTIONMENT.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN CH_APPORTIONMENT.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

ALTER TABLE CH_APPORTIONMENT ADD (CONSTRAINT PK_APPORTIONMENT PRIMARY KEY (APPORTIONMENTID));



CREATE TABLE CH_APPORTIONMENTRESULT
(
	APPORTIONMENTRESULTID		NUMBER(18,0)	NOT NULL,
	APPORTIONMENTID				NUMBER(18,0)	NOT NULL,
	DIRECTION					NUMBER(10,0)	NOT NULL,
	JOURNEYREF					NUMBER(10,0)	NOT NULL,
	FROMTRANSACTIONID			NUMBER(10,0)	NOT NULL,
	TOTRANSACTIONID				NUMBER(10,0)	NOT NULL,
	CARDID                		NUMBER(10,0)    NOT NULL,
	AMOUNT                 		NUMBER(18,0),
	FROMCLIENTID				NUMBER(18,0)	NOT NULL,
	TOCLIENTID					NUMBER(18,0)	NOT NULL,
	ACQUIRERID   				NUMBER(18,0),
	TIKNO						NUMBER(10,0),
	TICKETID					NUMBER(10,0),
	LASTUSER					NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
	LASTMODIFIED				DATE			DEFAULT sysdate		NOT NULL,
	TRANSACTIONCOUNTER			INTEGER								NOT NULL,
	DATAROWCREATIONDATE			DATE			DEFAULT sysdate
);

COMMENT ON TABLE CH_APPORTIONMENTRESULT IS 'Results of the different apportionment runs.';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.APPORTIONMENTRESULTID IS 'Id of the apportionment result.';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.APPORTIONMENTID IS 'Id of the apportionment run (references CH_APPORTIONMENT).';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.DIRECTION IS 'Receiver of the apportioned value';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.JOURNEYREF	IS 'Journey Idenfifier';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.FROMTRANSACTIONID IS 'Start transactionId associated with the journey (references RM_TRANSACTION)';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.TOTRANSACTIONID IS 'End TransactionId associated with the journey (references RM_TRANSACTION)';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.CARDID IS 'CardId associated with the journey (references RM_TRANSACTION)';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.AMOUNT IS 'Apportioned cost of journey leg';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.FROMCLIENTID IS 'Sending council associated with apportioned journey leg (references RM_TRANSACTION)';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.TOCLIENTID IS 'Receiving council associated with apportioned journey leg (references RM_TRANSACTION)';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.ACQUIRERID IS 'Operator associated with apportioned journey leg (references RM_TRANSACTION.CLIENTID)';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.TIKNO IS 'ticket number associated with apportioned journey leg for Institution apportioned fare (references RM_TRANSACTION)';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.TICKETID IS 'ticket id associated with apportioned journey leg for (references TM_TICKET)';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN CH_APPORTIONMENTRESULT.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

ALTER TABLE CH_APPORTIONMENTRESULT ADD (CONSTRAINT PK_APPORTIONMENTRESULT PRIMARY KEY (APPORTIONMENTRESULTID));
ALTER TABLE CH_APPORTIONMENTRESULT ADD (CONSTRAINT FK_APPORTIONMENT_APPORTID FOREIGN KEY (APPORTIONMENTID) REFERENCES CH_APPORTIONMENT(APPORTIONMENTID));
ALTER TABLE CH_APPORTIONMENTRESULT ADD (CONSTRAINT PK_APPORTIONMENT_TOTRANSID FOREIGN KEY (TOTRANSACTIONID) REFERENCES RM_TRANSACTION(TRANSACTIONID));
ALTER TABLE CH_APPORTIONMENTRESULT ADD (CONSTRAINT PK_APPORTIONMENT_FROMTRANSID FOREIGN KEY (FROMTRANSACTIONID) REFERENCES RM_TRANSACTION(TRANSACTIONID));
ALTER TABLE CH_APPORTIONMENTRESULT ADD (CONSTRAINT PK_APPORTIONMENT_TOCLIENTID FOREIGN KEY (TOCLIENTID) REFERENCES VARIO_CLIENT(CLIENTID));
ALTER TABLE CH_APPORTIONMENTRESULT ADD (CONSTRAINT PK_APPORTIONMENT_FROMCLIENTID FOREIGN KEY (FROMCLIENTID) REFERENCES VARIO_CLIENT(CLIENTID));
ALTER TABLE CH_APPORTIONMENTRESULT ADD (CONSTRAINT PK_APPORTIONMENT_ACQUIRERID FOREIGN KEY (ACQUIRERID) REFERENCES VARIO_CLIENT(CLIENTID));
ALTER TABLE CH_APPORTIONMENTRESULT ADD (CONSTRAINT PK_APPORTIONMENT_CARDID FOREIGN KEY (CARDID) REFERENCES SL_CARD(CARDID));
ALTER TABLE CH_APPORTIONMENTRESULT ADD (CONSTRAINT PK_APPORTIONMENT_TICKETID FOREIGN KEY (TICKETID) REFERENCES TM_TICKET(TICKETID));


-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(5) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'CH_APPORTIONMENT','CH_APPORTIONMENTRESULT');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/


-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(5) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'CH_APPORTIONMENT','CH_APPORTIONMENTRESULT');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/


-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
