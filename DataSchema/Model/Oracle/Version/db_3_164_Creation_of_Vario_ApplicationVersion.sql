SPOOL db_3_164.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.164', 'SOE', 'Creation of Vario_ApplicationVersion');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE VARIO_APPLICATIONVERSION
(
	APPLICATIONVERSIONID   NUMBER(18,0)    NOT NULL,
    APPLICATIONID   NUMBER(18,0)    NOT NULL,
    VERSION    NVARCHAR2(215)  NOT NULL,
	APPLICATIONDATE  DATE DEFAULT sysdate NOT NULL,
	WORKSTATION    NVARCHAR2(215)  NOT NULL
);

ALTER TABLE VARIO_APPLICATIONVERSION ADD CONSTRAINT PK_APPLICATIONVERSIONID PRIMARY KEY (APPLICATIONVERSIONID);
ALTER TABLE VARIO_APPLICATIONVERSION ADD CONSTRAINT FK_APPVERSION_APPLICATION FOREIGN KEY (APPLICATIONID) REFERENCES VARIO_APPLICATION (APPLICATIONID);

COMMENT ON TABLE VARIO_APPLICATIONVERSION is 'Table containing additional information about application version';
COMMENT ON COLUMN VARIO_APPLICATIONVERSION.APPLICATIONVERSIONID is 'Unique identifier of the applicationversion';
COMMENT ON COLUMN VARIO_APPLICATIONVERSION.APPLICATIONID is 'ID of the application';
COMMENT ON COLUMN VARIO_APPLICATIONVERSION.VERSION IS 'Version of the application';
COMMENT ON COLUMN VARIO_APPLICATIONVERSION.APPLICATIONDATE IS 'Start date of first installed application';
COMMENT ON COLUMN VARIO_APPLICATIONVERSION.WORKSTATION IS 'Workstation name of first installed application';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================


-- =======[ Trigger ]==============================================================================================


-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
