SPOOL db_3_551.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.551', 'SVR', 'Add view VIEW_DWH_ACC_SETTLEDREVENUE as data warehouse source.');

-- Start adding schema changes here

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW VIEW_DWH_ACC_SETTLEDREVENUE
(
    REVENUERECOGNITIONID,
    REVENUESETTLEMENTID,
    LASTMODIFIED
)
AS
    SELECT ACC_SETTLEDREVENUE.REVENUERECOGNITIONID,
           ACC_SETTLEDREVENUE.REVENUESETTLEMENTID,
           ACC_REVENUESETTLEMENT.LASTMODIFIED
      FROM ACC_SETTLEDREVENUE, ACC_REVENUESETTLEMENT
     WHERE ACC_SETTLEDREVENUE.REVENUESETTLEMENTID =
           ACC_REVENUESETTLEMENT.REVENUESETTLEMENTID;

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
