SPOOL db_3_110.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.110', 'FLF', 'New column DESCRIPTION in TM_FARESTAGELIST');

-- Start adding schema changes here

ALTER TABLE TM_FARESTAGELIST
 ADD (DESCRIPTION  VARCHAR2(300));
 
 comment on column TM_FARESTAGELIST.DESCRIPTION is 'Description of the fare stage list';

---End adding schema changes 


COMMIT;

PROMPT Done!

SPOOL OFF;
