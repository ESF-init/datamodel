SPOOL db_3_500.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.500', 'MEA', 'Added EXPORTSATE column to SL_Product and SL_card');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PRODUCT ADD EXPORTSATE NUMBER(1) DEFAULT 0 NOT NULL;
COMMENT ON COLUMN SL_PRODUCT.EXPORTSATE IS 'ID of the vdv export state of the Berechtigung TXSAUFB';

ALTER TABLE SL_Card ADD EXPORTSATE NUMBER(1) DEFAULT (0) NOT NULL;
COMMENT ON COLUMN SL_Card.EXPORTSATE IS 'ID of the vdv export state of the Applikation in card TXSAUFA';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
