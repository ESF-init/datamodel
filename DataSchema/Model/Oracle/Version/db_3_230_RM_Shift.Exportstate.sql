SPOOL db_3_230.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.230', 'COS', 'RM_Shift.Exportstate');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE RM_Shift
MODIFY EXPORTSTATE NUMBER(18);

COMMENT ON COLUMN RM_Shift.Exportstate IS 'Set of bits representing the export state';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
