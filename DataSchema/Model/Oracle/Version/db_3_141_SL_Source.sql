SPOOL db_3_141.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.141', 'SOE', 'Added new value 7 (StudentManagement) in SL_Source');

-- -------[ Enumerations ]------------------------------------------------------------------------------------------
Insert into SL_SOURCE
   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values
   (7, 'StudentManagement', 'StudentManagement');

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
