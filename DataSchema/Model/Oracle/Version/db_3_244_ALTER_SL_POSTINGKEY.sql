SPOOL db_3_244.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.244', 'SOE', 'Added IsVisibleInAccount to SL_POSTINGKEY.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_POSTINGKEY
ADD IsVisibleInAccount Number(1);

-- =======[ Commit ]===============================================================================================

COMMENT ON COLUMN SL_POSTINGKEY.IsVisibleInAccount IS 'IsVisibleInAccount for postingkey';
COMMIT;

PROMPT Done!

SPOOL OFF;
