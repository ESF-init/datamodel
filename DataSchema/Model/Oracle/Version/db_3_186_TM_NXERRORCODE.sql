SPOOL db_3_186.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.186', 'FLF', 'Add new TABLE TM_NXERRORCODE');

-- Start adding schema changes here


-- =======[ New Tables ]===========================================================================================

CREATE TABLE TM_NXERRORCODE
(
  ID    NUMBER(9)                               NOT NULL,
  NAME  VARCHAR2(300)                           NOT NULL
);

COMMENT ON COLUMN TM_NXERRORCODE.ID IS 'NX PROX Error Code ID';

COMMENT ON COLUMN TM_NXERRORCODE.NAME IS 'PROX Error code name';


ALTER TABLE TM_NXERRORCODE ADD (
  CONSTRAINT TM_NXERRORCODE_PK
  PRIMARY KEY
  (ID));

  
-- =======[ Changes to Existing Tables ]===========================================================================
  
ALTER TABLE TM_BUSINESSRULERESULT
 ADD (NXERRORCODE  NUMBER(18));
 
COMMENT ON COLUMN TM_BUSINESSRULERESULT.NXERRORCODE IS 'NX PROX Error Code ID';
 

  
-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
