SPOOL db_3_425.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.425', 'SVR', 'Add column ISBROKEN to RM_SHIFT');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE RM_SHIFT ADD (ISBROKEN NUMBER(1));

COMMENT ON COLUMN RM_SHIFT.ISBROKEN IS 'Flag if shift is broken.';


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
