SPOOL db_3_525.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.525', 'MMB', 'Add column into SL_ChannelToPaymentMethod.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CHANNELTOPAYMENTMETHOD ADD USAGE NUMBER(9) DEFAULT 1 NOT NULL;
COMMENT ON COLUMN SL_CHANNELTOPAYMENTMETHOD.USAGE IS 'Payment method usage';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;