SPOOL db_3_383.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.383', 'LJU', 'Added Unique Constraint on SL_PromoCode.PromoCodeNumber and increased Qunatity');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PROMOCODE
MODIFY (QUANTITY NUMBER(18));

ALTER TABLE SL_PROMOCODE
ADD CONSTRAINT UK_PROMOCODE_PROMOCODENUMBER UNIQUE (PROMOCODENUMBER);

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
