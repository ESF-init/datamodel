SPOOL db_3_133.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.133', 'EPA', 'Changed SL_AUDITREGISTER to shiftID relation');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
 
ALTER TABLE SL_AUDITREGISTER
DROP
 (
    SHIFTID,
	DEBTORNO,
	DEVICENO,
	DEVICECLASS,
	VEHICLENO
 ); 

ALTER TABLE SL_AUDITREGISTER
ADD
 (
   SHIFTID  VARCHAR2(32) NOT NULL
 );

ALTER TABLE SL_AUDITREGISTER
ADD
 (
    CONSTRAINT FK_AUDITREGISTER_SHIFTID FOREIGN KEY (SHIFTID) REFERENCES RM_SHIFTINVENTORY(HASHVALUE)
 );

CREATE INDEX IDX_SL_AUDITREGISTER_SHIFTID
ON SL_AUDITREGISTER (SHIFTID);

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
