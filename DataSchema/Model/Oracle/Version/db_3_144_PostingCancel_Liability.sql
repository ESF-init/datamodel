SPOOL db_3_144.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.144', 'AMA', 'Accounting: PostingCancel and Liability');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_PostingKey
 ADD 
 (
	Liability Number(9) DEFAULT 1   NOT NULL,
	CancelKey Number(18)
);
COMMENT ON COLUMN SL_PostingKey.Liability IS 'Rrepresents the liability value of this key. Expected values 1-12';
COMMENT ON COLUMN SL_PostingKey.CancelKey IS 'Represents the key that can be used to cancel postings of this key type. If this value is NULL, postings cannot be canceled';

ALTER TABLE SL_Posting
 ADD 
 (
	CancelPostingID Number(18)
 );
COMMENT ON COLUMN SL_Posting.CancelPostingID IS 'Represents the posting that used to cancel the current posting. If the value of this field is not NULL, means that the posting was canceled';

 
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
