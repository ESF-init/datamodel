SPOOL db_3_155.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.155', 'MIW', 'New tables for reconciliation/validation');

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_VALIDATIONRESULTRESOLVETYPE
(
  ENUMERATIONVALUE  NUMBER(9)     NOT NULL,
  LITERAL           NVARCHAR2(50) NOT NULL,
  DESCRIPTION       NVARCHAR2(50) NOT NULL
);

ALTER TABLE SL_VALIDATIONRESULTRESOLVETYPE ADD CONSTRAINT PK_VALIDATIONRESULTRESOLVETYPE PRIMARY KEY (ENUMERATIONVALUE);

COMMENT ON TABLE SL_VALIDATIONRESULTRESOLVETYPE IS 'This table contains a list of validation rule resolve types.';

COMMENT ON COLUMN SL_VALIDATIONRESULTRESOLVETYPE.ENUMERATIONVALUE IS 'Unique id of the validation rule resolve type.';
COMMENT ON COLUMN SL_VALIDATIONRESULTRESOLVETYPE.LITERAL 		  IS 'Name of the validation rule resolve type. Used internally.';
COMMENT ON COLUMN SL_VALIDATIONRESULTRESOLVETYPE.DESCRIPTION 	  IS 'Description of the validation rule resolve type. Used in the UI.';

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE SL_VALIDATIONRULE
(
  VALIDATIONRULEID       NUMBER(18)     NOT NULL,
  NAME                 	 NVARCHAR2(50)  NOT NULL,
  ISENABLED              NUMBER(1)      DEFAULT 1     	NOT NULL,
  ISAUTORESOLVEDALLOWED  NUMBER(1)      DEFAULT 1       NOT NULL,
  LASTUSER 			     NVARCHAR2(50) 	DEFAULT 'SYS' 	NOT NULL, 
  LASTMODIFIED           DATE 			DEFAULT SYSDATE NOT NULL, 
  TRANSACTIONCOUNTER     INTEGER 		NOT NULL
);

ALTER TABLE SL_VALIDATIONRULE ADD CONSTRAINT PK_VALIDATIONRULE PRIMARY KEY (VALIDATIONRULEID);

COMMENT ON TABLE SL_VALIDATIONRULE IS 'List of rules that are used for completness and correctness validations.';

COMMENT ON COLUMN SL_VALIDATIONRULE.VALIDATIONRULEID 	  IS 'Unique identifier of the validation rule.';
COMMENT ON COLUMN SL_VALIDATIONRULE.NAME 				  IS 'Name of the validation rule.';
COMMENT ON COLUMN SL_VALIDATIONRULE.ISENABLED 			  IS 'Identifies if the validation rule is enabled.';       
COMMENT ON COLUMN SL_VALIDATIONRULE.ISAUTORESOLVEDALLOWED IS 'Identifies if a validation rule violation of this rule can be automatically resolved.';
COMMENT ON COLUMN SL_VALIDATIONRULE.LASTMODIFIED 		  IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_VALIDATIONRULE.TRANSACTIONCOUNTER    IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_VALIDATIONRULE.LASTUSER 			  IS 'Technical field: last (system) user that changed this dataset';


CREATE TABLE SL_VALIDATIONRESULT
(
  VALIDATIONRESULTID  NUMBER(18)        NOT NULL,
  VALIDATIONRULEID    NUMBER(18)        NOT NULL,
  ISVALID             NUMBER(1)         NOT NULL,
  INFO                NVARCHAR2(100)    NOT NULL,
  DETAILS             NVARCHAR2(500),
  CHECKED             DATE              NOT NULL,
  RESOLVEDTYPE        NUMBER(9),
  RESOLVEDCOMMENT     NVARCHAR2(500),
  RESOLVEDUSER        NVARCHAR2(50),
  RESOLVED            DATE,
  LASTUSER 			  NVARCHAR2(50) 	DEFAULT 'SYS' 	NOT NULL, 
  LASTMODIFIED        DATE 				DEFAULT SYSDATE NOT NULL, 
  TRANSACTIONCOUNTER  INTEGER 			NOT NULL
);

ALTER TABLE SL_VALIDATIONRESULT ADD CONSTRAINT PK_VALIDATIONRESULT PRIMARY KEY (VALIDATIONRESULTID);
ALTER TABLE SL_VALIDATIONRESULT ADD CONSTRAINT FK_PK_VALIDATIONRESULT_VALRULE FOREIGN KEY (VALIDATIONRULEID) REFERENCES SL_VALIDATIONRULE (VALIDATIONRULEID);

COMMENT ON TABLE SL_VALIDATIONRESULT IS 'This table contains the resultes for completness and correctness validations.';

COMMENT ON COLUMN SL_VALIDATIONRESULT.VALIDATIONRESULTID IS 'Unique identifier of the validation result.';
COMMENT ON COLUMN SL_VALIDATIONRESULT.ISVALID         	 IS 'Indicates if the validation result was valid.';
COMMENT ON COLUMN SL_VALIDATIONRESULT.INFO            	 IS 'Short description of the result.';
COMMENT ON COLUMN SL_VALIDATIONRESULT.DETAILS            IS 'Detailed description of the result.';
COMMENT ON COLUMN SL_VALIDATIONRESULT.CHECKED            IS 'Date and time when the validation was confirmed.';
COMMENT ON COLUMN SL_VALIDATIONRESULT.VALIDATIONRULEID   IS 'Refernece to the validation rule for this result. SL_VALIDATIONRULE.VALIDATIONRULEID';
COMMENT ON COLUMN SL_VALIDATIONRESULT.RESOLVEDTYPE       IS 'Reference to the validation rule resolve type.  SL_VALIDATIONRESULTRESOLVETYPE.ENUMERATIONVALUE';
COMMENT ON COLUMN SL_VALIDATIONRESULT.RESOLVEDCOMMENT    IS 'Comment how the failed validation was resolved.';
COMMENT ON COLUMN SL_VALIDATIONRESULT.RESOLVEDUSER       IS 'User that has resolved the failed validation.';
COMMENT ON COLUMN SL_VALIDATIONRESULT.RESOLVED           IS 'Date and time when a failed validation was resolved.';
COMMENT ON COLUMN SL_VALIDATIONRESULT.LASTMODIFIED 		 IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_VALIDATIONRESULT.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_VALIDATIONRESULT.LASTUSER 			 IS 'Technical field: last (system) user that changed this dataset';

CREATE TABLE SL_VALIDATIONVALUE
(
  VALIDATIONVALUEID   NUMBER(18)        NOT NULL,
  NAME                NVARCHAR2(150)    NOT NULL,
  VALUE               NVARCHAR2(500)    NOT NULL,
  VALIDATIONRESULTID  NUMBER(18)        NOT NULL,
  LASTUSER 			  NVARCHAR2(50) 	DEFAULT 'SYS' 	NOT NULL, 
  LASTMODIFIED        DATE 				DEFAULT SYSDATE NOT NULL, 
  TRANSACTIONCOUNTER  INTEGER 			NOT NULL
);

ALTER TABLE SL_VALIDATIONVALUE ADD CONSTRAINT PK_VALIDATIONVALUE PRIMARY KEY (VALIDATIONVALUEID);
ALTER TABLE SL_VALIDATIONVALUE ADD CONSTRAINT FK_VALIDATIONVALUE_VALRESULT FOREIGN KEY (VALIDATIONRESULTID) REFERENCES SL_VALIDATIONRESULT (VALIDATIONRESULTID);

COMMENT ON TABLE SL_VALIDATIONVALUE IS 'This table contains additional information and refernces to validation result.';

COMMENT ON COLUMN SL_VALIDATIONVALUE.VALIDATIONVALUEID  IS 'Unique identifier of the validation value.';
COMMENT ON COLUMN SL_VALIDATIONVALUE.NAME  		  		IS 'Name of the validation value.'; 
COMMENT ON COLUMN SL_VALIDATIONVALUE.VALUE         		IS 'Value of the validation value.';
COMMENT ON COLUMN SL_VALIDATIONVALUE.VALIDATIONRESULTID IS 'Reference to the validation result. SL_VALIDATIONRESULT.VALIDATIONRESULTID';
COMMENT ON COLUMN SL_VALIDATIONVALUE.LASTMODIFIED 		IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_VALIDATIONVALUE.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_VALIDATIONVALUE.LASTUSER 			IS 'Technical field: last (system) user that changed this dataset';

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(3) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_VALIDATIONRESULT', 'SL_VALIDATIONRULE', 'SL_VALIDATIONVALUE');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(3) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_VALIDATIONRESULT', 'SL_VALIDATIONRULE', 'SL_VALIDATIONVALUE');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_VALIDATIONRESULTRESOLVETYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (0, 'AutoResolved', 'Check result was resolved automatically by system.');
INSERT INTO SL_VALIDATIONRESULTRESOLVETYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (1, 'ByUser', 'Check result error was resolved by user.');

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
