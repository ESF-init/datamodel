SPOOL db_3_335.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.335', 'EPA', 'Added pan columns to acc_bankstatements');


ALTER TABLE ACC_BANKSTATEMENT ADD MASKEDPAN NVARCHAR2(25);
ALTER TABLE ACC_BANKSTATEMENT ADD SHORTPAN NVARCHAR2(4);
ALTER TABLE ACC_BANKSTATEMENT ADD TRANSACTIONTIME NUMBER(9);

COMMENT ON COLUMN ACC_BANKSTATEMENT.MASKEDPAN IS 'Masked pan of the credit card.';
COMMENT ON COLUMN ACC_BANKSTATEMENT.SHORTPAN IS 'Last four digits of the credit card.';
COMMENT ON COLUMN ACC_BANKSTATEMENT.TRANSACTIONTIME IS 'Second past midnight of the transaction.';


-- =======[ Views ]================================================================================================
CREATE OR REPLACE FORCE VIEW ACC_BANKSTATEMENTVERIFICATION
(
    BANKSTATEMENTID,
    PAYMENTJOURNALID,
    SALEID,
    MATCHEDSTATEMENTS,
    AMOUNT,
    PAYMENTREFERENCE, 
    TRANSACTIONDATE,
    BANKSTATEMENTSTATE,
    PAYMENTSTATE,
	RECORDTYPE,
	RECONCILIATIONSTATE,
	MASKEDPAN,
	SHORTPAN)
AS SELECT 
        j.BANKSTATEMENTID as BANKSTATEMENTID,
        j.PAYMENTJOURNALID as PAYMENTJOURNALID,
        j.SALEID as SALEID,
        (case when j.PAYMENTJOURNALID is null then 0 else COUNT end) as MATCHEDSTATEMENTS, 
        bs.AMOUNT as AMOUNT,
        bs.PAYMENTREFERENCE as PAYMENTREFERENCE, 
        bs.TRANSACTIONDATE	as TRANSACTIONDATE,
        bs.STATE as BANKSTATEMENTSTATE,
        pj.STATE as PAYMENTSTATE,
		bs.RECORDTYPE as RECORDTYPE,
		pj.RECONCILIATIONSTATE as RECONCILIATIONSTATE,
		bs.MASKEDPAN as MASKEDPAN,
		bs.SHORTPAN as SHORTPAN
    from (
       SELECT 
            b.BANKSTATEMENTID as BANKSTATEMENTID, 
            count(b.bankstatementid) as COUNT, 
            min(p.PAYMENTJOURNALID) as PAYMENTJOURNALID, 
            min(s.SALEID) as SALEID
        FROM ACC_BANKSTATEMENT b
        left JOIN SL_PAYMENTJOURNAL p 
        ON  p.Amount = b.Amount and --Payment amount matches.
            --p.PAYMENTTYPE = 2 and
            --p.Code = b.PaymentReference and 
            p.state = 1 and --Payment is of approved state.
            substr(p.MASKEDPAN,-4) = b.SHORTPAN and     --Last 4 digits of PAN matches.
            ABS(TO_NUMBER( TO_CHAR( p.executiontime, 'SSSSS' ) - b.TRANSACTIONTIME)) < 120 --Payment is within +/- two minutes around bankstatement transaction time.
        left JOIN SL_SALE s 
        ON s.SALEID = p.SALEID and (
            (s.SALESCHANNELID = 70 and trunc(b.TRANSACTIONDATE) = trunc(p.executiontime)) OR        -- Transaction date matches
            (s.SALESCHANNELID <> 70 and trunc(b.TRANSACTIONDATE) = trunc(p.executiontime + (1/24*2.997))))  -- Server time corrected date matches.
        group by b.BANKSTATEMENTID
    ) j
    left join SL_PAYMENTJOURNAL pj on pj.PAYMENTJOURNALID = j.PAYMENTJOURNALID
    left join ACC_BANKSTATEMENT bs on bs.BANKSTATEMENTID = j.BANKSTATEMENTID;

-- =======[ Commit ]===============================================================================================
COMMIT;
PROMPT Done!
SPOOL OFF;
