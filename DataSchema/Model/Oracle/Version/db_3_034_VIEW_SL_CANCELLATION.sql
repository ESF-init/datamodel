SPOOL db_3_034.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.034', 'SLR', 'Add new view for sl transaction cancellations');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------
 
-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW view_sl_cancellation (transactionjournalid,
                                                          transactionid,
                                                          devicetime,
                                                          boardingtime,
                                                          operatorid,
                                                          clientid,
                                                          line,
                                                          coursenumber,
                                                          routenumber,
                                                          routecode,
                                                          direction,
                                                          ZONE,
                                                          saleschannelid,
                                                          devicenumber,
                                                          vehiclenumber,
                                                          mountingplatenumber,
                                                          debtornumber,
                                                          geolocationlongitude,
                                                          geolocationlatitude,
                                                          stopnumber,
                                                          transitaccountid,
                                                          faremediaid,
                                                          faremediatype,
                                                          productid,
                                                          ticketid,
                                                          fareamount,
                                                          customergroup,
                                                          transactiontype,
                                                          transactionnumber,
                                                          resulttype,
                                                          filllevel,
                                                          pursebalance,
                                                          pursecredit,
                                                          groupsize,
                                                          validfrom,
                                                          validto,
                                                          DURATION,
                                                          tariffdate,
                                                          tariffversion,
                                                          ticketinternalnumber,
                                                          whitelistversion,
                                                          cancellationreference,
                                                          cancellationreferenceguid,
                                                          lastuser,
                                                          lastmodified,
                                                          transactioncounter,
                                                          whitelistversioncreated,
                                                          properties,
                                                          saleid,
                                                          tripticketinternalnumber,
                                                          shiftbegin,
                                                          completionstate,
                                                          responsestate,
                                                          productid2,
                                                          pursebalance2,
                                                          pursecredit2,
                                                          creditcardauthorizationid,
                                                          tripcounter,
                                                          originalonlineresulttype,
                                                          originaldeviceresulttype
                                                         )
AS
   SELECT tj3.transactionjournalid, tj3.transactionid, tj3.devicetime,
          tj3.boardingtime, tj3.operatorid, tj3.clientid, tj3.line,
          tj3.coursenumber, tj3.routenumber, tj3.routecode, tj3.direction,
          tj3.ZONE, tj3.saleschannelid, tj3.devicenumber, tj3.vehiclenumber,
          tj3.mountingplatenumber, tj3.debtornumber, tj3.geolocationlongitude,
          tj3.geolocationlatitude, tj3.stopnumber, tj3.transitaccountid,
          tj3.faremediaid, tj3.faremediatype, tj3.productid, tj3.ticketid,
          tj3.fareamount, tj3.customergroup, tj3.transactiontype,
          tj3.transactionnumber, tj3.resulttype, tj3.filllevel,
          tj3.pursebalance, tj3.pursecredit, tj3.groupsize, tj3.validfrom,
          tj3.validto, tj3.DURATION, tj3.tariffdate, tj3.tariffversion,
          tj3.ticketinternalnumber, tj3.whitelistversion,
          tj3.cancellationreference, tj3.cancellationreferenceguid,
          tj3.lastuser, tj3.lastmodified, tj3.transactioncounter,
          tj3.whitelistversioncreated, tj3.properties, tj3.saleid,
          tj3.tripticketinternalnumber, tj3.shiftbegin, tj3.completionstate,
          tj3.responsestate, tj3.productid2, tj3.pursebalance2,
          tj3.pursecredit2, tj3.creditcardauthorizationid, tj3.tripcounter,
          tj3.originalonlineresulttype, tj3.originaldeviceresulttype
     FROM sl_transactionjournal tj3
          LEFT JOIN
          (SELECT tj1.transactionjournalid, tj1.transactionid, tj1.devicetime,
                  tj1.boardingtime, tj1.operatorid, tj1.clientid, tj1.line,
                  tj1.coursenumber, tj1.routenumber, tj1.routecode,
                  tj1.direction, tj1.ZONE, tj1.saleschannelid,
                  tj1.devicenumber, tj1.vehiclenumber,
                  tj1.mountingplatenumber, tj1.debtornumber,
                  tj1.geolocationlongitude, tj1.geolocationlatitude,
                  tj1.stopnumber, tj1.transitaccountid, tj1.faremediaid,
                  tj1.faremediatype, tj1.productid, tj1.ticketid,
                  tj1.fareamount, tj1.customergroup, tj1.transactiontype,
                  tj1.transactionnumber, tj1.resulttype, tj1.filllevel,
                  tj1.pursebalance, tj1.pursecredit, tj1.groupsize,
                  tj1.validfrom, tj1.validto, tj1.DURATION, tj1.tariffdate,
                  tj1.tariffversion, tj1.ticketinternalnumber,
                  tj1.whitelistversion, tj1.cancellationreference,
                  tj1.cancellationreferenceguid, tj1.lastuser,
                  tj1.lastmodified, tj1.transactioncounter,
                  tj1.whitelistversioncreated, tj1.properties, tj1.saleid,
                  tj1.tripticketinternalnumber, tj1.shiftbegin,
                  tj1.completionstate, tj1.responsestate, tj1.productid2,
                  tj1.pursebalance2, tj1.pursecredit2,
                  tj1.creditcardauthorizationid, tj1.tripcounter,
                  tj1.originalonlineresulttype, tj1.originaldeviceresulttype
             FROM sl_transactionjournal tj1 LEFT JOIN sl_transactionjournal tj2
                  ON tj2.transactionjournalid = tj1.cancellationreference
            WHERE tj1.transactiontype = 128
              AND tj1.cancellationreference IS NOT NULL) tj4
          ON tj3.transactionjournalid = tj4.transactionjournalid
    WHERE tj4.transactionjournalid IS NULL;


-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
