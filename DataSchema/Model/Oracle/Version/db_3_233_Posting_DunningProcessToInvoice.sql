SPOOL db_3_233.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.233', 'BVI', 'Fixed datatype in SL_Posting.InvoiceID, Added foreign key to DunningProcessToInvoice.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_Posting MODIFY INVOICEID NUMBER(18);
ALTER TABLE SL_DUNNINGTOINVOICE ADD CONSTRAINT FK_DUNPROCTOINV_DunProcess foreign key (DUNNINGPROCESSID) references SL_DUNNINGPROCESS (DUNNINGPROCESSID);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;