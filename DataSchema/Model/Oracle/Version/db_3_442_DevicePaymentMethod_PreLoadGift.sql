SPOOL db_3_xxx.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.442', 'STV', 'Added_DevicePaymentMethod_');

-- Start adding schema changes here

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------
--ULB:: Masterdata Provision for classic tables not by datamodellscripts. 
--  	272 Allready used for other purpose
--		SL_Numerations must be in sync with classic enumeration

-- Insert into RM_DEVICEPAYMENTMETHOD 
--    (DEVICEPAYMENTMETHODID, DESCRIPTION, VISIBLE, EVENDMASK, EXTERNALNUMBER, DISPLAYTEXT)
--  Values
--    (272, 'PreLoadGift', 0, NULL, NULL, NULL);
-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
