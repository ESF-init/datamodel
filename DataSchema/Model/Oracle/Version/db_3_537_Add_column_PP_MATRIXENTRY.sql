SPOOL db_3_537.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.537', 'ARH', 'Add column in PP_MATRIXENTRY');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE PP_MATRIXENTRY ADD (SHORTDISTANCEPOSSIBLE  NUMBER(1, 0) DEFAULT 0 NOT NULL);

COMMENT ON COLUMN PP_MATRIXENTRY.SHORTDISTANCEPOSSIBLE IS 'Flag to know if short distance is possible for this matrix entry.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
