SPOOL db_3_575.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.575', 'DST', 'Make SL_JobCardImport.CardPhysicalDetailID optional');

ALTER TABLE SL_JOBCARDIMPORT MODIFY CARDPHYSICALDETAILID NULL;

COMMIT;

PROMPT Done!

SPOOL OFF;
