SPOOL db_3_614.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.614', 'FLF', 'New index on VARIO_STOP');

-- =======[ Changes to Existing Tables ]===========================================================================

CREATE INDEX IDX_NETWORKSTOP ON VARIO_STOP
(STOPNO, NETID);


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
