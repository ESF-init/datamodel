SPOOL db_3_415.log;
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.415', 'FRD', 'ADD CH_VALIDATIONTRANSACTION Sequence');

-- =======[ Sequences ]============================================================================================

CREATE SEQUENCE CH_VALIDATIONTRANSACTION_SEQ START WITH 1
                                                  MAXVALUE 9999999999999999999999999999
                                                  MINVALUE 1
                                                  NOCYCLE
                                                  CACHE 20
                                                  NOORDER;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;