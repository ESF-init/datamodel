SPOOL db_3_646.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.646', 'BVI', 'Added new column SL_Invoice.UsedPaymentOptionID.');

Alter Table SL_Invoice add UsedPaymentOptionID Number(18);

COMMENT ON COLUMN SL_Invoice.UsedPaymentOptionID IS 'References SL_PaymentOption.PaymentOptionID and indicated the used paymentoption for this invoice instance.';

Alter Table SL_Invoice ADD CONSTRAINT FK_Invoice_PaymentOpt FOREIGN KEY (UsedPaymentOptionID) REFERENCES SL_PaymentOption (PaymentOptionID); 


COMMIT;

PROMPT Done!

SPOOL OFF;
