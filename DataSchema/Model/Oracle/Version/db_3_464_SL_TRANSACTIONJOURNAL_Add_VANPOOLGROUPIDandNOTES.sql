SPOOL db_3_464.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.464', 'MFA', 'Alter table SL_TRANSACTIONJOURNAL, add new column VANPOOLGROUPID, and NOTES.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_TRANSACTIONJOURNAL ADD VANPOOLGROUPID NVARCHAR2(100);
COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.VANPOOLGROUPID IS 'The van pool group id for the transaction.';

ALTER TABLE SL_TRANSACTIONJOURNAL ADD NOTES NVARCHAR2(255);
COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.NOTES IS 'Notes for the transaction.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
