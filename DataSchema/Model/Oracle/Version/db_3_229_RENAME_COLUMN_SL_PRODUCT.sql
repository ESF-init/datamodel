SPOOL db_3_229.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.229', 'SOE', 'Renamed EntitlementID as VdvEntNumber in SL_PRODUCT.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PRODUCT
RENAME COLUMN EntitlementID TO VdvEntNumber;

-- =======[ Commit ]===============================================================================================

COMMENT ON COLUMN SL_PRODUCT.VdvEntNumber IS 'VDV KA card entitlement number';
COMMIT;

PROMPT Done!

SPOOL OFF;