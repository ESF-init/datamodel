SPOOL db_3_149.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.149', 'BVI', 'Creation of SL_DunningToInvoice');

CREATE TABLE SL_DunningToInvoice
(
    DunningToInvoiceID                          NUMBER(18,0)            NOT NULL,
	INVOICEID	                                NUMBER(18,0)            NOT NULL,    	
	DunningProcessID                            NUMBER(18,0)            NOT NULL,
	DUNNINGLEVELID	  							NUMBER(18),		
	DESCRIPTION         						NVARCHAR2(250),
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL
);

alter table SL_DunningToInvoice add CONSTRAINT FK_DUNProcToInv_DUNNINGLEVEL FOREIGN KEY (DUNNINGLEVELID) REFERENCES SL_DUNNINGLEVEL (DUNNINGLEVELID);

alter table SL_DunningToInvoice add CONSTRAINT FK_DUNProcToInv_INVOICE FOREIGN KEY (INVOICEID) REFERENCES SL_Invoice (INVOICEID);

alter table SL_DunningToInvoice add CONSTRAINT PK_DunningToInvoiceID PRIMARY KEY (DunningToInvoiceID);

-- =======[ MODIFY existing tables ]==============================================================================================

alter table SL_DunningProcess drop constraint FK_Dunning_UserList;

alter table SL_DunningProcess drop constraint FK_DunningProcessToInvoice;

alter table SL_DunningProcess drop constraint FK_DUNNING_INVOICE;

alter table SL_DunningProcess drop constraint FK_Dunning_DunningLevel;

alter table SL_DunningProcess drop constraint FK_Dunning_Dunning;

alter table SL_DunningProcess drop column DunningLevelID;

alter table SL_DunningProcess drop column DunningInvoiceID;  

alter table SL_DunningProcess drop column InvoiceID;

alter table SL_DunningProcess drop column USERID;

alter table SL_DunningProcess drop column PROSPECTIVEDUNNINGID;

COMMENT ON TABLE SL_DunningToInvoice is 'Entry for a created dunning in a dunning process.';

COMMENT ON COLUMN SL_DunningToInvoice.DunningToInvoiceID is 'Unique ID of the Dunning Entry';

COMMENT ON COLUMN SL_DunningToInvoice.DUNNINGLEVELID is 'References SL_DUNNINGLEVEL.DUNNINGLEVELID';

COMMENT ON COLUMN SL_DunningToInvoice.INVOICEID is 'A reference to the invoice. References SL_Invoice.INVOICEID';

alter table SL_Invoice add
(
    USERID              						NUMBER(18)
);

COMMENT ON COLUMN SL_Invoice.USERID is 'The system user that generated the action. References USER_LIST.USERID';

alter table SL_Invoice add
(
    CONSTRAINT FK_DunProcToInv_USERLIST FOREIGN KEY (USERID) REFERENCES USER_LIST (USERID)
);

CREATE SEQUENCE SL_DunningToInvoice_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;

-- =======[ Trigger ]==============================================================================================
CREATE OR REPLACE TRIGGER SL_DunningToInvoice_BRI before insert on SL_DunningToInvoice for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_DunningToInvoice_BRU before update on SL_DunningToInvoice for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
