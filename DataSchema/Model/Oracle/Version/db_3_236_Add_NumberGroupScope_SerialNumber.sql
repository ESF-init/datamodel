SPOOL db_3_236.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.236', 'EPA', 'Added a number group scope for temporal serial numbers.');


-- -------[ Enumerations ]-----------------------------------------------------------------------------------------
Insert into SL_NUMBERGROUPSCOPE
   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values
   (18, 'SerialNumbers', 'Temporal Serial Numbers');

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
