SPOOL db_3_373.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.373', 'MEA', 'Rename TMEXPORTSATE column to TMEXPORTSTATE.');
-- Start adding schema changes here
-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE VDV_SAMSTATUS RENAME  COLUMN TMEXPORTSATE TO TMEXPORTSTATE;

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
