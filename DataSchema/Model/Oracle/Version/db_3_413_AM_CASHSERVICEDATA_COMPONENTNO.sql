SPOOL db_3_413.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.413', 'JGI', 'Extend column AM_CASHSERVICEDATA:COMPONENTNO');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE AM_CASHSERVICEDATA MODIFY(COMPONENTNO NUMBER(14));

-- =======[ New Tables ]===========================================================================================

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
