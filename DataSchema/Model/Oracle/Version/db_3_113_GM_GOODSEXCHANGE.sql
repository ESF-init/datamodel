SPOOL db_3_113.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.113', 'FLF', 'Extended max length of PRODUCTID to 20');

-- Start adding schema changes here

ALTER TABLE GM_GOODSEXCHANGE
MODIFY(PRODUCTID NUMBER(20));

---End adding schema changes 


COMMIT;

PROMPT Done!

SPOOL OFF;
