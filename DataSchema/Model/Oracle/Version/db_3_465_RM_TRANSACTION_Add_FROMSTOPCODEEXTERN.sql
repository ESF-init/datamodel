SPOOL db_3_465.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.465', 'JGI', 'Alter table RM_TRANSACTION, add new column FROMSTOPCODEEXTERN.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE RM_TRANSACTION ADD FROMSTOPCODEEXTERN VARCHAR2(100);
COMMENT ON COLUMN RM_TRANSACTION.FROMSTOPCODEEXTERN IS 'Stores the code for a stop from an external third party tool';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
