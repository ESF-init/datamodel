SPOOL db_3_095.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.095', 'STV', 'Add column PAYMENTINTERVALID to SL_ORDERDETAIL');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_ORDERDETAIL ADD (PAYMENTINTERVALID NUMBER(9) NULL);

-- =======[ Comment ]===========================================================================================

COMMENT ON COLUMN SL_ORDERDETAIL.PAYMENTINTERVALID IS 'References TM_PAYMENTINTERVAL.PAYMENTINTERVALID';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
