SPOOL db_3_187.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.187', 'FLF', 'Add distance to sl_product and sl_transactionjournal');

-- Start adding schema changes here


-- =======[ New Tables ]===========================================================================================
  
-- =======[ Changes to Existing Tables ]===========================================================================
  
ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (DISTANCE  NUMBER(9));

COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.DISTANCE IS 'Either temporal or local distance of product';


ALTER TABLE SL_PRODUCT
 ADD (DISTANCE  NUMBER(9));

COMMENT ON COLUMN SL_PRODUCT.DISTANCE IS 'Either temporal or local distance';

  
-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
