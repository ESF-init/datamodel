SPOOL db_3_182.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.182', 'DWD', 'Add Card event entry for Presentation');

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------
INSERT INTO SL_CARDEVENTTYPE
(
  ENUMERATIONVALUE,
  LITERAL,
  DESCRIPTION
)
VALUES
(
  19,
  N'Presentation',
  N'Presentation'
);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
