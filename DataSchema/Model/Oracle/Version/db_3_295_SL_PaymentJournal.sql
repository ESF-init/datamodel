SPOOL db_3_295.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.295', 'DST', 'Added WalletTransactionID column.');

alter table sl_paymentjournal add WalletTransactionID NVARCHAR2(150);

COMMENT ON COLUMN sl_paymentjournal.WalletTransactionID IS 'External transaction identifier for wallet payments.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
