SPOOL db_3_628.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.628', 'WBO', 'Updated SL_Emailevent AutoloadTransferred');


-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

UPDATE SL_EmailEvent SET EnumerationValue = 85, TransactionCounter = TransactionCounter +1 where EventName = 'AutoloadTransferred';

COMMIT;

PROMPT Done!

SPOOL OFF;
