SPOOL db_3_624.log;


------------------------------------------------------------------------------
--Version 3.624
------------------------------------------------------------------------------
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.624', 'JSB', 'Create view VIEW_UM_FORCELOAD');


-- -------[ New View ]------------------------------------------------------------------------------------------

CREATE OR REPLACE FORCE VIEW view_um_forceloads (unitid,
                                                 unitno,
                                                 deviceno,
                                                 unitcompleted,
                                                 archivejobid,
                                                 deviceclassid
                                                )
AS
   SELECT DISTINCT u.unitid, u.visibleunitno AS unitno,
                   NVL (d.deviceno, 0) AS deviceno,
                   MAX (udt.completed) AS unitcompleted, j.archivejobid,
                   j.deviceclassid
              FROM um_unit u INNER JOIN um_unitdistributiontask udt
                   ON u.unitid = udt.unitid
                   INNER JOIN um_archivedistributiontask adt
                   ON udt.archivedistributiontaskid =
                                                 adt.archivedistributiontaskid
                   INNER JOIN um_archivejob j
                   ON j.archivejobid = adt.archivejobid
                   LEFT OUTER JOIN um_device d ON u.unitid = d.unitid
             WHERE j.typeofarchivejobid = 3                  -- Forceload Jobs
          GROUP BY u.unitid,
                   u.visibleunitno,
                   d.deviceno,
                   j.archivejobid,
                   j.deviceclassid;
				   
COMMIT;

PROMPT Done!

SPOOL OFF;
