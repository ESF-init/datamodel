SPOOL db_3_380.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.380', 'STV', 'Add_Column_Stop_Number_To_SL_PRODUCTRELATION');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PRODUCTRELATION
ADD StopNumber NUMBER(10,0) null;

COMMENT ON COLUMN SL_PRODUCTRELATION.StopNumber IS 'References VARIO_STOP Stop number';

COMMIT;

PROMPT Done!

SPOOL OFF;