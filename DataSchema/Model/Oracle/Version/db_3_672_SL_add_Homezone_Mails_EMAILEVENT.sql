SPOOL db_3_672.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.672', 'LGA', 'Add HomeZoneEmails to SL_EmailEvent');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================



INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'HomeZoneBillingMail','Mail is sended after buying a HomeZone', 0, 'HomeZoneExpirationMailMerge', 105);

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'HomeZoneCancel','Mail is sended after an HomeZone is canceled', 0, 'HomeZoneExpirationMailMerge', 106);



---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
