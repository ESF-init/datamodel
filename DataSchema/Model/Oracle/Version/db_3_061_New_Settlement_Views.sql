SPOOL db_3_061.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '3.061', 'BAW', 'Views to support two settlement processes in parallel.');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE ACC_RevenueRecognition ADD (IncludeInSettlement NUMBER(1));

COMMENT ON COLUMN ACC_RevenueRecognition.IncludeInSettlement
IS 'Flag indicating whether the revenue transaction should be included in a following settlement run. If NULL, application code will determine based on tariff configuration (fallback to support transition phase).';

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================
CREATE OR REPLACE VIEW ACC_RECOGNIZED_TAP_CML_SETTLED
(revenuerecognitionid, revenuesettlementid, settlementtype, amount, premium, basefare,
 ticketnumber, customergroup, linegroupid, operatorid, creditaccountnumber,
 debitaccountnumber, postingdate, postingreference, includeinsettlement, closeoutperiodid, periodfrom,
 periodto, closeouttype, state, transactionjournalid, transitaccountid, devicetime,
 fareamount, line, ticketinternalnumber, ticketid, transactiontype, clientid,
 transactionoperatorid, saleschannelid, resulttype, cancellationreferenceguid,
 cancellationreference, productid, pursebalance, pursecredit, tariffdate, tariffversion,
 tripticketinternalnumber, cancelledtap)
AS
SELECT RR.revenuerecognitionid,
       RS.revenuesettlementid,
       RS.settlementtype,
	   RR.amount,
       RR.premium,
       RR.basefare,
       RR.ticketnumber,
       RR.customergroup,
       RR.linegroupid,
       RR.operatorid,
       RR.creditaccountnumber,
       RR.debitaccountnumber,
       RR.postingdate,
       RR.postingreference,
	   RR.includeinsettlement,
       CP.closeoutperiodid,
       CP.periodfrom,
       CP.periodto,
       CP.closeouttype,
       CP.state,
       TJ1.transactionjournalid,
       TJ1.transitaccountid,
       TJ1.devicetime,
       TJ1.fareamount,
       TJ1.line,
       TJ1.ticketinternalnumber,
       TJ1.ticketid,
       TJ1.transactiontype,
       TJ1.clientid,
       TJ1.operatorid AS transactionoperatorid,
       TJ1.saleschannelid,
       TJ1.resulttype,
       TJ1.cancellationreferenceguid,
       TJ1.cancellationreference,
       TJ1.productid,
       TJ1.pursebalance,
       TJ1.pursecredit,
       TJ1.tariffdate,
       TJ1.tariffversion,
       TJ1.tripticketinternalnumber,
       Cast (Decode (TJ1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)) AS cancelledtap
FROM acc_revenuerecognition RR
    LEFT JOIN (
        SELECT MIN(SR1.revenuesettlementid) as settlementid, revenuerecognitionid
        FROM acc_settledrevenue SR1
        INNER JOIN acc_revenuesettlement s1 ON S1.revenuesettlementid = SR1.revenuesettlementid
        WHERE S1.SETTLEMENTTYPE = 1 --CML based
        GROUP BY revenuerecognitionid
    ) SR ON SR.revenuerecognitionid = RR.revenuerecognitionid
    LEFT JOIN acc_revenuesettlement RS
        ON RS.revenuesettlementid = SR.settlementid AND SETTLEMENTTYPE = 1 --CML based
    INNER JOIN sl_transactionjournal TJ1
         ON TJ1.transactionjournalid = RR.transactionjournalid
    INNER JOIN acc_closeoutperiod CP
                ON CP.closeoutperiodid = RR.closeoutperiodid
WHERE TJ1.VALIDTO IS NULL AND (TJ1.TRANSACTIONTYPE = 66 OR TJ1.TRANSACTIONTYPE = 84);

CREATE OR REPLACE VIEW ACC_RECOGNIZED_TAP_TAB_SETTLED
(revenuerecognitionid, revenuesettlementid, settlementtype, amount, premium, basefare,
 ticketnumber, customergroup, linegroupid, operatorid, creditaccountnumber,
 debitaccountnumber, postingdate, postingreference, includeinsettlement, closeoutperiodid, periodfrom,
 periodto, closeouttype, state, transactionjournalid, transitaccountid, devicetime,
 fareamount, line, ticketinternalnumber, ticketid, transactiontype, clientid,
 transactionoperatorid, saleschannelid, resulttype, cancellationreferenceguid,
 cancellationreference, productid, pursebalance, pursecredit, tariffdate, tariffversion,
 tripticketinternalnumber, cancelledtap)
AS
SELECT RR.revenuerecognitionid,
       RS.revenuesettlementid,
       RS.settlementtype,
	   RR.amount,
       RR.premium,
       RR.basefare,
       RR.ticketnumber,
       RR.customergroup,
       RR.linegroupid,
       RR.operatorid,
       RR.creditaccountnumber,
       RR.debitaccountnumber,
       RR.postingdate,
       RR.postingreference,
	   RR.includeinsettlement,
       CP.closeoutperiodid,
       CP.periodfrom,
       CP.periodto,
       CP.closeouttype,
       CP.state,
       TJ1.transactionjournalid,
       TJ1.transitaccountid,
       TJ1.devicetime,
       TJ1.fareamount,
       TJ1.line,
       TJ1.ticketinternalnumber,
       TJ1.ticketid,
       TJ1.transactiontype,
       TJ1.clientid,
       TJ1.operatorid AS transactionoperatorid,
       TJ1.saleschannelid,
       TJ1.resulttype,
       TJ1.cancellationreferenceguid,
       TJ1.cancellationreference,
       TJ1.productid,
       TJ1.pursebalance,
       TJ1.pursecredit,
       TJ1.tariffdate,
       TJ1.tariffversion,
       TJ1.tripticketinternalnumber,
       Cast (Decode (TJ1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)) AS cancelledtap
FROM acc_revenuerecognition RR
    LEFT JOIN (
        SELECT MIN(SR1.revenuesettlementid) as settlementid, revenuerecognitionid
        FROM acc_settledrevenue SR1
        INNER JOIN acc_revenuesettlement s1 ON S1.revenuesettlementid = SR1.revenuesettlementid
        WHERE S1.SETTLEMENTTYPE = 0 --TAB based
        GROUP BY revenuerecognitionid
    ) SR ON SR.revenuerecognitionid = RR.revenuerecognitionid
    LEFT JOIN acc_revenuesettlement RS
        ON RS.revenuesettlementid = SR.settlementid AND SETTLEMENTTYPE = 0 --TAB based
    INNER JOIN sl_transactionjournal TJ1
         ON TJ1.transactionjournalid = RR.transactionjournalid
    INNER JOIN acc_closeoutperiod CP
                ON CP.closeoutperiodid = RR.closeoutperiodid
WHERE TJ1.VALIDTO IS NULL AND (TJ1.TRANSACTIONTYPE = 66 OR TJ1.TRANSACTIONTYPE = 84);

CREATE OR REPLACE VIEW ACC_CANCEL_REC_TAP_TAB_SETTLED
(revenuerecognitionid, revenuesettlementid, settlementtype, amount, premium, basefare,
 ticketnumber, customergroup, linegroupid, operatorid, creditaccountnumber,
 debitaccountnumber, postingdate, postingreference, includeinsettlement, closeoutperiodid, periodfrom,
 periodto, closeouttype, state, transactionjournalid, transitaccountid, devicetime,
 fareamount, line, ticketinternalnumber, ticketid, transactiontype, clientid,
 transactionoperatorid, saleschannelid, resulttype, cancellationreferenceguid,
 cancellationreference, productid, pursebalance, pursecredit, tariffdate, tariffversion,
 tripticketinternalnumber)
AS
SELECT RR.revenuerecognitionid,
       RS.revenuesettlementid,
       RS.settlementtype,
       RR.amount,
       RR.premium,
       RR.basefare,
       RR.ticketnumber,
       RR.customergroup,
       RR.linegroupid,
       RR.operatorid,
       RR.creditaccountnumber,
       RR.debitaccountnumber,
       RR.postingdate,
       RR.postingreference,
	   RR.includeinsettlement,
       CP.closeoutperiodid,
       CP.periodfrom,
       CP.periodto,
       CP.closeouttype,
       CP.state,
       TJ1.transactionjournalid,
       TJ1.transitaccountid,
       TJ1.devicetime,
       TJ1.fareamount,
       TJ1.line,
       TJ1.ticketinternalnumber,
       TJ1.ticketid,
       TJ1.transactiontype,
       TJ1.clientid,
       TJ1.operatorid AS transactionoperatorid,
       TJ1.saleschannelid,
       TJ1.resulttype,
       TJ1.cancellationreferenceguid,
       TJ1.cancellationreference,
       TJ1.productid,
       TJ1.pursebalance,
       TJ1.pursecredit,
       TJ1.tariffdate,
       TJ1.tariffversion,
       TJ1.tripticketinternalnumber
FROM acc_revenuerecognition RR
    LEFT JOIN (
        SELECT MIN(SR1.revenuesettlementid) as settlementid, revenuerecognitionid
        FROM acc_settledrevenue SR1
        INNER JOIN acc_revenuesettlement s1 ON S1.revenuesettlementid = SR1.revenuesettlementid
        WHERE S1.SETTLEMENTTYPE = 0 --TAB based
        GROUP BY revenuerecognitionid
    ) SR ON SR.revenuerecognitionid = RR.revenuerecognitionid
    LEFT JOIN acc_revenuesettlement RS
        ON RS.revenuesettlementid = SR.settlementid AND SETTLEMENTTYPE = 0 --TAB based
    INNER JOIN sl_transactionjournal TJ1
         ON TJ1.transactionjournalid = RR.transactionjournalid
    INNER JOIN acc_closeoutperiod CP
                ON CP.closeoutperiodid = RR.closeoutperiodid
	LEFT JOIN SL_TRANSACTIONJOURNAL TJ2 ON TJ2.TRANSACTIONJOURNALID = TJ1.CANCELLATIONREFERENCE
WHERE TJ1.TRANSACTIONTYPE = 128 AND (TJ2.VALIDTO IS NULL AND (TJ2.TRANSACTIONTYPE = 66 OR TJ2.TRANSACTIONTYPE = 84));

CREATE OR REPLACE VIEW ACC_CANCEL_REC_TAP_CML_SETTLED
(revenuerecognitionid, revenuesettlementid, settlementtype, amount, premium, basefare,
 ticketnumber, customergroup, linegroupid, operatorid, creditaccountnumber,
 debitaccountnumber, postingdate, postingreference, includeinsettlement, closeoutperiodid, periodfrom,
 periodto, closeouttype, state, transactionjournalid, transitaccountid, devicetime,
 fareamount, line, ticketinternalnumber, ticketid, transactiontype, clientid,
 transactionoperatorid, saleschannelid, resulttype, cancellationreferenceguid,
 cancellationreference, productid, pursebalance, pursecredit, tariffdate, tariffversion,
 tripticketinternalnumber)
AS
SELECT RR.revenuerecognitionid,
       RS.revenuesettlementid,
       RS.settlementtype,
       RR.amount,
       RR.premium,
       RR.basefare,
       RR.ticketnumber,
       RR.customergroup,
       RR.linegroupid,
       RR.operatorid,
       RR.creditaccountnumber,
       RR.debitaccountnumber,
       RR.postingdate,
       RR.postingreference,
	   RR.includeinsettlement,
       CP.closeoutperiodid,
       CP.periodfrom,
       CP.periodto,
       CP.closeouttype,
       CP.state,
       TJ1.transactionjournalid,
       TJ1.transitaccountid,
       TJ1.devicetime,
       TJ1.fareamount,
       TJ1.line,
       TJ1.ticketinternalnumber,
       TJ1.ticketid,
       TJ1.transactiontype,
       TJ1.clientid,
       TJ1.operatorid AS transactionoperatorid,
       TJ1.saleschannelid,
       TJ1.resulttype,
       TJ1.cancellationreferenceguid,
       TJ1.cancellationreference,
       TJ1.productid,
       TJ1.pursebalance,
       TJ1.pursecredit,
       TJ1.tariffdate,
       TJ1.tariffversion,
       TJ1.tripticketinternalnumber
FROM acc_revenuerecognition RR
    LEFT JOIN (
        SELECT MIN(SR1.revenuesettlementid) as settlementid, revenuerecognitionid
        FROM acc_settledrevenue SR1
        INNER JOIN acc_revenuesettlement s1 ON S1.revenuesettlementid = SR1.revenuesettlementid
        WHERE S1.SETTLEMENTTYPE = 1 --CML based
        GROUP BY revenuerecognitionid
    ) SR ON SR.revenuerecognitionid = RR.revenuerecognitionid
    LEFT JOIN acc_revenuesettlement RS
        ON RS.revenuesettlementid = SR.settlementid AND SETTLEMENTTYPE = 1 --CML based
    INNER JOIN sl_transactionjournal TJ1
         ON TJ1.transactionjournalid = RR.transactionjournalid
    INNER JOIN acc_closeoutperiod CP
                ON CP.closeoutperiodid = RR.closeoutperiodid
	LEFT JOIN SL_TRANSACTIONJOURNAL TJ2 ON TJ2.TRANSACTIONJOURNALID = TJ1.CANCELLATIONREFERENCE
WHERE TJ1.TRANSACTIONTYPE = 128 AND (TJ2.VALIDTO IS NULL AND (TJ2.TRANSACTIONTYPE = 66 OR TJ2.TRANSACTIONTYPE = 84));

CREATE OR REPLACE VIEW ACC_REC_LOAD_USE_CML_SETTLED
(revenuerecognitionid, revenuesettlementid, settlementtype, amount, premium, basefare,
 ticketnumber, customergroup, linegroupid, operatorid, creditaccountnumber,
 debitaccountnumber, postingdate, postingreference, includeinsettlement, closeoutperiodid, periodfrom,
 periodto, closeouttype, state, transactionjournalid, transitaccountid, devicetime,
 fareamount, line, ticketinternalnumber, ticketid, transactiontype, clientid,
 transactionoperatorid, saleschannelid, resulttype, cancellationreferenceguid,
 cancellationreference, productid, pursebalance, pursecredit, tariffdate, tariffversion,
 tripticketinternalnumber, cancelledtap)
AS
SELECT RR.revenuerecognitionid,
       RS.revenuesettlementid,
       RS.settlementtype,
	   RR.amount,
       RR.premium,
       RR.basefare,
       RR.ticketnumber,
       RR.customergroup,
       RR.linegroupid,
       RR.operatorid,
       RR.creditaccountnumber,
       RR.debitaccountnumber,
       RR.postingdate,
       RR.postingreference,
	   RR.includeinsettlement,
       CP.closeoutperiodid,
       CP.periodfrom,
       CP.periodto,
       CP.closeouttype,
       CP.state,
       TJ1.transactionjournalid,
       TJ1.transitaccountid,
       TJ1.devicetime,
       TJ1.fareamount,
       TJ1.line,
       TJ1.ticketinternalnumber,
       TJ1.ticketid,
       TJ1.transactiontype,
       TJ1.clientid,
       TJ1.operatorid AS transactionoperatorid,
       TJ1.saleschannelid,
       TJ1.resulttype,
       TJ1.cancellationreferenceguid,
       TJ1.cancellationreference,
       TJ1.productid,
       TJ1.pursebalance,
       TJ1.pursecredit,
       TJ1.tariffdate,
       TJ1.tariffversion,
       TJ1.tripticketinternalnumber,
       Cast (Decode (TJ1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)) AS cancelledtap
FROM acc_revenuerecognition RR
    LEFT JOIN (
        SELECT MIN(SR1.revenuesettlementid) as settlementid, revenuerecognitionid
        FROM acc_settledrevenue SR1
        INNER JOIN acc_revenuesettlement s1 ON S1.revenuesettlementid = SR1.revenuesettlementid
        WHERE S1.SETTLEMENTTYPE = 1 --CML based
        GROUP BY revenuerecognitionid
    ) SR ON SR.revenuerecognitionid = RR.revenuerecognitionid
    LEFT JOIN acc_revenuesettlement RS
        ON RS.revenuesettlementid = SR.settlementid AND SETTLEMENTTYPE = 1 --CML based
    INNER JOIN sl_transactionjournal TJ1
         ON TJ1.transactionjournalid = RR.transactionjournalid
    INNER JOIN acc_closeoutperiod CP
                ON CP.closeoutperiodid = RR.closeoutperiodid
WHERE TJ1.TRANSACTIONTYPE = 108 OR TJ1.TRANSACTIONTYPE = 85 OR (TJ1.TRANSACTIONTYPE = 66 AND TJ1.VALIDTO IS NOT NULL);

CREATE OR REPLACE VIEW ACC_REC_LOAD_USE_TAB_SETTLED
(revenuerecognitionid, revenuesettlementid, settlementtype, amount, premium, basefare,
 ticketnumber, customergroup, linegroupid, operatorid, creditaccountnumber,
 debitaccountnumber, postingdate, postingreference, includeinsettlement, closeoutperiodid, periodfrom,
 periodto, closeouttype, state, transactionjournalid, transitaccountid, devicetime,
 fareamount, line, ticketinternalnumber, ticketid, transactiontype, clientid,
 transactionoperatorid, saleschannelid, resulttype, cancellationreferenceguid,
 cancellationreference, productid, pursebalance, pursecredit, tariffdate, tariffversion,
 tripticketinternalnumber, cancelledtap)
AS
SELECT RR.revenuerecognitionid,
       RS.revenuesettlementid,
       RS.settlementtype,
	   RR.amount,
       RR.premium,
       RR.basefare,
       RR.ticketnumber,
       RR.customergroup,
       RR.linegroupid,
       RR.operatorid,
       RR.creditaccountnumber,
       RR.debitaccountnumber,
       RR.postingdate,
       RR.postingreference,
	   RR.includeinsettlement,
       CP.closeoutperiodid,
       CP.periodfrom,
       CP.periodto,
       CP.closeouttype,
       CP.state,
       TJ1.transactionjournalid,
       TJ1.transitaccountid,
       TJ1.devicetime,
       TJ1.fareamount,
       TJ1.line,
       TJ1.ticketinternalnumber,
       TJ1.ticketid,
       TJ1.transactiontype,
       TJ1.clientid,
       TJ1.operatorid AS transactionoperatorid,
       TJ1.saleschannelid,
       TJ1.resulttype,
       TJ1.cancellationreferenceguid,
       TJ1.cancellationreference,
       TJ1.productid,
       TJ1.pursebalance,
       TJ1.pursecredit,
       TJ1.tariffdate,
       TJ1.tariffversion,
       TJ1.tripticketinternalnumber,
       Cast (Decode (TJ1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)) AS cancelledtap
FROM acc_revenuerecognition RR
    LEFT JOIN (
        SELECT MIN(SR1.revenuesettlementid) as settlementid, revenuerecognitionid
        FROM acc_settledrevenue SR1
        INNER JOIN acc_revenuesettlement s1 ON S1.revenuesettlementid = SR1.revenuesettlementid
        WHERE S1.SETTLEMENTTYPE = 0 --TAB based
        GROUP BY revenuerecognitionid
    ) SR ON SR.revenuerecognitionid = RR.revenuerecognitionid
    LEFT JOIN acc_revenuesettlement RS
        ON RS.revenuesettlementid = SR.settlementid AND SETTLEMENTTYPE = 0 --TAB based
    INNER JOIN sl_transactionjournal TJ1
         ON TJ1.transactionjournalid = RR.transactionjournalid
    INNER JOIN acc_closeoutperiod CP
                ON CP.closeoutperiodid = RR.closeoutperiodid
WHERE TJ1.TRANSACTIONTYPE = 108 OR TJ1.TRANSACTIONTYPE = 85 OR (TJ1.TRANSACTIONTYPE = 66 AND TJ1.VALIDTO IS NOT NULL);

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
