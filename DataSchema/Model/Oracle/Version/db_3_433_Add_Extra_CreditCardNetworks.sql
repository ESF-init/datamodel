SPOOL db_3_433.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.433', 'STV', 'Added new Credit Card Network Types');

-- Start adding schema changes here

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
    INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (4, 'Maestro', 'Maestro')
    INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (5, 'Jcb', 'Jcb')
    INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (6, 'Diners', 'Diners')
    INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (7, 'CarteBleue', 'CarteBleue')	
	INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (8, 'CarteBlanc', 'CarteBlanc')
	INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (9, 'Voyager', 'Voyager')
	INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (10, 'Wex', 'Wex')
	INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (11, 'UnionPay', 'UnionPay')
	INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (12, 'Style', 'Style')
	INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (13, 'ValueLink', 'ValueLink')
	INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (14, 'Interac', 'Interac')
	INTO SL_CreditCardNetwork (EnumerationValue, Literal, Description) VALUES (15, 'Laser', 'Laser')
SELECT * FROM DUAL;
 
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
