SPOOL db_3_608.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.608', 'MKD', 'Reverted changes on SL_NotificationMessageOwner from db version scripts 3.582 and 3.580.');

-- =======[ Changes to Existing Tables ]===========================================================================

-- revert old primary key (contractid, notificationmessageid)
ALTER TABLE SL_NOTIFICATIONMESSAGEOWNER DROP CONSTRAINT PK_NOTIFICATIONMESSAGEOWNER;
ALTER TABLE SL_NOTIFICATIONMESSAGEOWNER DROP CONSTRAINT UC_NOTIFICATIONMESSAGEOWNER;

ALTER TABLE SL_NOTIFICATIONMESSAGEOWNER ADD CONSTRAINT PK_NOTIFICATIONMESSAGEOWNER PRIMARY KEY (CONTRACTID, NOTIFICATIONMESSAGEID);

ALTER TABLE SL_NOTIFICATIONMESSAGEOWNER DROP COLUMN NOTIFICATIONMESSAGEOWNERID;

-- drop column customeraccountid from script 3.580
ALTER TABLE SL_NOTIFICATIONMESSAGEOWNER  DROP COLUMN CUSTOMERACCOUNTID;

-- =======[ Sequences ]===========================================================================================

DROP SEQUENCE SL_NOTIFICATIONMESSAGEOWN_SEQ;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
