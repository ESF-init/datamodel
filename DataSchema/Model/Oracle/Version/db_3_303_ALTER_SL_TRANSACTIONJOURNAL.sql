SPOOL db_3_303.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.303', 'FLF', 'Added two new columns to SL_TRANSACTIONJOURNAL');

	

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (BOARDINGGUID  NVARCHAR2(40));

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (CHECKOUTTIME  DATE);


COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.BOARDINGGUID IS 'References the transactionid of the boarding. Can be used for transfers and check outs.';
COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.CHECKOUTTIME IS 'Time of checkout. Set on transition to check out (Alight state) and kept in the following transactions with the same trip (e.g. transfers)';

-- =======[ New SL Tables ]========================================================================================
-- =======[ Functions and Data Types ]=============================================================================
-- =======[ Views ]================================================================================================
-- =======[ Sequences ]============================================================================================
-- =======[ Trigger ]==============================================================================================
-- =======[ Data ]==================================================================================================
-- -------[ Enumerations ]------------------------------------------------------------------------------------------
-- -------[ Master-Tables ]----------------------------------------------------------------------------------------
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL off; 
