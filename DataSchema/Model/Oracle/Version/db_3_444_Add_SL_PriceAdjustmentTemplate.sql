SPOOL db_3_444.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
    VALUES (sysdate, '3.444', 'MMB', 'Added price adjustment template table');

-- -------[ New Tables ]--------------------------------------------------------------------------------------------

CREATE TABLE SL_PRICEADJUSTMENTTEMPLATE
(
	PRICEADJUSTMENTTEMPLATEID   NUMBER(18)      NOT NULL,
	ORGANIZATIONTYPEID        	NUMBER(18)      NOT NULL,
	TICKETINTERNALNUMBER  		NUMBER(9)       NOT NULL,
	PRICEADJUSTMENT       		NUMBER(9)       DEFAULT 0         NOT NULL,
	ISPERCENTAGE          		NUMBER(1)       DEFAULT 0         NOT NULL,
	LASTUSER              		NVARCHAR2(50)   	DEFAULT 'SYS'     NOT NULL,
	LASTMODIFIED          		DATE            DEFAULT sysdate   NOT NULL,
	TRANSACTIONCOUNTER    		INTEGER         NOT NULL
);

ALTER TABLE SL_PRICEADJUSTMENTTEMPLATE ADD CONSTRAINT PK_PRICEADJUSTMENTTEMPLATE PRIMARY KEY (PRICEADJUSTMENTTEMPLATEID);
ALTER TABLE SL_PRICEADJUSTMENTTEMPLATE ADD CONSTRAINT UK_PRICEADJUSTMENTTEMPLATE_01 UNIQUE (ORGANIZATIONTYPEID, TICKETINTERNALNUMBER);

COMMENT ON TABLE SL_PRICEADJUSTMENTTEMPLATE IS 'Price adjustment template table.';
COMMENT ON COLUMN SL_PRICEADJUSTMENTTEMPLATE.PRICEADJUSTMENTTEMPLATEID IS 'Unique identifier of the price adjustment template.';
COMMENT ON COLUMN SL_PRICEADJUSTMENTTEMPLATE.ORGANIZATIONTYPEID IS 'Type of organization, references SL_ORGANIZATIONTYPE.';
COMMENT ON COLUMN SL_PRICEADJUSTMENTTEMPLATE.TICKETINTERNALNUMBER IS 'References TM_TICKET.';
COMMENT ON COLUMN SL_PRICEADJUSTMENTTEMPLATE.PRICEADJUSTMENT IS 'Value of the price adjustment.';
COMMENT ON COLUMN SL_PRICEADJUSTMENTTEMPLATE.ISPERCENTAGE IS 'Idicates whether the price adjustment is percentage based or not.';
COMMENT ON COLUMN SL_PRICEADJUSTMENTTEMPLATE.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_PRICEADJUSTMENTTEMPLATE.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_PRICEADJUSTMENTTEMPLATE.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_PRICEADJUSTMENTTEMPLATE');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_PRICEADJUSTMENTTEMPLATE');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- -------[ Changes to existing Tables ]--------------------------------------------------------------------------------------------  

-- -------[ Data ]--------------------------------------------------------------------------------------------  

COMMIT;

PROMPT Done!
SPOOL off