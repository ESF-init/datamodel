SPOOL db_3_241.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.241', 'FRD', 'ClearingHouse extensions for new RITS ClearingHouse, Clearing Result Table and Level');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================


-- =======[ New Tables ]===========================================================================================

CREATE TABLE CH_CLEARINGRESULT
(
  CLEARINGRESULTID		NUMBER(18,0)						NOT NULL,
  CLEARINGID			NUMBER(18,0)						NOT NULL,
  CLEARINGRESULTLEVEL	NUMBER(9,0)							NOT NULL,
  GOODCOUNT				NUMBER(18,0)						NOT NULL,
  GOODAMOUNT			NUMBER(18,0)						NOT NULL,
  BADCOUNT				NUMBER(18,0)						NOT NULL,
  BADAMOUNT				NUMBER(18,0)						NOT NULL,
  CORRECTEDCOUNT		NUMBER(18,0)						NOT NULL,
  CORRECTEDAMOUNT		NUMBER(18,0)						NOT NULL,
  TOTALCOUNT			NUMBER(18,0)						NOT NULL,
  TOTALAMOUNT			NUMBER(18,0)						NOT NULL,
  CLIENTID				NUMBER(10,0), --Can be null depending on ClearingResultLevel!
  DEVICECLASSID			NUMBER(10,0), --Can be null depending on ClearingResultLevel!
  TRANSACTIONTYPEID		NUMBER(10,0), --Can be null depending on ClearingResultLevel!
  LINEGROUPID			NUMBER(18,0), --Can be null depending on ClearingResultLevel!
  LINEID				NUMBER(10,0), --Can be null depending on ClearingResultLevel!
  LASTUSER				NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
  LASTMODIFIED			DATE			DEFAULT sysdate		NOT NULL,
  TRANSACTIONCOUNTER	INTEGER								NOT NULL,
  DATAROWCREATIONDATE	DATE			DEFAULT sysdate
);

COMMENT ON TABLE CH_CLEARINGRESULT is 'Results of the different clearing runs.';
COMMENT ON COLUMN CH_CLEARINGRESULT.CLEARINGRESULTID IS 'Id of the clearing result.';
COMMENT ON COLUMN CH_CLEARINGRESULT.CLEARINGID IS 'Id of the clearing which created the result (references CH_CLEARING).';
COMMENT ON COLUMN CH_CLEARINGRESULT.CLEARINGRESULTLEVEL IS 'Level of the clearing result (references CH_CLEARINGRESULTLEVEL).';
COMMENT ON COLUMN CH_CLEARINGRESULT.GOODCOUNT IS 'Count of good transactions in the clearing.';
COMMENT ON COLUMN CH_CLEARINGRESULT.GOODAMOUNT IS 'Amount of value of good transactions in the clearing.';
COMMENT ON COLUMN CH_CLEARINGRESULT.BADCOUNT IS 'Count of bad transactions in the clearing.';
COMMENT ON COLUMN CH_CLEARINGRESULT.BADAMOUNT IS 'Amount of value of bad transactions in the clearing.';
COMMENT ON COLUMN CH_CLEARINGRESULT.CORRECTEDCOUNT IS 'Count of corrected transactions in the clearing.';
COMMENT ON COLUMN CH_CLEARINGRESULT.CORRECTEDAMOUNT IS 'Amount of value of corrected transactions in the clearing.';
COMMENT ON COLUMN CH_CLEARINGRESULT.TOTALCOUNT IS 'Total count of all transactions in the clearing.';
COMMENT ON COLUMN CH_CLEARINGRESULT.TOTALAMOUNT IS 'Total amount of value of all transactions in the clearing.';
COMMENT ON COLUMN CH_CLEARINGRESULT.CLIENTID IS 'Client id for which the values and counts have been summed up (references VARIO_CLIENT).';
COMMENT ON COLUMN CH_CLEARINGRESULT.DEVICECLASSID IS 'Device class id for which the values and counts have been summed up (references VARIO_DEVICECLASS).';
COMMENT ON COLUMN CH_CLEARINGRESULT.TRANSACTIONTYPEID IS 'Transaction type id for which the values and counts have been summed up (references RM_TRANSACTIONTYPE).';
COMMENT ON COLUMN CH_CLEARINGRESULT.LINEGROUPID IS 'Line group id for which the values and counts have been summed up (references TM_LINEGROUP).';
COMMENT ON COLUMN CH_CLEARINGRESULT.LINEID IS 'Line id for which the values and counts have been summed up (references VARIO_LINE).';
COMMENT ON COLUMN CH_CLEARINGRESULT.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN CH_CLEARINGRESULT.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN CH_CLEARINGRESULT.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN CH_CLEARINGRESULT.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

ALTER TABLE CH_CLEARINGRESULT ADD (CONSTRAINT PK_CLEARINGRESULT PRIMARY KEY (CLEARINGRESULTID));
-- CH_Clearing.ClearingId has no primary key defined on, so it is not possible to add a foreign key.
--ALTER TABLE CH_CLEARINGRESULT ADD (CONSTRAINT FK_CLEARINGRESULT_CLEARINGID FOREIGN KEY (CLEARINGID) REFERENCES CH_CLEARING(CLEARINGID));
--ALTER TABLE CH_CLEARINGRESULT ADD (CONSTRAINT FK_CLEARINGRESULT_CLIENTID FOREIGN KEY (CLIENTID) REFERENCES VARIO_CLIENT(CLIENTID));                  -- No FK because of non SL table?
--ALTER TABLE CH_CLEARINGRESULT ADD (CONSTRAINT FK_CLEARINGRESULT_DEVCLASSID FOREIGN KEY (DEVICECLASSID) REFERENCES VARIO_DEVICECLASS(DEVICECLASSID)); -- No FK because of non SL table?
--ALTER TABLE CH_CLEARINGRESULT ADD (CONSTRAINT FK_CLEARINGRESULT_TRANSTYPEID FOREIGN KEY (TRANSACTIONTYPEID) REFERENCES RM_TRANSACTIONTYPE(TYPEID));  -- No FK because of non SL table?
--ALTER TABLE CH_CLEARINGRESULT ADD (CONSTRAINT FK_CLEARINGRESULT_LINEGROUPID FOREIGN KEY (LINEGROUPID) REFERENCES TM_LINEGROUP(LINEGROUPID));         -- No FK because of non SL table?
--ALTER TABLE CH_CLEARINGRESULT ADD (CONSTRAINT FK_CLEARINGRESULT_LINEID FOREIGN KEY (LINEID) REFERENCES VARIO_LINE(LINEID));                          -- No FK because of non SL table?


CREATE TABLE CH_CLEARINGRESULTLEVEL
(
  ENUMERATIONVALUE  	NUMBER(9)          		         NOT NULL,
  LITERAL           	NVARCHAR2(50)      		         NOT NULL,
  DESCRIPTION       	NVARCHAR2(50)      		         NOT NULL,
  DATAROWCREATIONDATE	DATE							 DEFAULT sysdate
);

COMMENT ON TABLE CH_CLEARINGRESULTLEVEL IS 'This table contains a list of clearing result levels.';
COMMENT ON COLUMN CH_CLEARINGRESULTLEVEL.ENUMERATIONVALUE IS 'Unique id of the clearing result level.';
COMMENT ON COLUMN CH_CLEARINGRESULTLEVEL.LITERAL 		  IS 'Name of the clearing result level. Used internally.';
COMMENT ON COLUMN CH_CLEARINGRESULTLEVEL.DESCRIPTION 	  IS 'Description of the clearing result level. Used in the UI.';
COMMENT ON COLUMN CH_CLEARINGRESULTLEVEL.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

ALTER TABLE CH_CLEARINGRESULTLEVEL ADD (CONSTRAINT PK_CLEARINGRESULTLEVEL PRIMARY KEY (ENUMERATIONVALUE));

----------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(5) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'CH_CLEARINGRESULT');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/


-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(5) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'CH_CLEARINGRESULT');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/


-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO CH_CLEARINGRESULTLEVEL (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (0, 'Totals', 'Totals');
INSERT INTO CH_CLEARINGRESULTLEVEL (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (1, 'Client', 'Client');
INSERT INTO CH_CLEARINGRESULTLEVEL (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (2, 'DeviceClass', 'Sales Channel');
INSERT INTO CH_CLEARINGRESULTLEVEL (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (4, 'TransactionType', 'Transaction Type');
INSERT INTO CH_CLEARINGRESULTLEVEL (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (8, 'LineGroup', 'Unit');
INSERT INTO CH_CLEARINGRESULTLEVEL (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (16, 'Line', 'Route');

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
