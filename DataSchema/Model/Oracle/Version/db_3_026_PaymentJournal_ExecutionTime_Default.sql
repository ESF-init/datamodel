SPOOL db_3_026.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.026', 'DST', 'Added default value to ExecutionTime in PaymentJournal.');

-- Start adding schema changes here

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_PAYMENTJOURNAL MODIFY EXECUTIONTIME DEFAULT SYSDATE;

UPDATE SL_PAYMENTJOURNAL SET EXECUTIONTIME = LASTMODIFIED, TRANSACTIONCOUNTER = TRANSACTIONCOUNTER + 1 WHERE EXECUTIONTIME IS NULL;

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
