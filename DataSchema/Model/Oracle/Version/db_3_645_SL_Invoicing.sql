SPOOL db_3_645.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.645', 'BVI', 'Added new columns to SL_Invoicing.');

Alter Table SL_Invoicing add AccountingDueDate Date;
COMMENT ON COLUMN SL_Invoicing.AccountingDueDate IS 'Is used for the due date in sepa files.';

Alter Table SL_Invoicing add ValueDateTime Date;
COMMENT ON COLUMN SL_Invoicing.ValueDateTime IS 'Is used to create and select relevant posting by value date time.';

Alter Table SL_Invoicing add FormIDs NVARCHAR2(1000);
COMMENT ON COLUMN SL_Invoicing.FormIDs IS 'The selected forms for the invoicing process (SL_Form.FormID).';


COMMIT;

PROMPT Done!

SPOOL OFF;
