SPOOL db_3_098.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.098', 'STV', 'Alteration of SL_PRODUCTTERMINATION, SL_PRODUCTTERMINATIONTYPE ');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_PRODUCTTERMINATION MODIFY ( DIFFERENCECOST NUMBER(9) DEFAULT 0 NOT NULL);
ALTER TABLE SL_PRODUCTTERMINATION MODIFY ( BANKINGCOST NUMBER(9) DEFAULT 0 NOT NULL);
ALTER TABLE SL_PRODUCTTERMINATION MODIFY ( TOTALCOST NUMBER(9,0) DEFAULT 0 NOT NULL);       
ALTER TABLE SL_PRODUCTTERMINATIONTYPE MODIFY ( PROCESSINGCOST NUMBER(9) DEFAULT 0 NOT NULL); 
ALTER TABLE SL_PRODUCTTERMINATIONTYPE MODIFY ( TAXCOST	NUMBER(9) DEFAULT 0 NOT NULL);
ALTER TABLE SL_PRODUCTTERMINATIONTYPE MODIFY ( DUNNINGCOST NUMBER(9)DEFAULT 0 NOT NULL );

-- =======[ New Tables ]===========================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
