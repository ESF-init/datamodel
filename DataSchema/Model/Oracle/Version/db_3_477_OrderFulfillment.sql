INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.477', 'BVI', 'Order Fulfillment changes.');


-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_Printer
(
  PrinterID           NUMBER(18)                NOT NULL,
  Name                NVARCHAR2(50)             NOT NULL,
  PrinterType         Number(9)                 NOT NULL,
  ManufacturerDesignation            NVARCHAR2(50)             ,
  DisplayName         NVARCHAR2(50)             ,
  Description         NVARCHAR2(500)            ,
  GUID                NVARCHAR2(50)             ,
  IsDisabled          NUMBER(1)                 DEFAULT 0                     NOT NULL,
  RegistrationTime    DATE                      Default sysdate,
  LastResponse        DATE                      Default sysdate,
  PrinterState        Number(9)                 DEFAULT 0,
  LASTUSER            NVARCHAR2(50)             DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED        DATE                      DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER  INTEGER                   NOT NULL,
  CONSTRAINT PK_PrinterID PRIMARY KEY (PrinterID)
);

COMMENT ON TABLE SL_Printer IS 'Known printers by the printing service.';
COMMENT ON COLUMN SL_Printer.Name IS 'The name of the printer the way it is registered in the environment of the printing service.';
COMMENT ON COLUMN SL_Printer.PrinterType IS 'Type of the printer (BulkPrinting, ReceiptPrinter, Letter)';
COMMENT ON COLUMN SL_Printer.ManufacturerDesignation IS 'Name of the printer defined by the manufactorer.';
COMMENT ON COLUMN SL_Printer.DisplayName IS 'Customizeable display name.';
COMMENT ON COLUMN SL_Printer.Description IS 'Customizeable description for the printer.';
COMMENT ON COLUMN SL_Printer.GUID IS 'By the printing service defined GUID for the printer.';
COMMENT ON COLUMN SL_Printer.IsDisabled IS 'Disabled for usage from the printing service.';
COMMENT ON COLUMN SL_Printer.RegistrationTime IS 'The time when the printer has been registered.';
COMMENT ON COLUMN SL_Printer.LastResponse IS 'Last status report from the printer.';
COMMENT ON COLUMN SL_Printer.PrinterState IS 'High level state of the printer.';
COMMENT ON COLUMN SL_Printer.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_Printer.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_Printer.LASTUSER IS 'Technical field: last (system) user that changed this dataset';


-- =======[ Sequences ]===========================================================================================


create sequence SL_Printer_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]========================================================================================

create or replace trigger  SL_Printer_BRI
		before insert on SL_Printer
		for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;
		/

create or replace trigger SL_Printer_BRU
		before update on SL_Printer
		for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;		
		/

		
-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_orderdetail add ProcessingStartDate Date;
COMMENT ON COLUMN sl_orderdetail.ProcessingStartDate IS 'Date when the order detail processing starts.';

alter table sl_orderdetail add ProcessingEndDate Date;
COMMENT ON COLUMN sl_orderdetail.ProcessingEndDate IS 'Date when the order detail processing finished.';

alter table sl_order add Priority Number(9);
COMMENT ON COLUMN sl_order.Priority IS 'Manually defined priority for the order by the clerk.';

alter table sl_job add PrinterID Number (18);
COMMENT ON COLUMN sl_job.PrinterID IS 'References to SL_Printer.PrinterID.';
alter table sl_job add constraint FK_Job_Printer foreign key (PrinterID) references SL_Printer (PrinterID);
		
alter table sl_job add OrderDetailID Number (18);
COMMENT ON COLUMN sl_job.OrderDetailID IS 'References to SL_OrderDetail.OrderDetailID.';
alter table sl_job add constraint FK_Job_OrderDetail foreign key (OrderDetailID) references SL_OrderDetail (OrderDetailID);			




--add some missing comments:
COMMENT ON TABLE SAM_INVOICING IS 'Used for SAM application, used for student invoicing.';


COMMENT ON COLUMN SL_ADDRESS.ADDRESSEE IS 'Represents the first line in the address for a letter';
COMMENT ON COLUMN SL_ADDRESS.COORDINATEID IS 'References to SL_Coordinate.COORDINATEID.';

COMMENT ON COLUMN SAM_PUPIL.EXPORTSTATE IS 'Saves the information, if the data row was already exported.';
COMMENT ON COLUMN SAM_PUPIL.SALESPOINTID IS 'OBSOLETE.';


COMMENT ON COLUMN SAM_SCHOOLYEAR.EXPORTSTATE IS 'Saves the information, if the data row was already exported.';
COMMENT ON COLUMN SAM_SEASONTICKET.EXPORTSTATE IS 'Saves the information, if the data row was already exported.';
COMMENT ON COLUMN SAM_SEASONTICKETHOLDER.EXPORTSTATE IS 'Saves the information, if the data row was already exported.';
COMMENT ON COLUMN SAM_SEASONTICKETTYPE.EXPORTSTATE IS 'Saves the information, if the data row was already exported.';
COMMENT ON COLUMN SAM_TICKET_TO_ACCOUNTENTRY.EXPORTSTATE IS 'Saves the information, if the data row was already exported.';

COMMENT ON COLUMN SL_AUDITREGISTERVALUE.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_AUDITREGISTERVALUE.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_AUDITREGISTERVALUE.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

COMMENT ON COLUMN SL_CARD.NUMBEROFPRINTS IS 'Used for paper tickets, represents the number of tries to print.';

COMMENT ON COLUMN SL_CARDHOLDERORGANIZATION.CREATED IS 'Represents the insert date of the data row.';

COMMENT ON COLUMN SL_CARDHOLDERORGANIZATION.CREATED IS 'Represents the insert date of the data row.';


COMMENT ON COLUMN VARIO_LINE.ISACTIVE IS 'If the line is not in use anymore, the value is 0, otherwise 1/-1.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;