SPOOL db_3_329.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.329', 'MKD', 'Added new table PERF_ADDITIONALDATA');	

-- =======[ Changes on existing tables ]===========================================================================
-- =======[ New Table ]========================================================================================
CREATE TABLE PERF_ADDITIONALDATA
(
  ADDITIONALDATAID      NUMBER(18)              NOT NULL,
  PERFORMANCEID         NUMBER(18)              NOT NULL,
  HOSTNAME              NVARCHAR2(200)          ,
  TRANSACTIONID         NVARCHAR2(40)             ,
  CONSTRAINT PK_ADDITIONALDATA PRIMARY KEY (ADDITIONALDATAID),
  CONSTRAINT FK_ADDITIONALDATA_PERFORMANCE
    FOREIGN KEY (PERFORMANCEID) 
    REFERENCES PERF_PERFORMANCE (PERFORMANCEID)
    ON DELETE CASCADE,
  CONSTRAINT UK_PERFORMANCEID UNIQUE (PERFORMANCEID)
);

COMMENT ON TABLE PERF_ADDITIONALDATA IS 'Contains additional data for some Perf_Performance table entries.';
COMMENT ON COLUMN PERF_ADDITIONALDATA.ADDITIONALDATAID IS 'Unique id for this record.';
COMMENT ON COLUMN PERF_ADDITIONALDATA.PERFORMANCEID IS 'Foreign key to the Perf_Performance record.';
COMMENT ON COLUMN PERF_ADDITIONALDATA.HOSTNAME IS 'The host name of the request on which the performance was measured.';
COMMENT ON COLUMN PERF_ADDITIONALDATA.TRANSACTIONID IS 'The id of the corresponding transaction.';

-- =======[ Functions and Data Types ]=============================================================================
-- =======[ Views ]================================================================================================
-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(10) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('PERF_ADDITIONALDATA');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/
-- =======[ Trigger ]==============================================================================================
-- =======[ Data ]==================================================================================================
-- -------[ Enumerations ]------------------------------------------------------------------------------------------
-- -------[ Master-Tables ]----------------------------------------------------------------------------------------
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL off; 
