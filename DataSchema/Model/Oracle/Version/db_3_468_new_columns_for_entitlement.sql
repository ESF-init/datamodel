SPOOL db_3_468.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.468', 'OMA', 'Alter the entitlement tables to use them for tracking external agencies in ORCA');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_ENTITLEMENTTYPE ADD ISEXTERNALIDEDITABLE number(1) default 0 not null;
COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.CARDHOLDERID IS 'Determines if External Ids on Entitlements of this type are editable.';
ALTER TABLE SL_ENTITLEMENTTYPE ADD ISVALIDTOEDITABLE number(1) default 0 not null;
COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.CARDHOLDERID IS 'Determines if Valid Tos on Entitlements of this type are editable.';

ALTER TABLE SL_ENTITLEMENT ADD EXTERNALID nvarchar2(100) default null;
COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.CARDHOLDERID IS 'External ID of an agency service (ParatransitID, TaxiScripID, LIFTID).';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
