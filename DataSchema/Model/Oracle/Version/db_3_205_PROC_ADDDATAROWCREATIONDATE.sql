SPOOL db_3_205.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.205', 'JGI', 'Added procedure p_addDataRowCreationDate to enable incremental updates for DWH');

CREATE OR REPLACE PROCEDURE p_addDataRowCreationDate
(
   s_SchemaName IN VARCHAR2
)
AS

nCount NUMBER;

CURSOR schemaTables (s_Schema VARCHAR2)
  IS
    (select OBJECT_NAME from dba_objects b WHERE UPPER(b.OWNER) = UPPER(s_Schema) AND UPPER(b.OBJECT_NAME) <> UPPER('VARIO_DMODELLVERSION') AND b.object_type = 'TABLE' 
      AND B.OBJECT_NAME NOT IN ( SELECT OBJECT_NAME FROM dba_objects WHERE UPPER(OWNER) = UPPER(s_Schema) AND object_type <> 'TABLE') AND B.OBJECT_NAME NOT IN (SELECT TABLE_NAME FROM DBA_TABLES WHERE UPPER(OWNER) = UPPER(s_Schema) AND TABLESPACE_NAME IS NULL));

CURSOR schemaTriggers (s_Schema VARCHAR2)
  IS
    (select OBJECT_NAME from dba_objects b WHERE UPPER(b.OWNER) = UPPER(s_Schema) AND b.object_type = 'TRIGGER' );

BEGIN
  
   FOR sTriggers IN schemaTriggers(s_SchemaName)
   LOOP
     EXECUTE IMMEDIATE 'ALTER TRIGGER ' || s_SchemaName || '."' || sTriggers.OBJECT_NAME || '" DISABLE';
   END LOOP;
   
   FOR sTables IN schemaTables(s_SchemaName)
   LOOP
     nCount := 0;
     SELECT count(TABLE_NAME) INTO nCount FROM ALL_TAB_COLUMNS WHERE UPPER(OWNER) = UPPER(s_SchemaName) AND TABLE_NAME = sTables.OBJECT_NAME AND COLUMN_NAME = 'DATAROWCREATIONDATE';
     IF nCount = 0 
     THEN
        EXECUTE IMMEDIATE 'ALTER TABLE ' || s_SchemaName || '.' || sTables.OBJECT_NAME || ' ADD (DATAROWCREATIONDATE DATE DEFAULT SYSDATE)';
     END IF;
   END LOOP;
   
   FOR sTriggers IN schemaTriggers(s_SchemaName)
    LOOP
     EXECUTE IMMEDIATE 'ALTER TRIGGER ' || s_SchemaName || '."' || sTriggers.OBJECT_NAME || '" ENABLE';
     EXECUTE IMMEDIATE 'ALTER TRIGGER ' || s_SchemaName || '."' || sTriggers.OBJECT_NAME || '" COMPILE';
   END LOOP;
   
END p_addDataRowCreationDate;
/

COMMIT;

PROMPT Done!

SPOOL OFF;
