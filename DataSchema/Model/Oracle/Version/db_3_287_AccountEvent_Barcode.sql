SPOOL db_3_287.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.287', 'ULB', 'added PROT_ACCOUNTEVENT.Barcode');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE PROT_ACCOUNTEVENT
ADD (barcode VARCHAR2(13 BYTE));

COMMENT ON COLUMN 
PROT_ACCOUNTEVENT.barcode IS 
'Contains the papercode on the receipt''s backside ';


COMMIT;

PROMPT Done!

SPOOL OFF;
