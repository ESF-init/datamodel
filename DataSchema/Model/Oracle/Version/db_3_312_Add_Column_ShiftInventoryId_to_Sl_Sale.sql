SPOOL db_3_312.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.312', 'BRM', 'Added new column to SL_SL_SALE');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_SALE ADD SHIFTINVENTORYID VARCHAR2(32 Byte);

COMMIT;

PROMPT Done!

SPOOL off; 