SPOOL db_3_176.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.176', 'APD', 'Add creation date, insert enum values');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_COMMENT
ADD CREATIONDATE DATE DEFAULT sysdate NULL;

COMMENT ON COLUMN SL_COMMENT.CREATIONDATE IS 'Time this record was created.';

INSERT INTO SL_COMMENTPRIORITY (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
VALUES (0, N'low', N'low');

INSERT INTO SL_COMMENTPRIORITY (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
VALUES (100, N'high', N'high');

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
