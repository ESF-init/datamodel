SPOOL db_3_436.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.436', 'DST', 'Add ResetReplacementCounter and ResetInactivity to SL_CardEventType.');

INSERT INTO SL_CARDEVENTTYPE ( ENUMERATIONVALUE, LITERAL, DESCRIPTION)
VALUES ( 20, N'ResetReplacementCounter', N'ResetReplacementCounter');

INSERT INTO SL_CARDEVENTTYPE ( ENUMERATIONVALUE, LITERAL, DESCRIPTION)
VALUES ( 21, N'ResetInactivity', N'ResetInactivity');


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
