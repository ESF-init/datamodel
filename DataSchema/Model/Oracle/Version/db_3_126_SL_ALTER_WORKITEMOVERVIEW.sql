SPOOL db_3_126.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.126', 'EPA', 'Coalesced contractid field, added clientid and clientname');


-- =======[ Changes to Existing Tables ]===========================================================================
DROP VIEW sl_workitemoverview;
CREATE OR REPLACE FORCE VIEW sl_workitemoverview (workitemid,
                                                         cardid,
                                                         faremediaid,
                                                         printednumber,
                                                         orderid,
                                                         contractid,
                                                         contractnumber,
                                                         organizationid,
                                                         workitemsubjectid,
                                                         typediscriminator,
                                                         title,
                                                         memo,
                                                         SOURCE,
                                                         state,
                                                         assignee,
                                                         createdby,
                                                         created,
                                                         assigned,
                                                         executed,
                                                         lastmodified,
                                                         clientid,
                                                         clientname
                                                        )
AS
SELECT
        SL_WorkItem.WorkItemID,
        SL_WorkItem.CardID,
        SL_Card.FareMediaID,
        SL_Card.PrintedNumber,
        SL_WorkItem.OrderID,
        cast(coalesce(sl_workitem.contractid, sl_order.contractid) as Number(18,0)),
        SL_Contract.ContractNumber,
        SL_Contract.OrganizationID,
        SL_WorkItem.WorkItemSubjectID,
        SL_WorkItem.TypeDiscriminator,
        SL_WorkItem.Title,
        SL_WorkItem.Memo,
        SL_WorkItem.Source,
        SL_WorkItem.State,
        SL_WorkItem.Assignee,
        SL_WorkItem.CreatedBy,
        SL_WorkItem.Created,
        SL_WorkItem.Assigned,
        SL_WorkItem.Executed,
        SL_WorkItem.LastModified,
        sl_contract.clientid,
        vario_client.companyname as clientname
        FROM sl_workitem 
        LEFT OUTER JOIN sl_order
        ON sl_workitem.orderid = sl_order.orderid
        LEFT OUTER JOIN sl_contract
        ON coalesce(sl_workitem.contractid, sl_order.contractid) = sl_contract.contractid
        LEFT OUTER JOIN sl_card 
        ON sl_workitem.cardid = sl_card.cardid
        LEFT OUTER JOIN vario_client
        ON sl_contract.clientid = vario_client.clientid;


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
