SPOOL db_3_289.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.289', 'ULB', 'added AM_AUTOMAT.MountingPlateNo');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE AM_AUTOMAT
ADD (MountingPlateNo NUMBER(3));

COMMENT ON COLUMN 
AM_AUTOMAT.MountingPlateNo IS 
'The number of the mountingplate the device is mounted on.';


COMMIT;

PROMPT Done!

SPOOL OFF;
