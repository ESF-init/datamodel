SPOOL db_3_007.log

INSERT INTO vario_dmodellversion (rundate,dmodellno,responsible,annotation)
VALUES (SYSDATE,'3.007','FLF','Added DestinationFareStageId to TM_BusinessRuleResult');


---Start adding schema changes here

ALTER TABLE TM_BUSINESSRULERESULT
 ADD (DESTINATIONFARESTAGEID  NUMBER(18));

---End adding schema changes



COMMIT;

PROMPT Done!

SPOOL OFF