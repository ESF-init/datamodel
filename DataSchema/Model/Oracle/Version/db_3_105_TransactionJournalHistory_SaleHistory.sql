SPOOL db_3_105.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.105', 'DST', 'Refresh for SL_TRANSACTIONJOURNALHISTORY and VIEW_SL_SALEHISTORY');

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW VIEW_SL_SALEHISTORY AS
select 
    sl_sale.saleid,
    sl_sale.saletransactionid,
    sl_sale.receiptreference,
    sl_sale.saleschannelid,
    sl_sale.saletype,
    sl_sale.locationnumber,
    sl_sale.devicenumber,
    sl_sale.salespersonnumber,
    sl_sale.merchantnumber,
    sl_sale.networkreference,
    sl_sale.saledate,
    sl_sale.clientid   as operatorid,
    sl_sale.contractid as accountid,
    sl_sale.externalordernumber,
    sl_sale.orderid,
    sl_order.state,
    sl_order.ordernumber,
    case when sl_sale.saleid not in (select can.cancellationreferenceid from sl_sale can where can.cancellationreferenceid is not null) then 0 else 1 end iscanceled,
    (select sum(sl_paymentjournal.amount) from sl_paymentjournal where sl_paymentjournal.saleid = sl_sale.saleid) amount,
    c.lastcustomer
  from sl_sale  
        left outer join sl_order on sl_sale.orderid = sl_order.orderid
        left outer join ( select h.lastcustomer, h.orderid, row_number() over (partition by h.orderid order by h.lastmodified) as rn from sl_orderhistory h ) c on c.orderid = sl_sale.orderid and rn = 1
;

CREATE OR REPLACE VIEW SL_TRANSACTIONJOURNALHISTORY
(
  transactionid,
  operatorid,
  saleschannelid,
  transactiontype,
  timestamp,
  ticketexternalnumber,
  result,
  pursecredit,
  pursebalance,
  pursecreditpretax,
  pursebalancepretax,
  duration,
  validfrom,
  validto, 
  numberofpersons,
  stopno,
  stopname,
  lineno,
  linename,
  vehiclenumber,
  devicenumber,
  devicename,
  devicedescription,
  appliedcapping,
  producttickettype,
  ticketname,
  transitaccountid
)
AS
with
tariff as (
select tarifid tariffid, netid from (
select tarifid, netid, max(version), max(validfrom) from tm_tarif
group by tarifid, netid having tarifid in (select tariffid from tm_tariff_release where 
  releasetype = decode((select 1 from sl_configurationdefinition where name = 'UseTestTariffArchive' and lower(defaultvalue) like 'f%'), 1, 2, 1) /* returns 1 (testrelease) if UseTestTariffArchive is set to true, otherwise 2 (productiverelease) */
  and deviceclassid = 70)
order by max(validfrom) desc, max(version) desc
) where rownum = 1),
trans as (
select * 
from sl_transactionjournal
where
transactionjournalid not in (select cancellationreference from sl_transactionjournal can where cancellationreference is not null) and
transactiontype in (
66, -- Boarding
84, -- Transfer
85, -- Use
99, -- Charge
108, -- Load
261, -- ClientFare
262, -- OpenLoopVirtualCharge
7000, -- Inspection
9001, -- FareMediaSale
9003, -- Refund
9006, -- Adjustment
9007, -- DormancyFee
9009, -- BalanceTransfer
9010, -- ProductTransfer
9012, -- StoredValuePayment
9013 -- StoredValueRefund
))
select 
  trans.transactionid,
  trans.operatorid,
  trans.saleschannelid,
  case when trans.transactiontype = 66 and trans.validto is not null then cast(9011 /* ProductActivation */ as number(9)) else trans.transactiontype end transactiontype,
  trans.devicetime timestamp,
  trans.ticketinternalnumber ticketexternalnumber,
  trans.resulttype result,
  case 
    when trans.resulttype = 0 /* Ok */ 
      then case when trans.transactiontype = 99 /*charge*/ and prod.tickettype = 201 /*pursepretax*/ then cast(null as number(9)) else trans.pursecredit end /* PreTax charge does not get written in Product2, workaround */
    else cast(0 as number(9)) end pursecredit,
  case when trans.resulttype = 0 /* Ok */ 
    then case when trans.transactiontype = 99 /*charge*/ and prod.tickettype = 201 /*pursepretax*/ then cast(null as number(9)) else trans.pursebalance end /* PreTax charge does not get written in Product2, workaround */
    else cast(0 as number(9)) end pursebalance,
  case when trans.resulttype = 0 /* Ok */ 
    then case when trans.transactiontype = 99 /*charge*/ and prod.tickettype = 201 /*pursepretax*/ then trans.pursecredit else trans.pursecredit2 end /* PreTax charge does not get written in Product2, workaround */
    else cast(0 as number(9)) end pursecreditpretax,
  case when trans.resulttype = 0 /* Ok */
    then case when trans.transactiontype = 99 /*charge*/ and prod.tickettype = 201 /*pursepretax*/ then trans.pursebalance else trans.pursebalance2 end
    else cast(0 as number(9)) end pursebalancepretax,
  trans.duration,
  case
    when trans.validfrom is null and trans.validto is null and trans.duration > 0 then trans.devicetime 
    else trans.validfrom end validfrom, /* Transfers don't have ValidFrom and ValidTo set, workaround */
  case 
    when trans.validto is null and trans.validfrom is not null then trans.validfrom+trans.duration/(24*60*60) /* Boardings don't have ValidTo set, workaround */
    when trans.validfrom is null and trans.validto is null and trans.duration > 0 then trans.devicetime+trans.duration/(24*60*60) /* Transfers don't have ValidFrom and ValidTo set, workaround */
    else trans.validto end validto, 
  trans.groupsize numberofpersons,
  stop.stopno,
  stop.stopname,
  line.lineno,
  line.linename,
  trans.vehiclenumber,
  trans.devicenumber,
  device.name devicename,
  device.description devicedescription,
  cap.cappingname appliedcapping,
  prod.tickettype producttickettype,
  ticket.name ticketname,
  trans.transitaccountid
from trans
left join tariff on 1=1 -- tariff only contains one row
left join tm_ticket ticket on ticket.internalnumber = trans.ticketinternalnumber and tariff.tariffid = ticket.tarifid
left join vario_stop stop on stop.netid = tariff.netid and stop.stopno = trans.stopnumber
left join vario_line line on line.netid = tariff.netid and line.lineno = trans.line
left join um_device device on device.deviceno = trans.devicenumber
left join sl_product prod on prod.productid = trans.productid
left join sl_cappingjournal cappingjournal on cappingjournal.transactionjournalid = trans.transactionjournalid and cappingjournal.state in ( 3, /* applied */ 2 /* partiallyapplied */ )
left join TM_RULE_CAPPING cap on cap.potnumber = cappingjournal.potid and cap.tariffid in (select tariffid from tariff)
;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
