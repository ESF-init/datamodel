SPOOL db_3_618.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.618', 'FRD', 'Daily Clearing Retry SP to delete failed run data');

-- =======[ Stored Procedures ]===========================================================================================

CREATE OR REPLACE PROCEDURE P_CH_DeleteFailedClearingData
/******************************************************************************
   NAME:       CH_DeleteFailedClearing
   PURPOSE:    Delete failed clearing run data for a failed clearing run to facilitate retry.    

   REVISIONS:
   Ver        Date         Author     Description
   ---------  -----------  ---------  ------------------------------------
   1.0        12.05.2021   FRD        1. Created this procedure.

******************************************************************************/
( 
    clearing_Id IN NUMBER,
    return_code OUT NUMBER
)
IS
    periodStartDate TIMESTAMP;
    periodEndDate TIMESTAMP;
BEGIN
    SAVEPOINT sp_DeleteStart;
    
    return_code := 0;
    
    SELECT PERIODSTARTDATE, PERIODENDDATE INTO periodStartDate, periodEndDate FROM CH_CLEARING WHERE CLEARINGID = clearing_Id; 

    DELETE (SELECT * FROM CH_CLEARINGRESULT CR 
        JOIN CH_CLEARING C ON CR.CLEARINGID = C.CLEARINGID
        WHERE C.CLEARINGID = clearing_Id); 
        
    DELETE (SELECT * FROM CH_CLEARINGTRANSACTION CT 
        JOIN CH_CLEARING C ON CT.CLEARINGID = C.CLEARINGID 
        WHERE C.CLEARINGID = clearing_Id); 
        
    DELETE FROM CH_CLEARING WHERE CLEARINGID = clearing_Id; 
        
    DELETE (SELECT * FROM CH_TRANSACTIONADDITIONAL TA
        JOIN RM_TRANSACTION T ON TA.TRANSACTIONID = T.TRANSACTIONID
        WHERE T.IMPORTDATETIME >= periodStartDate
        AND T.IMPORTDATETIME <= periodEndDate);
        
    COMMIT;
    
    return_code := 1;
EXCEPTION
WHEN OTHERS THEN
   ROLLBACK TO sp_DeleteStart;
   raise_application_error(-20001,'An error was encountered - '||SQLCODE||' '||SQLERRM);
END;
/


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
