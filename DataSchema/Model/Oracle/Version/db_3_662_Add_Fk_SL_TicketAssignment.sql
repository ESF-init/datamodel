SPOOL db_3_662.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.662', 'ESF', 'Modify SL_TicketAssignment');
-- =======[ Changes on existing tables ]===========================================================================
ALTER TABLE SL_TICKETASSIGNMENT
ADD ENTITLEMENTID NUMBER(18) NULL;

ALTER TABLE SL_TICKETASSIGNMENT 
ADD CONSTRAINT FK_LINKEDENTITLEMENT 
FOREIGN KEY (ENTITLEMENTID) 
REFERENCES SL_ENTITLEMENT (ENTITLEMENTID);

COMMENT ON COLUMN SL_TICKETASSIGNMENT.ENTITLEMENTID 
IS 'Link to an entitlement that is related to the assignment, so that the status of the assignments can be synced based on the entitlement status.';
---End adding schema changes 
-- =======[ Commit ]===============================================================================================

COMMIT;
PROMPT Done!
SPOOL OFF;
