SPOOL db_3_620.log;

------------------------------------------------------------------------------
--Version 3.620
------------------------------------------------------------------------------
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.620', 'ARH', 'Update JobType');

-- =======[ Data ]==================================================================================================

UPDATE SL_JOBTYPE SET ENUMERATIONVALUE = 27 WHERE LITERAL ='SumInvoicing'; 

COMMIT;

PROMPT Done!

SPOOL OFF;
