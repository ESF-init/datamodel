SPOOL db_3_463.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.463', 'MFA', 'Alter table SL_SALE, add new column NOTES.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_SALE ADD NOTES NVARCHAR2(255);
COMMENT ON COLUMN SL_SALE.NOTES IS 'Notes for the sale.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
