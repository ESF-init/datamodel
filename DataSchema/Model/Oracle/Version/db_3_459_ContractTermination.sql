SPOOL db_3_459.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.459', 'BVI', 'Added new tables for contract termination.');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_CONTRACTTERMINATIONTYPE
(
	CONTRACTTERMINATIONTYPEID	NUMBER(18,0)    NOT NULL,
	CLIENTID NUMBER(10) NOT NULL,
	NAME    NVARCHAR2(128)  NOT NULL,
    DESCRIPTION    NVARCHAR2(250)  NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_CONTRACTTERMINATIONTYPEID PRIMARY KEY (CONTRACTTERMINATIONTYPEID)
);

COMMENT ON TABLE SL_CONTRACTTERMINATIONTYPE is 'Table containing information about a contract termination types';
COMMENT ON COLUMN SL_CONTRACTTERMINATIONTYPE.CONTRACTTERMINATIONTYPEID is 'Unique identifier of the contract termination';
COMMENT ON COLUMN SL_CONTRACTTERMINATIONTYPE.NAME is 'Name of the contract termination.';
COMMENT ON COLUMN SL_CONTRACTTERMINATIONTYPE.DESCRIPTION is 'Description of the contract termination';
COMMENT ON COLUMN SL_CONTRACTTERMINATIONTYPE.LastUser is 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_CONTRACTTERMINATIONTYPE.LastModified is 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_CONTRACTTERMINATIONTYPE.TransactionCounter is 'Technical field: counter to prevent concurrent changes to this dataset';


CREATE TABLE SL_CONTRACTTERMINATION
(
    CONTRACTTERMINATIONID       NUMBER(18,0)    NOT NULL,
	CONTRACTTERMINATIONTYPEID	NUMBER(18,0)    NOT NULL,    
    DESCRIPTION    NVARCHAR2(250)  NOT NULL,	
	CONTRACTID NUMBER(18,0) NOT NULL,
	EMPLOYEEID NUMBER(18,0) NOT NULL,
	TERMINATIONDATE DATE NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_CONTRACTTERMINATIONID PRIMARY KEY (CONTRACTTERMINATIONID)
);

alter table SL_CONTRACTTERMINATION add constraint FK_CONTRACTTERMINATIONTYPE foreign key (CONTRACTTERMINATIONTYPEID) references SL_CONTRACTTERMINATIONTYPE (CONTRACTTERMINATIONTYPEID);
alter table SL_CONTRACTTERMINATION add constraint FK_CONTTERMINATION_CONTRACT foreign key (CONTRACTID) references SL_CONTRACT (CONTRACTID);
alter table SL_CONTRACTTERMINATION add constraint FK_CONTTERMINATION_PERSON foreign key (EMPLOYEEID) references SL_PERSON (PERSONID);

COMMENT ON TABLE SL_CONTRACTTERMINATION is 'Table containing information about a contract termination';
COMMENT ON COLUMN SL_CONTRACTTERMINATION.CONTRACTTERMINATIONID is 'References SL_CONTRACTTERMINATIONTYPE.CONTRACTTERMINATIONTYPEID';
COMMENT ON COLUMN SL_CONTRACTTERMINATION.DESCRIPTION is 'Extra notes about this termination';
COMMENT ON COLUMN SL_CONTRACTTERMINATION.CONTRACTID is 'References SL_CONTRACT.CONTRACTID';
COMMENT ON COLUMN SL_CONTRACTTERMINATION.EMPLOYEEID is 'References SL_PERSON.PERSONID';
COMMENT ON COLUMN SL_CONTRACTTERMINATION.TERMINATIONDATE is 'Termination date of the contract.';
COMMENT ON COLUMN SL_CONTRACTTERMINATION.LastUser is 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_CONTRACTTERMINATION.LastModified is 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_CONTRACTTERMINATION.TransactionCounter is 'Technical field: counter to prevent concurrent changes to this dataset';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
