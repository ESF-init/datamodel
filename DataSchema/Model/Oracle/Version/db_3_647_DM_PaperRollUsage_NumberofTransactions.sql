SPOOL db_3_647.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.647', 'ULB', 'added dm_paperrollusage.NumberofTransactions');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE DM_PAPERROLLUSAGE
ADD (NumberofTransactions NUMBER(10) DEFAULT 0);
-- =======[ New Tables ]===========================================================================================
COMMENT ON COLUMN DM_PAPERROLLUSAGE.NumberofTransactions IS 'Number of transactions printed on this paperroll';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
