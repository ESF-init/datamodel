SPOOL db_3_505.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.505', 'MEA', 'Add Cardaction view');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

--COMMENT ON TABLE XXX is '';

--COMMENT ON COLUMN XXX.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
--COMMENT ON COLUMN XXX.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
--COMMENT ON COLUMN XXX.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW VIEW_CARDACTION
(
    CARDNR,
    REQUESTNO,   
    VALIDFROM,
    VALIDUNTIL,
    CHARGEVALUE,
    REQUESTTYPE,
    ID,   
    PRODUCTID,
    INSTANCENO,
    TICKETID,
    P_VALIDFROM,
    P_VALIDUNTIL,
    ORIGIN,
    DESTINATION,
    VIA,
    TRIPCOUNT,
    FARESTAGE,
    CANCELLED,
    CARDID,
    CARDACTIONID,
    P_EXPIRY,
    A_ID,
    CARDACTIONREQUESTID,
    EXPIRY,
    REGISTRATION,
    DATEOFBIRTH,
    VOICEFLAG,
    DEPOSIT,
    USERGROUP,
    PRINTEDCARDNO,
    ISSUEFLAG,
    ISSUERID,
    REMOVEBIRTHDAY,
    GROUPEXPIRY,
    TARIFID,
    INTERNALNUMBER
)
AS
      SELECT r.CardNo   AS Cardnr,
             r.requestno,
             r.validfrom,
             r.validuntil,
             r.CHARGEVALUE,
             r.Requesttype,
             r.ID,             
             p.PRODUCTID,
             p.INSTANCENO,
             p.TICKETID,
             p.VALIDFROM AS P_VALIDFROM,
             p.VALIDUNTIL AS P_VALIDUNTIL,
             p.ORIGIN,
             p.DESTINATION,
             p.VIA,
             p.TRIPCOUNT,
             p.FARESTAGE,
             p.CANCELLED,
             p.CARDID,
             p.CARDACTIONID,
             p.EXPIRY   AS P_EXPIRY,
             a.ID       AS A_ID,
             a.CARDACTIONREQUESTID,
             a.EXPIRY,
             a.REGISTRATION,
             a.DATEOFBIRTH,
             a.VOICEFLAG,
             a.DEPOSIT,
             a.USERGROUP,
             a.PrintedCardNo,
             a.ISSUEFLAG,
             a.ISSUERID,
             a.REMOVEBIRTHDAY,
             a.GROUPEXPIRY,
             m.TARIFID,
             m.INTERNALNUMBER
        FROM PP_CARDACTIONREQUEST r
             LEFT JOIN PP_CARDACTIONREQUESTTYPE t
                 ON t.REQUESTTYPEID = r.REQUESTNO
             LEFT JOIN PP_PRODUCTONCARD p ON p.CARDACTIONID = r.ID
             LEFT JOIN PP_CARDACTIONATTRIBUTE a ON a.CARDACTIONREQUESTID = r.ID
             LEFT JOIN TM_TICKET m ON p.TICKETID = m.TICKETID
       WHERE     r.ISPROCESSED = 0
             AND NVL (t.addtoactionlist, 1) <> 0
             AND r.CARDNO > 0
    ORDER BY r.CARDNO, r.ID DESC;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
