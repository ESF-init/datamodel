SPOOL db_3_497.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.497', 'ARH', 'add new column to PP_TRIP');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE PP_TRIP
 ADD (REFMATRIXENTRYID  NUMBER(10));

COMMENT ON COLUMN PP_TRIP.REFMATRIXENTRYID IS 'Refrennce of the original matrix entry id';



-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;