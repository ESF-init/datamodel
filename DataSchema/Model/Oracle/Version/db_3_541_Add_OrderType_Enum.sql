INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.541', 'EMN', 'Add OrderType enum.');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_OrderType
(
  EnumerationValue  NUMBER(9)           NOT NULL,
  Literal           NVARCHAR2(50)       NOT NULL,
  Description       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_OrderType PRIMARY KEY (EnumerationValue)
);

COMMENT ON TABLE SL_OrderType IS 'Enumeration values for SL_Order.OrderType.';
COMMENT ON COLUMN SL_OrderType.EnumerationValue IS 'Type identifier.';
COMMENT ON COLUMN SL_OrderType.Literal IS 'Type literal.';
COMMENT ON COLUMN SL_OrderType.Description IS 'Type description.';


INSERT INTO SL_OrderType (EnumerationValue, Literal, Description) VALUES (0, 'Unknown', 'Unknown');
INSERT INTO SL_OrderType (EnumerationValue, Literal, Description) VALUES (1, 'Anonymous', 'Anonymous');
INSERT INTO SL_OrderType (EnumerationValue, Literal, Description) VALUES (2, 'Individual', 'Individual');
INSERT INTO SL_OrderType (EnumerationValue, Literal, Description) VALUES (3, 'Organizational', 'Organizational');

ALTER TRIGGER SL_ORDER_BRU DISABLE;
ALTER TABLE SL_ORDER ADD ORDERTYPE Number(9) DEFAULT 0 NOT NULL;
ALTER TRIGGER SL_ORDER_BRU ENABLE;
COMMENT ON COLUMN SL_ORDER.OrderType IS 'Type of order';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;