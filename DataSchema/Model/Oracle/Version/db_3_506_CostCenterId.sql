SPOOL db_3_506.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.506', 'FLF', 'Added COSTCENTERKEY to SL_TRANSACTIONJOURNAL');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (COSTCENTERKEY  VARCHAR2(100 BYTE));
 
COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.COSTCENTERKEY IS 'Key which identifies the cost center the transaction journal belongs to';


-- =======[ Commit ]===============================================================================================


COMMIT;

PROMPT Done!

SPOOL OFF;
