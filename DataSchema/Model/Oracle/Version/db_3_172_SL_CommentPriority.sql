SPOOL db_3_172.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.172', 'DST', 'Added primary key to SL_CommentPriority');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_COMMENTPRIORITY ADD CONSTRAINT PK_COMMENTPRIORITY PRIMARY KEY (ENUMERATIONVALUE);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
