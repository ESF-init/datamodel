SPOOL db_3_158.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.158', 'DST', 'Added relation to photograph for OrganizationParticipant');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PHOTOGRAPH MODIFY PERSONID NULL;
ALTER TABLE SL_OrganizationParticipant ADD PhotographId NUMBER(18);
ALTER TABLE SL_OrganizationParticipant ADD CONSTRAINT FK_OrgParticipant_Photograph FOREIGN KEY (PhotographId) REFERENCES SL_Photograph (PhotographId);

COMMENT ON COLUMN SL_OrganizationParticipant.PhotographId IS 'References SL_PHOTGRAPH.PHOTOGRAPHID';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
