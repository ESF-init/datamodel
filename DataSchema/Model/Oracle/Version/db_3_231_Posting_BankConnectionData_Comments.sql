SPOOL db_3_231.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.231', 'BVI', 'Added missing comments.');

COMMENT ON COLUMN SL_SEPAOwnerType.ENUMERATIONVALUE is 'Enumeration value/ID.';
COMMENT ON COLUMN SL_SEPAOwnerType.LITERAL is 'Enumeration literal/lookup text.';
COMMENT ON COLUMN SL_SEPAOwnerType.DESCRIPTION is 'Description for the enumeration value.';

COMMENT ON COLUMN SL_SEPAFrequencyType.ENUMERATIONVALUE is 'Enumeration value/ID.';
COMMENT ON COLUMN SL_SEPAFrequencyType.LITERAL is 'Enumeration literal/lookup text.';
COMMENT ON COLUMN SL_SEPAFrequencyType.DESCRIPTION is 'Description for the enumeration value.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
