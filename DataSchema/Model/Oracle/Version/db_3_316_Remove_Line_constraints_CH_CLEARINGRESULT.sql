SPOOL db_3_316.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.316', 'FRD', 'Remove Line constaints on ClearingResult table.');

ALTER TABLE CH_CLEARINGRESULT DROP CONSTRAINT FK_CLEARINGRESULT_LINEID;
ALTER TABLE CH_CLEARINGRESULT DROP CONSTRAINT FK_CLEARINGRESULT_LINEGROUPID;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
