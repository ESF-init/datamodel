SPOOL db_3_579.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.579', 'DST', 'Add foreign key to SL_Product.TaxTicketID');

ALTER TABLE SL_PRODUCT ADD CONSTRAINT FK_PRODUCT_TAXTICKET FOREIGN KEY (TAXTICKETID) REFERENCES TM_TICKET(TICKETID);


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
