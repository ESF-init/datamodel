SPOOL db_3_246.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.246', 'STV', 'Added SL_COMMENTTYPE  Enum');

-- =======[ Add Table ]===========================================================================
CREATE TABLE SL_CommentType (
    EnumerationValue NUMBER(9,0)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_CommentType PRIMARY KEY (EnumerationValue)
);

-- -------[ Enumerations ]------------------------------------------------------------------------------------------
INSERT INTO SL_CommentType (EnumerationValue, Literal, Description) VALUES (0, 'Miscellaneous', 'Miscellaneous');
INSERT INTO SL_CommentType (EnumerationValue, Literal, Description) VALUES (1, 'NoCreditWorthiness', 'NoCreditWorthiness');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TRIGGER SL_COMMENT_BRU DISABLE;
ALTER TABLE SL_COMMENT
ADD CommentType Number(9) default 0 constraint FK_COMMENT_COMMENTTYPE references SL_CommentType(EnumerationValue);
ALTER TRIGGER SL_COMMENT_BRU ENABLE;

-- =======[ Comments ]===============================================================================================
COMMENT ON TABLE SL_CommentType is 'Hold a predifiend types of comment that are linked to a contract';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;