SPOOL db_3_333.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.333', 'EPA', 'Added view ACC_BANKSTATEMENTVERIFICATION');

-- =======[ Views ]================================================================================================
CREATE OR REPLACE FORCE VIEW ACC_BANKSTATEMENTVERIFICATION
(
    BANKSTATEMENTID,
    PAYMENTJOURNALID,
    SALEID,
    MATCHEDSTATEMENTS,
    AMOUNT,
    PAYMENTREFERENCE, 
    TRANSACTIONDATE,
    BANKSTATEMENTSTATE,
    PAYMENTSTATE,
	RECORDTYPE,
	RECONCILIATIONSTATE)
AS SELECT 
        j.BANKSTATEMENTID as BANKSTATEMENTID,
        j.PAYMENTJOURNALID as PAYMENTJOURNALID,
        j.SALEID as SALEID,
        (case when j.PAYMENTJOURNALID is null or j.saleid is null then 0 else COUNT end) as MATCHEDSTATEMENTS, 
        bs.AMOUNT as AMOUNT,
        bs.PAYMENTREFERENCE as PAYMENTREFERENCE, 
        bs.TRANSACTIONDATE	as TRANSACTIONDATE,
        bs.STATE as BANKSTATEMENTSTATE,
        pj.STATE as PAYMENTSTATE,
		bs.RECORDTYPE as RECORDTYPE,
		pj.RECONCILIATIONSTATE as RECONCILIATIONSTATE
    from (
       SELECT 
            b.BANKSTATEMENTID as BANKSTATEMENTID, 
            count(b.bankstatementid) as COUNT, 
            min(p.PAYMENTJOURNALID) as PAYMENTJOURNALID, 
            min(s.SALEID) as SALEID
        FROM ACC_BANKSTATEMENT b
        left JOIN SL_PAYMENTJOURNAL p ON p.Amount = b.Amount and p.Code = b.PaymentReference and p.state = 1
        left JOIN SL_SALE s ON s.SALEID = p.SALEID and (
            (s.SALESCHANNELID = 70 and trunc(b.TRANSACTIONDATE) = trunc(p.executiontime)) OR
            (s.SALESCHANNELID <> 70 and trunc(b.TRANSACTIONDATE + (1/24*2.997)) = trunc(p.executiontime)))    
        group by b.BANKSTATEMENTID
    ) j
    left join SL_PAYMENTJOURNAL pj on pj.PAYMENTJOURNALID = j.PAYMENTJOURNALID
    left join ACC_BANKSTATEMENT bs on bs.BANKSTATEMENTID = j.BANKSTATEMENTID;

COMMENT ON COLUMN ACC_BANKSTATEMENTVERIFICATION.RECORDTYPE IS 'Record type of the bankstatement entry.';
COMMENT ON COLUMN ACC_BANKSTATEMENTVERIFICATION.RECONCILIATIONSTATE IS 'Reconciliation state of the payment journal entry.';



-- =======[ Commit ]===============================================================================================
COMMIT;
PROMPT Done!
SPOOL OFF;
