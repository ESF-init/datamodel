SPOOL db_3_559.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.559', 'FLF', 'Add index on DEVICENUMBER to SL_CREDITCARDTERMINAL');

-- =======[ Changes to Existing Tables ]===========================================================================

CREATE INDEX IDX_CCTERMINALDEVICENUMBER ON SL_CREDITCARDTERMINAL 
(DEVICENUMBER);



-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
