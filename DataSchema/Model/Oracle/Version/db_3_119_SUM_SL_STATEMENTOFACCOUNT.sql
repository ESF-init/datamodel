
SPOOL db_3_119.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.119', 'BTS', 'Creation of SL_StatementOfAccount');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_STATEMENTOFACCOUNT
(
    STATEMENTOFACCOUNTID       NUMBER(18,0)    NOT NULL,
	INVOICEID	NUMBER(18,0)    NOT NULL,    	
	CONTRACTID NUMBER(18,0) NOT NULL,
	RETURNDEBITREASONID NUMBER(18,0) NOT NULL,
	RETURNDEBITENDTOENDID	NUMBER(18,0) NULL,
	REASONOFPAYMENT    NVARCHAR2(250)  NOT NULL,
	BOOKINGDATE DATE NOT NULL,
	RETURNDEBITDATE DATE NULL,
	VALUEDATE DATE NULL,
	IMPORTFILEDATE DATE NULL,
	TOTALAMOUNT NUMBER(9,0) NULL,
	MANUALLYBOOKED NUMBER(9,0) NOT NULL,
	IMPORTEDFILENAME NVARCHAR2(250) NOT NULL,
	TYPEDISKIRMINATOR NVARCHAR2(50),
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_STATEMENTOFACCOUNTID PRIMARY KEY (STATEMENTOFACCOUNTID)
);

alter table SL_STATEMENTOFACCOUNT add constraint FK_STATEMENTOFACCOUNT_REASON foreign key (RETURNDEBITREASONID) references SEPA_RETURNDEBITREASON (RETURNDEBITREASONID);
alter table SL_STATEMENTOFACCOUNT add constraint FK_STATEMENTOFACCOUNT_CONTRACT foreign key (CONTRACTID) references SL_CONTRACT (CONTRACTID);
alter table SL_STATEMENTOFACCOUNT add constraint FK_STATEMENTOFACCOUNT_INVOICE foreign key (INVOICEID) references SL_INVOICE (INVOICEID);


COMMENT ON TABLE SL_STATEMENTOFACCOUNT is 'Table containing information about imported items from the statement of account';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.STATEMENTOFACCOUNTID is 'Primary key';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.REASONOFPAYMENT is 'Purpose of use about the imported item';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.CONTRACTID is 'References SL_CONTRACT.CONTRACTID';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.INVOICEID is 'References SL_INVOICE.INVOICEID';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.RETURNDEBITENDTOENDID is 'END to end Id used to identify the return debit';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.RETURNDEBITREASONID is 'References VARIO_RETURNDEBITREASON.RETURNDEBITREASONID';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.BOOKINGDATE is 'Date when the item from the statement of account was booked';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.RETURNDEBITDATE is 'Date of the return debit';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.VALUEDATE is 'Value Date of the item in the statement of account';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.IMPORTFILEDATE is 'Import Date of the account statement to which the transaction belongs';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.TOTALAMOUNT is 'The total amount of the imported item from the statement of account';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.IMPORTEDFILENAME is 'Imported file name of the account statement to which the transaction belongs. The filename is composed as follows: file name + Date when the statement of account was generated  + initialBalance';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.LastUser is 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.TYPEDISKIRMINATOR is 'Identifies the imported type of item';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.LastModified is 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_STATEMENTOFACCOUNT.TransactionCounter is 'Technical field: counter to prevent concurrent changes to this dataset';


-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

CREATE SEQUENCE SL_STATEMENTOFACCOUNT_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;

-- =======[ Trigger ]==============================================================================================
CREATE OR REPLACE TRIGGER SL_STATEMENTOFACCOUNT_BRI before insert on SL_STATEMENTOFACCOUNT for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_STATEMENTOFACCOUNT_BRU before update on SL_STATEMENTOFACCOUNT for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
