SPOOL db_3_381.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.381', 'SLR', 'Add table SL_CUSTOMERACCTOVERATTEMPTTYPE');

-- Start adding schema changes here


CREATE TABLE SL_CUSTOMERACCTOVERATTEMPTTYPE
(
  CUSTOMERACCOUNTID     	 NUMBER(18)       NOT NULL,
  VERIFICATIONATTEMPTTYPE    NUMBER(9)        NOT NULL,
  TOTALATTEMPTS              NUMBER(9)        DEFAULT 0 NOT NULL,
  LASTUSER            		 NVARCHAR2(50)    DEFAULT 'SYS'    NOT NULL,
  LASTMODIFIED        		 DATE             DEFAULT sysdate  NOT NULL,
  TRANSACTIONCOUNTER  		 INTEGER          NOT NULL
);

COMMENT ON TABLE SL_CUSTOMERACCTOVERATTEMPTTYPE IS 'This table is filled with the total login attempts since last successful login or password reset for each verification attempt type.';
COMMENT ON COLUMN SL_CUSTOMERACCTOVERATTEMPTTYPE.CUSTOMERACCOUNTID 				IS 'Reference to SL_CUSTOMERACCOUNT';
COMMENT ON COLUMN SL_CUSTOMERACCTOVERATTEMPTTYPE.VERIFICATIONATTEMPTTYPE 				IS 'Reference to SL_VERIFICATIONATTEMPTTYPE';
COMMENT ON COLUMN SL_CUSTOMERACCTOVERATTEMPTTYPE.TOTALATTEMPTS 				IS 'total login attempts since last successful login or password reset of a specific verification attempt type.';

COMMENT ON COLUMN SL_CUSTOMERACCTOVERATTEMPTTYPE.LASTMODIFIED 			IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_CUSTOMERACCTOVERATTEMPTTYPE.TRANSACTIONCOUNTER 		IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_CUSTOMERACCTOVERATTEMPTTYPE.LASTUSER 				IS 'Technical field: last (system) user that changed this dataset';

ALTER TABLE SL_CUSTOMERACCTOVERATTEMPTTYPE ADD (
  CONSTRAINT PK_CUSTOMERACCTOVERATTEMPTTYPE
  PRIMARY KEY
  (CUSTOMERACCOUNTID, VERIFICATIONATTEMPTTYPE));
  
ALTER TABLE SL_CUSTOMERACCTOVERATTEMPTTYPE ADD (
  CONSTRAINT FK_CUSTOMERACCTOVERATTEMPTTYPE
  FOREIGN KEY (CUSTOMERACCOUNTID) 
  REFERENCES SL_CUSTOMERACCOUNT (CUSTOMERACCOUNTID));


-- =======[ Trigger ]==============================================================================================
   create or replace trigger SL_CUSTACCTOVERATTEMPTTYPE_BRI
        before insert on SL_CUSTOMERACCTOVERATTEMPTTYPE 
        for each row begin    :new.TransactionCounter := dbms_utility.get_time;
         end;
/
   create or replace trigger SL_CUSTACCTOVERATTEMPTTYPE_BRU
        before update on SL_CUSTOMERACCTOVERATTEMPTTYPE 
        for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; 	:new.TransactionCounter := dbms_utility.get_time;
         end;
/

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
