SPOOL db_3_414.log;
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.414', 'FRD', 'ADD CH_VALIDATIONTRANSACTION');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE CH_VALIDATIONTRANSACTION
(
    VALIDATIONTRANSACTIONID     NUMBER(18),
    VALIDATIONRUNID         	NUMBER(18),
    RULEID                  	NUMBER(18),
    TRANSACTIONID           	NUMBER(18),
    ISVALID                 	NUMBER(1),
    RESOLVED                	NUMBER(1),
    ISCRITICAL              	NUMBER(1),
    LASTUSER                	NVARCHAR2(50)        DEFAULT 'SYS',
    LASTMODIFIED            	DATE                 DEFAULT sysdate,
    TRANSACTIONCOUNTER      	INTEGER,
    DATAROWCREATIONDATE     	DATE                 DEFAULT sysdate
);

COMMENT ON TABLE CH_VALIDATIONTRANSACTION IS 'The table collects clearing data for validated transactions.';
COMMENT ON COLUMN CH_VALIDATIONTRANSACTION.VALIDATIONTRANSACTIONID IS 'Id of the validated transaction information.';
COMMENT ON COLUMN CH_VALIDATIONTRANSACTION.VALIDATIONRUNID IS 'Id of the validation run (references SL_VALIDATIONRUN).';
COMMENT ON COLUMN CH_VALIDATIONTRANSACTION.RULEID IS 'Id of the validation rule (references SL_VALIDATIONRULE).';
COMMENT ON COLUMN CH_VALIDATIONTRANSACTION.TRANSACTIONID IS 'Id of the transaction for which additional information is saved (references RM_TRANSACTION).';
COMMENT ON COLUMN CH_VALIDATIONTRANSACTION.ISVALID IS 'Indicates if the transaction passed all validation checks (references SL_VALIDATIONRESULT).';
COMMENT ON COLUMN CH_VALIDATIONTRANSACTION.RESOLVED IS 'Indicates if a failed validation rule is resolved (references SL_VALIDATIONRESULT).';
COMMENT ON COLUMN CH_VALIDATIONTRANSACTION.ISCRITICAL IS 'Indicates if the validation rule is a critical rule.';
COMMENT ON COLUMN CH_VALIDATIONTRANSACTION.LASTUSER IS 'Technical field: last (system) user that changed this dataset.';
COMMENT ON COLUMN CH_VALIDATIONTRANSACTION.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset.';
COMMENT ON COLUMN CH_VALIDATIONTRANSACTION.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset.';
COMMENT ON COLUMN CH_VALIDATIONTRANSACTION.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

CREATE UNIQUE INDEX PK_VALIDATEDTRANSACTION ON CH_VALIDATIONTRANSACTION (VALIDATIONTRANSACTIONID);
CREATE UNIQUE INDEX UK_VALIDATEDTRANSACTION ON CH_VALIDATIONTRANSACTION(TRANSACTIONID);

CREATE OR REPLACE TRIGGER CH_VALIDATIONRESULT_BRI before insert on CH_VALIDATIONTRANSACTION for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER CH_VALIDATIONRESULT_BRU before update on CH_VALIDATIONTRANSACTION for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

ALTER TABLE CH_VALIDATIONTRANSACTION ADD (
  CONSTRAINT PK_VALIDATEDCLEARINGRESULT PRIMARY KEY (VALIDATIONTRANSACTIONID) USING INDEX PK_VALIDATEDTRANSACTION ENABLE VALIDATE,
  CONSTRAINT UK_VALIDATEDTRANSACTION UNIQUE (TRANSACTIONID) USING INDEX UK_VALIDATEDTRANSACTION ENABLE VALIDATE
);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;