SPOOL db_3_258.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.258', 'FRD', 'Daily Clearing Validation, add new critical rule identifier');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_VALIDATIONRULE ADD ISCRITICAL NUMBER(1) DEFAULT 0 NOT NULL;

COMMENT ON COLUMN SL_VALIDATIONRULE.ISCRITICAL IS 'Determines if the rule failure will eliminate the transaction from a clearing run.';

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;

