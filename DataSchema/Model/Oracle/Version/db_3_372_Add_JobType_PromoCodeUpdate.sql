SPOOL db_3_372.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.372', 'MMB', 'Job type added');

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (15, 'PromoCodeUpdate', 'Promotional Codes bulk updating');


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;