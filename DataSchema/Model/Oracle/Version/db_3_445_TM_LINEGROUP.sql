SPOOL db_3_445.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.445', 'FLF', 'Added linegroup to linegroup relationshsip');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE TM_LINEGROUP
 ADD (LINEGROUPTYPE  NUMBER(9)                      DEFAULT 0                     NOT NULL);

COMMENT ON COLUMN TM_LINEGROUP.LINEGROUPTYPE IS 'Type of the line group. 0 = Default, 1= Master line group';


-- =======[ New Tables ]===========================================================================================

CREATE TABLE TM_LINEGROUPTOLINEGROUP
(
  MASTERLINEGROUPID  NUMBER(18)                 NOT NULL,
  LINEGROUPID        NUMBER(18)                 NOT NULL
);

COMMENT ON TABLE TM_LINEGROUPTOLINEGROUP IS 'Defines a relation between master and sub linegroups. Master line groups can contain multiple sub line groups.';
COMMENT ON COLUMN TM_LINEGROUPTOLINEGROUP.MASTERLINEGROUPID IS 'ID of the master line group.';
COMMENT ON COLUMN TM_LINEGROUPTOLINEGROUP.LINEGROUPID IS 'ID of the sub linegroup';


ALTER TABLE TM_LINEGROUPTOLINEGROUP ADD (
  CONSTRAINT TM_LINEGROUPTOLINEGROUP_PK
  PRIMARY KEY
  (MASTERLINEGROUPID, LINEGROUPID));

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
