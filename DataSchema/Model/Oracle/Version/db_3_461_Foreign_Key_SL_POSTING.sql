SPOOL db_3_461.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.461', 'SVR', 'Alter table SL_POSTING, add foreign key to VARIO_SETTLEMENT.ID');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_POSTING ADD CONSTRAINT FK_POSTING_SETTLEMENT FOREIGN KEY (ACCSETTLEMENTID) REFERENCES VARIO_SETTLEMENT (ID);


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
