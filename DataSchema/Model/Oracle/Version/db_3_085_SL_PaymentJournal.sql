SPOOL db_3_085.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.085', 'DST', 'Added PersonID to SL_PaymentJournal.');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_paymentjournal add personid number(18); 

alter table sl_paymentjournal add constraint fk_paymentjournal_person foreign key (personid) references sl_person (personid);

comment on column sl_paymentjournal.personid is 'Invoice data for this payment.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
