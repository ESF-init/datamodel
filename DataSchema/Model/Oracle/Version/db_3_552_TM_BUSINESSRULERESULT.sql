SPOOL db_3_552.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.552', 'FLF', 'Add new field and comments to TM_BUSINESSRULERESULT');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE TM_BUSINESSRULERESULT
 ADD (GUIPANELNAME  VARCHAR2(300));


COMMENT ON COLUMN TM_BUSINESSRULERESULT.PERIODCALCULATIONMODEID IS 'Defines the period calculation mode which will be used at runtime.';

COMMENT ON COLUMN TM_BUSINESSRULERESULT.TRANSACTIONTYPEID IS 'Defines the transaction type which will be selected at runtime';

COMMENT ON COLUMN TM_BUSINESSRULERESULT.GUIPANELNAME IS 'Name of the gui panel the result points to. Used with gui xml only.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
