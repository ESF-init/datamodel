SPOOL db_3_300.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.300', 'STV', 'Change_SL_Workitem_Memo_to_2000_chars');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_WORKITEM MODIFY MEMO NVARCHAR2(2000);

COMMIT;

PROMPT Done!

SPOOL OFF;
