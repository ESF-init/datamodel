SPOOL db_3_074.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.074', 'BVI', 'Added CreationDateTime to SAM_SeasonTicket.');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

alter table SAM_SEASONTICKET add CreationDateTime Date;

COMMENT ON COLUMN SAM_SEASONTICKET.CreationDateTime IS 'The date time when the data row has been inserted.';

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Sequence ]---------------------------------------------------------------------------------------------

-- =======[ Trigger ]==============================================================================================

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
