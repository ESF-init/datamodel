SPOOL db_3_267.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.267', 'EPA', 'Add OriginalValidationResult to SL_ValidationResult');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_VALIDATIONRESULT
ADD ORIGINALVALIDATIONRESULTID NUMBER(18) DEFAULT NULL;



ALTER TABLE SL_VALIDATIONRESULT
ADD ( 
    CONSTRAINT FK_VALIDATIONRESULTID
    FOREIGN KEY (ORIGINALVALIDATIONRESULTID)
    REFERENCES SL_VALIDATIONRESULT(VALIDATIONRESULTID)
    );
	
COMMENT ON COLUMN SL_VALIDATIONRESULT.ORIGINALVALIDATIONRESULTID IS 'Optional reference to a preceding vaidation result entry.';


COMMIT;

PROMPT Done!

SPOOL OFF;
