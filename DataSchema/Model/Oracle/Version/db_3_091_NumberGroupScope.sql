SPOOL db_3_091.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.091', 'MIW', 'New number group scope: Primary Account Number');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_NUMBERGROUPSCOPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (13, 'Primary Account Number', 'Primary Account Number');

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
