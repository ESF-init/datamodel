SPOOL db_3_269.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.269', 'SLR', 'Added index on SL_SALE');

-- =======[ Changes to Existing Tables ]===========================================================================

CREATE INDEX IDX_SALE_RECEIPTREFERENCE ON SL_SALE
(RECEIPTREFERENCE)
NOLOGGING
NOPARALLEL;   
  
COMMIT;

PROMPT Done!

SPOOL OFF;
