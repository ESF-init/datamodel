SPOOL db_3_627.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.627', 'DST', 'Increase length of SL_WorkItem.Title');

alter table sl_workitem modify title nvarchar2(150);

COMMIT;

PROMPT Done!

SPOOL OFF;
