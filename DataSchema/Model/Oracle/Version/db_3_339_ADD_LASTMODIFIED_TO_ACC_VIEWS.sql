SPOOL db_3_339.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.339', 'SVR', 'Add column LASTMODIFIED to accounting views.');

-- Start adding schema changes here

-- =======[ Views ]================================================================================================

-- acc_cancel_rec_tap_cml_settled
CREATE OR REPLACE FORCE VIEW acc_cancel_rec_tap_cml_settled 
AS
   SELECT rr.revenuerecognitionid, rs.revenuesettlementid, rs.settlementtype,
          rr.amount, rr.premium, rr.basefare, rr.ticketnumber,
          rr.customergroup, rr.linegroupid, rr.operatorid,
          rr.creditaccountnumber, rr.debitaccountnumber, rr.postingdate,
          rr.postingreference, rr.includeinsettlement, cp.closeoutperiodid,
          cp.periodfrom, cp.periodto, cp.closeouttype, cp.state,
          tj1.transactionjournalid, tj1.transitaccountid, tj1.devicetime,
          tj1.fareamount, tj1.line, tj1.ticketinternalnumber, tj1.ticketid,
          tj1.transactiontype, tj1.clientid,
          tj1.operatorid AS transactionoperatorid, tj1.saleschannelid,
          tj1.resulttype, tj1.cancellationreferenceguid,
          tj1.cancellationreference, tj1.productid, tj1.pursebalance,
          tj1.pursecredit, tj1.tariffdate, tj1.tariffversion,
          tj1.tripticketinternalnumber, rr.LASTMODIFIED
     FROM acc_revenuerecognition rr
          LEFT JOIN
          (SELECT   MIN (sr1.revenuesettlementid) AS settlementid,
                    revenuerecognitionid
               FROM acc_settledrevenue sr1 INNER JOIN acc_revenuesettlement s1
                    ON s1.revenuesettlementid = sr1.revenuesettlementid
              WHERE s1.settlementtype = 1                          --CML based
           GROUP BY revenuerecognitionid) sr
          ON sr.revenuerecognitionid = rr.revenuerecognitionid
          LEFT JOIN acc_revenuesettlement rs
          ON rs.revenuesettlementid = sr.settlementid
        AND settlementtype = 1                                     --CML based
          INNER JOIN sl_transactionjournal tj1
          ON tj1.transactionjournalid = rr.transactionjournalid
          INNER JOIN acc_closeoutperiod cp
          ON cp.closeoutperiodid = rr.closeoutperiodid
          LEFT JOIN sl_transactionjournal tj2
          ON tj2.transactionjournalid = tj1.cancellationreference
    WHERE tj1.transactiontype = 128
      AND (    tj2.validto IS NULL
           AND (tj2.transactiontype = 66 OR tj2.transactiontype = 84)
          );

-- acc_cancel_rec_tap_tab_settled
CREATE OR REPLACE FORCE VIEW acc_cancel_rec_tap_tab_settled
AS
   SELECT rr.revenuerecognitionid, rs.revenuesettlementid, rs.settlementtype,
          rr.amount, rr.premium, rr.basefare, rr.ticketnumber,
          rr.customergroup, rr.linegroupid, rr.operatorid,
          rr.creditaccountnumber, rr.debitaccountnumber, rr.postingdate,
          rr.postingreference, rr.includeinsettlement, cp.closeoutperiodid,
          cp.periodfrom, cp.periodto, cp.closeouttype, cp.state,
          tj1.transactionjournalid, tj1.transitaccountid, tj1.devicetime,
          tj1.fareamount, tj1.line, tj1.ticketinternalnumber, tj1.ticketid,
          tj1.transactiontype, tj1.clientid,
          tj1.operatorid AS transactionoperatorid, tj1.saleschannelid,
          tj1.resulttype, tj1.cancellationreferenceguid,
          tj1.cancellationreference, tj1.productid, tj1.pursebalance,
          tj1.pursecredit, tj1.tariffdate, tj1.tariffversion,
          tj1.tripticketinternalnumber, rr.LASTMODIFIED
     FROM acc_revenuerecognition rr
          LEFT JOIN
          (SELECT   MIN (sr1.revenuesettlementid) AS settlementid,
                    revenuerecognitionid
               FROM acc_settledrevenue sr1 INNER JOIN acc_revenuesettlement s1
                    ON s1.revenuesettlementid = sr1.revenuesettlementid
              WHERE s1.settlementtype = 0                          --TAB based
           GROUP BY revenuerecognitionid) sr
          ON sr.revenuerecognitionid = rr.revenuerecognitionid
          LEFT JOIN acc_revenuesettlement rs
          ON rs.revenuesettlementid = sr.settlementid
        AND settlementtype = 0                                     --TAB based
          INNER JOIN sl_transactionjournal tj1
          ON tj1.transactionjournalid = rr.transactionjournalid
          INNER JOIN acc_closeoutperiod cp
          ON cp.closeoutperiodid = rr.closeoutperiodid
          LEFT JOIN sl_transactionjournal tj2
          ON tj2.transactionjournalid = tj1.cancellationreference
    WHERE tj1.transactiontype = 128
      AND (    tj2.validto IS NULL
           AND (tj2.transactiontype = 66 OR tj2.transactiontype = 84)
          );

-- acc_rec_load_use_cml_settled
CREATE OR REPLACE FORCE VIEW acc_rec_load_use_cml_settled 
AS
   SELECT rr.revenuerecognitionid, rs.revenuesettlementid, rs.settlementtype,
          rr.amount, rr.premium, rr.basefare, rr.ticketnumber,
          rr.customergroup, rr.linegroupid, rr.operatorid,
          rr.creditaccountnumber, rr.debitaccountnumber, rr.postingdate,
          rr.postingreference, rr.includeinsettlement, cp.closeoutperiodid,
          cp.periodfrom, cp.periodto, cp.closeouttype, cp.state,
          tj1.transactionjournalid, tj1.transitaccountid, tj1.devicetime,
          tj1.fareamount, tj1.line, tj1.ticketinternalnumber, tj1.ticketid,
          tj1.transactiontype, tj1.clientid,
          tj1.operatorid AS transactionoperatorid, tj1.saleschannelid,
          tj1.resulttype, tj1.cancellationreferenceguid,
          tj1.cancellationreference, tj1.productid, tj1.pursebalance,
          tj1.pursecredit, tj1.tariffdate, tj1.tariffversion,
          tj1.tripticketinternalnumber, rr.LASTMODIFIED,
          CAST
             (DECODE (tj1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)
             ) AS cancelledtap
     FROM acc_revenuerecognition rr
          LEFT JOIN
          (SELECT   MIN (sr1.revenuesettlementid) AS settlementid,
                    revenuerecognitionid
               FROM acc_settledrevenue sr1 INNER JOIN acc_revenuesettlement s1
                    ON s1.revenuesettlementid = sr1.revenuesettlementid
              WHERE s1.settlementtype = 1                          --CML based
           GROUP BY revenuerecognitionid) sr
          ON sr.revenuerecognitionid = rr.revenuerecognitionid
          LEFT JOIN acc_revenuesettlement rs
          ON rs.revenuesettlementid = sr.settlementid
        AND settlementtype = 1                                     --CML based
          INNER JOIN sl_transactionjournal tj1
          ON tj1.transactionjournalid = rr.transactionjournalid
          INNER JOIN acc_closeoutperiod cp
          ON cp.closeoutperiodid = rr.closeoutperiodid
    WHERE tj1.transactiontype = 108
       OR tj1.transactiontype = 85
       OR (tj1.transactiontype = 66 AND tj1.validto IS NOT NULL);

-- acc_rec_load_use_tab_settled
CREATE OR REPLACE FORCE VIEW acc_rec_load_use_tab_settled 
AS
   SELECT rr.revenuerecognitionid, rs.revenuesettlementid, rs.settlementtype,
          rr.amount, rr.premium, rr.basefare, rr.ticketnumber,
          rr.customergroup, rr.linegroupid, rr.operatorid,
          rr.creditaccountnumber, rr.debitaccountnumber, rr.postingdate,
          rr.postingreference, rr.includeinsettlement, cp.closeoutperiodid,
          cp.periodfrom, cp.periodto, cp.closeouttype, cp.state,
          tj1.transactionjournalid, tj1.transitaccountid, tj1.devicetime,
          tj1.fareamount, tj1.line, tj1.ticketinternalnumber, tj1.ticketid,
          tj1.transactiontype, tj1.clientid,
          tj1.operatorid AS transactionoperatorid, tj1.saleschannelid,
          tj1.resulttype, tj1.cancellationreferenceguid,
          tj1.cancellationreference, tj1.productid, tj1.pursebalance,
          tj1.pursecredit, tj1.tariffdate, tj1.tariffversion,
          tj1.tripticketinternalnumber, rr.LASTMODIFIED,
          CAST
             (DECODE (tj1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)
             ) AS cancelledtap
     FROM acc_revenuerecognition rr
          LEFT JOIN
          (SELECT   MIN (sr1.revenuesettlementid) AS settlementid,
                    revenuerecognitionid
               FROM acc_settledrevenue sr1 INNER JOIN acc_revenuesettlement s1
                    ON s1.revenuesettlementid = sr1.revenuesettlementid
              WHERE s1.settlementtype = 0                          --TAB based
           GROUP BY revenuerecognitionid) sr
          ON sr.revenuerecognitionid = rr.revenuerecognitionid
          LEFT JOIN acc_revenuesettlement rs
          ON rs.revenuesettlementid = sr.settlementid
        AND settlementtype = 0                                     --TAB based
          INNER JOIN sl_transactionjournal tj1
          ON tj1.transactionjournalid = rr.transactionjournalid
          INNER JOIN acc_closeoutperiod cp
          ON cp.closeoutperiodid = rr.closeoutperiodid
    WHERE tj1.transactiontype = 108
       OR tj1.transactiontype = 85
       OR (tj1.transactiontype = 66 AND tj1.validto IS NOT NULL);

-- acc_recognized_tap_cml_settled
CREATE OR REPLACE FORCE VIEW acc_recognized_tap_cml_settled 
AS
   SELECT rr.revenuerecognitionid, rs.revenuesettlementid, rs.settlementtype,
          rr.amount, rr.premium, rr.basefare, rr.ticketnumber,
          rr.customergroup, rr.linegroupid, rr.operatorid,
          rr.creditaccountnumber, rr.debitaccountnumber, rr.postingdate,
          rr.postingreference, rr.includeinsettlement, cp.closeoutperiodid,
          cp.periodfrom, cp.periodto, cp.closeouttype, cp.state,
          tj1.transactionjournalid, tj1.transitaccountid, tj1.devicetime,
          tj1.fareamount, tj1.line, tj1.ticketinternalnumber, tj1.ticketid,
          tj1.transactiontype, tj1.clientid,
          tj1.operatorid AS transactionoperatorid, tj1.saleschannelid,
          tj1.resulttype, tj1.cancellationreferenceguid,
          tj1.cancellationreference, tj1.productid, tj1.pursebalance,
          tj1.pursecredit, tj1.tariffdate, tj1.tariffversion,
          tj1.tripticketinternalnumber, rr.LASTMODIFIED,
          CAST
             (DECODE (tj1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)
             ) AS cancelledtap
     FROM acc_revenuerecognition rr
          LEFT JOIN
          (SELECT   MIN (sr1.revenuesettlementid) AS settlementid,
                    revenuerecognitionid
               FROM acc_settledrevenue sr1 INNER JOIN acc_revenuesettlement s1
                    ON s1.revenuesettlementid = sr1.revenuesettlementid
              WHERE s1.settlementtype = 1                          --CML based
           GROUP BY revenuerecognitionid) sr
          ON sr.revenuerecognitionid = rr.revenuerecognitionid
          LEFT JOIN acc_revenuesettlement rs
          ON rs.revenuesettlementid = sr.settlementid
        AND settlementtype = 1                                     --CML based
          INNER JOIN sl_transactionjournal tj1
          ON tj1.transactionjournalid = rr.transactionjournalid
          INNER JOIN acc_closeoutperiod cp
          ON cp.closeoutperiodid = rr.closeoutperiodid
    WHERE tj1.validto IS NULL
      AND (tj1.transactiontype = 66 OR tj1.transactiontype = 84);

-- acc_recognized_tap_tab_settled
CREATE OR REPLACE FORCE VIEW acc_recognized_tap_tab_settled
AS
   SELECT rr.revenuerecognitionid, rs.revenuesettlementid, rs.settlementtype,
          rr.amount, rr.premium, rr.basefare, rr.ticketnumber,
          rr.customergroup, rr.linegroupid, rr.operatorid,
          rr.creditaccountnumber, rr.debitaccountnumber, rr.postingdate,
          rr.postingreference, rr.includeinsettlement, cp.closeoutperiodid,
          cp.periodfrom, cp.periodto, cp.closeouttype, cp.state,
          tj1.transactionjournalid, tj1.transitaccountid, tj1.devicetime,
          tj1.fareamount, tj1.line, tj1.ticketinternalnumber, tj1.ticketid,
          tj1.transactiontype, tj1.clientid,
          tj1.operatorid AS transactionoperatorid, tj1.saleschannelid,
          tj1.resulttype, tj1.cancellationreferenceguid,
          tj1.cancellationreference, tj1.productid, tj1.pursebalance,
          tj1.pursecredit, tj1.tariffdate, tj1.tariffversion,
          tj1.tripticketinternalnumber, rr.LASTMODIFIED,
          CAST
             (DECODE (tj1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)
             ) AS cancelledtap
     FROM acc_revenuerecognition rr
          LEFT JOIN
          (SELECT   MIN (sr1.revenuesettlementid) AS settlementid,
                    revenuerecognitionid
               FROM acc_settledrevenue sr1 INNER JOIN acc_revenuesettlement s1
                    ON s1.revenuesettlementid = sr1.revenuesettlementid
              WHERE s1.settlementtype = 0                          --TAB based
           GROUP BY revenuerecognitionid) sr
          ON sr.revenuerecognitionid = rr.revenuerecognitionid
          LEFT JOIN acc_revenuesettlement rs
          ON rs.revenuesettlementid = sr.settlementid
        AND settlementtype = 0                                     --TAB based
          INNER JOIN sl_transactionjournal tj1
          ON tj1.transactionjournalid = rr.transactionjournalid
          INNER JOIN acc_closeoutperiod cp
          ON cp.closeoutperiodid = rr.closeoutperiodid
    WHERE tj1.validto IS NULL
      AND (tj1.transactiontype = 66 OR tj1.transactiontype = 84);


-- acc_recognizedloadanduse
CREATE OR REPLACE FORCE VIEW acc_recognizedloadanduse
AS
   SELECT rr.amount, rr.basefare, rr.creditaccountnumber, rr.customergroup,
          rr.debitaccountnumber, rr.linegroupid, rr.postingdate,
          rr.postingreference, rr.premium, rr.revenuerecognitionid,
          rr.revenuesettlementid, rr.ticketnumber, rr.operatorid,
          cp.closeoutperiodid, cp.periodfrom, cp.periodto, cp.closeouttype,
          cp.state, tj1.cancellationreference, tj1.cancellationreferenceguid,
          tj1.clientid, tj1.devicetime, tj1.fareamount, tj1.line,
          tj1.operatorid AS transactionoperatorid, tj1.productid,
          tj1.pursebalance, tj1.pursecredit, tj1.resulttype,
          tj1.saleschannelid, tj1.tariffdate, tj1.tariffversion, tj1.ticketid,
          tj1.ticketinternalnumber, tj1.transactionjournalid,
          tj1.transactiontype, tj1.tripticketinternalnumber, rr.LASTMODIFIED,
          CAST
             (DECODE (tj1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)
             ) AS cancelledtap,
          tj1.datarowcreationdate
     FROM (sl_transactionjournal tj1 INNER JOIN acc_revenuerecognition rr
          ON tj1.transactionjournalid = rr.transactionjournalid)
          INNER JOIN
          acc_closeoutperiod cp ON cp.closeoutperiodid = rr.closeoutperiodid
    WHERE tj1.transactiontype = 108
       OR tj1.transactiontype = 85
       OR (tj1.transactiontype = 66 AND tj1.validto IS NOT NULL);

-- acc_recognizedsaletransactions
CREATE OR REPLACE FORCE VIEW acc_recognizedsaletransactions
AS
   SELECT cp.closeoutperiodid, cp.periodfrom, cp.periodto, cp.closeoutdate,
          cp.state, cp.closeouttype, sr.salesrevenueid, sr.postingdate,
          sr.postingreference, sr.creditaccountnumber, sr.debitaccountnumber,
          sr.amount, tj.transactionjournalid, tj.transactionid, tj.line,
          tj.coursenumber, tj.routenumber, tj.routecode, tj.direction,
          tj.ZONE, tj.vehiclenumber, tj.mountingplatenumber, tj.debtornumber,
          tj.stopnumber, tj.transitaccountid, tj.faremediaid,
          tj.faremediatype, tj.productid, tj.ticketid, tj.fareamount,
          tj.customergroup, tj.transactiontype, tj.resulttype, tj.filllevel,
          tj.pursebalance, tj.pursecredit, tj.groupsize, tj.validfrom,
          tj.validto, tj.DURATION, tj.tariffdate, tj.tariffversion,
          tj.ticketinternalnumber, tj.cancellationreference,
          tj.cancellationreferenceguid, tj.tripticketinternalnumber,
          tj.productid2, tj.pursebalance2, tj.pursecredit2, s.saleid,
          s.saletransactionid, s.saletype, s.saledate, s.clientid,
          s.saleschannelid, s.locationnumber, s.devicenumber,
          s.salespersonnumber, s.merchantnumber, s.receiptreference,
          s.networkreference,
          s.cancellationreferenceid AS salecancellationreferenceid,
          s.isclosed, s.orderid, s.externalordernumber, s.contractid,
          s.refundreferenceid, s.isorganizational, sr.LASTMODIFIED
     FROM acc_closeoutperiod cp,
          acc_salesrevenue sr,
          sl_transactionjournal tj,
          sl_sale s
    WHERE cp.closeoutperiodid = sr.closeoutperiodid
      AND sr.transactionjournalid = tj.transactionjournalid
      AND tj.saleid = s.saleid;

-- acc_recognizedtap 
CREATE OR REPLACE FORCE VIEW acc_recognizedtap 
AS
   SELECT rr.amount, rr.basefare, rr.creditaccountnumber, rr.customergroup,
          rr.debitaccountnumber, rr.linegroupid, rr.postingdate,
          rr.postingreference, rr.premium, rr.revenuerecognitionid,
          rr.revenuesettlementid, rr.ticketnumber, rr.operatorid,
          cp.closeoutperiodid, cp.periodfrom, cp.periodto, cp.closeouttype,
          cp.state, tj1.cancellationreference, tj1.cancellationreferenceguid,
          tj1.clientid, tj1.devicetime, tj1.fareamount, tj1.line,
          tj1.operatorid AS transactionoperatorid, tj1.productid,
          tj1.pursebalance, tj1.pursecredit, tj1.resulttype,
          tj1.saleschannelid, tj1.tariffdate, tj1.tariffversion, tj1.ticketid,
          tj1.ticketinternalnumber, tj1.transactionjournalid,
          tj1.transactiontype, tj1.tripticketinternalnumber, rr.LASTMODIFIED,
          CAST
             (DECODE (tj1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)
             ) AS cancelledtap,
          tj1.datarowcreationdate
     FROM ((sl_transactionjournal tj1 INNER JOIN acc_revenuerecognition rr
          ON tj1.transactionjournalid = rr.transactionjournalid)
          INNER JOIN
          acc_closeoutperiod cp ON cp.closeoutperiodid = rr.closeoutperiodid)
    WHERE tj1.validto IS NULL
      AND (tj1.transactiontype = 66 OR tj1.transactiontype = 84);

-- acc_recognizedtapcancellation
CREATE OR REPLACE FORCE VIEW acc_recognizedtapcancellation
AS
   SELECT rr.amount, rr.basefare, rr.creditaccountnumber, rr.customergroup,
          rr.debitaccountnumber, rr.linegroupid, rr.postingdate,
          rr.postingreference, rr.premium, rr.revenuerecognitionid,
          rr.revenuesettlementid, rr.ticketnumber, rr.operatorid,
          cp.closeoutperiodid, cp.periodfrom, cp.periodto, cp.closeouttype,
          cp.state, tj1.cancellationreference, tj1.cancellationreferenceguid,
          tj1.clientid, tj1.devicetime, tj1.fareamount, tj1.line,
          tj1.operatorid AS transactionoperatorid, tj1.productid,
          tj1.pursebalance, tj1.pursecredit, tj1.resulttype,
          tj1.saleschannelid, tj1.tariffdate, tj1.tariffversion, tj1.ticketid,
          tj1.ticketinternalnumber, tj1.transactionjournalid,
          tj1.transactiontype, tj1.tripticketinternalnumber, rr.LASTMODIFIED
     FROM ((sl_transactionjournal tj1 INNER JOIN acc_revenuerecognition rr
          ON tj1.transactionjournalid = rr.transactionjournalid)
          INNER JOIN
          acc_closeoutperiod cp ON cp.closeoutperiodid = rr.closeoutperiodid)
          LEFT JOIN
          sl_transactionjournal tj2
          ON tj2.transactionjournalid = tj1.cancellationreference
    WHERE tj1.transactiontype = 128
      AND (    tj2.validto IS NULL
           AND (tj2.transactiontype = 66 OR tj2.transactiontype = 84)
          );

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
