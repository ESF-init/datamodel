SPOOL db_3_286.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.286', 'ARH', 'Create tables for the Regiomove Project.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

--
-- SL_PROVIDERACCOUNT_STATUS
--
CREATE TABLE SL_PROVIDERACCOUNT_STATE
(
	EnumerationValue  NUMBER(9)		NOT NULL,
	Literal           NVARCHAR2(50)		NOT NULL,
	Description       NVARCHAR2(80)		NOT NULL
);
ALTER TABLE SL_PROVIDERACCOUNT_STATE ADD CONSTRAINT PK_PROVIDERACCOUNT_STATE PRIMARY KEY (EnumerationValue);

COMMENT ON TABLE SL_PROVIDERACCOUNT_STATE IS 'Enumeration of SL_CONTRACTTOPROVIDER State.';
COMMENT ON COLUMN SL_PROVIDERACCOUNT_STATE.ENUMERATIONVALUE IS 'Enumeration id.';
COMMENT ON COLUMN SL_PROVIDERACCOUNT_STATE.LITERAL IS 'Enumeration short name.';
COMMENT ON COLUMN SL_PROVIDERACCOUNT_STATE.Description IS 'Enumeration description..';

--
-- SL_BOOKINGTYPE
--
CREATE TABLE SL_BOOKINGTYPE(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL
);
ALTER TABLE SL_BOOKINGTYPE ADD CONSTRAINT PK_BOOKINGTYPE PRIMARY KEY (EnumerationValue);

COMMENT ON TABLE SL_BOOKINGTYPE IS 'Enumeration of all valid booking item types in the system.';
COMMENT ON COLUMN SL_BOOKINGTYPE.ENUMERATIONVALUE IS 'Unique id of the booking type';
COMMENT ON COLUMN SL_BOOKINGTYPE.LITERAL IS 'Name of the booking type; used internally';
COMMENT ON COLUMN SL_BOOKINGTYPE.DESCRIPTION IS 'Description of the booking type. Used in the UI to represent the sale type';

--
-- SL_MOBILITYPROVIDER
--
CREATE TABLE SL_MOBILITYPROVIDER
(
  MOBILITYPROVIDERID  NUMBER(18)                NOT NULL,
  PROVIDERNAME        VARCHAR2(50)              NOT NULL,
  ADDRESSID           NUMBER(18)
);

ALTER TABLE SL_MOBILITYPROVIDER ADD CONSTRAINT PK_MOBILITYPROVIDER PRIMARY KEY (MOBILITYPROVIDERID);
ALTER TABLE SL_MOBILITYPROVIDER ADD CONSTRAINT FK_MOBILITYPROVIDER_ADDRESS FOREIGN KEY (ADDRESSID) REFERENCES SL_ADDRESS (ADDRESSID);

COMMENT ON TABLE SL_MOBILITYPROVIDER IS 'This table contains information about the mobility providers : the providers involved in the REGIOMOVE project.';
COMMENT ON COLUMN SL_MOBILITYPROVIDER.MOBILITYPROVIDERID IS 'Unique id of the of a mobility provider.';
COMMENT ON COLUMN SL_MOBILITYPROVIDER.PROVIDERNAME IS 'Name of the mobility provider.';
COMMENT ON COLUMN SL_MOBILITYPROVIDER.ADDRESSID IS 'Reference to SL_ADDRESS.ADDRESSID.';

--
-- SL_CONTRACTTOPROVIDER
--
CREATE TABLE SL_CONTRACTTOPROVIDER
(
  CONTRACTTOPROVIDERID  NUMBER(18)                NOT NULL,
  CONTRACTID            NUMBER(18)                NOT NULL,
  MOBILITYPROVIDERID    NUMBER(18)                NOT NULL,
  PROVIDERREFERENCE     VARCHAR2(20),
  STATE					NUMBER(9)				default 0			     
);

ALTER TABLE SL_CONTRACTTOPROVIDER ADD CONSTRAINT PK_CONTRACTTOPROVIDER PRIMARY KEY (CONTRACTTOPROVIDERID);
ALTER TABLE SL_CONTRACTTOPROVIDER ADD CONSTRAINT FK_CONTRACTTOPROVIDER_CONTRACT FOREIGN KEY (CONTRACTID) REFERENCES SL_CONTRACT (CONTRACTID);
ALTER TABLE SL_CONTRACTTOPROVIDER ADD CONSTRAINT FK_CONTRACTTOPROVIDER FOREIGN KEY (MOBILITYPROVIDERID) REFERENCES SL_MOBILITYPROVIDER (MOBILITYPROVIDERID);

COMMENT ON TABLE SL_CONTRACTTOPROVIDER IS 'This table make the links between the contracts and the mobility providers (REGIOMOVE project).';
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.CONTRACTTOPROVIDERID IS 'Unique id of the table SL_CONTRACTTOPROVIDER.';
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.CONTRACTID IS 'SL_CONTRACT.CONTRACTID';
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.MOBILITYPROVIDERID IS 'Reference to SL_MOBILITYPROVIDER.MOBILITYPROVIDERID.';
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.PROVIDERREFERENCE IS 'Provider reference that has the customer to the mobility provider.';
COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.STATE IS 'State. Link with SL_PROVIDERACCOUNT_STATE';

--
-- SL_BOOKING
--
CREATE TABLE SL_BOOKING
(
  BOOKINGID     NUMBER(18)                          NOT NULL,
  CARDHOLDERID  NUMBER(18)                          NOT NULL,
  BOOKINGDATE   DATE
);

ALTER TABLE SL_BOOKING ADD CONSTRAINT PK_BOOKING PRIMARY KEY (BOOKINGID);
ALTER TABLE SL_BOOKING ADD CONSTRAINT FK_BOOKING_CARD FOREIGN KEY (CARDHOLDERID) REFERENCES SL_PERSON (PERSONID);

COMMENT ON TABLE SL_BOOKING IS 'Represents a booking in RegioMove and serves to group bookingitems.';
COMMENT ON COLUMN SL_BOOKING.BOOKINGID IS 'Unique ID of the booking.';
COMMENT ON COLUMN SL_BOOKING.CARDHOLDERID IS 'References SL_CARD.CARDHOLDERID.';
COMMENT ON COLUMN SL_BOOKING.BOOKINGDATE IS 'Date and time when the booking were completed.';

--
-- SL_BOOKINGITEM
--
CREATE TABLE SL_BOOKINGITEM
(
  BOOKINGITEMID      NUMBER(18)                 NOT NULL,
  BOOKINGID          NUMBER(18)                 NOT NULL,
  MOBILITYPROVIDERID NUMBER(18)                 NOT NULL,
  SALETYPE           NUMBER(9)                  NOT NULL,
  STATE				NUMBER(9)                   DEFAULT 0,
  STARTPLACE        NVARCHAR2(50),
  ENDPLACE          NVARCHAR2(50),
  STARTTIME         DATE                        NOT NULL,
  ENDTIME           DATE                        NOT NULL,
  AMOUNT            NUMBER(18)                  NOT NULL
);

ALTER TABLE SL_BOOKINGITEM ADD CONSTRAINT PK_BOOKINGITEM PRIMARY KEY (BOOKINGITEMID);
ALTER TABLE SL_BOOKINGITEM ADD CONSTRAINT FK_BOOKINGITEM_BOOKING FOREIGN KEY (BOOKINGID) REFERENCES SL_BOOKING (BOOKINGID);
ALTER TABLE SL_BOOKINGITEM ADD CONSTRAINT FK_BOOKING_PROVIDER FOREIGN KEY (MOBILITYPROVIDERID) REFERENCES SL_MOBILITYPROVIDER (MOBILITYPROVIDERID);

COMMENT ON TABLE SL_BOOKINGITEM IS 'Represents one booking element of a booking (SL_BOOKINGITEM).';
COMMENT ON COLUMN SL_BOOKINGITEM.BOOKINGITEMID IS 'Unique ID of the booking item.';
COMMENT ON COLUMN SL_BOOKINGITEM.BOOKINGID IS 'References SL_BOOKING.BOOKINGID.';
COMMENT ON COLUMN SL_BOOKINGITEM.MOBILITYPROVIDERID IS 'References SL_MOBILITYPROVIDER.';
COMMENT ON COLUMN SL_BOOKINGITEM.SALETYPE IS 'References SL_SALETYPE.SALETYPEID.';
COMMENT ON COLUMN SL_BOOKINGITEM.STATE IS 'State of the booking item.';
COMMENT ON COLUMN SL_BOOKINGITEM.STARTPLACE IS 'Starting place of the trip';
COMMENT ON COLUMN SL_BOOKINGITEM.ENDPLACE IS 'End place of the trip';
COMMENT ON COLUMN SL_BOOKINGITEM.STARTTIME IS 'Start time of the booking';
COMMENT ON COLUMN SL_BOOKINGITEM.ENDTIME IS 'End time of the booking';
COMMENT ON COLUMN SL_BOOKINGITEM.AMOUNT IS 'Transaction amount of the booking';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(10) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_MOBILITYPROVIDER','SL_CONTRACTTOPROVIDER', 'SL_BOOKING','SL_BOOKINGITEM');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
--
-- SL_MOBILITYPROVIDER
--

-- JED: Removed trgger creation, because required TransactionCounter is not available. 
--      Trigger are created in db_3_292_Created_RegioMove_Table.sql.

-- =======[ Data ]==================================================================================================


-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_PROVIDERACCOUNT_STATE (EnumerationValue, Literal, Description)
VALUES (0, 'Inactive', 'The customer does not have link with this mobility provider.');
INSERT INTO SL_PROVIDERACCOUNT_STATE (EnumerationValue, Literal, Description)
VALUES (1, 'Active', 'The customer created the account to have mobility with this provider.');
INSERT INTO SL_PROVIDERACCOUNT_STATE (EnumerationValue, Literal, Description)
VALUES (2, 'Pending', 'The customer does not fill all the criteria for using the mobility provider.');

INSERT INTO SL_BOOKINGTYPE (EnumerationValue, Literal, Description)
VALUES (0, 'Requested', 'Request was sent.');
INSERT INTO SL_BOOKINGTYPE (EnumerationValue, Literal, Description)
VALUES (1, 'Booked', 'Booking is completed.');
INSERT INTO SL_BOOKINGTYPE (EnumerationValue, Literal, Description)
VALUES (2, 'Pending', 'Waiting for an answer.');
INSERT INTO SL_BOOKINGTYPE (EnumerationValue, Literal, Description)
VALUES (3, 'Error', 'Error on request.');

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
