SPOOL db_3_082.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.082', 'SOE', 'Created new View SL_INVOICE_VIEW');

-- =======[ Views ]===========================================================================

CREATE OR REPLACE FORCE VIEW SL_INVOICE_VIEW
(NAME,
ORGANIZATIONNUMBER,
INVOICEENTRYAMOUNT,
CREATIONDATE,
FROMDATE,
INVOICEENTRYCOUNT,
INVOICEID,
INVOICENUMBER,
INVOICETYPE,
TODATE
)
AS
SELECT 
o.NAME,
o.ORGANIZATIONNUMBER,
i.INVOICEENTRYAMOUNT,
i.CREATIONDATE,
i.FROMDATE,
i.INVOICEENTRYCOUNT,
i.INVOICEID,
i.INVOICENUMBER,
i.INVOICETYPE,
i.TODATE
FROM sl_invoice  i
LEFT JOIN sl_contract co ON i.CONTRACTID = co.CONTRACTID
INNER JOIN sl_organization o ON o.ORGANIZATIONID = co.ORGANIZATIONID;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
