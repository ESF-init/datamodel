SPOOL db_3_340.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.340', 'ARH', 'Add Column RENTALSTATETIME to SL_BOOKINGITEM');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_BOOKINGITEM ADD RENTALSTATETIME 	DATE    NOT NULL;

COMMENT ON COLUMN SL_BOOKINGITEM.RENTALSTATETIME IS 'Timestamp for last change of RentatState.';

COMMIT;

PROMPT Done!

SPOOL OFF;
