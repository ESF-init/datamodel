SPOOL db_3_503.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.503', 'FLF', 'Added ExternalTicketName to SL_TRANSACTIONJOURNAL and TM_BUSINESSRULERESULT');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE TM_BUSINESSRULERESULT
 ADD (EXTERNALTICKETNAME  VARCHAR2(200 BYTE));
 
COMMENT ON COLUMN TM_BUSINESSRULERESULT.EXTERNALTICKETNAME IS 'External name of the selected ticket. Translated to AccountCode on device side.';


ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (EXTERNALTICKETNAME  VARCHAR2(200));

COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.EXTERNALTICKETNAME IS 'External name of the selected ticket. Translated to AccountCode on device side.';

-- =======[ Commit ]===============================================================================================


COMMIT;

PROMPT Done!

SPOOL OFF;
