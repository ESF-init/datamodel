SPOOL db_3_348.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.348', 'MIW', 'Add table CH_CARDLIABILITY');

-- Start adding schema changes here

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE CH_CARDLIABILITY
(
  CARDLIABILITYID     NUMBER(10)       NOT NULL,
  FINANCEDATE         DATE             NOT NULL,
  CARDID              NUMBER(10)       NOT NULL,
  CARDBALANCE         NUMBER(10)       NOT NULL,
  CARDTRANSACTIONCOUNTER    NUMBER(10) NOT NULL,
  TRANSACTIONDATE     DATE             NOT NULL,
  LASTUSER            NVARCHAR2(50)    DEFAULT 'SYS'    NOT NULL,
  LASTMODIFIED        DATE             DEFAULT sysdate  NOT NULL,
  TRANSACTIONCOUNTER  INTEGER          NOT NULL
);
ALTER TABLE CH_CARDLIABILITY ADD CONSTRAINT PK_CARDLIABILITY PRIMARY KEY (CARDLIABILITYID);

COMMENT ON TABLE CH_CARDLIABILITY IS 'This table is filled daily with the last reported card balance and transaction counter for a card.';

COMMENT ON COLUMN CH_CARDLIABILITY.FINANCEDATE 				IS 'Finance date this record belongs to';
COMMENT ON COLUMN CH_CARDLIABILITY.CARDID                   IS 'Reference to card id from SL_CARD';
COMMENT ON COLUMN CH_CARDLIABILITY.CARDBALANCE              IS 'Card balance of the transaction for the card.';
COMMENT ON COLUMN CH_CARDLIABILITY.CARDTRANSACTIONCOUNTER   IS 'Card transaction counter of the transaction for the card.';
COMMENT ON COLUMN CH_CARDLIABILITY.TRANSACTIONDATE          IS 'Date and time of the transaction for the card.';

COMMENT ON COLUMN CH_CARDLIABILITY.LASTMODIFIED 			IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN CH_CARDLIABILITY.TRANSACTIONCOUNTER 		IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN CH_CARDLIABILITY.LASTUSER 				IS 'Technical field: last (system) user that changed this dataset';

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(10) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('CH_CARDLIABILITY');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('CH_CARDLIABILITY');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/
-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
