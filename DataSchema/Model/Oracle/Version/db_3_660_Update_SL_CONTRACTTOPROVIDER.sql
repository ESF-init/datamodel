SPOOL db_3_660.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.660', 'HNI', 'Update SL_CONTRACTTOPROVIDER, increased size of ENCRYPTEDPASSWORD');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]
ALTER TABLE SL_CONTRACTTOPROVIDER
  MODIFY ENCRYPTEDPASSWORD VARCHAR2 (150 Byte);
--===========================================================================

---End adding schema changes 

--=======[ Commit ]==============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
