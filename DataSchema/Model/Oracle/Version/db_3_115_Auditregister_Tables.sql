SPOOL db_3_115.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.115', 'EPA', 'Introduced audit register tables.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================
CREATE TABLE SL_AUDITREGISTER
(
    AUDITREGISTERID            NUMBER(18,0) NOT NULL,
    SHIFTID                    NUMBER(18,0) DEFAULT NULL,
    DEBTORNO                NUMBER(18,0) DEFAULT NULL,
    DEVICENO                NUMBER(18,0) DEFAULT NULL,
    DEVICECLASS                NUMBER(18,0) DEFAULT NULL,
    VEHICLENO                NUMBER(18,0) DEFAULT NULL,
    REPORTDATETIME            DATE DEFAULT sysdate NOT NULL,
    LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
    LastModified DATE DEFAULT sysdate NOT NULL, 
    TransactionCounter INTEGER NOT NULL,
    CONSTRAINT PK_AUDITREGISTERID PRIMARY KEY (AUDITREGISTERID)
);

COMMENT ON TABLE SL_AUDITREGISTER is 'Table to gather statistical informations about reporting source. See SL_AUDITREGISTERVALUE for audit register values.';
COMMENT ON COLUMN SL_AUDITREGISTER.AUDITREGISTERID is 'Unique ID of entry.';
COMMENT ON COLUMN SL_AUDITREGISTER.SHIFTID is 'Shift id of reporting source.';
COMMENT ON COLUMN SL_AUDITREGISTER.DEBTORNO is 'Optional debtor number of reporting source.';
COMMENT ON COLUMN SL_AUDITREGISTER.DEVICENO is 'Optional device number of reporting source.';
COMMENT ON COLUMN SL_AUDITREGISTER.DEVICECLASS is 'Optional device class reporting source.';
COMMENT ON COLUMN SL_AUDITREGISTER.VEHICLENO is 'Optional vehicle number of reporting source.';
COMMENT ON COLUMN SL_AUDITREGISTER.REPORTDATETIME is 'Time and date of reported incident.';
COMMENT ON COLUMN SL_AUDITREGISTER.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_AUDITREGISTER.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_AUDITREGISTER.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

CREATE TABLE SL_AUDITREGISTERVALUE
(
    AUDITREGISTERVALUEID    NUMBER(18,0) NOT NULL,
    AUDITREGISTERID            NUMBER(18,0) NOT NULL,
    REGISTERVALUETYPE        NUMBER(9,0) NOT NULL,
    REGISTERVALUE             NUMBER(18,0) NOT NULL,
    SUBSET                    NUMBER(9,0) DEFAULT NULL,
    LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
    LastModified DATE DEFAULT sysdate NOT NULL, 
    TransactionCounter INTEGER NOT NULL,
    CONSTRAINT PK_AUDITREGISTERVALUEID PRIMARY KEY (AUDITREGISTERVALUEID),
    CONSTRAINT FK_AUDITREGISTERID FOREIGN KEY (AUDITREGISTERID) REFERENCES SL_AUDITREGISTER(AUDITREGISTERID)
);    

COMMENT ON TABLE SL_AUDITREGISTERVALUE is 'Statistics audit values. See SL_AUDITREGISTER for reporting source.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUE.AUDITREGISTERVALUEID is 'Unique ID of entry.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUE.AUDITREGISTERID is 'ID of reporting source.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUE.REGISTERVALUETYPE is 'Type of audit entry. See SL_AUDITREGISTERVALUETYPE enumeration for valid types.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUE.REGISTERVALUE is 'Value of audit entry.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUE.SUBSET is 'If audit entry type has more than one value, this subset id defines the different entry types. See SL_AUDITREGISTERSUBSETTYPE for valid subset types.';


CREATE TABLE SL_AUDITREGISTERVALUETYPE
(
    ENUMERATIONVALUE        NUMBER(9,0) NOT NULL,
    LITERAL                    NVARCHAR2(50) NOT NULL,
    DESCRIPTION                NVARCHAR2(250) NOT NULL,
    CONSTRAINT PK_AUDITREGISTERVALUETYPE PRIMARY KEY (ENUMERATIONVALUE)
);

COMMENT ON TABLE SL_AUDITREGISTERVALUETYPE is 'Enumeration of all available audit register types.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUETYPE.ENUMERATIONVALUE is 'Enumeration value of audit register value type.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUETYPE.LITERAL is 'Short name of enumeration value.';
COMMENT ON COLUMN SL_AUDITREGISTERVALUETYPE.DESCRIPTION is 'Detail name of enumeration value.';


CREATE TABLE SL_AUDITREGISTERSUBSETTYPE
(
    ENUMERATIONVALUE        NUMBER(9,0) NOT NULL,
    LITERAL                    NVARCHAR2(50) NOT NULL,
    DESCRIPTION                NVARCHAR2(250) NOT NULL,
    CONSTRAINT PK_AUDITREGISTERSUBSETTYPE PRIMARY KEY (ENUMERATIONVALUE)
);

COMMENT ON TABLE SL_AUDITREGISTERSUBSETTYPE is 'Some audit register entries may have more than one value. This subset enumeration identifies the different entries.';
COMMENT ON COLUMN SL_AUDITREGISTERSUBSETTYPE.ENUMERATIONVALUE is 'Enumeration value of audit register value type.';
COMMENT ON COLUMN SL_AUDITREGISTERSUBSETTYPE.LITERAL is 'Short name of enumeration value.';
COMMENT ON COLUMN SL_AUDITREGISTERSUBSETTYPE.DESCRIPTION is 'Detail name of enumeration value.';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (1, 'Validations', 'Total validation counts');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (2, 'RiderClassCounts', 'Rider class counts');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (3, 'ApprovedTransactions', 'Count of approved transactions');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (4, 'DeniedTransactions', 'Count of denied transactions');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (5, 'ReadFailures', 'Count of read failures');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (6, 'ValidatedFareProducts', 'Fare products validated');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (7, 'DeductedFareValues', 'Fare value deducted');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (8, 'NewFareMedia', 'Fare value deducted');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (9, 'FareProductSales', 'Fare product sold');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (10, 'StoredValueLoaded', 'Stored value loaded');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (11, 'AccountInquiries', 'Account inquiries');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (12, 'CashTransactions', 'Cash transactions by amount and denomination');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (13, 'CreditCardTransactions', 'Credit card transactions by amount');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (14, 'DebitCardTransactions', 'Debit card transactions by amount');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (15, 'Transactions', 'Count of approved and denied transactions');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (16, 'TotalCountsAndValues', 'The total count and value of all transactions completed since data was last uploaded to the back office');
INSERT INTO SL_AUDITREGISTERVALUETYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (17, 'SuccessDateTime', 'The date and time of the last successful data upload to the back office');


INSERT INTO SL_AUDITREGISTERSUBSETTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (1, 'Counts', 'Counts of given register value type');
INSERT INTO SL_AUDITREGISTERSUBSETTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (2, 'Sum', 'Value sum of given register value type');

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_AUDITREGISTER', 'SL_AUDITREGISTERVALUE');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_AUDITREGISTER', 'SL_AUDITREGISTERVALUE');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
