SPOOL db_3_017.log

INSERT INTO vario_dmodellversion (rundate,dmodellno,responsible,annotation)
VALUES (SYSDATE,'3.017','FLF','Extended nxname col size (500)');


---Start adding schema changes here

ALTER TABLE TM_SYSTEMFIELD
MODIFY(NXNAME VARCHAR2(500 BYTE));

---End adding schema changes



COMMIT;

PROMPT Done!

SPOOL OFF