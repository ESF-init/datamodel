SPOOL db_3_515.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.515', 'MFA', 'Adding columns to SL_ORGANIZATION to satisfy ORCA needs.');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_ORGANIZATION ADD TAXIDNUMBER NVARCHAR2(15);
COMMENT ON COLUMN SL_ORGANIZATION.TAXIDNUMBER IS 'Tax ID Number for the company';

ALTER TABLE SL_ORGANIZATION ADD ISPRETAXENABLED NUMBER(1,0) DEFAULT 0 NOT NULL ENABLE;
COMMENT ON COLUMN SL_ORGANIZATION.ISPRETAXENABLED IS 'Sets whether the organization is pre tax';

ALTER TABLE SL_ORGANIZATION ADD ISCARDOWNER NUMBER(1,0) DEFAULT 0 NOT NULL ENABLE;
COMMENT ON COLUMN SL_ORGANIZATION.ISCARDOWNER IS 'Sets whether the organization is a card owner';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
