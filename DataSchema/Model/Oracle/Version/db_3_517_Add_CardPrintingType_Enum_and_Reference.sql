INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.517', 'EMN', 'Card Printing Types.');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_CARDPRINTINGTYPE
(
  EnumerationValue  NUMBER(9)           NOT NULL,
  Literal           NVARCHAR2(50)       NOT NULL,
  Description       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_CARDPRINTINGTYPE PRIMARY KEY (EnumerationValue)
);

COMMENT ON TABLE SL_CARDPRINTINGTYPE IS 'Enumeration values for SL_OrderDetail.CardPrintingTypeId.';
COMMENT ON COLUMN SL_CARDPRINTINGTYPE.EnumerationValue IS 'Type identifier.';
COMMENT ON COLUMN SL_CARDPRINTINGTYPE.Literal IS 'Type literal.';
COMMENT ON COLUMN SL_CARDPRINTINGTYPE.Description IS 'Type description.';

INSERT	INTO SL_CARDPRINTINGTYPE (EnumerationValue, Literal, Description) VALUES (0, 'Unknown', 'Unknown');
INSERT	INTO SL_CARDPRINTINGTYPE (EnumerationValue, Literal, Description) VALUES (1, 'NoPrinting', 'No Printing');
INSERT	INTO SL_CARDPRINTINGTYPE (EnumerationValue, Literal, Description) VALUES (2, 'Individual', 'Individual');
INSERT	INTO SL_CARDPRINTINGTYPE (EnumerationValue, Literal, Description) VALUES (3, 'Organizational', 'Organizational');
INSERT	INTO SL_CARDPRINTINGTYPE (EnumerationValue, Literal, Description) VALUES (4, 'NonPersonalized', 'Non Personalized');
INSERT	INTO SL_CARDPRINTINGTYPE (EnumerationValue, Literal, Description) VALUES (5, 'Personalized', 'Personalized');
INSERT	INTO SL_CARDPRINTINGTYPE (EnumerationValue, Literal, Description) VALUES (6, 'Photo', 'Photo');

ALTER TABLE SL_ORDERDETAIL ADD CARDPRINTINGTYPEID Number(9) DEFAULT 0 NOT NULL;
COMMENT ON COLUMN SL_ORDERDETAIL.CARDPRINTINGTYPEID IS 'Type of printing for this order detail';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;