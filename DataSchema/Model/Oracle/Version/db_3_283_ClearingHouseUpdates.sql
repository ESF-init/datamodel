SPOOL db_3_283.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.283', 'FRD', 'Updates to Daily Clearing related tables, post RITS meeting');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE CH_CLEARINGRESULT ADD VALIDATIONCOUNT NUMBER(18,0) NOT NULL;
ALTER TABLE CH_CLEARINGRESULT ADD VALIDATIONAMOUNT NUMBER(18,0) NOT NULL;
ALTER TABLE CH_CLEARINGRESULT ADD QUARANTINEDCOUNT NUMBER(18,0) NOT NULL;
ALTER TABLE CH_CLEARINGRESULT ADD QUARANTINEDAMOUNT NUMBER(18,0) NOT NULL;

COMMENT ON COLUMN CH_CLEARINGRESULT.VALIDATIONCOUNT IS 'Count of transactions that have failed a non critical validation rule.';
COMMENT ON COLUMN CH_CLEARINGRESULT.VALIDATIONAMOUNT IS 'Amount of value of transactions that have failed a non critical validation rule.';
COMMENT ON COLUMN CH_CLEARINGRESULT.QUARANTINEDCOUNT IS 'Count of transactions that have passed validation but are part of aborted or backup shift.';
COMMENT ON COLUMN CH_CLEARINGRESULT.QUARANTINEDAMOUNT IS 'Amount of value of transactions that have passed validation but are part of aborted or backup shift.';

INSERT INTO CH_CLEARINGCLASSIFICATION (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (40, 'Quarantined', 'Quarantine State'); 
UPDATE CH_CLEARINGCLASSIFICATION SET LITERAL = 'Bad' WHERE ENUMERATIONVALUE = 100;	


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
