SPOOL db_3_048.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.048', 'BAW', 'Added new column to recognized tap view to indicate cancelled taps.');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================
CREATE OR REPLACE VIEW ACC_RECOGNIZEDTAP
(AMOUNT, BASEFARE, CREDITACCOUNTNUMBER, CUSTOMERGROUP, DEBITACCOUNTNUMBER, 
 LINEGROUPID, POSTINGDATE, POSTINGREFERENCE, PREMIUM, REVENUERECOGNITIONID, 
 REVENUESETTLEMENTID, TICKETNUMBER, OPERATORID, CLOSEOUTPERIODID, PERIODFROM, 
 PERIODTO, CLOSEOUTTYPE, STATE, CANCELLATIONREFERENCE, CANCELLATIONREFERENCEGUID, 
 CLIENTID, DEVICETIME, FAREAMOUNT, LINE, TRANSACTIONOPERATORID, 
 PRODUCTID, PURSEBALANCE, PURSECREDIT, RESULTTYPE, SALESCHANNELID, 
 TARIFFDATE, TARIFFVERSION, TICKETID, TICKETINTERNALNUMBER, TRANSACTIONJOURNALID, 
 TRANSACTIONTYPE, TRIPTICKETINTERNALNUMBER, CANCELLEDTAP)
AS 
SELECT 
  RR.AMOUNT,
  RR.BASEFARE,
  RR.CREDITACCOUNTNUMBER,
  RR.CUSTOMERGROUP,
  RR.DEBITACCOUNTNUMBER,
  RR.LINEGROUPID,
  RR.POSTINGDATE,
  RR.POSTINGREFERENCE,
  RR.PREMIUM,
  RR.REVENUERECOGNITIONID,
  RR.REVENUESETTLEMENTID,
  RR.TICKETNUMBER,
  RR.OPERATORID,
  CP.CLOSEOUTPERIODID,
  CP.PERIODFROM,
  CP.PERIODTO,
  CP.CLOSEOUTTYPE,
  CP.STATE,
  TJ1.CANCELLATIONREFERENCE,
  TJ1.CANCELLATIONREFERENCEGUID,
  TJ1.CLIENTID,
  TJ1.DEVICETIME,
  TJ1.FAREAMOUNT,
  TJ1.LINE,
  TJ1.OPERATORID AS TRANSACTIONOPERATORID,
  TJ1.PRODUCTID,
  TJ1.PURSEBALANCE,
  TJ1.PURSECREDIT,
  TJ1.RESULTTYPE,
  TJ1.SALESCHANNELID,
  TJ1.TARIFFDATE,
  TJ1.TARIFFVERSION,
  TJ1.TICKETID,
  TJ1.TICKETINTERNALNUMBER,
  TJ1.TRANSACTIONJOURNALID,
  TJ1.TRANSACTIONTYPE,
  TJ1.TRIPTICKETINTERNALNUMBER,
  CAST (DECODE (TJ1.CANCELLATIONREFERENCEGUID, NULL, 0, 1) AS NUMBER(9)) AS CANCELLEDTAP
FROM ((SL_TRANSACTIONJOURNAL TJ1 INNER JOIN ACC_REVENUERECOGNITION RR ON TJ1.TRANSACTIONJOURNALID = RR.TRANSACTIONJOURNALID)
  INNER JOIN ACC_CLOSEOUTPERIOD CP ON CP.CLOSEOUTPERIODID = RR.CLOSEOUTPERIODID)
WHERE TJ1.ValidTo IS NULL AND (TJ1.TRANSACTIONTYPE = 66 OR TJ1.TRANSACTIONTYPE = 84);

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
