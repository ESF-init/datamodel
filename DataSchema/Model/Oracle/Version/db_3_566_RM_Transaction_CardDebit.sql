SPOOL db_3_566.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.566', 'ULB', 'Add CardDebit to Rm_Transaction');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE RM_TRANSACTION
ADD (CARDDEBIT NUMBER(10));

COMMENT ON COLUMN 
RM_TRANSACTION.CARDDEBIT IS 
'Contains all purse value changes ';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
