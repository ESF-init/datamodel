SPOOL db_3_434.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.434', 'FLF', 'Add column ERRORMESSAGE to RM_BINARYDATA');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE RM_BINARYDATA
 ADD (ERRORMESSAGE  NCLOB);


COMMENT ON COLUMN RM_BINARYDATA.ERRORMESSAGE IS 'Message which is created by further data processing.';


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
