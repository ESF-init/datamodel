SPOOL db_3_648.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.648', 'MKG', 'Add ClientId column to ACC_ReconciledPayment from SL_SALE');


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

  CREATE OR REPLACE FORCE VIEW "ACC_RECONCILEDPAYMENT" ("SALESCHANNELID", "MERCHANTNUMBER", "SALETYPE", "PAYMENTTYPE", "PAYMENTRECONCILIATIONID", "PAYMENTJOURNALID", "POSTINGDATE", "POSTINGREFERENCE", "CREDITACCOUNTNUMBER", "DEBITACCOUNTNUMBER", "AMOUNT", "LASTUSER", "LASTMODIFIED", "TRANSACTIONCOUNTER", "CLOSEOUTPERIODID", "RECONCILED", "CLIENTID") AS 
  SELECT sl_sale.saleschannelid,
    sl_sale.merchantnumber,
    sl_sale.saletype,
    sl_paymentjournal.paymenttype,
    acc_paymentreconciliation.paymentreconciliationid,
    acc_paymentreconciliation.paymentjournalid,
    acc_paymentreconciliation.postingdate,
    acc_paymentreconciliation.postingreference,
    acc_paymentreconciliation.creditaccountnumber,
    acc_paymentreconciliation.debitaccountnumber,
    acc_paymentreconciliation.amount,
    acc_paymentreconciliation.lastuser,
    acc_paymentreconciliation.lastmodified,
    acc_paymentreconciliation.transactioncounter,
    acc_paymentreconciliation.closeoutperiodid,
    acc_paymentreconciliation.reconciled,	
	sl_sale.clientid
  FROM sl_sale,
    sl_paymentjournal,
    acc_paymentreconciliation
  WHERE sl_sale.saleid                   = sl_paymentjournal.saleid
  AND sl_paymentjournal.paymentjournalid = acc_paymentreconciliation.paymentjournalid
  ORDER BY sl_sale.saleschannelid,
    sl_sale.merchantnumber,
    sl_paymentjournal.paymenttype;

   COMMENT ON COLUMN "ACC_RECONCILEDPAYMENT"."LASTUSER" IS 'Technical field: last (system) user that changed this dataset.';
   COMMENT ON COLUMN "ACC_RECONCILEDPAYMENT"."LASTMODIFIED" IS 'Technical field: date time of the last change to this dataset.';
   COMMENT ON COLUMN "ACC_RECONCILEDPAYMENT"."TRANSACTIONCOUNTER" IS 'Technical field: counter to prevent concurrent changes to this dataset.';

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
