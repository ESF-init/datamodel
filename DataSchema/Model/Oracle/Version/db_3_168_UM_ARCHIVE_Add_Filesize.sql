SPOOL db_3_168.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.168', 'jsb', 'Add Column Filesize to Table UM_Archive');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE UM_ARCHIVE
ADD (FILESIZE varchar(50) DEFAULT '0');

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
