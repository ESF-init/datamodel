SPOOL db_3_601.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.601', 'DST', 'Add ExternalType and PurchaseOrderNumber to SL_Organization');

ALTER TABLE SL_ORGANIZATION ADD ExternalType NVARCHAR2(255);
COMMENT ON COLUMN SL_ORGANIZATION.ExternalType IS 'Organization type for third parties to define their own types (MOBILEvarioAPI).';

ALTER TABLE SL_ORGANIZATION ADD PurchaseOrderNumber NVARCHAR2(255);
COMMENT ON COLUMN SL_ORGANIZATION.PurchaseOrderNumber IS 'Current purchase order for this organization (MOBILEvarioAPI).';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
