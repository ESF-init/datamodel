SPOOL db_3_499.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.499', 'MFA', 'Added REPLACEMENTCARDID column to SL_ORDERDETAIL');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_ORDERDETAIL ADD REPLACEMENTCARDID NUMBER(18) CONSTRAINT REPLACEMENTCARDID_FK REFERENCES SL_CARD(CARDID);
COMMENT ON COLUMN SL_ORDERDETAIL.REPLACEMENTCARDID IS 'Id of card to be replaced.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
