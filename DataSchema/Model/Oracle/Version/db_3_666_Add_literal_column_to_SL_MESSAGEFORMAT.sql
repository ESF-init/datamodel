SPOOL db_3_666.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.666', 'FRD', 'Add literal column to SL_MESSAGEFORMAT');

-- =======[ Changes to Existing Tables ]===========================================================================

--Reconciliation with version part v3.599 in Create_Vario_Schema.sql

ALTER TABLE SL_MESSAGEFORMAT ADD (LITERAL NVARCHAR2(50));
UPDATE SL_MESSAGEFORMAT SET LITERAL = 'HTML' WHERE ENUMERATIONVALUE = 1;
UPDATE SL_MESSAGEFORMAT SET LITERAL = 'Text' WHERE ENUMERATIONVALUE = 2;
ALTER TABLE SL_MESSAGEFORMAT MODIFY (LITERAL NOT NULL);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
