SPOOL db_3_328.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.328', 'ARH', 'Add Column OtherProviderReference to SL_CONTRACTTOPROVIDER');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CONTRACTTOPROVIDER ADD OTHERPROVIDERREFERENCE NVARCHAR2(50) ;

COMMENT ON COLUMN SL_CONTRACTTOPROVIDER.OTHERPROVIDERREFERENCE IS 'Other/Second provider reference that has the customer to the mobility provider.';

COMMIT;

PROMPT Done!

SPOOL OFF;
