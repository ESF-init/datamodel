------------------------------------------------------------------------------
--Version 3.401
------------------------------------------------------------------------------
SPOOL db_3_401.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.401', 'FRD', 'ClearingHouse updates for Aborted Shift deleting transactions and re-import');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE CH_TRANSACTIONADDITIONAL DROP CONSTRAINT FK_TRANSADD_TRANSACTIONID;
ALTER TABLE CH_CLEARINGTRANSACTION DROP CONSTRAINT FK_CLEARINGTRANS_TRANSACTIONID;
ALTER TABLE CH_APPORTIONMENTRESULT DROP CONSTRAINT PK_APPORTIONMENT_FROMTRANSID;
ALTER TABLE CH_APPORTIONMENTRESULT DROP CONSTRAINT PK_APPORTIONMENT_TOTRANSID;

ALTER TABLE CH_TRANSACTIONADDITIONAL ADD SHIFTID NUMBER(10) default NULL; 
ALTER TABLE CH_TRANSACTIONADDITIONAL ADD ORIGINALSHIFTSTATE NUMBER(10) default NULL; 
COMMENT ON COLUMN CH_TRANSACTIONADDITIONAL.SHIFTID IS 'ShiftID of the Aborted Shift references RM_TRANSACTION.SHIFTID.';
COMMENT ON COLUMN CH_TRANSACTIONADDITIONAL.ORIGINALSHIFTSTATE IS 'Original Shift State of aborted or backup shift that has changed to original from backup, references RM_SHIFTSTATE.SHIFTSTATEID.';

-- =======[ New Tables ]===========================================================================================

CREATE TABLE CH_ABORTEDTRANSACTION
(
  TRANSACTIONID          NUMBER(10)             NOT NULL,
  CLIENTID               NUMBER(5)              NOT NULL,
  CARDID                 NUMBER(10)             NOT NULL,
  DEVICENO               NUMBER(8)              NOT NULL,
  SALESCHANNEL           NUMBER(3)              NOT NULL,
  TRIPDATETIME           DATE                   NOT NULL,
  LINE                   NUMBER(10),
  TRIPSERIALNO           NUMBER(8),
  IMPORTDATETIME         DATE,
  SHIFTID                NUMBER(10),
  TARIFFID               NUMBER(10),
  TYPEID                 NUMBER(10),
  PRICE                  NUMBER(10),
  VALUEOFRIDE            NUMBER(10),
  CANCELLATIONID         NUMBER(10),
  DEVICEPAYMENTMETHOD    NUMBER(3),
  LINENAME               VARCHAR2(10 BYTE),
  CARDTRANSACTIONNO      NUMBER(10),
  CARDREFTRANSACTIONNO   NUMBER(10),
  CARDBALANCE            NUMBER(10),
  DEVICEBOOKINGSTATE     NUMBER(4),
  PAYCARDNO              NVARCHAR2(50),
  ROUTENO                NUMBER(10),
  TIKNO                  NUMBER(10),
  STOPFROM               NUMBER(10),
  STOPTO                 NUMBER(10),
  TICKETINSTANCEID       NUMBER(10),
  CARDISSUERID           NUMBER(18)
);

COMMENT ON TABLE CH_ABORTEDTRANSACTION IS 'List of transactions including sales, cancellations, refunds,...';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.TRANSACTIONID IS 'Unique key for the transaction';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.CLIENTID IS 'References VARIO_CLIENT.CLIENTID';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.CARDID IS 'References SL_CARD.CARDID';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.DEVICENO IS 'Device number which created this transaction';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.SALESCHANNEL IS 'References VARIO_DEVICECLASS.DEVICECLASSID';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.TRIPDATETIME IS 'Toestamp when trip occured';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.LINE IS 'Line Number';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.TRIPSERIALNO IS 'Receipt counter for paper tickets. It is unique per device.';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.IMPORTDATETIME IS 'Timestamp when record was imported to the database';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.SHIFTID IS 'References RM_SHIFT.SHIFTID';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.TARIFFID IS 'References TM_TARIF.TARIFID';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.TYPEID IS 'References RM_TRANSACTIONTYPE.TYPEID';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.PRICE IS 'Price in cent';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.VALUEOFRIDE IS 'Worth of this transaction(e.g. price+subsidy)';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.CANCELLATIONID IS 'If transaction is cancelled or transaction is a cancellation references RM_TRANSACTION.TRIPSERIALNO';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.DEVICEPAYMENTMETHOD IS 'References RM_DEVICEPAYMENTMETHOD.DEVICEPAYMENTMETHODID';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.LINENAME IS 'Line name';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.CARDTRANSACTIONNO IS 'Current card TAN';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.CARDREFTRANSACTIONNO IS 'Referenced card TAN for transfer tickets ';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.CARDBALANCE IS 'Current puse balance on card.';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.DEVICEBOOKINGSTATE IS 'References RM_DEVICEBOOKINGSTATE.DEVICEBOOKINGSTATENO';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.PAYCARDNO IS 'Card number which was used to pay the transaction';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.ROUTENO IS 'Route number';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.TIKNO IS 'References TM_TICKET.INTERNALNUMBER';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.STOPFROM IS 'Source Stop number ';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.STOPTO IS 'Destination stop number';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.TICKETINSTANCEID IS 'InstanceId of the e-ticket used for this trip';
COMMENT ON COLUMN CH_ABORTEDTRANSACTION.CARDISSUERID IS 'Represents the client id of the issuer of the smartcard that was used in the transaction. Optional reference to VARIO_CLIENT.CLIENTID.';

CREATE INDEX IDX_RM_TICKET_AB ON CH_ABORTEDTRANSACTION (TARIFFID, TIKNO);
CREATE INDEX IDX_RM_TRIPDATETIME_AB ON CH_ABORTEDTRANSACTION (TRIPDATETIME);
CREATE INDEX IDX_TRANSACTION_SHIFTID_AB ON CH_ABORTEDTRANSACTION (SHIFTID);
CREATE INDEX XIF13RM_ABORTEDTRANSACTION_AB ON CH_ABORTEDTRANSACTION (CARDID);
CREATE UNIQUE INDEX XPKRM_ABORTEDTRANSACTION_AB ON CH_ABORTEDTRANSACTION (TRANSACTIONID);

ALTER TABLE CH_ABORTEDTRANSACTION ADD (
  PRIMARY KEY
  (TRANSACTIONID)
  USING INDEX XPKRM_ABORTEDTRANSACTION_AB
  ENABLE VALIDATE);

ALTER TABLE CH_ABORTEDTRANSACTION ADD (
  FOREIGN KEY (TYPEID) 
  REFERENCES RM_TRANSACTIONTYPE (TYPEID)
  ENABLE VALIDATE);
  
-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
