SPOOL db_3_643.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.643', 'EMN', 'Add SL_Printer.Capacity');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_PRINTER
ADD (Capacity NUMBER(9));

COMMENT ON COLUMN 
SL_PRINTER.Capacity IS 
'Capacity of printer card hopper';

COMMIT;

PROMPT Done!

SPOOL OFF;

