SPOOL db_3_678.log;

------------------------------------------------------------------------------
--Version 3.678
------------------------------------------------------------------------------
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.678', 'ARH', 'Add SL_BOOKINGITEMPAYMENT');

CREATE TABLE SL_BOOKINGITEMPAYMENT
(
  BOOKINGITEMPAYMENTID       	NUMBER(18)                 		NOT NULL,
  BOOKINGITEMID        	     	NUMBER(18)                 		NOT NULL,
  AMOUNT  				     	NUMBER(18)                 		NOT NULL,
  VATAMOUNT          		 	NUMBER(18)                 		NOT NULL,
  VATRATE 						NUMBER(18)                   	NOT NULL,
  EXECUTIONTIME         		DATE					     	NOT NULL,
  BOOKINGITEMPAYMENTTYPE        NUMBER(9)			         	NOT NULL,
  PAYMENTSTATE          		NUMBER(9)                    	NOT NULL,
  DESCRIPTION           		NVARCHAR2(100),
  PAYMENTREFERENCE      		NVARCHAR2(40),
  PAYMENTMETHOD 				NUMBER(9)						NOT NULL,
  LASTUSER						NVARCHAR2(50)					DEFAULT 'SYS'		NOT NULL,
  LASTMODIFIED					DATE							DEFAULT sysdate		NOT NULL,
  TRANSACTIONCOUNTER 			INTEGER							NOT NULL,
  DATAROWCREATIONDATE 			DATE							DEFAULT sysdate
);

ALTER TABLE SL_BOOKINGITEMPAYMENT ADD CONSTRAINT PK_BOOKINGITEMPAYMENT PRIMARY KEY (BOOKINGITEMPAYMENTID);
ALTER TABLE SL_BOOKINGITEMPAYMENT ADD CONSTRAINT FK_BOOKINGITEM_BOOKINGITEMID FOREIGN KEY (BOOKINGITEMID) REFERENCES SL_BOOKINGITEM (BOOKINGITEMID);


COMMENT ON TABLE SL_BOOKINGITEMPAYMENT IS 'Represents payment related to one booking element of a booking (SL_BOOKINGITEMPAYMENT).';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.BOOKINGITEMPAYMENTID IS 'Unique ID of BOOKINGITEMPAYMENT';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.BOOKINGITEMID IS 'References SL_BOOKINGITEM.BOOKINGITEMID';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.AMOUNT IS 'Transaction amount of the payment';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.VATAMOUNT IS 'VAT amount of the payment';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.VATRATE IS 'VAT rate in percent of the payment';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.EXECUTIONTIME IS 'Timestamp when the payment was executed.';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.BOOKINGITEMPAYMENTTYPE IS 'Info of the payment type : unknown, original purchase, optional fee, additional fee, refund. See SL_BOOKINGITEMPAYMENTTYPE.';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.PAYMENTSTATE IS 'State of the payment transaction.';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.DESCRIPTION IS 'Extended description of the payment.';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.PAYMENTREFERENCE IS 'Payment reference from the payment provider.';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.PAYMENTMETHOD IS 'See RM_DEVICEPAYMENTMETHOD';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENT.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';


CREATE TABLE SL_BOOKINGITEMPAYMENTTYPE
(
    ENUMERATIONVALUE NUMBER(9)     not null
        constraint PK_BOOKINGITEMPAYMENTTYPE
            primary key,
    LITERAL          NVARCHAR2(50) not null,
    DESCRIPTION      NVARCHAR2(50) not null
);
COMMENT ON TABLE SL_BOOKINGITEMPAYMENTTYPE is 'Enumeration of a booking item payment type, known to the system.';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENTTYPE.ENUMERATIONVALUE is 'Unique ID of the payment type';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENTTYPE.LITERAL is 'Name of the payment type; used internally.';
COMMENT ON COLUMN SL_BOOKINGITEMPAYMENTTYPE.DESCRIPTION is 'Descripton of the payment type. This is used in the UI to display the value.';

---- =======[ Functions and Data Types ]=============================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_BOOKINGITEMPAYMENTTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (0, 'Unknown', 'Unknown');
INSERT INTO SL_BOOKINGITEMPAYMENTTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (1, 'OriginalPurchase', 'Original purchase');
INSERT INTO SL_BOOKINGITEMPAYMENTTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (2, 'OptionalFee', 'Optional fee');
INSERT INTO SL_BOOKINGITEMPAYMENTTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (3, 'AdditionalFee', 'Additional fee');
INSERT INTO SL_BOOKINGITEMPAYMENTTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (4, 'Refund', 'Refund');

-- =======[ Sequences ]============================================================================================
create sequence SL_BOOKINGITEMPAYMENT_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================
CREATE OR REPLACE TRIGGER SL_BOOKINGITEMPAYMENT_BRI before insert on SL_BOOKINGITEMPAYMENT for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_BOOKINGITEMPAYMENT_BRU before update on SL_BOOKINGITEMPAYMENT for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/


COMMIT;

PROMPT Done!

SPOOL OFF;
