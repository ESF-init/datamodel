SPOOL db_3_297.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.297', 'BVI', 'New contract details view for sum.');

Create view VIEW_SL_ContractDetails as (
    select
        ContractID,
        OrganizationID,
        State,
        ContractNumber,
        ValidFrom,
        ValidTo,
        ClientID,
        PaymentModalityID,
        LastUser,
        LastModified,
        TransactionCounter,
        ClientAuthenticationToken,
        AccountingModeID,
        ContractType,
        AdditionalText,
        PersonFirstName,
        PersonLastName,
        PersonDateOfBirth,
        PersonGender,
        PersonSalutation,
        PersonTitle,
        ADDRESSID,
        AddressStreet,
        AddressPostalCode,
        ADDRESSCity,
        ADDRESSCountry,
        ADDRESSRegion,
        ADDRESSAddressee,
        ADDRESSAddressField1,
        ADDRESSAddressField2,
        ADDRESSSTREETNUMBER,
        OrganizationNumber,
        OrganizationName,
        BankConnectionDataIBAN,
        BankConnectionDataBIC,
        BankConnectionDataLastUsage,
        LastInvoiceNumber,
        LastInvoiceCreationDate,
        CommentText,
        PaymentModalityPaymentMethodID,
        PaymentModalityPaymentMethod
    from
    (
    with
        validContractAddress as
        (
            select * from(
               SELECT First_Value(cAddress_sub.AddressID) OVER (PARTITION BY cAddress_sub.ContractID Order by a_sub.ValidFrom desc) newestID, a_sub.*, cAddress_sub.ContractID
               FROM sl_address  a_sub
                    JOIN sl_contractaddress cAddress_sub ON a_sub.AddressID = cAddress_sub.AddressID
               WHERE     (a_sub.ValidFrom IS NULL OR a_sub.ValidFrom <= SYSDATE)
                    AND (a_sub.ValidUntil IS NULL OR a_sub.ValidUntil >= SYSDATE)
                    AND (cAddress_sub.AddressType = 5)
            ) where newestID = AddressID --Ensure there is maximum one address for each contract
        ),
        newestBankConnectionData as
        (
            select * from (
                select First_Value(bcd.BankConnectionDataID) OVER (PARTITION By po.ContractID Order by bcd.CreationDate desc) newestID, po.contractID contractID, bcd.*
                    from sl_paymentoption po 
                        join sl_bankconnectiondata bcd on po.BankConnectionDataID = bcd.BankConnectionDataID
                    where (bcd.ValidFrom is null or bcd.ValidFrom <= sysdate)
                        and (bcd.ValidUntil is null or bcd.ValidUntil >= sysdate)
                        and po.PAYMENTMETHOD = 269
                    )
                    where BankConnectionDataID = newestID --Ensure there is maximum one BankConnectionData for each contract
        ) ,
        newestInvoices as
        (
            select * from
                (select First_Value(i_sub.INVOICEID) OVER (PARTITION BY ContractID ORDER BY CreationDate desc) newestID,i_sub.*
                    from sl_invoice i_sub
                ) 
            where newestID = InvoiceID --Ensure there is maximum one Invoice for each contract
        ) ,
        customerAccount as 
        (
            select * from (
                select First_Value(customerAccount.customerAccountID) OVER (PARTITION By customerAccount.ContractID Order by customerAccount.CustomerAccountID desc) oldestID, customerAccount.*
                    from sl_customeraccount customerAccount)
                    where customerAccountID = oldestID  --Ensure there is maximum one CustomerAccount for each contract
        ) ,
        newestComment as 
        (
            select * from 
                (select First_Value(comment_sub.CommentID) OVER (PARTITION BY ContractID ORDER BY CreationDate desc) newestID, comment_sub.* 
                    from sl_comment comment_sub
                ) 
            where newestID = CommentID --Ensure there is maximum one comment for each contract
        ) ,
        newestDunningData as 
        (
            select * from
                    (select First_Value(i_sub.InvoiceID) OVER (PARTITION BY dp_sub.ContractID ORDER BY i_sub.CreationDate desc) newestID, i_sub.InvoiceID InvoiceID, dp_sub.ContractID, i_sub.CreationDate, i_sub.InvoiceNumber, dp_sub.PROCESSNUMBER, dlevel_sub.NAME
                        from sl_dunningprocess dp_sub
                        join sl_dunningtoinvoice dti_sub on dp_sub.DUNNINGPROCESSID = dti_sub.DUNNINGPROCESSID
                        join sl_invoice i_sub on i_sub.invoiceid = dti_sub.invoiceid
                        join sl_dunninglevel dlevel_sub on dti_sub.DUNNINGLEVELID = dlevel_sub.dunninglevelid
                    )
                where InvoiceID = newestID --Ensure there is maximum one dunning data for each contract
        )
    select
        contract.ContractID ContractID,
        contract.OrganizationID OrganizationID,
        contract.State State,
        contract.ContractNumber ContractNumber,
        contract.ValidFrom ValidFrom,
        contract.ValidTo ValidTo,
        contract.ClientID ClientID,
        contract.PaymentModalityID PaymentModalityID,
        contract.LastUser LastUser,
        contract.LastModified LastModified,
        contract.TransactionCounter TransactionCounter,
        contract.ClientAuthenticationToken ClientAuthenticationToken,
        contract.AccountingModeID AccountingModeID,
        contract.ContractType ContractType,
        contract.AdditionalText AdditionalText,
        (CASE WHEN contract.OrganizationID > 0 THEN p_OrganizationContactPerson.FirstName ELSE p_Contractor.FirstName END) AS PersonFirstName,
        (CASE WHEN contract.OrganizationID > 0 THEN p_OrganizationContactPerson.LastName ELSE p_Contractor.LastName END) AS PersonLastName,
        (CASE WHEN contract.OrganizationID > 0 THEN p_OrganizationContactPerson.DateOfBirth ELSE p_Contractor.DateOfBirth END) AS PersonDateOfBirth,
        (CASE WHEN contract.OrganizationID > 0 THEN p_OrganizationContactPerson.Gender ELSE p_Contractor.Gender END) AS PersonGender,
        (CASE WHEN contract.OrganizationID > 0 THEN p_OrganizationContactPerson.Salutation ELSE p_Contractor.Salutation END) AS PersonSalutation,
        (CASE WHEN contract.OrganizationID > 0 THEN p_OrganizationContactPerson.Title ELSE p_Contractor.Title END) AS PersonTitle,
        validContractAddress.ADDRESSID ADDRESSID,
        validContractAddress.Street AddressStreet,
        validContractAddress.POSTALCODE AddressPostalCode,
        validContractAddress.CITY ADDRESSCity,
        validContractAddress.Country ADDRESSCountry,
        validContractAddress.Region ADDRESSRegion,
        validContractAddress.ADDRESSEE ADDRESSAddressee,
        validContractAddress.ADDRESSFIELD1 ADDRESSAddressField1,
        validContractAddress.ADDRESSFIELD2 ADDRESSAddressField2,
        validContractAddress.STREETNUMBER ADDRESSSTREETNUMBER,
        organization.OrganizationNumber OrganizationNumber,
        organization.Name OrganizationName,
        newestBankConnectionData.IBAN BankConnectionDataIBAN,
        newestBankConnectionData.BIC BankConnectionDataBIC,
        newestBankConnectionData.LASTUSAGE BankConnectionDataLastUsage,
        newestInvoices.INVOICENUMBER LastInvoiceNumber,
        newestInvoices.CREATIONDATE LastInvoiceCreationDate,
        newestComment.TEXT CommentText,
        paymentModality.DEVICEPAYMENTMETHOD PaymentModalityPaymentMethodID,
        devicePaymentMethod.Description PaymentModalityPaymentMethod
    from sl_contract contract
        left join customerAccount on customerAccount.ContractID = contract.ContractID
        left join sl_person p_Contractor on p_Contractor.PersonID = customerAccount.PersonID
        left join sl_Organization organization on organization.OrganizationID = contract.OrganizationID
        left join sl_person p_OrganizationContactPerson on p_OrganizationContactPerson.PersonID = organization.CONTACTPERSONID
        left join validContractAddress on validContractAddress.ContractID = contract.ContractID
        left join newestBankConnectionData on newestBankConnectionData.ContractID = contract.ContractID
        left join newestInvoices on newestInvoices.contractid = contract.contractID
        left join newestComment on newestComment.ContractID = contract.CONTRACTID
        left join sl_paymentmodality paymentModality on contract.paymentmodalityid = paymentModality.paymentmodalityid
        left join rm_devicepaymentmethod devicePaymentMethod on paymentModality.DEVICEPAYMENTMETHOD = devicePaymentMethod.DEVICEPAYMENTMETHODID
        left join newestDunningData on newestDunningData.ContractID = contract.ContractID)
);

COMMIT;

PROMPT Done!

SPOOL OFF;
