SPOOL db_3_188.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.188', 'TIR', 'CREATE table UM_TYPEOFSAM');

-- Start adding schema changes here

CREATE TABLE UM_TYPEOFSAM
(
  TYPEID    NUMBER(9)                               NOT NULL,
  TYPENAME  VARCHAR2(100)                           NOT NULL,
  CONSTRAINT UM_TYPEOFSAM_PK PRIMARY KEY (TYPEID)
);
  
COMMENT ON TABLE UM_TYPEOFSAM is 'Table contains mapping information between sam type ids and sam type names.';
COMMENT ON COLUMN UM_TYPEOFSAM.TYPEID IS 'Sam type id';
COMMENT ON COLUMN UM_TYPEOFSAM.TYPENAME IS 'Sam type name';

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;