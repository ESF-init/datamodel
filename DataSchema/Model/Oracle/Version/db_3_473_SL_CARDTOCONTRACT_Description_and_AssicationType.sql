SPOOL db_3_473.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.473', 'MKD', 'Add column Description and AssociationType to SL_CARDTOCONTRACT');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_CARDTOCONTRACT
 ADD (Description VARCHAR2(40));

COMMENT ON COLUMN SL_CARDTOCONTRACT.Description IS 'The description or nickname of the transit account in the context of the associated contract';

ALTER TABLE SL_CARDTOCONTRACT
 ADD (AssociationType NUMBER(9));

COMMENT ON COLUMN SL_CARDTOCONTRACT.AssociationType IS 'The type of this association. It maps to an enum in the code basis. Possible values are eg. 1=Primary, 2=LoadOnly...';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
