SPOOL db_3_112.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.112', 'DST', 'Added SmartTapCounter and SmartTapID to SL_TransactionJournal for Google Smarttap');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_transactionjournal add SmartTapID NVARCHAR2(50);
alter table sl_transactionjournal add SmartTapCounter NVARCHAR2(25);
COMMENT ON COLUMN sl_transactionjournal.SmartTapID IS 'Google smart tap id.';
COMMENT ON COLUMN sl_transactionjournal.SmartTapCounter IS 'Google smart tap counter.';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
