SPOOL db_3_362.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.362', 'EPA', 'Card dormancy.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================
CREATE TABLE SL_DORMANCYNOTIFICATION
(
    DORMANCYNOTIFICATIONID  NUMBER(18,0) NOT NULL,
    CARDID                  NUMBER(18,0) NOT NULL,
    NOTIFICATIONDATE        DATE NOT NULL,
	LastUser                NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified            DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter      INTEGER NOT NULL,
	CONSTRAINT PK_DORMANYNOTIFICATION PRIMARY KEY (DORMANCYNOTIFICATIONID),
	CONSTRAINT FK_DORMANCYNOTIFICATION FOREIGN KEY (CARDID) REFERENCES SL_CARD(CARDID)
);


COMMENT ON TABLE SL_DORMANCYNOTIFICATION is 'Tracks when a card (owner) got a dormancy notification';
COMMENT ON COLUMN SL_DORMANCYNOTIFICATION.DORMANCYNOTIFICATIONID IS 'Primary key.';
COMMENT ON COLUMN SL_DORMANCYNOTIFICATION.CARDID IS 'ID of the card.';
COMMENT ON COLUMN SL_DORMANCYNOTIFICATION.NOTIFICATIONDATE IS 'Date when the card has been notified. Can be null.';

COMMENT ON COLUMN SL_DORMANCYNOTIFICATION.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_DORMANCYNOTIFICATION.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_DORMANCYNOTIFICATION.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW SL_CARDDORMANCY
(
    NOTIFICATIONDATE,
    LASTTRANSACTION,
    CARDID,
    PRINTEDNUMBER,
    STATE,
    EXPIRATION,
    CARDTYPE,
    FAREMEDIAID
 ) AS
SELECT 
    dm.notificationdate as NOTIFICATIONDATE, 
    tj.devicetime as LASTTRANSACTION,
    c.cardid as CARDID,
    c.PRINTEDNUMBER as PRINTEDNUMBER,
    c.STATE as STATE,
    c.EXPIRATION as EXPIRATION,
    c.cardtype as CARDTYPE,
    c.faremediaid as FAREMEDIAID
FROM sl_card c
JOIN (SELECT transitaccountid, MAX(devicetime) devicetime FROM sl_transactionjournal GROUP BY transitaccountid) tj ON tj.TRANSITACCOUNTID = c.cardid
LEFT JOIN (select cardid, max(NOTIFICATIONDATE) NOTIFICATIONDATE FROM SL_DORMANCYNOTIFICATION GROUP BY cardid) dm ON dm.cardid = c.cardid
WHERE (c.STATE = 1 OR c.STATE = 2) AND c.replacedcardid IS NULL;

DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_DORMANCYNOTIFICATION');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(2) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_DORMANCYNOTIFICATION');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/


COMMIT;

PROMPT Done!

SPOOL OFF;
