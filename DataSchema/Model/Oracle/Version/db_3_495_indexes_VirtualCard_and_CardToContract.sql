SPOOL db_3_495.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.495', 'LJU', 'New indexes on SL_VirtualCard and SL_CardToContract');

-- =======[ Changes to Existing Tables ]===========================================================================

CREATE INDEX IDX_VIRTUALCARD_CARD ON SL_VIRTUALCARD (CARDID);

CREATE INDEX IDX_CARDTOCONTRACT_CARD ON SL_CARDTOCONTRACT (CARDID, CONTRACTID);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
