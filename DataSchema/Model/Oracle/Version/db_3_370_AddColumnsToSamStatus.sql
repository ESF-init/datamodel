SPOOL db_3_370.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.370', 'MEA', 'Added columns to VDV_SAMSTATUS.');
-- Start adding schema changes here
-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE VDV_SAMSTATUS ADD 
	(
	   	TMEXPORTID				    NUMBER(10),
	   	TMEXPORTSEQUENCEID			NUMBER(10),
	   	TMEXPORTSATE				NUMBER(1),
		TMEXPORTCOUNTER				NUMBER(10)				
	);
COMMENT ON COLUMN VDV_SAMSTATUS.TMEXPORTID IS 'TmTerminal Export Id ';
COMMENT ON COLUMN VDV_SAMSTATUS.TMEXPORTSEQUENCEID IS 'TmTerminalConfig export Sequence Id.';
COMMENT ON COLUMN VDV_SAMSTATUS.TMEXPORTSATE IS 'TmTerminalConfig export State 0: not exported, 1: exported, 2: accepted, 3: to reexport, 4: Rejected.';
COMMENT ON COLUMN VDV_SAMSTATUS.TMEXPORTCOUNTER IS 'TmTerminalConfig export counter.';

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
