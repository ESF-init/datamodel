SPOOL db_3_607.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.607', 'EMN', 'Add eject state to printer states.');

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

Insert into SL_PRINTERSTATE
   (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
 Values
   (4, 'Eject', 'Eject');
   
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;