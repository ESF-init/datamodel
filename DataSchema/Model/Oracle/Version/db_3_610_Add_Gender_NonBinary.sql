SPOOL db_3_610.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.610', 'SMF', 'Added fourth gender value for non-binary.');

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------
INSERT INTO SL_Gender(EnumerationValue, Literal, Description)
    SELECT 3, 'NonBinary', 'NonBinary'
	FROM DUAL
	WHERE NOT EXISTS
	(
	     SELECT 1
		 FROM SL_Gender
		 WHERE
		 EnumerationValue = 3
	);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
