SPOOL db_3_066.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.066', 'ULB', 'added columns ISAM* and SEQ* to RM_ExternalData');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE RM_EXTERNALDATA
ADD (SAMID VARCHAR2(20 ));

ALTER TABLE RM_EXTERNALDATA
ADD (SamPrintedNo VARCHAR2(20 ));

ALTER TABLE RM_EXTERNALDATA
ADD (StartSequenceNo NUMBER(10 ));

ALTER TABLE RM_EXTERNALDATA
ADD (EndSequenceNo NUMBER(10 ));

COMMENT ON COLUMN RM_EXTERNALDATA.SAMID IS 'Only for ITSO : ISAM Id';
COMMENT ON COLUMN RM_EXTERNALDATA.SamPrintedNo IS 'Only for ITSO : ISAM printed number';
COMMENT ON COLUMN RM_EXTERNALDATA.StartSequenceNo IS 'Only for ITSO : First used  ISAM sequence number';
COMMENT ON COLUMN RM_EXTERNALDATA.EndSequenceNo IS 'Only for ITSO : Last used  ISAM sequence number';


-- =======[ New Tables ]===========================================================================================

--COMMENT ON TABLE XXX is '';

--COMMENT ON COLUMN XXX.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
--COMMENT ON COLUMN XXX.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
--COMMENT ON COLUMN XXX.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
