SPOOL db_3_550.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.550', 'SVR', 'Add missing enumeration values to SL_FAREPAYMENTRESULTTYPE.');

-- Start adding schema changes here

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 8, 'ManualOk', 'driver accepts invalid card manually' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 8 and Literal = 'ManualOk');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 119, 'ErrorSamkey', 'invalid SAM-Key' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 119 and Literal = 'ErrorSamkey');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 120, 'ErrorNoSession', 'no valid session' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 120 and Literal = 'ErrorNoSession');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 121, 'ErrorNoControl', 'no ctrl on reader' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 121 and Literal = 'ErrorNoControl');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 161, 'ErrorApduSendCardLost', 'Success of APDU execution is unknown' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 161 and Literal = 'ErrorApduSendCardLost');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 263, 'ErrorUnknownId', 'Unknown transit account ID' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 263 and Literal = 'ErrorUnknownId');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 550, 'ErrorEmvGeneralError', 'EMV Errors' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 550 and Literal = 'ErrorEmvGeneralError');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 551, 'ErrorEmvSeePhone', 'EMV Errors' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 551 and Literal = 'ErrorEmvSeePhone');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 552, 'ErrorEmvOdaFailedOrNotPerformed', 'EMV Errors' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 552 and Literal = 'ErrorEmvOdaFailedOrNotPerformed');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 603, 'ErrorVarioRequestAlreadyProcessed', 'Request was already processed' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 603 and Literal = 'ErrorVarioRequestAlreadyProcessed');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 604, 'ErrorVarioCreditCardAuthorizationDeclined', 'The authorization request was declined' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 604 and Literal = 'ErrorVarioCreditCardAuthorizationDeclined');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 605, 'ErrorVarioCreditCardRefundDeclined', 'The refund request was declined' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 605 and Literal = 'ErrorVarioCreditCardRefundDeclined');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 606, 'ErrorVarioOfflineTimestampDeny', 'Inspection denial because of timestamp' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 606 and Literal = 'ErrorVarioOfflineTimestampDeny');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 607, 'ErrorVarioCompletionRetry', 'Error in the credit card completion process' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 607 and Literal = 'ErrorVarioCompletionRetry');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 608, 'ErrorVarioTransactionDeprecated', 'Transaction is depecated (too old)' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 608 and Literal = 'ErrorVarioTransactionDeprecated');
INSERT INTO SL_FarePaymentResultType (EnumerationValue, Literal, Description) select 699, 'ErrorVarioMax', 'Max vario' from dual where not exists (select EnumerationValue from SL_FarePaymentResultType where EnumerationValue = 699 and Literal = 'ErrorVarioMax');

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
