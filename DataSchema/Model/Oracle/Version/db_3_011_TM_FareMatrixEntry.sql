SPOOL db_3_011.log

INSERT INTO vario_dmodellversion (rundate,dmodellno,responsible,annotation)
VALUES (SYSDATE,'3.011','FLF','Added DESCRIPTIONREFERENCE to TM_FareMatrixEntry');


---Start adding schema changes here

ALTER TABLE TM_FAREMATRIXENTRY
 ADD (DESCRIPTIONREFERENCE  VARCHAR2(200));

---End adding schema changes



COMMIT;

PROMPT Done!

SPOOL OFF