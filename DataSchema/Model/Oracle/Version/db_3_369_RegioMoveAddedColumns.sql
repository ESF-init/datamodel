SPOOL db_3_369.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.369', 'ARH', 'Added columns to SL_BOOKINGITEM.');
-- Start adding schema changes here
-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_BOOKINGITEM ADD 
	(
			BARCODE							CLOB,
			LONGITUDE						NVARCHAR2(20),
			LATITUDE						NVARCHAR2(20),
			OPTIONALFEATURETEXT				NVARCHAR2(255),	
			OPTIONALFEATUREREFERENCE		NVARCHAR2(255)				
	);
COMMENT ON COLUMN SL_BOOKINGITEM.BARCODE IS 'Storage for the generated barcode.';
COMMENT ON COLUMN SL_BOOKINGITEM.LONGITUDE IS 'Longitude of the vehicle s location.';
COMMENT ON COLUMN SL_BOOKINGITEM.LATITUDE IS 'Latitude of the vehicle s location.';
COMMENT ON COLUMN SL_BOOKINGITEM.OPTIONALFEATUREREFERENCE IS 'Optional feature reference for the Regiomove App developer.';
COMMENT ON COLUMN SL_BOOKINGITEM.OPTIONALFEATURETEXT IS 'Optional feature text for the Regiomove App developer.';

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
