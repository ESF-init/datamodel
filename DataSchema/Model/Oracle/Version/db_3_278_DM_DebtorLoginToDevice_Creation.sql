SPOOL db_3_278.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.278', 'LWU', 'Creation of DM_DEBTORLOGINTODEVICE');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE DM_DEBTORLOGINTODEVICE
(
	KEYID 					NUMBER(18) NOT NULL,
	CASHNO 					VARCHAR2(50),
	DEVICENO 				VARCHAR2(50),
	LOCATIONNO 				VARCHAR2(50),
	SALESCHANNEL 			VARCHAR2(50),
	DEBTORNO 				VARCHAR2(50),
	USERSHORTNAME 			VARCHAR2(50),
	INSERTDATETIME 			DATE DEFAULT SYSDATE,
	TRAININGFLAG 			VARCHAR2(50)
);

CREATE UNIQUE INDEX DM_DEBTORLOGINTODEVICE_PK ON DM_DEBTORLOGINTODEVICE (KEYID);

ALTER TABLE DM_DEBTORLOGINTODEVICE ADD (
  CONSTRAINT DM_DEBTORLOGINTODEVICE_PK
  PRIMARY KEY
  (keyId)
  USING INDEX DM_DEBTORLOGINTODEVICE_PK
  ENABLE VALIDATE);


COMMENT ON TABLE DM_DEBTORLOGINTODEVICE is 'Table logs when a Debtor logs into a Device to prevent dublet Login';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;