SPOOL db_3_247.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.247', 'SOE', 'Modify lenght of name in sl_from ');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_FORM
MODIFY Name NVARCHAR2(70);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;