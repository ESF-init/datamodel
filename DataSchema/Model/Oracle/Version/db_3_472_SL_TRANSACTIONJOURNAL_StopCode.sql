SPOOL db_3_472.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.472', 'FLF', 'Add column StopCode to sl_transactionjournal');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (STOPCODE  VARCHAR2(50));

COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.STOPCODE IS 'Stop code.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
