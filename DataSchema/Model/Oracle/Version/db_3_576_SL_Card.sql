SPOOL db_3_576.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.576', 'DST', 'Add ProjectIdentifier, OrderIdentifier and ProductionDate to SL_CARD');

ALTER TABLE SL_CARD ADD PROJECTIDENTIFIER NVARCHAR2(255);
ALTER TABLE SL_CARD ADD ORDERIDENTIFIER NVARCHAR2(255);
ALTER TABLE SL_CARD ADD PRODUCTIONDATE DATE;

COMMENT ON COLUMN SL_CARD.PROJECTIDENTIFIER IS 'ProjectID encoded in the card application';
COMMENT ON COLUMN SL_CARD.ORDERIDENTIFIER IS 'OrderID from the card manufacturer';
COMMENT ON COLUMN SL_CARD.PRODUCTIONDATE IS 'Production date of the card';

COMMIT;

PROMPT Done!

SPOOL OFF;
