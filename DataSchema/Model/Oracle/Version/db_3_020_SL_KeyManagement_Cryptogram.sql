SPOOL db_3_020.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.020', 'AMA', 'Key Management - CryptoResource');

-- Start adding schema changes here


-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================
CREATE TABLE SL_CRYPTORESOURCEDATA (
    CRYPTORESOURCEDATAID  		NUMBER(18)  				NOT NULL,		
	VERSION    					NUMBER(9)					DEFAULT 0                 		NOT NULL,
    RESOURCETYPE		    	NUMBER(9)					DEFAULT 0                 		NOT NULL,
	RawData         			BLOB                        NOT NULL,
	MetaData         			BLOB, 	
	VALIDFROM					DATE              			DEFAULT sysdate                 NOT NULL,
	VALIDTO						DATE,
	DESCRIPTION					NVARCHAR2(512),
	RESOURCESTATUS				NUMBER(9)              		DEFAULT 0                     	NOT NULL,
	LASTUPDATE					DATE                     	DEFAULT sysdate               	NOT NULL,
	LASTUSER					NVARCHAR2(50)            	DEFAULT 'SYS'                 	NOT NULL,
	LASTMODIFIED				DATE                     	DEFAULT sysdate               	NOT NULL,
	TRANSACTIONCOUNTER			NUMBER                  	NOT NULL,
	CONSTRAINT PK_CRYPTORESOURCEDATA PRIMARY KEY (CRYPTORESOURCEDATAID),
	CONSTRAINT CRYPTORESOURCEDATA_UC1 UNIQUE(RESOURCETYPE,VERSION)
);

CREATE TABLE SL_DEVICEREADERCRYPTORESOURCE (
    DEVICEREADERCRYPTORESOURCEID  				NUMBER(18)  				NOT NULL,		
    DEVICEREADERID  							NUMBER(18)  				NOT NULL,		
    CRYPTORESOURCEDATAID    					NUMBER(18)					NOT NULL,
	DEVICERESOURCESTATUS						NUMBER(9)              		DEFAULT 0						NOT NULL,
	LASTUPDATE									DATE                     	DEFAULT sysdate               	NOT NULL,
	LASTUSER									NVARCHAR2(50)            	DEFAULT 'SYS'                 	NOT NULL,
	LASTMODIFIED								DATE                     	DEFAULT sysdate               	NOT NULL,
	TRANSACTIONCOUNTER							NUMBER                  	NOT NULL,
	CONSTRAINT FK_DEVICEREADERRESOURCE_DEV FOREIGN KEY (DEVICEREADERID) REFERENCES SL_DEVICEREADER (DEVICEREADERID),
	CONSTRAINT FK_DEVICEREADERRESOURCE_RES FOREIGN KEY (CRYPTORESOURCEDATAID) REFERENCES SL_CRYPTORESOURCEDATA (CRYPTORESOURCEDATAID),
	CONSTRAINT PK_DEVICEREADERCRYPTORESOURCE PRIMARY KEY (DEVICEREADERCRYPTORESOURCEID)
);


-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

CREATE SEQUENCE SL_CRYPTORESOURCEDATA_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;

CREATE SEQUENCE SL_DEVICEREADERRESOURCE_SEQ
       INCREMENT BY 1
       NOMINVALUE
       NOMAXVALUE
       CACHE 20
       NOCYCLE
       NOORDER;
	   
-- =======[ Trigger ]==============================================================================================

CREATE OR REPLACE TRIGGER SL_CRYPTORESOURCEDATA_BRI before insert on SL_CRYPTORESOURCEDATA for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_CRYPTORESOURCEDATA_BRU before update on SL_CRYPTORESOURCEDATA for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_DEVICEREADERRESOURCE_BRI before insert on SL_DEVICEREADERCRYPTORESOURCE for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/
CREATE OR REPLACE TRIGGER SL_DEVICEREADERRESOURCE_BRU before update on SL_DEVICEREADERCRYPTORESOURCE for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
