SPOOL db_3_561.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.561', 'MKD', 'Corrected the number precision of the DURATION column in PERF_SRTDATAAGGREGATION + view update');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE PERF_SRTDATAAGGREGATION
MODIFY
(
   DURATION  NUMBER(12,3)
);

-- =======[ Views ]================================================================================================
CREATE OR REPLACE VIEW VIEW_SERVERRESPONSETIMEDATA AS
(SELECT p.PERFORMANCEID PerformanceId,
       p.EVENTTIME,
       p.INDICATORID,
       a.HOSTNAME,
       t.SALESCHANNELID,
       CAST(t.DEVICENUMBER as NUMBER(9)) DeviceNumber,
       t.SHIFTBEGIN,
       CAST(t.RESULTSOURCE as NUMBER(9)) NumberOfAuthorizedTx,
       CAST(p.DURATION as NUMBER(12,3)) Duration,
       CAST(p.QUANTITY as NUMBER(9)) Quantity,
       CAST(
           case when p.DURATION > 150 Then 1 ELSE 0 End
        as NUMBER(9)) NumberAboveThreshold,
       CAST(1 as NUMBER(9)) TotalNumber,
       CAST(0 as NUMBER(1)) IsAggregated,
       CAST(0 as NUMBER(9)) AggregationType
FROM PERF_PERFORMANCE p INNER JOIN PERF_ADDITIONALDATA a ON p.PERFORMANCEID = a.PERFORMANCEID
     LEFT OUTER JOIN SL_TRANSACTIONJOURNAL t ON a.TRANSACTIONID = t.TRANSACTIONID
UNION
SELECT PerformanceId,
    EventTime,
    IndicatorID,
    Hostname,
    SalesChannelId,
    DeviceNumber,
    ShiftBegin,
    NumberOfAuthorizedTx,
    Duration,
    Quantity,
    NumberAboveThreshold,
    TotalNumber,
    CAST(1 as NUMBER(1)) IsAggregated,
    AggregationType
FROM PERF_SRTDataAggregation p);
/
---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
