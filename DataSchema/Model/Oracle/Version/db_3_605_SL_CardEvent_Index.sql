SPOOL db_3.605.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.605', 'FLF', 'Added index to SL_CARDEVENT.CARDID.');

-- =======[ Changes to Existing Tables ]===========================================================================

CREATE INDEX IDX_CARDEVENT_CARDID ON SL_CARDEVENT
(CARDID);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off