SPOOL db_3_079.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.079', 'DST', 'Added ContractID to SL_Job.');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_job add contractid number(18);
alter table sl_job add constraint fk_job_contract foreign key (contractid) references sl_contract(contractid);
comment on column sl_job.contractid is 'Optional reference to contract.';

alter table sl_job modify clientid null;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
