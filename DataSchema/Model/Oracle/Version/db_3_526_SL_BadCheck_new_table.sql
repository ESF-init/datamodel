SPOOL db_3_526.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.526', 'MFA', 'New table to store bad checks');

-- Start adding schema changes here
-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_BADCHECK
(
	BADCHECKID NUMBER(18) NOT NULL,
	BRANCHROUTING NVARCHAR2(50) NOT NULL,
	CHECKINGACCOUNT NVARCHAR2(50) NOT NULL,
	LASTUSER NVARCHAR2(50) DEFAULT 'SYS' NOT NULL,
	LASTMODIFIED DATE DEFAULT sysdate NOT NULL,
	TRANSACTIONCOUNTER NUMBER(38) NOT NULL
);

COMMENT ON TABLE SL_BADCHECK IS 'The table used to store bad check information.';
COMMENT ON COLUMN SL_BADCHECK.BADCHECKID IS 'Primary key.';
COMMENT ON COLUMN SL_BADCHECK.BRANCHROUTING IS 'Branch routing number.';
COMMENT ON COLUMN SL_BADCHECK.CHECKINGACCOUNT IS 'Checking account number.';

ALTER TABLE SL_BADCHECK ADD CONSTRAINT PK_BADCHECKID PRIMARY KEY (BADCHECKID) USING INDEX;

-- =======[ Sequences ]============================================================================================

CREATE SEQUENCE SL_BADCHECK_seq INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================


CREATE OR REPLACE TRIGGER SL_BADCHECK_BRU 
	BEFORE UPDATE ON SL_BADCHECK 
	FOR EACH ROW BEGIN
		IF (:NEW.TransactionCounter != :OLD.TransactionCounter + 1) 
		THEN raise_application_error( -20000, 'Concurrency Failure' ); 
		END IF; 
		:NEW.TransactionCounter := dbms_utility.get_time; 
	END;
/

CREATE OR REPLACE TRIGGER SL_BADCHECK_BRI
	BEFORE INSERT ON SL_BADCHECK 
	FOR EACH ROW BEGIN
		:NEW.TransactionCounter := dbms_utility.get_time; 
	END;
/

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
