SPOOL db_3_086.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.086', 'MIW', 'Change defaults in ACC_RevenueSettlement.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE ACC_RevenueSettlement MODIFY (SettlementType DEFAULT 1);
ALTER TABLE ACC_RevenueSettlement MODIFY (SettlementDistributionPolicy DEFAULT 0);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
