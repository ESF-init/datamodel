SPOOL db_3_271.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.271', 'ULB', 'Extended length of rm_ext_transaction.note');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE RM_EXT_TRANSACTION MODIFY(NOTE VARCHAR2(500 BYTE));

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;