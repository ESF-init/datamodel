SPOOL db_3_084.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.084', 'DST', 'Added EncryptedData and EncryptedDataType to SL_PaymentJournal.');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_paymentjournal add EncryptedData NVARCHAR2(2000); 
alter table sl_paymentjournal add EncryptedDataType NUMBER(9);

COMMENT ON COLUMN sl_paymentjournal.EncryptedData IS 'Encrypted payment data.';
COMMENT ON COLUMN sl_paymentjournal.EncryptedDataType  IS 'Encrypted payment data type.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
