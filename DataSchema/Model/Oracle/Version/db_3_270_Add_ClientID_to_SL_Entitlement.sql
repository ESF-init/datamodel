SPOOL db_3_270.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.270', 'MMB', 'Added CLIENTID to SL_ENTITLEMENT.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_ENTITLEMENT 
ADD CLIENTID NUMBER (10);

-- =======[ Commit ]===============================================================================================

COMMENT ON COLUMN SL_ENTITLEMENT.CLIENTID IS 'ID of corresponding Client';
COMMIT;

PROMPT Done!

SPOOL OFF;