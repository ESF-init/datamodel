SPOOL db_3_542.log;


------------------------------------------------------------------------------
--Version 3.542
------------------------------------------------------------------------------

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.542', 'MEA', 'Columns CM_CRYPTOGRAMCERTIFICATE.MESSAGEID and CM_CRYPTOGRAMCERTIFICATE.LASTMODIFIED added.');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE CM_CRYPTOGRAMCERTIFICATE ADD (MESSAGEID  NUMBER(10));
ALTER TABLE CM_CRYPTOGRAMCERTIFICATE ADD (LASTMODIFIED  DATE);
COMMENT ON COLUMN CM_CRYPTOGRAMCERTIFICATE.MESSAGEID IS 'TMS message id';
COMMENT ON COLUMN CM_CRYPTOGRAMCERTIFICATE.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
