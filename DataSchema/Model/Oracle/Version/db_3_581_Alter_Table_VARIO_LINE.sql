SPOOL db_3_581.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.581', 'SLR', 'Added Column LINECODEEXTERN to VARIO_LINE');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE VARIO_LINE  ADD LINECODEEXTERN NVARCHAR2(200);          


-- =======[ Commit ]===============================================================================================

COMMENT ON COLUMN VARIO_LINE.LINECODEEXTERN IS 'Line Identifier of external system.';


COMMIT;

PROMPT Done!

SPOOL OFF;
