SPOOL db_3_200.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.200', 'ULB', 'added shiftid to rm_shiftinventory');

-- Start adding schema changes here
-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE RM_SHIFTINVENTORY
ADD (shiftid NUMBER(18) DEFAULT 0);

COMMENT ON COLUMN 
RM_SHIFTINVENTORY.shiftid IS 
'Refeferences RM_Shift.Shiftid if > 0';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
