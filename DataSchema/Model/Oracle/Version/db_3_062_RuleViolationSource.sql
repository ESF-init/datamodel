SPOOL db_3_062.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation)
VALUES (sysdate, '3.062', 'SLR', 'Enumeration for Rule Violation Source Type.');

-- Start adding schema changes here


-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_RULEVIOLATIONSOURCETYPE
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_RULEVIOLATIONSOURCETYPE PRIMARY KEY (ENUMERATIONVALUE)
);

COMMENT ON TABLE SL_RULEVIOLATIONSOURCETYPE IS 'Enumeration of velocity check source type. Defines a source for velocity checks such as FareMedia or FareProduct.';
COMMENT ON COLUMN SL_RULEVIOLATIONPROVIDER.ENUMERATIONVALUE IS 'Unique ID of the rule violation source type.';
COMMENT ON COLUMN SL_RULEVIOLATIONPROVIDER.DESCRIPTION IS 'Descripton of the source. This is used in the UI to display the value.';
COMMENT ON COLUMN SL_RULEVIOLATIONPROVIDER.LITERAL IS 'Name of the source; used internally.';

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_RULEVIOLATION ADD (RULEVIOLATIONSOURCETYPE NUMBER(9) DEFAULT 0 NOT NULL);
COMMENT ON COLUMN SL_RULEVIOLATION.RULEVIOLATIONSOURCETYPE IS 'Reference to enumeration SL_RULEVIOLATIONSOURCETYPE.';


-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================


-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
