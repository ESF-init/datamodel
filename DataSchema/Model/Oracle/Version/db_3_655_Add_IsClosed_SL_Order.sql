SPOOL db_3_655.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.655', 'EMN', 'Add SL_Order.IsClosed');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TRIGGER SL_ORDER_BRU DISABLE;

ALTER TABLE SL_ORDER
ADD ISCLOSED NUMBER(1) default 0;

COMMENT ON COLUMN 
SL_ORDER.IsClosed IS 
'Indicates if the order has been closed.';

ALTER TRIGGER SL_ORDER_BRU ENABLE;

COMMIT;

PROMPT Done!

SPOOL OFF;

