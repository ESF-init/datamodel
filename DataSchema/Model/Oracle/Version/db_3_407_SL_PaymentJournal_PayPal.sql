SPOOL db_3_407.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.407', 'DBY', 'Added paypal columns to SL_PAYMENTJOURNAL');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PAYMENTJOURNAL ADD PayPalAuthorizationId NVARCHAR2(200) NULL;
COMMENT ON COLUMN SL_PAYMENTJOURNAL.PayPalAuthorizationId IS 'PayPal parameter that identifies an authorization. More than one authorization ID can be associated with an order';

ALTER TABLE SL_PAYMENTJOURNAL ADD PayPalRequestId NVARCHAR2(200) NULL;
COMMENT ON COLUMN SL_PAYMENTJOURNAL.PayPalRequestId IS 'Reply message from cybersource during paypal transaction';

ALTER TABLE SL_PAYMENTJOURNAL ADD PayPalRequestToken NVARCHAR2(200) NULL;
COMMENT ON COLUMN SL_PAYMENTJOURNAL.PayPalRequestToken IS 'the request token from the authorization you want to capture or reverse';


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
