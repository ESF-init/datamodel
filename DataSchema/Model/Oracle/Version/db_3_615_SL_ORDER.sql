SPOOL db_3.615.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.615', 'ARH', 'New field PAYMENTPROVIDERTOKEN in SL_ORDER.');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_ORDER
ADD  PAYMENTPROVIDERTOKEN NVARCHAR2(50) NULL;
COMMENT ON COLUMN SL_ORDER.PAYMENTPROVIDERTOKEN IS 'If payment provider token needs temporary to be saved (for example : iFrame and SaferpayJsonApi).'; 


COMMIT;

PROMPT Done!
SPOOL off