SPOOL db_3_420.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.420', 'flf', 'Add VALUEPASSCREDIT to TM_Ticket. Add EXTCALENDARID to TM_RULE_PERIOD');


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE TM_TICKET ADD (VALUEPASSCREDIT  NUMBER(10));
COMMENT ON COLUMN TM_TICKET.VALUEPASSCREDIT IS 'Credit a value pass provides per new trip.';

ALTER TABLE TM_RULE_PERIOD ADD (EXTCALENDARID  NUMBER(18));
COMMENT ON COLUMN TM_RULE_PERIOD.EXTCALENDARID IS 'References TM_CALENDAR.CALENDARID. ID of the calandar that defines the extensions per period.';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
