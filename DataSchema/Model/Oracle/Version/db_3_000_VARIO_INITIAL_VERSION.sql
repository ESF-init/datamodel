SPOOL db_3_000.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.000', 'JGI', 'Just set initial version number');


COMMIT;

PROMPT Done!

SPOOL OFF
