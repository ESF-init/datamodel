SPOOL db_3_012.log

INSERT INTO vario_dmodellversion (rundate,dmodellno,responsible,annotation)
VALUES (SYSDATE,'3.012','FLF','Added Index to TM_FareMatrixEntry');


---Start adding schema changes here

CREATE INDEX IDX_FME_DESCRIPTION ON TM_FAREMATRIXENTRY
(DESCRIPTION);

---End adding schema changes



COMMIT;

PROMPT Done!

SPOOL OFF