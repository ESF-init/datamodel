SPOOL db_3_375.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.375', 'MMB', 'Set columns to nullable for promocode table');

-- =======[ Changes to Existing Tables ]===========================================================================

Alter Table SL_PROMOCODE modify TICKETID NULL;
Alter Table SL_PROMOCODE modify TICKETTYPEID NULL;
Alter Table SL_PROMOCODE modify REDEMPTIONDATE NULL;

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;






