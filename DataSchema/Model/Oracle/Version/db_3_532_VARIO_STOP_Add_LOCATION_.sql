SPOOL db_3_532.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.532', 'JGI', 'Add column LOCATION to VARIO_STOP');


-- =======[ New Tables ]===========================================================================================

ALTER TABLE VARIO_STOP ADD (LOCATION  VARCHAR2(100));

COMMENT ON COLUMN VARIO_STOP.LOCATION IS 'LOcation (e.g. City ) of stop';

COMMIT;

PROMPT Done!

SPOOL OFF;
