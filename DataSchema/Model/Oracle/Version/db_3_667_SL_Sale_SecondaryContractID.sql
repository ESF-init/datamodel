SPOOL db_3_667.log;


INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.667', 'DST', 'Add SecondaryContractID to SL_Sale');

ALTER TABLE SL_Sale ADD SecondaryContractID NUMBER(18);
ALTER TABLE SL_Sale 
ADD CONSTRAINT FK_SaleSecondaryContract 
FOREIGN KEY (SecondaryContractID) 
REFERENCES SL_Contract (ContractID);
COMMENT ON COLUMN SL_Sale.SecondaryContractID is 'Secondary reference to contract used for subsidy payments.';


-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW VIEW_SL_SALEHISTORY
(
  SALEID,
  SALETRANSACTIONID,
  RECEIPTREFERENCE,
  SALESCHANNELID,
  SALETYPE,
  LOCATIONNUMBER,
  DEVICENUMBER,
  SALESPERSONNUMBER,
  MERCHANTNUMBER,
  NETWORKREFERENCE,
  SALEDATE,
  OPERATORID,
  ACCOUNTID,
  SECONDARYACCOUNTID,
  EXTERNALORDERNUMBER,
  ORDERID,
  NOTES,
  STATE,
  ORDERNUMBER,
  ISCANCELED,
  AMOUNT,
  LASTCUSTOMER,
  SALESPERSONEMAIL
)
AS 
select
    sl_sale.saleid,
    sl_sale.saletransactionid,
    sl_sale.receiptreference,
    sl_sale.saleschannelid,
    sl_sale.saletype,
    sl_sale.locationnumber,
    sl_sale.devicenumber,
    sl_sale.salespersonnumber,
    sl_sale.merchantnumber,
    sl_sale.networkreference,
    sl_sale.saledate,
    sl_sale.clientid   as operatorid,
    sl_sale.contractid as accountid,
    sl_sale.secondarycontractid as secondaryaccountid,
    sl_sale.externalordernumber,
    sl_sale.orderid,
    sl_sale.notes,
    sl_order.state,
    sl_order.ordernumber,
    case when sl_sale.saleid not in (select can.cancellationreferenceid from sl_sale can where can.cancellationreferenceid is not null) then 0 else 1 end iscanceled,
    (select sum(sl_paymentjournal.amount) from sl_paymentjournal where sl_paymentjournal.saleid = sl_sale.saleid) amount,
    c.lastcustomer,
    sl_sale.salespersonemail
  from sl_sale
        left outer join sl_order on sl_sale.orderid = sl_order.orderid
        left outer join ( select h.lastcustomer, h.orderid, row_number() over (partition by h.orderid order by h.lastmodified) as rn from sl_orderhistory h ) c on c.orderid = sl_sale.orderid and rn = 1;

CREATE OR REPLACE VIEW VIEW_SL_SALEHISTORYDETAIL
as
SELECT       s.saleid,
             s.saletransactionid,
             s.receiptreference,
             s.saleschannelid,
             s.saletype,
             s.locationnumber,
             s.devicenumber,
             s.salespersonnumber,
             s.merchantnumber,
             s.networkreference,
             s.saledate,
             s.clientid operatorid,
             s.contractid accountid,
             s.secondarycontractid secondaryaccountid,
             s.externalordernumber,
             s.orderid,
             t.name ticketname,
             p.ticketid,
             CASE -- for topups, the value is in remainingvalue, not in price
               WHEN p.tickettype = 200 THEN p.remainingvalue
               ELSE p.price
             END price,
             o.state,
             o.ordernumber,
             p.productid,
             NVL(required_od.quantity - required_od.fulfilledquantity, 1) * (od.quantity - od.fulfilledquantity) quantity, -- use the quantity of the parent
             (od.quantity * nvl(required_od.fulfilledquantity, 1)) fulfilledquantity,
             od.orderdetailid,
             od.requiredorderdetailid parentorderdetailid
      FROM sl_orderdetail od
        JOIN sl_sale s ON s.orderid = od.orderid
        JOIN sl_product p ON p.productid = od.productid
        JOIN tm_ticket t ON t.ticketid = p.ticketid
        JOIN sl_order o ON o.orderid = s.orderid
        LEFT JOIN sl_orderdetail required_od ON od.requiredorderdetailid = required_od.orderdetailid
      UNION ALL
      -- fulfilled orders / normal sales
      SELECT s.saleid,
             s.saletransactionid,
             s.receiptreference,
             s.saleschannelid,
             s.saletype,
             s.locationnumber,
             s.devicenumber,
             s.salespersonnumber,
             s.merchantnumber,
             s.networkreference,
             s.saledate,
             s.clientid operatorid,
             s.contractid accountid,
             s.secondarycontractid secondaryaccountid,
             s.externalordernumber,
             s.orderid,
             t.name ticketname,
             tj.ticketid,
             tj.fareamount,
             NULL orderstate,
             NULL ordernumber,
             NULL productid,
             count(*) quantity,
             count(*) fulfilledquantity,
             cast(null as number(18)) orderdetailid,
             cast(null as number(18)) parentorderdetailid
      FROM sl_transactionjournal tj
        JOIN sl_sale s ON s.saleid = tj.saleid
        JOIN tm_ticket t ON t.ticketid = tj.ticketid
		WHERE t.tickettype <> 200 -- Filter out AddValueAccount transactions
        group by s.saleid,
             s.saletransactionid,
             s.receiptreference,
             s.saleschannelid,
             s.saletype,
             s.locationnumber,
             s.devicenumber,
             s.salespersonnumber,
             s.merchantnumber,
             s.networkreference,
             s.saledate,
             s.clientid,
             s.contractid,
             s.secondarycontractid,
             s.externalordernumber,
             s.orderid,
             t.name,
             tj.ticketid,
             tj.fareamount;

COMMIT;

PROMPT Done!

SPOOL OFF;
