SPOOL db_3_355.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.355', 'MMB', 'Job types added');

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (12, 'PromoCodeBlock', 'Promotional Codes bulk blocking');
INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (13, 'PromoCodeUnblock', 'Promotional Codes bulk unblocking');
INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (14, 'PromoCodeImport', 'Promotional Codes bulk importing');


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;


