SPOOL db_3_642.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.642', 'DCS', 'Add TaxLocationCode, TaxTicketId to SL_TransactionJournal');


-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_TRANSACTIONJOURNAL ADD TAXLOCATIONCODE NUMBER(10) NULL;
COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.TAXLOCATIONCODE IS 'Tax location code';

ALTER TABLE SL_TRANSACTIONJOURNAL ADD TAXTICKETID Number(18) NULL;
COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.TAXTICKETID IS 'Tax ticket ID';

ALTER TABLE SL_TRANSACTIONJOURNAL ADD CONSTRAINT FK_TRANSJOURNAL_TAXTICKET FOREIGN KEY (TAXTICKETID) REFERENCES TM_TICKET(TICKETID);

COMMENT ON COLUMN SL_PRODUCT.TAXLOCATIONCODE  IS 'OBSOLETE.';
COMMENT ON COLUMN SL_PRODUCT.TAXTICKETID IS 'OBSOLETE.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;