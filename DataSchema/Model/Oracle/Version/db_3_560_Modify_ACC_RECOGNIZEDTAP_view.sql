SPOOL db_3_560.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.560', 'MFA', 'Modify ACC_RECOGNIZEDTAP view to add BOARDINGGUID');

-- =======[ Views ]================================================================================================


  CREATE OR REPLACE FORCE VIEW "ACC_RECOGNIZEDTAP" ("AMOUNT", "BASEFARE", "CREDITACCOUNTNUMBER", "CUSTOMERGROUP", "DEBITACCOUNTNUMBER", "LINEGROUPID", "POSTINGDATE", "POSTINGREFERENCE", "PREMIUM", "REVENUERECOGNITIONID", "REVENUESETTLEMENTID", "TICKETNUMBER", "OPERATORID", "CLOSEOUTPERIODID", "PERIODFROM", "PERIODTO", "CLOSEOUTTYPE", "STATE", "CANCELLATIONREFERENCE", "CANCELLATIONREFERENCEGUID", "CLIENTID", "DEVICETIME", "FAREAMOUNT", "LINE", "TRANSACTIONOPERATORID", "PRODUCTID", "PURSEBALANCE", "PURSECREDIT", "RESULTTYPE", "SALESCHANNELID", "TARIFFDATE", "TARIFFVERSION", "TICKETID", "TICKETINTERNALNUMBER", "TRANSACTIONJOURNALID", "TRANSACTIONTYPE", "TRIPTICKETINTERNALNUMBER", "LASTMODIFIED", "CANCELLEDTAP", "BOARDINGGUID") AS 
  SELECT rr.amount, rr.basefare, rr.creditaccountnumber, rr.customergroup,
          rr.debitaccountnumber, rr.linegroupid, rr.postingdate,
          rr.postingreference, rr.premium, rr.revenuerecognitionid,
          rr.revenuesettlementid, rr.ticketnumber, rr.operatorid,
          cp.closeoutperiodid, cp.periodfrom, cp.periodto, cp.closeouttype,
          cp.state, tj1.cancellationreference, tj1.cancellationreferenceguid,
          tj1.clientid, tj1.devicetime, tj1.fareamount, tj1.line,
          tj1.operatorid AS transactionoperatorid, tj1.productid,
          tj1.pursebalance, tj1.pursecredit, tj1.resulttype,
          tj1.saleschannelid, tj1.tariffdate, tj1.tariffversion, tj1.ticketid,
          tj1.ticketinternalnumber, tj1.transactionjournalid,
          tj1.transactiontype, tj1.tripticketinternalnumber, rr.LASTMODIFIED,
          CAST
             (DECODE (tj1.cancellationreferenceguid, NULL, 0, 1) AS NUMBER (9)
             ) AS cancelledtap, tj1.boardingguid
     FROM ((sl_transactionjournal tj1 INNER JOIN acc_revenuerecognition rr
          ON tj1.transactionjournalid = rr.transactionjournalid)
          INNER JOIN
          acc_closeoutperiod cp ON cp.closeoutperiodid = rr.closeoutperiodid)
    WHERE tj1.validto IS NULL
      AND (tj1.transactiontype = 66 OR tj1.transactiontype = 84);


COMMIT;

PROMPT Done!

SPOOL OFF;
