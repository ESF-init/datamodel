SPOOL db_3_555.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.555', 'TIR', 'Adding peformance enhancing indexes and dropping a deprecated procedure.');

-- =======[ Changes to Existing Tables ]===========================================================================
CREATE INDEX UM_LSERRORTOTASK_ERRORID ON UM_LSERRORTOTASK (LOADINGSTATISTICSERRORID);
CREATE INDEX IDX_ARCHIVE_DEVCLASS_NAME ON UM_ARCHIVE (DEVICECLASSID,FILENAME);
DROP PROCEDURE P_ENSUREEXISTENCEOFSCRATCHUNIT;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
