SPOOL db_3_631.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.631', 'ULB', 'RM_Paymentdetal: Add Paymenttype and Resultcode ');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE RM_PAYMENTDETAILS
 ADD (Resultcode  NUMBER(10));

ALTER TABLE RM_PAYMENTDETAILS
 ADD (PaymentType  NUMBER(10));

COMMENT ON COLUMN RM_PAYMENTDETAILS.Resultcode IS 'Resultcode provided by Payment teminal/provider';
COMMENT ON COLUMN RM_PAYMENTDETAILS.PaymentType IS 'PaymentType provided by Payment teminal/provider';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
