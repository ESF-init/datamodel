SPOOL db_3_439.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.439', 'flf', 'New columns in SL_TRANSACTIONJOURNAL');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (TRIPVALUEFLOOR  NUMBER(9));
 COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.TRIPVALUEFLOOR IS 'Floor value of a trip which can not be undercut';
 
 ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (TRIPVALUEFLOOR2  NUMBER(9));
 COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.TRIPVALUEFLOOR2 IS 'Floor value of a trip (second purse) which can not be undercut';




---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
