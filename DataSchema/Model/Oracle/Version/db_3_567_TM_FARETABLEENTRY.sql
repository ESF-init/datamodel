SPOOL db_3_567.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.567', 'FLF', 'Add new field BASEFARE and comment to TM_FARETABLEENTRY');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE TM_FARETABLEENTRY
 ADD (BASEFARE  NUMBER(18));


COMMENT ON COLUMN TM_FARETABLEENTRY.BASEFARE IS 'Fare without premium part.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
