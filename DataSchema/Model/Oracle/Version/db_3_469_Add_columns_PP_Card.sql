SPOOL db_3_469.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.469', 'ARH', 'Add columns in PP_CARD');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE PP_CARD ADD (VERIFICATIONTOKEN  NVARCHAR2(100));
ALTER TABLE PP_CARD ADD (VERIFICATIONTOKENEXPIRYTIME DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL);
ALTER TABLE PP_CARD ADD (ISACTIVATEDWITHEMAIL  NUMBER(1, 0) DEFAULT 0 NOT NULL);

COMMENT ON COLUMN PP_CARD.VERIFICATIONTOKEN IS 'Token for email verification when registering a new virtual card.';
COMMENT ON COLUMN PP_CARD.VERIFICATIONTOKENEXPIRYTIME IS 'Expiry time for the verification token.';
COMMENT ON COLUMN PP_CARD.ISACTIVATEDWITHEMAIL IS 'Flag to know if the virtaul card was activated with an email.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
