SPOOL db_3_218.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.218', 'DST', 'Naming changes to SL_NUMBERGROUPRANDOMIZED.');

-- =======[ Changes to Existing Tables ]===========================================================================

DROP PROCEDURE GET_SL_NUMBERGROUPRANDOMIZED;

ALTER TABLE SL_NUMBERGROUPRANDOMIZED RENAME COLUMN VALUE TO RANDOMIZEDVALUE;
ALTER TABLE SL_NUMBERGROUPRANDOMIZED RENAME COLUMN NUMBERGROUPSCOPE TO SCOPE;

COMMENT ON TABLE SL_NumberGroupRandomized IS 'Contains pre-generated numbers for specific number groups, sorted by a random value.';
COMMENT ON COLUMN SL_NumberGroupRandomized.NumberGroupRandomizedID IS 'Unique identifier for this row.';
COMMENT ON COLUMN SL_NumberGroupRandomized.RandomizedValue IS 'The number created by the number group.';
COMMENT ON COLUMN SL_NumberGroupRandomized.RandomSortNumber IS 'Random number used to sort when getting values.';
COMMENT ON COLUMN SL_NumberGroupRandomized.IsUsed IS 'Marks if this value has been used already.';
COMMENT ON COLUMN SL_NumberGroupRandomized.Scope IS 'The number group scope this value belongs to.';
COMMENT ON COLUMN SL_NumberGroupRandomized.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_NumberGroupRandomized.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_NumberGroupRandomized.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
