SPOOL db_3_156.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.156', 'DST', 'New table SL_OrganizationParticipant');

-- =======[ New Tables ]===========================================================================================

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE SL_OrganizationParticipant
(
   OrganizationParticipantID NUMBER(18)        NOT NULL,
   CONTRACTID                NUMBER(18)        NOT NULL,  
   CARDID                    NUMBER(18),
   FIRSTNAME                 NVARCHAR2(50),
   LASTNAME                  NVARCHAR2(50),
   DATEOFBIRTH               DATE              DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
   EMAIL                     NVARCHAR2(512),
   PHONENUMBER               NVARCHAR2(50),
   CELLPHONENUMBER           NVARCHAR2(50),
   FAXNUMBER                 NVARCHAR2(50),
   GENDER                    NUMBER(9),
   IDENTIFIER                NVARCHAR2(20),
   LASTUSER                  NVARCHAR2(50)     DEFAULT 'SYS' NOT NULL,
   LASTMODIFIED              DATE              DEFAULT sysdate NOT NULL,
   TRANSACTIONCOUNTER        NUMBER            NOT NULL
);

ALTER TABLE SL_OrganizationParticipant ADD CONSTRAINT PK_OrganizationParticipant PRIMARY KEY (OrganizationParticipantID);
ALTER TABLE SL_OrganizationParticipant ADD CONSTRAINT FK_OrgParticipant_Contract FOREIGN KEY (CONTRACTID) REFERENCES SL_CONTRACT (CONTRACTID);
ALTER TABLE SL_OrganizationParticipant ADD CONSTRAINT FK_OrgParticipant_Card FOREIGN KEY (CARDID) REFERENCES SL_CARD (CARDID);

COMMENT ON TABLE SL_OrganizationParticipant IS 'All participants are stored here';
COMMENT ON COLUMN SL_OrganizationParticipant.OrganizationParticipantID IS 'Unique identifier of the participant';
COMMENT ON COLUMN SL_OrganizationParticipant.CONTRACTID IS 'References SL_CONTRACT.CONTRACTID';
COMMENT ON COLUMN SL_OrganizationParticipant.FIRSTNAME IS 'First name of the participant';
COMMENT ON COLUMN SL_OrganizationParticipant.LASTNAME IS 'Last name of the participant';
COMMENT ON COLUMN SL_OrganizationParticipant.DATEOFBIRTH IS 'The Date of birth of the participant';
COMMENT ON COLUMN SL_OrganizationParticipant.EMAIL IS 'Email of the participant';
COMMENT ON COLUMN SL_OrganizationParticipant.PHONENUMBER IS 'Phone number of the participant';
COMMENT ON COLUMN SL_OrganizationParticipant.CELLPHONENUMBER IS 'Cellphone number of the participant';
COMMENT ON COLUMN SL_OrganizationParticipant.FAXNUMBER IS 'Fax number of the participant';
COMMENT ON COLUMN SL_OrganizationParticipant.GENDER IS 'The gender of the participant';
COMMENT ON COLUMN SL_OrganizationParticipant.IDENTIFIER IS 'Identifier of the participant';
COMMENT ON COLUMN SL_OrganizationParticipant.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_OrganizationParticipant.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_OrganizationParticipant.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_OrganizationParticipant');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_OrganizationParticipant');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
