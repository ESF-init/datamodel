SPOOL db_3_132.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.132', 'ulb', 'Added Shoppingcart to rm_transaction');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE RM_TRANSACTION
 ADD 
 (
   ShoppingCart  Number(10)
 );

 COMMENT ON COLUMN RM_TRANSACTION.ShoppingCart IS 'ShoppingCart  <dddddnnnnn> (d=device, n= devicenumber)';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
