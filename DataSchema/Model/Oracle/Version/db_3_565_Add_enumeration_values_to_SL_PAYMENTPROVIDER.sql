SPOOL db_3_565.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.565', 'ARH', 'Add enumeration value to SL_PAYMENTPROVIDER.');

-- Start adding schema changes here

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_PaymentProvider(EnumerationValue, Literal, Description)VALUES (10, 'SaferpayJsonApi', 'SaferpayJsonApi');

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
