﻿SPOOL db_3_366.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.366', 'HNI', 'Updated RegioMove Enumerations');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- SL_ProviderAccountState

UPDATE SL_PROVIDERACCOUNTSTATE
SET 
    LITERAL = 'StartCreating',
    Description = 'Default value on creation'
WHERE 
    ENUMERATIONVALUE = 0;  

UPDATE SL_PROVIDERACCOUNTSTATE
SET 
    LITERAL = 'Created',
    Description = 'Provider account is created, may need verification.'
WHERE 
    ENUMERATIONVALUE = 1;  

UPDATE SL_PROVIDERACCOUNTSTATE
SET 
    LITERAL = 'Verified',
    Description = 'Provider account is verified.'
WHERE 
    ENUMERATIONVALUE = 2;  

INSERT INTO SL_PROVIDERACCOUNTSTATE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (3, 'Inactive', 'Provider account is inactive.');

INSERT INTO SL_PROVIDERACCOUNTSTATE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (4, 'Active', 'Provider account is active.');

INSERT INTO SL_PROVIDERACCOUNTSTATE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (5, 'Failed', 'An error occured during the creation process.');

-- SL_BookingType

UPDATE SL_BOOKINGTYPE
SET 
    LITERAL = 'Created',
    Description = 'Default value on creation.'
WHERE 
    ENUMERATIONVALUE = 1;  

UPDATE SL_BOOKINGTYPE
SET 
    LITERAL = 'Waiting',
    Description = 'External booking created, waiting for confirmation'
WHERE 
    ENUMERATIONVALUE = 2;  

UPDATE SL_BOOKINGTYPE
SET 
    LITERAL = 'Active',
    Description = 'Booking is Active.'
WHERE 
    ENUMERATIONVALUE = 3;

UPDATE SL_BOOKINGTYPE
SET 
    LITERAL = 'Canceled',
    Description = 'Reservation got canceled.'
WHERE 
    ENUMERATIONVALUE = 4;

INSERT INTO SL_BOOKINGTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (5, 'Cleared', 'Booking is cleared.');

INSERT INTO SL_BOOKINGTYPE(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (6, 'Failed', 'An error occured during the booking process.');
 
---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
