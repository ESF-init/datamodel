SPOOL db_3_260.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.260', 'EPA', 'Add JobType BulkTransfer to SL_JOBTYPE');

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (10, 'ProductTransfer', 'Transfers products from stock to cards');

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
