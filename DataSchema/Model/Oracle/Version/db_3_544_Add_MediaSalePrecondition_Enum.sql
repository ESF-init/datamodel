SPOOL db_3_544.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.544', 'SLR', 'Add enumerations to define media sale preconditions and media eligibility requirements.');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_MediaSalePrecondition
(
  EnumerationValue  NUMBER(9)           NOT NULL,
  Literal           NVARCHAR2(50)       NOT NULL,
  Description       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_MediaSalePrecondition PRIMARY KEY (EnumerationValue)
);

COMMENT ON TABLE SL_MediaSalePrecondition IS 'Enumeration values for SL_MediaSalePrecondition. Meet all definied criteria before media sale (AND logic).';
COMMENT ON COLUMN SL_MediaSalePrecondition.EnumerationValue IS 'Type identifier.';
COMMENT ON COLUMN SL_MediaSalePrecondition.Literal IS 'Type literal.';
COMMENT ON COLUMN SL_MediaSalePrecondition.Description IS 'Type description.';


INSERT INTO SL_MediaSalePrecondition (EnumerationValue, Literal, Description) VALUES (0, 'None', 'None');
INSERT INTO SL_MediaSalePrecondition (EnumerationValue, Literal, Description) VALUES (1, 'NameRequired', 'NameRequired');
INSERT INTO SL_MediaSalePrecondition (EnumerationValue, Literal, Description) VALUES (2, 'ExpirationDateRequired', 'ExpirationDateRequired');
INSERT INTO SL_MediaSalePrecondition (EnumerationValue, Literal, Description) VALUES (3, 'NameExpirationDateRequired', 'NameExpirationDateRequired');
INSERT INTO SL_MediaSalePrecondition (EnumerationValue, Literal, Description) VALUES (4, 'PhotoRequired', 'PhotoRequired');
INSERT INTO SL_MediaSalePrecondition (EnumerationValue, Literal, Description) VALUES (5, 'NamePhotoRequired', 'NamePhotoRequired');
INSERT INTO SL_MediaSalePrecondition (EnumerationValue, Literal, Description) VALUES (6, 'ExpirationDatePhotoRequired', 'ExpirationDatePhotoRequired');
INSERT INTO SL_MediaSalePrecondition (EnumerationValue, Literal, Description) VALUES (7, 'NameExpirationDatePhotoRequired', 'NameExpirationDatePhotoRequired');
INSERT INTO SL_MediaSalePrecondition (EnumerationValue, Literal, Description) VALUES (8, 'OrganizationLogoRequired', 'OrganizationLogoRequired');
INSERT INTO SL_MediaSalePrecondition (EnumerationValue, Literal, Description) VALUES (9, 'NameOrganizationLogoRequired', 'NameOrganizationLogoRequired');




CREATE TABLE SL_MediaEligibilityRequirement
(
  EnumerationValue  NUMBER(9)           NOT NULL,
  Literal           NVARCHAR2(50)       NOT NULL,
  Description       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_MediaEligibilityRequirement PRIMARY KEY (EnumerationValue)
);

COMMENT ON TABLE SL_MediaEligibilityRequirement IS 'Enumeration values for SL_MediaEligibilityRequirement. Meet at least one criteria to fulfill media eligibility (OR logic).';
COMMENT ON COLUMN SL_MediaEligibilityRequirement.EnumerationValue IS 'Type identifier.';
COMMENT ON COLUMN SL_MediaEligibilityRequirement.Literal IS 'Type literal.';
COMMENT ON COLUMN SL_MediaEligibilityRequirement.Description IS 'Type description.';


INSERT INTO SL_MediaEligibilityRequirement (EnumerationValue, Literal, Description) VALUES (0, 'None', 'None');
INSERT INTO SL_MediaEligibilityRequirement (EnumerationValue, Literal, Description) VALUES (1, 'ProofOfEligibilityRequired', 'Proof of Eligibility Required (unspecified)');
INSERT INTO SL_MediaEligibilityRequirement (EnumerationValue, Literal, Description) VALUES (2, 'DriverLicenseDocumentRequired', 'Driver License Required');
INSERT INTO SL_MediaEligibilityRequirement (EnumerationValue, Literal, Description) VALUES (4, 'IdentityRequired', 'Identity Required');

-- =======[ Commit ]===============================================================================================
Commit;

PROMPT Done!

SPOOL OFF;
