SPOOL db_3_285.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.285', 'FRD', 'View to get the Settlement result data');


-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW CH_SETTLEMENTRESULTS
(
    SETTLEMENTID,
    CLEARINGRESULTLEVEL,
    CLIENTID,
    DEVICECLASSID,
    TRANSACTIONTYPEID,
    LINEGROUPID,
    LINEID,
    CARDISSUERID,
    DEVICEPAYMENTMETHODID,
    DEBTORID,
    TICKETINTERNALNUMBER,
    GOODSUM,
    CORRECTEDSUM,
    INVALIDSUM,
    QUARANTINEDSUM,
    BADSUM
)
AS
    SELECT 
        C.SETTLEMENTID,
        CR.CLEARINGRESULTLEVEL,
        CR.CLIENTID,
        CR.DEVICECLASSID,
        CR.TRANSACTIONTYPEID,
        CR.LINEGROUPID,
        CR.LINEID,
        CR.CARDISSUERID,
        CR.DEVICEPAYMENTMETHODID,
        CR.DEBTORID,
        CR.TICKETINTERNALNUMBER,
        SUM(CR.GOODAMOUNT) AS "GOODSUM",
        SUM(CR.CORRECTEDAMOUNT) AS "CORRECTEDSUM",
        SUM(CR.VALIDATIONAMOUNT) AS "INVALIDSUM",
        SUM(CR.QUARANTINEDAMOUNT) AS "QUARANTINEDSUM",
        SUM(CR.BADAMOUNT)AS "BADSUM"
    FROM CH_CLEARING  C
    JOIN CH_CLEARINGRESULT CR ON C.CLEARINGID = CR.CLEARINGID
    GROUP BY 
        C.SETTLEMENTID,
        CR.CLEARINGRESULTLEVEL,
        CR.CLIENTID,
        CR.DEVICECLASSID,
        CR.TRANSACTIONTYPEID,
        CR.LINEGROUPID,
        CR.LINEID,
        CR.CARDISSUERID,
        CR.DEVICEPAYMENTMETHODID,
        CR.DEBTORID,
        CR.TICKETINTERNALNUMBER
ORDER BY C.SETTLEMENTID;


COMMENT ON COLUMN CH_SETTLEMENTRESULTS.SETTLEMENTID IS 'Referenece to the bi-monthly settlement results.';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.CLEARINGRESULTLEVEL IS 'Level of the clearing result (references CH_CLEARINGRESULTLEVEL).';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.CLIENTID IS 'Client id for which the values and counts have been summed up (references VARIO_CLIENT).';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.DEVICECLASSID IS 'Device class id for which the values and counts have been summed up (references VARIO_DEVICECLASS).';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.TRANSACTIONTYPEID IS 'Transaction type id for which the values and counts have been summed up (references RM_TRANSACTIONTYPE).';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.LINEGROUPID IS 'Line group id for which the values and counts have been summed up (references TM_LINEGROUP).';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.LINEID IS 'Line id for which the values and counts have been summed up (references VARIO_LINE).';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.CARDISSUERID IS 'Id of the client which issued the card (references VARIO_CLIENT).';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.DEVICEPAYMENTMETHODID IS 'Method of payment (references RM_DEVICEPAYMENTMETHODID).';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.DEBTORID IS 'Id of the debtor having performed the transactions (references DM_DEBTOR).';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.TICKETINTERNALNUMBER IS 'Internal number of the ticket (references TM_TICKET).';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.GOODSUM IS 'Sum value of the OK classified transactions.';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.CORRECTEDSUM IS 'Sum value of the CORRECTED classified transactions.';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.INVALIDSUM IS 'Sum value of the VALIDATION classified transactions.';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.QUARANTINEDSUM IS 'Sum value of the QUARANTINED classified transactions.';
COMMENT ON COLUMN CH_SETTLEMENTRESULTS.BADSUM IS 'Sum value of the BAD classified transactions.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
