SPOOL db_3_438.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.438', 'MEA', 'Add column KEYTYPE to SL_CERTIFICATE');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_CERTIFICATE ADD KEYTYPE NUMBER(1) DEFAULT 0 NOT NULL;

COMMENT ON COLUMN SL_CERTIFICATE.KEYTYPE IS 'KEYTYPE  0: Key is private, 1: Key is public.';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
