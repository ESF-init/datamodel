SPOOL db_3_352.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.352', 'MMB', 'Alter SL_PROMOCODE');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_PROMOCODE DROP CONSTRAINT FK_PROMOCODE_TICKET;

ALTER TABLE SL_PROMOCODE ADD TICKETTYPEID NUMBER(10) NOT NULL;

ALTER TABLE SL_PROMOCODE MODIFY (ORIGIN NUMBER(10));
ALTER TABLE SL_PROMOCODE MODIFY (DESTINATION NUMBER(10));


COMMIT;

PROMPT Done!

SPOOL OFF;