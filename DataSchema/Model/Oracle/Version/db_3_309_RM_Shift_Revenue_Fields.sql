SPOOL db_3_309.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.309', 'ULB', 'Added 2 revenue fields to rm_shift and RM_SHIFTInventory');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE RM_SHIFT ADD (BankCardRevenue NUMBER(10));
ALTER TABLE RM_SHIFT ADD (CustomerCardRevenue NUMBER(10));
COMMENT ON COLUMN RM_SHIFT.BankCardRevenue IS 'Contains the shift amount of valid transactions paid by Bankcard (Girogo, Debitcard,creditcard)';
COMMENT ON COLUMN RM_SHIFT.CustomerCardRevenue IS 'Contains the shift amount of valid transactions paid by customercard';

ALTER TABLE RM_SHIFTInventory ADD (BankCardRevenue NUMBER(10));
ALTER TABLE RM_SHIFTInventory ADD (CustomerCardRevenue NUMBER(10));
COMMENT ON COLUMN RM_SHIFTInventory.BankCardRevenue IS 'Contains the shift amount of valid transactions paid by Bankcard (Girogo, Debitcard,creditcard)';
COMMENT ON COLUMN RM_SHIFTInventory.CustomerCardRevenue IS 'Contains the shift amount of valid transactions paid by customercard';


COMMIT;

PROMPT Done!

SPOOL OFF;
