SPOOL db_3_127.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.127', 'DST', 'Fixed join on line using line no instead of code.');

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW SL_TRANSACTIONJOURNALHISTORY
AS
with
tariff as (
select tarifid tariffid, netid from (
select tarifid, netid, max(version), max(validfrom) from tm_tarif
group by tarifid, netid having tarifid in (select tariffid from tm_tariff_release where 
  releasetype = decode((select 1 from sl_configurationdefinition where name = 'UseTestTariffArchive' and lower(defaultvalue) like 'f%'), 1, 2, 1) /* returns 1 (testrelease) if UseTestTariffArchive is set to true, otherwise 2 (productiverelease) */
  and deviceclassid = 70)
order by max(validfrom) desc, max(version) desc
) where rownum = 1),
trans as (
select * 
from sl_transactionjournal
where
transactionjournalid not in (select cancellationreference from sl_transactionjournal can where cancellationreference is not null) and
transactiontype in (
66, -- Boarding
84, -- Transfer
85, -- Use
99, -- Charge
108, -- Load
261, -- ClientFare
262, -- OpenLoopVirtualCharge
7000, -- Inspection
9001, -- FareMediaSale
9003, -- Refund
9006, -- Adjustment
9007, -- DormancyFee
9009, -- BalanceTransfer
9010, -- ProductTransfer
9012, -- StoredValuePayment
9013 -- StoredValueRefund
))
select 
  trans.transactionid,
  trans.operatorid,
  trans.saleschannelid,
  case when trans.transactiontype = 66 and trans.validto is not null then cast(9011 /* ProductActivation */ as number(9)) else trans.transactiontype end transactiontype,
  trans.devicetime timestamp,
  trans.ticketinternalnumber ticketexternalnumber,
  trans.resulttype result,
  case 
    when trans.resulttype = 0 /* Ok */ 
      then case when trans.transactiontype = 99 /*charge*/ and prod.tickettype = 201 /*pursepretax*/ then cast(null as number(9)) else trans.pursecredit end /* PreTax charge does not get written in Product2, workaround */
    else cast(0 as number(9)) end pursecredit,
  case when trans.resulttype = 0 /* Ok */ 
    then case when trans.transactiontype = 99 /*charge*/ and prod.tickettype = 201 /*pursepretax*/ then cast(null as number(9)) else trans.pursebalance end /* PreTax charge does not get written in Product2, workaround */
    else cast(0 as number(9)) end pursebalance,
  case when trans.resulttype = 0 /* Ok */ 
    then case when trans.transactiontype = 99 /*charge*/ and prod.tickettype = 201 /*pursepretax*/ then trans.pursecredit else trans.pursecredit2 end /* PreTax charge does not get written in Product2, workaround */
    else cast(0 as number(9)) end pursecreditpretax,
  case when trans.resulttype = 0 /* Ok */
    then case when trans.transactiontype = 99 /*charge*/ and prod.tickettype = 201 /*pursepretax*/ then trans.pursebalance else trans.pursebalance2 end
    else cast(0 as number(9)) end pursebalancepretax,
  trans.fareamount price,
  case
    when trans.validfrom is null and trans.validto is null and trans.duration > 0 then trans.devicetime 
    else trans.validfrom end validfrom, /* Transfers don't have ValidFrom and ValidTo set, workaround */
  case 
    when trans.validto is null and trans.validfrom is not null then trans.validfrom+trans.duration/(24*60*60) /* Boardings don't have ValidTo set, workaround */
    when trans.validfrom is null and trans.validto is null and trans.duration > 0 then trans.devicetime+trans.duration/(24*60*60) /* Transfers don't have ValidFrom and ValidTo set, workaround */
    else trans.validto end validto, 
  trans.groupsize numberofpersons,
  stop.stopno,
  stop.stopname,
  line.lineno,
  trans.line linecode,
  line.linename,
  trans.vehiclenumber,
  trans.devicenumber,
  device.name devicename,
  device.description devicedescription,
  cap.cappingname appliedcapping,
  ticket.name ticketname,
  trans.inserttime,
  trans.transitaccountid
from trans
left join tariff on 1=1 -- tariff only contains one row
left join tm_ticket ticket on ticket.internalnumber = trans.ticketinternalnumber and tariff.tariffid = ticket.tarifid
left join vario_stop stop on stop.netid = tariff.netid and stop.stopno = trans.stopnumber
left join vario_line line on line.netid = tariff.netid and line.linecode = trans.line
left join um_device device on device.deviceno = trans.devicenumber
left join sl_product prod on prod.productid = trans.productid
left join sl_cappingjournal cappingjournal on cappingjournal.transactionjournalid = trans.transactionjournalid and cappingjournal.state in ( 3, /* applied */ 2 /* partiallyapplied */ )
left join tm_rule_capping cap on cap.potnumber = cappingjournal.potid and cap.tariffid = tariff.tariffid
;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
