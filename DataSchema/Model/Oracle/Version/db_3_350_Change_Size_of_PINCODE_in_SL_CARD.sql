SPOOL db_3_350.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.350', 'STV', 'Change_Size_of_PINCODE_in_SL_CARD');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CARD MODIFY (PINCODE NVARCHAR2(70));


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
