SPOOL db_3_530.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.530', 'ARH', 'added table PP_CARD_TOKEN_SUBJECT');


-- =======[ New Tables ]===========================================================================================
CREATE TABLE PP_CARD_TOKEN_SUBJECT
(
  CARDTOKENSUBJECTID    NUMBER(10)          NOT NULL,
  CARDID  				NUMBER(10)          NOT NULL,
  SUBJECT              	VARCHAR2(200)       NOT NULL
);

COMMENT ON TABLE  PP_CARD_TOKEN_SUBJECT is 'Lists the token subject id for each virtual card.';
COMMENT ON COLUMN PP_CARD_TOKEN_SUBJECT.CARDTOKENSUBJECTID   IS 'Unique identifier, which serves as primary key';
COMMENT ON COLUMN PP_CARD_TOKEN_SUBJECT.CARDID   IS 'References the CardID from PP_CARD.';
COMMENT ON COLUMN PP_CARD_TOKEN_SUBJECT.SUBJECT   IS 'The subject Id from the Bearer Token.';


ALTER TABLE PP_CARD_TOKEN_SUBJECT ADD (
  CONSTRAINT PP_CARD_TOKEN_SUBJECT_PK
  PRIMARY KEY
  (CARDTOKENSUBJECTID)
  ENABLE VALIDATE);
  

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;

