SPOOL db_3_259.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.259', 'FLF', 'Adding tariffid index to  TM_AREALISTELEMENT');

-- =======[ Changes to Existing Tables ]===========================================================================

CREATE INDEX IDX_AREALISTELEMENT_TARIFF ON TM_AREALISTELEMENT (TARIFFID);

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
