SPOOL db_3_099.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.099', 'SLR', 'New column PriceType in TM_TICKET that refers to enum TM_PRICETYPE.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------
CREATE TABLE TM_PRICETYPE
(
  PRICETYPEID         NUMBER(9)           NOT NULL,
  PRICETYPENAME       VARCHAR2(250 BYTE)  NOT NULL,
  DESCRIPTION               VARCHAR2(400 BYTE)
);

ALTER TABLE TM_PRICETYPE ADD (
   CONSTRAINT TM_PRICETYPE_PK
  PRIMARY KEY
 (PRICETYPEID));
 
COMMENT ON TABLE TM_PRICETYPE IS 'Enumeration table for allowed price type of ticket used in system.';
COMMENT ON COLUMN TM_PRICETYPE.PRICETYPEID IS 'Unique identifier of the current entity.';
COMMENT ON COLUMN TM_PRICETYPE.PRICETYPENAME IS 'Name of the source; used internally.';
COMMENT ON COLUMN TM_PRICETYPE.DESCRIPTION IS 'Descripton of the source. This is used in the UI to display the value.';

ALTER TABLE TM_TICKET 
ADD PRICETYPE Number(9);

COMMENT ON COLUMN TM_TICKET.PRICETYPE IS 'References TM_PRICETYPE.PRICETYPEID.';


-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
