SPOOL db_3_475.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.475', 'SVR', 'Added OwnerClientId to SL_CONFIGURATIONDEFINITION.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CONFIGURATIONDEFINITION ADD OWNERCLIENTID NUMBER(18) DEFAULT 0 NOT NULL; 

COMMENT ON COLUMN SL_CONFIGURATIONDEFINITION.OWNERCLIENTID IS 'Client value can only be changed by a user with the same clientId (ownerClientId == clientId) or if ownerClientId is 0 (default value).';

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
