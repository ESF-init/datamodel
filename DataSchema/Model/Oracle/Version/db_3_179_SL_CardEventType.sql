SPOOL db_3_179.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.179', 'DST', 'Added missing CardEvent types');

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_CARDEVENTTYPE
(
  ENUMERATIONVALUE,
  LITERAL,
  DESCRIPTION
)
VALUES
(
  15,
  N'Revoke',
  N'Revoke'
);

INSERT INTO SL_CARDEVENTTYPE
(
  ENUMERATIONVALUE,
  LITERAL,
  DESCRIPTION
)
VALUES
(
  16,
  N'Deposit',
  N'Deposit'
);

INSERT INTO SL_CARDEVENTTYPE
(
  ENUMERATIONVALUE,
  LITERAL,
  DESCRIPTION
)
VALUES
(
  17,
  N'CardHolder',
  N'CardHolder'
);

INSERT INTO SL_CARDEVENTTYPE
(
  ENUMERATIONVALUE,
  LITERAL,
  DESCRIPTION
)
VALUES
(
  18,
  N'Participant',
  N'Participant'
);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
