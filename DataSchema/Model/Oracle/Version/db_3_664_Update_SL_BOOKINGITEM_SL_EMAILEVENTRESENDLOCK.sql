SPOOL db_3_664.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.664', 'ARH', 'Update SL_BOOKINGITEM and SL_EMAILEVENTRESENDLOCK');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_PRODUCTTYPE
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_PRODUCTTYPE PRIMARY KEY (ENUMERATIONVALUE)
);

COMMENT ON TABLE SL_PRODUCTTYPE IS 'Enumeration of the product type for a booking item (SL_BOOKINGITEM).';
COMMENT ON COLUMN SL_PRODUCTTYPE.ENUMERATIONVALUE IS 'Unique ID of the product type (mapped on SL_BOOKINGITEM.PRODUCTTYPE).';
COMMENT ON COLUMN SL_PRODUCTTYPE.DESCRIPTION IS 'Descripton of the source. This is used in the UI to display the value.';
COMMENT ON COLUMN SL_PRODUCTTYPE.LITERAL IS 'Name of the source; used internally.';


-- -------[ Enumerations ]------------------------------------------------------------------------------------------
INSERT INTO SL_PRODUCTTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (0, 'None','No product type');
INSERT INTO SL_PRODUCTTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (1, 'HomeZone','KVV HomeZone');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_BOOKINGITEM 	ADD PRODUCTTYPE NUMBER(9) DEFAULT 0 NOT NULL;	
COMMENT ON COLUMN SL_BOOKINGITEM.PRODUCTTYPE IS 'The product type of the booking item (mapped on SL_PRODUCTTYPE.ENUMERATIONVALUE).';

ALTER TABLE SL_EMAILEVENTRESENDLOCK ADD BOOKINGITEMID NUMBER(18,0);
ALTER TABLE SL_EMAILEVENTRESENDLOCK ADD CONSTRAINT FK_EMAILEVTRESENDLK_BGITEM FOREIGN KEY (BOOKINGITEMID) REFERENCES SL_BOOKINGITEM (BOOKINGITEMID);
COMMENT ON COLUMN SL_EMAILEVENTRESENDLOCK.BOOKINGITEMID is 'References SL_BOOKINGITEM.BOOKINGITEMID.';

COMMIT;

PROMPT Done!

SPOOL OFF;
