SPOOL db_3_025.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.025', 'STV', 'Added column EndStation for FareEvasionIncident');

-- Start adding schema changes here

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_FAREEVASIONINCIDENT ADD (ENDSTATION  NVARCHAR2(200) NULL);

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
