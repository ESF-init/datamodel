SPOOL db_3_138.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.138', 'SOE', 'Added new column BankConnectionDataID as Foreign key to SL_AddressBookEntry.');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table SL_AddressBookEntry add BankConnectionDataID number(18);
alter table SL_AddressBookEntry add constraint fk_AddressBE_BankConnData foreign key (BankConnectionDataID) references SL_BankConnectionData (BankConnectionDataID);
 
comment on column SL_AddressBookEntry.BankConnectionDataID 
is 'A value returned by BankConnectionDataID to specify the IBAN used for the AddressBookEntryType.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;