SPOOL db_3_574.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.574', 'DIS', 'Add TicketInternalNumber to SL_VOUCHER');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_VOUCHER ADD TICKETINTERNALNUMBER NUMBER(10) NULL;

COMMENT ON COLUMN SL_VOUCHER.TICKETINTERNALNUMBER IS 'Ticket internal number';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
