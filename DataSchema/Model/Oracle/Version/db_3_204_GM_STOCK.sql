SPOOL db_3_204.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.204', 'JGI', 'Added column DEBTORID');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE GM_STOCK
 ADD (DEBTORID  NUMBER(10));
 
 COMMENT ON COLUMN GM_STOCK.DEBTORID IS 'Reference to DM_DEBTOR';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;