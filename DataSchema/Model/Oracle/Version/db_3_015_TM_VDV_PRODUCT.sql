SPOOL db_3_015.log

INSERT INTO vario_dmodellversion (rundate,dmodellno,responsible,annotation)
VALUES (SYSDATE,'3.015','FLF','Added BRTYPE2ID to TM_VDV_PRODUCT');


---Start adding schema changes here

ALTER TABLE TM_VDV_PRODUCT
 ADD (BRTYPE2ID  NUMBER(18));
 
ALTER TABLE TM_VDV_PRODUCT
 ADD (DESCRIPTION  VARCHAR2(300));


---End adding schema changes



COMMIT;

PROMPT Done!

SPOOL OFF