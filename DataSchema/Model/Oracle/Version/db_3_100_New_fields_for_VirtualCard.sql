SPOOL db_3_100.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.100', 'MIW', 'Virtual card for Apple integration');

-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_VIRTUALCARD ADD (DEVICETYPE NUMBER(9));
UPDATE SL_VIRTUALCARD SET DEVICETYPE = 0, TRANSACTIONCOUNTER = TRANSACTIONCOUNTER+1;
ALTER TABLE SL_VIRTUALCARD MODIFY DEVICETYPE DEFAULT 0;
ALTER TABLE SL_VIRTUALCARD ADD (EXTERNALUSERID NVARCHAR2(200));

COMMENT ON COLUMN SL_VIRTUALCARD.DEVICETYPE IS 'Defines the device type the virtual card. Values taken from SL_VIRTUALCARDDEVICETYPE.';
COMMENT ON COLUMN SL_VIRTUALCARD.EXTERNALUSERID IS 'Identifier of the device user.  Used by Apple wallet';

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_VIRTUALCARDDEVICETYPE
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL
);

ALTER TABLE SL_VIRTUALCARDDEVICETYPE ADD CONSTRAINT PK_VIRTUALCARDDEVICETYPE PRIMARY KEY (ENUMERATIONVALUE);

COMMENT ON TABLE SL_VIRTUALCARDDEVICETYPE IS 'This table contains a list of device type for virtual cards.';
COMMENT ON COLUMN SL_VIRTUALCARDDEVICETYPE.EnumerationValue IS 'Unique id of the device type.';
COMMENT ON COLUMN SL_VIRTUALCARDDEVICETYPE.Literal IS 'Name of the device type. Used internally.';
COMMENT ON COLUMN SL_VIRTUALCARDDEVICETYPE.Description IS 'Description of the device type. Used in the UI.';

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE SL_CARDLINK
(
	SOURCECARDID NUMBER(18) NOT NULL,
	TARGETCARDID NUMBER(18) NOT NULL
);

ALTER TABLE SL_CARDLINK ADD CONSTRAINT PK_CARDLINK PRIMARY KEY (TARGETCARDID);
ALTER TABLE SL_CARDLINK ADD CONSTRAINT FK_CARDLINK_CARD_SOURCE FOREIGN KEY (SOURCECARDID) REFERENCES SL_CARD (CARDID);
ALTER TABLE SL_CARDLINK ADD CONSTRAINT FK_CARDLINK_CARD_TARGET FOREIGN KEY (TARGETCARDID) REFERENCES SL_CARD (CARDID);

COMMENT ON COLUMN SL_CARDLINK.SOURCECARDID IS 'Unique identifier of the source card';
COMMENT ON COLUMN SL_CARDLINK.TARGETCARDID IS 'Unique identifier of the target card';

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_VIRTUALCARDDEVICETYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (0, 'Unknown', 'Unknown');
INSERT INTO SL_VIRTUALCARDDEVICETYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (1, 'Phone', 'Phone');
INSERT INTO SL_VIRTUALCARDDEVICETYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (2, 'Watch', 'Watch');
INSERT INTO SL_VIRTUALCARDDEVICETYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (3, 'Tablet', 'Tablet');
INSERT INTO SL_VIRTUALCARDDEVICETYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (99, 'Other', 'Other');

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
