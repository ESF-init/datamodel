SPOOL db_3_598.log;
------------------------------------------------------------------------------
--Version 3.598
------------------------------------------------------------------------------

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.598', 'MEA', 'Change CM_CERTIFICATE.RAWDATA from 2000 to VARCHAR2(4000)');

-- =======[ Changes to Existing Tables ]===========================================================================

DROP TRIGGER CM_CERTIFICATE_BRU;

alter table CM_CERTIFICATE ADD RAWDATA_temp VARCHAR2(4000) default ' ' not null;

alter table   CM_CERTIFICATE rename column   RAWDATA TO   RAWDATA_old;

update CM_CERTIFICATE set CM_CERTIFICATE.RAWDATA_temp = CM_CERTIFICATE.RAWDATA_old;

alter table   CM_CERTIFICATE drop column   RAWDATA_old;  

alter table   CM_CERTIFICATE rename column   RAWDATA_temp TO   RAWDATA;

CREATE OR REPLACE TRIGGER CM_Certificate_BRU before update ON CM_CERTIFICATE for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
