SPOOL db_3_360.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.360', 'DST', 'Add unique index to SL_Sale.SaleTransactionID.');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_sale add constraint UK_Sale_SaleTransactionID UNIQUE (SaleTransactionId);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
