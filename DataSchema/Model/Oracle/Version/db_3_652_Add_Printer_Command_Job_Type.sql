SPOOL db_3_652.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.652', 'EMN', 'Add PrinterCommand to SL_JobType');

-- -------[ Enumerations ]------------------------------------------------------------------------------------------
INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (29, 'PrinterCommand', 'Commands for Printers');

COMMIT;

PROMPT Done!

SPOOL OFF;


select * from sl_jobtype;