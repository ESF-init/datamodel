SPOOL db_3_377.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.377', 'SLR', 'Add column to SL_VerificationAttempt and SL_CustomerAccount.');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_VerificationAttempt ADD (ISLOCKOUTFORCED NUMBER(1)               DEFAULT 0                     NOT NULL);
COMMENT ON COLUMN SL_VERIFICATIONATTEMPT.ISLOCKOUTFORCED IS 'This attempt has generate a temporary account lockout.';

ALTER TABLE SL_CustomerAccount ADD (LOCKOUTUNTIL DATE DEFAULT TO_DATE('01-01-0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL);
COMMENT ON COLUMN SL_CustomerAccount.LOCKOUTUNTIL IS 'Datetime until credential is temporary locked out.';

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
