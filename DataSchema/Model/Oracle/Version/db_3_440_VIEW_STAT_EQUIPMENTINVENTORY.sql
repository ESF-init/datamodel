SPOOL db_3_440.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.440', 'SVR', 'View VIEW_STAT_EQUIPMENTINVENTORY added.');

-- Start adding schema changes here

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW VIEW_STAT_EQUIPMENTINVENTORY
(
    NAME,
    DEVICENO,
    COMPANYNAME,
    UNITNO,
    VISIBLEUNITNO,
    VALIDFROM,
    DEVICECLASSID,
    SHORTNAME,
    DESCRIPTION,
    UNITNAME,
    UNITID,
    CLIENTID
)
AS
    SELECT C.NAME,
           D.DEVICENO,
           NVL (A.COMPANYNAME, 'agency unknown') COMPANYNAME,
           NVL (U.UNITNO, -1)                    UNITNO,
           U.VISIBLEUNITNO,
           H.VALIDFROM,
           C.DEVICECLASSID,
           C.SHORTNAME,
           U.DESCRIPTION,
           U.UNITNAME,
           U.UNITID,
           NVL (A.CLIENTID, -1)                  CLIENTID
      FROM (SELECT *
              FROM UM_DEVICEHISTORY
             WHERE (DEVICEID, DEVICEHISTORYID) IN
                       (  SELECT T1.DEVICEID, MAX (T1.DEVICEHISTORYID)
                            FROM (SELECT ROWNUM RNUM, T.*
                                    FROM (  SELECT *
                                              FROM UM_DEVICEHISTORY
                                          ORDER BY DEVICEID, DEVICEHISTORYID) T)
                                 T1,
                                 (SELECT ROWNUM RNUM, T.*
                                    FROM (  SELECT *
                                              FROM UM_DEVICEHISTORY
                                          ORDER BY DEVICEID, DEVICEHISTORYID) T)
                                 T2
                           WHERE     T1.RNUM = T2.RNUM + 1
                                 AND NOT (    T1.DEVICEID = T2.DEVICEID
                                          AND T1.UNITID = T2.UNITID)
                        GROUP BY T1.DEVICEID)) H,
           UM_DEVICE          D,
           UM_UNIT            U,
           VARIO_DEVICECLASS  C,
           VARIO_CLIENT       A
     WHERE     D.DEVICEID = H.DEVICEID
           AND U.UNITID(+) = H.UNITID
           AND C.DEVICECLASSID = D.DEVICECLASSID
           AND C.TYPEOFUNITID = 3
           AND A.CLIENTID(+) = U.CLIENTID;

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
