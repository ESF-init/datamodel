SPOOL db_3_214.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.214', 'LWU', 'Added table DEBTORTOKEN(Tokens for MDE) to Database.');


-- =======[ Adding new Table ]===========================================================================

CREATE TABLE GM_DEBTORTOKEN
(
  TOKEN         VARCHAR2(255 BYTE)              NOT NULL,
  USERID        NUMBER,
  CREATIONDATE  DATE
);


CREATE UNIQUE INDEX GM_DEBTORTOKEN_PK ON GM_DEBTORTOKEN (TOKEN);

ALTER TABLE GM_DEBTORTOKEN ADD (
  CONSTRAINT GM_DEBTORTOKEN_PK
  PRIMARY KEY
  (TOKEN)
  USING INDEX GM_DEBTORTOKEN_PK
  ENABLE VALIDATE);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;