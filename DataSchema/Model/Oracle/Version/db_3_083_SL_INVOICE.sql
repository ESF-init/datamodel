SPOOL db_3_083.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.083', 'SOE', 'added lastPrintDate SL_INVOICE');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_invoice add lastPrintDate DATE null ;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
