SPOOL dbsl_3_003.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.003', 'AMA', 'CryptoManagement_QArchive');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE CM_CertificateFormat (
    EnumerationValue NUMBER(9,0)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_CertificateFormat PRIMARY KEY (EnumerationValue)
);

CREATE TABLE CM_CertificatePurpose (
    EnumerationValue NUMBER(9,0)   NOT NULL, 
    Literal          NVARCHAR2(50) NOT NULL, 
    Description      NVARCHAR2(50) NOT NULL, 
    CONSTRAINT PK_CertificatePurpose PRIMARY KEY (EnumerationValue)
);

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

CREATE TABLE CM_QArchive
(
    QArchiveID         NUMBER(18,0)                   NOT NULL,
    ClientID           NUMBER(18,0)                   NOT NULL,
    Name               NVARCHAR2(50)                  NOT NULL,
    Description        NVARCHAR2(250)                         ,
    Version			   NUMBER(9)					  NOT NULL,
    ValidFrom          DATE           DEFAULT TO_DATE('31.12.9999 23:59:59', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
    RELEASECOUNTER     NUMBER(9)      DEFAULT 0		  NOT NULL,
    DataStatus         NUMBER(9,0)    DEFAULT 1       NOT NULL,
    LastUser           NVARCHAR2(50)  DEFAULT 'SYS'   NOT NULL,
    LastModified       DATE           DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER                        NOT NULL,
    CONSTRAINT PK_QArchive PRIMARY KEY (QArchiveID),
    CONSTRAINT UK_QArchive_Name UNIQUE (Name, ClientID),
    CONSTRAINT FK_QArchive_ClientID FOREIGN KEY (ClientID) REFERENCES VARIO_CLIENT(ClientID)
);

CREATE TABLE CM_Certificate
(
    CertificateID      NUMBER(18,0)                   NOT NULL,
    QArchiveID         NUMBER(18,0)                   NOT NULL,
    Name               NVARCHAR2(512)                  NOT NULL,
    Description        NVARCHAR2(512)                         ,
	SIGNATURE		   NVARCHAR2(400)			      NOT NULL,
    IssuerName         NVARCHAR2(255)                  NOT NULL,
    SubjectName        NVARCHAR2(255)                  NOT NULL,
    ValidTo			   DATE           DEFAULT TO_DATE('31.12.9999 23:59:59', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
    ValidFrom		   DATE           DEFAULT TO_DATE('01.01.0001 00:00:00', 'DD-MM-YYYY HH24:MI:SS') NOT NULL,
    FileName           NVARCHAR2(512)                 NOT NULL,
    Format             NUMBER(9,0)    DEFAULT 0       NOT NULL,
    Purpose            NUMBER(9,0)    DEFAULT 0       NOT NULL,
    ParentID           NUMBER(18,0)                           ,
    RawData            NVARCHAR2(2000)                NOT NULL,
    LastUser           NVARCHAR2(50)  DEFAULT 'SYS'   NOT NULL,
    LastModified       DATE           DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER                        NOT NULL,
    CONSTRAINT PK_CmCertificate PRIMARY KEY (CertificateID),
    CONSTRAINT FK_CmCertificate_QArchive FOREIGN KEY (QArchiveID) REFERENCES CM_QArchive(QArchiveID),
    CONSTRAINT FK_CmCertificate_ParentID   FOREIGN KEY (ParentID) REFERENCES CM_Certificate(CertificateID) ON DELETE SET NULL
);
CREATE INDEX IDX_CmCertificate_Purpose on CM_Certificate (Purpose);

CREATE TABLE CM_UicCertificate
(
    UicCertificateID   NUMBER(18,0)                  NOT NULL,
    CertificateID      NUMBER(18,0)                  NOT NULL,
    RicsCode           NVARCHAR2(50)                 NOT NULL,
    SignKeyID          NVARCHAR2(50)                 NOT NULL,
	IsTrusted		   NUMBER(1)					 NOT NULL,
    LastUser           NVARCHAR2(50) DEFAULT 'SYS'   NOT NULL,
    LastModified       DATE          DEFAULT sysdate NOT NULL,
    TransactionCounter INTEGER                       NOT NULL,
    CONSTRAINT PK_UicCertificate PRIMARY KEY (UicCertificateID),
    CONSTRAINT FK_UicCertificate_Certificate FOREIGN KEY (CertificateID) REFERENCES CM_Certificate(CertificateID)
);

CREATE TABLE CM_SamSignCertificate
(
    SamSignCertificateID NUMBER(18,0)                  NOT NULL,
    CertificateID        NUMBER(18,0)                  NOT NULL,
    SamID                NVARCHAR2(50)                 NOT NULL,
    LastUser             NVARCHAR2(50) DEFAULT 'SYS'   NOT NULL,
    LastModified         DATE          DEFAULT sysdate NOT NULL,
    TransactionCounter   INTEGER                       NOT NULL,
    CONSTRAINT PK_SamSignCertificate PRIMARY KEY (SamSignCertificateID),
    CONSTRAINT FK_SamSignCert_Certificate FOREIGN KEY (CertificateID) REFERENCES CM_Certificate(CertificateID)
);

CREATE TABLE CM_OperatorActivationKey
(
    OperatorActivationKeyID NUMBER(18,0)                  NOT NULL,
    CertificateID			NUMBER(18,0)                  NOT NULL,
    SamOperatorID           NVARCHAR2(50)                 NOT NULL,
    KeyInfo					NVARCHAR2(2000)                NOT NULL,
    LastUser                NVARCHAR2(50) DEFAULT 'SYS'   NOT NULL,
    LastModified            DATE          DEFAULT sysdate NOT NULL,
    TransactionCounter      INTEGER                       NOT NULL,
    CONSTRAINT PK_OperatorActivationKey PRIMARY KEY (OperatorActivationKeyID),
    CONSTRAINT FK_OpActivationKey_Certificate FOREIGN KEY (CertificateID) REFERENCES CM_Certificate(CertificateID)
);

ALTER TABLE Vario_DeviceClass 
Add CryptoReleaseVisibility Number(1) DEFAULT 0  NOT NULL;

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(5) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'CM_QArchive', 'CM_Certificate', 'CM_UicCertificate', 'CM_SamSignCertificate', 'CM_OperatorActivationKey');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(5) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'CM_QArchive', 'CM_Certificate', 'CM_UicCertificate', 'CM_SamSignCertificate', 'CM_OperatorActivationKey');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
    INTO CM_CertificateFormat (EnumerationValue, Literal, Description) VALUES (0, 'X509', 'X.509')
    INTO CM_CertificateFormat (EnumerationValue, Literal, Description) VALUES (1, 'CV', 'CV')
SELECT * FROM DUAL;

INSERT ALL
    INTO CM_CertificatePurpose (EnumerationValue, Literal, Description) VALUES (0, 'VdvkaRoot', 'Vdvka Root')
    INTO CM_CertificatePurpose (EnumerationValue, Literal, Description) VALUES (1, 'VdvkaSubCa', 'Vdvka SubCa')
    INTO CM_CertificatePurpose (EnumerationValue, Literal, Description) VALUES (2, 'VdvkaSamSign', 'Vdvka SAM Signature')
    INTO CM_CertificatePurpose (EnumerationValue, Literal, Description) VALUES (3, 'UicSelfSign', 'UIC self signing')
    INTO CM_CertificatePurpose (EnumerationValue, Literal, Description) VALUES (4, 'OperatorActivationKey', 'Operator Activation Key')
SELECT * FROM DUAL;

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!
SPOOL off
