SPOOL db_3_308.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.308', 'STV', 'Add Column IsActive_to_VarioLine');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE VARIO_LINE ADD ISACTIVE NUMBER(1) DEFAULT 1;

COMMIT;

PROMPT Done!

SPOOL OFF;
