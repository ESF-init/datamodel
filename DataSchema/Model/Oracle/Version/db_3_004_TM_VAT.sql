SPOOL db_3_004.log

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.004', 'FLF', 'Added col VAT to TM_FARETABLEENTRY');

-- Start adding schema changes here

ALTER TABLE TM_FARETABLEENTRY
 ADD (VAT  NUMBER(10,3));

---End adding schema changes 


COMMIT;

PROMPT Done!

SPOOL OFF
