SPOOL db_3_036.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.036', 'DST', 'Added ClientID to SL_StorageLocation');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_storagelocation add clientid number(18);
alter table sl_storagelocation add constraint fk_storagelocation_client foreign key (clientid) references vario_client (clientid);
comment on column sl_storagelocation.clientid is 'Reference to operator';

-- =======[ New Tables ]===========================================================================================

--COMMENT ON TABLE XXX is '';

--COMMENT ON COLUMN XXX.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
--COMMENT ON COLUMN XXX.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
--COMMENT ON COLUMN XXX.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
