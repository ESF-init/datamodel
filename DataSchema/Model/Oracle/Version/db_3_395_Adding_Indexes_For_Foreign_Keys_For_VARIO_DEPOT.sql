SPOOL db_3_395.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.395', 'TIR', 'Adding_Indexes_For_Foreign_Keys_For_VARIO_DEPOT');

-- =======[ New Tables ]===========================================================================================

CREATE INDEX SAM_ADDTRANS_INDEX_VARIO_DEPOT ON SAM_ADDTRANSACTIONINFO(DEPOTID);
CREATE INDEX SAM_INVOICE_INDEX_VARIO_DEPOT ON SAM_INVOICE(DEPOTID);
CREATE INDEX SAM_INVENTRY_INDEX_VARIO_DEPOT ON SAM_INVOICEENTRY(DEPOTID);
CREATE INDEX SAM_INVOIC_INDEX_VARIO_DEPOT ON SAM_INVOICING(DEPOTID);
CREATE INDEX UM_UNIT_INDEX_VARIO_DEPOT ON UM_UNIT(DEPOTID);
CREATE INDEX USER_LIST_INDEX_VARIO_DEPOT ON USER_LIST(DEPOTID);

-- =======[ Sequences ]============================================================================================

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
