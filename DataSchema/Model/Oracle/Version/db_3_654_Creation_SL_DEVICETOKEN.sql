SPOOL db_3_654.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.654', 'SOE', 'Created new table SL_DEVICETOKEN, new enum SL_DEVICETOKENSTATE and added new column in SL_CUSTOMERACCOUNTNOTIFICATION.');

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_DEVICETOKENSTATE
(
  ENUMERATIONVALUE  NUMBER(9)           NOT NULL,
  LITERAL           NVARCHAR2(50)       NOT NULL,
  DESCRIPTION       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_DEVICETOKENSTATE PRIMARY KEY (ENUMERATIONVALUE)
);

COMMENT ON TABLE SL_DEVICETOKENSTATE IS 'Enumeration of device token state.';
COMMENT ON COLUMN SL_DEVICETOKENSTATE.ENUMERATIONVALUE IS 'Unique ID of the device token state.';
COMMENT ON COLUMN SL_DEVICETOKENSTATE.DESCRIPTION IS 'Descripton of the source. This is used in the UI to display the value.';
COMMENT ON COLUMN SL_DEVICETOKENSTATE.LITERAL IS 'Name of the source; used internally.';

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_DEVICETOKEN (
    DEVICETOKENID NUMBER(18, 0) NOT NULL,
    DEVICETOKEN VARCHAR2(255) NOT NULL,
    EXPIRATIONDATE DATE DEFAULT TO_TIMESTAMP('31.12.9999 23:59:59', 'DD.MM.YYYY HH24:MI:SS') NOT NULL,
	DEVICETOKENSTATE NUMBER(9) DEFAULT 0 NOT NULL,
	TRANSACTIONCOUNTER  INTEGER     				NOT NULL,
    LASTUSER            NVARCHAR2(50)   			DEFAULT 'SYS'     NOT NULL,
    LASTMODIFIED        DATE            			DEFAULT sysdate   NOT NULL,
    CONSTRAINT PK_DEVICETOKEN PRIMARY KEY (DEVICETOKENID)
);

COMMENT ON TABLE SL_DEVICETOKEN IS 'Contains the information of device token';
COMMENT ON COLUMN SL_DEVICETOKEN.DEVICETOKENID IS 'The ID of the device token';
COMMENT ON COLUMN SL_DEVICETOKEN.DEVICETOKEN IS 'Device token';
COMMENT ON COLUMN SL_DEVICETOKEN.EXPIRATIONDATE IS 'Expiration date of the token.';
COMMENT ON COLUMN SL_DEVICETOKEN.DEVICETOKENSTATE IS 'Reference to enumeration SL_DEVICETOKENSTATE.';
COMMENT ON COLUMN SL_DEVICETOKEN.LASTUSER IS 'Technical field: last (system) user that changed this dataset.';
COMMENT ON COLUMN SL_DEVICETOKEN.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset.';
COMMENT ON COLUMN SL_DEVICETOKEN.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset.';

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
     'SL_DEVICETOKEN');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_DEVICETOKEN');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CUSTOMERACCOUNTNOTIFICATION  ADD (DEVICETOKENID NUMBER(18));

ALTER TABLE SL_CUSTOMERACCOUNTNOTIFICATION ADD (
CONSTRAINT FK_CUSTOMERACCNOT_DEVICETOKEN 
 FOREIGN KEY (DEVICETOKENID) 
 REFERENCES SL_DEVICETOKEN (DEVICETOKENID));

COMMENT ON COLUMN SL_CUSTOMERACCOUNTNOTIFICATION.DEVICETOKENID IS 'Reference to SL_DEVICETOKEN.';
COMMENT ON COLUMN SL_CUSTOMERACCOUNTNOTIFICATION.DEVICETOKEN IS 'OBSOLETE';

COMMIT;

PROMPT Done!

SPOOL OFF;
