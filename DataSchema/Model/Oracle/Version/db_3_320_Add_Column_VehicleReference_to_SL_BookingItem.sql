SPOOL db_3_320.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.320', 'ARH', 'Add Column VehicleReference to SL_BookingItem');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_BOOKINGITEM ADD VEHICLEREFERENCE NVARCHAR2(50) ;

COMMENT ON COLUMN SL_BOOKINGITEM.VEHICLEREFERENCE IS 'Reference of the vehicle (bike, car, etc... ) at the mobility provider.';

COMMIT;

PROMPT Done!

SPOOL OFF;
