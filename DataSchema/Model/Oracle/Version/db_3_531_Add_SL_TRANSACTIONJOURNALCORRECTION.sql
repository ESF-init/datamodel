SPOOL db_3_531.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.531', 'STV', 'Add_SL_TRANSJOURNALCORRECTION');


-- =======[ New Tables ]===========================================================================================

CREATE OR REPLACE DIRECTORY 
SPXTRX_BACKUP_DIR AS 
'D:\initdata\MOBILE\ORA\externaltables\spx\backup';

CREATE OR REPLACE DIRECTORY 
SPXTRX_DIR AS 
'D:\initdata\MOBILE\ORA\externaltables\spx';

CREATE TABLE SPX_FILES
(
  FILE_NAME  VARCHAR2(255 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY SPXTRX_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE
       LOAD WHEN file_name != 'DIR'
       PREPROCESSOR SPXTRX_DIR: 'list_files.bat'
       FIELDS TERMINATED BY WHITESPACE
       )
     LOCATION (SPXTRX_DIR:'files.txt')
  )
REJECT LIMIT UNLIMITED
NOPARALLEL
NOMONITORING;

CREATE TABLE SPX_FILE_STATS
(
  FILESTATSID   NUMBER(10),
  CREATIONDATE  DATE                            DEFAULT sysdate,
  FILENAME      VARCHAR2(1000 BYTE),
  DESCRIPTION   VARCHAR2(1000 BYTE),
  IMPORTSTATE   NUMBER(1)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

CREATE TABLE SPX_LOG
(
  LOGID         NUMBER(10),
  CREATIONDATE  DATE                            DEFAULT sysdate,
  LOGTEXT       VARCHAR2(500 BYTE)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

CREATE TABLE SPX_TRANSACTIONS
(
  TYPE             VARCHAR2(300 BYTE),
  DATETIME         DATE,
  BUS              VARCHAR2(50 BYTE),
  ROUTE            VARCHAR2(50 BYTE),
  RUN              VARCHAR2(50 BYTE),
  TRIP             VARCHAR2(50 BYTE),
  DRIVER           VARCHAR2(50 BYTE),
  FARESET          VARCHAR2(50 BYTE),
  DIRECTION        VARCHAR2(50 BYTE),
  VALUE            VARCHAR2(50 BYTE),
  DESCRIPTION      VARCHAR2(300 BYTE),
  AMOUNT           VARCHAR2(50 BYTE),
  LONGITUDE        VARCHAR2(50 BYTE),
  LATITUDE         VARCHAR2(50 BYTE),
  STOP             VARCHAR2(50 BYTE),
  SPXTRANSID       NUMBER(10)                   NOT NULL,
  CREATIONDATE     DATE                         DEFAULT sysdate,
  EXTERNALTRANSID  NUMBER(20),
  PROBETIME        DATE,
  TTP              NUMBER(10),
  TPBC             NUMBER(10),
  CARD_ID          VARCHAR2(50)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE SPX_TRANSACTIONS_BUCKET
(
  TRANSACTIONID    NUMBER(20),
  PROBETIME        VARCHAR2(60 BYTE),
  TRANSDATE        VARCHAR2(10 BYTE),
  TRANSTIME        VARCHAR2(8 BYTE),
  FARESET          NUMBER(10),
  DRIVER           NUMBER(10),
  BUS              VARCHAR2(60 BYTE),
  ROUTE            VARCHAR2(50 BYTE),
  RUN              VARCHAR2(70 BYTE),
  LATITUDE         VARCHAR2(50 BYTE),
  LONGITUDE        VARCHAR2(50 BYTE),
  STOP             VARCHAR2(70 BYTE),
  DIRECTION        VARCHAR2(60 BYTE),
  TRIP             VARCHAR2(60 BYTE),
  TRANSACTIONTYPE  NUMBER(10),
  CASH             VARCHAR2(60 BYTE),
  TTP              VARCHAR2(60),
  KEYS             VARCHAR2(60),
  TPBC             VARCHAR2(60),
  CARD_ID          VARCHAR2(60)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY SPXTRX_DIR
     ACCESS PARAMETERS 
       ( RECORDS delimited BY NEWLINE
        SKIP 1
        FIELDS TERMINATED BY ','
        OPTIONALLY ENCLOSED BY '"'
        LRTRIM
        MISSING FIELD VALUES ARE NULL 
        (TRANSACTIONID,
        PROBETIME CHAR(19) DATE_FORMAT DATE MASK "YYYY-MM-DD HH24:MI:SS",
        TRANSDATE CHAR(10) DATE_FORMAT DATE MASK "YYYY-MM-DD",
        TRANSTIME CHAR(8) DATE_FORMAT TIME MASK "HH24:MI:SS",
        FARESET,
        DRIVER,
        BUS,
        ROUTE,
        RUN,
        LATITUDE,
        LONGITUDE,
        STOP,
        DIRECTION,
        TRIP,
        TRANSACTIONTYPE,
        CASH,
        TTP,
        KEYS,
        TPBC,
        CARD_ID)      
        )
     LOCATION (SPXTRX_DIR:'Transaction_detail20200903T000000_20200910T000000_20200910T060201020.csv')
  )
REJECT LIMIT 0
NOPARALLEL
NOMONITORING;

CREATE TABLE SPX_TRANSACTION_TYPE
(
  TRANSACTIONTYPENUMBER  NUMBER(10),
  DESCRIPTION            VARCHAR2(100 BYTE)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX SPX_LOG_PK ON SPX_LOG
(LOGID)
LOGGING
NOPARALLEL;

CREATE  UNIQUE INDEX SPX_TRANSACTIONS_PK ON SPX_TRANSACTIONS
(SPXTRANSID)
LOGGING
NOPARALLEL;

ALTER TABLE SPX_LOG ADD (
  CONSTRAINT SPX_LOG_PK
  PRIMARY KEY
  (LOGID)
  USING INDEX SPX_LOG_PK
  ENABLE VALIDATE);

ALTER TABLE SPX_TRANSACTIONS ADD (
  CONSTRAINT SPX_TRANSACTIONS_PK
  PRIMARY KEY
  (SPXTRANSID)
  USING INDEX SPX_TRANSACTIONS_PK
  ENABLE VALIDATE);


CREATE OR REPLACE PROCEDURE p_SPX_BACKUPFILE
(
   p_FileName IN VARCHAR2,
   p_DIRECTORY IN VARCHAR2,
   p_BACKUPDIRECTORY IN VARCHAR2,
   p_MOVEFILE IN BOOLEAN,
   p_RAISEEXCEPTION IN BOOLEAN DEFAULT FALSE
)
IS
   
    l_NewFileName VARCHAR2(300);
    
BEGIN

 l_NewFileName := TO_CHAR(sysdate, 'DDMMYYYY.HHMISS') || '_' || p_FileName;

 UTL_FILE.fCopy (p_DIRECTORY,
                 UPPER(p_FileName),
                 p_BACKUPDIRECTORY,
                 UPPER(l_NewFileName));
 IF p_MOVEFILE 
 THEN               
   UTL_FILE.FREMOVE (p_DIRECTORY,UPPER(p_FileName));
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN 
     IF p_RAISEEXCEPTION
     THEN
       RAISE;
     ELSE
      RETURN;
     END IF;
END p_SPX_BACKUPFILE;
/


CREATE OR REPLACE PROCEDURE p_SPX_INSERTTRANSACTIONS
IS
 
 TYPE TransBucketCursorType IS REF CURSOR;  -- define weak REF CURSOR type
 c1   TransBucketCursorType;  -- declare cursor variable
 r    SPX_TRANSACTIONS_BUCKET%ROWTYPE;
                 
 --CURSOR c2 (p_datetime IN VARCHAR2, p_driver IN VARCHAR2)
 -- IS
 --   (SELECT driver
 --   FROM SPX_TRANSACTIONS
 --   WHERE datetime = p_datetime
 --     AND driver = p_driver
 --   );
 
 CURSOR c2 (p_transid IN NUMBER)
   IS
     (SELECT EXTERNALTRANSID
        FROM SPX_TRANSACTIONS
       WHERE EXTERNALTRANSID = p_transid
     );
    
 CURSOR c3 
  IS
    (SELECT file_name
      FROM SPX_FILES WHERE FILE_NAME NOT IN (SELECT FILENAME FROM SPX_FILE_STATS)
    );
    
 l_sqlStaement VARCHAR2(1000);
 l_text VARCHAR2(500);
 tmp_transaction NUMBER;
 l_ClientFile VARCHAR2(1000);
 l_transDateTime DATE;
 l_probeTime DATE;
  
BEGIN
  
  l_text := 'Import started at: ' || sysdate;
  INSERT INTO SPX_LOG (LOGID,LOGTEXT) VALUES (getDataId(),l_text);   
  
  FOR r3 in c3
  LOOP
    l_ClientFile := r3.file_name; 
    l_text := 'Import table: ' || l_ClientFile;
    INSERT INTO SPX_LOG (LOGID,LOGTEXT) VALUES (getDataId(),l_text);
    COMMIT;   
    EXECUTE IMMEDIATE 'ALTER TABLE SPX_TRANSACTIONS_BUCKET LOCATION (SPXTRX_DIR:''' || l_ClientFile || ''')'; 
    l_sqlStaement := 'SELECT TRANSACTIONID,PROBETIME,TRANSDATE,TRANSTIME,FARESET,DRIVER,BUS,ROUTE,RUN,LATITUDE,LONGITUDE,STOP,DIRECTION,TRIP,TRANSACTIONTYPE,CASH,TTP,KEYS,TPBC,CARD_ID FROM SPX_TRANSACTIONS_BUCKET';
    OPEN c1 FOR l_sqlStaement;
       
     -- Fetch rows from result set one at a time:
    LOOP
    
      FETCH c1 INTO r;
    
     tmp_transaction := -1;
      FOR r2 in c2 (r.transactionid)
      LOOP
        tmp_transaction := r2.EXTERNALTRANSID;
      END LOOP;
     
     --INSERT INTO SPX_LOG (LOGID,LOGTEXT) VALUES (getDataId(),'HIER2');
     -- COMMIT;
    -------------------------------
    -- Test for duplicates disabled
    --tmp_driver := -1;
    -------------------------------
    IF ( tmp_transaction < 0 ) 
    THEN 
      --l_transDateTime := TO_DATE(TO_CHAR(r.TRANSDATETIME || ' ' || r.TRANSTIME), 'YYY-MM-DD HH24:MI:SS' ); 
      l_transDateTime := TO_DATE(r.TRANSDATE || ' ' || r.transtime, 'YYYY-MM-DD HH24:MI:SS');
      l_probeTime := TO_DATE(r.PROBETIME, 'YYYY-MM-DD HH24:MI:SS');
      INSERT INTO SPX_LOG (LOGID,LOGTEXT) VALUES (getDataId(),r.TRANSDATE || ' ' || r.transtime);
      COMMIT;
      INSERT INTO SPX_TRANSACTIONS
               (TYPE,DATETIME,BUS,ROUTE,RUN,TRIP,DRIVER,FARESET,DIRECTION,
                LONGITUDE,LATITUDE,STOP,SPXTRANSID,EXTERNALTRANSID,TTP,TPBC,CARD_ID,PROBETIME
               )
          VALUES (r.TRANSACTIONTYPE,l_transDateTime,r.BUS,r.ROUTE,r.RUN,r.TRIP,r.DRIVER,r.FARESET,r.DIRECTION,
                r.LONGITUDE,r.LATITUDE,r.STOP,getDataID(),r.TRANSACTIONID,r.TTP,r.TPBC,r.CARD_ID,l_probeTime);
    ELSE   
        l_text := '(EXTERNALTRANSID) (' || r.transactionid || ') already exists';
        INSERT INTO SPX_LOG (LOGID,LOGTEXT) VALUES (getDataId(),l_text);
        COMMIT;
    END IF;
    
    EXIT WHEN c1%NOTFOUND;
  END LOOP;
  
   -- Close cursor:
  CLOSE c1;
  l_text := 'Import of file: ' || l_ClientFile || ' finished';
  INSERT INTO SPX_LOG (LOGID,LOGTEXT) VALUES (getDataId(),l_text);
  -- Put entry into table
  INSERT INTO SPX_FILE_STATS (FILESTATSID,CREATIONDATE,FILENAME,DESCRIPTION,IMPORTSTATE) VALUES (getDataId(), sysdate, l_ClientFile, 'SUCCESS', 1);
  COMMIT;
  END LOOP;
  
  -- MOVE FILE IN BACKUPDIR
  p_SPX_BACKUPFILE(l_ClientFile, 'SPXTRX_DIR', 'SPXTRX_BACKUP_DIR', TRUE, FALSE);
  
  l_text := 'Import finished at: ' || sysdate;
  INSERT INTO SPX_LOG (LOGID,LOGTEXT) VALUES (getDataId(),l_text);
  COMMIT;
  EXCEPTION
       WHEN NO_DATA_FOUND THEN
           l_text := 'Import finished at: ' || sysdate;
           INSERT INTO SPX_LOG (LOGID,LOGTEXT) VALUES (getDataId(),l_text);
           COMMIT;  
       WHEN OTHERS THEN 
           RAISE;
           --INSERT INTO SPX_FILE_STATS (FILESTATSID,CREATIONDATE,FILENAME,DESCRIPTION,IMPORTSTATE) VALUES (getDataId(), sysdate, l_ClientFile, 'SUCCESS', 0);
           --COMMIT;
END p_SPX_INSERTTRANSACTIONS;
/


 CREATE TABLE SL_TRANSJOURNALCORRECTION
(
  TRANSJOURNALCORRECTIONID		        NUMBER(18,0)                	NOT NULL,
  TRANSACTIONJOURNALID    				NUMBER(18,0)                	NULL,
  SPXTRANSID 							NUMBER(10,0)                	NULL,
  ROUTENUMBER 							NUMBER(18,0)                	NULL,
  BLOCKNUMBER 							NUMBER(18,0)                	NULL,
  LASTUSER               				NVARCHAR2(50) DEFAULT 'SYS' 	NOT NULL,
  LASTMODIFIED           				DATE          DEFAULT sysdate 	NOT NULL,
  TRANSACTIONCOUNTER     				INTEGER                       	NOT NULL,
  CONSTRAINT PK_TRANSJOURCORRECTIONID PRIMARY KEY (TRANSJOURNALCORRECTIONID),
  CONSTRAINT FK_TRANSACTIONJOURNALID FOREIGN KEY (TRANSACTIONJOURNALID) REFERENCES SL_TRANSACTIONJOURNAL(TRANSACTIONJOURNALID),
  CONSTRAINT FK_SPXTRANSID FOREIGN KEY (SPXTRANSID) REFERENCES SPX_TRANSACTIONS(SPXTRANSID)
);

  COMMENT ON TABLE SL_TRANSJOURNALCORRECTION is 'Table for storing corrections to the Route numbers and block numbers  for a transaction';
  COMMENT ON COLUMN SL_TRANSJOURNALCORRECTION.TRANSACTIONJOURNALID IS 'References the Transaction in the transaction journal';  
  COMMENT ON COLUMN SL_TRANSJOURNALCORRECTION.SPXTRANSID IS 'References the Transaction in the spx transaction journal';
  COMMENT ON COLUMN SL_TRANSJOURNALCORRECTION.ROUTENUMBER IS 'The corrected rout number';
  COMMENT ON COLUMN SL_TRANSJOURNALCORRECTION.BLOCKNUMBER IS 'The corrected rout number';
  COMMENT ON COLUMN SL_TRANSJOURNALCORRECTION.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
  COMMENT ON COLUMN SL_TRANSJOURNALCORRECTION.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
  COMMENT ON COLUMN SL_TRANSJOURNALCORRECTION.LASTUSER IS 'Technical field: last (system) user that changed this dataset';


-- =======[ Sequences ]==============================================================================================

create sequence SL_TRANSJOURNALCORRECTION_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================

CREATE OR REPLACE TRIGGER SL_TRANSJOURNALCORRECTION_BRI before insert on SL_TRANSJOURNALCORRECTION for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_TRANSJOURNALCORRECTION_BRU before update on SL_TRANSJOURNALCORRECTION for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

COMMIT;

PROMPT Done!

SPOOL OFF;
