SPOOL db_3_435.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.435', 'MFA', 'Add column ISTRUSTED to SL_ORGANIZATION');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_ORGANIZATION ADD ISTRUSTED NUMBER(1) DEFAULT 1 NOT NULL;

COMMENT ON COLUMN SL_ORGANIZATION.ISTRUSTED IS 'Indicates trust status of organization in relation to fulfilling invoices. If untrusted an invoice must be paid before the sale can be completed.';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
