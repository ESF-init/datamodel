SPOOL db_3_585.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.585', 'MFA', 'Add columns to ACC_REVENUERECOGNITION to facilitate apportionment');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE ACC_REVENUERECOGNITION ADD TRANSITACCOUNTID NUMBER(18) NULL;
COMMENT ON COLUMN ACC_REVENUERECOGNITION.TRANSITACCOUNTID IS 'TransitAccount ID pulled from SL_TransactionJournal matches to SL_CARD.CARDID';

ALTER TABLE ACC_REVENUERECOGNITION ADD TICKETID NUMBER(18) NULL;
COMMENT ON COLUMN ACC_REVENUERECOGNITION.TICKETID IS 'Ticket ID pulled from SL_TransactionJournal matches to TM_TICKET.TICKETID';

ALTER TABLE ACC_REVENUERECOGNITION ADD APPLIEDPASSTICKETID NUMBER(18) NULL;
COMMENT ON COLUMN ACC_REVENUERECOGNITION.APPLIEDPASSTICKETID IS 'AppliedPassTicket ID pulled from SL_TransactionJournal matches to TM_TICKET.TICKETID. Pass which was applied to achieve a trip.';

ALTER TABLE ACC_REVENUERECOGNITION ADD BOARDINGGUID NVARCHAR2(40) NULL;
COMMENT ON COLUMN ACC_REVENUERECOGNITION.BOARDINGGUID IS 'Boarding guid pulled from SL_TransactionJournal unique identifier to separate taps into trips.';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
