SPOOL db_3_235.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.235', 'STV', 'Added EAV to SL_PRODUCTRELATION.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PRODUCTRELATION 
ADD EAV NVARCHAR2 (20);

-- =======[ Commit ]===============================================================================================

COMMENT ON COLUMN SL_PRODUCTRELATION.EAV IS 'EAV Code from the Relation Variant';
COMMIT;

PROMPT Done!

SPOOL OFF;