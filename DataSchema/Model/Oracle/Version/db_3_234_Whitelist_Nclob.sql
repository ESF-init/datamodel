SPOOL db_3_234.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.234', 'DST', 'Modified SL_Whitelist and SL_WhitelistJournal ByteRepresentation to NClob.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TRIGGER SL_WHITELIST_BRU disable;
ALTER TABLE sl_whitelist ADD (BYTEREPCLOB  NCLOB);
UPDATE sl_whitelist SET BYTEREPCLOB=BYTEREPRESENTATION;
ALTER TABLE sl_whitelist DROP COLUMN BYTEREPRESENTATION;
ALTER TABLE sl_whitelist RENAME COLUMN BYTEREPCLOB TO BYTEREPRESENTATION;
ALTER TRIGGER SL_WHITELIST_BRU enable;
COMMENT ON COLUMN SL_WHITELIST.BYTEREPRESENTATION is 'Contains the JSON data.';


ALTER TABLE sl_whitelistjournal ADD (BYTEREPCLOB  NCLOB);
UPDATE sl_whitelistjournal SET BYTEREPCLOB=BYTEREPRESENTATION;
ALTER TABLE sl_whitelistjournal DROP COLUMN BYTEREPRESENTATION;
ALTER TABLE sl_whitelistjournal RENAME COLUMN BYTEREPCLOB TO BYTEREPRESENTATION;
COMMENT ON COLUMN SL_WHITELISTJOURNAL.BYTEREPRESENTATION is 'Contains the JSON data.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
