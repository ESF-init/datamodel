SPOOL db_3_657.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.657', 'ESF', 'Add Expired to SL_TICKETASSIGNMENTSTATUS');

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------
INSERT INTO SL_TICKETASSIGNMENTSTATUS (ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (5, 'Expired', 'Ticket assignment is expired');

COMMIT;

PROMPT Done!

SPOOL OFF;