SPOOL db_3_291.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.291', 'PCO', 'Clearing House Validation Table for Bad Cards');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New SL Tables ]========================================================================================
CREATE TABLE SL_BADCARD
(
  BADCARDID     				NUMBER(10)                NOT NULL,
  FIRSTBADTRANSACTIONNUMBER     NUMBER(10)                NOT NULL,
  CREATEDATE          			DATE                      DEFAULT sysdate,
  LASTUSER						NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
  LASTMODIFIED					DATE			DEFAULT sysdate		NOT NULL,
  TRANSACTIONCOUNTER			INTEGER								NOT NULL,
  DATAROWCREATIONDATE			DATE			DEFAULT sysdate
);

--comments
COMMENT ON TABLE SL_BADCARD IS 'Contains the Ids of Bad Cards, which failed a card balance correctness check. Used for Clearing House.';

COMMENT ON COLUMN SL_BADCARD.BADCARDID IS 'Unique id of the bad card.';
COMMENT ON COLUMN SL_BADCARD.FIRSTBADTRANSACTIONNUMBER IS 'The transaction id of the initial bad transaction.';
COMMENT ON COLUMN SL_BADCARD.CREATEDATE IS 'Date when the card was marked as bad.';
COMMENT ON COLUMN SL_BADCARD.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_BADCARD.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_BADCARD.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_BADCARD.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';


--keys
ALTER TABLE SL_BADCARD ADD (CONSTRAINT PK_BADCARD PRIMARY KEY (BADCARDID));

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(5) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_BADCARD');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(5) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_BADCARD');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/
-- =======[ Data ]==================================================================================================
-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL off; 
