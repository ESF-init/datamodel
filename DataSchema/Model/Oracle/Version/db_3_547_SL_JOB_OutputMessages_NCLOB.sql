SPOOL db_3_547.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.547', 'LJU', 'SL_JOB add column OutputMessages');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE sl_Job
    ADD OutputMessages NCLOB;

COMMENT ON COLUMN sl_Job.OutputMessages IS 'Any messages that the job generates';


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;