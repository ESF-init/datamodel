SPOOL db_3_557.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.557', 'SOE', 'Add column EnumerationValue to SL_EmailEvent');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_EMAILEVENT ADD (ENUMERATIONVALUE NUMBER(9) DEFAULT 0 NOT NULL);

CREATE INDEX IDX_EMAILEVENT_ENUMERATION ON SL_EMAILEVENT(ENUMERATIONVALUE);

COMMENT ON COLUMN SL_EMAILEVENT.ENUMERATIONVALUE IS 'Enumeration value of the corresponding enumeration.';

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

UPDATE SL_Emailevent SET EnumerationValue = 1, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AccountConfirmation' ;
UPDATE SL_Emailevent SET EnumerationValue = 2, transactioncounter = transactioncounter + 1 WHERE Eventname = 'CloseAccount' ;
UPDATE SL_Emailevent SET EnumerationValue = 3, transactioncounter = transactioncounter + 1 WHERE Eventname = 'UpdateAccount' ;
UPDATE SL_Emailevent SET EnumerationValue = 4, transactioncounter = transactioncounter + 1 WHERE Eventname = 'ResetPassword' ;
UPDATE SL_Emailevent SET EnumerationValue = 5, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AddAdministrativeAccountConfirmation' ;
UPDATE SL_Emailevent SET EnumerationValue = 6, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AutoloadConfirmation' ;
UPDATE SL_Emailevent SET EnumerationValue = 7, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AutoloadSuspended' ;
UPDATE SL_Emailevent SET EnumerationValue = 8, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AutoloadPriceChange' ;
UPDATE SL_Emailevent SET EnumerationValue = 9, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AutoloadTransactionFailed' ;
UPDATE SL_Emailevent SET EnumerationValue = 10, transactioncounter = transactioncounter + 1 WHERE Eventname = 'RegisterSmartcard' ;
UPDATE SL_Emailevent SET EnumerationValue = 11, transactioncounter = transactioncounter + 1 WHERE Eventname = 'RemoveSmartcard' ;
UPDATE SL_Emailevent SET EnumerationValue = 12, transactioncounter = transactioncounter + 1 WHERE Eventname = 'BlockSmartcard' ;
UPDATE SL_Emailevent SET EnumerationValue = 13, transactioncounter = transactioncounter + 1 WHERE Eventname = 'UnblockSmartcard' ;
UPDATE SL_Emailevent SET EnumerationValue = 14, transactioncounter = transactioncounter + 1 WHERE Eventname = 'ReplaceSmartcard' ;
UPDATE SL_Emailevent SET EnumerationValue = 15, transactioncounter = transactioncounter + 1 WHERE Eventname = 'ReplaceSmartcardOrganization' ;
UPDATE SL_Emailevent SET EnumerationValue = 16, transactioncounter = transactioncounter + 1 WHERE Eventname = 'OrderConfirmation' ;
UPDATE SL_Emailevent SET EnumerationValue = 17, transactioncounter = transactioncounter + 1 WHERE Eventname = 'OrderPaymentResult' ;
UPDATE SL_Emailevent SET EnumerationValue = 18, transactioncounter = transactioncounter + 1 WHERE Eventname = 'BlockPaymentOption' ;
UPDATE SL_Emailevent SET EnumerationValue = 19, transactioncounter = transactioncounter + 1 WHERE Eventname = 'UnblockPaymentOption' ;
UPDATE SL_Emailevent SET EnumerationValue = 20, transactioncounter = transactioncounter + 1 WHERE Eventname = 'OrderStateUpdated' ;
UPDATE SL_Emailevent SET EnumerationValue = 21, transactioncounter = transactioncounter + 1 WHERE Eventname = 'OrderCreatedIncludingOrderState' ;
UPDATE SL_Emailevent SET EnumerationValue = 22, transactioncounter = transactioncounter + 1 WHERE Eventname = 'EmailConfirmation' ;
UPDATE SL_Emailevent SET EnumerationValue = 23, transactioncounter = transactioncounter + 1 WHERE Eventname = 'CloseSmartcard' ;
UPDATE SL_Emailevent SET EnumerationValue = 24, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AccountActivated' ;
UPDATE SL_Emailevent SET EnumerationValue = 25, transactioncounter = transactioncounter + 1 WHERE Eventname = 'WorkItemCreated' ;
UPDATE SL_Emailevent SET EnumerationValue = 26, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AddPaymentOption' ;
UPDATE SL_Emailevent SET EnumerationValue = 27, transactioncounter = transactioncounter + 1 WHERE Eventname = 'RemovePaymentOption' ;
UPDATE SL_Emailevent SET EnumerationValue = 28, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AccountChanged' ;
UPDATE SL_Emailevent SET EnumerationValue = 29, transactioncounter = transactioncounter + 1 WHERE Eventname = 'PasswordResetToken' ;
UPDATE SL_Emailevent SET EnumerationValue = 30, transactioncounter = transactioncounter + 1 WHERE Eventname = 'ServiceRequestSupportMail' ;
UPDATE SL_Emailevent SET EnumerationValue = 31, transactioncounter = transactioncounter + 1 WHERE Eventname = 'FraudDetectionNotification' ;
UPDATE SL_Emailevent SET EnumerationValue = 32, transactioncounter = transactioncounter + 1 WHERE Eventname = 'SaleResult' ;
UPDATE SL_Emailevent SET EnumerationValue = 33, transactioncounter = transactioncounter + 1 WHERE Eventname = 'DormancyWarning' ;
UPDATE SL_Emailevent SET EnumerationValue = 34, transactioncounter = transactioncounter + 1 WHERE Eventname = 'DormancyExecution' ;
UPDATE SL_Emailevent SET EnumerationValue = 35, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AutoloadCreated' ;
UPDATE SL_Emailevent SET EnumerationValue = 36, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AutoloadDeleted' ;
UPDATE SL_Emailevent SET EnumerationValue = 37, transactioncounter = transactioncounter + 1 WHERE Eventname = 'SmartcardLowBalance' ;
UPDATE SL_Emailevent SET EnumerationValue = 38, transactioncounter = transactioncounter + 1 WHERE Eventname = 'RefundResult' ;
UPDATE SL_Emailevent SET EnumerationValue = 39, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AutoloadUpdated' ;
UPDATE SL_Emailevent SET EnumerationValue = 40, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AutoloadSkipped' ;
UPDATE SL_Emailevent SET EnumerationValue = 41, transactioncounter = transactioncounter + 1 WHERE Eventname = 'AutoloadSkippedPassManuallyAdded' ;
UPDATE SL_Emailevent SET EnumerationValue = 42, transactioncounter = transactioncounter + 1 WHERE Eventname = 'FareCategoryExpiry' ;
UPDATE SL_Emailevent SET EnumerationValue = 43, transactioncounter = transactioncounter + 1 WHERE Eventname = 'UnusedPassExpiryWarning' ;
UPDATE SL_Emailevent SET EnumerationValue = 44, transactioncounter = transactioncounter + 1 WHERE Eventname = 'FareCategoryExpiryWarning' ;
UPDATE SL_Emailevent SET EnumerationValue = 45, transactioncounter = transactioncounter + 1 WHERE Eventname = 'PassExpiryWarning' ;
UPDATE SL_Emailevent SET EnumerationValue = 46, transactioncounter = transactioncounter + 1 WHERE Eventname = 'PassExpiry' ;
UPDATE SL_Emailevent SET EnumerationValue = 47, transactioncounter = transactioncounter + 1 WHERE Eventname = 'RegistrationWithPassword' ;

COMMIT;

PROMPT Done!

SPOOL OFF;
