SPOOL db_3_137.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.137', 'DST', 'Added new column CardLevelResult to SL_CreditCardAuthorization.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (CardLevelResult NVARCHAR2(2));
 
comment on column SL_CREDITCARDAUTHORIZATION.CardLevelResult is 'A value returned by Visa, to designate the type of card product used to process the transaction.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
