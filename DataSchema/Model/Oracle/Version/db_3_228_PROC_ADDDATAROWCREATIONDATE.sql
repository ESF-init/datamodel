SPOOL db_3_228.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.228', 'DST', 'Changed AddDataRowCreationDate procedure to USER_* tables.');

CREATE OR REPLACE PROCEDURE p_addDataRowCreationDate
AS

  nCount NUMBER;
  
  CURSOR schemaTables IS
    (select OBJECT_NAME from user_objects b WHERE UPPER(b.OBJECT_NAME) <> UPPER('VARIO_DMODELLVERSION') AND b.object_type = 'TABLE' 
      AND B.OBJECT_NAME NOT IN ( SELECT OBJECT_NAME FROM user_objects WHERE object_type <> 'TABLE') AND B.OBJECT_NAME NOT IN (SELECT TABLE_NAME FROM user_TABLES WHERE TABLESPACE_NAME IS NULL));
      
  CURSOR schemaTriggers (s_Table VARCHAR2) IS
    (select TRIGGER_NAME from user_triggers t WHERE t.table_name = s_table );

BEGIN
  FOR sTables IN schemaTables() LOOP
    nCount := 0;
    SELECT count(TABLE_NAME) INTO nCount FROM USER_TAB_COLUMNS WHERE TABLE_NAME = sTables.OBJECT_NAME AND COLUMN_NAME = 'DATAROWCREATIONDATE';
    IF nCount = 0 THEN
      FOR sTriggers IN schemaTriggers(sTables.OBJECT_NAME) LOOP
        EXECUTE IMMEDIATE 'ALTER TRIGGER "' || sTriggers.TRIGGER_NAME || '" DISABLE';
      END LOOP;
      
      EXECUTE IMMEDIATE 'ALTER TABLE ' || sTables.OBJECT_NAME || ' ADD (DATAROWCREATIONDATE DATE DEFAULT SYSDATE)';
      
      FOR sTriggers IN schemaTriggers(sTables.OBJECT_NAME) LOOP
        EXECUTE IMMEDIATE 'ALTER TRIGGER "' || sTriggers.TRIGGER_NAME || '" ENABLE';
        EXECUTE IMMEDIATE 'ALTER TRIGGER "' || sTriggers.TRIGGER_NAME || '" COMPILE';
      END LOOP;
    END IF;
  END LOOP;
END p_addDataRowCreationDate;
/

COMMIT;

PROMPT Done!

SPOOL OFF;
