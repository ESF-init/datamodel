SPOOL db_3_031.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.031', 'SLR', 'Added Columns to SL_RULEVIOLATION');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

ALTER TABLE SL_RULEVIOLATION
ADD (
  AGGREGATIONFROM       DATE            DEFAULT sysdate   NOT NULL,
  AGGREGATIONTO         DATE            DEFAULT sysdate   NOT NULL
);

COMMENT ON COLUMN SL_RULEVIOLATION.AGGREGATIONFROM IS 'Defines start of time aggregation of rule violation.';
COMMENT ON COLUMN SL_RULEVIOLATION.AGGREGATIONTO IS 'Defines end of time aggregation of rule violation.';


-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
