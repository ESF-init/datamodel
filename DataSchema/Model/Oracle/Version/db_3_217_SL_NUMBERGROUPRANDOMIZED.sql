SPOOL db_3_217.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.217', 'DST', 'Nullable changes to SL_NUMBERGROUPRANDOMIZED.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_NUMBERGROUPRANDOMIZED MODIFY VALUE NOT NULL;
ALTER TABLE SL_NUMBERGROUPRANDOMIZED MODIFY RANDOMSORTNUMBER NOT NULL;
ALTER TABLE SL_NUMBERGROUPRANDOMIZED MODIFY ISUSED DEFAULT 0 NOT NULL;
ALTER TABLE SL_NUMBERGROUPRANDOMIZED MODIFY NUMBERGROUPSCOPE NOT NULL;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
