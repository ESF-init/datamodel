SPOOL db_3_424.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.424', 'flf', 'New columns in SL_PRODUCT');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PRODUCT
 ADD (SUBVALIDFROM  DATE);

ALTER TABLE SL_PRODUCT
 ADD (SUBVALIDTO  DATE);


COMMENT ON COLUMN SL_PRODUCT.SUBVALIDFROM IS 'Valid from for single instances of multi ride/pass tickets.';

COMMENT ON COLUMN SL_PRODUCT.SUBVALIDTO IS 'Valid to for single instances of multi ride/pass tickets.';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
