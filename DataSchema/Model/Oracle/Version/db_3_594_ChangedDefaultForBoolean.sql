SPOOL db_3_594.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.594', 'BVI', 'Changed boolean defaults to 0.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER table sl_invoice modify IsPaid Number(1) default 0;
update sl_invoice set IsPaid = 0, transactioncounter =  null where IsPaid is null;


ALTER table  VAS_ReportJob modify EnableNotification Number(1) default 0;
update VAS_ReportJob set EnableNotification = 0, transactioncounter =  null where EnableNotification is null;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;