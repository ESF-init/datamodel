SPOOL db_3_462.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.462', 'MFA', 'Alter table SL_CONTRACT, add new column VANPOOLGROUPID.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CONTRACT ADD VANPOOLGROUPID NVARCHAR2(100);
COMMENT ON COLUMN SL_CONTRACT.VANPOOLGROUPID IS 'The van pool group id for the contract.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
