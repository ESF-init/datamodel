SPOOL db_3_212.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.212', 'LWU', 'Added table SL_NUMBERGROUPRANDOMIZED and Procedure GET_SL_NUMBERGROUPRANDOMIZED.');


-- =======[ Changes to Existing Tables ]===========================================================================

CREATE TABLE SL_NUMBERGROUPRANDOMIZED
(
  NUMBERGROUPRANDOMIZEDID NUMBER(18),
  CARDNUMBER              NVARCHAR2(200),
  RANDOMSORTNUMBER        NUMBER(18),
  ISUSED                  NUMBER(1),
  NUMBERGROUPSCOPE        NUMBER(9),
  LASTUSER                NVARCHAR2(50)             DEFAULT 'SYS'                 NOT NULL,
  LASTMODIFIED            DATE                      DEFAULT sysdate               NOT NULL,
  TRANSACTIONCOUNTER      INTEGER                   NOT NULL
);

ALTER TABLE SL_NUMBERGROUPRANDOMIZED ADD (
  CONSTRAINT PK_NUMBERGROUPRANDOMIZED
  PRIMARY KEY
  (NUMBERGROUPRANDOMIZEDID)
  );

CREATE SEQUENCE SL_NUMBERGROUPRANDOMIZED_SEQ START WITH 1
                                                  MAXVALUE 9999999999999999999999999999
                                                  MINVALUE 1
                                                  NOCYCLE
                                                  CACHE 20
                                                  NOORDER;


CREATE OR REPLACE TRIGGER SL_NUMBERGROUPRANDOMIZED_BRI before insert ON SL_NUMBERGROUPRANDOMIZED for each row
begin    :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_NUMBERGROUP_ANDOMIZED_BRU before update ON SL_NUMBERGROUPRANDOMIZED for each row
begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE PROCEDURE GET_SL_NUMBERGROUPRANDOMIZED(NUMBERGROUPSCOPE IN NUMBER, cardNumber OUT NUMBER )
IS
    l_SelectedfromRandom Integer;
BEGIN
    SELECT CardNumber into l_SelectedfromRandom from (SELECT CARDNUMBER FROM SL_NUMBERGROUPRANDOMIZED WHERE ISUSED = 0 AND NUMBERGROUPSCOPE = NUMBERGROUPSCOPE ORDER BY RANDOMSORTNUMBER) where rownum = 1;
    UPDATE SL_NUMBERGROUPRANDOMIZED SET ISUSED = 1 WHERE CARDNUMBER = l_SelectedfromRandom;
    COMMIT;
    cardNumber := l_SelectedfromRandom;
END;
/
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;