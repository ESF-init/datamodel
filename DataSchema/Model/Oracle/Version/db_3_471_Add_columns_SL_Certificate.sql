SPOOL db_3_471.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.471', 'MEA', 'Add columns in SL_CERTIFICATE');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_CERTIFICATE ADD (Name  NVARCHAR2(512));
ALTER TABLE SL_CERTIFICATE ADD (CRYPTOARCHIVEID  NUMBER(18));
ALTER TABLE SL_CERTIFICATE ADD (CRYPTOCLIENTID  NUMBER(18));
ALTER TABLE SL_CERTIFICATE ADD (INCLUDEINQARCHIVE  NUMBER(1, 0) DEFAULT 0 NOT NULL);

COMMENT ON COLUMN SL_CERTIFICATE.Name IS 'Certificate name.';
COMMENT ON COLUMN SL_CERTIFICATE.CRYPTOARCHIVEID IS 'Crypto archive Id';
COMMENT ON COLUMN SL_CERTIFICATE.CRYPTOCLIENTID IS 'Client Id';
COMMENT ON COLUMN SL_CERTIFICATE.INCLUDEINQARCHIVE IS '1: certificate will be included in QArchive';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
