SPOOL db_3_492.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.492', 'FLF', 'Added SalesFlag column to TM_FareMatrixEntry');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE TM_FAREMATRIXENTRY
 ADD (SALESFLAG  NUMBER(1));


COMMENT ON COLUMN TM_FAREMATRIXENTRY.SALESFLAG IS 'Generic flag for sales purposes';

-- =======[ New Tables ]===========================================================================================
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
