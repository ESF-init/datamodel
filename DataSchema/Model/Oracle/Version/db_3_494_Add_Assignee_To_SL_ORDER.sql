SPOOL db_3_494.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.494', 'EMN', 'Added Assignee to SL_ORDER.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_ORDER ADD Assignee NUMBER(18) NULL; 

ALTER TABLE SL_ORDER ADD CONSTRAINT FK_Order_Assignee FOREIGN KEY (Assignee) REFERENCES USER_LIST(UserID);

COMMENT ON COLUMN SL_ORDER.Assignee IS 'Id of the assigned user for fulfilling this order. References USER_LIST.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
