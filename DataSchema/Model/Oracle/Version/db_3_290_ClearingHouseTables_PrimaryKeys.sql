 SPOOL db_3_290.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.290', 'FRD', 'added primary keys to clearing tables');

-- =======[ Changes to Existing Tables ]===========================================================================

 ALTER TABLE CH_CLEARING ADD (
  PRIMARY KEY
  (CLEARINGID)
  USING INDEX PK_CH_CLEARING
  ENABLE VALIDATE);
  
 ALTER TABLE CH_CLEARINGSUM ADD (
  PRIMARY KEY
  (CLEARINGSUMID)
  USING INDEX PK_CH_CLEARINGSUM
  ENABLE VALIDATE);

COMMIT;

PROMPT Done!

SPOOL OFF;
 
