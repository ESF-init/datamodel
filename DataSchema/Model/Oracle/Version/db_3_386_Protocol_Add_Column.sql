SPOOL db_3_386.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.386', 'ARH', 'Add_Column_DeviceNo_To_Protocol');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE PROTOCOL
ADD DEVICENO NUMBER(10,0) null;

COMMENT ON COLUMN PROTOCOL.DEVICENO IS 'Device number (serial number) which created this protocol.';

COMMIT;

PROMPT Done!

SPOOL OFF;