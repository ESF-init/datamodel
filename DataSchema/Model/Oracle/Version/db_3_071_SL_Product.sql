SPOOL db_3_071.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.071', 'SLR', 'Added Blocking reason to sl_product (relation to enumeration).');

-- Open Issue: table contains a column named 'blockingreason' already which is only a comment (nvarchar2)
-- 		 TODO: * rename blockingreson to blockingcomment
--			   * rename blockingreasonenum to blockingreason

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_product add blockingreasonenum number(9) Default 0 NOT NULL;

COMMENT ON COLUMN sl_product.blockingreasonenum IS 'References to enum blocking reason.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
