SPOOL db_3_242.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.242', 'FRD', 'ClearingHouse extensions for new RITS ClearingHouse, Clearing Result Table constraints');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================


-- =======[ New Tables ]===========================================================================================

ALTER TABLE CH_CLEARINGRESULT ADD (CONSTRAINT FK_CLEARINGRESULT_CLIENTID FOREIGN KEY (CLIENTID) REFERENCES VARIO_CLIENT(CLIENTID));
ALTER TABLE CH_CLEARINGRESULT ADD (CONSTRAINT FK_CLEARINGRESULT_DEVCLASSID FOREIGN KEY (DEVICECLASSID) REFERENCES VARIO_DEVICECLASS(DEVICECLASSID));
ALTER TABLE CH_CLEARINGRESULT ADD (CONSTRAINT FK_CLEARINGRESULT_TRANSTYPEID FOREIGN KEY (TRANSACTIONTYPEID) REFERENCES RM_TRANSACTIONTYPE(TYPEID));
ALTER TABLE CH_CLEARINGRESULT ADD (CONSTRAINT FK_CLEARINGRESULT_LINEGROUPID FOREIGN KEY (LINEGROUPID) REFERENCES TM_LINEGROUP(LINEGROUPID));
ALTER TABLE CH_CLEARINGRESULT ADD (CONSTRAINT FK_CLEARINGRESULT_LINEID FOREIGN KEY (LINEID) REFERENCES VARIO_LINE(LINEID));

----------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
