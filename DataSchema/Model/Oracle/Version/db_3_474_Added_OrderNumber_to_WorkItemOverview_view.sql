SPOOL db_3_474.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.474', 'STV', 'Added ordernumber to WorkItemOverview View');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
DROP VIEW sl_workitemoverview;
CREATE OR REPLACE FORCE VIEW sl_workitemoverview (workitemid,
                                                         cardid,
                                                         faremediaid,
                                                         printednumber,
                                                         orderid,
                                                         contractid,
                                                         contractnumber,
                                                         organizationid,
                                                         workitemsubjectid,
                                                         typediscriminator,
                                                         title,
                                                         memo,
                                                         SOURCE,
                                                         state,
                                                         assignee,
                                                         createdby,
                                                         created,
                                                         assigned,
                                                         executed,
                                                         lastmodified,
                                                         clientid,
                                                         clientname,
                                                         ordernumber
                                                        )
AS
SELECT
        SL_WorkItem.WorkItemID,
        SL_WorkItem.CardID,
        SL_Card.FareMediaID,
        SL_Card.PrintedNumber,
        SL_WorkItem.OrderID,
        cast(coalesce(sl_workitem.contractid, sl_order.contractid) as Number(18,0)),
        SL_Contract.ContractNumber,
        SL_Contract.OrganizationID,
        SL_WorkItem.WorkItemSubjectID,
        SL_WorkItem.TypeDiscriminator,
        SL_WorkItem.Title,
        SL_WorkItem.Memo,
        SL_WorkItem.Source,
        SL_WorkItem.State,
        SL_WorkItem.Assignee,
        SL_WorkItem.CreatedBy,
        SL_WorkItem.Created,
        SL_WorkItem.Assigned,
        SL_WorkItem.Executed,
        SL_WorkItem.LastModified,
        sl_contract.clientid,        
        vario_client.companyname as clientname,
        sl_order.OrderNumber as ordernumber
        FROM sl_workitem 
        LEFT OUTER JOIN sl_order
        ON sl_workitem.orderid = sl_order.orderid
        LEFT OUTER JOIN sl_contract
        ON coalesce(sl_workitem.contractid, sl_order.contractid) = sl_contract.contractid
        LEFT OUTER JOIN sl_card 
        ON sl_workitem.cardid = sl_card.cardid
        LEFT OUTER JOIN vario_client
        ON sl_contract.clientid = vario_client.clientid;
-- =======[ New Tables ]===========================================================================================

--COMMENT ON TABLE XXX is '';

--COMMENT ON COLUMN XXX.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
--COMMENT ON COLUMN XXX.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
--COMMENT ON COLUMN XXX.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
