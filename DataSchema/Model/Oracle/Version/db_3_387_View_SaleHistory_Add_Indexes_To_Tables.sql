SPOOL db_3_387.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.387', 'LJU', 'add 2 indexes used by view sale history');

-- =======[ Changes to Existing Tables ]===========================================================================

CREATE INDEX IDX_ORDERHISTORY_ORDER ON SL_ORDERHISTORY
(ORDERID, LASTMODIFIED);

CREATE INDEX IDX_SALE_CONTRACT ON SL_SALE
(CONTRACTID);

---End adding schema changes 

COMMIT;

PROMPT Done!

SPOOL OFF;
