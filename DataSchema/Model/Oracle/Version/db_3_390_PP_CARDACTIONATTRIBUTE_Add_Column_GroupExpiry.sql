SPOOL db_3_390.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.390', 'MEA', 'Add Column GroupExpiry to table PP_CARDACTIONATTRIBUTE');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE PP_CARDACTIONATTRIBUTE ADD GroupExpiry DATE default NULL;
 
COMMENT ON COLUMN PP_CARDACTIONATTRIBUTE.GroupExpiry IS 'Date until the UserGroup is valid in format yymmdd';


COMMIT;

PROMPT Done!

SPOOL OFF;