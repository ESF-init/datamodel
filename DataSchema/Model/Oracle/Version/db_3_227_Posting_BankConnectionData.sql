SPOOL db_3_227.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.227', 'BVI', 'New collumns in SL_Posting and SL_SEPA.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_Posting ADD InvoiceID NUMBER (9);
ALTER TABLE SL_Posting ADD CONSTRAINT fk_Posting_Invoice foreign key (InvoiceID) references SL_Invoice (InvoiceID);
COMMENT ON COLUMN SL_Posting.InvoiceID IS 'References to a invoice without modifying it.';

CREATE TABLE SL_SEPAFrequencyType
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL,
  CONSTRAINT PK_SEPAFrequencyType PRIMARY KEY (EnumerationValue)
);
COMMENT ON TABLE SL_SEPAFrequencyType is 'This table contains a list of sepa frequency types.';

INSERT INTO SL_SEPAFrequencyType (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (0, 'Recurrently', 'Recurrently');
INSERT INTO SL_SEPAFrequencyType (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (1, 'Single', 'Single');

CREATE TABLE SL_SEPAOwnerType
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL,
  CONSTRAINT PK_SEPAOwnerType PRIMARY KEY (EnumerationValue)
);
COMMENT ON TABLE SL_SEPAOwnerType is 'This table contains a list of sepa owner types.';

INSERT INTO SL_SEPAOwnerType (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (0, 'Private account', 'Private account');
INSERT INTO SL_SEPAOwnerType (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (1, 'Company account', 'Company account');


ALTER TABLE SL_BankConnectionData ADD FrequencyType NUMBER (1) DEFAULT 0 NOT NULL;
COMMENT ON COLUMN SL_BankConnectionData.FrequencyType is 'Reference to SL_SEPAFrequencyType.ENUMERATIONVALUE.';

ALTER TABLE SL_BankConnectionData ADD OwnerType NUMBER (1) DEFAULT 0 NOT NULL;
COMMENT ON COLUMN SL_BankConnectionData.OwnerType is 'Reference to SL_SEPAOwnerType.ENUMERATIONVALUE.';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
