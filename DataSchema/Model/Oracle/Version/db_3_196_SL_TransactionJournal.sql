SPOOL db_3_196.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.196', 'DST', 'Increased Line size from 20 to 50 in SL_TransactionJournal.');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_transactionjournal modify line nvarchar2(50);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
