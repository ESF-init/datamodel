SPOOL db_3_619.log;


------------------------------------------------------------------------------
--Version 3.619
------------------------------------------------------------------------------
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.619', 'SOE', 'Updated SL_EmailEvent');


-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'OrderConfirmed_OrderState_2','OrderConfirmed for order state Processed', -1, 'OrderMailMerge', 82);

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'OrderConfirmed_OrderState_6','OrderConfirmed for order state Cancelled', -1, 'OrderMailMerge', 83);



COMMIT;

PROMPT Done!

SPOOL OFF;
