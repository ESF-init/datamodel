SPOOL db_3_587.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.587', 'MFA', 'Add SL_PERSON reference to SL_SALE for anonymous sales');

ALTER TABLE SL_SALE ADD PERSONID NUMBER(18) NULL;

COMMENT ON COLUMN SL_SALE.PERSONID IS 'Person Id references SL_PERSON used for anonymous sales';

ALTER TABLE SL_SALE ADD CONSTRAINT FK_SALE_PERSON FOREIGN KEY (PERSONID) REFERENCES SL_PERSON (PERSONID);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
