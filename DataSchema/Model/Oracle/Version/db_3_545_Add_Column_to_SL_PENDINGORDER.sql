SPOOL db_3_545.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.545', 'PCO', 'Add RetryCount column into SL_PENDINGORDER');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PENDINGORDER ADD RETRYCOUNT NUMBER(9,0) NULL;
COMMENT ON COLUMN SL_PENDINGORDER.RETRYCOUNT IS 'The number of times a payment has been attempted for a pending order.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
