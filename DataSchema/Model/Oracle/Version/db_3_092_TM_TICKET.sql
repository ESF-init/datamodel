SPOOL db_3_092.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.092', 'SLR', 'New column PaymentInterval in TM_TICKET that refers to enum TM_PAYMENTINTERVAL ');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------
CREATE TABLE TM_PAYMENTINTERVAL
(
  PAYMENTINTERVALID         NUMBER(9)           NOT NULL,
  PAYMENTINTERVALNAME       VARCHAR2(250 BYTE)  NOT NULL,
  DESCRIPTION               VARCHAR2(400 BYTE)
);

ALTER TABLE TM_PAYMENTINTERVAL ADD (
   CONSTRAINT TM_PAYMENTINTERVAL_PK
  PRIMARY KEY
 (PAYMENTINTERVALID));
 
COMMENT ON TABLE TM_PAYMENTINTERVAL IS 'Enumeration table for possible payment intervals of ticket used in system.';
COMMENT ON COLUMN TM_PAYMENTINTERVAL.PAYMENTINTERVALID IS 'Unique identifier of the current entity.';
COMMENT ON COLUMN TM_PAYMENTINTERVAL.PAYMENTINTERVALNAME IS 'Name of the source; used internally.';
COMMENT ON COLUMN TM_PAYMENTINTERVAL.DESCRIPTION IS 'Descripton of the source. This is used in the UI to display the value.';


CREATE TABLE TM_TICKET_PAYMENTINTERVAL
(
  TICKETID               NUMBER(10),
  PAYMENTINTERVALID        NUMBER(9)
);

ALTER TABLE TM_TICKET_PAYMENTINTERVAL ADD (
    CONSTRAINT TM_TICKET_PAYMENTINTERVAL_PK
  PRIMARY KEY
 (TICKETID, PAYMENTINTERVALID));

CREATE INDEX IDX_TICKETPAYINTERVALTICKETID 
  ON TM_TICKET_PAYMENTINTERVAL
 (TICKETID);
 
COMMENT ON TABLE TM_TICKET_PAYMENTINTERVAL IS 'Relation table to define which payment intervals are available for ticket entities.';
COMMENT ON COLUMN TM_TICKET_PAYMENTINTERVAL.TICKETID IS 'References TM_TICKET.TICKETID.';
COMMENT ON COLUMN TM_TICKET_PAYMENTINTERVAL.PAYMENTINTERVALID IS 'References TM_PAYMENTINTERVAL.PAYMENTINTERVALID.';

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
