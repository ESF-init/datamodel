SPOOL db_3_564.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.564', 'Ulb', 'New procedure P_WriteBulkMessage');


-- =======[ Functions and Data Types ]=============================================================================
CREATE OR REPLACE PROCEDURE P_WriteBulkMessage
(
nPROTMESSAGEID IN NUMBER,
nPROTACTIONID IN NUMBER,
nPROTID IN NUMBER,
nPROTMESSAGEPRIO IN NUMBER,
sPROTMESSAGETEXT IN VARCHAR,
dPROTMESSAGEDATE IN DATE
)
IS

nId NUMBER(10):=0;

BEGIN

INSERT INTO PROT_MESSAGE (PROTMESSAGEID,PROTACTIONID,PROTID,PROTMESSAGEPRIO,PROTMESSAGETEXT,PROTMESSAGEDATE)
VALUES (SEQ_protocolid.NEXTVAL,nPROTACTIONID,nPROTID,nPROTMESSAGEPRIO,sPROTMESSAGETEXT,dPROTMESSAGEDATE);

END P_WriteBulkMessage;
/
---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
