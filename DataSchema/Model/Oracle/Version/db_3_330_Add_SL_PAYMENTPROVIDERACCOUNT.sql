SPOOL db_3_330.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.330', 'ARH', 'Created SL_PAYMENTPROVIDERACCOUNT');

-- =======[ New Table  ]===========================================================================

--
-- SL_PAYMENTPROVIDERACCOUNT
--
CREATE TABLE SL_PAYMENTPROVIDERACCOUNT(
	PAYMENTPROVIDERACCOUNTID  	NUMBER(18)                NOT NULL,
	PAYMENTPROVIDERREFERENCE	VARCHAR2(32)              NOT NULL,
	CONTRACTID            		NUMBER(18)                NOT NULL,
	LASTUSER					NVARCHAR2(50)	DEFAULT 'SYS'		NOT NULL,
	LASTMODIFIED				DATE			DEFAULT sysdate		NOT NULL,
	TRANSACTIONCOUNTER			INTEGER								NOT NULL,
	DATAROWCREATIONDATE			DATE			DEFAULT sysdate	
);
ALTER TABLE SL_PAYMENTPROVIDERACCOUNT ADD CONSTRAINT PK_PAYMENTPROVIDERACCOUNT PRIMARY KEY (PAYMENTPROVIDERACCOUNTID);
ALTER TABLE SL_PAYMENTPROVIDERACCOUNT ADD CONSTRAINT FK_PPACCOUNT_CONTRACT FOREIGN KEY (CONTRACTID) REFERENCES SL_CONTRACT (CONTRACTID);

COMMENT ON TABLE SL_PAYMENTPROVIDERACCOUNT IS 'Account by payment provider for Regiomove';
COMMENT ON COLUMN SL_PAYMENTPROVIDERACCOUNT.PAYMENTPROVIDERACCOUNTID IS 'Unique ID of the payment provider account table.';
COMMENT ON COLUMN SL_PAYMENTPROVIDERACCOUNT.PAYMENTPROVIDERREFERENCE IS 'Reference of the costumer account by the payment provider.';
COMMENT ON COLUMN SL_PAYMENTPROVIDERACCOUNT.CONTRACTID IS 'SL_CONTRACT.CONTRACTID';
COMMENT ON COLUMN SL_PAYMENTPROVIDERACCOUNT.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_PAYMENTPROVIDERACCOUNT.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_PAYMENTPROVIDERACCOUNT.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_PAYMENTPROVIDERACCOUNT.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(10) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_PAYMENTPROVIDERACCOUNT');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(10) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_PAYMENTPROVIDERACCOUNT');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/


COMMIT;

PROMPT Done!

SPOOL OFF;
