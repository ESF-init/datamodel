SPOOL db_3_293.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.293', 'DST', 'Added CardID to SL_Job.');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_job add cardid number(18);
alter table sl_job add constraint fk_job_card foreign key (cardid) references sl_card(cardid);
comment on column sl_job.cardid is 'Optional reference to card.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
