
SPOOL db_3_023.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.023', 'init\ldo', 'Added tables for MOBILEvario messaging system (SL_MESSAGE, SL_RECIPIENTGROUP and dependencies)');

-- =======[ New Tables ]========================================================================================

CREATE TABLE SL_RECIPIENTGROUP
(
    RECIPIENTGROUPID    Number(18,0)    NOT NULL,
    DESCRIPTION         Nvarchar2(512),
    LASTUSER VARCHAR2(50  Byte) DEFAULT 'SYS' NOT NULL,
	LASTMODIFIED DATE DEFAULT sysdate NOT NULL,	
	TRANSACTIONCOUNTER INTEGER NOT NULL,
    CONSTRAINT PK_RECIPIENTGROUP PRIMARY KEY (RECIPIENTGROUPID)
);

COMMENT ON TABLE SL_RECIPIENTGROUP is 'Table containing recipient groups. The group members are configured by SL_RECIPIENTGROUPTOPERSON.';
COMMENT ON COLUMN SL_RECIPIENTGROUP.RECIPIENTGROUPID is 'Unique identifier of the recipient group.';
COMMENT ON COLUMN SL_RECIPIENTGROUP.DESCRIPTION is 'Description of the recipient group.';
COMMENT ON COLUMN SL_RECIPIENTGROUP.LASTMODIFIED is 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_RECIPIENTGROUP.TRANSACTIONCOUNTER is 'Technical field: counter to prevent concurrent changes to this dataset';

CREATE TABLE SL_RECIPIENTGROUPMEMBER
(
    RECIPIENTGROUPID    Number(18,0)    NOT NULL,
    PERSONID            Number(18,0)    NOT NULL,
    CONSTRAINT PK_RECIPIENTGROUPMEMBER PRIMARY KEY (RECIPIENTGROUPID, PERSONID),
    CONSTRAINT FK_RECIPIENTGROUPMEMBER_RECIP FOREIGN KEY (RECIPIENTGROUPID) REFERENCES SL_RECIPIENTGROUP (RECIPIENTGROUPID),
    CONSTRAINT FK_RECIPIENTGROUPMEMBER_PERSON FOREIGN KEY (PERSONID) REFERENCES SL_PERSON (PERSONID)
);

COMMENT ON TABLE SL_RECIPIENTGROUPMEMBER is 'Table defining the members of recipient groups.';
COMMENT ON COLUMN SL_RECIPIENTGROUPMEMBER.RECIPIENTGROUPID is 'Recipient group for which a member is defined.';
COMMENT ON COLUMN SL_RECIPIENTGROUPMEMBER.PERSONID is 'Person which is defined as member of a recipient group.';

CREATE TABLE SL_MESSAGETYPE
(
    ENUMERATIONVALUE    Number(9,0)     NOT NULL,
    LITERAL             Nvarchar2(50)   NOT NULL,
    DESCRIPTION         Nvarchar2(50)    NOT NULL,
    CONSTRAINT PK_MESSAGETYPE PRIMARY KEY (ENUMERATIONVALUE)
);

COMMENT ON TABLE SL_MESSAGETYPE is 'Enumeration for different message types (e.g. Email, SMS).';
COMMENT ON COLUMN SL_MESSAGETYPE.ENUMERATIONVALUE is 'Unique enumeration value for the message type. Used internally.';
COMMENT ON COLUMN SL_MESSAGETYPE.LITERAL is 'Name of the enumeration entry. Used to display type in UI.';
COMMENT ON COLUMN SL_MESSAGETYPE.DESCRIPTION is 'Description of the enumeration entry.';

CREATE TABLE SL_MESSAGE
(
    MESSAGEID           Number(18,0)    NOT NULL,
    MESSAGETYPE         Number(9,0)     NOT NULL,
    RECIPIENT           Nvarchar2(512),
    RECIPIENTGROUPID    Number(18,0),
    MESSAGECONTENT      Blob            NOT NULL,
    LASTUSER VARCHAR2(50  Byte) DEFAULT 'SYS' NOT NULL,
	LASTMODIFIED DATE DEFAULT sysdate NOT NULL,	
	TRANSACTIONCOUNTER INTEGER NOT NULL,
    CONSTRAINT PK_MESSAGE PRIMARY KEY (MESSAGEID),
    CONSTRAINT FK_MESSAGE_RECIPIENTGROUP FOREIGN KEY (RECIPIENTGROUPID) REFERENCES SL_RECIPIENTGROUP (RECIPIENTGROUPID)
);

COMMENT ON TABLE SL_MESSAGE is 'Table containing MOBILEvario messages to be sent to a recipient or recipient group. The corresponding jobs are contained in SL_JOB.';
COMMENT ON COLUMN SL_MESSAGE.MESSAGETYPE is 'Type of the message (e.g. Email, SMS)';
COMMENT ON COLUMN SL_MESSAGE.MESSAGEID is 'Unique identifier of the message.';
COMMENT ON COLUMN SL_MESSAGE.RECIPIENT is 'Recipient address in case the message is sent to a single recipient.';
COMMENT ON COLUMN SL_MESSAGE.RECIPIENTGROUPID is 'Id of the recipient group in case the message is sent to a group of recipients.';
COMMENT ON COLUMN SL_MESSAGE.MESSAGECONTENT is 'Content of the message. Data structure depending on message type.';
COMMENT ON COLUMN SL_MESSAGE.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_MESSAGE.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';


-- =======[ Changes on existing tables ]===========================================================================

ALTER TABLE SL_JOB ADD
(
    MESSAGEID           Number(18,0)
);

ALTER TABLE SL_JOB 
ADD CONSTRAINT SL_JOB_MESSAGE FOREIGN KEY (MESSAGEID) REFERENCES SL_MESSAGE (MESSAGEID);

COMMENT ON COLUMN SL_JOB.MESSAGEID is 'Id of the message to be sent in case of a messaging job.';


-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(7) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_MESSAGE', 'SL_RECIPIENTGROUP');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(7) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_MESSAGE', 'SL_RECIPIENTGROUP');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==============================================================================================

INSERT INTO SL_MESSAGETYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
VALUES
(0, 'Email', 'Email');

INSERT INTO SL_MESSAGETYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
VALUES
(1, 'SMS', 'SMS');

INSERT INTO SL_JOBTYPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION)
VALUES
(2, 'SendMessage', 'Send Message');

COMMIT;

spool off