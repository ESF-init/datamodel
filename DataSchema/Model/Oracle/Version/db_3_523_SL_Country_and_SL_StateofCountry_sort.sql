SPOOL db_3_523.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.523', 'MFA', 'Modifying SL_Country and SL_StateOfCountry to include a display sort column');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_COUNTRY ADD ORDERNUMBER NUMBER(9) DEFAULT 1 NOT NULL;
COMMENT ON COLUMN SL_COUNTRY.ORDERNUMBER IS 'Sort value for display purposes.';

ALTER TABLE SL_STATEOFCOUNTRY ADD ORDERNUMBER NUMBER(9) DEFAULT 1 NOT NULL;
COMMENT ON COLUMN SL_STATEOFCOUNTRY.ORDERNUMBER IS 'Sort value for display purposes.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
