SPOOL db_3_457.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.457', 'FLF', 'Added fields to tm panels and keys to allow navigation between panels');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE TM_USERKEYS
 ADD (DESTINATIONPANELID  NUMBER(18));

COMMENT ON COLUMN TM_USERKEYS.KEYSTYLEID IS 'Reference to TM_KEYSTYLE.KEYSTYLEID';
COMMENT ON COLUMN TM_USERKEYS.DESTINATIONPANELID IS 'Reference to TM_PANEL.PANELID. Usage of the key will open this panel.';



ALTER TABLE TM_PANELS
 ADD (APPLYCARDTICKETFILTER  NUMBER(1)              DEFAULT 0                     NOT NULL);
 
COMMENT ON COLUMN TM_PANELS.APPLYCARDTICKETFILTER IS 'Defines if pre-selected card ticket should be used to filter content of this panel.';

-- =======[ New Tables ]===========================================================================================
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
