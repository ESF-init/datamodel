SPOOL db_3_145.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.145', 'FLF', 'Added fare stage number override to tm_farestage.');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE TM_FARESTAGE
 ADD (NUMBEROVERRIDE  NUMBER(10));


COMMENT ON COLUMN TM_FARESTAGE.NUMBEROVERRIDE IS 'Alternative value for the number column. Will be set as TZCODE if not null and greater -1';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;