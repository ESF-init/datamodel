SPOOL db_3_096.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.096', 'STV', 'Creation of SL_PRODUCTTERMINATION, SL_PRODUCTTERMINATIONTYPE ');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_PRODUCTTERMINATIONTYPE
(
	PRODUCTTERMINATIONTYPEID	NUMBER(18,0)    NOT NULL,
	CLIENTID NUMBER(10) NOT NULL,
	NAME    NVARCHAR2(128)  NOT NULL,
    DESCRIPTION    NVARCHAR2(250)  NOT NULL,
	PROCESSINGCOST  NUMBER(9) NULL,
	TAXCOST	NUMBER(9) NULL,
	PAYMENTDUEDAYS NUMBER(9) NULL,	
	DUNNINGCOST NUMBER(9) NULL,	
	ADDDIFFERENCECOST  NUMBER(1,0) DEFAULT 0 NOT NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_PRODUCTTERMINATIONTYPEID PRIMARY KEY (PRODUCTTERMINATIONTYPEID)
);

CREATE TABLE SL_PRODUCTTERMINATION
(
    PRODUCTTERMINATIONID       NUMBER(18,0)    NOT NULL,
	PRODUCTTERMINATIONTYPEID	NUMBER(18,0)    NOT NULL,    
    DESCRIPTION    NVARCHAR2(250)  NOT NULL,	
	CONTRACTID NUMBER(18,0) NOT NULL,
	PRODUCTID NUMBER(18,0) NOT NULL,
	INVOICEID NUMBER(18,0) NULL,
	EMPLOYEEID NUMBER(18,0) NOT NULL,
	TERMINATIONDATE DATE NOT NULL,
	DEMANDDATE DATE NULL,
	DIFFERENCECOST NUMBER(9,0) NULL,
	BANKINGCOST NUMBER(9) NULL,
	TOTALCOST NUMBER(9,0) NULL,
	LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL, 
	LastModified DATE DEFAULT sysdate NOT NULL, 
	TransactionCounter INTEGER NOT NULL,
	CONSTRAINT PK_PRODUCTTERMINATIONID PRIMARY KEY (PRODUCTTERMINATIONID)
);

alter table SL_PRODUCTTERMINATION add constraint FK_PRODUCTTERMINATIONTYPE foreign key (PRODUCTTERMINATIONTYPEID) references SL_PRODUCTTERMINATIONTYPE (PRODUCTTERMINATIONTYPEID);
alter table SL_PRODUCTTERMINATION add constraint FK_PRODUCTTERMINATION_PRODUCT foreign key (PRODUCTID) references SL_PRODUCT (PRODUCTID);
alter table SL_PRODUCTTERMINATION add constraint FK_PRODUCTTERMINATION_CONTRACT foreign key (CONTRACTID) references SL_CONTRACT (CONTRACTID);
alter table SL_PRODUCTTERMINATION add constraint FK_PRODUCTTERMINATION_INVOICE foreign key (INVOICEID) references SL_INVOICE (INVOICEID);
alter table SL_PRODUCTTERMINATION add constraint FK_PRODUCTTERMINATION_PERSON foreign key (EMPLOYEEID) references SL_PERSON (PERSONID);

COMMENT ON TABLE SL_PRODUCTTERMINATION is 'Table containing information about a product termination';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.PRODUCTTERMINATIONID is 'References SL_PRODUCTTERMINATIONTYPE.PRODUCTTERMINATIONTYPEID';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.DESCRIPTION is 'Extra notes about this termination';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.CONTRACTID is 'References SL_CONTRACT.CONTRACTID';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.PRODUCTID is 'References SL_PRODUCT.PRODUCTID';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.INVOICEID is 'References SL_INVOICE.INVOICEID';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.EMPLOYEEID is 'References SL_PERSON.PERSONID';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.TERMINATIONDATE is 'Date of when the product should be terminated';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.DEMANDDATE is 'Date of when the product termination is demanded';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.DIFFERENCECOST is 'The difference cost compared with a non jearly product';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.BANKINGCOST is 'Unique identifier of the product termination';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.TOTALCOST is 'The total cost of the product termination';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.LastUser is 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.LastModified is 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_PRODUCTTERMINATION.TransactionCounter is 'Technical field: counter to prevent concurrent changes to this dataset';

COMMENT ON TABLE SL_PRODUCTTERMINATIONTYPE is 'Table containing information about a product termination types';
COMMENT ON COLUMN SL_PRODUCTTERMINATIONTYPE.PRODUCTTERMINATIONTYPEID is 'Unique identifier of the product termination';
COMMENT ON COLUMN SL_PRODUCTTERMINATIONTYPE.NAME is 'Unique identifier of the product termination';
COMMENT ON COLUMN SL_PRODUCTTERMINATIONTYPE.DESCRIPTION is 'Unique identifier of the product termination';
COMMENT ON COLUMN SL_PRODUCTTERMINATIONTYPE.PROCESSINGCOST is 'Unique identifier of the product termination';
COMMENT ON COLUMN SL_PRODUCTTERMINATIONTYPE.TAXCOST is 'Unique identifier of the product termination';
COMMENT ON COLUMN SL_PRODUCTTERMINATIONTYPE.PAYMENTDUEDAYS is 'Unique identifier of the product termination';
COMMENT ON COLUMN SL_PRODUCTTERMINATIONTYPE.DUNNINGCOST is 'Unique identifier of the product termination';
COMMENT ON COLUMN SL_PRODUCTTERMINATIONTYPE.ADDDIFFERENCECOST is 'Unique identifier of the product termination';
COMMENT ON COLUMN SL_PRODUCTTERMINATIONTYPE.LastUser is 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_PRODUCTTERMINATIONTYPE.LastModified is 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_PRODUCTTERMINATIONTYPE.TransactionCounter is 'Technical field: counter to prevent concurrent changes to this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(4) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_PRODUCTTERMINATION');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

DECLARE
  TYPE table_names_array IS VARRAY(4) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_PRODUCTTERMINATIONTYPE');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
	  dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
	  EXECUTE IMMEDIATE sql_string;
	  
	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating sequence: ' || sqlerrm);
	END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(6) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_PRODUCTTERMINATION');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/

DECLARE
  TYPE table_names_array IS VARRAY(6) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_PRODUCTTERMINATIONTYPE');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
	BEGIN
	  sql_string := '';

	  dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRI' ||
		' before insert on ' || table_names(table_name) || 
		' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
	  sql_string := 
		'create or replace trigger ' || table_names(table_name) || '_BRU' ||
		' before update on ' || table_names(table_name) || 
		' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
	  EXECUTE IMMEDIATE sql_string;

	  dbms_output.put_line('Done!');
	exception
	WHEN others THEN
	  dbms_output.put_line('Error creating trigger: ' || sqlerrm);
	END;
  END LOOP;
END;
/
-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
