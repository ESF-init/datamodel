SPOOL db_3_520.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.520', 'DST', 'Fix typos in CS_KVVSUBSCRIPTION');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE CS_KVVSUBSCRIPTION RENAME COLUMN oldFFAREZONETOID TO oldFAREZONETOID;
ALTER TABLE CS_KVVSUBSCRIPTION RENAME COLUMN FFAREZONETOID TO FAREZONETOID;


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
