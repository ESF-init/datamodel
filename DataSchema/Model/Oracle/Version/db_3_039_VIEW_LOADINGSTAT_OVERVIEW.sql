SPOOL db_3_039.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.039', 'MRP', 'Change datasource of deviceclassid from VIEW_LOADINGSTAT_OVERVIEW.');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

--COMMENT ON TABLE XXX is '';
--COMMENT ON COLUMN XXX.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
--COMMENT ON COLUMN XXX.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
--COMMENT ON COLUMN XXX.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================
CREATE OR REPLACE FORCE VIEW VIEW_LOADINGSTAT_OVERVIEW 
AS 
SELECT um_unit.unitid,
view_loadingstatistic_archive.loadingstatisticsid,
view_loadingstatistic_archive.deviceid,
um_device.deviceclassid,
view_loadingstatistic_archive.archiveid,
view_loadingstatistic_archive.loadedfrom,
view_loadingstatistic_archive.loadeduntil,
view_loadingstatistic_archive.typeofarchiveid,
view_loadingstatistic_archive.filename,
view_loadingstatistic_archive.filedate,
view_loadingstatistic_archive.version,
view_loadingstatistic_archive.lsstate,
um_unit.clientid,
um_device.externaldeviceno
FROM um_device
INNER JOIN um_unit
ON um_device.unitid = um_unit.unitid
RIGHT OUTER JOIN view_loadingstatistic_archive
ON um_device.deviceid = view_loadingstatistic_archive.deviceid;

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
