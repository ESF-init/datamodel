SPOOL db_3_441.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.441', 'ULB', 'AM_COMPONENTEXCHANGE, increased size of componentno');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE AM_COMPONENTEXCHANGE
MODIFY(COMPONENTNONEW NUMBER(14));


ALTER TABLE AM_COMPONENTEXCHANGE
MODIFY(COMPONENTNOOLD NUMBER(14));


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
