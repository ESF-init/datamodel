SPOOL db_3_120.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.120', 'EPA', 'Alter token size of SL_PAYMENTJOURNAL.TOKEN to 125');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PAYMENTJOURNAL MODIFY(TOKEN NVARCHAR2(125));


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
