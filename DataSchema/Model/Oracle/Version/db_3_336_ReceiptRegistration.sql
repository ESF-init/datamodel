SPOOL db_3_336.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.336', 'SVR', 'New table RM_RECEIPTREGISTRATION');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE RM_RECEIPTREGISTRATION
(
  ID       			NUMBER(9)  NOT NULL,
  TYPEID  			NUMBER(10),
  CLIENTID			NUMBER(5),
  TRANSACTIONID 	NUMBER(10),
  RECEIPTREFERENCE  VARCHAR2(13 BYTE),
  IMPORTDATETIME	DATE,
  NOTE 				VARCHAR2(500 BYTE)
);

ALTER TABLE RM_RECEIPTREGISTRATION ADD (
  FOREIGN KEY (TRANSACTIONID) 
 REFERENCES RM_TRANSACTION (TRANSACTIONID)
    ON DELETE CASCADE);

COMMENT ON TABLE RM_RECEIPTREGISTRATION IS 'This table holds information about a receipt registration.';
COMMENT ON COLUMN RM_RECEIPTREGISTRATION.ID IS 'Unique identifier of the attribute.';
COMMENT ON COLUMN RM_RECEIPTREGISTRATION.TYPEID IS 'References RM_TRANSACTIONTYPE.TYPEID.';
COMMENT ON COLUMN RM_RECEIPTREGISTRATION.CLIENTID IS 'References VARIO_CLIENT.CLIENTID.';
COMMENT ON COLUMN RM_RECEIPTREGISTRATION.TRANSACTIONID IS 'References RM_TRANSACTION.TRANSACTIONID.';
COMMENT ON COLUMN RM_RECEIPTREGISTRATION.RECEIPTREFERENCE IS 'Reference of the receipt registration e.g. barcode of the cancellation.';
COMMENT ON COLUMN RM_RECEIPTREGISTRATION.IMPORTDATETIME IS 'Timestamp when record was imported to the database.';
COMMENT ON COLUMN RM_RECEIPTREGISTRATION.NOTE IS 'Note of the receipt registration.';


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
