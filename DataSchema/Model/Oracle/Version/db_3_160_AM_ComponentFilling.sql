SPOOL db_3_160.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.160', 'BVI', 'Nullable Automat in AM_ComponentClearing');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table am_componentfilling modify automatid null;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
