SPOOL db_3_143.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.143', 'FLF', 'Added new fields to sl_transactionjournal');


ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (RESULTSOURCE  NUMBER(9));

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (TRANSACTIONTIME  NUMBER(9));

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (ROUNDTRIPTIME  NUMBER(9));

ALTER TABLE SL_TRANSACTIONJOURNAL
 ADD (JOURNEYSTARTTIME  DATE);


COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.RESULTSOURCE IS 'Source of the validation result. 0=OVS, 1=CLient.';

COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.TRANSACTIONTIME IS 'Processing time of the transaction on the client.';

COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.ROUNDTRIPTIME IS 'Round trip time from sending the request to the OVS to recieveing the result.';

COMMENT ON COLUMN SL_TRANSACTIONJOURNAL.JOURNEYSTARTTIME IS 'Start time of the journey. This won''t be updated for transfers.';

 
COMMIT;

PROMPT Done!

SPOOL OFF;