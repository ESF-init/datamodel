SPOOL db_3_508.log;

------------------------------------------------------------------------------
--Version 3.508
------------------------------------------------------------------------------

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.508', 'FRD', 'Add Numbergroup DocumentID');

-- =======[ Changes to Existing Tables ]===========================================================================

Insert into SL_NUMBERGROUP (NUMBERGROUPID, SCOPE, FORMAT, DESCRIPTION, STARTVALUE, CURRENTVALUE, RESETATENDOFMONTH, RESETATENDOFYEAR)
Values (51, 22, '[000000000000000000]', 'DocumentID', 1, 1, 0, 0);

-- =======[ Commit ]===============================================================================================


COMMIT;

PROMPT Done!

SPOOL OFF;
