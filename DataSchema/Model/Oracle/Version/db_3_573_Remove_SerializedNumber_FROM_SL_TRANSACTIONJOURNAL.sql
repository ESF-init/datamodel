SPOOL db_3_573.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.573', 'SLR', 'Remove obsolete SerializedNumber from SL_TRANSACTIONJOURNAL [3.572]');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

-- ALTER TABLE SL_TRANSACTIONJOURNAL DROP COLUMN SERIALIZEDNUMBER;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
