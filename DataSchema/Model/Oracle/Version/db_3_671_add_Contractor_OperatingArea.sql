SPOOL db_3_671.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.671', 'ULB', 'Add OperatingArea to DM_Contractor');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE DM_Contractor ADD (OperatingArea VARCHAR2(50 BYTE));

COMMENT ON COLUMN DM_Contractor.OperatingArea IS 'Contains the name of contractor OperatingArea';
-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
