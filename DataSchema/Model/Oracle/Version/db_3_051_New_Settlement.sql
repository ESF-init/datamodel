SPOOL db_3_051.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.051', 'BAW', 'Data model for transit account based settlement.');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE ACC_RevenueSettlement ADD (SettlementType NUMBER(9));
ALTER TABLE ACC_RevenueSettlement ADD (SettlementDistributionPolicy NUMBER(9));

UPDATE ACC_RevenueSettlement SET SettlementType = 1, transactioncounter = transactioncounter + 1;
UPDATE ACC_RevenueSettlement SET SettlementDistributionPolicy = 0, transactioncounter = transactioncounter + 1;
COMMIT;

ALTER TABLE ACC_RevenueSettlement MODIFY (SettlementType NOT NULL);
ALTER TABLE ACC_RevenueSettlement MODIFY (SettlementDistributionPolicy NOT NULL);

COMMENT ON COLUMN ACC_RevenueSettlement.SettlementType IS 'Type indicating the settlement algorithm used to gather the data for the settlement calculation.';
COMMENT ON COLUMN ACC_RevenueSettlement.SettlementDistributionPolicy IS 'Type indicating the distribution policy used to allocate the funds.';

-- =======[ New Tables ]===========================================================================================

CREATE TABLE ACC_SettledRevenue (
	RevenueRecognitionID NUMBER(18, 0) NOT NULL, 
	RevenueSettlementID NUMBER(18, 0) NOT NULL, 
	CONSTRAINT PK_SettledRevenue PRIMARY KEY (RevenueRecognitionID, RevenueSettlementID),
	CONSTRAINT FK_SettledRev_RevRecog FOREIGN KEY (RevenueRecognitionID) REFERENCES ACC_RevenueRecognition(RevenueRecognitionID),
	CONSTRAINT FK_SettledRev_Settlm FOREIGN KEY (RevenueSettlementID) REFERENCES ACC_RevenueSettlement(RevenueSettlementID)
);

COMMENT ON TABLE ACC_SettledRevenue IS 'Table that builds the relation between transactions that have been recorded as recognized revenue and settlement transactions.';
COMMENT ON COLUMN ACC_SettledRevenue.RevenueRecognitionID IS 'Reference to a transaction recorded as recognized revenue.';
COMMENT ON COLUMN ACC_SettledRevenue.RevenueSettlementID IS 'Reference to a settlement record.';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

CREATE TABLE SL_SettlementType
(
  EnumerationValue  NUMBER(9)           NOT NULL,
  Literal           NVARCHAR2(50)       NOT NULL,
  Description       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_SettlementType PRIMARY KEY (EnumerationValue)
);
COMMENT ON TABLE SL_SettlementType IS 'Table holding supported settlement algorithms to gather the data for the settlement calculation.';
COMMENT ON COLUMN SL_SettlementType.EnumerationValue IS 'Type identifier.';
COMMENT ON COLUMN SL_SettlementType.Literal IS 'Type literal.';
COMMENT ON COLUMN SL_SettlementType.Description IS 'Type description.';

CREATE TABLE SL_SettlementDistrPolicy
(
  EnumerationValue  NUMBER(9)           NOT NULL,
  Literal           NVARCHAR2(50)       NOT NULL,
  Description       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_SettlementDistrPolicy PRIMARY KEY (EnumerationValue)
);

COMMENT ON TABLE SL_SettlementDistrPolicy IS 'Table holding supported settlement distribution policies to allocate funds.';
COMMENT ON COLUMN SL_SettlementDistrPolicy.EnumerationValue IS 'Type identifier.';
COMMENT ON COLUMN SL_SettlementDistrPolicy.Literal IS 'Type literal.';
COMMENT ON COLUMN SL_SettlementDistrPolicy.Description IS 'Type description.';

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Trigger ]==============================================================================================

-- =======[ Data ]==================================================================================================

INSERT INTO ACC_SettledRevenue (RevenueRecognitionID, RevenueSettlementID)
SELECT RevenueRecognitionID, RevenueSettlementID FROM ACC_RevenueRecognition
WHERE RevenueSettlementID IS NOT NULL;

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT ALL
	INTO SL_SettlementType (EnumerationValue, Literal, Description) VALUES (0, 'TAB', 'Settlement by transit account.')
	INTO SL_SettlementType (EnumerationValue, Literal, Description) VALUES (1, 'Cumulative', 'Settlement over all transit accounts.')
SELECT * FROM DUAL; 

INSERT ALL
	INTO SL_SettlementDistrPolicy (EnumerationValue, Literal, Description) VALUES (0, 'Usage', 'Distribution by rides taken.')
	INTO SL_SettlementDistrPolicy (EnumerationValue, Literal, Description) VALUES (1, 'Percentage', 'Distribution by percentage.')
SELECT * FROM DUAL;

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
