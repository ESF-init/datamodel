SPOOL db_3_591.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.591', 'DST', 'Add FareScaleName to SL_TransJournHistWithCancelled');

-- =======[ Views ]================================================================================================

CREATE OR REPLACE FORCE VIEW sl_transjournhistwithcancelled (transactionid,
                                                                   operatorid,
                                                                   saleschannelid,
                                                                   transactiontype,
                                                                   TIMESTAMP,
                                                                   ticketexternalnumber,
                                                                   RESULT,
                                                                   pursecredit,
                                                                   pursebalance,
                                                                   pursecreditpretax,
                                                                   pursebalancepretax,
                                                                   price,
                                                                   validfrom,
                                                                   validto,
                                                                   numberofpersons,
                                                                   externaltransactionidentifier,
                                                                   stopno,
                                                                   stopcode,
                                                                   stopname,
                                                                   lineno,
                                                                   linecode,
                                                                   linename,
                                                                   vehiclenumber,
                                                                   devicenumber,
                                                                   devicename,
                                                                   devicedescription,
                                                                   appliedcapping,
                                                                   ticketname,
                                                                   inserttime,
                                                                   transitaccountid,
                                                                   cancellationreference,
                                                                   hascancellationreference,
                                                                   tokencardserialnumber,
                                                                   salespersonnumber,
                                                                   receiptreference,
                                                                   notes,
                                                                   transferredfromcardid,
                                                                   farescalename
                                                                  )
AS
   WITH trans AS
        (SELECT *
           FROM sl_transactionjournal
          WHERE
                --transactionjournalid NOT IN ( SELECT cancellationreference FROM sl_transactionjournal can WHERE cancellationreference IS NOT NULL) AND
                transactiontype IN
                   (
                    65,       -- Alight
                    66,       -- Boarding
                    84,       -- Transfer
                    85,       -- Use
                    99,       -- Charge
                    108,      -- Load
                    261,      -- ClientFare
                    262,      -- OpenLoopVirtualCharge
                    7000,     -- Inspection
                    9001,     -- FareMediaSale
                    9003,     -- Refund
                    9006,     -- Adjustment
                    9007,     -- DormancyFee
                    9009,     -- BalanceTransfer
                    9010,     -- ProductTransfer
                    9012,     -- StoredValuePayment
                    9013      -- StoredValueRefund
                    ))
   SELECT trans.transactionid, trans.operatorid, trans.saleschannelid,
          CASE
             WHEN trans.transactiontype = 66
             AND trans.validto IS NOT NULL
                THEN CAST (9011 AS NUMBER (9))         /* ProductActivation */
             ELSE trans.transactiontype
          END transactiontype,
          trans.devicetime TIMESTAMP,
          trans.ticketinternalnumber ticketexternalnumber,
          trans.resulttype RESULT,
          CASE
             WHEN trans.resulttype = 0                   /* Ok */
                THEN CASE
                       WHEN trans.transactiontype = 99 /*charge*/
                       AND prod.tickettype = 201               /*pursepretax*/
                          THEN CAST (NULL AS NUMBER (9))
                       ELSE trans.pursecredit
                    END
             /* PreTax charge does not get written in Product2, workaround */
          ELSE CAST (0 AS NUMBER (9))
          END pursecredit,
          CASE
             WHEN trans.resulttype = 0                  /* Ok */
                THEN CASE
                       WHEN trans.transactiontype = 99
                                                      /*charge*/
                       AND prod.tickettype = 201               /*pursepretax*/
                          THEN CAST (NULL AS NUMBER (9))
                       ELSE trans.pursebalance
                    END
             /* PreTax charge does not get written in Product2, workaround */
          ELSE CAST (0 AS NUMBER (9))
          END pursebalance,
          CASE
             WHEN trans.resulttype = 0             /* Ok */
                THEN CASE
                       WHEN trans.transactiontype =
                                               99
                                                 /*charge*/
                       AND prod.tickettype = 201               /*pursepretax*/
                          THEN trans.pursecredit
                       ELSE trans.pursecredit2
                    END
             /* PreTax charge does not get written in Product2, workaround */
          ELSE CAST (0 AS NUMBER (9))
          END pursecreditpretax,
          CASE
             WHEN trans.resulttype = 0            /* Ok */
                THEN CASE
                       WHEN trans.transactiontype =
                                              99
                                                /*charge*/
                       AND prod.tickettype = 201               /*pursepretax*/
                          THEN trans.pursebalance
                       ELSE trans.pursebalance2
                    END
             ELSE CAST (0 AS NUMBER (9))
          END pursebalancepretax,
          trans.fareamount price,
          CASE
             WHEN trans.validfrom IS NULL
             AND trans.validto IS NULL
             AND trans.DURATION > 0
                THEN trans.devicetime
             ELSE trans.validfrom
          END validfrom,
              /* Transfers don't have ValidFrom and ValidTo set, workaround */
          CASE
             WHEN trans.validto IS NULL
             AND trans.validfrom IS NOT NULL
                THEN trans.validfrom + trans.DURATION / (24 * 60 * 60)
             /* Boardings don't have ValidTo set, workaround */
          WHEN trans.validfrom IS NULL
          AND trans.validto IS NULL
          AND trans.DURATION > 0
                THEN trans.devicetime + trans.DURATION / (24 * 60 * 60)
             /* Transfers don't have ValidFrom and ValidTo set, workaround */
          ELSE trans.validto
          END validto,
          trans.groupsize numberofpersons,
          trans.externaltransactionidentifier, STOP.stopno, STOP.stopcode,
          STOP.stopname, line.lineno, trans.line linecode, line.linename,
          trans.vehiclenumber, trans.devicenumber, device.NAME devicename,
          device.description devicedescription,
          cap.cappingname appliedcapping, ticket.NAME ticketname,
          trans.inserttime, trans.transitaccountid,
          cjournal.transactionid cancellationreference,
          CASE
             WHEN cjournal.transactionid IS NULL
                THEN 0
             ELSE 1
          END hascancellationreference,
          tokencard.serialnumber tokencardserialnumber,
          trans.debtornumber salespersonnumber, sale.receiptreference, trans.notes,
          trans.transferredfromcardid,
          trans.farescalename
     FROM trans LEFT JOIN tm_ticket ticket ON ticket.ticketid = trans.ticketid
          LEFT JOIN tm_tarif tariff ON tariff.tarifid = ticket.tarifid
          LEFT JOIN vario_stop STOP
          ON STOP.netid = tariff.netid AND STOP.stopno = trans.stopnumber
          LEFT JOIN vario_line line
          ON line.netid = tariff.netid AND line.linecode = trans.line
          LEFT JOIN um_device device
          ON device.deviceno = trans.devicenumber
        AND device.deviceclassid = trans.saleschannelid
          LEFT JOIN sl_product prod ON prod.productid = trans.productid
          LEFT JOIN sl_cappingjournal cappingjournal
          ON cappingjournal.transactionjournalid = trans.transactionjournalid
        AND cappingjournal.state IN (3,                          /* applied */
                                       2                /* partiallyapplied */
                                        )
          LEFT JOIN tm_rule_capping cap
          ON cap.potnumber = cappingjournal.potid
        AND cap.tariffid = tariff.tarifid
          LEFT JOIN sl_transactionjournal cjournal
          ON trans.transactionjournalid = cjournal.cancellationreference
          LEFT JOIN sl_card tokencard ON trans.tokencardid = tokencard.cardid
          LEFT JOIN sl_sale sale ON trans.saleid = sale.saleid;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
