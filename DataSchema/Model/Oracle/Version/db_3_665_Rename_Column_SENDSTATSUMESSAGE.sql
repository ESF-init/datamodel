SPOOL db_3_665.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.665', 'TIR', 'Rename Column UM_UNIT_ACTIVITY_ALARM.Sendstatsumessage if it exists');

-- =======[ Changes to Existing Tables ]===========================================================================

DECLARE
  SENDSTATSUMESSAGE_EXISTS number := 0;  
BEGIN
  Select count(*) into SENDSTATSUMESSAGE_EXISTS
    from user_tab_cols
    where upper(column_name) = 'SENDSTATSUMESSAGE'
      and upper(table_name) = 'UM_UNIT_ACTIVITY_ALARM';

  if (SENDSTATSUMESSAGE_EXISTS = 1) then
      execute immediate 'ALTER TABLE UM_UNIT_ACTIVITY_ALARM RENAME COLUMN SENDSTATSUMESSAGE TO SENDSTATUSMESSAGE';
  end if;
end;
/

COMMIT;

PROMPT Done!

SPOOL OFF;
