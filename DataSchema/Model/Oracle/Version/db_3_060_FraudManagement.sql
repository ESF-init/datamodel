SPOOL db_3_060.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.060', 'SLR', 'Add table and column for Fraud Management');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_RULEVIOLATION ADD (PAYMENTOPTIONID NUMBER(18));

ALTER TABLE SL_RULEVIOLATION ADD (
CONSTRAINT FK_RULEVIOLATION_PAYMENTOPTION 
 FOREIGN KEY (PAYMENTOPTIONID) 
 REFERENCES SL_PAYMENTOPTION (PAYMENTOPTIONID));

COMMENT ON COLUMN SL_RULEVIOLATION.PAYMENTOPTIONID IS 'Reference to SL_PaymentOption.';


-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_CARDTORULEVIOLATION
(
    CARDTORULEVIOLATIONID       NUMBER(18,0)    NOT NULL,
    CARDID                      NUMBER(18,0)    NOT NULL,
    RULEVIOLATIONID             NUMBER(18,0)    NOT NULL,
    LASTUSER                    VARCHAR2(50 Byte)   DEFAULT 'SYS' NOT NULL,
    LASTMODIFIED                DATE            DEFAULT SYSDATE NOT NULL,
    TRANSACTIONCOUNTER          INTEGER         NOT NULL
);

ALTER TABLE SL_CARDTORULEVIOLATION
ADD (
   CONSTRAINT PK_CARDTORULEVIOLATION PRIMARY KEY (CARDTORULEVIOLATIONID)
);

ALTER TABLE SL_CARDTORULEVIOLATION ADD (
CONSTRAINT FK_CARDTORULEVIO_RULEVIOLATION
 FOREIGN KEY (RULEVIOLATIONID) 
 REFERENCES SL_RULEVIOLATION (RULEVIOLATIONID));

ALTER TABLE SL_CARDTORULEVIOLATION ADD (
CONSTRAINT FK_CARDTORULEVIOLATION_CARD
 FOREIGN KEY (CARDID) 
 REFERENCES SL_CARD (CARDID));


COMMENT ON TABLE SL_CARDTORULEVIOLATION is 'Table containing count of rule violations for card';
COMMENT ON COLUMN SL_CARDTORULEVIOLATION.CARDTORULEVIOLATIONID is 'Unique identifier of the counter';
COMMENT ON COLUMN SL_CARDTORULEVIOLATION.CARDID IS 'ID of the fraudulent card';
COMMENT ON COLUMN SL_CARDTORULEVIOLATION.RULEVIOLATIONID IS 'ID of the rule violation';
COMMENT ON COLUMN SL_CARDTORULEVIOLATION.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_CARDTORULEVIOLATION.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_CARDTORULEVIOLATION.LASTUSER IS 'Technical field: last (system) user that changed this dataset';


-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
     'SL_CARDTORULEVIOLATION');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_CARDTORULEVIOLATION');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_BRU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
