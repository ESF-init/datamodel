SPOOL db_3_046.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.046', 'DST', 'Reversed relation between Job and JobBukImport');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_jobbulkimport drop column jobid;

alter table sl_job add bulkimportid number(18,0);

alter table sl_job add constraint fk_job_jobbulkimport foreign key (bulkimportid) references sl_jobbulkimport (bulkimportid);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
