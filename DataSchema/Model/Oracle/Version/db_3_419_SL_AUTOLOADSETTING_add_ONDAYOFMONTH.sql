SPOOL db_3_419.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.419', 'dby', 'Add OnDayOfMonth Column to SL_AutoloadSetting');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_AUTOLOADSETTING ADD PeriodDayOfMonth NUMBER(9) NULL;
COMMENT ON COLUMN SL_AUTOLOADSETTING.PeriodDayOfMonth IS 'Field to capture on which day of the month an autoload should occur';

ALTER TABLE SL_AUTOLOADSETTING ADD PeriodRunDate DATE NULL;
COMMENT ON COLUMN SL_AUTOLOADSETTING.PeriodRunDate IS 'Field to capture next day an autoload should occur';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
