SPOOL db_3_226.log;


INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.226', 'SOE', 'Added MinimumTerm to SL_PRODUCT.');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_PRODUCT
ADD MinimumTerm NUMBER (9);

-- =======[ Commit ]===============================================================================================

COMMENT ON COLUMN SL_PRODUCT.MinimumTerm IS 'MinimumTerm for product';
COMMIT;

PROMPT Done!

SPOOL OFF;