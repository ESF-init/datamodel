SPOOL db_3_379.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.379', 'SLR', 'Add column to SL_CustomerAccount.');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CustomerAccount ADD (TOTALLOGINATTEMPTS NUMBER(9)               DEFAULT 0                     NOT NULL);
COMMENT ON COLUMN SL_CustomerAccount.TOTALLOGINATTEMPTS IS 'Total log in attempts since last successful log in or password reset.';



---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
