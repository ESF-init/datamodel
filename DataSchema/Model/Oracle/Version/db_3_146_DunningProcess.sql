SPOOL db_3_146.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.146', 'BVI', 'Change DataType Number(18) to nvarchar(50) of SL_DunningProcess.ProcessNumber.');

alter table sl_dunningprocess 
add (
    Processnumber2 NVArchar2(50)
);

update sl_dunningprocess set Processnumber2 = to_char(Processnumber), Transactioncounter = Transactioncounter+1;

alter table sl_dunningprocess 
drop column Processnumber;

alter table sl_dunningprocess 
add (
    Processnumber NVArchar2(50)
);

update sl_dunningprocess set Processnumber = Processnumber2, Transactioncounter = Transactioncounter+1;

alter table sl_dunningprocess 
drop column Processnumber2;

alter table sl_dunningprocess 
modify (
    Processnumber NVArchar2(50) NOT NULL
);

COMMENT ON COLUMN SL_DUNNINGPROCESS.PROCESSNUMBER is 'A number that defines the actions belong to one process';

-- =======[ Commit ]===============================================================================================
COMMIT;

PROMPT Done!

SPOOL OFF;
