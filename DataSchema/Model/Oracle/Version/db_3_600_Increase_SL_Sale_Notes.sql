SPOOL db_3_600.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.600', 'DST', 'Increase size of SL_Sale.Notes to 500');

ALTER TABLE SL_SALE MODIFY NOTES NVARCHAR2(500);

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
