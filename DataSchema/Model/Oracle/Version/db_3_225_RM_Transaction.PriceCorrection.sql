SPOOL db_3_225.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.225', 'ULB', 'RM_Transaction.PriceCorrection');


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE RM_transaction
ADD PriceCorrection NUMBER (9);

COMMENT ON COLUMN RM_transaction.PriceCorrection IS 'Price changed after initial sale (transfer, etc)';


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
