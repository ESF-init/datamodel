SPOOL db_3_451.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.451', 'BVI', 'New Column RelationNumber on SL_ProductRelation');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_ProductRelation
 ADD (RelationNumber  NVARCHAR2(200));

COMMENT ON COLUMN SL_ProductRelation.RelationNumber IS 'The value of the tm_farematrixentry.Description';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
