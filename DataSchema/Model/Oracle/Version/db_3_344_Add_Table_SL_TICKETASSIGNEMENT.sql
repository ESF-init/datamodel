SPOOL db_3_344.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.344', 'STV', 'Create Table SL_TICKETASSIGNMENT and SL_TICKETASSIGNEMENTSTATUS');

-- =======[ New Table  ]===========================================================================
-- SL_TICKETASSIGNEMENTSTATUS

CREATE TABLE SL_TICKETASSIGNEMENTSTATUS
(
  ENUMERATIONVALUE  NUMBER(9)                   NOT NULL,
  LITERAL           NVARCHAR2(50)               NOT NULL,
  DESCRIPTION       NVARCHAR2(50)               NOT NULL
);
 
COMMENT ON TABLE SL_TICKETASSIGNEMENTSTATUS IS 'Enumeration of all valid ticket assignment statuses.';
COMMENT ON COLUMN SL_TICKETASSIGNEMENTSTATUS.ENUMERATIONVALUE IS 'Unique id of the icket assignment status';
COMMENT ON COLUMN SL_TICKETASSIGNEMENTSTATUS.LITERAL IS 'Name of ticket assignment status';

INSERT INTO SL_TICKETASSIGNEMENTSTATUS(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (0, 'Unassigned', 'Ticket assignment is unassigned.');
INSERT INTO SL_TICKETASSIGNEMENTSTATUS(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (1, 'Active', 'Ticket assignment is active.');
INSERT INTO SL_TICKETASSIGNEMENTSTATUS(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (2, 'Hold', 'Ticket assignment is on hold.');
INSERT INTO SL_TICKETASSIGNEMENTSTATUS(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (3, 'Blocked', 'Ticket assignment is blocked.');
INSERT INTO SL_TICKETASSIGNEMENTSTATUS(ENUMERATIONVALUE, LITERAL, DESCRIPTION) 
VALUES (4, 'Cancelled', 'Ticket assignment is cancelled.');

--
-- SL_TICKETASSIGNMENT
--
CREATE TABLE SL_TICKETASSIGNMENT (
	TICKETASSIGNMENTID 		NUMBER(18)			NOT NULL,
	ASSIGNMENTNAME				NVARCHAR2(100)		NULL,
	CARDID						NUMBER(18)	    	NOT NULL,
	TICKETINTERNALNUMBER		NUMBER(10,0)		NULL,
	TICKETTYPEID				NUMBER(10,0)		NULL,
	STATUS					    NUMBER(10,0)        DEFAULT 0       	NOT NULL,
	ORIGIN						NUMBER(10,0)		NULL,
	DESTINATION					NUMBER(10,0)		NULL,
	QUANTITY					NUMBER(10,0)		NOT NULL,	
	TICKETSREMAINING			NUMBER(10,0)		NOT NULL,	
	INSTITUTIONID 				NUMBER(18,0)		NOT NULL,
	LASTUSER					NVARCHAR2(50)		DEFAULT 'SYS'		NOT NULL,
	LASTMODIFIED				DATE				DEFAULT sysdate		NOT NULL,
	TRANSACTIONCOUNTER			INTEGER				NOT NULL,
	DATAROWCREATIONDATE			DATE				DEFAULT sysdate	
);
ALTER TABLE SL_TICKETASSIGNMENT ADD CONSTRAINT PK_TICKETASSIGNMENT PRIMARY KEY (TICKETASSIGNMENTID);
ALTER TABLE SL_TICKETASSIGNMENT ADD CONSTRAINT FK_TICKETASSIGNEMENT_CARD FOREIGN KEY (CARDID) REFERENCES SL_CARD (CARDID);
ALTER TABLE SL_TICKETASSIGNMENT ADD CONSTRAINT FK_TICKETASGMNT_ORGANIZATION FOREIGN KEY (INSTITUTIONID) REFERENCES SL_ORGANIZATION (ORGANIZATIONID);

COMMENT ON TABLE SL_TICKETASSIGNMENT IS 'Assignment of tickets or tickettype to a card, quantity of 10 means the customer can buy 10 of these tickets a month';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.TICKETASSIGNMENTID IS 'Unique ID of the SL_TICKETASSIGNMENT table.';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.TICKETINTERNALNUMBER IS 'References TM_TICKET';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.TICKETTYPEID IS 'References TM_TICKET via ticket type, can be one or more ticket';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.STATUS IS 'Status of the assignement, references SL_TICKETASSIGNEMENTSTATUS';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.ORIGIN IS 'Origin No of the relation';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.DESTINATION IS 'Destination No of the relation';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.QUANTITY IS 'Amount of tickets available pro month';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.TICKETSREMAINING IS 'Amount of tickets used for the month';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.INSTITUTIONID IS 'References SL_ORGANIZATION';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.DATAROWCREATIONDATE IS 'Technical field: initial creation data of the data row.';

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(10) OF VARCHAR2(100);
  table_names table_names_array := table_names_array('SL_TICKETASSIGNMENT');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
