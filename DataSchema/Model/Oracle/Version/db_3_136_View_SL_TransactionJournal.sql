SPOOL db_3_136.log;

-- IMPORTANT: REQUIRES ITD UPDATE
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.136', 'DST', 'Refresh for VIEW_SL_TRANSACTIONJOURNAL');

-- =======[ Views ]================================================================================================

CREATE OR REPLACE VIEW VIEW_SL_TRANSACTIONJOURNAL
(
  TRANSACTIONJOURNALID,
  TRANSACTIONID,
  DEVICETIME,
  BOARDINGTIME,
  OPERATORID,
  CLIENTID,
  LINE,
  COURSENUMBER,
  ROUTENUMBER,
  ROUTECODE,
  DIRECTION,
  ZONE,
  SALESCHANNELID,
  DEVICENUMBER,
  VEHICLENUMBER,
  MOUNTINGPLATENUMBER,
  DEBTORNUMBER,
  GEOLOCATIONLONGITUDE,
  GEOLOCATIONLATITUDE,
  STOPNUMBER,
  TRANSITACCOUNTID,
  PRINTEDNUMBER,
  SERIALNUMBER,
  FAREMEDIAID,
  FAREMEDIATYPE,
  PRODUCTID,
  TICKETID,
  FAREAMOUNT,
  CUSTOMERGROUP,
  TRANSACTIONTYPE,
  TRANSACTIONNUMBER,
  RESULTTYPE,
  FILLLEVEL,
  PURSEBALANCE,
  PURSECREDIT,
  GROUPSIZE,
  VALIDFROM,
  VALIDTO,
  DURATION,
  TARIFFDATE,
  TARIFFVERSION,
  TICKETINTERNALNUMBER,
  WHITELISTVERSION,
  CANCELLATIONREFERENCE,
  CANCELLATIONREFERENCEGUID,
  LASTUSER,
  LASTMODIFIED,
  TRANSACTIONCOUNTER,
  WHITELISTVERSIONCREATED,
  PROPERTIES,
  SALEID,
  TRIPTICKETINTERNALNUMBER,
  TICKETNAME,
  SHIFTBEGIN,
  COMPLETIONSTATE,
  RESPONSESTATE,
  PRODUCTID2,
  PURSEBALANCE2,
  PURSECREDIT2,
  CREDITCARDAUTHORIZATIONID,
  TRIPCOUNTER,
  INSERTTIME,
  ISVOICEENABLED,
  ISTRAINING
)
AS
with
tariff as (
select tarifid tariffid, netid from (
select tarifid, netid, max(version), max(validfrom) from tm_tarif
group by tarifid, netid having tarifid in (select tariffid from tm_tariff_release where 
  releasetype = decode((select 1 from sl_configurationdefinition where name = 'UseTestTariffArchive' and lower(defaultvalue) like 'f%'), 1, 2, 1) /* returns 1 (testrelease) if UseTestTariffArchive is set to true, otherwise 2 (productiverelease) */
  and deviceclassid = 70)
order by max(validfrom) desc, max(version) desc
) where rownum = 1)
SELECT sl_transactionjournal.transactionjournalid,
          sl_transactionjournal.transactionid,
          sl_transactionjournal.devicetime,
          sl_transactionjournal.boardingtime,
          sl_transactionjournal.operatorid, sl_transactionjournal.clientid,
          sl_transactionjournal.line, sl_transactionjournal.coursenumber,
          sl_transactionjournal.routenumber, sl_transactionjournal.routecode,
          sl_transactionjournal.direction, sl_transactionjournal.ZONE,
          sl_transactionjournal.saleschannelid,
          sl_transactionjournal.devicenumber,
          sl_transactionjournal.vehiclenumber,
          sl_transactionjournal.mountingplatenumber,
          sl_transactionjournal.debtornumber,
          sl_transactionjournal.geolocationlongitude,
          sl_transactionjournal.geolocationlatitude,
          sl_transactionjournal.stopnumber,
          sl_transactionjournal.transitaccountid,
          sl_card.printednumber,
          sl_card.serialnumber,
          sl_transactionjournal.faremediaid,
          DECODE (sl_transactionjournal.faremediatype,
                  215, 'ClosedLoop',
                  217, 'OpenLoop',
                  220, 'Barcode',
                  224, 'VirtualCardGoogle',
                  225, 'VirtualCardApple',
                  226, 'ExternalUidCard',
                  227, 'ExternalBarcode',
                  sl_transactionjournal.faremediatype
                 ) AS faremediatype,
          sl_transactionjournal.productid, sl_transactionjournal.ticketid,
          sl_transactionjournal.fareamount,
          NVL(tm_attributevalue.name, sl_transactionjournal.customergroup) AS customergroup,
          sl_transactionjournal.transactiontype,
          sl_transactionjournal.transactionnumber,
          sl_transactionjournal.resulttype, sl_transactionjournal.filllevel,
          sl_transactionjournal.pursebalance,
          sl_transactionjournal.pursecredit, sl_transactionjournal.groupsize,
          sl_transactionjournal.validfrom, sl_transactionjournal.validto,
          sl_transactionjournal.DURATION, sl_transactionjournal.tariffdate,
          sl_transactionjournal.tariffversion,
          sl_transactionjournal.ticketinternalnumber,
          sl_transactionjournal.whitelistversion,
          sl_transactionjournal.cancellationreference,
          sl_transactionjournal.cancellationreferenceguid,
          sl_transactionjournal.lastuser, sl_transactionjournal.lastmodified,
          sl_transactionjournal.transactioncounter,
          sl_transactionjournal.whitelistversioncreated,
          sl_transactionjournal.properties, sl_transactionjournal.saleid,
          sl_transactionjournal.tripticketinternalnumber,
          tm_ticket.NAME AS ticketname, sl_transactionjournal.shiftbegin,
          DECODE (sl_transactionjournal.completionstate,
                  0, 'Reported',
                  1, 'Open',
                  2, 'Completed',
                  sl_transactionjournal.completionstate
                 ) AS completionstate,
          DECODE (sl_transactionjournal.responsestate,
                  0, 'Unknown',
                  1, 'Online',
                  2, 'Offline',
                  3, 'Error',
                  sl_transactionjournal.responsestate
                 ) AS responsestate,
          sl_transactionjournal.productid2,
          sl_transactionjournal.pursebalance2,
          sl_transactionjournal.pursecredit2,
          sl_transactionjournal.creditcardauthorizationid,
          sl_transactionjournal.tripcounter, sl_transactionjournal.inserttime,
          sl_card.isvoiceenabled, sl_card.istraining
     FROM sl_transactionjournal 
     INNER JOIN tm_ticket
          ON (sl_transactionjournal.ticketid = tm_ticket.ticketid)
     LEFT OUTER JOIN sl_card
          ON sl_transactionjournal.transitaccountid = sl_card.cardid
     left join tm_attribute on tm_attribute.tarifid in (select tariffid from tariff) and tm_attribute.attribclassid=14
     left join tm_attributevalue on tm_attributevalue.attributeid = tm_attribute.attributeid and tm_attributevalue.number_ = sl_transactionjournal.customergroup
;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
