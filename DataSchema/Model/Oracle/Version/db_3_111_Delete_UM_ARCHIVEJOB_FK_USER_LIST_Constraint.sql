SPOOL db_3_111.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.111', 'JSB', 'Delete USER_LIST constraint in table UM_ARCHIVEJOB');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE UM_ARCHIVEJOB
DROP CONSTRAINT UM_ARCHIVEJOB_FK_USER_LIST;

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
