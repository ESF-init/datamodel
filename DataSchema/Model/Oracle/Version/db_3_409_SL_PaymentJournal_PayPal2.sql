SPOOL db_3_409.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.409', 'DBY', 'Adding Additional Fields to SL_PaymentJournal for Paypal express checkout');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_PAYMENTJOURNAL ADD PayPalCaptureRequestId NVARCHAR2(200) NULL;
COMMENT ON COLUMN SL_PAYMENTJOURNAL.PayPalCaptureRequestId IS 'PayPal parameter that identifies an authorization. More than one authorization ID can be associated with an order';

ALTER TABLE SL_PAYMENTJOURNAL ADD PayPalCaptureId NVARCHAR2(200) NULL;
COMMENT ON COLUMN SL_PAYMENTJOURNAL.PayPalCaptureId IS 'Reply message from capture request during paypal transaction';

ALTER TABLE SL_PAYMENTJOURNAL ADD PayPalCaptureToken NVARCHAR2(200) NULL;
COMMENT ON COLUMN SL_PAYMENTJOURNAL.PayPalCaptureToken IS 'The request token from the capture you want to capture or refund';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
