SPOOL db_3_180.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.180', 'FLF', 'Add Column ALLOWACCUMULATION to  TM_BUSINESSRULETYPE');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE TM_BUSINESSRULETYPE
 ADD (ALLOWACCUMULATION  NUMBER(2)                  DEFAULT 0                     NOT NULL);


COMMENT ON COLUMN TM_BUSINESSRULETYPE.ALLOWACCUMULATION IS 'Allows business rule variable results to be accumulated in lists.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
