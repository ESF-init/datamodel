SPOOL db_3_571.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.571', 'EMN', 'Adding foreign key to SL_OrderDetailToCard.ReplacedOrderDetailToCardID column');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_ORDERDETAILTOCARD ADD CONSTRAINT FK_REPLACEDORDERDETAILTOCARD FOREIGN KEY (REPLACEDORDERDETAILTOCARDID) 
REFERENCES SL_ORDERDETAILTOCARD(ORDERDETAILTOCARDID);



-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
