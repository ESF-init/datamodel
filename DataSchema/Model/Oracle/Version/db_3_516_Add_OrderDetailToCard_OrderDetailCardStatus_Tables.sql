INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.516', 'EMN', 'Add ORDERDETAILTOCARD table and CARDFULFILLMENTSTATUS enum table');

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_ORDERDETAILTOCARD
(
  ORDERDETAILID     NUMBER(18)                      NOT NULL,
  CARDID            NUMBER(18)                      NULL,
  JOBID             NUMBER(18)                      NULL,
  STATUS            NUMBER(9)                       NOT NULL
);

CREATE UNIQUE INDEX UC_ORDERDETAILTOCARD ON SL_ORDERDETAILTOCARD(ORDERDETAILID, CARDID, JOBID);

COMMENT ON TABLE SL_ORDERDETAILTOCARD IS 'Contains relationship between order details, cards and jobs with the status of fulfillment.';
COMMENT ON COLUMN SL_ORDERDETAILTOCARD.ORDERDETAILID IS 'ID of the Order detail (SL_ORDERDETAIL).';
COMMENT ON COLUMN SL_ORDERDETAILTOCARD.CARDID IS 'ID of the Card (SL_CARD).';
COMMENT ON COLUMN SL_ORDERDETAILTOCARD.JOBID IS 'ID of the Job (SL_JOB).';
COMMENT ON COLUMN SL_ORDERDETAILTOCARD.STATUS IS 'Status of this indivdual fulfillment (SL_CARDFULFILLMENTSTATUS).';

---------------------------------------------------------------------------------------------------------------

CREATE TABLE SL_CARDFULFILLMENTSTATUS
(
  EnumerationValue  NUMBER(9)           NOT NULL,
  Literal           NVARCHAR2(50)       NOT NULL,
  Description       NVARCHAR2(50)       NOT NULL,
  CONSTRAINT PK_ORDERDETAILTOCARDSTATUS PRIMARY KEY (EnumerationValue)
);

COMMENT ON TABLE SL_CARDFULFILLMENTSTATUS IS 'Enumeration values for SL_ORDERDETAILTOCARD.Status.';
COMMENT ON COLUMN SL_CARDFULFILLMENTSTATUS.EnumerationValue IS 'Status identifier.';
COMMENT ON COLUMN SL_CARDFULFILLMENTSTATUS.Literal IS 'Status literal.';
COMMENT ON COLUMN SL_CARDFULFILLMENTSTATUS.Description IS 'Status description.';


INSERT INTO SL_CARDFULFILLMENTSTATUS (EnumerationValue, Literal, Description) VALUES (0, 'Unknown', 'Unknown');
INSERT INTO SL_CARDFULFILLMENTSTATUS (EnumerationValue, Literal, Description) VALUES (1, 'Open', 'Open');
INSERT INTO SL_CARDFULFILLMENTSTATUS (EnumerationValue, Literal, Description) VALUES (2, 'Processed', 'Processed');
INSERT INTO SL_CARDFULFILLMENTSTATUS (EnumerationValue, Literal, Description) VALUES (3, 'ReplacementRequested', 'Replacement Requested');
INSERT INTO SL_CARDFULFILLMENTSTATUS (EnumerationValue, Literal, Description) VALUES (4, 'Replaced', 'Replaced');

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;