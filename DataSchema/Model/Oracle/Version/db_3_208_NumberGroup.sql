SPOOL db_3_208.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.208', 'DST', 'Added new NumberGroupScopes.');

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_NUMBERGROUPSCOPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (14, 'Dunning', 'Dunning');
INSERT INTO SL_NUMBERGROUPSCOPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (15, 'VdvBer', 'VdvBer');
INSERT INTO SL_NUMBERGROUPSCOPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (16, 'Primary Account Number Youth', 'Primary Account Number Youth');
INSERT INTO SL_NUMBERGROUPSCOPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) VALUES (17, 'Primary Account Number Honored Citizen', 'Primary Account Number Honored Citizen');


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
