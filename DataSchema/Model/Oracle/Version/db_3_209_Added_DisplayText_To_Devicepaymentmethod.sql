SPOOL db_3_209.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.209', 'EPA', 'Added new column DISPLAYTEXT to RM_DEVICEPAYMENTMETHOD');

alter table RM_DEVICEPAYMENTMETHOD
add DISPLAYTEXT NVARCHAR2(50);


COMMENT ON COLUMN RM_DEVICEPAYMENTMETHOD.DISPLAYTEXT IS 'Optional field that holds an alternative name to display for the device payment method.';

COMMIT;

PROMPT Done!

SPOOL OFF;
