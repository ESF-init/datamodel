SPOOL db_3_008.log

INSERT INTO vario_dmodellversion (rundate,dmodellno,responsible,annotation)
VALUES (SYSDATE,'3.008','BVI','Added Primary Key to PK_CreditScreening');


---Start adding schema changes here

ALTER TABLE PP_CREDIT_SCREENING_FEATURE Drop 
  CONSTRAINT CS_FEATURE_CS_FK;

ALTER TABLE PP_Credit_Screening DROP constraint CREDIT_SCREEING_UNIQUE;

ALTER TABLE PP_Credit_Screening
 ADD Constraint PK_Credit_Screening PRIMARY KEY (CreditScreeningID);
 
 ALTER TABLE PP_CREDIT_SCREENING_FEATURE ADD (
  CONSTRAINT CS_FEATURE_CS_FK 
  FOREIGN KEY (CREDITSCREENINGID) 
  REFERENCES PP_CREDIT_SCREENING (CREDITSCREENINGID)
  ENABLE VALIDATE);

---End adding schema changes



COMMIT;

PROMPT Done!

SPOOL OFF