SPOOL db_3_476.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.476', 'HNI', 'Added column OptionalFeatureData to SL_BookingItem');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_BOOKINGITEM ADD OPTIONALFEATUREDATA CLOB;

COMMENT ON COLUMN SL_BOOKINGITEM.OPTIONALFEATUREDATA IS 'Optional feature data for the Regiomove app developer.';

---End adding schema changes 

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
