SPOOL db_3_073.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.073', 'AMA', 'Blocking VDV SAM Module');

-- Start adding schema changes here

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE UM_SAM ADD
(
	IsBlocked			Number(1)   DEFAULT 0 NOT NULL,
    LossDate        	DATE,
    LossDetectionDate   DATE,
    BlockingReason	    NVARCHAR2(2000)
);

COMMENT ON COLUMN UM_SAM.IsBlocked IS 'Indicates whether SAM is blocked.';
COMMENT ON COLUMN UM_SAM.LossDate IS 'Date of losing SAM Module.';
COMMENT ON COLUMN UM_SAM.LossDetectionDate IS 'Detection date of losing SAM Module.';
COMMENT ON COLUMN UM_SAM.BlockingReason IS 'Reason of blocking SAM Module.';


-- =======[ New Tables ]===========================================================================================

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Sequence ]---------------------------------------------------------------------------------------------

-- =======[ Trigger ]==============================================================================================

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
