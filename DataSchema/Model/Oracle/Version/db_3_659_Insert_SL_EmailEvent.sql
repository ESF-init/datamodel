SPOOL db_3_659.log;


------------------------------------------------------------------------------
--Version 3.659
------------------------------------------------------------------------------
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.659', 'DCS', 'AccountConfirmationNoPassword');


-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AccountConfirmationNoPassword','Set initial password', 0, 'AccountMailMerge', 102);

COMMIT;

PROMPT Done!

SPOOL OFF;
