SPOOL db_3_606.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.606', 'HNI', 'Sequence and Trigger for Tenant');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE SL_TENANT
    ADD LastUser NVARCHAR2(50) DEFAULT 'SYS' NOT NULL;
ALTER TABLE SL_TENANT
    ADD LastModified DATE DEFAULT sysdate NOT NULL;
ALTER TABLE SL_TENANT
    ADD TransactionCounter INTEGER NOT NULL;

COMMENT ON COLUMN SL_TENANT.LASTUSER IS 'Technical field: last (system) user that changed this dataset';
COMMENT ON COLUMN SL_TENANT.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_TENANT.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';

-- =======[ Sequences ]============================================================================================

create sequence SL_TenantHistory_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;
create sequence SL_Tenant_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;
create sequence SL_TenantPerson_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- =======[ Trigger ]==============================================================================================
CREATE OR REPLACE TRIGGER SL_Tenant_BRI before insert on SL_Tenant for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_Tenant_BRU before update on SL_Tenant for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/


CREATE OR REPLACE TRIGGER SL_TenantPerson_BRI before insert on SL_TenantPerson for each row begin	:new.TransactionCounter := dbms_utility.get_time; end;
/

CREATE OR REPLACE TRIGGER SL_TenantPerson_BRU before update on SL_TenantPerson for each row begin	if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, 'Concurrency Failure' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;
/
-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------


---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
