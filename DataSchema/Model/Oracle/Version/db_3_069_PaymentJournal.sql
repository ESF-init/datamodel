SPOOL db_3_069.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.069', 'DST', 'Added CardID to PaymentJournal for stored value payments.');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_paymentjournal add cardid NUMBER(18);
alter table sl_paymentjournal add constraint fk_paymentjournal_card foreign key (cardid) references sl_card (cardid);

COMMENT ON COLUMN sl_paymentjournal.cardid IS 'Reference to card for stored value payments.';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
