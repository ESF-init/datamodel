SPOOL db_3_193.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.193', 'JGI', 'Additional Columns for PP_CONTRACT');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE PP_CONTRACT ADD (CONTRACTLOCK  NUMBER(1) DEFAULT 0);

ALTER TABLE PP_CONTRACT ADD (CONTRACTLOCKDATE  DATE);

COMMENT ON COLUMN PP_CONTRACT.CONTRACTLOCK IS 'Flag, which indicates a contract is locked';
COMMENT ON COLUMN PP_CONTRACT.CONTRACTLOCKDATE IS 'Date, when the contract has been locked';

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
