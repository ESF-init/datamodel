SPOOL db_3_507.log;

------------------------------------------------------------------------------
--Version 3.507
------------------------------------------------------------------------------
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.507', 'FRD', 'Add NumberGroupScope DocumentId');

-- =======[ Enumerations ]===========================================================================

Insert into SL_NUMBERGROUPSCOPE (ENUMERATIONVALUE, LITERAL, DESCRIPTION) Values (22, 'DocumentId', 'Unique DocumentId for Document Management.');

-- =======[ Commit ]===============================================================================================


COMMIT;

PROMPT Done!

SPOOL OFF;
