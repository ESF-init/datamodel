SPOOL db_3_569.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.569', 'EMN', 'Add Coulmn HasSecondaryPrint to SL_OrderDetail');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TRIGGER SL_ORDERDETAIL_BRU DISABLE;

ALTER TABLE SL_ORDERDETAIL
ADD HASSECONDARYPRINT Number(1) DEFAULT 0;

ALTER TRIGGER SL_ORDERDETAIL_BRU ENABLE;

COMMENT ON COLUMN SL_ORDERDETAIL.HASSECONDARYPRINT IS 'Flag for determining printing required';

COMMIT;

PROMPT Done!

SPOOL OFF;