SPOOL db_3_651.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.651', 'SOE', 'New EventType AutoloadCreatedForPass inserted in SL_EmailEvent.');

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadCreatedForPass','Autoload created for pass', -1, 'AutoloadMailMerge', 98);
INSERT INTO SL_EmailEvent (EmailEventID, EventName, Description, IsActive, ValidFormSchemaName, EnumerationValue) 
VALUES (SL_EmailEvent_SEQ.NEXTVAL, 'AutoloadCreatedForPassFromNotPrimary','Autoload created for pass from not primary', -1, 'AutoloadMailMerge', 99);

COMMIT;

PROMPT Done!

SPOOL OFF;
