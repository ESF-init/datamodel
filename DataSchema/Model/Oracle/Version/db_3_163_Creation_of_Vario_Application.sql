SPOOL db_3_163.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.163', 'SOE', 'Creation of Vario_Application');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE VARIO_APPLICATION
(
    APPLICATIONID   NUMBER(18,0)    NOT NULL,
    NAME    NVARCHAR2(512)  NOT NULL,
	CONSTRAINT PK_APPLICATIONID PRIMARY KEY (APPLICATIONID)
);

COMMENT ON TABLE VARIO_APPLICATION is 'Table containing information about application';
COMMENT ON COLUMN VARIO_APPLICATION.APPLICATIONID is 'Unique identifier of the application';
COMMENT ON COLUMN VARIO_APPLICATION.NAME IS 'Name of the application';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
