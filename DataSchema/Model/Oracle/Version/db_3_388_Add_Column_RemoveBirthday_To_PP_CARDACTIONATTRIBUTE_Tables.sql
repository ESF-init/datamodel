SPOOL db_3_388.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.388', 'MEA', 'Add Column REMOVEBIRTHDAY_to_PP_CARDACTIONATTRIBUTE');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE PP_CARDACTIONATTRIBUTE ADD REMOVEBIRTHDAY NUMBER(1) DEFAULT 0;

COMMIT;

PROMPT Done!

SPOOL OFF;