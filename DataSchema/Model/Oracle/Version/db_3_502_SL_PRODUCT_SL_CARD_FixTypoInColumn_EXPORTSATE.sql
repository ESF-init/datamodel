SPOOL db_3_502.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.502', 'JED', 'Fix Typo in Column EXPORTSATE');

-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CARD RENAME COLUMN EXPORTSATE TO EXPORTSTATE;

ALTER TABLE SL_PRODUCT RENAME COLUMN EXPORTSATE TO EXPORTSTATE;


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
