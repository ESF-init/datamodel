SPOOL db_3_635.log;
INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.635', 'ESF', 'Modify SL_TicketAssignment');
-- =======[ Changes on existing tables ]===========================================================================
ALTER TABLE SL_TICKETASSIGNMENT MODIFY INSTITUTIONID NULL;
ALTER TABLE SL_TICKETASSIGNMENT ADD CONTRACTID NUMBER(10) NULL;
ALTER TABLE SL_TICKETASSIGNMENT ADD CONSTRAINT FK_TICKETASGMNT_CONTRACT FOREIGN KEY (CONTRACTID) REFERENCES SL_CONTRACT (CONTRACTID);
COMMENT ON COLUMN SL_TICKETASSIGNMENT.INSTITUTIONID IS 'References SL_ORGANIZATION (deprecated)';
COMMENT ON COLUMN SL_TICKETASSIGNMENT.CONTRACTID IS 'References SL_CONTRACT';
---End adding schema changes 
-- =======[ Commit ]===============================================================================================

COMMIT;
PROMPT Done!
SPOOL OFF;