SPOOL db_3_195.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.195', 'TIR', 'added missing MOBILEflow views');

CREATE OR REPLACE FORCE VIEW VIEW_UM_CURRELEASARCHIVES AS 
  select distinct a.ARCHIVEID, 
        j.archivejobid, 
        dt.ARCHIVEDISTRIBUTIONTASKID, 
        j.deviceclassid,
        a.filename, 
        j.FILENAME as title, 
        j.Clientid, 
        t.TYPENAME as ArchiveType,
        c.shortNAME as DeviceClassShort, 
        dt.STARTED, 
        j.USEALL
from UM_CURRENTDISTRIBUTEDARCHIVE cur
inner join UM_ARCHIVE a on a.archiveid=cur.archiveid
inner join um_archivejob j on j.archiveid=a.archiveid
inner join um_archivedistributiontask  dt on dt.ARCHIVEJOBID = j.ARCHIVEJOBID
inner join UM_TYPEOFARCHIVE t on a.TYPEOFARCHIVEID = t.TYPEOFARCHIVEID
inner join vario_deviceclass c on j.DEVICECLASSID = c.deviceclassid;

CREATE OR REPLACE FORCE VIEW VIEW_UM_TASKDISTRIBUTEDTOFTP AS 
  select vl.ARCHIVEDISTRIBUTIONTASKID, vl.LSSTATE, vl.LOADEDFROM, d.deviceid, d.NAME as FTPServerName 
from UM_Device d
inner join VIEW_LSOVERVIEWTOTASK vl on vl.DEVICEID = d.deviceid
where d.deviceclassid =-1;

CREATE MATERIALIZED VIEW VIEW_UM_CURRELEASLOADINGSTAT 
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
USING INDEX
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
AS select distinct r.ARCHIVEID, l.deviceid as deviceid, l.lsstate, l.LOADEDFROM from view_loadingstat_overview l, VIEW_UM_CurReleasArchives r 
where r.archiveid=l.archiveid;

CREATE OR REPLACE FORCE VIEW VIEW_UM_CURRELEASTASKTODEVICE AS
select c.ARCHIVEDISTRIBUTIONTASKID,      
      c.Archivejobid,
      c.archiveid,
        c.deviceid,
        c.deviceno,
        c.unitid,         
        c.Unitno,
        c.visibleunitno,
        c.lsstate,
        c.loadedfrom
  from(
 SELECT  distinct cda.ARCHIVEDISTRIBUTIONTASKID
          ,au.ARCHIVEJOBID
          ,cda.archiveid 
           ,d.DEVICEID
           ,d.DEVICENO
           ,u.unitid
           ,u.unitNo
           ,u.VISIBLEUNITNO
         ,l.lsstate
         ,l.loadedfrom
   FROM VIEW_UM_CurReleasArchives cda
   INNER JOIN UM_ARCHIVEJOBDISTRIBUTEDTOUNIT au ON cda.ARCHIVEJOBID = au.ARCHIVEJOBID
   inner join um_unit u on au.unitid=u.unitid
   inner JOIN um_device d ON d.unitid = u.unitid AND d.deviceclassid = cda.deviceclassid
  left join VIEW_UM_CURRELEASLOADINGSTAT l on (l.archiveid=cda.archiveid and l.deviceid=d.deviceid)
  ) c;

CREATE OR REPLACE FORCE VIEW VIEW_UM_CURRELEASCOUNTDEVICES AS 
select c.ARCHIVEDISTRIBUTIONTASKID,
	count(c.deviceid) as targetdevices,
	sum(decode(c.lsstate, 0,1, 1,1, 9,1, 10,1, 11,1,  0)) as loadeddevices
 FROM view_um_curReleasTaskToDevice c
 group by  c.ARCHIVEDISTRIBUTIONTASKID;
 
CREATE MATERIALIZED VIEW VIEW_UM_CURRELEASES 
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
USING INDEX
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
  AS SELECT DISTINCT cur.ARCHIVEID,
                cur.archivejobid,
                cur.ARCHIVEDISTRIBUTIONTASKID,
                cur.filename,
                cur.title,
                cur.Clientid,
                cur.ArchiveType,              
                cur.Deviceclassid,
                cur.DeviceClassShort,
                cur.STARTED,
                cur.USEALL,
                td.TargetFTPServer,
                td.loadedFTPServer,
                u.TargetUnits,
                cast(nvl(cu.targetdevices,'0')as number(8,0)) as targetdevices,
                cast(nvl(cu.loadeddevices,'0')as number(8,0)) as loadeddevices
FROM VIEW_UM_CurReleasArchives cur ,
  (SELECT ARCHIVEDISTRIBUTIONTASKID,
          cast( count(ftpservername) as number(8,0)) AS TargetFTPServer ,
          cast(sum(decode(lsstate, 0,1, 1,1, 9,1, 10,1, 11,1,  0)) as number(8,0)) AS loadedFTPServer
   FROM view_um_TaskDistributedToFTP
   GROUP BY archivedistributiontaskid) td ,
  (SELECT archivejobid,
          cast(count(unitid)as number(8,0)) AS TargetUnits
   FROM um_archivejobdistributedtoUnit
   GROUP BY archivejobid) u ,
   view_um_curReleasCountDevices cu
WHERE cur.ARCHIVEDISTRIBUTIONTASKID=td.ARCHIVEDISTRIBUTIONTASKID
  AND cur.ARCHIVEJOBID=u.ARCHIVEJOBID 
  and cur.ARCHIVEDISTRIBUTIONTASKID=cu.ARCHIVEDISTRIBUTIONTASKID(+);
 
CREATE MATERIALIZED VIEW VIEW_UNIT_DEVICE_LOADINGSTAT
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
USING INDEX
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
  AS select u.clientid, u.unitid, u.unitno, u.visibleunitno, u.unitname, u.description as unitdescription,
    u.license, u.lastwlanconnection, u.lastgprsconnection,
    v.depotid, v.depotname, 
    d.deviceid, d.deviceno, dc.deviceclassid, dc.shortname as deviceclass, d.name as devicename, d.description as devicedescription, 
    d.mountingplate, d.lastdata, d.inventorynumber, d.externaldeviceno, 
    a.archiveid, a.filename, a.version, a.filedate, t.typename, 
    l.loadingstatisticsid, l.loadedfrom, l.loadeduntil, ls.stateid, ls.description as lsstate
from um_unit u, vario_depot v, um_device d, vario_deviceclass dc, um_loadingstatistics l, um_archive a, um_typeofarchive t, um_lsstate ls
where u.typeofunitid = 3
and u.unitstate = 1
and v.depotid = u.depotid
and d.unitid(+) = u.unitid
and dc.deviceclassid(+) = d.deviceclassid
and l.deviceid(+) = d.deviceid 
and a.archiveid(+) = l.archiveid
and t.typeofarchiveid(+) = a.typeofarchiveid
and ls.stateid(+) = l.lsstate;

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
