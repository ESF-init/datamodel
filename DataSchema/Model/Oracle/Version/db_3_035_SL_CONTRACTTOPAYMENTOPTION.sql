SPOOL db_3_035.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.035', 'BTS', 'Added Table SL_CONTRACTTOPAYMENTOPTION');

-- Is not needed anymore
---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
