SPOOL db_3_280.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.280', 'DST', 'Increased size of SL_WorkItem.CreatedBy to 512 characters.');

-- =======[ Changes to Existing Tables ]===========================================================================

alter table sl_workitem modify CreatedBy NVARCHAR2(512);

COMMIT;

PROMPT Done!

SPOOL OFF;
