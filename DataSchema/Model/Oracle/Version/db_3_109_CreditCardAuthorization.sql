SPOOL db_3_109.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.109', 'FLF', 'Added new column EncryptedDdata to SL_CreditCardAuthorization');

-- Start adding schema changes here


-- =======[ Changes to Existing Tables ]===========================================================================

ALTER TABLE SL_CREDITCARDAUTHORIZATION
 ADD (ENCRYPTEDDATA  VARCHAR2(1000));
 
comment on column SL_CREDITCARDAUTHORIZATION.ENCRYPTEDDATA is 'The ENCRYPTEDDATA column is used to store encrypted creditcard/debitcard data to re-process requests when enryption is needed';

---End adding schema changes 


-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
