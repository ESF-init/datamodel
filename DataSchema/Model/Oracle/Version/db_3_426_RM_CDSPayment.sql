SPOOL db_3_426.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.426', 'COS', 'Extend column RM_CDSPAYMENT.TRANSACTIONNUMBER');

-- =======[ Changes to Existing Tables ]===========================================================================
ALTER TABLE RM_CDSPAYMENT MODIFY(TRANSACTIONNUMBER NUMBER(18));

-- =======[ New Tables ]===========================================================================================

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
