SPOOL db_3_044.log;

INSERT INTO vario_dmodellversion(rundate, dmodellno, responsible, annotation) 
VALUES (sysdate, '3.044', 'SOE', 'Creation of SL_JobBulkImport');

-- =======[ Changes on existing tables ]===========================================================================

-- =======[ New Tables ]===========================================================================================

CREATE TABLE SL_JOBBULKIMPORT
(
    BULKIMPORTID       NUMBER(18,0)    NOT NULL,
    JOBID              NUMBER(18,0)    NOT NULL,
    CONTRACTID              NUMBER(18,0)    NOT NULL,
    PATH    NVARCHAR2(512),
	LASTUSER            VARCHAR2(50 Byte)   DEFAULT 'SYS' NOT NULL,
    LASTMODIFIED        DATE            DEFAULT SYSDATE NOT NULL,
    TRANSACTIONCOUNTER  INTEGER         NOT NULL
);

ALTER TABLE SL_JOBBULKIMPORT ADD CONSTRAINT PK_JOBBULKIMPORT PRIMARY KEY (BULKIMPORTID);
ALTER TABLE SL_JOBBULKIMPORT ADD CONSTRAINT FK_JOBBULKIMPORT_JOB FOREIGN KEY (JOBID) REFERENCES SL_JOB (JOBID);
ALTER TABLE SL_JOBBULKIMPORT ADD CONSTRAINT FK_JOBBULKIMPORT_CONTRACT FOREIGN KEY (CONTRACTID) REFERENCES SL_CONTRACT (CONTRACTID);


COMMENT ON TABLE SL_JOBBULKIMPORT is 'Table containing additional information about job bulk import ';
COMMENT ON COLUMN SL_JOBBULKIMPORT.BULKIMPORTID is 'Unique identifier of the bulk import';
COMMENT ON COLUMN SL_JOBBULKIMPORT.JOBID IS 'ID of the job';
COMMENT ON COLUMN SL_JOBBULKIMPORT.CONTRACTID IS 'ID of the Instution';
COMMENT ON COLUMN SL_JOBBULKIMPORT.PATH is '';
COMMENT ON COLUMN SL_JOBBULKIMPORT.LASTMODIFIED IS 'Technical field: date time of the last change to this dataset';
COMMENT ON COLUMN SL_JOBBULKIMPORT.TRANSACTIONCOUNTER IS 'Technical field: counter to prevent concurrent changes to this dataset';
COMMENT ON COLUMN SL_JOBBULKIMPORT.LASTUSER IS 'Technical field: last (system) user that changed this dataset';

-- -------[ Enumerations ]-----------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Functions and Data Types ]=============================================================================

-- =======[ Views ]================================================================================================

-- =======[ Sequences ]============================================================================================

DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
     'SL_JOBBULKIMPORT');
    sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := 'create sequence ' || table_names(table_name) || '_SEQ INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE';
      dbms_output.put_line('Creating sequence for: ' || table_names(table_name));
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating sequence: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Trigger ]==============================================================================================
DECLARE
  TYPE table_names_array IS VARRAY(1) OF VARCHAR2(100);
  table_names table_names_array := table_names_array(
    'SL_JOBBULKIMPORT');
  sql_string VARCHAR2(500);
BEGIN
  FOR table_name IN table_names.FIRST..table_names.LAST
  LOOP
    BEGIN
      sql_string := '';
      dbms_output.put_line('Creating insert trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_JBI' ||
        ' before insert on ' || table_names(table_name) || 
        ' for each row begin    :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Creating update trigger for: ' || table_names(table_name));
      sql_string := 
        'create or replace trigger ' || table_names(table_name) || '_JBU' ||
        ' before update on ' || table_names(table_name) || 
        ' for each row begin    if (:new.TransactionCounter != :old.TransactionCounter + 1) then raise_application_error( -20000, ''Concurrency Failure'' ); end if; :new.TransactionCounter := dbms_utility.get_time; end;';
      EXECUTE IMMEDIATE sql_string;
      dbms_output.put_line('Done!');
    exception
    WHEN others THEN
      dbms_output.put_line('Error creating trigger: ' || sqlerrm);
    END;
  END LOOP;
END;
/

-- =======[ Data ]==================================================================================================

-- -------[ Enumerations ]------------------------------------------------------------------------------------------

-- -------[ Master-Tables ]----------------------------------------------------------------------------------------

-- =======[ Commit ]===============================================================================================

COMMIT;

PROMPT Done!

SPOOL OFF;
