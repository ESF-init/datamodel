<# .SYNOPSIS
	Validation of Vario DB model scripts
.DESCRIPTION
	This script will create a docker container with an oracle db an set up a new dabase
	using the CreateVarioSchema scripts.
.PARAMETER target
	Specifies the target of the buildfile. Possible values in order:
	    clean          - Only cleanup workspace
	    createContent  - crate content for dockerfile, then exit
	    build          - Build dockerfile
.NOTES
    Author     : JED
#>

param (
	[string][ValidateSet('clean', 'createContent', 'build')]$target = "build",
	[string]$tempdir = ".\Temp"
)

function Main
{
    Validate

	#Opening block for TeamCity formatinng. Closing block is within Dockerfile's RUN
	"##teamcity[blockOpened name='Preperation']"
	PrepareBuildFolder
	 
	"Creating CreateSchema scripts...";
	Create-DB-Schema fvs
	Create-DB-Schema fvs2
	 
	Fetch-DB-VersionScripts
	Fetch-DB-VarioSchema
	
	ExitIfTargetReached 'createContent'

	"Building docker container...";
	docker build --no-cache --progress plain -t init/dbschemavalidation . 2>&1 | Write-Host
	
	"Copy logfile to host..."
	$containerid = (docker create init/dbschemavalidation)
	docker cp $containerid`:/dbschema.log dbschemavalidation.log
	docker container rm $containerid

	"Output SQL tests"
	Get-Content -Path .\dbschemavalidation.log `
	| Select-String -Pattern '^##teamcity\[test' `
	| % { $_.Line }
}

function Validate
{
	"##teamcity[blockOpened name='Validation']"
    
    $ValidationErrors = $false

    "Checking for whitespaces in db script filenames."
    Get-ChildItem -Path ..\Model\Oracle\Version\*.sql `
    | Where-Object -FilterScript { $_.Name -match "^db_(?<version_major>\d)_(?<version_minor>\d{3}).*" } `
    | Select-Object -Property Name `
    | Select-String -Pattern "\s+" `
    | ForEach-Object `
    {
		"##teamcity[buildProblem description='No whitespace allowed in db script filename.']"
        Write-Error $_ -CategoryReason "No whitespace allowed in db script filename." 
        $ValidationErrors = $true
    }

    if ($ValidationErrors -eq $true) { throw "Validation errors found!" }

    "Validation OK"
    "##teamcity[blockClosed name='Validation']"
}

function ExitIfTargetReached { param( [string]$targetToCheck )
	if ($target -eq $targetToCheck) 
	{
		"Target '" + $target + "' reached, exiting"
		Exit
	}
}

function PrepareBuildFolder 
{
	if (Test-Path -Path .\Temp) {
		"Performing cleanup...";	
		Remove-Item .\Temp -Force -Recurse
	}
	if (Test-Path -Path .\dbschemavalidation.log) {
		"Clearing dbschemavalidation.log";
		Remove-Item .\dbschemavalidation.log
	}

	ExitIfTargetReached 'clean'

	"Creating Temp directory";
	New-Item -ItemType Directory -Force -Path Temp `
	| Out-Null
	
	"Copying necessary templates"
	Copy-Item -Path .\Template\db_0_000_MasterSchemaBefore3_000.sql -Destination .\Temp\
	Copy-Item -Path .\Template\build.sql -Destination .\Temp\
	Copy-Item -Path .\Template\setup.sh -Destination .\Temp\
	Copy-Item -Path .\Template\tests.sql -Destination .\Temp\
}

function Create-DB-Schema { param( [string]$name )

	$filename = "Create_" + $name + ".sql"
	$Destination = [io.path]::combine($tempdir, $filename)
	
	(Get-Content .\Template\CreateSchemaTemplate.sql).replace('&&SCHEMANAME', $name) `
	| Set-Content $Destination
	
	"prompt ##teamcity[blockOpened name='Creating Schema " + $name + "']`n" + `
	"@@" + $filename + "`n" + `
	"prompt ##teamcity[blockClosed name='Creating Schema " + $name + "']" `
	| Add-Content ([io.path]::combine($tempdir, "Create_Schemas.sql"))
}

function Fetch-DB-VersionScripts
{
	"Preparing single version scripts...";
	$db_version_files = Get-ChildItem -Path ..\Model\Oracle\Version\*.sql `
	| Where-Object -FilterScript { $_.Name -match "^db_(?<version_major>\d)_(?<version_minor>\d{3}).*" } `

	$db_version_files | Copy-Item -Destination .\Temp\

	if (Test-Path -Path .\Temp\db_3_000_VARIO_INITIAL_VERSION.sql) {
		"Ignoring db_3_000_VARIO_INITIAL_VERSION.sql";
		Remove-Item .\Temp\db_3_000_VARIO_INITIAL_VERSION.sql
	}

	$db_version = $db_version_files `
	| Select-Object -Last 1 `
	| %{ $matches["version_major"] + "." + $matches["version_minor"] }

	"Reporting Build version"
	"##teamcity[buildNumber '" + $db_version + ".{build.number}']" `
	| Out-Host

	"@@db_0_000_MasterSchemaBefore3_000.sql" `
	| Add-Content ".\Temp\version_fiels.sql"

	"Prepare for sql script:"
	$db_version_files `
	| %{
		"DECLARE`n" +
		"    maxversion VARCHAR2(10);`n" +
		"BEGIN`n" +
		"    select max(dmodellno) into maxversion from vario_dmodellversion;`n" +
		"    DBMS_OUTPUT.PUT_LINE(maxversion);`n" +
		"END;`n" +
		"/`n`n" + 
		"prompt ##teamcity[message text='Executing script " + $_.Name + "']`n" +
		"@@" + $_.Name 
	   } `
	| Add-Content ".\Temp\version_fiels.sql"
}

function Fetch-DB-VarioSchema
{
	"Preparing Create_Vario_Schema.sql...";
	Copy-Item -Path ..\Model\Oracle\Create_Vario_Schema.sql -Destination .\Temp\
}


Main
