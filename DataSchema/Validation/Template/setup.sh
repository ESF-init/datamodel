#!/bin/bash

# Prevent owner issues on mounted folders
chown -R oracle:dba /u01/app/oracle
rm -f /u01/app/oracle/product
ln -s /u01/app/oracle-product /u01/app/oracle/product
# Update hostname
sed -i -E "s/HOST = [^)]+/HOST = $HOSTNAME/g" /u01/app/oracle/product/11.2.0/xe/network/admin/listener.ora
sed -i -E "s/PORT = [^)]+/PORT = 1521/g" /u01/app/oracle/product/11.2.0/xe/network/admin/listener.ora
echo "export ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe" > /etc/profile.d/oracle-xe.sh
echo "export PATH=\$ORACLE_HOME/bin:\$PATH" >> /etc/profile.d/oracle-xe.sh
echo "export ORACLE_SID=XE" >> /etc/profile.d/oracle-xe.sh
. /etc/profile

echo "Database not initialized. Initializing database."

if [ -z "$CHARACTER_SET" ]; then
	export CHARACTER_SET="AL32UTF8"
fi

printf "Setting up:\nprocesses=$processes\nsessions=$sessions\ntransactions=$transactions\n"
echo "If you want to use different parameters set processes, sessions, transactions env variables and consider this formula:"
printf "processes=x\nsessions=x*1.1+5\ntransactions=sessions*1.1\n"

mv /u01/app/oracle-product/11.2.0/xe/dbs /u01/app/oracle/dbs
ln -s /u01/app/oracle/dbs /u01/app/oracle-product/11.2.0/xe/dbs

#Setting up processes, sessions, transactions.
sed -i -E "s/processes=[^)]+/processes=$processes/g" /u01/app/oracle/product/11.2.0/xe/config/scripts/init.ora
sed -i -E "s/processes=[^)]+/processes=$processes/g" /u01/app/oracle/product/11.2.0/xe/config/scripts/initXETemp.ora

sed -i -E "s/sessions=[^)]+/sessions=$sessions/g" /u01/app/oracle/product/11.2.0/xe/config/scripts/init.ora
sed -i -E "s/sessions=[^)]+/sessions=$sessions/g" /u01/app/oracle/product/11.2.0/xe/config/scripts/initXETemp.ora

sed -i -E "s/transactions=[^)]+/transactions=$transactions/g" /u01/app/oracle/product/11.2.0/xe/config/scripts/init.ora
sed -i -E "s/transactions=[^)]+/transactions=$transactions/g" /u01/app/oracle/product/11.2.0/xe/config/scripts/initXETemp.ora

printf 8080\\n1521\\n${DEFAULT_SYS_PASS}\\n${DEFAULT_SYS_PASS}\\ny\\n | /etc/init.d/oracle-xe configure
echo "Setting sys/system passwords"
echo  alter user sys identified by \"$DEFAULT_SYS_PASS\"\; | su oracle -s /bin/bash -c "$ORACLE_HOME/bin/sqlplus -s / as sysdba" > /dev/null 2>&1
echo  alter user system identified by \"$DEFAULT_SYS_PASS\"\; | su oracle -s /bin/bash -c "$ORACLE_HOME/bin/sqlplus -s / as sysdba" > /dev/null 2>&1

echo "Database initialized. Please visit http://#containeer:8080/apex to proceed with configuration"

/etc/init.d/oracle-xe start

echo "##teamcity[blockClosed name='Preperation']"

echo "Executing SQL..."
su oracle -c "NLS_LANG=.$CHARACTER_SET $ORACLE_HOME/bin/sqlplus / as sysdba @/content/build.sql" | tee /dbschema.log | grep --line-buffered "^##teamcity\[[bm]"


echo "##teamcity[blockOpened name='ORA-Errors']"
echo "============================================================================================="
echo "ORA-Errors:"
echo "============================================================================================="
ora_errors=$( grep -c "ORA-" /dbschema.log )
if [ $ora_errors -gt 0 ]; then
	echo "$ora_errors ORA-error(s) found!"
	echo
	grep "ORA-" /dbschema.log | sed -r "s/(.*)(ORA-[0-9]+).*/##teamcity[buildProblem description='SQL error encountered: \2']/g" 
	echo
	echo "Context (check dbschemavalidation.log for more details):"
	echo "---------------------------------------------------------------------------------------------"
	grep -n -B50 "ORA-" /dbschema.log 
else
	echo "No ORA-errors found!"
fi
echo "============================================================================================="
echo "##teamcity[blockClosed name='ORA-Errors']"
echo
echo "##teamcity[blockOpened name='SQL-Warnings']"
echo "============================================================================================="
echo "SQL Warnings:"
echo "============================================================================================="
sql_warnings=$( grep -c "Warning:" /dbschema.log )
if [ $sql_warnings -gt 0 ]; then
	echo "$sql_warnings warning(s) found!"
	echo
	grep "Warning:" /dbschema.log | sed -r "s/(.*)\./##teamcity[message text='\1' status='WARNING']/g" 
	echo
	echo "Context (check dbschemavalidation.log for more details):"
	echo "---------------------------------------------------------------------------------------------"
	grep -n -B50 "Warning:" /dbschema.log git
else
	echo "No SQL Warnings found!"
fi
echo "============================================================================================="
echo "##teamcity[blockClosed name='SQL-Warnings']"

trap "/etc/init.d/oracle-xe stop && END=1" INT TERM

