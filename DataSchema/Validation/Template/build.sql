set echo on
SET SERVEROUTPUT ON 
WHENEVER SQLERROR EXIT SQL.SQLCODE
set linesize 2000
set heading off
set PAGESIZE 0
.
prompt ##teamcity[blockOpened name='Create FVS schema']
@@Create_fvs
prompt ##teamcity[blockClosed name='Create FVS schema']
.
prompt ##teamcity[blockOpened name='Create FVS2 schema']
@@Create_fvs2
prompt ##teamcity[blockClosed name='Create FVS2 schema']
.
prompt ##teamcity[blockOpened name='FVS2: Creating DB from version scripts']
alter session set current_schema = fvs2; 
@@version_fiels
prompt ##teamcity[blockClosed name='FVS2: Creating DB from version scripts']
prompt ##teamcity[blockOpened name='FVS2: Executing p_addDataRowCreationDate']
exec p_addDataRowCDateAllTables;
prompt ##teamcity[blockClosed name='FVS2: Executing p_addDataRowCreationDate']
.
prompt ##teamcity[blockOpened name='FVS: Creating DB from Master Schema']
alter session set current_schema = fvs; 
@@Create_Vario_Schema
prompt ##teamcity[blockClosed name='FVS: Creating DB from Master Schema']
prompt ##teamcity[blockOpened name='FVS: Executing p_addDataRowCreationDate']
exec p_addDataRowCDateAllTables;
prompt ##teamcity[blockClosed name='FVS: Executing p_addDataRowCreationDate']
.
@@tests
.