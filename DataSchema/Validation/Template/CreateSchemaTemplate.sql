.                                                                             
CREATE USER &&SCHEMANAME                                                          
  IDENTIFIED BY         &&SCHEMANAME                                          
  DEFAULT TABLESPACE    USERS                                             
  TEMPORARY TABLESPACE  TEMP                                             
  PROFILE               DEFAULT;                                              
.                                                                             
GRANT DBA TO &&SCHEMANAME WITH ADMIN OPTION;                                      
GRANT UNLIMITED TABLESPACE TO &&SCHEMANAME;                                       
.                                                                             
ALTER USER &&SCHEMANAME  DEFAULT ROLE ALL;                                        
.                                                                             
GRANT EXECUTE ON sys.dbms_alert TO &&SCHEMANAME ;                                 
GRANT EXECUTE ON sys.dbms_random TO &&SCHEMANAME ;                                
GRANT EXECUTE ON sys.utl_tcp TO &&SCHEMANAME ;                                    
GRANT EXECUTE ON sys.utl_http TO &&SCHEMANAME ;                                   
GRANT EXECUTE ON sys.utl_smtp TO &&SCHEMANAME ;                                   
.                                                                             
GRANT SELECT  ANY DICTIONARY TO &&SCHEMANAME ;                                    
.                                                                             
GRANT SELECT  ON sys.v_$session TO &&SCHEMANAME ;                                 
GRANT SELECT  ON sys.v_$datafile TO &&SCHEMANAME ;                                
GRANT SELECT  ON sys.v_$tablespace TO &&SCHEMANAME ;                              
.                                                                             
GRANT SELECT  ON sys.dba_free_space TO &&SCHEMANAME ;                             
GRANT SELECT  ON sys.dba_data_files TO &&SCHEMANAME ;                             
GRANT SELECT  ON sys.dba_segments   TO &&SCHEMANAME ;                             
GRANT SELECT  ON sys.dba_extents    TO &&SCHEMANAME ;                             
.                                                                             
GRANT CREATE ANY VIEW TO &&SCHEMANAME WITH ADMIN OPTION;                          
.                                                                             
