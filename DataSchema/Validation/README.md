    # DBSchemavalidation

## Prerrequisites

- [docker](https://docs.docker.com/docker-for-windows/install/) must be installed.
- The docker image `sath89/oracle-xe-11g` must be available. This image is no longer servd by the official docker registry.

  If this image is missing on your machine, you can find it here: `\\srvhermes\z_sw\User\jed\Docker\`. 
  Make this image available to your local docker instance with:

        docker load -i w:\User\jed\Docker\sath89_oracle-xe-11g.image

## Running the validation

Just invoke the `make.ps1` Powershell script to perform the validation. 
A `dbeschemavalidation.log` file schould be created containing the detailed log of the build.

### Clean

After build you can invoke `make.ps1 -target=clean` to delete all temporary created files

### Running the generated image

After successfully running `make.ps1`, a new image should be available on your local docker instance. 
It should have the name `init\dbeschemavalidation`. You can check this with (it should list the image):

    docker images

If you want to start an empty Oracle DB with the latest db schema scripts applied, you can start the image as a container.
This can also be helpful, if you want to debug errors/issues:

    docker run -p 1521:1521 -d init/dbschemavalidation

Wait a minute; Oracle db needs to boot up. Then you should be able to connect. Use `LOCALHOST` as the target machine.

Two schemas should be available:

- `FVS/FVS` is the db schema generated from Master schema (`Create_Vario_Schema.sql`).
- `FVS2/FVS` is the db schema generated from individual schema scripts in `\DataSchema\Model\Oracle\Version\`.