﻿namespace VarioSL.Entities.SupportClasses
{
	public class ConfigurationScopeValues
	{
		public string UserName { get; set; }
		public string ApplicationName { get; set; }
		public string WorkstationName { get; set; }
		public long? UnitId { get; set; }
		public string CustomScopeValue { get; set; }
	}
}
