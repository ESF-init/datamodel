﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VarioSL.Entities.Tests
{
    [TestClass]
    public class LLblTypeImports
    {
        private const string TypeImportsFilename = @"VarioSL.DataAccess.Enumerations.typeimports";
        private readonly XName _typeImportName = XName.Get("typeImport");
        private readonly XName _typeNameName = XName.Get("typeName");
        private readonly XName _assemblyFileName = XName.Get("assemblyFile");

        private XElement _typeImportsXml;

        [TestInitialize]
        public void Initialize()
        {
            var location = Assembly.GetAssembly(typeof(LLblTypeImports))?.Location;
            var directory = Path.GetDirectoryName(location);

            Assert.IsNotNull(directory, "Cannot determine location of unit test assembly.");

            var typeImportsFile = Path.Combine(directory, TypeImportsFilename);

            Assert.IsTrue(File.Exists(typeImportsFile), $"'{typeImportsFile}' not found.");

            _typeImportsXml = XElement.Load(typeImportsFile);
        }
    
        
        [TestMethod]
        public void All_Entities_Enumerations_Enums_Must_Be_In_TypeImportFile()
        {
            var existingTypeImports = 
                from typeImport in _typeImportsXml.Descendants(_typeImportName)
                let typeName = typeImport.Attribute(_typeNameName)?.Value
                let assemblyFile = typeImport.Attribute(_assemblyFileName)?.Value
                select (typeName, assemblyFile);

            var entitiesEnumerationTypes =
                from type in Assembly.GetAssembly(typeof(Enumerations.TicketType))?.GetTypes()
                where type.IsEnum && type.IsVisible
                select (type.FullName, type.Module.Name);

            var missingInTypeImports =
                (
                    from missingTypeImport in entitiesEnumerationTypes.Except(existingTypeImports)
                    orderby missingTypeImport.Item1
                    let xml = new XElement(_typeImportName,
                        new XAttribute(_typeNameName, missingTypeImport.Item1),
                        new XAttribute(_assemblyFileName, missingTypeImport.Item2))
                    select xml.ToString()
                )
                .ToList();


            if (missingInTypeImports.Any())
            {
                Assert.Fail("Missing typeImport entries found!. " +
                    "When you encounter this issue, you can copy these lines into the file '{1}':\n\n{0}\n\n", 
                    string.Join(Environment.NewLine, missingInTypeImports),
                    TypeImportsFilename);
            }
        }

    }
}
