﻿using System;
using System.Security.Principal;
using System.Threading;
using Init.TestFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.CollectionClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.HelperClasses;

namespace VarioSL.Entities.Test.EntityClasses
{
    [TestClass]
    public class CommonEntityBaseTest
    {

        #region Additional test attributes

        private static IPrincipal originalPrincipal;

        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext) { }

        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize()
        {
            Impersonate();
        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
			SecurityQuestionCollection collection = new SecurityQuestionCollection();
			collection.DeleteMulti(SecurityQuestionFields.Text == "Test");
            UndoImpersonation();
        }

        private static void Impersonate()
        {
            originalPrincipal = Thread.CurrentPrincipal;
            var identity = new GenericIdentity(WindowsIdentity.GetCurrent().Name);
            var principal = new GenericPrincipal(identity, null);
            Thread.CurrentPrincipal = principal;
        }

        private static void UndoImpersonation()
        {
            Thread.CurrentPrincipal = originalPrincipal;
        }

        #endregion

        [TestMethod, TestCategory(TestCategories.Integration)]
        public void OnSave_Insert_UpdatesAllConcurrencyFields()
        {
            SecurityQuestionEntity entity = CreateNewOrderSourceEntity();
            Assert.IsTrue(entity.Save(true));
            Thread.Sleep(100);

            Assert.IsTrue(entity.TransactionCounter != 0);
            Assert.AreNotEqual<DateTime>(DateTime.MinValue, entity.LastModified);
            Assert.IsTrue(DateTime.Now > entity.LastModified);
            Assert.AreEqual<string>(Thread.CurrentPrincipal.Identity.Name, entity.LastUser);
        }

        //[TestMethod, ExpectedException(typeof(ORMQueryExecutionException))]
        //public void OnSave_InsertWithoutValidIdentity_ThrowsException()
        //{
        //    UndoImpersonation();
        //    SecurityQuestionEntity entity = CreateNewOrderSourceEntity();
        //    entity.Save();
        //    Impersonate();
        //}

        [TestMethod, TestCategory(TestCategories.Integration)]
        public void OnSave_Update_UpdatesAllConcurrencyFields()
        {
            SecurityQuestionEntity entity = CreateNewOrderSourceEntity();
            Assert.IsTrue(entity.Save());

            Thread.Sleep(1000);
            decimal actualTransactionCounter = entity.TransactionCounter;
            DateTime actualLastModified = entity.LastModified;
            entity.Description = "Changed Test Entity";
            Assert.IsTrue(entity.Save());

            Assert.IsTrue(entity.TransactionCounter > actualTransactionCounter);
            Assert.AreNotEqual<DateTime>(DateTime.MinValue, entity.LastModified);
            Assert.AreNotEqual<DateTime>(actualLastModified, entity.LastModified);
            Assert.IsTrue(DateTime.Now > entity.LastModified);
            Assert.AreEqual<string>(Thread.CurrentPrincipal.Identity.Name, entity.LastUser);
            Assert.AreEqual<string>("Changed Test Entity", entity.Description);
        }

        [TestMethod, TestCategory(TestCategories.Integration)]
        public void OnSave_UpdateWithoutChanges_DoesNothing()
        {
            SecurityQuestionEntity entity = CreateNewOrderSourceEntity();
            Assert.IsTrue(entity.Save());

            decimal actualTransactionCounter = entity.TransactionCounter;
            DateTime actualLastModified = entity.LastModified;
            Assert.IsTrue(entity.Save());

            Assert.AreEqual<decimal>(actualTransactionCounter, entity.TransactionCounter);
            Assert.AreEqual<DateTime>(actualLastModified, entity.LastModified);
        }
        
        private static SecurityQuestionEntity CreateNewOrderSourceEntity()
        {
            return new SecurityQuestionEntity
            {
                Text = "Test",
                Description = "Test Entity"
            };
        }
      
    }
}
