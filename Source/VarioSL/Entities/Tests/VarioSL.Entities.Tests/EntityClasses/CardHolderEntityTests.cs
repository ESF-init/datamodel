﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class CardHolderEntityTests
	{

		[TestMethod]
		public void CardHolderID_WithCardHolderEntity_ReturnsPersonID()
		{
			var data = new CardHolderEntity() { PersonID = 2 };

			var result = data.CardHolderID;

			Assert.AreEqual(2, result);
		}
	}
}
