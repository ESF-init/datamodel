﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class OrganizationEntityTests
	{
		#region ContractIDs

		[TestMethod]
		public void ContractIDs_WithoutContracts_ReturnEmptyList()
		{
			var organization = CreateExistingOrganization();

			Assert.AreEqual(0, organization.ContractIDs.Count);
		}

		[TestMethod]
		public void ContractIDs_WithSingleContract_ReturnListWithSingleContractID()
		{
			var organization = CreateExistingOrganization();
			var contract1 = CreateExistingContractWithID(100000000);
			organization.Contracts.Add(contract1);

			Assert.AreEqual(1, organization.ContractIDs.Count);
			Assert.AreEqual(contract1.ContractID, organization.ContractIDs.Single());
		}

		[TestMethod]
		public void ContractIDs_WithMultipleContracts_ReturnAllAssignedContractIDs()
		{
			var organization = CreateExistingOrganization();
			var contract1 = CreateExistingContractWithID(1000000001);
			var contract2 = CreateExistingContractWithID(1000000022);
			organization.Contracts.AddRange(new[] { contract1, contract2 });

			Assert.AreEqual(2, organization.ContractIDs.Count);
			Assert.IsTrue(organization.ContractIDs.Contains(contract1.ContractID));
			Assert.IsTrue(organization.ContractIDs.Contains(contract2.ContractID));
		}


		#endregion

		#region Helper

		private static OrganizationEntity CreateExistingOrganization()
		{
			return new OrganizationEntity
			{
				IsNew = false,
				AlreadyFetchedContracts = true,
			};
		}

		private static ContractEntity CreateExistingContractWithID(long contractID)
		{
			return new ContractEntity
			{
				ContractID = contractID,
				IsNew = false,
			};
		}

		#endregion
	}
}
