﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Linq;
using VarioSL.Entities.SupportClasses;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class ConfigurationDefinitionExtensionsTests
	{
		private List<ConfigurationDefinitionEntity> GetTestConfigurationDefinitions()
		{
			var mockList = new List<ConfigurationDefinitionEntity>(){
				new ConfigurationDefinitionEntity() { Name = "Key1", DefaultValue = "Value1", Scope = "Workstation", GroupKey = "GroupKey1", AlreadyFetchedConfigurationOverwrites = true},
				new ConfigurationDefinitionEntity() { Name = "Key2", DefaultValue = "Value2", GroupKey = "GroupKey1", AlreadyFetchedConfigurationOverwrites = true },
				new ConfigurationDefinitionEntity() { Name = "Key3", DefaultValue = "Value3", GroupKey = "GroupKey3", AlreadyFetchedConfigurationOverwrites = true }
			};

			mockList[0].ConfigurationOverwrites.Add(new ConfigurationOverwriteEntity()
			{
				Value = "ClientVal1",
				SpecifierType = null,
				SpecifierValue = null,
				ClientID = 1
			});

			mockList[0].ConfigurationOverwrites.Add(new ConfigurationOverwriteEntity()
			{
				Value = "ClientVal2",
				SpecifierType = null,
				SpecifierValue = null,
				ClientID = 2
			});

			mockList[0].ConfigurationOverwrites.Add(new ConfigurationOverwriteEntity()
			{
				Value = "WorkstationVal1",
				SpecifierType = "Workstation",
				SpecifierValue = "PC123",
				ClientID = 1
			});

			mockList[0].ConfigurationOverwrites.Add(new ConfigurationOverwriteEntity()
			{
				Value = "WorkstationVal2",
				SpecifierType = "Workstation",
				SpecifierValue = "PC124",
				ClientID = 1
			});
			return mockList;
		}

		[TestInitialize]
		public void Initialize()
		{
			var appConfigValues = new List<KeyValuePair<string, string>>(){
				new KeyValuePair<string, string>("Key3", "AppConfigValue"),
				new KeyValuePair<string, string>("Key10", "Val1"),
				new KeyValuePair<string, string>("KeY10", "Val2")
			};
			ConfigurationDefinitionExtensions.AppConfigValues = appConfigValues;
		}

		[TestMethod]
		public void GetDefaultConfigurationValue_WithValidKey_ReturnsValue()
		{
			var value = GetTestConfigurationDefinitions().AsQueryable().GetDefaultConfigurationValue("Key2");

			Assert.AreEqual("Value2", value);
		}

		[TestMethod]
		public void GetConfigurationValue_OverwritesByClientValue_ReturnsDefaultValue()
		{
			var value = GetTestConfigurationDefinitions().AsQueryable().GetConfigurationValue("Key1", 3L);

			Assert.AreEqual("Value1", value);
		}

		[TestMethod]
		public void GetConfigurationValue_OverwritesByClientValue_ReturnsSpecificValue()
		{
			var value = GetTestConfigurationDefinitions().AsQueryable().GetConfigurationValue("Key1", 1L);

			Assert.AreEqual("ClientVal1", value);
		}

		[TestMethod]
		public void GetConfigurationValue_OverwritesByWorkstationValue_ReturnsSpecificValue()
		{
			var value = GetTestConfigurationDefinitions().AsQueryable().GetConfigurationValue("Key1", 1L, new ConfigurationScopeValues() { WorkstationName = "PC123" }, deviceQuery: new List<DeviceEntity>().AsQueryable());

			Assert.AreEqual("WorkstationVal1", value);
		}

		[TestMethod]
		public void GetDefaultConfigurationValue_WithValidKey_ReturnsAppConfigOverwrite()
		{
			var value = GetTestConfigurationDefinitions().AsQueryable().GetDefaultConfigurationValue("Key3");

			Assert.AreEqual("AppConfigValue", value);
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void GetDefaultConfigurationValue_WithDoubleAppConfigKey_ThrowsException()
		{
			var value = GetTestConfigurationDefinitions().AsQueryable().GetDefaultConfigurationValue("Key10");

			Assert.AreEqual("AppConfigValue", value);
		}

		[TestMethod]
		public void GetDefaultConfigurationValues_WithoutGroupKey_ReturnsNotNull()
		{
			var values = GetTestConfigurationDefinitions().AsQueryable().GetDefaultConfigurationValues();
			Assert.IsNotNull(values);
		}

		[TestMethod]
		public void GetDefaultConfigurationValues_WithValidGroupKey_ReturnsNotNull()
		{
			var values = GetTestConfigurationDefinitions().AsQueryable().GetDefaultConfigurationValues("GroupKey1");
			Assert.IsNotNull(values);
		}

		[TestMethod]
		public void GetDefaultConfigurationValues_WithInvalidGroupKey_ReturnsEmptyResult()
		{
			var values = GetTestConfigurationDefinitions().AsQueryable().GetDefaultConfigurationValues("InvalidGroupKey");
			Assert.IsNotNull(values);
			Assert.AreEqual(0, values.Count);
		}

		[TestMethod]
		public void GetDefaultConfigurationValues_WithValidGroupKey_ReturnsExactOneValue()
		{
			var values = GetTestConfigurationDefinitions().AsQueryable().GetDefaultConfigurationValues("GroupKey3");
			Assert.IsNotNull(values);
			Assert.AreEqual(1, values.Count);
		}

		[TestMethod]
		public void GetDefaultConfigurationValues_WithoutGroupKey_ReturnsExactThreeValues()
		{
			var values = GetTestConfigurationDefinitions().AsQueryable().GetDefaultConfigurationValues();
			Assert.IsNotNull(values);
			Assert.AreEqual(3, values.Count);
		}

		[TestMethod]
		public void GetDefaultConfigurationValues_WithValidGroupKey_ReturnsValues()
		{
			var values = GetTestConfigurationDefinitions().AsQueryable().GetDefaultConfigurationValues("GroupKey1");

			var expected = new Dictionary<string, string>()
			{
				{"Key1", "Value1"},
				{"Key2", "Value2"}
			};

			CollectionAssert.AreEqual(expected, values);
		}

		[TestMethod]
		public void GetConfigurationValues_WithoutGroupKey_ClientSpecificValues()
		{
			var values = GetTestConfigurationDefinitions().AsQueryable().GetConfigurationValues(1L);

			var expected = new Dictionary<string, string>()
			{
				{"Key1", "ClientVal1"},
				{"Key2", "Value2"},
				{"Key3", "AppConfigValue"}
			};

			CollectionAssert.AreEqual(expected, values);
		}

		[TestMethod]
		public void GetConfigurationValues_WithoutGroupKey_WorkstationSpecificValues()
		{
			var values = GetTestConfigurationDefinitions().AsQueryable().GetConfigurationValues(1L, new ConfigurationScopeValues() { WorkstationName = "PC123" }, deviceQuery: new List<DeviceEntity>().AsQueryable());

			var expected = new Dictionary<string, string>()
			{
				{"Key1", "WorkstationVal1"},
				{"Key2", "Value2"},
				{"Key3", "AppConfigValue"}
			};

			CollectionAssert.AreEqual(expected, values);
		}

		[TestMethod]
		[ExpectedException(typeof(KeyNotFoundException))]
		public void GetDefaultConfigurationValue_DoubleKeyValues_ThrowsException()
		{
			var configData = GetTestConfigurationDefinitions();
			configData.Add(new ConfigurationDefinitionEntity() { Name = "key1", AlreadyFetchedConfigurationOverwrites = true });

			configData.AsQueryable().GetDefaultConfigurationValue("Key1");
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void SetDefaultConfigurationValue_InvalidKey_ThrowsException()
		{
			GetTestConfigurationDefinitions().AsQueryable().SetDefaultConfigurationValue(string.Empty, "NewValue");
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void SetClientConfigurationValue_InvalidClientID_ThrowsException()
		{
			GetTestConfigurationDefinitions().AsQueryable().SetClientOverwriteConfigurationValue("Key1", "NewValue", -1);
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void SetScopeConfigurationValue_InvalidSpecifierValue_ThrowsException()
		{
			GetTestConfigurationDefinitions().AsQueryable().SetScopeOverwriteConfigurationValue("Key1", "NewValue", 1, ConfigurationDefinitionExtensions.ScopeOverwriteTypes.User, string.Empty);
		}

	}
}
