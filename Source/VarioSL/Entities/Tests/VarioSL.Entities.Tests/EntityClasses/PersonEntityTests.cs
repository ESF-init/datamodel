﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class PersonEntityTests
	{
		[TestMethod]
		public void HasPhotograph_WithNull_ReturnsFalse()
		{
			var person = CreatePersonWithPhotograph(null);

			var result = person.HasPhotograph;

			Assert.IsFalse(result);
		}

		[TestMethod]
		public void HasPhotograph_WithPhoto_ReturnsTrue()
		{
			var person = CreatePersonWithPhotograph(new PhotographEntity());

			var result = person.HasPhotograph;

			Assert.IsTrue(result);
		}

		private PersonEntity CreatePersonWithPhotograph(PhotographEntity photo)
		{
			var ret = new PersonEntity() { AlreadyFetchedPhotographs = true };
			if (photo != null)
				ret.Photographs.Add(photo);
			return ret;
		}
	}
}
