﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VarioSL.Entities.CollectionClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;
using VaroSL.Entities.TestFramework;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class ContractEntityTests
	{

		[TestMethod]
		public void Initialize_ContractWithStateNoActive_StateIsActive()
		{
			ContractEntity contract = new ContractEntity()
				{
					State = ContractState.Inactive
				};

			const string contractNumber = "OK_Number";

			contract.Initialize(ContractState.Active, contractNumber);

			Assert.IsTrue(contract.State == ContractState.Active);
		}

		[TestMethod]
		public void Initialize_Contract_ValidFromIsSet()
		{
			ContractEntity contract = new ContractEntity();

			const string contractNumber = "OK_Number";

			contract.Initialize(ContractState.Active, contractNumber);

			Assert.IsTrue(contract.ValidFrom != DateTime.MinValue);
		}

		[TestMethod]
		public void Initialize_Contract_ContractNumberIsSet()
		{
			ContractEntity contract = new ContractEntity();

			const string contractNumber = "OK_Number";

			contract.Initialize(ContractState.Active, contractNumber);

			Assert.AreSame(contractNumber, contract.ContractNumber);
		}


		#region NeedsApproval

		[TestMethod]
		public void NeedsApproval_WithOrganizationIDSetAndStateInactive_ReturnsTrue()
		{
			var contractMock = NeedsApproval_CreateMockContract(ContractState.Inactive, 0);
			Assert.IsTrue(contractMock.Object.NeedApproval);
		}

		[TestMethod]
		public void NeedsApproval_WithOrganizationIDSetAndStateClosed_ReturnsFalse()
		{
			var contractMock = NeedsApproval_CreateMockContract(ContractState.Closed, 0);
			Assert.IsFalse(contractMock.Object.NeedApproval);
		}

		[TestMethod]
		public void NeedsApproval_WithOrganizationIDSetAndStateActive_ReturnsFalse()
		{
			var contractMock = NeedsApproval_CreateMockContract(ContractState.Active, 0);
			Assert.IsFalse(contractMock.Object.NeedApproval);
		}

		[TestMethod]
		public void NeedsApproval_WithoutOrganizationIDSetAndStateInactive_ReturnsFalse()
		{
			var contractMock = NeedsApproval_CreateMockContract(ContractState.Inactive, null);
			Assert.IsFalse(contractMock.Object.NeedApproval);
		}

		[TestMethod]
		public void NeedsApproval_WithoutOrganizationIDSetAndStateActive_ReturnsFalse()
		{
			var contractMock = NeedsApproval_CreateMockContract(ContractState.Active, null);
			Assert.IsFalse(contractMock.Object.NeedApproval);
		}

		[TestMethod]
		public void NeedsApproval_WithoutOrganizationIDSetAndStateClosed_ReturnsFalse()
		{
			var contractMock = NeedsApproval_CreateMockContract(ContractState.Closed, null);
			Assert.IsFalse(contractMock.Object.NeedApproval);
		}

		[TestMethod]
		public void NeedsApproval_ContractStateEnumCount()
		{
			Assert.AreEqual(5, Enum.GetNames(typeof(ContractState)).Length);
		}

		private static Mock<ContractEntity> NeedsApproval_CreateMockContract(ContractState state, long? OrganizationID)
		{
			var mock = new Mock<ContractEntity>() { CallBase = true };
			mock.Setup(contract => contract.OrganizationID).Returns(OrganizationID);
			mock.Setup(contract => contract.State).Returns(state);
			return mock;
		}

		#endregion

		#region OpenWorkItemID

		[TestMethod]
		public void OpenWorkItemID_WithUnassingedAndNoexecutedWorkitem_ReturnsWorkItemID()
		{
			var workItem = CreateWorkItemEntity(false, false, false);
			ContractWorkItemCollection workItems = new ContractWorkItemCollection(new List<ContractWorkItemEntity> { workItem });
			var contractMock = OpenWorkItemID_CreateMockContract(workItems);
			Assert.AreEqual(workItem.WorkItemID, contractMock.Object.OpenWorkItemID);
		}

		[TestMethod]
		public void OpenWorkItemID_WithNewWorkitem_Zero()
		{
			var workItem = CreateWorkItemEntity(false, false, true);
			ContractWorkItemCollection workItems = new ContractWorkItemCollection(new List<ContractWorkItemEntity> { workItem });
			var contractMock = OpenWorkItemID_CreateMockContract(workItems);
			Assert.AreEqual(0, contractMock.Object.OpenWorkItemID);
		}

		[TestMethod]
		public void OpenWorkItemID_WithAssingedAndNoexecutedWorkitem_Zero()
		{
			var workItem = CreateWorkItemEntity(true, false, false);
			ContractWorkItemCollection workItems = new ContractWorkItemCollection(new List<ContractWorkItemEntity> { workItem });
			var contractMock = OpenWorkItemID_CreateMockContract(workItems);
			Assert.AreEqual(0, contractMock.Object.OpenWorkItemID);
		}

		[TestMethod]
		public void OpenWorkItemID_WithAssingedAndExecutedWorkitem_Zero()
		{
			var workItem = CreateWorkItemEntity(true, true, false);
			ContractWorkItemCollection workItems = new ContractWorkItemCollection(new List<ContractWorkItemEntity> { workItem });
			var contractMock = OpenWorkItemID_CreateMockContract(workItems);
			Assert.AreEqual(0, contractMock.Object.OpenWorkItemID);
		}

		private static ContractWorkItemEntity CreateWorkItemEntity(bool isAssinged, bool isExecuted, bool isNew)
		{
			return new ContractWorkItemEntity
			{
				WorkItemID = 100,
				IsNew = isNew,
				Executed = isExecuted ? DateTime.Now : DateTime.MinValue,
				Assigned = isAssinged ? DateTime.Now : DateTime.MinValue,
			};
		}

		private static Mock<ContractEntity> OpenWorkItemID_CreateMockContract(ContractWorkItemCollection workitems)
		{
			var mock = new Mock<ContractEntity>() { CallBase = true };
			mock.Setup(contract => contract.WorkItems).Returns(workitems);
			return mock;
		}

		#endregion

		#region IsMaxCardLimitReached

		[TestMethod]
		public void IsMaxCardLimitReached_WithNewContract_IsFalse()
		{
			var contract = new ContractEntity();
			contract.CardToContractsForUnitTest = new List<CardToContractEntity>().AsQueryable();
			contract.ConfigurationForUnitTest = new List<ConfigurationDefinitionEntity>().AsQueryable();

			Assert.IsFalse(contract.IsMaxCardLimitReached);
		}

		[TestMethod]
		public void IsMaxCardLimitReached_With10Active_IsTrue()
		{
			var contract = new ContractEntity();
			contract.CardToContractsForUnitTest = GetCardToContracts(10, CardState.Active);
			contract.ConfigurationForUnitTest = new List<ConfigurationDefinitionEntity>().AsQueryable();
			
			Assert.IsTrue(contract.IsMaxCardLimitReached);
		}

		[TestMethod]
		public void IsMaxCardLimitReached_With10ActiveAndOrganization_IsFalse()
		{
			var contract = new ContractEntity() { OrganizationID = 0, AlreadyFetchedOrganization = true};
			contract.CardToContractsForUnitTest = GetCardToContracts(10, CardState.Active);
			contract.ConfigurationForUnitTest = new List<ConfigurationDefinitionEntity>().AsQueryable();

			Assert.IsFalse(contract.IsMaxCardLimitReached);
		}

		[TestMethod]
		public void IsMaxCardLimitReached_With10Blocked_IsFalse()
		{
			var contract = new ContractEntity();
			contract.CardToContractsForUnitTest = GetCardToContracts(10, CardState.Blocked);
			contract.ConfigurationForUnitTest = new List<ConfigurationDefinitionEntity>().AsQueryable();

			Assert.IsFalse(contract.IsMaxCardLimitReached);
		}

		[TestMethod]
		public void IsMaxCardLimitReached_WithConfiguration_IsTrue()
		{
			var contract = new ContractEntity();
			contract.CardToContractsForUnitTest = GetCardToContracts(3, CardState.Active);
			contract.ConfigurationForUnitTest = new List<ConfigurationDefinitionEntity>() {
				new ConfigurationDefinitionEntity { Name = "MaxCardsForContract", GroupKey = "System", DefaultValue = "3", AlreadyFetchedConfigurationOverwrites = true }
			}.AsQueryable();

			Assert.IsTrue(contract.IsMaxCardLimitReached);
		}

		private IQueryable<CardToContractEntity> GetCardToContracts(int numCards, CardState state)
		{
			var data = Enumerable.Range(0, numCards).Select(i => new CardToContractEntity { Card = new CardEntity { State = state } });
			return new List<CardToContractEntity>(data).AsQueryable();
		}

        #endregion

        #region ContractHash

		[TestMethod]
		public void ContractHash_RowTest()
		{
			var expected = new RowTest<ContractEntity, ContractEntity, bool>(con => con.ContractHash);

			expected.Add(new ContractEntity { }, true, "Account with the same fields did not have the same hash.");
			expected.Add(new ContractEntity { State = ContractState.Closed }, false, "ContractState is not included in the hash.");
			expected.Add(new ContractEntity { ClientID = 5 }, false, "ClientID is not included in the hash.");
			expected.Add(new ContractEntity { LastModified = DateTime.MaxValue }, false, "LastModified is not included in the hash.");

			expected.Assert(x => x, x => x.ContractHash == new ContractEntity().ContractHash);
		}

        #endregion
    }
}
