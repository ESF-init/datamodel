﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class ReportJobResultEntityTests
	{
		#region Initialize

		private Lazy<string> oldPathReplace;
		private Lazy<string> oldReportRootDir;

		[TestInitialize]
		public void Initialize()
		{
			oldPathReplace = ReportJobResultEntity.pathReplace;
			oldReportRootDir = ReportJobResultEntity.reportRootDirectory;
			ReportJobResultEntity.pathReplace = new Lazy<string>(() => "http://localhost");
			ReportJobResultEntity.reportRootDirectory = new Lazy<string>(() => @"\\srvhermes\vasa");
		}

		[TestCleanup]
		public void Cleanup()
		{
			ReportJobResultEntity.pathReplace = oldPathReplace;
			ReportJobResultEntity.reportRootDirectory = oldReportRootDir;
		}

		#endregion
		
		[TestMethod]
		public void HttpPath_WithSuccessfulJob_ReplacesDirectory()
		{
			var entity = new ReportJobResultEntity()
				{
					Success = true,
					FileName = @"\\srvhermes\vasa\test"
				};

			var expected = "http://localhost/test";

			Assert.AreEqual(expected, entity.HttpPath);
		}

		[TestMethod]
		public void HttpPath_WithUnsuccessfulJob_ReturnEmpty()
		{
			var entity = new ReportJobResultEntity()
			{
				Success = false,
				FileName = @"\\srvhermes\vasa\test"
			};

			var expected = string.Empty;

			Assert.AreEqual(expected, entity.HttpPath);
		}

		[TestMethod]
		public void HttpPath_WithDifferentFileName_ReturnEmpty()
		{
			var entity = new ReportJobResultEntity()
			{
				Success = true,
				FileName = @"\\srvdif\vasa\test"
			};

			var expected = string.Empty;

			Assert.AreEqual(expected, entity.HttpPath);
		}
	}
}
