﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;
using VaroSL.Entities.TestFramework;

namespace VarioSL.Entities.Test.EntityClasses
{
    [TestClass]
    public class ProductEntityTests
    {
        #region HasAutoload

        [TestMethod]
        public void HasAutoload_WithNoAutoloadSettings_ReturnsFalse()
        {
            var product = CreateProduct();

            var result = product.HasAutoload;

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void HasAutoload_WithAutoloadSettings_ReturnsTrue()
        {
            var product = CreateProduct();
            product.AutoloadSettings.Add(new AutoloadSettingEntity());

            var result = product.HasAutoload;

            Assert.IsTrue(result);
        }

        #endregion

        #region HasActiveAutoload

        [TestMethod]
        public void HasActiveAutoload_WithNoAutoloadSettings_ReturnsFalse()
        {
            var product = CreateProduct();

            var result = product.HasActiveAutoload;

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void HasActiveAutoload_WithSuspendedAutoloadSettings_ReturnsFalse()
        {
            var product = CreateProduct();
            product.AutoloadSettings.Add(new AutoloadSettingEntity()
            {
                SuspendedFrom = DateTime.Now,
                SuspendedTo = DateTime.MaxValue
            });

            var result = product.HasActiveAutoload;

            Assert.IsFalse(result);
        }

		[TestMethod]
		public void HasActiveAutoload_WithSuspendedAutoloadSettingsInTheFuture_ReturnsTrue()
		{
			var product = CreateProduct();
			product.AutoloadSettings.Add(new AutoloadSettingEntity()
			{
				SuspendedFrom = DateTime.Now.AddDays(1),
				SuspendedTo = DateTime.MaxValue
			});

			var result = product.HasActiveAutoload;

			Assert.IsTrue(result);
		}

		[TestMethod]
        public void HasActiveAutoload_WithAutoloadSettings_ReturnsTrue()
        {
            var product = CreateProduct();
            product.AutoloadSettings.Add(new AutoloadSettingEntity());

            var result = product.HasAutoload;

            Assert.IsTrue(result);
        }

        #endregion

        #region IsAutoloadAllowed


        [TestMethod]
        public void IsAutoloadAllowed_WithCashSaver_ReturnsFalse()
        {
            var result = CreateProductWithTicketType(TicketType.CashSaver).IsAutoloadAllowed;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsAutoloadAllowed_WithDriverIssued_ReturnsFalse()
        {
            var result = CreateProductWithTicketType(TicketType.DriverIssued).IsAutoloadAllowed;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsAutoloadAllowed_WithNormalTicket_ReturnsFalse()
        {
            var result = CreateProductWithTicketType(TicketType.NormalTicket).IsAutoloadAllowed;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsAutoloadAllowed_WithPhysicalCard_ReturnsFalse()
        {
            var result = CreateProductWithTicketType(TicketType.PhysicalCard).IsAutoloadAllowed;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsAutoloadAllowed_WithPunch_ReturnsFalse()
        {
            var result = CreateProductWithTicketType(TicketType.Punch).IsAutoloadAllowed;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsAutoloadAllowed_WithZeroValueTicket_ReturnsFalse()
        {
            var result = CreateProductWithTicketType(TicketType.ZeroValueTicket).IsAutoloadAllowed;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsAutoloadAllowed_WithRefundTicket_ReturnsFalse()
        {
            var result = CreateProductWithTicketType(TicketType.Refund).IsAutoloadAllowed;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsAutoloadAllowed_WithGoodwill_ReturnsFalse()
        {
            var result = CreateProductWithTicketType(TicketType.Goodwill).IsAutoloadAllowed;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsAutoloadAllowed_WithCapping_ReturnsFalse()
        {
            var result = CreateProductWithTicketType(TicketType.Capping).IsAutoloadAllowed;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsAutoloadAllowed_WithPurse_ReturnsFalse()
        {
            var result = CreateProductWithTicketType(TicketType.Purse).IsAutoloadAllowed;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsAutoloadAllowed_WithReplacmentFee_ReturnsFalse()
        {
            var result = CreateProductWithTicketType(TicketType.ReplacementFee).IsAutoloadAllowed;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsAutoloadAllowed_WithTopUp_ReturnsTrue()
        {
            var result = CreateProductWithTicketType(TicketType.TopUp).IsAutoloadAllowed;
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsAutoloadAllowed_WithPass_ReturnsTrue()
        {
            var result = CreateProductWithTicketType(TicketType.Pass).IsAutoloadAllowed;
            Assert.IsTrue(result);
        }



        #endregion

        #region IsRefundAllowed


        [TestMethod]
        public void IsRefundAllowed_WithRefund_ReturnsFalse()
        {
            var result = CreateProductWithRefund(1).IsRefundAllowed;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsRefundAllowed_WithoutRefund_ReturnsTrue()
        {
            var result = CreateProductWithRefund(null).IsRefundAllowed;
            Assert.IsTrue(result);
        }


        #endregion


        

        #region IsOrdered

        [TestMethod]
        public void IsOrdered_WithOrderStateNew_ReturnsFalse()
        {
            OrderEntity order = new OrderEntity() { State = OrderState.New };
            ProductEntity product = new ProductEntity() { AlreadyFetchedOrderDetails = true };

            OrderDetailEntity orderDetail = new OrderDetailEntity()
                {
                    Product = product,
                    Order = order
                };

            Assert.IsFalse(product.IsOrdered);
        }

        [TestMethod]
        public void IsOrdered_WithOrderStateOtherThanNew_ReturnsTrue()
        {
            OrderEntity order = new OrderEntity() { State = OrderState.Open };
            ProductEntity product = new ProductEntity() { AlreadyFetchedOrderDetails = true };

            OrderDetailEntity orderDetail = new OrderDetailEntity()
            {
                Product = product,
                Order = order
            };

            Assert.IsTrue(product.IsOrdered);
        }

        [TestMethod]
        public void IsOrdered_WithNoOrder_ReturnsFalse()
        {
            ProductEntity product = new ProductEntity() { AlreadyFetchedOrderDetails = true };

            Assert.IsFalse(product.IsOrdered);
        }

        #endregion

        #region IsActive

        [TestMethod]
        public void IsActive_ValidityIsCurrent_ReturnsTrue()
        {
            ProductEntity product = new ProductEntity()
            {
                IsLoaded = true,
                ValidFrom = DateTime.Now.AddDays(-1),
                ValidTo = DateTime.Now.AddDays(1)
            };

            Assert.IsTrue(product.IsActive);
        }

        [TestMethod]
        public void IsActive_NoValiditySet_ReturnsFalse()
        {
            ProductEntity product = new ProductEntity()
                {
                    IsLoaded = true
                };

            Assert.IsFalse(product.IsActive);
        }

        [TestMethod]
        public void IsActive_ValidityInThePast_ReturnsFalse()
        {
            ProductEntity product = new ProductEntity()
            {
                IsLoaded = true,
                ValidFrom = DateTime.Now.AddDays(-2),
                ValidTo = DateTime.Now.AddDays(-1)
            };

            Assert.IsFalse(product.IsActive);
        }

        [TestMethod]
        public void IsActive_ValidityInTheFuture_ReturnsFalse()
        {
            ProductEntity product = new ProductEntity()
            {
                IsLoaded = true,
                ValidFrom = DateTime.Now.AddDays(1),
                ValidTo = DateTime.Now.AddDays(2)
            };

            Assert.IsFalse(product.IsActive);
        }

        [TestMethod]
        public void IsActive_ValidFromNotSet_ReturnsTrue()
        {
            ProductEntity product = new ProductEntity()
            {
                IsLoaded = true,
                ValidTo = DateTime.Now.AddDays(1)
            };

            Assert.IsTrue(product.IsActive);
        }

        [TestMethod]
        public void IsActive_ValidToNotSet_ReturnsFalse()
        {
            ProductEntity product = new ProductEntity()
            {
                IsLoaded = true,
                ValidFrom = DateTime.Now.AddDays(-1)
            };

            Assert.IsFalse(product.IsActive);
        }

        [TestMethod]
        public void IsActive_NotLoaded_ReturnsFalse()
        {
            ProductEntity product = new ProductEntity()
            {
                IsLoaded = false,
                ValidFrom = DateTime.Now.AddDays(-1),
                ValidTo = DateTime.Now.AddDays(1)
            };

            Assert.IsFalse(product.IsActive);
        }

        #endregion

        #region IsPending


        [TestMethod]
        public void IsPending_States()
        {
            const string openOrderOrPaid = "A noncanceled nonloaded product should be pending if it is paid.";
            const string noOpenOrdersNotPaid = "An unpaid product should not be pending.";
            const string loaded = "A loaded product should never be pending.";
            const string canceled = "A canceled product should never be pending.";
            const string loadedAndCanceled = "A canceled and loaded product should never be pending.";

            var expected = new RowTest<ProductEntity, ProductEntity, bool>(x =>
                string.Format("IsCanceled({0}), IsLoaded({1}), IsPaid({2}), OrderStates({3})",
                    x.IsCanceled,
                    x.IsLoaded,
                    x.IsPaid,
                    x.OrderDetails.Select(od => od.Order.State.ToString()).Aggregate((a, b) => a + ", " + b)));

            expected.Add(PendingProduct(false, false, true), true, openOrderOrPaid);
            expected.Add(PendingProduct(false, false, false), false, noOpenOrdersNotPaid);
            expected.Add(PendingProduct(false, true, true), false, loaded);
            expected.Add(PendingProduct(true, false, true), false, canceled);
            expected.Add(PendingProduct(true, true, false), false, loadedAndCanceled);

            expected.Assert(x => x, x => x.IsPending);
        }

        private ProductEntity PendingProduct(bool isCanceled, bool isLoaded, bool isPaid)
        {
            ProductEntity product = new ProductEntity()
            {
                AlreadyFetchedOrderDetails = true,
                IsCanceled = isCanceled,
                IsLoaded = isLoaded,
                IsPaid = isPaid,
            };

            return product;
        }

        #endregion

        #region TicketRulePeriodDescription

        [TestMethod]
        public void TicketRulePeriodDescription_WithNullTicket_ReturnsEmptyString()
        {
            ProductEntity product = new ProductEntity()
                {
                    Ticket = null,
                    AlreadyFetchedTicket = true
                };

            Assert.AreEqual(string.Empty, product.TicketRulePeriodDescription);
        }

        [TestMethod]
        public void TicketRulePeriodDescription_WithNullRulePeriod_ReturnsEmptyString()
        {
            ProductEntity product = new ProductEntity()
            {
                Ticket = new TicketEntity()
                    {
                        RulePeriod = null,
                        AlreadyFetchedRulePeriod = true
                    },
                AlreadyFetchedTicket = true
            };

            Assert.AreEqual(string.Empty, product.TicketRulePeriodDescription);
        }

        [TestMethod]
        public void TicketRulePeriodDescription_WithNullDescription_ReturnsEmptyString()
        {
            ProductEntity product = new ProductEntity()
            {
                Ticket = new TicketEntity()
                {
                    RulePeriod = new RulePeriodEntity()
                    {
                        Description = null
                    },
                    AlreadyFetchedRulePeriod = true
                },
                AlreadyFetchedTicket = true
            };

            Assert.AreEqual(string.Empty, product.TicketRulePeriodDescription);
        }

        [TestMethod]
        public void TicketRulePeriodDescription_WithRulePeriod_ReturnsDescription()
        {
            const string rulePeriodDescription = "VALID_description";

            ProductEntity product = new ProductEntity()
            {
                Ticket = new TicketEntity()
                {
                    RulePeriod = new RulePeriodEntity()
                        {
                            Description = rulePeriodDescription
                        },
                    AlreadyFetchedRulePeriod = true
                },
                AlreadyFetchedTicket = true
            };

            Assert.AreEqual(rulePeriodDescription, product.TicketRulePeriodDescription);
        }

        #endregion

        #region IsReloadable

        [TestMethod]
        public void IsReloadable_WithLoadedPass_ReturnsTrue()
        {
            var product = CreateProduct();

            product.TicketType = TicketType.Pass;
            product.IsLoaded = true;

            Assert.IsTrue(product.IsReloadable);
        }

        [TestMethod]
        public void IsReloadable_WithLoadedTopup_ReturnsFalse()
        {
            var product = CreateProduct();

            product.TicketType = TicketType.TopUp;
            product.IsLoaded = true;

            Assert.IsFalse(product.IsReloadable);
        }

        [TestMethod]
        public void IsReloadable_WithNotLoadedPass_ReturnsFalse()
        {
            var product = CreateProduct();

            product.TicketType = TicketType.Pass;
            product.IsLoaded = false;

            Assert.IsFalse(product.IsReloadable);
        }

        #endregion

        #region PrintedCardNumber

        [TestMethod]
        public void PrintedCardNumber_WithValidCard_ReturnsPrintedCardNumber()
        {
            var product = CreateProduct();

            const string expected = "VALID_CARD_NUMBER";

            product.Card = new CardEntity()
            {
                PrintedNumber = expected
            };

            Assert.AreEqual(expected, product.PrintedCardNumber);
        }

        [TestMethod]
        public void PrintedCardNumber_WithoutCard_ReturnsEmptyString()
        {
            var product = CreateProduct();

            Assert.AreEqual(string.Empty, product.PrintedCardNumber);
        }

        #endregion

        private static ProductEntity CreateProduct()
        {
            return new ProductEntity()
                {
                    AlreadyFetchedAutoloadSettings = true
                };
        }

        internal static ProductEntity CreateProductWithTicketType(TicketType ticketType)
        {
            var ret = CreateProduct();
            ret.TicketType = ticketType;
            return ret;
        }

        private static ProductEntity CreateProductWithRefund(long? refundID)
        {
            var ret = CreateProduct();
            ret.RefundID = refundID;
            return ret;
        }
    }
}
