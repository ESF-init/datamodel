﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.SupportClasses;
using VaroSL.Entities.TestFramework;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class CustomerAccountEntityTests
	{

		#region Initialize

		[TestMethod]
		public void Initialize_CustomerAccountWithStateInactive_StateIsActive()
		{
			CustomerAccountEntity customerAccount = new CustomerAccountEntity()
				{
					State = CustomerAccountState.Inactive
				};
			customerAccount.Password = "OK_Password";
            customerAccount.Initialize(CustomerAccountState.Active);

			Assert.IsTrue(customerAccount.State == CustomerAccountState.Active);
		}

		[TestMethod]
		public void Initialize_WithCustomerAccountStub_SetPasswordIsCalled()
		{
			const string password = "OK_Password";

			var stub = new Mock<CustomerAccountEntity>() { CallBase = true };
			stub.Object.Password = password;
			stub.Setup(s => s.SetPassword(password));

			stub.Object.Initialize(CustomerAccountState.Active);

			stub.VerifyAll();
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void Initialize_WithExistingCustomerAccount_ThrowsException()
		{
			CustomerAccountEntity customerAccount = new CustomerAccountEntity()
				{
					IsNew = false
				};
			customerAccount.Password = "OK_Password";
            customerAccount.Initialize(CustomerAccountState.Active);
		}

		#endregion

		#region CheckPassword

		[TestMethod]
		public void CheckPassword_WithBcryptMock_BcryptCalledWithCorrectParameters()
		{
			CustomerAccountEntity customerAccount = new CustomerAccountEntity();
			const string password = "OK_Password";

			var mock = new Mock<BCrypt>();
			mock.Setup(s => s.CheckPassword(password, customerAccount.Password));

			CustomerAccountEntity.BCrypt = mock.Object;

			customerAccount.CheckPassword(password);

			CustomerAccountEntity.BCrypt = new BCrypt();

			mock.VerifyAll();
		}

		[TestMethod]
		public void CheckPassword_WithBcryptStubForTrue_ReturnsTrue()
		{
			CustomerAccountEntity customerAccount = new CustomerAccountEntity();

			var stub = new Mock<BCrypt>();
			stub.Setup(s => s.CheckPassword(It.IsAny<string>(), It.IsAny<string>())).Returns(true);

			CustomerAccountEntity.BCrypt = stub.Object;

			bool result = customerAccount.CheckPassword("OK_Password");

			CustomerAccountEntity.BCrypt = new BCrypt();

			Assert.IsTrue(result);
		}

		[TestMethod]
		public void CheckPassword_WithBcryptStubForFalse_ReturnsFalse()
		{
			CustomerAccountEntity customerAccount = new CustomerAccountEntity();

			var stub = new Mock<BCrypt>();
			stub.Setup(s => s.CheckPassword(It.IsAny<string>(), It.IsAny<string>())).Returns(false);

			CustomerAccountEntity.BCrypt = stub.Object;

			bool result = customerAccount.CheckPassword("OK_Password");

			CustomerAccountEntity.BCrypt = new BCrypt();

			Assert.IsFalse(result);
		}

		#endregion

		#region SetPassword

		[TestMethod]
		public void SetPassword_WithBcryptStubForHash_CorrectPasswordIsSet()
		{
			CustomerAccountEntity customerAccount = new CustomerAccountEntity();

			const string hash = "OK_Hash";
			const string password = "OK_Password";

			var stub = new Mock<BCrypt>();
			stub.Setup(s => s.HashPassword(It.IsAny<string>(), It.IsAny<string>())).Returns(hash);

			CustomerAccountEntity.BCrypt = stub.Object;

			customerAccount.SetPassword(password);

			CustomerAccountEntity.BCrypt = new BCrypt();

			Assert.AreEqual(hash, customerAccount.Password);
		}

		[TestMethod]
		public void SetPassword_WithBcryptMock_BcryptWasCalled()
		{
			CustomerAccountEntity customerAccount = new CustomerAccountEntity();

			const string password = "OK_Password";
			const string salt = "OK_Salt";

			var mock = new Mock<BCrypt>();
			mock.Setup(s => s.GenerateSalt()).Returns(salt);
			mock.Setup(s => s.HashPassword(password, salt));

			CustomerAccountEntity.BCrypt = mock.Object;

			customerAccount.SetPassword(password);

			CustomerAccountEntity.BCrypt = new BCrypt();

			mock.VerifyAll();
		}

		#endregion

		#region ChangePassword

        private Mock<CustomerAccountEntity> ChangePassword_AccountMock()
        {
            var account = new Mock<CustomerAccountEntity>() { CallBase = true };
            account.Setup(s => s.IsNew).Returns(false);
            account.Setup(s => s.State).Returns(CustomerAccountState.Active);
            account.Setup(c => c.Contract).Returns(new ContractEntity() { AlreadyFetchedContractHistoryItems = true });
            account.Setup(s => s.CheckPassword(It.IsAny<string>())).Returns(true);
            return account;
        }

		[TestMethod]
		public void ChangePassword_WithCustomerAccountStub_SetPasswordIsCalled()
		{
			const string oldPassword = "OK_OldPassword";
			const string newPassword = "OK_NewPassword";

            var stub = ChangePassword_AccountMock();

			stub.Setup(s => s.SetPassword(newPassword));

			stub.Object.ChangePassword(oldPassword, newPassword);

			stub.VerifyAll();
		}

		[TestMethod]
		public void ChangePassword_WithCustomerAccountStub_CheckPasswordIsCalled()
		{
			const string oldPassword = "OK_OldPassword";
			const string newPassword = "OK_NewPassword";

            var stub = ChangePassword_AccountMock();

			stub.Object.ChangePassword(oldPassword, newPassword);

			stub.VerifyAll();
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void ChangePassword_WithCheckPasswordStubForFalse_ThrowsException()
		{
			const string oldPassword = "OK_OldPassword";
			const string newPassword = "OK_NewPassword";

            var stub = ChangePassword_AccountMock();

			stub.Setup(s => s.CheckPassword(It.IsAny<string>())).Returns(false);

			stub.Object.ChangePassword(oldPassword, newPassword);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void ChangePassword_WithNewCustomerAccount_ThrowsException()
		{
			const string oldPassword = "OK_OldPassword";
			const string newPassword = "OK_NewPassword";

            var stub = ChangePassword_AccountMock();

			stub.Setup(s => s.IsNew).Returns(true);

			stub.Object.ChangePassword(oldPassword, newPassword);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void ChangePassword_CustomerAccountWithStateNotActive_ThrowsException()
		{
			const string oldPassword = "OK_OldPassword";
			const string newPassword = "OK_NewPassword";

            var stub = ChangePassword_AccountMock();

			stub.Setup(s => s.State).Returns(CustomerAccountState.Inactive);

			stub.Object.ChangePassword(oldPassword, newPassword);
		}

        [TestMethod]
        public void ChangePassword_WithStub_ContractHistoryGetsAdded()
        {
            var stub = ChangePassword_AccountMock();
            stub.Setup(s => s.CheckPassword(It.IsAny<string>())).Returns(true);
            
            stub.Object.ChangePassword("", "");
            Assert.IsTrue(stub.Object.Contract.ContractHistoryItems.Single().Text.StartsWith("Changed"));
        }

		#endregion

		#region Login

		[TestMethod]
		public void Login_WithCustomerAccountStub_CheckPasswordIsCalled()
		{
			const string password = "OK_Password";

			var stub = new Mock<CustomerAccountEntity>() { CallBase = true };
			stub.Setup(s => s.IsNew).Returns(false);
			stub.Setup(s => s.CheckPassword(It.IsAny<string>())).Returns(true);
			stub.Setup(s => s.State).Returns(CustomerAccountState.Active);

			stub.Object.Login(password);

			stub.VerifyAll();
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void Login_WithCheckPasswordStubForFalse_ThrowsException()
		{
			const string password = "OK_Password";

			var stub = new Mock<CustomerAccountEntity>() { CallBase = true };
			stub.Setup(s => s.IsNew).Returns(false);
			stub.Setup(s => s.State).Returns(CustomerAccountState.Active);

			stub.Setup(s => s.CheckPassword(password)).Returns(false);

			stub.Object.Login(password);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void Login_WithNewCustomerAccount_ThrowsException()
		{
			const string password = "OK_Password";

			var stub = new Mock<CustomerAccountEntity>() { CallBase = true };
			stub.Setup(s => s.CheckPassword(It.IsAny<string>())).Returns(true);
			stub.Setup(s => s.State).Returns(CustomerAccountState.Active);

			stub.Setup(s => s.IsNew).Returns(true);

			stub.Object.Login(password);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void Login_CustomerAccountWithStateNotActive_ThrowsException()
		{
			const string password = "OK_Password";

			var stub = new Mock<CustomerAccountEntity>() { CallBase = true };
			stub.Setup(s => s.IsNew).Returns(false);
			stub.Setup(s => s.CheckPassword(It.IsAny<string>())).Returns(true);

			stub.Setup(s => s.State).Returns(CustomerAccountState.Inactive);

			stub.Object.Login(password);
		}


		#endregion

		#region Activate

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void ActivateWithNewPassword_WithNewAccount_ThrowsException()
		{
			var mock = GetActivatableAccountMock();
			mock.Setup(c => c.IsNew).Returns(true);
			mock.Object.ActivateWithNewPassword("", "");
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void ActivateWithNewPassword_WithWrongPassword_ThrowsException()
		{
			var mock = GetActivatableAccountMock();
			mock.Setup(c => c.CheckPassword(It.IsAny<string>())).Returns(false);
			mock.Object.ActivateWithNewPassword("", "");
		}

		[TestMethod]
		public void ActivateWithNewPassword_WithWrongUsernameAndPassword_ThrowsSameException()
		{
			var wrongPasswordMock = GetActivatableAccountMock();
			wrongPasswordMock.Setup(c => c.CheckPassword(It.IsAny<string>())).Returns(false);
			var newAccountMock = GetActivatableAccountMock();
			newAccountMock.Setup(c => c.IsNew).Returns(true);

			try
			{
				newAccountMock.Object.ActivateWithNewPassword("", "");
			}
			catch (InvalidOperationException e)
			{
				try
				{
					wrongPasswordMock.Object.ActivateWithNewPassword("", "");
				}
				catch (InvalidOperationException e2)
				{
					Assert.AreEqual(e.Message, e2.Message, "The exception messages should be the same for wrong username and password.");
					return;
				}
			}
			Assert.Fail("One or both exceptions didn't throw");
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void Activate_WithActiveState_ThrowsException()
		{
			GetActivatableAccountWithState(CustomerAccountState.Active).Activate();
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void Activate_WithClosedState_ThrowsException()
		{
			GetActivatableAccountWithState(CustomerAccountState.Closed).Activate();
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void Activate_WithDisabledState_ThrowsException()
		{
			GetActivatableAccountWithState(CustomerAccountState.Disabled).Activate();
		}

		[TestMethod]
		public void ActivateWithNewPassword_WithMock_SetPasswordGetsCalledWithNewPassword()
		{
			var newPassword = "new";
			var mock = GetActivatableAccountMock();
			mock.Object.ActivateWithNewPassword("old", newPassword);
			mock.Verify(c => c.SetPassword(newPassword));
		}

		[TestMethod]
		public void Activate_WithMock_StateGetsSetToActive()
		{
			var mock = GetActivatableAccountMock();
			mock.Object.Activate();
			Assert.AreEqual(CustomerAccountState.Active, mock.Object.State);
		}

		[TestMethod]
		public void Activate_WithMock_ContractStateGetsSetToActive()
		{
			var mock = GetActivatableAccountMock();
			mock.Object.Contract.State = ContractState.Inactive;
			mock.Object.Activate();
			Assert.AreEqual(ContractState.Active, mock.Object.Contract.State);
		}

		[TestMethod]
		public void Activate_WithMock_ContractHistoryGetsAdded()
		{
			var mock = GetActivatableAccountMock();
			mock.Object.Activate();
			Assert.IsTrue(mock.Object.Contract.ContractHistoryItems.Single().Text.StartsWith("Activated"));
		}

		[TestMethod]
		public void ActivateWithNewPassword_WithMock_ActivateGetsCalled()
		{
			var mock = GetActivatableAccountMock();
			mock.Setup(c => c.Activate()).Verifiable();
			mock.Object.ActivateWithNewPassword("", "");
			mock.Verify();
		}

		private CustomerAccountEntity GetActivatableAccountWithState(CustomerAccountState state)
		{
			var ret = GetActivatableAccountMock();
			ret.Setup(c => c.State).Returns(state);
			return ret.Object;
		}

		private Mock<CustomerAccountEntity> GetActivatableAccountMock()
		{
			var account = new Mock<CustomerAccountEntity>() { CallBase = true };
			account.Setup(c => c.IsNew).Returns(false);
			account.Setup(c => c.CheckPassword(It.IsAny<string>())).Returns(true);
			account.SetupProperty(c => c.State, CustomerAccountState.Inactive);

			account.Setup(c => c.Contract).Returns(new ContractEntity() { AlreadyFetchedContractHistoryItems = true });
			account.Setup(c => c.Person).Returns(new PersonEntity());
			return account;
		}

		#endregion

		#region AccountHash

		[TestMethod]
		public void AccountHash_RowTest()
		{
			var expected = new RowTest<CustomerAccountEntity, CustomerAccountEntity, bool>(acc => acc.AccountHash);

			expected.Add(new CustomerAccountEntity { }, true, "Account with the same fields did not have the same hash.");
			expected.Add(new CustomerAccountEntity { State = CustomerAccountState.Closed }, false, "AccountState is not included in the hash.");
			expected.Add(new CustomerAccountEntity { UserName = "test" }, false, "UserName is not included in the hash.");
			expected.Add(new CustomerAccountEntity { LastModified = DateTime.MaxValue }, false, "LastModified is not included in the hash.");

			expected.Assert(x => x, x => x.AccountHash == new CustomerAccountEntity().AccountHash);
		}

		#endregion
	}

}
