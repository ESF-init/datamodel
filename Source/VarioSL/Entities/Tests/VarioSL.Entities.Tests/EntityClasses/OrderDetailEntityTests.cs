﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class OrderDetailEntityTests
	{
		[TestMethod]
		public void IsPaidWithPaymentIDValue_ReturnsTrue()
		{
			var od = new OrderDetailEntity()
			{
				PaymentID = 5
			};

			Assert.IsTrue(od.IsPaid);
		}

		[TestMethod]
		public void IsPaidWithPaymentIDNull_ReturnsFalse()
		{
			var od = new OrderDetailEntity()
				{
					PaymentID = null
				};

			Assert.IsFalse(od.IsPaid);
		}

		[TestMethod]
		public void IsPaidWithSufficientPaymentJournalEntries_ReturnsTrue()
		{
			var od = new OrderDetailEntity()
			{
				Product = new ProductEntity()
				{
					Price = 100
				},
				OrderID = 123,
				Quantity = 3
			};
			var o = new OrderEntity()
			{
				OrderID = 123
			};

			var stub = new Mock<SaleEntity>() { CallBase = true };
			stub.Setup(sale => sale.PaymentJournals).Returns(new VarioSL.Entities.CollectionClasses.PaymentJournalCollection() 
				{ new PaymentJournalEntity() { Amount = 210 }, new PaymentJournalEntity() { Amount = 90 } });

			var orderDetailStub = new Mock<OrderDetailEntity>() { CallBase = true };
			orderDetailStub.Setup(orderDetail => orderDetail.PaymentID).Returns((long?)null);
			orderDetailStub.Setup(orderDetail => orderDetail.Order.OrderDetails).Returns(new VarioSL.Entities.CollectionClasses.OrderDetailCollection() { od });
			orderDetailStub.Setup(orderDetail => orderDetail.Order.Sale).Returns(stub.Object);

			Assert.IsTrue(orderDetailStub.Object.IsPaid);
		}

		[TestMethod]
		public void IsPaidWithInsufficientPaymentJournalEntries_ReturnsFalse()
		{
			var od = new OrderDetailEntity()
			{
				Product = new ProductEntity()
				{
					Price = 100
				},
				OrderID = 123,
				Quantity = 3
			};
			var o = new OrderEntity()
			{
				OrderID = 123
			};

			var stub = new Mock<SaleEntity>() { CallBase = true };
			stub.Setup(sale => sale.PaymentJournals).Returns(new VarioSL.Entities.CollectionClasses.PaymentJournalCollection() 
				{ new PaymentJournalEntity() { Amount = 10 }, new PaymentJournalEntity() { Amount = 20 } });

			var orderDetailStub = new Mock<OrderDetailEntity>() { CallBase = true };
			orderDetailStub.Setup(orderDetail => orderDetail.PaymentID).Returns((long?)null);
			orderDetailStub.Setup(orderDetail => orderDetail.Order.OrderDetails).Returns(new VarioSL.Entities.CollectionClasses.OrderDetailCollection() { od });
			orderDetailStub.Setup(orderDetail => orderDetail.Order.Sale).Returns(stub.Object);

			Assert.IsFalse(orderDetailStub.Object.IsPaid);
		}

		[TestMethod]
		public void PaymentStateWithDefault_WithValidPayment_ReturnsPaymentState()
		{
			var expected = PaymentState.Rejected;

			var od = new OrderDetailEntity()
				{
					Payment = new PaymentEntity() { State = expected }
				};

			Assert.AreEqual(expected, od.PaymentStateWithDefault);
		}

		[TestMethod]
		public void PaymentStateWithDefault_WithoutPayment_ReturnsPaymentStateUnknown()
		{
			var expected = PaymentState.Unknown;

			var od = new OrderDetailEntity()
			{
				Payment = null
			};

			Assert.AreEqual(expected, od.PaymentStateWithDefault);
		}

		[TestMethod]
		public void PaymentExecutionResultWithDefault_WithValidPayment_ReturnsPaymentExecutionResult()
		{
			var expected = "Passed";

			var od = new OrderDetailEntity()
			{
				Payment = new PaymentEntity() { ExecutionResult = expected }
			};

			Assert.AreEqual(expected, od.PaymentExecutionResultWithDefault);
		}

		[TestMethod]
		public void PaymentExecutionResultWithDefault_WithoutPayment_ReturnsStringEmpty()
		{
			var expected = string.Empty;

			var od = new OrderDetailEntity()
			{
				Payment = null
			};

			Assert.AreEqual(expected, od.PaymentExecutionResultWithDefault);
		}

		[TestMethod]
		public void PaymentExecutionTimeWithDefault_WithValidPayment_ReturnsPaymentExecutionTime()
		{
			var expected = DateTime.Now;

			var od = new OrderDetailEntity()
			{
				Payment = new PaymentEntity() { ExecutionTime = expected }
			};

			Assert.AreEqual(expected, od.PaymentExecutionTimeWithDefault);
		}

		[TestMethod]
		public void PaymentExecutionTimeWithDefault_WithoutPayment_ReturnsNull()
		{
			DateTime? expected = null;

			var od = new OrderDetailEntity()
			{
				Payment = null
			};

			Assert.AreEqual(expected, od.PaymentExecutionTimeWithDefault);
		}

		[TestMethod]
		public void PaymentDescriptionWithDefault_WithValidPayment_ReturnsPaymentDescription()
		{
			var expected = "Description";

			var od = new OrderDetailEntity()
			{
				Payment = new PaymentEntity() { Description = expected }
			};

			Assert.AreEqual(expected, od.PaymentDescriptionWithDefault);
		}

		[TestMethod]
		public void PaymentDescriptionWithDefault_WithoutPayment_ReturnsStringEmpty()
		{
			string expected = string.Empty;

			var od = new OrderDetailEntity()
			{
				Payment = null
			};

			Assert.AreEqual(expected, od.PaymentDescriptionWithDefault);
		}

		[TestMethod]
		public void PaymentTransactionOrderNumberWithDefault_WithValidPayment_ReturnsPaymentTransactionOrderNumber()
		{
			var expected = "TransactionOrderNumber";

			var od = new OrderDetailEntity()
			{
				Payment = new PaymentEntity() { TransactionOrderNumber = expected }
			};

			Assert.AreEqual(expected, od.PaymentTransactionOrderNumberWithDefault);
		}

		[TestMethod]
		public void PaymentTransactionOrderNumberWithDefault_WithoutPayment_ReturnsStringEmpty()
		{
			string expected = string.Empty;

			var od = new OrderDetailEntity()
			{
				Payment = null
			};

			Assert.AreEqual(expected, od.PaymentTransactionOrderNumberWithDefault);
		}

		[TestMethod]
		public void PaymentReceiptNumberWithDefault_WithValidPayment_ReturnsPaymentReceiptNumber()
		{
			var expected = "ReceiptOrderNumber";

			var od = new OrderDetailEntity()
			{
				Payment = new PaymentEntity() { ReceiptNumber = expected }
			};

			Assert.AreEqual(expected, od.PaymentReceiptNumberWithDefault);
		}

		[TestMethod]
		public void PaymentReceiptNumberWithDefault_WithoutPayment_ReturnsStringEmpty()
		{
			string expected = string.Empty;

			var od = new OrderDetailEntity()
			{
				Payment = null
			};

			Assert.AreEqual(expected, od.PaymentReceiptNumberWithDefault);
		}

		[TestMethod]
		public void PaymentFullResultWithDefault_WithValidPayment_ReturnsPaymentFullResult()
		{
			var expected = "FullResult";

			var od = new OrderDetailEntity()
			{
				Payment = new PaymentEntity() { FullResult = expected }
			};

			Assert.AreEqual(expected, od.PaymentFullResultWithDefault);
		}

		[TestMethod]
		public void PaymentFullResultWithDefault_WithoutPayment_ReturnsStringEmpty()
		{
			string expected = string.Empty;

			var od = new OrderDetailEntity()
			{
				Payment = null
			};

			Assert.AreEqual(expected, od.PaymentFullResultWithDefault);
		}

		[TestMethod]
		public void CardHolderID_EmptyOrder_ReturnsZero()
		{
			long expected = 0;
			var stub = new Mock<OrderDetailEntity>() { CallBase = true };
			stub.Setup(s => s.OrderDetailToCardHolder).Returns(new VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection());
			Assert.AreEqual(expected, stub.Object.CardHolderID);
		}

		[TestMethod]
		public void CardHolderID_FilledOrder_ReturnsCardHolderID()
		{
			long expected = 12345;
            var newOrderDetailToPersonCollection = new VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection();
			newOrderDetailToPersonCollection.Add(new OrderDetailToCardHolderEntity() { CardHolderID = expected });
			var stub = new Mock<OrderDetailEntity>() { CallBase = true };
			stub.Setup(s => s.OrderDetailToCardHolder).Returns(newOrderDetailToPersonCollection);
			Assert.AreEqual(expected, stub.Object.CardHolderID);
		}
	}
}
