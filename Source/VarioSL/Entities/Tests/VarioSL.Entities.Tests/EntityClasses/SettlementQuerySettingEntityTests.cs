﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class SettlementQuerySettingEntityTests
	{
		#region ReleaseState

		[TestMethod]
		public void IsUpdateAllowed_WithRevokedSetup_ReturnsFalse()
		{
			var querySetting = CreateExistingSettlementQuerrySetting();
			querySetting.SettlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.Revoked);
			Assert.AreEqual(querySetting.IsUpdateAllowed, false);
		}

		[TestMethod]
		public void IsUpdateAllowed_WithReleasedSetup_ReturnsFalse()
		{
			var querySetting = CreateExistingSettlementQuerrySetting();
			querySetting.SettlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.Released);
			Assert.AreEqual(querySetting.IsUpdateAllowed, false);
		}

		[TestMethod]
		public void IsUpdateAllowed_WithNotReleasedSetup_ReturnsTrue()
		{
			var querySetting = CreateExistingSettlementQuerrySetting();
			querySetting.SettlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.NotReleased);
			Assert.AreEqual(querySetting.IsUpdateAllowed, true);
		}

		#endregion

		#region HasQueryValues

		[TestMethod]
		public void HasQueryValues_WithoutQueryValues_returns_False()
		{
			var settlementSetup = CreateQuerySettingWithValues(new List<SettlementQueryValueEntity>());
			Assert.AreEqual(settlementSetup.HasQueryValues, false);
		}

		[TestMethod]
		public void HasQuerySettings_WithQuerySettings_returns_True()
		{
			var settlementSetup = CreateQuerySettingWithValues(new List<SettlementQueryValueEntity>() { new SettlementQueryValueEntity() { Value = 0} });
			Assert.AreEqual(settlementSetup.HasQueryValues, true);
		}

		private SettlementQuerySettingEntity CreateQuerySettingWithValues(List<SettlementQueryValueEntity> queryvalues)
		{
			var output = new SettlementQuerySettingEntity()
			{
								IsNew = false,
								AlreadyFetchedSettlementQueryValues =true,
								AlreadyFetchedSettlementQuerySettingToTickets=true,
								AlreadyFetchedSettlementSetup=true,
								AlreadyFetchedTickets=true
			};
			output.SettlementQueryValues.AddRange(queryvalues);
			return output;
		}

		#endregion

		private static SettlementQuerySettingEntity CreateExistingSettlementQuerrySetting()
		{
			return new SettlementQuerySettingEntity()
			{
				IsNew = false,
				AlreadyFetchedSettlementQuerySettingToTickets = true,
				AlreadyFetchedSettlementQueryValues = true,
				AlreadyFetchedSettlementSetup = true,
				AlreadyFetchedTickets = true
			};
		}

		private static SettlementSetupEntity CreateSettlementSetupWithState(SettlementReleaseState state)
		{
			var ret = new SettlementSetupEntity() { IsNew = false };
			ret.ReleaseState = state;
			return ret;
		}

	}

}
