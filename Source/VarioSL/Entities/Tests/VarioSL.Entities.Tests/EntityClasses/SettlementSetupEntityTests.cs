﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;
using VaroSL.Entities.TestFramework;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class SettlementSetupEntityTests
	{
		#region ReleaseState

		[TestMethod]
		public void ReleaseState_SettlementReleaseStateEnumCount()
		{
			Assert.AreEqual(4, Enum.GetNames(typeof(SettlementReleaseState)).Length);
		}

		#endregion

		#region Revoke

		[TestMethod, ExpectedException(typeof(InvalidOperationException), "Settlement setup is allready revoked.")]
		public void Revoke_WithReleaseState_Revoked_ThrowsException()
		{
			var settlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.Revoked);
			settlementSetup.Revoke();
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException), "Settlement setup is not released yet.")]
		public void Revoke_WithReleaseState_NotReleased_ThrowsException()
		{
			var settlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.NotReleased);
			settlementSetup.Revoke();
		}

		[TestMethod]
		public void Revoke_WithReleaseState_Released_SetsReleaseState()
		{
			var settlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.Released);
			settlementSetup.Revoke();
			Assert.AreEqual(settlementSetup.ReleaseState, SettlementReleaseState.Revoked);
		}

		#endregion

		#region Properties

		[TestMethod]
		public void IsUpdateAllowed_WithAllStates_ReturnsExpectedStates()
		{
			var expected = new RowTest<SettlementSetupEntity, SettlementReleaseState, bool>(state => state.ToString());
			expected.Add(SettlementReleaseState.NotReleased, true);
			expected.Add(SettlementReleaseState.Released, false);
			expected.Add(SettlementReleaseState.Revoked, false);
			expected.Add(SettlementReleaseState.Removed, false);
			expected.Assert((x) => CreateSettlementSetupWithState(x), x => x.IsUpdateAllowed);
		}

		[TestMethod]
		public void IsReleaseAllowed_WithAllStates_ReturnsExpectedStates()
		{
			var expected = new RowTest<SettlementSetupEntity, SettlementReleaseState, bool>(state => state.ToString());
			expected.Add(SettlementReleaseState.NotReleased, true);
			expected.Add(SettlementReleaseState.Released, false);
			expected.Add(SettlementReleaseState.Revoked, false);
			expected.Add(SettlementReleaseState.Removed, false);
			expected.Assert((x) => CreateSettlementSetupWithState(x), x => x.IsReleaseAllowed);
		}

		[TestMethod]
		public void IsRevoke_WithAllStates_ReturnsExpectedStates()
		{
			var expected = new RowTest<SettlementSetupEntity, SettlementReleaseState, bool>(state => state.ToString());
			expected.Add(SettlementReleaseState.NotReleased, false);
			expected.Add(SettlementReleaseState.Released, true);
			expected.Add(SettlementReleaseState.Revoked, false);
			expected.Add(SettlementReleaseState.Removed, false);
			expected.Assert((x) => CreateSettlementSetupWithState(x), x => x.IsRevokeAllowed);
		}

		[TestMethod]
		public void IsUndoReleaseAllowed_WithAllStates_ReturnsExpectedStates()
		{
			var expected = new RowTest<SettlementSetupEntity, SettlementReleaseState, bool>(state => state.ToString());
			expected.Add(SettlementReleaseState.NotReleased, false);
			expected.Add(SettlementReleaseState.Released, true);
			expected.Add(SettlementReleaseState.Revoked, false);
			expected.Add(SettlementReleaseState.Removed, false);
			expected.Assert((x) => CreateSettlementSetupWithState(x), x => x.IsUndoReleaseAllowed);
		}

		[TestMethod]
		public void IsRemoveAllowed_WithAllStates_ReturnsExpectedStates()
		{
			var expected = new RowTest<SettlementSetupEntity, SettlementReleaseState, bool>(state => state.ToString());
			expected.Add(SettlementReleaseState.NotReleased, true);
			expected.Add(SettlementReleaseState.Released, false);
			expected.Add(SettlementReleaseState.Revoked, true);
			expected.Add(SettlementReleaseState.Removed, false);
			expected.Assert((x) => CreateSettlementSetupWithState(x), x => x.IsRemoveAllowed);
		}

		[TestMethod]
		public void IsPurgeAllowed_WithAllStates_ReturnsExpectedStates()
		{
			var expected = new RowTest<SettlementSetupEntity, SettlementReleaseState, bool>(state => state.ToString());
			expected.Add(SettlementReleaseState.NotReleased, true);
			expected.Add(SettlementReleaseState.Released, false);
			expected.Add(SettlementReleaseState.Revoked, false);
			expected.Add(SettlementReleaseState.Removed, false);
			expected.Assert((x) => CreateSettlementSetupWithState(x), x => x.IsPurgeAllowed);
		}

		#endregion

		#region Remove

		[TestMethod, ExpectedException(typeof(InvalidOperationException), "Remove settlement setup is not allowed.")]
		public void Remove_WithReleaseState_Removed_ThrowsException()
		{
			var settlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.Removed);
			settlementSetup.Remove();
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException), "Remove settlement setup is not allowed.")]
		public void Remove_WithReleaseState_Released_ThrowsException()
		{
			var settlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.Released);
			settlementSetup.Remove();
		}

		[TestMethod]
		public void Remove_WithReleaseState_Revoked_SetsReleaseState()
		{
			var settlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.Revoked);
			settlementSetup.Remove();
			Assert.AreEqual(settlementSetup.ReleaseState, SettlementReleaseState.Removed);
		}

		[TestMethod]
		public void Remove_WithReleaseState_NotReleased_SetsReleaseState()
		{
			var settlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.NotReleased);
			settlementSetup.Remove();
			Assert.AreEqual(settlementSetup.ReleaseState, SettlementReleaseState.Removed);
		}

		#endregion

		#region Release

		[TestMethod, ExpectedException(typeof(InvalidOperationException), "Settlement setup is allready released.")]
		public void Release_WithReleaseState_Released_ThrowsException()
		{
			var settlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.Released);
			settlementSetup.Release();
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException), "Settlement setup has been revoked.")]
		public void Release_WithReleaseState_Revoked_ThrowsException()
		{
			var settlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.Revoked);
			settlementSetup.ReleaseState = SettlementReleaseState.Revoked;
			settlementSetup.Release();
		}

		[TestMethod]
		public void Release_WithReleaseState_NotReleased_SetsReleaseState()
		{
			var settlementSetup = CreateSettlementSetupWithState(SettlementReleaseState.NotReleased);
			settlementSetup.Release();
			Assert.AreEqual(settlementSetup.ReleaseState, SettlementReleaseState.Released);
		}

		#endregion

		#region HasQuerySettings

		[TestMethod]
		public void HasQuerySettings_WithoutQuerySettings_returns_False()
		{
			var settlementSetup = CreateSetupWithQuerySetting(new List<SettlementQuerySettingEntity>());
			Assert.AreEqual(settlementSetup.HasQuerySettings, false);
		}

		[TestMethod]
		public void HasQuerySettings_WithQuerySettings_returns_True()
		{
			var settlementSetup = CreateSetupWithQuerySetting(new List<SettlementQuerySettingEntity>() { new SettlementQuerySettingEntity(){Name="test"}});
			Assert.AreEqual(settlementSetup.HasQuerySettings, true);
		}

		private SettlementSetupEntity CreateSetupWithQuerySetting(List<SettlementQuerySettingEntity> querySettings)
		{
			var output= new SettlementSetupEntity()
			{
				ReleaseState = SettlementReleaseState.NotReleased,
				AlreadyFetchedSettlementQuerySettings = true,
				AlreadyFetchedSettlementRuns = true,
				FormulaString = string.Empty,
				IsNew = false
			};
			output.SettlementQuerySettings.AddRange(querySettings);
			return output;
		}

		#endregion

		#region HasSettlementRuns


		[TestMethod]
		public void HasRuns_WithoutQuerySettings_returns_False()
		{
			var settlementSetup = CreateSetupWithSettlementRuns(new List<SettlementRunEntity>());
			Assert.AreEqual(settlementSetup.HasRuns, false);
		}

		[TestMethod]
		public void HasRuns_WithQuerySettings_returns_True()
		{
			var settlementSetup = CreateSetupWithSettlementRuns(new List<SettlementRunEntity>() { new SettlementRunEntity() {  } });
			Assert.AreEqual(settlementSetup.HasRuns, true);
		}

		private SettlementSetupEntity CreateSetupWithSettlementRuns(List<SettlementRunEntity> runs)
		{
			var output = new SettlementSetupEntity()
			{
				ReleaseState = SettlementReleaseState.NotReleased,
				AlreadyFetchedSettlementQuerySettings = true,
				AlreadyFetchedSettlementRuns = true,
				FormulaString = string.Empty,
				IsNew = false
			};
			output.SettlementRuns.AddRange(runs);
			return output;
		}

		#endregion

		private static SettlementSetupEntity CreateSettlementSetupWithState(SettlementReleaseState state)
		{
			var ret = new SettlementSetupEntity() { IsNew = false };
			ret.ReleaseState = state;
			return ret;
		}
	}

}
