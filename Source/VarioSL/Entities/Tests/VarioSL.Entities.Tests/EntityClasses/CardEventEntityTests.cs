﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;

namespace VarioSL.Entities.Test.EntityClasses
{

	[TestClass]
	public class CardEventEntityTests
	{
		#region CreateEventIssue

		[TestMethod]
		public void CreateEventIssue_WithExistingCard_CardEventTypeIsIssue()
		{
			CardEntity card = new CardEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventIssue(card);

			Assert.IsTrue(ce.CardEventType == CardEventType.Issue);
		}

		[TestMethod]
		public void CreateEventIssue_WithExistingCard_DescriptionIsSet()
		{
			CardEntity card = new CardEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventIssue(card);

			Assert.AreEqual("Smartcard was issued.", ce.Description);
		}

		[TestMethod]
		public void CreateEventIssue_WithExistingCard_DateIsSet()
		{
			CardEntity card = new CardEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventIssue(card);

			Assert.AreNotEqual(DateTime.MinValue, ce.EventTime);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void CreateEventIssue_WithNewCard_ThrowsException()
		{
			CardEntity card = new CardEntity();

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventIssue(card);
		}

		#endregion

		#region CreateEventAssign

		[TestMethod]
		public void CreateEventAssign_WithExistingCardAndContract_CardEventTypeIsAssign()
		{
			CardEntity card = new CardEntity() { IsNew = false };
			ContractEntity contract = new ContractEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventAssign(card, contract);

			Assert.IsTrue(ce.CardEventType == CardEventType.Assign);
		}

		[TestMethod]
		public void CreateEventAssign_WithExistingCardAndContract_DescriptionIsSet()
		{
			CardEntity card = new CardEntity() { IsNew = false };
			ContractEntity contract = new ContractEntity() { IsNew = false, ContractNumber = "OK_ContractNumber" };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventAssign(card, contract);

			Assert.AreEqual(string.Format("Smartcard was assigned to contract ({0}).", contract.ContractNumber), ce.Description);
		}

		[TestMethod]
		public void CreateEventAssign_WithExistingCardAndContract_DateIsSet()
		{
			CardEntity card = new CardEntity() { IsNew = false };
			ContractEntity contract = new ContractEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventAssign(card, contract);

			Assert.AreNotEqual(DateTime.MinValue, ce.EventTime);
		}

		[TestMethod]
		public void CreateEventAssign_WithExistingCardAndContract_ContractIDIsSet()
		{
			CardEntity card = new CardEntity() { IsNew = false };
			ContractEntity contract = new ContractEntity() { IsNew = false, ContractID = 1 };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventAssign(card, contract);

			Assert.IsTrue(ce.ContractID == contract.ContractID);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void CreateEventAssign_WithNewCardAndExistingContract_ThrowsException()
		{
			ContractEntity contract = new ContractEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventAssign(new CardEntity(), contract);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void CreateEventAssign_WithExistingCardAndNewContract_ThrowsException()
		{
			CardEntity card = new CardEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventAssign(card, new ContractEntity());
		}
		
		#endregion

		#region CreateEventReplace

		[TestMethod]
		public void CreateEventReplace_WithExistingCards_CardEventTypeIsReplace()
		{
			CardEntity oldCard = new CardEntity() { IsNew = false };
			CardEntity newCard = new CardEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventReplace(oldCard, newCard, true);

			Assert.IsTrue(ce.CardEventType == CardEventType.Replace);
		}

		[TestMethod]
		public void CreateEventReplace_ForNewCard_DescriptionIsSet()
		{
			CardEntity oldCard = new CardEntity() { IsNew = false };
			CardEntity newCard = new CardEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventReplace(oldCard, newCard, true);

			Assert.AreEqual(string.Format("Old card printed card number {0}.", oldCard.PrintedNumber), ce.Description);
		}
			
		[TestMethod]
		public void CreateEventReplace_ForOldCard_DescriptionIsSet()
		{
			CardEntity oldCard = new CardEntity() { IsNew = false };
			CardEntity newCard = new CardEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventReplace(oldCard, newCard, false);

			Assert.AreEqual(string.Format("New card printed card number {0}.", newCard.PrintedNumber), ce.Description);
		}

		[TestMethod]
		public void CreateEventReplace_ForNewCard_ReasonIsSet()
		{
			CardEntity oldCard = new CardEntity() { IsNew = false };
			CardEntity newCard = new CardEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventReplace(oldCard, newCard, true);

			Assert.AreEqual("Is replacement", ce.Reason);
		}

		[TestMethod]
		public void CreateEventReplace_ForOldCard_ReasonIsSet()
		{
			CardEntity oldCard = new CardEntity() { IsNew = false };
			CardEntity newCard = new CardEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventReplace(oldCard, newCard, false);

			Assert.AreEqual("Was replaced", ce.Reason);
		}

		[TestMethod]
		public void CreateEventReplac_WithExistingCards_DateIsSet()
		{
			CardEntity oldCard = new CardEntity() { IsNew = false };
			CardEntity newCard = new CardEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventReplace(oldCard, newCard, true);

			Assert.AreNotEqual(DateTime.MinValue, ce.EventTime);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void CreateEventReplace_SameCard_ThrowsException()
		{
			CardEntity card = new CardEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventReplace(card, card, true);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void CreateEventReplace_OldCardIsNew_ThrowsException()
		{
			CardEntity oldCard = new CardEntity() { IsNew = true };
			CardEntity newCard = new CardEntity() { IsNew = false };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventReplace(oldCard, newCard, true);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void CreateEventReplace_NewCardIsNew_ThrowsException()
		{
			CardEntity oldCard = new CardEntity() { IsNew = false };
			CardEntity newCard = new CardEntity() { IsNew = true };

			CardEventEntity ce = CardEventEntity.EventGenerator.CreateEventReplace(oldCard, newCard, true);
		}
	

		#endregion

	}

}
