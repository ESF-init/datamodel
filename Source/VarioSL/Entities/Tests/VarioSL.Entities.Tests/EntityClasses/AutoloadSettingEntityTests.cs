﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;

namespace VarioSL.Entities.Test.EntityClasses
{
    [TestClass]
    public class AutoloadSettingEntityTests
    {
        #region CreateAutoloadPass

        [TestMethod]
        public void CreateAutoloadPass_WithThreshold_TriggerTypeIsSetToInterval()
        {
            var data = CreateAutoloadSettingWithTriggerType(TriggerType.Threshold);

            data.CreateAutoloadPass(CreateProduct(), CreatePaymentOption());

            Assert.AreEqual(TriggerType.Interval, data.TriggerType);
        }

        [TestMethod]
        public void CreateAutoloadPass_WithValidOptions_ConditionIsSetToUndeterminedFrequency()
        {
            var data = CreateAutoloadSettingWithTriggerType();

            data.CreateAutoloadPass(CreateProduct(), CreatePaymentOption());

            Assert.AreEqual(Frequency.Undetermined, data.Condition);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateAutoloadPass_WithNewProduct_ThrowsException()
        {
            var data = CreateAutoloadSettingWithTriggerType();

            data.CreateAutoloadPass(new ProductEntity(), CreatePaymentOption());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateAutoloadPass_WithCanceledProduct_ThrowsException()
        {
            var data = CreateAutoloadSettingWithTriggerType();
            var product = CreateProduct();

            product.IsCanceled = true;

            data.CreateAutoloadPass(product, CreatePaymentOption());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateAutoloadPass_WithExistingAutoload_ThrowsException()
        {
            var data = CreateAutoloadSettingWithTriggerType();
            var product = CreateProduct();

            product.AutoloadSettings.Add(new AutoloadSettingEntity());

            data.CreateAutoloadPass(product, CreatePaymentOption());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateAutoloadPass_WithNewPaymentOption_ThrowsException()
        {
            var data = CreateAutoloadSettingWithTriggerType();

            data.CreateAutoloadPass(CreateProduct(), CreatePaymentOption(true));
        }

        [TestMethod]
        public void CreateAutoloadPass_WithPrice_AmountIsSetToPrice()
        {
            var data = CreateAutoloadSettingWithTriggerType();
            var product = CreateProduct();
            const int VALID_PRICE = 1;
            product.Price = VALID_PRICE;

            data.CreateAutoloadPass(product, CreatePaymentOption());

            Assert.AreEqual(VALID_PRICE, data.Amount, "Amount was not set to product price.");
        }

        #endregion

        #region CreateAutoloadTopUp

        [TestMethod]
        public void CreateAutoloadPass_WithInterval_ReturnsThreshold()
        {
            var data = CreateAutoloadSettingWithTriggerType(TriggerType.Interval);
            data.CreateAutoloadTopUp(CreateCard(), CreatePaymentOption(), CreateTickets());
            Assert.AreEqual(TriggerType.Threshold, data.TriggerType);
        }

        [TestMethod]
        public void CreateAutoloadTopUp_ReturnsUndeterminedFrequency()
        {
            var data = CreateAutoloadSettingWithTriggerType();
            data.CreateAutoloadTopUp(CreateCard(), CreatePaymentOption(), CreateTickets());
            Assert.AreEqual(Frequency.Undetermined, data.Condition);
        }


        [TestMethod]
        public void CreateAutoloadTopUp_WithMock_CallsAllowNewAutoload()
        {
            var mock = CreateAllowNewAutoloadMock(true);

            var data = CreateAutoloadSettingWithTriggerType();
            data.CreateAutoloadTopUp(mock.Object, CreatePaymentOption(), CreateTickets());
            mock.Verify(foo => foo.IsAddAutoloadAllowed, Times.AtLeastOnce());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateAutoloadTopUp_WithForbiddenAutoload_ThrowsException()
        {
            var mock = CreateAllowNewAutoloadMock(false);

            var data = CreateAutoloadSettingWithTriggerType();
            data.CreateAutoloadTopUp(mock.Object, CreatePaymentOption(), CreateTickets());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateAutoloadTopUp_WithNullTickets_ThrowsException()
        {
            var data = CreateAutoloadSettingWithTriggerType();
            data.CreateAutoloadTopUp(CreateCard(), CreatePaymentOption(), null);
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateAutoloadTopUp_WithEmptyTickets_ThrowsException()
        {
            var data = CreateAutoloadSettingWithTriggerType();
            data.CreateAutoloadTopUp(CreateCard(), CreatePaymentOption(), CreateTickets(0));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateAutoloadTopUp_WithNewPaymentOption_ThrowsException()
        {
            var data = CreateAutoloadSettingWithTriggerType();
            data.CreateAutoloadTopUp(CreateCard(), CreatePaymentOption(true), CreateTickets());
        }

        [TestMethod]
        public void CreateAutoloadPass_SetsProduct()
        {
            var data = CreateAutoloadSettingWithTriggerType(TriggerType.Interval);
            data.CreateAutoloadTopUp(CreateCard(), CreatePaymentOption(), CreateTickets());
            Assert.IsNotNull(data.Product);
        }

        #endregion

        #region IsTopUpAutoload

        [TestMethod]
        public void IsTopUpAutoload_TickedTypeEnumCount_Is43()
        {
            Assert.AreEqual(43, Enum.GetNames(typeof(TicketType)).Length);
        }

        [TestMethod]
        public void IsTopUpAutoload_WithTopUpProduct_ReturnsTrue()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.TopUp);
            Assert.IsTrue(data.IsTopUpAutoload);
        }

        [TestMethod]
        public void IsTopUpAutoload_WithPassProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.Pass);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

        [TestMethod]
        public void IsTopUpAutoload_WithCashSaverProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.CashSaver);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

        [TestMethod]
        public void IsTopUpAutoload_WithDriverIssuedProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.DriverIssued);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

        [TestMethod]
        public void IsTopUpAutoload_WithNormalTicketProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.NormalTicket);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

        [TestMethod]
        public void IsTopUpAutoload_WithPhysicalCardProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.PhysicalCard);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

        [TestMethod]
        public void IsTopUpAutoload_WithPunchProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.Punch);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

        [TestMethod]
        public void IsTopUpAutoload_WithZeroValueTicketProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.ZeroValueTicket);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

        [TestMethod]
        public void IsTopUpAutoload_WithRefundTicketProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.Refund);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

		[TestMethod]
		public void IsTopUpAutoload_WithRefundPurseProduct_ReturnsFalse()
		{
			var data = CreateAutoloadSettingWithTicketType(TicketType.RefundPurse);
			Assert.IsFalse(data.IsTopUpAutoload);
		}

		[TestMethod]
        public void IsTopUpAutoload_WithGoodwillProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.Goodwill);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

        [TestMethod]
        public void IsTopUpAutoload_WithCappingProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.Capping);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

        [TestMethod]
        public void IsTopUpAutoload_WithPurseProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.Purse);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

        [TestMethod]
        public void IsTopUpAutoload_ReplacementFeeProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.ReplacementFee);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

		[TestMethod]
		public void IsTopUpAutoload_WithPaperTicketProduct_ReturnsFalse()
		{
			var data = CreateAutoloadSettingWithTicketType(TicketType.PaperTicketPass);
			Assert.IsFalse(data.IsTopUpAutoload);
		}

        [TestMethod]
        public void IsTopUpAutoload_WithComboTicketProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.ComboTicket);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

        [TestMethod]
        public void IsTopUpAutoload_WithCheckInCheckOutProduct_ReturnsFalse()
        {
            var data = CreateAutoloadSettingWithTicketType(TicketType.CheckInCheckOut);
            Assert.IsFalse(data.IsTopUpAutoload);
        }

        #endregion

        #region Remove

        [TestMethod]
        public void Remove_WithPass_AddsSelfToUnitOfWork()
        {
            var entity = CreateAutoloadSettingWithTicketType(TicketType.Pass);
            var mock = new Mock<IUnitOfWorkCore>(MockBehavior.Strict);
            mock.Setup(uow => uow.AddForDelete(entity));

            entity.Remove(mock.Object);

            mock.VerifyAll();
        }

        [TestMethod]
        public void Remove_WithTopUp_AddsSelfAndProductToUnitOfWork()
        {
            var entity = CreateAutoloadSettingWithTicketType(TicketType.TopUp);
            var mock = new Mock<IUnitOfWorkCore>(MockBehavior.Strict);
            mock.Setup(uow => uow.AddForDelete(entity));
            entity.Remove(mock.Object);
			 
            mock.VerifyAll();
        }

        #endregion

        private static AutoloadSettingEntity CreateAutoloadSettingWithTriggerType(TriggerType triggerType = TriggerType.Interval)
        {
            var ret = new AutoloadSettingEntity()
                {
                    TriggerType = triggerType
                };
            return ret;
        }

        private static AutoloadSettingEntity CreateAutoloadSettingWithTicketType(TicketType t)
        {
            var ret = new AutoloadSettingEntity()
                {
                    Product = ProductEntityTests.CreateProductWithTicketType(t)
                };
            return ret;
        }

        private static ProductEntity CreateProduct()
        {
            var ret = new ProductEntity()
                {
                    IsNew = false,
                    AlreadyFetchedAutoloadSettings = true
                };
            return ret;
        }

        private static PaymentOptionEntity CreatePaymentOption(bool isNew = false)
        {
            var ret = new PaymentOptionEntity()
                {
                    IsNew = isNew
                };
            return ret;
        }

        private static CardEntity CreateCard(bool active = true)
        {
            return new CardEntity()
                {
                    State = active ? CardState.Active : CardState.Inactive,
                    IsNew = false,
                    AlreadyFetchedProducts = true,
                    CardPhysicalDetail = new CardPhysicalDetailEntity()
                };
        }

        private static IEnumerable<long> CreateTickets(int count = 1)
        {
            return Enumerable.Range(0, count).Select(i => (long)i);
        }

        private static Mock<CardEntity> CreateAllowNewAutoloadMock(bool allow)
        {
            var mock = new Mock<CardEntity>();
            mock.Setup(foo => foo.IsAddAutoloadAllowed).Returns(allow);
            return mock;
        }
    }
}
