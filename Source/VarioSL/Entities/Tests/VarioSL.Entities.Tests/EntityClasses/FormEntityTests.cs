﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;

namespace VarioSL.Entities.Tests.EntityClasses
{
	[TestClass]
	public class FormEntityTests
	{
		private string _givenSchemaName;
		private Func<FormEntity> Creator { get; set; }

		[TestInitialize]
		public void TestInitialize()
		{
			Creator = () => new FormEntity()
			{
				IsNew = false,
				SchemaName = _givenSchemaName,
			};
		}

		[TestMethod]
		public void SchemaNameResolved_Parses_Known_Schema()
		{
			_givenSchemaName = FormSchemaName.Smartcard.ToString();
			var form = Creator();

			Assert.AreEqual(_givenSchemaName, form.SchemaNameResolved.ToString());
		}

		[TestMethod]
		public void SchemaNameResolved_Parses_CaseSensitive()
		{
			_givenSchemaName = FormSchemaName.Smartcard.ToString().ToUpper();
			var form = Creator();

			Assert.AreEqual(FormSchemaName.Unknown, form.SchemaNameResolved);
		}

		[TestMethod]
		public void SchemaNameResolved_Returns_Unknown__When_SchemaName_Cannot_Be_Parsed()
		{
			_givenSchemaName = "asdfakjhasefhuklerwtg";
			var form = Creator();

			Assert.AreEqual(FormSchemaName.Unknown, form.SchemaNameResolved);
		}
	}
}
