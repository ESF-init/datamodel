﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VarioSL.Entities.CollectionClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;
using VaroSL.Entities.TestFramework;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class CardEntityTests
	{

		#region ContractIDs

		[TestMethod]
		public void ContractIDs_WithoutContracts_ReturnsEmptyList()
		{
			var card = CreateExistingCard();

			Assert.AreEqual(0, card.ContractIDs.Count);
		}

		[TestMethod]
		public void ContractIDs_WithSingleContract_ReturnsListWithSingleContractID()
		{
			var card = CreateExistingCard();
			var cardToContract = CreateExistingCardToContract(card.CardID, 100000000);
			card.CardsToContracts.Add(cardToContract);

			Assert.AreEqual(1, card.ContractIDs.Count);
			Assert.AreEqual(cardToContract.ContractID, card.ContractIDs.Single());
		}

		[TestMethod]
		public void ContractIDs_WithMultipleContracts_ReturnAllAssignedContractIDs()
		{
			var card = CreateExistingCard();
			var cardToContract1 = CreateExistingCardToContract(card.CardID, 1000000);
			var cardToContract2 = CreateExistingCardToContract(card.CardID, 1000001);
			card.CardsToContracts.AddRange(new[] { cardToContract1, cardToContract2 });

			Assert.AreEqual(2, card.ContractIDs.Count);
			Assert.IsTrue(card.ContractIDs.Contains(cardToContract1.ContractID));
			Assert.IsTrue(card.ContractIDs.Contains(cardToContract2.ContractID));
		}

		#endregion

		#region ActiveProducts

		private IQueryable<ProductEntity> oldProductData;

		[TestInitialize]
		public void Initialize()
		{
			oldProductData = CardEntity.ProductData;
		}

		[TestCleanup]
		public void Cleanup()
		{
			CardEntity.ProductData = oldProductData;
		}

		private ProductEntity CreatePass(
			bool isLoaded = true, 
			bool isCanceled = false,
			bool isPaid = true,
			long cardID = 1,
			TicketType ticketType = TicketType.Pass,
			DateTime? validFrom = null,
			DateTime? validTo = null,
			bool validateWhenSelling = false
			)
		{
			return new ProductEntity
			{
				IsNew = false, AlreadyFetchedAutoloadSettings = true, AlreadyFetchedOrderDetails = true,
				IsLoaded = isLoaded,
				IsCanceled = isCanceled,
				IsPaid = isPaid,
				ValidFrom = validFrom ?? DateTime.MinValue,
				ValidTo = validTo ?? DateTime.MaxValue,
				TicketType = ticketType,
				CardID = cardID
			};
		}

		private CardEntity CreateCardWithProduct(ProductEntity p)
		{
			var ret = new CardEntity { IsNew = false, AlreadyFetchedProducts = true, CardID = 1 };
			CardEntity.ProductData = new[] {p}.AsQueryable();
			return ret;
		}

		[TestMethod]
		public void ActiveProducts_OnlyLoadedValidPassesReturned()
		{				
			var expected = new RowTest<CardEntity, ProductEntity, int>(product => product.ToString());
			expected.Add(CreatePass(), 1);
			expected.Add(CreatePass(isLoaded: false), 0, "A non-loaded product can't be active.");
			expected.Add(CreatePass(cardID: 2), 0, "A product for the wrong card can't be active.");
			expected.Add(CreatePass(ticketType: TicketType.Punch), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(ticketType: TicketType.CashSaver), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(ticketType: TicketType.DriverIssued), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(ticketType: TicketType.PhysicalCard), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(ticketType: TicketType.Refund), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(ticketType: TicketType.TopUp), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(ticketType: TicketType.ZeroValueTicket), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(ticketType: TicketType.NormalTicket), 1, "Normal tickets should be returned.");
			expected.Add(CreatePass(validFrom: DateTime.MaxValue), 0, "A future product can't be active.");
			expected.Add(CreatePass(validTo: DateTime.MinValue), 0, "An expired product can't be active.");
			expected.Assert(x => CreateCardWithProduct(x), x => x.ActiveProducts);
		}

		#endregion

		#region PendingProducts

		[TestMethod]
		public void PendingProducts_RowTest()
		{
			var expected = new RowTest<CardEntity, ProductEntity, int>(product => product.ToString());
			expected.Add(CreatePass(), 0, "A loaded product should not be pending.");
			expected.Add(CreatePass(isLoaded: false), 1, "A non loaded product should be pending.");
			expected.Add(CreatePass(isLoaded: false, cardID: 2), 0, "A product for the wrong card can't be pending.");
			expected.Add(CreatePass(isLoaded: false, ticketType: TicketType.Punch), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(isLoaded: false, ticketType: TicketType.CashSaver), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(isLoaded: false, ticketType: TicketType.DriverIssued), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(isLoaded: false, ticketType: TicketType.PhysicalCard), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(isLoaded: false, ticketType: TicketType.Refund), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(isLoaded: false, ticketType: TicketType.TopUp), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(isLoaded: false, ticketType: TicketType.ZeroValueTicket), 0, "Only passes and normal tickets should be returned.");
			expected.Add(CreatePass(isLoaded: false, ticketType: TicketType.NormalTicket), 1, "Normal tickets should be returned.");
			expected.Assert(x => CreateCardWithProduct(x), x => x.PendingProducts);
		}

		#endregion

		#region CardState

		[TestMethod]
		public void IsActive_WithAllStates_ReturnsExpectedStates()
		{
			var expected = new RowTest<CardEntity, CardState, bool>(state => state.ToString());
			expected.Add(CardState.Active, true);
			expected.Add(CardState.Inactive, false);
			expected.Add(CardState.Replaced, false);
			expected.Add(CardState.ReplacementOrdered, false);
			expected.Add(CardState.Blocked, false);
			expected.Add(CardState.Dormant, false);
			expected.Add(CardState.Expired, false);
			expected.Assert((x) => CreateCardWithState(x), x => x.IsActive);
		}

		[TestMethod]
		public void IsReplaceAllowed_WithAllStates_ReturnsExpectedStates()
		{

			var expected = new RowTest<CardEntity, CardState, bool>(state => state.ToString());
			expected.Add(CardState.Active, false);
			expected.Add(CardState.Inactive, false);
			expected.Add(CardState.Replaced, false);
			expected.Add(CardState.ReplacementOrdered, true);
			expected.Add(CardState.Blocked, true);
			expected.Add(CardState.Dormant, false);
			expected.Add(CardState.Expired, true);

			expected.Assert((x) => CreateCardWithState(x), x => x.IsReplaceAllowed);
		}

		[TestMethod]
		public void IsReactivationAllowed_WithAllStates_ReturnsExpectedStates()
		{

			var expected = new RowTest<CardEntity, CardState, bool>(state => state.ToString());
			expected.Add(CardState.Active, false);
			expected.Add(CardState.Inactive, false);
			expected.Add(CardState.Replaced, false);
			expected.Add(CardState.ReplacementOrdered, false);
			expected.Add(CardState.Blocked, false);
			expected.Add(CardState.Dormant, true);
			expected.Add(CardState.Expired, false);
			
			expected.Assert((x) => CreateCardWithState(x), x => x.IsReactivationAllowed);
		}

		[TestMethod]
		public void IsActive_CardStateEnumCount()
		{
			Assert.AreEqual(7, Enum.GetNames(typeof(CardState)).Length);
		}

		private static CardEntity CreateCardWithState(CardState state)
		{
			var ret = new CardEntity() { IsNew = false };
			ret.State = state;
			return ret;
		}

		#endregion

		#region IsLimitedUse

		[TestMethod]
		public void IsLimitedUse_BodyTypeEnumCount()
		{
			Assert.AreEqual(4, Enum.GetNames(typeof(CardBodyType)).Length);
		}

		[TestMethod]
		public void IsLimitedUse_WithNonLimitedBodyTypes_ReturnsFalse()
		{
			NonLimitedUseTypes().ForEach(type => Assert.IsFalse(CreateCardWithBodyType(type).IsLimitedUse));
		}

		[TestMethod]
		public void IsLimitedUse_WithLimitedBodyTypes_ReturnsTrue()
		{
			LimitedUseTypes().ForEach(type => Assert.IsTrue(CreateCardWithBodyType(type).IsLimitedUse));
		}

		private static List<CardBodyType> NonLimitedUseTypes()
		{
			return new List<CardBodyType>
			{
				CardBodyType.Adhesive,
				CardBodyType.None,
				CardBodyType.Plastic
			};
		}

		private static List<CardBodyType> LimitedUseTypes()
		{
			return new List<CardBodyType>
			{
				CardBodyType.Paper
			};
		}

		private static CardEntity CreateCardWithBodyType(CardBodyType type)
		{
			var ret = new CardEntity() { IsNew = false, CardPhysicalDetail = new CardPhysicalDetailEntity() };
			ret.CardPhysicalDetail.BodyType = type;
			return ret;
		}

		#endregion

		#region HasTopUpAutoload

		[TestMethod]
		public void HasTopUpAutoload_NoProducts_ReturnsFalse()
		{
			var data = CreateCardWithProducts(EmptyProducts());
			Assert.IsFalse(data.HasTopUpAutoload);
		}

		[TestMethod]
		public void HasTopUpAutoload_ProductsWithoutAutoloads_ReturnsFalse()
		{
			var data = CreateCardWithProducts(MixedProductsWithoutAutoloads());
			Assert.IsFalse(data.HasTopUpAutoload);
		}

		[TestMethod]
		public void HasTopUpAutoload_NonTopUpWithAutoloads_ReturnsFalse()
		{
			var data = CreateCardWithProducts(MixedProductsWithNonTopUpAutoloads());
			Assert.IsFalse(data.HasTopUpAutoload);
		}

		[TestMethod]
		public void HasTopUpAutoload_TopUpWithAutoload_ReturnsTrue()
		{
			var data = CreateCardWithProducts(MixedProductsWithTopUpAutoloads());
			Assert.IsTrue(data.HasTopUpAutoload);
		}

		private static CardEntity CreateCardWithProducts(List<ProductEntity> products)
		{
			var ret = new CardEntity() { IsNew = false };
			ret.AlreadyFetchedProducts = true;
			ret.Products.AddRange(products);
			return ret;
		}

		private static List<ProductEntity> EmptyProducts()
		{
			return CreateProducts(0, false, 0, false);
		}

		private static List<ProductEntity> MixedProductsWithoutAutoloads()
		{
			return CreateProducts(2, false, 2, false);
		}

		private static List<ProductEntity> MixedProductsWithNonTopUpAutoloads()
		{
			return CreateProducts(2, false, 2, true);
		}

		private static List<ProductEntity> MixedProductsWithTopUpAutoloads()
		{
			return CreateProducts(2, true, 2, true);
		}

		private static List<ProductEntity> CreateProducts(long topUpProducts, bool topUpAutoloads, long normalTickets, bool normalAutoloads)
		{
			var ret = new List<ProductEntity>();
			for (int i = 0; i < topUpProducts; i++)
			{
				ret.Add(new ProductEntity { TicketType = TicketType.TopUp, AlreadyFetchedAutoloadSettings = true });
				if (topUpAutoloads) ret.Last().AutoloadSettings.Add(new AutoloadSettingEntity());
			}

			for (int i = 0; i < normalTickets; i++)
			{
				ret.Add(new ProductEntity { TicketType = TicketType.NormalTicket, AlreadyFetchedAutoloadSettings = true });
				if (normalAutoloads) ret.Last().AutoloadSettings.Add(new AutoloadSettingEntity());
			}

			return ret;
		}

		#endregion

		#region IsAddTopUpAllowed

		[TestMethod]
		public void IsAddTopUpAllowed_WithActiveNonLimitedExistingCard_ReturnsTrue()
		{
			var data = IsAddTopUpAllowed_CreateMockCard(true, false, false);
			var result = data.Object.IsAddTopUpAllowed;

			data.Verify(card => card.IsActive, Times.AtLeastOnce());
			data.Verify(card => card.IsLimitedUse, Times.AtLeastOnce());
			data.Verify(card => card.IsNew, Times.AtLeastOnce());
			Assert.IsTrue(result);
		}

		[TestMethod]
		public void IsAddTopUpAllowed_WithInActiveCard_ReturnsFalse()
		{
			var data = IsAddTopUpAllowed_CreateMockCard(false, false, false);
			Assert.IsFalse(data.Object.IsAddTopUpAllowed);
		}

		[TestMethod]
		public void IsAddTopUpAllowed_WithLimitedUseCard_ReturnsFalse()
		{
			var data = IsAddTopUpAllowed_CreateMockCard(true, true, false);
			Assert.IsFalse(data.Object.IsAddTopUpAllowed);
		}

		[TestMethod]
		public void IsAddTopUpAllowed_WithNewCard_ReturnsFalse()
		{
			var data = IsAddTopUpAllowed_CreateMockCard(true, false, true);
			Assert.IsFalse(data.Object.IsAddTopUpAllowed);
		}

		private static Mock<CardEntity> IsAddTopUpAllowed_CreateMockCard(bool isActive, bool isLimitedUse, bool isNew)
		{
			var mock = new Mock<CardEntity>() { CallBase = true };
			mock.Setup(card => card.IsActive).Returns(isActive);
			mock.Setup(card => card.IsNew).Returns(isNew);
			mock.Setup(card => card.IsLimitedUse).Returns(isLimitedUse);
			return mock;
		}

		#endregion

		#region IsAddAutoloadAllowed

		[TestMethod]
		public void IsAddAutoloadAllowed_WithAllowedCard_ReturnsTrue()
		{
			var data = IsAddAutoloadAllowed_CreateMockCard(true, false);
			var result = data.Object.IsAddAutoloadAllowed;

			data.Verify(card => card.IsAddTopUpAllowed, Times.AtLeastOnce());
			data.Verify(card => card.HasTopUpAutoload, Times.AtLeastOnce());
			Assert.IsTrue(result);
		}

		[TestMethod]
		public void IsAddTopUpAllowed_WithForbiddenTopUps_ReturnsFalse()
		{
			var data = IsAddAutoloadAllowed_CreateMockCard(false, false);
			Assert.IsFalse(data.Object.IsAddAutoloadAllowed);
		}

		[TestMethod]
		public void IsAddTopUpAllowed_WithTopUpAutoload_ReturnsFalse()
		{
			var data = IsAddAutoloadAllowed_CreateMockCard(true, true);
			Assert.IsFalse(data.Object.IsAddAutoloadAllowed);
		}

		private static Mock<CardEntity> IsAddAutoloadAllowed_CreateMockCard(bool IsAddTopUpAllowed, bool hasTopUpAutoload)
		{
			var mock = new Mock<CardEntity>() { CallBase = true };
			mock.Setup(card => card.IsAddTopUpAllowed).Returns(IsAddTopUpAllowed);
			mock.Setup(card => card.HasTopUpAutoload).Returns(hasTopUpAutoload);
			return mock;
		}

		#endregion

		#region IsAddCardHolderAllowed

		[TestMethod]
		public void IsAddCardHolderAllowed_WithActiveAndUnassignedCard_ReturnsTrue()
		{
			CardEntity card = CreateExistingCard();
			
			var result = card.IsAddCardHolderAllowed;
			
			Assert.IsTrue(result);
		}

		[TestMethod]
		public void IsAddCardHolderAllowed_WithInactiveAndUnassignedCard_ReturnsFales()
		{
			CardEntity card = CreateExistingCard();
			card.State = CardState.Inactive;

			var result = card.IsAddCardHolderAllowed;

			Assert.IsFalse(result);
		}

		[TestMethod]
		public void IsAddCardHolderAllowed_WithActiveAndAssignedCard_ReturnsFales()
		{
			CardEntity card = CreateExistingCard();
			card.CardHolder = new CardHolderEntity();

			var result = card.IsAddCardHolderAllowed;

			Assert.IsFalse(result);
		}

		[TestMethod]
		public void IsAddCardHolderAllowed_WithInactiveAndAssignedCard_ReturnsFales()
		{
			CardEntity card = CreateExistingCard();
			card.State = CardState.Inactive; 
			card.CardHolder = new CardHolderEntity();
			
			var result = card.IsAddCardHolderAllowed;

			Assert.IsFalse(result);
		}

		#endregion

		#region AddContract

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddContract_WithContractIsNull_ThrowsException()
		{
			CardEntity card = CreateExistingCard();

			card.AddContract(null);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddContract_WithContractIsNew_ThrowsException()
		{
			CardEntity card = CreateExistingCard();
			ContractEntity contract = CreateExistingContractWithNumber(contractNumber: "OK_ContractNumber");
			contract.IsNew = true;

			card.AddContract(contract);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddContract_WithCardIsNew_ThrowsException()
		{
			CardEntity card = CreateExistingCard();
			card.IsNew = true;
			ContractEntity contract = CreateExistingContractWithNumber(contractNumber: "OK_ContractNumber");

			card.AddContract(contract);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddContract_WithSameContract_ThrowsException()
		{
			CardEntity card = CreateExistingCard();
			ContractEntity contract = CreateExistingContractWithNumber(contractNumber: "OK_ContractNumber");

			card.Contracts.Add(contract);

			card.AddContract(contract);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddContract_WithInactiveCard_ThrowsException()
		{
			CardEntity card = CreateExistingCard();
			card.State = CardState.Inactive;

			ContractEntity contract = CreateExistingContractWithNumber(contractNumber: "OK_ContractNumber");

			card.AddContract(contract);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddContract_WithNonIssuedCard_ThrowsException()
		{
			CardEntity card = CreateExistingCard();
			card.InventoryState = InventoryState.InInventory;

			ContractEntity contract = CreateExistingContractWithNumber(contractNumber: "OK_ContractNumber");

			card.AddContract(contract);
		}

		[TestMethod]
		public void AddContract_WithNotSharedNoContracts_NoException()
		{
			CardEntity card = CreateExistingCard();
			card.CardPhysicalDetail = new CardPhysicalDetailEntity() { BodyType = CardBodyType.Plastic };
			ContractEntity contract = CreateExistingContractWithNumber(contractNumber: "OK_ContractNumber");

			card.AddContract(contract);

			var comp = new CardToContractCollection() 
				 { 
					 new CardToContractEntity
						{
							CardID = card.CardID, 
							ContractID = contract.ContractID
						}
				 };
			Assert.IsTrue(card.CardsToContracts.SequenceEqual(comp, new CardToContractEntityComparer()));
		}

		[TestMethod]
		public void AddContract_WithSharedNoContracts_NoException()
		{
			var card = CreateTestCase(true, 0);
			Assert.AreEqual(1, card.CardsToContracts.Count);
		}

		[TestMethod]
		public void AddContract_WithSharedOneContract_NoException()
		{
			var card = CreateTestCase(true, 1);
			Assert.AreEqual(2, card.CardsToContracts.Count);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddContract_WithNotSharedOneContract_ThrowException()
		{
			CreateTestCase(false, 1);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddContract_WithNotSharedTwoContracts_ThrowException()
		{
			CreateTestCase(false, 2);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddContract_WithSharedTwoContracts_ThrowException()
		{
			CreateTestCase(true, 2);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddContract_WithNotSharedManyContracts_ThrowException()
		{
			CreateTestCase(false, 10);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddContract_WithSharedManyContracts_ThrowException()
		{
			CreateTestCase(true, 10);
		}


		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddContract_WithPaperCard_ThrowException()
		{
			CreateTestCase(true, 0,CardBodyType.Paper);
		}

		private CardEntity CreateTestCase(bool isShared, int numberOfContracts,CardBodyType type=CardBodyType.Plastic)
		{
			CardEntity card = CreateExistingCard(type);
			card.IsShared = isShared;
			for (int i = 0; i < numberOfContracts; i++)
			{
				var contract = CreateExistingContractWithNumber(contractNumber: "OK_ContractNumber");
				contract.ContractID = i + 1;
				card.Contracts.Add(contract);
				card.CardsToContracts.Add(new CardToContractEntity() { CardID = card.CardID, ContractID = contract.ContractID });
			}

			ContractEntity newContract = CreateExistingContractWithNumber(contractNumber: "OK_ContractNumber2");
			card.AddContract(newContract);
			return card;
		}

		[TestMethod]
		public void AddContract_WithRegisterableCard_AssertCardStateIsActive()
		{
			CardEntity card = CreateExistingCard();
			ContractEntity contract = CreateExistingContractWithNumber(contractNumber: "OK_ContractNumber");

			card.AddContract(contract);

			Assert.IsTrue(card.State == CardState.Active);
		}

		[TestMethod]
		public void AddContract_WithRegisterableCard_AssertCardEventWasAdded()
		{
			CardEntity card = CreateExistingCard();

			ContractEntity contract = CreateExistingContractWithNumber(contractNumber: "OK_ContractNumber");

			var stub = new Mock<ICardEventGenerator>();
			var ret = new CardEventEntity();
			stub.Setup(e => e.CreateEventAssign(It.IsAny<CardEntity>(), It.IsAny<ContractEntity>())).Returns(ret);

			CardEventEntity.EventGenerator = stub.Object;
			card.AddContract(contract);
			CardEventEntity.EventGenerator = new CardEventEntity();

			Assert.AreEqual(ret, card.CardEvents.Single());
		}

		[TestMethod]
		public void AddContract_WithRegisterableCard_AssertContractHistoryEventWasAdded()
		{
			CardEntity card = CreateExistingCard();
			ContractEntity contract = CreateExistingContractWithNumber(contractNumber: "OK_ContractNumber");
			contract.ContractID = 10;
			contract.AlreadyFetchedContractHistoryItems = true;

			card.AddContract(contract);

			ContractHistoryEntity historyEvent = new ContractHistoryEntity()
			{
				Text = "Registered smartcard () to contract",
				ContractID = 10
			};

			Assert.IsTrue(contract.ContractHistoryItems.SequenceEqual(new ContractHistoryCollection() { historyEvent },
				new ContractHistoryEntityComparer()));
		}

		#endregion

		#region Issue Card

		[TestMethod]
		public void Issue_WithUnissuedCard_StateIsActive()
		{
			CardEntity card = CreateExistingCard();
			card.State = CardState.Inactive;
			card.InventoryState = InventoryState.InInventory;

			card.Issue(DateTime.MinValue);

			Assert.IsTrue(card.IsActive);
		}

		public void Issue_WithDeliveredCard_StateIsActive()
		{
			CardEntity card = CreateExistingCard();
			card.State = CardState.Inactive;
			card.InventoryState = InventoryState.Delivered;

			card.Issue(DateTime.MinValue);

			Assert.AreEqual(InventoryState.Issued, card.InventoryState);
		}

		[TestMethod]
		public void Issue_WithUnissuedCard_InventoryStateIsIssued()
		{
			CardEntity card = CreateExistingCard();
			card.State = CardState.Inactive;
			card.InventoryState = InventoryState.InInventory;

			card.Issue(DateTime.MinValue);

			Assert.IsTrue(card.InventoryState == InventoryState.Issued);
		}

		[TestMethod]
		public void Issue_WithUnissuedCard_AssertCardEventWasAdded()
		{
			CardEntity card = CreateExistingCard();
			card.State = CardState.Inactive;
			card.InventoryState = InventoryState.InInventory;

			var stub = new Mock<ICardEventGenerator>();
			var ret = new CardEventEntity();
			stub.Setup(e => e.CreateEventIssue(It.IsAny<CardEntity>())).Returns(ret);

			CardEventEntity.EventGenerator = stub.Object;
			card.Issue(DateTime.MinValue);
			CardEventEntity.EventGenerator = new CardEventEntity();

			Assert.AreEqual(ret, card.CardEvents.Single());
		}

		[TestMethod]
		public void Issue_WithUnissuedCard_ExpiryDateIsSet()
		{
			CardEntity card = CreateExistingCard();
			card.State = CardState.Inactive;
			card.InventoryState = InventoryState.InInventory;

			DateTime VALID_EXPIRYDATE = new DateTime(2050, 1, 1);

			card.Issue(VALID_EXPIRYDATE);

			Assert.AreEqual(VALID_EXPIRYDATE, card.Expiration);
		}


		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void Issue_WithStateOtherThanInactive_ThrowException()
		{
			CardEntity card = CreateExistingCard();
			card.State = CardState.Active;
			card.InventoryState = InventoryState.InInventory;

			card.Issue(DateTime.MinValue);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void Issue_WithInventoryStateThanInInventory_ThrowException()
		{
			CardEntity card = CreateExistingCard();
			card.State = CardState.Inactive;
			card.InventoryState = InventoryState.Issued;

			card.Issue(DateTime.MinValue);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void Issue_CardIsNew_ThrowException()
		{
			CardEntity card = CreateExistingCard();
			card.IsNew = true;
			card.State = CardState.Inactive;
			card.InventoryState = InventoryState.InInventory;

			card.Issue(DateTime.MinValue);
		}

		#endregion
		
		#region Revoke Card

		[TestMethod]
		public void Revoke_WithIssuedCard_InventoryStateChanged()
		{
			CardEntity card = CreateExistingCard();
			card.Revoke();

			Assert.AreEqual(InventoryState.Revoked, card.InventoryState);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void Revoke_WithNewCard_ThrowsException()
		{
			CardEntity card = CreateExistingCard();
			card.IsNew = true;
			card.Revoke();
		}

		[TestMethod]
		public void Revoke_WithIssuedCard_RevokeEventWasAdded()
		{
			CardEntity card = CreateExistingCard();

			var stub = new Mock<ICardEventGenerator>();
			var ret = new CardEventEntity();
			stub.Setup(e => e.CreateEventRevoke(It.IsAny<CardEntity>())).Returns(ret);

			CardEventEntity.EventGenerator = stub.Object;
			card.Revoke();
			CardEventEntity.EventGenerator = new CardEventEntity();

			Assert.AreEqual(ret, card.CardEvents.Single());
		}

		#endregion

		#region ToSmartcardDto

		

		#endregion

		#region GetCurrentProducts

		[TestMethod]
		public void GetCurrentProducts_WithActivePass_ReturnsProduct()
		{
			var card = CreateCardWithCurrentProduct();

			var result = card.GetCurrentProducts();

			Assert.AreEqual(card.Products.Single(), result.Single());
		}

		[TestMethod]
		public void GetCurrentProducts_WithActiveNormalTicket_ReturnsProduct()
		{
			var card = CreateCardWithCurrentProduct();
			card.Products.Single().TicketType = TicketType.NormalTicket;

			var result = card.GetCurrentProducts();

			Assert.AreEqual(card.Products.Single(), result.Single());
		}

		[TestMethod]
		public void GetCurrentProducts_WithOldDatesActivatedProduct_ReturnsNoProducts()
		{
			var card = CreateCardWithCurrentProduct();
			card.Products.Single().ValidTo = DateTime.MinValue;
			card.Products.Single().ValidateWhenSelling = true;

			var result = card.GetCurrentProducts();

			Assert.AreEqual(0, result.Count());
		}

		[TestMethod]
		public void GetCurrentProducts_WithPendingUnactivatedProduct_ReturnsProduct()
		{
			var card = CreateCardWithCurrentProduct();
			card.Products.Single().ValidFrom = DateTime.MinValue;
			card.Products.Single().ValidTo = DateTime.MinValue;
			card.Products.Single().ValidateWhenSelling = false;

			var result = card.GetCurrentProducts();

			Assert.AreEqual(card.Products.Single(), result.Single());
		}

		[TestMethod]
		public void GetCurrentProducts_WithCanceledProduct_ReturnsNoProducts()
		{
			var card = CreateCardWithCurrentProduct();
			card.Products.Single().IsCanceled = true;

			var result = card.GetCurrentProducts();

			Assert.AreEqual(0, result.Count());
		}

		[TestMethod]
		public void GetCurrentProducts_WithTopUpProduct_ReturnsNoProducts()
		{
			var card = CreateCardWithCurrentProduct();
			card.Products.Single().TicketType = TicketType.TopUp;

			var result = card.GetCurrentProducts();

			Assert.AreEqual(0, result.Count());
		}

		[TestMethod]
		public void GetCurrentProducts_WithUnpaidProduct_ReturnsZero()
		{
			var card = CreateCardWithCurrentProduct();
			card.Products.Single().IsPaid = false;

			var result = card.GetCurrentProducts();

			Assert.AreEqual(0, result.Count());
		}

		[TestMethod]
		public void GetCurrentProducts_WithOnlyDepositProduct_ReturnsNoProducts()
		{
			var products = new List<ProductEntity> { CreateDepositProduct() };
			var card = CreateCardWithProducts(products);

			var currentProducts = card.GetCurrentProducts();
			Assert.AreEqual(0, currentProducts.Count());
		}

		[TestMethod]
		public void GetCurrentProducts_MixedProductsWithDepositProduct_ReturnsProductsWithoutDeposi()
		{
			var products = CreateMultiplePasses();
			var productsWithoutDepositCount = products.Count;
			
			products.Add(CreateDepositProduct());
			var card = CreateCardWithProducts(products);

			var currentProducts = card.GetCurrentProducts();
			Assert.AreEqual(productsWithoutDepositCount, products.Count - 1);
			Assert.AreEqual(productsWithoutDepositCount, currentProducts.Count());			
		}

		private static List<ProductEntity> CreateMultiplePasses(int num = 3)
		{
			var list = new List<ProductEntity>();
			for (int i = 0; i < num; ++i)
			{
				list.Add(CreateProduct(TicketType.Pass));
			}
			return list;
		}

		#endregion

		#region OrganizationID

		[TestMethod]
		public void OrganizationID_WithoutContract_ReturnsNull()
		{
			var card = CreateExistingCard();

			Assert.AreEqual(null, card.OrganizationID);
		}

		[TestMethod]
		public void OrganizationID_ContractWithoutOrganization_ReturnsNull()
		{
			var card = CreateExistingCard();
			var contract = CreateExistingContractWithNumber("OK_NUMBER");
			card.Contracts.Add(contract);

			Assert.AreEqual(null, card.OrganizationID);
		}

		[TestMethod]
		public void OrganizationID_ContractWithOrganization_ReturnsOrganizationID()
		{
			var card = CreateExistingCard();
			var contract = CreateExistingContractWithNumber("OK_NUMBER");
			contract.OrganizationID = 1;
			card.Contracts.Add(contract);

			Assert.AreEqual(1, card.OrganizationID);
		}

		[TestMethod]
		public void OrganizationID_TwoContractWithOneOrganization_ReturnsOrganizationID()
		{
			var card = CreateExistingCard();
			var contract1 = CreateExistingContractWithNumber("OK_NUMBER1");
			card.Contracts.Add(contract1);
			var contract2 = CreateExistingContractWithNumber("OK_NUMBER2");
			contract2.OrganizationID = 2;
			card.Contracts.Add(contract2);

			Assert.AreEqual(2, card.OrganizationID);
		}

		#endregion

		#region CardTicketID

		[TestMethod]
		public void CardTicketID_WithoutProducts_ReturnsZero()
		{
			var card = CreateExistingCard();
			Assert.AreEqual(0, card.CardTicketID);
		}

		[TestMethod]
		public void CardTicketID_WithNoCardProduct_ReturnsZero()
		{
			var product = CreateProduct(TicketType.Pass);
			var card = CreateExistingCard();
			card.Products.Add(product);
			Assert.AreEqual(0, card.CardTicketID);
		}

		[TestMethod]
		public void CardTicketID_WithOneCardProduct_ReturnsTicketID()
		{
			const long ValidTicketID = 1;
			var product = CreateProduct(TicketType.PhysicalCard, ValidTicketID);
			var card = CreateExistingCard();
			card.Products.Add(product);
			Assert.AreEqual(ValidTicketID, card.CardTicketID);
		}

		[TestMethod]
		public void CardTicketID_WithTwoCardProducts_ReturnsTicketIDOfFirstCardProduct()
		{
			const long ValidTicketIDProduct1 = 1;
			const long ValidTicketIDProduct2 = 2;
			var product1 = CreateProduct(TicketType.PhysicalCard, ValidTicketIDProduct1);
			var product2 = CreateProduct(TicketType.PhysicalCard, ValidTicketIDProduct2);
			var card = CreateExistingCard();
			card.Products.Add(product1);
			card.Products.Add(product2);
			Assert.AreEqual(ValidTicketIDProduct1, card.CardTicketID);
		}

		#endregion

		#region GetCurrentPassesCount

		[TestMethod]
		public void GetCurrentPassesCount_WithValidProduct_ReturnsOne()
		{
			var card = CreateCardWithCurrentProduct();

			var result = card.GetCurrentPassesCount();

			Assert.AreEqual(1, result);
		}

		[TestMethod]
		public void GetCurrentPassesCount_NoProducts_ReturnsZero()
		{
			var card = CreateExistingCard();

			var result = card.GetCurrentPassesCount();

			Assert.AreEqual(0, result);
		}

		[TestMethod]
		public void GetCurrentPassesCount_WithOldDatesActivatedProduct_ReturnsZero()
		{
			var card = CreateCardWithCurrentProduct();
			card.Products.Single().ValidTo = DateTime.MinValue;
			card.Products.Single().ValidateWhenSelling = true;

			var result = card.GetCurrentPassesCount();

			Assert.AreEqual(0, result);
		}

		[TestMethod]
		public void GetCurrentPassesCount_WithPendingUnactivatedProduct_ReturnsOne()
		{
			var card = CreateCardWithCurrentProduct();
			card.Products.Single().ValidFrom = DateTime.MinValue;
			card.Products.Single().ValidTo = DateTime.MinValue;
			card.Products.Single().ValidateWhenSelling = false;

			var result = card.GetCurrentPassesCount();

			Assert.AreEqual(1, result);
		}

		[TestMethod]
		public void GetCurrentPassesCount_WithCanceledProduct_ReturnsZero()
		{
			var card = CreateCardWithCurrentProduct();
			card.Products.Single().IsCanceled = true;

			var result = card.GetCurrentPassesCount();

			Assert.AreEqual(0, result);
		}

		[TestMethod]
		public void GetCurrentPassesCount_WithTopUpProduct_ReturnsZero()
		{
			var card = CreateCardWithCurrentProduct();
			card.Products.Single().TicketType = TicketType.TopUp;

			var result = card.GetCurrentPassesCount();

			Assert.AreEqual(0, result);
		}

		[TestMethod]
		public void GetCurrentPassesCount_WithUnpaidProduct_ReturnsZero()
		{
			var card = CreateCardWithCurrentProduct();
			card.Products.Single().IsPaid = false;

			var result = card.GetCurrentPassesCount();

			Assert.AreEqual(0, result);
		}

		#endregion

		#region AddCardTicket

		[TestMethod]
		public void AddCardTicket_WithoutCardProduct_SetsCardTicket()
		{
			var card = CreateExistingCard();

			const long VALID_TicketID = 1;

			ProductEntity product = new ProductEntity()
				{
					TicketType = TicketType.PhysicalCard,
					TicketID = VALID_TicketID
				};

			card.AddCardTicket(product);

			var cardTicket = card.Products.Where(p =>
				p.TicketType == TicketType.PhysicalCard
				&& p.ValidFrom == DateTime.MinValue).First();

			Assert.AreEqual(VALID_TicketID, cardTicket.TicketID);
		}

		[TestMethod]
		public void AddCardTicket_WithoutCardProduct_ReturnsTrue()
		{
			var card = CreateExistingCard();

			const long VALID_TicketID = 1;

			ProductEntity product = new ProductEntity()
			{
				TicketType = TicketType.PhysicalCard,
				TicketID = VALID_TicketID
			};

			Assert.IsTrue(card.AddCardTicket(product));
		}

		[TestMethod]
		public void AddCardTicket_WithoutCardProduct_SetsValidFromToMinValue()
		{
			var card = CreateExistingCard();

			ProductEntity product = new ProductEntity()
				{
					TicketType = TicketType.PhysicalCard,
					ValidFrom = DateTime.Now,
					TicketID = 1
				};

			card.AddCardTicket(product);

			var cardTicket = card.Products.Where(p =>
				p.TicketType == TicketType.PhysicalCard
				&& p.ValidFrom == DateTime.MinValue).First();

			Assert.AreEqual(DateTime.MinValue, cardTicket.ValidFrom);
		}

		[TestMethod]
		public void AddCardTicket_WithoutCardProduct_SetsValidToToMaxValue()
		{
			var card = CreateExistingCard();

			ProductEntity product = new ProductEntity()
				{
					TicketType = TicketType.PhysicalCard,
					ValidTo = DateTime.Now,
					TicketID = 1
				};

			card.AddCardTicket(product);

			var cardTicket = card.Products.Where(p =>
				p.TicketType == TicketType.PhysicalCard
				&& p.ValidFrom == DateTime.MinValue).First();

			Assert.AreEqual(DateTime.MaxValue, cardTicket.ValidTo);
		}

		[TestMethod]
		public void AddCardTicket_WithoutCardProduct_SetsIsPaidTrue()
		{
			var card = CreateExistingCard();

			ProductEntity product = new ProductEntity()
				{
					TicketType = TicketType.PhysicalCard
				};

			card.AddCardTicket(product);

			var cardTicket = card.Products.Where(p =>
				p.TicketType == TicketType.PhysicalCard
				&& p.ValidFrom == DateTime.MinValue).First();

			Assert.IsTrue(cardTicket.IsPaid);
		}

		[TestMethod]
		public void AddCardTicket_WithoutCardProduct_SetsIsLoadTrue()
		{
			var card = CreateExistingCard();

			ProductEntity product = new ProductEntity()
				{
					TicketType = TicketType.PhysicalCard
				};

			card.AddCardTicket(product);

			var cardTicket = card.Products.Where(p =>
				p.TicketType == TicketType.PhysicalCard
				&& p.ValidFrom == DateTime.MinValue).First();

			Assert.IsTrue(cardTicket.IsLoaded);
		}

		[TestMethod]
		public void AddCardTicket_WithCardProduct_SetsNewCardTicket()
		{
			var card = CreateExistingCard();

			const long VALID_TicketID1 = 1;
			const long VALID_TicketID2 = 2;

			ProductEntity product1 = new ProductEntity()
				{
					TicketType = TicketType.PhysicalCard,
					TicketID = VALID_TicketID1
				};

			ProductEntity product2 = new ProductEntity()
				{
					TicketType = TicketType.PhysicalCard,
					TicketID = VALID_TicketID2
				};

			card.AddCardTicket(product1);
			card.AddCardTicket(product2);

			var cardTickets = card.Products.Where(p =>
				p.TicketType == TicketType.PhysicalCard
				&& p.ValidFrom == DateTime.MinValue);

			Assert.AreEqual(VALID_TicketID2, cardTickets.First().TicketID);
			Assert.AreEqual(1, cardTickets.Count());
		}

		[TestMethod]
		public void AddCardTicket_WithCardProduct_ReturnsFalse()
		{
			var card = CreateExistingCard();

			const long VALID_TicketID1 = 1;
			const long VALID_TicketID2 = 2;

			ProductEntity product1 = new ProductEntity()
				{
					TicketType = TicketType.PhysicalCard,
					TicketID = VALID_TicketID1
				};

			ProductEntity product2 = new ProductEntity()
				{
					TicketType = TicketType.PhysicalCard,
					TicketID = VALID_TicketID2
				};

			card.AddCardTicket(product1);

			Assert.IsFalse(card.AddCardTicket(product2));
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddCardTicket_WithWrongTicketType_ThrowsException()
		{
			var card = CreateExistingCard();

			const long VALID_TicketID = 1;

			ProductEntity product = new ProductEntity()
				{
					TicketType = TicketType.Pass,
					ValidFrom = DateTime.MinValue,
					TicketID = VALID_TicketID
				};

			card.AddCardTicket(product);
		}

		#endregion

		#region HasDepositProduct

		[TestMethod]
		public void HasDepositProduct_NoProducts_ReturnsFalse()
		{
			var card = CreateCardWithProducts(EmptyProducts());
			Assert.IsFalse(card.HasDepositProduct);
		}

		[TestMethod]
		public void HasDepositProduct_ProductsWithoutDeposit_ReturnsFalse()
		{
			var card = CreateCardWithProducts(MixedProductsWithoutDeposit());
			Assert.IsFalse(card.HasDepositProduct);
		}

		[TestMethod]
		public void HasDepositProduct_SingleDepositProduct_ReturnsTrue()
		{
			var card = CreateCardWithProducts(new List<ProductEntity> { CreateDepositProduct() });
			Assert.IsTrue(card.HasDepositProduct);
		}

		[TestMethod]
		public void HasDepositProduct_ProductsWithDepositProduct_ReturnsTrue()
		{
			var card = CreateCardWithProducts(MixedProductsWithDeposit());
			Assert.IsTrue(card.HasDepositProduct);
		}

		private static List<ProductEntity> MixedProductsWithoutDeposit()
		{
			return CreateProducts(2, false, 2, false);
		}

		private static List<ProductEntity> MixedProductsWithDeposit()
		{
			var products = MixedProductsWithoutDeposit();
			products.Add(CreateDepositProduct());
			return products;
		}

		private static ProductEntity CreateDepositProduct()
		{
			return new ProductEntity 
			{ 
				TicketType = TicketType.Deposit, 
				ValidFrom = DateTime.MinValue,
				ValidTo = DateTime.MaxValue,
			};
		}

		#endregion

		#region GetCurrentDepositProducts

		[TestMethod]
		public void GetCurrentDepositProducts_NoProducts_ReturnsNoProducts()
		{
			var card = CreateCardWithProducts(EmptyProducts());
			Assert.AreEqual(null, card.DepositProduct);
		}

		[TestMethod]
		public void GetCurrentDepositProducts_MixedProductsWithoutDeposit_ReturnsNoProducts()
		{
			var card = CreateCardWithProducts(MixedProductsWithoutDeposit());
			Assert.AreEqual(null, card.DepositProduct);
		}

		[TestMethod]
		public void GetCurrentDepositProducts_MixedProductsWithDepositProduct_ReturnsDepositProduct()
		{
			var depositProduct = CreateDepositProduct();
			var products = CreateMultiplePasses();
			products.Add(depositProduct);

			var card = CreateCardWithProducts(products);

			Assert.IsTrue(products.Count > 1, "Precondition: Not only the deposit product is in the product list");
			Assert.AreEqual(depositProduct, card.DepositProduct);
		} 

		#endregion

		#region Helper

		private static CardEntity CreateExistingCard(CardBodyType type=CardBodyType.Plastic)
		{
			return new CardEntity()
			{
				IsNew = false,
				State = CardState.Active,
				InventoryState = VarioSL.Entities.Enumerations.InventoryState.Issued,
				AlreadyFetchedContracts = true,
				AlreadyFetchedCardsToContracts = true,
				AlreadyFetchedCardEvents = true,
				AlreadyFetchedProducts = true,
				CardPhysicalDetail = new CardPhysicalDetailEntity() { BodyType=type}
			};
		}

		private static ProductEntity CreateProduct(TicketType ticketType, long ticketID = 0)
		{
			return new ProductEntity
			{
				TicketType = ticketType,
				TicketID = ticketID,
				ValidFrom = DateTime.MinValue,
				ValidTo = DateTime.MaxValue,
				IsCanceled = false,
				IsPaid = true,
				IsNew = false
			};
		}

		private static CardEntity CreateCardWithCurrentProduct()
		{
			var product = CreateProduct(TicketType.Pass);
			var card = CreateExistingCard();
			card.Products.Add(product);
			return card;
		}

		private static ContractEntity CreateExistingContractWithNumber(string contractNumber)
		{
			return new ContractEntity()
			{
				ContractNumber = contractNumber,
				IsNew = false,
				AlreadyFetchedContractHistoryItems = true
			};
		}

		private static CardToContractEntity CreateExistingCardToContract(long cardId, long contractId)
		{
			return new CardToContractEntity() 
			{
				CardID = cardId,
				ContractID = contractId,
				IsNew = false,
				AlreadyFetchedCard = true,
				AlreadyFetchedContract = true,
			};
		}

		#endregion
	}

	#region Comparer

	internal class CardEventEntityComparer : IEqualityComparer<CardEventEntity>
	{
		public bool Equals(CardEventEntity x, CardEventEntity y)
		{
			if (object.ReferenceEquals(x, y)) return true;

			if (object.ReferenceEquals(x, null) || object.ReferenceEquals(y, null)) return false;

			return x.CardEventType == y.CardEventType && x.Description == y.Description;
		}

		public int GetHashCode(CardEventEntity obj)
		{
			if (object.ReferenceEquals(obj, null)) return 0;

			int hashCodeDescription = obj.Description == null ? 0 : obj.Description.GetHashCode();
			int hasCodeCardEventType = obj.CardEventType.GetHashCode();

			return hashCodeDescription ^ hasCodeCardEventType;
		}
	}

	internal class CardToContractEntityComparer : IEqualityComparer<CardToContractEntity>
	{
		public bool Equals(CardToContractEntity x, CardToContractEntity y)
		{
			if (object.ReferenceEquals(x, y)) return true;

			if (object.ReferenceEquals(x, null) || object.ReferenceEquals(y, null)) return false;

			return x.ContractID == y.ContractID && x.CardID == y.CardID;
		}

		public int GetHashCode(CardToContractEntity obj)
		{
			if (object.ReferenceEquals(obj, null)) return 0;

			int hashCodeContractID = obj.ContractID.GetHashCode();
			int hasCodeCardID = obj.CardID.GetHashCode();

			return hashCodeContractID ^ hasCodeCardID;
		}
	}

	internal class ContractHistoryEntityComparer : IEqualityComparer<ContractHistoryEntity>
	{
		public bool Equals(ContractHistoryEntity x, ContractHistoryEntity y)
		{
			if (object.ReferenceEquals(x, y)) return true;

			if (object.ReferenceEquals(x, null) || object.ReferenceEquals(y, null)) return false;
			var ret = x.ContractID == y.ContractID && x.Text == y.Text;
			return ret;
		}

		public int GetHashCode(ContractHistoryEntity obj)
		{
			if (object.ReferenceEquals(obj, null)) return 0;

			int hashCodeContractID = obj.ContractID.GetHashCode();
			int hasCodeCardID = obj.Text.GetHashCode();

			return hashCodeContractID ^ hasCodeCardID;
		}
	}

	#endregion
}
