﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class DeviceClassEntityTests
	{
		private static readonly List<Tuple<long, string>> pairs = new List<Tuple<long, string>>()
		{
			new Tuple<long, string>(160, "G0"),
			new Tuple<long, string>(120, "C0"),
			new Tuple<long, string>(135, "D5"),
			new Tuple<long, string>(56, "56"),
			new Tuple<long, string>(9, "09"),
		};

		[TestMethod]
		public void InitEncoding_WithTuple_ExpectedResult()
		{
			pairs.ForEach(p => Assert.AreEqual(p.Item2, new DeviceClassEntity() { DeviceClassID = p.Item1 }.InitEncoding));
		}

		[TestMethod, ExpectedException(typeof(ArgumentException))]
		public void InitEncoding_WithWrongLength_ThrowsException()
		{
			var result = new DeviceClassEntity() { DeviceClassID = 55555 }.InitEncoding;
		}


		[TestMethod]
		public void FromInitBase_WithTuple_ExpectedResult()
		{
			pairs.ForEach(p => Assert.AreEqual(p.Item1, DeviceClassEntity.FromInitBase(p.Item2)));
		}

		[TestMethod, ExpectedException(typeof(ArgumentException))]
		public void FromInitBase_WithWrongLength_ThrowsException()
		{
			DeviceClassEntity.FromInitBase("G00");
		}
	}
}
