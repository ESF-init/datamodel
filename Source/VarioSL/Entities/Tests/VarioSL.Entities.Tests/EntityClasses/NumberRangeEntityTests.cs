﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;
using VaroSL.Entities.TestFramework;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class NumberRangeEntityTests
	{
		#region Initialize

		[TestMethod]
		public void InitializeDeviceCounter_WithValidNames_IsEqualToDefault()
		{
			const string deviceName = "OK_DEVICENAME";
			const string type = "OK_TYPE";

			var target = new NumberRangeEntity();
			target.InitializeDeviceCounter(deviceName, type);

			ObjectAssert.AreEntityFieldsEqual(target, CreateDefaultDeviceCounter(deviceName, type));
		}

		[TestMethod, ExpectedException(typeof(ArgumentException))]
		public void InitializeDeviceCounter_InvalidDeviceName_ThrowsException()
		{
			const string deviceName = null;
			const string type = "OK_TYPE";

			var target = new NumberRangeEntity();
			target.InitializeDeviceCounter(deviceName, type);
		}

		[TestMethod, ExpectedException(typeof(ArgumentException))]
		public void InitializeDeviceCounter_InvalidType_ThrowsException()
		{
			const string deviceName = "OK_DEVICENAME";
			const string type = null;

			var target = new NumberRangeEntity();
			target.InitializeDeviceCounter(deviceName, type);
		}
		
		#endregion

		#region GetNextValue

		[TestMethod]
		public void GetNextValue_DefaultDeviceCounter_ReturnsNextValue()
		{
			const string deviceName = "OK_DEVICENAME";
			const string type = "OK_TYPE";

			var target = CreateDefaultDeviceCounter(deviceName, type);

			var expected = target.NextValue;

			Assert.AreEqual(expected, target.NextValue);
		}

		[TestMethod]
		public void GetNextValue_DefaultDeviceCounter_IncrementationIsDone()
		{
			const string deviceName = "OK_DEVICENAME";
			const string type = "OK_TYPE";

			var target = CreateDefaultDeviceCounter(deviceName, type);

			var expected = target.NextValue + target.Increment;

			target.GetNextValue();

			Assert.AreEqual(expected, target.NextValue);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void GetNextValue_MaxCounterIsReached_ThrowsException()
		{
			const string deviceName = "OK_DEVICENAME";
			const string type = "OK_TYPE";

			var target = CreateDefaultDeviceCounter(deviceName, type);
			target.EndValue = Int32.MaxValue;
			target.NextValue = target.EndValue;
			
			target.GetNextValue();
		}

		#endregion

		private NumberRangeEntity CreateDefaultDeviceCounter(string deviceName, string type)
		{
			return new NumberRangeEntity()
				{
					Name = deviceName,
					Type = type,
					StartValue = 1,
					Increment = 1,
					NextValue = 2,
					EndValue = Int32.MaxValue,
					Cycle = false,
					Description = "Device transaction counter",
					ClientID = 0,
					Depot = 0
				};
		}
	}
}
