﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VarioSL.Entities.CollectionClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;
using VaroSL.Entities.TestFramework;

namespace VarioSL.Entities.Test.EntityClasses
{
    [TestClass]
    public class OrderEntityTests
    {
        private const long ValidShippingAddressID = 1;
        private const long ValidInvoiceAddressID = 2;

        #region SetPaymentOption

        [TestMethod]
        public void SetPaymentOption_ValidOrder_PaymentOptionIsSet()
        {
            OrderEntity order = new OrderEntity()
                {
                    IsNew = false,
                    State = OrderState.New
                };

            PaymentOptionEntity paymentOption = new PaymentOptionEntity()
                {
                    IsNew = false
                };

            order.SetPaymentOption(paymentOption);

            Assert.AreEqual(paymentOption, order.PaymentOption);
        }

        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        public void SetPaymentOption_PaymenOptionIsNew_ThrowException()
        {
            OrderEntity order = new OrderEntity()
                {
                    IsNew = false,
                    State = OrderState.New
                };

            PaymentOptionEntity paymentOption = new PaymentOptionEntity()
                {
                    IsNew = true
                };

            order.SetPaymentOption(paymentOption);
        }

        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        public void SetPaymentOption_OrderIsNew_ThrowException()
        {
            OrderEntity order = new OrderEntity()
                {
                    IsNew = true,
                    State = OrderState.New
                };

            PaymentOptionEntity paymentOption = new PaymentOptionEntity()
                {
                    IsNew = false
                };

            order.SetPaymentOption(paymentOption);
        }

        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        public void SetPaymentOption_PaymenOptionBelongsToDifferentContract_ThrowException()
        {
            const long contractID_A = 1;
            const long contractID_B = 2;

            OrderEntity order = new OrderEntity()
                {
                    IsNew = false,
                    State = OrderState.New,
                    ContractID = contractID_A
                };

            PaymentOptionEntity paymentOption = new PaymentOptionEntity()
                {
                    IsNew = false,
                    ContractID = contractID_B
                };

            order.SetPaymentOption(paymentOption);
        }

        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        public void SetPaymentOption_OrderStateIsNotNew_ThrowException()
        {
            OrderEntity order = new OrderEntity()
                {
                    IsNew = false,
                    State = OrderState.Failed
                };

            PaymentOptionEntity paymentOption = new PaymentOptionEntity()
                {
                    IsNew = false
                };

            order.SetPaymentOption(paymentOption);
        }

        #endregion

        #region VerifyContractAffiliation

        [TestMethod]
        public void ValidateContractAffiliation_OkContractID_NoException()
        {
            const long OK_contractID = 1;

            OrderEntity order = new OrderEntity()
                {
                    ContractID = OK_contractID
                };

            order.ValidateContractAffiliation(OK_contractID);
        }

        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        public void ValidateContractAffiliation_NotOkContractID_ThrowException()
        {
            const long OK_contractID = 1;
            const long NotOK_contractID = 2;

            OrderEntity order = new OrderEntity()
                {
                    ContractID = OK_contractID
                };

            order.ValidateContractAffiliation(NotOK_contractID);
        }

        #endregion

        #region ConfirmOrder

        [TestMethod]
        public void ConfirmOrder_ValidOrder_StateIsOpen()
        {
            OrderEntity order = new OrderEntity()
                {
                    IsNew = false,
                    State = OrderState.New
                };

            order.ConfirmOrder();

            Assert.AreEqual(OrderState.Open, order.State);
        }

        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        public void ConfirmOrder_OrderIsNew_ThrowException()
        {
            OrderEntity order = new OrderEntity()
                {
                    IsNew = true,
                    State = OrderState.New
                };

            order.ConfirmOrder();
        }

        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        public void ConfirmOrder_OrderStateNotNew_ThrowException()
        {
            OrderEntity order = new OrderEntity()
                {
                    IsNew = false,
                    State = OrderState.Open
                };

            order.ConfirmOrder();
        }

        #endregion

        #region SetContractAddresses

        [TestMethod]
        public void SetContractAddresses_ValidContractAddresses_ShippingAddressIDIsSet()
        {
            ContractEntity contract = new ContractEntity()
            {
                IsNew = false,
                AlreadyFetchedContractAddresses = true,
                ShippingAddress = new AddressEntity() { AddressID = ValidShippingAddressID, IsNew = true, IsDirty = false },
                InvoiceAddress = new AddressEntity() { AddressID = ValidInvoiceAddressID, IsNew = true, IsDirty = false }
            };

            OrderEntity order = new OrderEntity()
            {
                IsNew = false,
                State = OrderState.New,
                Contract = contract
            };

            order.SetContractAddresses();

            Assert.AreEqual(ValidShippingAddressID, order.ShippingAddressID);
        }

        [TestMethod]
        public void SetContractAddresses_ValidContractAddresses_InvoiceAddressIDIsSet()
        {
            ContractEntity contract = new ContractEntity()
            {
                IsNew = false,
                AlreadyFetchedContractAddresses = true,
                ShippingAddress = new AddressEntity() { AddressID = ValidShippingAddressID, IsNew = true, IsDirty = false },
                InvoiceAddress = new AddressEntity() { AddressID = ValidInvoiceAddressID, IsNew = true, IsDirty = false }
            };

            OrderEntity order = new OrderEntity()
            {
                IsNew = false,
                State = OrderState.New,
                Contract = contract
            };

            order.SetContractAddresses();

            Assert.AreEqual(ValidInvoiceAddressID, order.InvoiceAddressID);
        }

        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        [TestCategory("Integration")]
        public void SetContractAddresses_OrderIsNew_ThrowException()
        {
            ContractEntity contract = new ContractEntity()
            {
                IsNew = false,
                ShippingAddress = new AddressEntity() { AddressID = ValidShippingAddressID, IsNew = false, IsDirty = false },
                InvoiceAddress = new AddressEntity() { AddressID = ValidInvoiceAddressID, IsNew = false, IsDirty = false }
            };

            OrderEntity order = new OrderEntity()
            {
                IsNew = true,
                State = OrderState.New,
                Contract = contract
            };

            order.SetContractAddresses();
        }

        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        [TestCategory("Integration")]
        public void SetContractAddresses_OrderStateNotNew_ThrowException()
        {
            ContractEntity contract = new ContractEntity()
            {
                IsNew = false,
                ShippingAddress = new AddressEntity() { AddressID = ValidShippingAddressID, IsNew = false, IsDirty = false },
                InvoiceAddress = new AddressEntity() { AddressID = ValidInvoiceAddressID, IsNew = false, IsDirty = false }
            };

            OrderEntity order = new OrderEntity()
            {
                IsNew = false,
                State = OrderState.Open,
                Contract = contract
            };

            order.SetContractAddresses();
        }

        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        public void SetContractAddresses_ContractIsNull_ThrowException()
        {
            OrderEntity order = new OrderEntity()
            {
                IsNew = false,
                State = OrderState.New,
                Contract = null
            };

            order.SetContractAddresses();
        }

        //Will be fixed by EPA
        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        [TestCategory("Integration")]
        public void SetContractAddresses_ContractIsNew_ThrowException()
        {
            ContractEntity contract = new ContractEntity
            {
                IsNew = true,
                ShippingAddress = new AddressEntity { AddressID = ValidShippingAddressID, IsNew = false, IsDirty = false },
                InvoiceAddress = new AddressEntity { AddressID = ValidInvoiceAddressID, IsNew = false, IsDirty = false }
            };

            OrderEntity order = new OrderEntity
            {
                IsNew = false,
                State = OrderState.New,
                Contract = contract
            };

            order.SetContractAddresses();
        }

        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        [TestCategory("Integration")]
        public void SetContractAddresses_ShippingAddressesIDNotSet_ThrowException()
        {
            ContractEntity contract = new ContractEntity()
            {
                IsNew = false,
                InvoiceAddress = new AddressEntity() { AddressID = ValidInvoiceAddressID, IsNew = false, IsDirty = false }
            };

            OrderEntity order = new OrderEntity()
            {
                IsNew = false,
                State = OrderState.New,
                Contract = contract
            };

            order.SetContractAddresses();
        }

        [TestMethod, ExpectedException(typeof(InvalidOperationException))]
        [TestCategory("Integration")]
        public void SetContractAddresses_InvoiceAddressesIDNotSet_ThrowException()
        {
            ContractEntity contract = new ContractEntity()
            {
                IsNew = false,
                ShippingAddress = new AddressEntity() { AddressID = ValidShippingAddressID, IsNew = false, IsDirty = false },
            };

            OrderEntity order = new OrderEntity()
            {
                IsNew = false,
                State = OrderState.New,
                Contract = contract
            };

            order.SetContractAddresses();
        }

        #endregion

        #region OpenWorkItemID

        [TestMethod]
        public void OpenWorkItemID_WithUnassingedAndNoexecutedWorkitem_ReturnsWorkItemID()
        {
            var workItem = CreateWorkItemEntity(false, false, false);
            OrderWorkItemCollection workItems = new OrderWorkItemCollection(new List<OrderWorkItemEntity> { workItem });
            var orderMock = OpenWorkItemID_CreateMockOrder(workItems);
            Assert.AreEqual(workItem.WorkItemID, orderMock.Object.OpenWorkItemID);
        }

        [TestMethod]
        public void OpenWorkItemID_WithNewWorkitem_Zero()
        {
            var workItem = CreateWorkItemEntity(false, false, true);
            OrderWorkItemCollection workItems = new OrderWorkItemCollection(new List<OrderWorkItemEntity> { workItem });
            var orderMock = OpenWorkItemID_CreateMockOrder(workItems);
            Assert.AreEqual(0, orderMock.Object.OpenWorkItemID);
        }

        [TestMethod]
        public void OpenWorkItemID_WithAssingedAndNoexecutedWorkitem_Zero()
        {
            var workItem = CreateWorkItemEntity(true, false, false);
            OrderWorkItemCollection workItems = new OrderWorkItemCollection(new List<OrderWorkItemEntity> { workItem });
            var orderMock = OpenWorkItemID_CreateMockOrder(workItems);
            Assert.AreEqual(0, orderMock.Object.OpenWorkItemID);
        }

        [TestMethod]
        public void OpenWorkItemID_WithAssingedAndExecutedWorkitem_Zero()
        {
            var workItem = CreateWorkItemEntity(true, true, false);
            OrderWorkItemCollection workItems = new OrderWorkItemCollection(new List<OrderWorkItemEntity> { workItem });
            var orderMock = OpenWorkItemID_CreateMockOrder(workItems);
            Assert.AreEqual(0, orderMock.Object.OpenWorkItemID);
        }

        private static OrderWorkItemEntity CreateWorkItemEntity(bool isAssinged, bool isExecuted, bool isNew)
        {
            return new OrderWorkItemEntity
            {
                WorkItemID = 100,
                IsNew = isNew,
                Executed = isExecuted ? DateTime.Now : DateTime.MinValue,
                Assigned = isAssinged ? DateTime.Now : DateTime.MinValue,
            };
        }

        private static Mock<OrderEntity> OpenWorkItemID_CreateMockOrder(OrderWorkItemCollection workitems)
        {
            var mock = new Mock<OrderEntity>() { CallBase = true };
            mock.Setup(order => order.WorkItems).Returns(workitems);
            return mock;
        }

        #endregion

        #region TotalAmount

        [TestMethod]
        public void TotalAmount_OrderWithNoDetails_ReturnsZero()
        {
            OrderEntity order = new OrderEntity()
                {
                    AlreadyFetchedOrderDetails = true
                };

            Assert.AreEqual(order.TotalAmount(), 0);
        }

        [TestMethod]
        public void TotalAmount_OrderWithOneDetails_ReturnsTotal()
        {
            OrderEntity order = new OrderEntity()
                {
                    AlreadyFetchedOrderDetails = true
                };

            order.OrderDetails.Add(new OrderDetailEntity()
                    {
                        Quantity = 2,
                        Product = new ProductEntity()
                            {
                                Price = 555
                            }
                    });

            Assert.AreEqual(order.TotalAmount(), 2 * 555);
        }

        [TestMethod]
        public void TotalAmount_OrderWithMoreDetails_ReturnsTotal()
        {
            OrderEntity order = new OrderEntity()
                {
                    AlreadyFetchedOrderDetails = true
                };

            order.OrderDetails.Add(new OrderDetailEntity()
                {
                    Quantity = 2,
                    Product = new ProductEntity()
                        {
                            Price = 555
                        }
                });

            order.OrderDetails.Add(new OrderDetailEntity()
                {
                    Quantity = 1,
                    Product = new ProductEntity()
                        {
                            Price = 666
                        }
                });


            Assert.AreEqual(order.TotalAmount(), 2 * 555 + 1 * 666);
        }

        #endregion

		#region ExternalOrderNumber

		private IQueryable<SaleEntity> oldSaleData;

		[TestInitialize]
		public void Initialize()
		{
			oldSaleData = OrderEntity.saleData;
		}

		[TestCleanup]
		public void Cleanup()
		{
			OrderEntity.saleData = oldSaleData;
		}

		[TestMethod]
		public void ExternalOrderNumber_OrderWithoutSale_IsNull()
		{
			OrderEntity.saleData = new List<SaleEntity>().AsQueryable();

			OrderEntity order = new OrderEntity() { };

			Assert.IsNull(order.ExternalOrderNumberWithDefault);
		}

		[TestMethod]
		public void ExternalOrderNumber_OrderWithSale_IsEqual()
		{
			string eon = "Order-Number_123";
			OrderEntity.saleData = new List<SaleEntity>()
			{
				new SaleEntity(){
					OrderID = 123,
					ExternalOrderNumber = eon }
			}.AsQueryable();

			OrderEntity order = new OrderEntity() { OrderID = 123 };

			Assert.AreEqual(order.ExternalOrderNumberWithDefault, eon);
		}

		#endregion

		#region Copy

		private OrderEntity CreateOrder()
        {
            var order = new OrderEntity { AlreadyFetchedOrderDetails = true, IsNew = false, OrderID = 1 };
            var od = new OrderDetailEntity { OrderDetailID = 1, IsNew = false, Product = new ProductEntity { ProductID = 1, IsNew = false, SubsidyTransaction = new SubsidyTransactionEntity { SubsidyTransactionID = 1, IsNew = false } } };
            order.OrderDetails.Add(od);
            return order;
        }

        [TestMethod]
        public void Copy_ReturnsNewOrder()
        {
            var order = CreateOrder();
            var copy = order.Copy();

            Assert.IsTrue(copy.IsNew);
        }

        [TestMethod]
        public void Copy_ReturnsNewOrderDetail()
        {
            var order = CreateOrder();
            var copy = order.Copy();

            Assert.IsTrue(copy.OrderDetails.First().IsNew);
        }

        [TestMethod]
        public void Copy_ReturnsNewProduct()
        {
            var order = CreateOrder();
            var copy = order.Copy();

            Assert.IsTrue(copy.OrderDetails.First().Product.IsNew);
        }

        [TestMethod]
        public void Copy_CopyOrderFields()
        {
            var order = CreateOrder();
            var copy = order.Copy();

            ObjectAssert.AreEntityFieldsEqual(order, copy);
        }

        [TestMethod]
        public void Copy_CopyOrderDetailFields()
        {
            var order = CreateOrder();
            var copy = order.Copy();

            ObjectAssert.AreEntityEnumerablesEqual(order.OrderDetails, copy.OrderDetails);
        }

        [TestMethod]
        public void Copy_CopyProductFields()
        {
            var order = CreateOrder();
            var copy = order.Copy();

            ObjectAssert.AreEntityEnumerablesEqual(order.OrderDetails.Select(o => o.Product), copy.OrderDetails.Select(o => o.Product));
        }

        [TestMethod]
        public void Copy_CopySubsidyTranactionFields()
        {
            var order = CreateOrder();
            var copy = order.Copy();

            ObjectAssert.AreEntityEnumerablesEqual(order.OrderDetails.Select(o => o.Product.SubsidyTransaction), copy.OrderDetails.Select(o => o.Product.SubsidyTransaction));
        }

        [TestMethod]
        public void Copy_RunsOrderAction()
        {
            const string orderNumber = "Test";
            var order = CreateOrder();
            var copy = order.Copy(o => o.OrderNumber = orderNumber);

            Assert.AreEqual(orderNumber, copy.OrderNumber);
        }

        [TestMethod]
        public void Copy_RunsOrderDetailAction()
        {
            const int quantity = 3;
            var order = CreateOrder();

            var copy = order.Copy(orderDetailAction: od => od.Quantity = quantity);

            Assert.AreEqual(quantity, copy.OrderDetails.First().Quantity);
        }

        [TestMethod]
        public void Copy_RunsProductAction()
        {
            const int price = 55;
            var order = CreateOrder();

            var copy = order.Copy(productAction: p => p.Price = price);

            Assert.AreEqual(price, copy.OrderDetails.First().Product.Price);
        }

        [TestMethod]
        public void Copy_SetRequiredOrderDetail()
        {
            var order = CreateOrder();
            order.OrderDetails.Add(new OrderDetailEntity
            {
                IsNew = false,
                OrderDetailID = 2,
                RequiredOrderDetailID = 1,
                Product = new ProductEntity { IsNew = false }
                
            });

            var copy = order.Copy();

            Assert.AreEqual(copy.OrderDetails[0], copy.OrderDetails[1].RequiredOrderDetail);
        }


        #endregion

    }
}
