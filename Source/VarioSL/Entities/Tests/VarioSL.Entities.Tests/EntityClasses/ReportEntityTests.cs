﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;
using VaroSL.Entities.TestFramework;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class ReportEntityTests
	{
		[TestMethod]
		public void FilterCategories_WithOneCategory_ReturnsOneCategory()
		{
			var categories = CreateCategories(1);

			var result = CreateFilterListElements(categories).FilterCategories;

			ObjectAssert.AreEntityEnumerablesEqual(result, categories);
		}

		[TestMethod]
		public void FilterCategories_WithDuplicateCategories_ResultIsDistinct()
		{
			var categories = CreateCategories(2);
			var duplicateCategories = new List<FilterCategoryEntity> { categories[0], categories[1], categories[0] };

			var result = CreateFilterListElements(duplicateCategories).FilterCategories;

			ObjectAssert.AreEntityEnumerablesEqual(result, categories);
		}

		internal static List<FilterCategoryEntity> CreateCategories(int count)
		{
			return Enumerable.Range(1, count)
				.Select(i => new FilterCategoryEntity()
					{
						FilterCategoryID = i,
						Name = i.ToString(),
						Description = i.ToString()
					}).ToList();
		}

		private ReportEntity CreateFilterListElements(List<FilterCategoryEntity> categories)
		{
			var ret = new ReportEntity();
			ret.ReportCategory = new ReportCategoryEntity();
			ret.ReportCategory.FilterList = new FilterListEntity();
			ret.ReportCategory.FilterList.AlreadyFetchedFilterListElements = true;
			ret.ReportCategory.FilterList.FilterListElements.AddRange(
				categories.Select((category, i) => new FilterListElementEntity()
		        {
					FilterListElementID = i,
		            FilterCategory = category
		        }));
			return ret;
		}
	}
}
