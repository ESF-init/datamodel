﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class SettlementQueryValueEntityTests
	{
		private static SettlementQueryValueEntity CreateSettlementQueryValue_WithValue(long value, DateTime calculated)
		{
			var ret = new SettlementQueryValueEntity()
			{
				IsNew = false,
				AlreadyFetchedSettlementQuerySetting=true,
				//AlreadyFetchedSettlementRun=true,
				Value = value,
				Calculated = calculated
			};
			return ret;
		}

	}

}
