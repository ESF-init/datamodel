﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VarioSL.Entities.EntityClasses;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class WorkItemEntityTests
	{
		#region IsAssignAllowed

		[TestMethod]
		public void IsAssignAllowed_WithUnassignedUnexecutedWorkitem_ReturnsTrue()
		{
			var data = CreateWorkItemWithAssignedAndExecuted(DateTime.MinValue, DateTime.MinValue);
			Assert.IsTrue(data.IsAssignAllowed);
		}

		[TestMethod]
		public void IsAssignAllowed_WithAssignedUnexecutedWorkitem_ReturnsFalse()
		{
			var data = CreateWorkItemWithAssignedAndExecuted(DateTime.Now, DateTime.MinValue);
			Assert.IsFalse(data.IsAssignAllowed);
		}

		[TestMethod]
		public void IsAssignAllowed_WithUnassignedExecutedWorkitem_ReturnsFalse()
		{
			var data = CreateWorkItemWithAssignedAndExecuted(DateTime.MinValue, DateTime.Now);
			Assert.IsFalse(data.IsAssignAllowed);
		}

		[TestMethod]
		public void IsAssignAllowed_WithAssignedExecutedWorkitem_ReturnsFalse()
		{
			var data = CreateWorkItemWithAssignedAndExecuted(DateTime.Now, DateTime.Now);
			Assert.IsFalse(data.IsAssignAllowed);
		}



		#endregion

		#region IsUnassignAllowed

		[TestMethod]
		public void IsUnassignAllowed_WithAssignedUnexecutedWorkitem_ReturnsTrue()
		{
			var data = CreateWorkItemWithAssignedAndExecuted(DateTime.Now, DateTime.MinValue);
			Assert.IsTrue(data.IsUnassignAllowed);
		}

		[TestMethod]
		public void IsUnassignAllowed_WithAssignedExecutedWorkitem_ReturnsFalse()
		{
			var data = CreateWorkItemWithAssignedAndExecuted(DateTime.Now, DateTime.Now);
			Assert.IsFalse(data.IsUnassignAllowed);
		}

		[TestMethod]
		public void IsUnassignAllowed_WithUnassignedExecutedWorkitem_ReturnsFalse()
		{
			var data = CreateWorkItemWithAssignedAndExecuted(DateTime.MinValue, DateTime.Now);
			Assert.IsFalse(data.IsUnassignAllowed);
		}

		[TestMethod]
		public void IsUnassignAllowed_WithUnassignedUnexecutedWorkitem_ReturnsFalse()
		{
			var data = CreateWorkItemWithAssignedAndExecuted(DateTime.MinValue, DateTime.MinValue);
			Assert.IsFalse(data.IsUnassignAllowed);
		}

		#endregion

		#region IsExecuteAllowed

		[TestMethod]
		public void IsUnassignAllowed_WithUnexecutedWorkitem_ReturnsTrue()
		{
			var data = CreateWorkItemWithExecuted(DateTime.MinValue);
			Assert.IsTrue(data.IsExecuteAllowed);
		}

		[TestMethod]
		public void IsUnassignAllowed_WithExecutedWorkitem_ReturnsFalse()
		{
			var data = CreateWorkItemWithExecuted(DateTime.Now);
			Assert.IsFalse(data.IsExecuteAllowed);
		}

		#endregion

		#region IsUpdateMemoAllowed

		[TestMethod]
		public void IsUpdateMemoAllowed_WithAssignedUnexecutedWorkitem_ReturnsTrue()
		{
			var data = CreateWorkItemWithAssignedAndExecuted(DateTime.Now, DateTime.MinValue);
			Assert.IsTrue(data.IsUpdateMemoAllowed);
		}

		[TestMethod]
		public void IsUpdateMemoAllowed_WithAssignedExecutedWorkitem_ReturnsFalse()
		{
			var data = CreateWorkItemWithAssignedAndExecuted(DateTime.Now, DateTime.Now);
			Assert.IsFalse(data.IsUpdateMemoAllowed);
		}

		[TestMethod]
		public void IsUpdateMemoAllowed_WithUnassignedExecutedWorkitem_ReturnsFalse()
		{
			var data = CreateWorkItemWithAssignedAndExecuted(DateTime.MinValue, DateTime.Now);
			Assert.IsFalse(data.IsUpdateMemoAllowed);
		}

		[TestMethod]
		public void IsUpdateMemoAllowed_WithUnassignedUnexecutedWorkitem_ReturnsFalse()
		{
			var data = CreateWorkItemWithAssignedAndExecuted(DateTime.MinValue, DateTime.MinValue);
			Assert.IsFalse(data.IsUpdateMemoAllowed);
		}

		#endregion

		#region HasMemo

		[TestMethod]
		public void HasMemo_WithoutMemo_ReturnsFalse()
		{
			var workItemMock = HasMemo_CreateMockWorkItem(null);
			Assert.IsFalse(workItemMock.Object.HasMemo);
		}

		[TestMethod]
		public void HasMemo_WithEmptyMemo_ReturnsFalse()
		{
			var workItemMock = HasMemo_CreateMockWorkItem("");
			Assert.IsFalse(workItemMock.Object.HasMemo);
		}

		[TestMethod]
		public void HasMemo_WithFilledMemo_ReturnsTrue()
		{
			var workItemMock = HasMemo_CreateMockWorkItem("TEST");
			Assert.IsTrue(workItemMock.Object.HasMemo);
		}

		private static Mock<WorkItemEntity> HasMemo_CreateMockWorkItem(string memo)
		{
			var mock = new Mock<WorkItemEntity>() { CallBase = true };
			mock.Setup(workItem => workItem.Memo).Returns(memo);
			return mock;
		}

		#endregion

		#region SetAssign

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void SetAssigned_WithAssignedNotAllowed_ThrowsExeption()
		{
			var workItemMock = SetAssign_CreateMockWorkItem(false);
			workItemMock.Object.SetAssigned();

		}

		[TestMethod]
		public void SetAssigned_WithAssignedAllowed_AssignedChanged()
		{
			var workItemMock = SetAssign_CreateMockWorkItem(true);
			workItemMock.Object.SetAssigned();
			Assert.AreNotEqual(workItemMock.Object.Assigned, DateTime.MinValue);
		}

		private static Mock<WorkItemEntity> SetAssign_CreateMockWorkItem(bool isAssignAllowed)
		{
			var mock = new Mock<WorkItemEntity>() { CallBase = true };
			mock.Setup(workItem => workItem.IsAssignAllowed).Returns(isAssignAllowed);

			return mock;
		}

		#endregion

		#region SetUnassign

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void SetUnassigned_WithUnassignedNotAllowed_ThrowsExeption()
		{
			var workItemMock = SetUnassign_CreateMockWorkItem(false);
			workItemMock.Object.Assigned = DateTime.Now;
			workItemMock.Object.SetUnassigned();
		}

		[TestMethod]
		public void SetUnassigned_WithUnassignedAllowed_AssignedChanged()
		{
			var workItemMock = SetUnassign_CreateMockWorkItem(true);
			workItemMock.Object.Assigned = DateTime.Now;
			workItemMock.Object.SetUnassigned();
			Assert.AreEqual(DateTime.MinValue, workItemMock.Object.Assigned);
		}

		private static Mock<WorkItemEntity> SetUnassign_CreateMockWorkItem(bool isUnassignAllowed)
		{
			var mock = new Mock<WorkItemEntity>() { CallBase = true };
			mock.Setup(workItem => workItem.IsUnassignAllowed).Returns(isUnassignAllowed);
			return mock;
		}

		#endregion

		#region SetExecuted

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void SetExecuted_WithExecuteNotAllowed_ThrowsExeption()
		{
			var workItemMock = SetExecuted_CreateMockWorkItem(false);
			
			workItemMock.Object.SetExecuted();
		}

		[TestMethod]
		public void SetExecuted_WithExecuteAllowed_ExecutedChanged()
		{
			var workItemMock = SetExecuted_CreateMockWorkItem(true);
		
			workItemMock.Object.SetExecuted();
			Assert.AreNotEqual(DateTime.MinValue, workItemMock.Object.Executed);
		}

		private static Mock<WorkItemEntity> SetExecuted_CreateMockWorkItem(bool isExecuteAllowed)
		{
			var mock = new Mock<WorkItemEntity>() { CallBase = true };
			mock.Setup(workItem => workItem.IsExecuteAllowed).Returns(isExecuteAllowed);
			return mock;
		}

		#endregion

		#region UpdateMemo

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void UpdateMemo_WithUpdateMemoNotAllowed_ThrowsExeption()
		{
			var workItemMock = UpdateMemo_CreateMockWorkItem(false);
			workItemMock.Object.Memo = "OldMemo";
			workItemMock.Object.UpdateMemo("NewMemo");
		}

		[TestMethod]
		public void UpdateMemo_WithUpdateMemoAllowed_ExecutedChanged()
		{
			var workItemMock = UpdateMemo_CreateMockWorkItem(true);
			workItemMock.Object.Memo = "OldMemo";
			workItemMock.Object.UpdateMemo("NewMemo");
			Assert.AreNotEqual("OldMemo", workItemMock.Object.Memo);
		}

		[TestMethod]
		public void UpdateMemo_WithUpdateMemoAllowedAndWhiteSpacedMemo_ReturnTrimedMemo()
		{
			const string whiteSpacedMemo = "NewMemo	   ";
			var workItemMock = UpdateMemo_CreateMockWorkItem(true);
			workItemMock.Object.Memo = "OldMemo";
			workItemMock.Object.UpdateMemo(whiteSpacedMemo);
			Assert.AreEqual(whiteSpacedMemo.Trim(), workItemMock.Object.Memo);
		}

		private static Mock<WorkItemEntity> UpdateMemo_CreateMockWorkItem(bool isUpdateMemoAllowed)
		{
			var mock = new Mock<WorkItemEntity>() { CallBase = true };
			mock.Setup(workItem => workItem.IsUpdateMemoAllowed).Returns(isUpdateMemoAllowed);
			return mock;
		}

		#endregion

		#region IsAssigned

		[TestMethod]
		public void IsAssigned_WithoutAssignedSet_ReturnsFalse()
		{
			var workItemMock = IsAssigned_CreateMockWorkItem(DateTime.MinValue);
			Assert.IsFalse(workItemMock.Object.IsAssigned);
		}

		[TestMethod]
		public void IsAssigned_WithAssignedSet_ReturnsTrue()
		{
			var workItemMock = IsAssigned_CreateMockWorkItem(DateTime.Now);
			Assert.IsTrue(workItemMock.Object.IsAssigned);
		}
		
		private static Mock<WorkItemEntity> IsAssigned_CreateMockWorkItem(DateTime assigned)
		{
			var mock = new Mock<WorkItemEntity>() { CallBase = true };
			mock.Setup(workItem => workItem.Assigned).Returns(assigned);
			return mock;
		}

		#endregion

		#region IsExecuted

		[TestMethod]
		public void IsExecuted_WithoutAssignedSet_ReturnsFalse()
		{
			var workItemMock = IsExecuted_CreateMockWorkItem(DateTime.MinValue);
			Assert.IsFalse(workItemMock.Object.IsExecuted);
		}

		[TestMethod]
		public void IsExecuted_WithAssignedSet_ReturnsTrue()
		{
			var workItemMock = IsExecuted_CreateMockWorkItem(DateTime.Now);
			Assert.IsTrue(workItemMock.Object.IsExecuted);
		}

		private static Mock<WorkItemEntity> IsExecuted_CreateMockWorkItem(DateTime executed)
		{
			var mock = new Mock<WorkItemEntity>() { CallBase = true };
			mock.Setup(workItem => workItem.Executed).Returns(executed);
			return mock;
		}

		#endregion


		private WorkItemEntity CreateWorkItemWithExecuted(DateTime exeuted)
		{
			return new WorkItemEntity() { Executed = exeuted, Created = DateTime.Now };
		}

		private WorkItemEntity CreateWorkItemWithAssignedAndExecuted(DateTime assigned, DateTime exeuted)
		{
			return new WorkItemEntity() { Executed = exeuted, Assigned = assigned, Created = DateTime.Now };
		}
	}
}
