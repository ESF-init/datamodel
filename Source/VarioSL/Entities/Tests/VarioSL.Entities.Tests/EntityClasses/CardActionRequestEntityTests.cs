﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class CardActionRequestEntityTests
	{

		#region SetProcessed

		[TestMethod]
		public void SetProcessed_ValidCardActionRequest_ProcessDateIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity();

			DateTime VALID_date = DateTime.Now;

			car.SetProcessed(VALID_date);

			Assert.AreEqual(VALID_date, car.ProcessDate.Value);
		}

		[TestMethod]
		public void SetProcessed_ValidCardActionRequest_IsProcessedIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity();

			car.SetProcessed(DateTime.Now);

			Assert.IsTrue(car.IsProcessed.Value);
		}

		#endregion

		#region CreateRequest

		[TestMethod]
		public void CreateCardRegisteredRequest_ValidCardActionRequest_TypeIsSetToSetCardAttributes()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };
			var expected = (int)CardActionRequestType.Modify;

			car.CreateCardRegisteredRequest(card, 0, Source.Unknown);

			Assert.AreEqual(expected, car.RequestType);
		}

		[TestMethod]
		public void CreateCardRegisteredRequest_ValidCardActionRequest_RequestNoIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };
			var expected = 1;

			car.CreateCardRegisteredRequest(card, expected, Source.Unknown);

			Assert.AreEqual(expected, car.RequestNo);
		}

		[TestMethod]
		public void CreateCardRegisteredRequest_ValidCardActionRequest_RequestSourceIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };
			var expected = Source.Other;

			car.CreateCardRegisteredRequest(card, 0, expected);

			Assert.AreEqual((long)expected, car.RequestSource);
		}

		[TestMethod]
		public void CreateCardRegisteredRequest_ValidCardActionRequest_CreationDateIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };

			car.CreateCardRegisteredRequest(card, 0, Source.Unknown);

			Assert.AreNotEqual(DateTime.MinValue, car.CreationDate);
		}

		[TestMethod]
		public void CreateCardRegisteredRequest_IsValidNotSet_IsValidIsTrue()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };

			car.CreateCardRegisteredRequest(card, 0, Source.Unknown);

			Assert.IsTrue(car.IsValid);
		}

		[TestMethod]
		public void CreateCardRegisteredRequest_IsValidSetFalse_IsValidIsFalse()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };

			car.CreateCardRegisteredRequest(card, 0, Source.Unknown, false);

			Assert.IsFalse(car.IsValid);
		}

		[TestMethod]
		public void CreateCardRegisteredRequest_ValidCardNumber_CardNoIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			const long VALID_SERIALNUMBER = 123456;

			CardEntity card = new CardEntity() { SerialNumber = VALID_SERIALNUMBER.ToString() };

			car.CreateCardRegisteredRequest(card, 0, Source.Unknown);

			Assert.AreEqual(VALID_SERIALNUMBER, car.CardNo);
		}

		[TestMethod]
		public void CreateCardRegisteredRequest_ValidCardActionRequest_OneCardActionAttributeWasAdded()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };

			car.CreateCardRegisteredRequest(card, 0, Source.Unknown);

			Assert.AreEqual(1, car.CardActionAttributes.Count);
		}

		#endregion

		[TestMethod]
		public void CreateCardRegisteredRequest_ValidCardActionRequest_CardActionAttributeIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };
			const long expected = (long)CardRegistrationState.Registered;

			car.CreateCardRegisteredRequest(card, 0, Source.Unknown);

			Assert.AreEqual(expected, car.CardActionAttributes.First().Registration);
		}

		[TestMethod]
		public void CreateCardNotRegisteredRequest_ValidCardActionRequest_CardActionAttributeIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };
			const long expected = (long)CardRegistrationState.NotRegistered;

			car.CreateCardNotRegisteredRequest(card, 0, Source.Unknown);

			Assert.AreEqual(expected, car.CardActionAttributes.First().Registration);
		}

		[TestMethod]
		public void CreateCardRegisteredWithPaymentOptionRequest_ValidCardActionRequest_CardActionAttributeIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };
			const long expected = (long)CardRegistrationState.RegisteredWithPaymentOption;

			car.CreateCardRegisteredWithPaymentOptionRequest(card, 0, Source.Unknown);

			Assert.AreEqual(expected, car.CardActionAttributes.First().Registration);
		}

		[TestMethod]
		public void CreateSetDateOfBirthRequest_ValidCardActionRequest_CardActionAttributeIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };
			DateTime expected = new DateTime(2000, 1, 1);

			car.CreateSetDateOfBirthRequest(card, 0, expected, Source.Unknown);

			Assert.AreEqual(expected, car.CardActionAttributes.First().DateOfBirth);
		}

		[TestMethod]
		public void CreateSetExpiryDateRequest_ValidCardActionRequest_CardActionAttributeIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };
			DateTime expected = new DateTime(2050, 1, 1);

			car.CreateSetExpiryDateRequest(card, 0, expected, Source.Unknown);

			Assert.AreEqual(expected, car.CardActionAttributes.First().ExpiryDate);
		}

		[TestMethod]
		public void CreateSetVoiceEnabledRequest_ValidCardActionRequest_CardActionAttributeIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };
			bool expected = true;

			car.CreateSetVoiceEnabledRequest(card, 0, expected, Source.Unknown);

			Assert.AreEqual(expected, car.CardActionAttributes.First().VoiceFlag);
		}
			
		[TestMethod]
		public void CreateSetDisabledUserGroupRequest_ValidCardActionRequest_CardActionAttributeIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			CardEntity card = new CardEntity() { SerialNumber = "0" };
			var expected = TariffUserGroup.Disabled;
			const bool setDisabledUser = true;

			car.CreateSetDisabledUserGroupRequest(card, 0, setDisabledUser, Source.Unknown);

			Assert.AreEqual((long)expected, car.CardActionAttributes.First().UserGroupID);
		}

		[TestMethod]
		public void AddDateOfBirthRequest_ValidCardActionRequest_DateOfBirthIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			car.CardActionAttributes.Add(new CardActionAttributeEntity());

			DateTime expected = new DateTime(2000, 1, 1);

			car.AddDateOfBirth(expected);

			Assert.AreEqual(expected, car.CardActionAttributes.First().DateOfBirth);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddDateOfBirthRequest_WithoutCardActionAttribute_ThrowsException()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };

			car.AddDateOfBirth(new DateTime(2000, 1, 1));
		}

		[TestMethod]
		public void AddDisabledUserGroupRequest_ValidCardActionRequest_UserGroupIsSet()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };
			car.CardActionAttributes.Add(new CardActionAttributeEntity());

			var expected = TariffUserGroup.Disabled;
			const bool setDisabledUser = true;

			car.AddDisabledUserGroup(setDisabledUser);

			Assert.AreEqual((long)expected, car.CardActionAttributes.First().UserGroupID);
		}

		[TestMethod, ExpectedException(typeof(InvalidOperationException))]
		public void AddDisabledUserGroupRequest_WithoutCardActionAttribute_ThrowsException()
		{
			CardActionRequestEntity car = new CardActionRequestEntity() { AlreadyFetchedCardActionAttributes = true };

			car.AddDisabledUserGroup(true);
		}
	}

}
