﻿using System.Linq;
using System.Security.Principal;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.Linq;

namespace VarioSL.Entities.Test.EntityClasses
{
	[TestClass]
	public class CustomerAccountTest
	{
		#region Additional test attributes

		private static IPrincipal originalPrincipal;

		// Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext) { }

		// Use ClassCleanup to run code after all tests in a class have run
		[ClassCleanup()]
		public static void MyClassCleanup()
		{

		}

		// Use TestInitialize to run code before running each test 
		[TestInitialize()]
		public void MyTestInitialize()
		{
			Impersonate();
		}

		// Use TestCleanup to run code after each test has run
		[TestCleanup()]
		public void MyTestCleanup()
		{
			UndoImpersonation();
		}

		private static void Impersonate()
		{
			originalPrincipal = Thread.CurrentPrincipal;
			var identity = new GenericIdentity(WindowsIdentity.GetCurrent().Name);
			var principal = new GenericPrincipal(identity, null);
			Thread.CurrentPrincipal = principal;
		}

		private static void UndoImpersonation()
		{
			Thread.CurrentPrincipal = originalPrincipal;
		}

		#endregion
		
		[TestMethod]
		[TestCategory("Integration")]
		public void FetchUsing()
		{
			string userName = "mwirthmann123@init-ka.de";
			var customerAccount = new LinqMetaData().CustomerAccount
				.Where(c => c.State != CustomerAccountState.Closed)
				.Where(c => c.UserName == userName)
				.FirstOrDefault() ?? new CustomerAccountEntity();

			Assert.IsTrue(customerAccount.IsNew);
		}
	}
}
