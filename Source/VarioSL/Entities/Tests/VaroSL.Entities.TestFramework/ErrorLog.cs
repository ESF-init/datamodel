﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VaroSL.Entities.TestFramework
{
    public class ErrorLog
    {
        private readonly List<string> errors = new List<string>();

        public void ThrowErrors()
        {
            if (errors.Count == 0)
            {
                return;
            }
            var message = string.Join(Environment.NewLine, errors);
            errors.Clear();
            throw new AssertFailedException(message);
        }

        public void Add(string error)
        {
            errors.Add(error);
        }
    }
}