﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VarioSL.Entities.EntityClasses;

namespace VaroSL.Entities.TestFramework
{
    public static class ObjectAssert
    {
        public static void AreEntityFieldsEqual<T>(T left, T right) where T : CommonEntityBase
        {
            var errors = new ErrorLog();
            var zip = left.Fields.Zip(right.Fields, (l, r) => new { l, r });
            foreach (var item in zip)
            {
                if (item.l.CurrentValue == null && item.r.CurrentValue == null)
                    continue;
                if (!item.l.CurrentValue.Equals(item.r.CurrentValue))
                {
                    errors.Add(string.Format("Wrong field {0}: {1} - {2}",
                                                        item.l.Name,
                                                        item.l.CurrentValue,
                                                        item.r.CurrentValue));
                }

            }
            errors.ThrowErrors();
        }

        public static void AreObjectPropertiesEqual<T, V>(T source, V destination)
            where T : class
            where V : class
        {
            var errors = new ErrorLog();
            if (AnyNull(source, typeof(T).Name, destination, typeof(V).Name, errors))
            {
                errors.ThrowErrors();
                return;
            }

            Type sourceType = typeof(T);
            Type destinationType = typeof(V);
            PropertyInfo destinationProperty;

            foreach (PropertyInfo sourceProperty in sourceType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly))
            {
                destinationProperty = destinationType.GetProperty(sourceProperty.Name);
                if (destinationProperty == null)
                {
                    errors.Add(
                        string.Format("The property {0} doesn't exist in the destination object.", sourceProperty.Name));
                    continue;
                }

                var first = sourceProperty.GetValue(source, null);
                var second = destinationProperty.GetValue(destination, null);

                if (destinationProperty.PropertyType.IsEnum && sourceProperty.PropertyType.IsValueType)
                {
                    if ((int)first != (int)second)
                    {
                        errors.Add(
                            string.Format("The values are different for property {0}: {1} - {2}", sourceProperty.Name, first, second));
                    }
                }
                else if (sourceProperty.PropertyType.IsValueType || sourceProperty.PropertyType == typeof(string))
                {

                    if (AnyNull(first, sourceProperty.Name, second, destinationProperty.Name, errors))
                    {
                        continue;
                    }
                    else if (first.ToString() != second.ToString())
                    {
                        errors.Add(
                            string.Format("The values are different for property {0}: {1} - {2}", sourceProperty.Name, first, second));
                    }

                }
            }
            errors.ThrowErrors();
        }

        public static void AreEntityEnumerablesEqual<T>(IEnumerable<T> leftEnumerable, IEnumerable<T> rightEnumerable)
            where T : CommonEntityBase
        {
            EnumerableCompare<T, T>(leftEnumerable, rightEnumerable, (left, right) => AreEntityFieldsEqual(left, right));
        }

        public static void AreEnumerablesEqual<T, V>(IEnumerable<T> leftEnumerable, IEnumerable<V> rightEnumerable)
            where T : class
            where V : class
        {
            EnumerableCompare<T, V>(leftEnumerable, rightEnumerable, (left, right) => AreObjectPropertiesEqual(left, right));
        }

        private static void EnumerableCompare<T, V>(IEnumerable<T> leftEnumerable, IEnumerable<V> rightEnumerable, Action<T, V> a)
        {
            var errors = new ErrorLog();

            if (leftEnumerable.Count() != rightEnumerable.Count())
            {
                errors.Add("Counts do not match.");
                errors.ThrowErrors();
            }

            var zip = leftEnumerable.Zip(rightEnumerable, (left, right) => new { left, right });

            foreach (var item in zip)
            {
                try
                {
                    a(item.left, item.right);
                }
                catch (Exception e)
                {
                    var name = item.left.ToString() == item.right.ToString() ? item.left.ToString() :
                        item.left.ToString() + "/" + item.right.ToString();
                    errors.Add(name + " - " + e.Message);
                }
            }

            errors.ThrowErrors();
        }

        private static bool AnyNull(object one, string nameOne, object two, string nameTwo, ErrorLog errors)
        {
            if (one == null && two == null)
            {
                return true;
            }
            else if (one == null)
            {
                errors.Add(nameOne + " is null.");
                return true;
            }
            else if (two == null)
            {
                errors.Add(nameTwo + " is null.");
                return true;
            }

            return false;
        }
    }

    public class RowTest<Entity, Type, Result> where Entity : CommonEntityBase
    {
        private readonly List<Row> list = new List<Row>();
        private readonly Func<Type, string> parameterStringFunc;

        public RowTest(Func<Type, string> parameterStringFunc)
        {
            this.parameterStringFunc = parameterStringFunc;
        }

        public void Add(Type type, Result result, string message = null)
        {
            list.Add(new Row() { Type = type, Expected = result, Message = message });
        }

        public void Assert(Func<Type, Entity> createWithTypeAction, Func<Entity, Result> resultAction)
        {
            var result = list
                .Where(row => !resultAction(createWithTypeAction(row.Type))
                    .Equals(row.Expected))
                .Select(row => string.Format("{0}{1}: Expected({2}) - Was({3})", row, parameterStringFunc(row.Type), row.Expected, resultAction(createWithTypeAction(row.Type))));

            if (result.Any())
            {
                throw new AssertFailedException(result.Aggregate((a, b) => string.Format("{0}{1}{2}", a, Environment.NewLine, b)));
            }
        }

        private class Row
        {
            public Type Type { get; set; }
            public Result Expected { get; set; }
            public string Message { get; set; }

            public override string ToString()
            {
                return !string.IsNullOrEmpty(Message) ? string.Format("{0}{1}\t", Message, Environment.NewLine) : string.Empty;
            }
        }
    }
}
