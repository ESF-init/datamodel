﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// 
    /// </summary>
    [NotInDatabase]
    public enum WildcardSearchMode
    {
        /// <summary>
        /// Wildcards are not allowed in search. The search function must ensure that wildcard
        /// characters does not interfere in the search (for example by removing them).
        /// </summary>
        Disabled = 0,
        
        /// <summary>
        /// Implicit wildcard search. The search function should wrap the search term in wildcard
        /// characters which would result in a Contains() search.
        /// </summary>
        EnabledImplicit = 1,

        /// <summary>
        /// Explicit. Only user input wildcard characters lead to a wildcard search. If the search
        /// term does not contain wildcards, the search is not wild carded.
        /// </summary>
        EnabledExplicit = 2,
    }
}
