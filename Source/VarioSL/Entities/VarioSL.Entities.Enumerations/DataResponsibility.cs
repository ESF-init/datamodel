﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available data responsibilities.
    /// </summary>
	[NotInDatabase]
	public enum DataResponsibility
	{
        /// <summary>
        /// Data responsibility none.
        /// </summary>
        NONE = 0,

        /// <summary>
        ///Data responsibility tariff.
        /// </summary>
        TARIFF = 1,

        /// <summary>
        /// Data responsibility device parameter.
        /// </summary>
        DEVICE_PARAMETER = 2,

        /// <summary>
        /// Data responsibility line def.
        /// </summary>
        LINE_DEF = 3,

        /// <summary>
        /// Data responsibility crypto.
        /// </summary>
        CRYPTO = 4,

        /// <summary>
        /// Data responsibility cryptogram.
        /// </summary>
        CRYPTOGRAM = 5,

        /// <summary>
        /// Data responsibility gtfs file provider.
        /// </summary>
        GTFS_FILE_PROVIDER = 6,

        /// <summary>
        /// Data responsibility Girogo / EBICS Housekeeping.
        /// </summary>
        GIROGO_EBICS = 7,

        /// <summary>
        /// Data responsibility for reading and saving forms (SL_FORM)
        /// </summary>
        FORMS = 8
    }
}
