﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available unit types.
    /// </summary>
    public enum UnitType
    {
        /// <summary>
        /// Unit type garage (will be mapped to database value '0').
        /// </summary>
        Garage = 0,

        /// <summary>
        /// Unit type group (will be mapped to database value '1').
        /// </summary>
        Group = 1,

        /// <summary>
        /// Unit type data transfer (will be mapped to database value '2').
        /// </summary>
        DataTransfer = 2,

        /// <summary>
        /// Unit type vehicle (will be mapped to database value '3').
        /// </summary>
		Vehicle = 3,

        /// <summary>
        /// Unit type customer center (will be mapped to database value '4').
        /// </summary>
		CustomerCenter = 4,

        /// <summary>
        /// Unit type advance sale (will be mapped to database value '5').
        /// </summary>
        AdvanceSale = 5,

        /// <summary>
        /// Unit type INIT (will be mapped to database value '6').
        /// </summary>
        INIT = 6,

        /// <summary>
        /// Unit type Mobile Application (will be mapped to database value '7').
        /// </summary>
        MobileApplication = 7
    }
}
