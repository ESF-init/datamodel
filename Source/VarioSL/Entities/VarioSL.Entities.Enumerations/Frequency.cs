﻿namespace VarioSL.Entities.Enumerations
{
    public enum Frequency
    {
        Daily = 0,
        Weekly = 1,
        Biweekly = 2,
        Monthly = 3,
        Bimonthly = 4,
        Quarterly = 5,
        Semiannually = 6,
        Anually = 7,
        Undetermined = 8
    }
}
