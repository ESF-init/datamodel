﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Enumeration for the timings of the client side
    /// </summary>
	[NotInDatabase]
	public enum TransactionTransportState
	{
		/// <summary>
		/// The client did not wait for the response
		/// </summary>
		Unknown = 0,
		/// <summary>
		/// The client received the response in time
		/// </summary>
		Received = 1,
		/// <summary>
		/// The client did not receive the response in time
		/// </summary>
		Timeout = 2,
		/// <summary>
		/// Error
		/// </summary>
		Error = 3
	}
}
