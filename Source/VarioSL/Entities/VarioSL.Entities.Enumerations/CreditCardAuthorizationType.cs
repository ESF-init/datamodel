﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available credit card authorization types.
    /// </summary>
    [NotInDatabase]
    public enum CreditCardAuthorizationType
    {
        /// <summary>
        /// Credit card authorization type unknown (will be mapped to database value '0').
        /// </summary>
        External = 0,

        /// <summary>
        /// Credit card authorization type Internal (will be mapped to database value '1').
        /// </summary>
        Internal = 1,
        DebtRecoveryZeroValue = 2,
        DebtRecovery = 3
    }
}
