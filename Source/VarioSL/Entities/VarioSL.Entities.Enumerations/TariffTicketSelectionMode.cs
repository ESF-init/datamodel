﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum TariffTicketSelectionMode
    {
        Fixed = 1,
        Matrix = 2,
        DriverEntered = 4,
        Attribute = 7,
        MatrixOrAttribute = 8,
        Distance = 9,
        TemporalInterpolation=10
    }
}
