﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary> 
    /// Available archive state.
    /// </summary>
    public enum ArchiveState
    {
        /// <summary>
        /// Archive data is inconsistent. Data might be incomplete (will be mapped to database value '0').
        /// </summary>
        Inconsistent = 0,

        /// <summary>
        /// Archive data is consistent. All data is available and complete (will be mapped to database value '1').
        /// </summary>
        Consistent = 1,
        
        /// <summary>
        /// Archive has been released (will be mapped to database value '2').
        /// </summary>
        Released = 2,
        
        /// <summary>
        /// Archive has been deleted and is no more to be used for export (will be mapped to database value '3').
        /// </summary>
        Deleted = 3
    }
}
