using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum ReportParameterType
	{
		NUMBERS = 1,
		CURRENCY = 2,
		TEXT = 3,
		BOOLEAN = 4,
		DATE = 5,
		TIME24h = 6,
		DATETIME = 7
	};
}
