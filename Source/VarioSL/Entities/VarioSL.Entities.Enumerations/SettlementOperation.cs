﻿namespace VarioSL.Entities.Enumerations
{
	public enum SettlementOperation
	{
		Static = 0,
		Transaction_Count = 1,
		Transaction_Average_Price = 2,
		Transaction_Sum_Price = 3,
		Product_Average_Price = 4,
		Days_In_Month = 5,
		Business_Days_In_Month = 6
	}
}
