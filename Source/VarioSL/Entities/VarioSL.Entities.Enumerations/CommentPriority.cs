﻿namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Priority of a comment entity.
	/// </summary>
	public enum CommentPriority
	{
		Low = 0,
		High = 100,
	}
}
