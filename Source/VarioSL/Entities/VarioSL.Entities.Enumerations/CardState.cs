﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available card states.
    /// </summary>
	public enum CardState
	{
        /// <summary>
        /// Card state inactive (will be mapped to database value '0').
        /// </summary>
        Inactive = 0,

        /// <summary>
        /// Card state active (will be mapped to database value '1').
        /// </summary>
        Active = 1,

        /// <summary>
        /// Card state blocked (will be mapped to database value '2').
        /// </summary>
        Blocked = 2,

        /// <summary>
        /// Card state replaced (will be mapped to database value '3').
        /// </summary>
        Replaced = 3,

        /// <summary>
        /// Card state replacement ordered (will be mapped to database value '4').
        /// </summary>
        ReplacementOrdered = 4,

        /// <summary>
        /// Card state dormant (will be mapped to database value '7').
        /// </summary>
        Dormant = 7,

        /// <summary>
        /// Card state expired (will be mapped to database value '8').
        /// </summary>
        Expired = 8
	}
}
