﻿using System;
using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    [Flags]
    public enum TransactionJournalProperties
    {
        None = 0,
        OvsAutoCancellation = 1,
        OpenLoopTopUpExternalException = 2,
        BlockCard = 4,
        UnblockCard = 8,
        OvsApproveDeviceDeny = 16,
        OvsDenyDeviceApprove = 32,
        OvsDenyDeviceDenyDifferent = 64,
        IsActivation = 128,
        VirtualOpenLoopTopUp = 256,
        OpenLoopCompletion = 512,
        OpenLoopRefund = 1024,
        DontUnblock = 2048,
        MultiRidePass = 4096,
        ProductTransferLoad = 8192,
        ProductTransferAddValueAccount = 16384
    }

}
