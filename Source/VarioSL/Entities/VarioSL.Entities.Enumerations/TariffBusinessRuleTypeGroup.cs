﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum TariffBusinessRuleTypeGroup
	{
		TestSale = 1,
		TestBoarding = 2,
		TicketFilter = 3,
		CreditLimit = 4,
		LineOverride = 5,
        TicketAssignment = 9,
		Inspection = 10
	}
}
