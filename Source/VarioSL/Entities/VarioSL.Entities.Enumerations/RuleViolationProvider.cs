﻿namespace VarioSL.Entities.Enumerations
{
    public enum RuleViolationProvider
    {
        Unknown = 0,
        SL = 1,
        RM = 2,
        VDVKA = 3
    }
}
