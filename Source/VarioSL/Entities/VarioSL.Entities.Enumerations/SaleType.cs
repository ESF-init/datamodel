﻿
namespace VarioSL.Entities.Enumerations
{
	public enum SaleType
	{
		Sale = 0,
		Order = 1,
		Refund = 2,
		Cancellation = 3
	}
}
