﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available credit card authorization status.
    /// </summary>
    [NotInDatabase]
    public enum CreditCardAuthorizationStatus
    {
        /// <summary>
        /// Credit card authorization status AuthError.
        /// </summary>
        AuthError = -2,

        /// <summary>
        /// Credit card authorization status CompletionError.
        /// </summary>
        CompletionError = -1,

        /// <summary>
        /// Credit card authorization status Open.
        /// </summary>
        Open = 0,

        /// <summary>
        /// Credit card authorization status Completed.
        /// </summary>
        Completed = 1,

        /// <summary>
        /// Credit card authorization status Retry.
        /// </summary>
        Retry = 2,

        /// <summary>
        /// Authorization/Settlement was performed, but not yet booked in Init system.
        /// </summary>
        PreCompleted = 3,

        /// <summary>
        /// This status will be set when an error occured which doesn't block the card. 
        /// </summary>
        GoodwillCompletion = 4
    }
}
