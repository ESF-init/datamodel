﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available close out states.
    /// </summary>
	public enum CloseoutState
	{
        /// <summary>
        /// Close out state started (will be mapped to database value '0').
        /// </summary>
        Started = 0,

        /// <summary>
        /// CloseoutState recognized (will be mapped to database value '1').
        /// </summary>
        Recognized = 1,

        /// <summary>
        /// Close out state export started (will be mapped to database value '2').
        /// </summary>
        ExportStarted = 2,

        /// <summary>
        /// Close out state exported (will be mapped to database value '3').
        /// </summary>
        Exported = 3,

        /// <summary>
        /// Close out state failed (will be mapped to database value '4').
        /// </summary>
        Failed = 4,

        /// <summary>
        /// Close out state export failed (will be mapped to database value '5').
        /// </summary>
        ExportFailed = 5
	}
}
