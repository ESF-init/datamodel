﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available device token state.
    /// </summary>
    public enum DeviceTokenState
    {

        /// <summary>
        /// Device token state inactive (will be mapped to database value '0').
        /// </summary>
        Inactive = 0,

        /// <summary>
        /// Device token state active (will be mapped to database value '1').
        /// </summary>
        Active = 1,

       
    }
}
