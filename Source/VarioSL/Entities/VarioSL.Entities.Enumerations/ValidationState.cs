﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Validation state of a validation run.
    /// </summary>
    [NotInDatabase]
    public enum ValidationState
    {
        Started = 1,
        Finished = 2
    }
}
