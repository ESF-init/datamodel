﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum TariffVdvTypeGroup
    {
        IssueModeSe = 1,
        IssueModeNm = 2,
        PriorityMode = 3
    }
}
