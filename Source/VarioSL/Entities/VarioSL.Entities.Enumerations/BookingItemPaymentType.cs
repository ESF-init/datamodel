﻿
namespace VarioSL.Entities.Enumerations
{
	public enum BookingItemPaymentType
	{
		Unknown = 0,
        OriginalPurchase = 1,
        OptionalFee = 2,
        AdditionalFee = 3,
        Refund = 4,
    }
}
