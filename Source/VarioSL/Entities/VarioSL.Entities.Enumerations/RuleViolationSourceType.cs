﻿namespace VarioSL.Entities.Enumerations
{
	public enum RuleViolationSourceType
	{
		Unknown = 0,
		FareMedia = 1,
		FareProduct = 2,
		FarePayment = 3,
	}
}
