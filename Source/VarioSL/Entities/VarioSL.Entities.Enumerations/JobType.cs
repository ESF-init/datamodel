﻿namespace VarioSL.Entities.Enumerations
{
	public enum JobType
	{
		CardImport = 0,
		CreateReport = 1,
		SendMessage = 2,
		ParticipantImport = 3,
		ParticipantUpdate = 4,
        ParticipantDelete = 5,
		ThirdPartyFareMediaImport = 6,
        BulkSale = 7,
        TransitAccountBlock = 8,
        TransitAccountUnblock = 9,
		TransferProductFromStock = 10,
		TransferProductToStock = 11,
        PromoCodeBlock = 12,
        PromoCodeUnblock = 13,
        PromoCodeImport = 14,
        PromoCodeUpdate = 15,
		ParticipantExport = 16,
		TransitAccountExport = 17,
        ProcessEntitlements = 18,
        OrderFulfillment = 19,
		TransitAccountUnregisterFromInstitution = 20,
		TransitAccountRegisterToInstitution = 21,
		AutoloadImport = 22,
		AutoloadDelete = 23,
        SendNotification = 24,
		CardPrint = 25,
		ContractUpdate = 26, 
		SumInvoicing = 27,
		AutoloadUpdate = 28,
		PrinterCommand = 29
	}
}
