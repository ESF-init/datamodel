﻿
using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum FormSchemaName
	{
		/// <summary>
		/// Unknown Schema
		/// </summary>
		Unknown,

		AccountMailMerge,
		AutoloadMailMerge,
		CardHolderMailMerge,
		ContractMailMerge,
		OrderMailMerge,
		SmartcardMailMerge,

		Smartcard,
		Smartcard_Without_Photo,
		Smartcard_Non_Personalized,
		StudentCard,

		CardOverview,
		DeliveryNote,
		GoodsTransaction,
		PaperMail,
		Receipt,
		Shift,
		Ticket,
		BarcodeBatch,
	}
}
