﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum EncryptedDataType
    {
        AndroidPay = 1,
        ApplePay = 2
    }
}
