﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum TariffReleaseType
    {
        None = 0,
        Test = 1,
        Productive = 2
    }
}
