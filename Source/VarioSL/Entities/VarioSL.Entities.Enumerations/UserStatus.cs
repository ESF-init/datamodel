﻿namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Enumeration of user status.
	/// </summary>
	public enum UserStatus
	{
		/// <summary>
		/// User account is active.
		/// </summary>
		Active = 0,


		/// <summary>
		/// User account has been deleted.
		/// </summary>
		Closed = 1,


        /// <summary>
        /// User account has been blocked. (Assignment VarioUser -> VarioUserGroup denied).
        /// </summary>
        Blocked = 2,
    }
}
