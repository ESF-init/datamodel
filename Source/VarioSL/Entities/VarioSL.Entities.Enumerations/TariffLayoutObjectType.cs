﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum TariffLayoutObjectType
	{
		FixedText = 0,
		SystemFieldText= 1,
		Graphic =2,
		List = 3,
		ConditionalSubPage = 4
	}
}
