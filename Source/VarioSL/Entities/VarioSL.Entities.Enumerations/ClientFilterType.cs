﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available client filter types.
    /// </summary>
	public enum ClientFilterType
	{
        /// <summary>
        /// Client filter type none (will be mapped to database value '0').
        /// </summary>
        None = 0,

        /// <summary>
        /// Client filter type active (will be mapped to database value '1').
        /// </summary>
        Active = 1,

        /// <summary>
        /// Client filter type all (will be mapped to database value '2').
        /// </summary>
        All = 2
	}
}
