﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.Enumerations
{
	public enum TerminationStatus
	{
		/// <summary>
		/// Unknown termination state
		/// </summary>
		Unknown = 0,
		/// <summary>
		/// Termination is completed. 
		/// </summary>
		Final = 1,
		/// <summary>
		/// Termination was withdrawn
		/// </summary>
		Withdrawn = 2,
		/// <summary>
		/// Termination is not finished yet
		/// </summary>
		Pending = 3
	}
}
