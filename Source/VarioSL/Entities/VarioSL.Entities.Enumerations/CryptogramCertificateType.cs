﻿namespace VarioSL.Entities.Enumerations
{
    public enum CryptogramCertificateType
    {
        NMSAMAuthorizationkeys = 1,
        SAMAuthorizationkeys = 2,
        SAMEncryptionKey    = 3,
        SAMSignatureKey = 4,
        KeymanagementActivationkey = 5,
        KeymanagementOrganizationSignatureKey = 6,
        CACertificate
    }
}
