﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available accounting methods.
    /// </summary>
    public enum AccountingMethod
    {
        /// <summary>
        /// Accounting method undefined (will be mapped to database value '0').
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Accounting method partial 2 final 1, 50%-50% part payment, 1 accounting  (will be mapped to database value '1').
        /// </summary>
        Partial_2_Final_1 = 1,

        /// <summary>
        /// Accounting method partial n final 1, 100%/n part payment, 1 accounting (will be mapped to database value '2').
        /// </summary>
        Partial_N_Final_1 = 2,

        /// <summary>
        /// Accounting method partial 0  exact n final 1, no part payment, n peak accounting, 1 accounting (will be mapped to database value '3').
        /// </summary>
        Partial_0_Exact_N_Final_1 = 3,

        /// <summary>
        /// Accounting method payment by installments (will be mapped to database value '3')
        /// </summary>
        Instalment = 4
    }
}
