﻿namespace VarioSL.Entities.Enumerations
{
	public enum ProductType
	{
        /// <summary>
        /// When no product type is needed
        /// </summary>
		None = 0,
        /// <summary>
        /// KVV HomeZone
        /// </summary>
		HomeZone = 1
	}
}
