﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum AssociationType
    {
        Primary = 1,
        LoadOnly = 2
    }
}
