﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Change reason of the cash service component. 
	/// Copied from DataImportDll [initka.vario.tools.dataimport.dll.AM.CashServiceData.ChangeReason]
	/// </summary>
	[NotInDatabase]
	public enum ChangeReason
	{
		Ignore = -1,

		None = 0,

		ComponentChanged = 1,

		CashChanged = 2,
	}
}
