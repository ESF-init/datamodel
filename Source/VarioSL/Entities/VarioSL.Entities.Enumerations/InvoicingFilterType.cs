﻿namespace VarioSL.Entities.Enumerations
{
	public enum InvoicingFilterType
	{
        All = 0,
        Sepa = 1,
        Invoice = 2,
	}
}
