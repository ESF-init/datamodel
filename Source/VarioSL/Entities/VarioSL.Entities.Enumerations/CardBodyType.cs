﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available card body types
    /// </summary>
    public enum CardBodyType
    {
        /// <summary>
        /// Card body type plastic (will be mapped to database value '1').
        /// </summary>
        Plastic = 1,

        /// <summary>
        /// Card body type paper (will be mapped to database value '2').
        /// </summary>
        Paper = 2,

        /// <summary>
        /// Card body type adhesive (will be mapped to database value '3').
        /// </summary>
        Adhesive = 4,

        /// <summary>
        /// Card body type none (will be mapped to database value '4').
        /// </summary>
        None = 8
    }
}
