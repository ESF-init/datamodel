﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum TariffPeriodValidationType
    {
        WhenSelling = 1,
        AtFirstUse = 2,
        ClientDefined = 3
    }
}
