﻿namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Available Card Fulfillment status
	/// </summary>
	public enum CardFulfillmentStatus
	{
		/// <summary>
		/// Status Unknown (will be mapped to database value '0').
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// Status Open (will be mapped to database value '1').
		/// </summary>
		Open = 1,

		/// <summary>
		/// Status Processed (will be mapped to database value '2').
		/// </summary>
		Processed = 2,

		/// <summary>
		/// Status ReplacementRequested (will be mapped to database value '3').
		/// </summary>
		ReplacementRequested = 3,

		/// <summary>
		/// Status Replaced (will be mapped to database value '4').
		/// </summary>
		Replaced = 4
	}
}
