﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available capping states.
    /// </summary>
	public enum CappingState
	{
        /// <summary>
        /// Capping state unknown (will be mapped to database value '0').
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Capping state not applied (will be mapped to database value '1').
        /// </summary>
        NotApplied = 1,

        /// <summary>
        /// Capping state partially applied (will be mapped to database value '2').
        /// </summary>
        PartiallyApplied = 2,

        /// <summary>
        /// Capping state applied (will be mapped to database value '3').
        /// </summary>
        Applied = 3,

        /// <summary>
        /// Capping state out of scope (will be mapped to database value '4').
        /// </summary>
        OutOfScope = 4
	}
}
