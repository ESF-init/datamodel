﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum TariffCappingType
    {
        AmountCappingActive = 1,
        AmountCappingPassive = 2,
        CountCappingActive = 3,
        CountCappingPassive = 4,
        MaxNumberOfTripsPerDay = 5,
        CountCappingPerPeriodUnitActive = 6,
        CountCappingPerPeriodUnitPassive = 7,
    }
}
