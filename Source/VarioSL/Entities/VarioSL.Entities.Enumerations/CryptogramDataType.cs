﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Data type of a resource.
    /// </summary>
    [NotInDatabase]
    public enum CryptogramDataType
    {
        /// <summary>
        /// Resource contains cryptogram data.
        /// </summary>
        Cryptogram = 0,
        
        /// <summary>
        /// Resource contains sign certificate data.
        /// </summary>
        SignCertificate = 1,
        
        /// <summary>
        /// Resource contains sub CA certificate data.
        /// </summary>
        SubCertificate = 2
    }
}
