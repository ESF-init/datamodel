﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Enumeration of transaction journal transaction types
    /// </summary>
    public enum TransactionType
    {
        /// <summary>
        /// Has to be determined by OVS
        /// </summary>
        None = 0,
        /// <summary>
        /// Alight/Checkout
        /// </summary>
        Alight = 65,
        /// <summary>
        /// Checkin with purse
        /// </summary>
        Boarding = 66,
        /// <summary>
        /// Informational tap which uses TicketType.ZeroValueTicket
        /// </summary>
        Statistic = 83,
        /// <summary>
        /// Transfer from a previous boarding
        /// </summary>
        Transfer = 84,
        /// <summary>
        /// Pass use
        /// </summary>
        Use = 85,
        /// <summary>
        /// Topup
        /// </summary>
        Charge = 99,
        /// <summary>
        /// New Product
        /// </summary>
        Load = 108,
        /// <summary>
        /// Cancellation
        /// </summary>
        Cancellation = 128,
        /// <summary>
        /// Tupla Load
        /// </summary>
        TuplaLoad = 260,
        /// <summary>
        /// Client Fare
        /// </summary>
        ClientFare = 261,
        /// <summary>
        /// Credit Card Compensation (Open Loop)
        /// </summary>
        OpenLoopVirtualCharge = 262,
        /// <summary>
        /// Client Selected Product
        /// </summary>
        ClientSelectedProduct = 263,
        /// <summary>
        /// Inspection
        /// </summary>
        Inspection = 7000,
        /// <summary>
        /// Fare media sale
        /// </summary>
        FareMediaSale = 9001,
        /// <summary>
        /// Add purse
        /// </summary>
        AddValueAccount = 9002,
        /// <summary>
        /// Refund transaction
        /// </summary>
        Refund = 9003,
        /// <summary>
        /// Store item transaction
        /// </summary>
        ShopItemSale = 9004,
        /// <summary>
        /// Single ticket sale
        /// </summary>
        SingleTicketSale = 9005,
        /// <summary>
        /// Product adjustment
        /// </summary>
        Adjustment = 9006,
        /// <summary>
        /// Fee for deduction on unused cards
        /// </summary>
        DormancyFee = 9007,
        /// <summary>
        /// Card replacement
        /// </summary>
        CardReplacement = 9008,
        /// <summary>
        /// Transfer balance
        /// </summary>
        BalanceTransfer = 9009,
        /// <summary>
        /// Transfer product
        /// </summary>
        ProductTransfer = 9010,
        /// <summary>
        /// Activate product
        /// </summary>
        ProductActivation = 9011,
        /// <summary>
        /// Payment using stored value on a card
        /// </summary>
        StoredValuePayment = 9012,
        /// <summary>
        /// Refund using stored value on a card
        /// </summary>
        StoredValueRefund = 9013,
        /// <summary>
        /// Transaction of normal (paper) ticket sale
        /// </summary>
        PaperTicket = 9014,
        /// <summary>
        /// Transaction of change receipt
        /// </summary>
        ChangeReceipt = 9015,
        /// <summary>
        /// Transaction of Refund Fee
        /// </summary>
        RefundFee = 9016,
        /// <summary>
        /// External revenue transaction
        /// </summary>
        ExternalRevenue = 9018,
        /// <summary>
        /// Transfer product. Removal transaction. 
        /// </summary>
        ProductTransferRemoval = 9019,
        /// <summary>
        /// Tax transaction where the tax is deducted in a seperate transaction.
        /// </summary>
        Tax = 9020,
        /// <summary>
        /// Product is blocked
        /// </summary>
        ProductBlock = 9021,
        /// <summary>
        /// Product is unblocked
        /// </summary>
        ProductUnBlock = 9022,
        /// <summary>
        /// Serialized Product Item
        /// </summary>
        SerializedProductItem =9023,
        /// <summary>
        /// Institutional Fee
        /// </summary>
        InstitutionalFee = 9024,
        /// <summary>
        /// Dummy (test only)
        /// </summary>
        Dummy = 777777777
    }
}
