﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
	public enum OrganizationSubType
	{
		Unknown = 0,
		ElementarySchool = 1,
		MainSchool = 2,
		MiddleSchool = 3,
		HighSchool = 4,
		ComprehensiveSchool = 5,
		ProfessionalSchool = 6,
		SpecialNeedsSchool = 7,
		Kindergarden = 8,
		PrivatSchool = 9,
		SecondarySchool = 10,
	}
}
