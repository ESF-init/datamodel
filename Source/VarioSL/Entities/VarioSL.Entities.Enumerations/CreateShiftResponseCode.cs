﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available create shift response codes.
    /// </summary>
	[NotInDatabase]
	public enum CreateShiftResponseCode
	{
        /// <summary>
        /// Create shift response code unknown.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Create shift response code created.
        /// </summary>
        Created = 1,

        /// <summary>
        /// Create shift response code open shift found.
        /// </summary>
        OpenShiftFound = 2,

        /// <summary>
        /// Create shift response code device locked.
        /// </summary>
        DeviceLocked = 3,

        /// <summary>
        /// Create shift response code unknown device.
        /// </summary>
        UnknownDevice = 4
	}
}
