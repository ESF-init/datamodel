﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Classifications for clearing house.
	/// </summary>
	public enum ClearingClassification
	{
		OK = 0,
		Corrected = 1,
		Aborted = 10,
		Backup = 11,
		Training = 12,
		Validation = 20,
		BookingState = 30,
        Quarantined = 40,
        Bad = 100
	}
}
