﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum TariffAreaElementType
    {
        ReferenceNumber = 1,
        TariffZone = 2,
        FromTariffPoint = 3,
        ToTariffPoint = 4,
        ViaTariffPoint = 5,
    }
}
