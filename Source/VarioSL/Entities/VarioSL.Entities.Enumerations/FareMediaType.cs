﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Type of cards - NxCardAppType
	/// Attention: The values in this enum must be synced with TDSW (NxCardTypeDef.h)
	/// When adding a new value it must also be implemented in the DDI
	/// </summary>
	/// 
	[NotInDatabase]
	public enum FareMediaType
	{
		UNKNOWN = 0,					// <unbekannt>
		BANK = 1,						// Bankkarte
		WHITE = 2,						// White Card
		MONEY = 3,						// Geldkarte
		CICO = 4,						// CheckInCheckOut
		CASHSAVER = 5,					// Prepaid Zeitkarte
		MULTICARD = 6,					// Multikarte mit Geldkarte und Zeitkarte
		DEBTOR = 100,					// Verkauf
		MAINTENANCE = 101,				// Service
		CONTROLLER = 102,				// Kontroletti
		TLOG = 103,						// Transaction Log
		ALOG = 104,						// Action Log
		TC = 105,						// Transaction Counter
		PASSAUCARD = 200,				// PassauCard
		ALWACARD = 201,					// Allgaeu-Walser-Card
		METROCARD = 202,				// MetroCard ECAN
		VITALESLANDCARD = 203,			// Vitales Land
		ITSOCARD = 204,					// ITSO
		VDVCARD = 205,					// VDVKA Transaktion
		CONNECTCARD = 206,				// Connect card SACOG
		KINCHCARD = 207,				// Kinch card , TrBa
		BYTEMARKBARCODE = 208,			// GWW, als anonyme Karte implemiert
		EASYRIDER = 209,				// NCT
		FARACARD = 210,					// Fara card , Turku
		METROCARD_DF = 211,				// MetroCard ECAN, Desfire
		CORETHREEBARCODE = 212,			// HCC
		WALTTICARD_ID_BASED = 213,		// Waltti ID Based card, Funktion wie faracard
		PADERSPRINTER = 214,			// Paderborn 
		TCARD = 215,					// TriMet (Portland)
		IQPAY_BARCODE = 216,			// Turku, QR-Code Handyticket,
		OPENLOOP = 217,					// Open loop credit card
		REUTLINGENCARD = 218,			// Reutlingen
		EMDER_CARD = 219,				// SVE Emder Card
		IDBARCODE = 220,				// ID based barcode
		HNLEFARE = 221,					// eFare card for Honolulu HNL
		CBBARCODE = 222,				// content based barcode
		STBA_ALIENCARD = 223,			// Fremdkarte STBA
		VCARD_GOOGLE = 224,				// Virtual Card - Google 
		VCARD_APPLE = 225,				// Virtual Card - Apple 
		UIDCARD = 226,					// Third party ID based card identified by UID
		EXTERNALIDBARCODE = 227,		// Third party ID based barcode
		BEECARD = 228,					// Bee card for Rits
		FAMILYCARD = 229,				// Virtual card that stores value which cannot be used for fare payment, but only be transfered to value account of transit accounts of same customer account in system, Turku.
		GIFTCARD = 230,					// Virtual account that stores value which cannot be used for fare payment, but only be used to transfer to value account of transit account in system, Turku.
		CARPARKING = 231,				// Third party ID based card for car parking - Turku
		CQC = 232,						// Corporate Quick Card - LA
		EFARE_LEGACY = 233,				// Legacy/Hybrid Card - ORCA
		// !!!IMPORTANT!!! Do not add new values without talking to TDSW
		MoCa = 300 						// MoCa

	}
}
