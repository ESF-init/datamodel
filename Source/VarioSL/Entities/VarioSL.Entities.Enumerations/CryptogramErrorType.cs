﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Type of the error which occurred when loading a cryptogram to a SAM.
    /// </summary>
    public enum CryptogramErrorType
    {
        /// <summary>
        /// Cryptogram has successfully been loaded (will be mapped to database value '0').
        /// </summary>
        SuccessfullyLoaded = 0,

        /// <summary>
        /// Load key counter of the cryptogram is lower than the current load key counter of the SAM. This means the cryptogram
        /// has already been processed. This code does not tell whether loading the cryptogram was successful or not
        /// (will be mapped to database value '1').
        /// </summary>
        CounterTooLow = 1,

        /// <summary>
        /// Load key number of the cryptogram is higher than the current load key counter of the SAM. Loading the cryptogram has been refused (will be mapped to database value '2').
        /// </summary>
        CounterTooHigh = 2,

        /// <summary>
        /// A SAM commando has failed while loading the cryptogram (reply has not been 0x9000) (will be mapped to database value '3').
        /// </summary>
        CommandoFailed = 3,

        /// <summary>
        /// The sign key attached to the cryptogram is locked. Loading the cryptogram has been refused (will be mapped to database value '4').
        /// </summary>
        SignKeyLocked = 4,

        /// <summary>
        /// Loading the cryptogram failed with an unspecified error. Loading has to be tried again (will be mapped to database value '5').
        /// </summary>
        UnspecifiedError = 5
    }
}