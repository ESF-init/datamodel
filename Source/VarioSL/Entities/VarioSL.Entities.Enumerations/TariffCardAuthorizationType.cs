﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available card authorization types.
    /// </summary>
    [NotInDatabase]
    public enum TariffCardAuthorizationType
    {
        /// <summary>
        /// Card authorization type Transferable (übertragbar).
        /// </summary>
        Transferable = 1,

        /// <summary>
        /// Card authorization type Personalized (personalisiert).
        /// </summary>
        Personalized = 2,
    }
}
