﻿namespace VarioSL.Entities.Enumerations
{
	public enum OrderState
	{
		New = 0,
        Assigned = 1,
		Processed = 2,
		Incomplete = 3,
		Open = 4,
		Failed = 5,
        Cancelled = 6,
		OnHold = 7,
        VerificationRequired = 8,
        ManualVerification = 9,
        Closed = 10,
        Deleted = 11 
	}
}
