﻿
namespace VarioSL.Entities.Enumerations
{
	public enum PaymentState
	{
		Unknown = 0,
		Accepted = 1,
		Rejected = 2,
		Failed = 3,
		InProgress = 4,
        Voided = 5,
        Refunded = 6
	}
}
