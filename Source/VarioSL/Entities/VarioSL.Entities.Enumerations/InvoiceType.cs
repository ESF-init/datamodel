﻿namespace VarioSL.Entities.Enumerations
{
	public enum InvoiceType
	{
		Undefined = 0,
		Partial = 1,
		Final = 2,
		Exact = 3,
		Termination = 4,
		Cancelled = 5,
		Accounting = 6,
		Dunning = 7
	}
}
