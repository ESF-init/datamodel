﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum TariffPeriodCalculationMode
    {
        Default = 0, //Keep existing Period
        Extend = 1, //Extend period with period of new ticket
        Cut = 2 // Cut period to 0
    }
}
