﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available customer account states.
    /// </summary>
	public enum CustomerAccountState
	{
        /// <summary>
        /// Customer account state inactive (will be mapped to database value '0').
        /// </summary>
        Inactive = 0,

        /// <summary>
        /// Customer account state active (will be mapped to database value '1').
        /// </summary>
        Active = 1,

        /// <summary>
        /// Customer account state closed (will be mapped to database value '2').
        /// </summary>
        Closed = 2,

        /// <summary>
        /// Customer account state disabled (will be mapped to database value '3').
        /// </summary>
        Disabled = 3
	}
}
