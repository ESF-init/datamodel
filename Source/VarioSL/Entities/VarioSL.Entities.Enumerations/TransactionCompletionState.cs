﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum TransactionCompletionState
	{
		/// <summary>
		/// Full farepayment in 1 request. No more requests allowed
		/// </summary>
		Reported = 0,
		/// <summary>
		/// First part of transaction (no feedback)
		/// </summary>
		Open = 1,
		/// <summary>
		/// Second part of transaction (feedback)
		/// </summary>
		Completed = 2
	}
}
