﻿namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Available posting key categories.
	/// </summary>
	public enum PostingKeyCategory
	{
		///<summary>
		/// Not Specific (will be mapped to database value '0').
		/// </summary>
		None = 0,

		/// <summary>
		/// PostingKey will be managed in the accounting process (will be mapped to database value '1').
		/// </summary>
		Accounting = 1,

		/// <summary>
		///PostingKey will be managed in the termination process (will be mapped to database value '2').
		/// </summary>
		Termination = 2,

		/// <summary>
		/// PostingKey will be managed in the Dunning process (will be mapped to database value '3').
		/// </summary>
		Dunning = 3,

	}
}
