﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum TariffFareCalculationMode
    {
        None = 0,
        Subtracting = 1,
        //SubtractingNewPeriod = 2, -- Removed. Period calculation was put into its own emnum
        TestSalePrice = 3,
        TestSaleMatrixMax = 4,
        TestSaleMatrixMaxToHigherNumbers = 5,
        TestSaleMatrixMaxToLowerNumbers = 6   
    }
}
