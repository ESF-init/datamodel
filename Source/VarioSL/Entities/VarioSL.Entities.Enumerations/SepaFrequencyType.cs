﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available bank statement record types.
    /// </summary>
	public enum SepaFrequencyType
    {
		Recurrently = 0,
        Single = 1
	}
}
