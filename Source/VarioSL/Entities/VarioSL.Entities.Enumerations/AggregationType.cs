﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available aggregation types.
    /// </summary>
    public enum AggregationType
    {
        /// <summary>
        /// Aggregation type raw (will be mapped to database value '0').
        /// </summary>
        Raw = 0,

        /// <summary>
        /// Aggregation type year (will be mapped to database value '1').
        /// </summary>
        Year = 1,

        /// <summary>
        /// Aggregation type month (will be mapped to database value '2').
        /// </summary>
        Month = 2,

        /// <summary>
        /// Aggregation type day (will be mapped to database value '3').
        /// </summary>
        Day = 3,

        /// <summary>
        /// Aggregation type hour (will be mapped to database value '4').
        /// </summary>
        Hour = 4,
    }
}