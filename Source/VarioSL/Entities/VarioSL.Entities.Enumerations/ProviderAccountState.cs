﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// State of the Regio Move mobility provider.
    /// </summary>
    public enum ProviderAccountState
    {
        /// <summary>
        /// Default value on creation
        /// </summary>
        StartCreating = 0,

        /// <summary>
        /// Provider account is created, may need verification.
        /// </summary>
        Created = 1,

        /// <summary>
        /// Provider account is verified.
        /// </summary>
        Verified = 2,

        /// <summary>
        /// Provider account is inactive.
        /// </summary>
        Inactive = 3,

        /// <summary>
        /// Provider account is active.
        /// </summary>
        Active = 4,

        /// <summary>
        /// An error occured during the creation process.
        /// </summary>
        Failed = 5
    }
}
