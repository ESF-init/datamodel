﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    //Copied from Vario.Classic
    [NotInDatabase]
    public enum BlobType
    {
        UNDEFINED = 0,
        PASSAUCARD_CHECKIN = 1,
        PASSAUCARD_ACTIVATION = 2,
        SVV_SAD = 3,
        ITSO_CHECKIN = 4,
        ID_BASED_TICKETING = 5,
        FEIG_XBZ = 6,
        FEF_BEGIN = 7,     // als Konstanten definiert in SL_???
        FEF_END = 8,       // als Konstanten definiert in SL_???
        FEF_INCIDENT = 9,  // als Konstanten definiert in SL_???
        FEF_PAYMENT = 10,
        TRANSACTION_SIGNATURE = 1000, // Signature for "fiskalisierung"
    }
}