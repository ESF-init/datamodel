﻿namespace VarioSL.Entities.Enumerations
{
    public enum JobState
    {
        Ready = 0,
        Scheduled = 1,
        Running = 2,
        Finished = 3,
		DeletePending = 4,
        Failed = 5
    }
}
