﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Available device types for loading virtual cards
	/// </summary>
    [NotInDatabase]
	public enum VirtualCardDeviceType
	{
		/// <summary>
		/// Unknown
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// Phone
		/// </summary>
		Phone = 1,

		/// <summary>
		/// Watch
		/// </summary>
		Watch = 2,

		/// <summary>
		/// Tablet
		/// </summary>
		Tablet = 3,

		/// <summary>
		/// Other
		/// </summary>
		Other = 99
	}
}
