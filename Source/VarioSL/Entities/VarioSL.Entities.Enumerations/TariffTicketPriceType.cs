﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum TariffTicketPriceType
    {
        Normal = 1,
        Year = 2,
        Month = 3,
		Quarter = 4,
		Halfyear = 5
	}
}
