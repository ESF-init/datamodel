﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Class of attribute to determine which domain it belongs to (e.g. SL_PERSON)
    /// </summary>
    [NotInDatabase]
	public enum CustomAttributeClass
    {
        /// <summary>
        /// Attribute class is unspecified.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Attribute can be assigned to an SL_PERSON entry
        /// </summary>
        SL_Person = 1,
       
	}
}
