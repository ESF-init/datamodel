﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.Enumerations
{
    public enum RentalState
    {
        /// <summary>
        /// Cannont have a Rental .
        /// </summary>
        None = 0,

        /// <summary>
        /// Rental is inactive.
        /// </summary>
        Inactive = 1,

        /// <summary>
        /// Rental is still active.
        /// </summary>
        Active = 2,

        /// <summary>
        /// Rental is paused.
        /// </summary>
        Paused = 3,

        /// <summary>
        /// Rental is finished.
        /// </summary>
        Finished = 4
    }
}
