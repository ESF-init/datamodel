﻿using System;
using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	[Flags]
	public enum ClearingResultLevel
	{
		Totals = 0,
		Client = 1,
		DeviceClass = 2,
		TransactionType = 4,
		LineGroup = 8,
		Line = 16,
		CardIssuer = 32,
		PaymentMethod = 64,
		DebtorId = 128,
		TicketInternalNumber = 256,
        CustomerGroup = 512,
	    PTOMUnit = 1024,
	    TripCode = 2048,
	    ContractorId = 4096,
	    TripDateTime = 8192, 
	    IsCancellation = 16384,
    }
}
