﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum TariffLayoutType
	{
		Print = 1,
		Cancellation = 2,
		Receipt = 3
	}
}
