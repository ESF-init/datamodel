﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Types of Apportionment for accounting.
    /// </summary>
    [NotInDatabase]
    public enum SettlementPolicy
    {
        /// <summary>
        /// Apportionment based on product usage.
        /// </summary>
		Usage = 1,

        /// <summary>
        /// Fixed price (percentage) apportionment.
        /// </summary>
        Percentage = 2
    }
}
