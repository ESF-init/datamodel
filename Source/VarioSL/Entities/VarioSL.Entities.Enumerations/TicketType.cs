﻿using System.ComponentModel;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// If you add a new Ticket Type, please update test AutoloadSettingEntityTests.IsTopUpAutoload_TickedTypeEnumCountxx
    /// </summary>
    public enum TicketType
    {
        NormalTicket = 1,
        ZeroValueTicket = 2,
        Pass = 3,
        Punch = 4,
        Mango = 5,
        CashSaver = 6,
        DriverIssued = 7,
        [Description("VarioClassic:CASH_SAVER_YELLOW_RENEWAL")]
        TopUp = 8,
        [Description("VarioClassic:CASH_SAVER_BLUE_RENEWAL")]
        RefundPurse = 9,
        [Description("VarioClassic:CASH_SAVER_RED_RENEWAL")]
        CashSaverRedRenewal = 10,
        ShopItem = 11,
        Rebate = 12,
        Rebate_RBQ = 13,
        ChangeReceipt = 18,
        Refund = 19,
        ReplacementFee = 20,
        PayoutNonSalesRelevant = 29,
        PhysicalCard = 103,
        Goodwill = 104,
        Capping = 105,
        PenaltyFare = 106,
        AdminFee = 107,
        DormancyFee = 108,
        Purse = 200,
        PursePreTax = 201,
        Deposit = 202,
        Supplement = 203,
        MixedGroup = 204,
        /// <summary>
        /// Passes for a Group. Upon Sale the Group size is defined.
        /// </summary>
        DynamicNumberOfPersonsPass = 205,
        WhitelistOutdatedFallback = 206,
        PaperTicketPass = 207,
        CheckInCheckOut = 208,
        ComboTicket = 209,
        CardRelatedShopItem = 210,
        /// <summary>
        /// A pass which gives the holder a credit for each transaction they perform. The value is defined in TM_TICKET.VALUEPASSCREDIT and maintained in the Tariff Management. Needs to be assigned to a card ticket (PhysicalCard=103)
        /// </summary>
        ValuePass = 211,
        /// <summary>
        /// Fare evasion compensation.
        /// </summary>
        FareEvasionPayment = 212,
        /// <summary>
        /// Purse which can be used for a certain amount of time until it is being balanced. Values normally become negative. At the end of the defined period a service balances the invoiced purse.
        /// </summary>
        InvoicedPurse = 213,
        /// <summary>
        /// A fee which applies on refund.
        /// </summary>
        RefundFee = 214,
        /// <summary>
        /// Assigned to tickets which are placeholders for external revenue
        /// </summary>
        ExternalRevenue = 215,
        /// <summary>
        /// Purchase a serialized product (i.e. shop item) where the serialized number is typed in upon time of sale on client side (i.e. on CSW).
        /// </summary>
        SerializedProductClient = 216,
        /// <summary>
        /// Purchase a serialized product (i.e. shop item) where the serialized number is defined by the system within the sale process. Client can request the serialized number(s) of the sale after sale is completed (i.e. on CSW).
        /// </summary>
        SerializedProductGenerated = 217,
        /// <summary>
        /// A special ticket for sales where the tax is deducted in a seperate transaction with a ticket of this type.
        /// </summary>
        Tax = 218,
        /// <summary>
        /// A special ticket for institutional programs fees including revenue recognition.
        /// </summary>
        InstitutionalFee = 219

    }
}
