﻿
namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available credit card networks.
    /// </summary>
	public enum CreditCardNetwork
	{
        /// <summary>
        /// Credit card network visa (will be mapped to database value '0').
        /// </summary>
        Visa = 0,

        /// <summary>
        /// Credit card network master card (will be mapped to database value '1').
        /// </summary>
        MasterCard = 1,

        /// <summary>
        /// Credit card network american express (will be mapped to database value '2').
        /// </summary>
        AmericanExpress = 2,

        /// <summary>
        /// Credit card network discover (will be mapped to database value '3').
        /// </summary>
        Discover = 3,

        /// <summary>
        /// Credit card network Maestro (will be mapped to database value '4').
        /// </summary>
        Maestro = 4,

        /// <summary>
        /// Credit card network Jcb (will be mapped to database value '5').
        /// </summary>
        Jcb = 5,

        /// <summary>
        /// Credit card network Diners (will be mapped to database value '6').
        /// </summary>
        Diners = 6,

        /// <summary>
        /// Credit card network CarteBleue (will be mapped to database value '7').
        /// </summary>
        CarteBleue = 7,

        /// <summary>
        /// Credit card network CarteBlanc (will be mapped to database value '8').
        /// </summary>
        CarteBlanc = 8,

        /// <summary>
        /// Credit card network Voyager (will be mapped to database value '9').
        /// </summary>
        Voyager = 9,

        /// <summary>
        /// Credit card network Wex (will be mapped to database value '10').
        /// </summary>
        Wex = 10,

        /// <summary>
        /// Credit card network UnionPay (will be mapped to database value '11').
        /// </summary>
        UnionPay = 11,

        /// <summary>
        /// Credit card network Style (will be mapped to database value '12').
        /// </summary>
        Style = 12,

        /// <summary>
        /// Credit card network ValueLink (will be mapped to database value '13').
        /// </summary>
        ValueLink = 13,

        /// <summary>
        /// Credit card network Interac (will be mapped to database value '14').
        /// </summary>
        Interac = 14,

        /// <summary>
        /// Credit card network Laser (will be mapped to database value '15').
        /// </summary>
        Laser = 15


    }
}
