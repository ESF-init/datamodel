﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available content types.
    /// </summary>
    public enum ContentType
    {
        /// <summary>
        /// Content type page (will be mapped to database value '0').
        /// </summary>
        Page = 0,

        /// <summary>
        /// Content type page item (will be mapped to database value '1').
        /// </summary>
        Item = 1,

        /// <summary>
        /// Content type page list (will be mapped to database value '2').
        /// </summary>
        List = 2
    }
}
