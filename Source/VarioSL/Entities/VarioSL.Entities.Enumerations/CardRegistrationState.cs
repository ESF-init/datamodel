﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available card registration states.
    /// </summary>
	[NotInDatabase]
	public enum CardRegistrationState
	{
        /// <summary>
        /// Card registration state not registered.
        /// </summary>
        NotRegistered = 0,

        /// <summary>
        /// Card registration state registered.
        /// </summary>
        Registered = 1,

        /// <summary>
        /// Card registration state payment option.
        /// </summary>
        PaymentOption = 2,

        /// <summary>
        /// Card registration state registered with payment option.
        /// </summary>
        RegisteredWithPaymentOption = 3,
	}
}
