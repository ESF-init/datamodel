﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Actions has to be done for dunning levels.
    /// </summary>
    public enum DunningAction
    {
		///<summary>
		/// No Action has to be achieved.
		/// </summary>
		None = 0,

		/// <summary>
		/// The Product has to be closed.
		/// </summary>
		CloseContract = 1,

		/// <summary>
		/// The Process should be sent to the Inkasso Office.
		/// </summary>
		Inkasso = 2,

		/// <summary>
		/// The product validy will be changed
		/// </summary>
		TemporaryTermination = 3
	}
}
