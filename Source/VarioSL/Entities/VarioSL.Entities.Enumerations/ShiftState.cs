﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum ShiftState
    {
        /// <summary>
        /// Original shift.
        /// </summary>
        Original = 0,

        /// <summary>
        /// Shift created from backup file.
        /// </summary>
        OriginalFromBackup = 1,

        /// <summary>
        /// Shift edited later.
        /// </summary>
        Edited = 2,

        /// <summary>
        /// Shift imported.
        /// </summary>
        Imported = 3,

        /// <summary>
        /// Backup shift.
        /// </summary>
        Backup = 10,

        /// <summary>
        /// Shift broken.
        /// </summary>
        Broken = 20,

        /// <summary>
        /// Training shift.
        /// </summary>
        Training = 30,

        /// <summary>
        /// Realtime shift created and still open.
        /// </summary>
        Open = 40,

        /// <summary>
        /// Realtime shift closed but not processed.
        /// </summary>
        Pending = 41,

        /// <summary>
        ///  Invalid shift.
        /// </summary>
        Invalid = 99
    }
}
