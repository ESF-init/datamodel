﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available card event types.
    /// </summary>
	public enum CardEventType
	{
        /// <summary>
        /// Card event type import (will be mapped to database value '0').
        /// </summary>
        Import = 0,

        /// <summary>
        /// Card event type transfer (will be mapped to database value '1').
        /// </summary>
        Transfer = 1,

        /// <summary>
        /// Card event type issue (will be mapped to database value '2').
        /// </summary>
        Issue = 2,

        /// <summary>
        /// Card event type assign (will be mapped to database value '3').
        /// </summary>
        Assign = 3,

        /// <summary>
        /// Card event type unassign (will be mapped to database value '4').
        /// </summary>
        Unassign = 4,

        /// <summary>
        /// Card event type block (will be mapped to database value '5').
        /// </summary>
        Block = 5,

        /// <summary>
        /// Card event type unblock (will be mapped to database value '6').
        /// </summary>
        Unblock = 6,

        /// <summary>
        /// Card event type replace (will be mapped to database value '7').
        /// </summary>
        Replace = 7,

        /// <summary>
        /// Card event type share (will be mapped to database value '8').
        /// </summary>
        Share = 8,

        /// <summary>
        /// Card event type unshare (will be mapped to database value '9').
        /// </summary>
        Unshare = 9,

        /// <summary>
        /// Card event type service comment (will be mapped to database value '10').
        /// </summary>
        ServiceComment = 10,

        /// <summary>
        /// Card event type product modification (will be mapped to database value '11').
        /// </summary>
        ProductModification = 11,

        /// <summary>
        /// Card event type dormancy (will be mapped to database value '12').
        /// </summary>
        Dormancy = 12,

        /// <summary>
        /// Card event type expiration (will be mapped to database value '13').
        /// </summary>
        Expiration = 13,

        /// <summary>
        /// Card event type auto load (will be mapped to database value '14').
        /// </summary>
        Autoload = 14,

        /// <summary>
        /// Card event type revoke (will be mapped to database value '15').
        /// </summary>
        Revoke = 15,

        /// <summary>
        /// Card event type deposit (will be mapped to database value '16').
        /// </summary>
        Deposit = 16,

        /// <summary>
        /// Card event type CardHolder (will be mapped to database value '17').
        /// </summary>
        CardHolder = 17,

        /// <summary>
        /// Card event type Participant (will be mapped to database value '18').
        /// </summary>
        Participant = 18,

		/// <summary>
		/// Card event type Presented (will be mapped to database value '19').
		/// </summary>
		Presentation = 19,

		/// <summary>
		/// Card event type ResetReplacementCounter (will be mapped to database value '20').
		/// </summary>
		ResetReplacementCounter = 20,

		/// <summary>
		/// Card event type ResetInactivity (will be mapped to database value '21').
		/// </summary>
		ResetInactivity = 21,

		/// <summary>
		/// Card event type BalanceAdjustment (will be mapped to database value '22').
		/// </summary>
		BalanceAdjustment = 22
	}
}
