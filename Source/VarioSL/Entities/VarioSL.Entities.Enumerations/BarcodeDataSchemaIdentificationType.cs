﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum BarcodeDataSchemaIdentificationType
	{
		ContentBased = 1,
		IdBasedStaticRecord = 2,
		IdBasedTransientRecord = 3
	}
}
