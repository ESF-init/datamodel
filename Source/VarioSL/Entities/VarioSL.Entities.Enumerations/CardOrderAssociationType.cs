﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum CardOrderAssociationType
	{
		/// <summary>
		/// Association is Unknown
		/// </summary>
		Unknown = 0,
		/// <summary>
		/// Association is Primary
		/// </summary>
		Primary = 1,
		/// <summary>
		/// Association is LoadOnly
		/// </summary>
		LoadOnly = 2,
		/// <summary>
		/// Association is Anonymous
		/// </summary>
		Anonymous = 3
	}
}
