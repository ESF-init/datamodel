﻿
namespace VarioSL.Entities.Enumerations
{
    public enum CertificateType
    {
        /// <summary>
        /// Internal will be mapped to database value '0'.
        /// </summary>
        Internal = 0,

        /// <summary>
        /// Banking will be mapped to database value '1'.
        /// </summary>
        Banking = 1,

        /// <summary>
        /// Barcode2DPrivateKey is included on Q-Archive will be mapped to database value '2'.
        /// </summary>
        Barcode2DPrivateKey = 2,

        /// <summary>
        /// Barcode2DPublicKey is included on Q-Archive will be mapped to database value '3'.
        /// </summary>
        Barcode2DPublicKey = 3,

        /// <summary>
        /// APIBarcodeSalePrivateKey will be mapped to database value '4'.
        /// </summary>
        APIBarcodeSalePrivateKey = 4,

        /// <summary>
        /// APIBarcodeSalePublicKey will be mapped to database value '5'.
        /// </summary>
        APIBarcodeSalePublicKey = 5,

        /// <summary>
        /// APIMobileAppBarcodeSalePrivateKey will be mapped to database value '6'.
        /// </summary>
        APIMobileAppBarcodeSalePrivateKey = 6,

        /// <summary>
        /// APIBarcodePassphrase will be mapped to database value '7'.
        /// </summary>
        APIBarcodePassphrase = 7,
    }
}
