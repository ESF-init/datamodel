﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum TariffParameterType
    {
        NX_TAPAR_TICKETVALIDTIME = 1,  // validity for tickets (duration in s)
        NX_TAPAR_CREDITWARNLIMIT = 2,  // 'Credit to low' warning threshold (cent)
        NX_TAPAR_CHARGEDISCOUNT = 3,  // Discount for card charging (percentage)
        NX_TAPAR_CAPPINGPERCENTAGE = 4,  // Discount for capping rule (percentage)
        NX_TAPAR_TARIFFSYSTEM = 5,  // tariff system (MTS: 1 = urban, 2 = regional)
        NX_TAPAR_FULLFARETRIP = 6,  // etiKey for full fare ticket (fallback when discount expires)
        NX_TAPAR_PASSAUCARD_SID = 7,  // Service-ID for PassauCard
        NX_TAPAR_AWLACARD_SID = 8,  // Service-ID for Alwacard
        NX_TAPAR_DATAVERSION = 9,  // data model version number (project specific)
        NX_TAPAR_ECANMETROCARD_SID = 10, // Service-Id of DefaultTicket for ECAN Metrocard
        NX_TAPAR_PROX_SERVICEPROVIDERID = 11, // PROXCARD Service Provider Id
        NX_TAPAR_TICKETKEYPANELCONFIG = 12, // ticket key panel config string (see NX_TICKETKEYPANELCONFIG... constants)
        NX_TAPAR_VALIDATEID_SID = 13, // Service-ID of CI-Ticket for ValidateId
        NX_TAPAR_ITSOCONFARE_SID = 14, // Sercice_ID for Itso Concessionary-Fare
        NX_TAPAR_VDVKATXDATA_SID = 15, // Sercice_ID for Vdvka-CiCo-Datensaetze
        NX_TAPAR_MINAGE_ADULT = 16, // Minimum age for Adult
        NX_TAPAR_MINAGE_SENIOR = 17, // Minimum age for Senior
        NX_TAPAR_CICO_UPDATECARD = 18, // Update CICO-Card
        NX_TAPAR_BYTEMARK_SID = 19, // Service-Id for Bytemark barcode 
        NX_TAPAR_MAX,

        //Vario
        VARIO_EMV_INITIAL_AUTHORIZATION_AMOUNT = 1001, //Amount of the authorization sought from the card issuer to start a new aggregation period (places a hold for this amount on customer’s account). At least the negative purse balance will be authorized..
        VARIO_EMV_AGGREGATION_VALUE_LIMIT = 1002, //Value equal to the sum of aggregated payments where the aggregation period is closed and the final payment is processed. Before that a new validation period is being opened. + Any single day’s transaction that exceeds this amount must be authorised online at the end of the day
        VARIO_EMV_AGGREGATION_TIME_LIMIT = 1003, //Time following the start of a new aggregation period when the aggregation period is closed and the final payment is processed
        VARIO_PASSBACK_TIME_PASS = 1004, //Time within a second tap is being recognized as passback for passes. ATTENTION: Devices must also be provided with this setting via parameter management.
        VARIO_PASSBACK_TIME_AUTO = 1005, //Time within a second tap is being recognized as passback for check-ins in CiCo scenarios.
        VARIO_EMV_MAXIMUM_TIME_BETWEEN_ONLINE_AUTHORISATIONS = 1006, // When a card is used and the time since the last online authorisation exceeds this limit, the day’s transaction must be authorised online.
        VARIO_EMV_CUMULATIVE_OFFLINE_TRANSIT_AMOUNT_LIMIT = 1008, // The full value of the daily transaction that caused the cumulative total spend on a card to exceed this limit must be authorised online.
        VARIO_EMV_TRANSIT_DAILY_TRANSACTION_CHARGEBACK_THRESHOLD = 1009, //European issuers may not charge back transactions below this value, even if they declined the authorisation.
        VARIO_TIME_ALLOW_ADDITIONAL_RIDER = 1010, // Time after trip where another tap allows additional riders (defined by group size in ticket)
        VARIO_OVS_TRANSACTION_MAX_AGE_HOURS = 1011 // Hours after which a transaction will not be applied by the OVS anymore. Error transactions will be written instead.
    }
}
