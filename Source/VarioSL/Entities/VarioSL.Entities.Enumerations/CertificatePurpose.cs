﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Purpose for which the certificate is used.
    /// </summary>
    public enum CertificatePurpose
    {
        ///<summary>
        /// The certificate is a vdvka root certificate (will be mapped to database value '0').
        /// </summary>
        VdvkaRoot = 0,

        /// <summary>
        /// The certificate is a vdvka sub certificate (will be mapped to database value '1').
        /// </summary>
        VdvkaSubCa = 1,

        /// <summary>
        /// The certificate is a vdvka SAM sign certificate (will be mapped to database value '2').
        /// </summary>
        VdvkaSamSign = 2,

        /// <summary>
        /// The certificate is used for uic content and is self signing (will be mapped to database value '3').
        /// </summary>
        UicSelfSign = 3,

        /// <summary>
        /// The certificate is a vdvka Operator Activation Key certificate (will be mapped to database value '4').
        /// </summary>
        OperatorActivationKey = 4
	}
}
