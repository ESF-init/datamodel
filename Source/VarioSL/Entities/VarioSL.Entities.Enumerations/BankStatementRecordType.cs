﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available bank statement record types.
    /// </summary>
	public enum BankStatementRecordType
	{
        /// <summary>
        /// Bank statement record type payment (will be mapped to database value '0').
        /// </summary>
		Payment = 0,

        /// <summary>
        /// Bank statement record type charge back (will be mapped to database value '1').
        /// </summary>
        Chargeback = 1
	}
}
