﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Used for the promocodes
    /// can be percentage or absolute
    /// </summary>
    [NotInDatabase]
    public enum DiscountType
    {
        /// <summary>
        /// If Absolute the discount price is what is being deducted
        /// </summary>
        Absolute = 0,

        /// <summary>
        /// If Percentage the discount price is x % reduced.
        /// </summary>
        Percentage = 1,
      
        
    }
}
