﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available credit card completion types.
    /// </summary>
    [NotInDatabase]
    public enum CreditCardCompletionType
    {
        /// <summary>
        /// Credit card completion type none.
        /// </summary>
        None = 0,

        /// <summary>
        /// Credit card completion type offline.
        /// </summary>
        Offline = 1,

        /// <summary>
        /// Credit card completion type online.
        /// </summary>
        Online = 2
    }
}
