﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available address types.
    /// </summary>
    public enum AddressType
    {
        /// <summary>
        /// Address type invoice address (will be mapped to database value '0').
        /// </summary>
        InvoiceAddress = 0,

        /// <summary>
        /// Address type shipping address (will be mapped to database value '1').
        /// </summary>
        ShippingAddress = 1,

        /// <summary>
        /// Address type correspondence address (will be mapped to database value '2').
        /// </summary>
        CorrespondenceAddress = 2,

        /// <summary>
        /// Address type person address (will be mapped to database value '3').
        /// </summary>
		PersonAddress = 3,

        /// <summary>
        /// Address type person alternative address (will be mapped to database value '4').
        /// </summary>
		PersonAlternativeAddress = 4,

		/// <summary>
		/// Address type ContractAddress (will be mapped to database value '5').
		/// </summary>
		ContractAddress = 5,
        
        /// <summary>
        /// Address type ContractAddress (will be mapped to database value '5').
        /// </summary>
        OrganizationLocation = 6
    }
}
