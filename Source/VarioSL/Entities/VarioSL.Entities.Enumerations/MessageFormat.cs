﻿namespace VarioSL.Entities.Enumerations
{
    public enum MessageFormat
    {
        HTML = 1,
        Text = 2
    }
}
