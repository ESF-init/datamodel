﻿namespace VarioSL.Entities.Enumerations
{
	public enum VarioTypeOfSettlement
	{
		Unknown = 0,
		Driver = 1,
		Cash = 2
	}
}
