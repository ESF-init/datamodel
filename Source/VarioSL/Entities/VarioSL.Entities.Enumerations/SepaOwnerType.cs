﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available bank statement record types.
    /// </summary>
	public enum SepaOwnerType
    {
		PrivateAccount = 0,
        CompanyAccount = 1
	}
}
