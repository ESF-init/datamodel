namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Enumeration of all valid ticket assignment statuses.
    /// </summary>
    public enum TicketAssignmentStatus
    {
        /// <summary>
        /// Ticket assignment is unassigned.
        /// </summary>
        Unassigned = 0,

        /// <summary>
        /// Ticket assignment is assigned.
        /// </summary>
        Active = 1,

        /// <summary>
        /// Ticket assignment is on hold.
        /// </summary>
        Hold = 2,

        /// <summary>
        /// Ticket assignment is blocked.
        /// </summary>
        Blocked = 3,

        /// <summary>
        /// Ticket assignment has been cancelled.
        /// </summary>
        Cancelled = 4,

		/// <summary>
		/// Ticket assignment is expired.
		/// </summary>
		Expired = 5
	}
}
