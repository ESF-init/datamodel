﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Available Priorities 
	/// </summary>
	public enum Priority
	{
		/// <summary>
		/// Priority Unknown (will be mapped to database value '0').
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// Priority Lowest (will be mapped to database value '10').
		/// </summary>
		Lowest = 10,

		/// <summary>
		/// Priority Low (will be mapped to database value '20').
		/// </summary>
		Low = 20,

		/// <summary>
		/// Priority Normal (will be mapped to database value '30').
		/// </summary>
		Normal = 30,

		/// <summary>
		/// Priority High (will be mapped to database value '40').
		/// </summary>
		High = 40,

		/// <summary>
		/// Priority Urgent (will be mapped to database value '50').
		/// </summary>
		Urgent = 50,

		/// <summary>
		/// Priority Immediate (will be mapped to database value '60').
		/// </summary>
		Immediate = 60
	}
}
