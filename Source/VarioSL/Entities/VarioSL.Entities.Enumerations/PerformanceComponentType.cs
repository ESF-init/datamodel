﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum PerformanceComponentType
	{
		Whitelist = 1,
		FarePayment = 2,
		Api = 3,
		FareInspection = 4
	}
}
