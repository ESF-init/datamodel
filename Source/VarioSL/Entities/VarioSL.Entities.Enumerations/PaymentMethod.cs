﻿
using System;

namespace VarioSL.Entities.Enumerations
{
    [Obsolete("This enum should be replaced with DevicePaymentMethod.")]
    public enum PaymentMethod
    {
        DebitCard = 0,
        CreditCard = 1,
        ACH = 264,
        SEPA = 269,
    }
}
