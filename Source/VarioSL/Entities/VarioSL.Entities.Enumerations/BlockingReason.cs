﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available blocking reasons.
    /// </summary>
    public enum BlockingReason
    {
        /// <summary>
        /// Blocking reason none (will be mapped to database value '0').
        /// </summary>
        None = 0,

        /// <summary>
        /// Blocking reason lost (will be mapped to database value '1').
        /// </summary>
        Lost = 1,

        /// <summary>
        /// Blocking reason stolen (will be mapped to database value '2').
        /// </summary>
        Stolen = 2,

        /// <summary>
        /// Blocking reason receivable (will be mapped to database value '3').
        /// </summary>
        Receivable = 3,

        /// <summary>
        /// Blocking reason defective (will be mapped to database value '4').
        /// </summary>
        Defective = 4,

        /// <summary>
        /// Blocking reason revoked (will be mapped to database value '5').
        /// </summary>
        Revoked = 5,

        /// <summary>
        /// Blocking reason lost in shipping (will be mapped to database value '6').
        /// </summary>
        LostInShipping = 6,

        /// <summary>
        /// Blocking reason fraudulent use (will be mapped to database value '7').
        /// </summary>
        FraudulentUse = 7,

        /// <summary>
        /// Blocking reason no product (will be mapped to database value '8').
        /// </summary>
        NoProduct = 8,

        /// <summary>
        /// Blocking reason three wrong pin attempts (will be mapped to database value '9').
        /// </summary>
        ThreeWrongPinAttempts = 9,

		/// <summary>
		/// Blocking reason Order Fulfillment Replacement (will be mapped to database value '10').
		/// </summary>
		OrderFulfillmentReplacement = 10
	}
}
