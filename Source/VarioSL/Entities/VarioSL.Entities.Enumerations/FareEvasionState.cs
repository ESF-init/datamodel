﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available fare evasion states.
    /// </summary>
	public enum FareEvasionState
	{
        /// <summary>
        /// Fare evasion state open (will be mapped to database value '0').
        /// </summary>
        Open = 0,

        /// <summary>
        /// Fare evasion state in work (will be mapped to database value '1').
        /// </summary>
        InWork = 1,

        /// <summary>
        /// Fare evasion state completed payed (will be mapped to database value '2').
        /// </summary>
        CompletedPayed = 2,

        /// <summary>
        /// Fare evasion state completed non payed (will be mapped to database value '3').
        /// </summary>
        CompletedNonPayed = 3,

        /// <summary>
        /// Fare evasion state info letter (will be mapped to database value '4').
        /// </summary>
        InfoLetter = 4,

        /// <summary>
        /// Fare evasion state first dunning minor (will be mapped to database value '5').
        /// </summary>
        FirstDunningMinor = 5,

        /// <summary>
        /// Fare evasion state second dunning minor (will be mapped to database value '6').
        /// </summary>
        SecondDunningMinor = 6,

        /// <summary>
        /// Fare evasion state dunning full age (will be mapped to database value '7').
        /// </summary>
        DunningFullAge = 7,

        /// <summary>
        /// Fare evasion state completed encashment (will be mapped to database value '8').
        /// </summary>
        CompletedEncashment = 8,

	    /// <summary>
	    /// Fare evasion state error (will be mapped to database value '9').
	    /// </summary>
        Error = 9,

        /// <summary>
        /// Fare evasion state inactive (will be mapped to database value '10').
        /// </summary>
        Inactive = 10
	}
}