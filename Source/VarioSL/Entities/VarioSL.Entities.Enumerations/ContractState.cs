﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available contract states.
    /// </summary>
	public enum ContractState
	{
        /// <summary>
        /// Contract state inactive (will be mapped to database value '0').
        /// </summary>
        Inactive = 0,

        /// <summary>
        /// Contract state active (will be mapped to database value '1').
        /// </summary>
        Active = 1,

        /// <summary>
        /// Contract state closed (will be mapped to database value '2').
        /// </summary>
        Closed = 2,

        /// <summary>
        /// Contract state suspended (will be mapped to database value '3').
        /// </summary>
        Suspended = 3,

        /// <summary>
        /// Contract state anonymized (will be mapped to database value '4').
        /// </summary>
        Anonymized = 4
    }
}
