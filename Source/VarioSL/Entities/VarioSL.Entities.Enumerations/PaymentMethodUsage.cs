﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Payment Method Usage
    /// </summary>
    [NotInDatabase]
    [Flags]
    public enum PaymentMethodUsage
    {
        Unknown = 0,
        Sale = 1,
        Refund = 2,
        Adjustment = 4
    }
}
