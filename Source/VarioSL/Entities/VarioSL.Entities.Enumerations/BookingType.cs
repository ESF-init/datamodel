﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Represents the state of a Regio Move booking.
    /// </summary>
    public enum BookingType
    {
        /// <summary>
        /// Price request.
        /// </summary>
        PriceRequest = 0,

        /// <summary>
        /// Default value on creation.
        /// </summary>
        Created = 1,

        /// <summary>
        /// External booking created, waiting for confirmation
        /// </summary>
        Waiting = 2,

        /// <summary>
        /// Booking is Active.
        /// </summary>
        Active = 3,

        /// <summary>
        /// Reservation got canceled.
        /// </summary>
        Canceled = 4,

        /// <summary>
        /// Booking is cleared.
        /// </summary>
        Cleared = 5,

        /// <summary>
        /// An error occured during the booking process.
        /// </summary>
        Failed = 6,
    }
}
