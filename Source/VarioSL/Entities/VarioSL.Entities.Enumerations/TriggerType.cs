﻿
namespace VarioSL.Entities.Enumerations
{
    public enum TriggerType
    {
        Interval = 0,
        Threshold = 1
    }
}
