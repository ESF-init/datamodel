﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available archive types.
    /// </summary>
    [NotInDatabase]
    public enum ArchiveType
    {
        /// <summary>
        /// The archive contains cryptogram data.
        /// </summary>
        Cryptogram = 0,
        
        /// <summary>
        /// The archive contains long living data and is called Q archive.
        /// </summary>
        Q = 1
    }
}
