﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Enumeration of media eligibility requirements. Meet at least one criteria to fulfill media eligibility (OR logic).
    /// </summary>
    public enum MediaEligibilityRequirement
    {
        /// <summary>
        /// No Proof of Eligibility required
        /// </summary>
        None = 0,
        /// <summary>
        /// Proof of Eligibility required (documents unspecified)
        /// </summary>
        ProofOfEligibilityRequired = 1,
        /// <summary>
        /// Driver License required
        /// </summary>
        DriverLicenseDocumentRequired = 2,
        /// <summary>
        /// Identity required
        /// </summary>
        IdentityRequired = 4
    }
}
