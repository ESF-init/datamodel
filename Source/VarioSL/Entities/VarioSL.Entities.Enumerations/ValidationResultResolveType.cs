﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Ways a failed validation check can be resolved.
    /// </summary>
    public enum ValidationResultResolveType
	{
		/// <summary>
		/// Resolved by an automated process.
		/// </summary>
		AutoResolved = 0,

		/// <summary>
		/// Resolved by user intervention.
		/// </summary>
		ByUser = 1
    }
}
