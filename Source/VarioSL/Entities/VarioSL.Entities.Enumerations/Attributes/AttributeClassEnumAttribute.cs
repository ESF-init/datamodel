using System;

namespace VarioSL.Entities.Enumerations.Attributes
{
    public sealed class AttributeClassEnumAttribute : Attribute
    {
        public Type EnumType { get; private set; }
        public AttributeClassEnumAttribute(Type enumType)
        {
            if (!enumType.IsEnum)
                throw new ArgumentException(String.Format("Supplied type must be an Enum.  Type was {0}", enumType.ToString()));

            EnumType = enumType;
        }
    }
}
