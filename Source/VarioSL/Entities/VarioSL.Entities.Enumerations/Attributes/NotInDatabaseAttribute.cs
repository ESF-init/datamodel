﻿using System;

namespace VarioSL.Entities.Enumerations.Attributes
{
	[AttributeUsage(AttributeTargets.Enum)]
	public sealed class NotInDatabaseAttribute : Attribute
	{
	}
}
