﻿namespace VarioSL.Entities.Enumerations
{
    public enum JobFileType
    {
        PDF = 0,
        Excel = 1,
        Word = 2,
        XML = 3,
		CSV = 4,
		Report = 5
    }
}
