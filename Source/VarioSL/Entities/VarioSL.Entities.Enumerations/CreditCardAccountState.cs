﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available credit card account states.
    /// </summary>
    [NotInDatabase]
    public enum CreditCardAccountState
    {
        /// <summary>
        /// Credit card account state unknown.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Credit card account state verified.
        /// </summary>
        Verified = 1,

        /// <summary>
        /// Credit card account state declined.
        /// </summary>
        Declined = 2
    }
}
