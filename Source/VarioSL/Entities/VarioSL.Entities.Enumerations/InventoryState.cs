﻿namespace VarioSL.Entities.Enumerations
{
    public enum InventoryState
    {
        InInventory = 0,
        Delivered = 1,
        Issued = 2,
        Canceled = 3,
		Revoked = 4,
		ToBePrinted =5,
		Exchanged = 6
	}
}
