﻿namespace VarioSL.Entities.Enumerations
{
    public enum PromoCodeStatus
    {
        /// <summary>
        /// Promotional code is active.
        /// </summary>
        Active = 0,

        /// <summary>
        /// Promotional code is cancelled.
        /// </summary>
        Cancelled = 1,

        /// <summary>
        /// Promotional code is disabled.
        /// </summary>
        Disabled = 2,
    }
}
