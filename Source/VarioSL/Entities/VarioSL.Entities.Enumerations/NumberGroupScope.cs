﻿namespace VarioSL.Entities.Enumerations
{
    public enum NumberGroupScope
    {
        Order = 0,
        Contract = 1,
        InvoiceReference = 2,
        ShortCode = 3,
        SalesReference = 4,
        STAN = 5,
        BarcodeTransitAccount = 6,
        BarCode = 7,
        OrganizationIdentifier = 8,
        LineNo = 9,
        SEPAMandateReference = 10,
        PlasticCardPrintedNumber = 11,
        VirtualCardNumber = 12,
        PrimaryAccountNumberAdult = 13,
        Dunning = 14,
        VdvBer = 15, // VdvKa Berechtigungsnummer
        PrimaryAccountNumberYouth = 16,
        PrimaryAccountNumberHonoredCitizen = 17,
        SerialNumbers = 18,

        /// <summary>
        /// Scope for generated DeviceNumber in UM_DEVICE
        /// </summary>
        AutoGeneratedDeviceNumber = 19,
		VdvSessionID = 20,
 		StopNo = 21,
		Document = 22
    }
}
