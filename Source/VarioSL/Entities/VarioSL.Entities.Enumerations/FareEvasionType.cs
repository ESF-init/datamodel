﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available fare evasion types.
    /// </summary>
    [NotInDatabase]
	public enum FareEvasionType
	{
        /// <summary>
        /// fare evasion type none.
        /// </summary>
		Normal = 0,

        /// <summary>
        /// fare evasion type misuse.
        /// </summary>
        Misuse = 1,

        /// <summary>
        /// fare evasion type allowed area exceeded.
        /// </summary>
        AllowedAreaExceeded = 2,

        /// <summary>
        /// fare evasion type manipulation.
        /// </summary>
        Manipulation = 3,
	}
}
