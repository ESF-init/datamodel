﻿namespace VarioSL.Entities.Enumerations
{
    public enum Source
    {
        Unknown = 0,
        Vehicle = 1,
        Website = 2,
        CustomerService = 3,
        Other = 4,
        Autoload = 5,
		SubscriptionManagement = 6,
		StudentManagement = 7,
	}
}
