﻿namespace VarioSL.Entities.Enumerations
{
	public enum PaymentProvider
	{
		None = 0,
		Digicash = 1,
		Flashiz = 2,
		InternetSecure = 3,
		Saferpay = 4,
		Invoice = 5,
		ZeroValue = 6,
        Converge = 7,
        Paymark = 8,
		Sepa = 9,
        SaferpayJsonApi = 10,
	}
}
