﻿namespace VarioSL.Entities.Enumerations
{
    public enum ParameterType
    {
        Text = 0,
        Numeric = 1
    }
}
