﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum TariffAreaListType
    {
        A_Int3 = 0x01,
        A_Int2 = 0x04,
        B_Int3 = 0x05,
        B_Int2 = 0x08,
        C_Int3 = 0x09,
        C_Int2 = 0x0C,
        D_Int3 = 0x0D,
        D_Int2 = 0x10,
        E_Int3 = 0x11,
        E_Int2 = 0x14,
        F_Int3 = 0x15,
        F_Int2 = 0x18,
        G_Int3 = 0x19,
        G_Int2 = 0x1C,
        H_Int3 = 0x1D,
        H_Int2 = 0x20,
        I_Int3 = 0x21,
        I_Int2 = 0x24,
        J_Int3 = 0x25,
        J_Int2 = 0x28,
        K_Int3 = 0x29,
        K_Int2 = 0x2C
    }
}
