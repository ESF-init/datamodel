﻿
namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Represents an inspection result
	/// </summary>
	public enum InspectionResult : int
	{
		/// <summary>
		/// Has not been checked during the inspection.
		/// </summary>
		NotChecked = 0,

		/// <summary>
		/// Inspection passed successfully.
		/// </summary>
		Ok = 1,

		/// <summary>
		/// Inspection failed.
		/// </summary>
		Not_Ok = 2
	}

}
