﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
    public enum Gender
    {
        Unknown = 0,
        Male = 1,
        Female = 2,
        NonBinary = 3
    }
}
