﻿using System.Diagnostics.CodeAnalysis;
using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Enumeration values of audit register value type.
	/// </summary>
	[NotInDatabase]
	public enum AuditRegisterValueType
	{
		/// <summary>
		/// Total validation counts.
		/// Former register value name: Validations
		/// </summary>
		TotalValidations = 1,

		/// <summary>
		/// Rider class count.
		/// Former register value name: RiderClassCounts
		/// </summary>
		RiderClassCount = 2,

		/// <summary>
		/// Absolute count of approved transactions.
		/// New register value.
		/// </summary>
		ApprovedTransactionsAbsolute = 3,

		/// <summary>
		/// Absolute count of denied transactions.
		/// New register value.
		/// </summary>
		DeniedTransactionsAbsolute = 4,

		/// <summary>
		/// Absolute count of read failures.
		/// New register value.
		/// </summary>
		ReadFailuresAbsolute = 5,

		/// <summary>
		/// Fare products validated.
		/// </summary>
		ValidatedFareProducts = 6,

		/// <summary>
		/// Fare value deducted.
		/// </summary>
		DeductedFareValues = 7,

		/// <summary>
		/// Absolute count of new fare media issued.
		/// New register value.
		/// </summary>
		NewFareMediaAbsolute = 8,

		/// <summary>
		/// Absolute count of fare products sold.
		/// New register value.
		/// </summary>
		FareProductSalesAbsolute = 9,

		/// <summary>
		/// Absolute stored values loaded.
		/// New register value.
		/// </summary>
		StoredValueLoadedAbsolute = 10,

		/// <summary>
		/// Account inquiries.
		/// </summary>
		AccountInquiries = 11,

		/// <summary>
		/// Cash transactions by amount and denomination.
		/// </summary>
		CashTransactions = 12,

		/// <summary>
		/// Credit card (13) and debit card (14) transactions by amount. Both register 13 and 14 are captured as one since it can't be distinguished between credit card or debit card.
		/// </summary>
		CardTransactions = 1314,

		/// <summary>
		/// Absolute count of approved and denied transactions.
		/// New register value.
		/// </summary>
		ApprovedAndDeniedTransactionsAbsolute = 15,

		/// <summary>
		/// The total count of all transactions completed since data was last uploaded to the back office.
		/// </summary>
		AllTransactionsCount = 161,

		/// <summary>
		/// The total value of all transactions completed since data was last uploaded to the back office.
		/// </summary>
		AllTransactionsValue = 162,

		/// <summary>
		/// The date and time of the last successful data upload to the back office.
		/// </summary>
		SuccessDateTime = 17,

		/// <summary>
		/// Relative count of approved transactions since data was last uploaded.
		/// Former register value: ApprovedTransactions (3)
		/// </summary>
		ApprovedTransactionsRelative = 18,

		/// <summary>
		/// Count of denied transactions since data was last uploaded.
		/// Former register value: DeniedTransactions (4)
		/// </summary>
		DeniedTransactionsRelative = 19,

		/// <summary>
		/// Count of read failures since data was last uploaded.
		/// Former register value: ReadFailures (5)
		/// </summary>
		ReadFailuresRelative = 20,

		/// <summary>
		/// New fare media issued since data was las uploaded.
		/// Former register value: NewFareMedia (8)
		/// </summary>
		NewFareMediaRelative = 21,

		/// <summary>
		/// Fare products sold since data was last uploaded.
		/// Former register value: FareProductSales (9)
		/// </summary>
		FareProductSalesRelative = 22,

		/// <summary>
		/// Stored values loaded since data was last uploaded.
		/// Former register value: StoredValueLoaded (10)
		/// </summary>
		StoredValueLoadedRelative = 23,

		/// <summary>
		/// Count of approved and denied transactions since data was last uploaded.
		/// Former register value: Transactions (15)
		/// </summary>
		ApprovedAndDeniedTransactionsRelative = 24,

		/// <summary>
		/// Credit card transactions by amount
		/// </summary>
		CardTransactionsCredit = 25,

		/// <summary>
		/// Debit card transactions by amount
		/// </summary>
		CardTransactionsDebit = 26,

		/// <summary>
		/// Count of approved payment requests since data was last uploaded.
		/// </summary>
		ApprovedPaymentsRelative = 27,

		/// <summary>
		/// Count of declined payment requests since data was last uploaded.
		/// </summary>
		DeclinedPaymentsRelative = 28,

		/// <summary>
		/// Count of failed payment requests since data was last uploaded.
		/// </summary>
		FailedPaymentsRelative = 29,

		/// <summary>
		/// Count of requested hotlist/whitelist updates since data was last uploaded.
		/// </summary>
		RequestedHotlistWhitelistUpdatesRelative = 30,

		/// <summary>
		/// Count of failed hotlist/whitelist updates since data was last uploaded.
		/// </summary>
		FailedHotlistWhitelistUpdatesRelative = 31,

		/// <summary>
		/// Count of successful hotlist/whitelist updates since data was last uploaded.
		/// </summary>
		SuccessfulHotlistWhitelistUpdatesRelative = 32,

		/// <summary>
		/// ACH (Automated Clearing House) transactions by amount.
		/// </summary>
		ACHTransactions = 33,

		/// <summary>
		/// Check transactions by amount.
		/// </summary>
		CheckTransactions = 34,

		/// <summary>
		/// Contactless EMV (Europay Mastercard Visa; i.e. ApplePay, GooglePay) transactions by amount.
		/// </summary>
		ContactlessEMVTransactions = 35,

		/// <summary>
		/// 3rd party voucher transactions by amount.
		/// </summary>
		ThirdPartyVoucherTransactions = 36,

		/// <summary>
		/// Total count of cash transactions.
		/// </summary>
		TotalCashTransactions = 37,

		/// <summary>
		/// Total count of Check transactions.
		/// </summary>
		TotalCheckTransactions = 38,

		/// <summary>
		/// Total count of contactless EMV (Europay Mastercard Visa i.e. ApplePay, GooglePay) transactions.
		/// </summary>
		TotalContactlessEMVTransactions = 39,

		/// <summary>
		/// Total count of 3rd party voucher transactions.
		/// </summary>
		TotalThirdPartyVoucherTransactions = 40,

		/// <summary>
		/// Total count of credit and debit card transactions.
		/// </summary>
		TotalCreditDebitCardTransactions = 41,

		/// <summary>
		/// Total count of ACH (Automated Clearing House) transactions.
		/// </summary>
		TotalACHTransactions = 42,

		/// <summary>
		/// Total count of "goodwill" transactions.
		/// </summary>
		TotalGoodwillTransactions = 43,

		/// <summary>
		/// Total amount of "goodwill" transactions.
		/// </summary>
		GoodwillTransactions = 44

	}
}
