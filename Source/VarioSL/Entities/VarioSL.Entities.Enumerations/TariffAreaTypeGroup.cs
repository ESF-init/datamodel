﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum TariffAreaTypeGroup
    {
        AreaListType = 1,
        AreaInstanceType = 2,
    }
}
