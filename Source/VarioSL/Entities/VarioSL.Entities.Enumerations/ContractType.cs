﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available contact types.
    /// </summary>
	public enum ContractType
	{
        /// <summary>
        /// Contact type unknown (will be mapped to database value '0').
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Contact type customer contract (will be mapped to database value '1').
        /// </summary>
        CustomerContract = 1,

        /// <summary>
        /// Contact type school contract (will be mapped to database value '2').
        /// </summary>
        SchoolContract = 2,

        /// <summary>
        /// Contact type framework contract (will be mapped to database value '3').
        /// </summary>
        FrameworkContract = 3,

        /// <summary>
        /// Contact type abonement contract (will be mapped to database value '4').
        /// </summary>
        AbonementContract = 4,

        /// <summary>
        /// Contact type fare evasion fine contract (will be mapped to database value '5').
        /// </summary>
        FareEvasionFineContract = 5
	}
}
