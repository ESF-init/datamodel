﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available device payment methods.
    /// Attention: The values in this enum must match the PAYMENT_METHOD enum in ShiftData.cs (DataImport).
    /// </summary>
    public enum DevicePaymentMethod
	{
        /// <summary>
        /// Device payment method cash (will be mapped to database value '0').
        /// </summary>
		Cash = 0,

	    /// <summary>
	    /// Device payment method ec (will be mapped to database value '1').
	    /// </summary>
        EC = 1,

	    /// <summary>
	    /// Device payment method credit card (will be mapped to database value '2').
	    /// </summary>
        CreditCard = 2,

	    /// <summary>
	    /// Device payment method debit card (will be mapped to database value '3').
	    /// </summary>
        DebitCard = 3,

	    /// <summary>
	    /// Device payment method mw (will be mapped to database value '4').
	    /// </summary>
        MW = 4,

	    /// <summary>
	    /// Device payment method prox (will be mapped to database value '5').
	    /// </summary>
        Prox = 5, // Stored value

        /// <summary>
        /// Device payment method postpaid (will be mapped to database value '6').
        /// </summary>
        Postpaid = 6,

	    /// <summary>
	    /// Device payment method cash mm (will be mapped to database value '7').
	    /// </summary>
        Cash_MW = 7,

        /// <summary>
        /// Device payment method personal check (will be mapped to database value '8').
        /// </summary>
        PersonalCheck = 8, // Voucher

        /// <summary>
        /// Device payment method benefit program (will be mapped to database value '9').
        /// </summary>
        BenefitProgram = 9,

        /// <summary>
        /// Device payment method subsidy (will be mapped to database value '10').
        /// </summary>
        Subsidy = 10,

	    /// <summary>
	    /// Device payment method combined (will be mapped to database value '255').
	    /// </summary>
        Combined = 255,

	    /// <summary>
	    /// Device payment method digi cash (will be mapped to database value '256').
	    /// </summary>
        Digicash = 256,

	    /// <summary>
	    /// Device payment method flashiz (will be mapped to database value '257').
	    /// </summary>
        Flashiz = 257,

	    /// <summary>
	    /// Device payment method safer pay (will be mapped to database value '258').
	    /// </summary>
        Saferpay = 258,

	    /// <summary>
	    /// Device payment method v pay (will be mapped to database value '259').
	    /// </summary>
        VPay = 259,

	    /// <summary>
	    /// Device payment method wired bank transfer (will be mapped to database value '260').
	    /// </summary>
        BankTransfer = 260, // Überweisung

	    /// <summary>
	    /// Device payment method invoid (will be mapped to database value '261').
	    /// </summary>
        Invoice = 261,

	    /// <summary>
	    /// Device payment method godwill (will be mapped to database value '262').
	    /// </summary>
        Goodwill = 262,

	    /// <summary>
	    /// Device payment method check (will be mapped to database value '263').
	    /// </summary>
        Check = 263,

	    /// <summary>
	    /// Device payment method ach (will be mapped to database value '264').
	    /// </summary>
        ACH = 264,

        /// <summary>
        /// Device payment method pay pal (will be mapped to database value '265').
        /// </summary>
        PayPal = 265,

        /// <summary>
        /// Device payment method courtesy (will be mapped to database value '266').
        /// </summary>
        InactivityReinstatement = 266,

	    /// <summary>
	    /// Device payment method po invoice (will be mapped to database value '267').
	    /// </summary>
        POInvoice = 267,

	    /// <summary>
	    /// Device payment method external credit card (will be mapped to database value '268').
	    /// </summary>
        CreditCard_External = 268,

	    /// <summary>
	    /// Device payment method sepa (will be mapped to database value '269').
	    /// </summary>
        SEPA = 269, //SEPA Lastschrift

        /// <summary>
        /// Device payment method promotional code (will be mapped to database value '270').
        /// </summary>
        PromoCode = 270,
		
		/// <summary>
		/// Device payment method for chargebacks. (will be mapped to database value '271').
		/// A chargback is when the customer requests their bank to undo a charge transaction.
		/// This payment method is used in refund transactions to rollback the product/card changes
		/// without returning the money back to the customer (because this already happened in the
		/// chargeback).
		/// </summary>
		Debt_Recovery_Chargeback = 271,
        
        /// <summary>
		/// Device payment method pre load gift (will be mapped to database value '272').
		/// </summary>
		PreLoadGift = 272,

        /// <summary>
        /// Device payment method external debit card (will be mapped to database value '273').
        /// </summary>
        DebitCard_External = 273,

		/// <summary>
		/// Device payment method unknown (will be mapped to database value '999').
		/// </summary>
		Unknown = 999,
	}
}
