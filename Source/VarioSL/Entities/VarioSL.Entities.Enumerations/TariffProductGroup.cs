﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// group of a product
    /// </summary>
    [NotInDatabase]
    public enum TariffProductGroup
    {
        /// <summary>
        /// group has not been set
        /// </summary>
        None = 0,
        /// <summary>
        /// shipping costs
        /// </summary>
        Shipping = 1,

        /// <summary>
        /// else
        /// </summary>
        Else = 2
    }
}
