﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum VarioSettlementState
	{
		None = 0,
		Invalid = 1,
		Valid = 2,
		ValidArchive = 3
	}
}
