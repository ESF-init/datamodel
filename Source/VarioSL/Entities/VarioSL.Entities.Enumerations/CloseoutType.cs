﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available close out types.
    /// </summary>
	public enum CloseoutType
	{
        /// <summary>
        /// Close out type sale (will be mapped to database value '0').
        /// </summary>
        Sale = 0,

        /// <summary>
        /// Close out type payment (will be mapped to database value '0').
        /// </summary>
        Payment = 1,

        /// <summary>
        /// Close out type payment reconciliation (will be mapped to database value '0').
        /// </summary>
        PaymentReconciliation = 2,

        /// <summary>
        /// Close out type revenue recognition (will be mapped to database value '0').
        /// </summary>
        RevenueRecognition = 3,

        /// <summary>
        /// Close out type settlement (will be mapped to database value '0').
        /// </summary>
        Settlement = 4
	}
}
