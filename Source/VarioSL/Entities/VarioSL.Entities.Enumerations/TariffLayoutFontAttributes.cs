﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum TariffLayoutFontAttributes
	{
		None = 0x00,
		Underline = 0x10,
		Double = 0x02,
		Bold = 0x01,
		Inverse = 0x20
	}
}
