﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available card action request types.
    /// </summary>
	public enum CardActionRequestType
	{
        /// <summary>
        /// Card action request type unknown.
        /// </summary>
		Unknown = 0,

        /// <summary>
        /// Card action request type topup.
        /// </summary>
		Topup = 1,

        /// <summary>
        /// Card action request type pass.
        /// </summary>
        Pass = 2,

        /// <summary>
        /// Card action request type activate card.
        /// </summary>
        ActivateCard = 3,

        /// <summary>
        /// Card action request type block card.
        /// </summary>
        BlockCard = 4,

        /// <summary>
        /// Card action request type set default trip.
        /// </summary>
        SetDefaultTrip = 5,

        /// <summary>
        /// Card action request type load.
        /// </summary>
        Load = 6,

        /// <summary>
        /// Card action request type modify.
        /// </summary>
        Modify = 7,

        /// <summary>
        /// Card action request type delete.
        /// </summary>
        Delete = 8,

        /// <summary>
        /// AddressBookEntryType set card attributes.
        /// </summary>
        SetCardAttributes = 9,

        /// <summary>
        /// Card action request type reset dormancy.
        /// </summary>
        ResetDormancy = 10
	}
}
