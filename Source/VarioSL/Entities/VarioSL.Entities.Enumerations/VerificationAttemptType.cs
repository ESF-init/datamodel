﻿
namespace VarioSL.Entities.Enumerations
{
	public enum VerificationAttemptType
	{
		Unknown = 0,
		Phone = 1,
		SecurityQuestion = 2,
		Password = 3,
	}
}
