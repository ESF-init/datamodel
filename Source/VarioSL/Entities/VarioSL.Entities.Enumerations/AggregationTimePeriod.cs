﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available aggregation time periods.
    /// </summary>
	[NotInDatabase]
	public enum AggregationTimePeriod
	{
        /// <summary>
        /// Aggregation time period all before date.
        /// </summary>
        AllBeforeDate = 0,

        /// <summary>
        /// Aggregation time period current past day.
        /// </summary>
        CurrentPastDay = 1,

        /// <summary>
        /// Aggregation time period current past week.
        /// </summary>
        CurrentPastWeek = 2,

        /// <summary>
        /// Aggregation time period current past month.
        /// </summary>
        CurrentPastMonth = 3,

        /// <summary>
        /// Aggregation time period current past quarter.
        /// </summary>
        CurrentPastQuarter = 4,

        /// <summary>
        /// Aggregation time period current past year.
        /// </summary>
        CurrentPastYear = 5,

        /// <summary>
        /// Aggregation time period past 24 hours.
        /// </summary>
        Past24Hours = 6,

        /// <summary>
        /// Aggregation time period past 7 days.
        /// </summary>
        Past7Days = 7,

        /// <summary>
        /// Aggregation time period past 30 days.
        /// </summary>
        Past30Days = 8,

        /// <summary>
        /// Aggregation time period past 90 days.
        /// </summary>
        Past90Days = 9,

        /// <summary>
        /// Aggregation time period past 365 days.
        /// </summary>
        Past365Day = 10
	}
}
