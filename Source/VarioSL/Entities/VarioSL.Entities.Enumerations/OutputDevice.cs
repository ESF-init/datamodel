﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase] //TODO: Create entity and remove attribute
	public enum OutputDevice
	{
		NONE = 0,
		PAPER = 1,
		ET_GK = 2,
		PROX = 3,
		EE_GK = 4,
		DUMMY = 5,//nix ausgeben
		ALWA = 6,
		VDVKA=7,
		ITSO = 8
	}
}
