﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available document types.
    /// </summary>
	public enum DocumentType
	{
        /// <summary>
        /// Document type general (will be mapped to database value '0').
        /// Documententyp: Allgemein
        /// </summary>
		General = 0,

        /// <summary>
        /// Document type ebe (will be mapped to database value '1').
        /// Documententyp: EBE
        /// </summary>
        EBE = 1,

        /// <summary>
        /// Document type info letter (will be mapped to database value '2').
        /// Documententyp: Infobrief
        /// </summary>
        InfoLetter = 2,

        /// <summary>
        /// Document type first demand (will be mapped to database value '3').
        /// Documententyp: Erste Mahnung
        /// </summary>
        FirstDemand = 3,

        /// <summary>
        /// Document type last demand (will be mapped to database value '4').
        /// Documententyp: Letzte Mahnung
        /// </summary>
        LastDemand = 4,

        /// <summary>
        /// Document type invoice (will be mapped to database value '5').
        /// Documententyp: Rechnung
        /// </summary>
        Invoice = 5,

        /// <summary>
        /// Document type invoice receipt (will be mapped to database value '6').
        /// Documententyp: Rechnungsbeleg
        /// </summary>
        InvoiceReceipt = 6,

        /// <summary>
        /// Document type receivables (will be mapped to database value '7').
        /// Documententyp: Forderungen
        /// </summary>
        Receivables = 7,

        /// <summary>
        /// Document type payables (will be mapped to database value '8').
        /// Documententyp: Verbindlichkeiten
        /// </summary>
        Payables = 8,

        /// <summary>
        /// Document type reversal (will be mapped to database value '9').
        /// Documententyp: Belege
        /// </summary>
        Reversal = 9,

        /// <summary>
        /// Document type hire purchase agreement (will be mapped to database value '10').
        /// Documententyp: Ratenverträge
        /// </summary>
        HirePurchaseAgreement = 10,

        /// <summary>
        /// Document type rogatory letter (will be mapped to database value '11').
        /// Documententyp: Rechtshilfeanträge
        /// </summary>
        RogatoryLetter = 11,

        /// <summary>
        /// Document type others (will be mapped to database value '12').
        /// Documententyp: Andere
        /// </summary>
        Others = 12,

        /// <summary>
        /// Document type demand for prosecution (will be mapped to database value '13').
        /// Documententyp: Strafanträge
        /// </summary>
        DemandForProsecution = 13,

        /// <summary>
        /// Document type delivery note (will be mapped to database value '15').
        /// Documententyp: Bestellübersichten
        /// </summary>
        DeliveryNote = 15,

        /// <summary>
        /// Document type termination (will be mapped to database value '16').
        /// Documententyp: Kündigungen
        /// </summary>
        Termination = 16,

        /// <summary>
        /// Document type Entilement (will be mapped to database value '17').
        /// Documententyp: Berechtigungen
        /// </summary>
        Entitlement = 17,

        /// <summary>
        /// Document type Direct Debit (will be mapped to database value '18').
        /// Documententyp: Lastschrift
        /// </summary>
        DirectDebit = 18,

        /// <summary>
        /// Document type Direct Debit Authorization (will be mapped to database value '19').
        /// Documententyp: Lastschrift-Einzugsermächtigung
        /// </summary>
        DirectDebitAuthorization = 19,

        /// <summary>
        /// Document type Encashment (will be mapped to database value '20').
        /// Documententyp: Inkasso
        /// </summary>
        Encashment = 20,

        /// <summary>
        /// Document type Encashment Export (will be mapped to database value '21').
        /// Documententyp: Inkassoexport
        /// </summary>
        EncashmentExport = 21,

        /// <summary>
        /// Document type Letter Of Regret (will be mapped to database value '22').
        /// Documententyp: Ablehnungsschreiben
        /// </summary>
        LetterOfRegret = 22,
    }
}
