﻿namespace VarioSL.Entities.Enumerations
{
	public enum RuleViolationType
	{
		PRODUCT_USAGE_VELOCITY = 0,
		PRODUCT_LOAD_VELOCITY = 1,
		TRANSACTION_VELOCITY = 2,
		GEOGRAPHICAL_VELOCITY = 3,
		UNLIMITED_RIDE_USAGE_VELOCITY = 4,
	}
}