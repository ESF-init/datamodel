﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    public enum Salutation
    {
        /// <summary>
        /// Male salutation.
        /// </summary>
        Mr = 1,

        /// <summary>
        /// Female salutation
        /// </summary>
        Ms = 2,

        /// <summary>
        /// Juristic person salutation
        /// </summary>
        Company = 3,

        /// <summary>
        /// Unspecified/no salutation.
        /// </summary>
        Unknown = 4,

        /// <summary>
        /// Mx as salutation for non-binary people. There are more than this, but
        /// Mx is the most common used term.
        /// </summary>
        Mx = 5,
    }
}
