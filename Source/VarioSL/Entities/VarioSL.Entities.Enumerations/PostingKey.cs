﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum PostingKey
	{

		#region Posting Types

		MonthlyInstallment = 2003,
		QuarterlyInstallment = 2004,
		SemiannuallyInstallment = 2005,
		AnnuallyInstallment = 2006,

		ProductSale = 2001,
		ProductRenewal = 2007,

		OpenDemand = 2020,
		BillEntry = 2021,
		OpenDept = 2022,
		BillEntryAssets = 2023,

		MiscellaneousLiability = 2110,

		CashPayment = 2200,
		BankTransfarePayment = 2201,
		SepaBankPayment = 2202,

		ExtraPayment = 2205,
		OverPayment = 2206,
		Payment = 2207,
		RefundTicket = 2213,
		ProductCancel = 2251,

		/// <summary>
		/// Book out of credit from positive invoice
		/// </summary>
		InvoiceCreditBookOut = 2260,

		/// <summary>
		/// Invoice credit after book out
		/// </summary>
		InvoiceCredit = 2270,

		/// <summary>
		///  Ruecklastschrift
		///  Return debit
		/// </summary>
		ReturnDebitNote = 2302,

		/// <summary>
		/// Book out all open postings due to incasso movement
		/// Ausgebucht wegen Übergabe an Inkasso
		/// </summary>
		InkassoBookedUp = 2304,
		ProcessingCharge = 2308,
		BankCharge = 2310,
		ReplacementCharge = 2311,
		Demand = 2312,

		TerminationRefund = 2313,
		TerminationCharge = 2314,
		TerminationDifferenceCost = 2315,
        /// <summary>
        ///  Ruecklastschrift, manuell buchen
        ///  Return debit, book manually
        /// </summary>
        ReturnDebitNoteBookManually = 2319
		#endregion
	}
}
