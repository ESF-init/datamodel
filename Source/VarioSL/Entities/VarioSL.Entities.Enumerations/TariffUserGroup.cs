﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum TariffUserGroup
	{
		None = 0,
		Adult = 1,
		StudentYouth = 2,
		Senior = 3,
		Disabled = 4,
		DisabledAttendant = 5,
		SuperSenior = 6,
		GenericReducedFare = 7,
		LifetimeRider = 8
	}
}
