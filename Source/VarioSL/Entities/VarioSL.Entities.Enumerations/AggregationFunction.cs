﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available aggregation functions.
    /// </summary>
    public enum AggregationFunction
    {
        /// <summary>
        /// Aggregation function none (will be mapped to database value '0').
        /// </summary>
        None = 0,

        /// <summary>
        /// Aggregation function count distinct (will be mapped to database value '1').
        /// </summary>
        Count = 1,

        /// <summary>
        /// Aggregation function count distinct (will be mapped to database value '2').
        /// </summary>
        CountDistinct = 2,

        /// <summary>
        /// Aggregation function count row (will be mapped to database value '3').
        /// </summary>
        CountRow = 3,

        /// <summary>
        /// Aggregation function count big (will be mapped to database value '4').
        /// </summary>
        CountBig = 4,

        /// <summary>
        /// Aggregation function count big distinct (will be mapped to database value '5').
        /// </summary>
        CountBigDistinct = 5,

        /// <summary>
        /// Aggregation function count big row (will be mapped to database value '6').
        /// </summary>
        CountBigRow = 6,

        /// <summary>
        /// Aggregation function avg (will be mapped to database value '7').
        /// </summary>
        Avg = 7,

        /// <summary>
        /// Aggregation function avg distinct (will be mapped to database value '8').
        /// </summary>
        AvgDistinct = 8,

        /// <summary>
        /// Aggregation function max (will be mapped to database value '9').
        /// </summary>
        Max = 9,

        /// <summary>
        /// Aggregation function min (will be mapped to database value '10').
        /// </summary>
        Min = 10,

        /// <summary>
        /// Aggregation function sum (will be mapped to database value '11').
        /// </summary>
        Sum = 11,

        /// <summary>
        /// Aggregation function sum distinct (will be mapped to database value '12').
        /// </summary>
        SumDistinct = 12,

        /// <summary>
        /// Aggregation function st dev (will be mapped to database value '13').
        /// </summary>
        StDev = 13,

        /// <summary>
        /// Aggregation function st dev distinct (will be mapped to database value '14').
        /// </summary>
        StDevDistinct = 14,

        /// <summary>
        /// Aggregation function variance (will be mapped to database value '15').
        /// </summary>
        Variance = 15,

        /// <summary>
        /// Aggregation function variance distinct (will be mapped to database value '16').
        /// </summary>
        VarianceDistinct = 16
    }
}
