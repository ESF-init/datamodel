﻿
namespace VarioSL.Entities.Enumerations
{
	public enum OrderType
	{
		/// <summary>
		/// Order Type Unknown
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// Order Type Anonymous
		/// </summary>
		Anonymous = 1,

		/// <summary>
		/// Order Type Individual
		/// </summary>
		Individual = 2,

		/// <summary>
		/// Order Type Organizational
		/// </summary>
		Organizational = 3
	}
}
