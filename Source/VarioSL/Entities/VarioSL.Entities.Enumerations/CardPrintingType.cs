﻿
namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Available card printing types
	/// </summary>
	public enum CardPrintingType
	{
		/// <summary>
		/// Type Unknown (will be mapped to database value '0').
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// Type No Printing (will be mapped to database value '1').
		/// </summary>
		NoPrinting = 1,

		/// <summary>
		/// Type Individual (will be mapped to database value '2').
		/// </summary>
		Individual = 2,

		/// <summary>
		/// Type Organizational (will be mapped to database value '3').
		/// </summary>
		Organizational = 3,

		/// <summary>
		/// Type Non Personalized (will be mapped to database value '4').
		/// </summary>
		NonPersonalized = 4,

		/// <summary>
		/// Type Personalized (will be mapped to database value '5').
		/// </summary>
		Personalized = 5,

		/// <summary>
		/// Type Photo (will be mapped to database value '6').
		/// </summary>
		Photo = 6,
	}
}
