﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    // Source from nx\src\old\util\NxZeitkartentyp.h
    [NotInDatabase]
    public enum NxTemporalType
    {
        ZK_KEINE_ZEITKARTE = 0,

        //Wochenkarten
        ZK_AKTUELLE_WOCHE = 1, //Kalenderwochenkarte (Mo-So), aktuelle KW voreingestellt,
        ZK_FOLGEWOCHE = 2, //Kalenderwochenkarte, Folge-KW voreingestellt
        ZK_WOCHE_ABH_DO = 6, //Kalenderwochenkarte, bis Donnerstag ist die aktuelle KW gewaehlt, dannach die Folge-KW	
        ZK_WOCHE_ABH_X = 50,//Kalenderwochenkarte, aktuelle oder spaetere KWs koennen gewaehlt werden; bis zu einem per globalen Parameter einstellbaren Tag ist die aktuelle KW gewaehlt, dannach die Folge-KW	

        //Monatskarten
        ZK_AKTUELLER_MONAT = 51, //Kalendermonatskarte, aktuelle KM voreingestellt, 
        ZK_FOLGEMONAT = 52, //Kalendermonatskarte, Folge-KM voreingestellt
        ZK_TAG_PLUS_1MONAT = 54, //Monatskarte mit flexiblem Beginn (z.B 27.5 bis 27.6, 31.5 bis 30.6)
        ZK_MONAT_AB_20 = 72, //Kalendermonatskarte, bis zum 20ten ist der aktuelle KM gewaehlt, dannach der Folge KM	
        ZK_TAG_PLUS_1MONAT_MINUS_1TAG = 85, //Monatskarte mit flexiblem Beginn (z.B 27.5 bis 26.6, 31.5 bis 29.6)
        ZK_MONAT_ABH_X = 100,//Kalendermonatskarte, bis zu einem per globalen Parameter einstellbaren Tag ist die aktuelle KM gewaehlt, dannach der Folge-KM

        //Jahreskarten
        ZK_JAHR_AKTUELLER_MONAT = 151,//Monatsgenaue Jahreskarte (aktueller Monats ist voreingestellt)
        ZK_JAHR_FOLGEMONAT = 152,//Monatsgenaue Jahreskarte (Folgemonat ist voreingestellt)
        ZK_JAHR_MONAT_AB_20 = 153,//Monatsgenaue Jahreskarte (bis zum 20ten ist der aktuelle KM gewaehlt, dannach der Folge KM	)
        ZK_JAHR_MONAT_ABH_X = 154,//Monatsgenaue Jahreskarte bis zu einem per globalen Parameter einstellbaren Tag ist die aktuelle KM gewaehlt, dannach der Folge-KM	)
        ZK_TAG_PLUS_JAHR = 155,//Tagesgenaue Jahreskarte (vom 1.1.X bis 1.1.x+1)
        ZK_TAG_PLUS_JAHR_MINUS_1TAG = 156,//Tagesgenaue Jahreskarte (vom 1.1.x bis 31.12.x)

        //N-Tageskarten
        ZK_FLEXIBLER_TAG = 150, // 1-Tagekarte mit flexiblem Beginn (aktueller Tag voreingestellt)
        ZK_TAG_PLUS_3_TAGE = 11,  // 4-Tagekarte mit flexiblem Beginn (z.B. Tag voreingestellter Tag + 3Tage) (aktueller Tag voreingestellt)
        ZK_TAG_PLUS_6_TAGE = 10,  // 7-Tagekarte mit flexiblem Beginn (aktueller Tag voreingestellt)
        ZK_TAG_PLUS_7_TAGE = 5,   // 8-Tagekarte mit flexiblem Beginn (aktueller Tag voreingestellt) 
        ZK_TAG_PLUS_13TAGE = 200, //14-Tagekarte mit flexiblem Beginn (aktueller Tag voreingestellt)
        ZK_TAG_PLUS_27TAGE = 201, //28-Tagekarte mit flexiblem Beginn (aktueller Tag voreingestellt) 
        ZK_PLUS_30_TAGE = 53,  //31-Tagekarte mit flexiblem Beginn (aktueller Tag voreingestellt)
        ZK_PLUS_31_TAGE = 55,  //32-Tagekarte mit flexiblem Beginn (aktueller Tag voreingestellt)

        //N-Monatskarten
        ZK_DRITTEL_JAHR = 250, //1/3-Jahr-Karte, mit Beginn im Jan, Mai oder Sep. Aktueller Periodenbeginn voreingestellt.
        ZK_DRITTEL_JAHR_AB_20 = 251, //1/3-Jahr-Karte, mit Beginn im Jan, Mai oder Sep. Aktueller Periode ist bis zum 20ten des letzten Monats der Periode voreingestellt, danach die Folgeperiode
        ZK_DRITTEL_JAHR_ABH_X = 252, //1/3-Jahr-Karte, mit Beginn im Jan, Mai oder Sep. Aktueller Periode ist bis zu einem per globalen Parameter einstellbaren Tag des letzten Monats der Periode voreingestellt, danach die Folgeperiode
    }
}
