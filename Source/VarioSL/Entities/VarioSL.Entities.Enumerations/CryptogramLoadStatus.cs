﻿

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Status describing the load situation of a cryptogram.
    /// </summary>
    public enum CryptogramLoadStatus
    {
        /// <summary>
        /// Cryptogram has been imported and is ready to be loaded to the SAM but has not yet been loaded (will be mapped to database value '0').
        /// </summary>
        Imported = 0,

        /// <summary>
        /// Cryptogram has successfully been loaded to the SAM (will be mapped to database value '1').
        /// </summary>
        Loaded = 1,

        /// <summary>
        /// Cryptogram has not been loaded to the SAM due to an occurring error (will be mapped to database value '2').
        /// </summary>
        LoadingFailed = 2,

        /// <summary>
        /// Cryptogram has been with SYMKEYACK Confirmed  (will be mapped to database value '3').
        /// </summary>
        Confirmed = 3
    }
}