﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
    public enum TrafficLightState
    {
        Unknown = 0,
        Red = 1,
        Yellow =2,
        Green = 3
    }
}

  