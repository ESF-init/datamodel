﻿namespace VarioSL.Entities.Enumerations
{
    public enum PostingScope
    {
        Undefined=0,
        STM=1,
        ABO=2,
        EBE=3,
		MOBILEvarioAPI=4,
		CSW=16,
    }
}
