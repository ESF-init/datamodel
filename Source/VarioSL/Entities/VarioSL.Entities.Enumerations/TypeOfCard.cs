﻿namespace VarioSL.Entities.Enumerations
{
    public enum TypeOfCard
    {
        Staff = 0,
        ClosedLoop = 215,
        OpenLoop = 217,
        HNLEFARE = 221
    }
}
