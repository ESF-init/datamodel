﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available bank statement states
    /// </summary>
	public enum BankStatementState
	{
        /// <summary>
        /// Bank statement state new (will be mapped to database value '0').
        /// </summary>
        New = 0,

        /// <summary>
        /// Bank statement state match (will be mapped to database value '1').
        /// </summary>
        Match = 1,

        /// <summary>
        /// Bank statement state no match (will be mapped to database value '2').
        /// </summary>
        NoMatch = 2,

        /// <summary>
        /// Bank statement state duplicate (will be mapped to database value '3').
        /// </summary>
        Duplicate = 3,

        /// <summary>
        /// Bank statement state for multiple payment matches for one bank statement. This is a erroneous state.
        /// </summary>
        MultiMatch = 4,

        /// <summary>
        /// Bank statement state for a reconciled payment, that has no accepted or refunded state on MOBILEvario side.
        /// </summary>
        InvalidPayment = 5,
	}
}
