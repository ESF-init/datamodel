﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Format of the certificate. Describes how the certificate is set up.
    /// </summary>
    public enum CertificateFormat
    {
        /// <summary>
        /// Certificate in x.509 standard format (will be mapped to database value '0').
        /// </summary>
        X509 = 0,

        /// <summary>
        /// Card Verifiable certificate. Optimized to be compact (will be mapped to database value '1').
        /// </summary>
        CV = 1
    }
}
