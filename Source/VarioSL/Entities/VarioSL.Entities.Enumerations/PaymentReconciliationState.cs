﻿namespace VarioSL.Entities.Enumerations
{
	public enum PaymentReconciliationState
	{
		NotReconciled = 0,
		Reconciled = 1,
		Chargeback = 2
	}
}