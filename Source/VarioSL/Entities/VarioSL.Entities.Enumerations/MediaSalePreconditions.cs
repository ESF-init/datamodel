﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Enumeration of media sale preconditions. Meet all definied criteria before media sale (AND logic).
    /// </summary>
    public enum MediaSalePrecondition
    {
        /// <summary>
        /// No media sale preconditions
        /// </summary>
        None = 0,
        /// <summary>
        /// Cardholder Name required (or Organizational Name in case of a Institutional Sale)
        /// </summary>
        NameRequired = 1,
        /// <summary>
        /// Cardholder Expiration Date required
        /// </summary>
        ExpirationDateRequired = 2,
        /// <summary>
        /// Cardholder Name and Expiration Date required
        /// </summary>
        NameExpirationDateRequired = 3,
        /// <summary>
        /// Cardholder Photo required
        /// </summary>
        PhotoRequired = 4,
        /// <summary>
        /// Cardholder Name and Photo required
        /// </summary>
        NamePhotoRequired = 5,
        /// <summary>
        /// Cardholder Expiration Date and Photo required
        /// </summary>
        ExpirationDatePhotoRequired = 6,
        /// <summary>
        /// Cardholder Name and Expiration Date and Photo required
        /// </summary>
        NameExpirationDatePhotoRequired = 7,
		/// <summary>
		/// Organizational Logo can be provided
		/// </summary>
		CompanyLogoSupported = 8,
		/// <summary>
		/// Organizational Name and a logo can be provided
		/// </summary>
		NameRequiredCompanyLogoSupported = 9
    }
}