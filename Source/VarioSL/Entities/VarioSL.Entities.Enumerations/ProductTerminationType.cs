﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Available product termination types.
	/// </summary>
	[NotInDatabase]
	public enum ProductTerminationType
	{
		/// <summary>
		/// Product termination type by the customer (will be mapped to database value '1').
		/// </summary>
		ByTheCustomer = 1,

		/// <summary>
		/// Product termination type termination based on tariff provisions (will be mapped to database value '2').
		/// </summary>
		TariffRegulations = 2,

		/// <summary>
		/// Product termination type extraordinary termination (will be mapped to database value '3').
		/// </summary>
		Extraordinary = 3,

		/// <summary>
		/// Product termination type termination by transport companies (will be mapped to database value '4').
		/// </summary>
		TransportCompany = 4,

		/// <summary>
		/// Product termination type Termination for death (will be mapped to database value '5').
		/// </summary>
		Death = 5
    }
}
