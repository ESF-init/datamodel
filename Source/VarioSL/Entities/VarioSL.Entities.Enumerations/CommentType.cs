﻿namespace VarioSL.Entities.Enumerations
{
	/// <summary>
	/// Type of a comment entity.
	/// Comments(n) are linked to a contract(1)
	/// </summary>
	public enum CommentType
	{
		Miscellaneous = 0,
		NoCreditWorthiness = 1,
		Dunning = 2,
		Blocked = 3,
		DataProtected = 4 //Gezwärscht
	}
}
