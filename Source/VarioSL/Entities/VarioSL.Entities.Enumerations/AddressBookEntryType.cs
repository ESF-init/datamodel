﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available address book entry types.
    /// </summary>
    public enum AddressBookEntryType
    {
        /// <summary>
        /// Address book entry type unknown (will be mapped to database value '0').
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Address book entry type registration office (will be mapped to database value '1').
        /// </summary>
        RegistrationOffice = 1,

        /// <summary>
        /// Address book entry type police office (will be mapped to database value '2').
        /// </summary>
        PoliceOffice = 2
    }
}
