﻿namespace VarioSL.Entities.Enumerations
{
	public enum MessageType
	{
		Email = 0,
		SMS = 1,
		Push = 2 
	}
}
