﻿using System;
using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    [NotInDatabase]
    [Flags]
    public enum CreditCardRiskAnalysisFlags
    {
        None = 0,
        NewCard = 1,
        ARQC = 2,
        AggregationValueLimit = 4,
        MaxTimeBetweenAuthorizationsExeeded = 8,
        MaxCumulativeOfflineAmountExceeded = 16,
        IsMasterCard = 32,
        MaxDailyAmount = 64,
        IsRetry = 128
    }
}
