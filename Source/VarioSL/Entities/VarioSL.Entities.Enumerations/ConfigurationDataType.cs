﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available configuration data types
    /// </summary>
    public enum ConfigurationDataType
    {
        /// <summary>
        /// Configuration data type text (will be mapped to database value '0').
        /// </summary>
        Text = 0
    }
}
