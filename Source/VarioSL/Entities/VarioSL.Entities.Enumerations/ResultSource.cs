﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Enumeration that defines who decided the result
    /// </summary>
    [NotInDatabase]
    public enum ResultSource
    {
        /// <summary>
        /// Result was defined by the  server
        /// </summary>
        Ovs = 0,
        /// <summary>
        /// Result was defined by the client
        /// </summary>
        Client = 1
    }
}
