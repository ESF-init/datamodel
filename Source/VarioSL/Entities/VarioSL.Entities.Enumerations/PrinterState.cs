﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// High level printer state.
    /// </summary>
	public enum PrinterState
    {
        /// <summary>
        /// Printer state is unknown (will be mapped to database value '0').
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Printer is online (will be mapped to database value '1').
        /// </summary>
        Online = 1,

        /// <summary>
        /// Printer is Offline (will be mapped to database value '2').
        /// </summary>
        Offline = 2,

        /// <summary>
        /// Printer is Busy (will be mapped to database value '3').
        /// </summary>
        Busy = 3,

        /// <summary>
        /// Printer has card that should be ejected (will be mapped to database value '4').
        /// </summary>
        Eject = 4,
	}
}