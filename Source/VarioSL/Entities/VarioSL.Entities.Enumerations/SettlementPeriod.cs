﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Types of Apportionment for accounting.
    /// </summary>
    [NotInDatabase]
    public enum SettlementPeriod
    {
        /// <summary>
        /// Settle Monthly
        /// </summary>
		Monthly = 1,

        /// <summary>
        /// Settle Daily
        /// </summary>
        Daily = 2
    }
}