﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum TariffListLayoutType
	{
		Products = 0,
		VAT = 1,
		SumSingleItem = 2
	}
}
