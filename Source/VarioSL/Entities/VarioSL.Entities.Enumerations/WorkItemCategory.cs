﻿namespace VarioSL.Entities.Enumerations
{
	public enum WorkItemCategory
	{
		Unknown,
		Card,
		Order,
		CustomerContract,
		OrganizationContract
	}
}
