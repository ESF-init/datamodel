﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum TariffBusinessRuleVariableDirection
	{
		In = 1,
		Out = 2
	}
}
