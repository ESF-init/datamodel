﻿namespace VarioSL.Entities.Enumerations
{
	public enum RefundReason
	{
		CustomerChoosenWrongProduct = 0,
		IllnesOfCustomer = 1,
		NoFullService = 2,
		GoodWill = 3,
		Other = 4,
        Returned = 5,
		Abitur = 6,
		ChangeAddress = 7,
		Wrong = 8,
		Error = 9,
		Stolen = 10,
		LeaveSchool = 11,
		ChangeSchool = 12,
		TarifError = 13,
		Move = 14,
		Unreadable = 15,
		Lost = 16,
		Broken = 17,
		FourweeksFirst= 18
	}
}
