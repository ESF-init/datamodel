﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available printer types.
    /// </summary>
	public enum PrinterType
    {
        /// <summary>
        /// Printer type is unknown (will be mapped to database value '0').
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Printer for smart cards (will be mapped to database value '1').
        /// </summary>
        BulkPrinter = 1,

        /// <summary>
        /// Printer for receipts (will be mapped to database value '2').
        /// </summary>
        ReceiptPrinter = 2,

        /// <summary>
        /// Printer for letters (will be mapped to database value '3').
        /// </summary>
        LetterPrinter = 3,
    }
}