﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// State of the tenant.
    /// </summary>
    public enum TenantState
    {
        /// <summary>
        /// Tenant was declined.
        /// </summary>
        Declined = 0,

        /// <summary>
        /// Tenant is waiting for approval from legal department.
        /// </summary>
        WaitingForActivationFromLegal,

        /// <summary>
        /// Tenant is waiting for approval from sales department.
        /// </summary>
        WaitingForActivationFromSales,

        /// <summary>
        /// Tenant is waiting for approval from controlling.
        /// </summary>
        WaitingForActivationFromControlling,
        
        /// <summary>
        /// Tenant is waiting for approval from operations.
        /// </summary>
        WaitingForActivationFromOperations,

        /// <summary>
        /// Tenant is active.
        /// </summary>
        Active,
    }
}