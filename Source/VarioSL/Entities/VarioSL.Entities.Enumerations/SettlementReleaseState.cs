﻿namespace VarioSL.Entities.Enumerations
{
	public enum SettlementReleaseState
	{
		NotReleased = 0,
		Released = 1,
		Revoked = 2,
		Removed = 3
	}
}
