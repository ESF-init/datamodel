﻿namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available work item states.
    /// </summary>
    public enum WorkItemState
    {
        /// <summary>
        /// Work item state new (will be mapped to database value '0').
        /// </summary>
        New = 0,

        /// <summary>
        /// Work item state inWork (will be mapped to database value '1').
        /// </summary>
        InWork = 1,

        /// <summary>
        /// Work item state pending (will be mapped to database value '2').
        /// </summary>
        Pending = 2,

        /// <summary>
        /// Work item state need information (will be mapped to database value '3').
        /// </summary>
        NeedInformation = 3,

        /// <summary>
        /// Work item state resolved (will be mapped to database value '4').
        /// </summary>
        Resolved = 4,

        /// <summary>
        /// Work item state closed (will be mapped to database value '5').
        /// </summary>
        Closed = 5
    }
}
