﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
	[NotInDatabase]
	public enum ProductRelationType
	{
		Default = 0,
		AlternativeRelation = 1
	}
}
