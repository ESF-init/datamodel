﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Recognition Policies for accounting.
    /// </summary>
    [NotInDatabase]
    public enum RecognitionPolicy
    {
        /// <summary>
        /// Daily recognition.
        /// </summary>
		Daily = 1,

        /// <summary>
        /// Monthly recognition.
        /// </summary>
        Monthly = 2,

        /// <summary>
        /// Recognition upon product expiry.
        /// </summary>
        Expiry = 3,

        /// <summary>
        /// Recognition at time of sale.
        /// </summary>
        Sale = 4,

        /// <summary>
        /// Recognition at time of use.
        /// </summary>
        Use = 5
    }
}