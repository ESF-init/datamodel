﻿namespace VarioSL.Entities.Enumerations
{
    public enum FarePaymentResultType
    {
        /// <summary>
        ///  no Error
        /// </summary>
        Ok = 0,
        /// <summary>
        ///  as return value of cardopen: indicates that contents were modified
        /// </summary>
        Modify = 1,
        /// <summary>
        ///  proxcard still present after CardReleased
        /// </summary>
        CardPresent = 2,
        /// <summary>
        ///  application not responsible for handling prox transaction
        /// </summary>
        Unconcerned = 3,
        /// <summary>
        ///  application requests further processing (e.g.: setshareds())
        /// </summary>
        Continue = 4,
        /// <summary>
        ///  return value of handleAction: card modified, rescan required
        /// </summary>
        ModifyRescan = 5,
        /// <summary>
        ///  planned warm reset
        /// </summary>
        ResetWarm = 6,
        /// <summary>
        ///  creates a card from the prestate
        /// </summary>
        CloneCard = 7,
        /// <summary>
        /// driver accepts invalid card manually 
        /// </summary>
        ManualOk = 8,

        /// <summary>
        ///  real Errors start here...
        /// </summary>
        ErrorMin = 100,

        //
        //  range 100 - 199:  controller Errors
        //
        /// <summary>
        ///  general failure
        /// </summary>
        ErrorController = 101,
        /// <summary>
        ///  data corrupt
        /// </summary>
        ErrorInvalidData = 102,
        /// <summary>
        ///  wrong card-ID
        /// </summary>
        ErrorInvalidId = 103,
        /// <summary>
        ///  card corrupt
        /// </summary>
        ErrorInvalidCard = 104,
        /// <summary>
        ///  more than one valid card at the same time
        /// </summary>
        ErrorCollision = 105,
        /// <summary>
        ///  new TC smaller than card-TC
        /// </summary>
        ErrorInvalidTc = 106,
        /// <summary>
        ///  Mifare-authentication failed
        /// </summary>
        ErrorAuthFailed = 107,
        /// <summary>
        ///  unknown telegram
        /// </summary>
        ErrorTelegram = 108,
        /// <summary>
        ///  invalid Mifare-Keyset
        /// </summary>
        ErrorMifarekeys = 109,
        /// <summary>
        ///  card lost while waiting for host
        /// </summary>
        ErrorCardLost = 110,
        /// <summary>
        ///  address not start of block or out of range
        /// </summary>
        ErrorAddress = 111,
        /// <summary>
        ///  Wrong state for this action
        /// </summary>
        ErrorState = 112,
        /// <summary>
        ///  configuration Error in controller (eg. Key missing...s)
        /// </summary>
        ErrorConfiguration = 113,
        /// <summary>
        ///  requested slot not active
        /// </summary>
        ErrorSlotEmpty = 114,
        /// <summary>
        ///  Action is not supported
        /// </summary>
        ErrorNotSupported = 115,
        /// <summary>
        ///  function not available
        /// </summary>
        ErrorInvalidFunction = 116,
        /// <summary>
        ///  initialization failed
        /// </summary>
        ErrorInitialize = 117,
        /// <summary>
        ///  no response from card or SAM
        /// </summary>
        ErrorControllerTimeout = 118,
        /// <summary>
        /// invalid SAM-Key
        /// </summary>
        ErrorSamkey = 119,
        /// <summary>
        /// no valid session
        /// </summary>
        ErrorNoSession = 120,
        /// <summary>
        /// no ctrl on reader
        /// </summary>
        ErrorNoControl = 121,

        /// <summary>
        ///  read Error misc
        /// </summary>
        ErrorRead = 130,
        /// <summary>
        ///  read Error permanent data
        /// </summary>
        ErrorReadPermdat = 131,
        /// <summary>
        ///  read Error TC
        /// </summary>
        ErrorReadTc = 132,
        /// <summary>
        ///  read Error TA-block
        /// </summary>
        ErrorReadTa = 133,
        /// <summary>
        ///  read Error no data / record available 
        /// </summary>
        ErrorReadNoData = 134,

        /// <summary>
        ///  write Error misc
        /// </summary>
        ErrorWrite = 140,
        /// <summary>
        ///  write Error TA-block misc
        /// </summary>
        ErrorWriteTa = 141,
        /// <summary>
        ///  write Error TA-block NAK received
        /// </summary>
        ErrorWriteTaNak = 142,
        /// <summary>
        ///  write Error TA-Block card lost
        /// </summary>
        ErrorWriteTaCardLost = 143,
        /// <summary>
        ///  write Error TC misc
        /// </summary>
        ErrorWriteTc = 150,
        /// <summary>
        ///  write Error TC NAK received
        /// </summary>
        ErrorWriteTcNak = 151,
        /// <summary>
        ///  write Error TC card lost, TC not incremented
        /// </summary>
        ErrorWriteTcCardLost = 152,
        /// <summary>
        ///  write Error TC card lost on commit, TC-state unknown
        /// </summary>
        ErrorWriteTcCardLostCommit = 153,
        /// <summary>
        ///  write Error Card / ProxFile is out of Memory
        /// </summary>
        ErrorWriteOutOfMemory = 154,

        /// <summary>
        ///  reader is not turned on yet
        /// </summary>
        ErrorReaderOff = 160,
        /// <summary>
        /// Success of APDU execution is unknown
        /// </summary>
        ErrorApduSendCardLost = 161,

        /// <summary>
        ///  Unplanned warm reset, possible reader crash
        /// </summary>
        ErrorUnplannedReset = 198,
        /// <summary>
        ///  Controller busy, try again later
        /// </summary>
        ErrorControllerBusy = 199,

        //
        //  range 200 - 299:  host Errors
        //
        /// <summary>
        ///  invalid length field in transaction data
        /// </summary>
        ErrorInvalidLength = 200,
        /// <summary>
        ///  company id not matching
        /// </summary>
        ErrorInvalidCompany = 201,
        /// <summary>
        ///  card is blocked (blacklisted)
        /// </summary>
        ErrorBlocked = 202,
        /// <summary>
        ///  MAC checksum not matching
        /// </summary>
        ErrorInvalidMac = 203,
        /// <summary>
        ///  unknown master key id
        /// </summary>
        ErrorInvalidKeyid = 204,
        /// <summary>
        ///  get/set ValueBlock
        /// </summary>
        ErrorValueBlock = 205,
        /// <summary>
        ///  host can't process card (e.g. missing driver logon etc.)
        /// </summary>
        ErrorHostNotReady = 206,
        /// <summary>
        ///  not enough credit for desired transaction
        /// </summary>
        ErrorInsufficientCredit = 207,
        /// <summary>
        ///  timeout while host waits for response of controller
        /// </summary>
        ErrorHostTimeout = 208,
        /// <summary>
        ///  missing locating information (line, fare stage, ...)
        /// </summary>
        ErrorNoLocation = 209,
        /// <summary>
        ///  no fare stage found in tariff
        /// </summary>
        ErrorNoFarestage = 210,
        /// <summary>
        ///  no ticket found in tariff
        /// </summary>
        ErrorNoTicket = 211,
        /// <summary>
        ///  unable to compute fare from fare stage and ticket
        /// </summary>
        ErrorNoFare = 212,
        /// <summary>
        ///  check out fails: no check in found
        /// </summary>
        ErrorCiMissing = 213,
        /// <summary>
        ///  check out fails: check in is expired
        /// </summary>
        ErrorCiExpired = 214,
        /// <summary>
        ///  check out fails: check in on other line or route
        /// </summary>
        ErrorCiOutside = 215,
        /// <summary>
        ///  check out fails: check in at same stop
        /// </summary>
        ErrorCiSameStop = 216,
        /// <summary>
        ///  card is expired
        /// </summary>
        ErrorCardExpired = 217,
        /// <summary>
        ///  unknown card type
        /// </summary>
        ErrorInvalidCardtype = 218,
        /// <summary>
        ///  can't initialize accounting record
        /// </summary>
        ErrorAccountRecord = 219,
        /// <summary>
        ///  can't write accounting record
        /// </summary>
        ErrorAccountWrite = 220,
        /// <summary>
        ///  card has changed in 'long' transaction (e.g. cancellation)
        /// </summary>
        ErrorCardChanged = 221,
        /// <summary>
        ///  no new check in done because card is already checked in
        /// </summary>
        ErrorCiAlreadyDone = 222,
        /// <summary>
        ///  check out fails: check in in other vehicle
        /// </summary>
        ErrorCiWrongVehicle = 223,
        /// <summary>
        ///  internal host Error (software Error)
        /// </summary>
        ErrorHostInternal = 224,
        /// <summary>
        ///  not enough space on card
        /// </summary>
        ErrorOverflow = 225,
        /// <summary>
        ///  inconsistent data on card
        /// </summary>
        ErrorInvalidHostData = 226,
        /// <summary>
        ///  invalid action list entry
        /// </summary>
        ErrorInvalidAction = 227,
        /// <summary>
        ///  no more valid trips
        /// </summary>
        ErrorNoTripsLeft = 228,
        /// <summary>
        ///  reversal not possible
        /// </summary>
        ErrorReversal = 229,
        /// <summary>
        ///  reversal not possible
        /// </summary>
        ErrorActionProcessing = 230,
        /// <summary>
        ///  ticket not valid in current zone
        /// </summary>
        ErrorInvalidZone = 231,
        /// <summary>
        ///  card of the person who is logged on, but no logoff requested
        /// </summary>
        ErrorAlreadyLoggedOn = 232,
        /// <summary>
        ///  system currently can't process this card type
        /// </summary>
        ErrorCardIgnored = 233,
        /// <summary>
        ///  card not activated
        /// </summary>
        ErrorActivation = 234,
        /// <summary>
        ///  card not yet valid
        /// </summary>
        ErrorValidFuture = 235,
        /// <summary>
        ///  card or datastructure not initialized 
        /// </summary>
        ErrorUninitialized = 236,
        /// <summary>
        ///  Process aborted (eg by driver) 
        /// </summary>
        ErrorManualabort = 237,
        /// <summary>
        ///  invalid array on card 
        /// </summary>
        ErrorInvalidArray = 238,
        /// <summary>
        ///  missing application on card
        /// </summary>
        ErrorNoApplication = 239,
        /// <summary>
        ///  purse upper limit overrun
        /// </summary>
        ErrorPurseUpperLimit = 240,
        /// <summary>
        ///  can't perform action because it was already performed
        /// </summary>
        ErrorActionAlreadyDone = 241,
        /// <summary>
        ///  Could not activate card, because card is already activated.
        /// </summary>
        ErrorAlreadyActivated = 242,
        /// <summary>
        ///  Card is dormant
        /// </summary>
        ErrorCardDormant = 243,
        /// <summary>
        ///  invalid file id
        /// </summary>
        ErrorInvalidFileid = 244,
        /// <summary>
        ///  invalid ticket
        /// </summary>
        ErrorInvalidTicket = 245,
        /// <summary>
        ///  ???
        /// </summary>
        ErrorCryption = 246,
        /// <summary>
        ///  ticket duplicated, ticket already existing
        /// </summary>
        ErrorTicketDuplicate = 247,
        /// <summary>
        ///  Card configuration not appropriate
        /// </summary>
        ErrorCardConfiguration = 248,
        /// <summary>
        ///  SAM configuration not appropriate
        /// </summary>
        ErrorSamConfiguration = 249,
        /// <summary>
        ///  too many persons (group size / add. riders)
        /// </summary>
        ErrorTooManyPersons = 250,
        /// <summary>
        ///  ticket expired
        /// </summary>
        ErrorTicketExpired = 251,
        /// <summary>
        ///  ticket not valid at current time
        /// </summary>
        ErrorInvalidTime = 252,
        /// <summary>
        ///  ticket not valid on this line
        /// </summary>
        ErrorInvalidLine = 253,
        /// <summary>
        ///  ticket not valid due to business rule
        /// </summary>
        ErrorRuledOut = 254,
        /// <summary>
        ///  ticket not valid due to outmask condition
        /// </summary>
        ErrorInvalidOutmask = 255,
        /// <summary>
        ///  ticket not valid due to paymask condition
        /// </summary>
        ErrorInvalidPaymask = 256,
        /// <summary>
        /// to verify ticket signature
        /// </summary>
        ErrorInvalidSignature = 257,
        /// <summary>
        /// host is offline (ID based systems)
        /// </summary>
        ErrorOffline = 258,
        /// <summary>
        /// No (valid) ticket on card
        /// </summary>
        ErrorNoTicketOnCard = 259,
        /// <summary>
        /// Check in has already been denied
        /// </summary>
        ErrorCiAlreadyDenied = 260,
        /// <summary>
        /// Live card presented in training mode
        /// </summary>
        ErrorTrainingMode = 261,
        /// <summary>
        /// Training card presented (in live mode)
        /// </summary>
        ErrorTrainingCard = 262,
        /// <summary>
        ///Unknown transit account ID 
        /// </summary>
        ErrorUnknownId = 263,
        /// <summary>
        /// SAM cannot be accessed, maybe used by other module
        /// </summary>
        SamNotUsable = 264,
        /// <summary>
        /// // ticket blocked
        /// </summary>
        TicketBlocked = 265,
        /// <summary>
        /// Inspection negative due to not yet activated/validated product.
        /// </summary>
        NotValidated = 266,
        //
        //  range 300 - 399:  external prox device Errors
        //
        /// <summary>
        ///  Error opening connection to device
        /// </summary>
        ErrorDevOpen = 300,
        /// <summary>
        ///  Error loading device specific data
        /// </summary>
        ErrorDevConfig = 301,
        /// <summary>
        ///  timeout occured
        /// </summary>
        ErrorDevTimeout = 302,

        //
        //  range 400 - 499:  IPI server Errors
        //
        /// <summary>
        ///  internal Error (exception etc.)
        /// </summary>
        ErrorIpiInternal = 400,
        /// <summary>
        ///  invalid request
        /// </summary>
        ErrorIpiReqInvalid = 401,
        /// <summary>
        ///  card request timeout
        /// </summary>
        ErrorIpiTimeout = 402,
        /// <summary>
        ///  card state not matching
        /// </summary>
        ErrorIpiBadCardstate = 403,

        //
        //  range 500 - 599:  DESFire specification Errors
        //
        ErrorNoChanges = 501,
        ErrorOutOfEepromError = 502,
        ErrorIllegalCommandCode = 503,
        ErrorIntegrityError = 504,
        ErrorNoSuchKey = 505,
        /* ErrorLengthError           = 506, */
        ErrorPermissionError = 507,
        ErrorParameterError = 508,
        ErrorApplicationNotFound = 509,
        ErrorApplIntegrityError = 510,
        ErrorAuthenticationError = 511,
        ErrorAdditionalFrame = 512,
        ErrorBoundaryError = 513,
        ErrorPiccIntegrityError = 514,
        ErrorCommandAborted = 515,
        ErrorPiccDisabledError = 516,
        ErrorCountError = 517,
        ErrorDuplicateError = 518,
        ErrorEepromError = 519,
        ErrorFileNotFound = 520,
        ErrorFileIntegrityError = 521,
        ErrorLengthError = 522,

        //
        //  range 550 - 599:  EMV Errors
        //
        ErrorEmvGeneralError = 550,
        ErrorEmvSeePhone = 551,
		ErrorEmvOdaFailedOrNotPerformed = 552,

        /// <summary>
        /// tariff not found in Vario DB
        /// </summary>
        ErrorVarioNoTariff = 600,

        /// <summary>
        /// not yet implemented or not configured in this project
        /// </summary>
        ErrorVarioNotSupported = 601,

        /// <summary>
        /// Error in the credit card completion process
        /// </summary>
        ErrorVarioCompletion = 602,

        /// <summary>
        /// Request was already processed (same TransactionId already in DB)
        /// </summary>
        ErrorVarioRequestAlreadyProcessed = 603,

        /// <summary>
        /// The credit card processor declined the authorization request
        /// </summary>
        ErrorVarioCreditCardAuthorizationDeclined = 604,

        /// <summary>
        /// The credit card processor declined the refund request
        /// </summary>
        ErrorVarioCreditCardRefundDeclined = 605,

        /// <summary>
        /// Inspection denial because of timestamp
        /// </summary>
        ErrorVarioOfflineTimestampDeny = 606,

        /// <summary>
        /// Error in the credit card completion process
        /// </summary>
        ErrorVarioCompletionRetry = 607,
        
        /// <summary>
        /// Transaction is depecated (too old)
        /// </summary>
        ErrorVarioTransactionDeprecated = 608,

        /// <summary>
        /// Max vario
        /// </summary>
        ErrorVarioMax = 699,

        ErrorMax
    };
}
