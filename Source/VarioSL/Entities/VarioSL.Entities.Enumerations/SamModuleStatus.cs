﻿

namespace VarioSL.Entities.Enumerations
{
    /// <summary> Status of a SAM module. </summary>
	public enum SamModuleStatus
	{
        /// <summary> SAM module is inactive. </summary>
		Inactive = 0,
        /// <summary> SAM module is active. </summary>
		Active = 1
	}
}
