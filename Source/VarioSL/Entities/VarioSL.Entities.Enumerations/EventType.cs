﻿using VarioSL.Entities.Enumerations.Attributes;

namespace VarioSL.Entities.Enumerations
{
    /// <summary>
    /// Available event type states.
    /// </summary>
    [NotInDatabase]
    public enum EventType
    {
        /// <summary>
        /// Event type unknown (will be mapped to database value '0').
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Event type AccountConfirmation (will be mapped to database value '1').
        /// </summary>
        AccountConfirmation = 1,

        /// <summary>
        /// Event type CloseAccount (will be mapped to database value '2').
        /// </summary>
        CloseAccount = 2,

        /// <summary>
        /// Event type UpdateAccount (will be mapped to database value '3').
        /// </summary>
        UpdateAccount = 3,

        /// <summary>
        /// Event type ResetPassword (will be mapped to database value '4').
        /// </summary>
        ResetPassword = 4,

        /// <summary>
        /// Event type AddAdministrativeAccountConfirmation (will be mapped to database value '5').
        /// </summary>
        AddAdministrativeAccountConfirmation = 5,

        /// <summary>
        /// Event type AutoloadConfirmation (will be mapped to database value '6').
        /// </summary>
        AutoloadConfirmation = 6,

        /// <summary>
        /// Event type AutoloadSuspended (will be mapped to database value '7').
        /// </summary>
        AutoloadSuspended = 7,

        /// <summary>
        /// Event type AutoloadPriceChange (will be mapped to database value '8').
        /// </summary>
        AutoloadPriceChange = 8,

        /// <summary>
        /// Event type AutoloadTransactionFailed (will be mapped to database value '9').
        /// </summary>
        AutoloadTransactionFailed = 9,

        /// <summary>
        /// Event type RegisterSmartcard (will be mapped to database value '10').
        /// </summary>
        RegisterSmartcard = 10,

        /// <summary>
        /// Event type RemoveSmartcard (will be mapped to database value '11').
        /// </summary>
        RemoveSmartcard = 11,

        /// <summary>
        /// Event type BlockSmartcard (will be mapped to database value '12').
        /// </summary>
        BlockSmartcard = 12,

        /// <summary>
        /// Event type UnblockSmartcard (will be mapped to database value '13').
        /// </summary>
        UnblockSmartcard = 13,

        /// <summary>
        /// Event type ReplaceSmartcard (will be mapped to database value '14').
        /// </summary>
        ReplaceSmartcard = 14,

        /// <summary>
        /// Event type ReplaceSmartcardOrganization (will be mapped to database value '15').
        /// </summary>
        ReplaceSmartcardOrganization = 15,

        /// <summary>
        /// Event type OrderConfirmation (will be mapped to database value '16').
        /// </summary>
        OrderConfirmation = 16,

        /// <summary>
        /// Event type OrderPaymentResult (will be mapped to database value '17').
        /// </summary>
        OrderPaymentResult = 17,

        /// <summary>
        /// Event type BlockPaymentOption (will be mapped to database value '18').
        /// </summary>
        BlockPaymentOption = 18,

        /// <summary>
        /// Event type UnblockPaymentOption (will be mapped to database value '19').
        /// </summary>
        UnblockPaymentOption = 19,

        /// <summary>
        /// Event type OrderStateUpdated (will be mapped to database value '20').
        /// </summary>
        OrderStateUpdated = 20,


        /// <summary>
        /// Event type EmailConfirmation (will be mapped to database value '22').
        /// </summary>
        EmailConfirmation = 22,

        /// <summary>
        /// Event type CloseSmartcard (will be mapped to database value '23').
        /// </summary>
        CloseSmartcard = 23,

        /// <summary>
        /// Event type AccountActivated (will be mapped to database value '24').
        /// </summary>
        AccountActivated = 24,

        /// <summary>
        /// Event type WorkItemCreated (will be mapped to database value '25').
        /// </summary>
        WorkItemCreated = 25,

        /// <summary>
        /// Event type AddPaymentOption (will be mapped to database value '26').
        /// </summary>
        AddPaymentOption = 26,

        /// <summary>
        /// Event type RemovePaymentOption (will be mapped to database value '27').
        /// </summary>
        RemovePaymentOption = 27,

        /// <summary>
        /// Event type AccountChanged (will be mapped to database value '28').
        /// </summary>
        AccountChanged = 28,

        /// <summary>
        /// Event type PasswordResetToken (will be mapped to database value '29').
        /// </summary>
        PasswordResetToken = 29,

        /// <summary>
        /// Event type ServiceRequestSupportMail (will be mapped to database value '30').
        /// </summary>
        ServiceRequestSupportMail = 30,

        /// <summary>
        /// Event type FraudDetectionNotification (will be mapped to database value '31').
        /// </summary>
        FraudDetectionNotification = 31,

        /// <summary>
        /// Event type SaleResult (will be mapped to database value '32').
        /// </summary>
        SaleResult = 32,

        /// <summary>
        /// Event type DormancyWarning (will be mapped to database value '33').
        /// </summary>
        DormancyWarning = 33,

        /// <summary>
        /// Event type DormancyExecution (will be mapped to database value '34').
        /// </summary>
        DormancyExecution = 34,

        /// <summary>
        /// Event type AutoloadCreated (will be mapped to database value '35').
        /// </summary>
        AutoloadCreated = 35,

        /// <summary>
        /// Event type AutoloadDeleted (will be mapped to database value '36').
        /// </summary>
        AutoloadDeleted = 36,

        /// <summary>
        /// Event type SmartcardLowBalance (will be mapped to database value '37').
        /// </summary>
        SmartcardLowBalance = 37,

        /// <summary>
        /// Event type RefundResult (will be mapped to database value '38').
        /// </summary>
        RefundResult = 38,

        /// <summary>
        /// Event type AutoloadUpdated (will be mapped to database value '39').
        /// </summary>
        AutoloadUpdated = 39,

        /// <summary>
        /// Event type AutoloadSkipped (will be mapped to database value '40').
        /// </summary>
        AutoloadSkipped = 40,

        /// <summary>
        /// Event type AutoloadSkippedPassManuallyAdded (will be mapped to database value '41').
        /// </summary>
        AutoloadSkippedPassManuallyAdded = 41,

        /// <summary>
        /// Event type FareCategoryExpiry (will be mapped to database value '42').
        /// </summary>
        FareCategoryExpiry = 42,

        /// <summary>
        /// Event type UnusedPassExpiryWarning (will be mapped to database value '43').
        /// </summary>
        UnusedPassExpiryWarning = 43,

        /// <summary>
        /// Event type FareCategoryExpiryWarning (will be mapped to database value '44').
        /// </summary>
        FareCategoryExpiryWarning = 44,

        /// <summary>
        /// Event type PassExpiryWarning (will be mapped to database value '45').
        /// </summary>
        PassExpiryWarning = 45,

        /// <summary>
        /// Event type PassExpiry (will be mapped to database value '46').
        /// </summary>
        PassExpiry = 46,

        /// <summary>
        /// Event type RegistrationWithPassword (will be mapped to database value '47').
        /// </summary>
        RegistrationWithPassword = 47,

        /// <summary>
        /// Event type AccountActivation (will be mapped to database value '48').
        /// </summary>
        AccountActivation = 48,

        /// <summary>
        /// Event type AddCorporateCashValueAutoloadFailed (will be mapped to database value '49').
        /// </summary>
        AddCorporateCashValueAutoloadFailed = 49,

        /// <summary>
        /// Event type AddCorporatePassAutoloadFailed (will be mapped to database value '50').
        /// </summary>
        AddCorporatePassAutoloadFailed = 50,

        /// <summary>
        /// Event type AutoloadPayment (will be mapped to database value '51').
        /// </summary>
        AutoloadPayment = 51,

        /// <summary>
        /// Event type AutoRenewExecution (will be mapped to database value '52').
        /// </summary>
        AutoRenewExecution = 52,

        /// <summary>
        /// Event type AutoRenewSetup (will be mapped to database value '53').
        /// </summary>
        AutoRenewSetup = 53,

        /// <summary>
        /// Event type BillingMail (will be mapped to database value '54').
        /// </summary>
        BillingMail = 54,

        /// <summary>
        /// Event type BillingMailKVV (will be mapped to database value '55').
        /// </summary>
        BillingMailKVV = 55,

        /// <summary>
        /// Event type BobAppContractActivation (will be mapped to database value '56').
        /// </summary>
        BobAppContractActivation = 56,

        /// <summary>
        /// Event type BobAppContractUpdate (will be mapped to database value '57').
        /// </summary>
        BobAppContractUpdate = 57,

        /// <summary>
        /// Event type BobAppContractVerification (will be mapped to database value '58').
        /// </summary>
        BobAppContractVerification = 58,

        /// <summary>
        /// Event type BobAppRejectedContractActivation (will be mapped to database value '59').
        /// </summary>
        BobAppRejectedContractActivation = 59,

        /// <summary>
        /// Event type BobAppVirtualCardRegister (will be mapped to database value '60').
        /// </summary>
        BobAppVirtualCardRegister = 60,

        /// <summary>
        /// Event type ClientAuthenticationRequest (will be mapped to database value '61').
        /// </summary>
        ClientAuthenticationRequest = 61,

        /// <summary>
        /// Event type DeclineCorporateAccount (will be mapped to database value '62').
        /// </summary>
        DeclineCorporateAccount = 62,

        /// <summary>
        /// Event type NegativeBalance (will be mapped to database value '63').
        /// </summary>
        NegativeBalance = 63,

        /// <summary>
        /// Event type RegistrationWithPasswordCorporate (will be mapped to database value '64').
        /// </summary>
        RegistrationWithPasswordCorporate = 64,

        /// <summary>
        /// Event type RemoveCorporateCashValueAutoloadFailed (will be mapped to database value '65').
        /// </summary>
        RemoveCorporateCashValueAutoloadFailed = 65,

        /// <summary>
        /// Event type RemoveCorporatePassAutoloadFailed (will be mapped to database value '66').
        /// </summary>
        RemoveCorporatePassAutoloadFailed = 66,

        /// <summary>
        /// Event type ReportJobExecuted (will be mapped to database value '67').
        /// </summary>
        ReportJobExecuted = 67,

        /// <summary>
        /// Event type ResetPasswordConfirm (will be mapped to database value '68').
        /// </summary>
        ResetPasswordConfirm = 68,

        /// <summary>
        /// Event type ShareCard (will be mapped to database value '69').
        /// </summary>
        ShareCard = 69,

        /// <summary>
        /// Event type SmartcardExpiration12Months (will be mapped to database value '70').
        /// </summary>
        SmartcardExpiration12Months = 70,

        /// <summary>
        /// Event type SmartcardExpiration1Month (will be mapped to database value '71').
        /// </summary>
        SmartcardExpiration1Month = 71,

        /// <summary>
        /// Event type TelephonePinChanged (will be mapped to database value '72').
        /// </summary>
        TelephonePinChanged = 72,

        /// <summary>
        /// Event type TransferOfFunds (will be mapped to database value '73').
        /// </summary>
        TransferOfFunds = 73,

        /// <summary>
        /// Event type UsernameChanged (will be mapped to database value '74').
        /// </summary>
        UsernameChanged = 74,

        /// <summary>
        /// Event type SaleReceipt (will be mapped to database value '75').
        /// </summary>
        SaleReceipt = 75,

        /// <summary>
        /// Event type UnusedPassExpiry (will be mapped to database value '76').
        /// </summary>
        UnusedPassExpiry = 76,

        /// <summary>
        /// Event type OrderConfirmed_OrderState_7 On Hold (will be mapped to database value '77').
        /// </summary>
        OrderConfirmed_OrderState_7 = 77,

        /// <summary>
        /// Event type OrderConfirmed_OrderState_8 Verification required (will be mapped to database value '78').
        /// </summary>
        OrderConfirmed_OrderState_8 = 78,

        /// <summary>
        /// Event type OrderConfirmed_OrderState_9 Manual verification (will be mapped to database value '79').
        /// </summary>
        OrderConfirmed_OrderState_9 = 79,

        /// <summary>
        /// Event type OrderConfirmed_OrderState_4 'Open' (will be mapped to database value '80').
        /// </summary>
        OrderConfirmed_OrderState_4 = 80,

        /// <summary>
        /// Event type OrderConfirmed_OrderState_5 'Failed'(will be mapped to database value '81').
        /// </summary>
        OrderConfirmed_OrderState_5 = 81,

        /// <summary>
        /// Event type OrderConfirmed_OrderState_2 'Processed'(will be mapped to database value '82').
        /// </summary>
        OrderConfirmed_OrderState_2 = 82,

        /// <summary>
        /// Event type OrderConfirmed_OrderState_6 'Cancelled'(will be mapped to database value '83').
        /// </summary>
        OrderConfirmed_OrderState_6 = 83,

        /// <summary>
        /// Event type FareCategoryChanged (will be mapped to database value '84').
        /// </summary>
        FareCategoryChanged = 84,

        /// <summary>
        /// Event type AutoloadTransferred (will be mapped to database value '85').
        /// </summary>
        AutoloadTransferred = 85,

        /// <summary>
        /// Event type RegisterSmartcardFromNotPrimary (will be mapped to database value '86').
        /// </summary>
        RegisterSmartcardFromNotPrimary = 86,

        /// <summary>
        /// Event type RemoveSmartcardFromNotPrimary (will be mapped to database value '87').
        /// </summary>
        RemoveSmartcardFromNotPrimary = 87,

        /// <summary>
        /// Event type AutoloadCreatedFromNotPrimary (will be mapped to database value '88').
        /// </summary>
        AutoloadCreatedFromNotPrimary = 88,

        /// <summary>
        /// Event type AutoloadConfirmationFromNotPrimary (will be mapped to database value '89').
        /// </summary>
        AutoloadConfirmationFromNotPrimary = 89,

        /// <summary>
        /// Event type AutoloadUpdatedFromNotPrimary (will be mapped to database value '90').
        /// </summary>
        AutoloadUpdatedFromNotPrimary = 90,

        /// <summary>
        /// Event type AutoloadDeletedFromNotPrimary (will be mapped to database value '91').
        /// </summary>
        AutoloadDeletedFromNotPrimary = 91,

        /// <summary>
        /// Event type AutoloadSkippedFromNotPrimary  (will be mapped to database value '92').
        /// </summary>
        AutoloadSkippedFromNotPrimary = 92,

        /// <summary>
        /// Event type AutoloadSkippedPassManuallyAddedFromNotPrimary  (will be mapped to database value '93').
        /// </summary>
        AutoloadSkippedPassManuallyAddedFromNotPrimary = 93,

        /// <summary>
        /// Event type AutoloadSuspendedFromNotPrimary   (will be mapped to database value '94').
        /// </summary>
        AutoloadSuspendedFromNotPrimary = 94,

        /// <summary>
        /// Event type AutoloadPriceChangeFromNotPrimary  (will be mapped to database value '95').
        /// </summary>
        AutoloadPriceChangeFromNotPrimary = 95,

        /// <summary>
        /// Event type AutoloadTransactionFailedFromNotPrimary  (will be mapped to database value '96').
        /// </summary>
        AutoloadTransactionFailedFromNotPrimary = 96,

        /// <summary>
        /// Event type SmartcardFraudulentUse  (will be mapped to database value '97').
        /// </summary>
        SmartcardFraudulentUse = 97,

        /// <summary>
        /// Event type AutoloadCreatedForPass  (will be mapped to database value '98').
        /// </summary>
        AutoloadCreatedForPass = 98,

        /// <summary>
        /// Event type AutoloadCreatedForPassFromNotPrimary  (will be mapped to database value '99').
        /// </summary>
        AutoloadCreatedForPassFromNotPrimary = 99,

        /// <summary>
        /// Event type AutoloadUpdatedForPass  (will be mapped to database value '100').
        /// </summary>
        AutoloadUpdatedForPass = 100,

        /// <summary>
        /// Event type AutoloadUpdatedForPassFromNotPrimary  (will be mapped to database value '101').
        /// </summary>
        AutoloadUpdatedForPassFromNotPrimary = 101,

        /// <summary>
        /// Event type AccountConfirmationNoPassword (will be mapped to database value '102')
        /// </summary>
        AccountConfirmationNoPassword = 102,

        /// <summary>
        /// Event type RegiomoveHomeZoneWillExpire (will be mapped to database value '103')
        /// </summary>
        RegiomoveHomeZoneWillExpire = 103,

        /// <summary>
        /// Event type RegiomoveHomeZoneIsExpired (will be mapped to database value '104')
        /// </summary>
        RegiomoveHomeZoneIsExpired = 104,

        /// <summary>
        /// Event type HomeZoneBillingMail (will be mapped to database value '104')
        /// </summary>
        HomeZoneBillingMail = 105,

        /// <summary>
        /// Event type HomeZoneCancel (will be mapped to database value '104')
        /// </summary>
        HomeZoneCancel = 106,

        /// <summary>
        /// Event type AutoloadPaused (will be mapped to database value '107')
        /// </summary>
        AutoloadPaused = 107,

        /// <summary>
        /// Event type AutoloadPausedFromNotPrimary (will be mapped to database value '108')
        /// </summary>
        AutoloadPausedFromNotPrimary = 108,

        /// <summary>
        /// Event type UpdateSecurityQuestion (will be mapped to database value '109')
        /// </summary>
        UpdateSecurityQuestion = 109,
    }
}
