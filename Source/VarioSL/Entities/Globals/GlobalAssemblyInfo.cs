﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: CLSCompliant(false)]

[assembly: AssemblyCompany("INIT GmbH")]
[assembly: AssemblyCopyright("Copyright © INIT GmbH 2018")]

#if (DEBUG)
    [assembly: AssemblyConfiguration("Debug")]
    [assembly: AssemblyProduct("VarioSL Entities *** Debug Build ***")]
#else
    [assembly: AssemblyConfiguration("Retail")]
    [assembly: AssemblyProduct("VarioSL Entities")]
#endif