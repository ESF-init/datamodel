﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("VarioAPI.Tests")]
[assembly: InternalsVisibleTo("VarioSL.Entities.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]