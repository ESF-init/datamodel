﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Client'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ClientEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AccountEntryCollection	_accountEntries;
		private bool	_alwaysFetchAccountEntries, _alreadyFetchedAccountEntries;
		private VarioSL.Entities.CollectionClasses.ApportionmentResultCollection	_apportionmentResults;
		private bool	_alwaysFetchApportionmentResults, _alreadyFetchedApportionmentResults;
		private VarioSL.Entities.CollectionClasses.ApportionmentResultCollection	_apportionmentResults1;
		private bool	_alwaysFetchApportionmentResults1, _alreadyFetchedApportionmentResults1;
		private VarioSL.Entities.CollectionClasses.ApportionmentResultCollection	_apportionmentResults2;
		private bool	_alwaysFetchApportionmentResults2, _alreadyFetchedApportionmentResults2;
		private VarioSL.Entities.CollectionClasses.CalendarCollection	_calendar;
		private bool	_alwaysFetchCalendar, _alreadyFetchedCalendar;
		private VarioSL.Entities.CollectionClasses.ClearingResultCollection	_clearingResultsByCardIssuer;
		private bool	_alwaysFetchClearingResultsByCardIssuer, _alreadyFetchedClearingResultsByCardIssuer;
		private VarioSL.Entities.CollectionClasses.ClearingResultCollection	_clearingResults;
		private bool	_alwaysFetchClearingResults, _alreadyFetchedClearingResults;
		private VarioSL.Entities.CollectionClasses.ClearingResultCollection	_clearingResultsByClientFrom;
		private bool	_alwaysFetchClearingResultsByClientFrom, _alreadyFetchedClearingResultsByClientFrom;
		private VarioSL.Entities.CollectionClasses.ClearingResultCollection	_clearingResultsByClientTo;
		private bool	_alwaysFetchClearingResultsByClientTo, _alreadyFetchedClearingResultsByClientTo;
		private VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection	_clientAdaptedLayoutObjects;
		private bool	_alwaysFetchClientAdaptedLayoutObjects, _alreadyFetchedClientAdaptedLayoutObjects;
		private VarioSL.Entities.CollectionClasses.DebtorCollection	_debtors;
		private bool	_alwaysFetchDebtors, _alreadyFetchedDebtors;
		private VarioSL.Entities.CollectionClasses.DepotCollection	_depots;
		private bool	_alwaysFetchDepots, _alreadyFetchedDepots;
		private VarioSL.Entities.CollectionClasses.FareMatrixCollection	_fareMatrixes;
		private bool	_alwaysFetchFareMatrixes, _alreadyFetchedFareMatrixes;
		private VarioSL.Entities.CollectionClasses.FareTableCollection	_fareTables;
		private bool	_alwaysFetchFareTables, _alreadyFetchedFareTables;
		private VarioSL.Entities.CollectionClasses.LayoutCollection	_layouts;
		private bool	_alwaysFetchLayouts, _alreadyFetchedLayouts;
		private VarioSL.Entities.CollectionClasses.ResponsibilityCollection	_whoIsResponsible;
		private bool	_alwaysFetchWhoIsResponsible, _alreadyFetchedWhoIsResponsible;
		private VarioSL.Entities.CollectionClasses.ResponsibilityCollection	_responsibleFor;
		private bool	_alwaysFetchResponsibleFor, _alreadyFetchedResponsibleFor;
		private VarioSL.Entities.CollectionClasses.TariffCollection	_clientTariffs;
		private bool	_alwaysFetchClientTariffs, _alreadyFetchedClientTariffs;
		private VarioSL.Entities.CollectionClasses.TariffCollection	_ownerClientTariffs;
		private bool	_alwaysFetchOwnerClientTariffs, _alreadyFetchedOwnerClientTariffs;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_tickets;
		private bool	_alwaysFetchTickets, _alreadyFetchedTickets;
		private VarioSL.Entities.CollectionClasses.TicketCategoryCollection	_ticketCategories;
		private bool	_alwaysFetchTicketCategories, _alreadyFetchedTicketCategories;
		private VarioSL.Entities.CollectionClasses.TicketVendingClientCollection	_ticketVendingClient;
		private bool	_alwaysFetchTicketVendingClient, _alreadyFetchedTicketVendingClient;
		private VarioSL.Entities.CollectionClasses.TransactionCollection	_cardIssuerCardTransactions;
		private bool	_alwaysFetchCardIssuerCardTransactions, _alreadyFetchedCardIssuerCardTransactions;
		private VarioSL.Entities.CollectionClasses.UnitCollection	_umUnits;
		private bool	_alwaysFetchUmUnits, _alreadyFetchedUmUnits;
		private VarioSL.Entities.CollectionClasses.UserListCollection	_users;
		private bool	_alwaysFetchUsers, _alreadyFetchedUsers;
		private VarioSL.Entities.CollectionClasses.VarioSettlementCollection	_settlements;
		private bool	_alwaysFetchSettlements, _alreadyFetchedSettlements;
		private VarioSL.Entities.CollectionClasses.VdvKeySetCollection	_vdvKeySet;
		private bool	_alwaysFetchVdvKeySet, _alreadyFetchedVdvKeySet;
		private VarioSL.Entities.CollectionClasses.WorkstationCollection	_workstations;
		private bool	_alwaysFetchWorkstations, _alreadyFetchedWorkstations;
		private VarioSL.Entities.CollectionClasses.AddressBookEntryCollection	_addressBookEntries;
		private bool	_alwaysFetchAddressBookEntries, _alreadyFetchedAddressBookEntries;
		private VarioSL.Entities.CollectionClasses.CardCollection	_cards;
		private bool	_alwaysFetchCards, _alreadyFetchedCards;
		private VarioSL.Entities.CollectionClasses.ConfigurationDefinitionCollection	_configurationDefinitions;
		private bool	_alwaysFetchConfigurationDefinitions, _alreadyFetchedConfigurationDefinitions;
		private VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection	_configurationOverwrites;
		private bool	_alwaysFetchConfigurationOverwrites, _alreadyFetchedConfigurationOverwrites;
		private VarioSL.Entities.CollectionClasses.ContractCollection	_contracts;
		private bool	_alwaysFetchContracts, _alreadyFetchedContracts;
		private VarioSL.Entities.CollectionClasses.ContractCollection	_vanpoolContracts;
		private bool	_alwaysFetchVanpoolContracts, _alreadyFetchedVanpoolContracts;
		private VarioSL.Entities.CollectionClasses.CryptoCryptogramArchiveCollection	_cryptoCryptogramArchives;
		private bool	_alwaysFetchCryptoCryptogramArchives, _alreadyFetchedCryptoCryptogramArchives;
		private VarioSL.Entities.CollectionClasses.CryptoQArchiveCollection	_cryptoQArchives;
		private bool	_alwaysFetchCryptoQArchives, _alreadyFetchedCryptoQArchives;
		private VarioSL.Entities.CollectionClasses.FareChangeCauseCollection	_fareChangeCauses;
		private bool	_alwaysFetchFareChangeCauses, _alreadyFetchedFareChangeCauses;
		private VarioSL.Entities.CollectionClasses.FareEvasionBehaviourCollection	_fareEvasionBehaviours;
		private bool	_alwaysFetchFareEvasionBehaviours, _alreadyFetchedFareEvasionBehaviours;
		private VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection	_fareEvasionIncidents;
		private bool	_alwaysFetchFareEvasionIncidents, _alreadyFetchedFareEvasionIncidents;
		private VarioSL.Entities.CollectionClasses.FareEvasionInspectionCollection	_fareEvasionInspections;
		private bool	_alwaysFetchFareEvasionInspections, _alreadyFetchedFareEvasionInspections;
		private VarioSL.Entities.CollectionClasses.FilterValueSetCollection	_filterValueSets;
		private bool	_alwaysFetchFilterValueSets, _alreadyFetchedFilterValueSets;
		private VarioSL.Entities.CollectionClasses.FormCollection	_forms;
		private bool	_alwaysFetchForms, _alreadyFetchedForms;
		private VarioSL.Entities.CollectionClasses.IdentificationTypeCollection	_identificationTypes;
		private bool	_alwaysFetchIdentificationTypes, _alreadyFetchedIdentificationTypes;
		private VarioSL.Entities.CollectionClasses.InvoiceCollection	_invoices;
		private bool	_alwaysFetchInvoices, _alreadyFetchedInvoices;
		private VarioSL.Entities.CollectionClasses.InvoicingCollection	_invoicings;
		private bool	_alwaysFetchInvoicings, _alreadyFetchedInvoicings;
		private VarioSL.Entities.CollectionClasses.JobCollection	_jobs;
		private bool	_alwaysFetchJobs, _alreadyFetchedJobs;
		private VarioSL.Entities.CollectionClasses.ParameterArchiveCollection	_parameterArchives;
		private bool	_alwaysFetchParameterArchives, _alreadyFetchedParameterArchives;
		private VarioSL.Entities.CollectionClasses.PostingKeyCollection	_postingKeys;
		private bool	_alwaysFetchPostingKeys, _alreadyFetchedPostingKeys;
		private VarioSL.Entities.CollectionClasses.ProductCollection	_products;
		private bool	_alwaysFetchProducts, _alreadyFetchedProducts;
		private VarioSL.Entities.CollectionClasses.ReportJobCollection	_reportJobs;
		private bool	_alwaysFetchReportJobs, _alreadyFetchedReportJobs;
		private VarioSL.Entities.CollectionClasses.ReportJobResultCollection	_reportJobResults;
		private bool	_alwaysFetchReportJobResults, _alreadyFetchedReportJobResults;
		private VarioSL.Entities.CollectionClasses.ReportToClientCollection	_reportToClients;
		private bool	_alwaysFetchReportToClients, _alreadyFetchedReportToClients;
		private VarioSL.Entities.CollectionClasses.RevenueSettlementCollection	_revenueSettlements;
		private bool	_alwaysFetchRevenueSettlements, _alreadyFetchedRevenueSettlements;
		private VarioSL.Entities.CollectionClasses.SaleCollection	_sales;
		private bool	_alwaysFetchSales, _alreadyFetchedSales;
		private VarioSL.Entities.CollectionClasses.SchoolYearCollection	_schoolYears;
		private bool	_alwaysFetchSchoolYears, _alreadyFetchedSchoolYears;
		private VarioSL.Entities.CollectionClasses.StorageLocationCollection	_storageLocations;
		private bool	_alwaysFetchStorageLocations, _alreadyFetchedStorageLocations;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_transactionJournals;
		private bool	_alwaysFetchTransactionJournals, _alreadyFetchedTransactionJournals;
		private VarioSL.Entities.CollectionClasses.TransportCompanyCollection	_transportCompanies;
		private bool	_alwaysFetchTransportCompanies, _alreadyFetchedTransportCompanies;
		private VarioSL.Entities.CollectionClasses.TicketCollection _ticketCollectionViaTicketVendingClient;
		private bool	_alwaysFetchTicketCollectionViaTicketVendingClient, _alreadyFetchedTicketCollectionViaTicketVendingClient;
		private VarioSL.Entities.CollectionClasses.ReportCollection _reportCollectionViaReportToClient;
		private bool	_alwaysFetchReportCollectionViaReportToClient, _alreadyFetchedReportCollectionViaReportToClient;
		private AccountEntryNumberEntity _accountEntryNumber;
		private bool	_alwaysFetchAccountEntryNumber, _alreadyFetchedAccountEntryNumber, _accountEntryNumberReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AccountEntries</summary>
			public static readonly string AccountEntries = "AccountEntries";
			/// <summary>Member name ApportionmentResults</summary>
			public static readonly string ApportionmentResults = "ApportionmentResults";
			/// <summary>Member name ApportionmentResults1</summary>
			public static readonly string ApportionmentResults1 = "ApportionmentResults1";
			/// <summary>Member name ApportionmentResults2</summary>
			public static readonly string ApportionmentResults2 = "ApportionmentResults2";
			/// <summary>Member name Calendar</summary>
			public static readonly string Calendar = "Calendar";
			/// <summary>Member name ClearingResultsByCardIssuer</summary>
			public static readonly string ClearingResultsByCardIssuer = "ClearingResultsByCardIssuer";
			/// <summary>Member name ClearingResults</summary>
			public static readonly string ClearingResults = "ClearingResults";
			/// <summary>Member name ClearingResultsByClientFrom</summary>
			public static readonly string ClearingResultsByClientFrom = "ClearingResultsByClientFrom";
			/// <summary>Member name ClearingResultsByClientTo</summary>
			public static readonly string ClearingResultsByClientTo = "ClearingResultsByClientTo";
			/// <summary>Member name ClientAdaptedLayoutObjects</summary>
			public static readonly string ClientAdaptedLayoutObjects = "ClientAdaptedLayoutObjects";
			/// <summary>Member name Debtors</summary>
			public static readonly string Debtors = "Debtors";
			/// <summary>Member name Depots</summary>
			public static readonly string Depots = "Depots";
			/// <summary>Member name FareMatrixes</summary>
			public static readonly string FareMatrixes = "FareMatrixes";
			/// <summary>Member name FareTables</summary>
			public static readonly string FareTables = "FareTables";
			/// <summary>Member name Layouts</summary>
			public static readonly string Layouts = "Layouts";
			/// <summary>Member name WhoIsResponsible</summary>
			public static readonly string WhoIsResponsible = "WhoIsResponsible";
			/// <summary>Member name ResponsibleFor</summary>
			public static readonly string ResponsibleFor = "ResponsibleFor";
			/// <summary>Member name ClientTariffs</summary>
			public static readonly string ClientTariffs = "ClientTariffs";
			/// <summary>Member name OwnerClientTariffs</summary>
			public static readonly string OwnerClientTariffs = "OwnerClientTariffs";
			/// <summary>Member name Tickets</summary>
			public static readonly string Tickets = "Tickets";
			/// <summary>Member name TicketCategories</summary>
			public static readonly string TicketCategories = "TicketCategories";
			/// <summary>Member name TicketVendingClient</summary>
			public static readonly string TicketVendingClient = "TicketVendingClient";
			/// <summary>Member name CardIssuerCardTransactions</summary>
			public static readonly string CardIssuerCardTransactions = "CardIssuerCardTransactions";
			/// <summary>Member name UmUnits</summary>
			public static readonly string UmUnits = "UmUnits";
			/// <summary>Member name Users</summary>
			public static readonly string Users = "Users";
			/// <summary>Member name Settlements</summary>
			public static readonly string Settlements = "Settlements";
			/// <summary>Member name VdvKeySet</summary>
			public static readonly string VdvKeySet = "VdvKeySet";
			/// <summary>Member name Workstations</summary>
			public static readonly string Workstations = "Workstations";
			/// <summary>Member name AddressBookEntries</summary>
			public static readonly string AddressBookEntries = "AddressBookEntries";
			/// <summary>Member name Cards</summary>
			public static readonly string Cards = "Cards";
			/// <summary>Member name ConfigurationDefinitions</summary>
			public static readonly string ConfigurationDefinitions = "ConfigurationDefinitions";
			/// <summary>Member name ConfigurationOverwrites</summary>
			public static readonly string ConfigurationOverwrites = "ConfigurationOverwrites";
			/// <summary>Member name Contracts</summary>
			public static readonly string Contracts = "Contracts";
			/// <summary>Member name VanpoolContracts</summary>
			public static readonly string VanpoolContracts = "VanpoolContracts";
			/// <summary>Member name CryptoCryptogramArchives</summary>
			public static readonly string CryptoCryptogramArchives = "CryptoCryptogramArchives";
			/// <summary>Member name CryptoQArchives</summary>
			public static readonly string CryptoQArchives = "CryptoQArchives";
			/// <summary>Member name FareChangeCauses</summary>
			public static readonly string FareChangeCauses = "FareChangeCauses";
			/// <summary>Member name FareEvasionBehaviours</summary>
			public static readonly string FareEvasionBehaviours = "FareEvasionBehaviours";
			/// <summary>Member name FareEvasionIncidents</summary>
			public static readonly string FareEvasionIncidents = "FareEvasionIncidents";
			/// <summary>Member name FareEvasionInspections</summary>
			public static readonly string FareEvasionInspections = "FareEvasionInspections";
			/// <summary>Member name FilterValueSets</summary>
			public static readonly string FilterValueSets = "FilterValueSets";
			/// <summary>Member name Forms</summary>
			public static readonly string Forms = "Forms";
			/// <summary>Member name IdentificationTypes</summary>
			public static readonly string IdentificationTypes = "IdentificationTypes";
			/// <summary>Member name Invoices</summary>
			public static readonly string Invoices = "Invoices";
			/// <summary>Member name Invoicings</summary>
			public static readonly string Invoicings = "Invoicings";
			/// <summary>Member name Jobs</summary>
			public static readonly string Jobs = "Jobs";
			/// <summary>Member name ParameterArchives</summary>
			public static readonly string ParameterArchives = "ParameterArchives";
			/// <summary>Member name PostingKeys</summary>
			public static readonly string PostingKeys = "PostingKeys";
			/// <summary>Member name Products</summary>
			public static readonly string Products = "Products";
			/// <summary>Member name ReportJobs</summary>
			public static readonly string ReportJobs = "ReportJobs";
			/// <summary>Member name ReportJobResults</summary>
			public static readonly string ReportJobResults = "ReportJobResults";
			/// <summary>Member name ReportToClients</summary>
			public static readonly string ReportToClients = "ReportToClients";
			/// <summary>Member name RevenueSettlements</summary>
			public static readonly string RevenueSettlements = "RevenueSettlements";
			/// <summary>Member name Sales</summary>
			public static readonly string Sales = "Sales";
			/// <summary>Member name SchoolYears</summary>
			public static readonly string SchoolYears = "SchoolYears";
			/// <summary>Member name StorageLocations</summary>
			public static readonly string StorageLocations = "StorageLocations";
			/// <summary>Member name TransactionJournals</summary>
			public static readonly string TransactionJournals = "TransactionJournals";
			/// <summary>Member name TransportCompanies</summary>
			public static readonly string TransportCompanies = "TransportCompanies";
			/// <summary>Member name TicketCollectionViaTicketVendingClient</summary>
			public static readonly string TicketCollectionViaTicketVendingClient = "TicketCollectionViaTicketVendingClient";
			/// <summary>Member name ReportCollectionViaReportToClient</summary>
			public static readonly string ReportCollectionViaReportToClient = "ReportCollectionViaReportToClient";
			/// <summary>Member name AccountEntryNumber</summary>
			public static readonly string AccountEntryNumber = "AccountEntryNumber";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ClientEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ClientEntity() :base("ClientEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="clientID">PK value for Client which data should be fetched into this Client object</param>
		public ClientEntity(System.Int64 clientID):base("ClientEntity")
		{
			InitClassFetch(clientID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="clientID">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ClientEntity(System.Int64 clientID, IPrefetchPath prefetchPathToUse):base("ClientEntity")
		{
			InitClassFetch(clientID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="clientID">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="validator">The custom validator object for this ClientEntity</param>
		public ClientEntity(System.Int64 clientID, IValidator validator):base("ClientEntity")
		{
			InitClassFetch(clientID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_accountEntries = (VarioSL.Entities.CollectionClasses.AccountEntryCollection)info.GetValue("_accountEntries", typeof(VarioSL.Entities.CollectionClasses.AccountEntryCollection));
			_alwaysFetchAccountEntries = info.GetBoolean("_alwaysFetchAccountEntries");
			_alreadyFetchedAccountEntries = info.GetBoolean("_alreadyFetchedAccountEntries");

			_apportionmentResults = (VarioSL.Entities.CollectionClasses.ApportionmentResultCollection)info.GetValue("_apportionmentResults", typeof(VarioSL.Entities.CollectionClasses.ApportionmentResultCollection));
			_alwaysFetchApportionmentResults = info.GetBoolean("_alwaysFetchApportionmentResults");
			_alreadyFetchedApportionmentResults = info.GetBoolean("_alreadyFetchedApportionmentResults");

			_apportionmentResults1 = (VarioSL.Entities.CollectionClasses.ApportionmentResultCollection)info.GetValue("_apportionmentResults1", typeof(VarioSL.Entities.CollectionClasses.ApportionmentResultCollection));
			_alwaysFetchApportionmentResults1 = info.GetBoolean("_alwaysFetchApportionmentResults1");
			_alreadyFetchedApportionmentResults1 = info.GetBoolean("_alreadyFetchedApportionmentResults1");

			_apportionmentResults2 = (VarioSL.Entities.CollectionClasses.ApportionmentResultCollection)info.GetValue("_apportionmentResults2", typeof(VarioSL.Entities.CollectionClasses.ApportionmentResultCollection));
			_alwaysFetchApportionmentResults2 = info.GetBoolean("_alwaysFetchApportionmentResults2");
			_alreadyFetchedApportionmentResults2 = info.GetBoolean("_alreadyFetchedApportionmentResults2");

			_calendar = (VarioSL.Entities.CollectionClasses.CalendarCollection)info.GetValue("_calendar", typeof(VarioSL.Entities.CollectionClasses.CalendarCollection));
			_alwaysFetchCalendar = info.GetBoolean("_alwaysFetchCalendar");
			_alreadyFetchedCalendar = info.GetBoolean("_alreadyFetchedCalendar");

			_clearingResultsByCardIssuer = (VarioSL.Entities.CollectionClasses.ClearingResultCollection)info.GetValue("_clearingResultsByCardIssuer", typeof(VarioSL.Entities.CollectionClasses.ClearingResultCollection));
			_alwaysFetchClearingResultsByCardIssuer = info.GetBoolean("_alwaysFetchClearingResultsByCardIssuer");
			_alreadyFetchedClearingResultsByCardIssuer = info.GetBoolean("_alreadyFetchedClearingResultsByCardIssuer");

			_clearingResults = (VarioSL.Entities.CollectionClasses.ClearingResultCollection)info.GetValue("_clearingResults", typeof(VarioSL.Entities.CollectionClasses.ClearingResultCollection));
			_alwaysFetchClearingResults = info.GetBoolean("_alwaysFetchClearingResults");
			_alreadyFetchedClearingResults = info.GetBoolean("_alreadyFetchedClearingResults");

			_clearingResultsByClientFrom = (VarioSL.Entities.CollectionClasses.ClearingResultCollection)info.GetValue("_clearingResultsByClientFrom", typeof(VarioSL.Entities.CollectionClasses.ClearingResultCollection));
			_alwaysFetchClearingResultsByClientFrom = info.GetBoolean("_alwaysFetchClearingResultsByClientFrom");
			_alreadyFetchedClearingResultsByClientFrom = info.GetBoolean("_alreadyFetchedClearingResultsByClientFrom");

			_clearingResultsByClientTo = (VarioSL.Entities.CollectionClasses.ClearingResultCollection)info.GetValue("_clearingResultsByClientTo", typeof(VarioSL.Entities.CollectionClasses.ClearingResultCollection));
			_alwaysFetchClearingResultsByClientTo = info.GetBoolean("_alwaysFetchClearingResultsByClientTo");
			_alreadyFetchedClearingResultsByClientTo = info.GetBoolean("_alreadyFetchedClearingResultsByClientTo");

			_clientAdaptedLayoutObjects = (VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection)info.GetValue("_clientAdaptedLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection));
			_alwaysFetchClientAdaptedLayoutObjects = info.GetBoolean("_alwaysFetchClientAdaptedLayoutObjects");
			_alreadyFetchedClientAdaptedLayoutObjects = info.GetBoolean("_alreadyFetchedClientAdaptedLayoutObjects");

			_debtors = (VarioSL.Entities.CollectionClasses.DebtorCollection)info.GetValue("_debtors", typeof(VarioSL.Entities.CollectionClasses.DebtorCollection));
			_alwaysFetchDebtors = info.GetBoolean("_alwaysFetchDebtors");
			_alreadyFetchedDebtors = info.GetBoolean("_alreadyFetchedDebtors");

			_depots = (VarioSL.Entities.CollectionClasses.DepotCollection)info.GetValue("_depots", typeof(VarioSL.Entities.CollectionClasses.DepotCollection));
			_alwaysFetchDepots = info.GetBoolean("_alwaysFetchDepots");
			_alreadyFetchedDepots = info.GetBoolean("_alreadyFetchedDepots");

			_fareMatrixes = (VarioSL.Entities.CollectionClasses.FareMatrixCollection)info.GetValue("_fareMatrixes", typeof(VarioSL.Entities.CollectionClasses.FareMatrixCollection));
			_alwaysFetchFareMatrixes = info.GetBoolean("_alwaysFetchFareMatrixes");
			_alreadyFetchedFareMatrixes = info.GetBoolean("_alreadyFetchedFareMatrixes");

			_fareTables = (VarioSL.Entities.CollectionClasses.FareTableCollection)info.GetValue("_fareTables", typeof(VarioSL.Entities.CollectionClasses.FareTableCollection));
			_alwaysFetchFareTables = info.GetBoolean("_alwaysFetchFareTables");
			_alreadyFetchedFareTables = info.GetBoolean("_alreadyFetchedFareTables");

			_layouts = (VarioSL.Entities.CollectionClasses.LayoutCollection)info.GetValue("_layouts", typeof(VarioSL.Entities.CollectionClasses.LayoutCollection));
			_alwaysFetchLayouts = info.GetBoolean("_alwaysFetchLayouts");
			_alreadyFetchedLayouts = info.GetBoolean("_alreadyFetchedLayouts");

			_whoIsResponsible = (VarioSL.Entities.CollectionClasses.ResponsibilityCollection)info.GetValue("_whoIsResponsible", typeof(VarioSL.Entities.CollectionClasses.ResponsibilityCollection));
			_alwaysFetchWhoIsResponsible = info.GetBoolean("_alwaysFetchWhoIsResponsible");
			_alreadyFetchedWhoIsResponsible = info.GetBoolean("_alreadyFetchedWhoIsResponsible");

			_responsibleFor = (VarioSL.Entities.CollectionClasses.ResponsibilityCollection)info.GetValue("_responsibleFor", typeof(VarioSL.Entities.CollectionClasses.ResponsibilityCollection));
			_alwaysFetchResponsibleFor = info.GetBoolean("_alwaysFetchResponsibleFor");
			_alreadyFetchedResponsibleFor = info.GetBoolean("_alreadyFetchedResponsibleFor");

			_clientTariffs = (VarioSL.Entities.CollectionClasses.TariffCollection)info.GetValue("_clientTariffs", typeof(VarioSL.Entities.CollectionClasses.TariffCollection));
			_alwaysFetchClientTariffs = info.GetBoolean("_alwaysFetchClientTariffs");
			_alreadyFetchedClientTariffs = info.GetBoolean("_alreadyFetchedClientTariffs");

			_ownerClientTariffs = (VarioSL.Entities.CollectionClasses.TariffCollection)info.GetValue("_ownerClientTariffs", typeof(VarioSL.Entities.CollectionClasses.TariffCollection));
			_alwaysFetchOwnerClientTariffs = info.GetBoolean("_alwaysFetchOwnerClientTariffs");
			_alreadyFetchedOwnerClientTariffs = info.GetBoolean("_alreadyFetchedOwnerClientTariffs");

			_tickets = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_tickets", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTickets = info.GetBoolean("_alwaysFetchTickets");
			_alreadyFetchedTickets = info.GetBoolean("_alreadyFetchedTickets");

			_ticketCategories = (VarioSL.Entities.CollectionClasses.TicketCategoryCollection)info.GetValue("_ticketCategories", typeof(VarioSL.Entities.CollectionClasses.TicketCategoryCollection));
			_alwaysFetchTicketCategories = info.GetBoolean("_alwaysFetchTicketCategories");
			_alreadyFetchedTicketCategories = info.GetBoolean("_alreadyFetchedTicketCategories");

			_ticketVendingClient = (VarioSL.Entities.CollectionClasses.TicketVendingClientCollection)info.GetValue("_ticketVendingClient", typeof(VarioSL.Entities.CollectionClasses.TicketVendingClientCollection));
			_alwaysFetchTicketVendingClient = info.GetBoolean("_alwaysFetchTicketVendingClient");
			_alreadyFetchedTicketVendingClient = info.GetBoolean("_alreadyFetchedTicketVendingClient");

			_cardIssuerCardTransactions = (VarioSL.Entities.CollectionClasses.TransactionCollection)info.GetValue("_cardIssuerCardTransactions", typeof(VarioSL.Entities.CollectionClasses.TransactionCollection));
			_alwaysFetchCardIssuerCardTransactions = info.GetBoolean("_alwaysFetchCardIssuerCardTransactions");
			_alreadyFetchedCardIssuerCardTransactions = info.GetBoolean("_alreadyFetchedCardIssuerCardTransactions");

			_umUnits = (VarioSL.Entities.CollectionClasses.UnitCollection)info.GetValue("_umUnits", typeof(VarioSL.Entities.CollectionClasses.UnitCollection));
			_alwaysFetchUmUnits = info.GetBoolean("_alwaysFetchUmUnits");
			_alreadyFetchedUmUnits = info.GetBoolean("_alreadyFetchedUmUnits");

			_users = (VarioSL.Entities.CollectionClasses.UserListCollection)info.GetValue("_users", typeof(VarioSL.Entities.CollectionClasses.UserListCollection));
			_alwaysFetchUsers = info.GetBoolean("_alwaysFetchUsers");
			_alreadyFetchedUsers = info.GetBoolean("_alreadyFetchedUsers");

			_settlements = (VarioSL.Entities.CollectionClasses.VarioSettlementCollection)info.GetValue("_settlements", typeof(VarioSL.Entities.CollectionClasses.VarioSettlementCollection));
			_alwaysFetchSettlements = info.GetBoolean("_alwaysFetchSettlements");
			_alreadyFetchedSettlements = info.GetBoolean("_alreadyFetchedSettlements");

			_vdvKeySet = (VarioSL.Entities.CollectionClasses.VdvKeySetCollection)info.GetValue("_vdvKeySet", typeof(VarioSL.Entities.CollectionClasses.VdvKeySetCollection));
			_alwaysFetchVdvKeySet = info.GetBoolean("_alwaysFetchVdvKeySet");
			_alreadyFetchedVdvKeySet = info.GetBoolean("_alreadyFetchedVdvKeySet");

			_workstations = (VarioSL.Entities.CollectionClasses.WorkstationCollection)info.GetValue("_workstations", typeof(VarioSL.Entities.CollectionClasses.WorkstationCollection));
			_alwaysFetchWorkstations = info.GetBoolean("_alwaysFetchWorkstations");
			_alreadyFetchedWorkstations = info.GetBoolean("_alreadyFetchedWorkstations");

			_addressBookEntries = (VarioSL.Entities.CollectionClasses.AddressBookEntryCollection)info.GetValue("_addressBookEntries", typeof(VarioSL.Entities.CollectionClasses.AddressBookEntryCollection));
			_alwaysFetchAddressBookEntries = info.GetBoolean("_alwaysFetchAddressBookEntries");
			_alreadyFetchedAddressBookEntries = info.GetBoolean("_alreadyFetchedAddressBookEntries");

			_cards = (VarioSL.Entities.CollectionClasses.CardCollection)info.GetValue("_cards", typeof(VarioSL.Entities.CollectionClasses.CardCollection));
			_alwaysFetchCards = info.GetBoolean("_alwaysFetchCards");
			_alreadyFetchedCards = info.GetBoolean("_alreadyFetchedCards");

			_configurationDefinitions = (VarioSL.Entities.CollectionClasses.ConfigurationDefinitionCollection)info.GetValue("_configurationDefinitions", typeof(VarioSL.Entities.CollectionClasses.ConfigurationDefinitionCollection));
			_alwaysFetchConfigurationDefinitions = info.GetBoolean("_alwaysFetchConfigurationDefinitions");
			_alreadyFetchedConfigurationDefinitions = info.GetBoolean("_alreadyFetchedConfigurationDefinitions");

			_configurationOverwrites = (VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection)info.GetValue("_configurationOverwrites", typeof(VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection));
			_alwaysFetchConfigurationOverwrites = info.GetBoolean("_alwaysFetchConfigurationOverwrites");
			_alreadyFetchedConfigurationOverwrites = info.GetBoolean("_alreadyFetchedConfigurationOverwrites");

			_contracts = (VarioSL.Entities.CollectionClasses.ContractCollection)info.GetValue("_contracts", typeof(VarioSL.Entities.CollectionClasses.ContractCollection));
			_alwaysFetchContracts = info.GetBoolean("_alwaysFetchContracts");
			_alreadyFetchedContracts = info.GetBoolean("_alreadyFetchedContracts");

			_vanpoolContracts = (VarioSL.Entities.CollectionClasses.ContractCollection)info.GetValue("_vanpoolContracts", typeof(VarioSL.Entities.CollectionClasses.ContractCollection));
			_alwaysFetchVanpoolContracts = info.GetBoolean("_alwaysFetchVanpoolContracts");
			_alreadyFetchedVanpoolContracts = info.GetBoolean("_alreadyFetchedVanpoolContracts");

			_cryptoCryptogramArchives = (VarioSL.Entities.CollectionClasses.CryptoCryptogramArchiveCollection)info.GetValue("_cryptoCryptogramArchives", typeof(VarioSL.Entities.CollectionClasses.CryptoCryptogramArchiveCollection));
			_alwaysFetchCryptoCryptogramArchives = info.GetBoolean("_alwaysFetchCryptoCryptogramArchives");
			_alreadyFetchedCryptoCryptogramArchives = info.GetBoolean("_alreadyFetchedCryptoCryptogramArchives");

			_cryptoQArchives = (VarioSL.Entities.CollectionClasses.CryptoQArchiveCollection)info.GetValue("_cryptoQArchives", typeof(VarioSL.Entities.CollectionClasses.CryptoQArchiveCollection));
			_alwaysFetchCryptoQArchives = info.GetBoolean("_alwaysFetchCryptoQArchives");
			_alreadyFetchedCryptoQArchives = info.GetBoolean("_alreadyFetchedCryptoQArchives");

			_fareChangeCauses = (VarioSL.Entities.CollectionClasses.FareChangeCauseCollection)info.GetValue("_fareChangeCauses", typeof(VarioSL.Entities.CollectionClasses.FareChangeCauseCollection));
			_alwaysFetchFareChangeCauses = info.GetBoolean("_alwaysFetchFareChangeCauses");
			_alreadyFetchedFareChangeCauses = info.GetBoolean("_alreadyFetchedFareChangeCauses");

			_fareEvasionBehaviours = (VarioSL.Entities.CollectionClasses.FareEvasionBehaviourCollection)info.GetValue("_fareEvasionBehaviours", typeof(VarioSL.Entities.CollectionClasses.FareEvasionBehaviourCollection));
			_alwaysFetchFareEvasionBehaviours = info.GetBoolean("_alwaysFetchFareEvasionBehaviours");
			_alreadyFetchedFareEvasionBehaviours = info.GetBoolean("_alreadyFetchedFareEvasionBehaviours");

			_fareEvasionIncidents = (VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection)info.GetValue("_fareEvasionIncidents", typeof(VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection));
			_alwaysFetchFareEvasionIncidents = info.GetBoolean("_alwaysFetchFareEvasionIncidents");
			_alreadyFetchedFareEvasionIncidents = info.GetBoolean("_alreadyFetchedFareEvasionIncidents");

			_fareEvasionInspections = (VarioSL.Entities.CollectionClasses.FareEvasionInspectionCollection)info.GetValue("_fareEvasionInspections", typeof(VarioSL.Entities.CollectionClasses.FareEvasionInspectionCollection));
			_alwaysFetchFareEvasionInspections = info.GetBoolean("_alwaysFetchFareEvasionInspections");
			_alreadyFetchedFareEvasionInspections = info.GetBoolean("_alreadyFetchedFareEvasionInspections");

			_filterValueSets = (VarioSL.Entities.CollectionClasses.FilterValueSetCollection)info.GetValue("_filterValueSets", typeof(VarioSL.Entities.CollectionClasses.FilterValueSetCollection));
			_alwaysFetchFilterValueSets = info.GetBoolean("_alwaysFetchFilterValueSets");
			_alreadyFetchedFilterValueSets = info.GetBoolean("_alreadyFetchedFilterValueSets");

			_forms = (VarioSL.Entities.CollectionClasses.FormCollection)info.GetValue("_forms", typeof(VarioSL.Entities.CollectionClasses.FormCollection));
			_alwaysFetchForms = info.GetBoolean("_alwaysFetchForms");
			_alreadyFetchedForms = info.GetBoolean("_alreadyFetchedForms");

			_identificationTypes = (VarioSL.Entities.CollectionClasses.IdentificationTypeCollection)info.GetValue("_identificationTypes", typeof(VarioSL.Entities.CollectionClasses.IdentificationTypeCollection));
			_alwaysFetchIdentificationTypes = info.GetBoolean("_alwaysFetchIdentificationTypes");
			_alreadyFetchedIdentificationTypes = info.GetBoolean("_alreadyFetchedIdentificationTypes");

			_invoices = (VarioSL.Entities.CollectionClasses.InvoiceCollection)info.GetValue("_invoices", typeof(VarioSL.Entities.CollectionClasses.InvoiceCollection));
			_alwaysFetchInvoices = info.GetBoolean("_alwaysFetchInvoices");
			_alreadyFetchedInvoices = info.GetBoolean("_alreadyFetchedInvoices");

			_invoicings = (VarioSL.Entities.CollectionClasses.InvoicingCollection)info.GetValue("_invoicings", typeof(VarioSL.Entities.CollectionClasses.InvoicingCollection));
			_alwaysFetchInvoicings = info.GetBoolean("_alwaysFetchInvoicings");
			_alreadyFetchedInvoicings = info.GetBoolean("_alreadyFetchedInvoicings");

			_jobs = (VarioSL.Entities.CollectionClasses.JobCollection)info.GetValue("_jobs", typeof(VarioSL.Entities.CollectionClasses.JobCollection));
			_alwaysFetchJobs = info.GetBoolean("_alwaysFetchJobs");
			_alreadyFetchedJobs = info.GetBoolean("_alreadyFetchedJobs");

			_parameterArchives = (VarioSL.Entities.CollectionClasses.ParameterArchiveCollection)info.GetValue("_parameterArchives", typeof(VarioSL.Entities.CollectionClasses.ParameterArchiveCollection));
			_alwaysFetchParameterArchives = info.GetBoolean("_alwaysFetchParameterArchives");
			_alreadyFetchedParameterArchives = info.GetBoolean("_alreadyFetchedParameterArchives");

			_postingKeys = (VarioSL.Entities.CollectionClasses.PostingKeyCollection)info.GetValue("_postingKeys", typeof(VarioSL.Entities.CollectionClasses.PostingKeyCollection));
			_alwaysFetchPostingKeys = info.GetBoolean("_alwaysFetchPostingKeys");
			_alreadyFetchedPostingKeys = info.GetBoolean("_alreadyFetchedPostingKeys");

			_products = (VarioSL.Entities.CollectionClasses.ProductCollection)info.GetValue("_products", typeof(VarioSL.Entities.CollectionClasses.ProductCollection));
			_alwaysFetchProducts = info.GetBoolean("_alwaysFetchProducts");
			_alreadyFetchedProducts = info.GetBoolean("_alreadyFetchedProducts");

			_reportJobs = (VarioSL.Entities.CollectionClasses.ReportJobCollection)info.GetValue("_reportJobs", typeof(VarioSL.Entities.CollectionClasses.ReportJobCollection));
			_alwaysFetchReportJobs = info.GetBoolean("_alwaysFetchReportJobs");
			_alreadyFetchedReportJobs = info.GetBoolean("_alreadyFetchedReportJobs");

			_reportJobResults = (VarioSL.Entities.CollectionClasses.ReportJobResultCollection)info.GetValue("_reportJobResults", typeof(VarioSL.Entities.CollectionClasses.ReportJobResultCollection));
			_alwaysFetchReportJobResults = info.GetBoolean("_alwaysFetchReportJobResults");
			_alreadyFetchedReportJobResults = info.GetBoolean("_alreadyFetchedReportJobResults");

			_reportToClients = (VarioSL.Entities.CollectionClasses.ReportToClientCollection)info.GetValue("_reportToClients", typeof(VarioSL.Entities.CollectionClasses.ReportToClientCollection));
			_alwaysFetchReportToClients = info.GetBoolean("_alwaysFetchReportToClients");
			_alreadyFetchedReportToClients = info.GetBoolean("_alreadyFetchedReportToClients");

			_revenueSettlements = (VarioSL.Entities.CollectionClasses.RevenueSettlementCollection)info.GetValue("_revenueSettlements", typeof(VarioSL.Entities.CollectionClasses.RevenueSettlementCollection));
			_alwaysFetchRevenueSettlements = info.GetBoolean("_alwaysFetchRevenueSettlements");
			_alreadyFetchedRevenueSettlements = info.GetBoolean("_alreadyFetchedRevenueSettlements");

			_sales = (VarioSL.Entities.CollectionClasses.SaleCollection)info.GetValue("_sales", typeof(VarioSL.Entities.CollectionClasses.SaleCollection));
			_alwaysFetchSales = info.GetBoolean("_alwaysFetchSales");
			_alreadyFetchedSales = info.GetBoolean("_alreadyFetchedSales");

			_schoolYears = (VarioSL.Entities.CollectionClasses.SchoolYearCollection)info.GetValue("_schoolYears", typeof(VarioSL.Entities.CollectionClasses.SchoolYearCollection));
			_alwaysFetchSchoolYears = info.GetBoolean("_alwaysFetchSchoolYears");
			_alreadyFetchedSchoolYears = info.GetBoolean("_alreadyFetchedSchoolYears");

			_storageLocations = (VarioSL.Entities.CollectionClasses.StorageLocationCollection)info.GetValue("_storageLocations", typeof(VarioSL.Entities.CollectionClasses.StorageLocationCollection));
			_alwaysFetchStorageLocations = info.GetBoolean("_alwaysFetchStorageLocations");
			_alreadyFetchedStorageLocations = info.GetBoolean("_alreadyFetchedStorageLocations");

			_transactionJournals = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_transactionJournals", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTransactionJournals = info.GetBoolean("_alwaysFetchTransactionJournals");
			_alreadyFetchedTransactionJournals = info.GetBoolean("_alreadyFetchedTransactionJournals");

			_transportCompanies = (VarioSL.Entities.CollectionClasses.TransportCompanyCollection)info.GetValue("_transportCompanies", typeof(VarioSL.Entities.CollectionClasses.TransportCompanyCollection));
			_alwaysFetchTransportCompanies = info.GetBoolean("_alwaysFetchTransportCompanies");
			_alreadyFetchedTransportCompanies = info.GetBoolean("_alreadyFetchedTransportCompanies");
			_ticketCollectionViaTicketVendingClient = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketCollectionViaTicketVendingClient", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketCollectionViaTicketVendingClient = info.GetBoolean("_alwaysFetchTicketCollectionViaTicketVendingClient");
			_alreadyFetchedTicketCollectionViaTicketVendingClient = info.GetBoolean("_alreadyFetchedTicketCollectionViaTicketVendingClient");

			_reportCollectionViaReportToClient = (VarioSL.Entities.CollectionClasses.ReportCollection)info.GetValue("_reportCollectionViaReportToClient", typeof(VarioSL.Entities.CollectionClasses.ReportCollection));
			_alwaysFetchReportCollectionViaReportToClient = info.GetBoolean("_alwaysFetchReportCollectionViaReportToClient");
			_alreadyFetchedReportCollectionViaReportToClient = info.GetBoolean("_alreadyFetchedReportCollectionViaReportToClient");
			_accountEntryNumber = (AccountEntryNumberEntity)info.GetValue("_accountEntryNumber", typeof(AccountEntryNumberEntity));
			if(_accountEntryNumber!=null)
			{
				_accountEntryNumber.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_accountEntryNumberReturnsNewIfNotFound = info.GetBoolean("_accountEntryNumberReturnsNewIfNotFound");
			_alwaysFetchAccountEntryNumber = info.GetBoolean("_alwaysFetchAccountEntryNumber");
			_alreadyFetchedAccountEntryNumber = info.GetBoolean("_alreadyFetchedAccountEntryNumber");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ClientFieldIndex)fieldIndex)
			{
				case ClientFieldIndex.ClientID:
					DesetupSyncAccountEntryNumber(true, false);
					_alreadyFetchedAccountEntryNumber = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAccountEntries = (_accountEntries.Count > 0);
			_alreadyFetchedApportionmentResults = (_apportionmentResults.Count > 0);
			_alreadyFetchedApportionmentResults1 = (_apportionmentResults1.Count > 0);
			_alreadyFetchedApportionmentResults2 = (_apportionmentResults2.Count > 0);
			_alreadyFetchedCalendar = (_calendar.Count > 0);
			_alreadyFetchedClearingResultsByCardIssuer = (_clearingResultsByCardIssuer.Count > 0);
			_alreadyFetchedClearingResults = (_clearingResults.Count > 0);
			_alreadyFetchedClearingResultsByClientFrom = (_clearingResultsByClientFrom.Count > 0);
			_alreadyFetchedClearingResultsByClientTo = (_clearingResultsByClientTo.Count > 0);
			_alreadyFetchedClientAdaptedLayoutObjects = (_clientAdaptedLayoutObjects.Count > 0);
			_alreadyFetchedDebtors = (_debtors.Count > 0);
			_alreadyFetchedDepots = (_depots.Count > 0);
			_alreadyFetchedFareMatrixes = (_fareMatrixes.Count > 0);
			_alreadyFetchedFareTables = (_fareTables.Count > 0);
			_alreadyFetchedLayouts = (_layouts.Count > 0);
			_alreadyFetchedWhoIsResponsible = (_whoIsResponsible.Count > 0);
			_alreadyFetchedResponsibleFor = (_responsibleFor.Count > 0);
			_alreadyFetchedClientTariffs = (_clientTariffs.Count > 0);
			_alreadyFetchedOwnerClientTariffs = (_ownerClientTariffs.Count > 0);
			_alreadyFetchedTickets = (_tickets.Count > 0);
			_alreadyFetchedTicketCategories = (_ticketCategories.Count > 0);
			_alreadyFetchedTicketVendingClient = (_ticketVendingClient.Count > 0);
			_alreadyFetchedCardIssuerCardTransactions = (_cardIssuerCardTransactions.Count > 0);
			_alreadyFetchedUmUnits = (_umUnits.Count > 0);
			_alreadyFetchedUsers = (_users.Count > 0);
			_alreadyFetchedSettlements = (_settlements.Count > 0);
			_alreadyFetchedVdvKeySet = (_vdvKeySet.Count > 0);
			_alreadyFetchedWorkstations = (_workstations.Count > 0);
			_alreadyFetchedAddressBookEntries = (_addressBookEntries.Count > 0);
			_alreadyFetchedCards = (_cards.Count > 0);
			_alreadyFetchedConfigurationDefinitions = (_configurationDefinitions.Count > 0);
			_alreadyFetchedConfigurationOverwrites = (_configurationOverwrites.Count > 0);
			_alreadyFetchedContracts = (_contracts.Count > 0);
			_alreadyFetchedVanpoolContracts = (_vanpoolContracts.Count > 0);
			_alreadyFetchedCryptoCryptogramArchives = (_cryptoCryptogramArchives.Count > 0);
			_alreadyFetchedCryptoQArchives = (_cryptoQArchives.Count > 0);
			_alreadyFetchedFareChangeCauses = (_fareChangeCauses.Count > 0);
			_alreadyFetchedFareEvasionBehaviours = (_fareEvasionBehaviours.Count > 0);
			_alreadyFetchedFareEvasionIncidents = (_fareEvasionIncidents.Count > 0);
			_alreadyFetchedFareEvasionInspections = (_fareEvasionInspections.Count > 0);
			_alreadyFetchedFilterValueSets = (_filterValueSets.Count > 0);
			_alreadyFetchedForms = (_forms.Count > 0);
			_alreadyFetchedIdentificationTypes = (_identificationTypes.Count > 0);
			_alreadyFetchedInvoices = (_invoices.Count > 0);
			_alreadyFetchedInvoicings = (_invoicings.Count > 0);
			_alreadyFetchedJobs = (_jobs.Count > 0);
			_alreadyFetchedParameterArchives = (_parameterArchives.Count > 0);
			_alreadyFetchedPostingKeys = (_postingKeys.Count > 0);
			_alreadyFetchedProducts = (_products.Count > 0);
			_alreadyFetchedReportJobs = (_reportJobs.Count > 0);
			_alreadyFetchedReportJobResults = (_reportJobResults.Count > 0);
			_alreadyFetchedReportToClients = (_reportToClients.Count > 0);
			_alreadyFetchedRevenueSettlements = (_revenueSettlements.Count > 0);
			_alreadyFetchedSales = (_sales.Count > 0);
			_alreadyFetchedSchoolYears = (_schoolYears.Count > 0);
			_alreadyFetchedStorageLocations = (_storageLocations.Count > 0);
			_alreadyFetchedTransactionJournals = (_transactionJournals.Count > 0);
			_alreadyFetchedTransportCompanies = (_transportCompanies.Count > 0);
			_alreadyFetchedTicketCollectionViaTicketVendingClient = (_ticketCollectionViaTicketVendingClient.Count > 0);
			_alreadyFetchedReportCollectionViaReportToClient = (_reportCollectionViaReportToClient.Count > 0);
			_alreadyFetchedAccountEntryNumber = (_accountEntryNumber != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AccountEntries":
					toReturn.Add(Relations.AccountEntryEntityUsingClientID);
					break;
				case "ApportionmentResults":
					toReturn.Add(Relations.ApportionmentResultEntityUsingAcquirerID);
					break;
				case "ApportionmentResults1":
					toReturn.Add(Relations.ApportionmentResultEntityUsingFromClientID);
					break;
				case "ApportionmentResults2":
					toReturn.Add(Relations.ApportionmentResultEntityUsingToClientID);
					break;
				case "Calendar":
					toReturn.Add(Relations.CalendarEntityUsingOwnerClientId);
					break;
				case "ClearingResultsByCardIssuer":
					toReturn.Add(Relations.ClearingResultEntityUsingCardIssuerID);
					break;
				case "ClearingResults":
					toReturn.Add(Relations.ClearingResultEntityUsingClientID);
					break;
				case "ClearingResultsByClientFrom":
					toReturn.Add(Relations.ClearingResultEntityUsingFromClient);
					break;
				case "ClearingResultsByClientTo":
					toReturn.Add(Relations.ClearingResultEntityUsingToClient);
					break;
				case "ClientAdaptedLayoutObjects":
					toReturn.Add(Relations.ClientAdaptedLayoutObjectEntityUsingClientID);
					break;
				case "Debtors":
					toReturn.Add(Relations.DebtorEntityUsingClientID);
					break;
				case "Depots":
					toReturn.Add(Relations.DepotEntityUsingClientID);
					break;
				case "FareMatrixes":
					toReturn.Add(Relations.FareMatrixEntityUsingOwnerClientID);
					break;
				case "FareTables":
					toReturn.Add(Relations.FareTableEntityUsingOwnerCLientID);
					break;
				case "Layouts":
					toReturn.Add(Relations.LayoutEntityUsingOwnerClientID);
					break;
				case "WhoIsResponsible":
					toReturn.Add(Relations.ResponsibilityEntityUsingClientID);
					break;
				case "ResponsibleFor":
					toReturn.Add(Relations.ResponsibilityEntityUsingResponsibleClientID);
					break;
				case "ClientTariffs":
					toReturn.Add(Relations.TariffEntityUsingClientID);
					break;
				case "OwnerClientTariffs":
					toReturn.Add(Relations.TariffEntityUsingOwnerClientID);
					break;
				case "Tickets":
					toReturn.Add(Relations.TicketEntityUsingOwnerClientID);
					break;
				case "TicketCategories":
					toReturn.Add(Relations.TicketCategoryEntityUsingClientID);
					break;
				case "TicketVendingClient":
					toReturn.Add(Relations.TicketVendingClientEntityUsingClientid);
					break;
				case "CardIssuerCardTransactions":
					toReturn.Add(Relations.TransactionEntityUsingCardIssuerID);
					break;
				case "UmUnits":
					toReturn.Add(Relations.UnitEntityUsingClientID);
					break;
				case "Users":
					toReturn.Add(Relations.UserListEntityUsingClientID);
					break;
				case "Settlements":
					toReturn.Add(Relations.VarioSettlementEntityUsingClientID);
					break;
				case "VdvKeySet":
					toReturn.Add(Relations.VdvKeySetEntityUsingClientID);
					break;
				case "Workstations":
					toReturn.Add(Relations.WorkstationEntityUsingClientID);
					break;
				case "AddressBookEntries":
					toReturn.Add(Relations.AddressBookEntryEntityUsingClientID);
					break;
				case "Cards":
					toReturn.Add(Relations.CardEntityUsingClientID);
					break;
				case "ConfigurationDefinitions":
					toReturn.Add(Relations.ConfigurationDefinitionEntityUsingOwnerClientID);
					break;
				case "ConfigurationOverwrites":
					toReturn.Add(Relations.ConfigurationOverwriteEntityUsingClientID);
					break;
				case "Contracts":
					toReturn.Add(Relations.ContractEntityUsingClientID);
					break;
				case "VanpoolContracts":
					toReturn.Add(Relations.ContractEntityUsingVanpoolClientID);
					break;
				case "CryptoCryptogramArchives":
					toReturn.Add(Relations.CryptoCryptogramArchiveEntityUsingClientID);
					break;
				case "CryptoQArchives":
					toReturn.Add(Relations.CryptoQArchiveEntityUsingClientID);
					break;
				case "FareChangeCauses":
					toReturn.Add(Relations.FareChangeCauseEntityUsingClientID);
					break;
				case "FareEvasionBehaviours":
					toReturn.Add(Relations.FareEvasionBehaviourEntityUsingClientID);
					break;
				case "FareEvasionIncidents":
					toReturn.Add(Relations.FareEvasionIncidentEntityUsingClientID);
					break;
				case "FareEvasionInspections":
					toReturn.Add(Relations.FareEvasionInspectionEntityUsingClientID);
					break;
				case "FilterValueSets":
					toReturn.Add(Relations.FilterValueSetEntityUsingClientID);
					break;
				case "Forms":
					toReturn.Add(Relations.FormEntityUsingClientID);
					break;
				case "IdentificationTypes":
					toReturn.Add(Relations.IdentificationTypeEntityUsingClientID);
					break;
				case "Invoices":
					toReturn.Add(Relations.InvoiceEntityUsingClientID);
					break;
				case "Invoicings":
					toReturn.Add(Relations.InvoicingEntityUsingClientID);
					break;
				case "Jobs":
					toReturn.Add(Relations.JobEntityUsingClientID);
					break;
				case "ParameterArchives":
					toReturn.Add(Relations.ParameterArchiveEntityUsingClientID);
					break;
				case "PostingKeys":
					toReturn.Add(Relations.PostingKeyEntityUsingClientID);
					break;
				case "Products":
					toReturn.Add(Relations.ProductEntityUsingClientID);
					break;
				case "ReportJobs":
					toReturn.Add(Relations.ReportJobEntityUsingClientID);
					break;
				case "ReportJobResults":
					toReturn.Add(Relations.ReportJobResultEntityUsingClientID);
					break;
				case "ReportToClients":
					toReturn.Add(Relations.ReportToClientEntityUsingClientID);
					break;
				case "RevenueSettlements":
					toReturn.Add(Relations.RevenueSettlementEntityUsingClientID);
					break;
				case "Sales":
					toReturn.Add(Relations.SaleEntityUsingClientID);
					break;
				case "SchoolYears":
					toReturn.Add(Relations.SchoolYearEntityUsingClientID);
					break;
				case "StorageLocations":
					toReturn.Add(Relations.StorageLocationEntityUsingClientID);
					break;
				case "TransactionJournals":
					toReturn.Add(Relations.TransactionJournalEntityUsingClientID);
					break;
				case "TransportCompanies":
					toReturn.Add(Relations.TransportCompanyEntityUsingClientID);
					break;
				case "TicketCollectionViaTicketVendingClient":
					toReturn.Add(Relations.TicketVendingClientEntityUsingClientid, "ClientEntity__", "TicketVendingClient_", JoinHint.None);
					toReturn.Add(TicketVendingClientEntity.Relations.TicketEntityUsingTicketid, "TicketVendingClient_", string.Empty, JoinHint.None);
					break;
				case "ReportCollectionViaReportToClient":
					toReturn.Add(Relations.ReportToClientEntityUsingClientID, "ClientEntity__", "ReportToClient_", JoinHint.None);
					toReturn.Add(ReportToClientEntity.Relations.ReportEntityUsingReportID, "ReportToClient_", string.Empty, JoinHint.None);
					break;
				case "AccountEntryNumber":
					toReturn.Add(Relations.AccountEntryNumberEntityUsingClientID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_accountEntries", (!this.MarkedForDeletion?_accountEntries:null));
			info.AddValue("_alwaysFetchAccountEntries", _alwaysFetchAccountEntries);
			info.AddValue("_alreadyFetchedAccountEntries", _alreadyFetchedAccountEntries);
			info.AddValue("_apportionmentResults", (!this.MarkedForDeletion?_apportionmentResults:null));
			info.AddValue("_alwaysFetchApportionmentResults", _alwaysFetchApportionmentResults);
			info.AddValue("_alreadyFetchedApportionmentResults", _alreadyFetchedApportionmentResults);
			info.AddValue("_apportionmentResults1", (!this.MarkedForDeletion?_apportionmentResults1:null));
			info.AddValue("_alwaysFetchApportionmentResults1", _alwaysFetchApportionmentResults1);
			info.AddValue("_alreadyFetchedApportionmentResults1", _alreadyFetchedApportionmentResults1);
			info.AddValue("_apportionmentResults2", (!this.MarkedForDeletion?_apportionmentResults2:null));
			info.AddValue("_alwaysFetchApportionmentResults2", _alwaysFetchApportionmentResults2);
			info.AddValue("_alreadyFetchedApportionmentResults2", _alreadyFetchedApportionmentResults2);
			info.AddValue("_calendar", (!this.MarkedForDeletion?_calendar:null));
			info.AddValue("_alwaysFetchCalendar", _alwaysFetchCalendar);
			info.AddValue("_alreadyFetchedCalendar", _alreadyFetchedCalendar);
			info.AddValue("_clearingResultsByCardIssuer", (!this.MarkedForDeletion?_clearingResultsByCardIssuer:null));
			info.AddValue("_alwaysFetchClearingResultsByCardIssuer", _alwaysFetchClearingResultsByCardIssuer);
			info.AddValue("_alreadyFetchedClearingResultsByCardIssuer", _alreadyFetchedClearingResultsByCardIssuer);
			info.AddValue("_clearingResults", (!this.MarkedForDeletion?_clearingResults:null));
			info.AddValue("_alwaysFetchClearingResults", _alwaysFetchClearingResults);
			info.AddValue("_alreadyFetchedClearingResults", _alreadyFetchedClearingResults);
			info.AddValue("_clearingResultsByClientFrom", (!this.MarkedForDeletion?_clearingResultsByClientFrom:null));
			info.AddValue("_alwaysFetchClearingResultsByClientFrom", _alwaysFetchClearingResultsByClientFrom);
			info.AddValue("_alreadyFetchedClearingResultsByClientFrom", _alreadyFetchedClearingResultsByClientFrom);
			info.AddValue("_clearingResultsByClientTo", (!this.MarkedForDeletion?_clearingResultsByClientTo:null));
			info.AddValue("_alwaysFetchClearingResultsByClientTo", _alwaysFetchClearingResultsByClientTo);
			info.AddValue("_alreadyFetchedClearingResultsByClientTo", _alreadyFetchedClearingResultsByClientTo);
			info.AddValue("_clientAdaptedLayoutObjects", (!this.MarkedForDeletion?_clientAdaptedLayoutObjects:null));
			info.AddValue("_alwaysFetchClientAdaptedLayoutObjects", _alwaysFetchClientAdaptedLayoutObjects);
			info.AddValue("_alreadyFetchedClientAdaptedLayoutObjects", _alreadyFetchedClientAdaptedLayoutObjects);
			info.AddValue("_debtors", (!this.MarkedForDeletion?_debtors:null));
			info.AddValue("_alwaysFetchDebtors", _alwaysFetchDebtors);
			info.AddValue("_alreadyFetchedDebtors", _alreadyFetchedDebtors);
			info.AddValue("_depots", (!this.MarkedForDeletion?_depots:null));
			info.AddValue("_alwaysFetchDepots", _alwaysFetchDepots);
			info.AddValue("_alreadyFetchedDepots", _alreadyFetchedDepots);
			info.AddValue("_fareMatrixes", (!this.MarkedForDeletion?_fareMatrixes:null));
			info.AddValue("_alwaysFetchFareMatrixes", _alwaysFetchFareMatrixes);
			info.AddValue("_alreadyFetchedFareMatrixes", _alreadyFetchedFareMatrixes);
			info.AddValue("_fareTables", (!this.MarkedForDeletion?_fareTables:null));
			info.AddValue("_alwaysFetchFareTables", _alwaysFetchFareTables);
			info.AddValue("_alreadyFetchedFareTables", _alreadyFetchedFareTables);
			info.AddValue("_layouts", (!this.MarkedForDeletion?_layouts:null));
			info.AddValue("_alwaysFetchLayouts", _alwaysFetchLayouts);
			info.AddValue("_alreadyFetchedLayouts", _alreadyFetchedLayouts);
			info.AddValue("_whoIsResponsible", (!this.MarkedForDeletion?_whoIsResponsible:null));
			info.AddValue("_alwaysFetchWhoIsResponsible", _alwaysFetchWhoIsResponsible);
			info.AddValue("_alreadyFetchedWhoIsResponsible", _alreadyFetchedWhoIsResponsible);
			info.AddValue("_responsibleFor", (!this.MarkedForDeletion?_responsibleFor:null));
			info.AddValue("_alwaysFetchResponsibleFor", _alwaysFetchResponsibleFor);
			info.AddValue("_alreadyFetchedResponsibleFor", _alreadyFetchedResponsibleFor);
			info.AddValue("_clientTariffs", (!this.MarkedForDeletion?_clientTariffs:null));
			info.AddValue("_alwaysFetchClientTariffs", _alwaysFetchClientTariffs);
			info.AddValue("_alreadyFetchedClientTariffs", _alreadyFetchedClientTariffs);
			info.AddValue("_ownerClientTariffs", (!this.MarkedForDeletion?_ownerClientTariffs:null));
			info.AddValue("_alwaysFetchOwnerClientTariffs", _alwaysFetchOwnerClientTariffs);
			info.AddValue("_alreadyFetchedOwnerClientTariffs", _alreadyFetchedOwnerClientTariffs);
			info.AddValue("_tickets", (!this.MarkedForDeletion?_tickets:null));
			info.AddValue("_alwaysFetchTickets", _alwaysFetchTickets);
			info.AddValue("_alreadyFetchedTickets", _alreadyFetchedTickets);
			info.AddValue("_ticketCategories", (!this.MarkedForDeletion?_ticketCategories:null));
			info.AddValue("_alwaysFetchTicketCategories", _alwaysFetchTicketCategories);
			info.AddValue("_alreadyFetchedTicketCategories", _alreadyFetchedTicketCategories);
			info.AddValue("_ticketVendingClient", (!this.MarkedForDeletion?_ticketVendingClient:null));
			info.AddValue("_alwaysFetchTicketVendingClient", _alwaysFetchTicketVendingClient);
			info.AddValue("_alreadyFetchedTicketVendingClient", _alreadyFetchedTicketVendingClient);
			info.AddValue("_cardIssuerCardTransactions", (!this.MarkedForDeletion?_cardIssuerCardTransactions:null));
			info.AddValue("_alwaysFetchCardIssuerCardTransactions", _alwaysFetchCardIssuerCardTransactions);
			info.AddValue("_alreadyFetchedCardIssuerCardTransactions", _alreadyFetchedCardIssuerCardTransactions);
			info.AddValue("_umUnits", (!this.MarkedForDeletion?_umUnits:null));
			info.AddValue("_alwaysFetchUmUnits", _alwaysFetchUmUnits);
			info.AddValue("_alreadyFetchedUmUnits", _alreadyFetchedUmUnits);
			info.AddValue("_users", (!this.MarkedForDeletion?_users:null));
			info.AddValue("_alwaysFetchUsers", _alwaysFetchUsers);
			info.AddValue("_alreadyFetchedUsers", _alreadyFetchedUsers);
			info.AddValue("_settlements", (!this.MarkedForDeletion?_settlements:null));
			info.AddValue("_alwaysFetchSettlements", _alwaysFetchSettlements);
			info.AddValue("_alreadyFetchedSettlements", _alreadyFetchedSettlements);
			info.AddValue("_vdvKeySet", (!this.MarkedForDeletion?_vdvKeySet:null));
			info.AddValue("_alwaysFetchVdvKeySet", _alwaysFetchVdvKeySet);
			info.AddValue("_alreadyFetchedVdvKeySet", _alreadyFetchedVdvKeySet);
			info.AddValue("_workstations", (!this.MarkedForDeletion?_workstations:null));
			info.AddValue("_alwaysFetchWorkstations", _alwaysFetchWorkstations);
			info.AddValue("_alreadyFetchedWorkstations", _alreadyFetchedWorkstations);
			info.AddValue("_addressBookEntries", (!this.MarkedForDeletion?_addressBookEntries:null));
			info.AddValue("_alwaysFetchAddressBookEntries", _alwaysFetchAddressBookEntries);
			info.AddValue("_alreadyFetchedAddressBookEntries", _alreadyFetchedAddressBookEntries);
			info.AddValue("_cards", (!this.MarkedForDeletion?_cards:null));
			info.AddValue("_alwaysFetchCards", _alwaysFetchCards);
			info.AddValue("_alreadyFetchedCards", _alreadyFetchedCards);
			info.AddValue("_configurationDefinitions", (!this.MarkedForDeletion?_configurationDefinitions:null));
			info.AddValue("_alwaysFetchConfigurationDefinitions", _alwaysFetchConfigurationDefinitions);
			info.AddValue("_alreadyFetchedConfigurationDefinitions", _alreadyFetchedConfigurationDefinitions);
			info.AddValue("_configurationOverwrites", (!this.MarkedForDeletion?_configurationOverwrites:null));
			info.AddValue("_alwaysFetchConfigurationOverwrites", _alwaysFetchConfigurationOverwrites);
			info.AddValue("_alreadyFetchedConfigurationOverwrites", _alreadyFetchedConfigurationOverwrites);
			info.AddValue("_contracts", (!this.MarkedForDeletion?_contracts:null));
			info.AddValue("_alwaysFetchContracts", _alwaysFetchContracts);
			info.AddValue("_alreadyFetchedContracts", _alreadyFetchedContracts);
			info.AddValue("_vanpoolContracts", (!this.MarkedForDeletion?_vanpoolContracts:null));
			info.AddValue("_alwaysFetchVanpoolContracts", _alwaysFetchVanpoolContracts);
			info.AddValue("_alreadyFetchedVanpoolContracts", _alreadyFetchedVanpoolContracts);
			info.AddValue("_cryptoCryptogramArchives", (!this.MarkedForDeletion?_cryptoCryptogramArchives:null));
			info.AddValue("_alwaysFetchCryptoCryptogramArchives", _alwaysFetchCryptoCryptogramArchives);
			info.AddValue("_alreadyFetchedCryptoCryptogramArchives", _alreadyFetchedCryptoCryptogramArchives);
			info.AddValue("_cryptoQArchives", (!this.MarkedForDeletion?_cryptoQArchives:null));
			info.AddValue("_alwaysFetchCryptoQArchives", _alwaysFetchCryptoQArchives);
			info.AddValue("_alreadyFetchedCryptoQArchives", _alreadyFetchedCryptoQArchives);
			info.AddValue("_fareChangeCauses", (!this.MarkedForDeletion?_fareChangeCauses:null));
			info.AddValue("_alwaysFetchFareChangeCauses", _alwaysFetchFareChangeCauses);
			info.AddValue("_alreadyFetchedFareChangeCauses", _alreadyFetchedFareChangeCauses);
			info.AddValue("_fareEvasionBehaviours", (!this.MarkedForDeletion?_fareEvasionBehaviours:null));
			info.AddValue("_alwaysFetchFareEvasionBehaviours", _alwaysFetchFareEvasionBehaviours);
			info.AddValue("_alreadyFetchedFareEvasionBehaviours", _alreadyFetchedFareEvasionBehaviours);
			info.AddValue("_fareEvasionIncidents", (!this.MarkedForDeletion?_fareEvasionIncidents:null));
			info.AddValue("_alwaysFetchFareEvasionIncidents", _alwaysFetchFareEvasionIncidents);
			info.AddValue("_alreadyFetchedFareEvasionIncidents", _alreadyFetchedFareEvasionIncidents);
			info.AddValue("_fareEvasionInspections", (!this.MarkedForDeletion?_fareEvasionInspections:null));
			info.AddValue("_alwaysFetchFareEvasionInspections", _alwaysFetchFareEvasionInspections);
			info.AddValue("_alreadyFetchedFareEvasionInspections", _alreadyFetchedFareEvasionInspections);
			info.AddValue("_filterValueSets", (!this.MarkedForDeletion?_filterValueSets:null));
			info.AddValue("_alwaysFetchFilterValueSets", _alwaysFetchFilterValueSets);
			info.AddValue("_alreadyFetchedFilterValueSets", _alreadyFetchedFilterValueSets);
			info.AddValue("_forms", (!this.MarkedForDeletion?_forms:null));
			info.AddValue("_alwaysFetchForms", _alwaysFetchForms);
			info.AddValue("_alreadyFetchedForms", _alreadyFetchedForms);
			info.AddValue("_identificationTypes", (!this.MarkedForDeletion?_identificationTypes:null));
			info.AddValue("_alwaysFetchIdentificationTypes", _alwaysFetchIdentificationTypes);
			info.AddValue("_alreadyFetchedIdentificationTypes", _alreadyFetchedIdentificationTypes);
			info.AddValue("_invoices", (!this.MarkedForDeletion?_invoices:null));
			info.AddValue("_alwaysFetchInvoices", _alwaysFetchInvoices);
			info.AddValue("_alreadyFetchedInvoices", _alreadyFetchedInvoices);
			info.AddValue("_invoicings", (!this.MarkedForDeletion?_invoicings:null));
			info.AddValue("_alwaysFetchInvoicings", _alwaysFetchInvoicings);
			info.AddValue("_alreadyFetchedInvoicings", _alreadyFetchedInvoicings);
			info.AddValue("_jobs", (!this.MarkedForDeletion?_jobs:null));
			info.AddValue("_alwaysFetchJobs", _alwaysFetchJobs);
			info.AddValue("_alreadyFetchedJobs", _alreadyFetchedJobs);
			info.AddValue("_parameterArchives", (!this.MarkedForDeletion?_parameterArchives:null));
			info.AddValue("_alwaysFetchParameterArchives", _alwaysFetchParameterArchives);
			info.AddValue("_alreadyFetchedParameterArchives", _alreadyFetchedParameterArchives);
			info.AddValue("_postingKeys", (!this.MarkedForDeletion?_postingKeys:null));
			info.AddValue("_alwaysFetchPostingKeys", _alwaysFetchPostingKeys);
			info.AddValue("_alreadyFetchedPostingKeys", _alreadyFetchedPostingKeys);
			info.AddValue("_products", (!this.MarkedForDeletion?_products:null));
			info.AddValue("_alwaysFetchProducts", _alwaysFetchProducts);
			info.AddValue("_alreadyFetchedProducts", _alreadyFetchedProducts);
			info.AddValue("_reportJobs", (!this.MarkedForDeletion?_reportJobs:null));
			info.AddValue("_alwaysFetchReportJobs", _alwaysFetchReportJobs);
			info.AddValue("_alreadyFetchedReportJobs", _alreadyFetchedReportJobs);
			info.AddValue("_reportJobResults", (!this.MarkedForDeletion?_reportJobResults:null));
			info.AddValue("_alwaysFetchReportJobResults", _alwaysFetchReportJobResults);
			info.AddValue("_alreadyFetchedReportJobResults", _alreadyFetchedReportJobResults);
			info.AddValue("_reportToClients", (!this.MarkedForDeletion?_reportToClients:null));
			info.AddValue("_alwaysFetchReportToClients", _alwaysFetchReportToClients);
			info.AddValue("_alreadyFetchedReportToClients", _alreadyFetchedReportToClients);
			info.AddValue("_revenueSettlements", (!this.MarkedForDeletion?_revenueSettlements:null));
			info.AddValue("_alwaysFetchRevenueSettlements", _alwaysFetchRevenueSettlements);
			info.AddValue("_alreadyFetchedRevenueSettlements", _alreadyFetchedRevenueSettlements);
			info.AddValue("_sales", (!this.MarkedForDeletion?_sales:null));
			info.AddValue("_alwaysFetchSales", _alwaysFetchSales);
			info.AddValue("_alreadyFetchedSales", _alreadyFetchedSales);
			info.AddValue("_schoolYears", (!this.MarkedForDeletion?_schoolYears:null));
			info.AddValue("_alwaysFetchSchoolYears", _alwaysFetchSchoolYears);
			info.AddValue("_alreadyFetchedSchoolYears", _alreadyFetchedSchoolYears);
			info.AddValue("_storageLocations", (!this.MarkedForDeletion?_storageLocations:null));
			info.AddValue("_alwaysFetchStorageLocations", _alwaysFetchStorageLocations);
			info.AddValue("_alreadyFetchedStorageLocations", _alreadyFetchedStorageLocations);
			info.AddValue("_transactionJournals", (!this.MarkedForDeletion?_transactionJournals:null));
			info.AddValue("_alwaysFetchTransactionJournals", _alwaysFetchTransactionJournals);
			info.AddValue("_alreadyFetchedTransactionJournals", _alreadyFetchedTransactionJournals);
			info.AddValue("_transportCompanies", (!this.MarkedForDeletion?_transportCompanies:null));
			info.AddValue("_alwaysFetchTransportCompanies", _alwaysFetchTransportCompanies);
			info.AddValue("_alreadyFetchedTransportCompanies", _alreadyFetchedTransportCompanies);
			info.AddValue("_ticketCollectionViaTicketVendingClient", (!this.MarkedForDeletion?_ticketCollectionViaTicketVendingClient:null));
			info.AddValue("_alwaysFetchTicketCollectionViaTicketVendingClient", _alwaysFetchTicketCollectionViaTicketVendingClient);
			info.AddValue("_alreadyFetchedTicketCollectionViaTicketVendingClient", _alreadyFetchedTicketCollectionViaTicketVendingClient);
			info.AddValue("_reportCollectionViaReportToClient", (!this.MarkedForDeletion?_reportCollectionViaReportToClient:null));
			info.AddValue("_alwaysFetchReportCollectionViaReportToClient", _alwaysFetchReportCollectionViaReportToClient);
			info.AddValue("_alreadyFetchedReportCollectionViaReportToClient", _alreadyFetchedReportCollectionViaReportToClient);

			info.AddValue("_accountEntryNumber", (!this.MarkedForDeletion?_accountEntryNumber:null));
			info.AddValue("_accountEntryNumberReturnsNewIfNotFound", _accountEntryNumberReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAccountEntryNumber", _alwaysFetchAccountEntryNumber);
			info.AddValue("_alreadyFetchedAccountEntryNumber", _alreadyFetchedAccountEntryNumber);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AccountEntries":
					_alreadyFetchedAccountEntries = true;
					if(entity!=null)
					{
						this.AccountEntries.Add((AccountEntryEntity)entity);
					}
					break;
				case "ApportionmentResults":
					_alreadyFetchedApportionmentResults = true;
					if(entity!=null)
					{
						this.ApportionmentResults.Add((ApportionmentResultEntity)entity);
					}
					break;
				case "ApportionmentResults1":
					_alreadyFetchedApportionmentResults1 = true;
					if(entity!=null)
					{
						this.ApportionmentResults1.Add((ApportionmentResultEntity)entity);
					}
					break;
				case "ApportionmentResults2":
					_alreadyFetchedApportionmentResults2 = true;
					if(entity!=null)
					{
						this.ApportionmentResults2.Add((ApportionmentResultEntity)entity);
					}
					break;
				case "Calendar":
					_alreadyFetchedCalendar = true;
					if(entity!=null)
					{
						this.Calendar.Add((CalendarEntity)entity);
					}
					break;
				case "ClearingResultsByCardIssuer":
					_alreadyFetchedClearingResultsByCardIssuer = true;
					if(entity!=null)
					{
						this.ClearingResultsByCardIssuer.Add((ClearingResultEntity)entity);
					}
					break;
				case "ClearingResults":
					_alreadyFetchedClearingResults = true;
					if(entity!=null)
					{
						this.ClearingResults.Add((ClearingResultEntity)entity);
					}
					break;
				case "ClearingResultsByClientFrom":
					_alreadyFetchedClearingResultsByClientFrom = true;
					if(entity!=null)
					{
						this.ClearingResultsByClientFrom.Add((ClearingResultEntity)entity);
					}
					break;
				case "ClearingResultsByClientTo":
					_alreadyFetchedClearingResultsByClientTo = true;
					if(entity!=null)
					{
						this.ClearingResultsByClientTo.Add((ClearingResultEntity)entity);
					}
					break;
				case "ClientAdaptedLayoutObjects":
					_alreadyFetchedClientAdaptedLayoutObjects = true;
					if(entity!=null)
					{
						this.ClientAdaptedLayoutObjects.Add((ClientAdaptedLayoutObjectEntity)entity);
					}
					break;
				case "Debtors":
					_alreadyFetchedDebtors = true;
					if(entity!=null)
					{
						this.Debtors.Add((DebtorEntity)entity);
					}
					break;
				case "Depots":
					_alreadyFetchedDepots = true;
					if(entity!=null)
					{
						this.Depots.Add((DepotEntity)entity);
					}
					break;
				case "FareMatrixes":
					_alreadyFetchedFareMatrixes = true;
					if(entity!=null)
					{
						this.FareMatrixes.Add((FareMatrixEntity)entity);
					}
					break;
				case "FareTables":
					_alreadyFetchedFareTables = true;
					if(entity!=null)
					{
						this.FareTables.Add((FareTableEntity)entity);
					}
					break;
				case "Layouts":
					_alreadyFetchedLayouts = true;
					if(entity!=null)
					{
						this.Layouts.Add((LayoutEntity)entity);
					}
					break;
				case "WhoIsResponsible":
					_alreadyFetchedWhoIsResponsible = true;
					if(entity!=null)
					{
						this.WhoIsResponsible.Add((ResponsibilityEntity)entity);
					}
					break;
				case "ResponsibleFor":
					_alreadyFetchedResponsibleFor = true;
					if(entity!=null)
					{
						this.ResponsibleFor.Add((ResponsibilityEntity)entity);
					}
					break;
				case "ClientTariffs":
					_alreadyFetchedClientTariffs = true;
					if(entity!=null)
					{
						this.ClientTariffs.Add((TariffEntity)entity);
					}
					break;
				case "OwnerClientTariffs":
					_alreadyFetchedOwnerClientTariffs = true;
					if(entity!=null)
					{
						this.OwnerClientTariffs.Add((TariffEntity)entity);
					}
					break;
				case "Tickets":
					_alreadyFetchedTickets = true;
					if(entity!=null)
					{
						this.Tickets.Add((TicketEntity)entity);
					}
					break;
				case "TicketCategories":
					_alreadyFetchedTicketCategories = true;
					if(entity!=null)
					{
						this.TicketCategories.Add((TicketCategoryEntity)entity);
					}
					break;
				case "TicketVendingClient":
					_alreadyFetchedTicketVendingClient = true;
					if(entity!=null)
					{
						this.TicketVendingClient.Add((TicketVendingClientEntity)entity);
					}
					break;
				case "CardIssuerCardTransactions":
					_alreadyFetchedCardIssuerCardTransactions = true;
					if(entity!=null)
					{
						this.CardIssuerCardTransactions.Add((TransactionEntity)entity);
					}
					break;
				case "UmUnits":
					_alreadyFetchedUmUnits = true;
					if(entity!=null)
					{
						this.UmUnits.Add((UnitEntity)entity);
					}
					break;
				case "Users":
					_alreadyFetchedUsers = true;
					if(entity!=null)
					{
						this.Users.Add((UserListEntity)entity);
					}
					break;
				case "Settlements":
					_alreadyFetchedSettlements = true;
					if(entity!=null)
					{
						this.Settlements.Add((VarioSettlementEntity)entity);
					}
					break;
				case "VdvKeySet":
					_alreadyFetchedVdvKeySet = true;
					if(entity!=null)
					{
						this.VdvKeySet.Add((VdvKeySetEntity)entity);
					}
					break;
				case "Workstations":
					_alreadyFetchedWorkstations = true;
					if(entity!=null)
					{
						this.Workstations.Add((WorkstationEntity)entity);
					}
					break;
				case "AddressBookEntries":
					_alreadyFetchedAddressBookEntries = true;
					if(entity!=null)
					{
						this.AddressBookEntries.Add((AddressBookEntryEntity)entity);
					}
					break;
				case "Cards":
					_alreadyFetchedCards = true;
					if(entity!=null)
					{
						this.Cards.Add((CardEntity)entity);
					}
					break;
				case "ConfigurationDefinitions":
					_alreadyFetchedConfigurationDefinitions = true;
					if(entity!=null)
					{
						this.ConfigurationDefinitions.Add((ConfigurationDefinitionEntity)entity);
					}
					break;
				case "ConfigurationOverwrites":
					_alreadyFetchedConfigurationOverwrites = true;
					if(entity!=null)
					{
						this.ConfigurationOverwrites.Add((ConfigurationOverwriteEntity)entity);
					}
					break;
				case "Contracts":
					_alreadyFetchedContracts = true;
					if(entity!=null)
					{
						this.Contracts.Add((ContractEntity)entity);
					}
					break;
				case "VanpoolContracts":
					_alreadyFetchedVanpoolContracts = true;
					if(entity!=null)
					{
						this.VanpoolContracts.Add((ContractEntity)entity);
					}
					break;
				case "CryptoCryptogramArchives":
					_alreadyFetchedCryptoCryptogramArchives = true;
					if(entity!=null)
					{
						this.CryptoCryptogramArchives.Add((CryptoCryptogramArchiveEntity)entity);
					}
					break;
				case "CryptoQArchives":
					_alreadyFetchedCryptoQArchives = true;
					if(entity!=null)
					{
						this.CryptoQArchives.Add((CryptoQArchiveEntity)entity);
					}
					break;
				case "FareChangeCauses":
					_alreadyFetchedFareChangeCauses = true;
					if(entity!=null)
					{
						this.FareChangeCauses.Add((FareChangeCauseEntity)entity);
					}
					break;
				case "FareEvasionBehaviours":
					_alreadyFetchedFareEvasionBehaviours = true;
					if(entity!=null)
					{
						this.FareEvasionBehaviours.Add((FareEvasionBehaviourEntity)entity);
					}
					break;
				case "FareEvasionIncidents":
					_alreadyFetchedFareEvasionIncidents = true;
					if(entity!=null)
					{
						this.FareEvasionIncidents.Add((FareEvasionIncidentEntity)entity);
					}
					break;
				case "FareEvasionInspections":
					_alreadyFetchedFareEvasionInspections = true;
					if(entity!=null)
					{
						this.FareEvasionInspections.Add((FareEvasionInspectionEntity)entity);
					}
					break;
				case "FilterValueSets":
					_alreadyFetchedFilterValueSets = true;
					if(entity!=null)
					{
						this.FilterValueSets.Add((FilterValueSetEntity)entity);
					}
					break;
				case "Forms":
					_alreadyFetchedForms = true;
					if(entity!=null)
					{
						this.Forms.Add((FormEntity)entity);
					}
					break;
				case "IdentificationTypes":
					_alreadyFetchedIdentificationTypes = true;
					if(entity!=null)
					{
						this.IdentificationTypes.Add((IdentificationTypeEntity)entity);
					}
					break;
				case "Invoices":
					_alreadyFetchedInvoices = true;
					if(entity!=null)
					{
						this.Invoices.Add((InvoiceEntity)entity);
					}
					break;
				case "Invoicings":
					_alreadyFetchedInvoicings = true;
					if(entity!=null)
					{
						this.Invoicings.Add((InvoicingEntity)entity);
					}
					break;
				case "Jobs":
					_alreadyFetchedJobs = true;
					if(entity!=null)
					{
						this.Jobs.Add((JobEntity)entity);
					}
					break;
				case "ParameterArchives":
					_alreadyFetchedParameterArchives = true;
					if(entity!=null)
					{
						this.ParameterArchives.Add((ParameterArchiveEntity)entity);
					}
					break;
				case "PostingKeys":
					_alreadyFetchedPostingKeys = true;
					if(entity!=null)
					{
						this.PostingKeys.Add((PostingKeyEntity)entity);
					}
					break;
				case "Products":
					_alreadyFetchedProducts = true;
					if(entity!=null)
					{
						this.Products.Add((ProductEntity)entity);
					}
					break;
				case "ReportJobs":
					_alreadyFetchedReportJobs = true;
					if(entity!=null)
					{
						this.ReportJobs.Add((ReportJobEntity)entity);
					}
					break;
				case "ReportJobResults":
					_alreadyFetchedReportJobResults = true;
					if(entity!=null)
					{
						this.ReportJobResults.Add((ReportJobResultEntity)entity);
					}
					break;
				case "ReportToClients":
					_alreadyFetchedReportToClients = true;
					if(entity!=null)
					{
						this.ReportToClients.Add((ReportToClientEntity)entity);
					}
					break;
				case "RevenueSettlements":
					_alreadyFetchedRevenueSettlements = true;
					if(entity!=null)
					{
						this.RevenueSettlements.Add((RevenueSettlementEntity)entity);
					}
					break;
				case "Sales":
					_alreadyFetchedSales = true;
					if(entity!=null)
					{
						this.Sales.Add((SaleEntity)entity);
					}
					break;
				case "SchoolYears":
					_alreadyFetchedSchoolYears = true;
					if(entity!=null)
					{
						this.SchoolYears.Add((SchoolYearEntity)entity);
					}
					break;
				case "StorageLocations":
					_alreadyFetchedStorageLocations = true;
					if(entity!=null)
					{
						this.StorageLocations.Add((StorageLocationEntity)entity);
					}
					break;
				case "TransactionJournals":
					_alreadyFetchedTransactionJournals = true;
					if(entity!=null)
					{
						this.TransactionJournals.Add((TransactionJournalEntity)entity);
					}
					break;
				case "TransportCompanies":
					_alreadyFetchedTransportCompanies = true;
					if(entity!=null)
					{
						this.TransportCompanies.Add((TransportCompanyEntity)entity);
					}
					break;
				case "TicketCollectionViaTicketVendingClient":
					_alreadyFetchedTicketCollectionViaTicketVendingClient = true;
					if(entity!=null)
					{
						this.TicketCollectionViaTicketVendingClient.Add((TicketEntity)entity);
					}
					break;
				case "ReportCollectionViaReportToClient":
					_alreadyFetchedReportCollectionViaReportToClient = true;
					if(entity!=null)
					{
						this.ReportCollectionViaReportToClient.Add((ReportEntity)entity);
					}
					break;
				case "AccountEntryNumber":
					_alreadyFetchedAccountEntryNumber = true;
					this.AccountEntryNumber = (AccountEntryNumberEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AccountEntries":
					_accountEntries.Add((AccountEntryEntity)relatedEntity);
					break;
				case "ApportionmentResults":
					_apportionmentResults.Add((ApportionmentResultEntity)relatedEntity);
					break;
				case "ApportionmentResults1":
					_apportionmentResults1.Add((ApportionmentResultEntity)relatedEntity);
					break;
				case "ApportionmentResults2":
					_apportionmentResults2.Add((ApportionmentResultEntity)relatedEntity);
					break;
				case "Calendar":
					_calendar.Add((CalendarEntity)relatedEntity);
					break;
				case "ClearingResultsByCardIssuer":
					_clearingResultsByCardIssuer.Add((ClearingResultEntity)relatedEntity);
					break;
				case "ClearingResults":
					_clearingResults.Add((ClearingResultEntity)relatedEntity);
					break;
				case "ClearingResultsByClientFrom":
					_clearingResultsByClientFrom.Add((ClearingResultEntity)relatedEntity);
					break;
				case "ClearingResultsByClientTo":
					_clearingResultsByClientTo.Add((ClearingResultEntity)relatedEntity);
					break;
				case "ClientAdaptedLayoutObjects":
					_clientAdaptedLayoutObjects.Add((ClientAdaptedLayoutObjectEntity)relatedEntity);
					break;
				case "Debtors":
					_debtors.Add((DebtorEntity)relatedEntity);
					break;
				case "Depots":
					_depots.Add((DepotEntity)relatedEntity);
					break;
				case "FareMatrixes":
					_fareMatrixes.Add((FareMatrixEntity)relatedEntity);
					break;
				case "FareTables":
					_fareTables.Add((FareTableEntity)relatedEntity);
					break;
				case "Layouts":
					_layouts.Add((LayoutEntity)relatedEntity);
					break;
				case "WhoIsResponsible":
					_whoIsResponsible.Add((ResponsibilityEntity)relatedEntity);
					break;
				case "ResponsibleFor":
					_responsibleFor.Add((ResponsibilityEntity)relatedEntity);
					break;
				case "ClientTariffs":
					_clientTariffs.Add((TariffEntity)relatedEntity);
					break;
				case "OwnerClientTariffs":
					_ownerClientTariffs.Add((TariffEntity)relatedEntity);
					break;
				case "Tickets":
					_tickets.Add((TicketEntity)relatedEntity);
					break;
				case "TicketCategories":
					_ticketCategories.Add((TicketCategoryEntity)relatedEntity);
					break;
				case "TicketVendingClient":
					_ticketVendingClient.Add((TicketVendingClientEntity)relatedEntity);
					break;
				case "CardIssuerCardTransactions":
					_cardIssuerCardTransactions.Add((TransactionEntity)relatedEntity);
					break;
				case "UmUnits":
					_umUnits.Add((UnitEntity)relatedEntity);
					break;
				case "Users":
					_users.Add((UserListEntity)relatedEntity);
					break;
				case "Settlements":
					_settlements.Add((VarioSettlementEntity)relatedEntity);
					break;
				case "VdvKeySet":
					_vdvKeySet.Add((VdvKeySetEntity)relatedEntity);
					break;
				case "Workstations":
					_workstations.Add((WorkstationEntity)relatedEntity);
					break;
				case "AddressBookEntries":
					_addressBookEntries.Add((AddressBookEntryEntity)relatedEntity);
					break;
				case "Cards":
					_cards.Add((CardEntity)relatedEntity);
					break;
				case "ConfigurationDefinitions":
					_configurationDefinitions.Add((ConfigurationDefinitionEntity)relatedEntity);
					break;
				case "ConfigurationOverwrites":
					_configurationOverwrites.Add((ConfigurationOverwriteEntity)relatedEntity);
					break;
				case "Contracts":
					_contracts.Add((ContractEntity)relatedEntity);
					break;
				case "VanpoolContracts":
					_vanpoolContracts.Add((ContractEntity)relatedEntity);
					break;
				case "CryptoCryptogramArchives":
					_cryptoCryptogramArchives.Add((CryptoCryptogramArchiveEntity)relatedEntity);
					break;
				case "CryptoQArchives":
					_cryptoQArchives.Add((CryptoQArchiveEntity)relatedEntity);
					break;
				case "FareChangeCauses":
					_fareChangeCauses.Add((FareChangeCauseEntity)relatedEntity);
					break;
				case "FareEvasionBehaviours":
					_fareEvasionBehaviours.Add((FareEvasionBehaviourEntity)relatedEntity);
					break;
				case "FareEvasionIncidents":
					_fareEvasionIncidents.Add((FareEvasionIncidentEntity)relatedEntity);
					break;
				case "FareEvasionInspections":
					_fareEvasionInspections.Add((FareEvasionInspectionEntity)relatedEntity);
					break;
				case "FilterValueSets":
					_filterValueSets.Add((FilterValueSetEntity)relatedEntity);
					break;
				case "Forms":
					_forms.Add((FormEntity)relatedEntity);
					break;
				case "IdentificationTypes":
					_identificationTypes.Add((IdentificationTypeEntity)relatedEntity);
					break;
				case "Invoices":
					_invoices.Add((InvoiceEntity)relatedEntity);
					break;
				case "Invoicings":
					_invoicings.Add((InvoicingEntity)relatedEntity);
					break;
				case "Jobs":
					_jobs.Add((JobEntity)relatedEntity);
					break;
				case "ParameterArchives":
					_parameterArchives.Add((ParameterArchiveEntity)relatedEntity);
					break;
				case "PostingKeys":
					_postingKeys.Add((PostingKeyEntity)relatedEntity);
					break;
				case "Products":
					_products.Add((ProductEntity)relatedEntity);
					break;
				case "ReportJobs":
					_reportJobs.Add((ReportJobEntity)relatedEntity);
					break;
				case "ReportJobResults":
					_reportJobResults.Add((ReportJobResultEntity)relatedEntity);
					break;
				case "ReportToClients":
					_reportToClients.Add((ReportToClientEntity)relatedEntity);
					break;
				case "RevenueSettlements":
					_revenueSettlements.Add((RevenueSettlementEntity)relatedEntity);
					break;
				case "Sales":
					_sales.Add((SaleEntity)relatedEntity);
					break;
				case "SchoolYears":
					_schoolYears.Add((SchoolYearEntity)relatedEntity);
					break;
				case "StorageLocations":
					_storageLocations.Add((StorageLocationEntity)relatedEntity);
					break;
				case "TransactionJournals":
					_transactionJournals.Add((TransactionJournalEntity)relatedEntity);
					break;
				case "TransportCompanies":
					_transportCompanies.Add((TransportCompanyEntity)relatedEntity);
					break;
				case "AccountEntryNumber":
					SetupSyncAccountEntryNumber(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AccountEntries":
					this.PerformRelatedEntityRemoval(_accountEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ApportionmentResults":
					this.PerformRelatedEntityRemoval(_apportionmentResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ApportionmentResults1":
					this.PerformRelatedEntityRemoval(_apportionmentResults1, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ApportionmentResults2":
					this.PerformRelatedEntityRemoval(_apportionmentResults2, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Calendar":
					this.PerformRelatedEntityRemoval(_calendar, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClearingResultsByCardIssuer":
					this.PerformRelatedEntityRemoval(_clearingResultsByCardIssuer, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClearingResults":
					this.PerformRelatedEntityRemoval(_clearingResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClearingResultsByClientFrom":
					this.PerformRelatedEntityRemoval(_clearingResultsByClientFrom, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClearingResultsByClientTo":
					this.PerformRelatedEntityRemoval(_clearingResultsByClientTo, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClientAdaptedLayoutObjects":
					this.PerformRelatedEntityRemoval(_clientAdaptedLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Debtors":
					this.PerformRelatedEntityRemoval(_debtors, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Depots":
					this.PerformRelatedEntityRemoval(_depots, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareMatrixes":
					this.PerformRelatedEntityRemoval(_fareMatrixes, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareTables":
					this.PerformRelatedEntityRemoval(_fareTables, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Layouts":
					this.PerformRelatedEntityRemoval(_layouts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "WhoIsResponsible":
					this.PerformRelatedEntityRemoval(_whoIsResponsible, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ResponsibleFor":
					this.PerformRelatedEntityRemoval(_responsibleFor, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClientTariffs":
					this.PerformRelatedEntityRemoval(_clientTariffs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OwnerClientTariffs":
					this.PerformRelatedEntityRemoval(_ownerClientTariffs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Tickets":
					this.PerformRelatedEntityRemoval(_tickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketCategories":
					this.PerformRelatedEntityRemoval(_ticketCategories, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketVendingClient":
					this.PerformRelatedEntityRemoval(_ticketVendingClient, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardIssuerCardTransactions":
					this.PerformRelatedEntityRemoval(_cardIssuerCardTransactions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UmUnits":
					this.PerformRelatedEntityRemoval(_umUnits, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Users":
					this.PerformRelatedEntityRemoval(_users, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Settlements":
					this.PerformRelatedEntityRemoval(_settlements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvKeySet":
					this.PerformRelatedEntityRemoval(_vdvKeySet, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Workstations":
					this.PerformRelatedEntityRemoval(_workstations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AddressBookEntries":
					this.PerformRelatedEntityRemoval(_addressBookEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Cards":
					this.PerformRelatedEntityRemoval(_cards, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ConfigurationDefinitions":
					this.PerformRelatedEntityRemoval(_configurationDefinitions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ConfigurationOverwrites":
					this.PerformRelatedEntityRemoval(_configurationOverwrites, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Contracts":
					this.PerformRelatedEntityRemoval(_contracts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VanpoolContracts":
					this.PerformRelatedEntityRemoval(_vanpoolContracts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CryptoCryptogramArchives":
					this.PerformRelatedEntityRemoval(_cryptoCryptogramArchives, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CryptoQArchives":
					this.PerformRelatedEntityRemoval(_cryptoQArchives, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareChangeCauses":
					this.PerformRelatedEntityRemoval(_fareChangeCauses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareEvasionBehaviours":
					this.PerformRelatedEntityRemoval(_fareEvasionBehaviours, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareEvasionIncidents":
					this.PerformRelatedEntityRemoval(_fareEvasionIncidents, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareEvasionInspections":
					this.PerformRelatedEntityRemoval(_fareEvasionInspections, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FilterValueSets":
					this.PerformRelatedEntityRemoval(_filterValueSets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Forms":
					this.PerformRelatedEntityRemoval(_forms, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "IdentificationTypes":
					this.PerformRelatedEntityRemoval(_identificationTypes, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Invoices":
					this.PerformRelatedEntityRemoval(_invoices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Invoicings":
					this.PerformRelatedEntityRemoval(_invoicings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Jobs":
					this.PerformRelatedEntityRemoval(_jobs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterArchives":
					this.PerformRelatedEntityRemoval(_parameterArchives, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PostingKeys":
					this.PerformRelatedEntityRemoval(_postingKeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Products":
					this.PerformRelatedEntityRemoval(_products, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReportJobs":
					this.PerformRelatedEntityRemoval(_reportJobs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReportJobResults":
					this.PerformRelatedEntityRemoval(_reportJobResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReportToClients":
					this.PerformRelatedEntityRemoval(_reportToClients, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RevenueSettlements":
					this.PerformRelatedEntityRemoval(_revenueSettlements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Sales":
					this.PerformRelatedEntityRemoval(_sales, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SchoolYears":
					this.PerformRelatedEntityRemoval(_schoolYears, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "StorageLocations":
					this.PerformRelatedEntityRemoval(_storageLocations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournals":
					this.PerformRelatedEntityRemoval(_transactionJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransportCompanies":
					this.PerformRelatedEntityRemoval(_transportCompanies, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AccountEntryNumber":
					DesetupSyncAccountEntryNumber(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_accountEntryNumber!=null)
			{
				toReturn.Add(_accountEntryNumber);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_accountEntries);
			toReturn.Add(_apportionmentResults);
			toReturn.Add(_apportionmentResults1);
			toReturn.Add(_apportionmentResults2);
			toReturn.Add(_calendar);
			toReturn.Add(_clearingResultsByCardIssuer);
			toReturn.Add(_clearingResults);
			toReturn.Add(_clearingResultsByClientFrom);
			toReturn.Add(_clearingResultsByClientTo);
			toReturn.Add(_clientAdaptedLayoutObjects);
			toReturn.Add(_debtors);
			toReturn.Add(_depots);
			toReturn.Add(_fareMatrixes);
			toReturn.Add(_fareTables);
			toReturn.Add(_layouts);
			toReturn.Add(_whoIsResponsible);
			toReturn.Add(_responsibleFor);
			toReturn.Add(_clientTariffs);
			toReturn.Add(_ownerClientTariffs);
			toReturn.Add(_tickets);
			toReturn.Add(_ticketCategories);
			toReturn.Add(_ticketVendingClient);
			toReturn.Add(_cardIssuerCardTransactions);
			toReturn.Add(_umUnits);
			toReturn.Add(_users);
			toReturn.Add(_settlements);
			toReturn.Add(_vdvKeySet);
			toReturn.Add(_workstations);
			toReturn.Add(_addressBookEntries);
			toReturn.Add(_cards);
			toReturn.Add(_configurationDefinitions);
			toReturn.Add(_configurationOverwrites);
			toReturn.Add(_contracts);
			toReturn.Add(_vanpoolContracts);
			toReturn.Add(_cryptoCryptogramArchives);
			toReturn.Add(_cryptoQArchives);
			toReturn.Add(_fareChangeCauses);
			toReturn.Add(_fareEvasionBehaviours);
			toReturn.Add(_fareEvasionIncidents);
			toReturn.Add(_fareEvasionInspections);
			toReturn.Add(_filterValueSets);
			toReturn.Add(_forms);
			toReturn.Add(_identificationTypes);
			toReturn.Add(_invoices);
			toReturn.Add(_invoicings);
			toReturn.Add(_jobs);
			toReturn.Add(_parameterArchives);
			toReturn.Add(_postingKeys);
			toReturn.Add(_products);
			toReturn.Add(_reportJobs);
			toReturn.Add(_reportJobResults);
			toReturn.Add(_reportToClients);
			toReturn.Add(_revenueSettlements);
			toReturn.Add(_sales);
			toReturn.Add(_schoolYears);
			toReturn.Add(_storageLocations);
			toReturn.Add(_transactionJournals);
			toReturn.Add(_transportCompanies);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientID">PK value for Client which data should be fetched into this Client object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 clientID)
		{
			return FetchUsingPK(clientID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientID">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 clientID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(clientID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientID">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 clientID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(clientID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientID">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 clientID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(clientID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ClientID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ClientRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AccountEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AccountEntryCollection GetMultiAccountEntries(bool forceFetch)
		{
			return GetMultiAccountEntries(forceFetch, _accountEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AccountEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AccountEntryCollection GetMultiAccountEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAccountEntries(forceFetch, _accountEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AccountEntryCollection GetMultiAccountEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAccountEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AccountEntryCollection GetMultiAccountEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAccountEntries || forceFetch || _alwaysFetchAccountEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_accountEntries);
				_accountEntries.SuppressClearInGetMulti=!forceFetch;
				_accountEntries.EntityFactoryToUse = entityFactoryToUse;
				_accountEntries.GetMultiManyToOne(this, null, null, filter);
				_accountEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedAccountEntries = true;
			}
			return _accountEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'AccountEntries'. These settings will be taken into account
		/// when the property AccountEntries is requested or GetMultiAccountEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAccountEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_accountEntries.SortClauses=sortClauses;
			_accountEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ApportionmentResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults(bool forceFetch)
		{
			return GetMultiApportionmentResults(forceFetch, _apportionmentResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ApportionmentResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiApportionmentResults(forceFetch, _apportionmentResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiApportionmentResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedApportionmentResults || forceFetch || _alwaysFetchApportionmentResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_apportionmentResults);
				_apportionmentResults.SuppressClearInGetMulti=!forceFetch;
				_apportionmentResults.EntityFactoryToUse = entityFactoryToUse;
				_apportionmentResults.GetMultiManyToOne(null, this, null, null, null, null, filter);
				_apportionmentResults.SuppressClearInGetMulti=false;
				_alreadyFetchedApportionmentResults = true;
			}
			return _apportionmentResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ApportionmentResults'. These settings will be taken into account
		/// when the property ApportionmentResults is requested or GetMultiApportionmentResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersApportionmentResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_apportionmentResults.SortClauses=sortClauses;
			_apportionmentResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ApportionmentResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults1(bool forceFetch)
		{
			return GetMultiApportionmentResults1(forceFetch, _apportionmentResults1.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ApportionmentResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults1(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiApportionmentResults1(forceFetch, _apportionmentResults1.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults1(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiApportionmentResults1(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults1(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedApportionmentResults1 || forceFetch || _alwaysFetchApportionmentResults1) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_apportionmentResults1);
				_apportionmentResults1.SuppressClearInGetMulti=!forceFetch;
				_apportionmentResults1.EntityFactoryToUse = entityFactoryToUse;
				_apportionmentResults1.GetMultiManyToOne(null, null, this, null, null, null, filter);
				_apportionmentResults1.SuppressClearInGetMulti=false;
				_alreadyFetchedApportionmentResults1 = true;
			}
			return _apportionmentResults1;
		}

		/// <summary> Sets the collection parameters for the collection for 'ApportionmentResults1'. These settings will be taken into account
		/// when the property ApportionmentResults1 is requested or GetMultiApportionmentResults1 is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersApportionmentResults1(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_apportionmentResults1.SortClauses=sortClauses;
			_apportionmentResults1.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ApportionmentResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults2(bool forceFetch)
		{
			return GetMultiApportionmentResults2(forceFetch, _apportionmentResults2.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ApportionmentResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults2(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiApportionmentResults2(forceFetch, _apportionmentResults2.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults2(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiApportionmentResults2(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults2(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedApportionmentResults2 || forceFetch || _alwaysFetchApportionmentResults2) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_apportionmentResults2);
				_apportionmentResults2.SuppressClearInGetMulti=!forceFetch;
				_apportionmentResults2.EntityFactoryToUse = entityFactoryToUse;
				_apportionmentResults2.GetMultiManyToOne(null, null, null, this, null, null, filter);
				_apportionmentResults2.SuppressClearInGetMulti=false;
				_alreadyFetchedApportionmentResults2 = true;
			}
			return _apportionmentResults2;
		}

		/// <summary> Sets the collection parameters for the collection for 'ApportionmentResults2'. These settings will be taken into account
		/// when the property ApportionmentResults2 is requested or GetMultiApportionmentResults2 is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersApportionmentResults2(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_apportionmentResults2.SortClauses=sortClauses;
			_apportionmentResults2.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CalendarEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CalendarCollection GetMultiCalendar(bool forceFetch)
		{
			return GetMultiCalendar(forceFetch, _calendar.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CalendarEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CalendarCollection GetMultiCalendar(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCalendar(forceFetch, _calendar.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CalendarCollection GetMultiCalendar(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCalendar(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CalendarCollection GetMultiCalendar(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCalendar || forceFetch || _alwaysFetchCalendar) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_calendar);
				_calendar.SuppressClearInGetMulti=!forceFetch;
				_calendar.EntityFactoryToUse = entityFactoryToUse;
				_calendar.GetMultiManyToOne(this, null, filter);
				_calendar.SuppressClearInGetMulti=false;
				_alreadyFetchedCalendar = true;
			}
			return _calendar;
		}

		/// <summary> Sets the collection parameters for the collection for 'Calendar'. These settings will be taken into account
		/// when the property Calendar is requested or GetMultiCalendar is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCalendar(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_calendar.SortClauses=sortClauses;
			_calendar.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResultsByCardIssuer(bool forceFetch)
		{
			return GetMultiClearingResultsByCardIssuer(forceFetch, _clearingResultsByCardIssuer.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResultsByCardIssuer(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClearingResultsByCardIssuer(forceFetch, _clearingResultsByCardIssuer.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResultsByCardIssuer(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClearingResultsByCardIssuer(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResultsByCardIssuer(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClearingResultsByCardIssuer || forceFetch || _alwaysFetchClearingResultsByCardIssuer) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clearingResultsByCardIssuer);
				_clearingResultsByCardIssuer.SuppressClearInGetMulti=!forceFetch;
				_clearingResultsByCardIssuer.EntityFactoryToUse = entityFactoryToUse;
				_clearingResultsByCardIssuer.GetMultiManyToOne(this, null, null, null, null, null, null, null, filter);
				_clearingResultsByCardIssuer.SuppressClearInGetMulti=false;
				_alreadyFetchedClearingResultsByCardIssuer = true;
			}
			return _clearingResultsByCardIssuer;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClearingResultsByCardIssuer'. These settings will be taken into account
		/// when the property ClearingResultsByCardIssuer is requested or GetMultiClearingResultsByCardIssuer is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClearingResultsByCardIssuer(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clearingResultsByCardIssuer.SortClauses=sortClauses;
			_clearingResultsByCardIssuer.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch)
		{
			return GetMultiClearingResults(forceFetch, _clearingResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClearingResults(forceFetch, _clearingResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClearingResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClearingResults || forceFetch || _alwaysFetchClearingResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clearingResults);
				_clearingResults.SuppressClearInGetMulti=!forceFetch;
				_clearingResults.EntityFactoryToUse = entityFactoryToUse;
				_clearingResults.GetMultiManyToOne(null, this, null, null, null, null, null, null, filter);
				_clearingResults.SuppressClearInGetMulti=false;
				_alreadyFetchedClearingResults = true;
			}
			return _clearingResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClearingResults'. These settings will be taken into account
		/// when the property ClearingResults is requested or GetMultiClearingResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClearingResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clearingResults.SortClauses=sortClauses;
			_clearingResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResultsByClientFrom(bool forceFetch)
		{
			return GetMultiClearingResultsByClientFrom(forceFetch, _clearingResultsByClientFrom.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResultsByClientFrom(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClearingResultsByClientFrom(forceFetch, _clearingResultsByClientFrom.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResultsByClientFrom(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClearingResultsByClientFrom(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResultsByClientFrom(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClearingResultsByClientFrom || forceFetch || _alwaysFetchClearingResultsByClientFrom) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clearingResultsByClientFrom);
				_clearingResultsByClientFrom.SuppressClearInGetMulti=!forceFetch;
				_clearingResultsByClientFrom.EntityFactoryToUse = entityFactoryToUse;
				_clearingResultsByClientFrom.GetMultiManyToOne(null, null, this, null, null, null, null, null, filter);
				_clearingResultsByClientFrom.SuppressClearInGetMulti=false;
				_alreadyFetchedClearingResultsByClientFrom = true;
			}
			return _clearingResultsByClientFrom;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClearingResultsByClientFrom'. These settings will be taken into account
		/// when the property ClearingResultsByClientFrom is requested or GetMultiClearingResultsByClientFrom is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClearingResultsByClientFrom(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clearingResultsByClientFrom.SortClauses=sortClauses;
			_clearingResultsByClientFrom.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResultsByClientTo(bool forceFetch)
		{
			return GetMultiClearingResultsByClientTo(forceFetch, _clearingResultsByClientTo.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResultsByClientTo(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClearingResultsByClientTo(forceFetch, _clearingResultsByClientTo.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResultsByClientTo(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClearingResultsByClientTo(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResultsByClientTo(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClearingResultsByClientTo || forceFetch || _alwaysFetchClearingResultsByClientTo) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clearingResultsByClientTo);
				_clearingResultsByClientTo.SuppressClearInGetMulti=!forceFetch;
				_clearingResultsByClientTo.EntityFactoryToUse = entityFactoryToUse;
				_clearingResultsByClientTo.GetMultiManyToOne(null, null, null, this, null, null, null, null, filter);
				_clearingResultsByClientTo.SuppressClearInGetMulti=false;
				_alreadyFetchedClearingResultsByClientTo = true;
			}
			return _clearingResultsByClientTo;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClearingResultsByClientTo'. These settings will be taken into account
		/// when the property ClearingResultsByClientTo is requested or GetMultiClearingResultsByClientTo is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClearingResultsByClientTo(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clearingResultsByClientTo.SortClauses=sortClauses;
			_clearingResultsByClientTo.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientAdaptedLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientAdaptedLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection GetMultiClientAdaptedLayoutObjects(bool forceFetch)
		{
			return GetMultiClientAdaptedLayoutObjects(forceFetch, _clientAdaptedLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientAdaptedLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientAdaptedLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection GetMultiClientAdaptedLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientAdaptedLayoutObjects(forceFetch, _clientAdaptedLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientAdaptedLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection GetMultiClientAdaptedLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientAdaptedLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientAdaptedLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection GetMultiClientAdaptedLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientAdaptedLayoutObjects || forceFetch || _alwaysFetchClientAdaptedLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientAdaptedLayoutObjects);
				_clientAdaptedLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_clientAdaptedLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_clientAdaptedLayoutObjects.GetMultiManyToOne(this, null, filter);
				_clientAdaptedLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedClientAdaptedLayoutObjects = true;
			}
			return _clientAdaptedLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientAdaptedLayoutObjects'. These settings will be taken into account
		/// when the property ClientAdaptedLayoutObjects is requested or GetMultiClientAdaptedLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientAdaptedLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientAdaptedLayoutObjects.SortClauses=sortClauses;
			_clientAdaptedLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DebtorEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DebtorEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DebtorCollection GetMultiDebtors(bool forceFetch)
		{
			return GetMultiDebtors(forceFetch, _debtors.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DebtorEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DebtorEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DebtorCollection GetMultiDebtors(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDebtors(forceFetch, _debtors.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DebtorEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DebtorCollection GetMultiDebtors(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDebtors(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DebtorEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DebtorCollection GetMultiDebtors(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDebtors || forceFetch || _alwaysFetchDebtors) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_debtors);
				_debtors.SuppressClearInGetMulti=!forceFetch;
				_debtors.EntityFactoryToUse = entityFactoryToUse;
				_debtors.GetMultiManyToOne(this, null, filter);
				_debtors.SuppressClearInGetMulti=false;
				_alreadyFetchedDebtors = true;
			}
			return _debtors;
		}

		/// <summary> Sets the collection parameters for the collection for 'Debtors'. These settings will be taken into account
		/// when the property Debtors is requested or GetMultiDebtors is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDebtors(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_debtors.SortClauses=sortClauses;
			_debtors.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DepotEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DepotEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DepotCollection GetMultiDepots(bool forceFetch)
		{
			return GetMultiDepots(forceFetch, _depots.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DepotEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DepotEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DepotCollection GetMultiDepots(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDepots(forceFetch, _depots.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DepotEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DepotCollection GetMultiDepots(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDepots(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DepotEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DepotCollection GetMultiDepots(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDepots || forceFetch || _alwaysFetchDepots) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_depots);
				_depots.SuppressClearInGetMulti=!forceFetch;
				_depots.EntityFactoryToUse = entityFactoryToUse;
				_depots.GetMultiManyToOne(this, filter);
				_depots.SuppressClearInGetMulti=false;
				_alreadyFetchedDepots = true;
			}
			return _depots;
		}

		/// <summary> Sets the collection parameters for the collection for 'Depots'. These settings will be taken into account
		/// when the property Depots is requested or GetMultiDepots is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDepots(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_depots.SortClauses=sortClauses;
			_depots.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFareMatrixes(bool forceFetch)
		{
			return GetMultiFareMatrixes(forceFetch, _fareMatrixes.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFareMatrixes(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareMatrixes(forceFetch, _fareMatrixes.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFareMatrixes(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareMatrixes(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFareMatrixes(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareMatrixes || forceFetch || _alwaysFetchFareMatrixes) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareMatrixes);
				_fareMatrixes.SuppressClearInGetMulti=!forceFetch;
				_fareMatrixes.EntityFactoryToUse = entityFactoryToUse;
				_fareMatrixes.GetMultiManyToOne(this, null, null, null, filter);
				_fareMatrixes.SuppressClearInGetMulti=false;
				_alreadyFetchedFareMatrixes = true;
			}
			return _fareMatrixes;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareMatrixes'. These settings will be taken into account
		/// when the property FareMatrixes is requested or GetMultiFareMatrixes is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareMatrixes(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareMatrixes.SortClauses=sortClauses;
			_fareMatrixes.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareTableEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareTableCollection GetMultiFareTables(bool forceFetch)
		{
			return GetMultiFareTables(forceFetch, _fareTables.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareTableEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareTableCollection GetMultiFareTables(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareTables(forceFetch, _fareTables.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareTableCollection GetMultiFareTables(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareTables(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareTableCollection GetMultiFareTables(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareTables || forceFetch || _alwaysFetchFareTables) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareTables);
				_fareTables.SuppressClearInGetMulti=!forceFetch;
				_fareTables.EntityFactoryToUse = entityFactoryToUse;
				_fareTables.GetMultiManyToOne(this, null, null, filter);
				_fareTables.SuppressClearInGetMulti=false;
				_alreadyFetchedFareTables = true;
			}
			return _fareTables;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareTables'. These settings will be taken into account
		/// when the property FareTables is requested or GetMultiFareTables is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareTables(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareTables.SortClauses=sortClauses;
			_fareTables.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LayoutEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch)
		{
			return GetMultiLayouts(forceFetch, _layouts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LayoutEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLayouts(forceFetch, _layouts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLayouts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLayouts || forceFetch || _alwaysFetchLayouts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_layouts);
				_layouts.SuppressClearInGetMulti=!forceFetch;
				_layouts.EntityFactoryToUse = entityFactoryToUse;
				_layouts.GetMultiManyToOne(this, null, null, null, filter);
				_layouts.SuppressClearInGetMulti=false;
				_alreadyFetchedLayouts = true;
			}
			return _layouts;
		}

		/// <summary> Sets the collection parameters for the collection for 'Layouts'. These settings will be taken into account
		/// when the property Layouts is requested or GetMultiLayouts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLayouts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_layouts.SortClauses=sortClauses;
			_layouts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ResponsibilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ResponsibilityEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ResponsibilityCollection GetMultiWhoIsResponsible(bool forceFetch)
		{
			return GetMultiWhoIsResponsible(forceFetch, _whoIsResponsible.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ResponsibilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ResponsibilityEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ResponsibilityCollection GetMultiWhoIsResponsible(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiWhoIsResponsible(forceFetch, _whoIsResponsible.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ResponsibilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ResponsibilityCollection GetMultiWhoIsResponsible(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiWhoIsResponsible(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ResponsibilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ResponsibilityCollection GetMultiWhoIsResponsible(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedWhoIsResponsible || forceFetch || _alwaysFetchWhoIsResponsible) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_whoIsResponsible);
				_whoIsResponsible.SuppressClearInGetMulti=!forceFetch;
				_whoIsResponsible.EntityFactoryToUse = entityFactoryToUse;
				_whoIsResponsible.GetMultiManyToOne(this, null, filter);
				_whoIsResponsible.SuppressClearInGetMulti=false;
				_alreadyFetchedWhoIsResponsible = true;
			}
			return _whoIsResponsible;
		}

		/// <summary> Sets the collection parameters for the collection for 'WhoIsResponsible'. These settings will be taken into account
		/// when the property WhoIsResponsible is requested or GetMultiWhoIsResponsible is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWhoIsResponsible(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_whoIsResponsible.SortClauses=sortClauses;
			_whoIsResponsible.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ResponsibilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ResponsibilityEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ResponsibilityCollection GetMultiResponsibleFor(bool forceFetch)
		{
			return GetMultiResponsibleFor(forceFetch, _responsibleFor.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ResponsibilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ResponsibilityEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ResponsibilityCollection GetMultiResponsibleFor(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiResponsibleFor(forceFetch, _responsibleFor.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ResponsibilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ResponsibilityCollection GetMultiResponsibleFor(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiResponsibleFor(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ResponsibilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ResponsibilityCollection GetMultiResponsibleFor(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedResponsibleFor || forceFetch || _alwaysFetchResponsibleFor) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_responsibleFor);
				_responsibleFor.SuppressClearInGetMulti=!forceFetch;
				_responsibleFor.EntityFactoryToUse = entityFactoryToUse;
				_responsibleFor.GetMultiManyToOne(null, this, filter);
				_responsibleFor.SuppressClearInGetMulti=false;
				_alreadyFetchedResponsibleFor = true;
			}
			return _responsibleFor;
		}

		/// <summary> Sets the collection parameters for the collection for 'ResponsibleFor'. These settings will be taken into account
		/// when the property ResponsibleFor is requested or GetMultiResponsibleFor is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersResponsibleFor(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_responsibleFor.SortClauses=sortClauses;
			_responsibleFor.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiClientTariffs(bool forceFetch)
		{
			return GetMultiClientTariffs(forceFetch, _clientTariffs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiClientTariffs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientTariffs(forceFetch, _clientTariffs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiClientTariffs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientTariffs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TariffCollection GetMultiClientTariffs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientTariffs || forceFetch || _alwaysFetchClientTariffs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientTariffs);
				_clientTariffs.SuppressClearInGetMulti=!forceFetch;
				_clientTariffs.EntityFactoryToUse = entityFactoryToUse;
				_clientTariffs.GetMultiManyToOne(this, null, null, null, null, null, filter);
				_clientTariffs.SuppressClearInGetMulti=false;
				_alreadyFetchedClientTariffs = true;
			}
			return _clientTariffs;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientTariffs'. These settings will be taken into account
		/// when the property ClientTariffs is requested or GetMultiClientTariffs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientTariffs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientTariffs.SortClauses=sortClauses;
			_clientTariffs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiOwnerClientTariffs(bool forceFetch)
		{
			return GetMultiOwnerClientTariffs(forceFetch, _ownerClientTariffs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiOwnerClientTariffs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOwnerClientTariffs(forceFetch, _ownerClientTariffs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiOwnerClientTariffs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOwnerClientTariffs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TariffCollection GetMultiOwnerClientTariffs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOwnerClientTariffs || forceFetch || _alwaysFetchOwnerClientTariffs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ownerClientTariffs);
				_ownerClientTariffs.SuppressClearInGetMulti=!forceFetch;
				_ownerClientTariffs.EntityFactoryToUse = entityFactoryToUse;
				_ownerClientTariffs.GetMultiManyToOne(null, this, null, null, null, null, filter);
				_ownerClientTariffs.SuppressClearInGetMulti=false;
				_alreadyFetchedOwnerClientTariffs = true;
			}
			return _ownerClientTariffs;
		}

		/// <summary> Sets the collection parameters for the collection for 'OwnerClientTariffs'. These settings will be taken into account
		/// when the property OwnerClientTariffs is requested or GetMultiOwnerClientTariffs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOwnerClientTariffs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ownerClientTariffs.SortClauses=sortClauses;
			_ownerClientTariffs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTickets || forceFetch || _alwaysFetchTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tickets);
				_tickets.SuppressClearInGetMulti=!forceFetch;
				_tickets.EntityFactoryToUse = entityFactoryToUse;
				_tickets.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_tickets.SuppressClearInGetMulti=false;
				_alreadyFetchedTickets = true;
			}
			return _tickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'Tickets'. These settings will be taken into account
		/// when the property Tickets is requested or GetMultiTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tickets.SortClauses=sortClauses;
			_tickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketCategoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCategoryCollection GetMultiTicketCategories(bool forceFetch)
		{
			return GetMultiTicketCategories(forceFetch, _ticketCategories.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketCategoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCategoryCollection GetMultiTicketCategories(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketCategories(forceFetch, _ticketCategories.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCategoryCollection GetMultiTicketCategories(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketCategories(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCategoryCollection GetMultiTicketCategories(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketCategories || forceFetch || _alwaysFetchTicketCategories) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketCategories);
				_ticketCategories.SuppressClearInGetMulti=!forceFetch;
				_ticketCategories.EntityFactoryToUse = entityFactoryToUse;
				_ticketCategories.GetMultiManyToOne(this, filter);
				_ticketCategories.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketCategories = true;
			}
			return _ticketCategories;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketCategories'. These settings will be taken into account
		/// when the property TicketCategories is requested or GetMultiTicketCategories is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketCategories(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketCategories.SortClauses=sortClauses;
			_ticketCategories.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketVendingClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketVendingClientEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketVendingClientCollection GetMultiTicketVendingClient(bool forceFetch)
		{
			return GetMultiTicketVendingClient(forceFetch, _ticketVendingClient.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketVendingClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketVendingClientEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketVendingClientCollection GetMultiTicketVendingClient(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketVendingClient(forceFetch, _ticketVendingClient.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketVendingClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketVendingClientCollection GetMultiTicketVendingClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketVendingClient(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketVendingClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketVendingClientCollection GetMultiTicketVendingClient(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketVendingClient || forceFetch || _alwaysFetchTicketVendingClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketVendingClient);
				_ticketVendingClient.SuppressClearInGetMulti=!forceFetch;
				_ticketVendingClient.EntityFactoryToUse = entityFactoryToUse;
				_ticketVendingClient.GetMultiManyToOne(this, null, filter);
				_ticketVendingClient.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketVendingClient = true;
			}
			return _ticketVendingClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketVendingClient'. These settings will be taken into account
		/// when the property TicketVendingClient is requested or GetMultiTicketVendingClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketVendingClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketVendingClient.SortClauses=sortClauses;
			_ticketVendingClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionCollection GetMultiCardIssuerCardTransactions(bool forceFetch)
		{
			return GetMultiCardIssuerCardTransactions(forceFetch, _cardIssuerCardTransactions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionCollection GetMultiCardIssuerCardTransactions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardIssuerCardTransactions(forceFetch, _cardIssuerCardTransactions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionCollection GetMultiCardIssuerCardTransactions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardIssuerCardTransactions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionCollection GetMultiCardIssuerCardTransactions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardIssuerCardTransactions || forceFetch || _alwaysFetchCardIssuerCardTransactions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardIssuerCardTransactions);
				_cardIssuerCardTransactions.SuppressClearInGetMulti=!forceFetch;
				_cardIssuerCardTransactions.EntityFactoryToUse = entityFactoryToUse;
				_cardIssuerCardTransactions.GetMultiManyToOne(this, null, null, filter);
				_cardIssuerCardTransactions.SuppressClearInGetMulti=false;
				_alreadyFetchedCardIssuerCardTransactions = true;
			}
			return _cardIssuerCardTransactions;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardIssuerCardTransactions'. These settings will be taken into account
		/// when the property CardIssuerCardTransactions is requested or GetMultiCardIssuerCardTransactions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardIssuerCardTransactions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardIssuerCardTransactions.SortClauses=sortClauses;
			_cardIssuerCardTransactions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UnitEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollection GetMultiUmUnits(bool forceFetch)
		{
			return GetMultiUmUnits(forceFetch, _umUnits.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UnitEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollection GetMultiUmUnits(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUmUnits(forceFetch, _umUnits.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollection GetMultiUmUnits(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUmUnits(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollection GetMultiUmUnits(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUmUnits || forceFetch || _alwaysFetchUmUnits) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_umUnits);
				_umUnits.SuppressClearInGetMulti=!forceFetch;
				_umUnits.EntityFactoryToUse = entityFactoryToUse;
				_umUnits.GetMultiManyToOne(this, null, null, filter);
				_umUnits.SuppressClearInGetMulti=false;
				_alreadyFetchedUmUnits = true;
			}
			return _umUnits;
		}

		/// <summary> Sets the collection parameters for the collection for 'UmUnits'. These settings will be taken into account
		/// when the property UmUnits is requested or GetMultiUmUnits is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUmUnits(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_umUnits.SortClauses=sortClauses;
			_umUnits.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUsers(bool forceFetch)
		{
			return GetMultiUsers(forceFetch, _users.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUsers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUsers(forceFetch, _users.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUsers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUsers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUsers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUsers || forceFetch || _alwaysFetchUsers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_users);
				_users.SuppressClearInGetMulti=!forceFetch;
				_users.EntityFactoryToUse = entityFactoryToUse;
				_users.GetMultiManyToOne(this, null, null, null, null, null, filter);
				_users.SuppressClearInGetMulti=false;
				_alreadyFetchedUsers = true;
			}
			return _users;
		}

		/// <summary> Sets the collection parameters for the collection for 'Users'. These settings will be taken into account
		/// when the property Users is requested or GetMultiUsers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUsers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_users.SortClauses=sortClauses;
			_users.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VarioSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiSettlements(bool forceFetch)
		{
			return GetMultiSettlements(forceFetch, _settlements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VarioSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiSettlements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlements(forceFetch, _settlements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiSettlements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiSettlements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlements || forceFetch || _alwaysFetchSettlements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlements);
				_settlements.SuppressClearInGetMulti=!forceFetch;
				_settlements.EntityFactoryToUse = entityFactoryToUse;
				_settlements.GetMultiManyToOne(this, null, null, filter);
				_settlements.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlements = true;
			}
			return _settlements;
		}

		/// <summary> Sets the collection parameters for the collection for 'Settlements'. These settings will be taken into account
		/// when the property Settlements is requested or GetMultiSettlements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlements.SortClauses=sortClauses;
			_settlements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvKeySetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvKeySetCollection GetMultiVdvKeySet(bool forceFetch)
		{
			return GetMultiVdvKeySet(forceFetch, _vdvKeySet.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvKeySetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvKeySetCollection GetMultiVdvKeySet(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvKeySet(forceFetch, _vdvKeySet.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvKeySetCollection GetMultiVdvKeySet(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvKeySet(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvKeySetCollection GetMultiVdvKeySet(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvKeySet || forceFetch || _alwaysFetchVdvKeySet) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvKeySet);
				_vdvKeySet.SuppressClearInGetMulti=!forceFetch;
				_vdvKeySet.EntityFactoryToUse = entityFactoryToUse;
				_vdvKeySet.GetMultiManyToOne(this, null, null, filter);
				_vdvKeySet.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvKeySet = true;
			}
			return _vdvKeySet;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvKeySet'. These settings will be taken into account
		/// when the property VdvKeySet is requested or GetMultiVdvKeySet is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvKeySet(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvKeySet.SortClauses=sortClauses;
			_vdvKeySet.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'WorkstationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'WorkstationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.WorkstationCollection GetMultiWorkstations(bool forceFetch)
		{
			return GetMultiWorkstations(forceFetch, _workstations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WorkstationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'WorkstationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.WorkstationCollection GetMultiWorkstations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiWorkstations(forceFetch, _workstations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'WorkstationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.WorkstationCollection GetMultiWorkstations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiWorkstations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WorkstationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.WorkstationCollection GetMultiWorkstations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedWorkstations || forceFetch || _alwaysFetchWorkstations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_workstations);
				_workstations.SuppressClearInGetMulti=!forceFetch;
				_workstations.EntityFactoryToUse = entityFactoryToUse;
				_workstations.GetMultiManyToOne(this, filter);
				_workstations.SuppressClearInGetMulti=false;
				_alreadyFetchedWorkstations = true;
			}
			return _workstations;
		}

		/// <summary> Sets the collection parameters for the collection for 'Workstations'. These settings will be taken into account
		/// when the property Workstations is requested or GetMultiWorkstations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWorkstations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_workstations.SortClauses=sortClauses;
			_workstations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AddressBookEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AddressBookEntryCollection GetMultiAddressBookEntries(bool forceFetch)
		{
			return GetMultiAddressBookEntries(forceFetch, _addressBookEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AddressBookEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AddressBookEntryCollection GetMultiAddressBookEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAddressBookEntries(forceFetch, _addressBookEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AddressBookEntryCollection GetMultiAddressBookEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAddressBookEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AddressBookEntryCollection GetMultiAddressBookEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAddressBookEntries || forceFetch || _alwaysFetchAddressBookEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_addressBookEntries);
				_addressBookEntries.SuppressClearInGetMulti=!forceFetch;
				_addressBookEntries.EntityFactoryToUse = entityFactoryToUse;
				_addressBookEntries.GetMultiManyToOne(this, null, null, filter);
				_addressBookEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedAddressBookEntries = true;
			}
			return _addressBookEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'AddressBookEntries'. These settings will be taken into account
		/// when the property AddressBookEntries is requested or GetMultiAddressBookEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAddressBookEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_addressBookEntries.SortClauses=sortClauses;
			_addressBookEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch)
		{
			return GetMultiCards(forceFetch, _cards.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCards(forceFetch, _cards.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCards(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCards || forceFetch || _alwaysFetchCards) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cards);
				_cards.SuppressClearInGetMulti=!forceFetch;
				_cards.EntityFactoryToUse = entityFactoryToUse;
				_cards.GetMultiManyToOne(this, null, null, null, null, null, null, filter);
				_cards.SuppressClearInGetMulti=false;
				_alreadyFetchedCards = true;
			}
			return _cards;
		}

		/// <summary> Sets the collection parameters for the collection for 'Cards'. These settings will be taken into account
		/// when the property Cards is requested or GetMultiCards is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCards(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cards.SortClauses=sortClauses;
			_cards.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationDefinitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ConfigurationDefinitionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ConfigurationDefinitionCollection GetMultiConfigurationDefinitions(bool forceFetch)
		{
			return GetMultiConfigurationDefinitions(forceFetch, _configurationDefinitions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationDefinitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ConfigurationDefinitionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ConfigurationDefinitionCollection GetMultiConfigurationDefinitions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiConfigurationDefinitions(forceFetch, _configurationDefinitions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationDefinitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ConfigurationDefinitionCollection GetMultiConfigurationDefinitions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiConfigurationDefinitions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationDefinitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ConfigurationDefinitionCollection GetMultiConfigurationDefinitions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedConfigurationDefinitions || forceFetch || _alwaysFetchConfigurationDefinitions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_configurationDefinitions);
				_configurationDefinitions.SuppressClearInGetMulti=!forceFetch;
				_configurationDefinitions.EntityFactoryToUse = entityFactoryToUse;
				_configurationDefinitions.GetMultiManyToOne(this, filter);
				_configurationDefinitions.SuppressClearInGetMulti=false;
				_alreadyFetchedConfigurationDefinitions = true;
			}
			return _configurationDefinitions;
		}

		/// <summary> Sets the collection parameters for the collection for 'ConfigurationDefinitions'. These settings will be taken into account
		/// when the property ConfigurationDefinitions is requested or GetMultiConfigurationDefinitions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersConfigurationDefinitions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_configurationDefinitions.SortClauses=sortClauses;
			_configurationDefinitions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationOverwriteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ConfigurationOverwriteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection GetMultiConfigurationOverwrites(bool forceFetch)
		{
			return GetMultiConfigurationOverwrites(forceFetch, _configurationOverwrites.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationOverwriteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ConfigurationOverwriteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection GetMultiConfigurationOverwrites(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiConfigurationOverwrites(forceFetch, _configurationOverwrites.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationOverwriteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection GetMultiConfigurationOverwrites(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiConfigurationOverwrites(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationOverwriteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection GetMultiConfigurationOverwrites(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedConfigurationOverwrites || forceFetch || _alwaysFetchConfigurationOverwrites) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_configurationOverwrites);
				_configurationOverwrites.SuppressClearInGetMulti=!forceFetch;
				_configurationOverwrites.EntityFactoryToUse = entityFactoryToUse;
				_configurationOverwrites.GetMultiManyToOne(this, null, filter);
				_configurationOverwrites.SuppressClearInGetMulti=false;
				_alreadyFetchedConfigurationOverwrites = true;
			}
			return _configurationOverwrites;
		}

		/// <summary> Sets the collection parameters for the collection for 'ConfigurationOverwrites'. These settings will be taken into account
		/// when the property ConfigurationOverwrites is requested or GetMultiConfigurationOverwrites is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersConfigurationOverwrites(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_configurationOverwrites.SortClauses=sortClauses;
			_configurationOverwrites.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch)
		{
			return GetMultiContracts(forceFetch, _contracts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContracts(forceFetch, _contracts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContracts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContracts || forceFetch || _alwaysFetchContracts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contracts);
				_contracts.SuppressClearInGetMulti=!forceFetch;
				_contracts.EntityFactoryToUse = entityFactoryToUse;
				_contracts.GetMultiManyToOne(this, null, null, null, filter);
				_contracts.SuppressClearInGetMulti=false;
				_alreadyFetchedContracts = true;
			}
			return _contracts;
		}

		/// <summary> Sets the collection parameters for the collection for 'Contracts'. These settings will be taken into account
		/// when the property Contracts is requested or GetMultiContracts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContracts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contracts.SortClauses=sortClauses;
			_contracts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiVanpoolContracts(bool forceFetch)
		{
			return GetMultiVanpoolContracts(forceFetch, _vanpoolContracts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiVanpoolContracts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVanpoolContracts(forceFetch, _vanpoolContracts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiVanpoolContracts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVanpoolContracts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractCollection GetMultiVanpoolContracts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVanpoolContracts || forceFetch || _alwaysFetchVanpoolContracts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vanpoolContracts);
				_vanpoolContracts.SuppressClearInGetMulti=!forceFetch;
				_vanpoolContracts.EntityFactoryToUse = entityFactoryToUse;
				_vanpoolContracts.GetMultiManyToOne(null, this, null, null, filter);
				_vanpoolContracts.SuppressClearInGetMulti=false;
				_alreadyFetchedVanpoolContracts = true;
			}
			return _vanpoolContracts;
		}

		/// <summary> Sets the collection parameters for the collection for 'VanpoolContracts'. These settings will be taken into account
		/// when the property VanpoolContracts is requested or GetMultiVanpoolContracts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVanpoolContracts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vanpoolContracts.SortClauses=sortClauses;
			_vanpoolContracts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CryptoCryptogramArchiveEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CryptoCryptogramArchiveEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoCryptogramArchiveCollection GetMultiCryptoCryptogramArchives(bool forceFetch)
		{
			return GetMultiCryptoCryptogramArchives(forceFetch, _cryptoCryptogramArchives.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoCryptogramArchiveEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CryptoCryptogramArchiveEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoCryptogramArchiveCollection GetMultiCryptoCryptogramArchives(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCryptoCryptogramArchives(forceFetch, _cryptoCryptogramArchives.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CryptoCryptogramArchiveEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CryptoCryptogramArchiveCollection GetMultiCryptoCryptogramArchives(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCryptoCryptogramArchives(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoCryptogramArchiveEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CryptoCryptogramArchiveCollection GetMultiCryptoCryptogramArchives(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCryptoCryptogramArchives || forceFetch || _alwaysFetchCryptoCryptogramArchives) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cryptoCryptogramArchives);
				_cryptoCryptogramArchives.SuppressClearInGetMulti=!forceFetch;
				_cryptoCryptogramArchives.EntityFactoryToUse = entityFactoryToUse;
				_cryptoCryptogramArchives.GetMultiManyToOne(this, filter);
				_cryptoCryptogramArchives.SuppressClearInGetMulti=false;
				_alreadyFetchedCryptoCryptogramArchives = true;
			}
			return _cryptoCryptogramArchives;
		}

		/// <summary> Sets the collection parameters for the collection for 'CryptoCryptogramArchives'. These settings will be taken into account
		/// when the property CryptoCryptogramArchives is requested or GetMultiCryptoCryptogramArchives is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCryptoCryptogramArchives(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cryptoCryptogramArchives.SortClauses=sortClauses;
			_cryptoCryptogramArchives.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CryptoQArchiveEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CryptoQArchiveEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoQArchiveCollection GetMultiCryptoQArchives(bool forceFetch)
		{
			return GetMultiCryptoQArchives(forceFetch, _cryptoQArchives.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoQArchiveEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CryptoQArchiveEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoQArchiveCollection GetMultiCryptoQArchives(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCryptoQArchives(forceFetch, _cryptoQArchives.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CryptoQArchiveEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CryptoQArchiveCollection GetMultiCryptoQArchives(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCryptoQArchives(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoQArchiveEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CryptoQArchiveCollection GetMultiCryptoQArchives(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCryptoQArchives || forceFetch || _alwaysFetchCryptoQArchives) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cryptoQArchives);
				_cryptoQArchives.SuppressClearInGetMulti=!forceFetch;
				_cryptoQArchives.EntityFactoryToUse = entityFactoryToUse;
				_cryptoQArchives.GetMultiManyToOne(this, filter);
				_cryptoQArchives.SuppressClearInGetMulti=false;
				_alreadyFetchedCryptoQArchives = true;
			}
			return _cryptoQArchives;
		}

		/// <summary> Sets the collection parameters for the collection for 'CryptoQArchives'. These settings will be taken into account
		/// when the property CryptoQArchives is requested or GetMultiCryptoQArchives is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCryptoQArchives(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cryptoQArchives.SortClauses=sortClauses;
			_cryptoQArchives.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareChangeCauseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareChangeCauseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareChangeCauseCollection GetMultiFareChangeCauses(bool forceFetch)
		{
			return GetMultiFareChangeCauses(forceFetch, _fareChangeCauses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareChangeCauseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareChangeCauseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareChangeCauseCollection GetMultiFareChangeCauses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareChangeCauses(forceFetch, _fareChangeCauses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareChangeCauseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareChangeCauseCollection GetMultiFareChangeCauses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareChangeCauses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareChangeCauseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareChangeCauseCollection GetMultiFareChangeCauses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareChangeCauses || forceFetch || _alwaysFetchFareChangeCauses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareChangeCauses);
				_fareChangeCauses.SuppressClearInGetMulti=!forceFetch;
				_fareChangeCauses.EntityFactoryToUse = entityFactoryToUse;
				_fareChangeCauses.GetMultiManyToOne(this, filter);
				_fareChangeCauses.SuppressClearInGetMulti=false;
				_alreadyFetchedFareChangeCauses = true;
			}
			return _fareChangeCauses;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareChangeCauses'. These settings will be taken into account
		/// when the property FareChangeCauses is requested or GetMultiFareChangeCauses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareChangeCauses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareChangeCauses.SortClauses=sortClauses;
			_fareChangeCauses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionBehaviourEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionBehaviourEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionBehaviourCollection GetMultiFareEvasionBehaviours(bool forceFetch)
		{
			return GetMultiFareEvasionBehaviours(forceFetch, _fareEvasionBehaviours.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionBehaviourEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionBehaviourEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionBehaviourCollection GetMultiFareEvasionBehaviours(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareEvasionBehaviours(forceFetch, _fareEvasionBehaviours.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionBehaviourEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionBehaviourCollection GetMultiFareEvasionBehaviours(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareEvasionBehaviours(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionBehaviourEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionBehaviourCollection GetMultiFareEvasionBehaviours(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareEvasionBehaviours || forceFetch || _alwaysFetchFareEvasionBehaviours) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareEvasionBehaviours);
				_fareEvasionBehaviours.SuppressClearInGetMulti=!forceFetch;
				_fareEvasionBehaviours.EntityFactoryToUse = entityFactoryToUse;
				_fareEvasionBehaviours.GetMultiManyToOne(this, filter);
				_fareEvasionBehaviours.SuppressClearInGetMulti=false;
				_alreadyFetchedFareEvasionBehaviours = true;
			}
			return _fareEvasionBehaviours;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareEvasionBehaviours'. These settings will be taken into account
		/// when the property FareEvasionBehaviours is requested or GetMultiFareEvasionBehaviours is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareEvasionBehaviours(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareEvasionBehaviours.SortClauses=sortClauses;
			_fareEvasionBehaviours.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch)
		{
			return GetMultiFareEvasionIncidents(forceFetch, _fareEvasionIncidents.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareEvasionIncidents(forceFetch, _fareEvasionIncidents.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareEvasionIncidents(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareEvasionIncidents || forceFetch || _alwaysFetchFareEvasionIncidents) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareEvasionIncidents);
				_fareEvasionIncidents.SuppressClearInGetMulti=!forceFetch;
				_fareEvasionIncidents.EntityFactoryToUse = entityFactoryToUse;
				_fareEvasionIncidents.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, null, null, null, null, filter);
				_fareEvasionIncidents.SuppressClearInGetMulti=false;
				_alreadyFetchedFareEvasionIncidents = true;
			}
			return _fareEvasionIncidents;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareEvasionIncidents'. These settings will be taken into account
		/// when the property FareEvasionIncidents is requested or GetMultiFareEvasionIncidents is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareEvasionIncidents(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareEvasionIncidents.SortClauses=sortClauses;
			_fareEvasionIncidents.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionInspectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionInspectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionInspectionCollection GetMultiFareEvasionInspections(bool forceFetch)
		{
			return GetMultiFareEvasionInspections(forceFetch, _fareEvasionInspections.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionInspectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionInspectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionInspectionCollection GetMultiFareEvasionInspections(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareEvasionInspections(forceFetch, _fareEvasionInspections.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionInspectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionInspectionCollection GetMultiFareEvasionInspections(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareEvasionInspections(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionInspectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionInspectionCollection GetMultiFareEvasionInspections(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareEvasionInspections || forceFetch || _alwaysFetchFareEvasionInspections) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareEvasionInspections);
				_fareEvasionInspections.SuppressClearInGetMulti=!forceFetch;
				_fareEvasionInspections.EntityFactoryToUse = entityFactoryToUse;
				_fareEvasionInspections.GetMultiManyToOne(this, filter);
				_fareEvasionInspections.SuppressClearInGetMulti=false;
				_alreadyFetchedFareEvasionInspections = true;
			}
			return _fareEvasionInspections;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareEvasionInspections'. These settings will be taken into account
		/// when the property FareEvasionInspections is requested or GetMultiFareEvasionInspections is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareEvasionInspections(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareEvasionInspections.SortClauses=sortClauses;
			_fareEvasionInspections.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FilterValueSetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets(bool forceFetch)
		{
			return GetMultiFilterValueSets(forceFetch, _filterValueSets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FilterValueSetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFilterValueSets(forceFetch, _filterValueSets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFilterValueSets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFilterValueSets || forceFetch || _alwaysFetchFilterValueSets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_filterValueSets);
				_filterValueSets.SuppressClearInGetMulti=!forceFetch;
				_filterValueSets.EntityFactoryToUse = entityFactoryToUse;
				_filterValueSets.GetMultiManyToOne(this, null, null, null, filter);
				_filterValueSets.SuppressClearInGetMulti=false;
				_alreadyFetchedFilterValueSets = true;
			}
			return _filterValueSets;
		}

		/// <summary> Sets the collection parameters for the collection for 'FilterValueSets'. These settings will be taken into account
		/// when the property FilterValueSets is requested or GetMultiFilterValueSets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFilterValueSets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_filterValueSets.SortClauses=sortClauses;
			_filterValueSets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FormEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FormEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FormCollection GetMultiForms(bool forceFetch)
		{
			return GetMultiForms(forceFetch, _forms.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FormEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FormEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FormCollection GetMultiForms(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiForms(forceFetch, _forms.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FormEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FormCollection GetMultiForms(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiForms(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FormEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FormCollection GetMultiForms(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedForms || forceFetch || _alwaysFetchForms) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_forms);
				_forms.SuppressClearInGetMulti=!forceFetch;
				_forms.EntityFactoryToUse = entityFactoryToUse;
				_forms.GetMultiManyToOne(this, filter);
				_forms.SuppressClearInGetMulti=false;
				_alreadyFetchedForms = true;
			}
			return _forms;
		}

		/// <summary> Sets the collection parameters for the collection for 'Forms'. These settings will be taken into account
		/// when the property Forms is requested or GetMultiForms is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersForms(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_forms.SortClauses=sortClauses;
			_forms.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'IdentificationTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'IdentificationTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.IdentificationTypeCollection GetMultiIdentificationTypes(bool forceFetch)
		{
			return GetMultiIdentificationTypes(forceFetch, _identificationTypes.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'IdentificationTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'IdentificationTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.IdentificationTypeCollection GetMultiIdentificationTypes(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiIdentificationTypes(forceFetch, _identificationTypes.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'IdentificationTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.IdentificationTypeCollection GetMultiIdentificationTypes(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiIdentificationTypes(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'IdentificationTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.IdentificationTypeCollection GetMultiIdentificationTypes(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedIdentificationTypes || forceFetch || _alwaysFetchIdentificationTypes) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_identificationTypes);
				_identificationTypes.SuppressClearInGetMulti=!forceFetch;
				_identificationTypes.EntityFactoryToUse = entityFactoryToUse;
				_identificationTypes.GetMultiManyToOne(this, filter);
				_identificationTypes.SuppressClearInGetMulti=false;
				_alreadyFetchedIdentificationTypes = true;
			}
			return _identificationTypes;
		}

		/// <summary> Sets the collection parameters for the collection for 'IdentificationTypes'. These settings will be taken into account
		/// when the property IdentificationTypes is requested or GetMultiIdentificationTypes is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersIdentificationTypes(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_identificationTypes.SortClauses=sortClauses;
			_identificationTypes.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch)
		{
			return GetMultiInvoices(forceFetch, _invoices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiInvoices(forceFetch, _invoices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiInvoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedInvoices || forceFetch || _alwaysFetchInvoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_invoices);
				_invoices.SuppressClearInGetMulti=!forceFetch;
				_invoices.EntityFactoryToUse = entityFactoryToUse;
				_invoices.GetMultiManyToOne(this, null, null, null, null, null, filter);
				_invoices.SuppressClearInGetMulti=false;
				_alreadyFetchedInvoices = true;
			}
			return _invoices;
		}

		/// <summary> Sets the collection parameters for the collection for 'Invoices'. These settings will be taken into account
		/// when the property Invoices is requested or GetMultiInvoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersInvoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_invoices.SortClauses=sortClauses;
			_invoices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'InvoicingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'InvoicingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoicingCollection GetMultiInvoicings(bool forceFetch)
		{
			return GetMultiInvoicings(forceFetch, _invoicings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoicingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'InvoicingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoicingCollection GetMultiInvoicings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiInvoicings(forceFetch, _invoicings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'InvoicingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.InvoicingCollection GetMultiInvoicings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiInvoicings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoicingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.InvoicingCollection GetMultiInvoicings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedInvoicings || forceFetch || _alwaysFetchInvoicings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_invoicings);
				_invoicings.SuppressClearInGetMulti=!forceFetch;
				_invoicings.EntityFactoryToUse = entityFactoryToUse;
				_invoicings.GetMultiManyToOne(this, null, filter);
				_invoicings.SuppressClearInGetMulti=false;
				_alreadyFetchedInvoicings = true;
			}
			return _invoicings;
		}

		/// <summary> Sets the collection parameters for the collection for 'Invoicings'. These settings will be taken into account
		/// when the property Invoicings is requested or GetMultiInvoicings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersInvoicings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_invoicings.SortClauses=sortClauses;
			_invoicings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'JobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch)
		{
			return GetMultiJobs(forceFetch, _jobs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'JobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiJobs(forceFetch, _jobs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiJobs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedJobs || forceFetch || _alwaysFetchJobs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_jobs);
				_jobs.SuppressClearInGetMulti=!forceFetch;
				_jobs.EntityFactoryToUse = entityFactoryToUse;
				_jobs.GetMultiManyToOne(this, null, null, null, null, null, null, null, filter);
				_jobs.SuppressClearInGetMulti=false;
				_alreadyFetchedJobs = true;
			}
			return _jobs;
		}

		/// <summary> Sets the collection parameters for the collection for 'Jobs'. These settings will be taken into account
		/// when the property Jobs is requested or GetMultiJobs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersJobs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_jobs.SortClauses=sortClauses;
			_jobs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterArchiveEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterArchiveCollection GetMultiParameterArchives(bool forceFetch)
		{
			return GetMultiParameterArchives(forceFetch, _parameterArchives.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterArchiveEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterArchiveCollection GetMultiParameterArchives(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterArchives(forceFetch, _parameterArchives.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterArchiveCollection GetMultiParameterArchives(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterArchives(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterArchiveCollection GetMultiParameterArchives(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterArchives || forceFetch || _alwaysFetchParameterArchives) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterArchives);
				_parameterArchives.SuppressClearInGetMulti=!forceFetch;
				_parameterArchives.EntityFactoryToUse = entityFactoryToUse;
				_parameterArchives.GetMultiManyToOne(this, filter);
				_parameterArchives.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterArchives = true;
			}
			return _parameterArchives;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterArchives'. These settings will be taken into account
		/// when the property ParameterArchives is requested or GetMultiParameterArchives is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterArchives(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterArchives.SortClauses=sortClauses;
			_parameterArchives.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PostingKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PostingKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingKeyCollection GetMultiPostingKeys(bool forceFetch)
		{
			return GetMultiPostingKeys(forceFetch, _postingKeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PostingKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingKeyCollection GetMultiPostingKeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPostingKeys(forceFetch, _postingKeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PostingKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PostingKeyCollection GetMultiPostingKeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPostingKeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PostingKeyCollection GetMultiPostingKeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPostingKeys || forceFetch || _alwaysFetchPostingKeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_postingKeys);
				_postingKeys.SuppressClearInGetMulti=!forceFetch;
				_postingKeys.EntityFactoryToUse = entityFactoryToUse;
				_postingKeys.GetMultiManyToOne(this, filter);
				_postingKeys.SuppressClearInGetMulti=false;
				_alreadyFetchedPostingKeys = true;
			}
			return _postingKeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'PostingKeys'. These settings will be taken into account
		/// when the property PostingKeys is requested or GetMultiPostingKeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPostingKeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_postingKeys.SortClauses=sortClauses;
			_postingKeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProducts || forceFetch || _alwaysFetchProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_products);
				_products.SuppressClearInGetMulti=!forceFetch;
				_products.EntityFactoryToUse = entityFactoryToUse;
				_products.GetMultiManyToOne(this, null, null, null, null, null, null, null, null, null, filter);
				_products.SuppressClearInGetMulti=false;
				_alreadyFetchedProducts = true;
			}
			return _products;
		}

		/// <summary> Sets the collection parameters for the collection for 'Products'. These settings will be taken into account
		/// when the property Products is requested or GetMultiProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_products.SortClauses=sortClauses;
			_products.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch)
		{
			return GetMultiReportJobs(forceFetch, _reportJobs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportJobs(forceFetch, _reportJobs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportJobs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportJobs || forceFetch || _alwaysFetchReportJobs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportJobs);
				_reportJobs.SuppressClearInGetMulti=!forceFetch;
				_reportJobs.EntityFactoryToUse = entityFactoryToUse;
				_reportJobs.GetMultiManyToOne(this, null, null, null, filter);
				_reportJobs.SuppressClearInGetMulti=false;
				_alreadyFetchedReportJobs = true;
			}
			return _reportJobs;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportJobs'. These settings will be taken into account
		/// when the property ReportJobs is requested or GetMultiReportJobs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportJobs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportJobs.SortClauses=sortClauses;
			_reportJobs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobResultCollection GetMultiReportJobResults(bool forceFetch)
		{
			return GetMultiReportJobResults(forceFetch, _reportJobResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobResultCollection GetMultiReportJobResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportJobResults(forceFetch, _reportJobResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobResultCollection GetMultiReportJobResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportJobResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobResultCollection GetMultiReportJobResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportJobResults || forceFetch || _alwaysFetchReportJobResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportJobResults);
				_reportJobResults.SuppressClearInGetMulti=!forceFetch;
				_reportJobResults.EntityFactoryToUse = entityFactoryToUse;
				_reportJobResults.GetMultiManyToOne(this, null, null, filter);
				_reportJobResults.SuppressClearInGetMulti=false;
				_alreadyFetchedReportJobResults = true;
			}
			return _reportJobResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportJobResults'. These settings will be taken into account
		/// when the property ReportJobResults is requested or GetMultiReportJobResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportJobResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportJobResults.SortClauses=sortClauses;
			_reportJobResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportToClientEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportToClientCollection GetMultiReportToClients(bool forceFetch)
		{
			return GetMultiReportToClients(forceFetch, _reportToClients.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportToClientEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportToClientCollection GetMultiReportToClients(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportToClients(forceFetch, _reportToClients.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportToClientCollection GetMultiReportToClients(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportToClients(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportToClientCollection GetMultiReportToClients(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportToClients || forceFetch || _alwaysFetchReportToClients) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportToClients);
				_reportToClients.SuppressClearInGetMulti=!forceFetch;
				_reportToClients.EntityFactoryToUse = entityFactoryToUse;
				_reportToClients.GetMultiManyToOne(this, null, null, null, filter);
				_reportToClients.SuppressClearInGetMulti=false;
				_alreadyFetchedReportToClients = true;
			}
			return _reportToClients;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportToClients'. These settings will be taken into account
		/// when the property ReportToClients is requested or GetMultiReportToClients is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportToClients(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportToClients.SortClauses=sortClauses;
			_reportToClients.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RevenueSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RevenueSettlementCollection GetMultiRevenueSettlements(bool forceFetch)
		{
			return GetMultiRevenueSettlements(forceFetch, _revenueSettlements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RevenueSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RevenueSettlementCollection GetMultiRevenueSettlements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRevenueSettlements(forceFetch, _revenueSettlements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RevenueSettlementCollection GetMultiRevenueSettlements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRevenueSettlements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RevenueSettlementCollection GetMultiRevenueSettlements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRevenueSettlements || forceFetch || _alwaysFetchRevenueSettlements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_revenueSettlements);
				_revenueSettlements.SuppressClearInGetMulti=!forceFetch;
				_revenueSettlements.EntityFactoryToUse = entityFactoryToUse;
				_revenueSettlements.GetMultiManyToOne(this, null, null, filter);
				_revenueSettlements.SuppressClearInGetMulti=false;
				_alreadyFetchedRevenueSettlements = true;
			}
			return _revenueSettlements;
		}

		/// <summary> Sets the collection parameters for the collection for 'RevenueSettlements'. These settings will be taken into account
		/// when the property RevenueSettlements is requested or GetMultiRevenueSettlements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRevenueSettlements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_revenueSettlements.SortClauses=sortClauses;
			_revenueSettlements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch)
		{
			return GetMultiSales(forceFetch, _sales.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSales(forceFetch, _sales.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSales(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSales || forceFetch || _alwaysFetchSales) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_sales);
				_sales.SuppressClearInGetMulti=!forceFetch;
				_sales.EntityFactoryToUse = entityFactoryToUse;
				_sales.GetMultiManyToOne(this, null, null, null, null, null, null, null, filter);
				_sales.SuppressClearInGetMulti=false;
				_alreadyFetchedSales = true;
			}
			return _sales;
		}

		/// <summary> Sets the collection parameters for the collection for 'Sales'. These settings will be taken into account
		/// when the property Sales is requested or GetMultiSales is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSales(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_sales.SortClauses=sortClauses;
			_sales.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SchoolYearEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SchoolYearEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SchoolYearCollection GetMultiSchoolYears(bool forceFetch)
		{
			return GetMultiSchoolYears(forceFetch, _schoolYears.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SchoolYearEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SchoolYearEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SchoolYearCollection GetMultiSchoolYears(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSchoolYears(forceFetch, _schoolYears.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SchoolYearEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SchoolYearCollection GetMultiSchoolYears(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSchoolYears(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SchoolYearEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SchoolYearCollection GetMultiSchoolYears(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSchoolYears || forceFetch || _alwaysFetchSchoolYears) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_schoolYears);
				_schoolYears.SuppressClearInGetMulti=!forceFetch;
				_schoolYears.EntityFactoryToUse = entityFactoryToUse;
				_schoolYears.GetMultiManyToOne(this, filter);
				_schoolYears.SuppressClearInGetMulti=false;
				_alreadyFetchedSchoolYears = true;
			}
			return _schoolYears;
		}

		/// <summary> Sets the collection parameters for the collection for 'SchoolYears'. These settings will be taken into account
		/// when the property SchoolYears is requested or GetMultiSchoolYears is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSchoolYears(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_schoolYears.SortClauses=sortClauses;
			_schoolYears.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'StorageLocationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'StorageLocationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StorageLocationCollection GetMultiStorageLocations(bool forceFetch)
		{
			return GetMultiStorageLocations(forceFetch, _storageLocations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StorageLocationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'StorageLocationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StorageLocationCollection GetMultiStorageLocations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiStorageLocations(forceFetch, _storageLocations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'StorageLocationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.StorageLocationCollection GetMultiStorageLocations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiStorageLocations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StorageLocationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.StorageLocationCollection GetMultiStorageLocations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedStorageLocations || forceFetch || _alwaysFetchStorageLocations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_storageLocations);
				_storageLocations.SuppressClearInGetMulti=!forceFetch;
				_storageLocations.EntityFactoryToUse = entityFactoryToUse;
				_storageLocations.GetMultiManyToOne(this, null, filter);
				_storageLocations.SuppressClearInGetMulti=false;
				_alreadyFetchedStorageLocations = true;
			}
			return _storageLocations;
		}

		/// <summary> Sets the collection parameters for the collection for 'StorageLocations'. These settings will be taken into account
		/// when the property StorageLocations is requested or GetMultiStorageLocations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersStorageLocations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_storageLocations.SortClauses=sortClauses;
			_storageLocations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournals || forceFetch || _alwaysFetchTransactionJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournals);
				_transactionJournals.SuppressClearInGetMulti=!forceFetch;
				_transactionJournals.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournals.GetMultiManyToOne(this, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_transactionJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournals = true;
			}
			return _transactionJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournals'. These settings will be taken into account
		/// when the property TransactionJournals is requested or GetMultiTransactionJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournals.SortClauses=sortClauses;
			_transactionJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransportCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransportCompanyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransportCompanyCollection GetMultiTransportCompanies(bool forceFetch)
		{
			return GetMultiTransportCompanies(forceFetch, _transportCompanies.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransportCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransportCompanyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransportCompanyCollection GetMultiTransportCompanies(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransportCompanies(forceFetch, _transportCompanies.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransportCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransportCompanyCollection GetMultiTransportCompanies(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransportCompanies(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransportCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransportCompanyCollection GetMultiTransportCompanies(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransportCompanies || forceFetch || _alwaysFetchTransportCompanies) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transportCompanies);
				_transportCompanies.SuppressClearInGetMulti=!forceFetch;
				_transportCompanies.EntityFactoryToUse = entityFactoryToUse;
				_transportCompanies.GetMultiManyToOne(this, filter);
				_transportCompanies.SuppressClearInGetMulti=false;
				_alreadyFetchedTransportCompanies = true;
			}
			return _transportCompanies;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransportCompanies'. These settings will be taken into account
		/// when the property TransportCompanies is requested or GetMultiTransportCompanies is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransportCompanies(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transportCompanies.SortClauses=sortClauses;
			_transportCompanies.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaTicketVendingClient(bool forceFetch)
		{
			return GetMultiTicketCollectionViaTicketVendingClient(forceFetch, _ticketCollectionViaTicketVendingClient.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaTicketVendingClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTicketCollectionViaTicketVendingClient || forceFetch || _alwaysFetchTicketCollectionViaTicketVendingClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketCollectionViaTicketVendingClient);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ClientFields.ClientID, ComparisonOperator.Equal, this.ClientID, "ClientEntity__"));
				_ticketCollectionViaTicketVendingClient.SuppressClearInGetMulti=!forceFetch;
				_ticketCollectionViaTicketVendingClient.EntityFactoryToUse = entityFactoryToUse;
				_ticketCollectionViaTicketVendingClient.GetMulti(filter, GetRelationsForField("TicketCollectionViaTicketVendingClient"));
				_ticketCollectionViaTicketVendingClient.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketCollectionViaTicketVendingClient = true;
			}
			return _ticketCollectionViaTicketVendingClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketCollectionViaTicketVendingClient'. These settings will be taken into account
		/// when the property TicketCollectionViaTicketVendingClient is requested or GetMultiTicketCollectionViaTicketVendingClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketCollectionViaTicketVendingClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketCollectionViaTicketVendingClient.SortClauses=sortClauses;
			_ticketCollectionViaTicketVendingClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReportCollectionViaReportToClient(bool forceFetch)
		{
			return GetMultiReportCollectionViaReportToClient(forceFetch, _reportCollectionViaReportToClient.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReportCollectionViaReportToClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedReportCollectionViaReportToClient || forceFetch || _alwaysFetchReportCollectionViaReportToClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportCollectionViaReportToClient);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ClientFields.ClientID, ComparisonOperator.Equal, this.ClientID, "ClientEntity__"));
				_reportCollectionViaReportToClient.SuppressClearInGetMulti=!forceFetch;
				_reportCollectionViaReportToClient.EntityFactoryToUse = entityFactoryToUse;
				_reportCollectionViaReportToClient.GetMulti(filter, GetRelationsForField("ReportCollectionViaReportToClient"));
				_reportCollectionViaReportToClient.SuppressClearInGetMulti=false;
				_alreadyFetchedReportCollectionViaReportToClient = true;
			}
			return _reportCollectionViaReportToClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportCollectionViaReportToClient'. These settings will be taken into account
		/// when the property ReportCollectionViaReportToClient is requested or GetMultiReportCollectionViaReportToClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportCollectionViaReportToClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportCollectionViaReportToClient.SortClauses=sortClauses;
			_reportCollectionViaReportToClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AccountEntryNumberEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'AccountEntryNumberEntity' which is related to this entity.</returns>
		public AccountEntryNumberEntity GetSingleAccountEntryNumber()
		{
			return GetSingleAccountEntryNumber(false);
		}
		
		/// <summary> Retrieves the related entity of type 'AccountEntryNumberEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AccountEntryNumberEntity' which is related to this entity.</returns>
		public virtual AccountEntryNumberEntity GetSingleAccountEntryNumber(bool forceFetch)
		{
			if( ( !_alreadyFetchedAccountEntryNumber || forceFetch || _alwaysFetchAccountEntryNumber) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AccountEntryNumberEntityUsingClientID);
				AccountEntryNumberEntity newEntity = new AccountEntryNumberEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (AccountEntryNumberEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_accountEntryNumberReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AccountEntryNumber = newEntity;
				_alreadyFetchedAccountEntryNumber = fetchResult;
			}
			return _accountEntryNumber;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AccountEntries", _accountEntries);
			toReturn.Add("ApportionmentResults", _apportionmentResults);
			toReturn.Add("ApportionmentResults1", _apportionmentResults1);
			toReturn.Add("ApportionmentResults2", _apportionmentResults2);
			toReturn.Add("Calendar", _calendar);
			toReturn.Add("ClearingResultsByCardIssuer", _clearingResultsByCardIssuer);
			toReturn.Add("ClearingResults", _clearingResults);
			toReturn.Add("ClearingResultsByClientFrom", _clearingResultsByClientFrom);
			toReturn.Add("ClearingResultsByClientTo", _clearingResultsByClientTo);
			toReturn.Add("ClientAdaptedLayoutObjects", _clientAdaptedLayoutObjects);
			toReturn.Add("Debtors", _debtors);
			toReturn.Add("Depots", _depots);
			toReturn.Add("FareMatrixes", _fareMatrixes);
			toReturn.Add("FareTables", _fareTables);
			toReturn.Add("Layouts", _layouts);
			toReturn.Add("WhoIsResponsible", _whoIsResponsible);
			toReturn.Add("ResponsibleFor", _responsibleFor);
			toReturn.Add("ClientTariffs", _clientTariffs);
			toReturn.Add("OwnerClientTariffs", _ownerClientTariffs);
			toReturn.Add("Tickets", _tickets);
			toReturn.Add("TicketCategories", _ticketCategories);
			toReturn.Add("TicketVendingClient", _ticketVendingClient);
			toReturn.Add("CardIssuerCardTransactions", _cardIssuerCardTransactions);
			toReturn.Add("UmUnits", _umUnits);
			toReturn.Add("Users", _users);
			toReturn.Add("Settlements", _settlements);
			toReturn.Add("VdvKeySet", _vdvKeySet);
			toReturn.Add("Workstations", _workstations);
			toReturn.Add("AddressBookEntries", _addressBookEntries);
			toReturn.Add("Cards", _cards);
			toReturn.Add("ConfigurationDefinitions", _configurationDefinitions);
			toReturn.Add("ConfigurationOverwrites", _configurationOverwrites);
			toReturn.Add("Contracts", _contracts);
			toReturn.Add("VanpoolContracts", _vanpoolContracts);
			toReturn.Add("CryptoCryptogramArchives", _cryptoCryptogramArchives);
			toReturn.Add("CryptoQArchives", _cryptoQArchives);
			toReturn.Add("FareChangeCauses", _fareChangeCauses);
			toReturn.Add("FareEvasionBehaviours", _fareEvasionBehaviours);
			toReturn.Add("FareEvasionIncidents", _fareEvasionIncidents);
			toReturn.Add("FareEvasionInspections", _fareEvasionInspections);
			toReturn.Add("FilterValueSets", _filterValueSets);
			toReturn.Add("Forms", _forms);
			toReturn.Add("IdentificationTypes", _identificationTypes);
			toReturn.Add("Invoices", _invoices);
			toReturn.Add("Invoicings", _invoicings);
			toReturn.Add("Jobs", _jobs);
			toReturn.Add("ParameterArchives", _parameterArchives);
			toReturn.Add("PostingKeys", _postingKeys);
			toReturn.Add("Products", _products);
			toReturn.Add("ReportJobs", _reportJobs);
			toReturn.Add("ReportJobResults", _reportJobResults);
			toReturn.Add("ReportToClients", _reportToClients);
			toReturn.Add("RevenueSettlements", _revenueSettlements);
			toReturn.Add("Sales", _sales);
			toReturn.Add("SchoolYears", _schoolYears);
			toReturn.Add("StorageLocations", _storageLocations);
			toReturn.Add("TransactionJournals", _transactionJournals);
			toReturn.Add("TransportCompanies", _transportCompanies);
			toReturn.Add("TicketCollectionViaTicketVendingClient", _ticketCollectionViaTicketVendingClient);
			toReturn.Add("ReportCollectionViaReportToClient", _reportCollectionViaReportToClient);
			toReturn.Add("AccountEntryNumber", _accountEntryNumber);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="clientID">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="validator">The validator object for this ClientEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 clientID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(clientID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_accountEntries = new VarioSL.Entities.CollectionClasses.AccountEntryCollection();
			_accountEntries.SetContainingEntityInfo(this, "Client");

			_apportionmentResults = new VarioSL.Entities.CollectionClasses.ApportionmentResultCollection();
			_apportionmentResults.SetContainingEntityInfo(this, "AcquirerClient");

			_apportionmentResults1 = new VarioSL.Entities.CollectionClasses.ApportionmentResultCollection();
			_apportionmentResults1.SetContainingEntityInfo(this, "FromClient");

			_apportionmentResults2 = new VarioSL.Entities.CollectionClasses.ApportionmentResultCollection();
			_apportionmentResults2.SetContainingEntityInfo(this, "ToClient");

			_calendar = new VarioSL.Entities.CollectionClasses.CalendarCollection();
			_calendar.SetContainingEntityInfo(this, "Client");

			_clearingResultsByCardIssuer = new VarioSL.Entities.CollectionClasses.ClearingResultCollection();
			_clearingResultsByCardIssuer.SetContainingEntityInfo(this, "CardIssuer");

			_clearingResults = new VarioSL.Entities.CollectionClasses.ClearingResultCollection();
			_clearingResults.SetContainingEntityInfo(this, "Client");

			_clearingResultsByClientFrom = new VarioSL.Entities.CollectionClasses.ClearingResultCollection();
			_clearingResultsByClientFrom.SetContainingEntityInfo(this, "ClientFrom");

			_clearingResultsByClientTo = new VarioSL.Entities.CollectionClasses.ClearingResultCollection();
			_clearingResultsByClientTo.SetContainingEntityInfo(this, "ClientTo");

			_clientAdaptedLayoutObjects = new VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection();
			_clientAdaptedLayoutObjects.SetContainingEntityInfo(this, "Client");

			_debtors = new VarioSL.Entities.CollectionClasses.DebtorCollection();
			_debtors.SetContainingEntityInfo(this, "Client");

			_depots = new VarioSL.Entities.CollectionClasses.DepotCollection();
			_depots.SetContainingEntityInfo(this, "Client");

			_fareMatrixes = new VarioSL.Entities.CollectionClasses.FareMatrixCollection();
			_fareMatrixes.SetContainingEntityInfo(this, "Client");

			_fareTables = new VarioSL.Entities.CollectionClasses.FareTableCollection();
			_fareTables.SetContainingEntityInfo(this, "Client");

			_layouts = new VarioSL.Entities.CollectionClasses.LayoutCollection();
			_layouts.SetContainingEntityInfo(this, "Client");

			_whoIsResponsible = new VarioSL.Entities.CollectionClasses.ResponsibilityCollection();
			_whoIsResponsible.SetContainingEntityInfo(this, "Client");

			_responsibleFor = new VarioSL.Entities.CollectionClasses.ResponsibilityCollection();
			_responsibleFor.SetContainingEntityInfo(this, "ResponsibleClient");

			_clientTariffs = new VarioSL.Entities.CollectionClasses.TariffCollection();
			_clientTariffs.SetContainingEntityInfo(this, "Client");

			_ownerClientTariffs = new VarioSL.Entities.CollectionClasses.TariffCollection();
			_ownerClientTariffs.SetContainingEntityInfo(this, "OwnerClient");

			_tickets = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_tickets.SetContainingEntityInfo(this, "Client");

			_ticketCategories = new VarioSL.Entities.CollectionClasses.TicketCategoryCollection();
			_ticketCategories.SetContainingEntityInfo(this, "Client");

			_ticketVendingClient = new VarioSL.Entities.CollectionClasses.TicketVendingClientCollection();
			_ticketVendingClient.SetContainingEntityInfo(this, "Client");

			_cardIssuerCardTransactions = new VarioSL.Entities.CollectionClasses.TransactionCollection();
			_cardIssuerCardTransactions.SetContainingEntityInfo(this, "CardIssuer");

			_umUnits = new VarioSL.Entities.CollectionClasses.UnitCollection();
			_umUnits.SetContainingEntityInfo(this, "Client");

			_users = new VarioSL.Entities.CollectionClasses.UserListCollection();
			_users.SetContainingEntityInfo(this, "Client");

			_settlements = new VarioSL.Entities.CollectionClasses.VarioSettlementCollection();
			_settlements.SetContainingEntityInfo(this, "Client");

			_vdvKeySet = new VarioSL.Entities.CollectionClasses.VdvKeySetCollection();
			_vdvKeySet.SetContainingEntityInfo(this, "Client");

			_workstations = new VarioSL.Entities.CollectionClasses.WorkstationCollection();
			_workstations.SetContainingEntityInfo(this, "Client");

			_addressBookEntries = new VarioSL.Entities.CollectionClasses.AddressBookEntryCollection();
			_addressBookEntries.SetContainingEntityInfo(this, "Client");

			_cards = new VarioSL.Entities.CollectionClasses.CardCollection();
			_cards.SetContainingEntityInfo(this, "Client");

			_configurationDefinitions = new VarioSL.Entities.CollectionClasses.ConfigurationDefinitionCollection();
			_configurationDefinitions.SetContainingEntityInfo(this, "Client");

			_configurationOverwrites = new VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection();
			_configurationOverwrites.SetContainingEntityInfo(this, "Client");

			_contracts = new VarioSL.Entities.CollectionClasses.ContractCollection();
			_contracts.SetContainingEntityInfo(this, "Client");

			_vanpoolContracts = new VarioSL.Entities.CollectionClasses.ContractCollection();
			_vanpoolContracts.SetContainingEntityInfo(this, "VanpoolClient");

			_cryptoCryptogramArchives = new VarioSL.Entities.CollectionClasses.CryptoCryptogramArchiveCollection();
			_cryptoCryptogramArchives.SetContainingEntityInfo(this, "Client");

			_cryptoQArchives = new VarioSL.Entities.CollectionClasses.CryptoQArchiveCollection();
			_cryptoQArchives.SetContainingEntityInfo(this, "Client");

			_fareChangeCauses = new VarioSL.Entities.CollectionClasses.FareChangeCauseCollection();
			_fareChangeCauses.SetContainingEntityInfo(this, "Client");

			_fareEvasionBehaviours = new VarioSL.Entities.CollectionClasses.FareEvasionBehaviourCollection();
			_fareEvasionBehaviours.SetContainingEntityInfo(this, "Client");

			_fareEvasionIncidents = new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection();
			_fareEvasionIncidents.SetContainingEntityInfo(this, "Client");

			_fareEvasionInspections = new VarioSL.Entities.CollectionClasses.FareEvasionInspectionCollection();
			_fareEvasionInspections.SetContainingEntityInfo(this, "Client");

			_filterValueSets = new VarioSL.Entities.CollectionClasses.FilterValueSetCollection();
			_filterValueSets.SetContainingEntityInfo(this, "Client");

			_forms = new VarioSL.Entities.CollectionClasses.FormCollection();
			_forms.SetContainingEntityInfo(this, "Client");

			_identificationTypes = new VarioSL.Entities.CollectionClasses.IdentificationTypeCollection();
			_identificationTypes.SetContainingEntityInfo(this, "Client");

			_invoices = new VarioSL.Entities.CollectionClasses.InvoiceCollection();
			_invoices.SetContainingEntityInfo(this, "Client");

			_invoicings = new VarioSL.Entities.CollectionClasses.InvoicingCollection();
			_invoicings.SetContainingEntityInfo(this, "Client");

			_jobs = new VarioSL.Entities.CollectionClasses.JobCollection();
			_jobs.SetContainingEntityInfo(this, "Client");

			_parameterArchives = new VarioSL.Entities.CollectionClasses.ParameterArchiveCollection();
			_parameterArchives.SetContainingEntityInfo(this, "Client");

			_postingKeys = new VarioSL.Entities.CollectionClasses.PostingKeyCollection();
			_postingKeys.SetContainingEntityInfo(this, "Client");

			_products = new VarioSL.Entities.CollectionClasses.ProductCollection();
			_products.SetContainingEntityInfo(this, "Client");

			_reportJobs = new VarioSL.Entities.CollectionClasses.ReportJobCollection();
			_reportJobs.SetContainingEntityInfo(this, "Client");

			_reportJobResults = new VarioSL.Entities.CollectionClasses.ReportJobResultCollection();
			_reportJobResults.SetContainingEntityInfo(this, "Client");

			_reportToClients = new VarioSL.Entities.CollectionClasses.ReportToClientCollection();
			_reportToClients.SetContainingEntityInfo(this, "Client");

			_revenueSettlements = new VarioSL.Entities.CollectionClasses.RevenueSettlementCollection();
			_revenueSettlements.SetContainingEntityInfo(this, "Client");

			_sales = new VarioSL.Entities.CollectionClasses.SaleCollection();
			_sales.SetContainingEntityInfo(this, "Client");

			_schoolYears = new VarioSL.Entities.CollectionClasses.SchoolYearCollection();
			_schoolYears.SetContainingEntityInfo(this, "Client");

			_storageLocations = new VarioSL.Entities.CollectionClasses.StorageLocationCollection();
			_storageLocations.SetContainingEntityInfo(this, "Client");

			_transactionJournals = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_transactionJournals.SetContainingEntityInfo(this, "Client");

			_transportCompanies = new VarioSL.Entities.CollectionClasses.TransportCompanyCollection();
			_transportCompanies.SetContainingEntityInfo(this, "Client");
			_ticketCollectionViaTicketVendingClient = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_reportCollectionViaReportToClient = new VarioSL.Entities.CollectionClasses.ReportCollection();
			_accountEntryNumberReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addition", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankAccountNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankLocation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BIC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("City", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultLineNoBundle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalCompanyNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Fax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Fon", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HandlingChargedDebitEntry", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IBAN", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LongName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MasterClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PoBox", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostalCodePoBox", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostalCodeStreet", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Sign", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Street", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StreetNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TermOfPayment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VolNo", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _accountEntryNumber</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAccountEntryNumber(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _accountEntryNumber, new PropertyChangedEventHandler( OnAccountEntryNumberPropertyChanged ), "AccountEntryNumber", VarioSL.Entities.RelationClasses.StaticClientRelations.AccountEntryNumberEntityUsingClientIDStatic, true, signalRelatedEntity, "Client", false, new int[] { (int)ClientFieldIndex.ClientID } );
			_accountEntryNumber = null;
		}
	
		/// <summary> setups the sync logic for member _accountEntryNumber</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAccountEntryNumber(IEntityCore relatedEntity)
		{
			if(_accountEntryNumber!=relatedEntity)
			{
				DesetupSyncAccountEntryNumber(true, true);
				_accountEntryNumber = (AccountEntryNumberEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _accountEntryNumber, new PropertyChangedEventHandler( OnAccountEntryNumberPropertyChanged ), "AccountEntryNumber", VarioSL.Entities.RelationClasses.StaticClientRelations.AccountEntryNumberEntityUsingClientIDStatic, true, ref _alreadyFetchedAccountEntryNumber, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAccountEntryNumberPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="clientID">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 clientID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ClientFieldIndex.ClientID].ForcedCurrentValueWrite(clientID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateClientDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ClientEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ClientRelations Relations
		{
			get	{ return new ClientRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AccountEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccountEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AccountEntryCollection(), (IEntityRelation)GetRelationsForField("AccountEntries")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.AccountEntryEntity, 0, null, null, null, "AccountEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ApportionmentResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApportionmentResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ApportionmentResultCollection(), (IEntityRelation)GetRelationsForField("ApportionmentResults")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ApportionmentResultEntity, 0, null, null, null, "ApportionmentResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ApportionmentResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApportionmentResults1
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ApportionmentResultCollection(), (IEntityRelation)GetRelationsForField("ApportionmentResults1")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ApportionmentResultEntity, 0, null, null, null, "ApportionmentResults1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ApportionmentResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApportionmentResults2
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ApportionmentResultCollection(), (IEntityRelation)GetRelationsForField("ApportionmentResults2")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ApportionmentResultEntity, 0, null, null, null, "ApportionmentResults2", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Calendar' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCalendar
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CalendarCollection(), (IEntityRelation)GetRelationsForField("Calendar")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.CalendarEntity, 0, null, null, null, "Calendar", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClearingResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClearingResultsByCardIssuer
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClearingResultCollection(), (IEntityRelation)GetRelationsForField("ClearingResultsByCardIssuer")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ClearingResultEntity, 0, null, null, null, "ClearingResultsByCardIssuer", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClearingResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClearingResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClearingResultCollection(), (IEntityRelation)GetRelationsForField("ClearingResults")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ClearingResultEntity, 0, null, null, null, "ClearingResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClearingResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClearingResultsByClientFrom
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClearingResultCollection(), (IEntityRelation)GetRelationsForField("ClearingResultsByClientFrom")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ClearingResultEntity, 0, null, null, null, "ClearingResultsByClientFrom", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClearingResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClearingResultsByClientTo
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClearingResultCollection(), (IEntityRelation)GetRelationsForField("ClearingResultsByClientTo")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ClearingResultEntity, 0, null, null, null, "ClearingResultsByClientTo", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientAdaptedLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientAdaptedLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("ClientAdaptedLayoutObjects")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ClientAdaptedLayoutObjectEntity, 0, null, null, null, "ClientAdaptedLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Debtor' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDebtors
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCollection(), (IEntityRelation)GetRelationsForField("Debtors")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.DebtorEntity, 0, null, null, null, "Debtors", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Depot' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDepots
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DepotCollection(), (IEntityRelation)GetRelationsForField("Depots")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.DepotEntity, 0, null, null, null, "Depots", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrix' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareMatrixes
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixCollection(), (IEntityRelation)GetRelationsForField("FareMatrixes")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntity, 0, null, null, null, "FareMatrixes", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareTable' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareTables
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareTableCollection(), (IEntityRelation)GetRelationsForField("FareTables")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.FareTableEntity, 0, null, null, null, "FareTables", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLayouts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("Layouts")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "Layouts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Responsibility' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWhoIsResponsible
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ResponsibilityCollection(), (IEntityRelation)GetRelationsForField("WhoIsResponsible")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ResponsibilityEntity, 0, null, null, null, "WhoIsResponsible", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Responsibility' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathResponsibleFor
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ResponsibilityCollection(), (IEntityRelation)GetRelationsForField("ResponsibleFor")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ResponsibilityEntity, 0, null, null, null, "ResponsibleFor", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientTariffs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("ClientTariffs")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "ClientTariffs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOwnerClientTariffs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("OwnerClientTariffs")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "OwnerClientTariffs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Tickets")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Tickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketCategories
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCategoryCollection(), (IEntityRelation)GetRelationsForField("TicketCategories")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.TicketCategoryEntity, 0, null, null, null, "TicketCategories", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketVendingClient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketVendingClient
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketVendingClientCollection(), (IEntityRelation)GetRelationsForField("TicketVendingClient")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.TicketVendingClientEntity, 0, null, null, null, "TicketVendingClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Transaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardIssuerCardTransactions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionCollection(), (IEntityRelation)GetRelationsForField("CardIssuerCardTransactions")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.TransactionEntity, 0, null, null, null, "CardIssuerCardTransactions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Unit' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUmUnits
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollection(), (IEntityRelation)GetRelationsForField("UmUnits")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.UnitEntity, 0, null, null, null, "UmUnits", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUsers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("Users")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "Users", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VarioSettlement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VarioSettlementCollection(), (IEntityRelation)GetRelationsForField("Settlements")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.VarioSettlementEntity, 0, null, null, null, "Settlements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvKeySet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvKeySet
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvKeySetCollection(), (IEntityRelation)GetRelationsForField("VdvKeySet")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.VdvKeySetEntity, 0, null, null, null, "VdvKeySet", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Workstation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWorkstations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.WorkstationCollection(), (IEntityRelation)GetRelationsForField("Workstations")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.WorkstationEntity, 0, null, null, null, "Workstations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AddressBookEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAddressBookEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressBookEntryCollection(), (IEntityRelation)GetRelationsForField("AddressBookEntries")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.AddressBookEntryEntity, 0, null, null, null, "AddressBookEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCards
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Cards")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Cards", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ConfigurationDefinition' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathConfigurationDefinitions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ConfigurationDefinitionCollection(), (IEntityRelation)GetRelationsForField("ConfigurationDefinitions")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ConfigurationDefinitionEntity, 0, null, null, null, "ConfigurationDefinitions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ConfigurationOverwrite' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathConfigurationOverwrites
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection(), (IEntityRelation)GetRelationsForField("ConfigurationOverwrites")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ConfigurationOverwriteEntity, 0, null, null, null, "ConfigurationOverwrites", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContracts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contracts")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contracts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVanpoolContracts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("VanpoolContracts")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "VanpoolContracts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoCryptogramArchive' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoCryptogramArchives
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoCryptogramArchiveCollection(), (IEntityRelation)GetRelationsForField("CryptoCryptogramArchives")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.CryptoCryptogramArchiveEntity, 0, null, null, null, "CryptoCryptogramArchives", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoQArchive' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoQArchives
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoQArchiveCollection(), (IEntityRelation)GetRelationsForField("CryptoQArchives")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.CryptoQArchiveEntity, 0, null, null, null, "CryptoQArchives", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareChangeCause' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareChangeCauses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareChangeCauseCollection(), (IEntityRelation)GetRelationsForField("FareChangeCauses")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.FareChangeCauseEntity, 0, null, null, null, "FareChangeCauses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionBehaviour' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionBehaviours
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionBehaviourCollection(), (IEntityRelation)GetRelationsForField("FareEvasionBehaviours")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.FareEvasionBehaviourEntity, 0, null, null, null, "FareEvasionBehaviours", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionIncident' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionIncidents
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection(), (IEntityRelation)GetRelationsForField("FareEvasionIncidents")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, 0, null, null, null, "FareEvasionIncidents", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionInspection' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionInspections
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionInspectionCollection(), (IEntityRelation)GetRelationsForField("FareEvasionInspections")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.FareEvasionInspectionEntity, 0, null, null, null, "FareEvasionInspections", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterValueSet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterValueSets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterValueSetCollection(), (IEntityRelation)GetRelationsForField("FilterValueSets")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.FilterValueSetEntity, 0, null, null, null, "FilterValueSets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Form' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathForms
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FormCollection(), (IEntityRelation)GetRelationsForField("Forms")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.FormEntity, 0, null, null, null, "Forms", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'IdentificationType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIdentificationTypes
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.IdentificationTypeCollection(), (IEntityRelation)GetRelationsForField("IdentificationTypes")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.IdentificationTypeEntity, 0, null, null, null, "IdentificationTypes", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoiceCollection(), (IEntityRelation)GetRelationsForField("Invoices")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.InvoiceEntity, 0, null, null, null, "Invoices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoicing' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoicings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoicingCollection(), (IEntityRelation)GetRelationsForField("Invoicings")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.InvoicingEntity, 0, null, null, null, "Invoicings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Job' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathJobs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.JobCollection(), (IEntityRelation)GetRelationsForField("Jobs")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.JobEntity, 0, null, null, null, "Jobs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterArchive' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterArchives
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterArchiveCollection(), (IEntityRelation)GetRelationsForField("ParameterArchives")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ParameterArchiveEntity, 0, null, null, null, "ParameterArchives", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PostingKey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPostingKeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PostingKeyCollection(), (IEntityRelation)GetRelationsForField("PostingKeys")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.PostingKeyEntity, 0, null, null, null, "PostingKeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Products")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Products", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportJob' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportJobs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportJobCollection(), (IEntityRelation)GetRelationsForField("ReportJobs")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ReportJobEntity, 0, null, null, null, "ReportJobs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportJobResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportJobResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportJobResultCollection(), (IEntityRelation)GetRelationsForField("ReportJobResults")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ReportJobResultEntity, 0, null, null, null, "ReportJobResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportToClient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportToClients
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportToClientCollection(), (IEntityRelation)GetRelationsForField("ReportToClients")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ReportToClientEntity, 0, null, null, null, "ReportToClients", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RevenueSettlement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRevenueSettlements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RevenueSettlementCollection(), (IEntityRelation)GetRelationsForField("RevenueSettlements")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.RevenueSettlementEntity, 0, null, null, null, "RevenueSettlements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSales
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("Sales")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "Sales", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SchoolYear' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSchoolYears
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SchoolYearCollection(), (IEntityRelation)GetRelationsForField("SchoolYears")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.SchoolYearEntity, 0, null, null, null, "SchoolYears", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StorageLocation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathStorageLocations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StorageLocationCollection(), (IEntityRelation)GetRelationsForField("StorageLocations")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.StorageLocationEntity, 0, null, null, null, "StorageLocations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournals")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransportCompany' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransportCompanies
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransportCompanyCollection(), (IEntityRelation)GetRelationsForField("TransportCompanies")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.TransportCompanyEntity, 0, null, null, null, "TransportCompanies", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketCollectionViaTicketVendingClient
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketVendingClientEntityUsingClientid;
				intermediateRelation.SetAliases(string.Empty, "TicketVendingClient_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, GetRelationsForField("TicketCollectionViaTicketVendingClient"), "TicketCollectionViaTicketVendingClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Report'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportCollectionViaReportToClient
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ReportToClientEntityUsingClientID;
				intermediateRelation.SetAliases(string.Empty, "ReportToClient_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.ReportEntity, 0, null, null, GetRelationsForField("ReportCollectionViaReportToClient"), "ReportCollectionViaReportToClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AccountEntryNumber'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccountEntryNumber
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AccountEntryNumberCollection(), (IEntityRelation)GetRelationsForField("AccountEntryNumber")[0], (int)VarioSL.Entities.EntityType.ClientEntity, (int)VarioSL.Entities.EntityType.AccountEntryNumberEntity, 0, null, null, null, "AccountEntryNumber", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Addition property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."ADDITION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addition
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.Addition, true); }
			set	{ SetValue((int)ClientFieldIndex.Addition, value, true); }
		}

		/// <summary> The BankAccountNo property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."BANKACCOUNTNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BankAccountNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClientFieldIndex.BankAccountNo, false); }
			set	{ SetValue((int)ClientFieldIndex.BankAccountNo, value, true); }
		}

		/// <summary> The BankLocation property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."BANKLOCATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BankLocation
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.BankLocation, true); }
			set	{ SetValue((int)ClientFieldIndex.BankLocation, value, true); }
		}

		/// <summary> The BankName property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."BANKNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BankName
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.BankName, true); }
			set	{ SetValue((int)ClientFieldIndex.BankName, value, true); }
		}

		/// <summary> The BankNo property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."BANKNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BankNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClientFieldIndex.BankNo, false); }
			set	{ SetValue((int)ClientFieldIndex.BankNo, value, true); }
		}

		/// <summary> The BIC property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."BIC"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BIC
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.BIC, true); }
			set	{ SetValue((int)ClientFieldIndex.BIC, value, true); }
		}

		/// <summary> The City property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."CITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String City
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.City, true); }
			set	{ SetValue((int)ClientFieldIndex.City, value, true); }
		}

		/// <summary> The ClientID property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)ClientFieldIndex.ClientID, true); }
			set	{ SetValue((int)ClientFieldIndex.ClientID, value, true); }
		}

		/// <summary> The ClientKey property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."CLIENTKEY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ClientKey
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.ClientKey, true); }
			set	{ SetValue((int)ClientFieldIndex.ClientKey, value, true); }
		}

		/// <summary> The CompanyID property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."COMPANYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> CompanyID
		{
			get { return (Nullable<System.Int16>)GetValue((int)ClientFieldIndex.CompanyID, false); }
			set	{ SetValue((int)ClientFieldIndex.CompanyID, value, true); }
		}

		/// <summary> The CompanyName property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."COMPANYNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CompanyName
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.CompanyName, true); }
			set	{ SetValue((int)ClientFieldIndex.CompanyName, value, true); }
		}

		/// <summary> The CreditorID property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."CREDITORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CreditorID
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.CreditorID, true); }
			set	{ SetValue((int)ClientFieldIndex.CreditorID, value, true); }
		}

		/// <summary> The DefaultLineNoBundle property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."DEFAULTLINENOBUNDLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DefaultLineNoBundle
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.DefaultLineNoBundle, true); }
			set	{ SetValue((int)ClientFieldIndex.DefaultLineNoBundle, value, true); }
		}

		/// <summary> The Description property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.Description, true); }
			set	{ SetValue((int)ClientFieldIndex.Description, value, true); }
		}

		/// <summary> The Email property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."EMAIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.Email, true); }
			set	{ SetValue((int)ClientFieldIndex.Email, value, true); }
		}

		/// <summary> The ExternalCompanyNo property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."EXTERNALCOMPANYNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalCompanyNo
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.ExternalCompanyNo, false); }
			set	{ SetValue((int)ClientFieldIndex.ExternalCompanyNo, value, true); }
		}

		/// <summary> The Fax property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."FAX"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 16<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Fax
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.Fax, true); }
			set	{ SetValue((int)ClientFieldIndex.Fax, value, true); }
		}

		/// <summary> The Fon property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."FON"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 16<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Fon
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.Fon, true); }
			set	{ SetValue((int)ClientFieldIndex.Fon, value, true); }
		}

		/// <summary> The HandlingChargedDebitEntry property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."HANDLINGCHARGEDEBITENTRY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 8, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> HandlingChargedDebitEntry
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.HandlingChargedDebitEntry, false); }
			set	{ SetValue((int)ClientFieldIndex.HandlingChargedDebitEntry, value, true); }
		}

		/// <summary> The IBAN property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."IBAN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String IBAN
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.IBAN, true); }
			set	{ SetValue((int)ClientFieldIndex.IBAN, value, true); }
		}

		/// <summary> The LongName property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."LONGNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LongName
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.LongName, true); }
			set	{ SetValue((int)ClientFieldIndex.LongName, value, true); }
		}

		/// <summary> The MasterClientID property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."MASTERCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MasterClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClientFieldIndex.MasterClientID, false); }
			set	{ SetValue((int)ClientFieldIndex.MasterClientID, value, true); }
		}

		/// <summary> The PoBox property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."POBOX"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 6, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PoBox
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.PoBox, false); }
			set	{ SetValue((int)ClientFieldIndex.PoBox, value, true); }
		}

		/// <summary> The PostalCodePoBox property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."POSTALCODEPOBOX"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostalCodePoBox
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.PostalCodePoBox, true); }
			set	{ SetValue((int)ClientFieldIndex.PostalCodePoBox, value, true); }
		}

		/// <summary> The PostalCodeStreet property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."POSTALCODESTREET"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostalCodeStreet
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.PostalCodeStreet, true); }
			set	{ SetValue((int)ClientFieldIndex.PostalCodeStreet, value, true); }
		}

		/// <summary> The Sign property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."SIGN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Sign
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.Sign, true); }
			set	{ SetValue((int)ClientFieldIndex.Sign, value, true); }
		}

		/// <summary> The Street property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."STREET"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Street
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.Street, true); }
			set	{ SetValue((int)ClientFieldIndex.Street, value, true); }
		}

		/// <summary> The StreetNo property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."STREETNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StreetNo
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.StreetNo, true); }
			set	{ SetValue((int)ClientFieldIndex.StreetNo, value, true); }
		}

		/// <summary> The TermOfPayment property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."TERMOFPAYMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 8, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TermOfPayment
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.TermOfPayment, false); }
			set	{ SetValue((int)ClientFieldIndex.TermOfPayment, value, true); }
		}

		/// <summary> The VolNo property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_CLIENT"."VOLNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> VolNo
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ClientFieldIndex.VolNo, false); }
			set	{ SetValue((int)ClientFieldIndex.VolNo, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAccountEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AccountEntryCollection AccountEntries
		{
			get	{ return GetMultiAccountEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AccountEntries. When set to true, AccountEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccountEntries is accessed. You can always execute/ a forced fetch by calling GetMultiAccountEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccountEntries
		{
			get	{ return _alwaysFetchAccountEntries; }
			set	{ _alwaysFetchAccountEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AccountEntries already has been fetched. Setting this property to false when AccountEntries has been fetched
		/// will clear the AccountEntries collection well. Setting this property to true while AccountEntries hasn't been fetched disables lazy loading for AccountEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccountEntries
		{
			get { return _alreadyFetchedAccountEntries;}
			set 
			{
				if(_alreadyFetchedAccountEntries && !value && (_accountEntries != null))
				{
					_accountEntries.Clear();
				}
				_alreadyFetchedAccountEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiApportionmentResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ApportionmentResultCollection ApportionmentResults
		{
			get	{ return GetMultiApportionmentResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ApportionmentResults. When set to true, ApportionmentResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApportionmentResults is accessed. You can always execute/ a forced fetch by calling GetMultiApportionmentResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApportionmentResults
		{
			get	{ return _alwaysFetchApportionmentResults; }
			set	{ _alwaysFetchApportionmentResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApportionmentResults already has been fetched. Setting this property to false when ApportionmentResults has been fetched
		/// will clear the ApportionmentResults collection well. Setting this property to true while ApportionmentResults hasn't been fetched disables lazy loading for ApportionmentResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApportionmentResults
		{
			get { return _alreadyFetchedApportionmentResults;}
			set 
			{
				if(_alreadyFetchedApportionmentResults && !value && (_apportionmentResults != null))
				{
					_apportionmentResults.Clear();
				}
				_alreadyFetchedApportionmentResults = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiApportionmentResults1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ApportionmentResultCollection ApportionmentResults1
		{
			get	{ return GetMultiApportionmentResults1(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ApportionmentResults1. When set to true, ApportionmentResults1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApportionmentResults1 is accessed. You can always execute/ a forced fetch by calling GetMultiApportionmentResults1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApportionmentResults1
		{
			get	{ return _alwaysFetchApportionmentResults1; }
			set	{ _alwaysFetchApportionmentResults1 = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApportionmentResults1 already has been fetched. Setting this property to false when ApportionmentResults1 has been fetched
		/// will clear the ApportionmentResults1 collection well. Setting this property to true while ApportionmentResults1 hasn't been fetched disables lazy loading for ApportionmentResults1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApportionmentResults1
		{
			get { return _alreadyFetchedApportionmentResults1;}
			set 
			{
				if(_alreadyFetchedApportionmentResults1 && !value && (_apportionmentResults1 != null))
				{
					_apportionmentResults1.Clear();
				}
				_alreadyFetchedApportionmentResults1 = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiApportionmentResults2()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ApportionmentResultCollection ApportionmentResults2
		{
			get	{ return GetMultiApportionmentResults2(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ApportionmentResults2. When set to true, ApportionmentResults2 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApportionmentResults2 is accessed. You can always execute/ a forced fetch by calling GetMultiApportionmentResults2(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApportionmentResults2
		{
			get	{ return _alwaysFetchApportionmentResults2; }
			set	{ _alwaysFetchApportionmentResults2 = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApportionmentResults2 already has been fetched. Setting this property to false when ApportionmentResults2 has been fetched
		/// will clear the ApportionmentResults2 collection well. Setting this property to true while ApportionmentResults2 hasn't been fetched disables lazy loading for ApportionmentResults2</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApportionmentResults2
		{
			get { return _alreadyFetchedApportionmentResults2;}
			set 
			{
				if(_alreadyFetchedApportionmentResults2 && !value && (_apportionmentResults2 != null))
				{
					_apportionmentResults2.Clear();
				}
				_alreadyFetchedApportionmentResults2 = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CalendarEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCalendar()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CalendarCollection Calendar
		{
			get	{ return GetMultiCalendar(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Calendar. When set to true, Calendar is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Calendar is accessed. You can always execute/ a forced fetch by calling GetMultiCalendar(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCalendar
		{
			get	{ return _alwaysFetchCalendar; }
			set	{ _alwaysFetchCalendar = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Calendar already has been fetched. Setting this property to false when Calendar has been fetched
		/// will clear the Calendar collection well. Setting this property to true while Calendar hasn't been fetched disables lazy loading for Calendar</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCalendar
		{
			get { return _alreadyFetchedCalendar;}
			set 
			{
				if(_alreadyFetchedCalendar && !value && (_calendar != null))
				{
					_calendar.Clear();
				}
				_alreadyFetchedCalendar = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClearingResultsByCardIssuer()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection ClearingResultsByCardIssuer
		{
			get	{ return GetMultiClearingResultsByCardIssuer(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClearingResultsByCardIssuer. When set to true, ClearingResultsByCardIssuer is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClearingResultsByCardIssuer is accessed. You can always execute/ a forced fetch by calling GetMultiClearingResultsByCardIssuer(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClearingResultsByCardIssuer
		{
			get	{ return _alwaysFetchClearingResultsByCardIssuer; }
			set	{ _alwaysFetchClearingResultsByCardIssuer = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClearingResultsByCardIssuer already has been fetched. Setting this property to false when ClearingResultsByCardIssuer has been fetched
		/// will clear the ClearingResultsByCardIssuer collection well. Setting this property to true while ClearingResultsByCardIssuer hasn't been fetched disables lazy loading for ClearingResultsByCardIssuer</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClearingResultsByCardIssuer
		{
			get { return _alreadyFetchedClearingResultsByCardIssuer;}
			set 
			{
				if(_alreadyFetchedClearingResultsByCardIssuer && !value && (_clearingResultsByCardIssuer != null))
				{
					_clearingResultsByCardIssuer.Clear();
				}
				_alreadyFetchedClearingResultsByCardIssuer = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClearingResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection ClearingResults
		{
			get	{ return GetMultiClearingResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClearingResults. When set to true, ClearingResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClearingResults is accessed. You can always execute/ a forced fetch by calling GetMultiClearingResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClearingResults
		{
			get	{ return _alwaysFetchClearingResults; }
			set	{ _alwaysFetchClearingResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClearingResults already has been fetched. Setting this property to false when ClearingResults has been fetched
		/// will clear the ClearingResults collection well. Setting this property to true while ClearingResults hasn't been fetched disables lazy loading for ClearingResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClearingResults
		{
			get { return _alreadyFetchedClearingResults;}
			set 
			{
				if(_alreadyFetchedClearingResults && !value && (_clearingResults != null))
				{
					_clearingResults.Clear();
				}
				_alreadyFetchedClearingResults = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClearingResultsByClientFrom()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection ClearingResultsByClientFrom
		{
			get	{ return GetMultiClearingResultsByClientFrom(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClearingResultsByClientFrom. When set to true, ClearingResultsByClientFrom is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClearingResultsByClientFrom is accessed. You can always execute/ a forced fetch by calling GetMultiClearingResultsByClientFrom(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClearingResultsByClientFrom
		{
			get	{ return _alwaysFetchClearingResultsByClientFrom; }
			set	{ _alwaysFetchClearingResultsByClientFrom = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClearingResultsByClientFrom already has been fetched. Setting this property to false when ClearingResultsByClientFrom has been fetched
		/// will clear the ClearingResultsByClientFrom collection well. Setting this property to true while ClearingResultsByClientFrom hasn't been fetched disables lazy loading for ClearingResultsByClientFrom</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClearingResultsByClientFrom
		{
			get { return _alreadyFetchedClearingResultsByClientFrom;}
			set 
			{
				if(_alreadyFetchedClearingResultsByClientFrom && !value && (_clearingResultsByClientFrom != null))
				{
					_clearingResultsByClientFrom.Clear();
				}
				_alreadyFetchedClearingResultsByClientFrom = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClearingResultsByClientTo()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection ClearingResultsByClientTo
		{
			get	{ return GetMultiClearingResultsByClientTo(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClearingResultsByClientTo. When set to true, ClearingResultsByClientTo is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClearingResultsByClientTo is accessed. You can always execute/ a forced fetch by calling GetMultiClearingResultsByClientTo(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClearingResultsByClientTo
		{
			get	{ return _alwaysFetchClearingResultsByClientTo; }
			set	{ _alwaysFetchClearingResultsByClientTo = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClearingResultsByClientTo already has been fetched. Setting this property to false when ClearingResultsByClientTo has been fetched
		/// will clear the ClearingResultsByClientTo collection well. Setting this property to true while ClearingResultsByClientTo hasn't been fetched disables lazy loading for ClearingResultsByClientTo</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClearingResultsByClientTo
		{
			get { return _alreadyFetchedClearingResultsByClientTo;}
			set 
			{
				if(_alreadyFetchedClearingResultsByClientTo && !value && (_clearingResultsByClientTo != null))
				{
					_clearingResultsByClientTo.Clear();
				}
				_alreadyFetchedClearingResultsByClientTo = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClientAdaptedLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientAdaptedLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection ClientAdaptedLayoutObjects
		{
			get	{ return GetMultiClientAdaptedLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientAdaptedLayoutObjects. When set to true, ClientAdaptedLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientAdaptedLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiClientAdaptedLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientAdaptedLayoutObjects
		{
			get	{ return _alwaysFetchClientAdaptedLayoutObjects; }
			set	{ _alwaysFetchClientAdaptedLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientAdaptedLayoutObjects already has been fetched. Setting this property to false when ClientAdaptedLayoutObjects has been fetched
		/// will clear the ClientAdaptedLayoutObjects collection well. Setting this property to true while ClientAdaptedLayoutObjects hasn't been fetched disables lazy loading for ClientAdaptedLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientAdaptedLayoutObjects
		{
			get { return _alreadyFetchedClientAdaptedLayoutObjects;}
			set 
			{
				if(_alreadyFetchedClientAdaptedLayoutObjects && !value && (_clientAdaptedLayoutObjects != null))
				{
					_clientAdaptedLayoutObjects.Clear();
				}
				_alreadyFetchedClientAdaptedLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DebtorEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDebtors()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DebtorCollection Debtors
		{
			get	{ return GetMultiDebtors(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Debtors. When set to true, Debtors is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Debtors is accessed. You can always execute/ a forced fetch by calling GetMultiDebtors(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDebtors
		{
			get	{ return _alwaysFetchDebtors; }
			set	{ _alwaysFetchDebtors = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Debtors already has been fetched. Setting this property to false when Debtors has been fetched
		/// will clear the Debtors collection well. Setting this property to true while Debtors hasn't been fetched disables lazy loading for Debtors</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDebtors
		{
			get { return _alreadyFetchedDebtors;}
			set 
			{
				if(_alreadyFetchedDebtors && !value && (_debtors != null))
				{
					_debtors.Clear();
				}
				_alreadyFetchedDebtors = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DepotEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDepots()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DepotCollection Depots
		{
			get	{ return GetMultiDepots(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Depots. When set to true, Depots is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Depots is accessed. You can always execute/ a forced fetch by calling GetMultiDepots(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDepots
		{
			get	{ return _alwaysFetchDepots; }
			set	{ _alwaysFetchDepots = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Depots already has been fetched. Setting this property to false when Depots has been fetched
		/// will clear the Depots collection well. Setting this property to true while Depots hasn't been fetched disables lazy loading for Depots</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDepots
		{
			get { return _alreadyFetchedDepots;}
			set 
			{
				if(_alreadyFetchedDepots && !value && (_depots != null))
				{
					_depots.Clear();
				}
				_alreadyFetchedDepots = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareMatrixes()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixCollection FareMatrixes
		{
			get	{ return GetMultiFareMatrixes(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareMatrixes. When set to true, FareMatrixes is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareMatrixes is accessed. You can always execute/ a forced fetch by calling GetMultiFareMatrixes(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareMatrixes
		{
			get	{ return _alwaysFetchFareMatrixes; }
			set	{ _alwaysFetchFareMatrixes = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareMatrixes already has been fetched. Setting this property to false when FareMatrixes has been fetched
		/// will clear the FareMatrixes collection well. Setting this property to true while FareMatrixes hasn't been fetched disables lazy loading for FareMatrixes</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareMatrixes
		{
			get { return _alreadyFetchedFareMatrixes;}
			set 
			{
				if(_alreadyFetchedFareMatrixes && !value && (_fareMatrixes != null))
				{
					_fareMatrixes.Clear();
				}
				_alreadyFetchedFareMatrixes = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareTables()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareTableCollection FareTables
		{
			get	{ return GetMultiFareTables(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareTables. When set to true, FareTables is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareTables is accessed. You can always execute/ a forced fetch by calling GetMultiFareTables(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareTables
		{
			get	{ return _alwaysFetchFareTables; }
			set	{ _alwaysFetchFareTables = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareTables already has been fetched. Setting this property to false when FareTables has been fetched
		/// will clear the FareTables collection well. Setting this property to true while FareTables hasn't been fetched disables lazy loading for FareTables</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareTables
		{
			get { return _alreadyFetchedFareTables;}
			set 
			{
				if(_alreadyFetchedFareTables && !value && (_fareTables != null))
				{
					_fareTables.Clear();
				}
				_alreadyFetchedFareTables = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLayouts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.LayoutCollection Layouts
		{
			get	{ return GetMultiLayouts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Layouts. When set to true, Layouts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Layouts is accessed. You can always execute/ a forced fetch by calling GetMultiLayouts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLayouts
		{
			get	{ return _alwaysFetchLayouts; }
			set	{ _alwaysFetchLayouts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Layouts already has been fetched. Setting this property to false when Layouts has been fetched
		/// will clear the Layouts collection well. Setting this property to true while Layouts hasn't been fetched disables lazy loading for Layouts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLayouts
		{
			get { return _alreadyFetchedLayouts;}
			set 
			{
				if(_alreadyFetchedLayouts && !value && (_layouts != null))
				{
					_layouts.Clear();
				}
				_alreadyFetchedLayouts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ResponsibilityEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWhoIsResponsible()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ResponsibilityCollection WhoIsResponsible
		{
			get	{ return GetMultiWhoIsResponsible(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for WhoIsResponsible. When set to true, WhoIsResponsible is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WhoIsResponsible is accessed. You can always execute/ a forced fetch by calling GetMultiWhoIsResponsible(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWhoIsResponsible
		{
			get	{ return _alwaysFetchWhoIsResponsible; }
			set	{ _alwaysFetchWhoIsResponsible = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property WhoIsResponsible already has been fetched. Setting this property to false when WhoIsResponsible has been fetched
		/// will clear the WhoIsResponsible collection well. Setting this property to true while WhoIsResponsible hasn't been fetched disables lazy loading for WhoIsResponsible</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWhoIsResponsible
		{
			get { return _alreadyFetchedWhoIsResponsible;}
			set 
			{
				if(_alreadyFetchedWhoIsResponsible && !value && (_whoIsResponsible != null))
				{
					_whoIsResponsible.Clear();
				}
				_alreadyFetchedWhoIsResponsible = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ResponsibilityEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiResponsibleFor()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ResponsibilityCollection ResponsibleFor
		{
			get	{ return GetMultiResponsibleFor(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ResponsibleFor. When set to true, ResponsibleFor is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ResponsibleFor is accessed. You can always execute/ a forced fetch by calling GetMultiResponsibleFor(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchResponsibleFor
		{
			get	{ return _alwaysFetchResponsibleFor; }
			set	{ _alwaysFetchResponsibleFor = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ResponsibleFor already has been fetched. Setting this property to false when ResponsibleFor has been fetched
		/// will clear the ResponsibleFor collection well. Setting this property to true while ResponsibleFor hasn't been fetched disables lazy loading for ResponsibleFor</summary>
		[Browsable(false)]
		public bool AlreadyFetchedResponsibleFor
		{
			get { return _alreadyFetchedResponsibleFor;}
			set 
			{
				if(_alreadyFetchedResponsibleFor && !value && (_responsibleFor != null))
				{
					_responsibleFor.Clear();
				}
				_alreadyFetchedResponsibleFor = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientTariffs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TariffCollection ClientTariffs
		{
			get	{ return GetMultiClientTariffs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientTariffs. When set to true, ClientTariffs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientTariffs is accessed. You can always execute/ a forced fetch by calling GetMultiClientTariffs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientTariffs
		{
			get	{ return _alwaysFetchClientTariffs; }
			set	{ _alwaysFetchClientTariffs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientTariffs already has been fetched. Setting this property to false when ClientTariffs has been fetched
		/// will clear the ClientTariffs collection well. Setting this property to true while ClientTariffs hasn't been fetched disables lazy loading for ClientTariffs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientTariffs
		{
			get { return _alreadyFetchedClientTariffs;}
			set 
			{
				if(_alreadyFetchedClientTariffs && !value && (_clientTariffs != null))
				{
					_clientTariffs.Clear();
				}
				_alreadyFetchedClientTariffs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOwnerClientTariffs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TariffCollection OwnerClientTariffs
		{
			get	{ return GetMultiOwnerClientTariffs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OwnerClientTariffs. When set to true, OwnerClientTariffs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OwnerClientTariffs is accessed. You can always execute/ a forced fetch by calling GetMultiOwnerClientTariffs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOwnerClientTariffs
		{
			get	{ return _alwaysFetchOwnerClientTariffs; }
			set	{ _alwaysFetchOwnerClientTariffs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OwnerClientTariffs already has been fetched. Setting this property to false when OwnerClientTariffs has been fetched
		/// will clear the OwnerClientTariffs collection well. Setting this property to true while OwnerClientTariffs hasn't been fetched disables lazy loading for OwnerClientTariffs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOwnerClientTariffs
		{
			get { return _alreadyFetchedOwnerClientTariffs;}
			set 
			{
				if(_alreadyFetchedOwnerClientTariffs && !value && (_ownerClientTariffs != null))
				{
					_ownerClientTariffs.Clear();
				}
				_alreadyFetchedOwnerClientTariffs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection Tickets
		{
			get	{ return GetMultiTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Tickets. When set to true, Tickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tickets is accessed. You can always execute/ a forced fetch by calling GetMultiTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTickets
		{
			get	{ return _alwaysFetchTickets; }
			set	{ _alwaysFetchTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tickets already has been fetched. Setting this property to false when Tickets has been fetched
		/// will clear the Tickets collection well. Setting this property to true while Tickets hasn't been fetched disables lazy loading for Tickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTickets
		{
			get { return _alreadyFetchedTickets;}
			set 
			{
				if(_alreadyFetchedTickets && !value && (_tickets != null))
				{
					_tickets.Clear();
				}
				_alreadyFetchedTickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketCategories()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCategoryCollection TicketCategories
		{
			get	{ return GetMultiTicketCategories(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketCategories. When set to true, TicketCategories is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketCategories is accessed. You can always execute/ a forced fetch by calling GetMultiTicketCategories(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketCategories
		{
			get	{ return _alwaysFetchTicketCategories; }
			set	{ _alwaysFetchTicketCategories = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketCategories already has been fetched. Setting this property to false when TicketCategories has been fetched
		/// will clear the TicketCategories collection well. Setting this property to true while TicketCategories hasn't been fetched disables lazy loading for TicketCategories</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketCategories
		{
			get { return _alreadyFetchedTicketCategories;}
			set 
			{
				if(_alreadyFetchedTicketCategories && !value && (_ticketCategories != null))
				{
					_ticketCategories.Clear();
				}
				_alreadyFetchedTicketCategories = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketVendingClientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketVendingClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketVendingClientCollection TicketVendingClient
		{
			get	{ return GetMultiTicketVendingClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketVendingClient. When set to true, TicketVendingClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketVendingClient is accessed. You can always execute/ a forced fetch by calling GetMultiTicketVendingClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketVendingClient
		{
			get	{ return _alwaysFetchTicketVendingClient; }
			set	{ _alwaysFetchTicketVendingClient = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketVendingClient already has been fetched. Setting this property to false when TicketVendingClient has been fetched
		/// will clear the TicketVendingClient collection well. Setting this property to true while TicketVendingClient hasn't been fetched disables lazy loading for TicketVendingClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketVendingClient
		{
			get { return _alreadyFetchedTicketVendingClient;}
			set 
			{
				if(_alreadyFetchedTicketVendingClient && !value && (_ticketVendingClient != null))
				{
					_ticketVendingClient.Clear();
				}
				_alreadyFetchedTicketVendingClient = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardIssuerCardTransactions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionCollection CardIssuerCardTransactions
		{
			get	{ return GetMultiCardIssuerCardTransactions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardIssuerCardTransactions. When set to true, CardIssuerCardTransactions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardIssuerCardTransactions is accessed. You can always execute/ a forced fetch by calling GetMultiCardIssuerCardTransactions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardIssuerCardTransactions
		{
			get	{ return _alwaysFetchCardIssuerCardTransactions; }
			set	{ _alwaysFetchCardIssuerCardTransactions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardIssuerCardTransactions already has been fetched. Setting this property to false when CardIssuerCardTransactions has been fetched
		/// will clear the CardIssuerCardTransactions collection well. Setting this property to true while CardIssuerCardTransactions hasn't been fetched disables lazy loading for CardIssuerCardTransactions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardIssuerCardTransactions
		{
			get { return _alreadyFetchedCardIssuerCardTransactions;}
			set 
			{
				if(_alreadyFetchedCardIssuerCardTransactions && !value && (_cardIssuerCardTransactions != null))
				{
					_cardIssuerCardTransactions.Clear();
				}
				_alreadyFetchedCardIssuerCardTransactions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUmUnits()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollection UmUnits
		{
			get	{ return GetMultiUmUnits(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UmUnits. When set to true, UmUnits is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UmUnits is accessed. You can always execute/ a forced fetch by calling GetMultiUmUnits(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUmUnits
		{
			get	{ return _alwaysFetchUmUnits; }
			set	{ _alwaysFetchUmUnits = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UmUnits already has been fetched. Setting this property to false when UmUnits has been fetched
		/// will clear the UmUnits collection well. Setting this property to true while UmUnits hasn't been fetched disables lazy loading for UmUnits</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUmUnits
		{
			get { return _alreadyFetchedUmUnits;}
			set 
			{
				if(_alreadyFetchedUmUnits && !value && (_umUnits != null))
				{
					_umUnits.Clear();
				}
				_alreadyFetchedUmUnits = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUsers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UserListCollection Users
		{
			get	{ return GetMultiUsers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Users. When set to true, Users is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Users is accessed. You can always execute/ a forced fetch by calling GetMultiUsers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUsers
		{
			get	{ return _alwaysFetchUsers; }
			set	{ _alwaysFetchUsers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Users already has been fetched. Setting this property to false when Users has been fetched
		/// will clear the Users collection well. Setting this property to true while Users hasn't been fetched disables lazy loading for Users</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUsers
		{
			get { return _alreadyFetchedUsers;}
			set 
			{
				if(_alreadyFetchedUsers && !value && (_users != null))
				{
					_users.Clear();
				}
				_alreadyFetchedUsers = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VarioSettlementCollection Settlements
		{
			get	{ return GetMultiSettlements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Settlements. When set to true, Settlements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Settlements is accessed. You can always execute/ a forced fetch by calling GetMultiSettlements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlements
		{
			get	{ return _alwaysFetchSettlements; }
			set	{ _alwaysFetchSettlements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Settlements already has been fetched. Setting this property to false when Settlements has been fetched
		/// will clear the Settlements collection well. Setting this property to true while Settlements hasn't been fetched disables lazy loading for Settlements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlements
		{
			get { return _alreadyFetchedSettlements;}
			set 
			{
				if(_alreadyFetchedSettlements && !value && (_settlements != null))
				{
					_settlements.Clear();
				}
				_alreadyFetchedSettlements = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvKeySet()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvKeySetCollection VdvKeySet
		{
			get	{ return GetMultiVdvKeySet(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvKeySet. When set to true, VdvKeySet is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvKeySet is accessed. You can always execute/ a forced fetch by calling GetMultiVdvKeySet(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvKeySet
		{
			get	{ return _alwaysFetchVdvKeySet; }
			set	{ _alwaysFetchVdvKeySet = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvKeySet already has been fetched. Setting this property to false when VdvKeySet has been fetched
		/// will clear the VdvKeySet collection well. Setting this property to true while VdvKeySet hasn't been fetched disables lazy loading for VdvKeySet</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvKeySet
		{
			get { return _alreadyFetchedVdvKeySet;}
			set 
			{
				if(_alreadyFetchedVdvKeySet && !value && (_vdvKeySet != null))
				{
					_vdvKeySet.Clear();
				}
				_alreadyFetchedVdvKeySet = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'WorkstationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWorkstations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.WorkstationCollection Workstations
		{
			get	{ return GetMultiWorkstations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Workstations. When set to true, Workstations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Workstations is accessed. You can always execute/ a forced fetch by calling GetMultiWorkstations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWorkstations
		{
			get	{ return _alwaysFetchWorkstations; }
			set	{ _alwaysFetchWorkstations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Workstations already has been fetched. Setting this property to false when Workstations has been fetched
		/// will clear the Workstations collection well. Setting this property to true while Workstations hasn't been fetched disables lazy loading for Workstations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWorkstations
		{
			get { return _alreadyFetchedWorkstations;}
			set 
			{
				if(_alreadyFetchedWorkstations && !value && (_workstations != null))
				{
					_workstations.Clear();
				}
				_alreadyFetchedWorkstations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAddressBookEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AddressBookEntryCollection AddressBookEntries
		{
			get	{ return GetMultiAddressBookEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AddressBookEntries. When set to true, AddressBookEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AddressBookEntries is accessed. You can always execute/ a forced fetch by calling GetMultiAddressBookEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAddressBookEntries
		{
			get	{ return _alwaysFetchAddressBookEntries; }
			set	{ _alwaysFetchAddressBookEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AddressBookEntries already has been fetched. Setting this property to false when AddressBookEntries has been fetched
		/// will clear the AddressBookEntries collection well. Setting this property to true while AddressBookEntries hasn't been fetched disables lazy loading for AddressBookEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAddressBookEntries
		{
			get { return _alreadyFetchedAddressBookEntries;}
			set 
			{
				if(_alreadyFetchedAddressBookEntries && !value && (_addressBookEntries != null))
				{
					_addressBookEntries.Clear();
				}
				_alreadyFetchedAddressBookEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCards()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection Cards
		{
			get	{ return GetMultiCards(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Cards. When set to true, Cards is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Cards is accessed. You can always execute/ a forced fetch by calling GetMultiCards(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCards
		{
			get	{ return _alwaysFetchCards; }
			set	{ _alwaysFetchCards = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Cards already has been fetched. Setting this property to false when Cards has been fetched
		/// will clear the Cards collection well. Setting this property to true while Cards hasn't been fetched disables lazy loading for Cards</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCards
		{
			get { return _alreadyFetchedCards;}
			set 
			{
				if(_alreadyFetchedCards && !value && (_cards != null))
				{
					_cards.Clear();
				}
				_alreadyFetchedCards = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ConfigurationDefinitionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiConfigurationDefinitions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ConfigurationDefinitionCollection ConfigurationDefinitions
		{
			get	{ return GetMultiConfigurationDefinitions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ConfigurationDefinitions. When set to true, ConfigurationDefinitions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ConfigurationDefinitions is accessed. You can always execute/ a forced fetch by calling GetMultiConfigurationDefinitions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchConfigurationDefinitions
		{
			get	{ return _alwaysFetchConfigurationDefinitions; }
			set	{ _alwaysFetchConfigurationDefinitions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ConfigurationDefinitions already has been fetched. Setting this property to false when ConfigurationDefinitions has been fetched
		/// will clear the ConfigurationDefinitions collection well. Setting this property to true while ConfigurationDefinitions hasn't been fetched disables lazy loading for ConfigurationDefinitions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedConfigurationDefinitions
		{
			get { return _alreadyFetchedConfigurationDefinitions;}
			set 
			{
				if(_alreadyFetchedConfigurationDefinitions && !value && (_configurationDefinitions != null))
				{
					_configurationDefinitions.Clear();
				}
				_alreadyFetchedConfigurationDefinitions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ConfigurationOverwriteEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiConfigurationOverwrites()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection ConfigurationOverwrites
		{
			get	{ return GetMultiConfigurationOverwrites(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ConfigurationOverwrites. When set to true, ConfigurationOverwrites is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ConfigurationOverwrites is accessed. You can always execute/ a forced fetch by calling GetMultiConfigurationOverwrites(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchConfigurationOverwrites
		{
			get	{ return _alwaysFetchConfigurationOverwrites; }
			set	{ _alwaysFetchConfigurationOverwrites = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ConfigurationOverwrites already has been fetched. Setting this property to false when ConfigurationOverwrites has been fetched
		/// will clear the ConfigurationOverwrites collection well. Setting this property to true while ConfigurationOverwrites hasn't been fetched disables lazy loading for ConfigurationOverwrites</summary>
		[Browsable(false)]
		public bool AlreadyFetchedConfigurationOverwrites
		{
			get { return _alreadyFetchedConfigurationOverwrites;}
			set 
			{
				if(_alreadyFetchedConfigurationOverwrites && !value && (_configurationOverwrites != null))
				{
					_configurationOverwrites.Clear();
				}
				_alreadyFetchedConfigurationOverwrites = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContracts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractCollection Contracts
		{
			get	{ return GetMultiContracts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Contracts. When set to true, Contracts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contracts is accessed. You can always execute/ a forced fetch by calling GetMultiContracts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContracts
		{
			get	{ return _alwaysFetchContracts; }
			set	{ _alwaysFetchContracts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contracts already has been fetched. Setting this property to false when Contracts has been fetched
		/// will clear the Contracts collection well. Setting this property to true while Contracts hasn't been fetched disables lazy loading for Contracts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContracts
		{
			get { return _alreadyFetchedContracts;}
			set 
			{
				if(_alreadyFetchedContracts && !value && (_contracts != null))
				{
					_contracts.Clear();
				}
				_alreadyFetchedContracts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVanpoolContracts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractCollection VanpoolContracts
		{
			get	{ return GetMultiVanpoolContracts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VanpoolContracts. When set to true, VanpoolContracts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VanpoolContracts is accessed. You can always execute/ a forced fetch by calling GetMultiVanpoolContracts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVanpoolContracts
		{
			get	{ return _alwaysFetchVanpoolContracts; }
			set	{ _alwaysFetchVanpoolContracts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VanpoolContracts already has been fetched. Setting this property to false when VanpoolContracts has been fetched
		/// will clear the VanpoolContracts collection well. Setting this property to true while VanpoolContracts hasn't been fetched disables lazy loading for VanpoolContracts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVanpoolContracts
		{
			get { return _alreadyFetchedVanpoolContracts;}
			set 
			{
				if(_alreadyFetchedVanpoolContracts && !value && (_vanpoolContracts != null))
				{
					_vanpoolContracts.Clear();
				}
				_alreadyFetchedVanpoolContracts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CryptoCryptogramArchiveEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCryptoCryptogramArchives()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CryptoCryptogramArchiveCollection CryptoCryptogramArchives
		{
			get	{ return GetMultiCryptoCryptogramArchives(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoCryptogramArchives. When set to true, CryptoCryptogramArchives is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoCryptogramArchives is accessed. You can always execute/ a forced fetch by calling GetMultiCryptoCryptogramArchives(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoCryptogramArchives
		{
			get	{ return _alwaysFetchCryptoCryptogramArchives; }
			set	{ _alwaysFetchCryptoCryptogramArchives = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoCryptogramArchives already has been fetched. Setting this property to false when CryptoCryptogramArchives has been fetched
		/// will clear the CryptoCryptogramArchives collection well. Setting this property to true while CryptoCryptogramArchives hasn't been fetched disables lazy loading for CryptoCryptogramArchives</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoCryptogramArchives
		{
			get { return _alreadyFetchedCryptoCryptogramArchives;}
			set 
			{
				if(_alreadyFetchedCryptoCryptogramArchives && !value && (_cryptoCryptogramArchives != null))
				{
					_cryptoCryptogramArchives.Clear();
				}
				_alreadyFetchedCryptoCryptogramArchives = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CryptoQArchiveEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCryptoQArchives()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CryptoQArchiveCollection CryptoQArchives
		{
			get	{ return GetMultiCryptoQArchives(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoQArchives. When set to true, CryptoQArchives is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoQArchives is accessed. You can always execute/ a forced fetch by calling GetMultiCryptoQArchives(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoQArchives
		{
			get	{ return _alwaysFetchCryptoQArchives; }
			set	{ _alwaysFetchCryptoQArchives = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoQArchives already has been fetched. Setting this property to false when CryptoQArchives has been fetched
		/// will clear the CryptoQArchives collection well. Setting this property to true while CryptoQArchives hasn't been fetched disables lazy loading for CryptoQArchives</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoQArchives
		{
			get { return _alreadyFetchedCryptoQArchives;}
			set 
			{
				if(_alreadyFetchedCryptoQArchives && !value && (_cryptoQArchives != null))
				{
					_cryptoQArchives.Clear();
				}
				_alreadyFetchedCryptoQArchives = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareChangeCauseEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareChangeCauses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareChangeCauseCollection FareChangeCauses
		{
			get	{ return GetMultiFareChangeCauses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareChangeCauses. When set to true, FareChangeCauses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareChangeCauses is accessed. You can always execute/ a forced fetch by calling GetMultiFareChangeCauses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareChangeCauses
		{
			get	{ return _alwaysFetchFareChangeCauses; }
			set	{ _alwaysFetchFareChangeCauses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareChangeCauses already has been fetched. Setting this property to false when FareChangeCauses has been fetched
		/// will clear the FareChangeCauses collection well. Setting this property to true while FareChangeCauses hasn't been fetched disables lazy loading for FareChangeCauses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareChangeCauses
		{
			get { return _alreadyFetchedFareChangeCauses;}
			set 
			{
				if(_alreadyFetchedFareChangeCauses && !value && (_fareChangeCauses != null))
				{
					_fareChangeCauses.Clear();
				}
				_alreadyFetchedFareChangeCauses = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareEvasionBehaviourEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareEvasionBehaviours()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionBehaviourCollection FareEvasionBehaviours
		{
			get	{ return GetMultiFareEvasionBehaviours(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionBehaviours. When set to true, FareEvasionBehaviours is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionBehaviours is accessed. You can always execute/ a forced fetch by calling GetMultiFareEvasionBehaviours(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionBehaviours
		{
			get	{ return _alwaysFetchFareEvasionBehaviours; }
			set	{ _alwaysFetchFareEvasionBehaviours = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionBehaviours already has been fetched. Setting this property to false when FareEvasionBehaviours has been fetched
		/// will clear the FareEvasionBehaviours collection well. Setting this property to true while FareEvasionBehaviours hasn't been fetched disables lazy loading for FareEvasionBehaviours</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionBehaviours
		{
			get { return _alreadyFetchedFareEvasionBehaviours;}
			set 
			{
				if(_alreadyFetchedFareEvasionBehaviours && !value && (_fareEvasionBehaviours != null))
				{
					_fareEvasionBehaviours.Clear();
				}
				_alreadyFetchedFareEvasionBehaviours = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareEvasionIncidents()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection FareEvasionIncidents
		{
			get	{ return GetMultiFareEvasionIncidents(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionIncidents. When set to true, FareEvasionIncidents is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionIncidents is accessed. You can always execute/ a forced fetch by calling GetMultiFareEvasionIncidents(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionIncidents
		{
			get	{ return _alwaysFetchFareEvasionIncidents; }
			set	{ _alwaysFetchFareEvasionIncidents = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionIncidents already has been fetched. Setting this property to false when FareEvasionIncidents has been fetched
		/// will clear the FareEvasionIncidents collection well. Setting this property to true while FareEvasionIncidents hasn't been fetched disables lazy loading for FareEvasionIncidents</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionIncidents
		{
			get { return _alreadyFetchedFareEvasionIncidents;}
			set 
			{
				if(_alreadyFetchedFareEvasionIncidents && !value && (_fareEvasionIncidents != null))
				{
					_fareEvasionIncidents.Clear();
				}
				_alreadyFetchedFareEvasionIncidents = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareEvasionInspectionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareEvasionInspections()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionInspectionCollection FareEvasionInspections
		{
			get	{ return GetMultiFareEvasionInspections(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionInspections. When set to true, FareEvasionInspections is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionInspections is accessed. You can always execute/ a forced fetch by calling GetMultiFareEvasionInspections(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionInspections
		{
			get	{ return _alwaysFetchFareEvasionInspections; }
			set	{ _alwaysFetchFareEvasionInspections = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionInspections already has been fetched. Setting this property to false when FareEvasionInspections has been fetched
		/// will clear the FareEvasionInspections collection well. Setting this property to true while FareEvasionInspections hasn't been fetched disables lazy loading for FareEvasionInspections</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionInspections
		{
			get { return _alreadyFetchedFareEvasionInspections;}
			set 
			{
				if(_alreadyFetchedFareEvasionInspections && !value && (_fareEvasionInspections != null))
				{
					_fareEvasionInspections.Clear();
				}
				_alreadyFetchedFareEvasionInspections = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFilterValueSets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FilterValueSetCollection FilterValueSets
		{
			get	{ return GetMultiFilterValueSets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FilterValueSets. When set to true, FilterValueSets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterValueSets is accessed. You can always execute/ a forced fetch by calling GetMultiFilterValueSets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterValueSets
		{
			get	{ return _alwaysFetchFilterValueSets; }
			set	{ _alwaysFetchFilterValueSets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterValueSets already has been fetched. Setting this property to false when FilterValueSets has been fetched
		/// will clear the FilterValueSets collection well. Setting this property to true while FilterValueSets hasn't been fetched disables lazy loading for FilterValueSets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterValueSets
		{
			get { return _alreadyFetchedFilterValueSets;}
			set 
			{
				if(_alreadyFetchedFilterValueSets && !value && (_filterValueSets != null))
				{
					_filterValueSets.Clear();
				}
				_alreadyFetchedFilterValueSets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FormEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiForms()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FormCollection Forms
		{
			get	{ return GetMultiForms(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Forms. When set to true, Forms is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Forms is accessed. You can always execute/ a forced fetch by calling GetMultiForms(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchForms
		{
			get	{ return _alwaysFetchForms; }
			set	{ _alwaysFetchForms = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Forms already has been fetched. Setting this property to false when Forms has been fetched
		/// will clear the Forms collection well. Setting this property to true while Forms hasn't been fetched disables lazy loading for Forms</summary>
		[Browsable(false)]
		public bool AlreadyFetchedForms
		{
			get { return _alreadyFetchedForms;}
			set 
			{
				if(_alreadyFetchedForms && !value && (_forms != null))
				{
					_forms.Clear();
				}
				_alreadyFetchedForms = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'IdentificationTypeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiIdentificationTypes()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.IdentificationTypeCollection IdentificationTypes
		{
			get	{ return GetMultiIdentificationTypes(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for IdentificationTypes. When set to true, IdentificationTypes is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IdentificationTypes is accessed. You can always execute/ a forced fetch by calling GetMultiIdentificationTypes(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIdentificationTypes
		{
			get	{ return _alwaysFetchIdentificationTypes; }
			set	{ _alwaysFetchIdentificationTypes = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property IdentificationTypes already has been fetched. Setting this property to false when IdentificationTypes has been fetched
		/// will clear the IdentificationTypes collection well. Setting this property to true while IdentificationTypes hasn't been fetched disables lazy loading for IdentificationTypes</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIdentificationTypes
		{
			get { return _alreadyFetchedIdentificationTypes;}
			set 
			{
				if(_alreadyFetchedIdentificationTypes && !value && (_identificationTypes != null))
				{
					_identificationTypes.Clear();
				}
				_alreadyFetchedIdentificationTypes = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiInvoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceCollection Invoices
		{
			get	{ return GetMultiInvoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Invoices. When set to true, Invoices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoices is accessed. You can always execute/ a forced fetch by calling GetMultiInvoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoices
		{
			get	{ return _alwaysFetchInvoices; }
			set	{ _alwaysFetchInvoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoices already has been fetched. Setting this property to false when Invoices has been fetched
		/// will clear the Invoices collection well. Setting this property to true while Invoices hasn't been fetched disables lazy loading for Invoices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoices
		{
			get { return _alreadyFetchedInvoices;}
			set 
			{
				if(_alreadyFetchedInvoices && !value && (_invoices != null))
				{
					_invoices.Clear();
				}
				_alreadyFetchedInvoices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'InvoicingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiInvoicings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.InvoicingCollection Invoicings
		{
			get	{ return GetMultiInvoicings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Invoicings. When set to true, Invoicings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoicings is accessed. You can always execute/ a forced fetch by calling GetMultiInvoicings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoicings
		{
			get	{ return _alwaysFetchInvoicings; }
			set	{ _alwaysFetchInvoicings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoicings already has been fetched. Setting this property to false when Invoicings has been fetched
		/// will clear the Invoicings collection well. Setting this property to true while Invoicings hasn't been fetched disables lazy loading for Invoicings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoicings
		{
			get { return _alreadyFetchedInvoicings;}
			set 
			{
				if(_alreadyFetchedInvoicings && !value && (_invoicings != null))
				{
					_invoicings.Clear();
				}
				_alreadyFetchedInvoicings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiJobs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.JobCollection Jobs
		{
			get	{ return GetMultiJobs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Jobs. When set to true, Jobs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Jobs is accessed. You can always execute/ a forced fetch by calling GetMultiJobs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchJobs
		{
			get	{ return _alwaysFetchJobs; }
			set	{ _alwaysFetchJobs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Jobs already has been fetched. Setting this property to false when Jobs has been fetched
		/// will clear the Jobs collection well. Setting this property to true while Jobs hasn't been fetched disables lazy loading for Jobs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedJobs
		{
			get { return _alreadyFetchedJobs;}
			set 
			{
				if(_alreadyFetchedJobs && !value && (_jobs != null))
				{
					_jobs.Clear();
				}
				_alreadyFetchedJobs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterArchiveEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterArchives()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterArchiveCollection ParameterArchives
		{
			get	{ return GetMultiParameterArchives(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterArchives. When set to true, ParameterArchives is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterArchives is accessed. You can always execute/ a forced fetch by calling GetMultiParameterArchives(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterArchives
		{
			get	{ return _alwaysFetchParameterArchives; }
			set	{ _alwaysFetchParameterArchives = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterArchives already has been fetched. Setting this property to false when ParameterArchives has been fetched
		/// will clear the ParameterArchives collection well. Setting this property to true while ParameterArchives hasn't been fetched disables lazy loading for ParameterArchives</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterArchives
		{
			get { return _alreadyFetchedParameterArchives;}
			set 
			{
				if(_alreadyFetchedParameterArchives && !value && (_parameterArchives != null))
				{
					_parameterArchives.Clear();
				}
				_alreadyFetchedParameterArchives = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PostingKeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPostingKeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PostingKeyCollection PostingKeys
		{
			get	{ return GetMultiPostingKeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PostingKeys. When set to true, PostingKeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PostingKeys is accessed. You can always execute/ a forced fetch by calling GetMultiPostingKeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPostingKeys
		{
			get	{ return _alwaysFetchPostingKeys; }
			set	{ _alwaysFetchPostingKeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PostingKeys already has been fetched. Setting this property to false when PostingKeys has been fetched
		/// will clear the PostingKeys collection well. Setting this property to true while PostingKeys hasn't been fetched disables lazy loading for PostingKeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPostingKeys
		{
			get { return _alreadyFetchedPostingKeys;}
			set 
			{
				if(_alreadyFetchedPostingKeys && !value && (_postingKeys != null))
				{
					_postingKeys.Clear();
				}
				_alreadyFetchedPostingKeys = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection Products
		{
			get	{ return GetMultiProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Products. When set to true, Products is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Products is accessed. You can always execute/ a forced fetch by calling GetMultiProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProducts
		{
			get	{ return _alwaysFetchProducts; }
			set	{ _alwaysFetchProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Products already has been fetched. Setting this property to false when Products has been fetched
		/// will clear the Products collection well. Setting this property to true while Products hasn't been fetched disables lazy loading for Products</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProducts
		{
			get { return _alreadyFetchedProducts;}
			set 
			{
				if(_alreadyFetchedProducts && !value && (_products != null))
				{
					_products.Clear();
				}
				_alreadyFetchedProducts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportJobs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobCollection ReportJobs
		{
			get	{ return GetMultiReportJobs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportJobs. When set to true, ReportJobs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportJobs is accessed. You can always execute/ a forced fetch by calling GetMultiReportJobs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportJobs
		{
			get	{ return _alwaysFetchReportJobs; }
			set	{ _alwaysFetchReportJobs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportJobs already has been fetched. Setting this property to false when ReportJobs has been fetched
		/// will clear the ReportJobs collection well. Setting this property to true while ReportJobs hasn't been fetched disables lazy loading for ReportJobs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportJobs
		{
			get { return _alreadyFetchedReportJobs;}
			set 
			{
				if(_alreadyFetchedReportJobs && !value && (_reportJobs != null))
				{
					_reportJobs.Clear();
				}
				_alreadyFetchedReportJobs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportJobResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobResultCollection ReportJobResults
		{
			get	{ return GetMultiReportJobResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportJobResults. When set to true, ReportJobResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportJobResults is accessed. You can always execute/ a forced fetch by calling GetMultiReportJobResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportJobResults
		{
			get	{ return _alwaysFetchReportJobResults; }
			set	{ _alwaysFetchReportJobResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportJobResults already has been fetched. Setting this property to false when ReportJobResults has been fetched
		/// will clear the ReportJobResults collection well. Setting this property to true while ReportJobResults hasn't been fetched disables lazy loading for ReportJobResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportJobResults
		{
			get { return _alreadyFetchedReportJobResults;}
			set 
			{
				if(_alreadyFetchedReportJobResults && !value && (_reportJobResults != null))
				{
					_reportJobResults.Clear();
				}
				_alreadyFetchedReportJobResults = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportToClients()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportToClientCollection ReportToClients
		{
			get	{ return GetMultiReportToClients(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportToClients. When set to true, ReportToClients is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportToClients is accessed. You can always execute/ a forced fetch by calling GetMultiReportToClients(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportToClients
		{
			get	{ return _alwaysFetchReportToClients; }
			set	{ _alwaysFetchReportToClients = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportToClients already has been fetched. Setting this property to false when ReportToClients has been fetched
		/// will clear the ReportToClients collection well. Setting this property to true while ReportToClients hasn't been fetched disables lazy loading for ReportToClients</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportToClients
		{
			get { return _alreadyFetchedReportToClients;}
			set 
			{
				if(_alreadyFetchedReportToClients && !value && (_reportToClients != null))
				{
					_reportToClients.Clear();
				}
				_alreadyFetchedReportToClients = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRevenueSettlements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RevenueSettlementCollection RevenueSettlements
		{
			get	{ return GetMultiRevenueSettlements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RevenueSettlements. When set to true, RevenueSettlements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RevenueSettlements is accessed. You can always execute/ a forced fetch by calling GetMultiRevenueSettlements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRevenueSettlements
		{
			get	{ return _alwaysFetchRevenueSettlements; }
			set	{ _alwaysFetchRevenueSettlements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RevenueSettlements already has been fetched. Setting this property to false when RevenueSettlements has been fetched
		/// will clear the RevenueSettlements collection well. Setting this property to true while RevenueSettlements hasn't been fetched disables lazy loading for RevenueSettlements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRevenueSettlements
		{
			get { return _alreadyFetchedRevenueSettlements;}
			set 
			{
				if(_alreadyFetchedRevenueSettlements && !value && (_revenueSettlements != null))
				{
					_revenueSettlements.Clear();
				}
				_alreadyFetchedRevenueSettlements = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSales()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection Sales
		{
			get	{ return GetMultiSales(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Sales. When set to true, Sales is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Sales is accessed. You can always execute/ a forced fetch by calling GetMultiSales(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSales
		{
			get	{ return _alwaysFetchSales; }
			set	{ _alwaysFetchSales = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Sales already has been fetched. Setting this property to false when Sales has been fetched
		/// will clear the Sales collection well. Setting this property to true while Sales hasn't been fetched disables lazy loading for Sales</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSales
		{
			get { return _alreadyFetchedSales;}
			set 
			{
				if(_alreadyFetchedSales && !value && (_sales != null))
				{
					_sales.Clear();
				}
				_alreadyFetchedSales = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SchoolYearEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSchoolYears()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SchoolYearCollection SchoolYears
		{
			get	{ return GetMultiSchoolYears(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SchoolYears. When set to true, SchoolYears is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SchoolYears is accessed. You can always execute/ a forced fetch by calling GetMultiSchoolYears(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSchoolYears
		{
			get	{ return _alwaysFetchSchoolYears; }
			set	{ _alwaysFetchSchoolYears = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SchoolYears already has been fetched. Setting this property to false when SchoolYears has been fetched
		/// will clear the SchoolYears collection well. Setting this property to true while SchoolYears hasn't been fetched disables lazy loading for SchoolYears</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSchoolYears
		{
			get { return _alreadyFetchedSchoolYears;}
			set 
			{
				if(_alreadyFetchedSchoolYears && !value && (_schoolYears != null))
				{
					_schoolYears.Clear();
				}
				_alreadyFetchedSchoolYears = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'StorageLocationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiStorageLocations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.StorageLocationCollection StorageLocations
		{
			get	{ return GetMultiStorageLocations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for StorageLocations. When set to true, StorageLocations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time StorageLocations is accessed. You can always execute/ a forced fetch by calling GetMultiStorageLocations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchStorageLocations
		{
			get	{ return _alwaysFetchStorageLocations; }
			set	{ _alwaysFetchStorageLocations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property StorageLocations already has been fetched. Setting this property to false when StorageLocations has been fetched
		/// will clear the StorageLocations collection well. Setting this property to true while StorageLocations hasn't been fetched disables lazy loading for StorageLocations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedStorageLocations
		{
			get { return _alreadyFetchedStorageLocations;}
			set 
			{
				if(_alreadyFetchedStorageLocations && !value && (_storageLocations != null))
				{
					_storageLocations.Clear();
				}
				_alreadyFetchedStorageLocations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TransactionJournals
		{
			get	{ return GetMultiTransactionJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournals. When set to true, TransactionJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournals is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournals
		{
			get	{ return _alwaysFetchTransactionJournals; }
			set	{ _alwaysFetchTransactionJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournals already has been fetched. Setting this property to false when TransactionJournals has been fetched
		/// will clear the TransactionJournals collection well. Setting this property to true while TransactionJournals hasn't been fetched disables lazy loading for TransactionJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournals
		{
			get { return _alreadyFetchedTransactionJournals;}
			set 
			{
				if(_alreadyFetchedTransactionJournals && !value && (_transactionJournals != null))
				{
					_transactionJournals.Clear();
				}
				_alreadyFetchedTransactionJournals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransportCompanyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransportCompanies()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransportCompanyCollection TransportCompanies
		{
			get	{ return GetMultiTransportCompanies(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransportCompanies. When set to true, TransportCompanies is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransportCompanies is accessed. You can always execute/ a forced fetch by calling GetMultiTransportCompanies(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransportCompanies
		{
			get	{ return _alwaysFetchTransportCompanies; }
			set	{ _alwaysFetchTransportCompanies = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransportCompanies already has been fetched. Setting this property to false when TransportCompanies has been fetched
		/// will clear the TransportCompanies collection well. Setting this property to true while TransportCompanies hasn't been fetched disables lazy loading for TransportCompanies</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransportCompanies
		{
			get { return _alreadyFetchedTransportCompanies;}
			set 
			{
				if(_alreadyFetchedTransportCompanies && !value && (_transportCompanies != null))
				{
					_transportCompanies.Clear();
				}
				_alreadyFetchedTransportCompanies = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketCollectionViaTicketVendingClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketCollectionViaTicketVendingClient
		{
			get { return GetMultiTicketCollectionViaTicketVendingClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketCollectionViaTicketVendingClient. When set to true, TicketCollectionViaTicketVendingClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketCollectionViaTicketVendingClient is accessed. You can always execute a forced fetch by calling GetMultiTicketCollectionViaTicketVendingClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketCollectionViaTicketVendingClient
		{
			get	{ return _alwaysFetchTicketCollectionViaTicketVendingClient; }
			set	{ _alwaysFetchTicketCollectionViaTicketVendingClient = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketCollectionViaTicketVendingClient already has been fetched. Setting this property to false when TicketCollectionViaTicketVendingClient has been fetched
		/// will clear the TicketCollectionViaTicketVendingClient collection well. Setting this property to true while TicketCollectionViaTicketVendingClient hasn't been fetched disables lazy loading for TicketCollectionViaTicketVendingClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketCollectionViaTicketVendingClient
		{
			get { return _alreadyFetchedTicketCollectionViaTicketVendingClient;}
			set 
			{
				if(_alreadyFetchedTicketCollectionViaTicketVendingClient && !value && (_ticketCollectionViaTicketVendingClient != null))
				{
					_ticketCollectionViaTicketVendingClient.Clear();
				}
				_alreadyFetchedTicketCollectionViaTicketVendingClient = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportCollectionViaReportToClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportCollection ReportCollectionViaReportToClient
		{
			get { return GetMultiReportCollectionViaReportToClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportCollectionViaReportToClient. When set to true, ReportCollectionViaReportToClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportCollectionViaReportToClient is accessed. You can always execute a forced fetch by calling GetMultiReportCollectionViaReportToClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportCollectionViaReportToClient
		{
			get	{ return _alwaysFetchReportCollectionViaReportToClient; }
			set	{ _alwaysFetchReportCollectionViaReportToClient = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportCollectionViaReportToClient already has been fetched. Setting this property to false when ReportCollectionViaReportToClient has been fetched
		/// will clear the ReportCollectionViaReportToClient collection well. Setting this property to true while ReportCollectionViaReportToClient hasn't been fetched disables lazy loading for ReportCollectionViaReportToClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportCollectionViaReportToClient
		{
			get { return _alreadyFetchedReportCollectionViaReportToClient;}
			set 
			{
				if(_alreadyFetchedReportCollectionViaReportToClient && !value && (_reportCollectionViaReportToClient != null))
				{
					_reportCollectionViaReportToClient.Clear();
				}
				_alreadyFetchedReportCollectionViaReportToClient = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AccountEntryNumberEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAccountEntryNumber()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AccountEntryNumberEntity AccountEntryNumber
		{
			get	{ return GetSingleAccountEntryNumber(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncAccountEntryNumber(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_accountEntryNumber !=null);
						DesetupSyncAccountEntryNumber(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("AccountEntryNumber");
						}
					}
					else
					{
						if(_accountEntryNumber!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Client");
							SetupSyncAccountEntryNumber(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AccountEntryNumber. When set to true, AccountEntryNumber is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccountEntryNumber is accessed. You can always execute a forced fetch by calling GetSingleAccountEntryNumber(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccountEntryNumber
		{
			get	{ return _alwaysFetchAccountEntryNumber; }
			set	{ _alwaysFetchAccountEntryNumber = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property AccountEntryNumber already has been fetched. Setting this property to false when AccountEntryNumber has been fetched
		/// will set AccountEntryNumber to null as well. Setting this property to true while AccountEntryNumber hasn't been fetched disables lazy loading for AccountEntryNumber</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccountEntryNumber
		{
			get { return _alreadyFetchedAccountEntryNumber;}
			set 
			{
				if(_alreadyFetchedAccountEntryNumber && !value)
				{
					this.AccountEntryNumber = null;
				}
				_alreadyFetchedAccountEntryNumber = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AccountEntryNumber is not found
		/// in the database. When set to true, AccountEntryNumber will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AccountEntryNumberReturnsNewIfNotFound
		{
			get	{ return _accountEntryNumberReturnsNewIfNotFound; }
			set	{ _accountEntryNumberReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ClientEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
