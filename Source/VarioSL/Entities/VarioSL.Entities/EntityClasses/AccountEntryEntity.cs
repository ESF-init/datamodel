﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AccountEntry'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AccountEntryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private DebtorEntity _debtor;
		private bool	_alwaysFetchDebtor, _alreadyFetchedDebtor, _debtorReturnsNewIfNotFound;
		private UserListEntity _user;
		private bool	_alwaysFetchUser, _alreadyFetchedUser, _userReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name Debtor</summary>
			public static readonly string Debtor = "Debtor";
			/// <summary>Member name User</summary>
			public static readonly string User = "User";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AccountEntryEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AccountEntryEntity() :base("AccountEntryEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="entryID">PK value for AccountEntry which data should be fetched into this AccountEntry object</param>
		public AccountEntryEntity(System.Int64 entryID):base("AccountEntryEntity")
		{
			InitClassFetch(entryID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="entryID">PK value for AccountEntry which data should be fetched into this AccountEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AccountEntryEntity(System.Int64 entryID, IPrefetchPath prefetchPathToUse):base("AccountEntryEntity")
		{
			InitClassFetch(entryID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="entryID">PK value for AccountEntry which data should be fetched into this AccountEntry object</param>
		/// <param name="validator">The custom validator object for this AccountEntryEntity</param>
		public AccountEntryEntity(System.Int64 entryID, IValidator validator):base("AccountEntryEntity")
		{
			InitClassFetch(entryID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AccountEntryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_debtor = (DebtorEntity)info.GetValue("_debtor", typeof(DebtorEntity));
			if(_debtor!=null)
			{
				_debtor.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_debtorReturnsNewIfNotFound = info.GetBoolean("_debtorReturnsNewIfNotFound");
			_alwaysFetchDebtor = info.GetBoolean("_alwaysFetchDebtor");
			_alreadyFetchedDebtor = info.GetBoolean("_alreadyFetchedDebtor");

			_user = (UserListEntity)info.GetValue("_user", typeof(UserListEntity));
			if(_user!=null)
			{
				_user.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userReturnsNewIfNotFound = info.GetBoolean("_userReturnsNewIfNotFound");
			_alwaysFetchUser = info.GetBoolean("_alwaysFetchUser");
			_alreadyFetchedUser = info.GetBoolean("_alreadyFetchedUser");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AccountEntryFieldIndex)fieldIndex)
			{
				case AccountEntryFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case AccountEntryFieldIndex.ContractID:
					DesetupSyncDebtor(true, false);
					_alreadyFetchedDebtor = false;
					break;
				case AccountEntryFieldIndex.UserID:
					DesetupSyncUser(true, false);
					_alreadyFetchedUser = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedDebtor = (_debtor != null);
			_alreadyFetchedUser = (_user != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "Debtor":
					toReturn.Add(Relations.DebtorEntityUsingContractID);
					break;
				case "User":
					toReturn.Add(Relations.UserListEntityUsingUserID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_debtor", (!this.MarkedForDeletion?_debtor:null));
			info.AddValue("_debtorReturnsNewIfNotFound", _debtorReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDebtor", _alwaysFetchDebtor);
			info.AddValue("_alreadyFetchedDebtor", _alreadyFetchedDebtor);
			info.AddValue("_user", (!this.MarkedForDeletion?_user:null));
			info.AddValue("_userReturnsNewIfNotFound", _userReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUser", _alwaysFetchUser);
			info.AddValue("_alreadyFetchedUser", _alreadyFetchedUser);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "Debtor":
					_alreadyFetchedDebtor = true;
					this.Debtor = (DebtorEntity)entity;
					break;
				case "User":
					_alreadyFetchedUser = true;
					this.User = (UserListEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "Debtor":
					SetupSyncDebtor(relatedEntity);
					break;
				case "User":
					SetupSyncUser(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "Debtor":
					DesetupSyncDebtor(false, true);
					break;
				case "User":
					DesetupSyncUser(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_debtor!=null)
			{
				toReturn.Add(_debtor);
			}
			if(_user!=null)
			{
				toReturn.Add(_user);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entryID">PK value for AccountEntry which data should be fetched into this AccountEntry object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 entryID)
		{
			return FetchUsingPK(entryID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entryID">PK value for AccountEntry which data should be fetched into this AccountEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 entryID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(entryID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entryID">PK value for AccountEntry which data should be fetched into this AccountEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 entryID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(entryID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entryID">PK value for AccountEntry which data should be fetched into this AccountEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 entryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(entryID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EntryID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AccountEntryRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public DebtorEntity GetSingleDebtor()
		{
			return GetSingleDebtor(false);
		}

		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public virtual DebtorEntity GetSingleDebtor(bool forceFetch)
		{
			if( ( !_alreadyFetchedDebtor || forceFetch || _alwaysFetchDebtor) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DebtorEntityUsingContractID);
				DebtorEntity newEntity = new DebtorEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID);
				}
				if(fetchResult)
				{
					newEntity = (DebtorEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_debtorReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Debtor = newEntity;
				_alreadyFetchedDebtor = fetchResult;
			}
			return _debtor;
		}


		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public UserListEntity GetSingleUser()
		{
			return GetSingleUser(false);
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public virtual UserListEntity GetSingleUser(bool forceFetch)
		{
			if( ( !_alreadyFetchedUser || forceFetch || _alwaysFetchUser) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserListEntityUsingUserID);
				UserListEntity newEntity = new UserListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UserID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.User = newEntity;
				_alreadyFetchedUser = fetchResult;
			}
			return _user;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("Debtor", _debtor);
			toReturn.Add("User", _user);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="entryID">PK value for AccountEntry which data should be fetched into this AccountEntry object</param>
		/// <param name="validator">The validator object for this AccountEntryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 entryID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(entryID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_clientReturnsNewIfNotFound = false;
			_debtorReturnsNewIfNotFound = false;
			_userReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccSettlementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AmountCashDelivered", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AmountCashDeposit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AmountPosted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AmountSubContractor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BillID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingKeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingSummaryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancelationEntryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashCoverage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashDeficit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CostLocationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DutyBalanceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntryNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntryTypeNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExportDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExportState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromStopID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IncomingTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntermediateFareStageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCleared", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineFreeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentMethodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonQuantity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoeBookingKeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoeCostLocationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoeIncomingTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoeQuantityPerson", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoeTicketSpeciesID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesTaxID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SeasonTicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketSpeciesID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToStopID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VoucherAmount", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticAccountEntryRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "AccountEntries", resetFKFields, new int[] { (int)AccountEntryFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticAccountEntryRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _debtor</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDebtor(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _debtor, new PropertyChangedEventHandler( OnDebtorPropertyChanged ), "Debtor", VarioSL.Entities.RelationClasses.StaticAccountEntryRelations.DebtorEntityUsingContractIDStatic, true, signalRelatedEntity, "AccountEntries", resetFKFields, new int[] { (int)AccountEntryFieldIndex.ContractID } );		
			_debtor = null;
		}
		
		/// <summary> setups the sync logic for member _debtor</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDebtor(IEntityCore relatedEntity)
		{
			if(_debtor!=relatedEntity)
			{		
				DesetupSyncDebtor(true, true);
				_debtor = (DebtorEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _debtor, new PropertyChangedEventHandler( OnDebtorPropertyChanged ), "Debtor", VarioSL.Entities.RelationClasses.StaticAccountEntryRelations.DebtorEntityUsingContractIDStatic, true, ref _alreadyFetchedDebtor, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDebtorPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _user</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUser(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _user, new PropertyChangedEventHandler( OnUserPropertyChanged ), "User", VarioSL.Entities.RelationClasses.StaticAccountEntryRelations.UserListEntityUsingUserIDStatic, true, signalRelatedEntity, "AccountEntries", resetFKFields, new int[] { (int)AccountEntryFieldIndex.UserID } );		
			_user = null;
		}
		
		/// <summary> setups the sync logic for member _user</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUser(IEntityCore relatedEntity)
		{
			if(_user!=relatedEntity)
			{		
				DesetupSyncUser(true, true);
				_user = (UserListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _user, new PropertyChangedEventHandler( OnUserPropertyChanged ), "User", VarioSL.Entities.RelationClasses.StaticAccountEntryRelations.UserListEntityUsingUserIDStatic, true, ref _alreadyFetchedUser, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="entryID">PK value for AccountEntry which data should be fetched into this AccountEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 entryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AccountEntryFieldIndex.EntryID].ForcedCurrentValueWrite(entryID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAccountEntryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AccountEntryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AccountEntryRelations Relations
		{
			get	{ return new AccountEntryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.AccountEntryEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Debtor'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDebtor
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCollection(), (IEntityRelation)GetRelationsForField("Debtor")[0], (int)VarioSL.Entities.EntityType.AccountEntryEntity, (int)VarioSL.Entities.EntityType.DebtorEntity, 0, null, null, null, "Debtor", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUser
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("User")[0], (int)VarioSL.Entities.EntityType.AccountEntryEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "User", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccountType property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."ACCOUNTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AccountType
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountEntryFieldIndex.AccountType, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.AccountType, value, true); }
		}

		/// <summary> The AccSettlementID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."ACCSETTLEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AccSettlementID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.AccSettlementID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.AccSettlementID, value, true); }
		}

		/// <summary> The Amount property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Amount
		{
			get { return (System.Int64)GetValue((int)AccountEntryFieldIndex.Amount, true); }
			set	{ SetValue((int)AccountEntryFieldIndex.Amount, value, true); }
		}

		/// <summary> The AmountCashDelivered property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."AMOUNTCASHDELIVERED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AmountCashDelivered
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.AmountCashDelivered, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.AmountCashDelivered, value, true); }
		}

		/// <summary> The AmountCashDeposit property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."AMOUNTCASHDEPOSIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AmountCashDeposit
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.AmountCashDeposit, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.AmountCashDeposit, value, true); }
		}

		/// <summary> The AmountPosted property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."AMOUNTPOSTED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AmountPosted
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.AmountPosted, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.AmountPosted, value, true); }
		}

		/// <summary> The AmountSubContractor property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."AMOUNTSUBCONTRACTOR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AmountSubContractor
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.AmountSubContractor, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.AmountSubContractor, value, true); }
		}

		/// <summary> The BillID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."BILLID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BillID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.BillID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.BillID, value, true); }
		}

		/// <summary> The BookDateTime property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."BOOKDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BookDateTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccountEntryFieldIndex.BookDateTime, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.BookDateTime, value, true); }
		}

		/// <summary> The BookingKeyID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."BOOKINGKEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BookingKeyID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.BookingKeyID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.BookingKeyID, value, true); }
		}

		/// <summary> The BookingSummaryID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."BOOKINGSUMMARYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BookingSummaryID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.BookingSummaryID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.BookingSummaryID, value, true); }
		}

		/// <summary> The CancelationEntryID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."CANCELATIONENTRYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.Int64 CancelationEntryID
		{
			get { return (System.Int64)GetValue((int)AccountEntryFieldIndex.CancelationEntryID, true); }
			set	{ SetValue((int)AccountEntryFieldIndex.CancelationEntryID, value, true); }
		}

		/// <summary> The CashCoverage property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."CASHOVERAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashCoverage
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.CashCoverage, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.CashCoverage, value, true); }
		}

		/// <summary> The CashDeficit property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."CASHDEFICIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashDeficit
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.CashDeficit, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.CashDeficit, value, true); }
		}

		/// <summary> The ClientID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 22, 2, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.ClientID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.ClientID, value, true); }
		}

		/// <summary> The ContractID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ContractID
		{
			get { return (System.Int64)GetValue((int)AccountEntryFieldIndex.ContractID, true); }
			set	{ SetValue((int)AccountEntryFieldIndex.ContractID, value, true); }
		}

		/// <summary> The CostLocationID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."COSTLOCATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CostLocationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.CostLocationID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.CostLocationID, value, true); }
		}

		/// <summary> The DutyBalanceID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."DUTYBALANCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DutyBalanceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.DutyBalanceID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.DutyBalanceID, value, true); }
		}

		/// <summary> The EntryID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."ENTRYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 EntryID
		{
			get { return (System.Int64)GetValue((int)AccountEntryFieldIndex.EntryID, true); }
			set	{ SetValue((int)AccountEntryFieldIndex.EntryID, value, true); }
		}

		/// <summary> The EntryNumber property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."ENTRYNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 EntryNumber
		{
			get { return (System.Int64)GetValue((int)AccountEntryFieldIndex.EntryNumber, true); }
			set	{ SetValue((int)AccountEntryFieldIndex.EntryNumber, value, true); }
		}

		/// <summary> The EntryTypeNumber property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."ENTRYTYPENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 EntryTypeNumber
		{
			get { return (System.Int64)GetValue((int)AccountEntryFieldIndex.EntryTypeNumber, true); }
			set	{ SetValue((int)AccountEntryFieldIndex.EntryTypeNumber, value, true); }
		}

		/// <summary> The ExportDateTime property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."EXPORTDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ExportDateTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccountEntryFieldIndex.ExportDateTime, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.ExportDateTime, value, true); }
		}

		/// <summary> The ExportState property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."EXPORTSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ExportState
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.ExportState, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.ExportState, value, true); }
		}

		/// <summary> The FromStopID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."FROMSTOPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FromStopID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.FromStopID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.FromStopID, value, true); }
		}

		/// <summary> The IncomingTypeID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."INCOMINGTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> IncomingTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.IncomingTypeID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.IncomingTypeID, value, true); }
		}

		/// <summary> The IntermediateFareStageID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."INTERMEDIATEFARESTAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> IntermediateFareStageID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.IntermediateFareStageID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.IntermediateFareStageID, value, true); }
		}

		/// <summary> The InvoiceNumber property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."INVOICENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String InvoiceNumber
		{
			get { return (System.String)GetValue((int)AccountEntryFieldIndex.InvoiceNumber, true); }
			set	{ SetValue((int)AccountEntryFieldIndex.InvoiceNumber, value, true); }
		}

		/// <summary> The IsCleared property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."CLEARED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsCleared
		{
			get { return (Nullable<System.Boolean>)GetValue((int)AccountEntryFieldIndex.IsCleared, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.IsCleared, value, true); }
		}

		/// <summary> The LineFreeText property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."LINEFREETEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LineFreeText
		{
			get { return (System.String)GetValue((int)AccountEntryFieldIndex.LineFreeText, true); }
			set	{ SetValue((int)AccountEntryFieldIndex.LineFreeText, value, true); }
		}

		/// <summary> The LineID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."LINEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.LineID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.LineID, value, true); }
		}

		/// <summary> The PaymentMethodID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."PAYMENTMETHODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentMethodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.PaymentMethodID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.PaymentMethodID, value, true); }
		}

		/// <summary> The PersonQuantity property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."PERSONQUANTITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PersonQuantity
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.PersonQuantity, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.PersonQuantity, value, true); }
		}

		/// <summary> The PostingText property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."POSTINGTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostingText
		{
			get { return (System.String)GetValue((int)AccountEntryFieldIndex.PostingText, true); }
			set	{ SetValue((int)AccountEntryFieldIndex.PostingText, value, true); }
		}

		/// <summary> The PrintCount property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."PRINTCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PrintCount
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.PrintCount, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.PrintCount, value, true); }
		}

		/// <summary> The RoeBookingKeyID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."ROE_BOOKINGKEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RoeBookingKeyID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.RoeBookingKeyID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.RoeBookingKeyID, value, true); }
		}

		/// <summary> The RoeCostLocationID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."ROE_COSTLOCATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RoeCostLocationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.RoeCostLocationID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.RoeCostLocationID, value, true); }
		}

		/// <summary> The RoeIncomingTypeID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."ROE_INCOMINGTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RoeIncomingTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.RoeIncomingTypeID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.RoeIncomingTypeID, value, true); }
		}

		/// <summary> The RoeQuantityPerson property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."ROE_QUANTITYPERSON"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RoeQuantityPerson
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.RoeQuantityPerson, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.RoeQuantityPerson, value, true); }
		}

		/// <summary> The RoeTicketSpeciesID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."ROE_TICKET_SPECIESID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RoeTicketSpeciesID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.RoeTicketSpeciesID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.RoeTicketSpeciesID, value, true); }
		}

		/// <summary> The SalesTaxID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."SALESTAXID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SalesTaxID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.SalesTaxID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.SalesTaxID, value, true); }
		}

		/// <summary> The SeasonTicketID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."SEASONTICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SeasonTicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.SeasonTicketID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.SeasonTicketID, value, true); }
		}

		/// <summary> The TicketSpeciesID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."TICKETSPECIESID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketSpeciesID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.TicketSpeciesID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.TicketSpeciesID, value, true); }
		}

		/// <summary> The ToStopID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."TOSTOPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ToStopID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.ToStopID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.ToStopID, value, true); }
		}

		/// <summary> The UserID property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."USERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UserID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.UserID, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.UserID, value, true); }
		}

		/// <summary> The ValueDateTime property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."VALUEDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValueDateTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccountEntryFieldIndex.ValueDateTime, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.ValueDateTime, value, true); }
		}

		/// <summary> The VoucherAmount property of the Entity AccountEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_ACCOUNTENTRY"."VOUCHERAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> VoucherAmount
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountEntryFieldIndex.VoucherAmount, false); }
			set	{ SetValue((int)AccountEntryFieldIndex.VoucherAmount, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AccountEntries", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DebtorEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDebtor()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DebtorEntity Debtor
		{
			get	{ return GetSingleDebtor(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDebtor(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AccountEntries", "Debtor", _debtor, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Debtor. When set to true, Debtor is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Debtor is accessed. You can always execute a forced fetch by calling GetSingleDebtor(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDebtor
		{
			get	{ return _alwaysFetchDebtor; }
			set	{ _alwaysFetchDebtor = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Debtor already has been fetched. Setting this property to false when Debtor has been fetched
		/// will set Debtor to null as well. Setting this property to true while Debtor hasn't been fetched disables lazy loading for Debtor</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDebtor
		{
			get { return _alreadyFetchedDebtor;}
			set 
			{
				if(_alreadyFetchedDebtor && !value)
				{
					this.Debtor = null;
				}
				_alreadyFetchedDebtor = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Debtor is not found
		/// in the database. When set to true, Debtor will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DebtorReturnsNewIfNotFound
		{
			get	{ return _debtorReturnsNewIfNotFound; }
			set { _debtorReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUser()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserListEntity User
		{
			get	{ return GetSingleUser(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUser(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AccountEntries", "User", _user, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for User. When set to true, User is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time User is accessed. You can always execute a forced fetch by calling GetSingleUser(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUser
		{
			get	{ return _alwaysFetchUser; }
			set	{ _alwaysFetchUser = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property User already has been fetched. Setting this property to false when User has been fetched
		/// will set User to null as well. Setting this property to true while User hasn't been fetched disables lazy loading for User</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUser
		{
			get { return _alreadyFetchedUser;}
			set 
			{
				if(_alreadyFetchedUser && !value)
				{
					this.User = null;
				}
				_alreadyFetchedUser = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property User is not found
		/// in the database. When set to true, User will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserReturnsNewIfNotFound
		{
			get	{ return _userReturnsNewIfNotFound; }
			set { _userReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AccountEntryEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
