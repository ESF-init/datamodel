﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CreditCardTerminal'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CreditCardTerminalEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection	_creditCardAuthorizations;
		private bool	_alwaysFetchCreditCardAuthorizations, _alreadyFetchedCreditCardAuthorizations;
		private CreditCardMerchantEntity _creditCardMerchant;
		private bool	_alwaysFetchCreditCardMerchant, _alreadyFetchedCreditCardMerchant, _creditCardMerchantReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CreditCardMerchant</summary>
			public static readonly string CreditCardMerchant = "CreditCardMerchant";
			/// <summary>Member name CreditCardAuthorizations</summary>
			public static readonly string CreditCardAuthorizations = "CreditCardAuthorizations";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CreditCardTerminalEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CreditCardTerminalEntity() :base("CreditCardTerminalEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="creditCardTerminalID">PK value for CreditCardTerminal which data should be fetched into this CreditCardTerminal object</param>
		public CreditCardTerminalEntity(System.Int64 creditCardTerminalID):base("CreditCardTerminalEntity")
		{
			InitClassFetch(creditCardTerminalID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="creditCardTerminalID">PK value for CreditCardTerminal which data should be fetched into this CreditCardTerminal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CreditCardTerminalEntity(System.Int64 creditCardTerminalID, IPrefetchPath prefetchPathToUse):base("CreditCardTerminalEntity")
		{
			InitClassFetch(creditCardTerminalID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="creditCardTerminalID">PK value for CreditCardTerminal which data should be fetched into this CreditCardTerminal object</param>
		/// <param name="validator">The custom validator object for this CreditCardTerminalEntity</param>
		public CreditCardTerminalEntity(System.Int64 creditCardTerminalID, IValidator validator):base("CreditCardTerminalEntity")
		{
			InitClassFetch(creditCardTerminalID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CreditCardTerminalEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_creditCardAuthorizations = (VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection)info.GetValue("_creditCardAuthorizations", typeof(VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection));
			_alwaysFetchCreditCardAuthorizations = info.GetBoolean("_alwaysFetchCreditCardAuthorizations");
			_alreadyFetchedCreditCardAuthorizations = info.GetBoolean("_alreadyFetchedCreditCardAuthorizations");
			_creditCardMerchant = (CreditCardMerchantEntity)info.GetValue("_creditCardMerchant", typeof(CreditCardMerchantEntity));
			if(_creditCardMerchant!=null)
			{
				_creditCardMerchant.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_creditCardMerchantReturnsNewIfNotFound = info.GetBoolean("_creditCardMerchantReturnsNewIfNotFound");
			_alwaysFetchCreditCardMerchant = info.GetBoolean("_alwaysFetchCreditCardMerchant");
			_alreadyFetchedCreditCardMerchant = info.GetBoolean("_alreadyFetchedCreditCardMerchant");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CreditCardTerminalFieldIndex)fieldIndex)
			{
				case CreditCardTerminalFieldIndex.CreditCardMerchantID:
					DesetupSyncCreditCardMerchant(true, false);
					_alreadyFetchedCreditCardMerchant = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCreditCardAuthorizations = (_creditCardAuthorizations.Count > 0);
			_alreadyFetchedCreditCardMerchant = (_creditCardMerchant != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CreditCardMerchant":
					toReturn.Add(Relations.CreditCardMerchantEntityUsingCreditCardMerchantID);
					break;
				case "CreditCardAuthorizations":
					toReturn.Add(Relations.CreditCardAuthorizationEntityUsingCreditCardTerminalID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_creditCardAuthorizations", (!this.MarkedForDeletion?_creditCardAuthorizations:null));
			info.AddValue("_alwaysFetchCreditCardAuthorizations", _alwaysFetchCreditCardAuthorizations);
			info.AddValue("_alreadyFetchedCreditCardAuthorizations", _alreadyFetchedCreditCardAuthorizations);
			info.AddValue("_creditCardMerchant", (!this.MarkedForDeletion?_creditCardMerchant:null));
			info.AddValue("_creditCardMerchantReturnsNewIfNotFound", _creditCardMerchantReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCreditCardMerchant", _alwaysFetchCreditCardMerchant);
			info.AddValue("_alreadyFetchedCreditCardMerchant", _alreadyFetchedCreditCardMerchant);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CreditCardMerchant":
					_alreadyFetchedCreditCardMerchant = true;
					this.CreditCardMerchant = (CreditCardMerchantEntity)entity;
					break;
				case "CreditCardAuthorizations":
					_alreadyFetchedCreditCardAuthorizations = true;
					if(entity!=null)
					{
						this.CreditCardAuthorizations.Add((CreditCardAuthorizationEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CreditCardMerchant":
					SetupSyncCreditCardMerchant(relatedEntity);
					break;
				case "CreditCardAuthorizations":
					_creditCardAuthorizations.Add((CreditCardAuthorizationEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CreditCardMerchant":
					DesetupSyncCreditCardMerchant(false, true);
					break;
				case "CreditCardAuthorizations":
					this.PerformRelatedEntityRemoval(_creditCardAuthorizations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_creditCardMerchant!=null)
			{
				toReturn.Add(_creditCardMerchant);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_creditCardAuthorizations);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardTerminalID">PK value for CreditCardTerminal which data should be fetched into this CreditCardTerminal object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardTerminalID)
		{
			return FetchUsingPK(creditCardTerminalID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardTerminalID">PK value for CreditCardTerminal which data should be fetched into this CreditCardTerminal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardTerminalID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(creditCardTerminalID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardTerminalID">PK value for CreditCardTerminal which data should be fetched into this CreditCardTerminal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardTerminalID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(creditCardTerminalID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardTerminalID">PK value for CreditCardTerminal which data should be fetched into this CreditCardTerminal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardTerminalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(creditCardTerminalID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CreditCardTerminalID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CreditCardTerminalRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CreditCardAuthorizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection GetMultiCreditCardAuthorizations(bool forceFetch)
		{
			return GetMultiCreditCardAuthorizations(forceFetch, _creditCardAuthorizations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CreditCardAuthorizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection GetMultiCreditCardAuthorizations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCreditCardAuthorizations(forceFetch, _creditCardAuthorizations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection GetMultiCreditCardAuthorizations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCreditCardAuthorizations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection GetMultiCreditCardAuthorizations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCreditCardAuthorizations || forceFetch || _alwaysFetchCreditCardAuthorizations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_creditCardAuthorizations);
				_creditCardAuthorizations.SuppressClearInGetMulti=!forceFetch;
				_creditCardAuthorizations.EntityFactoryToUse = entityFactoryToUse;
				_creditCardAuthorizations.GetMultiManyToOne(null, this, filter);
				_creditCardAuthorizations.SuppressClearInGetMulti=false;
				_alreadyFetchedCreditCardAuthorizations = true;
			}
			return _creditCardAuthorizations;
		}

		/// <summary> Sets the collection parameters for the collection for 'CreditCardAuthorizations'. These settings will be taken into account
		/// when the property CreditCardAuthorizations is requested or GetMultiCreditCardAuthorizations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCreditCardAuthorizations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_creditCardAuthorizations.SortClauses=sortClauses;
			_creditCardAuthorizations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CreditCardMerchantEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CreditCardMerchantEntity' which is related to this entity.</returns>
		public CreditCardMerchantEntity GetSingleCreditCardMerchant()
		{
			return GetSingleCreditCardMerchant(false);
		}

		/// <summary> Retrieves the related entity of type 'CreditCardMerchantEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CreditCardMerchantEntity' which is related to this entity.</returns>
		public virtual CreditCardMerchantEntity GetSingleCreditCardMerchant(bool forceFetch)
		{
			if( ( !_alreadyFetchedCreditCardMerchant || forceFetch || _alwaysFetchCreditCardMerchant) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CreditCardMerchantEntityUsingCreditCardMerchantID);
				CreditCardMerchantEntity newEntity = new CreditCardMerchantEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CreditCardMerchantID);
				}
				if(fetchResult)
				{
					newEntity = (CreditCardMerchantEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_creditCardMerchantReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CreditCardMerchant = newEntity;
				_alreadyFetchedCreditCardMerchant = fetchResult;
			}
			return _creditCardMerchant;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CreditCardMerchant", _creditCardMerchant);
			toReturn.Add("CreditCardAuthorizations", _creditCardAuthorizations);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="creditCardTerminalID">PK value for CreditCardTerminal which data should be fetched into this CreditCardTerminal object</param>
		/// <param name="validator">The validator object for this CreditCardTerminalEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 creditCardTerminalID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(creditCardTerminalID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_creditCardAuthorizations = new VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection();
			_creditCardAuthorizations.SetContainingEntityInfo(this, "CreditCardTerminal");
			_creditCardMerchantReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditCardMerchantID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditCardTerminalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataWireID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalTerminalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _creditCardMerchant</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCreditCardMerchant(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _creditCardMerchant, new PropertyChangedEventHandler( OnCreditCardMerchantPropertyChanged ), "CreditCardMerchant", VarioSL.Entities.RelationClasses.StaticCreditCardTerminalRelations.CreditCardMerchantEntityUsingCreditCardMerchantIDStatic, true, signalRelatedEntity, "CreditCardTerminals", resetFKFields, new int[] { (int)CreditCardTerminalFieldIndex.CreditCardMerchantID } );		
			_creditCardMerchant = null;
		}
		
		/// <summary> setups the sync logic for member _creditCardMerchant</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCreditCardMerchant(IEntityCore relatedEntity)
		{
			if(_creditCardMerchant!=relatedEntity)
			{		
				DesetupSyncCreditCardMerchant(true, true);
				_creditCardMerchant = (CreditCardMerchantEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _creditCardMerchant, new PropertyChangedEventHandler( OnCreditCardMerchantPropertyChanged ), "CreditCardMerchant", VarioSL.Entities.RelationClasses.StaticCreditCardTerminalRelations.CreditCardMerchantEntityUsingCreditCardMerchantIDStatic, true, ref _alreadyFetchedCreditCardMerchant, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCreditCardMerchantPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="creditCardTerminalID">PK value for CreditCardTerminal which data should be fetched into this CreditCardTerminal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 creditCardTerminalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CreditCardTerminalFieldIndex.CreditCardTerminalID].ForcedCurrentValueWrite(creditCardTerminalID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCreditCardTerminalDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CreditCardTerminalEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CreditCardTerminalRelations Relations
		{
			get	{ return new CreditCardTerminalRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CreditCardAuthorization' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCreditCardAuthorizations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection(), (IEntityRelation)GetRelationsForField("CreditCardAuthorizations")[0], (int)VarioSL.Entities.EntityType.CreditCardTerminalEntity, (int)VarioSL.Entities.EntityType.CreditCardAuthorizationEntity, 0, null, null, null, "CreditCardAuthorizations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CreditCardMerchant'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCreditCardMerchant
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CreditCardMerchantCollection(), (IEntityRelation)GetRelationsForField("CreditCardMerchant")[0], (int)VarioSL.Entities.EntityType.CreditCardTerminalEntity, (int)VarioSL.Entities.EntityType.CreditCardMerchantEntity, 0, null, null, null, "CreditCardMerchant", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CreditCardMerchantID property of the Entity CreditCardTerminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDTERMINAL"."CREDITCARDMERCHANTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CreditCardMerchantID
		{
			get { return (System.Int64)GetValue((int)CreditCardTerminalFieldIndex.CreditCardMerchantID, true); }
			set	{ SetValue((int)CreditCardTerminalFieldIndex.CreditCardMerchantID, value, true); }
		}

		/// <summary> The CreditCardTerminalID property of the Entity CreditCardTerminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDTERMINAL"."CREDITCARDTERMINALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CreditCardTerminalID
		{
			get { return (System.Int64)GetValue((int)CreditCardTerminalFieldIndex.CreditCardTerminalID, true); }
			set	{ SetValue((int)CreditCardTerminalFieldIndex.CreditCardTerminalID, value, true); }
		}

		/// <summary> The DataWireID property of the Entity CreditCardTerminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDTERMINAL"."DATAWIREID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 128<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DataWireID
		{
			get { return (System.String)GetValue((int)CreditCardTerminalFieldIndex.DataWireID, true); }
			set	{ SetValue((int)CreditCardTerminalFieldIndex.DataWireID, value, true); }
		}

		/// <summary> The DeviceClassID property of the Entity CreditCardTerminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDTERMINAL"."DEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceClassID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CreditCardTerminalFieldIndex.DeviceClassID, false); }
			set	{ SetValue((int)CreditCardTerminalFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The DeviceNumber property of the Entity CreditCardTerminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDTERMINAL"."DEVICENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)CreditCardTerminalFieldIndex.DeviceNumber, false); }
			set	{ SetValue((int)CreditCardTerminalFieldIndex.DeviceNumber, value, true); }
		}

		/// <summary> The ExternalTerminalID property of the Entity CreditCardTerminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDTERMINAL"."EXTERNALTERMINALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 128<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ExternalTerminalID
		{
			get { return (System.String)GetValue((int)CreditCardTerminalFieldIndex.ExternalTerminalID, true); }
			set	{ SetValue((int)CreditCardTerminalFieldIndex.ExternalTerminalID, value, true); }
		}

		/// <summary> The LastModified property of the Entity CreditCardTerminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDTERMINAL"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CreditCardTerminalFieldIndex.LastModified, true); }
			set	{ SetValue((int)CreditCardTerminalFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CreditCardTerminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDTERMINAL"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CreditCardTerminalFieldIndex.LastUser, true); }
			set	{ SetValue((int)CreditCardTerminalFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CreditCardTerminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDTERMINAL"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CreditCardTerminalFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CreditCardTerminalFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCreditCardAuthorizations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection CreditCardAuthorizations
		{
			get	{ return GetMultiCreditCardAuthorizations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CreditCardAuthorizations. When set to true, CreditCardAuthorizations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CreditCardAuthorizations is accessed. You can always execute/ a forced fetch by calling GetMultiCreditCardAuthorizations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCreditCardAuthorizations
		{
			get	{ return _alwaysFetchCreditCardAuthorizations; }
			set	{ _alwaysFetchCreditCardAuthorizations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CreditCardAuthorizations already has been fetched. Setting this property to false when CreditCardAuthorizations has been fetched
		/// will clear the CreditCardAuthorizations collection well. Setting this property to true while CreditCardAuthorizations hasn't been fetched disables lazy loading for CreditCardAuthorizations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCreditCardAuthorizations
		{
			get { return _alreadyFetchedCreditCardAuthorizations;}
			set 
			{
				if(_alreadyFetchedCreditCardAuthorizations && !value && (_creditCardAuthorizations != null))
				{
					_creditCardAuthorizations.Clear();
				}
				_alreadyFetchedCreditCardAuthorizations = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CreditCardMerchantEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCreditCardMerchant()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CreditCardMerchantEntity CreditCardMerchant
		{
			get	{ return GetSingleCreditCardMerchant(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCreditCardMerchant(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CreditCardTerminals", "CreditCardMerchant", _creditCardMerchant, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CreditCardMerchant. When set to true, CreditCardMerchant is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CreditCardMerchant is accessed. You can always execute a forced fetch by calling GetSingleCreditCardMerchant(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCreditCardMerchant
		{
			get	{ return _alwaysFetchCreditCardMerchant; }
			set	{ _alwaysFetchCreditCardMerchant = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CreditCardMerchant already has been fetched. Setting this property to false when CreditCardMerchant has been fetched
		/// will set CreditCardMerchant to null as well. Setting this property to true while CreditCardMerchant hasn't been fetched disables lazy loading for CreditCardMerchant</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCreditCardMerchant
		{
			get { return _alreadyFetchedCreditCardMerchant;}
			set 
			{
				if(_alreadyFetchedCreditCardMerchant && !value)
				{
					this.CreditCardMerchant = null;
				}
				_alreadyFetchedCreditCardMerchant = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CreditCardMerchant is not found
		/// in the database. When set to true, CreditCardMerchant will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CreditCardMerchantReturnsNewIfNotFound
		{
			get	{ return _creditCardMerchantReturnsNewIfNotFound; }
			set { _creditCardMerchantReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CreditCardTerminalEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
