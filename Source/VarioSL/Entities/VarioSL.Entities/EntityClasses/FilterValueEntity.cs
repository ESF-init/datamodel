﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FilterValue'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FilterValueEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private FilterListElementEntity _filterListElement;
		private bool	_alwaysFetchFilterListElement, _alreadyFetchedFilterListElement, _filterListElementReturnsNewIfNotFound;
		private FilterValueSetEntity _filterValueSet;
		private bool	_alwaysFetchFilterValueSet, _alreadyFetchedFilterValueSet, _filterValueSetReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name FilterListElement</summary>
			public static readonly string FilterListElement = "FilterListElement";
			/// <summary>Member name FilterValueSet</summary>
			public static readonly string FilterValueSet = "FilterValueSet";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FilterValueEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FilterValueEntity() :base("FilterValueEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="filterValueID">PK value for FilterValue which data should be fetched into this FilterValue object</param>
		public FilterValueEntity(System.Int64 filterValueID):base("FilterValueEntity")
		{
			InitClassFetch(filterValueID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="filterValueID">PK value for FilterValue which data should be fetched into this FilterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FilterValueEntity(System.Int64 filterValueID, IPrefetchPath prefetchPathToUse):base("FilterValueEntity")
		{
			InitClassFetch(filterValueID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="filterValueID">PK value for FilterValue which data should be fetched into this FilterValue object</param>
		/// <param name="validator">The custom validator object for this FilterValueEntity</param>
		public FilterValueEntity(System.Int64 filterValueID, IValidator validator):base("FilterValueEntity")
		{
			InitClassFetch(filterValueID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FilterValueEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_filterListElement = (FilterListElementEntity)info.GetValue("_filterListElement", typeof(FilterListElementEntity));
			if(_filterListElement!=null)
			{
				_filterListElement.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_filterListElementReturnsNewIfNotFound = info.GetBoolean("_filterListElementReturnsNewIfNotFound");
			_alwaysFetchFilterListElement = info.GetBoolean("_alwaysFetchFilterListElement");
			_alreadyFetchedFilterListElement = info.GetBoolean("_alreadyFetchedFilterListElement");

			_filterValueSet = (FilterValueSetEntity)info.GetValue("_filterValueSet", typeof(FilterValueSetEntity));
			if(_filterValueSet!=null)
			{
				_filterValueSet.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_filterValueSetReturnsNewIfNotFound = info.GetBoolean("_filterValueSetReturnsNewIfNotFound");
			_alwaysFetchFilterValueSet = info.GetBoolean("_alwaysFetchFilterValueSet");
			_alreadyFetchedFilterValueSet = info.GetBoolean("_alreadyFetchedFilterValueSet");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FilterValueFieldIndex)fieldIndex)
			{
				case FilterValueFieldIndex.FilterListElementID:
					DesetupSyncFilterListElement(true, false);
					_alreadyFetchedFilterListElement = false;
					break;
				case FilterValueFieldIndex.FilterValueSetID:
					DesetupSyncFilterValueSet(true, false);
					_alreadyFetchedFilterValueSet = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFilterListElement = (_filterListElement != null);
			_alreadyFetchedFilterValueSet = (_filterValueSet != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "FilterListElement":
					toReturn.Add(Relations.FilterListElementEntityUsingFilterListElementID);
					break;
				case "FilterValueSet":
					toReturn.Add(Relations.FilterValueSetEntityUsingFilterValueSetID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_filterListElement", (!this.MarkedForDeletion?_filterListElement:null));
			info.AddValue("_filterListElementReturnsNewIfNotFound", _filterListElementReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFilterListElement", _alwaysFetchFilterListElement);
			info.AddValue("_alreadyFetchedFilterListElement", _alreadyFetchedFilterListElement);
			info.AddValue("_filterValueSet", (!this.MarkedForDeletion?_filterValueSet:null));
			info.AddValue("_filterValueSetReturnsNewIfNotFound", _filterValueSetReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFilterValueSet", _alwaysFetchFilterValueSet);
			info.AddValue("_alreadyFetchedFilterValueSet", _alreadyFetchedFilterValueSet);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "FilterListElement":
					_alreadyFetchedFilterListElement = true;
					this.FilterListElement = (FilterListElementEntity)entity;
					break;
				case "FilterValueSet":
					_alreadyFetchedFilterValueSet = true;
					this.FilterValueSet = (FilterValueSetEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "FilterListElement":
					SetupSyncFilterListElement(relatedEntity);
					break;
				case "FilterValueSet":
					SetupSyncFilterValueSet(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "FilterListElement":
					DesetupSyncFilterListElement(false, true);
					break;
				case "FilterValueSet":
					DesetupSyncFilterValueSet(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_filterListElement!=null)
			{
				toReturn.Add(_filterListElement);
			}
			if(_filterValueSet!=null)
			{
				toReturn.Add(_filterValueSet);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterValueID">PK value for FilterValue which data should be fetched into this FilterValue object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterValueID)
		{
			return FetchUsingPK(filterValueID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterValueID">PK value for FilterValue which data should be fetched into this FilterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterValueID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(filterValueID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterValueID">PK value for FilterValue which data should be fetched into this FilterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterValueID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(filterValueID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterValueID">PK value for FilterValue which data should be fetched into this FilterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterValueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(filterValueID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FilterValueID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FilterValueRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'FilterListElementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FilterListElementEntity' which is related to this entity.</returns>
		public FilterListElementEntity GetSingleFilterListElement()
		{
			return GetSingleFilterListElement(false);
		}

		/// <summary> Retrieves the related entity of type 'FilterListElementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FilterListElementEntity' which is related to this entity.</returns>
		public virtual FilterListElementEntity GetSingleFilterListElement(bool forceFetch)
		{
			if( ( !_alreadyFetchedFilterListElement || forceFetch || _alwaysFetchFilterListElement) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FilterListElementEntityUsingFilterListElementID);
				FilterListElementEntity newEntity = new FilterListElementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FilterListElementID);
				}
				if(fetchResult)
				{
					newEntity = (FilterListElementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_filterListElementReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FilterListElement = newEntity;
				_alreadyFetchedFilterListElement = fetchResult;
			}
			return _filterListElement;
		}


		/// <summary> Retrieves the related entity of type 'FilterValueSetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FilterValueSetEntity' which is related to this entity.</returns>
		public FilterValueSetEntity GetSingleFilterValueSet()
		{
			return GetSingleFilterValueSet(false);
		}

		/// <summary> Retrieves the related entity of type 'FilterValueSetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FilterValueSetEntity' which is related to this entity.</returns>
		public virtual FilterValueSetEntity GetSingleFilterValueSet(bool forceFetch)
		{
			if( ( !_alreadyFetchedFilterValueSet || forceFetch || _alwaysFetchFilterValueSet) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FilterValueSetEntityUsingFilterValueSetID);
				FilterValueSetEntity newEntity = new FilterValueSetEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FilterValueSetID);
				}
				if(fetchResult)
				{
					newEntity = (FilterValueSetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_filterValueSetReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FilterValueSet = newEntity;
				_alreadyFetchedFilterValueSet = fetchResult;
			}
			return _filterValueSet;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("FilterListElement", _filterListElement);
			toReturn.Add("FilterValueSet", _filterValueSet);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="filterValueID">PK value for FilterValue which data should be fetched into this FilterValue object</param>
		/// <param name="validator">The validator object for this FilterValueEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 filterValueID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(filterValueID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_filterListElementReturnsNewIfNotFound = false;
			_filterValueSetReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Content", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterListElementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterValueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterValueSetID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ranger", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Separator", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _filterListElement</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFilterListElement(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _filterListElement, new PropertyChangedEventHandler( OnFilterListElementPropertyChanged ), "FilterListElement", VarioSL.Entities.RelationClasses.StaticFilterValueRelations.FilterListElementEntityUsingFilterListElementIDStatic, true, signalRelatedEntity, "FilterValues", resetFKFields, new int[] { (int)FilterValueFieldIndex.FilterListElementID } );		
			_filterListElement = null;
		}
		
		/// <summary> setups the sync logic for member _filterListElement</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFilterListElement(IEntityCore relatedEntity)
		{
			if(_filterListElement!=relatedEntity)
			{		
				DesetupSyncFilterListElement(true, true);
				_filterListElement = (FilterListElementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _filterListElement, new PropertyChangedEventHandler( OnFilterListElementPropertyChanged ), "FilterListElement", VarioSL.Entities.RelationClasses.StaticFilterValueRelations.FilterListElementEntityUsingFilterListElementIDStatic, true, ref _alreadyFetchedFilterListElement, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFilterListElementPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _filterValueSet</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFilterValueSet(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _filterValueSet, new PropertyChangedEventHandler( OnFilterValueSetPropertyChanged ), "FilterValueSet", VarioSL.Entities.RelationClasses.StaticFilterValueRelations.FilterValueSetEntityUsingFilterValueSetIDStatic, true, signalRelatedEntity, "FilterValues", resetFKFields, new int[] { (int)FilterValueFieldIndex.FilterValueSetID } );		
			_filterValueSet = null;
		}
		
		/// <summary> setups the sync logic for member _filterValueSet</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFilterValueSet(IEntityCore relatedEntity)
		{
			if(_filterValueSet!=relatedEntity)
			{		
				DesetupSyncFilterValueSet(true, true);
				_filterValueSet = (FilterValueSetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _filterValueSet, new PropertyChangedEventHandler( OnFilterValueSetPropertyChanged ), "FilterValueSet", VarioSL.Entities.RelationClasses.StaticFilterValueRelations.FilterValueSetEntityUsingFilterValueSetIDStatic, true, ref _alreadyFetchedFilterValueSet, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFilterValueSetPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="filterValueID">PK value for FilterValue which data should be fetched into this FilterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 filterValueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FilterValueFieldIndex.FilterValueID].ForcedCurrentValueWrite(filterValueID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFilterValueDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FilterValueEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FilterValueRelations Relations
		{
			get	{ return new FilterValueRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterListElement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterListElement
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterListElementCollection(), (IEntityRelation)GetRelationsForField("FilterListElement")[0], (int)VarioSL.Entities.EntityType.FilterValueEntity, (int)VarioSL.Entities.EntityType.FilterListElementEntity, 0, null, null, null, "FilterListElement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterValueSet'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterValueSet
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterValueSetCollection(), (IEntityRelation)GetRelationsForField("FilterValueSet")[0], (int)VarioSL.Entities.EntityType.FilterValueEntity, (int)VarioSL.Entities.EntityType.FilterValueSetEntity, 0, null, null, null, "FilterValueSet", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Content property of the Entity FilterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUE"."CONTENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Content
		{
			get { return (System.String)GetValue((int)FilterValueFieldIndex.Content, true); }
			set	{ SetValue((int)FilterValueFieldIndex.Content, value, true); }
		}

		/// <summary> The FilterListElementID property of the Entity FilterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUE"."FILTERLISTELEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FilterListElementID
		{
			get { return (System.Int64)GetValue((int)FilterValueFieldIndex.FilterListElementID, true); }
			set	{ SetValue((int)FilterValueFieldIndex.FilterListElementID, value, true); }
		}

		/// <summary> The FilterValueID property of the Entity FilterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUE"."FILTERVALUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 FilterValueID
		{
			get { return (System.Int64)GetValue((int)FilterValueFieldIndex.FilterValueID, true); }
			set	{ SetValue((int)FilterValueFieldIndex.FilterValueID, value, true); }
		}

		/// <summary> The FilterValueSetID property of the Entity FilterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUE"."FILTERVALUESETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FilterValueSetID
		{
			get { return (System.Int64)GetValue((int)FilterValueFieldIndex.FilterValueSetID, true); }
			set	{ SetValue((int)FilterValueFieldIndex.FilterValueSetID, value, true); }
		}

		/// <summary> The IsActive property of the Entity FilterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUE"."ISACTIVE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsActive
		{
			get { return (System.Boolean)GetValue((int)FilterValueFieldIndex.IsActive, true); }
			set	{ SetValue((int)FilterValueFieldIndex.IsActive, value, true); }
		}

		/// <summary> The LastModified property of the Entity FilterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)FilterValueFieldIndex.LastModified, true); }
			set	{ SetValue((int)FilterValueFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity FilterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)FilterValueFieldIndex.LastUser, true); }
			set	{ SetValue((int)FilterValueFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Ranger property of the Entity FilterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUE"."RANGER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Ranger
		{
			get { return (System.String)GetValue((int)FilterValueFieldIndex.Ranger, true); }
			set	{ SetValue((int)FilterValueFieldIndex.Ranger, value, true); }
		}

		/// <summary> The Separator property of the Entity FilterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUE"."SEPARATOR"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Separator
		{
			get { return (System.String)GetValue((int)FilterValueFieldIndex.Separator, true); }
			set	{ SetValue((int)FilterValueFieldIndex.Separator, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity FilterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)FilterValueFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)FilterValueFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'FilterListElementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFilterListElement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FilterListElementEntity FilterListElement
		{
			get	{ return GetSingleFilterListElement(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFilterListElement(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FilterValues", "FilterListElement", _filterListElement, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FilterListElement. When set to true, FilterListElement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterListElement is accessed. You can always execute a forced fetch by calling GetSingleFilterListElement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterListElement
		{
			get	{ return _alwaysFetchFilterListElement; }
			set	{ _alwaysFetchFilterListElement = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterListElement already has been fetched. Setting this property to false when FilterListElement has been fetched
		/// will set FilterListElement to null as well. Setting this property to true while FilterListElement hasn't been fetched disables lazy loading for FilterListElement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterListElement
		{
			get { return _alreadyFetchedFilterListElement;}
			set 
			{
				if(_alreadyFetchedFilterListElement && !value)
				{
					this.FilterListElement = null;
				}
				_alreadyFetchedFilterListElement = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FilterListElement is not found
		/// in the database. When set to true, FilterListElement will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FilterListElementReturnsNewIfNotFound
		{
			get	{ return _filterListElementReturnsNewIfNotFound; }
			set { _filterListElementReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FilterValueSetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFilterValueSet()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FilterValueSetEntity FilterValueSet
		{
			get	{ return GetSingleFilterValueSet(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFilterValueSet(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FilterValues", "FilterValueSet", _filterValueSet, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FilterValueSet. When set to true, FilterValueSet is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterValueSet is accessed. You can always execute a forced fetch by calling GetSingleFilterValueSet(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterValueSet
		{
			get	{ return _alwaysFetchFilterValueSet; }
			set	{ _alwaysFetchFilterValueSet = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterValueSet already has been fetched. Setting this property to false when FilterValueSet has been fetched
		/// will set FilterValueSet to null as well. Setting this property to true while FilterValueSet hasn't been fetched disables lazy loading for FilterValueSet</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterValueSet
		{
			get { return _alreadyFetchedFilterValueSet;}
			set 
			{
				if(_alreadyFetchedFilterValueSet && !value)
				{
					this.FilterValueSet = null;
				}
				_alreadyFetchedFilterValueSet = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FilterValueSet is not found
		/// in the database. When set to true, FilterValueSet will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FilterValueSetReturnsNewIfNotFound
		{
			get	{ return _filterValueSetReturnsNewIfNotFound; }
			set { _filterValueSetReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FilterValueEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
