﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DeviceReaderKeyToCert'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DeviceReaderKeyToCertEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CertificateEntity _certificate;
		private bool	_alwaysFetchCertificate, _alreadyFetchedCertificate, _certificateReturnsNewIfNotFound;
		private DeviceReaderKeyEntity _deviceReaderKey;
		private bool	_alwaysFetchDeviceReaderKey, _alreadyFetchedDeviceReaderKey, _deviceReaderKeyReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Certificate</summary>
			public static readonly string Certificate = "Certificate";
			/// <summary>Member name DeviceReaderKey</summary>
			public static readonly string DeviceReaderKey = "DeviceReaderKey";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeviceReaderKeyToCertEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DeviceReaderKeyToCertEntity() :base("DeviceReaderKeyToCertEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="deviceReaderKeyToCertificateID">PK value for DeviceReaderKeyToCert which data should be fetched into this DeviceReaderKeyToCert object</param>
		public DeviceReaderKeyToCertEntity(System.Int64 deviceReaderKeyToCertificateID):base("DeviceReaderKeyToCertEntity")
		{
			InitClassFetch(deviceReaderKeyToCertificateID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceReaderKeyToCertificateID">PK value for DeviceReaderKeyToCert which data should be fetched into this DeviceReaderKeyToCert object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeviceReaderKeyToCertEntity(System.Int64 deviceReaderKeyToCertificateID, IPrefetchPath prefetchPathToUse):base("DeviceReaderKeyToCertEntity")
		{
			InitClassFetch(deviceReaderKeyToCertificateID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceReaderKeyToCertificateID">PK value for DeviceReaderKeyToCert which data should be fetched into this DeviceReaderKeyToCert object</param>
		/// <param name="validator">The custom validator object for this DeviceReaderKeyToCertEntity</param>
		public DeviceReaderKeyToCertEntity(System.Int64 deviceReaderKeyToCertificateID, IValidator validator):base("DeviceReaderKeyToCertEntity")
		{
			InitClassFetch(deviceReaderKeyToCertificateID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeviceReaderKeyToCertEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_certificate = (CertificateEntity)info.GetValue("_certificate", typeof(CertificateEntity));
			if(_certificate!=null)
			{
				_certificate.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_certificateReturnsNewIfNotFound = info.GetBoolean("_certificateReturnsNewIfNotFound");
			_alwaysFetchCertificate = info.GetBoolean("_alwaysFetchCertificate");
			_alreadyFetchedCertificate = info.GetBoolean("_alreadyFetchedCertificate");

			_deviceReaderKey = (DeviceReaderKeyEntity)info.GetValue("_deviceReaderKey", typeof(DeviceReaderKeyEntity));
			if(_deviceReaderKey!=null)
			{
				_deviceReaderKey.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceReaderKeyReturnsNewIfNotFound = info.GetBoolean("_deviceReaderKeyReturnsNewIfNotFound");
			_alwaysFetchDeviceReaderKey = info.GetBoolean("_alwaysFetchDeviceReaderKey");
			_alreadyFetchedDeviceReaderKey = info.GetBoolean("_alreadyFetchedDeviceReaderKey");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DeviceReaderKeyToCertFieldIndex)fieldIndex)
			{
				case DeviceReaderKeyToCertFieldIndex.CertificateID:
					DesetupSyncCertificate(true, false);
					_alreadyFetchedCertificate = false;
					break;
				case DeviceReaderKeyToCertFieldIndex.DeviceReaderKeyID:
					DesetupSyncDeviceReaderKey(true, false);
					_alreadyFetchedDeviceReaderKey = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCertificate = (_certificate != null);
			_alreadyFetchedDeviceReaderKey = (_deviceReaderKey != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Certificate":
					toReturn.Add(Relations.CertificateEntityUsingCertificateID);
					break;
				case "DeviceReaderKey":
					toReturn.Add(Relations.DeviceReaderKeyEntityUsingDeviceReaderKeyID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_certificate", (!this.MarkedForDeletion?_certificate:null));
			info.AddValue("_certificateReturnsNewIfNotFound", _certificateReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCertificate", _alwaysFetchCertificate);
			info.AddValue("_alreadyFetchedCertificate", _alreadyFetchedCertificate);
			info.AddValue("_deviceReaderKey", (!this.MarkedForDeletion?_deviceReaderKey:null));
			info.AddValue("_deviceReaderKeyReturnsNewIfNotFound", _deviceReaderKeyReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceReaderKey", _alwaysFetchDeviceReaderKey);
			info.AddValue("_alreadyFetchedDeviceReaderKey", _alreadyFetchedDeviceReaderKey);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Certificate":
					_alreadyFetchedCertificate = true;
					this.Certificate = (CertificateEntity)entity;
					break;
				case "DeviceReaderKey":
					_alreadyFetchedDeviceReaderKey = true;
					this.DeviceReaderKey = (DeviceReaderKeyEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Certificate":
					SetupSyncCertificate(relatedEntity);
					break;
				case "DeviceReaderKey":
					SetupSyncDeviceReaderKey(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Certificate":
					DesetupSyncCertificate(false, true);
					break;
				case "DeviceReaderKey":
					DesetupSyncDeviceReaderKey(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_certificate!=null)
			{
				toReturn.Add(_certificate);
			}
			if(_deviceReaderKey!=null)
			{
				toReturn.Add(_deviceReaderKey);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderKeyToCertificateID">PK value for DeviceReaderKeyToCert which data should be fetched into this DeviceReaderKeyToCert object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderKeyToCertificateID)
		{
			return FetchUsingPK(deviceReaderKeyToCertificateID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderKeyToCertificateID">PK value for DeviceReaderKeyToCert which data should be fetched into this DeviceReaderKeyToCert object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderKeyToCertificateID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deviceReaderKeyToCertificateID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderKeyToCertificateID">PK value for DeviceReaderKeyToCert which data should be fetched into this DeviceReaderKeyToCert object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderKeyToCertificateID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deviceReaderKeyToCertificateID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderKeyToCertificateID">PK value for DeviceReaderKeyToCert which data should be fetched into this DeviceReaderKeyToCert object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderKeyToCertificateID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deviceReaderKeyToCertificateID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeviceReaderKeyToCertificateID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeviceReaderKeyToCertRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CertificateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CertificateEntity' which is related to this entity.</returns>
		public CertificateEntity GetSingleCertificate()
		{
			return GetSingleCertificate(false);
		}

		/// <summary> Retrieves the related entity of type 'CertificateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CertificateEntity' which is related to this entity.</returns>
		public virtual CertificateEntity GetSingleCertificate(bool forceFetch)
		{
			if( ( !_alreadyFetchedCertificate || forceFetch || _alwaysFetchCertificate) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CertificateEntityUsingCertificateID);
				CertificateEntity newEntity = new CertificateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CertificateID);
				}
				if(fetchResult)
				{
					newEntity = (CertificateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_certificateReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Certificate = newEntity;
				_alreadyFetchedCertificate = fetchResult;
			}
			return _certificate;
		}


		/// <summary> Retrieves the related entity of type 'DeviceReaderKeyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceReaderKeyEntity' which is related to this entity.</returns>
		public DeviceReaderKeyEntity GetSingleDeviceReaderKey()
		{
			return GetSingleDeviceReaderKey(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceReaderKeyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceReaderKeyEntity' which is related to this entity.</returns>
		public virtual DeviceReaderKeyEntity GetSingleDeviceReaderKey(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceReaderKey || forceFetch || _alwaysFetchDeviceReaderKey) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceReaderKeyEntityUsingDeviceReaderKeyID);
				DeviceReaderKeyEntity newEntity = new DeviceReaderKeyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceReaderKeyID);
				}
				if(fetchResult)
				{
					newEntity = (DeviceReaderKeyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceReaderKeyReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceReaderKey = newEntity;
				_alreadyFetchedDeviceReaderKey = fetchResult;
			}
			return _deviceReaderKey;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Certificate", _certificate);
			toReturn.Add("DeviceReaderKey", _deviceReaderKey);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deviceReaderKeyToCertificateID">PK value for DeviceReaderKeyToCert which data should be fetched into this DeviceReaderKeyToCert object</param>
		/// <param name="validator">The validator object for this DeviceReaderKeyToCertEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 deviceReaderKeyToCertificateID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deviceReaderKeyToCertificateID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_certificateReturnsNewIfNotFound = false;
			_deviceReaderKeyReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CertificateID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceReaderKeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceReaderKeyToCertificateID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Label", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _certificate</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCertificate(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _certificate, new PropertyChangedEventHandler( OnCertificatePropertyChanged ), "Certificate", VarioSL.Entities.RelationClasses.StaticDeviceReaderKeyToCertRelations.CertificateEntityUsingCertificateIDStatic, true, signalRelatedEntity, "DeviceReaderKeyToCerts", resetFKFields, new int[] { (int)DeviceReaderKeyToCertFieldIndex.CertificateID } );		
			_certificate = null;
		}
		
		/// <summary> setups the sync logic for member _certificate</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCertificate(IEntityCore relatedEntity)
		{
			if(_certificate!=relatedEntity)
			{		
				DesetupSyncCertificate(true, true);
				_certificate = (CertificateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _certificate, new PropertyChangedEventHandler( OnCertificatePropertyChanged ), "Certificate", VarioSL.Entities.RelationClasses.StaticDeviceReaderKeyToCertRelations.CertificateEntityUsingCertificateIDStatic, true, ref _alreadyFetchedCertificate, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCertificatePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deviceReaderKey</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceReaderKey(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceReaderKey, new PropertyChangedEventHandler( OnDeviceReaderKeyPropertyChanged ), "DeviceReaderKey", VarioSL.Entities.RelationClasses.StaticDeviceReaderKeyToCertRelations.DeviceReaderKeyEntityUsingDeviceReaderKeyIDStatic, true, signalRelatedEntity, "DeviceReaderKeyToCerts", resetFKFields, new int[] { (int)DeviceReaderKeyToCertFieldIndex.DeviceReaderKeyID } );		
			_deviceReaderKey = null;
		}
		
		/// <summary> setups the sync logic for member _deviceReaderKey</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceReaderKey(IEntityCore relatedEntity)
		{
			if(_deviceReaderKey!=relatedEntity)
			{		
				DesetupSyncDeviceReaderKey(true, true);
				_deviceReaderKey = (DeviceReaderKeyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceReaderKey, new PropertyChangedEventHandler( OnDeviceReaderKeyPropertyChanged ), "DeviceReaderKey", VarioSL.Entities.RelationClasses.StaticDeviceReaderKeyToCertRelations.DeviceReaderKeyEntityUsingDeviceReaderKeyIDStatic, true, ref _alreadyFetchedDeviceReaderKey, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceReaderKeyPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deviceReaderKeyToCertificateID">PK value for DeviceReaderKeyToCert which data should be fetched into this DeviceReaderKeyToCert object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 deviceReaderKeyToCertificateID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeviceReaderKeyToCertFieldIndex.DeviceReaderKeyToCertificateID].ForcedCurrentValueWrite(deviceReaderKeyToCertificateID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeviceReaderKeyToCertDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeviceReaderKeyToCertEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeviceReaderKeyToCertRelations Relations
		{
			get	{ return new DeviceReaderKeyToCertRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Certificate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCertificate
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CertificateCollection(), (IEntityRelation)GetRelationsForField("Certificate")[0], (int)VarioSL.Entities.EntityType.DeviceReaderKeyToCertEntity, (int)VarioSL.Entities.EntityType.CertificateEntity, 0, null, null, null, "Certificate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReaderKey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceReaderKey
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderKeyCollection(), (IEntityRelation)GetRelationsForField("DeviceReaderKey")[0], (int)VarioSL.Entities.EntityType.DeviceReaderKeyToCertEntity, (int)VarioSL.Entities.EntityType.DeviceReaderKeyEntity, 0, null, null, null, "DeviceReaderKey", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CertificateID property of the Entity DeviceReaderKeyToCert<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEYTOCERT"."CERTIFICATEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CertificateID
		{
			get { return (System.Int64)GetValue((int)DeviceReaderKeyToCertFieldIndex.CertificateID, true); }
			set	{ SetValue((int)DeviceReaderKeyToCertFieldIndex.CertificateID, value, true); }
		}

		/// <summary> The DeviceReaderKeyID property of the Entity DeviceReaderKeyToCert<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEYTOCERT"."DEVICEREADERKEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DeviceReaderKeyID
		{
			get { return (System.Int64)GetValue((int)DeviceReaderKeyToCertFieldIndex.DeviceReaderKeyID, true); }
			set	{ SetValue((int)DeviceReaderKeyToCertFieldIndex.DeviceReaderKeyID, value, true); }
		}

		/// <summary> The DeviceReaderKeyToCertificateID property of the Entity DeviceReaderKeyToCert<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEYTOCERT"."DEVICEREADERKEYTOCERTIFICATEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 DeviceReaderKeyToCertificateID
		{
			get { return (System.Int64)GetValue((int)DeviceReaderKeyToCertFieldIndex.DeviceReaderKeyToCertificateID, true); }
			set	{ SetValue((int)DeviceReaderKeyToCertFieldIndex.DeviceReaderKeyToCertificateID, value, true); }
		}

		/// <summary> The Label property of the Entity DeviceReaderKeyToCert<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEYTOCERT"."LABEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Label
		{
			get { return (System.String)GetValue((int)DeviceReaderKeyToCertFieldIndex.Label, true); }
			set	{ SetValue((int)DeviceReaderKeyToCertFieldIndex.Label, value, true); }
		}

		/// <summary> The LastModified property of the Entity DeviceReaderKeyToCert<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEYTOCERT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)DeviceReaderKeyToCertFieldIndex.LastModified, true); }
			set	{ SetValue((int)DeviceReaderKeyToCertFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity DeviceReaderKeyToCert<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEYTOCERT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)DeviceReaderKeyToCertFieldIndex.LastUser, true); }
			set	{ SetValue((int)DeviceReaderKeyToCertFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity DeviceReaderKeyToCert<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEYTOCERT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)DeviceReaderKeyToCertFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)DeviceReaderKeyToCertFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CertificateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCertificate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CertificateEntity Certificate
		{
			get	{ return GetSingleCertificate(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCertificate(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeviceReaderKeyToCerts", "Certificate", _certificate, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Certificate. When set to true, Certificate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Certificate is accessed. You can always execute a forced fetch by calling GetSingleCertificate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCertificate
		{
			get	{ return _alwaysFetchCertificate; }
			set	{ _alwaysFetchCertificate = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Certificate already has been fetched. Setting this property to false when Certificate has been fetched
		/// will set Certificate to null as well. Setting this property to true while Certificate hasn't been fetched disables lazy loading for Certificate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCertificate
		{
			get { return _alreadyFetchedCertificate;}
			set 
			{
				if(_alreadyFetchedCertificate && !value)
				{
					this.Certificate = null;
				}
				_alreadyFetchedCertificate = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Certificate is not found
		/// in the database. When set to true, Certificate will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CertificateReturnsNewIfNotFound
		{
			get	{ return _certificateReturnsNewIfNotFound; }
			set { _certificateReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeviceReaderKeyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceReaderKey()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceReaderKeyEntity DeviceReaderKey
		{
			get	{ return GetSingleDeviceReaderKey(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceReaderKey(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeviceReaderKeyToCerts", "DeviceReaderKey", _deviceReaderKey, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceReaderKey. When set to true, DeviceReaderKey is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceReaderKey is accessed. You can always execute a forced fetch by calling GetSingleDeviceReaderKey(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceReaderKey
		{
			get	{ return _alwaysFetchDeviceReaderKey; }
			set	{ _alwaysFetchDeviceReaderKey = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceReaderKey already has been fetched. Setting this property to false when DeviceReaderKey has been fetched
		/// will set DeviceReaderKey to null as well. Setting this property to true while DeviceReaderKey hasn't been fetched disables lazy loading for DeviceReaderKey</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceReaderKey
		{
			get { return _alreadyFetchedDeviceReaderKey;}
			set 
			{
				if(_alreadyFetchedDeviceReaderKey && !value)
				{
					this.DeviceReaderKey = null;
				}
				_alreadyFetchedDeviceReaderKey = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceReaderKey is not found
		/// in the database. When set to true, DeviceReaderKey will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceReaderKeyReturnsNewIfNotFound
		{
			get	{ return _deviceReaderKeyReturnsNewIfNotFound; }
			set { _deviceReaderKeyReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DeviceReaderKeyToCertEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
