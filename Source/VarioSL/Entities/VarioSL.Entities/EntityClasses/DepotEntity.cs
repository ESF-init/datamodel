﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Depot'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DepotEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DebtorCollection	_debtors;
		private bool	_alwaysFetchDebtors, _alreadyFetchedDebtors;
		private VarioSL.Entities.CollectionClasses.UnitCollection	_units;
		private bool	_alwaysFetchUnits, _alreadyFetchedUnits;
		private VarioSL.Entities.CollectionClasses.UserListCollection	_userLists;
		private bool	_alwaysFetchUserLists, _alreadyFetchedUserLists;
		private VarioSL.Entities.CollectionClasses.VarioSettlementCollection	_settlements;
		private bool	_alwaysFetchSettlements, _alreadyFetchedSettlements;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private VarioAddressEntity _address;
		private bool	_alwaysFetchAddress, _alreadyFetchedAddress, _addressReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name Debtors</summary>
			public static readonly string Debtors = "Debtors";
			/// <summary>Member name Units</summary>
			public static readonly string Units = "Units";
			/// <summary>Member name UserLists</summary>
			public static readonly string UserLists = "UserLists";
			/// <summary>Member name Settlements</summary>
			public static readonly string Settlements = "Settlements";
			/// <summary>Member name Address</summary>
			public static readonly string Address = "Address";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DepotEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DepotEntity() :base("DepotEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="depotID">PK value for Depot which data should be fetched into this Depot object</param>
		public DepotEntity(System.Int64 depotID):base("DepotEntity")
		{
			InitClassFetch(depotID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="depotID">PK value for Depot which data should be fetched into this Depot object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DepotEntity(System.Int64 depotID, IPrefetchPath prefetchPathToUse):base("DepotEntity")
		{
			InitClassFetch(depotID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="depotID">PK value for Depot which data should be fetched into this Depot object</param>
		/// <param name="validator">The custom validator object for this DepotEntity</param>
		public DepotEntity(System.Int64 depotID, IValidator validator):base("DepotEntity")
		{
			InitClassFetch(depotID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DepotEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_debtors = (VarioSL.Entities.CollectionClasses.DebtorCollection)info.GetValue("_debtors", typeof(VarioSL.Entities.CollectionClasses.DebtorCollection));
			_alwaysFetchDebtors = info.GetBoolean("_alwaysFetchDebtors");
			_alreadyFetchedDebtors = info.GetBoolean("_alreadyFetchedDebtors");

			_units = (VarioSL.Entities.CollectionClasses.UnitCollection)info.GetValue("_units", typeof(VarioSL.Entities.CollectionClasses.UnitCollection));
			_alwaysFetchUnits = info.GetBoolean("_alwaysFetchUnits");
			_alreadyFetchedUnits = info.GetBoolean("_alreadyFetchedUnits");

			_userLists = (VarioSL.Entities.CollectionClasses.UserListCollection)info.GetValue("_userLists", typeof(VarioSL.Entities.CollectionClasses.UserListCollection));
			_alwaysFetchUserLists = info.GetBoolean("_alwaysFetchUserLists");
			_alreadyFetchedUserLists = info.GetBoolean("_alreadyFetchedUserLists");

			_settlements = (VarioSL.Entities.CollectionClasses.VarioSettlementCollection)info.GetValue("_settlements", typeof(VarioSL.Entities.CollectionClasses.VarioSettlementCollection));
			_alwaysFetchSettlements = info.GetBoolean("_alwaysFetchSettlements");
			_alreadyFetchedSettlements = info.GetBoolean("_alreadyFetchedSettlements");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");
			_address = (VarioAddressEntity)info.GetValue("_address", typeof(VarioAddressEntity));
			if(_address!=null)
			{
				_address.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_addressReturnsNewIfNotFound = info.GetBoolean("_addressReturnsNewIfNotFound");
			_alwaysFetchAddress = info.GetBoolean("_alwaysFetchAddress");
			_alreadyFetchedAddress = info.GetBoolean("_alreadyFetchedAddress");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DepotFieldIndex)fieldIndex)
			{
				case DepotFieldIndex.AddressID:
					DesetupSyncAddress(true, false);
					_alreadyFetchedAddress = false;
					break;
				case DepotFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDebtors = (_debtors.Count > 0);
			_alreadyFetchedUnits = (_units.Count > 0);
			_alreadyFetchedUserLists = (_userLists.Count > 0);
			_alreadyFetchedSettlements = (_settlements.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedAddress = (_address != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "Debtors":
					toReturn.Add(Relations.DebtorEntityUsingDepotID);
					break;
				case "Units":
					toReturn.Add(Relations.UnitEntityUsingDepotID);
					break;
				case "UserLists":
					toReturn.Add(Relations.UserListEntityUsingDepotID);
					break;
				case "Settlements":
					toReturn.Add(Relations.VarioSettlementEntityUsingDepotID);
					break;
				case "Address":
					toReturn.Add(Relations.VarioAddressEntityUsingAddressID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_debtors", (!this.MarkedForDeletion?_debtors:null));
			info.AddValue("_alwaysFetchDebtors", _alwaysFetchDebtors);
			info.AddValue("_alreadyFetchedDebtors", _alreadyFetchedDebtors);
			info.AddValue("_units", (!this.MarkedForDeletion?_units:null));
			info.AddValue("_alwaysFetchUnits", _alwaysFetchUnits);
			info.AddValue("_alreadyFetchedUnits", _alreadyFetchedUnits);
			info.AddValue("_userLists", (!this.MarkedForDeletion?_userLists:null));
			info.AddValue("_alwaysFetchUserLists", _alwaysFetchUserLists);
			info.AddValue("_alreadyFetchedUserLists", _alreadyFetchedUserLists);
			info.AddValue("_settlements", (!this.MarkedForDeletion?_settlements:null));
			info.AddValue("_alwaysFetchSettlements", _alwaysFetchSettlements);
			info.AddValue("_alreadyFetchedSettlements", _alreadyFetchedSettlements);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);

			info.AddValue("_address", (!this.MarkedForDeletion?_address:null));
			info.AddValue("_addressReturnsNewIfNotFound", _addressReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAddress", _alwaysFetchAddress);
			info.AddValue("_alreadyFetchedAddress", _alreadyFetchedAddress);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "Debtors":
					_alreadyFetchedDebtors = true;
					if(entity!=null)
					{
						this.Debtors.Add((DebtorEntity)entity);
					}
					break;
				case "Units":
					_alreadyFetchedUnits = true;
					if(entity!=null)
					{
						this.Units.Add((UnitEntity)entity);
					}
					break;
				case "UserLists":
					_alreadyFetchedUserLists = true;
					if(entity!=null)
					{
						this.UserLists.Add((UserListEntity)entity);
					}
					break;
				case "Settlements":
					_alreadyFetchedSettlements = true;
					if(entity!=null)
					{
						this.Settlements.Add((VarioSettlementEntity)entity);
					}
					break;
				case "Address":
					_alreadyFetchedAddress = true;
					this.Address = (VarioAddressEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "Debtors":
					_debtors.Add((DebtorEntity)relatedEntity);
					break;
				case "Units":
					_units.Add((UnitEntity)relatedEntity);
					break;
				case "UserLists":
					_userLists.Add((UserListEntity)relatedEntity);
					break;
				case "Settlements":
					_settlements.Add((VarioSettlementEntity)relatedEntity);
					break;
				case "Address":
					SetupSyncAddress(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "Debtors":
					this.PerformRelatedEntityRemoval(_debtors, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Units":
					this.PerformRelatedEntityRemoval(_units, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UserLists":
					this.PerformRelatedEntityRemoval(_userLists, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Settlements":
					this.PerformRelatedEntityRemoval(_settlements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Address":
					DesetupSyncAddress(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_address!=null)
			{
				toReturn.Add(_address);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_debtors);
			toReturn.Add(_units);
			toReturn.Add(_userLists);
			toReturn.Add(_settlements);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAddressID(Nullable<System.Int64> addressID)
		{
			return FetchUsingUCAddressID( addressID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAddressID(Nullable<System.Int64> addressID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCAddressID( addressID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAddressID(Nullable<System.Int64> addressID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCAddressID( addressID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAddressID(Nullable<System.Int64> addressID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((DepotDAO)CreateDAOInstance()).FetchDepotUsingUCAddressID(this, this.Transaction, addressID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="depotID">PK value for Depot which data should be fetched into this Depot object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 depotID)
		{
			return FetchUsingPK(depotID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="depotID">PK value for Depot which data should be fetched into this Depot object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 depotID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(depotID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="depotID">PK value for Depot which data should be fetched into this Depot object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 depotID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(depotID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="depotID">PK value for Depot which data should be fetched into this Depot object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 depotID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(depotID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DepotID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DepotRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DebtorEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DebtorEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DebtorCollection GetMultiDebtors(bool forceFetch)
		{
			return GetMultiDebtors(forceFetch, _debtors.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DebtorEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DebtorEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DebtorCollection GetMultiDebtors(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDebtors(forceFetch, _debtors.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DebtorEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DebtorCollection GetMultiDebtors(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDebtors(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DebtorEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DebtorCollection GetMultiDebtors(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDebtors || forceFetch || _alwaysFetchDebtors) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_debtors);
				_debtors.SuppressClearInGetMulti=!forceFetch;
				_debtors.EntityFactoryToUse = entityFactoryToUse;
				_debtors.GetMultiManyToOne(null, this, filter);
				_debtors.SuppressClearInGetMulti=false;
				_alreadyFetchedDebtors = true;
			}
			return _debtors;
		}

		/// <summary> Sets the collection parameters for the collection for 'Debtors'. These settings will be taken into account
		/// when the property Debtors is requested or GetMultiDebtors is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDebtors(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_debtors.SortClauses=sortClauses;
			_debtors.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UnitEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollection GetMultiUnits(bool forceFetch)
		{
			return GetMultiUnits(forceFetch, _units.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UnitEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollection GetMultiUnits(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUnits(forceFetch, _units.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollection GetMultiUnits(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUnits(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollection GetMultiUnits(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUnits || forceFetch || _alwaysFetchUnits) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_units);
				_units.SuppressClearInGetMulti=!forceFetch;
				_units.EntityFactoryToUse = entityFactoryToUse;
				_units.GetMultiManyToOne(null, this, null, filter);
				_units.SuppressClearInGetMulti=false;
				_alreadyFetchedUnits = true;
			}
			return _units;
		}

		/// <summary> Sets the collection parameters for the collection for 'Units'. These settings will be taken into account
		/// when the property Units is requested or GetMultiUnits is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUnits(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_units.SortClauses=sortClauses;
			_units.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUserLists(bool forceFetch)
		{
			return GetMultiUserLists(forceFetch, _userLists.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUserLists(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserLists(forceFetch, _userLists.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUserLists(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserLists(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUserLists(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserLists || forceFetch || _alwaysFetchUserLists) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userLists);
				_userLists.SuppressClearInGetMulti=!forceFetch;
				_userLists.EntityFactoryToUse = entityFactoryToUse;
				_userLists.GetMultiManyToOne(null, null, this, null, null, null, filter);
				_userLists.SuppressClearInGetMulti=false;
				_alreadyFetchedUserLists = true;
			}
			return _userLists;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserLists'. These settings will be taken into account
		/// when the property UserLists is requested or GetMultiUserLists is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserLists(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userLists.SortClauses=sortClauses;
			_userLists.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VarioSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiSettlements(bool forceFetch)
		{
			return GetMultiSettlements(forceFetch, _settlements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VarioSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiSettlements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlements(forceFetch, _settlements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiSettlements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiSettlements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlements || forceFetch || _alwaysFetchSettlements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlements);
				_settlements.SuppressClearInGetMulti=!forceFetch;
				_settlements.EntityFactoryToUse = entityFactoryToUse;
				_settlements.GetMultiManyToOne(null, this, null, filter);
				_settlements.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlements = true;
			}
			return _settlements;
		}

		/// <summary> Sets the collection parameters for the collection for 'Settlements'. These settings will be taken into account
		/// when the property Settlements is requested or GetMultiSettlements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlements.SortClauses=sortClauses;
			_settlements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}

		/// <summary> Retrieves the related entity of type 'VarioAddressEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'VarioAddressEntity' which is related to this entity.</returns>
		public VarioAddressEntity GetSingleAddress()
		{
			return GetSingleAddress(false);
		}
		
		/// <summary> Retrieves the related entity of type 'VarioAddressEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VarioAddressEntity' which is related to this entity.</returns>
		public virtual VarioAddressEntity GetSingleAddress(bool forceFetch)
		{
			if( ( !_alreadyFetchedAddress || forceFetch || _alwaysFetchAddress) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VarioAddressEntityUsingAddressID);
				VarioAddressEntity newEntity = new VarioAddressEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AddressID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (VarioAddressEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_addressReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Address = newEntity;
				_alreadyFetchedAddress = fetchResult;
			}
			return _address;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("Debtors", _debtors);
			toReturn.Add("Units", _units);
			toReturn.Add("UserLists", _userLists);
			toReturn.Add("Settlements", _settlements);
			toReturn.Add("Address", _address);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="depotID">PK value for Depot which data should be fetched into this Depot object</param>
		/// <param name="validator">The validator object for this DepotEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 depotID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(depotID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_debtors = new VarioSL.Entities.CollectionClasses.DebtorCollection();
			_debtors.SetContainingEntityInfo(this, "Depot");

			_units = new VarioSL.Entities.CollectionClasses.UnitCollection();
			_units.SetContainingEntityInfo(this, "VarioDepot");

			_userLists = new VarioSL.Entities.CollectionClasses.UserListCollection();
			_userLists.SetContainingEntityInfo(this, "VarioDepot");

			_settlements = new VarioSL.Entities.CollectionClasses.VarioSettlementCollection();
			_settlements.SetContainingEntityInfo(this, "Depot");
			_clientReturnsNewIfNotFound = false;
			_addressReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountID2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepotDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepotID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepotLongName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepotName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepotNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastTimeAvailabe", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotificationAddress", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticDepotRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "Depots", resetFKFields, new int[] { (int)DepotFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticDepotRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _address</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAddress(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _address, new PropertyChangedEventHandler( OnAddressPropertyChanged ), "Address", VarioSL.Entities.RelationClasses.StaticDepotRelations.VarioAddressEntityUsingAddressIDStatic, true, signalRelatedEntity, "Depot", resetFKFields, new int[] { (int)DepotFieldIndex.AddressID } );
			_address = null;
		}
	
		/// <summary> setups the sync logic for member _address</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAddress(IEntityCore relatedEntity)
		{
			if(_address!=relatedEntity)
			{
				DesetupSyncAddress(true, true);
				_address = (VarioAddressEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _address, new PropertyChangedEventHandler( OnAddressPropertyChanged ), "Address", VarioSL.Entities.RelationClasses.StaticDepotRelations.VarioAddressEntityUsingAddressIDStatic, true, ref _alreadyFetchedAddress, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAddressPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="depotID">PK value for Depot which data should be fetched into this Depot object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 depotID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DepotFieldIndex.DepotID].ForcedCurrentValueWrite(depotID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDepotDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DepotEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DepotRelations Relations
		{
			get	{ return new DepotRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Debtor' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDebtors
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCollection(), (IEntityRelation)GetRelationsForField("Debtors")[0], (int)VarioSL.Entities.EntityType.DepotEntity, (int)VarioSL.Entities.EntityType.DebtorEntity, 0, null, null, null, "Debtors", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Unit' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnits
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollection(), (IEntityRelation)GetRelationsForField("Units")[0], (int)VarioSL.Entities.EntityType.DepotEntity, (int)VarioSL.Entities.EntityType.UnitEntity, 0, null, null, null, "Units", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserLists
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("UserLists")[0], (int)VarioSL.Entities.EntityType.DepotEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "UserLists", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VarioSettlement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VarioSettlementCollection(), (IEntityRelation)GetRelationsForField("Settlements")[0], (int)VarioSL.Entities.EntityType.DepotEntity, (int)VarioSL.Entities.EntityType.VarioSettlementEntity, 0, null, null, null, "Settlements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.DepotEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VarioAddress'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAddress
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VarioAddressCollection(), (IEntityRelation)GetRelationsForField("Address")[0], (int)VarioSL.Entities.EntityType.DepotEntity, (int)VarioSL.Entities.EntityType.VarioAddressEntity, 0, null, null, null, "Address", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccountID property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."ACCOUNTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AccountID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DepotFieldIndex.AccountID, false); }
			set	{ SetValue((int)DepotFieldIndex.AccountID, value, true); }
		}

		/// <summary> The AccountID2 property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."ACCOUNTID2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AccountID2
		{
			get { return (Nullable<System.Int64>)GetValue((int)DepotFieldIndex.AccountID2, false); }
			set	{ SetValue((int)DepotFieldIndex.AccountID2, value, true); }
		}

		/// <summary> The AddressID property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."ADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AddressID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DepotFieldIndex.AddressID, false); }
			set	{ SetValue((int)DepotFieldIndex.AddressID, value, true); }
		}

		/// <summary> The CashID property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."CASHID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DepotFieldIndex.CashID, false); }
			set	{ SetValue((int)DepotFieldIndex.CashID, value, true); }
		}

		/// <summary> The ClientID property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DepotFieldIndex.ClientID, false); }
			set	{ SetValue((int)DepotFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CustomerNumber property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."CUSTOMERNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomerNumber
		{
			get { return (System.String)GetValue((int)DepotFieldIndex.CustomerNumber, true); }
			set	{ SetValue((int)DepotFieldIndex.CustomerNumber, value, true); }
		}

		/// <summary> The DepotDescription property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."DEPOTDESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DepotDescription
		{
			get { return (System.String)GetValue((int)DepotFieldIndex.DepotDescription, true); }
			set	{ SetValue((int)DepotFieldIndex.DepotDescription, value, true); }
		}

		/// <summary> The DepotID property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."DEPOTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 DepotID
		{
			get { return (System.Int64)GetValue((int)DepotFieldIndex.DepotID, true); }
			set	{ SetValue((int)DepotFieldIndex.DepotID, value, true); }
		}

		/// <summary> The DepotLongName property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."DEPOTLONGNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DepotLongName
		{
			get { return (System.String)GetValue((int)DepotFieldIndex.DepotLongName, true); }
			set	{ SetValue((int)DepotFieldIndex.DepotLongName, value, true); }
		}

		/// <summary> The DepotName property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."DEPOTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DepotName
		{
			get { return (System.String)GetValue((int)DepotFieldIndex.DepotName, true); }
			set	{ SetValue((int)DepotFieldIndex.DepotName, value, true); }
		}

		/// <summary> The DepotNumber property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."DEPOTNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DepotNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)DepotFieldIndex.DepotNumber, false); }
			set	{ SetValue((int)DepotFieldIndex.DepotNumber, value, true); }
		}

		/// <summary> The LastTimeAvailabe property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."LASTTIMEAVAILABE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastTimeAvailabe
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DepotFieldIndex.LastTimeAvailabe, false); }
			set	{ SetValue((int)DepotFieldIndex.LastTimeAvailabe, value, true); }
		}

		/// <summary> The NotificationAddress property of the Entity Depot<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEPOT"."NOTIFICATIONADDRESS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NotificationAddress
		{
			get { return (System.String)GetValue((int)DepotFieldIndex.NotificationAddress, true); }
			set	{ SetValue((int)DepotFieldIndex.NotificationAddress, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DebtorEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDebtors()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DebtorCollection Debtors
		{
			get	{ return GetMultiDebtors(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Debtors. When set to true, Debtors is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Debtors is accessed. You can always execute/ a forced fetch by calling GetMultiDebtors(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDebtors
		{
			get	{ return _alwaysFetchDebtors; }
			set	{ _alwaysFetchDebtors = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Debtors already has been fetched. Setting this property to false when Debtors has been fetched
		/// will clear the Debtors collection well. Setting this property to true while Debtors hasn't been fetched disables lazy loading for Debtors</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDebtors
		{
			get { return _alreadyFetchedDebtors;}
			set 
			{
				if(_alreadyFetchedDebtors && !value && (_debtors != null))
				{
					_debtors.Clear();
				}
				_alreadyFetchedDebtors = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUnits()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollection Units
		{
			get	{ return GetMultiUnits(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Units. When set to true, Units is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Units is accessed. You can always execute/ a forced fetch by calling GetMultiUnits(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnits
		{
			get	{ return _alwaysFetchUnits; }
			set	{ _alwaysFetchUnits = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Units already has been fetched. Setting this property to false when Units has been fetched
		/// will clear the Units collection well. Setting this property to true while Units hasn't been fetched disables lazy loading for Units</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnits
		{
			get { return _alreadyFetchedUnits;}
			set 
			{
				if(_alreadyFetchedUnits && !value && (_units != null))
				{
					_units.Clear();
				}
				_alreadyFetchedUnits = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserLists()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UserListCollection UserLists
		{
			get	{ return GetMultiUserLists(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserLists. When set to true, UserLists is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserLists is accessed. You can always execute/ a forced fetch by calling GetMultiUserLists(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserLists
		{
			get	{ return _alwaysFetchUserLists; }
			set	{ _alwaysFetchUserLists = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserLists already has been fetched. Setting this property to false when UserLists has been fetched
		/// will clear the UserLists collection well. Setting this property to true while UserLists hasn't been fetched disables lazy loading for UserLists</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserLists
		{
			get { return _alreadyFetchedUserLists;}
			set 
			{
				if(_alreadyFetchedUserLists && !value && (_userLists != null))
				{
					_userLists.Clear();
				}
				_alreadyFetchedUserLists = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VarioSettlementCollection Settlements
		{
			get	{ return GetMultiSettlements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Settlements. When set to true, Settlements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Settlements is accessed. You can always execute/ a forced fetch by calling GetMultiSettlements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlements
		{
			get	{ return _alwaysFetchSettlements; }
			set	{ _alwaysFetchSettlements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Settlements already has been fetched. Setting this property to false when Settlements has been fetched
		/// will clear the Settlements collection well. Setting this property to true while Settlements hasn't been fetched disables lazy loading for Settlements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlements
		{
			get { return _alreadyFetchedSettlements;}
			set 
			{
				if(_alreadyFetchedSettlements && !value && (_settlements != null))
				{
					_settlements.Clear();
				}
				_alreadyFetchedSettlements = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Depots", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VarioAddressEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAddress()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VarioAddressEntity Address
		{
			get	{ return GetSingleAddress(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncAddress(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_address !=null);
						DesetupSyncAddress(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("Address");
						}
					}
					else
					{
						if(_address!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Depot");
							SetupSyncAddress(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Address. When set to true, Address is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Address is accessed. You can always execute a forced fetch by calling GetSingleAddress(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAddress
		{
			get	{ return _alwaysFetchAddress; }
			set	{ _alwaysFetchAddress = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property Address already has been fetched. Setting this property to false when Address has been fetched
		/// will set Address to null as well. Setting this property to true while Address hasn't been fetched disables lazy loading for Address</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAddress
		{
			get { return _alreadyFetchedAddress;}
			set 
			{
				if(_alreadyFetchedAddress && !value)
				{
					this.Address = null;
				}
				_alreadyFetchedAddress = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Address is not found
		/// in the database. When set to true, Address will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AddressReturnsNewIfNotFound
		{
			get	{ return _addressReturnsNewIfNotFound; }
			set	{ _addressReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DepotEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
