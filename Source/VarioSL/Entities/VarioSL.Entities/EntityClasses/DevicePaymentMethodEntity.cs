﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DevicePaymentMethod'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DevicePaymentMethodEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ClearingResultCollection	_clearingResults;
		private bool	_alwaysFetchClearingResults, _alreadyFetchedClearingResults;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection	_ticketDeviceClassPaymentMethod;
		private bool	_alwaysFetchTicketDeviceClassPaymentMethod, _alreadyFetchedTicketDeviceClassPaymentMethod;
		private VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection	_ticketDevicePaymentMethods;
		private bool	_alwaysFetchTicketDevicePaymentMethods, _alreadyFetchedTicketDevicePaymentMethods;
		private VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection	_contractToPaymentMethods;
		private bool	_alwaysFetchContractToPaymentMethods, _alreadyFetchedContractToPaymentMethods;
		private VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection	_salesChannelToPaymentMethods;
		private bool	_alwaysFetchSalesChannelToPaymentMethods, _alreadyFetchedSalesChannelToPaymentMethods;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection _ticketDeviceClassCollection;
		private bool	_alwaysFetchTicketDeviceClassCollection, _alreadyFetchedTicketDeviceClassCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClearingResults</summary>
			public static readonly string ClearingResults = "ClearingResults";
			/// <summary>Member name TicketDeviceClassPaymentMethod</summary>
			public static readonly string TicketDeviceClassPaymentMethod = "TicketDeviceClassPaymentMethod";
			/// <summary>Member name TicketDevicePaymentMethods</summary>
			public static readonly string TicketDevicePaymentMethods = "TicketDevicePaymentMethods";
			/// <summary>Member name ContractToPaymentMethods</summary>
			public static readonly string ContractToPaymentMethods = "ContractToPaymentMethods";
			/// <summary>Member name SalesChannelToPaymentMethods</summary>
			public static readonly string SalesChannelToPaymentMethods = "SalesChannelToPaymentMethods";
			/// <summary>Member name TicketDeviceClassCollection</summary>
			public static readonly string TicketDeviceClassCollection = "TicketDeviceClassCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DevicePaymentMethodEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DevicePaymentMethodEntity() :base("DevicePaymentMethodEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="devicePaymentMethodID">PK value for DevicePaymentMethod which data should be fetched into this DevicePaymentMethod object</param>
		public DevicePaymentMethodEntity(System.Int64 devicePaymentMethodID):base("DevicePaymentMethodEntity")
		{
			InitClassFetch(devicePaymentMethodID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="devicePaymentMethodID">PK value for DevicePaymentMethod which data should be fetched into this DevicePaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DevicePaymentMethodEntity(System.Int64 devicePaymentMethodID, IPrefetchPath prefetchPathToUse):base("DevicePaymentMethodEntity")
		{
			InitClassFetch(devicePaymentMethodID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="devicePaymentMethodID">PK value for DevicePaymentMethod which data should be fetched into this DevicePaymentMethod object</param>
		/// <param name="validator">The custom validator object for this DevicePaymentMethodEntity</param>
		public DevicePaymentMethodEntity(System.Int64 devicePaymentMethodID, IValidator validator):base("DevicePaymentMethodEntity")
		{
			InitClassFetch(devicePaymentMethodID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DevicePaymentMethodEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clearingResults = (VarioSL.Entities.CollectionClasses.ClearingResultCollection)info.GetValue("_clearingResults", typeof(VarioSL.Entities.CollectionClasses.ClearingResultCollection));
			_alwaysFetchClearingResults = info.GetBoolean("_alwaysFetchClearingResults");
			_alreadyFetchedClearingResults = info.GetBoolean("_alreadyFetchedClearingResults");

			_ticketDeviceClassPaymentMethod = (VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection)info.GetValue("_ticketDeviceClassPaymentMethod", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection));
			_alwaysFetchTicketDeviceClassPaymentMethod = info.GetBoolean("_alwaysFetchTicketDeviceClassPaymentMethod");
			_alreadyFetchedTicketDeviceClassPaymentMethod = info.GetBoolean("_alreadyFetchedTicketDeviceClassPaymentMethod");

			_ticketDevicePaymentMethods = (VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection)info.GetValue("_ticketDevicePaymentMethods", typeof(VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection));
			_alwaysFetchTicketDevicePaymentMethods = info.GetBoolean("_alwaysFetchTicketDevicePaymentMethods");
			_alreadyFetchedTicketDevicePaymentMethods = info.GetBoolean("_alreadyFetchedTicketDevicePaymentMethods");

			_contractToPaymentMethods = (VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection)info.GetValue("_contractToPaymentMethods", typeof(VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection));
			_alwaysFetchContractToPaymentMethods = info.GetBoolean("_alwaysFetchContractToPaymentMethods");
			_alreadyFetchedContractToPaymentMethods = info.GetBoolean("_alreadyFetchedContractToPaymentMethods");

			_salesChannelToPaymentMethods = (VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection)info.GetValue("_salesChannelToPaymentMethods", typeof(VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection));
			_alwaysFetchSalesChannelToPaymentMethods = info.GetBoolean("_alwaysFetchSalesChannelToPaymentMethods");
			_alreadyFetchedSalesChannelToPaymentMethods = info.GetBoolean("_alreadyFetchedSalesChannelToPaymentMethods");
			_ticketDeviceClassCollection = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceClassCollection", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceClassCollection = info.GetBoolean("_alwaysFetchTicketDeviceClassCollection");
			_alreadyFetchedTicketDeviceClassCollection = info.GetBoolean("_alreadyFetchedTicketDeviceClassCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClearingResults = (_clearingResults.Count > 0);
			_alreadyFetchedTicketDeviceClassPaymentMethod = (_ticketDeviceClassPaymentMethod.Count > 0);
			_alreadyFetchedTicketDevicePaymentMethods = (_ticketDevicePaymentMethods.Count > 0);
			_alreadyFetchedContractToPaymentMethods = (_contractToPaymentMethods.Count > 0);
			_alreadyFetchedSalesChannelToPaymentMethods = (_salesChannelToPaymentMethods.Count > 0);
			_alreadyFetchedTicketDeviceClassCollection = (_ticketDeviceClassCollection.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ClearingResults":
					toReturn.Add(Relations.ClearingResultEntityUsingDevicePaymentMethodID);
					break;
				case "TicketDeviceClassPaymentMethod":
					toReturn.Add(Relations.TicketDeviceClassPaymentMethodEntityUsingDevicePaymentMethodID);
					break;
				case "TicketDevicePaymentMethods":
					toReturn.Add(Relations.TicketDevicePaymentMethodEntityUsingDevicePaymentMethodID);
					break;
				case "ContractToPaymentMethods":
					toReturn.Add(Relations.ContractToPaymentMethodEntityUsingDevicePaymentMethodID);
					break;
				case "SalesChannelToPaymentMethods":
					toReturn.Add(Relations.SalesChannelToPaymentMethodEntityUsingDevicePaymentMethodID);
					break;
				case "TicketDeviceClassCollection":
					toReturn.Add(Relations.TicketDeviceClassPaymentMethodEntityUsingDevicePaymentMethodID, "DevicePaymentMethodEntity__", "TicketDeviceClassPaymentMethod_", JoinHint.None);
					toReturn.Add(TicketDeviceClassPaymentMethodEntity.Relations.TicketDeviceClassEntityUsingTicketDeviceClassID, "TicketDeviceClassPaymentMethod_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clearingResults", (!this.MarkedForDeletion?_clearingResults:null));
			info.AddValue("_alwaysFetchClearingResults", _alwaysFetchClearingResults);
			info.AddValue("_alreadyFetchedClearingResults", _alreadyFetchedClearingResults);
			info.AddValue("_ticketDeviceClassPaymentMethod", (!this.MarkedForDeletion?_ticketDeviceClassPaymentMethod:null));
			info.AddValue("_alwaysFetchTicketDeviceClassPaymentMethod", _alwaysFetchTicketDeviceClassPaymentMethod);
			info.AddValue("_alreadyFetchedTicketDeviceClassPaymentMethod", _alreadyFetchedTicketDeviceClassPaymentMethod);
			info.AddValue("_ticketDevicePaymentMethods", (!this.MarkedForDeletion?_ticketDevicePaymentMethods:null));
			info.AddValue("_alwaysFetchTicketDevicePaymentMethods", _alwaysFetchTicketDevicePaymentMethods);
			info.AddValue("_alreadyFetchedTicketDevicePaymentMethods", _alreadyFetchedTicketDevicePaymentMethods);
			info.AddValue("_contractToPaymentMethods", (!this.MarkedForDeletion?_contractToPaymentMethods:null));
			info.AddValue("_alwaysFetchContractToPaymentMethods", _alwaysFetchContractToPaymentMethods);
			info.AddValue("_alreadyFetchedContractToPaymentMethods", _alreadyFetchedContractToPaymentMethods);
			info.AddValue("_salesChannelToPaymentMethods", (!this.MarkedForDeletion?_salesChannelToPaymentMethods:null));
			info.AddValue("_alwaysFetchSalesChannelToPaymentMethods", _alwaysFetchSalesChannelToPaymentMethods);
			info.AddValue("_alreadyFetchedSalesChannelToPaymentMethods", _alreadyFetchedSalesChannelToPaymentMethods);
			info.AddValue("_ticketDeviceClassCollection", (!this.MarkedForDeletion?_ticketDeviceClassCollection:null));
			info.AddValue("_alwaysFetchTicketDeviceClassCollection", _alwaysFetchTicketDeviceClassCollection);
			info.AddValue("_alreadyFetchedTicketDeviceClassCollection", _alreadyFetchedTicketDeviceClassCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ClearingResults":
					_alreadyFetchedClearingResults = true;
					if(entity!=null)
					{
						this.ClearingResults.Add((ClearingResultEntity)entity);
					}
					break;
				case "TicketDeviceClassPaymentMethod":
					_alreadyFetchedTicketDeviceClassPaymentMethod = true;
					if(entity!=null)
					{
						this.TicketDeviceClassPaymentMethod.Add((TicketDeviceClassPaymentMethodEntity)entity);
					}
					break;
				case "TicketDevicePaymentMethods":
					_alreadyFetchedTicketDevicePaymentMethods = true;
					if(entity!=null)
					{
						this.TicketDevicePaymentMethods.Add((TicketDevicePaymentMethodEntity)entity);
					}
					break;
				case "ContractToPaymentMethods":
					_alreadyFetchedContractToPaymentMethods = true;
					if(entity!=null)
					{
						this.ContractToPaymentMethods.Add((ContractToPaymentMethodEntity)entity);
					}
					break;
				case "SalesChannelToPaymentMethods":
					_alreadyFetchedSalesChannelToPaymentMethods = true;
					if(entity!=null)
					{
						this.SalesChannelToPaymentMethods.Add((SalesChannelToPaymentMethodEntity)entity);
					}
					break;
				case "TicketDeviceClassCollection":
					_alreadyFetchedTicketDeviceClassCollection = true;
					if(entity!=null)
					{
						this.TicketDeviceClassCollection.Add((TicketDeviceClassEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ClearingResults":
					_clearingResults.Add((ClearingResultEntity)relatedEntity);
					break;
				case "TicketDeviceClassPaymentMethod":
					_ticketDeviceClassPaymentMethod.Add((TicketDeviceClassPaymentMethodEntity)relatedEntity);
					break;
				case "TicketDevicePaymentMethods":
					_ticketDevicePaymentMethods.Add((TicketDevicePaymentMethodEntity)relatedEntity);
					break;
				case "ContractToPaymentMethods":
					_contractToPaymentMethods.Add((ContractToPaymentMethodEntity)relatedEntity);
					break;
				case "SalesChannelToPaymentMethods":
					_salesChannelToPaymentMethods.Add((SalesChannelToPaymentMethodEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ClearingResults":
					this.PerformRelatedEntityRemoval(_clearingResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceClassPaymentMethod":
					this.PerformRelatedEntityRemoval(_ticketDeviceClassPaymentMethod, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDevicePaymentMethods":
					this.PerformRelatedEntityRemoval(_ticketDevicePaymentMethods, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ContractToPaymentMethods":
					this.PerformRelatedEntityRemoval(_contractToPaymentMethods, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SalesChannelToPaymentMethods":
					this.PerformRelatedEntityRemoval(_salesChannelToPaymentMethods, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_clearingResults);
			toReturn.Add(_ticketDeviceClassPaymentMethod);
			toReturn.Add(_ticketDevicePaymentMethods);
			toReturn.Add(_contractToPaymentMethods);
			toReturn.Add(_salesChannelToPaymentMethods);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="devicePaymentMethodID">PK value for DevicePaymentMethod which data should be fetched into this DevicePaymentMethod object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 devicePaymentMethodID)
		{
			return FetchUsingPK(devicePaymentMethodID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="devicePaymentMethodID">PK value for DevicePaymentMethod which data should be fetched into this DevicePaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 devicePaymentMethodID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(devicePaymentMethodID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="devicePaymentMethodID">PK value for DevicePaymentMethod which data should be fetched into this DevicePaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 devicePaymentMethodID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(devicePaymentMethodID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="devicePaymentMethodID">PK value for DevicePaymentMethod which data should be fetched into this DevicePaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 devicePaymentMethodID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(devicePaymentMethodID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DevicePaymentMethodID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DevicePaymentMethodRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch)
		{
			return GetMultiClearingResults(forceFetch, _clearingResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClearingResults(forceFetch, _clearingResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClearingResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClearingResults || forceFetch || _alwaysFetchClearingResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clearingResults);
				_clearingResults.SuppressClearInGetMulti=!forceFetch;
				_clearingResults.EntityFactoryToUse = entityFactoryToUse;
				_clearingResults.GetMultiManyToOne(null, null, null, null, null, null, this, null, filter);
				_clearingResults.SuppressClearInGetMulti=false;
				_alreadyFetchedClearingResults = true;
			}
			return _clearingResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClearingResults'. These settings will be taken into account
		/// when the property ClearingResults is requested or GetMultiClearingResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClearingResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clearingResults.SortClauses=sortClauses;
			_clearingResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassPaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection GetMultiTicketDeviceClassPaymentMethod(bool forceFetch)
		{
			return GetMultiTicketDeviceClassPaymentMethod(forceFetch, _ticketDeviceClassPaymentMethod.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassPaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection GetMultiTicketDeviceClassPaymentMethod(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceClassPaymentMethod(forceFetch, _ticketDeviceClassPaymentMethod.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection GetMultiTicketDeviceClassPaymentMethod(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceClassPaymentMethod(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection GetMultiTicketDeviceClassPaymentMethod(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceClassPaymentMethod || forceFetch || _alwaysFetchTicketDeviceClassPaymentMethod) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceClassPaymentMethod);
				_ticketDeviceClassPaymentMethod.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceClassPaymentMethod.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceClassPaymentMethod.GetMultiManyToOne(this, null, filter);
				_ticketDeviceClassPaymentMethod.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceClassPaymentMethod = true;
			}
			return _ticketDeviceClassPaymentMethod;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceClassPaymentMethod'. These settings will be taken into account
		/// when the property TicketDeviceClassPaymentMethod is requested or GetMultiTicketDeviceClassPaymentMethod is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceClassPaymentMethod(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceClassPaymentMethod.SortClauses=sortClauses;
			_ticketDeviceClassPaymentMethod.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDevicePaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDevicePaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection GetMultiTicketDevicePaymentMethods(bool forceFetch)
		{
			return GetMultiTicketDevicePaymentMethods(forceFetch, _ticketDevicePaymentMethods.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDevicePaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDevicePaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection GetMultiTicketDevicePaymentMethods(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDevicePaymentMethods(forceFetch, _ticketDevicePaymentMethods.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDevicePaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection GetMultiTicketDevicePaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDevicePaymentMethods(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDevicePaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection GetMultiTicketDevicePaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDevicePaymentMethods || forceFetch || _alwaysFetchTicketDevicePaymentMethods) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDevicePaymentMethods);
				_ticketDevicePaymentMethods.SuppressClearInGetMulti=!forceFetch;
				_ticketDevicePaymentMethods.EntityFactoryToUse = entityFactoryToUse;
				_ticketDevicePaymentMethods.GetMultiManyToOne(this, null, filter);
				_ticketDevicePaymentMethods.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDevicePaymentMethods = true;
			}
			return _ticketDevicePaymentMethods;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDevicePaymentMethods'. These settings will be taken into account
		/// when the property TicketDevicePaymentMethods is requested or GetMultiTicketDevicePaymentMethods is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDevicePaymentMethods(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDevicePaymentMethods.SortClauses=sortClauses;
			_ticketDevicePaymentMethods.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractToPaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection GetMultiContractToPaymentMethods(bool forceFetch)
		{
			return GetMultiContractToPaymentMethods(forceFetch, _contractToPaymentMethods.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractToPaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection GetMultiContractToPaymentMethods(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContractToPaymentMethods(forceFetch, _contractToPaymentMethods.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection GetMultiContractToPaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContractToPaymentMethods(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection GetMultiContractToPaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContractToPaymentMethods || forceFetch || _alwaysFetchContractToPaymentMethods) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contractToPaymentMethods);
				_contractToPaymentMethods.SuppressClearInGetMulti=!forceFetch;
				_contractToPaymentMethods.EntityFactoryToUse = entityFactoryToUse;
				_contractToPaymentMethods.GetMultiManyToOne(this, null, filter);
				_contractToPaymentMethods.SuppressClearInGetMulti=false;
				_alreadyFetchedContractToPaymentMethods = true;
			}
			return _contractToPaymentMethods;
		}

		/// <summary> Sets the collection parameters for the collection for 'ContractToPaymentMethods'. These settings will be taken into account
		/// when the property ContractToPaymentMethods is requested or GetMultiContractToPaymentMethods is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContractToPaymentMethods(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contractToPaymentMethods.SortClauses=sortClauses;
			_contractToPaymentMethods.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SalesChannelToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SalesChannelToPaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection GetMultiSalesChannelToPaymentMethods(bool forceFetch)
		{
			return GetMultiSalesChannelToPaymentMethods(forceFetch, _salesChannelToPaymentMethods.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SalesChannelToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SalesChannelToPaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection GetMultiSalesChannelToPaymentMethods(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSalesChannelToPaymentMethods(forceFetch, _salesChannelToPaymentMethods.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SalesChannelToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection GetMultiSalesChannelToPaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSalesChannelToPaymentMethods(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SalesChannelToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection GetMultiSalesChannelToPaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSalesChannelToPaymentMethods || forceFetch || _alwaysFetchSalesChannelToPaymentMethods) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_salesChannelToPaymentMethods);
				_salesChannelToPaymentMethods.SuppressClearInGetMulti=!forceFetch;
				_salesChannelToPaymentMethods.EntityFactoryToUse = entityFactoryToUse;
				_salesChannelToPaymentMethods.GetMultiManyToOne(null, this, filter);
				_salesChannelToPaymentMethods.SuppressClearInGetMulti=false;
				_alreadyFetchedSalesChannelToPaymentMethods = true;
			}
			return _salesChannelToPaymentMethods;
		}

		/// <summary> Sets the collection parameters for the collection for 'SalesChannelToPaymentMethods'. These settings will be taken into account
		/// when the property SalesChannelToPaymentMethods is requested or GetMultiSalesChannelToPaymentMethods is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSalesChannelToPaymentMethods(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_salesChannelToPaymentMethods.SortClauses=sortClauses;
			_salesChannelToPaymentMethods.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClassCollection(bool forceFetch)
		{
			return GetMultiTicketDeviceClassCollection(forceFetch, _ticketDeviceClassCollection.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClassCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTicketDeviceClassCollection || forceFetch || _alwaysFetchTicketDeviceClassCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceClassCollection);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DevicePaymentMethodFields.DevicePaymentMethodID, ComparisonOperator.Equal, this.DevicePaymentMethodID, "DevicePaymentMethodEntity__"));
				_ticketDeviceClassCollection.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceClassCollection.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceClassCollection.GetMulti(filter, GetRelationsForField("TicketDeviceClassCollection"));
				_ticketDeviceClassCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceClassCollection = true;
			}
			return _ticketDeviceClassCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceClassCollection'. These settings will be taken into account
		/// when the property TicketDeviceClassCollection is requested or GetMultiTicketDeviceClassCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceClassCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceClassCollection.SortClauses=sortClauses;
			_ticketDeviceClassCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ClearingResults", _clearingResults);
			toReturn.Add("TicketDeviceClassPaymentMethod", _ticketDeviceClassPaymentMethod);
			toReturn.Add("TicketDevicePaymentMethods", _ticketDevicePaymentMethods);
			toReturn.Add("ContractToPaymentMethods", _contractToPaymentMethods);
			toReturn.Add("SalesChannelToPaymentMethods", _salesChannelToPaymentMethods);
			toReturn.Add("TicketDeviceClassCollection", _ticketDeviceClassCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="devicePaymentMethodID">PK value for DevicePaymentMethod which data should be fetched into this DevicePaymentMethod object</param>
		/// <param name="validator">The validator object for this DevicePaymentMethodEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 devicePaymentMethodID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(devicePaymentMethodID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_clearingResults = new VarioSL.Entities.CollectionClasses.ClearingResultCollection();
			_clearingResults.SetContainingEntityInfo(this, "DevicePaymentMethod");

			_ticketDeviceClassPaymentMethod = new VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection();
			_ticketDeviceClassPaymentMethod.SetContainingEntityInfo(this, "DevicePaymentMethod");

			_ticketDevicePaymentMethods = new VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection();
			_ticketDevicePaymentMethods.SetContainingEntityInfo(this, "DevicePaymentMethod");

			_contractToPaymentMethods = new VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection();
			_contractToPaymentMethods.SetContainingEntityInfo(this, "DevicePaymentMethod");

			_salesChannelToPaymentMethods = new VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection();
			_salesChannelToPaymentMethods.SetContainingEntityInfo(this, "DevicePaymentMethod");
			_ticketDeviceClassCollection = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DevicePaymentMethodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EvendMask", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="devicePaymentMethodID">PK value for DevicePaymentMethod which data should be fetched into this DevicePaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 devicePaymentMethodID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DevicePaymentMethodFieldIndex.DevicePaymentMethodID].ForcedCurrentValueWrite(devicePaymentMethodID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDevicePaymentMethodDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DevicePaymentMethodEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DevicePaymentMethodRelations Relations
		{
			get	{ return new DevicePaymentMethodRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClearingResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClearingResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClearingResultCollection(), (IEntityRelation)GetRelationsForField("ClearingResults")[0], (int)VarioSL.Entities.EntityType.DevicePaymentMethodEntity, (int)VarioSL.Entities.EntityType.ClearingResultEntity, 0, null, null, null, "ClearingResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClassPaymentMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceClassPaymentMethod
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceClassPaymentMethod")[0], (int)VarioSL.Entities.EntityType.DevicePaymentMethodEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassPaymentMethodEntity, 0, null, null, null, "TicketDeviceClassPaymentMethod", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDevicePaymentMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDevicePaymentMethods
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection(), (IEntityRelation)GetRelationsForField("TicketDevicePaymentMethods")[0], (int)VarioSL.Entities.EntityType.DevicePaymentMethodEntity, (int)VarioSL.Entities.EntityType.TicketDevicePaymentMethodEntity, 0, null, null, null, "TicketDevicePaymentMethods", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractToPaymentMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContractToPaymentMethods
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection(), (IEntityRelation)GetRelationsForField("ContractToPaymentMethods")[0], (int)VarioSL.Entities.EntityType.DevicePaymentMethodEntity, (int)VarioSL.Entities.EntityType.ContractToPaymentMethodEntity, 0, null, null, null, "ContractToPaymentMethods", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SalesChannelToPaymentMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSalesChannelToPaymentMethods
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection(), (IEntityRelation)GetRelationsForField("SalesChannelToPaymentMethods")[0], (int)VarioSL.Entities.EntityType.DevicePaymentMethodEntity, (int)VarioSL.Entities.EntityType.SalesChannelToPaymentMethodEntity, 0, null, null, null, "SalesChannelToPaymentMethods", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceClassCollection
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketDeviceClassPaymentMethodEntityUsingDevicePaymentMethodID;
				intermediateRelation.SetAliases(string.Empty, "TicketDeviceClassPaymentMethod_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.DevicePaymentMethodEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, GetRelationsForField("TicketDeviceClassCollection"), "TicketDeviceClassCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity DevicePaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_DEVICEPAYMENTMETHOD"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)DevicePaymentMethodFieldIndex.Description, true); }
			set	{ SetValue((int)DevicePaymentMethodFieldIndex.Description, value, true); }
		}

		/// <summary> The DevicePaymentMethodID property of the Entity DevicePaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_DEVICEPAYMENTMETHOD"."DEVICEPAYMENTMETHODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 DevicePaymentMethodID
		{
			get { return (System.Int64)GetValue((int)DevicePaymentMethodFieldIndex.DevicePaymentMethodID, true); }
			set	{ SetValue((int)DevicePaymentMethodFieldIndex.DevicePaymentMethodID, value, true); }
		}

		/// <summary> The DisplayText property of the Entity DevicePaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_DEVICEPAYMENTMETHOD"."DISPLAYTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DisplayText
		{
			get { return (System.String)GetValue((int)DevicePaymentMethodFieldIndex.DisplayText, true); }
			set	{ SetValue((int)DevicePaymentMethodFieldIndex.DisplayText, value, true); }
		}

		/// <summary> The EvendMask property of the Entity DevicePaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_DEVICEPAYMENTMETHOD"."EVENDMASK"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EvendMask
		{
			get { return (Nullable<System.Int64>)GetValue((int)DevicePaymentMethodFieldIndex.EvendMask, false); }
			set	{ SetValue((int)DevicePaymentMethodFieldIndex.EvendMask, value, true); }
		}

		/// <summary> The ExternalNumber property of the Entity DevicePaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_DEVICEPAYMENTMETHOD"."EXTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ExternalNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)DevicePaymentMethodFieldIndex.ExternalNumber, false); }
			set	{ SetValue((int)DevicePaymentMethodFieldIndex.ExternalNumber, value, true); }
		}

		/// <summary> The Visible property of the Entity DevicePaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_DEVICEPAYMENTMETHOD"."VISIBLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Visible
		{
			get { return (Nullable<System.Boolean>)GetValue((int)DevicePaymentMethodFieldIndex.Visible, false); }
			set	{ SetValue((int)DevicePaymentMethodFieldIndex.Visible, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClearingResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection ClearingResults
		{
			get	{ return GetMultiClearingResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClearingResults. When set to true, ClearingResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClearingResults is accessed. You can always execute/ a forced fetch by calling GetMultiClearingResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClearingResults
		{
			get	{ return _alwaysFetchClearingResults; }
			set	{ _alwaysFetchClearingResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClearingResults already has been fetched. Setting this property to false when ClearingResults has been fetched
		/// will clear the ClearingResults collection well. Setting this property to true while ClearingResults hasn't been fetched disables lazy loading for ClearingResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClearingResults
		{
			get { return _alreadyFetchedClearingResults;}
			set 
			{
				if(_alreadyFetchedClearingResults && !value && (_clearingResults != null))
				{
					_clearingResults.Clear();
				}
				_alreadyFetchedClearingResults = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassPaymentMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceClassPaymentMethod()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection TicketDeviceClassPaymentMethod
		{
			get	{ return GetMultiTicketDeviceClassPaymentMethod(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceClassPaymentMethod. When set to true, TicketDeviceClassPaymentMethod is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceClassPaymentMethod is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceClassPaymentMethod(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceClassPaymentMethod
		{
			get	{ return _alwaysFetchTicketDeviceClassPaymentMethod; }
			set	{ _alwaysFetchTicketDeviceClassPaymentMethod = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceClassPaymentMethod already has been fetched. Setting this property to false when TicketDeviceClassPaymentMethod has been fetched
		/// will clear the TicketDeviceClassPaymentMethod collection well. Setting this property to true while TicketDeviceClassPaymentMethod hasn't been fetched disables lazy loading for TicketDeviceClassPaymentMethod</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceClassPaymentMethod
		{
			get { return _alreadyFetchedTicketDeviceClassPaymentMethod;}
			set 
			{
				if(_alreadyFetchedTicketDeviceClassPaymentMethod && !value && (_ticketDeviceClassPaymentMethod != null))
				{
					_ticketDeviceClassPaymentMethod.Clear();
				}
				_alreadyFetchedTicketDeviceClassPaymentMethod = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDevicePaymentMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDevicePaymentMethods()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection TicketDevicePaymentMethods
		{
			get	{ return GetMultiTicketDevicePaymentMethods(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDevicePaymentMethods. When set to true, TicketDevicePaymentMethods is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDevicePaymentMethods is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDevicePaymentMethods(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDevicePaymentMethods
		{
			get	{ return _alwaysFetchTicketDevicePaymentMethods; }
			set	{ _alwaysFetchTicketDevicePaymentMethods = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDevicePaymentMethods already has been fetched. Setting this property to false when TicketDevicePaymentMethods has been fetched
		/// will clear the TicketDevicePaymentMethods collection well. Setting this property to true while TicketDevicePaymentMethods hasn't been fetched disables lazy loading for TicketDevicePaymentMethods</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDevicePaymentMethods
		{
			get { return _alreadyFetchedTicketDevicePaymentMethods;}
			set 
			{
				if(_alreadyFetchedTicketDevicePaymentMethods && !value && (_ticketDevicePaymentMethods != null))
				{
					_ticketDevicePaymentMethods.Clear();
				}
				_alreadyFetchedTicketDevicePaymentMethods = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractToPaymentMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContractToPaymentMethods()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection ContractToPaymentMethods
		{
			get	{ return GetMultiContractToPaymentMethods(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ContractToPaymentMethods. When set to true, ContractToPaymentMethods is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContractToPaymentMethods is accessed. You can always execute/ a forced fetch by calling GetMultiContractToPaymentMethods(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContractToPaymentMethods
		{
			get	{ return _alwaysFetchContractToPaymentMethods; }
			set	{ _alwaysFetchContractToPaymentMethods = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ContractToPaymentMethods already has been fetched. Setting this property to false when ContractToPaymentMethods has been fetched
		/// will clear the ContractToPaymentMethods collection well. Setting this property to true while ContractToPaymentMethods hasn't been fetched disables lazy loading for ContractToPaymentMethods</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContractToPaymentMethods
		{
			get { return _alreadyFetchedContractToPaymentMethods;}
			set 
			{
				if(_alreadyFetchedContractToPaymentMethods && !value && (_contractToPaymentMethods != null))
				{
					_contractToPaymentMethods.Clear();
				}
				_alreadyFetchedContractToPaymentMethods = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SalesChannelToPaymentMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSalesChannelToPaymentMethods()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection SalesChannelToPaymentMethods
		{
			get	{ return GetMultiSalesChannelToPaymentMethods(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SalesChannelToPaymentMethods. When set to true, SalesChannelToPaymentMethods is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SalesChannelToPaymentMethods is accessed. You can always execute/ a forced fetch by calling GetMultiSalesChannelToPaymentMethods(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSalesChannelToPaymentMethods
		{
			get	{ return _alwaysFetchSalesChannelToPaymentMethods; }
			set	{ _alwaysFetchSalesChannelToPaymentMethods = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SalesChannelToPaymentMethods already has been fetched. Setting this property to false when SalesChannelToPaymentMethods has been fetched
		/// will clear the SalesChannelToPaymentMethods collection well. Setting this property to true while SalesChannelToPaymentMethods hasn't been fetched disables lazy loading for SalesChannelToPaymentMethods</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSalesChannelToPaymentMethods
		{
			get { return _alreadyFetchedSalesChannelToPaymentMethods;}
			set 
			{
				if(_alreadyFetchedSalesChannelToPaymentMethods && !value && (_salesChannelToPaymentMethods != null))
				{
					_salesChannelToPaymentMethods.Clear();
				}
				_alreadyFetchedSalesChannelToPaymentMethods = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceClassCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceClassCollection
		{
			get { return GetMultiTicketDeviceClassCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceClassCollection. When set to true, TicketDeviceClassCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceClassCollection is accessed. You can always execute a forced fetch by calling GetMultiTicketDeviceClassCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceClassCollection
		{
			get	{ return _alwaysFetchTicketDeviceClassCollection; }
			set	{ _alwaysFetchTicketDeviceClassCollection = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceClassCollection already has been fetched. Setting this property to false when TicketDeviceClassCollection has been fetched
		/// will clear the TicketDeviceClassCollection collection well. Setting this property to true while TicketDeviceClassCollection hasn't been fetched disables lazy loading for TicketDeviceClassCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceClassCollection
		{
			get { return _alreadyFetchedTicketDeviceClassCollection;}
			set 
			{
				if(_alreadyFetchedTicketDeviceClassCollection && !value && (_ticketDeviceClassCollection != null))
				{
					_ticketDeviceClassCollection.Clear();
				}
				_alreadyFetchedTicketDeviceClassCollection = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DevicePaymentMethodEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
