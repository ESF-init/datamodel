﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'BankConnectionData'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class BankConnectionDataEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CreditScreeningCollection	_creditScreenings;
		private bool	_alwaysFetchCreditScreenings, _alreadyFetchedCreditScreenings;
		private VarioSL.Entities.CollectionClasses.AddressBookEntryCollection	_addressBookEntries;
		private bool	_alwaysFetchAddressBookEntries, _alreadyFetchedAddressBookEntries;
		private PersonEntity _accountOwner;
		private bool	_alwaysFetchAccountOwner, _alreadyFetchedAccountOwner, _accountOwnerReturnsNewIfNotFound;
		private PaymentOptionEntity _paymentOption;
		private bool	_alwaysFetchPaymentOption, _alreadyFetchedPaymentOption, _paymentOptionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AccountOwner</summary>
			public static readonly string AccountOwner = "AccountOwner";
			/// <summary>Member name CreditScreenings</summary>
			public static readonly string CreditScreenings = "CreditScreenings";
			/// <summary>Member name AddressBookEntries</summary>
			public static readonly string AddressBookEntries = "AddressBookEntries";
			/// <summary>Member name PaymentOption</summary>
			public static readonly string PaymentOption = "PaymentOption";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static BankConnectionDataEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public BankConnectionDataEntity() :base("BankConnectionDataEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="bankConnectionDataID">PK value for BankConnectionData which data should be fetched into this BankConnectionData object</param>
		public BankConnectionDataEntity(System.Int64 bankConnectionDataID):base("BankConnectionDataEntity")
		{
			InitClassFetch(bankConnectionDataID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="bankConnectionDataID">PK value for BankConnectionData which data should be fetched into this BankConnectionData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public BankConnectionDataEntity(System.Int64 bankConnectionDataID, IPrefetchPath prefetchPathToUse):base("BankConnectionDataEntity")
		{
			InitClassFetch(bankConnectionDataID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="bankConnectionDataID">PK value for BankConnectionData which data should be fetched into this BankConnectionData object</param>
		/// <param name="validator">The custom validator object for this BankConnectionDataEntity</param>
		public BankConnectionDataEntity(System.Int64 bankConnectionDataID, IValidator validator):base("BankConnectionDataEntity")
		{
			InitClassFetch(bankConnectionDataID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BankConnectionDataEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_creditScreenings = (VarioSL.Entities.CollectionClasses.CreditScreeningCollection)info.GetValue("_creditScreenings", typeof(VarioSL.Entities.CollectionClasses.CreditScreeningCollection));
			_alwaysFetchCreditScreenings = info.GetBoolean("_alwaysFetchCreditScreenings");
			_alreadyFetchedCreditScreenings = info.GetBoolean("_alreadyFetchedCreditScreenings");

			_addressBookEntries = (VarioSL.Entities.CollectionClasses.AddressBookEntryCollection)info.GetValue("_addressBookEntries", typeof(VarioSL.Entities.CollectionClasses.AddressBookEntryCollection));
			_alwaysFetchAddressBookEntries = info.GetBoolean("_alwaysFetchAddressBookEntries");
			_alreadyFetchedAddressBookEntries = info.GetBoolean("_alreadyFetchedAddressBookEntries");
			_accountOwner = (PersonEntity)info.GetValue("_accountOwner", typeof(PersonEntity));
			if(_accountOwner!=null)
			{
				_accountOwner.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_accountOwnerReturnsNewIfNotFound = info.GetBoolean("_accountOwnerReturnsNewIfNotFound");
			_alwaysFetchAccountOwner = info.GetBoolean("_alwaysFetchAccountOwner");
			_alreadyFetchedAccountOwner = info.GetBoolean("_alreadyFetchedAccountOwner");
			_paymentOption = (PaymentOptionEntity)info.GetValue("_paymentOption", typeof(PaymentOptionEntity));
			if(_paymentOption!=null)
			{
				_paymentOption.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentOptionReturnsNewIfNotFound = info.GetBoolean("_paymentOptionReturnsNewIfNotFound");
			_alwaysFetchPaymentOption = info.GetBoolean("_alwaysFetchPaymentOption");
			_alreadyFetchedPaymentOption = info.GetBoolean("_alreadyFetchedPaymentOption");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((BankConnectionDataFieldIndex)fieldIndex)
			{
				case BankConnectionDataFieldIndex.AccountOwnerID:
					DesetupSyncAccountOwner(true, false);
					_alreadyFetchedAccountOwner = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCreditScreenings = (_creditScreenings.Count > 0);
			_alreadyFetchedAddressBookEntries = (_addressBookEntries.Count > 0);
			_alreadyFetchedAccountOwner = (_accountOwner != null);
			_alreadyFetchedPaymentOption = (_paymentOption != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AccountOwner":
					toReturn.Add(Relations.PersonEntityUsingAccountOwnerID);
					break;
				case "CreditScreenings":
					toReturn.Add(Relations.CreditScreeningEntityUsingBankconnectionDataID);
					break;
				case "AddressBookEntries":
					toReturn.Add(Relations.AddressBookEntryEntityUsingBankConnectionDataID);
					break;
				case "PaymentOption":
					toReturn.Add(Relations.PaymentOptionEntityUsingBankConnectionDataID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_creditScreenings", (!this.MarkedForDeletion?_creditScreenings:null));
			info.AddValue("_alwaysFetchCreditScreenings", _alwaysFetchCreditScreenings);
			info.AddValue("_alreadyFetchedCreditScreenings", _alreadyFetchedCreditScreenings);
			info.AddValue("_addressBookEntries", (!this.MarkedForDeletion?_addressBookEntries:null));
			info.AddValue("_alwaysFetchAddressBookEntries", _alwaysFetchAddressBookEntries);
			info.AddValue("_alreadyFetchedAddressBookEntries", _alreadyFetchedAddressBookEntries);
			info.AddValue("_accountOwner", (!this.MarkedForDeletion?_accountOwner:null));
			info.AddValue("_accountOwnerReturnsNewIfNotFound", _accountOwnerReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAccountOwner", _alwaysFetchAccountOwner);
			info.AddValue("_alreadyFetchedAccountOwner", _alreadyFetchedAccountOwner);

			info.AddValue("_paymentOption", (!this.MarkedForDeletion?_paymentOption:null));
			info.AddValue("_paymentOptionReturnsNewIfNotFound", _paymentOptionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentOption", _alwaysFetchPaymentOption);
			info.AddValue("_alreadyFetchedPaymentOption", _alreadyFetchedPaymentOption);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AccountOwner":
					_alreadyFetchedAccountOwner = true;
					this.AccountOwner = (PersonEntity)entity;
					break;
				case "CreditScreenings":
					_alreadyFetchedCreditScreenings = true;
					if(entity!=null)
					{
						this.CreditScreenings.Add((CreditScreeningEntity)entity);
					}
					break;
				case "AddressBookEntries":
					_alreadyFetchedAddressBookEntries = true;
					if(entity!=null)
					{
						this.AddressBookEntries.Add((AddressBookEntryEntity)entity);
					}
					break;
				case "PaymentOption":
					_alreadyFetchedPaymentOption = true;
					this.PaymentOption = (PaymentOptionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AccountOwner":
					SetupSyncAccountOwner(relatedEntity);
					break;
				case "CreditScreenings":
					_creditScreenings.Add((CreditScreeningEntity)relatedEntity);
					break;
				case "AddressBookEntries":
					_addressBookEntries.Add((AddressBookEntryEntity)relatedEntity);
					break;
				case "PaymentOption":
					SetupSyncPaymentOption(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AccountOwner":
					DesetupSyncAccountOwner(false, true);
					break;
				case "CreditScreenings":
					this.PerformRelatedEntityRemoval(_creditScreenings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AddressBookEntries":
					this.PerformRelatedEntityRemoval(_addressBookEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentOption":
					DesetupSyncPaymentOption(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_paymentOption!=null)
			{
				toReturn.Add(_paymentOption);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_accountOwner!=null)
			{
				toReturn.Add(_accountOwner);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_creditScreenings);
			toReturn.Add(_addressBookEntries);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bankConnectionDataID">PK value for BankConnectionData which data should be fetched into this BankConnectionData object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bankConnectionDataID)
		{
			return FetchUsingPK(bankConnectionDataID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bankConnectionDataID">PK value for BankConnectionData which data should be fetched into this BankConnectionData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bankConnectionDataID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(bankConnectionDataID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bankConnectionDataID">PK value for BankConnectionData which data should be fetched into this BankConnectionData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bankConnectionDataID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(bankConnectionDataID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bankConnectionDataID">PK value for BankConnectionData which data should be fetched into this BankConnectionData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bankConnectionDataID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(bankConnectionDataID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.BankConnectionDataID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new BankConnectionDataRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CreditScreeningEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CreditScreeningEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CreditScreeningCollection GetMultiCreditScreenings(bool forceFetch)
		{
			return GetMultiCreditScreenings(forceFetch, _creditScreenings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CreditScreeningEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CreditScreeningEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CreditScreeningCollection GetMultiCreditScreenings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCreditScreenings(forceFetch, _creditScreenings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CreditScreeningEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CreditScreeningCollection GetMultiCreditScreenings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCreditScreenings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CreditScreeningEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CreditScreeningCollection GetMultiCreditScreenings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCreditScreenings || forceFetch || _alwaysFetchCreditScreenings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_creditScreenings);
				_creditScreenings.SuppressClearInGetMulti=!forceFetch;
				_creditScreenings.EntityFactoryToUse = entityFactoryToUse;
				_creditScreenings.GetMultiManyToOne(this, filter);
				_creditScreenings.SuppressClearInGetMulti=false;
				_alreadyFetchedCreditScreenings = true;
			}
			return _creditScreenings;
		}

		/// <summary> Sets the collection parameters for the collection for 'CreditScreenings'. These settings will be taken into account
		/// when the property CreditScreenings is requested or GetMultiCreditScreenings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCreditScreenings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_creditScreenings.SortClauses=sortClauses;
			_creditScreenings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AddressBookEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AddressBookEntryCollection GetMultiAddressBookEntries(bool forceFetch)
		{
			return GetMultiAddressBookEntries(forceFetch, _addressBookEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AddressBookEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AddressBookEntryCollection GetMultiAddressBookEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAddressBookEntries(forceFetch, _addressBookEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AddressBookEntryCollection GetMultiAddressBookEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAddressBookEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AddressBookEntryCollection GetMultiAddressBookEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAddressBookEntries || forceFetch || _alwaysFetchAddressBookEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_addressBookEntries);
				_addressBookEntries.SuppressClearInGetMulti=!forceFetch;
				_addressBookEntries.EntityFactoryToUse = entityFactoryToUse;
				_addressBookEntries.GetMultiManyToOne(null, null, this, filter);
				_addressBookEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedAddressBookEntries = true;
			}
			return _addressBookEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'AddressBookEntries'. These settings will be taken into account
		/// when the property AddressBookEntries is requested or GetMultiAddressBookEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAddressBookEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_addressBookEntries.SortClauses=sortClauses;
			_addressBookEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSingleAccountOwner()
		{
			return GetSingleAccountOwner(false);
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSingleAccountOwner(bool forceFetch)
		{
			if( ( !_alreadyFetchedAccountOwner || forceFetch || _alwaysFetchAccountOwner) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingAccountOwnerID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphic(this.Transaction, this.AccountOwnerID.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_accountOwnerReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AccountOwner = newEntity;
				_alreadyFetchedAccountOwner = fetchResult;
			}
			return _accountOwner;
		}

		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public PaymentOptionEntity GetSinglePaymentOption()
		{
			return GetSinglePaymentOption(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public virtual PaymentOptionEntity GetSinglePaymentOption(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentOption || forceFetch || _alwaysFetchPaymentOption) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentOptionEntityUsingBankConnectionDataID);
				PaymentOptionEntity newEntity = new PaymentOptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCBankConnectionDataID(this.BankConnectionDataID);
				}
				if(fetchResult)
				{
					newEntity = (PaymentOptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentOptionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentOption = newEntity;
				_alreadyFetchedPaymentOption = fetchResult;
			}
			return _paymentOption;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AccountOwner", _accountOwner);
			toReturn.Add("CreditScreenings", _creditScreenings);
			toReturn.Add("AddressBookEntries", _addressBookEntries);
			toReturn.Add("PaymentOption", _paymentOption);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="bankConnectionDataID">PK value for BankConnectionData which data should be fetched into this BankConnectionData object</param>
		/// <param name="validator">The validator object for this BankConnectionDataEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 bankConnectionDataID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(bankConnectionDataID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_creditScreenings = new VarioSL.Entities.CollectionClasses.CreditScreeningCollection();
			_creditScreenings.SetContainingEntityInfo(this, "BankConnectionData");

			_addressBookEntries = new VarioSL.Entities.CollectionClasses.AddressBookEntryCollection();
			_addressBookEntries.SetContainingEntityInfo(this, "BankConnectionData");
			_accountOwnerReturnsNewIfNotFound = false;
			_paymentOptionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountOwnerID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankConnectionDataID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Bic", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FrequencyType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Iban", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUsage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MandateReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OwnerType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidUntil", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _accountOwner</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAccountOwner(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _accountOwner, new PropertyChangedEventHandler( OnAccountOwnerPropertyChanged ), "AccountOwner", VarioSL.Entities.RelationClasses.StaticBankConnectionDataRelations.PersonEntityUsingAccountOwnerIDStatic, true, signalRelatedEntity, "BankConnectionDatas", resetFKFields, new int[] { (int)BankConnectionDataFieldIndex.AccountOwnerID } );		
			_accountOwner = null;
		}
		
		/// <summary> setups the sync logic for member _accountOwner</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAccountOwner(IEntityCore relatedEntity)
		{
			if(_accountOwner!=relatedEntity)
			{		
				DesetupSyncAccountOwner(true, true);
				_accountOwner = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _accountOwner, new PropertyChangedEventHandler( OnAccountOwnerPropertyChanged ), "AccountOwner", VarioSL.Entities.RelationClasses.StaticBankConnectionDataRelations.PersonEntityUsingAccountOwnerIDStatic, true, ref _alreadyFetchedAccountOwner, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAccountOwnerPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentOption</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentOption(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticBankConnectionDataRelations.PaymentOptionEntityUsingBankConnectionDataIDStatic, false, signalRelatedEntity, "BankConnectionData", false, new int[] { (int)BankConnectionDataFieldIndex.BankConnectionDataID } );
			_paymentOption = null;
		}
	
		/// <summary> setups the sync logic for member _paymentOption</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentOption(IEntityCore relatedEntity)
		{
			if(_paymentOption!=relatedEntity)
			{
				DesetupSyncPaymentOption(true, true);
				_paymentOption = (PaymentOptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticBankConnectionDataRelations.PaymentOptionEntityUsingBankConnectionDataIDStatic, false, ref _alreadyFetchedPaymentOption, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentOptionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="bankConnectionDataID">PK value for BankConnectionData which data should be fetched into this BankConnectionData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 bankConnectionDataID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)BankConnectionDataFieldIndex.BankConnectionDataID].ForcedCurrentValueWrite(bankConnectionDataID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateBankConnectionDataDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new BankConnectionDataEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static BankConnectionDataRelations Relations
		{
			get	{ return new BankConnectionDataRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CreditScreening' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCreditScreenings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CreditScreeningCollection(), (IEntityRelation)GetRelationsForField("CreditScreenings")[0], (int)VarioSL.Entities.EntityType.BankConnectionDataEntity, (int)VarioSL.Entities.EntityType.CreditScreeningEntity, 0, null, null, null, "CreditScreenings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AddressBookEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAddressBookEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressBookEntryCollection(), (IEntityRelation)GetRelationsForField("AddressBookEntries")[0], (int)VarioSL.Entities.EntityType.BankConnectionDataEntity, (int)VarioSL.Entities.EntityType.AddressBookEntryEntity, 0, null, null, null, "AddressBookEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccountOwner
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("AccountOwner")[0], (int)VarioSL.Entities.EntityType.BankConnectionDataEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "AccountOwner", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentOption
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("PaymentOption")[0], (int)VarioSL.Entities.EntityType.BankConnectionDataEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "PaymentOption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccountOwnerID property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."ACCOUNTOWNERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AccountOwnerID
		{
			get { return (Nullable<System.Int64>)GetValue((int)BankConnectionDataFieldIndex.AccountOwnerID, false); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.AccountOwnerID, value, true); }
		}

		/// <summary> The BankConnectionDataID property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."BANKCONNECTIONDATAID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 BankConnectionDataID
		{
			get { return (System.Int64)GetValue((int)BankConnectionDataFieldIndex.BankConnectionDataID, true); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.BankConnectionDataID, value, true); }
		}

		/// <summary> The Bic property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."BIC"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 11<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Bic
		{
			get { return (System.String)GetValue((int)BankConnectionDataFieldIndex.Bic, true); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.Bic, value, true); }
		}

		/// <summary> The CreationDate property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."CREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreationDate
		{
			get { return (System.DateTime)GetValue((int)BankConnectionDataFieldIndex.CreationDate, true); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.CreationDate, value, true); }
		}

		/// <summary> The Description property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)BankConnectionDataFieldIndex.Description, true); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.Description, value, true); }
		}

		/// <summary> The FrequencyType property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."FREQUENCYTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.SepaFrequencyType FrequencyType
		{
			get { return (VarioSL.Entities.Enumerations.SepaFrequencyType)GetValue((int)BankConnectionDataFieldIndex.FrequencyType, true); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.FrequencyType, value, true); }
		}

		/// <summary> The Iban property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."IBAN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 34<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Iban
		{
			get { return (System.String)GetValue((int)BankConnectionDataFieldIndex.Iban, true); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.Iban, value, true); }
		}

		/// <summary> The LastModified property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)BankConnectionDataFieldIndex.LastModified, true); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUsage property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."LASTUSAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastUsage
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BankConnectionDataFieldIndex.LastUsage, false); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.LastUsage, value, true); }
		}

		/// <summary> The LastUser property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)BankConnectionDataFieldIndex.LastUser, true); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MandateReference property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."MANDATEREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 35<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String MandateReference
		{
			get { return (System.String)GetValue((int)BankConnectionDataFieldIndex.MandateReference, true); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.MandateReference, value, true); }
		}

		/// <summary> The OwnerType property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."OWNERTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.SepaOwnerType OwnerType
		{
			get { return (VarioSL.Entities.Enumerations.SepaOwnerType)GetValue((int)BankConnectionDataFieldIndex.OwnerType, true); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.OwnerType, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)BankConnectionDataFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BankConnectionDataFieldIndex.ValidFrom, false); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidUntil property of the Entity BankConnectionData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BANKCONNECTIONDATA"."VALIDUNTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidUntil
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BankConnectionDataFieldIndex.ValidUntil, false); }
			set	{ SetValue((int)BankConnectionDataFieldIndex.ValidUntil, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CreditScreeningEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCreditScreenings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CreditScreeningCollection CreditScreenings
		{
			get	{ return GetMultiCreditScreenings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CreditScreenings. When set to true, CreditScreenings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CreditScreenings is accessed. You can always execute/ a forced fetch by calling GetMultiCreditScreenings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCreditScreenings
		{
			get	{ return _alwaysFetchCreditScreenings; }
			set	{ _alwaysFetchCreditScreenings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CreditScreenings already has been fetched. Setting this property to false when CreditScreenings has been fetched
		/// will clear the CreditScreenings collection well. Setting this property to true while CreditScreenings hasn't been fetched disables lazy loading for CreditScreenings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCreditScreenings
		{
			get { return _alreadyFetchedCreditScreenings;}
			set 
			{
				if(_alreadyFetchedCreditScreenings && !value && (_creditScreenings != null))
				{
					_creditScreenings.Clear();
				}
				_alreadyFetchedCreditScreenings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAddressBookEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AddressBookEntryCollection AddressBookEntries
		{
			get	{ return GetMultiAddressBookEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AddressBookEntries. When set to true, AddressBookEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AddressBookEntries is accessed. You can always execute/ a forced fetch by calling GetMultiAddressBookEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAddressBookEntries
		{
			get	{ return _alwaysFetchAddressBookEntries; }
			set	{ _alwaysFetchAddressBookEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AddressBookEntries already has been fetched. Setting this property to false when AddressBookEntries has been fetched
		/// will clear the AddressBookEntries collection well. Setting this property to true while AddressBookEntries hasn't been fetched disables lazy loading for AddressBookEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAddressBookEntries
		{
			get { return _alreadyFetchedAddressBookEntries;}
			set 
			{
				if(_alreadyFetchedAddressBookEntries && !value && (_addressBookEntries != null))
				{
					_addressBookEntries.Clear();
				}
				_alreadyFetchedAddressBookEntries = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAccountOwner()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity AccountOwner
		{
			get	{ return GetSingleAccountOwner(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAccountOwner(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BankConnectionDatas", "AccountOwner", _accountOwner, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AccountOwner. When set to true, AccountOwner is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccountOwner is accessed. You can always execute a forced fetch by calling GetSingleAccountOwner(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccountOwner
		{
			get	{ return _alwaysFetchAccountOwner; }
			set	{ _alwaysFetchAccountOwner = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AccountOwner already has been fetched. Setting this property to false when AccountOwner has been fetched
		/// will set AccountOwner to null as well. Setting this property to true while AccountOwner hasn't been fetched disables lazy loading for AccountOwner</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccountOwner
		{
			get { return _alreadyFetchedAccountOwner;}
			set 
			{
				if(_alreadyFetchedAccountOwner && !value)
				{
					this.AccountOwner = null;
				}
				_alreadyFetchedAccountOwner = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AccountOwner is not found
		/// in the database. When set to true, AccountOwner will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AccountOwnerReturnsNewIfNotFound
		{
			get	{ return _accountOwnerReturnsNewIfNotFound; }
			set { _accountOwnerReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentOptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentOption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentOptionEntity PaymentOption
		{
			get	{ return GetSinglePaymentOption(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPaymentOption(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_paymentOption !=null);
						DesetupSyncPaymentOption(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("PaymentOption");
						}
					}
					else
					{
						if(_paymentOption!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "BankConnectionData");
							SetupSyncPaymentOption(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentOption. When set to true, PaymentOption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentOption is accessed. You can always execute a forced fetch by calling GetSinglePaymentOption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentOption
		{
			get	{ return _alwaysFetchPaymentOption; }
			set	{ _alwaysFetchPaymentOption = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentOption already has been fetched. Setting this property to false when PaymentOption has been fetched
		/// will set PaymentOption to null as well. Setting this property to true while PaymentOption hasn't been fetched disables lazy loading for PaymentOption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentOption
		{
			get { return _alreadyFetchedPaymentOption;}
			set 
			{
				if(_alreadyFetchedPaymentOption && !value)
				{
					this.PaymentOption = null;
				}
				_alreadyFetchedPaymentOption = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentOption is not found
		/// in the database. When set to true, PaymentOption will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentOptionReturnsNewIfNotFound
		{
			get	{ return _paymentOptionReturnsNewIfNotFound; }
			set	{ _paymentOptionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.BankConnectionDataEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
