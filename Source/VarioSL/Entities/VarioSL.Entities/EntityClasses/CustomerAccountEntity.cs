﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CustomerAccount'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CustomerAccountEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection	_accountMessageSettings;
		private bool	_alwaysFetchAccountMessageSettings, _alreadyFetchedAccountMessageSettings;
		private VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection	_customerAccountNotifications;
		private bool	_alwaysFetchCustomerAccountNotifications, _alreadyFetchedCustomerAccountNotifications;
		private VarioSL.Entities.CollectionClasses.CustomerAccountToVerificationAttemptTypeCollection	_customerAccountToVerificationAttemptTypes;
		private bool	_alwaysFetchCustomerAccountToVerificationAttemptTypes, _alreadyFetchedCustomerAccountToVerificationAttemptTypes;
		private VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection	_notificationMessageToCustomers;
		private bool	_alwaysFetchNotificationMessageToCustomers, _alreadyFetchedNotificationMessageToCustomers;
		private VarioSL.Entities.CollectionClasses.SecurityResponseCollection	_securityResponses;
		private bool	_alwaysFetchSecurityResponses, _alreadyFetchedSecurityResponses;
		private VarioSL.Entities.CollectionClasses.VerificationAttemptCollection	_verificationAttempts;
		private bool	_alwaysFetchVerificationAttempts, _alreadyFetchedVerificationAttempts;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private CustomerAccountRoleEntity _customerAccountRole;
		private bool	_alwaysFetchCustomerAccountRole, _alreadyFetchedCustomerAccountRole, _customerAccountRoleReturnsNewIfNotFound;
		private CustomerLanguageEntity _customerLanguage;
		private bool	_alwaysFetchCustomerLanguage, _alreadyFetchedCustomerLanguage, _customerLanguageReturnsNewIfNotFound;
		private PersonEntity _person;
		private bool	_alwaysFetchPerson, _alreadyFetchedPerson, _personReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name CustomerAccountRole</summary>
			public static readonly string CustomerAccountRole = "CustomerAccountRole";
			/// <summary>Member name CustomerLanguage</summary>
			public static readonly string CustomerLanguage = "CustomerLanguage";
			/// <summary>Member name Person</summary>
			public static readonly string Person = "Person";
			/// <summary>Member name AccountMessageSettings</summary>
			public static readonly string AccountMessageSettings = "AccountMessageSettings";
			/// <summary>Member name CustomerAccountNotifications</summary>
			public static readonly string CustomerAccountNotifications = "CustomerAccountNotifications";
			/// <summary>Member name CustomerAccountToVerificationAttemptTypes</summary>
			public static readonly string CustomerAccountToVerificationAttemptTypes = "CustomerAccountToVerificationAttemptTypes";
			/// <summary>Member name NotificationMessageToCustomers</summary>
			public static readonly string NotificationMessageToCustomers = "NotificationMessageToCustomers";
			/// <summary>Member name SecurityResponses</summary>
			public static readonly string SecurityResponses = "SecurityResponses";
			/// <summary>Member name VerificationAttempts</summary>
			public static readonly string VerificationAttempts = "VerificationAttempts";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CustomerAccountEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CustomerAccountEntity() :base("CustomerAccountEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="customerAccountID">PK value for CustomerAccount which data should be fetched into this CustomerAccount object</param>
		public CustomerAccountEntity(System.Int64 customerAccountID):base("CustomerAccountEntity")
		{
			InitClassFetch(customerAccountID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="customerAccountID">PK value for CustomerAccount which data should be fetched into this CustomerAccount object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CustomerAccountEntity(System.Int64 customerAccountID, IPrefetchPath prefetchPathToUse):base("CustomerAccountEntity")
		{
			InitClassFetch(customerAccountID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="customerAccountID">PK value for CustomerAccount which data should be fetched into this CustomerAccount object</param>
		/// <param name="validator">The custom validator object for this CustomerAccountEntity</param>
		public CustomerAccountEntity(System.Int64 customerAccountID, IValidator validator):base("CustomerAccountEntity")
		{
			InitClassFetch(customerAccountID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CustomerAccountEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_accountMessageSettings = (VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection)info.GetValue("_accountMessageSettings", typeof(VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection));
			_alwaysFetchAccountMessageSettings = info.GetBoolean("_alwaysFetchAccountMessageSettings");
			_alreadyFetchedAccountMessageSettings = info.GetBoolean("_alreadyFetchedAccountMessageSettings");

			_customerAccountNotifications = (VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection)info.GetValue("_customerAccountNotifications", typeof(VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection));
			_alwaysFetchCustomerAccountNotifications = info.GetBoolean("_alwaysFetchCustomerAccountNotifications");
			_alreadyFetchedCustomerAccountNotifications = info.GetBoolean("_alreadyFetchedCustomerAccountNotifications");

			_customerAccountToVerificationAttemptTypes = (VarioSL.Entities.CollectionClasses.CustomerAccountToVerificationAttemptTypeCollection)info.GetValue("_customerAccountToVerificationAttemptTypes", typeof(VarioSL.Entities.CollectionClasses.CustomerAccountToVerificationAttemptTypeCollection));
			_alwaysFetchCustomerAccountToVerificationAttemptTypes = info.GetBoolean("_alwaysFetchCustomerAccountToVerificationAttemptTypes");
			_alreadyFetchedCustomerAccountToVerificationAttemptTypes = info.GetBoolean("_alreadyFetchedCustomerAccountToVerificationAttemptTypes");

			_notificationMessageToCustomers = (VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection)info.GetValue("_notificationMessageToCustomers", typeof(VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection));
			_alwaysFetchNotificationMessageToCustomers = info.GetBoolean("_alwaysFetchNotificationMessageToCustomers");
			_alreadyFetchedNotificationMessageToCustomers = info.GetBoolean("_alreadyFetchedNotificationMessageToCustomers");

			_securityResponses = (VarioSL.Entities.CollectionClasses.SecurityResponseCollection)info.GetValue("_securityResponses", typeof(VarioSL.Entities.CollectionClasses.SecurityResponseCollection));
			_alwaysFetchSecurityResponses = info.GetBoolean("_alwaysFetchSecurityResponses");
			_alreadyFetchedSecurityResponses = info.GetBoolean("_alreadyFetchedSecurityResponses");

			_verificationAttempts = (VarioSL.Entities.CollectionClasses.VerificationAttemptCollection)info.GetValue("_verificationAttempts", typeof(VarioSL.Entities.CollectionClasses.VerificationAttemptCollection));
			_alwaysFetchVerificationAttempts = info.GetBoolean("_alwaysFetchVerificationAttempts");
			_alreadyFetchedVerificationAttempts = info.GetBoolean("_alreadyFetchedVerificationAttempts");
			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_customerAccountRole = (CustomerAccountRoleEntity)info.GetValue("_customerAccountRole", typeof(CustomerAccountRoleEntity));
			if(_customerAccountRole!=null)
			{
				_customerAccountRole.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_customerAccountRoleReturnsNewIfNotFound = info.GetBoolean("_customerAccountRoleReturnsNewIfNotFound");
			_alwaysFetchCustomerAccountRole = info.GetBoolean("_alwaysFetchCustomerAccountRole");
			_alreadyFetchedCustomerAccountRole = info.GetBoolean("_alreadyFetchedCustomerAccountRole");

			_customerLanguage = (CustomerLanguageEntity)info.GetValue("_customerLanguage", typeof(CustomerLanguageEntity));
			if(_customerLanguage!=null)
			{
				_customerLanguage.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_customerLanguageReturnsNewIfNotFound = info.GetBoolean("_customerLanguageReturnsNewIfNotFound");
			_alwaysFetchCustomerLanguage = info.GetBoolean("_alwaysFetchCustomerLanguage");
			_alreadyFetchedCustomerLanguage = info.GetBoolean("_alreadyFetchedCustomerLanguage");

			_person = (PersonEntity)info.GetValue("_person", typeof(PersonEntity));
			if(_person!=null)
			{
				_person.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_personReturnsNewIfNotFound = info.GetBoolean("_personReturnsNewIfNotFound");
			_alwaysFetchPerson = info.GetBoolean("_alwaysFetchPerson");
			_alreadyFetchedPerson = info.GetBoolean("_alreadyFetchedPerson");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CustomerAccountFieldIndex)fieldIndex)
			{
				case CustomerAccountFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case CustomerAccountFieldIndex.CustomerAccountRoleID:
					DesetupSyncCustomerAccountRole(true, false);
					_alreadyFetchedCustomerAccountRole = false;
					break;
				case CustomerAccountFieldIndex.CustomerLanguageID:
					DesetupSyncCustomerLanguage(true, false);
					_alreadyFetchedCustomerLanguage = false;
					break;
				case CustomerAccountFieldIndex.PersonID:
					DesetupSyncPerson(true, false);
					_alreadyFetchedPerson = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAccountMessageSettings = (_accountMessageSettings.Count > 0);
			_alreadyFetchedCustomerAccountNotifications = (_customerAccountNotifications.Count > 0);
			_alreadyFetchedCustomerAccountToVerificationAttemptTypes = (_customerAccountToVerificationAttemptTypes.Count > 0);
			_alreadyFetchedNotificationMessageToCustomers = (_notificationMessageToCustomers.Count > 0);
			_alreadyFetchedSecurityResponses = (_securityResponses.Count > 0);
			_alreadyFetchedVerificationAttempts = (_verificationAttempts.Count > 0);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedCustomerAccountRole = (_customerAccountRole != null);
			_alreadyFetchedCustomerLanguage = (_customerLanguage != null);
			_alreadyFetchedPerson = (_person != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "CustomerAccountRole":
					toReturn.Add(Relations.CustomerAccountRoleEntityUsingCustomerAccountRoleID);
					break;
				case "CustomerLanguage":
					toReturn.Add(Relations.CustomerLanguageEntityUsingCustomerLanguageID);
					break;
				case "Person":
					toReturn.Add(Relations.PersonEntityUsingPersonID);
					break;
				case "AccountMessageSettings":
					toReturn.Add(Relations.AccountMessageSettingEntityUsingCustomerAccountID);
					break;
				case "CustomerAccountNotifications":
					toReturn.Add(Relations.CustomerAccountNotificationEntityUsingCustomerAccountID);
					break;
				case "CustomerAccountToVerificationAttemptTypes":
					toReturn.Add(Relations.CustomerAccountToVerificationAttemptTypeEntityUsingCustomerAccountID);
					break;
				case "NotificationMessageToCustomers":
					toReturn.Add(Relations.NotificationMessageToCustomerEntityUsingCustomerAccountID);
					break;
				case "SecurityResponses":
					toReturn.Add(Relations.SecurityResponseEntityUsingCustomerAccountID);
					break;
				case "VerificationAttempts":
					toReturn.Add(Relations.VerificationAttemptEntityUsingCustomerAccountID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_accountMessageSettings", (!this.MarkedForDeletion?_accountMessageSettings:null));
			info.AddValue("_alwaysFetchAccountMessageSettings", _alwaysFetchAccountMessageSettings);
			info.AddValue("_alreadyFetchedAccountMessageSettings", _alreadyFetchedAccountMessageSettings);
			info.AddValue("_customerAccountNotifications", (!this.MarkedForDeletion?_customerAccountNotifications:null));
			info.AddValue("_alwaysFetchCustomerAccountNotifications", _alwaysFetchCustomerAccountNotifications);
			info.AddValue("_alreadyFetchedCustomerAccountNotifications", _alreadyFetchedCustomerAccountNotifications);
			info.AddValue("_customerAccountToVerificationAttemptTypes", (!this.MarkedForDeletion?_customerAccountToVerificationAttemptTypes:null));
			info.AddValue("_alwaysFetchCustomerAccountToVerificationAttemptTypes", _alwaysFetchCustomerAccountToVerificationAttemptTypes);
			info.AddValue("_alreadyFetchedCustomerAccountToVerificationAttemptTypes", _alreadyFetchedCustomerAccountToVerificationAttemptTypes);
			info.AddValue("_notificationMessageToCustomers", (!this.MarkedForDeletion?_notificationMessageToCustomers:null));
			info.AddValue("_alwaysFetchNotificationMessageToCustomers", _alwaysFetchNotificationMessageToCustomers);
			info.AddValue("_alreadyFetchedNotificationMessageToCustomers", _alreadyFetchedNotificationMessageToCustomers);
			info.AddValue("_securityResponses", (!this.MarkedForDeletion?_securityResponses:null));
			info.AddValue("_alwaysFetchSecurityResponses", _alwaysFetchSecurityResponses);
			info.AddValue("_alreadyFetchedSecurityResponses", _alreadyFetchedSecurityResponses);
			info.AddValue("_verificationAttempts", (!this.MarkedForDeletion?_verificationAttempts:null));
			info.AddValue("_alwaysFetchVerificationAttempts", _alwaysFetchVerificationAttempts);
			info.AddValue("_alreadyFetchedVerificationAttempts", _alreadyFetchedVerificationAttempts);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_customerAccountRole", (!this.MarkedForDeletion?_customerAccountRole:null));
			info.AddValue("_customerAccountRoleReturnsNewIfNotFound", _customerAccountRoleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCustomerAccountRole", _alwaysFetchCustomerAccountRole);
			info.AddValue("_alreadyFetchedCustomerAccountRole", _alreadyFetchedCustomerAccountRole);
			info.AddValue("_customerLanguage", (!this.MarkedForDeletion?_customerLanguage:null));
			info.AddValue("_customerLanguageReturnsNewIfNotFound", _customerLanguageReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCustomerLanguage", _alwaysFetchCustomerLanguage);
			info.AddValue("_alreadyFetchedCustomerLanguage", _alreadyFetchedCustomerLanguage);
			info.AddValue("_person", (!this.MarkedForDeletion?_person:null));
			info.AddValue("_personReturnsNewIfNotFound", _personReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerson", _alwaysFetchPerson);
			info.AddValue("_alreadyFetchedPerson", _alreadyFetchedPerson);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "CustomerAccountRole":
					_alreadyFetchedCustomerAccountRole = true;
					this.CustomerAccountRole = (CustomerAccountRoleEntity)entity;
					break;
				case "CustomerLanguage":
					_alreadyFetchedCustomerLanguage = true;
					this.CustomerLanguage = (CustomerLanguageEntity)entity;
					break;
				case "Person":
					_alreadyFetchedPerson = true;
					this.Person = (PersonEntity)entity;
					break;
				case "AccountMessageSettings":
					_alreadyFetchedAccountMessageSettings = true;
					if(entity!=null)
					{
						this.AccountMessageSettings.Add((AccountMessageSettingEntity)entity);
					}
					break;
				case "CustomerAccountNotifications":
					_alreadyFetchedCustomerAccountNotifications = true;
					if(entity!=null)
					{
						this.CustomerAccountNotifications.Add((CustomerAccountNotificationEntity)entity);
					}
					break;
				case "CustomerAccountToVerificationAttemptTypes":
					_alreadyFetchedCustomerAccountToVerificationAttemptTypes = true;
					if(entity!=null)
					{
						this.CustomerAccountToVerificationAttemptTypes.Add((CustomerAccountToVerificationAttemptTypeEntity)entity);
					}
					break;
				case "NotificationMessageToCustomers":
					_alreadyFetchedNotificationMessageToCustomers = true;
					if(entity!=null)
					{
						this.NotificationMessageToCustomers.Add((NotificationMessageToCustomerEntity)entity);
					}
					break;
				case "SecurityResponses":
					_alreadyFetchedSecurityResponses = true;
					if(entity!=null)
					{
						this.SecurityResponses.Add((SecurityResponseEntity)entity);
					}
					break;
				case "VerificationAttempts":
					_alreadyFetchedVerificationAttempts = true;
					if(entity!=null)
					{
						this.VerificationAttempts.Add((VerificationAttemptEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "CustomerAccountRole":
					SetupSyncCustomerAccountRole(relatedEntity);
					break;
				case "CustomerLanguage":
					SetupSyncCustomerLanguage(relatedEntity);
					break;
				case "Person":
					SetupSyncPerson(relatedEntity);
					break;
				case "AccountMessageSettings":
					_accountMessageSettings.Add((AccountMessageSettingEntity)relatedEntity);
					break;
				case "CustomerAccountNotifications":
					_customerAccountNotifications.Add((CustomerAccountNotificationEntity)relatedEntity);
					break;
				case "CustomerAccountToVerificationAttemptTypes":
					_customerAccountToVerificationAttemptTypes.Add((CustomerAccountToVerificationAttemptTypeEntity)relatedEntity);
					break;
				case "NotificationMessageToCustomers":
					_notificationMessageToCustomers.Add((NotificationMessageToCustomerEntity)relatedEntity);
					break;
				case "SecurityResponses":
					_securityResponses.Add((SecurityResponseEntity)relatedEntity);
					break;
				case "VerificationAttempts":
					_verificationAttempts.Add((VerificationAttemptEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "CustomerAccountRole":
					DesetupSyncCustomerAccountRole(false, true);
					break;
				case "CustomerLanguage":
					DesetupSyncCustomerLanguage(false, true);
					break;
				case "Person":
					DesetupSyncPerson(false, true);
					break;
				case "AccountMessageSettings":
					this.PerformRelatedEntityRemoval(_accountMessageSettings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomerAccountNotifications":
					this.PerformRelatedEntityRemoval(_customerAccountNotifications, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomerAccountToVerificationAttemptTypes":
					this.PerformRelatedEntityRemoval(_customerAccountToVerificationAttemptTypes, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "NotificationMessageToCustomers":
					this.PerformRelatedEntityRemoval(_notificationMessageToCustomers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SecurityResponses":
					this.PerformRelatedEntityRemoval(_securityResponses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VerificationAttempts":
					this.PerformRelatedEntityRemoval(_verificationAttempts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_customerAccountRole!=null)
			{
				toReturn.Add(_customerAccountRole);
			}
			if(_customerLanguage!=null)
			{
				toReturn.Add(_customerLanguage);
			}
			if(_person!=null)
			{
				toReturn.Add(_person);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_accountMessageSettings);
			toReturn.Add(_customerAccountNotifications);
			toReturn.Add(_customerAccountToVerificationAttemptTypes);
			toReturn.Add(_notificationMessageToCustomers);
			toReturn.Add(_securityResponses);
			toReturn.Add(_verificationAttempts);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerAccountID">PK value for CustomerAccount which data should be fetched into this CustomerAccount object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customerAccountID)
		{
			return FetchUsingPK(customerAccountID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerAccountID">PK value for CustomerAccount which data should be fetched into this CustomerAccount object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customerAccountID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(customerAccountID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerAccountID">PK value for CustomerAccount which data should be fetched into this CustomerAccount object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customerAccountID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(customerAccountID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerAccountID">PK value for CustomerAccount which data should be fetched into this CustomerAccount object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customerAccountID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(customerAccountID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CustomerAccountID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CustomerAccountRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AccountMessageSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AccountMessageSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection GetMultiAccountMessageSettings(bool forceFetch)
		{
			return GetMultiAccountMessageSettings(forceFetch, _accountMessageSettings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccountMessageSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AccountMessageSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection GetMultiAccountMessageSettings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAccountMessageSettings(forceFetch, _accountMessageSettings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AccountMessageSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection GetMultiAccountMessageSettings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAccountMessageSettings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccountMessageSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection GetMultiAccountMessageSettings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAccountMessageSettings || forceFetch || _alwaysFetchAccountMessageSettings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_accountMessageSettings);
				_accountMessageSettings.SuppressClearInGetMulti=!forceFetch;
				_accountMessageSettings.EntityFactoryToUse = entityFactoryToUse;
				_accountMessageSettings.GetMultiManyToOne(this, null, filter);
				_accountMessageSettings.SuppressClearInGetMulti=false;
				_alreadyFetchedAccountMessageSettings = true;
			}
			return _accountMessageSettings;
		}

		/// <summary> Sets the collection parameters for the collection for 'AccountMessageSettings'. These settings will be taken into account
		/// when the property AccountMessageSettings is requested or GetMultiAccountMessageSettings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAccountMessageSettings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_accountMessageSettings.SortClauses=sortClauses;
			_accountMessageSettings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerAccountNotificationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection GetMultiCustomerAccountNotifications(bool forceFetch)
		{
			return GetMultiCustomerAccountNotifications(forceFetch, _customerAccountNotifications.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomerAccountNotificationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection GetMultiCustomerAccountNotifications(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomerAccountNotifications(forceFetch, _customerAccountNotifications.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection GetMultiCustomerAccountNotifications(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomerAccountNotifications(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection GetMultiCustomerAccountNotifications(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomerAccountNotifications || forceFetch || _alwaysFetchCustomerAccountNotifications) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerAccountNotifications);
				_customerAccountNotifications.SuppressClearInGetMulti=!forceFetch;
				_customerAccountNotifications.EntityFactoryToUse = entityFactoryToUse;
				_customerAccountNotifications.GetMultiManyToOne(this, null, null, filter);
				_customerAccountNotifications.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerAccountNotifications = true;
			}
			return _customerAccountNotifications;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerAccountNotifications'. These settings will be taken into account
		/// when the property CustomerAccountNotifications is requested or GetMultiCustomerAccountNotifications is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerAccountNotifications(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerAccountNotifications.SortClauses=sortClauses;
			_customerAccountNotifications.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountToVerificationAttemptTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerAccountToVerificationAttemptTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountToVerificationAttemptTypeCollection GetMultiCustomerAccountToVerificationAttemptTypes(bool forceFetch)
		{
			return GetMultiCustomerAccountToVerificationAttemptTypes(forceFetch, _customerAccountToVerificationAttemptTypes.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountToVerificationAttemptTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomerAccountToVerificationAttemptTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountToVerificationAttemptTypeCollection GetMultiCustomerAccountToVerificationAttemptTypes(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomerAccountToVerificationAttemptTypes(forceFetch, _customerAccountToVerificationAttemptTypes.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountToVerificationAttemptTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountToVerificationAttemptTypeCollection GetMultiCustomerAccountToVerificationAttemptTypes(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomerAccountToVerificationAttemptTypes(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountToVerificationAttemptTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CustomerAccountToVerificationAttemptTypeCollection GetMultiCustomerAccountToVerificationAttemptTypes(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomerAccountToVerificationAttemptTypes || forceFetch || _alwaysFetchCustomerAccountToVerificationAttemptTypes) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerAccountToVerificationAttemptTypes);
				_customerAccountToVerificationAttemptTypes.SuppressClearInGetMulti=!forceFetch;
				_customerAccountToVerificationAttemptTypes.EntityFactoryToUse = entityFactoryToUse;
				_customerAccountToVerificationAttemptTypes.GetMultiManyToOne(this, filter);
				_customerAccountToVerificationAttemptTypes.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerAccountToVerificationAttemptTypes = true;
			}
			return _customerAccountToVerificationAttemptTypes;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerAccountToVerificationAttemptTypes'. These settings will be taken into account
		/// when the property CustomerAccountToVerificationAttemptTypes is requested or GetMultiCustomerAccountToVerificationAttemptTypes is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerAccountToVerificationAttemptTypes(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerAccountToVerificationAttemptTypes.SortClauses=sortClauses;
			_customerAccountToVerificationAttemptTypes.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageToCustomerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageToCustomerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection GetMultiNotificationMessageToCustomers(bool forceFetch)
		{
			return GetMultiNotificationMessageToCustomers(forceFetch, _notificationMessageToCustomers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageToCustomerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageToCustomerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection GetMultiNotificationMessageToCustomers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNotificationMessageToCustomers(forceFetch, _notificationMessageToCustomers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageToCustomerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection GetMultiNotificationMessageToCustomers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNotificationMessageToCustomers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageToCustomerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection GetMultiNotificationMessageToCustomers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNotificationMessageToCustomers || forceFetch || _alwaysFetchNotificationMessageToCustomers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_notificationMessageToCustomers);
				_notificationMessageToCustomers.SuppressClearInGetMulti=!forceFetch;
				_notificationMessageToCustomers.EntityFactoryToUse = entityFactoryToUse;
				_notificationMessageToCustomers.GetMultiManyToOne(this, null, filter);
				_notificationMessageToCustomers.SuppressClearInGetMulti=false;
				_alreadyFetchedNotificationMessageToCustomers = true;
			}
			return _notificationMessageToCustomers;
		}

		/// <summary> Sets the collection parameters for the collection for 'NotificationMessageToCustomers'. These settings will be taken into account
		/// when the property NotificationMessageToCustomers is requested or GetMultiNotificationMessageToCustomers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNotificationMessageToCustomers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_notificationMessageToCustomers.SortClauses=sortClauses;
			_notificationMessageToCustomers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SecurityResponseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SecurityResponseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SecurityResponseCollection GetMultiSecurityResponses(bool forceFetch)
		{
			return GetMultiSecurityResponses(forceFetch, _securityResponses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SecurityResponseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SecurityResponseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SecurityResponseCollection GetMultiSecurityResponses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSecurityResponses(forceFetch, _securityResponses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SecurityResponseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SecurityResponseCollection GetMultiSecurityResponses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSecurityResponses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SecurityResponseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SecurityResponseCollection GetMultiSecurityResponses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSecurityResponses || forceFetch || _alwaysFetchSecurityResponses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_securityResponses);
				_securityResponses.SuppressClearInGetMulti=!forceFetch;
				_securityResponses.EntityFactoryToUse = entityFactoryToUse;
				_securityResponses.GetMultiManyToOne(this, null, filter);
				_securityResponses.SuppressClearInGetMulti=false;
				_alreadyFetchedSecurityResponses = true;
			}
			return _securityResponses;
		}

		/// <summary> Sets the collection parameters for the collection for 'SecurityResponses'. These settings will be taken into account
		/// when the property SecurityResponses is requested or GetMultiSecurityResponses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSecurityResponses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_securityResponses.SortClauses=sortClauses;
			_securityResponses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VerificationAttemptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VerificationAttemptEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VerificationAttemptCollection GetMultiVerificationAttempts(bool forceFetch)
		{
			return GetMultiVerificationAttempts(forceFetch, _verificationAttempts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VerificationAttemptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VerificationAttemptEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VerificationAttemptCollection GetMultiVerificationAttempts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVerificationAttempts(forceFetch, _verificationAttempts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VerificationAttemptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VerificationAttemptCollection GetMultiVerificationAttempts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVerificationAttempts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VerificationAttemptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VerificationAttemptCollection GetMultiVerificationAttempts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVerificationAttempts || forceFetch || _alwaysFetchVerificationAttempts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_verificationAttempts);
				_verificationAttempts.SuppressClearInGetMulti=!forceFetch;
				_verificationAttempts.EntityFactoryToUse = entityFactoryToUse;
				_verificationAttempts.GetMultiManyToOne(this, filter);
				_verificationAttempts.SuppressClearInGetMulti=false;
				_alreadyFetchedVerificationAttempts = true;
			}
			return _verificationAttempts;
		}

		/// <summary> Sets the collection parameters for the collection for 'VerificationAttempts'. These settings will be taken into account
		/// when the property VerificationAttempts is requested or GetMultiVerificationAttempts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVerificationAttempts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_verificationAttempts.SortClauses=sortClauses;
			_verificationAttempts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID);
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'CustomerAccountRoleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomerAccountRoleEntity' which is related to this entity.</returns>
		public CustomerAccountRoleEntity GetSingleCustomerAccountRole()
		{
			return GetSingleCustomerAccountRole(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomerAccountRoleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomerAccountRoleEntity' which is related to this entity.</returns>
		public virtual CustomerAccountRoleEntity GetSingleCustomerAccountRole(bool forceFetch)
		{
			if( ( !_alreadyFetchedCustomerAccountRole || forceFetch || _alwaysFetchCustomerAccountRole) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomerAccountRoleEntityUsingCustomerAccountRoleID);
				CustomerAccountRoleEntity newEntity = new CustomerAccountRoleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CustomerAccountRoleID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CustomerAccountRoleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_customerAccountRoleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CustomerAccountRole = newEntity;
				_alreadyFetchedCustomerAccountRole = fetchResult;
			}
			return _customerAccountRole;
		}


		/// <summary> Retrieves the related entity of type 'CustomerLanguageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomerLanguageEntity' which is related to this entity.</returns>
		public CustomerLanguageEntity GetSingleCustomerLanguage()
		{
			return GetSingleCustomerLanguage(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomerLanguageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomerLanguageEntity' which is related to this entity.</returns>
		public virtual CustomerLanguageEntity GetSingleCustomerLanguage(bool forceFetch)
		{
			if( ( !_alreadyFetchedCustomerLanguage || forceFetch || _alwaysFetchCustomerLanguage) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomerLanguageEntityUsingCustomerLanguageID);
				CustomerLanguageEntity newEntity = new CustomerLanguageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CustomerLanguageID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CustomerLanguageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_customerLanguageReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CustomerLanguage = newEntity;
				_alreadyFetchedCustomerLanguage = fetchResult;
			}
			return _customerLanguage;
		}


		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSinglePerson()
		{
			return GetSinglePerson(false);
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSinglePerson(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerson || forceFetch || _alwaysFetchPerson) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingPersonID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphic(this.Transaction, this.PersonID, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_personReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Person = newEntity;
				_alreadyFetchedPerson = fetchResult;
			}
			return _person;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Contract", _contract);
			toReturn.Add("CustomerAccountRole", _customerAccountRole);
			toReturn.Add("CustomerLanguage", _customerLanguage);
			toReturn.Add("Person", _person);
			toReturn.Add("AccountMessageSettings", _accountMessageSettings);
			toReturn.Add("CustomerAccountNotifications", _customerAccountNotifications);
			toReturn.Add("CustomerAccountToVerificationAttemptTypes", _customerAccountToVerificationAttemptTypes);
			toReturn.Add("NotificationMessageToCustomers", _notificationMessageToCustomers);
			toReturn.Add("SecurityResponses", _securityResponses);
			toReturn.Add("VerificationAttempts", _verificationAttempts);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="customerAccountID">PK value for CustomerAccount which data should be fetched into this CustomerAccount object</param>
		/// <param name="validator">The validator object for this CustomerAccountEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 customerAccountID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(customerAccountID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_accountMessageSettings = new VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection();
			_accountMessageSettings.SetContainingEntityInfo(this, "CustomerAccount");

			_customerAccountNotifications = new VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection();
			_customerAccountNotifications.SetContainingEntityInfo(this, "CustomerAccount");

			_customerAccountToVerificationAttemptTypes = new VarioSL.Entities.CollectionClasses.CustomerAccountToVerificationAttemptTypeCollection();
			_customerAccountToVerificationAttemptTypes.SetContainingEntityInfo(this, "CustomerAccount");

			_notificationMessageToCustomers = new VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection();
			_notificationMessageToCustomers.SetContainingEntityInfo(this, "CustomerAccount");

			_securityResponses = new VarioSL.Entities.CollectionClasses.SecurityResponseCollection();
			_securityResponses.SetContainingEntityInfo(this, "CustomerAccount");

			_verificationAttempts = new VarioSL.Entities.CollectionClasses.VerificationAttemptCollection();
			_verificationAttempts.SetContainingEntityInfo(this, "CustomerAccount");
			_contractReturnsNewIfNotFound = false;
			_customerAccountRoleReturnsNewIfNotFound = false;
			_customerLanguageReturnsNewIfNotFound = false;
			_personReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerAccountID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerAccountRoleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerLanguageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsPasswordTemporary", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LockoutUntil", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Password", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PasswordChanged", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PhonePassword", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TotalLoginAttempts", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VerificationToken", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VerificationTokenExpiryTime", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticCustomerAccountRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "CustomerAccounts", resetFKFields, new int[] { (int)CustomerAccountFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticCustomerAccountRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _customerAccountRole</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCustomerAccountRole(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _customerAccountRole, new PropertyChangedEventHandler( OnCustomerAccountRolePropertyChanged ), "CustomerAccountRole", VarioSL.Entities.RelationClasses.StaticCustomerAccountRelations.CustomerAccountRoleEntityUsingCustomerAccountRoleIDStatic, true, signalRelatedEntity, "CustomerAccounts", resetFKFields, new int[] { (int)CustomerAccountFieldIndex.CustomerAccountRoleID } );		
			_customerAccountRole = null;
		}
		
		/// <summary> setups the sync logic for member _customerAccountRole</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCustomerAccountRole(IEntityCore relatedEntity)
		{
			if(_customerAccountRole!=relatedEntity)
			{		
				DesetupSyncCustomerAccountRole(true, true);
				_customerAccountRole = (CustomerAccountRoleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _customerAccountRole, new PropertyChangedEventHandler( OnCustomerAccountRolePropertyChanged ), "CustomerAccountRole", VarioSL.Entities.RelationClasses.StaticCustomerAccountRelations.CustomerAccountRoleEntityUsingCustomerAccountRoleIDStatic, true, ref _alreadyFetchedCustomerAccountRole, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCustomerAccountRolePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _customerLanguage</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCustomerLanguage(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _customerLanguage, new PropertyChangedEventHandler( OnCustomerLanguagePropertyChanged ), "CustomerLanguage", VarioSL.Entities.RelationClasses.StaticCustomerAccountRelations.CustomerLanguageEntityUsingCustomerLanguageIDStatic, true, signalRelatedEntity, "CustomerAccounts", resetFKFields, new int[] { (int)CustomerAccountFieldIndex.CustomerLanguageID } );		
			_customerLanguage = null;
		}
		
		/// <summary> setups the sync logic for member _customerLanguage</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCustomerLanguage(IEntityCore relatedEntity)
		{
			if(_customerLanguage!=relatedEntity)
			{		
				DesetupSyncCustomerLanguage(true, true);
				_customerLanguage = (CustomerLanguageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _customerLanguage, new PropertyChangedEventHandler( OnCustomerLanguagePropertyChanged ), "CustomerLanguage", VarioSL.Entities.RelationClasses.StaticCustomerAccountRelations.CustomerLanguageEntityUsingCustomerLanguageIDStatic, true, ref _alreadyFetchedCustomerLanguage, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCustomerLanguagePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _person</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerson(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticCustomerAccountRelations.PersonEntityUsingPersonIDStatic, true, signalRelatedEntity, "CustomerAccounts", resetFKFields, new int[] { (int)CustomerAccountFieldIndex.PersonID } );		
			_person = null;
		}
		
		/// <summary> setups the sync logic for member _person</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerson(IEntityCore relatedEntity)
		{
			if(_person!=relatedEntity)
			{		
				DesetupSyncPerson(true, true);
				_person = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticCustomerAccountRelations.PersonEntityUsingPersonIDStatic, true, ref _alreadyFetchedPerson, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPersonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="customerAccountID">PK value for CustomerAccount which data should be fetched into this CustomerAccount object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 customerAccountID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CustomerAccountFieldIndex.CustomerAccountID].ForcedCurrentValueWrite(customerAccountID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCustomerAccountDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CustomerAccountEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CustomerAccountRelations Relations
		{
			get	{ return new CustomerAccountRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AccountMessageSetting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccountMessageSettings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection(), (IEntityRelation)GetRelationsForField("AccountMessageSettings")[0], (int)VarioSL.Entities.EntityType.CustomerAccountEntity, (int)VarioSL.Entities.EntityType.AccountMessageSettingEntity, 0, null, null, null, "AccountMessageSettings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomerAccountNotification' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerAccountNotifications
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection(), (IEntityRelation)GetRelationsForField("CustomerAccountNotifications")[0], (int)VarioSL.Entities.EntityType.CustomerAccountEntity, (int)VarioSL.Entities.EntityType.CustomerAccountNotificationEntity, 0, null, null, null, "CustomerAccountNotifications", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomerAccountToVerificationAttemptType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerAccountToVerificationAttemptTypes
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomerAccountToVerificationAttemptTypeCollection(), (IEntityRelation)GetRelationsForField("CustomerAccountToVerificationAttemptTypes")[0], (int)VarioSL.Entities.EntityType.CustomerAccountEntity, (int)VarioSL.Entities.EntityType.CustomerAccountToVerificationAttemptTypeEntity, 0, null, null, null, "CustomerAccountToVerificationAttemptTypes", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NotificationMessageToCustomer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNotificationMessageToCustomers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection(), (IEntityRelation)GetRelationsForField("NotificationMessageToCustomers")[0], (int)VarioSL.Entities.EntityType.CustomerAccountEntity, (int)VarioSL.Entities.EntityType.NotificationMessageToCustomerEntity, 0, null, null, null, "NotificationMessageToCustomers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SecurityResponse' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSecurityResponses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SecurityResponseCollection(), (IEntityRelation)GetRelationsForField("SecurityResponses")[0], (int)VarioSL.Entities.EntityType.CustomerAccountEntity, (int)VarioSL.Entities.EntityType.SecurityResponseEntity, 0, null, null, null, "SecurityResponses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VerificationAttempt' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVerificationAttempts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VerificationAttemptCollection(), (IEntityRelation)GetRelationsForField("VerificationAttempts")[0], (int)VarioSL.Entities.EntityType.CustomerAccountEntity, (int)VarioSL.Entities.EntityType.VerificationAttemptEntity, 0, null, null, null, "VerificationAttempts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.CustomerAccountEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomerAccountRole'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerAccountRole
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomerAccountRoleCollection(), (IEntityRelation)GetRelationsForField("CustomerAccountRole")[0], (int)VarioSL.Entities.EntityType.CustomerAccountEntity, (int)VarioSL.Entities.EntityType.CustomerAccountRoleEntity, 0, null, null, null, "CustomerAccountRole", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomerLanguage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerLanguage
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomerLanguageCollection(), (IEntityRelation)GetRelationsForField("CustomerLanguage")[0], (int)VarioSL.Entities.EntityType.CustomerAccountEntity, (int)VarioSL.Entities.EntityType.CustomerLanguageEntity, 0, null, null, null, "CustomerLanguage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerson
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("Person")[0], (int)VarioSL.Entities.EntityType.CustomerAccountEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "Person", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ContractID property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ContractID
		{
			get { return (System.Int64)GetValue((int)CustomerAccountFieldIndex.ContractID, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.ContractID, value, true); }
		}

		/// <summary> The CustomerAccountID property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."CUSTOMERACCOUNTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CustomerAccountID
		{
			get { return (System.Int64)GetValue((int)CustomerAccountFieldIndex.CustomerAccountID, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.CustomerAccountID, value, true); }
		}

		/// <summary> The CustomerAccountRoleID property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."CUSTOMERACCOUNTROLEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CustomerAccountRoleID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CustomerAccountFieldIndex.CustomerAccountRoleID, false); }
			set	{ SetValue((int)CustomerAccountFieldIndex.CustomerAccountRoleID, value, true); }
		}

		/// <summary> The CustomerLanguageID property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."CUSTOMERLANGUAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CustomerLanguageID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CustomerAccountFieldIndex.CustomerLanguageID, false); }
			set	{ SetValue((int)CustomerAccountFieldIndex.CustomerLanguageID, value, true); }
		}

		/// <summary> The IsPasswordTemporary property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."ISPASSWORDTEMPORARY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsPasswordTemporary
		{
			get { return (System.Boolean)GetValue((int)CustomerAccountFieldIndex.IsPasswordTemporary, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.IsPasswordTemporary, value, true); }
		}

		/// <summary> The LastModified property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CustomerAccountFieldIndex.LastModified, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CustomerAccountFieldIndex.LastUser, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.LastUser, value, true); }
		}

		/// <summary> The LockoutUntil property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."LOCKOUTUNTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LockoutUntil
		{
			get { return (System.DateTime)GetValue((int)CustomerAccountFieldIndex.LockoutUntil, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.LockoutUntil, value, true); }
		}

		/// <summary> The Password property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."PASSWORD"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Password
		{
			get { return (System.String)GetValue((int)CustomerAccountFieldIndex.Password, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.Password, value, true); }
		}

		/// <summary> The PasswordChanged property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."PASSWORDCHANGED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PasswordChanged
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomerAccountFieldIndex.PasswordChanged, false); }
			set	{ SetValue((int)CustomerAccountFieldIndex.PasswordChanged, value, true); }
		}

		/// <summary> The PersonID property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."PERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PersonID
		{
			get { return (System.Int64)GetValue((int)CustomerAccountFieldIndex.PersonID, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.PersonID, value, true); }
		}

		/// <summary> The PhonePassword property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."PHONEPASSWORD"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PhonePassword
		{
			get { return (System.String)GetValue((int)CustomerAccountFieldIndex.PhonePassword, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.PhonePassword, value, true); }
		}

		/// <summary> The State property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CustomerAccountState State
		{
			get { return (VarioSL.Entities.Enumerations.CustomerAccountState)GetValue((int)CustomerAccountFieldIndex.State, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.State, value, true); }
		}

		/// <summary> The TotalLoginAttempts property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."TOTALLOGINATTEMPTS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TotalLoginAttempts
		{
			get { return (System.Int32)GetValue((int)CustomerAccountFieldIndex.TotalLoginAttempts, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.TotalLoginAttempts, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CustomerAccountFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The UserName property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."USERNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String UserName
		{
			get { return (System.String)GetValue((int)CustomerAccountFieldIndex.UserName, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.UserName, value, true); }
		}

		/// <summary> The VerificationToken property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."VERIFICATIONTOKEN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VerificationToken
		{
			get { return (System.String)GetValue((int)CustomerAccountFieldIndex.VerificationToken, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.VerificationToken, value, true); }
		}

		/// <summary> The VerificationTokenExpiryTime property of the Entity CustomerAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNT"."VERIFICATIONTOKENEXPIRYTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime VerificationTokenExpiryTime
		{
			get { return (System.DateTime)GetValue((int)CustomerAccountFieldIndex.VerificationTokenExpiryTime, true); }
			set	{ SetValue((int)CustomerAccountFieldIndex.VerificationTokenExpiryTime, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AccountMessageSettingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAccountMessageSettings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection AccountMessageSettings
		{
			get	{ return GetMultiAccountMessageSettings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AccountMessageSettings. When set to true, AccountMessageSettings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccountMessageSettings is accessed. You can always execute/ a forced fetch by calling GetMultiAccountMessageSettings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccountMessageSettings
		{
			get	{ return _alwaysFetchAccountMessageSettings; }
			set	{ _alwaysFetchAccountMessageSettings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AccountMessageSettings already has been fetched. Setting this property to false when AccountMessageSettings has been fetched
		/// will clear the AccountMessageSettings collection well. Setting this property to true while AccountMessageSettings hasn't been fetched disables lazy loading for AccountMessageSettings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccountMessageSettings
		{
			get { return _alreadyFetchedAccountMessageSettings;}
			set 
			{
				if(_alreadyFetchedAccountMessageSettings && !value && (_accountMessageSettings != null))
				{
					_accountMessageSettings.Clear();
				}
				_alreadyFetchedAccountMessageSettings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerAccountNotifications()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection CustomerAccountNotifications
		{
			get	{ return GetMultiCustomerAccountNotifications(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerAccountNotifications. When set to true, CustomerAccountNotifications is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerAccountNotifications is accessed. You can always execute/ a forced fetch by calling GetMultiCustomerAccountNotifications(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerAccountNotifications
		{
			get	{ return _alwaysFetchCustomerAccountNotifications; }
			set	{ _alwaysFetchCustomerAccountNotifications = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerAccountNotifications already has been fetched. Setting this property to false when CustomerAccountNotifications has been fetched
		/// will clear the CustomerAccountNotifications collection well. Setting this property to true while CustomerAccountNotifications hasn't been fetched disables lazy loading for CustomerAccountNotifications</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerAccountNotifications
		{
			get { return _alreadyFetchedCustomerAccountNotifications;}
			set 
			{
				if(_alreadyFetchedCustomerAccountNotifications && !value && (_customerAccountNotifications != null))
				{
					_customerAccountNotifications.Clear();
				}
				_alreadyFetchedCustomerAccountNotifications = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomerAccountToVerificationAttemptTypeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerAccountToVerificationAttemptTypes()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CustomerAccountToVerificationAttemptTypeCollection CustomerAccountToVerificationAttemptTypes
		{
			get	{ return GetMultiCustomerAccountToVerificationAttemptTypes(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerAccountToVerificationAttemptTypes. When set to true, CustomerAccountToVerificationAttemptTypes is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerAccountToVerificationAttemptTypes is accessed. You can always execute/ a forced fetch by calling GetMultiCustomerAccountToVerificationAttemptTypes(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerAccountToVerificationAttemptTypes
		{
			get	{ return _alwaysFetchCustomerAccountToVerificationAttemptTypes; }
			set	{ _alwaysFetchCustomerAccountToVerificationAttemptTypes = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerAccountToVerificationAttemptTypes already has been fetched. Setting this property to false when CustomerAccountToVerificationAttemptTypes has been fetched
		/// will clear the CustomerAccountToVerificationAttemptTypes collection well. Setting this property to true while CustomerAccountToVerificationAttemptTypes hasn't been fetched disables lazy loading for CustomerAccountToVerificationAttemptTypes</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerAccountToVerificationAttemptTypes
		{
			get { return _alreadyFetchedCustomerAccountToVerificationAttemptTypes;}
			set 
			{
				if(_alreadyFetchedCustomerAccountToVerificationAttemptTypes && !value && (_customerAccountToVerificationAttemptTypes != null))
				{
					_customerAccountToVerificationAttemptTypes.Clear();
				}
				_alreadyFetchedCustomerAccountToVerificationAttemptTypes = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NotificationMessageToCustomerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNotificationMessageToCustomers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection NotificationMessageToCustomers
		{
			get	{ return GetMultiNotificationMessageToCustomers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NotificationMessageToCustomers. When set to true, NotificationMessageToCustomers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NotificationMessageToCustomers is accessed. You can always execute/ a forced fetch by calling GetMultiNotificationMessageToCustomers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNotificationMessageToCustomers
		{
			get	{ return _alwaysFetchNotificationMessageToCustomers; }
			set	{ _alwaysFetchNotificationMessageToCustomers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NotificationMessageToCustomers already has been fetched. Setting this property to false when NotificationMessageToCustomers has been fetched
		/// will clear the NotificationMessageToCustomers collection well. Setting this property to true while NotificationMessageToCustomers hasn't been fetched disables lazy loading for NotificationMessageToCustomers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNotificationMessageToCustomers
		{
			get { return _alreadyFetchedNotificationMessageToCustomers;}
			set 
			{
				if(_alreadyFetchedNotificationMessageToCustomers && !value && (_notificationMessageToCustomers != null))
				{
					_notificationMessageToCustomers.Clear();
				}
				_alreadyFetchedNotificationMessageToCustomers = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SecurityResponseEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSecurityResponses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SecurityResponseCollection SecurityResponses
		{
			get	{ return GetMultiSecurityResponses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SecurityResponses. When set to true, SecurityResponses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SecurityResponses is accessed. You can always execute/ a forced fetch by calling GetMultiSecurityResponses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSecurityResponses
		{
			get	{ return _alwaysFetchSecurityResponses; }
			set	{ _alwaysFetchSecurityResponses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SecurityResponses already has been fetched. Setting this property to false when SecurityResponses has been fetched
		/// will clear the SecurityResponses collection well. Setting this property to true while SecurityResponses hasn't been fetched disables lazy loading for SecurityResponses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSecurityResponses
		{
			get { return _alreadyFetchedSecurityResponses;}
			set 
			{
				if(_alreadyFetchedSecurityResponses && !value && (_securityResponses != null))
				{
					_securityResponses.Clear();
				}
				_alreadyFetchedSecurityResponses = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VerificationAttemptEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVerificationAttempts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VerificationAttemptCollection VerificationAttempts
		{
			get	{ return GetMultiVerificationAttempts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VerificationAttempts. When set to true, VerificationAttempts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VerificationAttempts is accessed. You can always execute/ a forced fetch by calling GetMultiVerificationAttempts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVerificationAttempts
		{
			get	{ return _alwaysFetchVerificationAttempts; }
			set	{ _alwaysFetchVerificationAttempts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VerificationAttempts already has been fetched. Setting this property to false when VerificationAttempts has been fetched
		/// will clear the VerificationAttempts collection well. Setting this property to true while VerificationAttempts hasn't been fetched disables lazy loading for VerificationAttempts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVerificationAttempts
		{
			get { return _alreadyFetchedVerificationAttempts;}
			set 
			{
				if(_alreadyFetchedVerificationAttempts && !value && (_verificationAttempts != null))
				{
					_verificationAttempts.Clear();
				}
				_alreadyFetchedVerificationAttempts = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomerAccounts", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CustomerAccountRoleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCustomerAccountRole()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CustomerAccountRoleEntity CustomerAccountRole
		{
			get	{ return GetSingleCustomerAccountRole(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCustomerAccountRole(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomerAccounts", "CustomerAccountRole", _customerAccountRole, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerAccountRole. When set to true, CustomerAccountRole is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerAccountRole is accessed. You can always execute a forced fetch by calling GetSingleCustomerAccountRole(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerAccountRole
		{
			get	{ return _alwaysFetchCustomerAccountRole; }
			set	{ _alwaysFetchCustomerAccountRole = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerAccountRole already has been fetched. Setting this property to false when CustomerAccountRole has been fetched
		/// will set CustomerAccountRole to null as well. Setting this property to true while CustomerAccountRole hasn't been fetched disables lazy loading for CustomerAccountRole</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerAccountRole
		{
			get { return _alreadyFetchedCustomerAccountRole;}
			set 
			{
				if(_alreadyFetchedCustomerAccountRole && !value)
				{
					this.CustomerAccountRole = null;
				}
				_alreadyFetchedCustomerAccountRole = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CustomerAccountRole is not found
		/// in the database. When set to true, CustomerAccountRole will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CustomerAccountRoleReturnsNewIfNotFound
		{
			get	{ return _customerAccountRoleReturnsNewIfNotFound; }
			set { _customerAccountRoleReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CustomerLanguageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCustomerLanguage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CustomerLanguageEntity CustomerLanguage
		{
			get	{ return GetSingleCustomerLanguage(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCustomerLanguage(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomerAccounts", "CustomerLanguage", _customerLanguage, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerLanguage. When set to true, CustomerLanguage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerLanguage is accessed. You can always execute a forced fetch by calling GetSingleCustomerLanguage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerLanguage
		{
			get	{ return _alwaysFetchCustomerLanguage; }
			set	{ _alwaysFetchCustomerLanguage = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerLanguage already has been fetched. Setting this property to false when CustomerLanguage has been fetched
		/// will set CustomerLanguage to null as well. Setting this property to true while CustomerLanguage hasn't been fetched disables lazy loading for CustomerLanguage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerLanguage
		{
			get { return _alreadyFetchedCustomerLanguage;}
			set 
			{
				if(_alreadyFetchedCustomerLanguage && !value)
				{
					this.CustomerLanguage = null;
				}
				_alreadyFetchedCustomerLanguage = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CustomerLanguage is not found
		/// in the database. When set to true, CustomerLanguage will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CustomerLanguageReturnsNewIfNotFound
		{
			get	{ return _customerLanguageReturnsNewIfNotFound; }
			set { _customerLanguageReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity Person
		{
			get	{ return GetSinglePerson(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPerson(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomerAccounts", "Person", _person, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Person. When set to true, Person is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Person is accessed. You can always execute a forced fetch by calling GetSinglePerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerson
		{
			get	{ return _alwaysFetchPerson; }
			set	{ _alwaysFetchPerson = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Person already has been fetched. Setting this property to false when Person has been fetched
		/// will set Person to null as well. Setting this property to true while Person hasn't been fetched disables lazy loading for Person</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerson
		{
			get { return _alreadyFetchedPerson;}
			set 
			{
				if(_alreadyFetchedPerson && !value)
				{
					this.Person = null;
				}
				_alreadyFetchedPerson = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Person is not found
		/// in the database. When set to true, Person will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PersonReturnsNewIfNotFound
		{
			get	{ return _personReturnsNewIfNotFound; }
			set { _personReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CustomerAccountEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
