﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ProductRelation'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ProductRelationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AddressEntity _fromAddress;
		private bool	_alwaysFetchFromAddress, _alreadyFetchedFromAddress, _fromAddressReturnsNewIfNotFound;
		private AddressEntity _toAddress;
		private bool	_alwaysFetchToAddress, _alreadyFetchedToAddress, _toAddressReturnsNewIfNotFound;
		private ProductEntity _product;
		private bool	_alwaysFetchProduct, _alreadyFetchedProduct, _productReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name FromAddress</summary>
			public static readonly string FromAddress = "FromAddress";
			/// <summary>Member name ToAddress</summary>
			public static readonly string ToAddress = "ToAddress";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ProductRelationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ProductRelationEntity() :base("ProductRelationEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="productRelationID">PK value for ProductRelation which data should be fetched into this ProductRelation object</param>
		public ProductRelationEntity(System.Int64 productRelationID):base("ProductRelationEntity")
		{
			InitClassFetch(productRelationID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="productRelationID">PK value for ProductRelation which data should be fetched into this ProductRelation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProductRelationEntity(System.Int64 productRelationID, IPrefetchPath prefetchPathToUse):base("ProductRelationEntity")
		{
			InitClassFetch(productRelationID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="productRelationID">PK value for ProductRelation which data should be fetched into this ProductRelation object</param>
		/// <param name="validator">The custom validator object for this ProductRelationEntity</param>
		public ProductRelationEntity(System.Int64 productRelationID, IValidator validator):base("ProductRelationEntity")
		{
			InitClassFetch(productRelationID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductRelationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_fromAddress = (AddressEntity)info.GetValue("_fromAddress", typeof(AddressEntity));
			if(_fromAddress!=null)
			{
				_fromAddress.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fromAddressReturnsNewIfNotFound = info.GetBoolean("_fromAddressReturnsNewIfNotFound");
			_alwaysFetchFromAddress = info.GetBoolean("_alwaysFetchFromAddress");
			_alreadyFetchedFromAddress = info.GetBoolean("_alreadyFetchedFromAddress");

			_toAddress = (AddressEntity)info.GetValue("_toAddress", typeof(AddressEntity));
			if(_toAddress!=null)
			{
				_toAddress.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_toAddressReturnsNewIfNotFound = info.GetBoolean("_toAddressReturnsNewIfNotFound");
			_alwaysFetchToAddress = info.GetBoolean("_alwaysFetchToAddress");
			_alreadyFetchedToAddress = info.GetBoolean("_alreadyFetchedToAddress");
			_product = (ProductEntity)info.GetValue("_product", typeof(ProductEntity));
			if(_product!=null)
			{
				_product.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productReturnsNewIfNotFound = info.GetBoolean("_productReturnsNewIfNotFound");
			_alwaysFetchProduct = info.GetBoolean("_alwaysFetchProduct");
			_alreadyFetchedProduct = info.GetBoolean("_alreadyFetchedProduct");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ProductRelationFieldIndex)fieldIndex)
			{
				case ProductRelationFieldIndex.FromAddressID:
					DesetupSyncFromAddress(true, false);
					_alreadyFetchedFromAddress = false;
					break;
				case ProductRelationFieldIndex.ProductID:
					DesetupSyncProduct(true, false);
					_alreadyFetchedProduct = false;
					break;
				case ProductRelationFieldIndex.ToAddressID:
					DesetupSyncToAddress(true, false);
					_alreadyFetchedToAddress = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFromAddress = (_fromAddress != null);
			_alreadyFetchedToAddress = (_toAddress != null);
			_alreadyFetchedProduct = (_product != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "FromAddress":
					toReturn.Add(Relations.AddressEntityUsingFromAddressID);
					break;
				case "ToAddress":
					toReturn.Add(Relations.AddressEntityUsingToAddressID);
					break;
				case "Product":
					toReturn.Add(Relations.ProductEntityUsingProductID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_fromAddress", (!this.MarkedForDeletion?_fromAddress:null));
			info.AddValue("_fromAddressReturnsNewIfNotFound", _fromAddressReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFromAddress", _alwaysFetchFromAddress);
			info.AddValue("_alreadyFetchedFromAddress", _alreadyFetchedFromAddress);
			info.AddValue("_toAddress", (!this.MarkedForDeletion?_toAddress:null));
			info.AddValue("_toAddressReturnsNewIfNotFound", _toAddressReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchToAddress", _alwaysFetchToAddress);
			info.AddValue("_alreadyFetchedToAddress", _alreadyFetchedToAddress);

			info.AddValue("_product", (!this.MarkedForDeletion?_product:null));
			info.AddValue("_productReturnsNewIfNotFound", _productReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProduct", _alwaysFetchProduct);
			info.AddValue("_alreadyFetchedProduct", _alreadyFetchedProduct);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "FromAddress":
					_alreadyFetchedFromAddress = true;
					this.FromAddress = (AddressEntity)entity;
					break;
				case "ToAddress":
					_alreadyFetchedToAddress = true;
					this.ToAddress = (AddressEntity)entity;
					break;
				case "Product":
					_alreadyFetchedProduct = true;
					this.Product = (ProductEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "FromAddress":
					SetupSyncFromAddress(relatedEntity);
					break;
				case "ToAddress":
					SetupSyncToAddress(relatedEntity);
					break;
				case "Product":
					SetupSyncProduct(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "FromAddress":
					DesetupSyncFromAddress(false, true);
					break;
				case "ToAddress":
					DesetupSyncToAddress(false, true);
					break;
				case "Product":
					DesetupSyncProduct(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_fromAddress!=null)
			{
				toReturn.Add(_fromAddress);
			}
			if(_toAddress!=null)
			{
				toReturn.Add(_toAddress);
			}
			if(_product!=null)
			{
				toReturn.Add(_product);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="productID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCProductID(System.Int64 productID)
		{
			return FetchUsingUCProductID( productID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="productID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCProductID(System.Int64 productID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCProductID( productID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="productID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCProductID(System.Int64 productID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCProductID( productID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="productID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCProductID(System.Int64 productID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((ProductRelationDAO)CreateDAOInstance()).FetchProductRelationUsingUCProductID(this, this.Transaction, productID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productRelationID">PK value for ProductRelation which data should be fetched into this ProductRelation object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 productRelationID)
		{
			return FetchUsingPK(productRelationID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productRelationID">PK value for ProductRelation which data should be fetched into this ProductRelation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 productRelationID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(productRelationID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productRelationID">PK value for ProductRelation which data should be fetched into this ProductRelation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 productRelationID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(productRelationID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productRelationID">PK value for ProductRelation which data should be fetched into this ProductRelation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 productRelationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(productRelationID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ProductRelationID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ProductRelationRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public AddressEntity GetSingleFromAddress()
		{
			return GetSingleFromAddress(false);
		}

		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public virtual AddressEntity GetSingleFromAddress(bool forceFetch)
		{
			if( ( !_alreadyFetchedFromAddress || forceFetch || _alwaysFetchFromAddress) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AddressEntityUsingFromAddressID);
				AddressEntity newEntity = new AddressEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FromAddressID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AddressEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fromAddressReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FromAddress = newEntity;
				_alreadyFetchedFromAddress = fetchResult;
			}
			return _fromAddress;
		}


		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public AddressEntity GetSingleToAddress()
		{
			return GetSingleToAddress(false);
		}

		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public virtual AddressEntity GetSingleToAddress(bool forceFetch)
		{
			if( ( !_alreadyFetchedToAddress || forceFetch || _alwaysFetchToAddress) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AddressEntityUsingToAddressID);
				AddressEntity newEntity = new AddressEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ToAddressID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AddressEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_toAddressReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ToAddress = newEntity;
				_alreadyFetchedToAddress = fetchResult;
			}
			return _toAddress;
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProduct()
		{
			return GetSingleProduct(false);
		}
		
		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProduct(bool forceFetch)
		{
			if( ( !_alreadyFetchedProduct || forceFetch || _alwaysFetchProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductID);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductID);
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Product = newEntity;
				_alreadyFetchedProduct = fetchResult;
			}
			return _product;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("FromAddress", _fromAddress);
			toReturn.Add("ToAddress", _toAddress);
			toReturn.Add("Product", _product);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="productRelationID">PK value for ProductRelation which data should be fetched into this ProductRelation object</param>
		/// <param name="validator">The validator object for this ProductRelationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 productRelationID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(productRelationID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_fromAddressReturnsNewIfNotFound = false;
			_toAddressReturnsNewIfNotFound = false;
			_productReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Eav", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareMatrixEntryPriority", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromAddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductRelationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RelationFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RelationNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RelationTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RelationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RelationVia", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToAddressID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _fromAddress</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFromAddress(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fromAddress, new PropertyChangedEventHandler( OnFromAddressPropertyChanged ), "FromAddress", VarioSL.Entities.RelationClasses.StaticProductRelationRelations.AddressEntityUsingFromAddressIDStatic, true, signalRelatedEntity, "ProductRelationsFrom", resetFKFields, new int[] { (int)ProductRelationFieldIndex.FromAddressID } );		
			_fromAddress = null;
		}
		
		/// <summary> setups the sync logic for member _fromAddress</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFromAddress(IEntityCore relatedEntity)
		{
			if(_fromAddress!=relatedEntity)
			{		
				DesetupSyncFromAddress(true, true);
				_fromAddress = (AddressEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fromAddress, new PropertyChangedEventHandler( OnFromAddressPropertyChanged ), "FromAddress", VarioSL.Entities.RelationClasses.StaticProductRelationRelations.AddressEntityUsingFromAddressIDStatic, true, ref _alreadyFetchedFromAddress, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFromAddressPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _toAddress</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncToAddress(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _toAddress, new PropertyChangedEventHandler( OnToAddressPropertyChanged ), "ToAddress", VarioSL.Entities.RelationClasses.StaticProductRelationRelations.AddressEntityUsingToAddressIDStatic, true, signalRelatedEntity, "ProductRelationsTo", resetFKFields, new int[] { (int)ProductRelationFieldIndex.ToAddressID } );		
			_toAddress = null;
		}
		
		/// <summary> setups the sync logic for member _toAddress</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncToAddress(IEntityCore relatedEntity)
		{
			if(_toAddress!=relatedEntity)
			{		
				DesetupSyncToAddress(true, true);
				_toAddress = (AddressEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _toAddress, new PropertyChangedEventHandler( OnToAddressPropertyChanged ), "ToAddress", VarioSL.Entities.RelationClasses.StaticProductRelationRelations.AddressEntityUsingToAddressIDStatic, true, ref _alreadyFetchedToAddress, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnToAddressPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _product</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProduct(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticProductRelationRelations.ProductEntityUsingProductIDStatic, true, signalRelatedEntity, "ProductRelation", resetFKFields, new int[] { (int)ProductRelationFieldIndex.ProductID } );
			_product = null;
		}
	
		/// <summary> setups the sync logic for member _product</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProduct(IEntityCore relatedEntity)
		{
			if(_product!=relatedEntity)
			{
				DesetupSyncProduct(true, true);
				_product = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticProductRelationRelations.ProductEntityUsingProductIDStatic, true, ref _alreadyFetchedProduct, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="productRelationID">PK value for ProductRelation which data should be fetched into this ProductRelation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 productRelationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ProductRelationFieldIndex.ProductRelationID].ForcedCurrentValueWrite(productRelationID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateProductRelationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ProductRelationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ProductRelationRelations Relations
		{
			get	{ return new ProductRelationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Address'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFromAddress
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressCollection(), (IEntityRelation)GetRelationsForField("FromAddress")[0], (int)VarioSL.Entities.EntityType.ProductRelationEntity, (int)VarioSL.Entities.EntityType.AddressEntity, 0, null, null, null, "FromAddress", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Address'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathToAddress
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressCollection(), (IEntityRelation)GetRelationsForField("ToAddress")[0], (int)VarioSL.Entities.EntityType.ProductRelationEntity, (int)VarioSL.Entities.EntityType.AddressEntity, 0, null, null, null, "ToAddress", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProduct
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Product")[0], (int)VarioSL.Entities.EntityType.ProductRelationEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Product", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Eav property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."EAV"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Eav
		{
			get { return (System.String)GetValue((int)ProductRelationFieldIndex.Eav, true); }
			set	{ SetValue((int)ProductRelationFieldIndex.Eav, value, true); }
		}

		/// <summary> The FareMatrixEntryPriority property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."FAREMATRIXENTRYPRIORITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareMatrixEntryPriority
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductRelationFieldIndex.FareMatrixEntryPriority, false); }
			set	{ SetValue((int)ProductRelationFieldIndex.FareMatrixEntryPriority, value, true); }
		}

		/// <summary> The FareStage property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."FARESTAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FareStage
		{
			get { return (System.String)GetValue((int)ProductRelationFieldIndex.FareStage, true); }
			set	{ SetValue((int)ProductRelationFieldIndex.FareStage, value, true); }
		}

		/// <summary> The FromAddressID property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."FROMADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FromAddressID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductRelationFieldIndex.FromAddressID, false); }
			set	{ SetValue((int)ProductRelationFieldIndex.FromAddressID, value, true); }
		}

		/// <summary> The ProductID property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."PRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ProductID
		{
			get { return (System.Int64)GetValue((int)ProductRelationFieldIndex.ProductID, true); }
			set	{ SetValue((int)ProductRelationFieldIndex.ProductID, value, true); }
		}

		/// <summary> The ProductRelationID property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."PRODUCTRELATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ProductRelationID
		{
			get { return (System.Int64)GetValue((int)ProductRelationFieldIndex.ProductRelationID, true); }
			set	{ SetValue((int)ProductRelationFieldIndex.ProductRelationID, value, true); }
		}

		/// <summary> The RelationFrom property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."RELATIONFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RelationFrom
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductRelationFieldIndex.RelationFrom, false); }
			set	{ SetValue((int)ProductRelationFieldIndex.RelationFrom, value, true); }
		}

		/// <summary> The RelationNumber property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."RELATIONNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RelationNumber
		{
			get { return (System.String)GetValue((int)ProductRelationFieldIndex.RelationNumber, true); }
			set	{ SetValue((int)ProductRelationFieldIndex.RelationNumber, value, true); }
		}

		/// <summary> The RelationTo property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."RELATIONTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RelationTo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductRelationFieldIndex.RelationTo, false); }
			set	{ SetValue((int)ProductRelationFieldIndex.RelationTo, value, true); }
		}

		/// <summary> The RelationType property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."RELATIONTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.ProductRelationType> RelationType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.ProductRelationType>)GetValue((int)ProductRelationFieldIndex.RelationType, false); }
			set	{ SetValue((int)ProductRelationFieldIndex.RelationType, value, true); }
		}

		/// <summary> The RelationVia property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."RELATIONVIA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RelationVia
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductRelationFieldIndex.RelationVia, false); }
			set	{ SetValue((int)ProductRelationFieldIndex.RelationVia, value, true); }
		}

		/// <summary> The StopNumber property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."STOPNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> StopNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductRelationFieldIndex.StopNumber, false); }
			set	{ SetValue((int)ProductRelationFieldIndex.StopNumber, value, true); }
		}

		/// <summary> The ToAddressID property of the Entity ProductRelation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTRELATION"."TOADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ToAddressID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductRelationFieldIndex.ToAddressID, false); }
			set	{ SetValue((int)ProductRelationFieldIndex.ToAddressID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AddressEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFromAddress()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AddressEntity FromAddress
		{
			get	{ return GetSingleFromAddress(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFromAddress(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductRelationsFrom", "FromAddress", _fromAddress, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FromAddress. When set to true, FromAddress is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FromAddress is accessed. You can always execute a forced fetch by calling GetSingleFromAddress(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFromAddress
		{
			get	{ return _alwaysFetchFromAddress; }
			set	{ _alwaysFetchFromAddress = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FromAddress already has been fetched. Setting this property to false when FromAddress has been fetched
		/// will set FromAddress to null as well. Setting this property to true while FromAddress hasn't been fetched disables lazy loading for FromAddress</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFromAddress
		{
			get { return _alreadyFetchedFromAddress;}
			set 
			{
				if(_alreadyFetchedFromAddress && !value)
				{
					this.FromAddress = null;
				}
				_alreadyFetchedFromAddress = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FromAddress is not found
		/// in the database. When set to true, FromAddress will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FromAddressReturnsNewIfNotFound
		{
			get	{ return _fromAddressReturnsNewIfNotFound; }
			set { _fromAddressReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AddressEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleToAddress()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AddressEntity ToAddress
		{
			get	{ return GetSingleToAddress(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncToAddress(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductRelationsTo", "ToAddress", _toAddress, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ToAddress. When set to true, ToAddress is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ToAddress is accessed. You can always execute a forced fetch by calling GetSingleToAddress(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchToAddress
		{
			get	{ return _alwaysFetchToAddress; }
			set	{ _alwaysFetchToAddress = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ToAddress already has been fetched. Setting this property to false when ToAddress has been fetched
		/// will set ToAddress to null as well. Setting this property to true while ToAddress hasn't been fetched disables lazy loading for ToAddress</summary>
		[Browsable(false)]
		public bool AlreadyFetchedToAddress
		{
			get { return _alreadyFetchedToAddress;}
			set 
			{
				if(_alreadyFetchedToAddress && !value)
				{
					this.ToAddress = null;
				}
				_alreadyFetchedToAddress = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ToAddress is not found
		/// in the database. When set to true, ToAddress will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ToAddressReturnsNewIfNotFound
		{
			get	{ return _toAddressReturnsNewIfNotFound; }
			set { _toAddressReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get	{ return GetSingleProduct(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncProduct(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_product !=null);
						DesetupSyncProduct(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("Product");
						}
					}
					else
					{
						if(_product!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "ProductRelation");
							SetupSyncProduct(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Product. When set to true, Product is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Product is accessed. You can always execute a forced fetch by calling GetSingleProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProduct
		{
			get	{ return _alwaysFetchProduct; }
			set	{ _alwaysFetchProduct = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property Product already has been fetched. Setting this property to false when Product has been fetched
		/// will set Product to null as well. Setting this property to true while Product hasn't been fetched disables lazy loading for Product</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProduct
		{
			get { return _alreadyFetchedProduct;}
			set 
			{
				if(_alreadyFetchedProduct && !value)
				{
					this.Product = null;
				}
				_alreadyFetchedProduct = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Product is not found
		/// in the database. When set to true, Product will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProductReturnsNewIfNotFound
		{
			get	{ return _productReturnsNewIfNotFound; }
			set	{ _productReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ProductRelationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
