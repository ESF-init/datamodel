﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TicketDeviceClassOutputDevice'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TicketDeviceClassOutputDeviceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private OutputDeviceEntity _outputDevice;
		private bool	_alwaysFetchOutputDevice, _alreadyFetchedOutputDevice, _outputDeviceReturnsNewIfNotFound;
		private TicketDeviceClassEntity _ticketDeviceClass;
		private bool	_alwaysFetchTicketDeviceClass, _alreadyFetchedTicketDeviceClass, _ticketDeviceClassReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name OutputDevice</summary>
			public static readonly string OutputDevice = "OutputDevice";
			/// <summary>Member name TicketDeviceClass</summary>
			public static readonly string TicketDeviceClass = "TicketDeviceClass";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TicketDeviceClassOutputDeviceEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TicketDeviceClassOutputDeviceEntity() :base("TicketDeviceClassOutputDeviceEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="outputDeviceID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		public TicketDeviceClassOutputDeviceEntity(System.Int64 outputDeviceID, System.Int64 ticketDeviceClassID):base("TicketDeviceClassOutputDeviceEntity")
		{
			InitClassFetch(outputDeviceID, ticketDeviceClassID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="outputDeviceID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TicketDeviceClassOutputDeviceEntity(System.Int64 outputDeviceID, System.Int64 ticketDeviceClassID, IPrefetchPath prefetchPathToUse):base("TicketDeviceClassOutputDeviceEntity")
		{
			InitClassFetch(outputDeviceID, ticketDeviceClassID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="outputDeviceID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="validator">The custom validator object for this TicketDeviceClassOutputDeviceEntity</param>
		public TicketDeviceClassOutputDeviceEntity(System.Int64 outputDeviceID, System.Int64 ticketDeviceClassID, IValidator validator):base("TicketDeviceClassOutputDeviceEntity")
		{
			InitClassFetch(outputDeviceID, ticketDeviceClassID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TicketDeviceClassOutputDeviceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_outputDevice = (OutputDeviceEntity)info.GetValue("_outputDevice", typeof(OutputDeviceEntity));
			if(_outputDevice!=null)
			{
				_outputDevice.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_outputDeviceReturnsNewIfNotFound = info.GetBoolean("_outputDeviceReturnsNewIfNotFound");
			_alwaysFetchOutputDevice = info.GetBoolean("_alwaysFetchOutputDevice");
			_alreadyFetchedOutputDevice = info.GetBoolean("_alreadyFetchedOutputDevice");

			_ticketDeviceClass = (TicketDeviceClassEntity)info.GetValue("_ticketDeviceClass", typeof(TicketDeviceClassEntity));
			if(_ticketDeviceClass!=null)
			{
				_ticketDeviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketDeviceClassReturnsNewIfNotFound = info.GetBoolean("_ticketDeviceClassReturnsNewIfNotFound");
			_alwaysFetchTicketDeviceClass = info.GetBoolean("_alwaysFetchTicketDeviceClass");
			_alreadyFetchedTicketDeviceClass = info.GetBoolean("_alreadyFetchedTicketDeviceClass");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TicketDeviceClassOutputDeviceFieldIndex)fieldIndex)
			{
				case TicketDeviceClassOutputDeviceFieldIndex.OutputDeviceID:
					DesetupSyncOutputDevice(true, false);
					_alreadyFetchedOutputDevice = false;
					break;
				case TicketDeviceClassOutputDeviceFieldIndex.TicketDeviceClassID:
					DesetupSyncTicketDeviceClass(true, false);
					_alreadyFetchedTicketDeviceClass = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedOutputDevice = (_outputDevice != null);
			_alreadyFetchedTicketDeviceClass = (_ticketDeviceClass != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "OutputDevice":
					toReturn.Add(Relations.OutputDeviceEntityUsingOutputDeviceID);
					break;
				case "TicketDeviceClass":
					toReturn.Add(Relations.TicketDeviceClassEntityUsingTicketDeviceClassID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_outputDevice", (!this.MarkedForDeletion?_outputDevice:null));
			info.AddValue("_outputDeviceReturnsNewIfNotFound", _outputDeviceReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOutputDevice", _alwaysFetchOutputDevice);
			info.AddValue("_alreadyFetchedOutputDevice", _alreadyFetchedOutputDevice);
			info.AddValue("_ticketDeviceClass", (!this.MarkedForDeletion?_ticketDeviceClass:null));
			info.AddValue("_ticketDeviceClassReturnsNewIfNotFound", _ticketDeviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicketDeviceClass", _alwaysFetchTicketDeviceClass);
			info.AddValue("_alreadyFetchedTicketDeviceClass", _alreadyFetchedTicketDeviceClass);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "OutputDevice":
					_alreadyFetchedOutputDevice = true;
					this.OutputDevice = (OutputDeviceEntity)entity;
					break;
				case "TicketDeviceClass":
					_alreadyFetchedTicketDeviceClass = true;
					this.TicketDeviceClass = (TicketDeviceClassEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "OutputDevice":
					SetupSyncOutputDevice(relatedEntity);
					break;
				case "TicketDeviceClass":
					SetupSyncTicketDeviceClass(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "OutputDevice":
					DesetupSyncOutputDevice(false, true);
					break;
				case "TicketDeviceClass":
					DesetupSyncTicketDeviceClass(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_outputDevice!=null)
			{
				toReturn.Add(_outputDevice);
			}
			if(_ticketDeviceClass!=null)
			{
				toReturn.Add(_ticketDeviceClass);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outputDeviceID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 outputDeviceID, System.Int64 ticketDeviceClassID)
		{
			return FetchUsingPK(outputDeviceID, ticketDeviceClassID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outputDeviceID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 outputDeviceID, System.Int64 ticketDeviceClassID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(outputDeviceID, ticketDeviceClassID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outputDeviceID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 outputDeviceID, System.Int64 ticketDeviceClassID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(outputDeviceID, ticketDeviceClassID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outputDeviceID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 outputDeviceID, System.Int64 ticketDeviceClassID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(outputDeviceID, ticketDeviceClassID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OutputDeviceID, this.TicketDeviceClassID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TicketDeviceClassOutputDeviceRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'OutputDeviceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OutputDeviceEntity' which is related to this entity.</returns>
		public OutputDeviceEntity GetSingleOutputDevice()
		{
			return GetSingleOutputDevice(false);
		}

		/// <summary> Retrieves the related entity of type 'OutputDeviceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OutputDeviceEntity' which is related to this entity.</returns>
		public virtual OutputDeviceEntity GetSingleOutputDevice(bool forceFetch)
		{
			if( ( !_alreadyFetchedOutputDevice || forceFetch || _alwaysFetchOutputDevice) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OutputDeviceEntityUsingOutputDeviceID);
				OutputDeviceEntity newEntity = new OutputDeviceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OutputDeviceID);
				}
				if(fetchResult)
				{
					newEntity = (OutputDeviceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_outputDeviceReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OutputDevice = newEntity;
				_alreadyFetchedOutputDevice = fetchResult;
			}
			return _outputDevice;
		}


		/// <summary> Retrieves the related entity of type 'TicketDeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketDeviceClassEntity' which is related to this entity.</returns>
		public TicketDeviceClassEntity GetSingleTicketDeviceClass()
		{
			return GetSingleTicketDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketDeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketDeviceClassEntity' which is related to this entity.</returns>
		public virtual TicketDeviceClassEntity GetSingleTicketDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicketDeviceClass || forceFetch || _alwaysFetchTicketDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketDeviceClassEntityUsingTicketDeviceClassID);
				TicketDeviceClassEntity newEntity = new TicketDeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketDeviceClassID);
				}
				if(fetchResult)
				{
					newEntity = (TicketDeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketDeviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TicketDeviceClass = newEntity;
				_alreadyFetchedTicketDeviceClass = fetchResult;
			}
			return _ticketDeviceClass;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("OutputDevice", _outputDevice);
			toReturn.Add("TicketDeviceClass", _ticketDeviceClass);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="outputDeviceID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="validator">The validator object for this TicketDeviceClassOutputDeviceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 outputDeviceID, System.Int64 ticketDeviceClassID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(outputDeviceID, ticketDeviceClassID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_outputDeviceReturnsNewIfNotFound = false;
			_ticketDeviceClassReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutputDeviceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketDeviceClassID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _outputDevice</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOutputDevice(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _outputDevice, new PropertyChangedEventHandler( OnOutputDevicePropertyChanged ), "OutputDevice", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassOutputDeviceRelations.OutputDeviceEntityUsingOutputDeviceIDStatic, true, signalRelatedEntity, "TicketDeviceClassOutputDevice", resetFKFields, new int[] { (int)TicketDeviceClassOutputDeviceFieldIndex.OutputDeviceID } );		
			_outputDevice = null;
		}
		
		/// <summary> setups the sync logic for member _outputDevice</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOutputDevice(IEntityCore relatedEntity)
		{
			if(_outputDevice!=relatedEntity)
			{		
				DesetupSyncOutputDevice(true, true);
				_outputDevice = (OutputDeviceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _outputDevice, new PropertyChangedEventHandler( OnOutputDevicePropertyChanged ), "OutputDevice", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassOutputDeviceRelations.OutputDeviceEntityUsingOutputDeviceIDStatic, true, ref _alreadyFetchedOutputDevice, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOutputDevicePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticketDeviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicketDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticketDeviceClass, new PropertyChangedEventHandler( OnTicketDeviceClassPropertyChanged ), "TicketDeviceClass", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassOutputDeviceRelations.TicketDeviceClassEntityUsingTicketDeviceClassIDStatic, true, signalRelatedEntity, "TicketDeviceClassOutputDevice", resetFKFields, new int[] { (int)TicketDeviceClassOutputDeviceFieldIndex.TicketDeviceClassID } );		
			_ticketDeviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _ticketDeviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicketDeviceClass(IEntityCore relatedEntity)
		{
			if(_ticketDeviceClass!=relatedEntity)
			{		
				DesetupSyncTicketDeviceClass(true, true);
				_ticketDeviceClass = (TicketDeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticketDeviceClass, new PropertyChangedEventHandler( OnTicketDeviceClassPropertyChanged ), "TicketDeviceClass", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassOutputDeviceRelations.TicketDeviceClassEntityUsingTicketDeviceClassIDStatic, true, ref _alreadyFetchedTicketDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="outputDeviceID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClassOutputDevice which data should be fetched into this TicketDeviceClassOutputDevice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 outputDeviceID, System.Int64 ticketDeviceClassID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TicketDeviceClassOutputDeviceFieldIndex.OutputDeviceID].ForcedCurrentValueWrite(outputDeviceID);
				this.Fields[(int)TicketDeviceClassOutputDeviceFieldIndex.TicketDeviceClassID].ForcedCurrentValueWrite(ticketDeviceClassID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTicketDeviceClassOutputDeviceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TicketDeviceClassOutputDeviceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TicketDeviceClassOutputDeviceRelations Relations
		{
			get	{ return new TicketDeviceClassOutputDeviceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OutputDevice'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOutputDevice
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OutputDeviceCollection(), (IEntityRelation)GetRelationsForField("OutputDevice")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassOutputDeviceEntity, (int)VarioSL.Entities.EntityType.OutputDeviceEntity, 0, null, null, null, "OutputDevice", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceClass")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassOutputDeviceEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, null, "TicketDeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The OutputDeviceID property of the Entity TicketDeviceClassOutputDevice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TDC_OUTPUTDEVICE"."OUTPUTDEVICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 OutputDeviceID
		{
			get { return (System.Int64)GetValue((int)TicketDeviceClassOutputDeviceFieldIndex.OutputDeviceID, true); }
			set	{ SetValue((int)TicketDeviceClassOutputDeviceFieldIndex.OutputDeviceID, value, true); }
		}

		/// <summary> The TicketDeviceClassID property of the Entity TicketDeviceClassOutputDevice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TDC_OUTPUTDEVICE"."TICKETDEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 TicketDeviceClassID
		{
			get { return (System.Int64)GetValue((int)TicketDeviceClassOutputDeviceFieldIndex.TicketDeviceClassID, true); }
			set	{ SetValue((int)TicketDeviceClassOutputDeviceFieldIndex.TicketDeviceClassID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'OutputDeviceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOutputDevice()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OutputDeviceEntity OutputDevice
		{
			get	{ return GetSingleOutputDevice(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOutputDevice(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceClassOutputDevice", "OutputDevice", _outputDevice, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OutputDevice. When set to true, OutputDevice is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OutputDevice is accessed. You can always execute a forced fetch by calling GetSingleOutputDevice(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOutputDevice
		{
			get	{ return _alwaysFetchOutputDevice; }
			set	{ _alwaysFetchOutputDevice = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OutputDevice already has been fetched. Setting this property to false when OutputDevice has been fetched
		/// will set OutputDevice to null as well. Setting this property to true while OutputDevice hasn't been fetched disables lazy loading for OutputDevice</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOutputDevice
		{
			get { return _alreadyFetchedOutputDevice;}
			set 
			{
				if(_alreadyFetchedOutputDevice && !value)
				{
					this.OutputDevice = null;
				}
				_alreadyFetchedOutputDevice = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OutputDevice is not found
		/// in the database. When set to true, OutputDevice will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OutputDeviceReturnsNewIfNotFound
		{
			get	{ return _outputDeviceReturnsNewIfNotFound; }
			set { _outputDeviceReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketDeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicketDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketDeviceClassEntity TicketDeviceClass
		{
			get	{ return GetSingleTicketDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicketDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceClassOutputDevice", "TicketDeviceClass", _ticketDeviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceClass. When set to true, TicketDeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceClass is accessed. You can always execute a forced fetch by calling GetSingleTicketDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceClass
		{
			get	{ return _alwaysFetchTicketDeviceClass; }
			set	{ _alwaysFetchTicketDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceClass already has been fetched. Setting this property to false when TicketDeviceClass has been fetched
		/// will set TicketDeviceClass to null as well. Setting this property to true while TicketDeviceClass hasn't been fetched disables lazy loading for TicketDeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceClass
		{
			get { return _alreadyFetchedTicketDeviceClass;}
			set 
			{
				if(_alreadyFetchedTicketDeviceClass && !value)
				{
					this.TicketDeviceClass = null;
				}
				_alreadyFetchedTicketDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TicketDeviceClass is not found
		/// in the database. When set to true, TicketDeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketDeviceClassReturnsNewIfNotFound
		{
			get	{ return _ticketDeviceClassReturnsNewIfNotFound; }
			set { _ticketDeviceClassReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TicketDeviceClassOutputDeviceEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
