﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SchoolYear'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SchoolYearEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CardHolderCollection	_cardHolder;
		private bool	_alwaysFetchCardHolder, _alreadyFetchedCardHolder;
		private VarioSL.Entities.CollectionClasses.InvoiceCollection	_invoices;
		private bool	_alwaysFetchInvoices, _alreadyFetchedInvoices;
		private VarioSL.Entities.CollectionClasses.OrderCollection	_orders;
		private bool	_alwaysFetchOrders, _alreadyFetchedOrders;
		private VarioSL.Entities.CollectionClasses.OrderDetailCollection	_orderDetails;
		private bool	_alwaysFetchOrderDetails, _alreadyFetchedOrderDetails;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private SchoolYearEntity _followingSchoolYear;
		private bool	_alwaysFetchFollowingSchoolYear, _alreadyFetchedFollowingSchoolYear, _followingSchoolYearReturnsNewIfNotFound;
		private SchoolYearEntity _previousSchoolYear;
		private bool	_alwaysFetchPreviousSchoolYear, _alreadyFetchedPreviousSchoolYear, _previousSchoolYearReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name CardHolder</summary>
			public static readonly string CardHolder = "CardHolder";
			/// <summary>Member name Invoices</summary>
			public static readonly string Invoices = "Invoices";
			/// <summary>Member name Orders</summary>
			public static readonly string Orders = "Orders";
			/// <summary>Member name OrderDetails</summary>
			public static readonly string OrderDetails = "OrderDetails";
			/// <summary>Member name FollowingSchoolYear</summary>
			public static readonly string FollowingSchoolYear = "FollowingSchoolYear";
			/// <summary>Member name PreviousSchoolYear</summary>
			public static readonly string PreviousSchoolYear = "PreviousSchoolYear";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SchoolYearEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SchoolYearEntity() :base("SchoolYearEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="schoolYearID">PK value for SchoolYear which data should be fetched into this SchoolYear object</param>
		public SchoolYearEntity(System.Int64 schoolYearID):base("SchoolYearEntity")
		{
			InitClassFetch(schoolYearID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="schoolYearID">PK value for SchoolYear which data should be fetched into this SchoolYear object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SchoolYearEntity(System.Int64 schoolYearID, IPrefetchPath prefetchPathToUse):base("SchoolYearEntity")
		{
			InitClassFetch(schoolYearID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="schoolYearID">PK value for SchoolYear which data should be fetched into this SchoolYear object</param>
		/// <param name="validator">The custom validator object for this SchoolYearEntity</param>
		public SchoolYearEntity(System.Int64 schoolYearID, IValidator validator):base("SchoolYearEntity")
		{
			InitClassFetch(schoolYearID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SchoolYearEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cardHolder = (VarioSL.Entities.CollectionClasses.CardHolderCollection)info.GetValue("_cardHolder", typeof(VarioSL.Entities.CollectionClasses.CardHolderCollection));
			_alwaysFetchCardHolder = info.GetBoolean("_alwaysFetchCardHolder");
			_alreadyFetchedCardHolder = info.GetBoolean("_alreadyFetchedCardHolder");

			_invoices = (VarioSL.Entities.CollectionClasses.InvoiceCollection)info.GetValue("_invoices", typeof(VarioSL.Entities.CollectionClasses.InvoiceCollection));
			_alwaysFetchInvoices = info.GetBoolean("_alwaysFetchInvoices");
			_alreadyFetchedInvoices = info.GetBoolean("_alreadyFetchedInvoices");

			_orders = (VarioSL.Entities.CollectionClasses.OrderCollection)info.GetValue("_orders", typeof(VarioSL.Entities.CollectionClasses.OrderCollection));
			_alwaysFetchOrders = info.GetBoolean("_alwaysFetchOrders");
			_alreadyFetchedOrders = info.GetBoolean("_alreadyFetchedOrders");

			_orderDetails = (VarioSL.Entities.CollectionClasses.OrderDetailCollection)info.GetValue("_orderDetails", typeof(VarioSL.Entities.CollectionClasses.OrderDetailCollection));
			_alwaysFetchOrderDetails = info.GetBoolean("_alwaysFetchOrderDetails");
			_alreadyFetchedOrderDetails = info.GetBoolean("_alreadyFetchedOrderDetails");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");
			_followingSchoolYear = (SchoolYearEntity)info.GetValue("_followingSchoolYear", typeof(SchoolYearEntity));
			if(_followingSchoolYear!=null)
			{
				_followingSchoolYear.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_followingSchoolYearReturnsNewIfNotFound = info.GetBoolean("_followingSchoolYearReturnsNewIfNotFound");
			_alwaysFetchFollowingSchoolYear = info.GetBoolean("_alwaysFetchFollowingSchoolYear");
			_alreadyFetchedFollowingSchoolYear = info.GetBoolean("_alreadyFetchedFollowingSchoolYear");

			_previousSchoolYear = (SchoolYearEntity)info.GetValue("_previousSchoolYear", typeof(SchoolYearEntity));
			if(_previousSchoolYear!=null)
			{
				_previousSchoolYear.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_previousSchoolYearReturnsNewIfNotFound = info.GetBoolean("_previousSchoolYearReturnsNewIfNotFound");
			_alwaysFetchPreviousSchoolYear = info.GetBoolean("_alwaysFetchPreviousSchoolYear");
			_alreadyFetchedPreviousSchoolYear = info.GetBoolean("_alreadyFetchedPreviousSchoolYear");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SchoolYearFieldIndex)fieldIndex)
			{
				case SchoolYearFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case SchoolYearFieldIndex.PreviousSchoolYearID:
					DesetupSyncFollowingSchoolYear(true, false);
					_alreadyFetchedFollowingSchoolYear = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCardHolder = (_cardHolder.Count > 0);
			_alreadyFetchedInvoices = (_invoices.Count > 0);
			_alreadyFetchedOrders = (_orders.Count > 0);
			_alreadyFetchedOrderDetails = (_orderDetails.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedFollowingSchoolYear = (_followingSchoolYear != null);
			_alreadyFetchedPreviousSchoolYear = (_previousSchoolYear != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "CardHolder":
					toReturn.Add(Relations.CardHolderEntityUsingSchoolYearID);
					break;
				case "Invoices":
					toReturn.Add(Relations.InvoiceEntityUsingSchoolYearID);
					break;
				case "Orders":
					toReturn.Add(Relations.OrderEntityUsingSchoolYearID);
					break;
				case "OrderDetails":
					toReturn.Add(Relations.OrderDetailEntityUsingSchoolyearID);
					break;
				case "FollowingSchoolYear":
					toReturn.Add(Relations.SchoolYearEntityUsingSchoolYearID);
					break;
				case "PreviousSchoolYear":
					toReturn.Add(Relations.SchoolYearEntityUsingPreviousSchoolYearID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cardHolder", (!this.MarkedForDeletion?_cardHolder:null));
			info.AddValue("_alwaysFetchCardHolder", _alwaysFetchCardHolder);
			info.AddValue("_alreadyFetchedCardHolder", _alreadyFetchedCardHolder);
			info.AddValue("_invoices", (!this.MarkedForDeletion?_invoices:null));
			info.AddValue("_alwaysFetchInvoices", _alwaysFetchInvoices);
			info.AddValue("_alreadyFetchedInvoices", _alreadyFetchedInvoices);
			info.AddValue("_orders", (!this.MarkedForDeletion?_orders:null));
			info.AddValue("_alwaysFetchOrders", _alwaysFetchOrders);
			info.AddValue("_alreadyFetchedOrders", _alreadyFetchedOrders);
			info.AddValue("_orderDetails", (!this.MarkedForDeletion?_orderDetails:null));
			info.AddValue("_alwaysFetchOrderDetails", _alwaysFetchOrderDetails);
			info.AddValue("_alreadyFetchedOrderDetails", _alreadyFetchedOrderDetails);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);

			info.AddValue("_followingSchoolYear", (!this.MarkedForDeletion?_followingSchoolYear:null));
			info.AddValue("_followingSchoolYearReturnsNewIfNotFound", _followingSchoolYearReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFollowingSchoolYear", _alwaysFetchFollowingSchoolYear);
			info.AddValue("_alreadyFetchedFollowingSchoolYear", _alreadyFetchedFollowingSchoolYear);

			info.AddValue("_previousSchoolYear", (!this.MarkedForDeletion?_previousSchoolYear:null));
			info.AddValue("_previousSchoolYearReturnsNewIfNotFound", _previousSchoolYearReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPreviousSchoolYear", _alwaysFetchPreviousSchoolYear);
			info.AddValue("_alreadyFetchedPreviousSchoolYear", _alreadyFetchedPreviousSchoolYear);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "CardHolder":
					_alreadyFetchedCardHolder = true;
					if(entity!=null)
					{
						this.CardHolder.Add((CardHolderEntity)entity);
					}
					break;
				case "Invoices":
					_alreadyFetchedInvoices = true;
					if(entity!=null)
					{
						this.Invoices.Add((InvoiceEntity)entity);
					}
					break;
				case "Orders":
					_alreadyFetchedOrders = true;
					if(entity!=null)
					{
						this.Orders.Add((OrderEntity)entity);
					}
					break;
				case "OrderDetails":
					_alreadyFetchedOrderDetails = true;
					if(entity!=null)
					{
						this.OrderDetails.Add((OrderDetailEntity)entity);
					}
					break;
				case "FollowingSchoolYear":
					_alreadyFetchedFollowingSchoolYear = true;
					this.FollowingSchoolYear = (SchoolYearEntity)entity;
					break;
				case "PreviousSchoolYear":
					_alreadyFetchedPreviousSchoolYear = true;
					this.PreviousSchoolYear = (SchoolYearEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "CardHolder":
					_cardHolder.Add((CardHolderEntity)relatedEntity);
					break;
				case "Invoices":
					_invoices.Add((InvoiceEntity)relatedEntity);
					break;
				case "Orders":
					_orders.Add((OrderEntity)relatedEntity);
					break;
				case "OrderDetails":
					_orderDetails.Add((OrderDetailEntity)relatedEntity);
					break;
				case "FollowingSchoolYear":
					SetupSyncFollowingSchoolYear(relatedEntity);
					break;
				case "PreviousSchoolYear":
					SetupSyncPreviousSchoolYear(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "CardHolder":
					this.PerformRelatedEntityRemoval(_cardHolder, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Invoices":
					this.PerformRelatedEntityRemoval(_invoices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Orders":
					this.PerformRelatedEntityRemoval(_orders, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderDetails":
					this.PerformRelatedEntityRemoval(_orderDetails, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FollowingSchoolYear":
					DesetupSyncFollowingSchoolYear(false, true);
					break;
				case "PreviousSchoolYear":
					DesetupSyncPreviousSchoolYear(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_previousSchoolYear!=null)
			{
				toReturn.Add(_previousSchoolYear);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_followingSchoolYear!=null)
			{
				toReturn.Add(_followingSchoolYear);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cardHolder);
			toReturn.Add(_invoices);
			toReturn.Add(_orders);
			toReturn.Add(_orderDetails);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="previousSchoolYearID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPreviousSchoolYearID(Nullable<System.Int64> previousSchoolYearID)
		{
			return FetchUsingUCPreviousSchoolYearID( previousSchoolYearID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="previousSchoolYearID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPreviousSchoolYearID(Nullable<System.Int64> previousSchoolYearID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCPreviousSchoolYearID( previousSchoolYearID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="previousSchoolYearID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPreviousSchoolYearID(Nullable<System.Int64> previousSchoolYearID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCPreviousSchoolYearID( previousSchoolYearID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="previousSchoolYearID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPreviousSchoolYearID(Nullable<System.Int64> previousSchoolYearID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((SchoolYearDAO)CreateDAOInstance()).FetchSchoolYearUsingUCPreviousSchoolYearID(this, this.Transaction, previousSchoolYearID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="schoolYearID">PK value for SchoolYear which data should be fetched into this SchoolYear object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 schoolYearID)
		{
			return FetchUsingPK(schoolYearID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="schoolYearID">PK value for SchoolYear which data should be fetched into this SchoolYear object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 schoolYearID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(schoolYearID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="schoolYearID">PK value for SchoolYear which data should be fetched into this SchoolYear object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 schoolYearID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(schoolYearID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="schoolYearID">PK value for SchoolYear which data should be fetched into this SchoolYear object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 schoolYearID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(schoolYearID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SchoolYearID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SchoolYearRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardHolderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardHolderCollection GetMultiCardHolder(bool forceFetch)
		{
			return GetMultiCardHolder(forceFetch, _cardHolder.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardHolderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardHolderCollection GetMultiCardHolder(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardHolder(forceFetch, _cardHolder.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardHolderCollection GetMultiCardHolder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardHolder(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardHolderCollection GetMultiCardHolder(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardHolder || forceFetch || _alwaysFetchCardHolder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardHolder);
				_cardHolder.SuppressClearInGetMulti=!forceFetch;
				_cardHolder.EntityFactoryToUse = entityFactoryToUse;
				_cardHolder.GetMultiManyToOne(null, this, filter);
				_cardHolder.SuppressClearInGetMulti=false;
				_alreadyFetchedCardHolder = true;
			}
			return _cardHolder;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardHolder'. These settings will be taken into account
		/// when the property CardHolder is requested or GetMultiCardHolder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardHolder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardHolder.SortClauses=sortClauses;
			_cardHolder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch)
		{
			return GetMultiInvoices(forceFetch, _invoices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiInvoices(forceFetch, _invoices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiInvoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedInvoices || forceFetch || _alwaysFetchInvoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_invoices);
				_invoices.SuppressClearInGetMulti=!forceFetch;
				_invoices.EntityFactoryToUse = entityFactoryToUse;
				_invoices.GetMultiManyToOne(null, null, null, null, null, this, filter);
				_invoices.SuppressClearInGetMulti=false;
				_alreadyFetchedInvoices = true;
			}
			return _invoices;
		}

		/// <summary> Sets the collection parameters for the collection for 'Invoices'. These settings will be taken into account
		/// when the property Invoices is requested or GetMultiInvoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersInvoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_invoices.SortClauses=sortClauses;
			_invoices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch)
		{
			return GetMultiOrders(forceFetch, _orders.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrders(forceFetch, _orders.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrders(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrders || forceFetch || _alwaysFetchOrders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orders);
				_orders.SuppressClearInGetMulti=!forceFetch;
				_orders.EntityFactoryToUse = entityFactoryToUse;
				_orders.GetMultiManyToOne(null, null, null, null, null, this, filter);
				_orders.SuppressClearInGetMulti=false;
				_alreadyFetchedOrders = true;
			}
			return _orders;
		}

		/// <summary> Sets the collection parameters for the collection for 'Orders'. These settings will be taken into account
		/// when the property Orders is requested or GetMultiOrders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orders.SortClauses=sortClauses;
			_orders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch)
		{
			return GetMultiOrderDetails(forceFetch, _orderDetails.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderDetails(forceFetch, _orderDetails.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderDetails(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderDetails || forceFetch || _alwaysFetchOrderDetails) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderDetails);
				_orderDetails.SuppressClearInGetMulti=!forceFetch;
				_orderDetails.EntityFactoryToUse = entityFactoryToUse;
				_orderDetails.GetMultiManyToOne(null, null, null, null, null, this, filter);
				_orderDetails.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderDetails = true;
			}
			return _orderDetails;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderDetails'. These settings will be taken into account
		/// when the property OrderDetails is requested or GetMultiOrderDetails is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderDetails(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderDetails.SortClauses=sortClauses;
			_orderDetails.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}

		/// <summary> Retrieves the related entity of type 'SchoolYearEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'SchoolYearEntity' which is related to this entity.</returns>
		public SchoolYearEntity GetSingleFollowingSchoolYear()
		{
			return GetSingleFollowingSchoolYear(false);
		}
		
		/// <summary> Retrieves the related entity of type 'SchoolYearEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SchoolYearEntity' which is related to this entity.</returns>
		public virtual SchoolYearEntity GetSingleFollowingSchoolYear(bool forceFetch)
		{
			if( ( !_alreadyFetchedFollowingSchoolYear || forceFetch || _alwaysFetchFollowingSchoolYear) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SchoolYearEntityUsingPreviousSchoolYearID);
				SchoolYearEntity newEntity = new SchoolYearEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PreviousSchoolYearID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SchoolYearEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_followingSchoolYearReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FollowingSchoolYear = newEntity;
				_alreadyFetchedFollowingSchoolYear = fetchResult;
			}
			return _followingSchoolYear;
		}

		/// <summary> Retrieves the related entity of type 'SchoolYearEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'SchoolYearEntity' which is related to this entity.</returns>
		public SchoolYearEntity GetSinglePreviousSchoolYear()
		{
			return GetSinglePreviousSchoolYear(false);
		}
		
		/// <summary> Retrieves the related entity of type 'SchoolYearEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SchoolYearEntity' which is related to this entity.</returns>
		public virtual SchoolYearEntity GetSinglePreviousSchoolYear(bool forceFetch)
		{
			if( ( !_alreadyFetchedPreviousSchoolYear || forceFetch || _alwaysFetchPreviousSchoolYear) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SchoolYearEntityUsingPreviousSchoolYearID);
				SchoolYearEntity newEntity = new SchoolYearEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCPreviousSchoolYearID(this.SchoolYearID);
				}
				if(fetchResult)
				{
					newEntity = (SchoolYearEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_previousSchoolYearReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PreviousSchoolYear = newEntity;
				_alreadyFetchedPreviousSchoolYear = fetchResult;
			}
			return _previousSchoolYear;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("CardHolder", _cardHolder);
			toReturn.Add("Invoices", _invoices);
			toReturn.Add("Orders", _orders);
			toReturn.Add("OrderDetails", _orderDetails);
			toReturn.Add("FollowingSchoolYear", _followingSchoolYear);
			toReturn.Add("PreviousSchoolYear", _previousSchoolYear);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="schoolYearID">PK value for SchoolYear which data should be fetched into this SchoolYear object</param>
		/// <param name="validator">The validator object for this SchoolYearEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 schoolYearID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(schoolYearID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cardHolder = new VarioSL.Entities.CollectionClasses.CardHolderCollection();
			_cardHolder.SetContainingEntityInfo(this, "SchoolYear");

			_invoices = new VarioSL.Entities.CollectionClasses.InvoiceCollection();
			_invoices.SetContainingEntityInfo(this, "SchoolYear");

			_orders = new VarioSL.Entities.CollectionClasses.OrderCollection();
			_orders.SetContainingEntityInfo(this, "SchoolYear");

			_orderDetails = new VarioSL.Entities.CollectionClasses.OrderDetailCollection();
			_orderDetails.SetContainingEntityInfo(this, "SchoolYear");
			_clientReturnsNewIfNotFound = false;
			_followingSchoolYearReturnsNewIfNotFound = false;
			_previousSchoolYearReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCurrentSchoolYear", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumberOfAccountingMonths", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumberOfMonths", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PreviousSchoolYearID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SchoolYearID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SchoolYearName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticSchoolYearRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "SchoolYears", resetFKFields, new int[] { (int)SchoolYearFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticSchoolYearRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _followingSchoolYear</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFollowingSchoolYear(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _followingSchoolYear, new PropertyChangedEventHandler( OnFollowingSchoolYearPropertyChanged ), "FollowingSchoolYear", VarioSL.Entities.RelationClasses.StaticSchoolYearRelations.SchoolYearEntityUsingPreviousSchoolYearIDStatic, true, signalRelatedEntity, "PreviousSchoolYear", resetFKFields, new int[] { (int)SchoolYearFieldIndex.PreviousSchoolYearID } );
			_followingSchoolYear = null;
		}
	
		/// <summary> setups the sync logic for member _followingSchoolYear</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFollowingSchoolYear(IEntityCore relatedEntity)
		{
			if(_followingSchoolYear!=relatedEntity)
			{
				DesetupSyncFollowingSchoolYear(true, true);
				_followingSchoolYear = (SchoolYearEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _followingSchoolYear, new PropertyChangedEventHandler( OnFollowingSchoolYearPropertyChanged ), "FollowingSchoolYear", VarioSL.Entities.RelationClasses.StaticSchoolYearRelations.SchoolYearEntityUsingPreviousSchoolYearIDStatic, true, ref _alreadyFetchedFollowingSchoolYear, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFollowingSchoolYearPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _previousSchoolYear</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPreviousSchoolYear(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _previousSchoolYear, new PropertyChangedEventHandler( OnPreviousSchoolYearPropertyChanged ), "PreviousSchoolYear", VarioSL.Entities.RelationClasses.StaticSchoolYearRelations.SchoolYearEntityUsingPreviousSchoolYearIDStatic, false, signalRelatedEntity, "FollowingSchoolYear", false, new int[] { (int)SchoolYearFieldIndex.SchoolYearID } );
			_previousSchoolYear = null;
		}
	
		/// <summary> setups the sync logic for member _previousSchoolYear</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPreviousSchoolYear(IEntityCore relatedEntity)
		{
			if(_previousSchoolYear!=relatedEntity)
			{
				DesetupSyncPreviousSchoolYear(true, true);
				_previousSchoolYear = (SchoolYearEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _previousSchoolYear, new PropertyChangedEventHandler( OnPreviousSchoolYearPropertyChanged ), "PreviousSchoolYear", VarioSL.Entities.RelationClasses.StaticSchoolYearRelations.SchoolYearEntityUsingPreviousSchoolYearIDStatic, false, ref _alreadyFetchedPreviousSchoolYear, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPreviousSchoolYearPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="schoolYearID">PK value for SchoolYear which data should be fetched into this SchoolYear object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 schoolYearID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SchoolYearFieldIndex.SchoolYearID].ForcedCurrentValueWrite(schoolYearID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSchoolYearDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SchoolYearEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SchoolYearRelations Relations
		{
			get	{ return new SchoolYearRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardHolder' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardHolder
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardHolderCollection(), (IEntityRelation)GetRelationsForField("CardHolder")[0], (int)VarioSL.Entities.EntityType.SchoolYearEntity, (int)VarioSL.Entities.EntityType.CardHolderEntity, 0, null, null, null, "CardHolder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoiceCollection(), (IEntityRelation)GetRelationsForField("Invoices")[0], (int)VarioSL.Entities.EntityType.SchoolYearEntity, (int)VarioSL.Entities.EntityType.InvoiceEntity, 0, null, null, null, "Invoices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrders
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("Orders")[0], (int)VarioSL.Entities.EntityType.SchoolYearEntity, (int)VarioSL.Entities.EntityType.OrderEntity, 0, null, null, null, "Orders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetail' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderDetails
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailCollection(), (IEntityRelation)GetRelationsForField("OrderDetails")[0], (int)VarioSL.Entities.EntityType.SchoolYearEntity, (int)VarioSL.Entities.EntityType.OrderDetailEntity, 0, null, null, null, "OrderDetails", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.SchoolYearEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SchoolYear'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFollowingSchoolYear
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SchoolYearCollection(), (IEntityRelation)GetRelationsForField("FollowingSchoolYear")[0], (int)VarioSL.Entities.EntityType.SchoolYearEntity, (int)VarioSL.Entities.EntityType.SchoolYearEntity, 0, null, null, null, "FollowingSchoolYear", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SchoolYear'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPreviousSchoolYear
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SchoolYearCollection(), (IEntityRelation)GetRelationsForField("PreviousSchoolYear")[0], (int)VarioSL.Entities.EntityType.SchoolYearEntity, (int)VarioSL.Entities.EntityType.SchoolYearEntity, 0, null, null, null, "PreviousSchoolYear", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientID property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)SchoolYearFieldIndex.ClientID, true); }
			set	{ SetValue((int)SchoolYearFieldIndex.ClientID, value, true); }
		}

		/// <summary> The Description property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)SchoolYearFieldIndex.Description, true); }
			set	{ SetValue((int)SchoolYearFieldIndex.Description, value, true); }
		}

		/// <summary> The EndDate property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."ENDDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime EndDate
		{
			get { return (System.DateTime)GetValue((int)SchoolYearFieldIndex.EndDate, true); }
			set	{ SetValue((int)SchoolYearFieldIndex.EndDate, value, true); }
		}

		/// <summary> The IsActive property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."ISACTIVE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsActive
		{
			get { return (System.Boolean)GetValue((int)SchoolYearFieldIndex.IsActive, true); }
			set	{ SetValue((int)SchoolYearFieldIndex.IsActive, value, true); }
		}

		/// <summary> The IsCurrentSchoolYear property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."ISCURRENTSCHOOLYEAR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsCurrentSchoolYear
		{
			get { return (Nullable<System.Boolean>)GetValue((int)SchoolYearFieldIndex.IsCurrentSchoolYear, false); }
			set	{ SetValue((int)SchoolYearFieldIndex.IsCurrentSchoolYear, value, true); }
		}

		/// <summary> The LastModified property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SchoolYearFieldIndex.LastModified, true); }
			set	{ SetValue((int)SchoolYearFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SchoolYearFieldIndex.LastUser, true); }
			set	{ SetValue((int)SchoolYearFieldIndex.LastUser, value, true); }
		}

		/// <summary> The NumberOfAccountingMonths property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."NUMBEROFACCOUNTINGMONTHS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 NumberOfAccountingMonths
		{
			get { return (System.Int64)GetValue((int)SchoolYearFieldIndex.NumberOfAccountingMonths, true); }
			set	{ SetValue((int)SchoolYearFieldIndex.NumberOfAccountingMonths, value, true); }
		}

		/// <summary> The NumberOfMonths property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."NUMBEROFMONTHS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 NumberOfMonths
		{
			get { return (System.Int64)GetValue((int)SchoolYearFieldIndex.NumberOfMonths, true); }
			set	{ SetValue((int)SchoolYearFieldIndex.NumberOfMonths, value, true); }
		}

		/// <summary> The PreviousSchoolYearID property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."PREVIOUSSCHOOLYEARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PreviousSchoolYearID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SchoolYearFieldIndex.PreviousSchoolYearID, false); }
			set	{ SetValue((int)SchoolYearFieldIndex.PreviousSchoolYearID, value, true); }
		}

		/// <summary> The SchoolYearID property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."SCHOOLYEARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SchoolYearID
		{
			get { return (System.Int64)GetValue((int)SchoolYearFieldIndex.SchoolYearID, true); }
			set	{ SetValue((int)SchoolYearFieldIndex.SchoolYearID, value, true); }
		}

		/// <summary> The SchoolYearName property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."SCHOOLYEARNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SchoolYearName
		{
			get { return (System.String)GetValue((int)SchoolYearFieldIndex.SchoolYearName, true); }
			set	{ SetValue((int)SchoolYearFieldIndex.SchoolYearName, value, true); }
		}

		/// <summary> The StartDate property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."STARTDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime StartDate
		{
			get { return (System.DateTime)GetValue((int)SchoolYearFieldIndex.StartDate, true); }
			set	{ SetValue((int)SchoolYearFieldIndex.StartDate, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity SchoolYear<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SCHOOLYEAR"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SchoolYearFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SchoolYearFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardHolder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardHolderCollection CardHolder
		{
			get	{ return GetMultiCardHolder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardHolder. When set to true, CardHolder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardHolder is accessed. You can always execute/ a forced fetch by calling GetMultiCardHolder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardHolder
		{
			get	{ return _alwaysFetchCardHolder; }
			set	{ _alwaysFetchCardHolder = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardHolder already has been fetched. Setting this property to false when CardHolder has been fetched
		/// will clear the CardHolder collection well. Setting this property to true while CardHolder hasn't been fetched disables lazy loading for CardHolder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardHolder
		{
			get { return _alreadyFetchedCardHolder;}
			set 
			{
				if(_alreadyFetchedCardHolder && !value && (_cardHolder != null))
				{
					_cardHolder.Clear();
				}
				_alreadyFetchedCardHolder = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiInvoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceCollection Invoices
		{
			get	{ return GetMultiInvoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Invoices. When set to true, Invoices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoices is accessed. You can always execute/ a forced fetch by calling GetMultiInvoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoices
		{
			get	{ return _alwaysFetchInvoices; }
			set	{ _alwaysFetchInvoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoices already has been fetched. Setting this property to false when Invoices has been fetched
		/// will clear the Invoices collection well. Setting this property to true while Invoices hasn't been fetched disables lazy loading for Invoices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoices
		{
			get { return _alreadyFetchedInvoices;}
			set 
			{
				if(_alreadyFetchedInvoices && !value && (_invoices != null))
				{
					_invoices.Clear();
				}
				_alreadyFetchedInvoices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderCollection Orders
		{
			get	{ return GetMultiOrders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Orders. When set to true, Orders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Orders is accessed. You can always execute/ a forced fetch by calling GetMultiOrders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrders
		{
			get	{ return _alwaysFetchOrders; }
			set	{ _alwaysFetchOrders = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Orders already has been fetched. Setting this property to false when Orders has been fetched
		/// will clear the Orders collection well. Setting this property to true while Orders hasn't been fetched disables lazy loading for Orders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrders
		{
			get { return _alreadyFetchedOrders;}
			set 
			{
				if(_alreadyFetchedOrders && !value && (_orders != null))
				{
					_orders.Clear();
				}
				_alreadyFetchedOrders = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderDetails()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailCollection OrderDetails
		{
			get	{ return GetMultiOrderDetails(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderDetails. When set to true, OrderDetails is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderDetails is accessed. You can always execute/ a forced fetch by calling GetMultiOrderDetails(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderDetails
		{
			get	{ return _alwaysFetchOrderDetails; }
			set	{ _alwaysFetchOrderDetails = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderDetails already has been fetched. Setting this property to false when OrderDetails has been fetched
		/// will clear the OrderDetails collection well. Setting this property to true while OrderDetails hasn't been fetched disables lazy loading for OrderDetails</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderDetails
		{
			get { return _alreadyFetchedOrderDetails;}
			set 
			{
				if(_alreadyFetchedOrderDetails && !value && (_orderDetails != null))
				{
					_orderDetails.Clear();
				}
				_alreadyFetchedOrderDetails = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SchoolYears", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SchoolYearEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFollowingSchoolYear()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SchoolYearEntity FollowingSchoolYear
		{
			get	{ return GetSingleFollowingSchoolYear(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncFollowingSchoolYear(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_followingSchoolYear !=null);
						DesetupSyncFollowingSchoolYear(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("FollowingSchoolYear");
						}
					}
					else
					{
						if(_followingSchoolYear!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "PreviousSchoolYear");
							SetupSyncFollowingSchoolYear(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FollowingSchoolYear. When set to true, FollowingSchoolYear is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FollowingSchoolYear is accessed. You can always execute a forced fetch by calling GetSingleFollowingSchoolYear(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFollowingSchoolYear
		{
			get	{ return _alwaysFetchFollowingSchoolYear; }
			set	{ _alwaysFetchFollowingSchoolYear = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property FollowingSchoolYear already has been fetched. Setting this property to false when FollowingSchoolYear has been fetched
		/// will set FollowingSchoolYear to null as well. Setting this property to true while FollowingSchoolYear hasn't been fetched disables lazy loading for FollowingSchoolYear</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFollowingSchoolYear
		{
			get { return _alreadyFetchedFollowingSchoolYear;}
			set 
			{
				if(_alreadyFetchedFollowingSchoolYear && !value)
				{
					this.FollowingSchoolYear = null;
				}
				_alreadyFetchedFollowingSchoolYear = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FollowingSchoolYear is not found
		/// in the database. When set to true, FollowingSchoolYear will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FollowingSchoolYearReturnsNewIfNotFound
		{
			get	{ return _followingSchoolYearReturnsNewIfNotFound; }
			set	{ _followingSchoolYearReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'SchoolYearEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePreviousSchoolYear()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SchoolYearEntity PreviousSchoolYear
		{
			get	{ return GetSinglePreviousSchoolYear(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPreviousSchoolYear(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_previousSchoolYear !=null);
						DesetupSyncPreviousSchoolYear(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("PreviousSchoolYear");
						}
					}
					else
					{
						if(_previousSchoolYear!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "FollowingSchoolYear");
							SetupSyncPreviousSchoolYear(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PreviousSchoolYear. When set to true, PreviousSchoolYear is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PreviousSchoolYear is accessed. You can always execute a forced fetch by calling GetSinglePreviousSchoolYear(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPreviousSchoolYear
		{
			get	{ return _alwaysFetchPreviousSchoolYear; }
			set	{ _alwaysFetchPreviousSchoolYear = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property PreviousSchoolYear already has been fetched. Setting this property to false when PreviousSchoolYear has been fetched
		/// will set PreviousSchoolYear to null as well. Setting this property to true while PreviousSchoolYear hasn't been fetched disables lazy loading for PreviousSchoolYear</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPreviousSchoolYear
		{
			get { return _alreadyFetchedPreviousSchoolYear;}
			set 
			{
				if(_alreadyFetchedPreviousSchoolYear && !value)
				{
					this.PreviousSchoolYear = null;
				}
				_alreadyFetchedPreviousSchoolYear = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PreviousSchoolYear is not found
		/// in the database. When set to true, PreviousSchoolYear will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PreviousSchoolYearReturnsNewIfNotFound
		{
			get	{ return _previousSchoolYearReturnsNewIfNotFound; }
			set	{ _previousSchoolYearReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SchoolYearEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
