﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SettlementRunValue'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SettlementRunValueEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection	_settlementQueryValues;
		private bool	_alwaysFetchSettlementQueryValues, _alreadyFetchedSettlementQueryValues;
		private VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection	_settlementRunValueToVarioSettlements;
		private bool	_alwaysFetchSettlementRunValueToVarioSettlements, _alreadyFetchedSettlementRunValueToVarioSettlements;
		private VarioSL.Entities.CollectionClasses.VarioSettlementCollection _varioSettlements;
		private bool	_alwaysFetchVarioSettlements, _alreadyFetchedVarioSettlements;
		private SettlementRunEntity _settlementRun;
		private bool	_alwaysFetchSettlementRun, _alreadyFetchedSettlementRun, _settlementRunReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SettlementRun</summary>
			public static readonly string SettlementRun = "SettlementRun";
			/// <summary>Member name SettlementQueryValues</summary>
			public static readonly string SettlementQueryValues = "SettlementQueryValues";
			/// <summary>Member name SettlementRunValueToVarioSettlements</summary>
			public static readonly string SettlementRunValueToVarioSettlements = "SettlementRunValueToVarioSettlements";
			/// <summary>Member name VarioSettlements</summary>
			public static readonly string VarioSettlements = "VarioSettlements";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SettlementRunValueEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SettlementRunValueEntity() :base("SettlementRunValueEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValue which data should be fetched into this SettlementRunValue object</param>
		public SettlementRunValueEntity(System.Int64 settlementRunValueID):base("SettlementRunValueEntity")
		{
			InitClassFetch(settlementRunValueID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValue which data should be fetched into this SettlementRunValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SettlementRunValueEntity(System.Int64 settlementRunValueID, IPrefetchPath prefetchPathToUse):base("SettlementRunValueEntity")
		{
			InitClassFetch(settlementRunValueID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValue which data should be fetched into this SettlementRunValue object</param>
		/// <param name="validator">The custom validator object for this SettlementRunValueEntity</param>
		public SettlementRunValueEntity(System.Int64 settlementRunValueID, IValidator validator):base("SettlementRunValueEntity")
		{
			InitClassFetch(settlementRunValueID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SettlementRunValueEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_settlementQueryValues = (VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection)info.GetValue("_settlementQueryValues", typeof(VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection));
			_alwaysFetchSettlementQueryValues = info.GetBoolean("_alwaysFetchSettlementQueryValues");
			_alreadyFetchedSettlementQueryValues = info.GetBoolean("_alreadyFetchedSettlementQueryValues");

			_settlementRunValueToVarioSettlements = (VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection)info.GetValue("_settlementRunValueToVarioSettlements", typeof(VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection));
			_alwaysFetchSettlementRunValueToVarioSettlements = info.GetBoolean("_alwaysFetchSettlementRunValueToVarioSettlements");
			_alreadyFetchedSettlementRunValueToVarioSettlements = info.GetBoolean("_alreadyFetchedSettlementRunValueToVarioSettlements");
			_varioSettlements = (VarioSL.Entities.CollectionClasses.VarioSettlementCollection)info.GetValue("_varioSettlements", typeof(VarioSL.Entities.CollectionClasses.VarioSettlementCollection));
			_alwaysFetchVarioSettlements = info.GetBoolean("_alwaysFetchVarioSettlements");
			_alreadyFetchedVarioSettlements = info.GetBoolean("_alreadyFetchedVarioSettlements");
			_settlementRun = (SettlementRunEntity)info.GetValue("_settlementRun", typeof(SettlementRunEntity));
			if(_settlementRun!=null)
			{
				_settlementRun.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_settlementRunReturnsNewIfNotFound = info.GetBoolean("_settlementRunReturnsNewIfNotFound");
			_alwaysFetchSettlementRun = info.GetBoolean("_alwaysFetchSettlementRun");
			_alreadyFetchedSettlementRun = info.GetBoolean("_alreadyFetchedSettlementRun");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SettlementRunValueFieldIndex)fieldIndex)
			{
				case SettlementRunValueFieldIndex.SettlementRunID:
					DesetupSyncSettlementRun(true, false);
					_alreadyFetchedSettlementRun = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSettlementQueryValues = (_settlementQueryValues.Count > 0);
			_alreadyFetchedSettlementRunValueToVarioSettlements = (_settlementRunValueToVarioSettlements.Count > 0);
			_alreadyFetchedVarioSettlements = (_varioSettlements.Count > 0);
			_alreadyFetchedSettlementRun = (_settlementRun != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SettlementRun":
					toReturn.Add(Relations.SettlementRunEntityUsingSettlementRunID);
					break;
				case "SettlementQueryValues":
					toReturn.Add(Relations.SettlementQueryValueEntityUsingSettlementRunValueID);
					break;
				case "SettlementRunValueToVarioSettlements":
					toReturn.Add(Relations.SettlementRunValueToVarioSettlementEntityUsingSettlementRunValueID);
					break;
				case "VarioSettlements":
					toReturn.Add(Relations.SettlementRunValueToVarioSettlementEntityUsingSettlementRunValueID, "SettlementRunValueEntity__", "SettlementRunValueToVarioSettlement_", JoinHint.None);
					toReturn.Add(SettlementRunValueToVarioSettlementEntity.Relations.VarioSettlementEntityUsingVarioSettlementID, "SettlementRunValueToVarioSettlement_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_settlementQueryValues", (!this.MarkedForDeletion?_settlementQueryValues:null));
			info.AddValue("_alwaysFetchSettlementQueryValues", _alwaysFetchSettlementQueryValues);
			info.AddValue("_alreadyFetchedSettlementQueryValues", _alreadyFetchedSettlementQueryValues);
			info.AddValue("_settlementRunValueToVarioSettlements", (!this.MarkedForDeletion?_settlementRunValueToVarioSettlements:null));
			info.AddValue("_alwaysFetchSettlementRunValueToVarioSettlements", _alwaysFetchSettlementRunValueToVarioSettlements);
			info.AddValue("_alreadyFetchedSettlementRunValueToVarioSettlements", _alreadyFetchedSettlementRunValueToVarioSettlements);
			info.AddValue("_varioSettlements", (!this.MarkedForDeletion?_varioSettlements:null));
			info.AddValue("_alwaysFetchVarioSettlements", _alwaysFetchVarioSettlements);
			info.AddValue("_alreadyFetchedVarioSettlements", _alreadyFetchedVarioSettlements);
			info.AddValue("_settlementRun", (!this.MarkedForDeletion?_settlementRun:null));
			info.AddValue("_settlementRunReturnsNewIfNotFound", _settlementRunReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSettlementRun", _alwaysFetchSettlementRun);
			info.AddValue("_alreadyFetchedSettlementRun", _alreadyFetchedSettlementRun);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SettlementRun":
					_alreadyFetchedSettlementRun = true;
					this.SettlementRun = (SettlementRunEntity)entity;
					break;
				case "SettlementQueryValues":
					_alreadyFetchedSettlementQueryValues = true;
					if(entity!=null)
					{
						this.SettlementQueryValues.Add((SettlementQueryValueEntity)entity);
					}
					break;
				case "SettlementRunValueToVarioSettlements":
					_alreadyFetchedSettlementRunValueToVarioSettlements = true;
					if(entity!=null)
					{
						this.SettlementRunValueToVarioSettlements.Add((SettlementRunValueToVarioSettlementEntity)entity);
					}
					break;
				case "VarioSettlements":
					_alreadyFetchedVarioSettlements = true;
					if(entity!=null)
					{
						this.VarioSettlements.Add((VarioSettlementEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SettlementRun":
					SetupSyncSettlementRun(relatedEntity);
					break;
				case "SettlementQueryValues":
					_settlementQueryValues.Add((SettlementQueryValueEntity)relatedEntity);
					break;
				case "SettlementRunValueToVarioSettlements":
					_settlementRunValueToVarioSettlements.Add((SettlementRunValueToVarioSettlementEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SettlementRun":
					DesetupSyncSettlementRun(false, true);
					break;
				case "SettlementQueryValues":
					this.PerformRelatedEntityRemoval(_settlementQueryValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SettlementRunValueToVarioSettlements":
					this.PerformRelatedEntityRemoval(_settlementRunValueToVarioSettlements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_settlementRun!=null)
			{
				toReturn.Add(_settlementRun);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_settlementQueryValues);
			toReturn.Add(_settlementRunValueToVarioSettlements);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValue which data should be fetched into this SettlementRunValue object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementRunValueID)
		{
			return FetchUsingPK(settlementRunValueID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValue which data should be fetched into this SettlementRunValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementRunValueID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(settlementRunValueID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValue which data should be fetched into this SettlementRunValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementRunValueID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(settlementRunValueID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValue which data should be fetched into this SettlementRunValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementRunValueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(settlementRunValueID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SettlementRunValueID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SettlementRunValueRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SettlementQueryValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQueryValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection GetMultiSettlementQueryValues(bool forceFetch)
		{
			return GetMultiSettlementQueryValues(forceFetch, _settlementQueryValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQueryValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQueryValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection GetMultiSettlementQueryValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlementQueryValues(forceFetch, _settlementQueryValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQueryValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection GetMultiSettlementQueryValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlementQueryValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQueryValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection GetMultiSettlementQueryValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlementQueryValues || forceFetch || _alwaysFetchSettlementQueryValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementQueryValues);
				_settlementQueryValues.SuppressClearInGetMulti=!forceFetch;
				_settlementQueryValues.EntityFactoryToUse = entityFactoryToUse;
				_settlementQueryValues.GetMultiManyToOne(null, this, filter);
				_settlementQueryValues.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementQueryValues = true;
			}
			return _settlementQueryValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementQueryValues'. These settings will be taken into account
		/// when the property SettlementQueryValues is requested or GetMultiSettlementQueryValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementQueryValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementQueryValues.SortClauses=sortClauses;
			_settlementQueryValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueToVarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementRunValueToVarioSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection GetMultiSettlementRunValueToVarioSettlements(bool forceFetch)
		{
			return GetMultiSettlementRunValueToVarioSettlements(forceFetch, _settlementRunValueToVarioSettlements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueToVarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementRunValueToVarioSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection GetMultiSettlementRunValueToVarioSettlements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlementRunValueToVarioSettlements(forceFetch, _settlementRunValueToVarioSettlements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueToVarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection GetMultiSettlementRunValueToVarioSettlements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlementRunValueToVarioSettlements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueToVarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection GetMultiSettlementRunValueToVarioSettlements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlementRunValueToVarioSettlements || forceFetch || _alwaysFetchSettlementRunValueToVarioSettlements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementRunValueToVarioSettlements);
				_settlementRunValueToVarioSettlements.SuppressClearInGetMulti=!forceFetch;
				_settlementRunValueToVarioSettlements.EntityFactoryToUse = entityFactoryToUse;
				_settlementRunValueToVarioSettlements.GetMultiManyToOne(null, this, filter);
				_settlementRunValueToVarioSettlements.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementRunValueToVarioSettlements = true;
			}
			return _settlementRunValueToVarioSettlements;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementRunValueToVarioSettlements'. These settings will be taken into account
		/// when the property SettlementRunValueToVarioSettlements is requested or GetMultiSettlementRunValueToVarioSettlements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementRunValueToVarioSettlements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementRunValueToVarioSettlements.SortClauses=sortClauses;
			_settlementRunValueToVarioSettlements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VarioSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiVarioSettlements(bool forceFetch)
		{
			return GetMultiVarioSettlements(forceFetch, _varioSettlements.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiVarioSettlements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedVarioSettlements || forceFetch || _alwaysFetchVarioSettlements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_varioSettlements);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SettlementRunValueFields.SettlementRunValueID, ComparisonOperator.Equal, this.SettlementRunValueID, "SettlementRunValueEntity__"));
				_varioSettlements.SuppressClearInGetMulti=!forceFetch;
				_varioSettlements.EntityFactoryToUse = entityFactoryToUse;
				_varioSettlements.GetMulti(filter, GetRelationsForField("VarioSettlements"));
				_varioSettlements.SuppressClearInGetMulti=false;
				_alreadyFetchedVarioSettlements = true;
			}
			return _varioSettlements;
		}

		/// <summary> Sets the collection parameters for the collection for 'VarioSettlements'. These settings will be taken into account
		/// when the property VarioSettlements is requested or GetMultiVarioSettlements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVarioSettlements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_varioSettlements.SortClauses=sortClauses;
			_varioSettlements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'SettlementRunEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SettlementRunEntity' which is related to this entity.</returns>
		public SettlementRunEntity GetSingleSettlementRun()
		{
			return GetSingleSettlementRun(false);
		}

		/// <summary> Retrieves the related entity of type 'SettlementRunEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SettlementRunEntity' which is related to this entity.</returns>
		public virtual SettlementRunEntity GetSingleSettlementRun(bool forceFetch)
		{
			if( ( !_alreadyFetchedSettlementRun || forceFetch || _alwaysFetchSettlementRun) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SettlementRunEntityUsingSettlementRunID);
				SettlementRunEntity newEntity = new SettlementRunEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SettlementRunID);
				}
				if(fetchResult)
				{
					newEntity = (SettlementRunEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_settlementRunReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SettlementRun = newEntity;
				_alreadyFetchedSettlementRun = fetchResult;
			}
			return _settlementRun;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SettlementRun", _settlementRun);
			toReturn.Add("SettlementQueryValues", _settlementQueryValues);
			toReturn.Add("SettlementRunValueToVarioSettlements", _settlementRunValueToVarioSettlements);
			toReturn.Add("VarioSettlements", _varioSettlements);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValue which data should be fetched into this SettlementRunValue object</param>
		/// <param name="validator">The validator object for this SettlementRunValueEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 settlementRunValueID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(settlementRunValueID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_settlementQueryValues = new VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection();
			_settlementQueryValues.SetContainingEntityInfo(this, "SettlementRunValue");

			_settlementRunValueToVarioSettlements = new VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection();
			_settlementRunValueToVarioSettlements.SetContainingEntityInfo(this, "SettlementRunValue");
			_varioSettlements = new VarioSL.Entities.CollectionClasses.VarioSettlementCollection();
			_settlementRunReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Calculated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementRunID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementRunValueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Value", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _settlementRun</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSettlementRun(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _settlementRun, new PropertyChangedEventHandler( OnSettlementRunPropertyChanged ), "SettlementRun", VarioSL.Entities.RelationClasses.StaticSettlementRunValueRelations.SettlementRunEntityUsingSettlementRunIDStatic, true, signalRelatedEntity, "SettlementRunValues", resetFKFields, new int[] { (int)SettlementRunValueFieldIndex.SettlementRunID } );		
			_settlementRun = null;
		}
		
		/// <summary> setups the sync logic for member _settlementRun</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSettlementRun(IEntityCore relatedEntity)
		{
			if(_settlementRun!=relatedEntity)
			{		
				DesetupSyncSettlementRun(true, true);
				_settlementRun = (SettlementRunEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _settlementRun, new PropertyChangedEventHandler( OnSettlementRunPropertyChanged ), "SettlementRun", VarioSL.Entities.RelationClasses.StaticSettlementRunValueRelations.SettlementRunEntityUsingSettlementRunIDStatic, true, ref _alreadyFetchedSettlementRun, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSettlementRunPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValue which data should be fetched into this SettlementRunValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 settlementRunValueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SettlementRunValueFieldIndex.SettlementRunValueID].ForcedCurrentValueWrite(settlementRunValueID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSettlementRunValueDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SettlementRunValueEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SettlementRunValueRelations Relations
		{
			get	{ return new SettlementRunValueRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementQueryValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementQueryValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection(), (IEntityRelation)GetRelationsForField("SettlementQueryValues")[0], (int)VarioSL.Entities.EntityType.SettlementRunValueEntity, (int)VarioSL.Entities.EntityType.SettlementQueryValueEntity, 0, null, null, null, "SettlementQueryValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementRunValueToVarioSettlement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementRunValueToVarioSettlements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection(), (IEntityRelation)GetRelationsForField("SettlementRunValueToVarioSettlements")[0], (int)VarioSL.Entities.EntityType.SettlementRunValueEntity, (int)VarioSL.Entities.EntityType.SettlementRunValueToVarioSettlementEntity, 0, null, null, null, "SettlementRunValueToVarioSettlements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VarioSettlement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVarioSettlements
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.SettlementRunValueToVarioSettlementEntityUsingSettlementRunValueID;
				intermediateRelation.SetAliases(string.Empty, "SettlementRunValueToVarioSettlement_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VarioSettlementCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.SettlementRunValueEntity, (int)VarioSL.Entities.EntityType.VarioSettlementEntity, 0, null, null, GetRelationsForField("VarioSettlements"), "VarioSettlements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementRun'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementRun
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementRunCollection(), (IEntityRelation)GetRelationsForField("SettlementRun")[0], (int)VarioSL.Entities.EntityType.SettlementRunValueEntity, (int)VarioSL.Entities.EntityType.SettlementRunEntity, 0, null, null, null, "SettlementRun", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Calculated property of the Entity SettlementRunValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUNVALUE"."CALCULATED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Calculated
		{
			get { return (System.DateTime)GetValue((int)SettlementRunValueFieldIndex.Calculated, true); }
			set	{ SetValue((int)SettlementRunValueFieldIndex.Calculated, value, true); }
		}

		/// <summary> The LastModified property of the Entity SettlementRunValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUNVALUE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SettlementRunValueFieldIndex.LastModified, true); }
			set	{ SetValue((int)SettlementRunValueFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity SettlementRunValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUNVALUE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SettlementRunValueFieldIndex.LastUser, true); }
			set	{ SetValue((int)SettlementRunValueFieldIndex.LastUser, value, true); }
		}

		/// <summary> The SettlementRunID property of the Entity SettlementRunValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUNVALUE"."SETTLEMENTRUNID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SettlementRunID
		{
			get { return (System.Int64)GetValue((int)SettlementRunValueFieldIndex.SettlementRunID, true); }
			set	{ SetValue((int)SettlementRunValueFieldIndex.SettlementRunID, value, true); }
		}

		/// <summary> The SettlementRunValueID property of the Entity SettlementRunValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUNVALUE"."SETTLEMENTRUNVALUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SettlementRunValueID
		{
			get { return (System.Int64)GetValue((int)SettlementRunValueFieldIndex.SettlementRunValueID, true); }
			set	{ SetValue((int)SettlementRunValueFieldIndex.SettlementRunValueID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity SettlementRunValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUNVALUE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SettlementRunValueFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SettlementRunValueFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The Value property of the Entity SettlementRunValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUNVALUE"."VALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): BinaryDouble, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Value
		{
			get { return (System.Double)GetValue((int)SettlementRunValueFieldIndex.Value, true); }
			set	{ SetValue((int)SettlementRunValueFieldIndex.Value, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SettlementQueryValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementQueryValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection SettlementQueryValues
		{
			get	{ return GetMultiSettlementQueryValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementQueryValues. When set to true, SettlementQueryValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementQueryValues is accessed. You can always execute/ a forced fetch by calling GetMultiSettlementQueryValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementQueryValues
		{
			get	{ return _alwaysFetchSettlementQueryValues; }
			set	{ _alwaysFetchSettlementQueryValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementQueryValues already has been fetched. Setting this property to false when SettlementQueryValues has been fetched
		/// will clear the SettlementQueryValues collection well. Setting this property to true while SettlementQueryValues hasn't been fetched disables lazy loading for SettlementQueryValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementQueryValues
		{
			get { return _alreadyFetchedSettlementQueryValues;}
			set 
			{
				if(_alreadyFetchedSettlementQueryValues && !value && (_settlementQueryValues != null))
				{
					_settlementQueryValues.Clear();
				}
				_alreadyFetchedSettlementQueryValues = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SettlementRunValueToVarioSettlementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementRunValueToVarioSettlements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection SettlementRunValueToVarioSettlements
		{
			get	{ return GetMultiSettlementRunValueToVarioSettlements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementRunValueToVarioSettlements. When set to true, SettlementRunValueToVarioSettlements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementRunValueToVarioSettlements is accessed. You can always execute/ a forced fetch by calling GetMultiSettlementRunValueToVarioSettlements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementRunValueToVarioSettlements
		{
			get	{ return _alwaysFetchSettlementRunValueToVarioSettlements; }
			set	{ _alwaysFetchSettlementRunValueToVarioSettlements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementRunValueToVarioSettlements already has been fetched. Setting this property to false when SettlementRunValueToVarioSettlements has been fetched
		/// will clear the SettlementRunValueToVarioSettlements collection well. Setting this property to true while SettlementRunValueToVarioSettlements hasn't been fetched disables lazy loading for SettlementRunValueToVarioSettlements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementRunValueToVarioSettlements
		{
			get { return _alreadyFetchedSettlementRunValueToVarioSettlements;}
			set 
			{
				if(_alreadyFetchedSettlementRunValueToVarioSettlements && !value && (_settlementRunValueToVarioSettlements != null))
				{
					_settlementRunValueToVarioSettlements.Clear();
				}
				_alreadyFetchedSettlementRunValueToVarioSettlements = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVarioSettlements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VarioSettlementCollection VarioSettlements
		{
			get { return GetMultiVarioSettlements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VarioSettlements. When set to true, VarioSettlements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VarioSettlements is accessed. You can always execute a forced fetch by calling GetMultiVarioSettlements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVarioSettlements
		{
			get	{ return _alwaysFetchVarioSettlements; }
			set	{ _alwaysFetchVarioSettlements = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VarioSettlements already has been fetched. Setting this property to false when VarioSettlements has been fetched
		/// will clear the VarioSettlements collection well. Setting this property to true while VarioSettlements hasn't been fetched disables lazy loading for VarioSettlements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVarioSettlements
		{
			get { return _alreadyFetchedVarioSettlements;}
			set 
			{
				if(_alreadyFetchedVarioSettlements && !value && (_varioSettlements != null))
				{
					_varioSettlements.Clear();
				}
				_alreadyFetchedVarioSettlements = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'SettlementRunEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSettlementRun()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SettlementRunEntity SettlementRun
		{
			get	{ return GetSingleSettlementRun(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSettlementRun(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SettlementRunValues", "SettlementRun", _settlementRun, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementRun. When set to true, SettlementRun is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementRun is accessed. You can always execute a forced fetch by calling GetSingleSettlementRun(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementRun
		{
			get	{ return _alwaysFetchSettlementRun; }
			set	{ _alwaysFetchSettlementRun = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementRun already has been fetched. Setting this property to false when SettlementRun has been fetched
		/// will set SettlementRun to null as well. Setting this property to true while SettlementRun hasn't been fetched disables lazy loading for SettlementRun</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementRun
		{
			get { return _alreadyFetchedSettlementRun;}
			set 
			{
				if(_alreadyFetchedSettlementRun && !value)
				{
					this.SettlementRun = null;
				}
				_alreadyFetchedSettlementRun = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SettlementRun is not found
		/// in the database. When set to true, SettlementRun will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SettlementRunReturnsNewIfNotFound
		{
			get	{ return _settlementRunReturnsNewIfNotFound; }
			set { _settlementRunReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SettlementRunValueEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
