﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CalendarEntry'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CalendarEntryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CalendarEntity _calendar;
		private bool	_alwaysFetchCalendar, _alreadyFetchedCalendar, _calendarReturnsNewIfNotFound;
		private DayTypeEntity _dayType;
		private bool	_alwaysFetchDayType, _alreadyFetchedDayType, _dayTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Calendar</summary>
			public static readonly string Calendar = "Calendar";
			/// <summary>Member name DayType</summary>
			public static readonly string DayType = "DayType";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CalendarEntryEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CalendarEntryEntity() :base("CalendarEntryEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="calndarEntryId">PK value for CalendarEntry which data should be fetched into this CalendarEntry object</param>
		public CalendarEntryEntity(System.Int64 calndarEntryId):base("CalendarEntryEntity")
		{
			InitClassFetch(calndarEntryId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="calndarEntryId">PK value for CalendarEntry which data should be fetched into this CalendarEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CalendarEntryEntity(System.Int64 calndarEntryId, IPrefetchPath prefetchPathToUse):base("CalendarEntryEntity")
		{
			InitClassFetch(calndarEntryId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="calndarEntryId">PK value for CalendarEntry which data should be fetched into this CalendarEntry object</param>
		/// <param name="validator">The custom validator object for this CalendarEntryEntity</param>
		public CalendarEntryEntity(System.Int64 calndarEntryId, IValidator validator):base("CalendarEntryEntity")
		{
			InitClassFetch(calndarEntryId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CalendarEntryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_calendar = (CalendarEntity)info.GetValue("_calendar", typeof(CalendarEntity));
			if(_calendar!=null)
			{
				_calendar.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_calendarReturnsNewIfNotFound = info.GetBoolean("_calendarReturnsNewIfNotFound");
			_alwaysFetchCalendar = info.GetBoolean("_alwaysFetchCalendar");
			_alreadyFetchedCalendar = info.GetBoolean("_alreadyFetchedCalendar");

			_dayType = (DayTypeEntity)info.GetValue("_dayType", typeof(DayTypeEntity));
			if(_dayType!=null)
			{
				_dayType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_dayTypeReturnsNewIfNotFound = info.GetBoolean("_dayTypeReturnsNewIfNotFound");
			_alwaysFetchDayType = info.GetBoolean("_alwaysFetchDayType");
			_alreadyFetchedDayType = info.GetBoolean("_alreadyFetchedDayType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CalendarEntryFieldIndex)fieldIndex)
			{
				case CalendarEntryFieldIndex.CalendarID:
					DesetupSyncCalendar(true, false);
					_alreadyFetchedCalendar = false;
					break;
				case CalendarEntryFieldIndex.DayTypeEntryID:
					DesetupSyncDayType(true, false);
					_alreadyFetchedDayType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCalendar = (_calendar != null);
			_alreadyFetchedDayType = (_dayType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Calendar":
					toReturn.Add(Relations.CalendarEntityUsingCalendarID);
					break;
				case "DayType":
					toReturn.Add(Relations.DayTypeEntityUsingDayTypeEntryID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_calendar", (!this.MarkedForDeletion?_calendar:null));
			info.AddValue("_calendarReturnsNewIfNotFound", _calendarReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCalendar", _alwaysFetchCalendar);
			info.AddValue("_alreadyFetchedCalendar", _alreadyFetchedCalendar);
			info.AddValue("_dayType", (!this.MarkedForDeletion?_dayType:null));
			info.AddValue("_dayTypeReturnsNewIfNotFound", _dayTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDayType", _alwaysFetchDayType);
			info.AddValue("_alreadyFetchedDayType", _alreadyFetchedDayType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Calendar":
					_alreadyFetchedCalendar = true;
					this.Calendar = (CalendarEntity)entity;
					break;
				case "DayType":
					_alreadyFetchedDayType = true;
					this.DayType = (DayTypeEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Calendar":
					SetupSyncCalendar(relatedEntity);
					break;
				case "DayType":
					SetupSyncDayType(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Calendar":
					DesetupSyncCalendar(false, true);
					break;
				case "DayType":
					DesetupSyncDayType(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_calendar!=null)
			{
				toReturn.Add(_calendar);
			}
			if(_dayType!=null)
			{
				toReturn.Add(_dayType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="calndarEntryId">PK value for CalendarEntry which data should be fetched into this CalendarEntry object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 calndarEntryId)
		{
			return FetchUsingPK(calndarEntryId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="calndarEntryId">PK value for CalendarEntry which data should be fetched into this CalendarEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 calndarEntryId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(calndarEntryId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="calndarEntryId">PK value for CalendarEntry which data should be fetched into this CalendarEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 calndarEntryId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(calndarEntryId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="calndarEntryId">PK value for CalendarEntry which data should be fetched into this CalendarEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 calndarEntryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(calndarEntryId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CalndarEntryId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CalendarEntryRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CalendarEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CalendarEntity' which is related to this entity.</returns>
		public CalendarEntity GetSingleCalendar()
		{
			return GetSingleCalendar(false);
		}

		/// <summary> Retrieves the related entity of type 'CalendarEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CalendarEntity' which is related to this entity.</returns>
		public virtual CalendarEntity GetSingleCalendar(bool forceFetch)
		{
			if( ( !_alreadyFetchedCalendar || forceFetch || _alwaysFetchCalendar) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CalendarEntityUsingCalendarID);
				CalendarEntity newEntity = new CalendarEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CalendarID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CalendarEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_calendarReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Calendar = newEntity;
				_alreadyFetchedCalendar = fetchResult;
			}
			return _calendar;
		}


		/// <summary> Retrieves the related entity of type 'DayTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DayTypeEntity' which is related to this entity.</returns>
		public DayTypeEntity GetSingleDayType()
		{
			return GetSingleDayType(false);
		}

		/// <summary> Retrieves the related entity of type 'DayTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DayTypeEntity' which is related to this entity.</returns>
		public virtual DayTypeEntity GetSingleDayType(bool forceFetch)
		{
			if( ( !_alreadyFetchedDayType || forceFetch || _alwaysFetchDayType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DayTypeEntityUsingDayTypeEntryID);
				DayTypeEntity newEntity = new DayTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DayTypeEntryID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DayTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_dayTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DayType = newEntity;
				_alreadyFetchedDayType = fetchResult;
			}
			return _dayType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Calendar", _calendar);
			toReturn.Add("DayType", _dayType);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="calndarEntryId">PK value for CalendarEntry which data should be fetched into this CalendarEntry object</param>
		/// <param name="validator">The validator object for this CalendarEntryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 calndarEntryId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(calndarEntryId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_calendarReturnsNewIfNotFound = false;
			_dayTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CalendarID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CalndarEntryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Date", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DayTypeEntryID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _calendar</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCalendar(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _calendar, new PropertyChangedEventHandler( OnCalendarPropertyChanged ), "Calendar", VarioSL.Entities.RelationClasses.StaticCalendarEntryRelations.CalendarEntityUsingCalendarIDStatic, true, signalRelatedEntity, "CalendarEntries", resetFKFields, new int[] { (int)CalendarEntryFieldIndex.CalendarID } );		
			_calendar = null;
		}
		
		/// <summary> setups the sync logic for member _calendar</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCalendar(IEntityCore relatedEntity)
		{
			if(_calendar!=relatedEntity)
			{		
				DesetupSyncCalendar(true, true);
				_calendar = (CalendarEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _calendar, new PropertyChangedEventHandler( OnCalendarPropertyChanged ), "Calendar", VarioSL.Entities.RelationClasses.StaticCalendarEntryRelations.CalendarEntityUsingCalendarIDStatic, true, ref _alreadyFetchedCalendar, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCalendarPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _dayType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDayType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _dayType, new PropertyChangedEventHandler( OnDayTypePropertyChanged ), "DayType", VarioSL.Entities.RelationClasses.StaticCalendarEntryRelations.DayTypeEntityUsingDayTypeEntryIDStatic, true, signalRelatedEntity, "CalendarEntries", resetFKFields, new int[] { (int)CalendarEntryFieldIndex.DayTypeEntryID } );		
			_dayType = null;
		}
		
		/// <summary> setups the sync logic for member _dayType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDayType(IEntityCore relatedEntity)
		{
			if(_dayType!=relatedEntity)
			{		
				DesetupSyncDayType(true, true);
				_dayType = (DayTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _dayType, new PropertyChangedEventHandler( OnDayTypePropertyChanged ), "DayType", VarioSL.Entities.RelationClasses.StaticCalendarEntryRelations.DayTypeEntityUsingDayTypeEntryIDStatic, true, ref _alreadyFetchedDayType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDayTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="calndarEntryId">PK value for CalendarEntry which data should be fetched into this CalendarEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 calndarEntryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CalendarEntryFieldIndex.CalndarEntryId].ForcedCurrentValueWrite(calndarEntryId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCalendarEntryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CalendarEntryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CalendarEntryRelations Relations
		{
			get	{ return new CalendarEntryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Calendar'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCalendar
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CalendarCollection(), (IEntityRelation)GetRelationsForField("Calendar")[0], (int)VarioSL.Entities.EntityType.CalendarEntryEntity, (int)VarioSL.Entities.EntityType.CalendarEntity, 0, null, null, null, "Calendar", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DayType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDayType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DayTypeCollection(), (IEntityRelation)GetRelationsForField("DayType")[0], (int)VarioSL.Entities.EntityType.CalendarEntryEntity, (int)VarioSL.Entities.EntityType.DayTypeEntity, 0, null, null, null, "DayType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CalendarID property of the Entity CalendarEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CALENDARENTRY"."CALENDARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CalendarID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CalendarEntryFieldIndex.CalendarID, false); }
			set	{ SetValue((int)CalendarEntryFieldIndex.CalendarID, value, true); }
		}

		/// <summary> The CalndarEntryId property of the Entity CalendarEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CALENDARENTRY"."CALENDARENTRYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 CalndarEntryId
		{
			get { return (System.Int64)GetValue((int)CalendarEntryFieldIndex.CalndarEntryId, true); }
			set	{ SetValue((int)CalendarEntryFieldIndex.CalndarEntryId, value, true); }
		}

		/// <summary> The Date property of the Entity CalendarEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CALENDARENTRY"."DATE_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Date
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CalendarEntryFieldIndex.Date, false); }
			set	{ SetValue((int)CalendarEntryFieldIndex.Date, value, true); }
		}

		/// <summary> The DayTypeEntryID property of the Entity CalendarEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CALENDARENTRY"."DAYTYPENTRYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DayTypeEntryID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CalendarEntryFieldIndex.DayTypeEntryID, false); }
			set	{ SetValue((int)CalendarEntryFieldIndex.DayTypeEntryID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CalendarEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCalendar()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CalendarEntity Calendar
		{
			get	{ return GetSingleCalendar(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCalendar(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CalendarEntries", "Calendar", _calendar, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Calendar. When set to true, Calendar is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Calendar is accessed. You can always execute a forced fetch by calling GetSingleCalendar(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCalendar
		{
			get	{ return _alwaysFetchCalendar; }
			set	{ _alwaysFetchCalendar = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Calendar already has been fetched. Setting this property to false when Calendar has been fetched
		/// will set Calendar to null as well. Setting this property to true while Calendar hasn't been fetched disables lazy loading for Calendar</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCalendar
		{
			get { return _alreadyFetchedCalendar;}
			set 
			{
				if(_alreadyFetchedCalendar && !value)
				{
					this.Calendar = null;
				}
				_alreadyFetchedCalendar = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Calendar is not found
		/// in the database. When set to true, Calendar will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CalendarReturnsNewIfNotFound
		{
			get	{ return _calendarReturnsNewIfNotFound; }
			set { _calendarReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DayTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDayType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DayTypeEntity DayType
		{
			get	{ return GetSingleDayType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDayType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CalendarEntries", "DayType", _dayType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DayType. When set to true, DayType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DayType is accessed. You can always execute a forced fetch by calling GetSingleDayType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDayType
		{
			get	{ return _alwaysFetchDayType; }
			set	{ _alwaysFetchDayType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DayType already has been fetched. Setting this property to false when DayType has been fetched
		/// will set DayType to null as well. Setting this property to true while DayType hasn't been fetched disables lazy loading for DayType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDayType
		{
			get { return _alreadyFetchedDayType;}
			set 
			{
				if(_alreadyFetchedDayType && !value)
				{
					this.DayType = null;
				}
				_alreadyFetchedDayType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DayType is not found
		/// in the database. When set to true, DayType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DayTypeReturnsNewIfNotFound
		{
			get	{ return _dayTypeReturnsNewIfNotFound; }
			set { _dayTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CalendarEntryEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
