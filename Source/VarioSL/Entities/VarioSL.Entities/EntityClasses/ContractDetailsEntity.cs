﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ContractDetails'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ContractDetailsEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ContractDetailsEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ContractDetailsEntity() :base("ContractDetailsEntity")
		{
			InitClassEmpty(null);
		}
		


		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ContractDetailsEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}




				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ContractDetailsRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		



		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountBalanceEndLastMonth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountBalanceStartLastMonth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountBalanceToday", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountingModeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdditionalText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressAddressee", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressAddressField1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressAddressField2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressCity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressCountry", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressPostalCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressRegion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressStreet", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressStreetNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankConnectionDataBic", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankConnectionDataIban", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankConnectionDataLastUsage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientAuthenticationToken", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommentPriorityName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommentPriorityNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommentText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DunningCreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DunningInvoiceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DunningLevelName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DunningProcessNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastInvoiceCreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastInvoiceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationAbbreviation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationExternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentModalityID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentModalityPaymentMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentModalityPaymentMethodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonCellPhoneNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonDateOfBirth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonEmail", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonFaxNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonFirstName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonGender", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonLastName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonMiddleName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonPersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonPhoneNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonSalutation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingAmountSumCurrentMonth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingAmountSumLastMonth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductTerminationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductTerminationDemandDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductTerminationProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductTerminationTypeName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
		}
		#endregion

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. </summary>
		/// <returns>true</returns>
		public override bool Refetch()
		{
			return true;
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateContractDetailsDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ContractDetailsEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ContractDetailsRelations Relations
		{
			get	{ return new ContractDetailsRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccountBalanceEndLastMonth property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ACCOUNTBALANCEENDLASTMONTH"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> AccountBalanceEndLastMonth
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ContractDetailsFieldIndex.AccountBalanceEndLastMonth, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AccountBalanceEndLastMonth, value, true); }
		}

		/// <summary> The AccountBalanceStartLastMonth property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ACCOUNTBALANCESTARTLASTMONTH"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> AccountBalanceStartLastMonth
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ContractDetailsFieldIndex.AccountBalanceStartLastMonth, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AccountBalanceStartLastMonth, value, true); }
		}

		/// <summary> The AccountBalanceToday property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ACCOUNTBALANCETODAY"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> AccountBalanceToday
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ContractDetailsFieldIndex.AccountBalanceToday, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AccountBalanceToday, value, true); }
		}

		/// <summary> The AccountingModeID property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ACCOUNTINGMODEID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AccountingModeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractDetailsFieldIndex.AccountingModeID, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AccountingModeID, value, true); }
		}

		/// <summary> The AdditionalText property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ADDITIONALTEXT"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdditionalText
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.AdditionalText, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AdditionalText, value, true); }
		}

		/// <summary> The AddressAddressee property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSADDRESSEE"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressAddressee
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.AddressAddressee, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AddressAddressee, value, true); }
		}

		/// <summary> The AddressAddressField1 property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSADDRESSFIELD1"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressAddressField1
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.AddressAddressField1, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AddressAddressField1, value, true); }
		}

		/// <summary> The AddressAddressField2 property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSADDRESSFIELD2"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressAddressField2
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.AddressAddressField2, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AddressAddressField2, value, true); }
		}

		/// <summary> The AddressCity property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSCITY"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressCity
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.AddressCity, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AddressCity, value, true); }
		}

		/// <summary> The AddressCountry property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSCOUNTRY"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressCountry
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.AddressCountry, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AddressCountry, value, true); }
		}

		/// <summary> The AddressID property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AddressID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractDetailsFieldIndex.AddressID, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AddressID, value, true); }
		}

		/// <summary> The AddressPostalCode property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSPOSTALCODE"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressPostalCode
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.AddressPostalCode, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AddressPostalCode, value, true); }
		}

		/// <summary> The AddressRegion property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSREGION"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressRegion
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.AddressRegion, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AddressRegion, value, true); }
		}

		/// <summary> The AddressStreet property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSSTREET"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressStreet
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.AddressStreet, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AddressStreet, value, true); }
		}

		/// <summary> The AddressStreetNumber property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSSTREETNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressStreetNumber
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.AddressStreetNumber, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.AddressStreetNumber, value, true); }
		}

		/// <summary> The BankConnectionDataBic property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."BANKCONNECTIONDATABIC"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 11<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BankConnectionDataBic
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.BankConnectionDataBic, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.BankConnectionDataBic, value, true); }
		}

		/// <summary> The BankConnectionDataIban property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."BANKCONNECTIONDATAIBAN"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 34<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BankConnectionDataIban
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.BankConnectionDataIban, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.BankConnectionDataIban, value, true); }
		}

		/// <summary> The BankConnectionDataLastUsage property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."BANKCONNECTIONDATALASTUSAGE"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BankConnectionDataLastUsage
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ContractDetailsFieldIndex.BankConnectionDataLastUsage, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.BankConnectionDataLastUsage, value, true); }
		}

		/// <summary> The ClientAuthenticationToken property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."CLIENTAUTHENTICATIONTOKEN"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 120<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClientAuthenticationToken
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.ClientAuthenticationToken, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.ClientAuthenticationToken, value, true); }
		}

		/// <summary> The ClientID property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."CLIENTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractDetailsFieldIndex.ClientID, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CommentPriorityName property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."COMMENTPRIORITYNAME"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CommentPriorityName
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.CommentPriorityName, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.CommentPriorityName, value, true); }
		}

		/// <summary> The CommentPriorityNumber property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."COMMENTPRIORITYNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.CommentPriority> CommentPriorityNumber
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.CommentPriority>)GetValue((int)ContractDetailsFieldIndex.CommentPriorityNumber, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.CommentPriorityNumber, value, true); }
		}

		/// <summary> The CommentText property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."COMMENTTEXT"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CommentText
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.CommentText, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.CommentText, value, true); }
		}

		/// <summary> The ContractID property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."CONTRACTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ContractID
		{
			get { return (System.Int64)GetValue((int)ContractDetailsFieldIndex.ContractID, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.ContractID, value, true); }
		}

		/// <summary> The ContractNumber property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."CONTRACTNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ContractNumber
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.ContractNumber, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.ContractNumber, value, true); }
		}

		/// <summary> The ContractType property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."CONTRACTTYPE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.ContentType> ContractType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.ContentType>)GetValue((int)ContractDetailsFieldIndex.ContractType, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.ContractType, value, true); }
		}

		/// <summary> The DunningCreationDate property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."DUNNINGCREATIONDATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DunningCreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ContractDetailsFieldIndex.DunningCreationDate, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.DunningCreationDate, value, true); }
		}

		/// <summary> The DunningInvoiceNumber property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."DUNNINGINVOICENUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DunningInvoiceNumber
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.DunningInvoiceNumber, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.DunningInvoiceNumber, value, true); }
		}

		/// <summary> The DunningLevelName property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."DUNNINGLEVELNAME"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DunningLevelName
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.DunningLevelName, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.DunningLevelName, value, true); }
		}

		/// <summary> The DunningProcessNumber property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."DUNNINGPROCESSNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DunningProcessNumber
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.DunningProcessNumber, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.DunningProcessNumber, value, true); }
		}

		/// <summary> The LastInvoiceCreationDate property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."LASTINVOICECREATIONDATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastInvoiceCreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ContractDetailsFieldIndex.LastInvoiceCreationDate, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.LastInvoiceCreationDate, value, true); }
		}

		/// <summary> The LastInvoiceNumber property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."LASTINVOICENUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastInvoiceNumber
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.LastInvoiceNumber, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.LastInvoiceNumber, value, true); }
		}

		/// <summary> The LastModified property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."LASTMODIFIED"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ContractDetailsFieldIndex.LastModified, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."LASTUSER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.LastUser, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.LastUser, value, true); }
		}

		/// <summary> The OrganizationAbbreviation property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ORGANIZATIONABBREVIATION"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrganizationAbbreviation
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.OrganizationAbbreviation, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.OrganizationAbbreviation, value, true); }
		}

		/// <summary> The OrganizationDescription property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ORGANIZATIONDESCRIPTION"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrganizationDescription
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.OrganizationDescription, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.OrganizationDescription, value, true); }
		}

		/// <summary> The OrganizationExternalNumber property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ORGANIZATIONEXTERNALNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrganizationExternalNumber
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.OrganizationExternalNumber, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.OrganizationExternalNumber, value, true); }
		}

		/// <summary> The OrganizationID property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ORGANIZATIONID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OrganizationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractDetailsFieldIndex.OrganizationID, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.OrganizationID, value, true); }
		}

		/// <summary> The OrganizationName property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ORGANIZATIONNAME"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrganizationName
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.OrganizationName, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.OrganizationName, value, true); }
		}

		/// <summary> The OrganizationNumber property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."ORGANIZATIONNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrganizationNumber
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.OrganizationNumber, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.OrganizationNumber, value, true); }
		}

		/// <summary> The PaymentModalityID property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PAYMENTMODALITYID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentModalityID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractDetailsFieldIndex.PaymentModalityID, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PaymentModalityID, value, true); }
		}

		/// <summary> The PaymentModalityPaymentMethod property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PAYMENTMODALITYPAYMENTMETHOD"<br/>
		/// View field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 40<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentModalityPaymentMethod
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.PaymentModalityPaymentMethod, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PaymentModalityPaymentMethod, value, true); }
		}

		/// <summary> The PaymentModalityPaymentMethodID property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PAYMENTMODALITYPAYMENTMETHODID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentModalityPaymentMethodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractDetailsFieldIndex.PaymentModalityPaymentMethodID, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PaymentModalityPaymentMethodID, value, true); }
		}

		/// <summary> The PersonCellPhoneNumber property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PERSONCELLPHONENUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PersonCellPhoneNumber
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.PersonCellPhoneNumber, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PersonCellPhoneNumber, value, true); }
		}

		/// <summary> The PersonDateOfBirth property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PERSONDATEOFBIRTH"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PersonDateOfBirth
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ContractDetailsFieldIndex.PersonDateOfBirth, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PersonDateOfBirth, value, true); }
		}

		/// <summary> The PersonEmail property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PERSONEMAIL"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PersonEmail
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.PersonEmail, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PersonEmail, value, true); }
		}

		/// <summary> The PersonFaxNumber property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PERSONFAXNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PersonFaxNumber
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.PersonFaxNumber, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PersonFaxNumber, value, true); }
		}

		/// <summary> The PersonFirstName property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PERSONFIRSTNAME"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PersonFirstName
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.PersonFirstName, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PersonFirstName, value, true); }
		}

		/// <summary> The PersonGender property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PERSONGENDER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PersonGender
		{
			get { return (Nullable<System.Int32>)GetValue((int)ContractDetailsFieldIndex.PersonGender, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PersonGender, value, true); }
		}

		/// <summary> The PersonLastName property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PERSONLASTNAME"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PersonLastName
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.PersonLastName, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PersonLastName, value, true); }
		}

		/// <summary> The PersonMiddleName property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PERSONMIDDLENAME"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PersonMiddleName
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.PersonMiddleName, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PersonMiddleName, value, true); }
		}

		/// <summary> The PersonPersonID property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PERSONPERSONID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PersonPersonID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractDetailsFieldIndex.PersonPersonID, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PersonPersonID, value, true); }
		}

		/// <summary> The PersonPhoneNumber property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PERSONPHONENUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PersonPhoneNumber
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.PersonPhoneNumber, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PersonPhoneNumber, value, true); }
		}

		/// <summary> The PersonSalutation property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PERSONSALUTATION"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PersonSalutation
		{
			get { return (Nullable<System.Int32>)GetValue((int)ContractDetailsFieldIndex.PersonSalutation, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PersonSalutation, value, true); }
		}

		/// <summary> The PersonTitle property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PERSONTITLE"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PersonTitle
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.PersonTitle, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PersonTitle, value, true); }
		}

		/// <summary> The PostingAmountSumCurrentMonth property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."POSTINGAMOUNTSUMCURRENTMONTH"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> PostingAmountSumCurrentMonth
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ContractDetailsFieldIndex.PostingAmountSumCurrentMonth, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PostingAmountSumCurrentMonth, value, true); }
		}

		/// <summary> The PostingAmountSumLastMonth property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."POSTINGAMOUNTSUMLASTMONTH"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> PostingAmountSumLastMonth
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ContractDetailsFieldIndex.PostingAmountSumLastMonth, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.PostingAmountSumLastMonth, value, true); }
		}

		/// <summary> The ProductTerminationDate property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PRODUCTTERMINATIONDATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ProductTerminationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ContractDetailsFieldIndex.ProductTerminationDate, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.ProductTerminationDate, value, true); }
		}

		/// <summary> The ProductTerminationDemandDate property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PRODUCTTERMINATIONDEMANDDATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ProductTerminationDemandDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ContractDetailsFieldIndex.ProductTerminationDemandDate, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.ProductTerminationDemandDate, value, true); }
		}

		/// <summary> The ProductTerminationProductID property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PRODUCTTERMINATIONPRODUCTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ProductTerminationProductID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractDetailsFieldIndex.ProductTerminationProductID, false); }
			set	{ SetValue((int)ContractDetailsFieldIndex.ProductTerminationProductID, value, true); }
		}

		/// <summary> The ProductTerminationTypeName property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."PRODUCTTERMINATIONTYPENAME"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 128<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProductTerminationTypeName
		{
			get { return (System.String)GetValue((int)ContractDetailsFieldIndex.ProductTerminationTypeName, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.ProductTerminationTypeName, value, true); }
		}

		/// <summary> The State property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."STATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.ContractState State
		{
			get { return (VarioSL.Entities.Enumerations.ContractState)GetValue((int)ContractDetailsFieldIndex.State, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.State, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."TRANSACTIONCOUNTER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ContractDetailsFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."VALIDFROM"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)ContractDetailsFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidTo property of the Entity ContractDetails<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_CONTRACTDETAILS"."VALIDTO"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidTo
		{
			get { return (System.DateTime)GetValue((int)ContractDetailsFieldIndex.ValidTo, true); }
			set	{ SetValue((int)ContractDetailsFieldIndex.ValidTo, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ContractDetailsEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
