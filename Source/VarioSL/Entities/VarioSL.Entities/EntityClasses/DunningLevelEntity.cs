﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DunningLevel'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DunningLevelEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DunningLevelEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DunningLevelEntity() :base("DunningLevelEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="dunningLevelID">PK value for DunningLevel which data should be fetched into this DunningLevel object</param>
		public DunningLevelEntity(System.Int64 dunningLevelID):base("DunningLevelEntity")
		{
			InitClassFetch(dunningLevelID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="dunningLevelID">PK value for DunningLevel which data should be fetched into this DunningLevel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DunningLevelEntity(System.Int64 dunningLevelID, IPrefetchPath prefetchPathToUse):base("DunningLevelEntity")
		{
			InitClassFetch(dunningLevelID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="dunningLevelID">PK value for DunningLevel which data should be fetched into this DunningLevel object</param>
		/// <param name="validator">The custom validator object for this DunningLevelEntity</param>
		public DunningLevelEntity(System.Int64 dunningLevelID, IValidator validator):base("DunningLevelEntity")
		{
			InitClassFetch(dunningLevelID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DunningLevelEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningLevelID">PK value for DunningLevel which data should be fetched into this DunningLevel object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningLevelID)
		{
			return FetchUsingPK(dunningLevelID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningLevelID">PK value for DunningLevel which data should be fetched into this DunningLevel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningLevelID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(dunningLevelID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningLevelID">PK value for DunningLevel which data should be fetched into this DunningLevel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningLevelID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(dunningLevelID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningLevelID">PK value for DunningLevel which data should be fetched into this DunningLevel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningLevelID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(dunningLevelID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DunningLevelID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DunningLevelRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="dunningLevelID">PK value for DunningLevel which data should be fetched into this DunningLevel object</param>
		/// <param name="validator">The validator object for this DunningLevelEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 dunningLevelID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(dunningLevelID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancelContract", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DunningLevelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Fee", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Language", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LockBilling", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LockCards", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NextDunningLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoDaysForPayment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentMethodID", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="dunningLevelID">PK value for DunningLevel which data should be fetched into this DunningLevel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 dunningLevelID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DunningLevelFieldIndex.DunningLevelID].ForcedCurrentValueWrite(dunningLevelID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDunningLevelDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DunningLevelEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DunningLevelRelations Relations
		{
			get	{ return new DunningLevelRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CancelContract property of the Entity DunningLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DUNNINGLEVEL"."CANCELCONTRACT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> CancelContract
		{
			get { return (Nullable<System.Int16>)GetValue((int)DunningLevelFieldIndex.CancelContract, false); }
			set	{ SetValue((int)DunningLevelFieldIndex.CancelContract, value, true); }
		}

		/// <summary> The ClientID property of the Entity DunningLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DUNNINGLEVEL"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)DunningLevelFieldIndex.ClientID, true); }
			set	{ SetValue((int)DunningLevelFieldIndex.ClientID, value, true); }
		}

		/// <summary> The Description property of the Entity DunningLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DUNNINGLEVEL"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)DunningLevelFieldIndex.Description, true); }
			set	{ SetValue((int)DunningLevelFieldIndex.Description, value, true); }
		}

		/// <summary> The DunningLevelID property of the Entity DunningLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DUNNINGLEVEL"."DUNNINGLEVELID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 DunningLevelID
		{
			get { return (System.Int64)GetValue((int)DunningLevelFieldIndex.DunningLevelID, true); }
			set	{ SetValue((int)DunningLevelFieldIndex.DunningLevelID, value, true); }
		}

		/// <summary> The Fee property of the Entity DunningLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DUNNINGLEVEL"."FEE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Fee
		{
			get { return (Nullable<System.Int32>)GetValue((int)DunningLevelFieldIndex.Fee, false); }
			set	{ SetValue((int)DunningLevelFieldIndex.Fee, value, true); }
		}

		/// <summary> The Language property of the Entity DunningLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DUNNINGLEVEL"."LANGUAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Language
		{
			get { return (System.String)GetValue((int)DunningLevelFieldIndex.Language, true); }
			set	{ SetValue((int)DunningLevelFieldIndex.Language, value, true); }
		}

		/// <summary> The LockBilling property of the Entity DunningLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DUNNINGLEVEL"."LOCKBILLING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> LockBilling
		{
			get { return (Nullable<System.Int16>)GetValue((int)DunningLevelFieldIndex.LockBilling, false); }
			set	{ SetValue((int)DunningLevelFieldIndex.LockBilling, value, true); }
		}

		/// <summary> The LockCards property of the Entity DunningLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DUNNINGLEVEL"."LOCKCARDS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> LockCards
		{
			get { return (Nullable<System.Int16>)GetValue((int)DunningLevelFieldIndex.LockCards, false); }
			set	{ SetValue((int)DunningLevelFieldIndex.LockCards, value, true); }
		}

		/// <summary> The NextDunningLevel property of the Entity DunningLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DUNNINGLEVEL"."NEXTDUNNINGLEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NextDunningLevel
		{
			get { return (Nullable<System.Int64>)GetValue((int)DunningLevelFieldIndex.NextDunningLevel, false); }
			set	{ SetValue((int)DunningLevelFieldIndex.NextDunningLevel, value, true); }
		}

		/// <summary> The NoDaysForPayment property of the Entity DunningLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DUNNINGLEVEL"."NODAYSFORPAYMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> NoDaysForPayment
		{
			get { return (Nullable<System.Int16>)GetValue((int)DunningLevelFieldIndex.NoDaysForPayment, false); }
			set	{ SetValue((int)DunningLevelFieldIndex.NoDaysForPayment, value, true); }
		}

		/// <summary> The OrderNo property of the Entity DunningLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DUNNINGLEVEL"."ORDERNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 OrderNo
		{
			get { return (System.Int16)GetValue((int)DunningLevelFieldIndex.OrderNo, true); }
			set	{ SetValue((int)DunningLevelFieldIndex.OrderNo, value, true); }
		}

		/// <summary> The PaymentMethodID property of the Entity DunningLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DUNNINGLEVEL"."PAYMENTMETHODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PaymentMethodID
		{
			get { return (System.Int64)GetValue((int)DunningLevelFieldIndex.PaymentMethodID, true); }
			set	{ SetValue((int)DunningLevelFieldIndex.PaymentMethodID, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DunningLevelEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
