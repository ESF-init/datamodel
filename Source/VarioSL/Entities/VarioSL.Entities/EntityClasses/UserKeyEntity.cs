﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UserKey'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class UserKeyEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.TicketCollection	_ticketsKey1;
		private bool	_alwaysFetchTicketsKey1, _alreadyFetchedTicketsKey1;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_ticketsKey2;
		private bool	_alwaysFetchTicketsKey2, _alreadyFetchedTicketsKey2;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection	_ticketDeviceclassesKey1;
		private bool	_alwaysFetchTicketDeviceclassesKey1, _alreadyFetchedTicketDeviceclassesKey1;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection	_ticketDeviceclassesKey2;
		private bool	_alwaysFetchTicketDeviceclassesKey2, _alreadyFetchedTicketDeviceclassesKey2;
		private PanelEntity _destinationPanel;
		private bool	_alwaysFetchDestinationPanel, _alreadyFetchedDestinationPanel, _destinationPanelReturnsNewIfNotFound;
		private PanelEntity _panel;
		private bool	_alwaysFetchPanel, _alreadyFetchedPanel, _panelReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DestinationPanel</summary>
			public static readonly string DestinationPanel = "DestinationPanel";
			/// <summary>Member name Panel</summary>
			public static readonly string Panel = "Panel";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name TicketsKey1</summary>
			public static readonly string TicketsKey1 = "TicketsKey1";
			/// <summary>Member name TicketsKey2</summary>
			public static readonly string TicketsKey2 = "TicketsKey2";
			/// <summary>Member name TicketDeviceclassesKey1</summary>
			public static readonly string TicketDeviceclassesKey1 = "TicketDeviceclassesKey1";
			/// <summary>Member name TicketDeviceclassesKey2</summary>
			public static readonly string TicketDeviceclassesKey2 = "TicketDeviceclassesKey2";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UserKeyEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public UserKeyEntity() :base("UserKeyEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="keyID">PK value for UserKey which data should be fetched into this UserKey object</param>
		public UserKeyEntity(System.Int64 keyID):base("UserKeyEntity")
		{
			InitClassFetch(keyID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="keyID">PK value for UserKey which data should be fetched into this UserKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UserKeyEntity(System.Int64 keyID, IPrefetchPath prefetchPathToUse):base("UserKeyEntity")
		{
			InitClassFetch(keyID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="keyID">PK value for UserKey which data should be fetched into this UserKey object</param>
		/// <param name="validator">The custom validator object for this UserKeyEntity</param>
		public UserKeyEntity(System.Int64 keyID, IValidator validator):base("UserKeyEntity")
		{
			InitClassFetch(keyID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UserKeyEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_ticketsKey1 = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketsKey1", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketsKey1 = info.GetBoolean("_alwaysFetchTicketsKey1");
			_alreadyFetchedTicketsKey1 = info.GetBoolean("_alreadyFetchedTicketsKey1");

			_ticketsKey2 = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketsKey2", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketsKey2 = info.GetBoolean("_alwaysFetchTicketsKey2");
			_alreadyFetchedTicketsKey2 = info.GetBoolean("_alreadyFetchedTicketsKey2");

			_ticketDeviceclassesKey1 = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceclassesKey1", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceclassesKey1 = info.GetBoolean("_alwaysFetchTicketDeviceclassesKey1");
			_alreadyFetchedTicketDeviceclassesKey1 = info.GetBoolean("_alreadyFetchedTicketDeviceclassesKey1");

			_ticketDeviceclassesKey2 = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceclassesKey2", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceclassesKey2 = info.GetBoolean("_alwaysFetchTicketDeviceclassesKey2");
			_alreadyFetchedTicketDeviceclassesKey2 = info.GetBoolean("_alreadyFetchedTicketDeviceclassesKey2");
			_destinationPanel = (PanelEntity)info.GetValue("_destinationPanel", typeof(PanelEntity));
			if(_destinationPanel!=null)
			{
				_destinationPanel.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_destinationPanelReturnsNewIfNotFound = info.GetBoolean("_destinationPanelReturnsNewIfNotFound");
			_alwaysFetchDestinationPanel = info.GetBoolean("_alwaysFetchDestinationPanel");
			_alreadyFetchedDestinationPanel = info.GetBoolean("_alreadyFetchedDestinationPanel");

			_panel = (PanelEntity)info.GetValue("_panel", typeof(PanelEntity));
			if(_panel!=null)
			{
				_panel.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_panelReturnsNewIfNotFound = info.GetBoolean("_panelReturnsNewIfNotFound");
			_alwaysFetchPanel = info.GetBoolean("_alwaysFetchPanel");
			_alreadyFetchedPanel = info.GetBoolean("_alreadyFetchedPanel");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UserKeyFieldIndex)fieldIndex)
			{
				case UserKeyFieldIndex.DestinationPanelID:
					DesetupSyncDestinationPanel(true, false);
					_alreadyFetchedDestinationPanel = false;
					break;
				case UserKeyFieldIndex.PanelID:
					DesetupSyncPanel(true, false);
					_alreadyFetchedPanel = false;
					break;
				case UserKeyFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTicketsKey1 = (_ticketsKey1.Count > 0);
			_alreadyFetchedTicketsKey2 = (_ticketsKey2.Count > 0);
			_alreadyFetchedTicketDeviceclassesKey1 = (_ticketDeviceclassesKey1.Count > 0);
			_alreadyFetchedTicketDeviceclassesKey2 = (_ticketDeviceclassesKey2.Count > 0);
			_alreadyFetchedDestinationPanel = (_destinationPanel != null);
			_alreadyFetchedPanel = (_panel != null);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DestinationPanel":
					toReturn.Add(Relations.PanelEntityUsingDestinationPanelID);
					break;
				case "Panel":
					toReturn.Add(Relations.PanelEntityUsingPanelID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "TicketsKey1":
					toReturn.Add(Relations.TicketEntityUsingKey1);
					break;
				case "TicketsKey2":
					toReturn.Add(Relations.TicketEntityUsingKey2);
					break;
				case "TicketDeviceclassesKey1":
					toReturn.Add(Relations.TicketDeviceClassEntityUsingKey1);
					break;
				case "TicketDeviceclassesKey2":
					toReturn.Add(Relations.TicketDeviceClassEntityUsingKey2);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_ticketsKey1", (!this.MarkedForDeletion?_ticketsKey1:null));
			info.AddValue("_alwaysFetchTicketsKey1", _alwaysFetchTicketsKey1);
			info.AddValue("_alreadyFetchedTicketsKey1", _alreadyFetchedTicketsKey1);
			info.AddValue("_ticketsKey2", (!this.MarkedForDeletion?_ticketsKey2:null));
			info.AddValue("_alwaysFetchTicketsKey2", _alwaysFetchTicketsKey2);
			info.AddValue("_alreadyFetchedTicketsKey2", _alreadyFetchedTicketsKey2);
			info.AddValue("_ticketDeviceclassesKey1", (!this.MarkedForDeletion?_ticketDeviceclassesKey1:null));
			info.AddValue("_alwaysFetchTicketDeviceclassesKey1", _alwaysFetchTicketDeviceclassesKey1);
			info.AddValue("_alreadyFetchedTicketDeviceclassesKey1", _alreadyFetchedTicketDeviceclassesKey1);
			info.AddValue("_ticketDeviceclassesKey2", (!this.MarkedForDeletion?_ticketDeviceclassesKey2:null));
			info.AddValue("_alwaysFetchTicketDeviceclassesKey2", _alwaysFetchTicketDeviceclassesKey2);
			info.AddValue("_alreadyFetchedTicketDeviceclassesKey2", _alreadyFetchedTicketDeviceclassesKey2);
			info.AddValue("_destinationPanel", (!this.MarkedForDeletion?_destinationPanel:null));
			info.AddValue("_destinationPanelReturnsNewIfNotFound", _destinationPanelReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDestinationPanel", _alwaysFetchDestinationPanel);
			info.AddValue("_alreadyFetchedDestinationPanel", _alreadyFetchedDestinationPanel);
			info.AddValue("_panel", (!this.MarkedForDeletion?_panel:null));
			info.AddValue("_panelReturnsNewIfNotFound", _panelReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPanel", _alwaysFetchPanel);
			info.AddValue("_alreadyFetchedPanel", _alreadyFetchedPanel);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DestinationPanel":
					_alreadyFetchedDestinationPanel = true;
					this.DestinationPanel = (PanelEntity)entity;
					break;
				case "Panel":
					_alreadyFetchedPanel = true;
					this.Panel = (PanelEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "TicketsKey1":
					_alreadyFetchedTicketsKey1 = true;
					if(entity!=null)
					{
						this.TicketsKey1.Add((TicketEntity)entity);
					}
					break;
				case "TicketsKey2":
					_alreadyFetchedTicketsKey2 = true;
					if(entity!=null)
					{
						this.TicketsKey2.Add((TicketEntity)entity);
					}
					break;
				case "TicketDeviceclassesKey1":
					_alreadyFetchedTicketDeviceclassesKey1 = true;
					if(entity!=null)
					{
						this.TicketDeviceclassesKey1.Add((TicketDeviceClassEntity)entity);
					}
					break;
				case "TicketDeviceclassesKey2":
					_alreadyFetchedTicketDeviceclassesKey2 = true;
					if(entity!=null)
					{
						this.TicketDeviceclassesKey2.Add((TicketDeviceClassEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DestinationPanel":
					SetupSyncDestinationPanel(relatedEntity);
					break;
				case "Panel":
					SetupSyncPanel(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "TicketsKey1":
					_ticketsKey1.Add((TicketEntity)relatedEntity);
					break;
				case "TicketsKey2":
					_ticketsKey2.Add((TicketEntity)relatedEntity);
					break;
				case "TicketDeviceclassesKey1":
					_ticketDeviceclassesKey1.Add((TicketDeviceClassEntity)relatedEntity);
					break;
				case "TicketDeviceclassesKey2":
					_ticketDeviceclassesKey2.Add((TicketDeviceClassEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DestinationPanel":
					DesetupSyncDestinationPanel(false, true);
					break;
				case "Panel":
					DesetupSyncPanel(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "TicketsKey1":
					this.PerformRelatedEntityRemoval(_ticketsKey1, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketsKey2":
					this.PerformRelatedEntityRemoval(_ticketsKey2, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceclassesKey1":
					this.PerformRelatedEntityRemoval(_ticketDeviceclassesKey1, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceclassesKey2":
					this.PerformRelatedEntityRemoval(_ticketDeviceclassesKey2, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_destinationPanel!=null)
			{
				toReturn.Add(_destinationPanel);
			}
			if(_panel!=null)
			{
				toReturn.Add(_panel);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_ticketsKey1);
			toReturn.Add(_ticketsKey2);
			toReturn.Add(_ticketDeviceclassesKey1);
			toReturn.Add(_ticketDeviceclassesKey2);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="keyID">PK value for UserKey which data should be fetched into this UserKey object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 keyID)
		{
			return FetchUsingPK(keyID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="keyID">PK value for UserKey which data should be fetched into this UserKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 keyID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(keyID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="keyID">PK value for UserKey which data should be fetched into this UserKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 keyID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(keyID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="keyID">PK value for UserKey which data should be fetched into this UserKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 keyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(keyID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.KeyID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UserKeyRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsKey1(bool forceFetch)
		{
			return GetMultiTicketsKey1(forceFetch, _ticketsKey1.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsKey1(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketsKey1(forceFetch, _ticketsKey1.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsKey1(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketsKey1(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsKey1(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketsKey1 || forceFetch || _alwaysFetchTicketsKey1) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketsKey1);
				_ticketsKey1.SuppressClearInGetMulti=!forceFetch;
				_ticketsKey1.EntityFactoryToUse = entityFactoryToUse;
				_ticketsKey1.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, filter);
				_ticketsKey1.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketsKey1 = true;
			}
			return _ticketsKey1;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketsKey1'. These settings will be taken into account
		/// when the property TicketsKey1 is requested or GetMultiTicketsKey1 is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketsKey1(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketsKey1.SortClauses=sortClauses;
			_ticketsKey1.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsKey2(bool forceFetch)
		{
			return GetMultiTicketsKey2(forceFetch, _ticketsKey2.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsKey2(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketsKey2(forceFetch, _ticketsKey2.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsKey2(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketsKey2(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsKey2(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketsKey2 || forceFetch || _alwaysFetchTicketsKey2) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketsKey2);
				_ticketsKey2.SuppressClearInGetMulti=!forceFetch;
				_ticketsKey2.EntityFactoryToUse = entityFactoryToUse;
				_ticketsKey2.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, filter);
				_ticketsKey2.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketsKey2 = true;
			}
			return _ticketsKey2;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketsKey2'. These settings will be taken into account
		/// when the property TicketsKey2 is requested or GetMultiTicketsKey2 is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketsKey2(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketsKey2.SortClauses=sortClauses;
			_ticketsKey2.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesKey1(bool forceFetch)
		{
			return GetMultiTicketDeviceclassesKey1(forceFetch, _ticketDeviceclassesKey1.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesKey1(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceclassesKey1(forceFetch, _ticketDeviceclassesKey1.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesKey1(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceclassesKey1(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesKey1(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceclassesKey1 || forceFetch || _alwaysFetchTicketDeviceclassesKey1) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceclassesKey1);
				_ticketDeviceclassesKey1.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceclassesKey1.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceclassesKey1.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, this, null, filter);
				_ticketDeviceclassesKey1.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceclassesKey1 = true;
			}
			return _ticketDeviceclassesKey1;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceclassesKey1'. These settings will be taken into account
		/// when the property TicketDeviceclassesKey1 is requested or GetMultiTicketDeviceclassesKey1 is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceclassesKey1(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceclassesKey1.SortClauses=sortClauses;
			_ticketDeviceclassesKey1.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesKey2(bool forceFetch)
		{
			return GetMultiTicketDeviceclassesKey2(forceFetch, _ticketDeviceclassesKey2.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesKey2(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceclassesKey2(forceFetch, _ticketDeviceclassesKey2.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesKey2(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceclassesKey2(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesKey2(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceclassesKey2 || forceFetch || _alwaysFetchTicketDeviceclassesKey2) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceclassesKey2);
				_ticketDeviceclassesKey2.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceclassesKey2.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceclassesKey2.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, this, filter);
				_ticketDeviceclassesKey2.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceclassesKey2 = true;
			}
			return _ticketDeviceclassesKey2;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceclassesKey2'. These settings will be taken into account
		/// when the property TicketDeviceclassesKey2 is requested or GetMultiTicketDeviceclassesKey2 is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceclassesKey2(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceclassesKey2.SortClauses=sortClauses;
			_ticketDeviceclassesKey2.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'PanelEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PanelEntity' which is related to this entity.</returns>
		public PanelEntity GetSingleDestinationPanel()
		{
			return GetSingleDestinationPanel(false);
		}

		/// <summary> Retrieves the related entity of type 'PanelEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PanelEntity' which is related to this entity.</returns>
		public virtual PanelEntity GetSingleDestinationPanel(bool forceFetch)
		{
			if( ( !_alreadyFetchedDestinationPanel || forceFetch || _alwaysFetchDestinationPanel) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PanelEntityUsingDestinationPanelID);
				PanelEntity newEntity = new PanelEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DestinationPanelID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PanelEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_destinationPanelReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DestinationPanel = newEntity;
				_alreadyFetchedDestinationPanel = fetchResult;
			}
			return _destinationPanel;
		}


		/// <summary> Retrieves the related entity of type 'PanelEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PanelEntity' which is related to this entity.</returns>
		public PanelEntity GetSinglePanel()
		{
			return GetSinglePanel(false);
		}

		/// <summary> Retrieves the related entity of type 'PanelEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PanelEntity' which is related to this entity.</returns>
		public virtual PanelEntity GetSinglePanel(bool forceFetch)
		{
			if( ( !_alreadyFetchedPanel || forceFetch || _alwaysFetchPanel) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PanelEntityUsingPanelID);
				PanelEntity newEntity = new PanelEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PanelID);
				}
				if(fetchResult)
				{
					newEntity = (PanelEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_panelReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Panel = newEntity;
				_alreadyFetchedPanel = fetchResult;
			}
			return _panel;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DestinationPanel", _destinationPanel);
			toReturn.Add("Panel", _panel);
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("TicketsKey1", _ticketsKey1);
			toReturn.Add("TicketsKey2", _ticketsKey2);
			toReturn.Add("TicketDeviceclassesKey1", _ticketDeviceclassesKey1);
			toReturn.Add("TicketDeviceclassesKey2", _ticketDeviceclassesKey2);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="keyID">PK value for UserKey which data should be fetched into this UserKey object</param>
		/// <param name="validator">The validator object for this UserKeyEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 keyID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(keyID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_ticketsKey1 = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_ticketsKey1.SetContainingEntityInfo(this, "UserKey1");

			_ticketsKey2 = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_ticketsKey2.SetContainingEntityInfo(this, "UserKey2");

			_ticketDeviceclassesKey1 = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			_ticketDeviceclassesKey1.SetContainingEntityInfo(this, "UserKey1");

			_ticketDeviceclassesKey2 = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			_ticketDeviceclassesKey2.SetContainingEntityInfo(this, "UserKey2");
			_destinationPanelReturnsNewIfNotFound = false;
			_panelReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Caption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DestinationPanelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyStyleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PanelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _destinationPanel</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDestinationPanel(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _destinationPanel, new PropertyChangedEventHandler( OnDestinationPanelPropertyChanged ), "DestinationPanel", VarioSL.Entities.RelationClasses.StaticUserKeyRelations.PanelEntityUsingDestinationPanelIDStatic, true, signalRelatedEntity, "OriginKeys", resetFKFields, new int[] { (int)UserKeyFieldIndex.DestinationPanelID } );		
			_destinationPanel = null;
		}
		
		/// <summary> setups the sync logic for member _destinationPanel</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDestinationPanel(IEntityCore relatedEntity)
		{
			if(_destinationPanel!=relatedEntity)
			{		
				DesetupSyncDestinationPanel(true, true);
				_destinationPanel = (PanelEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _destinationPanel, new PropertyChangedEventHandler( OnDestinationPanelPropertyChanged ), "DestinationPanel", VarioSL.Entities.RelationClasses.StaticUserKeyRelations.PanelEntityUsingDestinationPanelIDStatic, true, ref _alreadyFetchedDestinationPanel, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDestinationPanelPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _panel</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPanel(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _panel, new PropertyChangedEventHandler( OnPanelPropertyChanged ), "Panel", VarioSL.Entities.RelationClasses.StaticUserKeyRelations.PanelEntityUsingPanelIDStatic, true, signalRelatedEntity, "UserKeys", resetFKFields, new int[] { (int)UserKeyFieldIndex.PanelID } );		
			_panel = null;
		}
		
		/// <summary> setups the sync logic for member _panel</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPanel(IEntityCore relatedEntity)
		{
			if(_panel!=relatedEntity)
			{		
				DesetupSyncPanel(true, true);
				_panel = (PanelEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _panel, new PropertyChangedEventHandler( OnPanelPropertyChanged ), "Panel", VarioSL.Entities.RelationClasses.StaticUserKeyRelations.PanelEntityUsingPanelIDStatic, true, ref _alreadyFetchedPanel, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPanelPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticUserKeyRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "UserKeys", resetFKFields, new int[] { (int)UserKeyFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticUserKeyRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="keyID">PK value for UserKey which data should be fetched into this UserKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 keyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UserKeyFieldIndex.KeyID].ForcedCurrentValueWrite(keyID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUserKeyDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UserKeyEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UserKeyRelations Relations
		{
			get	{ return new UserKeyRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketsKey1
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("TicketsKey1")[0], (int)VarioSL.Entities.EntityType.UserKeyEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "TicketsKey1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketsKey2
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("TicketsKey2")[0], (int)VarioSL.Entities.EntityType.UserKeyEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "TicketsKey2", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceclassesKey1
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceclassesKey1")[0], (int)VarioSL.Entities.EntityType.UserKeyEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, null, "TicketDeviceclassesKey1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceclassesKey2
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceclassesKey2")[0], (int)VarioSL.Entities.EntityType.UserKeyEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, null, "TicketDeviceclassesKey2", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Panel'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDestinationPanel
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PanelCollection(), (IEntityRelation)GetRelationsForField("DestinationPanel")[0], (int)VarioSL.Entities.EntityType.UserKeyEntity, (int)VarioSL.Entities.EntityType.PanelEntity, 0, null, null, null, "DestinationPanel", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Panel'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPanel
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PanelCollection(), (IEntityRelation)GetRelationsForField("Panel")[0], (int)VarioSL.Entities.EntityType.UserKeyEntity, (int)VarioSL.Entities.EntityType.PanelEntity, 0, null, null, null, "Panel", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.UserKeyEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Caption property of the Entity UserKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_USERKEYS"."CAPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Caption
		{
			get { return (System.String)GetValue((int)UserKeyFieldIndex.Caption, true); }
			set	{ SetValue((int)UserKeyFieldIndex.Caption, value, true); }
		}

		/// <summary> The Description property of the Entity UserKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_USERKEYS"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)UserKeyFieldIndex.Description, true); }
			set	{ SetValue((int)UserKeyFieldIndex.Description, value, true); }
		}

		/// <summary> The DestinationPanelID property of the Entity UserKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_USERKEYS"."DESTINATIONPANELID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DestinationPanelID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UserKeyFieldIndex.DestinationPanelID, false); }
			set	{ SetValue((int)UserKeyFieldIndex.DestinationPanelID, value, true); }
		}

		/// <summary> The KeyID property of the Entity UserKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_USERKEYS"."KEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 KeyID
		{
			get { return (System.Int64)GetValue((int)UserKeyFieldIndex.KeyID, true); }
			set	{ SetValue((int)UserKeyFieldIndex.KeyID, value, true); }
		}

		/// <summary> The KeyNumber property of the Entity UserKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_USERKEYS"."KEYNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 KeyNumber
		{
			get { return (System.Int64)GetValue((int)UserKeyFieldIndex.KeyNumber, true); }
			set	{ SetValue((int)UserKeyFieldIndex.KeyNumber, value, true); }
		}

		/// <summary> The KeyStyleID property of the Entity UserKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_USERKEYS"."KEYSTYLEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> KeyStyleID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UserKeyFieldIndex.KeyStyleID, false); }
			set	{ SetValue((int)UserKeyFieldIndex.KeyStyleID, value, true); }
		}

		/// <summary> The PanelID property of the Entity UserKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_USERKEYS"."PANELID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PanelID
		{
			get { return (System.Int64)GetValue((int)UserKeyFieldIndex.PanelID, true); }
			set	{ SetValue((int)UserKeyFieldIndex.PanelID, value, true); }
		}

		/// <summary> The TariffID property of the Entity UserKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_USERKEYS"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UserKeyFieldIndex.TariffID, false); }
			set	{ SetValue((int)UserKeyFieldIndex.TariffID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketsKey1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketsKey1
		{
			get	{ return GetMultiTicketsKey1(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketsKey1. When set to true, TicketsKey1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketsKey1 is accessed. You can always execute/ a forced fetch by calling GetMultiTicketsKey1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketsKey1
		{
			get	{ return _alwaysFetchTicketsKey1; }
			set	{ _alwaysFetchTicketsKey1 = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketsKey1 already has been fetched. Setting this property to false when TicketsKey1 has been fetched
		/// will clear the TicketsKey1 collection well. Setting this property to true while TicketsKey1 hasn't been fetched disables lazy loading for TicketsKey1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketsKey1
		{
			get { return _alreadyFetchedTicketsKey1;}
			set 
			{
				if(_alreadyFetchedTicketsKey1 && !value && (_ticketsKey1 != null))
				{
					_ticketsKey1.Clear();
				}
				_alreadyFetchedTicketsKey1 = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketsKey2()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketsKey2
		{
			get	{ return GetMultiTicketsKey2(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketsKey2. When set to true, TicketsKey2 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketsKey2 is accessed. You can always execute/ a forced fetch by calling GetMultiTicketsKey2(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketsKey2
		{
			get	{ return _alwaysFetchTicketsKey2; }
			set	{ _alwaysFetchTicketsKey2 = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketsKey2 already has been fetched. Setting this property to false when TicketsKey2 has been fetched
		/// will clear the TicketsKey2 collection well. Setting this property to true while TicketsKey2 hasn't been fetched disables lazy loading for TicketsKey2</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketsKey2
		{
			get { return _alreadyFetchedTicketsKey2;}
			set 
			{
				if(_alreadyFetchedTicketsKey2 && !value && (_ticketsKey2 != null))
				{
					_ticketsKey2.Clear();
				}
				_alreadyFetchedTicketsKey2 = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceclassesKey1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceclassesKey1
		{
			get	{ return GetMultiTicketDeviceclassesKey1(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceclassesKey1. When set to true, TicketDeviceclassesKey1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceclassesKey1 is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceclassesKey1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceclassesKey1
		{
			get	{ return _alwaysFetchTicketDeviceclassesKey1; }
			set	{ _alwaysFetchTicketDeviceclassesKey1 = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceclassesKey1 already has been fetched. Setting this property to false when TicketDeviceclassesKey1 has been fetched
		/// will clear the TicketDeviceclassesKey1 collection well. Setting this property to true while TicketDeviceclassesKey1 hasn't been fetched disables lazy loading for TicketDeviceclassesKey1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceclassesKey1
		{
			get { return _alreadyFetchedTicketDeviceclassesKey1;}
			set 
			{
				if(_alreadyFetchedTicketDeviceclassesKey1 && !value && (_ticketDeviceclassesKey1 != null))
				{
					_ticketDeviceclassesKey1.Clear();
				}
				_alreadyFetchedTicketDeviceclassesKey1 = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceclassesKey2()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceclassesKey2
		{
			get	{ return GetMultiTicketDeviceclassesKey2(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceclassesKey2. When set to true, TicketDeviceclassesKey2 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceclassesKey2 is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceclassesKey2(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceclassesKey2
		{
			get	{ return _alwaysFetchTicketDeviceclassesKey2; }
			set	{ _alwaysFetchTicketDeviceclassesKey2 = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceclassesKey2 already has been fetched. Setting this property to false when TicketDeviceclassesKey2 has been fetched
		/// will clear the TicketDeviceclassesKey2 collection well. Setting this property to true while TicketDeviceclassesKey2 hasn't been fetched disables lazy loading for TicketDeviceclassesKey2</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceclassesKey2
		{
			get { return _alreadyFetchedTicketDeviceclassesKey2;}
			set 
			{
				if(_alreadyFetchedTicketDeviceclassesKey2 && !value && (_ticketDeviceclassesKey2 != null))
				{
					_ticketDeviceclassesKey2.Clear();
				}
				_alreadyFetchedTicketDeviceclassesKey2 = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'PanelEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDestinationPanel()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PanelEntity DestinationPanel
		{
			get	{ return GetSingleDestinationPanel(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDestinationPanel(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OriginKeys", "DestinationPanel", _destinationPanel, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DestinationPanel. When set to true, DestinationPanel is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DestinationPanel is accessed. You can always execute a forced fetch by calling GetSingleDestinationPanel(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDestinationPanel
		{
			get	{ return _alwaysFetchDestinationPanel; }
			set	{ _alwaysFetchDestinationPanel = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DestinationPanel already has been fetched. Setting this property to false when DestinationPanel has been fetched
		/// will set DestinationPanel to null as well. Setting this property to true while DestinationPanel hasn't been fetched disables lazy loading for DestinationPanel</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDestinationPanel
		{
			get { return _alreadyFetchedDestinationPanel;}
			set 
			{
				if(_alreadyFetchedDestinationPanel && !value)
				{
					this.DestinationPanel = null;
				}
				_alreadyFetchedDestinationPanel = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DestinationPanel is not found
		/// in the database. When set to true, DestinationPanel will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DestinationPanelReturnsNewIfNotFound
		{
			get	{ return _destinationPanelReturnsNewIfNotFound; }
			set { _destinationPanelReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PanelEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePanel()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PanelEntity Panel
		{
			get	{ return GetSinglePanel(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPanel(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UserKeys", "Panel", _panel, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Panel. When set to true, Panel is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Panel is accessed. You can always execute a forced fetch by calling GetSinglePanel(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPanel
		{
			get	{ return _alwaysFetchPanel; }
			set	{ _alwaysFetchPanel = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Panel already has been fetched. Setting this property to false when Panel has been fetched
		/// will set Panel to null as well. Setting this property to true while Panel hasn't been fetched disables lazy loading for Panel</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPanel
		{
			get { return _alreadyFetchedPanel;}
			set 
			{
				if(_alreadyFetchedPanel && !value)
				{
					this.Panel = null;
				}
				_alreadyFetchedPanel = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Panel is not found
		/// in the database. When set to true, Panel will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PanelReturnsNewIfNotFound
		{
			get	{ return _panelReturnsNewIfNotFound; }
			set { _panelReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UserKeys", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.UserKeyEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
