﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RulePeriod'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RulePeriodEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.RuleCappingCollection	_ruleCapping;
		private bool	_alwaysFetchRuleCapping, _alreadyFetchedRuleCapping;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_tickets;
		private bool	_alwaysFetchTickets, _alreadyFetchedTickets;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection	_ticketDeviceClass;
		private bool	_alwaysFetchTicketDeviceClass, _alreadyFetchedTicketDeviceClass;
		private VarioSL.Entities.CollectionClasses.VdvProductCollection	_vdvProduct;
		private bool	_alwaysFetchVdvProduct, _alreadyFetchedVdvProduct;
		private CalendarEntity _calendar;
		private bool	_alwaysFetchCalendar, _alreadyFetchedCalendar, _calendarReturnsNewIfNotFound;
		private CalendarEntity _extensionCalendar;
		private bool	_alwaysFetchExtensionCalendar, _alreadyFetchedExtensionCalendar, _extensionCalendarReturnsNewIfNotFound;
		private RuleTypeEntity _ruleTypePeriodUnit;
		private bool	_alwaysFetchRuleTypePeriodUnit, _alreadyFetchedRuleTypePeriodUnit, _ruleTypePeriodUnitReturnsNewIfNotFound;
		private RuleTypeEntity _ruleTypesValidTimeUnit;
		private bool	_alwaysFetchRuleTypesValidTimeUnit, _alreadyFetchedRuleTypesValidTimeUnit, _ruleTypesValidTimeUnitReturnsNewIfNotFound;
		private RuleTypeEntity _ruleTypeValidationType;
		private bool	_alwaysFetchRuleTypeValidationType, _alreadyFetchedRuleTypeValidationType, _ruleTypeValidationTypeReturnsNewIfNotFound;
		private RuleTypeEntity _ruleTypeExtTimeUnit;
		private bool	_alwaysFetchRuleTypeExtTimeUnit, _alreadyFetchedRuleTypeExtTimeUnit, _ruleTypeExtTimeUnitReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Calendar</summary>
			public static readonly string Calendar = "Calendar";
			/// <summary>Member name ExtensionCalendar</summary>
			public static readonly string ExtensionCalendar = "ExtensionCalendar";
			/// <summary>Member name RuleTypePeriodUnit</summary>
			public static readonly string RuleTypePeriodUnit = "RuleTypePeriodUnit";
			/// <summary>Member name RuleTypesValidTimeUnit</summary>
			public static readonly string RuleTypesValidTimeUnit = "RuleTypesValidTimeUnit";
			/// <summary>Member name RuleTypeValidationType</summary>
			public static readonly string RuleTypeValidationType = "RuleTypeValidationType";
			/// <summary>Member name RuleTypeExtTimeUnit</summary>
			public static readonly string RuleTypeExtTimeUnit = "RuleTypeExtTimeUnit";
			/// <summary>Member name RuleCapping</summary>
			public static readonly string RuleCapping = "RuleCapping";
			/// <summary>Member name Tickets</summary>
			public static readonly string Tickets = "Tickets";
			/// <summary>Member name TicketDeviceClass</summary>
			public static readonly string TicketDeviceClass = "TicketDeviceClass";
			/// <summary>Member name VdvProduct</summary>
			public static readonly string VdvProduct = "VdvProduct";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RulePeriodEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RulePeriodEntity() :base("RulePeriodEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="rulePeriodID">PK value for RulePeriod which data should be fetched into this RulePeriod object</param>
		public RulePeriodEntity(System.Int64 rulePeriodID):base("RulePeriodEntity")
		{
			InitClassFetch(rulePeriodID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="rulePeriodID">PK value for RulePeriod which data should be fetched into this RulePeriod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RulePeriodEntity(System.Int64 rulePeriodID, IPrefetchPath prefetchPathToUse):base("RulePeriodEntity")
		{
			InitClassFetch(rulePeriodID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="rulePeriodID">PK value for RulePeriod which data should be fetched into this RulePeriod object</param>
		/// <param name="validator">The custom validator object for this RulePeriodEntity</param>
		public RulePeriodEntity(System.Int64 rulePeriodID, IValidator validator):base("RulePeriodEntity")
		{
			InitClassFetch(rulePeriodID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RulePeriodEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_ruleCapping = (VarioSL.Entities.CollectionClasses.RuleCappingCollection)info.GetValue("_ruleCapping", typeof(VarioSL.Entities.CollectionClasses.RuleCappingCollection));
			_alwaysFetchRuleCapping = info.GetBoolean("_alwaysFetchRuleCapping");
			_alreadyFetchedRuleCapping = info.GetBoolean("_alreadyFetchedRuleCapping");

			_tickets = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_tickets", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTickets = info.GetBoolean("_alwaysFetchTickets");
			_alreadyFetchedTickets = info.GetBoolean("_alreadyFetchedTickets");

			_ticketDeviceClass = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceClass", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceClass = info.GetBoolean("_alwaysFetchTicketDeviceClass");
			_alreadyFetchedTicketDeviceClass = info.GetBoolean("_alreadyFetchedTicketDeviceClass");

			_vdvProduct = (VarioSL.Entities.CollectionClasses.VdvProductCollection)info.GetValue("_vdvProduct", typeof(VarioSL.Entities.CollectionClasses.VdvProductCollection));
			_alwaysFetchVdvProduct = info.GetBoolean("_alwaysFetchVdvProduct");
			_alreadyFetchedVdvProduct = info.GetBoolean("_alreadyFetchedVdvProduct");
			_calendar = (CalendarEntity)info.GetValue("_calendar", typeof(CalendarEntity));
			if(_calendar!=null)
			{
				_calendar.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_calendarReturnsNewIfNotFound = info.GetBoolean("_calendarReturnsNewIfNotFound");
			_alwaysFetchCalendar = info.GetBoolean("_alwaysFetchCalendar");
			_alreadyFetchedCalendar = info.GetBoolean("_alreadyFetchedCalendar");

			_extensionCalendar = (CalendarEntity)info.GetValue("_extensionCalendar", typeof(CalendarEntity));
			if(_extensionCalendar!=null)
			{
				_extensionCalendar.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_extensionCalendarReturnsNewIfNotFound = info.GetBoolean("_extensionCalendarReturnsNewIfNotFound");
			_alwaysFetchExtensionCalendar = info.GetBoolean("_alwaysFetchExtensionCalendar");
			_alreadyFetchedExtensionCalendar = info.GetBoolean("_alreadyFetchedExtensionCalendar");

			_ruleTypePeriodUnit = (RuleTypeEntity)info.GetValue("_ruleTypePeriodUnit", typeof(RuleTypeEntity));
			if(_ruleTypePeriodUnit!=null)
			{
				_ruleTypePeriodUnit.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ruleTypePeriodUnitReturnsNewIfNotFound = info.GetBoolean("_ruleTypePeriodUnitReturnsNewIfNotFound");
			_alwaysFetchRuleTypePeriodUnit = info.GetBoolean("_alwaysFetchRuleTypePeriodUnit");
			_alreadyFetchedRuleTypePeriodUnit = info.GetBoolean("_alreadyFetchedRuleTypePeriodUnit");

			_ruleTypesValidTimeUnit = (RuleTypeEntity)info.GetValue("_ruleTypesValidTimeUnit", typeof(RuleTypeEntity));
			if(_ruleTypesValidTimeUnit!=null)
			{
				_ruleTypesValidTimeUnit.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ruleTypesValidTimeUnitReturnsNewIfNotFound = info.GetBoolean("_ruleTypesValidTimeUnitReturnsNewIfNotFound");
			_alwaysFetchRuleTypesValidTimeUnit = info.GetBoolean("_alwaysFetchRuleTypesValidTimeUnit");
			_alreadyFetchedRuleTypesValidTimeUnit = info.GetBoolean("_alreadyFetchedRuleTypesValidTimeUnit");

			_ruleTypeValidationType = (RuleTypeEntity)info.GetValue("_ruleTypeValidationType", typeof(RuleTypeEntity));
			if(_ruleTypeValidationType!=null)
			{
				_ruleTypeValidationType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ruleTypeValidationTypeReturnsNewIfNotFound = info.GetBoolean("_ruleTypeValidationTypeReturnsNewIfNotFound");
			_alwaysFetchRuleTypeValidationType = info.GetBoolean("_alwaysFetchRuleTypeValidationType");
			_alreadyFetchedRuleTypeValidationType = info.GetBoolean("_alreadyFetchedRuleTypeValidationType");

			_ruleTypeExtTimeUnit = (RuleTypeEntity)info.GetValue("_ruleTypeExtTimeUnit", typeof(RuleTypeEntity));
			if(_ruleTypeExtTimeUnit!=null)
			{
				_ruleTypeExtTimeUnit.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ruleTypeExtTimeUnitReturnsNewIfNotFound = info.GetBoolean("_ruleTypeExtTimeUnitReturnsNewIfNotFound");
			_alwaysFetchRuleTypeExtTimeUnit = info.GetBoolean("_alwaysFetchRuleTypeExtTimeUnit");
			_alreadyFetchedRuleTypeExtTimeUnit = info.GetBoolean("_alreadyFetchedRuleTypeExtTimeUnit");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RulePeriodFieldIndex)fieldIndex)
			{
				case RulePeriodFieldIndex.CalendarID:
					DesetupSyncCalendar(true, false);
					_alreadyFetchedCalendar = false;
					break;
				case RulePeriodFieldIndex.ExtCalendarID:
					DesetupSyncExtensionCalendar(true, false);
					_alreadyFetchedExtensionCalendar = false;
					break;
				case RulePeriodFieldIndex.ExtTimeUnit:
					DesetupSyncRuleTypeExtTimeUnit(true, false);
					_alreadyFetchedRuleTypeExtTimeUnit = false;
					break;
				case RulePeriodFieldIndex.PeriodUnit:
					DesetupSyncRuleTypePeriodUnit(true, false);
					_alreadyFetchedRuleTypePeriodUnit = false;
					break;
				case RulePeriodFieldIndex.ValidationType:
					DesetupSyncRuleTypeValidationType(true, false);
					_alreadyFetchedRuleTypeValidationType = false;
					break;
				case RulePeriodFieldIndex.ValidTimeUnit:
					DesetupSyncRuleTypesValidTimeUnit(true, false);
					_alreadyFetchedRuleTypesValidTimeUnit = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRuleCapping = (_ruleCapping.Count > 0);
			_alreadyFetchedTickets = (_tickets.Count > 0);
			_alreadyFetchedTicketDeviceClass = (_ticketDeviceClass.Count > 0);
			_alreadyFetchedVdvProduct = (_vdvProduct.Count > 0);
			_alreadyFetchedCalendar = (_calendar != null);
			_alreadyFetchedExtensionCalendar = (_extensionCalendar != null);
			_alreadyFetchedRuleTypePeriodUnit = (_ruleTypePeriodUnit != null);
			_alreadyFetchedRuleTypesValidTimeUnit = (_ruleTypesValidTimeUnit != null);
			_alreadyFetchedRuleTypeValidationType = (_ruleTypeValidationType != null);
			_alreadyFetchedRuleTypeExtTimeUnit = (_ruleTypeExtTimeUnit != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Calendar":
					toReturn.Add(Relations.CalendarEntityUsingCalendarID);
					break;
				case "ExtensionCalendar":
					toReturn.Add(Relations.CalendarEntityUsingExtCalendarID);
					break;
				case "RuleTypePeriodUnit":
					toReturn.Add(Relations.RuleTypeEntityUsingPeriodUnit);
					break;
				case "RuleTypesValidTimeUnit":
					toReturn.Add(Relations.RuleTypeEntityUsingValidTimeUnit);
					break;
				case "RuleTypeValidationType":
					toReturn.Add(Relations.RuleTypeEntityUsingValidationType);
					break;
				case "RuleTypeExtTimeUnit":
					toReturn.Add(Relations.RuleTypeEntityUsingExtTimeUnit);
					break;
				case "RuleCapping":
					toReturn.Add(Relations.RuleCappingEntityUsingRulePeriodID);
					break;
				case "Tickets":
					toReturn.Add(Relations.TicketEntityUsingRulePeriodID);
					break;
				case "TicketDeviceClass":
					toReturn.Add(Relations.TicketDeviceClassEntityUsingRulePeriodID);
					break;
				case "VdvProduct":
					toReturn.Add(Relations.VdvProductEntityUsingPeriodID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_ruleCapping", (!this.MarkedForDeletion?_ruleCapping:null));
			info.AddValue("_alwaysFetchRuleCapping", _alwaysFetchRuleCapping);
			info.AddValue("_alreadyFetchedRuleCapping", _alreadyFetchedRuleCapping);
			info.AddValue("_tickets", (!this.MarkedForDeletion?_tickets:null));
			info.AddValue("_alwaysFetchTickets", _alwaysFetchTickets);
			info.AddValue("_alreadyFetchedTickets", _alreadyFetchedTickets);
			info.AddValue("_ticketDeviceClass", (!this.MarkedForDeletion?_ticketDeviceClass:null));
			info.AddValue("_alwaysFetchTicketDeviceClass", _alwaysFetchTicketDeviceClass);
			info.AddValue("_alreadyFetchedTicketDeviceClass", _alreadyFetchedTicketDeviceClass);
			info.AddValue("_vdvProduct", (!this.MarkedForDeletion?_vdvProduct:null));
			info.AddValue("_alwaysFetchVdvProduct", _alwaysFetchVdvProduct);
			info.AddValue("_alreadyFetchedVdvProduct", _alreadyFetchedVdvProduct);
			info.AddValue("_calendar", (!this.MarkedForDeletion?_calendar:null));
			info.AddValue("_calendarReturnsNewIfNotFound", _calendarReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCalendar", _alwaysFetchCalendar);
			info.AddValue("_alreadyFetchedCalendar", _alreadyFetchedCalendar);
			info.AddValue("_extensionCalendar", (!this.MarkedForDeletion?_extensionCalendar:null));
			info.AddValue("_extensionCalendarReturnsNewIfNotFound", _extensionCalendarReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExtensionCalendar", _alwaysFetchExtensionCalendar);
			info.AddValue("_alreadyFetchedExtensionCalendar", _alreadyFetchedExtensionCalendar);
			info.AddValue("_ruleTypePeriodUnit", (!this.MarkedForDeletion?_ruleTypePeriodUnit:null));
			info.AddValue("_ruleTypePeriodUnitReturnsNewIfNotFound", _ruleTypePeriodUnitReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRuleTypePeriodUnit", _alwaysFetchRuleTypePeriodUnit);
			info.AddValue("_alreadyFetchedRuleTypePeriodUnit", _alreadyFetchedRuleTypePeriodUnit);
			info.AddValue("_ruleTypesValidTimeUnit", (!this.MarkedForDeletion?_ruleTypesValidTimeUnit:null));
			info.AddValue("_ruleTypesValidTimeUnitReturnsNewIfNotFound", _ruleTypesValidTimeUnitReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRuleTypesValidTimeUnit", _alwaysFetchRuleTypesValidTimeUnit);
			info.AddValue("_alreadyFetchedRuleTypesValidTimeUnit", _alreadyFetchedRuleTypesValidTimeUnit);
			info.AddValue("_ruleTypeValidationType", (!this.MarkedForDeletion?_ruleTypeValidationType:null));
			info.AddValue("_ruleTypeValidationTypeReturnsNewIfNotFound", _ruleTypeValidationTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRuleTypeValidationType", _alwaysFetchRuleTypeValidationType);
			info.AddValue("_alreadyFetchedRuleTypeValidationType", _alreadyFetchedRuleTypeValidationType);
			info.AddValue("_ruleTypeExtTimeUnit", (!this.MarkedForDeletion?_ruleTypeExtTimeUnit:null));
			info.AddValue("_ruleTypeExtTimeUnitReturnsNewIfNotFound", _ruleTypeExtTimeUnitReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRuleTypeExtTimeUnit", _alwaysFetchRuleTypeExtTimeUnit);
			info.AddValue("_alreadyFetchedRuleTypeExtTimeUnit", _alreadyFetchedRuleTypeExtTimeUnit);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Calendar":
					_alreadyFetchedCalendar = true;
					this.Calendar = (CalendarEntity)entity;
					break;
				case "ExtensionCalendar":
					_alreadyFetchedExtensionCalendar = true;
					this.ExtensionCalendar = (CalendarEntity)entity;
					break;
				case "RuleTypePeriodUnit":
					_alreadyFetchedRuleTypePeriodUnit = true;
					this.RuleTypePeriodUnit = (RuleTypeEntity)entity;
					break;
				case "RuleTypesValidTimeUnit":
					_alreadyFetchedRuleTypesValidTimeUnit = true;
					this.RuleTypesValidTimeUnit = (RuleTypeEntity)entity;
					break;
				case "RuleTypeValidationType":
					_alreadyFetchedRuleTypeValidationType = true;
					this.RuleTypeValidationType = (RuleTypeEntity)entity;
					break;
				case "RuleTypeExtTimeUnit":
					_alreadyFetchedRuleTypeExtTimeUnit = true;
					this.RuleTypeExtTimeUnit = (RuleTypeEntity)entity;
					break;
				case "RuleCapping":
					_alreadyFetchedRuleCapping = true;
					if(entity!=null)
					{
						this.RuleCapping.Add((RuleCappingEntity)entity);
					}
					break;
				case "Tickets":
					_alreadyFetchedTickets = true;
					if(entity!=null)
					{
						this.Tickets.Add((TicketEntity)entity);
					}
					break;
				case "TicketDeviceClass":
					_alreadyFetchedTicketDeviceClass = true;
					if(entity!=null)
					{
						this.TicketDeviceClass.Add((TicketDeviceClassEntity)entity);
					}
					break;
				case "VdvProduct":
					_alreadyFetchedVdvProduct = true;
					if(entity!=null)
					{
						this.VdvProduct.Add((VdvProductEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Calendar":
					SetupSyncCalendar(relatedEntity);
					break;
				case "ExtensionCalendar":
					SetupSyncExtensionCalendar(relatedEntity);
					break;
				case "RuleTypePeriodUnit":
					SetupSyncRuleTypePeriodUnit(relatedEntity);
					break;
				case "RuleTypesValidTimeUnit":
					SetupSyncRuleTypesValidTimeUnit(relatedEntity);
					break;
				case "RuleTypeValidationType":
					SetupSyncRuleTypeValidationType(relatedEntity);
					break;
				case "RuleTypeExtTimeUnit":
					SetupSyncRuleTypeExtTimeUnit(relatedEntity);
					break;
				case "RuleCapping":
					_ruleCapping.Add((RuleCappingEntity)relatedEntity);
					break;
				case "Tickets":
					_tickets.Add((TicketEntity)relatedEntity);
					break;
				case "TicketDeviceClass":
					_ticketDeviceClass.Add((TicketDeviceClassEntity)relatedEntity);
					break;
				case "VdvProduct":
					_vdvProduct.Add((VdvProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Calendar":
					DesetupSyncCalendar(false, true);
					break;
				case "ExtensionCalendar":
					DesetupSyncExtensionCalendar(false, true);
					break;
				case "RuleTypePeriodUnit":
					DesetupSyncRuleTypePeriodUnit(false, true);
					break;
				case "RuleTypesValidTimeUnit":
					DesetupSyncRuleTypesValidTimeUnit(false, true);
					break;
				case "RuleTypeValidationType":
					DesetupSyncRuleTypeValidationType(false, true);
					break;
				case "RuleTypeExtTimeUnit":
					DesetupSyncRuleTypeExtTimeUnit(false, true);
					break;
				case "RuleCapping":
					this.PerformRelatedEntityRemoval(_ruleCapping, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Tickets":
					this.PerformRelatedEntityRemoval(_tickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceClass":
					this.PerformRelatedEntityRemoval(_ticketDeviceClass, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvProduct":
					this.PerformRelatedEntityRemoval(_vdvProduct, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_calendar!=null)
			{
				toReturn.Add(_calendar);
			}
			if(_extensionCalendar!=null)
			{
				toReturn.Add(_extensionCalendar);
			}
			if(_ruleTypePeriodUnit!=null)
			{
				toReturn.Add(_ruleTypePeriodUnit);
			}
			if(_ruleTypesValidTimeUnit!=null)
			{
				toReturn.Add(_ruleTypesValidTimeUnit);
			}
			if(_ruleTypeValidationType!=null)
			{
				toReturn.Add(_ruleTypeValidationType);
			}
			if(_ruleTypeExtTimeUnit!=null)
			{
				toReturn.Add(_ruleTypeExtTimeUnit);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_ruleCapping);
			toReturn.Add(_tickets);
			toReturn.Add(_ticketDeviceClass);
			toReturn.Add(_vdvProduct);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="rulePeriodID">PK value for RulePeriod which data should be fetched into this RulePeriod object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 rulePeriodID)
		{
			return FetchUsingPK(rulePeriodID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="rulePeriodID">PK value for RulePeriod which data should be fetched into this RulePeriod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 rulePeriodID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(rulePeriodID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="rulePeriodID">PK value for RulePeriod which data should be fetched into this RulePeriod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 rulePeriodID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(rulePeriodID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="rulePeriodID">PK value for RulePeriod which data should be fetched into this RulePeriod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 rulePeriodID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(rulePeriodID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RulePeriodID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RulePeriodRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCapping(bool forceFetch)
		{
			return GetMultiRuleCapping(forceFetch, _ruleCapping.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCapping(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleCapping(forceFetch, _ruleCapping.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCapping(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleCapping(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCapping(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleCapping || forceFetch || _alwaysFetchRuleCapping) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleCapping);
				_ruleCapping.SuppressClearInGetMulti=!forceFetch;
				_ruleCapping.EntityFactoryToUse = entityFactoryToUse;
				_ruleCapping.GetMultiManyToOne(null, this, null, null, filter);
				_ruleCapping.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleCapping = true;
			}
			return _ruleCapping;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleCapping'. These settings will be taken into account
		/// when the property RuleCapping is requested or GetMultiRuleCapping is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleCapping(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleCapping.SortClauses=sortClauses;
			_ruleCapping.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTickets || forceFetch || _alwaysFetchTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tickets);
				_tickets.SuppressClearInGetMulti=!forceFetch;
				_tickets.EntityFactoryToUse = entityFactoryToUse;
				_tickets.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, filter);
				_tickets.SuppressClearInGetMulti=false;
				_alreadyFetchedTickets = true;
			}
			return _tickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'Tickets'. These settings will be taken into account
		/// when the property Tickets is requested or GetMultiTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tickets.SortClauses=sortClauses;
			_tickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClass(bool forceFetch)
		{
			return GetMultiTicketDeviceClass(forceFetch, _ticketDeviceClass.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClass(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceClass(forceFetch, _ticketDeviceClass.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClass(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceClass(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClass(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceClass || forceFetch || _alwaysFetchTicketDeviceClass) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceClass);
				_ticketDeviceClass.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceClass.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceClass.GetMultiManyToOne(null, null, null, null, null, null, null, null, this, null, null, null, null, filter);
				_ticketDeviceClass.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceClass = true;
			}
			return _ticketDeviceClass;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceClass'. These settings will be taken into account
		/// when the property TicketDeviceClass is requested or GetMultiTicketDeviceClass is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceClass(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceClass.SortClauses=sortClauses;
			_ticketDeviceClass.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch)
		{
			return GetMultiVdvProduct(forceFetch, _vdvProduct.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvProduct(forceFetch, _vdvProduct.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvProduct(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvProduct || forceFetch || _alwaysFetchVdvProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvProduct);
				_vdvProduct.SuppressClearInGetMulti=!forceFetch;
				_vdvProduct.EntityFactoryToUse = entityFactoryToUse;
				_vdvProduct.GetMultiManyToOne(null, this, null, null, null, null, null, null, filter);
				_vdvProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvProduct = true;
			}
			return _vdvProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvProduct'. These settings will be taken into account
		/// when the property VdvProduct is requested or GetMultiVdvProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvProduct.SortClauses=sortClauses;
			_vdvProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CalendarEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CalendarEntity' which is related to this entity.</returns>
		public CalendarEntity GetSingleCalendar()
		{
			return GetSingleCalendar(false);
		}

		/// <summary> Retrieves the related entity of type 'CalendarEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CalendarEntity' which is related to this entity.</returns>
		public virtual CalendarEntity GetSingleCalendar(bool forceFetch)
		{
			if( ( !_alreadyFetchedCalendar || forceFetch || _alwaysFetchCalendar) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CalendarEntityUsingCalendarID);
				CalendarEntity newEntity = new CalendarEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CalendarID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CalendarEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_calendarReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Calendar = newEntity;
				_alreadyFetchedCalendar = fetchResult;
			}
			return _calendar;
		}


		/// <summary> Retrieves the related entity of type 'CalendarEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CalendarEntity' which is related to this entity.</returns>
		public CalendarEntity GetSingleExtensionCalendar()
		{
			return GetSingleExtensionCalendar(false);
		}

		/// <summary> Retrieves the related entity of type 'CalendarEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CalendarEntity' which is related to this entity.</returns>
		public virtual CalendarEntity GetSingleExtensionCalendar(bool forceFetch)
		{
			if( ( !_alreadyFetchedExtensionCalendar || forceFetch || _alwaysFetchExtensionCalendar) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CalendarEntityUsingExtCalendarID);
				CalendarEntity newEntity = new CalendarEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ExtCalendarID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CalendarEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_extensionCalendarReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExtensionCalendar = newEntity;
				_alreadyFetchedExtensionCalendar = fetchResult;
			}
			return _extensionCalendar;
		}


		/// <summary> Retrieves the related entity of type 'RuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RuleTypeEntity' which is related to this entity.</returns>
		public RuleTypeEntity GetSingleRuleTypePeriodUnit()
		{
			return GetSingleRuleTypePeriodUnit(false);
		}

		/// <summary> Retrieves the related entity of type 'RuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RuleTypeEntity' which is related to this entity.</returns>
		public virtual RuleTypeEntity GetSingleRuleTypePeriodUnit(bool forceFetch)
		{
			if( ( !_alreadyFetchedRuleTypePeriodUnit || forceFetch || _alwaysFetchRuleTypePeriodUnit) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RuleTypeEntityUsingPeriodUnit);
				RuleTypeEntity newEntity = new RuleTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PeriodUnit);
				}
				if(fetchResult)
				{
					newEntity = (RuleTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ruleTypePeriodUnitReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RuleTypePeriodUnit = newEntity;
				_alreadyFetchedRuleTypePeriodUnit = fetchResult;
			}
			return _ruleTypePeriodUnit;
		}


		/// <summary> Retrieves the related entity of type 'RuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RuleTypeEntity' which is related to this entity.</returns>
		public RuleTypeEntity GetSingleRuleTypesValidTimeUnit()
		{
			return GetSingleRuleTypesValidTimeUnit(false);
		}

		/// <summary> Retrieves the related entity of type 'RuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RuleTypeEntity' which is related to this entity.</returns>
		public virtual RuleTypeEntity GetSingleRuleTypesValidTimeUnit(bool forceFetch)
		{
			if( ( !_alreadyFetchedRuleTypesValidTimeUnit || forceFetch || _alwaysFetchRuleTypesValidTimeUnit) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RuleTypeEntityUsingValidTimeUnit);
				RuleTypeEntity newEntity = new RuleTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ValidTimeUnit);
				}
				if(fetchResult)
				{
					newEntity = (RuleTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ruleTypesValidTimeUnitReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RuleTypesValidTimeUnit = newEntity;
				_alreadyFetchedRuleTypesValidTimeUnit = fetchResult;
			}
			return _ruleTypesValidTimeUnit;
		}


		/// <summary> Retrieves the related entity of type 'RuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RuleTypeEntity' which is related to this entity.</returns>
		public RuleTypeEntity GetSingleRuleTypeValidationType()
		{
			return GetSingleRuleTypeValidationType(false);
		}

		/// <summary> Retrieves the related entity of type 'RuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RuleTypeEntity' which is related to this entity.</returns>
		public virtual RuleTypeEntity GetSingleRuleTypeValidationType(bool forceFetch)
		{
			if( ( !_alreadyFetchedRuleTypeValidationType || forceFetch || _alwaysFetchRuleTypeValidationType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RuleTypeEntityUsingValidationType);
				RuleTypeEntity newEntity = new RuleTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ValidationType);
				}
				if(fetchResult)
				{
					newEntity = (RuleTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ruleTypeValidationTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RuleTypeValidationType = newEntity;
				_alreadyFetchedRuleTypeValidationType = fetchResult;
			}
			return _ruleTypeValidationType;
		}


		/// <summary> Retrieves the related entity of type 'RuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RuleTypeEntity' which is related to this entity.</returns>
		public RuleTypeEntity GetSingleRuleTypeExtTimeUnit()
		{
			return GetSingleRuleTypeExtTimeUnit(false);
		}

		/// <summary> Retrieves the related entity of type 'RuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RuleTypeEntity' which is related to this entity.</returns>
		public virtual RuleTypeEntity GetSingleRuleTypeExtTimeUnit(bool forceFetch)
		{
			if( ( !_alreadyFetchedRuleTypeExtTimeUnit || forceFetch || _alwaysFetchRuleTypeExtTimeUnit) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RuleTypeEntityUsingExtTimeUnit);
				RuleTypeEntity newEntity = new RuleTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ExtTimeUnit);
				}
				if(fetchResult)
				{
					newEntity = (RuleTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ruleTypeExtTimeUnitReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RuleTypeExtTimeUnit = newEntity;
				_alreadyFetchedRuleTypeExtTimeUnit = fetchResult;
			}
			return _ruleTypeExtTimeUnit;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Calendar", _calendar);
			toReturn.Add("ExtensionCalendar", _extensionCalendar);
			toReturn.Add("RuleTypePeriodUnit", _ruleTypePeriodUnit);
			toReturn.Add("RuleTypesValidTimeUnit", _ruleTypesValidTimeUnit);
			toReturn.Add("RuleTypeValidationType", _ruleTypeValidationType);
			toReturn.Add("RuleTypeExtTimeUnit", _ruleTypeExtTimeUnit);
			toReturn.Add("RuleCapping", _ruleCapping);
			toReturn.Add("Tickets", _tickets);
			toReturn.Add("TicketDeviceClass", _ticketDeviceClass);
			toReturn.Add("VdvProduct", _vdvProduct);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="rulePeriodID">PK value for RulePeriod which data should be fetched into this RulePeriod object</param>
		/// <param name="validator">The validator object for this RulePeriodEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 rulePeriodID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(rulePeriodID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_ruleCapping = new VarioSL.Entities.CollectionClasses.RuleCappingCollection();
			_ruleCapping.SetContainingEntityInfo(this, "RulePeriod");

			_tickets = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_tickets.SetContainingEntityInfo(this, "RulePeriod");

			_ticketDeviceClass = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			_ticketDeviceClass.SetContainingEntityInfo(this, "RulePeriod");

			_vdvProduct = new VarioSL.Entities.CollectionClasses.VdvProductCollection();
			_vdvProduct.SetContainingEntityInfo(this, "RulePeriod");
			_calendarReturnsNewIfNotFound = false;
			_extensionCalendarReturnsNewIfNotFound = false;
			_ruleTypePeriodUnitReturnsNewIfNotFound = false;
			_ruleTypesValidTimeUnitReturnsNewIfNotFound = false;
			_ruleTypeValidationTypeReturnsNewIfNotFound = false;
			_ruleTypeExtTimeUnitReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CalendarID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExtCalendarID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalName1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExtTimeUnit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExtTimeValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodStart", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodUnit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Readonly", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RulePeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RulePeriodNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Selectable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SellableFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SellableUntil", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidityFromFaretable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTimeUnit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTimeValue", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _calendar</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCalendar(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _calendar, new PropertyChangedEventHandler( OnCalendarPropertyChanged ), "Calendar", VarioSL.Entities.RelationClasses.StaticRulePeriodRelations.CalendarEntityUsingCalendarIDStatic, true, signalRelatedEntity, "RulePeriod", resetFKFields, new int[] { (int)RulePeriodFieldIndex.CalendarID } );		
			_calendar = null;
		}
		
		/// <summary> setups the sync logic for member _calendar</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCalendar(IEntityCore relatedEntity)
		{
			if(_calendar!=relatedEntity)
			{		
				DesetupSyncCalendar(true, true);
				_calendar = (CalendarEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _calendar, new PropertyChangedEventHandler( OnCalendarPropertyChanged ), "Calendar", VarioSL.Entities.RelationClasses.StaticRulePeriodRelations.CalendarEntityUsingCalendarIDStatic, true, ref _alreadyFetchedCalendar, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCalendarPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _extensionCalendar</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExtensionCalendar(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _extensionCalendar, new PropertyChangedEventHandler( OnExtensionCalendarPropertyChanged ), "ExtensionCalendar", VarioSL.Entities.RelationClasses.StaticRulePeriodRelations.CalendarEntityUsingExtCalendarIDStatic, true, signalRelatedEntity, "RulePeriodByExtensionCalendar", resetFKFields, new int[] { (int)RulePeriodFieldIndex.ExtCalendarID } );		
			_extensionCalendar = null;
		}
		
		/// <summary> setups the sync logic for member _extensionCalendar</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExtensionCalendar(IEntityCore relatedEntity)
		{
			if(_extensionCalendar!=relatedEntity)
			{		
				DesetupSyncExtensionCalendar(true, true);
				_extensionCalendar = (CalendarEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _extensionCalendar, new PropertyChangedEventHandler( OnExtensionCalendarPropertyChanged ), "ExtensionCalendar", VarioSL.Entities.RelationClasses.StaticRulePeriodRelations.CalendarEntityUsingExtCalendarIDStatic, true, ref _alreadyFetchedExtensionCalendar, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExtensionCalendarPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ruleTypePeriodUnit</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRuleTypePeriodUnit(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ruleTypePeriodUnit, new PropertyChangedEventHandler( OnRuleTypePeriodUnitPropertyChanged ), "RuleTypePeriodUnit", VarioSL.Entities.RelationClasses.StaticRulePeriodRelations.RuleTypeEntityUsingPeriodUnitStatic, true, signalRelatedEntity, "RulePeriodPeriodUnit", resetFKFields, new int[] { (int)RulePeriodFieldIndex.PeriodUnit } );		
			_ruleTypePeriodUnit = null;
		}
		
		/// <summary> setups the sync logic for member _ruleTypePeriodUnit</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRuleTypePeriodUnit(IEntityCore relatedEntity)
		{
			if(_ruleTypePeriodUnit!=relatedEntity)
			{		
				DesetupSyncRuleTypePeriodUnit(true, true);
				_ruleTypePeriodUnit = (RuleTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ruleTypePeriodUnit, new PropertyChangedEventHandler( OnRuleTypePeriodUnitPropertyChanged ), "RuleTypePeriodUnit", VarioSL.Entities.RelationClasses.StaticRulePeriodRelations.RuleTypeEntityUsingPeriodUnitStatic, true, ref _alreadyFetchedRuleTypePeriodUnit, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRuleTypePeriodUnitPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ruleTypesValidTimeUnit</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRuleTypesValidTimeUnit(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ruleTypesValidTimeUnit, new PropertyChangedEventHandler( OnRuleTypesValidTimeUnitPropertyChanged ), "RuleTypesValidTimeUnit", VarioSL.Entities.RelationClasses.StaticRulePeriodRelations.RuleTypeEntityUsingValidTimeUnitStatic, true, signalRelatedEntity, "RulePeriodValidTimeUnit", resetFKFields, new int[] { (int)RulePeriodFieldIndex.ValidTimeUnit } );		
			_ruleTypesValidTimeUnit = null;
		}
		
		/// <summary> setups the sync logic for member _ruleTypesValidTimeUnit</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRuleTypesValidTimeUnit(IEntityCore relatedEntity)
		{
			if(_ruleTypesValidTimeUnit!=relatedEntity)
			{		
				DesetupSyncRuleTypesValidTimeUnit(true, true);
				_ruleTypesValidTimeUnit = (RuleTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ruleTypesValidTimeUnit, new PropertyChangedEventHandler( OnRuleTypesValidTimeUnitPropertyChanged ), "RuleTypesValidTimeUnit", VarioSL.Entities.RelationClasses.StaticRulePeriodRelations.RuleTypeEntityUsingValidTimeUnitStatic, true, ref _alreadyFetchedRuleTypesValidTimeUnit, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRuleTypesValidTimeUnitPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ruleTypeValidationType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRuleTypeValidationType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ruleTypeValidationType, new PropertyChangedEventHandler( OnRuleTypeValidationTypePropertyChanged ), "RuleTypeValidationType", VarioSL.Entities.RelationClasses.StaticRulePeriodRelations.RuleTypeEntityUsingValidationTypeStatic, true, signalRelatedEntity, "RulePeriodsPeriodType", resetFKFields, new int[] { (int)RulePeriodFieldIndex.ValidationType } );		
			_ruleTypeValidationType = null;
		}
		
		/// <summary> setups the sync logic for member _ruleTypeValidationType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRuleTypeValidationType(IEntityCore relatedEntity)
		{
			if(_ruleTypeValidationType!=relatedEntity)
			{		
				DesetupSyncRuleTypeValidationType(true, true);
				_ruleTypeValidationType = (RuleTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ruleTypeValidationType, new PropertyChangedEventHandler( OnRuleTypeValidationTypePropertyChanged ), "RuleTypeValidationType", VarioSL.Entities.RelationClasses.StaticRulePeriodRelations.RuleTypeEntityUsingValidationTypeStatic, true, ref _alreadyFetchedRuleTypeValidationType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRuleTypeValidationTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ruleTypeExtTimeUnit</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRuleTypeExtTimeUnit(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ruleTypeExtTimeUnit, new PropertyChangedEventHandler( OnRuleTypeExtTimeUnitPropertyChanged ), "RuleTypeExtTimeUnit", VarioSL.Entities.RelationClasses.StaticRulePeriodRelations.RuleTypeEntityUsingExtTimeUnitStatic, true, signalRelatedEntity, "RulePeriodExtTimeUnit", resetFKFields, new int[] { (int)RulePeriodFieldIndex.ExtTimeUnit } );		
			_ruleTypeExtTimeUnit = null;
		}
		
		/// <summary> setups the sync logic for member _ruleTypeExtTimeUnit</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRuleTypeExtTimeUnit(IEntityCore relatedEntity)
		{
			if(_ruleTypeExtTimeUnit!=relatedEntity)
			{		
				DesetupSyncRuleTypeExtTimeUnit(true, true);
				_ruleTypeExtTimeUnit = (RuleTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ruleTypeExtTimeUnit, new PropertyChangedEventHandler( OnRuleTypeExtTimeUnitPropertyChanged ), "RuleTypeExtTimeUnit", VarioSL.Entities.RelationClasses.StaticRulePeriodRelations.RuleTypeEntityUsingExtTimeUnitStatic, true, ref _alreadyFetchedRuleTypeExtTimeUnit, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRuleTypeExtTimeUnitPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="rulePeriodID">PK value for RulePeriod which data should be fetched into this RulePeriod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 rulePeriodID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RulePeriodFieldIndex.RulePeriodID].ForcedCurrentValueWrite(rulePeriodID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRulePeriodDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RulePeriodEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RulePeriodRelations Relations
		{
			get	{ return new RulePeriodRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleCapping' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleCapping
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleCappingCollection(), (IEntityRelation)GetRelationsForField("RuleCapping")[0], (int)VarioSL.Entities.EntityType.RulePeriodEntity, (int)VarioSL.Entities.EntityType.RuleCappingEntity, 0, null, null, null, "RuleCapping", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Tickets")[0], (int)VarioSL.Entities.EntityType.RulePeriodEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Tickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceClass
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceClass")[0], (int)VarioSL.Entities.EntityType.RulePeriodEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, null, "TicketDeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvProduct
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvProductCollection(), (IEntityRelation)GetRelationsForField("VdvProduct")[0], (int)VarioSL.Entities.EntityType.RulePeriodEntity, (int)VarioSL.Entities.EntityType.VdvProductEntity, 0, null, null, null, "VdvProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Calendar'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCalendar
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CalendarCollection(), (IEntityRelation)GetRelationsForField("Calendar")[0], (int)VarioSL.Entities.EntityType.RulePeriodEntity, (int)VarioSL.Entities.EntityType.CalendarEntity, 0, null, null, null, "Calendar", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Calendar'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExtensionCalendar
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CalendarCollection(), (IEntityRelation)GetRelationsForField("ExtensionCalendar")[0], (int)VarioSL.Entities.EntityType.RulePeriodEntity, (int)VarioSL.Entities.EntityType.CalendarEntity, 0, null, null, null, "ExtensionCalendar", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleTypePeriodUnit
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleTypeCollection(), (IEntityRelation)GetRelationsForField("RuleTypePeriodUnit")[0], (int)VarioSL.Entities.EntityType.RulePeriodEntity, (int)VarioSL.Entities.EntityType.RuleTypeEntity, 0, null, null, null, "RuleTypePeriodUnit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleTypesValidTimeUnit
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleTypeCollection(), (IEntityRelation)GetRelationsForField("RuleTypesValidTimeUnit")[0], (int)VarioSL.Entities.EntityType.RulePeriodEntity, (int)VarioSL.Entities.EntityType.RuleTypeEntity, 0, null, null, null, "RuleTypesValidTimeUnit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleTypeValidationType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleTypeCollection(), (IEntityRelation)GetRelationsForField("RuleTypeValidationType")[0], (int)VarioSL.Entities.EntityType.RulePeriodEntity, (int)VarioSL.Entities.EntityType.RuleTypeEntity, 0, null, null, null, "RuleTypeValidationType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleTypeExtTimeUnit
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleTypeCollection(), (IEntityRelation)GetRelationsForField("RuleTypeExtTimeUnit")[0], (int)VarioSL.Entities.EntityType.RulePeriodEntity, (int)VarioSL.Entities.EntityType.RuleTypeEntity, 0, null, null, null, "RuleTypeExtTimeUnit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CalendarID property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."CALENDARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CalendarID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RulePeriodFieldIndex.CalendarID, false); }
			set	{ SetValue((int)RulePeriodFieldIndex.CalendarID, value, true); }
		}

		/// <summary> The DefaultFrom property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."DEFAULTFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DefaultFrom
		{
			get { return (System.DateTime)GetValue((int)RulePeriodFieldIndex.DefaultFrom, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.DefaultFrom, value, true); }
		}

		/// <summary> The Description property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)RulePeriodFieldIndex.Description, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.Description, value, true); }
		}

		/// <summary> The ExtCalendarID property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."EXTCALENDARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ExtCalendarID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RulePeriodFieldIndex.ExtCalendarID, false); }
			set	{ SetValue((int)RulePeriodFieldIndex.ExtCalendarID, value, true); }
		}

		/// <summary> The ExternalName1 property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."EXTERNALNAME1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalName1
		{
			get { return (System.String)GetValue((int)RulePeriodFieldIndex.ExternalName1, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.ExternalName1, value, true); }
		}

		/// <summary> The ExtTimeUnit property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."EXTTIMEUNIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ExtTimeUnit
		{
			get { return (System.Int64)GetValue((int)RulePeriodFieldIndex.ExtTimeUnit, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.ExtTimeUnit, value, true); }
		}

		/// <summary> The ExtTimeValue property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."EXTTIMEVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ExtTimeValue
		{
			get { return (System.Int64)GetValue((int)RulePeriodFieldIndex.ExtTimeValue, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.ExtTimeValue, value, true); }
		}

		/// <summary> The Name property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 400<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)RulePeriodFieldIndex.Name, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.Name, value, true); }
		}

		/// <summary> The PeriodStart property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."PERIODSTART"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime PeriodStart
		{
			get { return (System.DateTime)GetValue((int)RulePeriodFieldIndex.PeriodStart, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.PeriodStart, value, true); }
		}

		/// <summary> The PeriodUnit property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."PERIODUNIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PeriodUnit
		{
			get { return (System.Int64)GetValue((int)RulePeriodFieldIndex.PeriodUnit, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.PeriodUnit, value, true); }
		}

		/// <summary> The PeriodValue property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."PERIODVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PeriodValue
		{
			get { return (System.Int64)GetValue((int)RulePeriodFieldIndex.PeriodValue, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.PeriodValue, value, true); }
		}

		/// <summary> The Readonly property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."READONLY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Readonly
		{
			get { return (System.Int16)GetValue((int)RulePeriodFieldIndex.Readonly, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.Readonly, value, true); }
		}

		/// <summary> The RulePeriodID property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."RULEPERIODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 RulePeriodID
		{
			get { return (System.Int64)GetValue((int)RulePeriodFieldIndex.RulePeriodID, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.RulePeriodID, value, true); }
		}

		/// <summary> The RulePeriodNumber property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."RULEPERIODNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 RulePeriodNumber
		{
			get { return (System.Int64)GetValue((int)RulePeriodFieldIndex.RulePeriodNumber, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.RulePeriodNumber, value, true); }
		}

		/// <summary> The Selectable property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."SELECTABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Selectable
		{
			get { return (System.Int16)GetValue((int)RulePeriodFieldIndex.Selectable, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.Selectable, value, true); }
		}

		/// <summary> The SellableFrom property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."SELLABLEFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime SellableFrom
		{
			get { return (System.DateTime)GetValue((int)RulePeriodFieldIndex.SellableFrom, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.SellableFrom, value, true); }
		}

		/// <summary> The SellableUntil property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."SELLABLEUNTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> SellableUntil
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RulePeriodFieldIndex.SellableUntil, false); }
			set	{ SetValue((int)RulePeriodFieldIndex.SellableUntil, value, true); }
		}

		/// <summary> The TariffID property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)RulePeriodFieldIndex.TariffID, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.TariffID, value, true); }
		}

		/// <summary> The ValidationType property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."VALIDATIONTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ValidationType
		{
			get { return (System.Int64)GetValue((int)RulePeriodFieldIndex.ValidationType, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.ValidationType, value, true); }
		}

		/// <summary> The ValidityFromFaretable property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."VALIDITYFROMFARETABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> ValidityFromFaretable
		{
			get { return (Nullable<System.Boolean>)GetValue((int)RulePeriodFieldIndex.ValidityFromFaretable, false); }
			set	{ SetValue((int)RulePeriodFieldIndex.ValidityFromFaretable, value, true); }
		}

		/// <summary> The ValidTimeUnit property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."VALIDTIMEUNIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ValidTimeUnit
		{
			get { return (System.Int64)GetValue((int)RulePeriodFieldIndex.ValidTimeUnit, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.ValidTimeUnit, value, true); }
		}

		/// <summary> The ValidTimeValue property of the Entity RulePeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_PERIOD"."VALIDTIMEVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ValidTimeValue
		{
			get { return (System.Int64)GetValue((int)RulePeriodFieldIndex.ValidTimeValue, true); }
			set	{ SetValue((int)RulePeriodFieldIndex.ValidTimeValue, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleCapping()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingCollection RuleCapping
		{
			get	{ return GetMultiRuleCapping(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleCapping. When set to true, RuleCapping is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleCapping is accessed. You can always execute/ a forced fetch by calling GetMultiRuleCapping(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleCapping
		{
			get	{ return _alwaysFetchRuleCapping; }
			set	{ _alwaysFetchRuleCapping = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleCapping already has been fetched. Setting this property to false when RuleCapping has been fetched
		/// will clear the RuleCapping collection well. Setting this property to true while RuleCapping hasn't been fetched disables lazy loading for RuleCapping</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleCapping
		{
			get { return _alreadyFetchedRuleCapping;}
			set 
			{
				if(_alreadyFetchedRuleCapping && !value && (_ruleCapping != null))
				{
					_ruleCapping.Clear();
				}
				_alreadyFetchedRuleCapping = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection Tickets
		{
			get	{ return GetMultiTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Tickets. When set to true, Tickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tickets is accessed. You can always execute/ a forced fetch by calling GetMultiTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTickets
		{
			get	{ return _alwaysFetchTickets; }
			set	{ _alwaysFetchTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tickets already has been fetched. Setting this property to false when Tickets has been fetched
		/// will clear the Tickets collection well. Setting this property to true while Tickets hasn't been fetched disables lazy loading for Tickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTickets
		{
			get { return _alreadyFetchedTickets;}
			set 
			{
				if(_alreadyFetchedTickets && !value && (_tickets != null))
				{
					_tickets.Clear();
				}
				_alreadyFetchedTickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceClass
		{
			get	{ return GetMultiTicketDeviceClass(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceClass. When set to true, TicketDeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceClass is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceClass
		{
			get	{ return _alwaysFetchTicketDeviceClass; }
			set	{ _alwaysFetchTicketDeviceClass = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceClass already has been fetched. Setting this property to false when TicketDeviceClass has been fetched
		/// will clear the TicketDeviceClass collection well. Setting this property to true while TicketDeviceClass hasn't been fetched disables lazy loading for TicketDeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceClass
		{
			get { return _alreadyFetchedTicketDeviceClass;}
			set 
			{
				if(_alreadyFetchedTicketDeviceClass && !value && (_ticketDeviceClass != null))
				{
					_ticketDeviceClass.Clear();
				}
				_alreadyFetchedTicketDeviceClass = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection VdvProduct
		{
			get	{ return GetMultiVdvProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvProduct. When set to true, VdvProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvProduct is accessed. You can always execute/ a forced fetch by calling GetMultiVdvProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvProduct
		{
			get	{ return _alwaysFetchVdvProduct; }
			set	{ _alwaysFetchVdvProduct = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvProduct already has been fetched. Setting this property to false when VdvProduct has been fetched
		/// will clear the VdvProduct collection well. Setting this property to true while VdvProduct hasn't been fetched disables lazy loading for VdvProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvProduct
		{
			get { return _alreadyFetchedVdvProduct;}
			set 
			{
				if(_alreadyFetchedVdvProduct && !value && (_vdvProduct != null))
				{
					_vdvProduct.Clear();
				}
				_alreadyFetchedVdvProduct = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CalendarEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCalendar()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CalendarEntity Calendar
		{
			get	{ return GetSingleCalendar(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCalendar(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RulePeriod", "Calendar", _calendar, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Calendar. When set to true, Calendar is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Calendar is accessed. You can always execute a forced fetch by calling GetSingleCalendar(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCalendar
		{
			get	{ return _alwaysFetchCalendar; }
			set	{ _alwaysFetchCalendar = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Calendar already has been fetched. Setting this property to false when Calendar has been fetched
		/// will set Calendar to null as well. Setting this property to true while Calendar hasn't been fetched disables lazy loading for Calendar</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCalendar
		{
			get { return _alreadyFetchedCalendar;}
			set 
			{
				if(_alreadyFetchedCalendar && !value)
				{
					this.Calendar = null;
				}
				_alreadyFetchedCalendar = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Calendar is not found
		/// in the database. When set to true, Calendar will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CalendarReturnsNewIfNotFound
		{
			get	{ return _calendarReturnsNewIfNotFound; }
			set { _calendarReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CalendarEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExtensionCalendar()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CalendarEntity ExtensionCalendar
		{
			get	{ return GetSingleExtensionCalendar(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExtensionCalendar(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RulePeriodByExtensionCalendar", "ExtensionCalendar", _extensionCalendar, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExtensionCalendar. When set to true, ExtensionCalendar is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExtensionCalendar is accessed. You can always execute a forced fetch by calling GetSingleExtensionCalendar(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExtensionCalendar
		{
			get	{ return _alwaysFetchExtensionCalendar; }
			set	{ _alwaysFetchExtensionCalendar = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExtensionCalendar already has been fetched. Setting this property to false when ExtensionCalendar has been fetched
		/// will set ExtensionCalendar to null as well. Setting this property to true while ExtensionCalendar hasn't been fetched disables lazy loading for ExtensionCalendar</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExtensionCalendar
		{
			get { return _alreadyFetchedExtensionCalendar;}
			set 
			{
				if(_alreadyFetchedExtensionCalendar && !value)
				{
					this.ExtensionCalendar = null;
				}
				_alreadyFetchedExtensionCalendar = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExtensionCalendar is not found
		/// in the database. When set to true, ExtensionCalendar will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ExtensionCalendarReturnsNewIfNotFound
		{
			get	{ return _extensionCalendarReturnsNewIfNotFound; }
			set { _extensionCalendarReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RuleTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRuleTypePeriodUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RuleTypeEntity RuleTypePeriodUnit
		{
			get	{ return GetSingleRuleTypePeriodUnit(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRuleTypePeriodUnit(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RulePeriodPeriodUnit", "RuleTypePeriodUnit", _ruleTypePeriodUnit, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RuleTypePeriodUnit. When set to true, RuleTypePeriodUnit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleTypePeriodUnit is accessed. You can always execute a forced fetch by calling GetSingleRuleTypePeriodUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleTypePeriodUnit
		{
			get	{ return _alwaysFetchRuleTypePeriodUnit; }
			set	{ _alwaysFetchRuleTypePeriodUnit = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleTypePeriodUnit already has been fetched. Setting this property to false when RuleTypePeriodUnit has been fetched
		/// will set RuleTypePeriodUnit to null as well. Setting this property to true while RuleTypePeriodUnit hasn't been fetched disables lazy loading for RuleTypePeriodUnit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleTypePeriodUnit
		{
			get { return _alreadyFetchedRuleTypePeriodUnit;}
			set 
			{
				if(_alreadyFetchedRuleTypePeriodUnit && !value)
				{
					this.RuleTypePeriodUnit = null;
				}
				_alreadyFetchedRuleTypePeriodUnit = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RuleTypePeriodUnit is not found
		/// in the database. When set to true, RuleTypePeriodUnit will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RuleTypePeriodUnitReturnsNewIfNotFound
		{
			get	{ return _ruleTypePeriodUnitReturnsNewIfNotFound; }
			set { _ruleTypePeriodUnitReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RuleTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRuleTypesValidTimeUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RuleTypeEntity RuleTypesValidTimeUnit
		{
			get	{ return GetSingleRuleTypesValidTimeUnit(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRuleTypesValidTimeUnit(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RulePeriodValidTimeUnit", "RuleTypesValidTimeUnit", _ruleTypesValidTimeUnit, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RuleTypesValidTimeUnit. When set to true, RuleTypesValidTimeUnit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleTypesValidTimeUnit is accessed. You can always execute a forced fetch by calling GetSingleRuleTypesValidTimeUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleTypesValidTimeUnit
		{
			get	{ return _alwaysFetchRuleTypesValidTimeUnit; }
			set	{ _alwaysFetchRuleTypesValidTimeUnit = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleTypesValidTimeUnit already has been fetched. Setting this property to false when RuleTypesValidTimeUnit has been fetched
		/// will set RuleTypesValidTimeUnit to null as well. Setting this property to true while RuleTypesValidTimeUnit hasn't been fetched disables lazy loading for RuleTypesValidTimeUnit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleTypesValidTimeUnit
		{
			get { return _alreadyFetchedRuleTypesValidTimeUnit;}
			set 
			{
				if(_alreadyFetchedRuleTypesValidTimeUnit && !value)
				{
					this.RuleTypesValidTimeUnit = null;
				}
				_alreadyFetchedRuleTypesValidTimeUnit = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RuleTypesValidTimeUnit is not found
		/// in the database. When set to true, RuleTypesValidTimeUnit will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RuleTypesValidTimeUnitReturnsNewIfNotFound
		{
			get	{ return _ruleTypesValidTimeUnitReturnsNewIfNotFound; }
			set { _ruleTypesValidTimeUnitReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RuleTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRuleTypeValidationType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RuleTypeEntity RuleTypeValidationType
		{
			get	{ return GetSingleRuleTypeValidationType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRuleTypeValidationType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RulePeriodsPeriodType", "RuleTypeValidationType", _ruleTypeValidationType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RuleTypeValidationType. When set to true, RuleTypeValidationType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleTypeValidationType is accessed. You can always execute a forced fetch by calling GetSingleRuleTypeValidationType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleTypeValidationType
		{
			get	{ return _alwaysFetchRuleTypeValidationType; }
			set	{ _alwaysFetchRuleTypeValidationType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleTypeValidationType already has been fetched. Setting this property to false when RuleTypeValidationType has been fetched
		/// will set RuleTypeValidationType to null as well. Setting this property to true while RuleTypeValidationType hasn't been fetched disables lazy loading for RuleTypeValidationType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleTypeValidationType
		{
			get { return _alreadyFetchedRuleTypeValidationType;}
			set 
			{
				if(_alreadyFetchedRuleTypeValidationType && !value)
				{
					this.RuleTypeValidationType = null;
				}
				_alreadyFetchedRuleTypeValidationType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RuleTypeValidationType is not found
		/// in the database. When set to true, RuleTypeValidationType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RuleTypeValidationTypeReturnsNewIfNotFound
		{
			get	{ return _ruleTypeValidationTypeReturnsNewIfNotFound; }
			set { _ruleTypeValidationTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RuleTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRuleTypeExtTimeUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RuleTypeEntity RuleTypeExtTimeUnit
		{
			get	{ return GetSingleRuleTypeExtTimeUnit(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRuleTypeExtTimeUnit(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RulePeriodExtTimeUnit", "RuleTypeExtTimeUnit", _ruleTypeExtTimeUnit, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RuleTypeExtTimeUnit. When set to true, RuleTypeExtTimeUnit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleTypeExtTimeUnit is accessed. You can always execute a forced fetch by calling GetSingleRuleTypeExtTimeUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleTypeExtTimeUnit
		{
			get	{ return _alwaysFetchRuleTypeExtTimeUnit; }
			set	{ _alwaysFetchRuleTypeExtTimeUnit = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleTypeExtTimeUnit already has been fetched. Setting this property to false when RuleTypeExtTimeUnit has been fetched
		/// will set RuleTypeExtTimeUnit to null as well. Setting this property to true while RuleTypeExtTimeUnit hasn't been fetched disables lazy loading for RuleTypeExtTimeUnit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleTypeExtTimeUnit
		{
			get { return _alreadyFetchedRuleTypeExtTimeUnit;}
			set 
			{
				if(_alreadyFetchedRuleTypeExtTimeUnit && !value)
				{
					this.RuleTypeExtTimeUnit = null;
				}
				_alreadyFetchedRuleTypeExtTimeUnit = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RuleTypeExtTimeUnit is not found
		/// in the database. When set to true, RuleTypeExtTimeUnit will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RuleTypeExtTimeUnitReturnsNewIfNotFound
		{
			get	{ return _ruleTypeExtTimeUnitReturnsNewIfNotFound; }
			set { _ruleTypeExtTimeUnitReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.RulePeriodEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
