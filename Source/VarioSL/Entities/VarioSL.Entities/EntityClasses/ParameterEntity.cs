﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Parameter'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ParameterEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ParameterGroupToParameterCollection	_parameterGroupToParameters;
		private bool	_alwaysFetchParameterGroupToParameters, _alreadyFetchedParameterGroupToParameters;
		private VarioSL.Entities.CollectionClasses.ParameterValueCollection	_parameterValues;
		private bool	_alwaysFetchParameterValues, _alreadyFetchedParameterValues;
		private VarioSL.Entities.CollectionClasses.ParameterGroupCollection _parameterGroups;
		private bool	_alwaysFetchParameterGroups, _alreadyFetchedParameterGroups;
		private DeviceClassEntity _deviceClass;
		private bool	_alwaysFetchDeviceClass, _alreadyFetchedDeviceClass, _deviceClassReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeviceClass</summary>
			public static readonly string DeviceClass = "DeviceClass";
			/// <summary>Member name ParameterGroupToParameters</summary>
			public static readonly string ParameterGroupToParameters = "ParameterGroupToParameters";
			/// <summary>Member name ParameterValues</summary>
			public static readonly string ParameterValues = "ParameterValues";
			/// <summary>Member name ParameterGroups</summary>
			public static readonly string ParameterGroups = "ParameterGroups";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ParameterEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ParameterEntity() :base("ParameterEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="parameterID">PK value for Parameter which data should be fetched into this Parameter object</param>
		public ParameterEntity(System.Int64 parameterID):base("ParameterEntity")
		{
			InitClassFetch(parameterID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="parameterID">PK value for Parameter which data should be fetched into this Parameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ParameterEntity(System.Int64 parameterID, IPrefetchPath prefetchPathToUse):base("ParameterEntity")
		{
			InitClassFetch(parameterID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="parameterID">PK value for Parameter which data should be fetched into this Parameter object</param>
		/// <param name="validator">The custom validator object for this ParameterEntity</param>
		public ParameterEntity(System.Int64 parameterID, IValidator validator):base("ParameterEntity")
		{
			InitClassFetch(parameterID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ParameterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_parameterGroupToParameters = (VarioSL.Entities.CollectionClasses.ParameterGroupToParameterCollection)info.GetValue("_parameterGroupToParameters", typeof(VarioSL.Entities.CollectionClasses.ParameterGroupToParameterCollection));
			_alwaysFetchParameterGroupToParameters = info.GetBoolean("_alwaysFetchParameterGroupToParameters");
			_alreadyFetchedParameterGroupToParameters = info.GetBoolean("_alreadyFetchedParameterGroupToParameters");

			_parameterValues = (VarioSL.Entities.CollectionClasses.ParameterValueCollection)info.GetValue("_parameterValues", typeof(VarioSL.Entities.CollectionClasses.ParameterValueCollection));
			_alwaysFetchParameterValues = info.GetBoolean("_alwaysFetchParameterValues");
			_alreadyFetchedParameterValues = info.GetBoolean("_alreadyFetchedParameterValues");
			_parameterGroups = (VarioSL.Entities.CollectionClasses.ParameterGroupCollection)info.GetValue("_parameterGroups", typeof(VarioSL.Entities.CollectionClasses.ParameterGroupCollection));
			_alwaysFetchParameterGroups = info.GetBoolean("_alwaysFetchParameterGroups");
			_alreadyFetchedParameterGroups = info.GetBoolean("_alreadyFetchedParameterGroups");
			_deviceClass = (DeviceClassEntity)info.GetValue("_deviceClass", typeof(DeviceClassEntity));
			if(_deviceClass!=null)
			{
				_deviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceClassReturnsNewIfNotFound = info.GetBoolean("_deviceClassReturnsNewIfNotFound");
			_alwaysFetchDeviceClass = info.GetBoolean("_alwaysFetchDeviceClass");
			_alreadyFetchedDeviceClass = info.GetBoolean("_alreadyFetchedDeviceClass");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ParameterFieldIndex)fieldIndex)
			{
				case ParameterFieldIndex.DeviceClassID:
					DesetupSyncDeviceClass(true, false);
					_alreadyFetchedDeviceClass = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedParameterGroupToParameters = (_parameterGroupToParameters.Count > 0);
			_alreadyFetchedParameterValues = (_parameterValues.Count > 0);
			_alreadyFetchedParameterGroups = (_parameterGroups.Count > 0);
			_alreadyFetchedDeviceClass = (_deviceClass != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeviceClass":
					toReturn.Add(Relations.DeviceClassEntityUsingDeviceClassID);
					break;
				case "ParameterGroupToParameters":
					toReturn.Add(Relations.ParameterGroupToParameterEntityUsingParameterID);
					break;
				case "ParameterValues":
					toReturn.Add(Relations.ParameterValueEntityUsingParameterID);
					break;
				case "ParameterGroups":
					toReturn.Add(Relations.ParameterGroupToParameterEntityUsingParameterID, "ParameterEntity__", "ParameterGroupToParameter_", JoinHint.None);
					toReturn.Add(ParameterGroupToParameterEntity.Relations.ParameterGroupEntityUsingParameterGroupID, "ParameterGroupToParameter_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_parameterGroupToParameters", (!this.MarkedForDeletion?_parameterGroupToParameters:null));
			info.AddValue("_alwaysFetchParameterGroupToParameters", _alwaysFetchParameterGroupToParameters);
			info.AddValue("_alreadyFetchedParameterGroupToParameters", _alreadyFetchedParameterGroupToParameters);
			info.AddValue("_parameterValues", (!this.MarkedForDeletion?_parameterValues:null));
			info.AddValue("_alwaysFetchParameterValues", _alwaysFetchParameterValues);
			info.AddValue("_alreadyFetchedParameterValues", _alreadyFetchedParameterValues);
			info.AddValue("_parameterGroups", (!this.MarkedForDeletion?_parameterGroups:null));
			info.AddValue("_alwaysFetchParameterGroups", _alwaysFetchParameterGroups);
			info.AddValue("_alreadyFetchedParameterGroups", _alreadyFetchedParameterGroups);
			info.AddValue("_deviceClass", (!this.MarkedForDeletion?_deviceClass:null));
			info.AddValue("_deviceClassReturnsNewIfNotFound", _deviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceClass", _alwaysFetchDeviceClass);
			info.AddValue("_alreadyFetchedDeviceClass", _alreadyFetchedDeviceClass);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeviceClass":
					_alreadyFetchedDeviceClass = true;
					this.DeviceClass = (DeviceClassEntity)entity;
					break;
				case "ParameterGroupToParameters":
					_alreadyFetchedParameterGroupToParameters = true;
					if(entity!=null)
					{
						this.ParameterGroupToParameters.Add((ParameterGroupToParameterEntity)entity);
					}
					break;
				case "ParameterValues":
					_alreadyFetchedParameterValues = true;
					if(entity!=null)
					{
						this.ParameterValues.Add((ParameterValueEntity)entity);
					}
					break;
				case "ParameterGroups":
					_alreadyFetchedParameterGroups = true;
					if(entity!=null)
					{
						this.ParameterGroups.Add((ParameterGroupEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeviceClass":
					SetupSyncDeviceClass(relatedEntity);
					break;
				case "ParameterGroupToParameters":
					_parameterGroupToParameters.Add((ParameterGroupToParameterEntity)relatedEntity);
					break;
				case "ParameterValues":
					_parameterValues.Add((ParameterValueEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeviceClass":
					DesetupSyncDeviceClass(false, true);
					break;
				case "ParameterGroupToParameters":
					this.PerformRelatedEntityRemoval(_parameterGroupToParameters, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterValues":
					this.PerformRelatedEntityRemoval(_parameterValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_deviceClass!=null)
			{
				toReturn.Add(_deviceClass);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_parameterGroupToParameters);
			toReturn.Add(_parameterValues);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterID">PK value for Parameter which data should be fetched into this Parameter object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterID)
		{
			return FetchUsingPK(parameterID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterID">PK value for Parameter which data should be fetched into this Parameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(parameterID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterID">PK value for Parameter which data should be fetched into this Parameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(parameterID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterID">PK value for Parameter which data should be fetched into this Parameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(parameterID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ParameterID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ParameterRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ParameterGroupToParameterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterGroupToParameterEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterGroupToParameterCollection GetMultiParameterGroupToParameters(bool forceFetch)
		{
			return GetMultiParameterGroupToParameters(forceFetch, _parameterGroupToParameters.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterGroupToParameterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterGroupToParameterEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterGroupToParameterCollection GetMultiParameterGroupToParameters(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterGroupToParameters(forceFetch, _parameterGroupToParameters.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterGroupToParameterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterGroupToParameterCollection GetMultiParameterGroupToParameters(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterGroupToParameters(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterGroupToParameterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterGroupToParameterCollection GetMultiParameterGroupToParameters(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterGroupToParameters || forceFetch || _alwaysFetchParameterGroupToParameters) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterGroupToParameters);
				_parameterGroupToParameters.SuppressClearInGetMulti=!forceFetch;
				_parameterGroupToParameters.EntityFactoryToUse = entityFactoryToUse;
				_parameterGroupToParameters.GetMultiManyToOne(this, null, filter);
				_parameterGroupToParameters.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterGroupToParameters = true;
			}
			return _parameterGroupToParameters;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterGroupToParameters'. These settings will be taken into account
		/// when the property ParameterGroupToParameters is requested or GetMultiParameterGroupToParameters is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterGroupToParameters(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterGroupToParameters.SortClauses=sortClauses;
			_parameterGroupToParameters.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch)
		{
			return GetMultiParameterValues(forceFetch, _parameterValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterValues(forceFetch, _parameterValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterValues || forceFetch || _alwaysFetchParameterValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterValues);
				_parameterValues.SuppressClearInGetMulti=!forceFetch;
				_parameterValues.EntityFactoryToUse = entityFactoryToUse;
				_parameterValues.GetMultiManyToOne(null, null, this, null, null, filter);
				_parameterValues.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterValues = true;
			}
			return _parameterValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterValues'. These settings will be taken into account
		/// when the property ParameterValues is requested or GetMultiParameterValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterValues.SortClauses=sortClauses;
			_parameterValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterGroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterGroupCollection GetMultiParameterGroups(bool forceFetch)
		{
			return GetMultiParameterGroups(forceFetch, _parameterGroups.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ParameterGroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterGroupCollection GetMultiParameterGroups(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedParameterGroups || forceFetch || _alwaysFetchParameterGroups) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterGroups);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ParameterFields.ParameterID, ComparisonOperator.Equal, this.ParameterID, "ParameterEntity__"));
				_parameterGroups.SuppressClearInGetMulti=!forceFetch;
				_parameterGroups.EntityFactoryToUse = entityFactoryToUse;
				_parameterGroups.GetMulti(filter, GetRelationsForField("ParameterGroups"));
				_parameterGroups.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterGroups = true;
			}
			return _parameterGroups;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterGroups'. These settings will be taken into account
		/// when the property ParameterGroups is requested or GetMultiParameterGroups is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterGroups(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterGroups.SortClauses=sortClauses;
			_parameterGroups.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public DeviceClassEntity GetSingleDeviceClass()
		{
			return GetSingleDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public virtual DeviceClassEntity GetSingleDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceClass || forceFetch || _alwaysFetchDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceClassEntityUsingDeviceClassID);
				DeviceClassEntity newEntity = new DeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceClassID);
				}
				if(fetchResult)
				{
					newEntity = (DeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceClass = newEntity;
				_alreadyFetchedDeviceClass = fetchResult;
			}
			return _deviceClass;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeviceClass", _deviceClass);
			toReturn.Add("ParameterGroupToParameters", _parameterGroupToParameters);
			toReturn.Add("ParameterValues", _parameterValues);
			toReturn.Add("ParameterGroups", _parameterGroups);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="parameterID">PK value for Parameter which data should be fetched into this Parameter object</param>
		/// <param name="validator">The validator object for this ParameterEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 parameterID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(parameterID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_parameterGroupToParameters = new VarioSL.Entities.CollectionClasses.ParameterGroupToParameterCollection();
			_parameterGroupToParameters.SetContainingEntityInfo(this, "Parameter");

			_parameterValues = new VarioSL.Entities.CollectionClasses.ParameterValueCollection();
			_parameterValues.SetContainingEntityInfo(this, "Parameter");
			_parameterGroups = new VarioSL.Entities.CollectionClasses.ParameterGroupCollection();
			_deviceClassReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Unit", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _deviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticParameterRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, signalRelatedEntity, "Parameters", resetFKFields, new int[] { (int)ParameterFieldIndex.DeviceClassID } );		
			_deviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _deviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceClass(IEntityCore relatedEntity)
		{
			if(_deviceClass!=relatedEntity)
			{		
				DesetupSyncDeviceClass(true, true);
				_deviceClass = (DeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticParameterRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, ref _alreadyFetchedDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="parameterID">PK value for Parameter which data should be fetched into this Parameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 parameterID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ParameterFieldIndex.ParameterID].ForcedCurrentValueWrite(parameterID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateParameterDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ParameterEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ParameterRelations Relations
		{
			get	{ return new ParameterRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterGroupToParameter' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterGroupToParameters
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterGroupToParameterCollection(), (IEntityRelation)GetRelationsForField("ParameterGroupToParameters")[0], (int)VarioSL.Entities.EntityType.ParameterEntity, (int)VarioSL.Entities.EntityType.ParameterGroupToParameterEntity, 0, null, null, null, "ParameterGroupToParameters", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterValueCollection(), (IEntityRelation)GetRelationsForField("ParameterValues")[0], (int)VarioSL.Entities.EntityType.ParameterEntity, (int)VarioSL.Entities.EntityType.ParameterValueEntity, 0, null, null, null, "ParameterValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterGroups
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ParameterGroupToParameterEntityUsingParameterID;
				intermediateRelation.SetAliases(string.Empty, "ParameterGroupToParameter_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterGroupCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.ParameterEntity, (int)VarioSL.Entities.EntityType.ParameterGroupEntity, 0, null, null, GetRelationsForField("ParameterGroups"), "ParameterGroups", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceClassCollection(), (IEntityRelation)GetRelationsForField("DeviceClass")[0], (int)VarioSL.Entities.EntityType.ParameterEntity, (int)VarioSL.Entities.EntityType.DeviceClassEntity, 0, null, null, null, "DeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DefaultValue property of the Entity Parameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETER"."DEFAULTVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DefaultValue
		{
			get { return (System.String)GetValue((int)ParameterFieldIndex.DefaultValue, true); }
			set	{ SetValue((int)ParameterFieldIndex.DefaultValue, value, true); }
		}

		/// <summary> The Description property of the Entity Parameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETER"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ParameterFieldIndex.Description, true); }
			set	{ SetValue((int)ParameterFieldIndex.Description, value, true); }
		}

		/// <summary> The DeviceClassID property of the Entity Parameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETER"."DEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DeviceClassID
		{
			get { return (System.Int64)GetValue((int)ParameterFieldIndex.DeviceClassID, true); }
			set	{ SetValue((int)ParameterFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The LastModified property of the Entity Parameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETER"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ParameterFieldIndex.LastModified, true); }
			set	{ SetValue((int)ParameterFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Parameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETER"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ParameterFieldIndex.LastUser, true); }
			set	{ SetValue((int)ParameterFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MaxValue property of the Entity Parameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETER"."MAXVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxValue
		{
			get { return (System.Int32)GetValue((int)ParameterFieldIndex.MaxValue, true); }
			set	{ SetValue((int)ParameterFieldIndex.MaxValue, value, true); }
		}

		/// <summary> The MinValue property of the Entity Parameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETER"."MINVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MinValue
		{
			get { return (System.Int32)GetValue((int)ParameterFieldIndex.MinValue, true); }
			set	{ SetValue((int)ParameterFieldIndex.MinValue, value, true); }
		}

		/// <summary> The ParameterID property of the Entity Parameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETER"."PARAMETERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ParameterID
		{
			get { return (System.Int64)GetValue((int)ParameterFieldIndex.ParameterID, true); }
			set	{ SetValue((int)ParameterFieldIndex.ParameterID, value, true); }
		}

		/// <summary> The ParameterNumber property of the Entity Parameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETER"."PARAMETERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParameterNumber
		{
			get { return (System.Int32)GetValue((int)ParameterFieldIndex.ParameterNumber, true); }
			set	{ SetValue((int)ParameterFieldIndex.ParameterNumber, value, true); }
		}

		/// <summary> The ParameterType property of the Entity Parameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETER"."PARAMETERTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.ParameterType ParameterType
		{
			get { return (VarioSL.Entities.Enumerations.ParameterType)GetValue((int)ParameterFieldIndex.ParameterType, true); }
			set	{ SetValue((int)ParameterFieldIndex.ParameterType, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Parameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETER"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ParameterFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ParameterFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The Unit property of the Entity Parameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETER"."UNIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Unit
		{
			get { return (System.String)GetValue((int)ParameterFieldIndex.Unit, true); }
			set	{ SetValue((int)ParameterFieldIndex.Unit, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ParameterGroupToParameterEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterGroupToParameters()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterGroupToParameterCollection ParameterGroupToParameters
		{
			get	{ return GetMultiParameterGroupToParameters(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterGroupToParameters. When set to true, ParameterGroupToParameters is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterGroupToParameters is accessed. You can always execute/ a forced fetch by calling GetMultiParameterGroupToParameters(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterGroupToParameters
		{
			get	{ return _alwaysFetchParameterGroupToParameters; }
			set	{ _alwaysFetchParameterGroupToParameters = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterGroupToParameters already has been fetched. Setting this property to false when ParameterGroupToParameters has been fetched
		/// will clear the ParameterGroupToParameters collection well. Setting this property to true while ParameterGroupToParameters hasn't been fetched disables lazy loading for ParameterGroupToParameters</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterGroupToParameters
		{
			get { return _alreadyFetchedParameterGroupToParameters;}
			set 
			{
				if(_alreadyFetchedParameterGroupToParameters && !value && (_parameterGroupToParameters != null))
				{
					_parameterGroupToParameters.Clear();
				}
				_alreadyFetchedParameterGroupToParameters = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterValueCollection ParameterValues
		{
			get	{ return GetMultiParameterValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterValues. When set to true, ParameterValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterValues is accessed. You can always execute/ a forced fetch by calling GetMultiParameterValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterValues
		{
			get	{ return _alwaysFetchParameterValues; }
			set	{ _alwaysFetchParameterValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterValues already has been fetched. Setting this property to false when ParameterValues has been fetched
		/// will clear the ParameterValues collection well. Setting this property to true while ParameterValues hasn't been fetched disables lazy loading for ParameterValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterValues
		{
			get { return _alreadyFetchedParameterValues;}
			set 
			{
				if(_alreadyFetchedParameterValues && !value && (_parameterValues != null))
				{
					_parameterValues.Clear();
				}
				_alreadyFetchedParameterValues = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ParameterGroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterGroups()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterGroupCollection ParameterGroups
		{
			get { return GetMultiParameterGroups(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterGroups. When set to true, ParameterGroups is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterGroups is accessed. You can always execute a forced fetch by calling GetMultiParameterGroups(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterGroups
		{
			get	{ return _alwaysFetchParameterGroups; }
			set	{ _alwaysFetchParameterGroups = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterGroups already has been fetched. Setting this property to false when ParameterGroups has been fetched
		/// will clear the ParameterGroups collection well. Setting this property to true while ParameterGroups hasn't been fetched disables lazy loading for ParameterGroups</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterGroups
		{
			get { return _alreadyFetchedParameterGroups;}
			set 
			{
				if(_alreadyFetchedParameterGroups && !value && (_parameterGroups != null))
				{
					_parameterGroups.Clear();
				}
				_alreadyFetchedParameterGroups = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'DeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceClassEntity DeviceClass
		{
			get	{ return GetSingleDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Parameters", "DeviceClass", _deviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceClass. When set to true, DeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceClass is accessed. You can always execute a forced fetch by calling GetSingleDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceClass
		{
			get	{ return _alwaysFetchDeviceClass; }
			set	{ _alwaysFetchDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceClass already has been fetched. Setting this property to false when DeviceClass has been fetched
		/// will set DeviceClass to null as well. Setting this property to true while DeviceClass hasn't been fetched disables lazy loading for DeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceClass
		{
			get { return _alreadyFetchedDeviceClass;}
			set 
			{
				if(_alreadyFetchedDeviceClass && !value)
				{
					this.DeviceClass = null;
				}
				_alreadyFetchedDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceClass is not found
		/// in the database. When set to true, DeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceClassReturnsNewIfNotFound
		{
			get	{ return _deviceClassReturnsNewIfNotFound; }
			set { _deviceClassReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ParameterEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
