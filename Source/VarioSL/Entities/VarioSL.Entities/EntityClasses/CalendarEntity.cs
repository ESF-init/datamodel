﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Calendar'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CalendarEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CalendarEntryCollection	_calendarEntries;
		private bool	_alwaysFetchCalendarEntries, _alreadyFetchedCalendarEntries;
		private VarioSL.Entities.CollectionClasses.RuleCappingCollection	_ruleCapping;
		private bool	_alwaysFetchRuleCapping, _alreadyFetchedRuleCapping;
		private VarioSL.Entities.CollectionClasses.RulePeriodCollection	_rulePeriod;
		private bool	_alwaysFetchRulePeriod, _alreadyFetchedRulePeriod;
		private VarioSL.Entities.CollectionClasses.RulePeriodCollection	_rulePeriodByExtensionCalendar;
		private bool	_alwaysFetchRulePeriodByExtensionCalendar, _alreadyFetchedRulePeriodByExtensionCalendar;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_tickets;
		private bool	_alwaysFetchTickets, _alreadyFetchedTickets;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection	_ticketDeviceClass;
		private bool	_alwaysFetchTicketDeviceClass, _alreadyFetchedTicketDeviceClass;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name CalendarEntries</summary>
			public static readonly string CalendarEntries = "CalendarEntries";
			/// <summary>Member name RuleCapping</summary>
			public static readonly string RuleCapping = "RuleCapping";
			/// <summary>Member name RulePeriod</summary>
			public static readonly string RulePeriod = "RulePeriod";
			/// <summary>Member name RulePeriodByExtensionCalendar</summary>
			public static readonly string RulePeriodByExtensionCalendar = "RulePeriodByExtensionCalendar";
			/// <summary>Member name Tickets</summary>
			public static readonly string Tickets = "Tickets";
			/// <summary>Member name TicketDeviceClass</summary>
			public static readonly string TicketDeviceClass = "TicketDeviceClass";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CalendarEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CalendarEntity() :base("CalendarEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="calendarID">PK value for Calendar which data should be fetched into this Calendar object</param>
		public CalendarEntity(System.Int64 calendarID):base("CalendarEntity")
		{
			InitClassFetch(calendarID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="calendarID">PK value for Calendar which data should be fetched into this Calendar object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CalendarEntity(System.Int64 calendarID, IPrefetchPath prefetchPathToUse):base("CalendarEntity")
		{
			InitClassFetch(calendarID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="calendarID">PK value for Calendar which data should be fetched into this Calendar object</param>
		/// <param name="validator">The custom validator object for this CalendarEntity</param>
		public CalendarEntity(System.Int64 calendarID, IValidator validator):base("CalendarEntity")
		{
			InitClassFetch(calendarID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CalendarEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_calendarEntries = (VarioSL.Entities.CollectionClasses.CalendarEntryCollection)info.GetValue("_calendarEntries", typeof(VarioSL.Entities.CollectionClasses.CalendarEntryCollection));
			_alwaysFetchCalendarEntries = info.GetBoolean("_alwaysFetchCalendarEntries");
			_alreadyFetchedCalendarEntries = info.GetBoolean("_alreadyFetchedCalendarEntries");

			_ruleCapping = (VarioSL.Entities.CollectionClasses.RuleCappingCollection)info.GetValue("_ruleCapping", typeof(VarioSL.Entities.CollectionClasses.RuleCappingCollection));
			_alwaysFetchRuleCapping = info.GetBoolean("_alwaysFetchRuleCapping");
			_alreadyFetchedRuleCapping = info.GetBoolean("_alreadyFetchedRuleCapping");

			_rulePeriod = (VarioSL.Entities.CollectionClasses.RulePeriodCollection)info.GetValue("_rulePeriod", typeof(VarioSL.Entities.CollectionClasses.RulePeriodCollection));
			_alwaysFetchRulePeriod = info.GetBoolean("_alwaysFetchRulePeriod");
			_alreadyFetchedRulePeriod = info.GetBoolean("_alreadyFetchedRulePeriod");

			_rulePeriodByExtensionCalendar = (VarioSL.Entities.CollectionClasses.RulePeriodCollection)info.GetValue("_rulePeriodByExtensionCalendar", typeof(VarioSL.Entities.CollectionClasses.RulePeriodCollection));
			_alwaysFetchRulePeriodByExtensionCalendar = info.GetBoolean("_alwaysFetchRulePeriodByExtensionCalendar");
			_alreadyFetchedRulePeriodByExtensionCalendar = info.GetBoolean("_alreadyFetchedRulePeriodByExtensionCalendar");

			_tickets = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_tickets", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTickets = info.GetBoolean("_alwaysFetchTickets");
			_alreadyFetchedTickets = info.GetBoolean("_alreadyFetchedTickets");

			_ticketDeviceClass = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceClass", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceClass = info.GetBoolean("_alwaysFetchTicketDeviceClass");
			_alreadyFetchedTicketDeviceClass = info.GetBoolean("_alreadyFetchedTicketDeviceClass");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CalendarFieldIndex)fieldIndex)
			{
				case CalendarFieldIndex.OwnerClientId:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case CalendarFieldIndex.TariffId:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCalendarEntries = (_calendarEntries.Count > 0);
			_alreadyFetchedRuleCapping = (_ruleCapping.Count > 0);
			_alreadyFetchedRulePeriod = (_rulePeriod.Count > 0);
			_alreadyFetchedRulePeriodByExtensionCalendar = (_rulePeriodByExtensionCalendar.Count > 0);
			_alreadyFetchedTickets = (_tickets.Count > 0);
			_alreadyFetchedTicketDeviceClass = (_ticketDeviceClass.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingOwnerClientId);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffId);
					break;
				case "CalendarEntries":
					toReturn.Add(Relations.CalendarEntryEntityUsingCalendarID);
					break;
				case "RuleCapping":
					toReturn.Add(Relations.RuleCappingEntityUsingCalendarID);
					break;
				case "RulePeriod":
					toReturn.Add(Relations.RulePeriodEntityUsingCalendarID);
					break;
				case "RulePeriodByExtensionCalendar":
					toReturn.Add(Relations.RulePeriodEntityUsingExtCalendarID);
					break;
				case "Tickets":
					toReturn.Add(Relations.TicketEntityUsingCalendarID);
					break;
				case "TicketDeviceClass":
					toReturn.Add(Relations.TicketDeviceClassEntityUsingCalendarID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_calendarEntries", (!this.MarkedForDeletion?_calendarEntries:null));
			info.AddValue("_alwaysFetchCalendarEntries", _alwaysFetchCalendarEntries);
			info.AddValue("_alreadyFetchedCalendarEntries", _alreadyFetchedCalendarEntries);
			info.AddValue("_ruleCapping", (!this.MarkedForDeletion?_ruleCapping:null));
			info.AddValue("_alwaysFetchRuleCapping", _alwaysFetchRuleCapping);
			info.AddValue("_alreadyFetchedRuleCapping", _alreadyFetchedRuleCapping);
			info.AddValue("_rulePeriod", (!this.MarkedForDeletion?_rulePeriod:null));
			info.AddValue("_alwaysFetchRulePeriod", _alwaysFetchRulePeriod);
			info.AddValue("_alreadyFetchedRulePeriod", _alreadyFetchedRulePeriod);
			info.AddValue("_rulePeriodByExtensionCalendar", (!this.MarkedForDeletion?_rulePeriodByExtensionCalendar:null));
			info.AddValue("_alwaysFetchRulePeriodByExtensionCalendar", _alwaysFetchRulePeriodByExtensionCalendar);
			info.AddValue("_alreadyFetchedRulePeriodByExtensionCalendar", _alreadyFetchedRulePeriodByExtensionCalendar);
			info.AddValue("_tickets", (!this.MarkedForDeletion?_tickets:null));
			info.AddValue("_alwaysFetchTickets", _alwaysFetchTickets);
			info.AddValue("_alreadyFetchedTickets", _alreadyFetchedTickets);
			info.AddValue("_ticketDeviceClass", (!this.MarkedForDeletion?_ticketDeviceClass:null));
			info.AddValue("_alwaysFetchTicketDeviceClass", _alwaysFetchTicketDeviceClass);
			info.AddValue("_alreadyFetchedTicketDeviceClass", _alreadyFetchedTicketDeviceClass);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "CalendarEntries":
					_alreadyFetchedCalendarEntries = true;
					if(entity!=null)
					{
						this.CalendarEntries.Add((CalendarEntryEntity)entity);
					}
					break;
				case "RuleCapping":
					_alreadyFetchedRuleCapping = true;
					if(entity!=null)
					{
						this.RuleCapping.Add((RuleCappingEntity)entity);
					}
					break;
				case "RulePeriod":
					_alreadyFetchedRulePeriod = true;
					if(entity!=null)
					{
						this.RulePeriod.Add((RulePeriodEntity)entity);
					}
					break;
				case "RulePeriodByExtensionCalendar":
					_alreadyFetchedRulePeriodByExtensionCalendar = true;
					if(entity!=null)
					{
						this.RulePeriodByExtensionCalendar.Add((RulePeriodEntity)entity);
					}
					break;
				case "Tickets":
					_alreadyFetchedTickets = true;
					if(entity!=null)
					{
						this.Tickets.Add((TicketEntity)entity);
					}
					break;
				case "TicketDeviceClass":
					_alreadyFetchedTicketDeviceClass = true;
					if(entity!=null)
					{
						this.TicketDeviceClass.Add((TicketDeviceClassEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "CalendarEntries":
					_calendarEntries.Add((CalendarEntryEntity)relatedEntity);
					break;
				case "RuleCapping":
					_ruleCapping.Add((RuleCappingEntity)relatedEntity);
					break;
				case "RulePeriod":
					_rulePeriod.Add((RulePeriodEntity)relatedEntity);
					break;
				case "RulePeriodByExtensionCalendar":
					_rulePeriodByExtensionCalendar.Add((RulePeriodEntity)relatedEntity);
					break;
				case "Tickets":
					_tickets.Add((TicketEntity)relatedEntity);
					break;
				case "TicketDeviceClass":
					_ticketDeviceClass.Add((TicketDeviceClassEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "CalendarEntries":
					this.PerformRelatedEntityRemoval(_calendarEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RuleCapping":
					this.PerformRelatedEntityRemoval(_ruleCapping, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RulePeriod":
					this.PerformRelatedEntityRemoval(_rulePeriod, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RulePeriodByExtensionCalendar":
					this.PerformRelatedEntityRemoval(_rulePeriodByExtensionCalendar, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Tickets":
					this.PerformRelatedEntityRemoval(_tickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceClass":
					this.PerformRelatedEntityRemoval(_ticketDeviceClass, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_calendarEntries);
			toReturn.Add(_ruleCapping);
			toReturn.Add(_rulePeriod);
			toReturn.Add(_rulePeriodByExtensionCalendar);
			toReturn.Add(_tickets);
			toReturn.Add(_ticketDeviceClass);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="calendarID">PK value for Calendar which data should be fetched into this Calendar object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 calendarID)
		{
			return FetchUsingPK(calendarID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="calendarID">PK value for Calendar which data should be fetched into this Calendar object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 calendarID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(calendarID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="calendarID">PK value for Calendar which data should be fetched into this Calendar object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 calendarID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(calendarID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="calendarID">PK value for Calendar which data should be fetched into this Calendar object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 calendarID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(calendarID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CalendarID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CalendarRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CalendarEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CalendarEntryCollection GetMultiCalendarEntries(bool forceFetch)
		{
			return GetMultiCalendarEntries(forceFetch, _calendarEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CalendarEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CalendarEntryCollection GetMultiCalendarEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCalendarEntries(forceFetch, _calendarEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CalendarEntryCollection GetMultiCalendarEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCalendarEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CalendarEntryCollection GetMultiCalendarEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCalendarEntries || forceFetch || _alwaysFetchCalendarEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_calendarEntries);
				_calendarEntries.SuppressClearInGetMulti=!forceFetch;
				_calendarEntries.EntityFactoryToUse = entityFactoryToUse;
				_calendarEntries.GetMultiManyToOne(this, null, filter);
				_calendarEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedCalendarEntries = true;
			}
			return _calendarEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'CalendarEntries'. These settings will be taken into account
		/// when the property CalendarEntries is requested or GetMultiCalendarEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCalendarEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_calendarEntries.SortClauses=sortClauses;
			_calendarEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCapping(bool forceFetch)
		{
			return GetMultiRuleCapping(forceFetch, _ruleCapping.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCapping(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleCapping(forceFetch, _ruleCapping.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCapping(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleCapping(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCapping(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleCapping || forceFetch || _alwaysFetchRuleCapping) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleCapping);
				_ruleCapping.SuppressClearInGetMulti=!forceFetch;
				_ruleCapping.EntityFactoryToUse = entityFactoryToUse;
				_ruleCapping.GetMultiManyToOne(this, null, null, null, filter);
				_ruleCapping.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleCapping = true;
			}
			return _ruleCapping;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleCapping'. These settings will be taken into account
		/// when the property RuleCapping is requested or GetMultiRuleCapping is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleCapping(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleCapping.SortClauses=sortClauses;
			_ruleCapping.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RulePeriodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriod(bool forceFetch)
		{
			return GetMultiRulePeriod(forceFetch, _rulePeriod.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RulePeriodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriod(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRulePeriod(forceFetch, _rulePeriod.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriod(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRulePeriod(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriod(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRulePeriod || forceFetch || _alwaysFetchRulePeriod) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_rulePeriod);
				_rulePeriod.SuppressClearInGetMulti=!forceFetch;
				_rulePeriod.EntityFactoryToUse = entityFactoryToUse;
				_rulePeriod.GetMultiManyToOne(this, null, null, null, null, null, filter);
				_rulePeriod.SuppressClearInGetMulti=false;
				_alreadyFetchedRulePeriod = true;
			}
			return _rulePeriod;
		}

		/// <summary> Sets the collection parameters for the collection for 'RulePeriod'. These settings will be taken into account
		/// when the property RulePeriod is requested or GetMultiRulePeriod is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRulePeriod(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_rulePeriod.SortClauses=sortClauses;
			_rulePeriod.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RulePeriodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodByExtensionCalendar(bool forceFetch)
		{
			return GetMultiRulePeriodByExtensionCalendar(forceFetch, _rulePeriodByExtensionCalendar.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RulePeriodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodByExtensionCalendar(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRulePeriodByExtensionCalendar(forceFetch, _rulePeriodByExtensionCalendar.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodByExtensionCalendar(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRulePeriodByExtensionCalendar(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodByExtensionCalendar(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRulePeriodByExtensionCalendar || forceFetch || _alwaysFetchRulePeriodByExtensionCalendar) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_rulePeriodByExtensionCalendar);
				_rulePeriodByExtensionCalendar.SuppressClearInGetMulti=!forceFetch;
				_rulePeriodByExtensionCalendar.EntityFactoryToUse = entityFactoryToUse;
				_rulePeriodByExtensionCalendar.GetMultiManyToOne(null, this, null, null, null, null, filter);
				_rulePeriodByExtensionCalendar.SuppressClearInGetMulti=false;
				_alreadyFetchedRulePeriodByExtensionCalendar = true;
			}
			return _rulePeriodByExtensionCalendar;
		}

		/// <summary> Sets the collection parameters for the collection for 'RulePeriodByExtensionCalendar'. These settings will be taken into account
		/// when the property RulePeriodByExtensionCalendar is requested or GetMultiRulePeriodByExtensionCalendar is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRulePeriodByExtensionCalendar(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_rulePeriodByExtensionCalendar.SortClauses=sortClauses;
			_rulePeriodByExtensionCalendar.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTickets || forceFetch || _alwaysFetchTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tickets);
				_tickets.SuppressClearInGetMulti=!forceFetch;
				_tickets.EntityFactoryToUse = entityFactoryToUse;
				_tickets.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_tickets.SuppressClearInGetMulti=false;
				_alreadyFetchedTickets = true;
			}
			return _tickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'Tickets'. These settings will be taken into account
		/// when the property Tickets is requested or GetMultiTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tickets.SortClauses=sortClauses;
			_tickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClass(bool forceFetch)
		{
			return GetMultiTicketDeviceClass(forceFetch, _ticketDeviceClass.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClass(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceClass(forceFetch, _ticketDeviceClass.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClass(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceClass(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClass(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceClass || forceFetch || _alwaysFetchTicketDeviceClass) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceClass);
				_ticketDeviceClass.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceClass.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceClass.GetMultiManyToOne(this, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_ticketDeviceClass.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceClass = true;
			}
			return _ticketDeviceClass;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceClass'. These settings will be taken into account
		/// when the property TicketDeviceClass is requested or GetMultiTicketDeviceClass is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceClass(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceClass.SortClauses=sortClauses;
			_ticketDeviceClass.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingOwnerClientId);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OwnerClientId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffId);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("CalendarEntries", _calendarEntries);
			toReturn.Add("RuleCapping", _ruleCapping);
			toReturn.Add("RulePeriod", _rulePeriod);
			toReturn.Add("RulePeriodByExtensionCalendar", _rulePeriodByExtensionCalendar);
			toReturn.Add("Tickets", _tickets);
			toReturn.Add("TicketDeviceClass", _ticketDeviceClass);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="calendarID">PK value for Calendar which data should be fetched into this Calendar object</param>
		/// <param name="validator">The validator object for this CalendarEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 calendarID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(calendarID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_calendarEntries = new VarioSL.Entities.CollectionClasses.CalendarEntryCollection();
			_calendarEntries.SetContainingEntityInfo(this, "Calendar");

			_ruleCapping = new VarioSL.Entities.CollectionClasses.RuleCappingCollection();
			_ruleCapping.SetContainingEntityInfo(this, "Calendar");

			_rulePeriod = new VarioSL.Entities.CollectionClasses.RulePeriodCollection();
			_rulePeriod.SetContainingEntityInfo(this, "Calendar");

			_rulePeriodByExtensionCalendar = new VarioSL.Entities.CollectionClasses.RulePeriodCollection();
			_rulePeriodByExtensionCalendar.SetContainingEntityInfo(this, "ExtensionCalendar");

			_tickets = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_tickets.SetContainingEntityInfo(this, "Calendar");

			_ticketDeviceClass = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			_ticketDeviceClass.SetContainingEntityInfo(this, "Calendar");
			_clientReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CalendarID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OwnerClientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticCalendarRelations.ClientEntityUsingOwnerClientIdStatic, true, signalRelatedEntity, "Calendar", resetFKFields, new int[] { (int)CalendarFieldIndex.OwnerClientId } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticCalendarRelations.ClientEntityUsingOwnerClientIdStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticCalendarRelations.TariffEntityUsingTariffIdStatic, true, signalRelatedEntity, "Calendars", resetFKFields, new int[] { (int)CalendarFieldIndex.TariffId } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticCalendarRelations.TariffEntityUsingTariffIdStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="calendarID">PK value for Calendar which data should be fetched into this Calendar object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 calendarID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CalendarFieldIndex.CalendarID].ForcedCurrentValueWrite(calendarID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCalendarDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CalendarEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CalendarRelations Relations
		{
			get	{ return new CalendarRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CalendarEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCalendarEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CalendarEntryCollection(), (IEntityRelation)GetRelationsForField("CalendarEntries")[0], (int)VarioSL.Entities.EntityType.CalendarEntity, (int)VarioSL.Entities.EntityType.CalendarEntryEntity, 0, null, null, null, "CalendarEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleCapping' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleCapping
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleCappingCollection(), (IEntityRelation)GetRelationsForField("RuleCapping")[0], (int)VarioSL.Entities.EntityType.CalendarEntity, (int)VarioSL.Entities.EntityType.RuleCappingEntity, 0, null, null, null, "RuleCapping", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RulePeriod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRulePeriod
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RulePeriodCollection(), (IEntityRelation)GetRelationsForField("RulePeriod")[0], (int)VarioSL.Entities.EntityType.CalendarEntity, (int)VarioSL.Entities.EntityType.RulePeriodEntity, 0, null, null, null, "RulePeriod", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RulePeriod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRulePeriodByExtensionCalendar
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RulePeriodCollection(), (IEntityRelation)GetRelationsForField("RulePeriodByExtensionCalendar")[0], (int)VarioSL.Entities.EntityType.CalendarEntity, (int)VarioSL.Entities.EntityType.RulePeriodEntity, 0, null, null, null, "RulePeriodByExtensionCalendar", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Tickets")[0], (int)VarioSL.Entities.EntityType.CalendarEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Tickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceClass
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceClass")[0], (int)VarioSL.Entities.EntityType.CalendarEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, null, "TicketDeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.CalendarEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.CalendarEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CalendarID property of the Entity Calendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CALENDAR"."CALENDARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 CalendarID
		{
			get { return (System.Int64)GetValue((int)CalendarFieldIndex.CalendarID, true); }
			set	{ SetValue((int)CalendarFieldIndex.CalendarID, value, true); }
		}

		/// <summary> The Description property of the Entity Calendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CALENDAR"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CalendarFieldIndex.Description, true); }
			set	{ SetValue((int)CalendarFieldIndex.Description, value, true); }
		}

		/// <summary> The Name property of the Entity Calendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CALENDAR"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CalendarFieldIndex.Name, true); }
			set	{ SetValue((int)CalendarFieldIndex.Name, value, true); }
		}

		/// <summary> The Number property of the Entity Calendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CALENDAR"."NUMBER_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Number
		{
			get { return (Nullable<System.Int64>)GetValue((int)CalendarFieldIndex.Number, false); }
			set	{ SetValue((int)CalendarFieldIndex.Number, value, true); }
		}

		/// <summary> The OwnerClientId property of the Entity Calendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CALENDAR"."OWNERCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OwnerClientId
		{
			get { return (Nullable<System.Int64>)GetValue((int)CalendarFieldIndex.OwnerClientId, false); }
			set	{ SetValue((int)CalendarFieldIndex.OwnerClientId, value, true); }
		}

		/// <summary> The TariffId property of the Entity Calendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CALENDAR"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffId
		{
			get { return (Nullable<System.Int64>)GetValue((int)CalendarFieldIndex.TariffId, false); }
			set	{ SetValue((int)CalendarFieldIndex.TariffId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCalendarEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CalendarEntryCollection CalendarEntries
		{
			get	{ return GetMultiCalendarEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CalendarEntries. When set to true, CalendarEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CalendarEntries is accessed. You can always execute/ a forced fetch by calling GetMultiCalendarEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCalendarEntries
		{
			get	{ return _alwaysFetchCalendarEntries; }
			set	{ _alwaysFetchCalendarEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CalendarEntries already has been fetched. Setting this property to false when CalendarEntries has been fetched
		/// will clear the CalendarEntries collection well. Setting this property to true while CalendarEntries hasn't been fetched disables lazy loading for CalendarEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCalendarEntries
		{
			get { return _alreadyFetchedCalendarEntries;}
			set 
			{
				if(_alreadyFetchedCalendarEntries && !value && (_calendarEntries != null))
				{
					_calendarEntries.Clear();
				}
				_alreadyFetchedCalendarEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleCapping()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingCollection RuleCapping
		{
			get	{ return GetMultiRuleCapping(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleCapping. When set to true, RuleCapping is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleCapping is accessed. You can always execute/ a forced fetch by calling GetMultiRuleCapping(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleCapping
		{
			get	{ return _alwaysFetchRuleCapping; }
			set	{ _alwaysFetchRuleCapping = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleCapping already has been fetched. Setting this property to false when RuleCapping has been fetched
		/// will clear the RuleCapping collection well. Setting this property to true while RuleCapping hasn't been fetched disables lazy loading for RuleCapping</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleCapping
		{
			get { return _alreadyFetchedRuleCapping;}
			set 
			{
				if(_alreadyFetchedRuleCapping && !value && (_ruleCapping != null))
				{
					_ruleCapping.Clear();
				}
				_alreadyFetchedRuleCapping = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRulePeriod()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RulePeriodCollection RulePeriod
		{
			get	{ return GetMultiRulePeriod(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RulePeriod. When set to true, RulePeriod is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RulePeriod is accessed. You can always execute/ a forced fetch by calling GetMultiRulePeriod(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRulePeriod
		{
			get	{ return _alwaysFetchRulePeriod; }
			set	{ _alwaysFetchRulePeriod = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RulePeriod already has been fetched. Setting this property to false when RulePeriod has been fetched
		/// will clear the RulePeriod collection well. Setting this property to true while RulePeriod hasn't been fetched disables lazy loading for RulePeriod</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRulePeriod
		{
			get { return _alreadyFetchedRulePeriod;}
			set 
			{
				if(_alreadyFetchedRulePeriod && !value && (_rulePeriod != null))
				{
					_rulePeriod.Clear();
				}
				_alreadyFetchedRulePeriod = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRulePeriodByExtensionCalendar()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RulePeriodCollection RulePeriodByExtensionCalendar
		{
			get	{ return GetMultiRulePeriodByExtensionCalendar(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RulePeriodByExtensionCalendar. When set to true, RulePeriodByExtensionCalendar is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RulePeriodByExtensionCalendar is accessed. You can always execute/ a forced fetch by calling GetMultiRulePeriodByExtensionCalendar(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRulePeriodByExtensionCalendar
		{
			get	{ return _alwaysFetchRulePeriodByExtensionCalendar; }
			set	{ _alwaysFetchRulePeriodByExtensionCalendar = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RulePeriodByExtensionCalendar already has been fetched. Setting this property to false when RulePeriodByExtensionCalendar has been fetched
		/// will clear the RulePeriodByExtensionCalendar collection well. Setting this property to true while RulePeriodByExtensionCalendar hasn't been fetched disables lazy loading for RulePeriodByExtensionCalendar</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRulePeriodByExtensionCalendar
		{
			get { return _alreadyFetchedRulePeriodByExtensionCalendar;}
			set 
			{
				if(_alreadyFetchedRulePeriodByExtensionCalendar && !value && (_rulePeriodByExtensionCalendar != null))
				{
					_rulePeriodByExtensionCalendar.Clear();
				}
				_alreadyFetchedRulePeriodByExtensionCalendar = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection Tickets
		{
			get	{ return GetMultiTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Tickets. When set to true, Tickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tickets is accessed. You can always execute/ a forced fetch by calling GetMultiTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTickets
		{
			get	{ return _alwaysFetchTickets; }
			set	{ _alwaysFetchTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tickets already has been fetched. Setting this property to false when Tickets has been fetched
		/// will clear the Tickets collection well. Setting this property to true while Tickets hasn't been fetched disables lazy loading for Tickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTickets
		{
			get { return _alreadyFetchedTickets;}
			set 
			{
				if(_alreadyFetchedTickets && !value && (_tickets != null))
				{
					_tickets.Clear();
				}
				_alreadyFetchedTickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceClass
		{
			get	{ return GetMultiTicketDeviceClass(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceClass. When set to true, TicketDeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceClass is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceClass
		{
			get	{ return _alwaysFetchTicketDeviceClass; }
			set	{ _alwaysFetchTicketDeviceClass = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceClass already has been fetched. Setting this property to false when TicketDeviceClass has been fetched
		/// will clear the TicketDeviceClass collection well. Setting this property to true while TicketDeviceClass hasn't been fetched disables lazy loading for TicketDeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceClass
		{
			get { return _alreadyFetchedTicketDeviceClass;}
			set 
			{
				if(_alreadyFetchedTicketDeviceClass && !value && (_ticketDeviceClass != null))
				{
					_ticketDeviceClass.Clear();
				}
				_alreadyFetchedTicketDeviceClass = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Calendar", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Calendars", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CalendarEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
