﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'VdvLayoutObject'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class VdvLayoutObjectEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection	_childrenVdvLayoutObjects;
		private bool	_alwaysFetchChildrenVdvLayoutObjects, _alreadyFetchedChildrenVdvLayoutObjects;
		private VdvLayoutEntity _vdvLayout;
		private bool	_alwaysFetchVdvLayout, _alreadyFetchedVdvLayout, _vdvLayoutReturnsNewIfNotFound;
		private VdvLayoutObjectEntity _parentVdvLayoutObject;
		private bool	_alwaysFetchParentVdvLayoutObject, _alreadyFetchedParentVdvLayoutObject, _parentVdvLayoutObjectReturnsNewIfNotFound;
		private VdvTagEntity _vdvTag;
		private bool	_alwaysFetchVdvTag, _alreadyFetchedVdvTag, _vdvTagReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name VdvLayout</summary>
			public static readonly string VdvLayout = "VdvLayout";
			/// <summary>Member name ParentVdvLayoutObject</summary>
			public static readonly string ParentVdvLayoutObject = "ParentVdvLayoutObject";
			/// <summary>Member name VdvTag</summary>
			public static readonly string VdvTag = "VdvTag";
			/// <summary>Member name ChildrenVdvLayoutObjects</summary>
			public static readonly string ChildrenVdvLayoutObjects = "ChildrenVdvLayoutObjects";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static VdvLayoutObjectEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public VdvLayoutObjectEntity() :base("VdvLayoutObjectEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for VdvLayoutObject which data should be fetched into this VdvLayoutObject object</param>
		public VdvLayoutObjectEntity(System.Int64 layoutObjectID):base("VdvLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for VdvLayoutObject which data should be fetched into this VdvLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public VdvLayoutObjectEntity(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse):base("VdvLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for VdvLayoutObject which data should be fetched into this VdvLayoutObject object</param>
		/// <param name="validator">The custom validator object for this VdvLayoutObjectEntity</param>
		public VdvLayoutObjectEntity(System.Int64 layoutObjectID, IValidator validator):base("VdvLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VdvLayoutObjectEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_childrenVdvLayoutObjects = (VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection)info.GetValue("_childrenVdvLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection));
			_alwaysFetchChildrenVdvLayoutObjects = info.GetBoolean("_alwaysFetchChildrenVdvLayoutObjects");
			_alreadyFetchedChildrenVdvLayoutObjects = info.GetBoolean("_alreadyFetchedChildrenVdvLayoutObjects");
			_vdvLayout = (VdvLayoutEntity)info.GetValue("_vdvLayout", typeof(VdvLayoutEntity));
			if(_vdvLayout!=null)
			{
				_vdvLayout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_vdvLayoutReturnsNewIfNotFound = info.GetBoolean("_vdvLayoutReturnsNewIfNotFound");
			_alwaysFetchVdvLayout = info.GetBoolean("_alwaysFetchVdvLayout");
			_alreadyFetchedVdvLayout = info.GetBoolean("_alreadyFetchedVdvLayout");

			_parentVdvLayoutObject = (VdvLayoutObjectEntity)info.GetValue("_parentVdvLayoutObject", typeof(VdvLayoutObjectEntity));
			if(_parentVdvLayoutObject!=null)
			{
				_parentVdvLayoutObject.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parentVdvLayoutObjectReturnsNewIfNotFound = info.GetBoolean("_parentVdvLayoutObjectReturnsNewIfNotFound");
			_alwaysFetchParentVdvLayoutObject = info.GetBoolean("_alwaysFetchParentVdvLayoutObject");
			_alreadyFetchedParentVdvLayoutObject = info.GetBoolean("_alreadyFetchedParentVdvLayoutObject");

			_vdvTag = (VdvTagEntity)info.GetValue("_vdvTag", typeof(VdvTagEntity));
			if(_vdvTag!=null)
			{
				_vdvTag.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_vdvTagReturnsNewIfNotFound = info.GetBoolean("_vdvTagReturnsNewIfNotFound");
			_alwaysFetchVdvTag = info.GetBoolean("_alwaysFetchVdvTag");
			_alreadyFetchedVdvTag = info.GetBoolean("_alreadyFetchedVdvTag");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((VdvLayoutObjectFieldIndex)fieldIndex)
			{
				case VdvLayoutObjectFieldIndex.LayoutID:
					DesetupSyncVdvLayout(true, false);
					_alreadyFetchedVdvLayout = false;
					break;
				case VdvLayoutObjectFieldIndex.ParentLayoutObjectID:
					DesetupSyncParentVdvLayoutObject(true, false);
					_alreadyFetchedParentVdvLayoutObject = false;
					break;
				case VdvLayoutObjectFieldIndex.VdvTagID:
					DesetupSyncVdvTag(true, false);
					_alreadyFetchedVdvTag = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedChildrenVdvLayoutObjects = (_childrenVdvLayoutObjects.Count > 0);
			_alreadyFetchedVdvLayout = (_vdvLayout != null);
			_alreadyFetchedParentVdvLayoutObject = (_parentVdvLayoutObject != null);
			_alreadyFetchedVdvTag = (_vdvTag != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "VdvLayout":
					toReturn.Add(Relations.VdvLayoutEntityUsingLayoutID);
					break;
				case "ParentVdvLayoutObject":
					toReturn.Add(Relations.VdvLayoutObjectEntityUsingLayoutObjectIDParentLayoutObjectID);
					break;
				case "VdvTag":
					toReturn.Add(Relations.VdvTagEntityUsingVdvTagID);
					break;
				case "ChildrenVdvLayoutObjects":
					toReturn.Add(Relations.VdvLayoutObjectEntityUsingParentLayoutObjectID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_childrenVdvLayoutObjects", (!this.MarkedForDeletion?_childrenVdvLayoutObjects:null));
			info.AddValue("_alwaysFetchChildrenVdvLayoutObjects", _alwaysFetchChildrenVdvLayoutObjects);
			info.AddValue("_alreadyFetchedChildrenVdvLayoutObjects", _alreadyFetchedChildrenVdvLayoutObjects);
			info.AddValue("_vdvLayout", (!this.MarkedForDeletion?_vdvLayout:null));
			info.AddValue("_vdvLayoutReturnsNewIfNotFound", _vdvLayoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVdvLayout", _alwaysFetchVdvLayout);
			info.AddValue("_alreadyFetchedVdvLayout", _alreadyFetchedVdvLayout);
			info.AddValue("_parentVdvLayoutObject", (!this.MarkedForDeletion?_parentVdvLayoutObject:null));
			info.AddValue("_parentVdvLayoutObjectReturnsNewIfNotFound", _parentVdvLayoutObjectReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParentVdvLayoutObject", _alwaysFetchParentVdvLayoutObject);
			info.AddValue("_alreadyFetchedParentVdvLayoutObject", _alreadyFetchedParentVdvLayoutObject);
			info.AddValue("_vdvTag", (!this.MarkedForDeletion?_vdvTag:null));
			info.AddValue("_vdvTagReturnsNewIfNotFound", _vdvTagReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVdvTag", _alwaysFetchVdvTag);
			info.AddValue("_alreadyFetchedVdvTag", _alreadyFetchedVdvTag);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "VdvLayout":
					_alreadyFetchedVdvLayout = true;
					this.VdvLayout = (VdvLayoutEntity)entity;
					break;
				case "ParentVdvLayoutObject":
					_alreadyFetchedParentVdvLayoutObject = true;
					this.ParentVdvLayoutObject = (VdvLayoutObjectEntity)entity;
					break;
				case "VdvTag":
					_alreadyFetchedVdvTag = true;
					this.VdvTag = (VdvTagEntity)entity;
					break;
				case "ChildrenVdvLayoutObjects":
					_alreadyFetchedChildrenVdvLayoutObjects = true;
					if(entity!=null)
					{
						this.ChildrenVdvLayoutObjects.Add((VdvLayoutObjectEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "VdvLayout":
					SetupSyncVdvLayout(relatedEntity);
					break;
				case "ParentVdvLayoutObject":
					SetupSyncParentVdvLayoutObject(relatedEntity);
					break;
				case "VdvTag":
					SetupSyncVdvTag(relatedEntity);
					break;
				case "ChildrenVdvLayoutObjects":
					_childrenVdvLayoutObjects.Add((VdvLayoutObjectEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "VdvLayout":
					DesetupSyncVdvLayout(false, true);
					break;
				case "ParentVdvLayoutObject":
					DesetupSyncParentVdvLayoutObject(false, true);
					break;
				case "VdvTag":
					DesetupSyncVdvTag(false, true);
					break;
				case "ChildrenVdvLayoutObjects":
					this.PerformRelatedEntityRemoval(_childrenVdvLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_vdvLayout!=null)
			{
				toReturn.Add(_vdvLayout);
			}
			if(_parentVdvLayoutObject!=null)
			{
				toReturn.Add(_parentVdvLayoutObject);
			}
			if(_vdvTag!=null)
			{
				toReturn.Add(_vdvTag);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_childrenVdvLayoutObjects);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for VdvLayoutObject which data should be fetched into this VdvLayoutObject object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID)
		{
			return FetchUsingPK(layoutObjectID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for VdvLayoutObject which data should be fetched into this VdvLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(layoutObjectID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for VdvLayoutObject which data should be fetched into this VdvLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(layoutObjectID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for VdvLayoutObject which data should be fetched into this VdvLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(layoutObjectID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.LayoutObjectID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new VdvLayoutObjectRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection GetMultiChildrenVdvLayoutObjects(bool forceFetch)
		{
			return GetMultiChildrenVdvLayoutObjects(forceFetch, _childrenVdvLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection GetMultiChildrenVdvLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiChildrenVdvLayoutObjects(forceFetch, _childrenVdvLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection GetMultiChildrenVdvLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiChildrenVdvLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection GetMultiChildrenVdvLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedChildrenVdvLayoutObjects || forceFetch || _alwaysFetchChildrenVdvLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_childrenVdvLayoutObjects);
				_childrenVdvLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_childrenVdvLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_childrenVdvLayoutObjects.GetMultiManyToOne(null, this, null, filter);
				_childrenVdvLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedChildrenVdvLayoutObjects = true;
			}
			return _childrenVdvLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'ChildrenVdvLayoutObjects'. These settings will be taken into account
		/// when the property ChildrenVdvLayoutObjects is requested or GetMultiChildrenVdvLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersChildrenVdvLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_childrenVdvLayoutObjects.SortClauses=sortClauses;
			_childrenVdvLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'VdvLayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VdvLayoutEntity' which is related to this entity.</returns>
		public VdvLayoutEntity GetSingleVdvLayout()
		{
			return GetSingleVdvLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'VdvLayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VdvLayoutEntity' which is related to this entity.</returns>
		public virtual VdvLayoutEntity GetSingleVdvLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedVdvLayout || forceFetch || _alwaysFetchVdvLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VdvLayoutEntityUsingLayoutID);
				VdvLayoutEntity newEntity = new VdvLayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LayoutID);
				}
				if(fetchResult)
				{
					newEntity = (VdvLayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_vdvLayoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VdvLayout = newEntity;
				_alreadyFetchedVdvLayout = fetchResult;
			}
			return _vdvLayout;
		}


		/// <summary> Retrieves the related entity of type 'VdvLayoutObjectEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VdvLayoutObjectEntity' which is related to this entity.</returns>
		public VdvLayoutObjectEntity GetSingleParentVdvLayoutObject()
		{
			return GetSingleParentVdvLayoutObject(false);
		}

		/// <summary> Retrieves the related entity of type 'VdvLayoutObjectEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VdvLayoutObjectEntity' which is related to this entity.</returns>
		public virtual VdvLayoutObjectEntity GetSingleParentVdvLayoutObject(bool forceFetch)
		{
			if( ( !_alreadyFetchedParentVdvLayoutObject || forceFetch || _alwaysFetchParentVdvLayoutObject) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VdvLayoutObjectEntityUsingLayoutObjectIDParentLayoutObjectID);
				VdvLayoutObjectEntity newEntity = new VdvLayoutObjectEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParentLayoutObjectID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (VdvLayoutObjectEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parentVdvLayoutObjectReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParentVdvLayoutObject = newEntity;
				_alreadyFetchedParentVdvLayoutObject = fetchResult;
			}
			return _parentVdvLayoutObject;
		}


		/// <summary> Retrieves the related entity of type 'VdvTagEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VdvTagEntity' which is related to this entity.</returns>
		public VdvTagEntity GetSingleVdvTag()
		{
			return GetSingleVdvTag(false);
		}

		/// <summary> Retrieves the related entity of type 'VdvTagEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VdvTagEntity' which is related to this entity.</returns>
		public virtual VdvTagEntity GetSingleVdvTag(bool forceFetch)
		{
			if( ( !_alreadyFetchedVdvTag || forceFetch || _alwaysFetchVdvTag) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VdvTagEntityUsingVdvTagID);
				VdvTagEntity newEntity = new VdvTagEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.VdvTagID);
				}
				if(fetchResult)
				{
					newEntity = (VdvTagEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_vdvTagReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VdvTag = newEntity;
				_alreadyFetchedVdvTag = fetchResult;
			}
			return _vdvTag;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("VdvLayout", _vdvLayout);
			toReturn.Add("ParentVdvLayoutObject", _parentVdvLayoutObject);
			toReturn.Add("VdvTag", _vdvTag);
			toReturn.Add("ChildrenVdvLayoutObjects", _childrenVdvLayoutObjects);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="layoutObjectID">PK value for VdvLayoutObject which data should be fetched into this VdvLayoutObject object</param>
		/// <param name="validator">The validator object for this VdvLayoutObjectEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 layoutObjectID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(layoutObjectID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_childrenVdvLayoutObjects = new VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection();
			_childrenVdvLayoutObjects.SetContainingEntityInfo(this, "ParentVdvLayoutObject");
			_vdvLayoutReturnsNewIfNotFound = false;
			_parentVdvLayoutObjectReturnsNewIfNotFound = false;
			_vdvTagReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutObjectID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentLayoutObjectID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VdvTagID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _vdvLayout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVdvLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _vdvLayout, new PropertyChangedEventHandler( OnVdvLayoutPropertyChanged ), "VdvLayout", VarioSL.Entities.RelationClasses.StaticVdvLayoutObjectRelations.VdvLayoutEntityUsingLayoutIDStatic, true, signalRelatedEntity, "VdvLayoutObjects", resetFKFields, new int[] { (int)VdvLayoutObjectFieldIndex.LayoutID } );		
			_vdvLayout = null;
		}
		
		/// <summary> setups the sync logic for member _vdvLayout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVdvLayout(IEntityCore relatedEntity)
		{
			if(_vdvLayout!=relatedEntity)
			{		
				DesetupSyncVdvLayout(true, true);
				_vdvLayout = (VdvLayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _vdvLayout, new PropertyChangedEventHandler( OnVdvLayoutPropertyChanged ), "VdvLayout", VarioSL.Entities.RelationClasses.StaticVdvLayoutObjectRelations.VdvLayoutEntityUsingLayoutIDStatic, true, ref _alreadyFetchedVdvLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVdvLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _parentVdvLayoutObject</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParentVdvLayoutObject(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parentVdvLayoutObject, new PropertyChangedEventHandler( OnParentVdvLayoutObjectPropertyChanged ), "ParentVdvLayoutObject", VarioSL.Entities.RelationClasses.StaticVdvLayoutObjectRelations.VdvLayoutObjectEntityUsingLayoutObjectIDParentLayoutObjectIDStatic, true, signalRelatedEntity, "ChildrenVdvLayoutObjects", resetFKFields, new int[] { (int)VdvLayoutObjectFieldIndex.ParentLayoutObjectID } );		
			_parentVdvLayoutObject = null;
		}
		
		/// <summary> setups the sync logic for member _parentVdvLayoutObject</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParentVdvLayoutObject(IEntityCore relatedEntity)
		{
			if(_parentVdvLayoutObject!=relatedEntity)
			{		
				DesetupSyncParentVdvLayoutObject(true, true);
				_parentVdvLayoutObject = (VdvLayoutObjectEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parentVdvLayoutObject, new PropertyChangedEventHandler( OnParentVdvLayoutObjectPropertyChanged ), "ParentVdvLayoutObject", VarioSL.Entities.RelationClasses.StaticVdvLayoutObjectRelations.VdvLayoutObjectEntityUsingLayoutObjectIDParentLayoutObjectIDStatic, true, ref _alreadyFetchedParentVdvLayoutObject, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParentVdvLayoutObjectPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _vdvTag</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVdvTag(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _vdvTag, new PropertyChangedEventHandler( OnVdvTagPropertyChanged ), "VdvTag", VarioSL.Entities.RelationClasses.StaticVdvLayoutObjectRelations.VdvTagEntityUsingVdvTagIDStatic, true, signalRelatedEntity, "VdvLayoutObjects", resetFKFields, new int[] { (int)VdvLayoutObjectFieldIndex.VdvTagID } );		
			_vdvTag = null;
		}
		
		/// <summary> setups the sync logic for member _vdvTag</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVdvTag(IEntityCore relatedEntity)
		{
			if(_vdvTag!=relatedEntity)
			{		
				DesetupSyncVdvTag(true, true);
				_vdvTag = (VdvTagEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _vdvTag, new PropertyChangedEventHandler( OnVdvTagPropertyChanged ), "VdvTag", VarioSL.Entities.RelationClasses.StaticVdvLayoutObjectRelations.VdvTagEntityUsingVdvTagIDStatic, true, ref _alreadyFetchedVdvTag, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVdvTagPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="layoutObjectID">PK value for VdvLayoutObject which data should be fetched into this VdvLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)VdvLayoutObjectFieldIndex.LayoutObjectID].ForcedCurrentValueWrite(layoutObjectID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateVdvLayoutObjectDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new VdvLayoutObjectEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static VdvLayoutObjectRelations Relations
		{
			get	{ return new VdvLayoutObjectRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathChildrenVdvLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("ChildrenVdvLayoutObjects")[0], (int)VarioSL.Entities.EntityType.VdvLayoutObjectEntity, (int)VarioSL.Entities.EntityType.VdvLayoutObjectEntity, 0, null, null, null, "ChildrenVdvLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvLayout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvLayoutCollection(), (IEntityRelation)GetRelationsForField("VdvLayout")[0], (int)VarioSL.Entities.EntityType.VdvLayoutObjectEntity, (int)VarioSL.Entities.EntityType.VdvLayoutEntity, 0, null, null, null, "VdvLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvLayoutObject'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParentVdvLayoutObject
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("ParentVdvLayoutObject")[0], (int)VarioSL.Entities.EntityType.VdvLayoutObjectEntity, (int)VarioSL.Entities.EntityType.VdvLayoutObjectEntity, 0, null, null, null, "ParentVdvLayoutObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvTag'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvTag
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvTagCollection(), (IEntityRelation)GetRelationsForField("VdvTag")[0], (int)VarioSL.Entities.EntityType.VdvLayoutObjectEntity, (int)VarioSL.Entities.EntityType.VdvTagEntity, 0, null, null, null, "VdvTag", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LayoutObjectID property of the Entity VdvLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_LAYOUTOBJECT"."LAYOUTOBJECTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 LayoutObjectID
		{
			get { return (System.Int64)GetValue((int)VdvLayoutObjectFieldIndex.LayoutObjectID, true); }
			set	{ SetValue((int)VdvLayoutObjectFieldIndex.LayoutObjectID, value, true); }
		}

		/// <summary> The LayoutID property of the Entity VdvLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_LAYOUTOBJECT"."LAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 LayoutID
		{
			get { return (System.Int64)GetValue((int)VdvLayoutObjectFieldIndex.LayoutID, true); }
			set	{ SetValue((int)VdvLayoutObjectFieldIndex.LayoutID, value, true); }
		}

		/// <summary> The OrderNumber property of the Entity VdvLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_LAYOUTOBJECT"."ORDERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 OrderNumber
		{
			get { return (System.Int64)GetValue((int)VdvLayoutObjectFieldIndex.OrderNumber, true); }
			set	{ SetValue((int)VdvLayoutObjectFieldIndex.OrderNumber, value, true); }
		}

		/// <summary> The ParentLayoutObjectID property of the Entity VdvLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_LAYOUTOBJECT"."PARENTLAYOUTOBJECTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ParentLayoutObjectID
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvLayoutObjectFieldIndex.ParentLayoutObjectID, false); }
			set	{ SetValue((int)VdvLayoutObjectFieldIndex.ParentLayoutObjectID, value, true); }
		}

		/// <summary> The VdvTagID property of the Entity VdvLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_LAYOUTOBJECT"."VDVTAGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 VdvTagID
		{
			get { return (System.Int64)GetValue((int)VdvLayoutObjectFieldIndex.VdvTagID, true); }
			set	{ SetValue((int)VdvLayoutObjectFieldIndex.VdvTagID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiChildrenVdvLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection ChildrenVdvLayoutObjects
		{
			get	{ return GetMultiChildrenVdvLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ChildrenVdvLayoutObjects. When set to true, ChildrenVdvLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ChildrenVdvLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiChildrenVdvLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchChildrenVdvLayoutObjects
		{
			get	{ return _alwaysFetchChildrenVdvLayoutObjects; }
			set	{ _alwaysFetchChildrenVdvLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ChildrenVdvLayoutObjects already has been fetched. Setting this property to false when ChildrenVdvLayoutObjects has been fetched
		/// will clear the ChildrenVdvLayoutObjects collection well. Setting this property to true while ChildrenVdvLayoutObjects hasn't been fetched disables lazy loading for ChildrenVdvLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedChildrenVdvLayoutObjects
		{
			get { return _alreadyFetchedChildrenVdvLayoutObjects;}
			set 
			{
				if(_alreadyFetchedChildrenVdvLayoutObjects && !value && (_childrenVdvLayoutObjects != null))
				{
					_childrenVdvLayoutObjects.Clear();
				}
				_alreadyFetchedChildrenVdvLayoutObjects = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'VdvLayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVdvLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VdvLayoutEntity VdvLayout
		{
			get	{ return GetSingleVdvLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVdvLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvLayoutObjects", "VdvLayout", _vdvLayout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VdvLayout. When set to true, VdvLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvLayout is accessed. You can always execute a forced fetch by calling GetSingleVdvLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvLayout
		{
			get	{ return _alwaysFetchVdvLayout; }
			set	{ _alwaysFetchVdvLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvLayout already has been fetched. Setting this property to false when VdvLayout has been fetched
		/// will set VdvLayout to null as well. Setting this property to true while VdvLayout hasn't been fetched disables lazy loading for VdvLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvLayout
		{
			get { return _alreadyFetchedVdvLayout;}
			set 
			{
				if(_alreadyFetchedVdvLayout && !value)
				{
					this.VdvLayout = null;
				}
				_alreadyFetchedVdvLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VdvLayout is not found
		/// in the database. When set to true, VdvLayout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool VdvLayoutReturnsNewIfNotFound
		{
			get	{ return _vdvLayoutReturnsNewIfNotFound; }
			set { _vdvLayoutReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VdvLayoutObjectEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParentVdvLayoutObject()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VdvLayoutObjectEntity ParentVdvLayoutObject
		{
			get	{ return GetSingleParentVdvLayoutObject(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParentVdvLayoutObject(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ChildrenVdvLayoutObjects", "ParentVdvLayoutObject", _parentVdvLayoutObject, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParentVdvLayoutObject. When set to true, ParentVdvLayoutObject is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParentVdvLayoutObject is accessed. You can always execute a forced fetch by calling GetSingleParentVdvLayoutObject(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParentVdvLayoutObject
		{
			get	{ return _alwaysFetchParentVdvLayoutObject; }
			set	{ _alwaysFetchParentVdvLayoutObject = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParentVdvLayoutObject already has been fetched. Setting this property to false when ParentVdvLayoutObject has been fetched
		/// will set ParentVdvLayoutObject to null as well. Setting this property to true while ParentVdvLayoutObject hasn't been fetched disables lazy loading for ParentVdvLayoutObject</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParentVdvLayoutObject
		{
			get { return _alreadyFetchedParentVdvLayoutObject;}
			set 
			{
				if(_alreadyFetchedParentVdvLayoutObject && !value)
				{
					this.ParentVdvLayoutObject = null;
				}
				_alreadyFetchedParentVdvLayoutObject = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParentVdvLayoutObject is not found
		/// in the database. When set to true, ParentVdvLayoutObject will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ParentVdvLayoutObjectReturnsNewIfNotFound
		{
			get	{ return _parentVdvLayoutObjectReturnsNewIfNotFound; }
			set { _parentVdvLayoutObjectReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VdvTagEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVdvTag()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VdvTagEntity VdvTag
		{
			get	{ return GetSingleVdvTag(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVdvTag(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvLayoutObjects", "VdvTag", _vdvTag, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VdvTag. When set to true, VdvTag is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvTag is accessed. You can always execute a forced fetch by calling GetSingleVdvTag(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvTag
		{
			get	{ return _alwaysFetchVdvTag; }
			set	{ _alwaysFetchVdvTag = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvTag already has been fetched. Setting this property to false when VdvTag has been fetched
		/// will set VdvTag to null as well. Setting this property to true while VdvTag hasn't been fetched disables lazy loading for VdvTag</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvTag
		{
			get { return _alreadyFetchedVdvTag;}
			set 
			{
				if(_alreadyFetchedVdvTag && !value)
				{
					this.VdvTag = null;
				}
				_alreadyFetchedVdvTag = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VdvTag is not found
		/// in the database. When set to true, VdvTag will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool VdvTagReturnsNewIfNotFound
		{
			get	{ return _vdvTagReturnsNewIfNotFound; }
			set { _vdvTagReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.VdvLayoutObjectEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
