﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PaymentOption'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PaymentOptionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AutoloadSettingCollection	_autoloadSettingsAlternative;
		private bool	_alwaysFetchAutoloadSettingsAlternative, _alreadyFetchedAutoloadSettingsAlternative;
		private VarioSL.Entities.CollectionClasses.AutoloadSettingCollection	_autoloadSettings;
		private bool	_alwaysFetchAutoloadSettings, _alreadyFetchedAutoloadSettings;
		private VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection	_backupAutoloadToPaymentOptions;
		private bool	_alwaysFetchBackupAutoloadToPaymentOptions, _alreadyFetchedBackupAutoloadToPaymentOptions;
		private VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection	_autoloadToPaymentOptions;
		private bool	_alwaysFetchAutoloadToPaymentOptions, _alreadyFetchedAutoloadToPaymentOptions;
		private VarioSL.Entities.CollectionClasses.InvoiceCollection	_invoices;
		private bool	_alwaysFetchInvoices, _alreadyFetchedInvoices;
		private VarioSL.Entities.CollectionClasses.OrderCollection	_orders;
		private bool	_alwaysFetchOrders, _alreadyFetchedOrders;
		private VarioSL.Entities.CollectionClasses.PaymentJournalCollection	_paymentJournals;
		private bool	_alwaysFetchPaymentJournals, _alreadyFetchedPaymentJournals;
		private VarioSL.Entities.CollectionClasses.RuleViolationCollection	_ruleViolations;
		private bool	_alwaysFetchRuleViolations, _alreadyFetchedRuleViolations;
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private BankConnectionDataEntity _bankConnectionData;
		private bool	_alwaysFetchBankConnectionData, _alreadyFetchedBankConnectionData, _bankConnectionDataReturnsNewIfNotFound;
		private PaymentOptionEntity _previousPaymentOption;
		private bool	_alwaysFetchPreviousPaymentOption, _alreadyFetchedPreviousPaymentOption, _previousPaymentOptionReturnsNewIfNotFound;
		private PaymentOptionEntity _prospectivePaymentOption;
		private bool	_alwaysFetchProspectivePaymentOption, _alreadyFetchedProspectivePaymentOption, _prospectivePaymentOptionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name AutoloadSettingsAlternative</summary>
			public static readonly string AutoloadSettingsAlternative = "AutoloadSettingsAlternative";
			/// <summary>Member name AutoloadSettings</summary>
			public static readonly string AutoloadSettings = "AutoloadSettings";
			/// <summary>Member name BackupAutoloadToPaymentOptions</summary>
			public static readonly string BackupAutoloadToPaymentOptions = "BackupAutoloadToPaymentOptions";
			/// <summary>Member name AutoloadToPaymentOptions</summary>
			public static readonly string AutoloadToPaymentOptions = "AutoloadToPaymentOptions";
			/// <summary>Member name Invoices</summary>
			public static readonly string Invoices = "Invoices";
			/// <summary>Member name Orders</summary>
			public static readonly string Orders = "Orders";
			/// <summary>Member name PaymentJournals</summary>
			public static readonly string PaymentJournals = "PaymentJournals";
			/// <summary>Member name RuleViolations</summary>
			public static readonly string RuleViolations = "RuleViolations";
			/// <summary>Member name BankConnectionData</summary>
			public static readonly string BankConnectionData = "BankConnectionData";
			/// <summary>Member name PreviousPaymentOption</summary>
			public static readonly string PreviousPaymentOption = "PreviousPaymentOption";
			/// <summary>Member name ProspectivePaymentOption</summary>
			public static readonly string ProspectivePaymentOption = "ProspectivePaymentOption";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PaymentOptionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PaymentOptionEntity() :base("PaymentOptionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="paymentOptionID">PK value for PaymentOption which data should be fetched into this PaymentOption object</param>
		public PaymentOptionEntity(System.Int64 paymentOptionID):base("PaymentOptionEntity")
		{
			InitClassFetch(paymentOptionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentOptionID">PK value for PaymentOption which data should be fetched into this PaymentOption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PaymentOptionEntity(System.Int64 paymentOptionID, IPrefetchPath prefetchPathToUse):base("PaymentOptionEntity")
		{
			InitClassFetch(paymentOptionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentOptionID">PK value for PaymentOption which data should be fetched into this PaymentOption object</param>
		/// <param name="validator">The custom validator object for this PaymentOptionEntity</param>
		public PaymentOptionEntity(System.Int64 paymentOptionID, IValidator validator):base("PaymentOptionEntity")
		{
			InitClassFetch(paymentOptionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentOptionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_autoloadSettingsAlternative = (VarioSL.Entities.CollectionClasses.AutoloadSettingCollection)info.GetValue("_autoloadSettingsAlternative", typeof(VarioSL.Entities.CollectionClasses.AutoloadSettingCollection));
			_alwaysFetchAutoloadSettingsAlternative = info.GetBoolean("_alwaysFetchAutoloadSettingsAlternative");
			_alreadyFetchedAutoloadSettingsAlternative = info.GetBoolean("_alreadyFetchedAutoloadSettingsAlternative");

			_autoloadSettings = (VarioSL.Entities.CollectionClasses.AutoloadSettingCollection)info.GetValue("_autoloadSettings", typeof(VarioSL.Entities.CollectionClasses.AutoloadSettingCollection));
			_alwaysFetchAutoloadSettings = info.GetBoolean("_alwaysFetchAutoloadSettings");
			_alreadyFetchedAutoloadSettings = info.GetBoolean("_alreadyFetchedAutoloadSettings");

			_backupAutoloadToPaymentOptions = (VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection)info.GetValue("_backupAutoloadToPaymentOptions", typeof(VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection));
			_alwaysFetchBackupAutoloadToPaymentOptions = info.GetBoolean("_alwaysFetchBackupAutoloadToPaymentOptions");
			_alreadyFetchedBackupAutoloadToPaymentOptions = info.GetBoolean("_alreadyFetchedBackupAutoloadToPaymentOptions");

			_autoloadToPaymentOptions = (VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection)info.GetValue("_autoloadToPaymentOptions", typeof(VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection));
			_alwaysFetchAutoloadToPaymentOptions = info.GetBoolean("_alwaysFetchAutoloadToPaymentOptions");
			_alreadyFetchedAutoloadToPaymentOptions = info.GetBoolean("_alreadyFetchedAutoloadToPaymentOptions");

			_invoices = (VarioSL.Entities.CollectionClasses.InvoiceCollection)info.GetValue("_invoices", typeof(VarioSL.Entities.CollectionClasses.InvoiceCollection));
			_alwaysFetchInvoices = info.GetBoolean("_alwaysFetchInvoices");
			_alreadyFetchedInvoices = info.GetBoolean("_alreadyFetchedInvoices");

			_orders = (VarioSL.Entities.CollectionClasses.OrderCollection)info.GetValue("_orders", typeof(VarioSL.Entities.CollectionClasses.OrderCollection));
			_alwaysFetchOrders = info.GetBoolean("_alwaysFetchOrders");
			_alreadyFetchedOrders = info.GetBoolean("_alreadyFetchedOrders");

			_paymentJournals = (VarioSL.Entities.CollectionClasses.PaymentJournalCollection)info.GetValue("_paymentJournals", typeof(VarioSL.Entities.CollectionClasses.PaymentJournalCollection));
			_alwaysFetchPaymentJournals = info.GetBoolean("_alwaysFetchPaymentJournals");
			_alreadyFetchedPaymentJournals = info.GetBoolean("_alreadyFetchedPaymentJournals");

			_ruleViolations = (VarioSL.Entities.CollectionClasses.RuleViolationCollection)info.GetValue("_ruleViolations", typeof(VarioSL.Entities.CollectionClasses.RuleViolationCollection));
			_alwaysFetchRuleViolations = info.GetBoolean("_alwaysFetchRuleViolations");
			_alreadyFetchedRuleViolations = info.GetBoolean("_alreadyFetchedRuleViolations");
			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");
			_bankConnectionData = (BankConnectionDataEntity)info.GetValue("_bankConnectionData", typeof(BankConnectionDataEntity));
			if(_bankConnectionData!=null)
			{
				_bankConnectionData.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_bankConnectionDataReturnsNewIfNotFound = info.GetBoolean("_bankConnectionDataReturnsNewIfNotFound");
			_alwaysFetchBankConnectionData = info.GetBoolean("_alwaysFetchBankConnectionData");
			_alreadyFetchedBankConnectionData = info.GetBoolean("_alreadyFetchedBankConnectionData");

			_previousPaymentOption = (PaymentOptionEntity)info.GetValue("_previousPaymentOption", typeof(PaymentOptionEntity));
			if(_previousPaymentOption!=null)
			{
				_previousPaymentOption.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_previousPaymentOptionReturnsNewIfNotFound = info.GetBoolean("_previousPaymentOptionReturnsNewIfNotFound");
			_alwaysFetchPreviousPaymentOption = info.GetBoolean("_alwaysFetchPreviousPaymentOption");
			_alreadyFetchedPreviousPaymentOption = info.GetBoolean("_alreadyFetchedPreviousPaymentOption");

			_prospectivePaymentOption = (PaymentOptionEntity)info.GetValue("_prospectivePaymentOption", typeof(PaymentOptionEntity));
			if(_prospectivePaymentOption!=null)
			{
				_prospectivePaymentOption.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_prospectivePaymentOptionReturnsNewIfNotFound = info.GetBoolean("_prospectivePaymentOptionReturnsNewIfNotFound");
			_alwaysFetchProspectivePaymentOption = info.GetBoolean("_alwaysFetchProspectivePaymentOption");
			_alreadyFetchedProspectivePaymentOption = info.GetBoolean("_alreadyFetchedProspectivePaymentOption");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PaymentOptionFieldIndex)fieldIndex)
			{
				case PaymentOptionFieldIndex.BankConnectionDataID:
					DesetupSyncBankConnectionData(true, false);
					_alreadyFetchedBankConnectionData = false;
					break;
				case PaymentOptionFieldIndex.CardID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				case PaymentOptionFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case PaymentOptionFieldIndex.ProspectivePaymentOptionID:
					DesetupSyncPreviousPaymentOption(true, false);
					_alreadyFetchedPreviousPaymentOption = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAutoloadSettingsAlternative = (_autoloadSettingsAlternative.Count > 0);
			_alreadyFetchedAutoloadSettings = (_autoloadSettings.Count > 0);
			_alreadyFetchedBackupAutoloadToPaymentOptions = (_backupAutoloadToPaymentOptions.Count > 0);
			_alreadyFetchedAutoloadToPaymentOptions = (_autoloadToPaymentOptions.Count > 0);
			_alreadyFetchedInvoices = (_invoices.Count > 0);
			_alreadyFetchedOrders = (_orders.Count > 0);
			_alreadyFetchedPaymentJournals = (_paymentJournals.Count > 0);
			_alreadyFetchedRuleViolations = (_ruleViolations.Count > 0);
			_alreadyFetchedCard = (_card != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedBankConnectionData = (_bankConnectionData != null);
			_alreadyFetchedPreviousPaymentOption = (_previousPaymentOption != null);
			_alreadyFetchedProspectivePaymentOption = (_prospectivePaymentOption != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Card":
					toReturn.Add(Relations.CardEntityUsingCardID);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "AutoloadSettingsAlternative":
					toReturn.Add(Relations.AutoloadSettingEntityUsingAlternativePaymentOptionId);
					break;
				case "AutoloadSettings":
					toReturn.Add(Relations.AutoloadSettingEntityUsingPaymentOptionID);
					break;
				case "BackupAutoloadToPaymentOptions":
					toReturn.Add(Relations.AutoloadToPaymentOptionEntityUsingBackupPaymentOptionID);
					break;
				case "AutoloadToPaymentOptions":
					toReturn.Add(Relations.AutoloadToPaymentOptionEntityUsingPaymentOptionID);
					break;
				case "Invoices":
					toReturn.Add(Relations.InvoiceEntityUsingUsedPaymentOptionID);
					break;
				case "Orders":
					toReturn.Add(Relations.OrderEntityUsingPaymentOptionID);
					break;
				case "PaymentJournals":
					toReturn.Add(Relations.PaymentJournalEntityUsingPaymentOptionID);
					break;
				case "RuleViolations":
					toReturn.Add(Relations.RuleViolationEntityUsingPaymentOptionID);
					break;
				case "BankConnectionData":
					toReturn.Add(Relations.BankConnectionDataEntityUsingBankConnectionDataID);
					break;
				case "PreviousPaymentOption":
					toReturn.Add(Relations.PaymentOptionEntityUsingPaymentOptionID);
					break;
				case "ProspectivePaymentOption":
					toReturn.Add(Relations.PaymentOptionEntityUsingProspectivePaymentOptionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_autoloadSettingsAlternative", (!this.MarkedForDeletion?_autoloadSettingsAlternative:null));
			info.AddValue("_alwaysFetchAutoloadSettingsAlternative", _alwaysFetchAutoloadSettingsAlternative);
			info.AddValue("_alreadyFetchedAutoloadSettingsAlternative", _alreadyFetchedAutoloadSettingsAlternative);
			info.AddValue("_autoloadSettings", (!this.MarkedForDeletion?_autoloadSettings:null));
			info.AddValue("_alwaysFetchAutoloadSettings", _alwaysFetchAutoloadSettings);
			info.AddValue("_alreadyFetchedAutoloadSettings", _alreadyFetchedAutoloadSettings);
			info.AddValue("_backupAutoloadToPaymentOptions", (!this.MarkedForDeletion?_backupAutoloadToPaymentOptions:null));
			info.AddValue("_alwaysFetchBackupAutoloadToPaymentOptions", _alwaysFetchBackupAutoloadToPaymentOptions);
			info.AddValue("_alreadyFetchedBackupAutoloadToPaymentOptions", _alreadyFetchedBackupAutoloadToPaymentOptions);
			info.AddValue("_autoloadToPaymentOptions", (!this.MarkedForDeletion?_autoloadToPaymentOptions:null));
			info.AddValue("_alwaysFetchAutoloadToPaymentOptions", _alwaysFetchAutoloadToPaymentOptions);
			info.AddValue("_alreadyFetchedAutoloadToPaymentOptions", _alreadyFetchedAutoloadToPaymentOptions);
			info.AddValue("_invoices", (!this.MarkedForDeletion?_invoices:null));
			info.AddValue("_alwaysFetchInvoices", _alwaysFetchInvoices);
			info.AddValue("_alreadyFetchedInvoices", _alreadyFetchedInvoices);
			info.AddValue("_orders", (!this.MarkedForDeletion?_orders:null));
			info.AddValue("_alwaysFetchOrders", _alwaysFetchOrders);
			info.AddValue("_alreadyFetchedOrders", _alreadyFetchedOrders);
			info.AddValue("_paymentJournals", (!this.MarkedForDeletion?_paymentJournals:null));
			info.AddValue("_alwaysFetchPaymentJournals", _alwaysFetchPaymentJournals);
			info.AddValue("_alreadyFetchedPaymentJournals", _alreadyFetchedPaymentJournals);
			info.AddValue("_ruleViolations", (!this.MarkedForDeletion?_ruleViolations:null));
			info.AddValue("_alwaysFetchRuleViolations", _alwaysFetchRuleViolations);
			info.AddValue("_alreadyFetchedRuleViolations", _alreadyFetchedRuleViolations);
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);

			info.AddValue("_bankConnectionData", (!this.MarkedForDeletion?_bankConnectionData:null));
			info.AddValue("_bankConnectionDataReturnsNewIfNotFound", _bankConnectionDataReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBankConnectionData", _alwaysFetchBankConnectionData);
			info.AddValue("_alreadyFetchedBankConnectionData", _alreadyFetchedBankConnectionData);

			info.AddValue("_previousPaymentOption", (!this.MarkedForDeletion?_previousPaymentOption:null));
			info.AddValue("_previousPaymentOptionReturnsNewIfNotFound", _previousPaymentOptionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPreviousPaymentOption", _alwaysFetchPreviousPaymentOption);
			info.AddValue("_alreadyFetchedPreviousPaymentOption", _alreadyFetchedPreviousPaymentOption);

			info.AddValue("_prospectivePaymentOption", (!this.MarkedForDeletion?_prospectivePaymentOption:null));
			info.AddValue("_prospectivePaymentOptionReturnsNewIfNotFound", _prospectivePaymentOptionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProspectivePaymentOption", _alwaysFetchProspectivePaymentOption);
			info.AddValue("_alreadyFetchedProspectivePaymentOption", _alreadyFetchedProspectivePaymentOption);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "AutoloadSettingsAlternative":
					_alreadyFetchedAutoloadSettingsAlternative = true;
					if(entity!=null)
					{
						this.AutoloadSettingsAlternative.Add((AutoloadSettingEntity)entity);
					}
					break;
				case "AutoloadSettings":
					_alreadyFetchedAutoloadSettings = true;
					if(entity!=null)
					{
						this.AutoloadSettings.Add((AutoloadSettingEntity)entity);
					}
					break;
				case "BackupAutoloadToPaymentOptions":
					_alreadyFetchedBackupAutoloadToPaymentOptions = true;
					if(entity!=null)
					{
						this.BackupAutoloadToPaymentOptions.Add((AutoloadToPaymentOptionEntity)entity);
					}
					break;
				case "AutoloadToPaymentOptions":
					_alreadyFetchedAutoloadToPaymentOptions = true;
					if(entity!=null)
					{
						this.AutoloadToPaymentOptions.Add((AutoloadToPaymentOptionEntity)entity);
					}
					break;
				case "Invoices":
					_alreadyFetchedInvoices = true;
					if(entity!=null)
					{
						this.Invoices.Add((InvoiceEntity)entity);
					}
					break;
				case "Orders":
					_alreadyFetchedOrders = true;
					if(entity!=null)
					{
						this.Orders.Add((OrderEntity)entity);
					}
					break;
				case "PaymentJournals":
					_alreadyFetchedPaymentJournals = true;
					if(entity!=null)
					{
						this.PaymentJournals.Add((PaymentJournalEntity)entity);
					}
					break;
				case "RuleViolations":
					_alreadyFetchedRuleViolations = true;
					if(entity!=null)
					{
						this.RuleViolations.Add((RuleViolationEntity)entity);
					}
					break;
				case "BankConnectionData":
					_alreadyFetchedBankConnectionData = true;
					this.BankConnectionData = (BankConnectionDataEntity)entity;
					break;
				case "PreviousPaymentOption":
					_alreadyFetchedPreviousPaymentOption = true;
					this.PreviousPaymentOption = (PaymentOptionEntity)entity;
					break;
				case "ProspectivePaymentOption":
					_alreadyFetchedProspectivePaymentOption = true;
					this.ProspectivePaymentOption = (PaymentOptionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "AutoloadSettingsAlternative":
					_autoloadSettingsAlternative.Add((AutoloadSettingEntity)relatedEntity);
					break;
				case "AutoloadSettings":
					_autoloadSettings.Add((AutoloadSettingEntity)relatedEntity);
					break;
				case "BackupAutoloadToPaymentOptions":
					_backupAutoloadToPaymentOptions.Add((AutoloadToPaymentOptionEntity)relatedEntity);
					break;
				case "AutoloadToPaymentOptions":
					_autoloadToPaymentOptions.Add((AutoloadToPaymentOptionEntity)relatedEntity);
					break;
				case "Invoices":
					_invoices.Add((InvoiceEntity)relatedEntity);
					break;
				case "Orders":
					_orders.Add((OrderEntity)relatedEntity);
					break;
				case "PaymentJournals":
					_paymentJournals.Add((PaymentJournalEntity)relatedEntity);
					break;
				case "RuleViolations":
					_ruleViolations.Add((RuleViolationEntity)relatedEntity);
					break;
				case "BankConnectionData":
					SetupSyncBankConnectionData(relatedEntity);
					break;
				case "PreviousPaymentOption":
					SetupSyncPreviousPaymentOption(relatedEntity);
					break;
				case "ProspectivePaymentOption":
					SetupSyncProspectivePaymentOption(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Card":
					DesetupSyncCard(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "AutoloadSettingsAlternative":
					this.PerformRelatedEntityRemoval(_autoloadSettingsAlternative, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AutoloadSettings":
					this.PerformRelatedEntityRemoval(_autoloadSettings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BackupAutoloadToPaymentOptions":
					this.PerformRelatedEntityRemoval(_backupAutoloadToPaymentOptions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AutoloadToPaymentOptions":
					this.PerformRelatedEntityRemoval(_autoloadToPaymentOptions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Invoices":
					this.PerformRelatedEntityRemoval(_invoices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Orders":
					this.PerformRelatedEntityRemoval(_orders, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentJournals":
					this.PerformRelatedEntityRemoval(_paymentJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RuleViolations":
					this.PerformRelatedEntityRemoval(_ruleViolations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BankConnectionData":
					DesetupSyncBankConnectionData(false, true);
					break;
				case "PreviousPaymentOption":
					DesetupSyncPreviousPaymentOption(false, true);
					break;
				case "ProspectivePaymentOption":
					DesetupSyncProspectivePaymentOption(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_prospectivePaymentOption!=null)
			{
				toReturn.Add(_prospectivePaymentOption);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_bankConnectionData!=null)
			{
				toReturn.Add(_bankConnectionData);
			}
			if(_previousPaymentOption!=null)
			{
				toReturn.Add(_previousPaymentOption);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_autoloadSettingsAlternative);
			toReturn.Add(_autoloadSettings);
			toReturn.Add(_backupAutoloadToPaymentOptions);
			toReturn.Add(_autoloadToPaymentOptions);
			toReturn.Add(_invoices);
			toReturn.Add(_orders);
			toReturn.Add(_paymentJournals);
			toReturn.Add(_ruleViolations);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="bankConnectionDataID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCBankConnectionDataID(Nullable<System.Int64> bankConnectionDataID)
		{
			return FetchUsingUCBankConnectionDataID( bankConnectionDataID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="bankConnectionDataID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCBankConnectionDataID(Nullable<System.Int64> bankConnectionDataID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCBankConnectionDataID( bankConnectionDataID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="bankConnectionDataID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCBankConnectionDataID(Nullable<System.Int64> bankConnectionDataID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCBankConnectionDataID( bankConnectionDataID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="bankConnectionDataID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCBankConnectionDataID(Nullable<System.Int64> bankConnectionDataID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((PaymentOptionDAO)CreateDAOInstance()).FetchPaymentOptionUsingUCBankConnectionDataID(this, this.Transaction, bankConnectionDataID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="prospectivePaymentOptionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCProspectivePaymentOptionID(Nullable<System.Int64> prospectivePaymentOptionID)
		{
			return FetchUsingUCProspectivePaymentOptionID( prospectivePaymentOptionID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="prospectivePaymentOptionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCProspectivePaymentOptionID(Nullable<System.Int64> prospectivePaymentOptionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCProspectivePaymentOptionID( prospectivePaymentOptionID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="prospectivePaymentOptionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCProspectivePaymentOptionID(Nullable<System.Int64> prospectivePaymentOptionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCProspectivePaymentOptionID( prospectivePaymentOptionID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="prospectivePaymentOptionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCProspectivePaymentOptionID(Nullable<System.Int64> prospectivePaymentOptionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((PaymentOptionDAO)CreateDAOInstance()).FetchPaymentOptionUsingUCProspectivePaymentOptionID(this, this.Transaction, prospectivePaymentOptionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentOptionID">PK value for PaymentOption which data should be fetched into this PaymentOption object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentOptionID)
		{
			return FetchUsingPK(paymentOptionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentOptionID">PK value for PaymentOption which data should be fetched into this PaymentOption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentOptionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(paymentOptionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentOptionID">PK value for PaymentOption which data should be fetched into this PaymentOption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentOptionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(paymentOptionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentOptionID">PK value for PaymentOption which data should be fetched into this PaymentOption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentOptionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(paymentOptionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PaymentOptionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PaymentOptionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AutoloadSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadSettingCollection GetMultiAutoloadSettingsAlternative(bool forceFetch)
		{
			return GetMultiAutoloadSettingsAlternative(forceFetch, _autoloadSettingsAlternative.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AutoloadSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadSettingCollection GetMultiAutoloadSettingsAlternative(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAutoloadSettingsAlternative(forceFetch, _autoloadSettingsAlternative.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadSettingCollection GetMultiAutoloadSettingsAlternative(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAutoloadSettingsAlternative(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AutoloadSettingCollection GetMultiAutoloadSettingsAlternative(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAutoloadSettingsAlternative || forceFetch || _alwaysFetchAutoloadSettingsAlternative) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_autoloadSettingsAlternative);
				_autoloadSettingsAlternative.SuppressClearInGetMulti=!forceFetch;
				_autoloadSettingsAlternative.EntityFactoryToUse = entityFactoryToUse;
				_autoloadSettingsAlternative.GetMultiManyToOne(this, null, null, filter);
				_autoloadSettingsAlternative.SuppressClearInGetMulti=false;
				_alreadyFetchedAutoloadSettingsAlternative = true;
			}
			return _autoloadSettingsAlternative;
		}

		/// <summary> Sets the collection parameters for the collection for 'AutoloadSettingsAlternative'. These settings will be taken into account
		/// when the property AutoloadSettingsAlternative is requested or GetMultiAutoloadSettingsAlternative is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAutoloadSettingsAlternative(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_autoloadSettingsAlternative.SortClauses=sortClauses;
			_autoloadSettingsAlternative.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AutoloadSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadSettingCollection GetMultiAutoloadSettings(bool forceFetch)
		{
			return GetMultiAutoloadSettings(forceFetch, _autoloadSettings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AutoloadSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadSettingCollection GetMultiAutoloadSettings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAutoloadSettings(forceFetch, _autoloadSettings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadSettingCollection GetMultiAutoloadSettings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAutoloadSettings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AutoloadSettingCollection GetMultiAutoloadSettings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAutoloadSettings || forceFetch || _alwaysFetchAutoloadSettings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_autoloadSettings);
				_autoloadSettings.SuppressClearInGetMulti=!forceFetch;
				_autoloadSettings.EntityFactoryToUse = entityFactoryToUse;
				_autoloadSettings.GetMultiManyToOne(null, this, null, filter);
				_autoloadSettings.SuppressClearInGetMulti=false;
				_alreadyFetchedAutoloadSettings = true;
			}
			return _autoloadSettings;
		}

		/// <summary> Sets the collection parameters for the collection for 'AutoloadSettings'. These settings will be taken into account
		/// when the property AutoloadSettings is requested or GetMultiAutoloadSettings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAutoloadSettings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_autoloadSettings.SortClauses=sortClauses;
			_autoloadSettings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AutoloadToPaymentOptionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection GetMultiBackupAutoloadToPaymentOptions(bool forceFetch)
		{
			return GetMultiBackupAutoloadToPaymentOptions(forceFetch, _backupAutoloadToPaymentOptions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AutoloadToPaymentOptionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection GetMultiBackupAutoloadToPaymentOptions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBackupAutoloadToPaymentOptions(forceFetch, _backupAutoloadToPaymentOptions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection GetMultiBackupAutoloadToPaymentOptions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBackupAutoloadToPaymentOptions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection GetMultiBackupAutoloadToPaymentOptions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBackupAutoloadToPaymentOptions || forceFetch || _alwaysFetchBackupAutoloadToPaymentOptions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_backupAutoloadToPaymentOptions);
				_backupAutoloadToPaymentOptions.SuppressClearInGetMulti=!forceFetch;
				_backupAutoloadToPaymentOptions.EntityFactoryToUse = entityFactoryToUse;
				_backupAutoloadToPaymentOptions.GetMultiManyToOne(null, this, null, filter);
				_backupAutoloadToPaymentOptions.SuppressClearInGetMulti=false;
				_alreadyFetchedBackupAutoloadToPaymentOptions = true;
			}
			return _backupAutoloadToPaymentOptions;
		}

		/// <summary> Sets the collection parameters for the collection for 'BackupAutoloadToPaymentOptions'. These settings will be taken into account
		/// when the property BackupAutoloadToPaymentOptions is requested or GetMultiBackupAutoloadToPaymentOptions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBackupAutoloadToPaymentOptions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_backupAutoloadToPaymentOptions.SortClauses=sortClauses;
			_backupAutoloadToPaymentOptions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AutoloadToPaymentOptionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection GetMultiAutoloadToPaymentOptions(bool forceFetch)
		{
			return GetMultiAutoloadToPaymentOptions(forceFetch, _autoloadToPaymentOptions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AutoloadToPaymentOptionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection GetMultiAutoloadToPaymentOptions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAutoloadToPaymentOptions(forceFetch, _autoloadToPaymentOptions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection GetMultiAutoloadToPaymentOptions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAutoloadToPaymentOptions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection GetMultiAutoloadToPaymentOptions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAutoloadToPaymentOptions || forceFetch || _alwaysFetchAutoloadToPaymentOptions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_autoloadToPaymentOptions);
				_autoloadToPaymentOptions.SuppressClearInGetMulti=!forceFetch;
				_autoloadToPaymentOptions.EntityFactoryToUse = entityFactoryToUse;
				_autoloadToPaymentOptions.GetMultiManyToOne(null, null, this, filter);
				_autoloadToPaymentOptions.SuppressClearInGetMulti=false;
				_alreadyFetchedAutoloadToPaymentOptions = true;
			}
			return _autoloadToPaymentOptions;
		}

		/// <summary> Sets the collection parameters for the collection for 'AutoloadToPaymentOptions'. These settings will be taken into account
		/// when the property AutoloadToPaymentOptions is requested or GetMultiAutoloadToPaymentOptions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAutoloadToPaymentOptions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_autoloadToPaymentOptions.SortClauses=sortClauses;
			_autoloadToPaymentOptions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch)
		{
			return GetMultiInvoices(forceFetch, _invoices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiInvoices(forceFetch, _invoices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiInvoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedInvoices || forceFetch || _alwaysFetchInvoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_invoices);
				_invoices.SuppressClearInGetMulti=!forceFetch;
				_invoices.EntityFactoryToUse = entityFactoryToUse;
				_invoices.GetMultiManyToOne(null, null, null, null, this, null, filter);
				_invoices.SuppressClearInGetMulti=false;
				_alreadyFetchedInvoices = true;
			}
			return _invoices;
		}

		/// <summary> Sets the collection parameters for the collection for 'Invoices'. These settings will be taken into account
		/// when the property Invoices is requested or GetMultiInvoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersInvoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_invoices.SortClauses=sortClauses;
			_invoices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch)
		{
			return GetMultiOrders(forceFetch, _orders.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrders(forceFetch, _orders.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrders(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrders || forceFetch || _alwaysFetchOrders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orders);
				_orders.SuppressClearInGetMulti=!forceFetch;
				_orders.EntityFactoryToUse = entityFactoryToUse;
				_orders.GetMultiManyToOne(null, null, null, null, this, null, filter);
				_orders.SuppressClearInGetMulti=false;
				_alreadyFetchedOrders = true;
			}
			return _orders;
		}

		/// <summary> Sets the collection parameters for the collection for 'Orders'. These settings will be taken into account
		/// when the property Orders is requested or GetMultiOrders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orders.SortClauses=sortClauses;
			_orders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch)
		{
			return GetMultiPaymentJournals(forceFetch, _paymentJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentJournals(forceFetch, _paymentJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentJournals || forceFetch || _alwaysFetchPaymentJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentJournals);
				_paymentJournals.SuppressClearInGetMulti=!forceFetch;
				_paymentJournals.EntityFactoryToUse = entityFactoryToUse;
				_paymentJournals.GetMultiManyToOne(null, null, this, null, null, null, filter);
				_paymentJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentJournals = true;
			}
			return _paymentJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentJournals'. These settings will be taken into account
		/// when the property PaymentJournals is requested or GetMultiPaymentJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentJournals.SortClauses=sortClauses;
			_paymentJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationCollection GetMultiRuleViolations(bool forceFetch)
		{
			return GetMultiRuleViolations(forceFetch, _ruleViolations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationCollection GetMultiRuleViolations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleViolations(forceFetch, _ruleViolations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationCollection GetMultiRuleViolations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleViolations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationCollection GetMultiRuleViolations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleViolations || forceFetch || _alwaysFetchRuleViolations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleViolations);
				_ruleViolations.SuppressClearInGetMulti=!forceFetch;
				_ruleViolations.EntityFactoryToUse = entityFactoryToUse;
				_ruleViolations.GetMultiManyToOne(null, this, null, filter);
				_ruleViolations.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleViolations = true;
			}
			return _ruleViolations;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleViolations'. These settings will be taken into account
		/// when the property RuleViolations is requested or GetMultiRuleViolations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleViolations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleViolations.SortClauses=sortClauses;
			_ruleViolations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID);
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}

		/// <summary> Retrieves the related entity of type 'BankConnectionDataEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'BankConnectionDataEntity' which is related to this entity.</returns>
		public BankConnectionDataEntity GetSingleBankConnectionData()
		{
			return GetSingleBankConnectionData(false);
		}
		
		/// <summary> Retrieves the related entity of type 'BankConnectionDataEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BankConnectionDataEntity' which is related to this entity.</returns>
		public virtual BankConnectionDataEntity GetSingleBankConnectionData(bool forceFetch)
		{
			if( ( !_alreadyFetchedBankConnectionData || forceFetch || _alwaysFetchBankConnectionData) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BankConnectionDataEntityUsingBankConnectionDataID);
				BankConnectionDataEntity newEntity = new BankConnectionDataEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BankConnectionDataID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BankConnectionDataEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_bankConnectionDataReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BankConnectionData = newEntity;
				_alreadyFetchedBankConnectionData = fetchResult;
			}
			return _bankConnectionData;
		}

		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public PaymentOptionEntity GetSinglePreviousPaymentOption()
		{
			return GetSinglePreviousPaymentOption(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public virtual PaymentOptionEntity GetSinglePreviousPaymentOption(bool forceFetch)
		{
			if( ( !_alreadyFetchedPreviousPaymentOption || forceFetch || _alwaysFetchPreviousPaymentOption) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentOptionEntityUsingProspectivePaymentOptionID);
				PaymentOptionEntity newEntity = new PaymentOptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProspectivePaymentOptionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentOptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_previousPaymentOptionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PreviousPaymentOption = newEntity;
				_alreadyFetchedPreviousPaymentOption = fetchResult;
			}
			return _previousPaymentOption;
		}

		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public PaymentOptionEntity GetSingleProspectivePaymentOption()
		{
			return GetSingleProspectivePaymentOption(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public virtual PaymentOptionEntity GetSingleProspectivePaymentOption(bool forceFetch)
		{
			if( ( !_alreadyFetchedProspectivePaymentOption || forceFetch || _alwaysFetchProspectivePaymentOption) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentOptionEntityUsingProspectivePaymentOptionID);
				PaymentOptionEntity newEntity = new PaymentOptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCProspectivePaymentOptionID(this.PaymentOptionID);
				}
				if(fetchResult)
				{
					newEntity = (PaymentOptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_prospectivePaymentOptionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProspectivePaymentOption = newEntity;
				_alreadyFetchedProspectivePaymentOption = fetchResult;
			}
			return _prospectivePaymentOption;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Card", _card);
			toReturn.Add("Contract", _contract);
			toReturn.Add("AutoloadSettingsAlternative", _autoloadSettingsAlternative);
			toReturn.Add("AutoloadSettings", _autoloadSettings);
			toReturn.Add("BackupAutoloadToPaymentOptions", _backupAutoloadToPaymentOptions);
			toReturn.Add("AutoloadToPaymentOptions", _autoloadToPaymentOptions);
			toReturn.Add("Invoices", _invoices);
			toReturn.Add("Orders", _orders);
			toReturn.Add("PaymentJournals", _paymentJournals);
			toReturn.Add("RuleViolations", _ruleViolations);
			toReturn.Add("BankConnectionData", _bankConnectionData);
			toReturn.Add("PreviousPaymentOption", _previousPaymentOption);
			toReturn.Add("ProspectivePaymentOption", _prospectivePaymentOption);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="paymentOptionID">PK value for PaymentOption which data should be fetched into this PaymentOption object</param>
		/// <param name="validator">The validator object for this PaymentOptionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 paymentOptionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(paymentOptionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_autoloadSettingsAlternative = new VarioSL.Entities.CollectionClasses.AutoloadSettingCollection();
			_autoloadSettingsAlternative.SetContainingEntityInfo(this, "AltPaymentOption");

			_autoloadSettings = new VarioSL.Entities.CollectionClasses.AutoloadSettingCollection();
			_autoloadSettings.SetContainingEntityInfo(this, "PaymentOption");

			_backupAutoloadToPaymentOptions = new VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection();
			_backupAutoloadToPaymentOptions.SetContainingEntityInfo(this, "BackupPaymentOption");

			_autoloadToPaymentOptions = new VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection();
			_autoloadToPaymentOptions.SetContainingEntityInfo(this, "PaymentOption");

			_invoices = new VarioSL.Entities.CollectionClasses.InvoiceCollection();
			_invoices.SetContainingEntityInfo(this, "PaymentOption");

			_orders = new VarioSL.Entities.CollectionClasses.OrderCollection();
			_orders.SetContainingEntityInfo(this, "PaymentOption");

			_paymentJournals = new VarioSL.Entities.CollectionClasses.PaymentJournalCollection();
			_paymentJournals.SetContainingEntityInfo(this, "PaymentOption");

			_ruleViolations = new VarioSL.Entities.CollectionClasses.RuleViolationCollection();
			_ruleViolations.SetContainingEntityInfo(this, "PaymentOption");
			_cardReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_bankConnectionDataReturnsNewIfNotFound = false;
			_previousPaymentOptionReturnsNewIfNotFound = false;
			_prospectivePaymentOptionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankConnectionDataID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankingServiceToken", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockingComment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockingDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockingReason", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditCardNetwork", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Expiry", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsBlocked", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsRetired", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTemporary", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentOptionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Priority", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProspectivePaymentOptionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticPaymentOptionRelations.CardEntityUsingCardIDStatic, true, signalRelatedEntity, "PaymentOptions", resetFKFields, new int[] { (int)PaymentOptionFieldIndex.CardID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticPaymentOptionRelations.CardEntityUsingCardIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticPaymentOptionRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "PaymentOptions", resetFKFields, new int[] { (int)PaymentOptionFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticPaymentOptionRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _bankConnectionData</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBankConnectionData(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _bankConnectionData, new PropertyChangedEventHandler( OnBankConnectionDataPropertyChanged ), "BankConnectionData", VarioSL.Entities.RelationClasses.StaticPaymentOptionRelations.BankConnectionDataEntityUsingBankConnectionDataIDStatic, true, signalRelatedEntity, "PaymentOption", resetFKFields, new int[] { (int)PaymentOptionFieldIndex.BankConnectionDataID } );
			_bankConnectionData = null;
		}
	
		/// <summary> setups the sync logic for member _bankConnectionData</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBankConnectionData(IEntityCore relatedEntity)
		{
			if(_bankConnectionData!=relatedEntity)
			{
				DesetupSyncBankConnectionData(true, true);
				_bankConnectionData = (BankConnectionDataEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _bankConnectionData, new PropertyChangedEventHandler( OnBankConnectionDataPropertyChanged ), "BankConnectionData", VarioSL.Entities.RelationClasses.StaticPaymentOptionRelations.BankConnectionDataEntityUsingBankConnectionDataIDStatic, true, ref _alreadyFetchedBankConnectionData, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBankConnectionDataPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _previousPaymentOption</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPreviousPaymentOption(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _previousPaymentOption, new PropertyChangedEventHandler( OnPreviousPaymentOptionPropertyChanged ), "PreviousPaymentOption", VarioSL.Entities.RelationClasses.StaticPaymentOptionRelations.PaymentOptionEntityUsingProspectivePaymentOptionIDStatic, true, signalRelatedEntity, "ProspectivePaymentOption", resetFKFields, new int[] { (int)PaymentOptionFieldIndex.ProspectivePaymentOptionID } );
			_previousPaymentOption = null;
		}
	
		/// <summary> setups the sync logic for member _previousPaymentOption</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPreviousPaymentOption(IEntityCore relatedEntity)
		{
			if(_previousPaymentOption!=relatedEntity)
			{
				DesetupSyncPreviousPaymentOption(true, true);
				_previousPaymentOption = (PaymentOptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _previousPaymentOption, new PropertyChangedEventHandler( OnPreviousPaymentOptionPropertyChanged ), "PreviousPaymentOption", VarioSL.Entities.RelationClasses.StaticPaymentOptionRelations.PaymentOptionEntityUsingProspectivePaymentOptionIDStatic, true, ref _alreadyFetchedPreviousPaymentOption, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPreviousPaymentOptionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _prospectivePaymentOption</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProspectivePaymentOption(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _prospectivePaymentOption, new PropertyChangedEventHandler( OnProspectivePaymentOptionPropertyChanged ), "ProspectivePaymentOption", VarioSL.Entities.RelationClasses.StaticPaymentOptionRelations.PaymentOptionEntityUsingProspectivePaymentOptionIDStatic, false, signalRelatedEntity, "PreviousPaymentOption", false, new int[] { (int)PaymentOptionFieldIndex.PaymentOptionID } );
			_prospectivePaymentOption = null;
		}
	
		/// <summary> setups the sync logic for member _prospectivePaymentOption</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProspectivePaymentOption(IEntityCore relatedEntity)
		{
			if(_prospectivePaymentOption!=relatedEntity)
			{
				DesetupSyncProspectivePaymentOption(true, true);
				_prospectivePaymentOption = (PaymentOptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _prospectivePaymentOption, new PropertyChangedEventHandler( OnProspectivePaymentOptionPropertyChanged ), "ProspectivePaymentOption", VarioSL.Entities.RelationClasses.StaticPaymentOptionRelations.PaymentOptionEntityUsingProspectivePaymentOptionIDStatic, false, ref _alreadyFetchedProspectivePaymentOption, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProspectivePaymentOptionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="paymentOptionID">PK value for PaymentOption which data should be fetched into this PaymentOption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 paymentOptionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PaymentOptionFieldIndex.PaymentOptionID].ForcedCurrentValueWrite(paymentOptionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePaymentOptionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PaymentOptionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PaymentOptionRelations Relations
		{
			get	{ return new PaymentOptionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AutoloadSetting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutoloadSettingsAlternative
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AutoloadSettingCollection(), (IEntityRelation)GetRelationsForField("AutoloadSettingsAlternative")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.AutoloadSettingEntity, 0, null, null, null, "AutoloadSettingsAlternative", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AutoloadSetting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutoloadSettings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AutoloadSettingCollection(), (IEntityRelation)GetRelationsForField("AutoloadSettings")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.AutoloadSettingEntity, 0, null, null, null, "AutoloadSettings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AutoloadToPaymentOption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBackupAutoloadToPaymentOptions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection(), (IEntityRelation)GetRelationsForField("BackupAutoloadToPaymentOptions")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.AutoloadToPaymentOptionEntity, 0, null, null, null, "BackupAutoloadToPaymentOptions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AutoloadToPaymentOption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutoloadToPaymentOptions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection(), (IEntityRelation)GetRelationsForField("AutoloadToPaymentOptions")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.AutoloadToPaymentOptionEntity, 0, null, null, null, "AutoloadToPaymentOptions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoiceCollection(), (IEntityRelation)GetRelationsForField("Invoices")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.InvoiceEntity, 0, null, null, null, "Invoices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrders
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("Orders")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.OrderEntity, 0, null, null, null, "Orders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentJournalCollection(), (IEntityRelation)GetRelationsForField("PaymentJournals")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.PaymentJournalEntity, 0, null, null, null, "PaymentJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleViolation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleViolations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleViolationCollection(), (IEntityRelation)GetRelationsForField("RuleViolations")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.RuleViolationEntity, 0, null, null, null, "RuleViolations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BankConnectionData'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBankConnectionData
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BankConnectionDataCollection(), (IEntityRelation)GetRelationsForField("BankConnectionData")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.BankConnectionDataEntity, 0, null, null, null, "BankConnectionData", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPreviousPaymentOption
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("PreviousPaymentOption")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "PreviousPaymentOption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProspectivePaymentOption
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("ProspectivePaymentOption")[0], (int)VarioSL.Entities.EntityType.PaymentOptionEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "ProspectivePaymentOption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BankConnectionDataID property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."BANKCONNECTIONDATAID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BankConnectionDataID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PaymentOptionFieldIndex.BankConnectionDataID, false); }
			set	{ SetValue((int)PaymentOptionFieldIndex.BankConnectionDataID, value, true); }
		}

		/// <summary> The BankingServiceToken property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."BANKINGSERVICETOKEN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String BankingServiceToken
		{
			get { return (System.String)GetValue((int)PaymentOptionFieldIndex.BankingServiceToken, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.BankingServiceToken, value, true); }
		}

		/// <summary> The BlockingComment property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."BLOCKINGCOMMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BlockingComment
		{
			get { return (System.String)GetValue((int)PaymentOptionFieldIndex.BlockingComment, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.BlockingComment, value, true); }
		}

		/// <summary> The BlockingDate property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."BLOCKINGDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BlockingDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentOptionFieldIndex.BlockingDate, false); }
			set	{ SetValue((int)PaymentOptionFieldIndex.BlockingDate, value, true); }
		}

		/// <summary> The BlockingReason property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."BLOCKINGREASON"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BlockingReason
		{
			get { return (System.Int32)GetValue((int)PaymentOptionFieldIndex.BlockingReason, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.BlockingReason, value, true); }
		}

		/// <summary> The CardID property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PaymentOptionFieldIndex.CardID, false); }
			set	{ SetValue((int)PaymentOptionFieldIndex.CardID, value, true); }
		}

		/// <summary> The ContractID property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ContractID
		{
			get { return (System.Int64)GetValue((int)PaymentOptionFieldIndex.ContractID, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.ContractID, value, true); }
		}

		/// <summary> The CreditCardNetwork property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."CREDITCARDNETWORK"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CreditCardNetwork
		{
			get { return (System.String)GetValue((int)PaymentOptionFieldIndex.CreditCardNetwork, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.CreditCardNetwork, value, true); }
		}

		/// <summary> The Description property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)PaymentOptionFieldIndex.Description, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.Description, value, true); }
		}

		/// <summary> The Expiry property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."EXPIRY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Expiry
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentOptionFieldIndex.Expiry, false); }
			set	{ SetValue((int)PaymentOptionFieldIndex.Expiry, value, true); }
		}

		/// <summary> The IsBlocked property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."ISBLOCKED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsBlocked
		{
			get { return (System.Boolean)GetValue((int)PaymentOptionFieldIndex.IsBlocked, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.IsBlocked, value, true); }
		}

		/// <summary> The IsRetired property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."ISRETIRED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsRetired
		{
			get { return (System.Boolean)GetValue((int)PaymentOptionFieldIndex.IsRetired, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.IsRetired, value, true); }
		}

		/// <summary> The IsTemporary property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."ISTEMPORARY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTemporary
		{
			get { return (System.Boolean)GetValue((int)PaymentOptionFieldIndex.IsTemporary, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.IsTemporary, value, true); }
		}

		/// <summary> The LastModified property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)PaymentOptionFieldIndex.LastModified, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)PaymentOptionFieldIndex.LastUser, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PaymentOptionFieldIndex.Name, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.Name, value, true); }
		}

		/// <summary> The PaymentMethod property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."PAYMENTMETHOD"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.DevicePaymentMethod PaymentMethod
		{
			get { return (VarioSL.Entities.Enumerations.DevicePaymentMethod)GetValue((int)PaymentOptionFieldIndex.PaymentMethod, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.PaymentMethod, value, true); }
		}

		/// <summary> The PaymentOptionID property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."PAYMENTOPTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 PaymentOptionID
		{
			get { return (System.Int64)GetValue((int)PaymentOptionFieldIndex.PaymentOptionID, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.PaymentOptionID, value, true); }
		}

		/// <summary> The Priority property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."PRIORITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Priority
		{
			get { return (System.Int32)GetValue((int)PaymentOptionFieldIndex.Priority, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.Priority, value, true); }
		}

		/// <summary> The ProspectivePaymentOptionID property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."PROSPECTIVEPAYMENTOPTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ProspectivePaymentOptionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PaymentOptionFieldIndex.ProspectivePaymentOptionID, false); }
			set	{ SetValue((int)PaymentOptionFieldIndex.ProspectivePaymentOptionID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity PaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTOPTION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)PaymentOptionFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)PaymentOptionFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAutoloadSettingsAlternative()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AutoloadSettingCollection AutoloadSettingsAlternative
		{
			get	{ return GetMultiAutoloadSettingsAlternative(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AutoloadSettingsAlternative. When set to true, AutoloadSettingsAlternative is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AutoloadSettingsAlternative is accessed. You can always execute/ a forced fetch by calling GetMultiAutoloadSettingsAlternative(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutoloadSettingsAlternative
		{
			get	{ return _alwaysFetchAutoloadSettingsAlternative; }
			set	{ _alwaysFetchAutoloadSettingsAlternative = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AutoloadSettingsAlternative already has been fetched. Setting this property to false when AutoloadSettingsAlternative has been fetched
		/// will clear the AutoloadSettingsAlternative collection well. Setting this property to true while AutoloadSettingsAlternative hasn't been fetched disables lazy loading for AutoloadSettingsAlternative</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutoloadSettingsAlternative
		{
			get { return _alreadyFetchedAutoloadSettingsAlternative;}
			set 
			{
				if(_alreadyFetchedAutoloadSettingsAlternative && !value && (_autoloadSettingsAlternative != null))
				{
					_autoloadSettingsAlternative.Clear();
				}
				_alreadyFetchedAutoloadSettingsAlternative = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAutoloadSettings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AutoloadSettingCollection AutoloadSettings
		{
			get	{ return GetMultiAutoloadSettings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AutoloadSettings. When set to true, AutoloadSettings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AutoloadSettings is accessed. You can always execute/ a forced fetch by calling GetMultiAutoloadSettings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutoloadSettings
		{
			get	{ return _alwaysFetchAutoloadSettings; }
			set	{ _alwaysFetchAutoloadSettings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AutoloadSettings already has been fetched. Setting this property to false when AutoloadSettings has been fetched
		/// will clear the AutoloadSettings collection well. Setting this property to true while AutoloadSettings hasn't been fetched disables lazy loading for AutoloadSettings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutoloadSettings
		{
			get { return _alreadyFetchedAutoloadSettings;}
			set 
			{
				if(_alreadyFetchedAutoloadSettings && !value && (_autoloadSettings != null))
				{
					_autoloadSettings.Clear();
				}
				_alreadyFetchedAutoloadSettings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBackupAutoloadToPaymentOptions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection BackupAutoloadToPaymentOptions
		{
			get	{ return GetMultiBackupAutoloadToPaymentOptions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BackupAutoloadToPaymentOptions. When set to true, BackupAutoloadToPaymentOptions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BackupAutoloadToPaymentOptions is accessed. You can always execute/ a forced fetch by calling GetMultiBackupAutoloadToPaymentOptions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBackupAutoloadToPaymentOptions
		{
			get	{ return _alwaysFetchBackupAutoloadToPaymentOptions; }
			set	{ _alwaysFetchBackupAutoloadToPaymentOptions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BackupAutoloadToPaymentOptions already has been fetched. Setting this property to false when BackupAutoloadToPaymentOptions has been fetched
		/// will clear the BackupAutoloadToPaymentOptions collection well. Setting this property to true while BackupAutoloadToPaymentOptions hasn't been fetched disables lazy loading for BackupAutoloadToPaymentOptions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBackupAutoloadToPaymentOptions
		{
			get { return _alreadyFetchedBackupAutoloadToPaymentOptions;}
			set 
			{
				if(_alreadyFetchedBackupAutoloadToPaymentOptions && !value && (_backupAutoloadToPaymentOptions != null))
				{
					_backupAutoloadToPaymentOptions.Clear();
				}
				_alreadyFetchedBackupAutoloadToPaymentOptions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAutoloadToPaymentOptions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection AutoloadToPaymentOptions
		{
			get	{ return GetMultiAutoloadToPaymentOptions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AutoloadToPaymentOptions. When set to true, AutoloadToPaymentOptions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AutoloadToPaymentOptions is accessed. You can always execute/ a forced fetch by calling GetMultiAutoloadToPaymentOptions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutoloadToPaymentOptions
		{
			get	{ return _alwaysFetchAutoloadToPaymentOptions; }
			set	{ _alwaysFetchAutoloadToPaymentOptions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AutoloadToPaymentOptions already has been fetched. Setting this property to false when AutoloadToPaymentOptions has been fetched
		/// will clear the AutoloadToPaymentOptions collection well. Setting this property to true while AutoloadToPaymentOptions hasn't been fetched disables lazy loading for AutoloadToPaymentOptions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutoloadToPaymentOptions
		{
			get { return _alreadyFetchedAutoloadToPaymentOptions;}
			set 
			{
				if(_alreadyFetchedAutoloadToPaymentOptions && !value && (_autoloadToPaymentOptions != null))
				{
					_autoloadToPaymentOptions.Clear();
				}
				_alreadyFetchedAutoloadToPaymentOptions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiInvoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceCollection Invoices
		{
			get	{ return GetMultiInvoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Invoices. When set to true, Invoices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoices is accessed. You can always execute/ a forced fetch by calling GetMultiInvoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoices
		{
			get	{ return _alwaysFetchInvoices; }
			set	{ _alwaysFetchInvoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoices already has been fetched. Setting this property to false when Invoices has been fetched
		/// will clear the Invoices collection well. Setting this property to true while Invoices hasn't been fetched disables lazy loading for Invoices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoices
		{
			get { return _alreadyFetchedInvoices;}
			set 
			{
				if(_alreadyFetchedInvoices && !value && (_invoices != null))
				{
					_invoices.Clear();
				}
				_alreadyFetchedInvoices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderCollection Orders
		{
			get	{ return GetMultiOrders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Orders. When set to true, Orders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Orders is accessed. You can always execute/ a forced fetch by calling GetMultiOrders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrders
		{
			get	{ return _alwaysFetchOrders; }
			set	{ _alwaysFetchOrders = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Orders already has been fetched. Setting this property to false when Orders has been fetched
		/// will clear the Orders collection well. Setting this property to true while Orders hasn't been fetched disables lazy loading for Orders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrders
		{
			get { return _alreadyFetchedOrders;}
			set 
			{
				if(_alreadyFetchedOrders && !value && (_orders != null))
				{
					_orders.Clear();
				}
				_alreadyFetchedOrders = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalCollection PaymentJournals
		{
			get	{ return GetMultiPaymentJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentJournals. When set to true, PaymentJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentJournals is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentJournals
		{
			get	{ return _alwaysFetchPaymentJournals; }
			set	{ _alwaysFetchPaymentJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentJournals already has been fetched. Setting this property to false when PaymentJournals has been fetched
		/// will clear the PaymentJournals collection well. Setting this property to true while PaymentJournals hasn't been fetched disables lazy loading for PaymentJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentJournals
		{
			get { return _alreadyFetchedPaymentJournals;}
			set 
			{
				if(_alreadyFetchedPaymentJournals && !value && (_paymentJournals != null))
				{
					_paymentJournals.Clear();
				}
				_alreadyFetchedPaymentJournals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleViolations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationCollection RuleViolations
		{
			get	{ return GetMultiRuleViolations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleViolations. When set to true, RuleViolations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleViolations is accessed. You can always execute/ a forced fetch by calling GetMultiRuleViolations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleViolations
		{
			get	{ return _alwaysFetchRuleViolations; }
			set	{ _alwaysFetchRuleViolations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleViolations already has been fetched. Setting this property to false when RuleViolations has been fetched
		/// will clear the RuleViolations collection well. Setting this property to true while RuleViolations hasn't been fetched disables lazy loading for RuleViolations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleViolations
		{
			get { return _alreadyFetchedRuleViolations;}
			set 
			{
				if(_alreadyFetchedRuleViolations && !value && (_ruleViolations != null))
				{
					_ruleViolations.Clear();
				}
				_alreadyFetchedRuleViolations = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentOptions", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentOptions", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BankConnectionDataEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBankConnectionData()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BankConnectionDataEntity BankConnectionData
		{
			get	{ return GetSingleBankConnectionData(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncBankConnectionData(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_bankConnectionData !=null);
						DesetupSyncBankConnectionData(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("BankConnectionData");
						}
					}
					else
					{
						if(_bankConnectionData!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "PaymentOption");
							SetupSyncBankConnectionData(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BankConnectionData. When set to true, BankConnectionData is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BankConnectionData is accessed. You can always execute a forced fetch by calling GetSingleBankConnectionData(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBankConnectionData
		{
			get	{ return _alwaysFetchBankConnectionData; }
			set	{ _alwaysFetchBankConnectionData = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property BankConnectionData already has been fetched. Setting this property to false when BankConnectionData has been fetched
		/// will set BankConnectionData to null as well. Setting this property to true while BankConnectionData hasn't been fetched disables lazy loading for BankConnectionData</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBankConnectionData
		{
			get { return _alreadyFetchedBankConnectionData;}
			set 
			{
				if(_alreadyFetchedBankConnectionData && !value)
				{
					this.BankConnectionData = null;
				}
				_alreadyFetchedBankConnectionData = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BankConnectionData is not found
		/// in the database. When set to true, BankConnectionData will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BankConnectionDataReturnsNewIfNotFound
		{
			get	{ return _bankConnectionDataReturnsNewIfNotFound; }
			set	{ _bankConnectionDataReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'PaymentOptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePreviousPaymentOption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentOptionEntity PreviousPaymentOption
		{
			get	{ return GetSinglePreviousPaymentOption(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPreviousPaymentOption(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_previousPaymentOption !=null);
						DesetupSyncPreviousPaymentOption(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("PreviousPaymentOption");
						}
					}
					else
					{
						if(_previousPaymentOption!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "ProspectivePaymentOption");
							SetupSyncPreviousPaymentOption(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PreviousPaymentOption. When set to true, PreviousPaymentOption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PreviousPaymentOption is accessed. You can always execute a forced fetch by calling GetSinglePreviousPaymentOption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPreviousPaymentOption
		{
			get	{ return _alwaysFetchPreviousPaymentOption; }
			set	{ _alwaysFetchPreviousPaymentOption = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property PreviousPaymentOption already has been fetched. Setting this property to false when PreviousPaymentOption has been fetched
		/// will set PreviousPaymentOption to null as well. Setting this property to true while PreviousPaymentOption hasn't been fetched disables lazy loading for PreviousPaymentOption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPreviousPaymentOption
		{
			get { return _alreadyFetchedPreviousPaymentOption;}
			set 
			{
				if(_alreadyFetchedPreviousPaymentOption && !value)
				{
					this.PreviousPaymentOption = null;
				}
				_alreadyFetchedPreviousPaymentOption = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PreviousPaymentOption is not found
		/// in the database. When set to true, PreviousPaymentOption will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PreviousPaymentOptionReturnsNewIfNotFound
		{
			get	{ return _previousPaymentOptionReturnsNewIfNotFound; }
			set	{ _previousPaymentOptionReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'PaymentOptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProspectivePaymentOption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentOptionEntity ProspectivePaymentOption
		{
			get	{ return GetSingleProspectivePaymentOption(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncProspectivePaymentOption(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_prospectivePaymentOption !=null);
						DesetupSyncProspectivePaymentOption(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("ProspectivePaymentOption");
						}
					}
					else
					{
						if(_prospectivePaymentOption!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "PreviousPaymentOption");
							SetupSyncProspectivePaymentOption(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProspectivePaymentOption. When set to true, ProspectivePaymentOption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProspectivePaymentOption is accessed. You can always execute a forced fetch by calling GetSingleProspectivePaymentOption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProspectivePaymentOption
		{
			get	{ return _alwaysFetchProspectivePaymentOption; }
			set	{ _alwaysFetchProspectivePaymentOption = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property ProspectivePaymentOption already has been fetched. Setting this property to false when ProspectivePaymentOption has been fetched
		/// will set ProspectivePaymentOption to null as well. Setting this property to true while ProspectivePaymentOption hasn't been fetched disables lazy loading for ProspectivePaymentOption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProspectivePaymentOption
		{
			get { return _alreadyFetchedProspectivePaymentOption;}
			set 
			{
				if(_alreadyFetchedProspectivePaymentOption && !value)
				{
					this.ProspectivePaymentOption = null;
				}
				_alreadyFetchedProspectivePaymentOption = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProspectivePaymentOption is not found
		/// in the database. When set to true, ProspectivePaymentOption will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProspectivePaymentOptionReturnsNewIfNotFound
		{
			get	{ return _prospectivePaymentOptionReturnsNewIfNotFound; }
			set	{ _prospectivePaymentOptionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PaymentOptionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
