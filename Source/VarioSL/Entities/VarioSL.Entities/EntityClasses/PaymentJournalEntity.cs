﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PaymentJournal'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PaymentJournalEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.BankStatementCollection	_bankStatements;
		private bool	_alwaysFetchBankStatements, _alreadyFetchedBankStatements;
		private VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection	_paymentJournalToComponent;
		private bool	_alwaysFetchPaymentJournalToComponent, _alreadyFetchedPaymentJournalToComponent;
		private VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection	_ruleViolationToTransactions;
		private bool	_alwaysFetchRuleViolationToTransactions, _alreadyFetchedRuleViolationToTransactions;
		private VarioSL.Entities.CollectionClasses.SaleToComponentCollection	_saleToComponents;
		private bool	_alwaysFetchSaleToComponents, _alreadyFetchedSaleToComponents;
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private PaymentOptionEntity _paymentOption;
		private bool	_alwaysFetchPaymentOption, _alreadyFetchedPaymentOption, _paymentOptionReturnsNewIfNotFound;
		private PersonEntity _person;
		private bool	_alwaysFetchPerson, _alreadyFetchedPerson, _personReturnsNewIfNotFound;
		private SaleEntity _sale;
		private bool	_alwaysFetchSale, _alreadyFetchedSale, _saleReturnsNewIfNotFound;
		private TransactionJournalEntity _transactionJournal;
		private bool	_alwaysFetchTransactionJournal, _alreadyFetchedTransactionJournal, _transactionJournalReturnsNewIfNotFound;
		private BankStatementVerificationEntity _bankStatementVerification;
		private bool	_alwaysFetchBankStatementVerification, _alreadyFetchedBankStatementVerification, _bankStatementVerificationReturnsNewIfNotFound;
		private PaymentRecognitionEntity _paymentRecognition;
		private bool	_alwaysFetchPaymentRecognition, _alreadyFetchedPaymentRecognition, _paymentRecognitionReturnsNewIfNotFound;
		private PaymentReconciliationEntity _paymentReconciliation;
		private bool	_alwaysFetchPaymentReconciliation, _alreadyFetchedPaymentReconciliation, _paymentReconciliationReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name PaymentOption</summary>
			public static readonly string PaymentOption = "PaymentOption";
			/// <summary>Member name Person</summary>
			public static readonly string Person = "Person";
			/// <summary>Member name Sale</summary>
			public static readonly string Sale = "Sale";
			/// <summary>Member name TransactionJournal</summary>
			public static readonly string TransactionJournal = "TransactionJournal";
			/// <summary>Member name BankStatements</summary>
			public static readonly string BankStatements = "BankStatements";
			/// <summary>Member name PaymentJournalToComponent</summary>
			public static readonly string PaymentJournalToComponent = "PaymentJournalToComponent";
			/// <summary>Member name RuleViolationToTransactions</summary>
			public static readonly string RuleViolationToTransactions = "RuleViolationToTransactions";
			/// <summary>Member name SaleToComponents</summary>
			public static readonly string SaleToComponents = "SaleToComponents";
			/// <summary>Member name BankStatementVerification</summary>
			public static readonly string BankStatementVerification = "BankStatementVerification";
			/// <summary>Member name PaymentRecognition</summary>
			public static readonly string PaymentRecognition = "PaymentRecognition";
			/// <summary>Member name PaymentReconciliation</summary>
			public static readonly string PaymentReconciliation = "PaymentReconciliation";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PaymentJournalEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PaymentJournalEntity() :base("PaymentJournalEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="paymentJournalID">PK value for PaymentJournal which data should be fetched into this PaymentJournal object</param>
		public PaymentJournalEntity(System.Int64 paymentJournalID):base("PaymentJournalEntity")
		{
			InitClassFetch(paymentJournalID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentJournalID">PK value for PaymentJournal which data should be fetched into this PaymentJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PaymentJournalEntity(System.Int64 paymentJournalID, IPrefetchPath prefetchPathToUse):base("PaymentJournalEntity")
		{
			InitClassFetch(paymentJournalID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentJournalID">PK value for PaymentJournal which data should be fetched into this PaymentJournal object</param>
		/// <param name="validator">The custom validator object for this PaymentJournalEntity</param>
		public PaymentJournalEntity(System.Int64 paymentJournalID, IValidator validator):base("PaymentJournalEntity")
		{
			InitClassFetch(paymentJournalID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentJournalEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_bankStatements = (VarioSL.Entities.CollectionClasses.BankStatementCollection)info.GetValue("_bankStatements", typeof(VarioSL.Entities.CollectionClasses.BankStatementCollection));
			_alwaysFetchBankStatements = info.GetBoolean("_alwaysFetchBankStatements");
			_alreadyFetchedBankStatements = info.GetBoolean("_alreadyFetchedBankStatements");

			_paymentJournalToComponent = (VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection)info.GetValue("_paymentJournalToComponent", typeof(VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection));
			_alwaysFetchPaymentJournalToComponent = info.GetBoolean("_alwaysFetchPaymentJournalToComponent");
			_alreadyFetchedPaymentJournalToComponent = info.GetBoolean("_alreadyFetchedPaymentJournalToComponent");

			_ruleViolationToTransactions = (VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection)info.GetValue("_ruleViolationToTransactions", typeof(VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection));
			_alwaysFetchRuleViolationToTransactions = info.GetBoolean("_alwaysFetchRuleViolationToTransactions");
			_alreadyFetchedRuleViolationToTransactions = info.GetBoolean("_alreadyFetchedRuleViolationToTransactions");

			_saleToComponents = (VarioSL.Entities.CollectionClasses.SaleToComponentCollection)info.GetValue("_saleToComponents", typeof(VarioSL.Entities.CollectionClasses.SaleToComponentCollection));
			_alwaysFetchSaleToComponents = info.GetBoolean("_alwaysFetchSaleToComponents");
			_alreadyFetchedSaleToComponents = info.GetBoolean("_alreadyFetchedSaleToComponents");
			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_paymentOption = (PaymentOptionEntity)info.GetValue("_paymentOption", typeof(PaymentOptionEntity));
			if(_paymentOption!=null)
			{
				_paymentOption.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentOptionReturnsNewIfNotFound = info.GetBoolean("_paymentOptionReturnsNewIfNotFound");
			_alwaysFetchPaymentOption = info.GetBoolean("_alwaysFetchPaymentOption");
			_alreadyFetchedPaymentOption = info.GetBoolean("_alreadyFetchedPaymentOption");

			_person = (PersonEntity)info.GetValue("_person", typeof(PersonEntity));
			if(_person!=null)
			{
				_person.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_personReturnsNewIfNotFound = info.GetBoolean("_personReturnsNewIfNotFound");
			_alwaysFetchPerson = info.GetBoolean("_alwaysFetchPerson");
			_alreadyFetchedPerson = info.GetBoolean("_alreadyFetchedPerson");

			_sale = (SaleEntity)info.GetValue("_sale", typeof(SaleEntity));
			if(_sale!=null)
			{
				_sale.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_saleReturnsNewIfNotFound = info.GetBoolean("_saleReturnsNewIfNotFound");
			_alwaysFetchSale = info.GetBoolean("_alwaysFetchSale");
			_alreadyFetchedSale = info.GetBoolean("_alreadyFetchedSale");

			_transactionJournal = (TransactionJournalEntity)info.GetValue("_transactionJournal", typeof(TransactionJournalEntity));
			if(_transactionJournal!=null)
			{
				_transactionJournal.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_transactionJournalReturnsNewIfNotFound = info.GetBoolean("_transactionJournalReturnsNewIfNotFound");
			_alwaysFetchTransactionJournal = info.GetBoolean("_alwaysFetchTransactionJournal");
			_alreadyFetchedTransactionJournal = info.GetBoolean("_alreadyFetchedTransactionJournal");
			_bankStatementVerification = (BankStatementVerificationEntity)info.GetValue("_bankStatementVerification", typeof(BankStatementVerificationEntity));
			if(_bankStatementVerification!=null)
			{
				_bankStatementVerification.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_bankStatementVerificationReturnsNewIfNotFound = info.GetBoolean("_bankStatementVerificationReturnsNewIfNotFound");
			_alwaysFetchBankStatementVerification = info.GetBoolean("_alwaysFetchBankStatementVerification");
			_alreadyFetchedBankStatementVerification = info.GetBoolean("_alreadyFetchedBankStatementVerification");

			_paymentRecognition = (PaymentRecognitionEntity)info.GetValue("_paymentRecognition", typeof(PaymentRecognitionEntity));
			if(_paymentRecognition!=null)
			{
				_paymentRecognition.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentRecognitionReturnsNewIfNotFound = info.GetBoolean("_paymentRecognitionReturnsNewIfNotFound");
			_alwaysFetchPaymentRecognition = info.GetBoolean("_alwaysFetchPaymentRecognition");
			_alreadyFetchedPaymentRecognition = info.GetBoolean("_alreadyFetchedPaymentRecognition");

			_paymentReconciliation = (PaymentReconciliationEntity)info.GetValue("_paymentReconciliation", typeof(PaymentReconciliationEntity));
			if(_paymentReconciliation!=null)
			{
				_paymentReconciliation.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentReconciliationReturnsNewIfNotFound = info.GetBoolean("_paymentReconciliationReturnsNewIfNotFound");
			_alwaysFetchPaymentReconciliation = info.GetBoolean("_alwaysFetchPaymentReconciliation");
			_alreadyFetchedPaymentReconciliation = info.GetBoolean("_alreadyFetchedPaymentReconciliation");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PaymentJournalFieldIndex)fieldIndex)
			{
				case PaymentJournalFieldIndex.CardID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				case PaymentJournalFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case PaymentJournalFieldIndex.PaymentOptionID:
					DesetupSyncPaymentOption(true, false);
					_alreadyFetchedPaymentOption = false;
					break;
				case PaymentJournalFieldIndex.PersonID:
					DesetupSyncPerson(true, false);
					_alreadyFetchedPerson = false;
					break;
				case PaymentJournalFieldIndex.SaleID:
					DesetupSyncSale(true, false);
					_alreadyFetchedSale = false;
					break;
				case PaymentJournalFieldIndex.TransactionJournalID:
					DesetupSyncTransactionJournal(true, false);
					_alreadyFetchedTransactionJournal = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedBankStatements = (_bankStatements.Count > 0);
			_alreadyFetchedPaymentJournalToComponent = (_paymentJournalToComponent.Count > 0);
			_alreadyFetchedRuleViolationToTransactions = (_ruleViolationToTransactions.Count > 0);
			_alreadyFetchedSaleToComponents = (_saleToComponents.Count > 0);
			_alreadyFetchedCard = (_card != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedPaymentOption = (_paymentOption != null);
			_alreadyFetchedPerson = (_person != null);
			_alreadyFetchedSale = (_sale != null);
			_alreadyFetchedTransactionJournal = (_transactionJournal != null);
			_alreadyFetchedBankStatementVerification = (_bankStatementVerification != null);
			_alreadyFetchedPaymentRecognition = (_paymentRecognition != null);
			_alreadyFetchedPaymentReconciliation = (_paymentReconciliation != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Card":
					toReturn.Add(Relations.CardEntityUsingCardID);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "PaymentOption":
					toReturn.Add(Relations.PaymentOptionEntityUsingPaymentOptionID);
					break;
				case "Person":
					toReturn.Add(Relations.PersonEntityUsingPersonID);
					break;
				case "Sale":
					toReturn.Add(Relations.SaleEntityUsingSaleID);
					break;
				case "TransactionJournal":
					toReturn.Add(Relations.TransactionJournalEntityUsingTransactionJournalID);
					break;
				case "BankStatements":
					toReturn.Add(Relations.BankStatementEntityUsingPaymentJournalID);
					break;
				case "PaymentJournalToComponent":
					toReturn.Add(Relations.PaymentJournalToComponentEntityUsingPaymentjournalID);
					break;
				case "RuleViolationToTransactions":
					toReturn.Add(Relations.RuleViolationToTransactionEntityUsingPaymentJournalID);
					break;
				case "SaleToComponents":
					toReturn.Add(Relations.SaleToComponentEntityUsingSaleID);
					break;
				case "BankStatementVerification":
					toReturn.Add(Relations.BankStatementVerificationEntityUsingPaymentJournalID);
					break;
				case "PaymentRecognition":
					toReturn.Add(Relations.PaymentRecognitionEntityUsingPaymentJournalID);
					break;
				case "PaymentReconciliation":
					toReturn.Add(Relations.PaymentReconciliationEntityUsingPaymentJournalID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_bankStatements", (!this.MarkedForDeletion?_bankStatements:null));
			info.AddValue("_alwaysFetchBankStatements", _alwaysFetchBankStatements);
			info.AddValue("_alreadyFetchedBankStatements", _alreadyFetchedBankStatements);
			info.AddValue("_paymentJournalToComponent", (!this.MarkedForDeletion?_paymentJournalToComponent:null));
			info.AddValue("_alwaysFetchPaymentJournalToComponent", _alwaysFetchPaymentJournalToComponent);
			info.AddValue("_alreadyFetchedPaymentJournalToComponent", _alreadyFetchedPaymentJournalToComponent);
			info.AddValue("_ruleViolationToTransactions", (!this.MarkedForDeletion?_ruleViolationToTransactions:null));
			info.AddValue("_alwaysFetchRuleViolationToTransactions", _alwaysFetchRuleViolationToTransactions);
			info.AddValue("_alreadyFetchedRuleViolationToTransactions", _alreadyFetchedRuleViolationToTransactions);
			info.AddValue("_saleToComponents", (!this.MarkedForDeletion?_saleToComponents:null));
			info.AddValue("_alwaysFetchSaleToComponents", _alwaysFetchSaleToComponents);
			info.AddValue("_alreadyFetchedSaleToComponents", _alreadyFetchedSaleToComponents);
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_paymentOption", (!this.MarkedForDeletion?_paymentOption:null));
			info.AddValue("_paymentOptionReturnsNewIfNotFound", _paymentOptionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentOption", _alwaysFetchPaymentOption);
			info.AddValue("_alreadyFetchedPaymentOption", _alreadyFetchedPaymentOption);
			info.AddValue("_person", (!this.MarkedForDeletion?_person:null));
			info.AddValue("_personReturnsNewIfNotFound", _personReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerson", _alwaysFetchPerson);
			info.AddValue("_alreadyFetchedPerson", _alreadyFetchedPerson);
			info.AddValue("_sale", (!this.MarkedForDeletion?_sale:null));
			info.AddValue("_saleReturnsNewIfNotFound", _saleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSale", _alwaysFetchSale);
			info.AddValue("_alreadyFetchedSale", _alreadyFetchedSale);
			info.AddValue("_transactionJournal", (!this.MarkedForDeletion?_transactionJournal:null));
			info.AddValue("_transactionJournalReturnsNewIfNotFound", _transactionJournalReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTransactionJournal", _alwaysFetchTransactionJournal);
			info.AddValue("_alreadyFetchedTransactionJournal", _alreadyFetchedTransactionJournal);

			info.AddValue("_bankStatementVerification", (!this.MarkedForDeletion?_bankStatementVerification:null));
			info.AddValue("_bankStatementVerificationReturnsNewIfNotFound", _bankStatementVerificationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBankStatementVerification", _alwaysFetchBankStatementVerification);
			info.AddValue("_alreadyFetchedBankStatementVerification", _alreadyFetchedBankStatementVerification);

			info.AddValue("_paymentRecognition", (!this.MarkedForDeletion?_paymentRecognition:null));
			info.AddValue("_paymentRecognitionReturnsNewIfNotFound", _paymentRecognitionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentRecognition", _alwaysFetchPaymentRecognition);
			info.AddValue("_alreadyFetchedPaymentRecognition", _alreadyFetchedPaymentRecognition);

			info.AddValue("_paymentReconciliation", (!this.MarkedForDeletion?_paymentReconciliation:null));
			info.AddValue("_paymentReconciliationReturnsNewIfNotFound", _paymentReconciliationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentReconciliation", _alwaysFetchPaymentReconciliation);
			info.AddValue("_alreadyFetchedPaymentReconciliation", _alreadyFetchedPaymentReconciliation);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "PaymentOption":
					_alreadyFetchedPaymentOption = true;
					this.PaymentOption = (PaymentOptionEntity)entity;
					break;
				case "Person":
					_alreadyFetchedPerson = true;
					this.Person = (PersonEntity)entity;
					break;
				case "Sale":
					_alreadyFetchedSale = true;
					this.Sale = (SaleEntity)entity;
					break;
				case "TransactionJournal":
					_alreadyFetchedTransactionJournal = true;
					this.TransactionJournal = (TransactionJournalEntity)entity;
					break;
				case "BankStatements":
					_alreadyFetchedBankStatements = true;
					if(entity!=null)
					{
						this.BankStatements.Add((BankStatementEntity)entity);
					}
					break;
				case "PaymentJournalToComponent":
					_alreadyFetchedPaymentJournalToComponent = true;
					if(entity!=null)
					{
						this.PaymentJournalToComponent.Add((PaymentJournalToComponentEntity)entity);
					}
					break;
				case "RuleViolationToTransactions":
					_alreadyFetchedRuleViolationToTransactions = true;
					if(entity!=null)
					{
						this.RuleViolationToTransactions.Add((RuleViolationToTransactionEntity)entity);
					}
					break;
				case "SaleToComponents":
					_alreadyFetchedSaleToComponents = true;
					if(entity!=null)
					{
						this.SaleToComponents.Add((SaleToComponentEntity)entity);
					}
					break;
				case "BankStatementVerification":
					_alreadyFetchedBankStatementVerification = true;
					this.BankStatementVerification = (BankStatementVerificationEntity)entity;
					break;
				case "PaymentRecognition":
					_alreadyFetchedPaymentRecognition = true;
					this.PaymentRecognition = (PaymentRecognitionEntity)entity;
					break;
				case "PaymentReconciliation":
					_alreadyFetchedPaymentReconciliation = true;
					this.PaymentReconciliation = (PaymentReconciliationEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "PaymentOption":
					SetupSyncPaymentOption(relatedEntity);
					break;
				case "Person":
					SetupSyncPerson(relatedEntity);
					break;
				case "Sale":
					SetupSyncSale(relatedEntity);
					break;
				case "TransactionJournal":
					SetupSyncTransactionJournal(relatedEntity);
					break;
				case "BankStatements":
					_bankStatements.Add((BankStatementEntity)relatedEntity);
					break;
				case "PaymentJournalToComponent":
					_paymentJournalToComponent.Add((PaymentJournalToComponentEntity)relatedEntity);
					break;
				case "RuleViolationToTransactions":
					_ruleViolationToTransactions.Add((RuleViolationToTransactionEntity)relatedEntity);
					break;
				case "SaleToComponents":
					_saleToComponents.Add((SaleToComponentEntity)relatedEntity);
					break;
				case "BankStatementVerification":
					SetupSyncBankStatementVerification(relatedEntity);
					break;
				case "PaymentRecognition":
					SetupSyncPaymentRecognition(relatedEntity);
					break;
				case "PaymentReconciliation":
					SetupSyncPaymentReconciliation(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Card":
					DesetupSyncCard(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "PaymentOption":
					DesetupSyncPaymentOption(false, true);
					break;
				case "Person":
					DesetupSyncPerson(false, true);
					break;
				case "Sale":
					DesetupSyncSale(false, true);
					break;
				case "TransactionJournal":
					DesetupSyncTransactionJournal(false, true);
					break;
				case "BankStatements":
					this.PerformRelatedEntityRemoval(_bankStatements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentJournalToComponent":
					this.PerformRelatedEntityRemoval(_paymentJournalToComponent, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RuleViolationToTransactions":
					this.PerformRelatedEntityRemoval(_ruleViolationToTransactions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SaleToComponents":
					this.PerformRelatedEntityRemoval(_saleToComponents, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BankStatementVerification":
					DesetupSyncBankStatementVerification(false, true);
					break;
				case "PaymentRecognition":
					DesetupSyncPaymentRecognition(false, true);
					break;
				case "PaymentReconciliation":
					DesetupSyncPaymentReconciliation(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_bankStatementVerification!=null)
			{
				toReturn.Add(_bankStatementVerification);
			}
			if(_paymentRecognition!=null)
			{
				toReturn.Add(_paymentRecognition);
			}
			if(_paymentReconciliation!=null)
			{
				toReturn.Add(_paymentReconciliation);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_paymentOption!=null)
			{
				toReturn.Add(_paymentOption);
			}
			if(_person!=null)
			{
				toReturn.Add(_person);
			}
			if(_sale!=null)
			{
				toReturn.Add(_sale);
			}
			if(_transactionJournal!=null)
			{
				toReturn.Add(_transactionJournal);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_bankStatements);
			toReturn.Add(_paymentJournalToComponent);
			toReturn.Add(_ruleViolationToTransactions);
			toReturn.Add(_saleToComponents);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="walletTransactionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCWalletTransactionID(System.String walletTransactionID)
		{
			return FetchUsingUCWalletTransactionID( walletTransactionID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="walletTransactionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCWalletTransactionID(System.String walletTransactionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCWalletTransactionID( walletTransactionID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="walletTransactionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCWalletTransactionID(System.String walletTransactionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCWalletTransactionID( walletTransactionID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="walletTransactionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCWalletTransactionID(System.String walletTransactionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((PaymentJournalDAO)CreateDAOInstance()).FetchPaymentJournalUsingUCWalletTransactionID(this, this.Transaction, walletTransactionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentJournalID">PK value for PaymentJournal which data should be fetched into this PaymentJournal object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentJournalID)
		{
			return FetchUsingPK(paymentJournalID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentJournalID">PK value for PaymentJournal which data should be fetched into this PaymentJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentJournalID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(paymentJournalID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentJournalID">PK value for PaymentJournal which data should be fetched into this PaymentJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(paymentJournalID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentJournalID">PK value for PaymentJournal which data should be fetched into this PaymentJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(paymentJournalID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PaymentJournalID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PaymentJournalRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'BankStatementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BankStatementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BankStatementCollection GetMultiBankStatements(bool forceFetch)
		{
			return GetMultiBankStatements(forceFetch, _bankStatements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BankStatementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BankStatementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BankStatementCollection GetMultiBankStatements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBankStatements(forceFetch, _bankStatements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BankStatementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BankStatementCollection GetMultiBankStatements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBankStatements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BankStatementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BankStatementCollection GetMultiBankStatements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBankStatements || forceFetch || _alwaysFetchBankStatements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_bankStatements);
				_bankStatements.SuppressClearInGetMulti=!forceFetch;
				_bankStatements.EntityFactoryToUse = entityFactoryToUse;
				_bankStatements.GetMultiManyToOne(this, filter);
				_bankStatements.SuppressClearInGetMulti=false;
				_alreadyFetchedBankStatements = true;
			}
			return _bankStatements;
		}

		/// <summary> Sets the collection parameters for the collection for 'BankStatements'. These settings will be taken into account
		/// when the property BankStatements is requested or GetMultiBankStatements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBankStatements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_bankStatements.SortClauses=sortClauses;
			_bankStatements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalToComponentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection GetMultiPaymentJournalToComponent(bool forceFetch)
		{
			return GetMultiPaymentJournalToComponent(forceFetch, _paymentJournalToComponent.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalToComponentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection GetMultiPaymentJournalToComponent(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentJournalToComponent(forceFetch, _paymentJournalToComponent.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection GetMultiPaymentJournalToComponent(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentJournalToComponent(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection GetMultiPaymentJournalToComponent(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentJournalToComponent || forceFetch || _alwaysFetchPaymentJournalToComponent) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentJournalToComponent);
				_paymentJournalToComponent.SuppressClearInGetMulti=!forceFetch;
				_paymentJournalToComponent.EntityFactoryToUse = entityFactoryToUse;
				_paymentJournalToComponent.GetMultiManyToOne(null, this, filter);
				_paymentJournalToComponent.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentJournalToComponent = true;
			}
			return _paymentJournalToComponent;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentJournalToComponent'. These settings will be taken into account
		/// when the property PaymentJournalToComponent is requested or GetMultiPaymentJournalToComponent is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentJournalToComponent(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentJournalToComponent.SortClauses=sortClauses;
			_paymentJournalToComponent.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationToTransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch)
		{
			return GetMultiRuleViolationToTransactions(forceFetch, _ruleViolationToTransactions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationToTransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleViolationToTransactions(forceFetch, _ruleViolationToTransactions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleViolationToTransactions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleViolationToTransactions || forceFetch || _alwaysFetchRuleViolationToTransactions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleViolationToTransactions);
				_ruleViolationToTransactions.SuppressClearInGetMulti=!forceFetch;
				_ruleViolationToTransactions.EntityFactoryToUse = entityFactoryToUse;
				_ruleViolationToTransactions.GetMultiManyToOne(null, this, null, null, filter);
				_ruleViolationToTransactions.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleViolationToTransactions = true;
			}
			return _ruleViolationToTransactions;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleViolationToTransactions'. These settings will be taken into account
		/// when the property RuleViolationToTransactions is requested or GetMultiRuleViolationToTransactions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleViolationToTransactions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleViolationToTransactions.SortClauses=sortClauses;
			_ruleViolationToTransactions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SaleToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SaleToComponentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleToComponentCollection GetMultiSaleToComponents(bool forceFetch)
		{
			return GetMultiSaleToComponents(forceFetch, _saleToComponents.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SaleToComponentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleToComponentCollection GetMultiSaleToComponents(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSaleToComponents(forceFetch, _saleToComponents.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SaleToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SaleToComponentCollection GetMultiSaleToComponents(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSaleToComponents(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SaleToComponentCollection GetMultiSaleToComponents(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSaleToComponents || forceFetch || _alwaysFetchSaleToComponents) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_saleToComponents);
				_saleToComponents.SuppressClearInGetMulti=!forceFetch;
				_saleToComponents.EntityFactoryToUse = entityFactoryToUse;
				_saleToComponents.GetMultiManyToOne(null, this, filter);
				_saleToComponents.SuppressClearInGetMulti=false;
				_alreadyFetchedSaleToComponents = true;
			}
			return _saleToComponents;
		}

		/// <summary> Sets the collection parameters for the collection for 'SaleToComponents'. These settings will be taken into account
		/// when the property SaleToComponents is requested or GetMultiSaleToComponents is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSaleToComponents(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_saleToComponents.SortClauses=sortClauses;
			_saleToComponents.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public PaymentOptionEntity GetSinglePaymentOption()
		{
			return GetSinglePaymentOption(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public virtual PaymentOptionEntity GetSinglePaymentOption(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentOption || forceFetch || _alwaysFetchPaymentOption) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentOptionEntityUsingPaymentOptionID);
				PaymentOptionEntity newEntity = new PaymentOptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentOptionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentOptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentOptionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentOption = newEntity;
				_alreadyFetchedPaymentOption = fetchResult;
			}
			return _paymentOption;
		}


		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSinglePerson()
		{
			return GetSinglePerson(false);
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSinglePerson(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerson || forceFetch || _alwaysFetchPerson) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingPersonID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphic(this.Transaction, this.PersonID.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_personReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Person = newEntity;
				_alreadyFetchedPerson = fetchResult;
			}
			return _person;
		}


		/// <summary> Retrieves the related entity of type 'SaleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SaleEntity' which is related to this entity.</returns>
		public SaleEntity GetSingleSale()
		{
			return GetSingleSale(false);
		}

		/// <summary> Retrieves the related entity of type 'SaleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SaleEntity' which is related to this entity.</returns>
		public virtual SaleEntity GetSingleSale(bool forceFetch)
		{
			if( ( !_alreadyFetchedSale || forceFetch || _alwaysFetchSale) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SaleEntityUsingSaleID);
				SaleEntity newEntity = new SaleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SaleID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SaleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_saleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Sale = newEntity;
				_alreadyFetchedSale = fetchResult;
			}
			return _sale;
		}


		/// <summary> Retrieves the related entity of type 'TransactionJournalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TransactionJournalEntity' which is related to this entity.</returns>
		public TransactionJournalEntity GetSingleTransactionJournal()
		{
			return GetSingleTransactionJournal(false);
		}

		/// <summary> Retrieves the related entity of type 'TransactionJournalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TransactionJournalEntity' which is related to this entity.</returns>
		public virtual TransactionJournalEntity GetSingleTransactionJournal(bool forceFetch)
		{
			if( ( !_alreadyFetchedTransactionJournal || forceFetch || _alwaysFetchTransactionJournal) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TransactionJournalEntityUsingTransactionJournalID);
				TransactionJournalEntity newEntity = new TransactionJournalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TransactionJournalID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TransactionJournalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_transactionJournalReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TransactionJournal = newEntity;
				_alreadyFetchedTransactionJournal = fetchResult;
			}
			return _transactionJournal;
		}

		/// <summary> Retrieves the related entity of type 'BankStatementVerificationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'BankStatementVerificationEntity' which is related to this entity.</returns>
		public BankStatementVerificationEntity GetSingleBankStatementVerification()
		{
			return GetSingleBankStatementVerification(false);
		}
		
		/// <summary> Retrieves the related entity of type 'BankStatementVerificationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BankStatementVerificationEntity' which is related to this entity.</returns>
		public virtual BankStatementVerificationEntity GetSingleBankStatementVerification(bool forceFetch)
		{
			if( ( !_alreadyFetchedBankStatementVerification || forceFetch || _alwaysFetchBankStatementVerification) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BankStatementVerificationEntityUsingPaymentJournalID);
				BankStatementVerificationEntity newEntity = new BankStatementVerificationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCPaymentJournalID(this.PaymentJournalID);
				}
				if(fetchResult)
				{
					newEntity = (BankStatementVerificationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_bankStatementVerificationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BankStatementVerification = newEntity;
				_alreadyFetchedBankStatementVerification = fetchResult;
			}
			return _bankStatementVerification;
		}

		/// <summary> Retrieves the related entity of type 'PaymentRecognitionEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PaymentRecognitionEntity' which is related to this entity.</returns>
		public PaymentRecognitionEntity GetSinglePaymentRecognition()
		{
			return GetSinglePaymentRecognition(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PaymentRecognitionEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentRecognitionEntity' which is related to this entity.</returns>
		public virtual PaymentRecognitionEntity GetSinglePaymentRecognition(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentRecognition || forceFetch || _alwaysFetchPaymentRecognition) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentRecognitionEntityUsingPaymentJournalID);
				PaymentRecognitionEntity newEntity = new PaymentRecognitionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCPaymentJournalID(this.PaymentJournalID);
				}
				if(fetchResult)
				{
					newEntity = (PaymentRecognitionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentRecognitionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentRecognition = newEntity;
				_alreadyFetchedPaymentRecognition = fetchResult;
			}
			return _paymentRecognition;
		}

		/// <summary> Retrieves the related entity of type 'PaymentReconciliationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PaymentReconciliationEntity' which is related to this entity.</returns>
		public PaymentReconciliationEntity GetSinglePaymentReconciliation()
		{
			return GetSinglePaymentReconciliation(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PaymentReconciliationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentReconciliationEntity' which is related to this entity.</returns>
		public virtual PaymentReconciliationEntity GetSinglePaymentReconciliation(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentReconciliation || forceFetch || _alwaysFetchPaymentReconciliation) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentReconciliationEntityUsingPaymentJournalID);
				PaymentReconciliationEntity newEntity = new PaymentReconciliationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCPaymentJournalID(this.PaymentJournalID);
				}
				if(fetchResult)
				{
					newEntity = (PaymentReconciliationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentReconciliationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentReconciliation = newEntity;
				_alreadyFetchedPaymentReconciliation = fetchResult;
			}
			return _paymentReconciliation;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Card", _card);
			toReturn.Add("Contract", _contract);
			toReturn.Add("PaymentOption", _paymentOption);
			toReturn.Add("Person", _person);
			toReturn.Add("Sale", _sale);
			toReturn.Add("TransactionJournal", _transactionJournal);
			toReturn.Add("BankStatements", _bankStatements);
			toReturn.Add("PaymentJournalToComponent", _paymentJournalToComponent);
			toReturn.Add("RuleViolationToTransactions", _ruleViolationToTransactions);
			toReturn.Add("SaleToComponents", _saleToComponents);
			toReturn.Add("BankStatementVerification", _bankStatementVerification);
			toReturn.Add("PaymentRecognition", _paymentRecognition);
			toReturn.Add("PaymentReconciliation", _paymentReconciliation);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="paymentJournalID">PK value for PaymentJournal which data should be fetched into this PaymentJournal object</param>
		/// <param name="validator">The validator object for this PaymentJournalEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 paymentJournalID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(paymentJournalID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_bankStatements = new VarioSL.Entities.CollectionClasses.BankStatementCollection();
			_bankStatements.SetContainingEntityInfo(this, "PaymentJournal");

			_paymentJournalToComponent = new VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection();
			_paymentJournalToComponent.SetContainingEntityInfo(this, "PaymentJournal");

			_ruleViolationToTransactions = new VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection();
			_ruleViolationToTransactions.SetContainingEntityInfo(this, "PaymentJournal");

			_saleToComponents = new VarioSL.Entities.CollectionClasses.SaleToComponentCollection();
			_saleToComponents.SetContainingEntityInfo(this, "PaymentJournal");
			_cardReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_paymentOptionReturnsNewIfNotFound = false;
			_personReturnsNewIfNotFound = false;
			_saleReturnsNewIfNotFound = false;
			_transactionJournalReturnsNewIfNotFound = false;
			_bankStatementVerificationReturnsNewIfNotFound = false;
			_paymentRecognitionReturnsNewIfNotFound = false;
			_paymentReconciliationReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashReceived", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashReturn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckBranchRouting", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckCustomerIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckingAccount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Code", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Confirmed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditCardNetwork", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EncryptedData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EncryptedDataType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExecutionResult", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExecutionTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsPreTax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaskedCheckingAccount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaskedPan", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentOptionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PayPalAuthorizationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PayPalCaptureID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PayPalCaptureRequestID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaypalCaptureToken", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PayPalRequestID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PayPalRequestToken", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProviderReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProviderReferenceExternal", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurchaseOrderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReconciliationState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReferenceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SaleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Token", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WalletTransactionID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.CardEntityUsingCardIDStatic, true, signalRelatedEntity, "PaymentJournals", resetFKFields, new int[] { (int)PaymentJournalFieldIndex.CardID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.CardEntityUsingCardIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "PaymentJournals", resetFKFields, new int[] { (int)PaymentJournalFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentOption</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentOption(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.PaymentOptionEntityUsingPaymentOptionIDStatic, true, signalRelatedEntity, "PaymentJournals", resetFKFields, new int[] { (int)PaymentJournalFieldIndex.PaymentOptionID } );		
			_paymentOption = null;
		}
		
		/// <summary> setups the sync logic for member _paymentOption</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentOption(IEntityCore relatedEntity)
		{
			if(_paymentOption!=relatedEntity)
			{		
				DesetupSyncPaymentOption(true, true);
				_paymentOption = (PaymentOptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.PaymentOptionEntityUsingPaymentOptionIDStatic, true, ref _alreadyFetchedPaymentOption, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentOptionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _person</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerson(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.PersonEntityUsingPersonIDStatic, true, signalRelatedEntity, "PaymentJournals", resetFKFields, new int[] { (int)PaymentJournalFieldIndex.PersonID } );		
			_person = null;
		}
		
		/// <summary> setups the sync logic for member _person</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerson(IEntityCore relatedEntity)
		{
			if(_person!=relatedEntity)
			{		
				DesetupSyncPerson(true, true);
				_person = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.PersonEntityUsingPersonIDStatic, true, ref _alreadyFetchedPerson, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPersonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _sale</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSale(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _sale, new PropertyChangedEventHandler( OnSalePropertyChanged ), "Sale", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.SaleEntityUsingSaleIDStatic, true, signalRelatedEntity, "PaymentJournals", resetFKFields, new int[] { (int)PaymentJournalFieldIndex.SaleID } );		
			_sale = null;
		}
		
		/// <summary> setups the sync logic for member _sale</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSale(IEntityCore relatedEntity)
		{
			if(_sale!=relatedEntity)
			{		
				DesetupSyncSale(true, true);
				_sale = (SaleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _sale, new PropertyChangedEventHandler( OnSalePropertyChanged ), "Sale", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.SaleEntityUsingSaleIDStatic, true, ref _alreadyFetchedSale, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSalePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _transactionJournal</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTransactionJournal(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _transactionJournal, new PropertyChangedEventHandler( OnTransactionJournalPropertyChanged ), "TransactionJournal", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.TransactionJournalEntityUsingTransactionJournalIDStatic, true, signalRelatedEntity, "PaymentJournals", resetFKFields, new int[] { (int)PaymentJournalFieldIndex.TransactionJournalID } );		
			_transactionJournal = null;
		}
		
		/// <summary> setups the sync logic for member _transactionJournal</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTransactionJournal(IEntityCore relatedEntity)
		{
			if(_transactionJournal!=relatedEntity)
			{		
				DesetupSyncTransactionJournal(true, true);
				_transactionJournal = (TransactionJournalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _transactionJournal, new PropertyChangedEventHandler( OnTransactionJournalPropertyChanged ), "TransactionJournal", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.TransactionJournalEntityUsingTransactionJournalIDStatic, true, ref _alreadyFetchedTransactionJournal, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTransactionJournalPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _bankStatementVerification</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBankStatementVerification(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _bankStatementVerification, new PropertyChangedEventHandler( OnBankStatementVerificationPropertyChanged ), "BankStatementVerification", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.BankStatementVerificationEntityUsingPaymentJournalIDStatic, false, signalRelatedEntity, "PaymentJournal", false, new int[] { (int)PaymentJournalFieldIndex.PaymentJournalID } );
			_bankStatementVerification = null;
		}
	
		/// <summary> setups the sync logic for member _bankStatementVerification</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBankStatementVerification(IEntityCore relatedEntity)
		{
			if(_bankStatementVerification!=relatedEntity)
			{
				DesetupSyncBankStatementVerification(true, true);
				_bankStatementVerification = (BankStatementVerificationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _bankStatementVerification, new PropertyChangedEventHandler( OnBankStatementVerificationPropertyChanged ), "BankStatementVerification", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.BankStatementVerificationEntityUsingPaymentJournalIDStatic, false, ref _alreadyFetchedBankStatementVerification, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBankStatementVerificationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentRecognition</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentRecognition(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentRecognition, new PropertyChangedEventHandler( OnPaymentRecognitionPropertyChanged ), "PaymentRecognition", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.PaymentRecognitionEntityUsingPaymentJournalIDStatic, false, signalRelatedEntity, "PaymentJournal", false, new int[] { (int)PaymentJournalFieldIndex.PaymentJournalID } );
			_paymentRecognition = null;
		}
	
		/// <summary> setups the sync logic for member _paymentRecognition</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentRecognition(IEntityCore relatedEntity)
		{
			if(_paymentRecognition!=relatedEntity)
			{
				DesetupSyncPaymentRecognition(true, true);
				_paymentRecognition = (PaymentRecognitionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentRecognition, new PropertyChangedEventHandler( OnPaymentRecognitionPropertyChanged ), "PaymentRecognition", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.PaymentRecognitionEntityUsingPaymentJournalIDStatic, false, ref _alreadyFetchedPaymentRecognition, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentRecognitionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentReconciliation</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentReconciliation(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentReconciliation, new PropertyChangedEventHandler( OnPaymentReconciliationPropertyChanged ), "PaymentReconciliation", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.PaymentReconciliationEntityUsingPaymentJournalIDStatic, false, signalRelatedEntity, "PaymentJournal", false, new int[] { (int)PaymentJournalFieldIndex.PaymentJournalID } );
			_paymentReconciliation = null;
		}
	
		/// <summary> setups the sync logic for member _paymentReconciliation</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentReconciliation(IEntityCore relatedEntity)
		{
			if(_paymentReconciliation!=relatedEntity)
			{
				DesetupSyncPaymentReconciliation(true, true);
				_paymentReconciliation = (PaymentReconciliationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentReconciliation, new PropertyChangedEventHandler( OnPaymentReconciliationPropertyChanged ), "PaymentReconciliation", VarioSL.Entities.RelationClasses.StaticPaymentJournalRelations.PaymentReconciliationEntityUsingPaymentJournalIDStatic, false, ref _alreadyFetchedPaymentReconciliation, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentReconciliationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="paymentJournalID">PK value for PaymentJournal which data should be fetched into this PaymentJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 paymentJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PaymentJournalFieldIndex.PaymentJournalID].ForcedCurrentValueWrite(paymentJournalID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePaymentJournalDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PaymentJournalEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PaymentJournalRelations Relations
		{
			get	{ return new PaymentJournalRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BankStatement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBankStatements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BankStatementCollection(), (IEntityRelation)GetRelationsForField("BankStatements")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.BankStatementEntity, 0, null, null, null, "BankStatements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentJournalToComponent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentJournalToComponent
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection(), (IEntityRelation)GetRelationsForField("PaymentJournalToComponent")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.PaymentJournalToComponentEntity, 0, null, null, null, "PaymentJournalToComponent", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleViolationToTransaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleViolationToTransactions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection(), (IEntityRelation)GetRelationsForField("RuleViolationToTransactions")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.RuleViolationToTransactionEntity, 0, null, null, null, "RuleViolationToTransactions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SaleToComponent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSaleToComponents
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleToComponentCollection(), (IEntityRelation)GetRelationsForField("SaleToComponents")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.SaleToComponentEntity, 0, null, null, null, "SaleToComponents", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentOption
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("PaymentOption")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "PaymentOption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerson
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("Person")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "Person", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSale
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("Sale")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "Sale", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournal
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournal")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BankStatementVerification'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBankStatementVerification
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BankStatementVerificationCollection(), (IEntityRelation)GetRelationsForField("BankStatementVerification")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.BankStatementVerificationEntity, 0, null, null, null, "BankStatementVerification", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentRecognition'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentRecognition
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentRecognitionCollection(), (IEntityRelation)GetRelationsForField("PaymentRecognition")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.PaymentRecognitionEntity, 0, null, null, null, "PaymentRecognition", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentReconciliation'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentReconciliation
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentReconciliationCollection(), (IEntityRelation)GetRelationsForField("PaymentReconciliation")[0], (int)VarioSL.Entities.EntityType.PaymentJournalEntity, (int)VarioSL.Entities.EntityType.PaymentReconciliationEntity, 0, null, null, null, "PaymentReconciliation", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Amount
		{
			get { return (System.Int32)GetValue((int)PaymentJournalFieldIndex.Amount, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.Amount, value, true); }
		}

		/// <summary> The CardID property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PaymentJournalFieldIndex.CardID, false); }
			set	{ SetValue((int)PaymentJournalFieldIndex.CardID, value, true); }
		}

		/// <summary> The CashReceived property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."CASHRECEIVED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CashReceived
		{
			get { return (Nullable<System.Int32>)GetValue((int)PaymentJournalFieldIndex.CashReceived, false); }
			set	{ SetValue((int)PaymentJournalFieldIndex.CashReceived, value, true); }
		}

		/// <summary> The CashReturn property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."CASHRETURN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CashReturn
		{
			get { return (Nullable<System.Int32>)GetValue((int)PaymentJournalFieldIndex.CashReturn, false); }
			set	{ SetValue((int)PaymentJournalFieldIndex.CashReturn, value, true); }
		}

		/// <summary> The CheckBranchRouting property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."CHECKBRANCHROUTING"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CheckBranchRouting
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.CheckBranchRouting, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.CheckBranchRouting, value, true); }
		}

		/// <summary> The CheckCustomerIdentifier property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."CHECKCUSTOMERIDENTIFIER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CheckCustomerIdentifier
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.CheckCustomerIdentifier, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.CheckCustomerIdentifier, value, true); }
		}

		/// <summary> The CheckingAccount property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."CHECKINGACCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 64<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CheckingAccount
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.CheckingAccount, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.CheckingAccount, value, true); }
		}

		/// <summary> The CheckNumber property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."CHECKNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CheckNumber
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.CheckNumber, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.CheckNumber, value, true); }
		}

		/// <summary> The Code property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."CODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Code
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.Code, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.Code, value, true); }
		}

		/// <summary> The Confirmed property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."CONFIRMED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Confirmed
		{
			get { return (System.Int16)GetValue((int)PaymentJournalFieldIndex.Confirmed, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.Confirmed, value, true); }
		}

		/// <summary> The ContractID property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PaymentJournalFieldIndex.ContractID, false); }
			set	{ SetValue((int)PaymentJournalFieldIndex.ContractID, value, true); }
		}

		/// <summary> The CreditCardNetwork property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."CREDITCARDNETWORK"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.CreditCardNetwork> CreditCardNetwork
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.CreditCardNetwork>)GetValue((int)PaymentJournalFieldIndex.CreditCardNetwork, false); }
			set	{ SetValue((int)PaymentJournalFieldIndex.CreditCardNetwork, value, true); }
		}

		/// <summary> The EncryptedData property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."ENCRYPTEDDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): NClob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EncryptedData
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.EncryptedData, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.EncryptedData, value, true); }
		}

		/// <summary> The EncryptedDataType property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."ENCRYPTEDDATATYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.EncryptedDataType> EncryptedDataType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.EncryptedDataType>)GetValue((int)PaymentJournalFieldIndex.EncryptedDataType, false); }
			set	{ SetValue((int)PaymentJournalFieldIndex.EncryptedDataType, value, true); }
		}

		/// <summary> The ExecutionResult property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."EXECUTIONRESULT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExecutionResult
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.ExecutionResult, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.ExecutionResult, value, true); }
		}

		/// <summary> The ExecutionTime property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."EXECUTIONTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ExecutionTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentJournalFieldIndex.ExecutionTime, false); }
			set	{ SetValue((int)PaymentJournalFieldIndex.ExecutionTime, value, true); }
		}

		/// <summary> The IsPreTax property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."ISPRETAX"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsPreTax
		{
			get { return (System.Boolean)GetValue((int)PaymentJournalFieldIndex.IsPreTax, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.IsPreTax, value, true); }
		}

		/// <summary> The LastModified property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)PaymentJournalFieldIndex.LastModified, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.LastUser, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MaskedCheckingAccount property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."MASKEDCHECKINGACCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MaskedCheckingAccount
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.MaskedCheckingAccount, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.MaskedCheckingAccount, value, true); }
		}

		/// <summary> The MaskedPan property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."MASKEDPAN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MaskedPan
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.MaskedPan, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.MaskedPan, value, true); }
		}

		/// <summary> The PaymentJournalID property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PAYMENTJOURNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 PaymentJournalID
		{
			get { return (System.Int64)GetValue((int)PaymentJournalFieldIndex.PaymentJournalID, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.PaymentJournalID, value, true); }
		}

		/// <summary> The PaymentOptionID property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PAYMENTOPTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentOptionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PaymentJournalFieldIndex.PaymentOptionID, false); }
			set	{ SetValue((int)PaymentJournalFieldIndex.PaymentOptionID, value, true); }
		}

		/// <summary> The PaymentType property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PAYMENTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.DevicePaymentMethod PaymentType
		{
			get { return (VarioSL.Entities.Enumerations.DevicePaymentMethod)GetValue((int)PaymentJournalFieldIndex.PaymentType, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.PaymentType, value, true); }
		}

		/// <summary> The PayPalAuthorizationID property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PAYPALAUTHORIZATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PayPalAuthorizationID
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.PayPalAuthorizationID, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.PayPalAuthorizationID, value, true); }
		}

		/// <summary> The PayPalCaptureID property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PAYPALCAPTUREID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PayPalCaptureID
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.PayPalCaptureID, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.PayPalCaptureID, value, true); }
		}

		/// <summary> The PayPalCaptureRequestID property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PAYPALCAPTUREREQUESTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PayPalCaptureRequestID
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.PayPalCaptureRequestID, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.PayPalCaptureRequestID, value, true); }
		}

		/// <summary> The PaypalCaptureToken property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PAYPALCAPTURETOKEN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaypalCaptureToken
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.PaypalCaptureToken, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.PaypalCaptureToken, value, true); }
		}

		/// <summary> The PayPalRequestID property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PAYPALREQUESTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PayPalRequestID
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.PayPalRequestID, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.PayPalRequestID, value, true); }
		}

		/// <summary> The PayPalRequestToken property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PAYPALREQUESTTOKEN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PayPalRequestToken
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.PayPalRequestToken, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.PayPalRequestToken, value, true); }
		}

		/// <summary> The PersonID property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PersonID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PaymentJournalFieldIndex.PersonID, false); }
			set	{ SetValue((int)PaymentJournalFieldIndex.PersonID, value, true); }
		}

		/// <summary> The ProviderReference property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PROVIDERREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProviderReference
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.ProviderReference, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.ProviderReference, value, true); }
		}

		/// <summary> The ProviderReferenceExternal property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PROVIDERREFERENCEEXTERNAL"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProviderReferenceExternal
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.ProviderReferenceExternal, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.ProviderReferenceExternal, value, true); }
		}

		/// <summary> The PurchaseOrderNumber property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."PURCHASEORDERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PurchaseOrderNumber
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.PurchaseOrderNumber, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.PurchaseOrderNumber, value, true); }
		}

		/// <summary> The ReconciliationState property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."RECONCILIATIONSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.PaymentReconciliationState ReconciliationState
		{
			get { return (VarioSL.Entities.Enumerations.PaymentReconciliationState)GetValue((int)PaymentJournalFieldIndex.ReconciliationState, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.ReconciliationState, value, true); }
		}

		/// <summary> The ReferenceNumber property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."REFERENCENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReferenceNumber
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.ReferenceNumber, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.ReferenceNumber, value, true); }
		}

		/// <summary> The SaleID property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."SALEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SaleID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PaymentJournalFieldIndex.SaleID, false); }
			set	{ SetValue((int)PaymentJournalFieldIndex.SaleID, value, true); }
		}

		/// <summary> The State property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> State
		{
			get { return (Nullable<System.Int32>)GetValue((int)PaymentJournalFieldIndex.State, false); }
			set	{ SetValue((int)PaymentJournalFieldIndex.State, value, true); }
		}

		/// <summary> The Token property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."TOKEN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Token
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.Token, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.Token, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)PaymentJournalFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransactionJournalID property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."TRANSACTIONJOURNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionJournalID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PaymentJournalFieldIndex.TransactionJournalID, false); }
			set	{ SetValue((int)PaymentJournalFieldIndex.TransactionJournalID, value, true); }
		}

		/// <summary> The WalletTransactionID property of the Entity PaymentJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PAYMENTJOURNAL"."WALLETTRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WalletTransactionID
		{
			get { return (System.String)GetValue((int)PaymentJournalFieldIndex.WalletTransactionID, true); }
			set	{ SetValue((int)PaymentJournalFieldIndex.WalletTransactionID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'BankStatementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBankStatements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BankStatementCollection BankStatements
		{
			get	{ return GetMultiBankStatements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BankStatements. When set to true, BankStatements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BankStatements is accessed. You can always execute/ a forced fetch by calling GetMultiBankStatements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBankStatements
		{
			get	{ return _alwaysFetchBankStatements; }
			set	{ _alwaysFetchBankStatements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BankStatements already has been fetched. Setting this property to false when BankStatements has been fetched
		/// will clear the BankStatements collection well. Setting this property to true while BankStatements hasn't been fetched disables lazy loading for BankStatements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBankStatements
		{
			get { return _alreadyFetchedBankStatements;}
			set 
			{
				if(_alreadyFetchedBankStatements && !value && (_bankStatements != null))
				{
					_bankStatements.Clear();
				}
				_alreadyFetchedBankStatements = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentJournalToComponentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentJournalToComponent()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection PaymentJournalToComponent
		{
			get	{ return GetMultiPaymentJournalToComponent(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentJournalToComponent. When set to true, PaymentJournalToComponent is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentJournalToComponent is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentJournalToComponent(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentJournalToComponent
		{
			get	{ return _alwaysFetchPaymentJournalToComponent; }
			set	{ _alwaysFetchPaymentJournalToComponent = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentJournalToComponent already has been fetched. Setting this property to false when PaymentJournalToComponent has been fetched
		/// will clear the PaymentJournalToComponent collection well. Setting this property to true while PaymentJournalToComponent hasn't been fetched disables lazy loading for PaymentJournalToComponent</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentJournalToComponent
		{
			get { return _alreadyFetchedPaymentJournalToComponent;}
			set 
			{
				if(_alreadyFetchedPaymentJournalToComponent && !value && (_paymentJournalToComponent != null))
				{
					_paymentJournalToComponent.Clear();
				}
				_alreadyFetchedPaymentJournalToComponent = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleViolationToTransactions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection RuleViolationToTransactions
		{
			get	{ return GetMultiRuleViolationToTransactions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleViolationToTransactions. When set to true, RuleViolationToTransactions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleViolationToTransactions is accessed. You can always execute/ a forced fetch by calling GetMultiRuleViolationToTransactions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleViolationToTransactions
		{
			get	{ return _alwaysFetchRuleViolationToTransactions; }
			set	{ _alwaysFetchRuleViolationToTransactions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleViolationToTransactions already has been fetched. Setting this property to false when RuleViolationToTransactions has been fetched
		/// will clear the RuleViolationToTransactions collection well. Setting this property to true while RuleViolationToTransactions hasn't been fetched disables lazy loading for RuleViolationToTransactions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleViolationToTransactions
		{
			get { return _alreadyFetchedRuleViolationToTransactions;}
			set 
			{
				if(_alreadyFetchedRuleViolationToTransactions && !value && (_ruleViolationToTransactions != null))
				{
					_ruleViolationToTransactions.Clear();
				}
				_alreadyFetchedRuleViolationToTransactions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SaleToComponentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSaleToComponents()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SaleToComponentCollection SaleToComponents
		{
			get	{ return GetMultiSaleToComponents(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SaleToComponents. When set to true, SaleToComponents is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SaleToComponents is accessed. You can always execute/ a forced fetch by calling GetMultiSaleToComponents(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSaleToComponents
		{
			get	{ return _alwaysFetchSaleToComponents; }
			set	{ _alwaysFetchSaleToComponents = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SaleToComponents already has been fetched. Setting this property to false when SaleToComponents has been fetched
		/// will clear the SaleToComponents collection well. Setting this property to true while SaleToComponents hasn't been fetched disables lazy loading for SaleToComponents</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSaleToComponents
		{
			get { return _alreadyFetchedSaleToComponents;}
			set 
			{
				if(_alreadyFetchedSaleToComponents && !value && (_saleToComponents != null))
				{
					_saleToComponents.Clear();
				}
				_alreadyFetchedSaleToComponents = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentJournals", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentJournals", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentOptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentOption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentOptionEntity PaymentOption
		{
			get	{ return GetSinglePaymentOption(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentOption(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentJournals", "PaymentOption", _paymentOption, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentOption. When set to true, PaymentOption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentOption is accessed. You can always execute a forced fetch by calling GetSinglePaymentOption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentOption
		{
			get	{ return _alwaysFetchPaymentOption; }
			set	{ _alwaysFetchPaymentOption = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentOption already has been fetched. Setting this property to false when PaymentOption has been fetched
		/// will set PaymentOption to null as well. Setting this property to true while PaymentOption hasn't been fetched disables lazy loading for PaymentOption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentOption
		{
			get { return _alreadyFetchedPaymentOption;}
			set 
			{
				if(_alreadyFetchedPaymentOption && !value)
				{
					this.PaymentOption = null;
				}
				_alreadyFetchedPaymentOption = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentOption is not found
		/// in the database. When set to true, PaymentOption will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentOptionReturnsNewIfNotFound
		{
			get	{ return _paymentOptionReturnsNewIfNotFound; }
			set { _paymentOptionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity Person
		{
			get	{ return GetSinglePerson(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPerson(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentJournals", "Person", _person, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Person. When set to true, Person is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Person is accessed. You can always execute a forced fetch by calling GetSinglePerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerson
		{
			get	{ return _alwaysFetchPerson; }
			set	{ _alwaysFetchPerson = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Person already has been fetched. Setting this property to false when Person has been fetched
		/// will set Person to null as well. Setting this property to true while Person hasn't been fetched disables lazy loading for Person</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerson
		{
			get { return _alreadyFetchedPerson;}
			set 
			{
				if(_alreadyFetchedPerson && !value)
				{
					this.Person = null;
				}
				_alreadyFetchedPerson = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Person is not found
		/// in the database. When set to true, Person will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PersonReturnsNewIfNotFound
		{
			get	{ return _personReturnsNewIfNotFound; }
			set { _personReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SaleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSale()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SaleEntity Sale
		{
			get	{ return GetSingleSale(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSale(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentJournals", "Sale", _sale, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Sale. When set to true, Sale is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Sale is accessed. You can always execute a forced fetch by calling GetSingleSale(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSale
		{
			get	{ return _alwaysFetchSale; }
			set	{ _alwaysFetchSale = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Sale already has been fetched. Setting this property to false when Sale has been fetched
		/// will set Sale to null as well. Setting this property to true while Sale hasn't been fetched disables lazy loading for Sale</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSale
		{
			get { return _alreadyFetchedSale;}
			set 
			{
				if(_alreadyFetchedSale && !value)
				{
					this.Sale = null;
				}
				_alreadyFetchedSale = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Sale is not found
		/// in the database. When set to true, Sale will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SaleReturnsNewIfNotFound
		{
			get	{ return _saleReturnsNewIfNotFound; }
			set { _saleReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TransactionJournalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTransactionJournal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TransactionJournalEntity TransactionJournal
		{
			get	{ return GetSingleTransactionJournal(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTransactionJournal(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentJournals", "TransactionJournal", _transactionJournal, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournal. When set to true, TransactionJournal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournal is accessed. You can always execute a forced fetch by calling GetSingleTransactionJournal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournal
		{
			get	{ return _alwaysFetchTransactionJournal; }
			set	{ _alwaysFetchTransactionJournal = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournal already has been fetched. Setting this property to false when TransactionJournal has been fetched
		/// will set TransactionJournal to null as well. Setting this property to true while TransactionJournal hasn't been fetched disables lazy loading for TransactionJournal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournal
		{
			get { return _alreadyFetchedTransactionJournal;}
			set 
			{
				if(_alreadyFetchedTransactionJournal && !value)
				{
					this.TransactionJournal = null;
				}
				_alreadyFetchedTransactionJournal = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TransactionJournal is not found
		/// in the database. When set to true, TransactionJournal will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TransactionJournalReturnsNewIfNotFound
		{
			get	{ return _transactionJournalReturnsNewIfNotFound; }
			set { _transactionJournalReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BankStatementVerificationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBankStatementVerification()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BankStatementVerificationEntity BankStatementVerification
		{
			get	{ return GetSingleBankStatementVerification(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncBankStatementVerification(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_bankStatementVerification !=null);
						DesetupSyncBankStatementVerification(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("BankStatementVerification");
						}
					}
					else
					{
						if(_bankStatementVerification!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "PaymentJournal");
							SetupSyncBankStatementVerification(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BankStatementVerification. When set to true, BankStatementVerification is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BankStatementVerification is accessed. You can always execute a forced fetch by calling GetSingleBankStatementVerification(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBankStatementVerification
		{
			get	{ return _alwaysFetchBankStatementVerification; }
			set	{ _alwaysFetchBankStatementVerification = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property BankStatementVerification already has been fetched. Setting this property to false when BankStatementVerification has been fetched
		/// will set BankStatementVerification to null as well. Setting this property to true while BankStatementVerification hasn't been fetched disables lazy loading for BankStatementVerification</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBankStatementVerification
		{
			get { return _alreadyFetchedBankStatementVerification;}
			set 
			{
				if(_alreadyFetchedBankStatementVerification && !value)
				{
					this.BankStatementVerification = null;
				}
				_alreadyFetchedBankStatementVerification = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BankStatementVerification is not found
		/// in the database. When set to true, BankStatementVerification will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BankStatementVerificationReturnsNewIfNotFound
		{
			get	{ return _bankStatementVerificationReturnsNewIfNotFound; }
			set	{ _bankStatementVerificationReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'PaymentRecognitionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentRecognition()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentRecognitionEntity PaymentRecognition
		{
			get	{ return GetSinglePaymentRecognition(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPaymentRecognition(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_paymentRecognition !=null);
						DesetupSyncPaymentRecognition(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("PaymentRecognition");
						}
					}
					else
					{
						if(_paymentRecognition!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "PaymentJournal");
							SetupSyncPaymentRecognition(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentRecognition. When set to true, PaymentRecognition is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentRecognition is accessed. You can always execute a forced fetch by calling GetSinglePaymentRecognition(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentRecognition
		{
			get	{ return _alwaysFetchPaymentRecognition; }
			set	{ _alwaysFetchPaymentRecognition = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentRecognition already has been fetched. Setting this property to false when PaymentRecognition has been fetched
		/// will set PaymentRecognition to null as well. Setting this property to true while PaymentRecognition hasn't been fetched disables lazy loading for PaymentRecognition</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentRecognition
		{
			get { return _alreadyFetchedPaymentRecognition;}
			set 
			{
				if(_alreadyFetchedPaymentRecognition && !value)
				{
					this.PaymentRecognition = null;
				}
				_alreadyFetchedPaymentRecognition = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentRecognition is not found
		/// in the database. When set to true, PaymentRecognition will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentRecognitionReturnsNewIfNotFound
		{
			get	{ return _paymentRecognitionReturnsNewIfNotFound; }
			set	{ _paymentRecognitionReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'PaymentReconciliationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentReconciliation()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentReconciliationEntity PaymentReconciliation
		{
			get	{ return GetSinglePaymentReconciliation(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPaymentReconciliation(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_paymentReconciliation !=null);
						DesetupSyncPaymentReconciliation(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("PaymentReconciliation");
						}
					}
					else
					{
						if(_paymentReconciliation!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "PaymentJournal");
							SetupSyncPaymentReconciliation(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentReconciliation. When set to true, PaymentReconciliation is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentReconciliation is accessed. You can always execute a forced fetch by calling GetSinglePaymentReconciliation(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentReconciliation
		{
			get	{ return _alwaysFetchPaymentReconciliation; }
			set	{ _alwaysFetchPaymentReconciliation = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentReconciliation already has been fetched. Setting this property to false when PaymentReconciliation has been fetched
		/// will set PaymentReconciliation to null as well. Setting this property to true while PaymentReconciliation hasn't been fetched disables lazy loading for PaymentReconciliation</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentReconciliation
		{
			get { return _alreadyFetchedPaymentReconciliation;}
			set 
			{
				if(_alreadyFetchedPaymentReconciliation && !value)
				{
					this.PaymentReconciliation = null;
				}
				_alreadyFetchedPaymentReconciliation = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentReconciliation is not found
		/// in the database. When set to true, PaymentReconciliation will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentReconciliationReturnsNewIfNotFound
		{
			get	{ return _paymentReconciliationReturnsNewIfNotFound; }
			set	{ _paymentReconciliationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PaymentJournalEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
