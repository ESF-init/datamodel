﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Report'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ReportEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ReportDataFileCollection	_reportDataFiles;
		private bool	_alwaysFetchReportDataFiles, _alreadyFetchedReportDataFiles;
		private VarioSL.Entities.CollectionClasses.ReportJobCollection	_reportJobs;
		private bool	_alwaysFetchReportJobs, _alreadyFetchedReportJobs;
		private VarioSL.Entities.CollectionClasses.ReportJobResultCollection	_reportJobResults;
		private bool	_alwaysFetchReportJobResults, _alreadyFetchedReportJobResults;
		private VarioSL.Entities.CollectionClasses.ReportToClientCollection	_reportToClients;
		private bool	_alwaysFetchReportToClients, _alreadyFetchedReportToClients;
		private VarioSL.Entities.CollectionClasses.ClientCollection _clients;
		private bool	_alwaysFetchClients, _alreadyFetchedClients;
		private UserResourceEntity _userResource;
		private bool	_alwaysFetchUserResource, _alreadyFetchedUserResource, _userResourceReturnsNewIfNotFound;
		private FilterListEntity _filterList;
		private bool	_alwaysFetchFilterList, _alreadyFetchedFilterList, _filterListReturnsNewIfNotFound;
		private ReportCategoryEntity _reportCategory;
		private bool	_alwaysFetchReportCategory, _alreadyFetchedReportCategory, _reportCategoryReturnsNewIfNotFound;
		private ReportDataFileEntity _defaultReportDataFile;
		private bool	_alwaysFetchDefaultReportDataFile, _alreadyFetchedDefaultReportDataFile, _defaultReportDataFileReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserResource</summary>
			public static readonly string UserResource = "UserResource";
			/// <summary>Member name FilterList</summary>
			public static readonly string FilterList = "FilterList";
			/// <summary>Member name ReportCategory</summary>
			public static readonly string ReportCategory = "ReportCategory";
			/// <summary>Member name DefaultReportDataFile</summary>
			public static readonly string DefaultReportDataFile = "DefaultReportDataFile";
			/// <summary>Member name ReportDataFiles</summary>
			public static readonly string ReportDataFiles = "ReportDataFiles";
			/// <summary>Member name ReportJobs</summary>
			public static readonly string ReportJobs = "ReportJobs";
			/// <summary>Member name ReportJobResults</summary>
			public static readonly string ReportJobResults = "ReportJobResults";
			/// <summary>Member name ReportToClients</summary>
			public static readonly string ReportToClients = "ReportToClients";
			/// <summary>Member name Clients</summary>
			public static readonly string Clients = "Clients";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ReportEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ReportEntity() :base("ReportEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="reportID">PK value for Report which data should be fetched into this Report object</param>
		public ReportEntity(System.Int64 reportID):base("ReportEntity")
		{
			InitClassFetch(reportID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="reportID">PK value for Report which data should be fetched into this Report object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ReportEntity(System.Int64 reportID, IPrefetchPath prefetchPathToUse):base("ReportEntity")
		{
			InitClassFetch(reportID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="reportID">PK value for Report which data should be fetched into this Report object</param>
		/// <param name="validator">The custom validator object for this ReportEntity</param>
		public ReportEntity(System.Int64 reportID, IValidator validator):base("ReportEntity")
		{
			InitClassFetch(reportID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReportEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_reportDataFiles = (VarioSL.Entities.CollectionClasses.ReportDataFileCollection)info.GetValue("_reportDataFiles", typeof(VarioSL.Entities.CollectionClasses.ReportDataFileCollection));
			_alwaysFetchReportDataFiles = info.GetBoolean("_alwaysFetchReportDataFiles");
			_alreadyFetchedReportDataFiles = info.GetBoolean("_alreadyFetchedReportDataFiles");

			_reportJobs = (VarioSL.Entities.CollectionClasses.ReportJobCollection)info.GetValue("_reportJobs", typeof(VarioSL.Entities.CollectionClasses.ReportJobCollection));
			_alwaysFetchReportJobs = info.GetBoolean("_alwaysFetchReportJobs");
			_alreadyFetchedReportJobs = info.GetBoolean("_alreadyFetchedReportJobs");

			_reportJobResults = (VarioSL.Entities.CollectionClasses.ReportJobResultCollection)info.GetValue("_reportJobResults", typeof(VarioSL.Entities.CollectionClasses.ReportJobResultCollection));
			_alwaysFetchReportJobResults = info.GetBoolean("_alwaysFetchReportJobResults");
			_alreadyFetchedReportJobResults = info.GetBoolean("_alreadyFetchedReportJobResults");

			_reportToClients = (VarioSL.Entities.CollectionClasses.ReportToClientCollection)info.GetValue("_reportToClients", typeof(VarioSL.Entities.CollectionClasses.ReportToClientCollection));
			_alwaysFetchReportToClients = info.GetBoolean("_alwaysFetchReportToClients");
			_alreadyFetchedReportToClients = info.GetBoolean("_alreadyFetchedReportToClients");
			_clients = (VarioSL.Entities.CollectionClasses.ClientCollection)info.GetValue("_clients", typeof(VarioSL.Entities.CollectionClasses.ClientCollection));
			_alwaysFetchClients = info.GetBoolean("_alwaysFetchClients");
			_alreadyFetchedClients = info.GetBoolean("_alreadyFetchedClients");
			_userResource = (UserResourceEntity)info.GetValue("_userResource", typeof(UserResourceEntity));
			if(_userResource!=null)
			{
				_userResource.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userResourceReturnsNewIfNotFound = info.GetBoolean("_userResourceReturnsNewIfNotFound");
			_alwaysFetchUserResource = info.GetBoolean("_alwaysFetchUserResource");
			_alreadyFetchedUserResource = info.GetBoolean("_alreadyFetchedUserResource");

			_filterList = (FilterListEntity)info.GetValue("_filterList", typeof(FilterListEntity));
			if(_filterList!=null)
			{
				_filterList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_filterListReturnsNewIfNotFound = info.GetBoolean("_filterListReturnsNewIfNotFound");
			_alwaysFetchFilterList = info.GetBoolean("_alwaysFetchFilterList");
			_alreadyFetchedFilterList = info.GetBoolean("_alreadyFetchedFilterList");

			_reportCategory = (ReportCategoryEntity)info.GetValue("_reportCategory", typeof(ReportCategoryEntity));
			if(_reportCategory!=null)
			{
				_reportCategory.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_reportCategoryReturnsNewIfNotFound = info.GetBoolean("_reportCategoryReturnsNewIfNotFound");
			_alwaysFetchReportCategory = info.GetBoolean("_alwaysFetchReportCategory");
			_alreadyFetchedReportCategory = info.GetBoolean("_alreadyFetchedReportCategory");

			_defaultReportDataFile = (ReportDataFileEntity)info.GetValue("_defaultReportDataFile", typeof(ReportDataFileEntity));
			if(_defaultReportDataFile!=null)
			{
				_defaultReportDataFile.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_defaultReportDataFileReturnsNewIfNotFound = info.GetBoolean("_defaultReportDataFileReturnsNewIfNotFound");
			_alwaysFetchDefaultReportDataFile = info.GetBoolean("_alwaysFetchDefaultReportDataFile");
			_alreadyFetchedDefaultReportDataFile = info.GetBoolean("_alreadyFetchedDefaultReportDataFile");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ReportFieldIndex)fieldIndex)
			{
				case ReportFieldIndex.CategoryID:
					DesetupSyncReportCategory(true, false);
					_alreadyFetchedReportCategory = false;
					break;
				case ReportFieldIndex.DataFileID:
					DesetupSyncDefaultReportDataFile(true, false);
					_alreadyFetchedDefaultReportDataFile = false;
					break;
				case ReportFieldIndex.FilterListID:
					DesetupSyncFilterList(true, false);
					_alreadyFetchedFilterList = false;
					break;
				case ReportFieldIndex.ResourceID:
					DesetupSyncUserResource(true, false);
					_alreadyFetchedUserResource = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedReportDataFiles = (_reportDataFiles.Count > 0);
			_alreadyFetchedReportJobs = (_reportJobs.Count > 0);
			_alreadyFetchedReportJobResults = (_reportJobResults.Count > 0);
			_alreadyFetchedReportToClients = (_reportToClients.Count > 0);
			_alreadyFetchedClients = (_clients.Count > 0);
			_alreadyFetchedUserResource = (_userResource != null);
			_alreadyFetchedFilterList = (_filterList != null);
			_alreadyFetchedReportCategory = (_reportCategory != null);
			_alreadyFetchedDefaultReportDataFile = (_defaultReportDataFile != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "UserResource":
					toReturn.Add(Relations.UserResourceEntityUsingResourceID);
					break;
				case "FilterList":
					toReturn.Add(Relations.FilterListEntityUsingFilterListID);
					break;
				case "ReportCategory":
					toReturn.Add(Relations.ReportCategoryEntityUsingCategoryID);
					break;
				case "DefaultReportDataFile":
					toReturn.Add(Relations.ReportDataFileEntityUsingDataFileID);
					break;
				case "ReportDataFiles":
					toReturn.Add(Relations.ReportDataFileEntityUsingReportID);
					break;
				case "ReportJobs":
					toReturn.Add(Relations.ReportJobEntityUsingReportID);
					break;
				case "ReportJobResults":
					toReturn.Add(Relations.ReportJobResultEntityUsingReportID);
					break;
				case "ReportToClients":
					toReturn.Add(Relations.ReportToClientEntityUsingReportID);
					break;
				case "Clients":
					toReturn.Add(Relations.ReportToClientEntityUsingReportID, "ReportEntity__", "ReportToClient_", JoinHint.None);
					toReturn.Add(ReportToClientEntity.Relations.ClientEntityUsingClientID, "ReportToClient_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_reportDataFiles", (!this.MarkedForDeletion?_reportDataFiles:null));
			info.AddValue("_alwaysFetchReportDataFiles", _alwaysFetchReportDataFiles);
			info.AddValue("_alreadyFetchedReportDataFiles", _alreadyFetchedReportDataFiles);
			info.AddValue("_reportJobs", (!this.MarkedForDeletion?_reportJobs:null));
			info.AddValue("_alwaysFetchReportJobs", _alwaysFetchReportJobs);
			info.AddValue("_alreadyFetchedReportJobs", _alreadyFetchedReportJobs);
			info.AddValue("_reportJobResults", (!this.MarkedForDeletion?_reportJobResults:null));
			info.AddValue("_alwaysFetchReportJobResults", _alwaysFetchReportJobResults);
			info.AddValue("_alreadyFetchedReportJobResults", _alreadyFetchedReportJobResults);
			info.AddValue("_reportToClients", (!this.MarkedForDeletion?_reportToClients:null));
			info.AddValue("_alwaysFetchReportToClients", _alwaysFetchReportToClients);
			info.AddValue("_alreadyFetchedReportToClients", _alreadyFetchedReportToClients);
			info.AddValue("_clients", (!this.MarkedForDeletion?_clients:null));
			info.AddValue("_alwaysFetchClients", _alwaysFetchClients);
			info.AddValue("_alreadyFetchedClients", _alreadyFetchedClients);
			info.AddValue("_userResource", (!this.MarkedForDeletion?_userResource:null));
			info.AddValue("_userResourceReturnsNewIfNotFound", _userResourceReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserResource", _alwaysFetchUserResource);
			info.AddValue("_alreadyFetchedUserResource", _alreadyFetchedUserResource);
			info.AddValue("_filterList", (!this.MarkedForDeletion?_filterList:null));
			info.AddValue("_filterListReturnsNewIfNotFound", _filterListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFilterList", _alwaysFetchFilterList);
			info.AddValue("_alreadyFetchedFilterList", _alreadyFetchedFilterList);
			info.AddValue("_reportCategory", (!this.MarkedForDeletion?_reportCategory:null));
			info.AddValue("_reportCategoryReturnsNewIfNotFound", _reportCategoryReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReportCategory", _alwaysFetchReportCategory);
			info.AddValue("_alreadyFetchedReportCategory", _alreadyFetchedReportCategory);
			info.AddValue("_defaultReportDataFile", (!this.MarkedForDeletion?_defaultReportDataFile:null));
			info.AddValue("_defaultReportDataFileReturnsNewIfNotFound", _defaultReportDataFileReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDefaultReportDataFile", _alwaysFetchDefaultReportDataFile);
			info.AddValue("_alreadyFetchedDefaultReportDataFile", _alreadyFetchedDefaultReportDataFile);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "UserResource":
					_alreadyFetchedUserResource = true;
					this.UserResource = (UserResourceEntity)entity;
					break;
				case "FilterList":
					_alreadyFetchedFilterList = true;
					this.FilterList = (FilterListEntity)entity;
					break;
				case "ReportCategory":
					_alreadyFetchedReportCategory = true;
					this.ReportCategory = (ReportCategoryEntity)entity;
					break;
				case "DefaultReportDataFile":
					_alreadyFetchedDefaultReportDataFile = true;
					this.DefaultReportDataFile = (ReportDataFileEntity)entity;
					break;
				case "ReportDataFiles":
					_alreadyFetchedReportDataFiles = true;
					if(entity!=null)
					{
						this.ReportDataFiles.Add((ReportDataFileEntity)entity);
					}
					break;
				case "ReportJobs":
					_alreadyFetchedReportJobs = true;
					if(entity!=null)
					{
						this.ReportJobs.Add((ReportJobEntity)entity);
					}
					break;
				case "ReportJobResults":
					_alreadyFetchedReportJobResults = true;
					if(entity!=null)
					{
						this.ReportJobResults.Add((ReportJobResultEntity)entity);
					}
					break;
				case "ReportToClients":
					_alreadyFetchedReportToClients = true;
					if(entity!=null)
					{
						this.ReportToClients.Add((ReportToClientEntity)entity);
					}
					break;
				case "Clients":
					_alreadyFetchedClients = true;
					if(entity!=null)
					{
						this.Clients.Add((ClientEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "UserResource":
					SetupSyncUserResource(relatedEntity);
					break;
				case "FilterList":
					SetupSyncFilterList(relatedEntity);
					break;
				case "ReportCategory":
					SetupSyncReportCategory(relatedEntity);
					break;
				case "DefaultReportDataFile":
					SetupSyncDefaultReportDataFile(relatedEntity);
					break;
				case "ReportDataFiles":
					_reportDataFiles.Add((ReportDataFileEntity)relatedEntity);
					break;
				case "ReportJobs":
					_reportJobs.Add((ReportJobEntity)relatedEntity);
					break;
				case "ReportJobResults":
					_reportJobResults.Add((ReportJobResultEntity)relatedEntity);
					break;
				case "ReportToClients":
					_reportToClients.Add((ReportToClientEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "UserResource":
					DesetupSyncUserResource(false, true);
					break;
				case "FilterList":
					DesetupSyncFilterList(false, true);
					break;
				case "ReportCategory":
					DesetupSyncReportCategory(false, true);
					break;
				case "DefaultReportDataFile":
					DesetupSyncDefaultReportDataFile(false, true);
					break;
				case "ReportDataFiles":
					this.PerformRelatedEntityRemoval(_reportDataFiles, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReportJobs":
					this.PerformRelatedEntityRemoval(_reportJobs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReportJobResults":
					this.PerformRelatedEntityRemoval(_reportJobResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReportToClients":
					this.PerformRelatedEntityRemoval(_reportToClients, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_userResource!=null)
			{
				toReturn.Add(_userResource);
			}
			if(_filterList!=null)
			{
				toReturn.Add(_filterList);
			}
			if(_reportCategory!=null)
			{
				toReturn.Add(_reportCategory);
			}
			if(_defaultReportDataFile!=null)
			{
				toReturn.Add(_defaultReportDataFile);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_reportDataFiles);
			toReturn.Add(_reportJobs);
			toReturn.Add(_reportJobResults);
			toReturn.Add(_reportToClients);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportID">PK value for Report which data should be fetched into this Report object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportID)
		{
			return FetchUsingPK(reportID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportID">PK value for Report which data should be fetched into this Report object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(reportID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportID">PK value for Report which data should be fetched into this Report object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(reportID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportID">PK value for Report which data should be fetched into this Report object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(reportID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ReportID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ReportRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ReportDataFileEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportDataFileEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportDataFileCollection GetMultiReportDataFiles(bool forceFetch)
		{
			return GetMultiReportDataFiles(forceFetch, _reportDataFiles.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportDataFileEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportDataFileEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportDataFileCollection GetMultiReportDataFiles(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportDataFiles(forceFetch, _reportDataFiles.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportDataFileEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportDataFileCollection GetMultiReportDataFiles(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportDataFiles(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportDataFileEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportDataFileCollection GetMultiReportDataFiles(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportDataFiles || forceFetch || _alwaysFetchReportDataFiles) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportDataFiles);
				_reportDataFiles.SuppressClearInGetMulti=!forceFetch;
				_reportDataFiles.EntityFactoryToUse = entityFactoryToUse;
				_reportDataFiles.GetMultiManyToOne(this, filter);
				_reportDataFiles.SuppressClearInGetMulti=false;
				_alreadyFetchedReportDataFiles = true;
			}
			return _reportDataFiles;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportDataFiles'. These settings will be taken into account
		/// when the property ReportDataFiles is requested or GetMultiReportDataFiles is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportDataFiles(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportDataFiles.SortClauses=sortClauses;
			_reportDataFiles.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch)
		{
			return GetMultiReportJobs(forceFetch, _reportJobs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportJobs(forceFetch, _reportJobs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportJobs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportJobs || forceFetch || _alwaysFetchReportJobs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportJobs);
				_reportJobs.SuppressClearInGetMulti=!forceFetch;
				_reportJobs.EntityFactoryToUse = entityFactoryToUse;
				_reportJobs.GetMultiManyToOne(null, null, null, this, filter);
				_reportJobs.SuppressClearInGetMulti=false;
				_alreadyFetchedReportJobs = true;
			}
			return _reportJobs;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportJobs'. These settings will be taken into account
		/// when the property ReportJobs is requested or GetMultiReportJobs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportJobs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportJobs.SortClauses=sortClauses;
			_reportJobs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobResultCollection GetMultiReportJobResults(bool forceFetch)
		{
			return GetMultiReportJobResults(forceFetch, _reportJobResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobResultCollection GetMultiReportJobResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportJobResults(forceFetch, _reportJobResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobResultCollection GetMultiReportJobResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportJobResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobResultCollection GetMultiReportJobResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportJobResults || forceFetch || _alwaysFetchReportJobResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportJobResults);
				_reportJobResults.SuppressClearInGetMulti=!forceFetch;
				_reportJobResults.EntityFactoryToUse = entityFactoryToUse;
				_reportJobResults.GetMultiManyToOne(null, this, null, filter);
				_reportJobResults.SuppressClearInGetMulti=false;
				_alreadyFetchedReportJobResults = true;
			}
			return _reportJobResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportJobResults'. These settings will be taken into account
		/// when the property ReportJobResults is requested or GetMultiReportJobResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportJobResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportJobResults.SortClauses=sortClauses;
			_reportJobResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportToClientEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportToClientCollection GetMultiReportToClients(bool forceFetch)
		{
			return GetMultiReportToClients(forceFetch, _reportToClients.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportToClientEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportToClientCollection GetMultiReportToClients(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportToClients(forceFetch, _reportToClients.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportToClientCollection GetMultiReportToClients(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportToClients(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportToClientCollection GetMultiReportToClients(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportToClients || forceFetch || _alwaysFetchReportToClients) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportToClients);
				_reportToClients.SuppressClearInGetMulti=!forceFetch;
				_reportToClients.EntityFactoryToUse = entityFactoryToUse;
				_reportToClients.GetMultiManyToOne(null, null, null, this, filter);
				_reportToClients.SuppressClearInGetMulti=false;
				_alreadyFetchedReportToClients = true;
			}
			return _reportToClients;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportToClients'. These settings will be taken into account
		/// when the property ReportToClients is requested or GetMultiReportToClients is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportToClients(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportToClients.SortClauses=sortClauses;
			_reportToClients.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClientCollection GetMultiClients(bool forceFetch)
		{
			return GetMultiClients(forceFetch, _clients.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClientCollection GetMultiClients(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedClients || forceFetch || _alwaysFetchClients) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clients);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ReportFields.ReportID, ComparisonOperator.Equal, this.ReportID, "ReportEntity__"));
				_clients.SuppressClearInGetMulti=!forceFetch;
				_clients.EntityFactoryToUse = entityFactoryToUse;
				_clients.GetMulti(filter, GetRelationsForField("Clients"));
				_clients.SuppressClearInGetMulti=false;
				_alreadyFetchedClients = true;
			}
			return _clients;
		}

		/// <summary> Sets the collection parameters for the collection for 'Clients'. These settings will be taken into account
		/// when the property Clients is requested or GetMultiClients is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClients(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clients.SortClauses=sortClauses;
			_clients.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'UserResourceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserResourceEntity' which is related to this entity.</returns>
		public UserResourceEntity GetSingleUserResource()
		{
			return GetSingleUserResource(false);
		}

		/// <summary> Retrieves the related entity of type 'UserResourceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserResourceEntity' which is related to this entity.</returns>
		public virtual UserResourceEntity GetSingleUserResource(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserResource || forceFetch || _alwaysFetchUserResource) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserResourceEntityUsingResourceID);
				UserResourceEntity newEntity = new UserResourceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ResourceID);
				}
				if(fetchResult)
				{
					newEntity = (UserResourceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userResourceReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserResource = newEntity;
				_alreadyFetchedUserResource = fetchResult;
			}
			return _userResource;
		}


		/// <summary> Retrieves the related entity of type 'FilterListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FilterListEntity' which is related to this entity.</returns>
		public FilterListEntity GetSingleFilterList()
		{
			return GetSingleFilterList(false);
		}

		/// <summary> Retrieves the related entity of type 'FilterListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FilterListEntity' which is related to this entity.</returns>
		public virtual FilterListEntity GetSingleFilterList(bool forceFetch)
		{
			if( ( !_alreadyFetchedFilterList || forceFetch || _alwaysFetchFilterList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FilterListEntityUsingFilterListID);
				FilterListEntity newEntity = new FilterListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FilterListID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FilterListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_filterListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FilterList = newEntity;
				_alreadyFetchedFilterList = fetchResult;
			}
			return _filterList;
		}


		/// <summary> Retrieves the related entity of type 'ReportCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReportCategoryEntity' which is related to this entity.</returns>
		public ReportCategoryEntity GetSingleReportCategory()
		{
			return GetSingleReportCategory(false);
		}

		/// <summary> Retrieves the related entity of type 'ReportCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReportCategoryEntity' which is related to this entity.</returns>
		public virtual ReportCategoryEntity GetSingleReportCategory(bool forceFetch)
		{
			if( ( !_alreadyFetchedReportCategory || forceFetch || _alwaysFetchReportCategory) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReportCategoryEntityUsingCategoryID);
				ReportCategoryEntity newEntity = new ReportCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CategoryID);
				}
				if(fetchResult)
				{
					newEntity = (ReportCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_reportCategoryReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReportCategory = newEntity;
				_alreadyFetchedReportCategory = fetchResult;
			}
			return _reportCategory;
		}


		/// <summary> Retrieves the related entity of type 'ReportDataFileEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReportDataFileEntity' which is related to this entity.</returns>
		public ReportDataFileEntity GetSingleDefaultReportDataFile()
		{
			return GetSingleDefaultReportDataFile(false);
		}

		/// <summary> Retrieves the related entity of type 'ReportDataFileEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReportDataFileEntity' which is related to this entity.</returns>
		public virtual ReportDataFileEntity GetSingleDefaultReportDataFile(bool forceFetch)
		{
			if( ( !_alreadyFetchedDefaultReportDataFile || forceFetch || _alwaysFetchDefaultReportDataFile) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReportDataFileEntityUsingDataFileID);
				ReportDataFileEntity newEntity = new ReportDataFileEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DataFileID);
				}
				if(fetchResult)
				{
					newEntity = (ReportDataFileEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_defaultReportDataFileReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DefaultReportDataFile = newEntity;
				_alreadyFetchedDefaultReportDataFile = fetchResult;
			}
			return _defaultReportDataFile;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("UserResource", _userResource);
			toReturn.Add("FilterList", _filterList);
			toReturn.Add("ReportCategory", _reportCategory);
			toReturn.Add("DefaultReportDataFile", _defaultReportDataFile);
			toReturn.Add("ReportDataFiles", _reportDataFiles);
			toReturn.Add("ReportJobs", _reportJobs);
			toReturn.Add("ReportJobResults", _reportJobResults);
			toReturn.Add("ReportToClients", _reportToClients);
			toReturn.Add("Clients", _clients);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="reportID">PK value for Report which data should be fetched into this Report object</param>
		/// <param name="validator">The validator object for this ReportEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 reportID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(reportID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_reportDataFiles = new VarioSL.Entities.CollectionClasses.ReportDataFileCollection();
			_reportDataFiles.SetContainingEntityInfo(this, "Report");

			_reportJobs = new VarioSL.Entities.CollectionClasses.ReportJobCollection();
			_reportJobs.SetContainingEntityInfo(this, "Report");

			_reportJobResults = new VarioSL.Entities.CollectionClasses.ReportJobResultCollection();
			_reportJobResults.SetContainingEntityInfo(this, "Report");

			_reportToClients = new VarioSL.Entities.CollectionClasses.ReportToClientCollection();
			_reportToClients.SetContainingEntityInfo(this, "Report");
			_clients = new VarioSL.Entities.CollectionClasses.ClientCollection();
			_userResourceReturnsNewIfNotFound = false;
			_filterListReturnsNewIfNotFound = false;
			_reportCategoryReturnsNewIfNotFound = false;
			_defaultReportDataFileReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataFileID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterListID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResourceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _userResource</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserResource(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userResource, new PropertyChangedEventHandler( OnUserResourcePropertyChanged ), "UserResource", VarioSL.Entities.RelationClasses.StaticReportRelations.UserResourceEntityUsingResourceIDStatic, true, signalRelatedEntity, "Reports", resetFKFields, new int[] { (int)ReportFieldIndex.ResourceID } );		
			_userResource = null;
		}
		
		/// <summary> setups the sync logic for member _userResource</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserResource(IEntityCore relatedEntity)
		{
			if(_userResource!=relatedEntity)
			{		
				DesetupSyncUserResource(true, true);
				_userResource = (UserResourceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userResource, new PropertyChangedEventHandler( OnUserResourcePropertyChanged ), "UserResource", VarioSL.Entities.RelationClasses.StaticReportRelations.UserResourceEntityUsingResourceIDStatic, true, ref _alreadyFetchedUserResource, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserResourcePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _filterList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFilterList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _filterList, new PropertyChangedEventHandler( OnFilterListPropertyChanged ), "FilterList", VarioSL.Entities.RelationClasses.StaticReportRelations.FilterListEntityUsingFilterListIDStatic, true, signalRelatedEntity, "Reports", resetFKFields, new int[] { (int)ReportFieldIndex.FilterListID } );		
			_filterList = null;
		}
		
		/// <summary> setups the sync logic for member _filterList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFilterList(IEntityCore relatedEntity)
		{
			if(_filterList!=relatedEntity)
			{		
				DesetupSyncFilterList(true, true);
				_filterList = (FilterListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _filterList, new PropertyChangedEventHandler( OnFilterListPropertyChanged ), "FilterList", VarioSL.Entities.RelationClasses.StaticReportRelations.FilterListEntityUsingFilterListIDStatic, true, ref _alreadyFetchedFilterList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFilterListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _reportCategory</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReportCategory(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _reportCategory, new PropertyChangedEventHandler( OnReportCategoryPropertyChanged ), "ReportCategory", VarioSL.Entities.RelationClasses.StaticReportRelations.ReportCategoryEntityUsingCategoryIDStatic, true, signalRelatedEntity, "Reports", resetFKFields, new int[] { (int)ReportFieldIndex.CategoryID } );		
			_reportCategory = null;
		}
		
		/// <summary> setups the sync logic for member _reportCategory</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReportCategory(IEntityCore relatedEntity)
		{
			if(_reportCategory!=relatedEntity)
			{		
				DesetupSyncReportCategory(true, true);
				_reportCategory = (ReportCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _reportCategory, new PropertyChangedEventHandler( OnReportCategoryPropertyChanged ), "ReportCategory", VarioSL.Entities.RelationClasses.StaticReportRelations.ReportCategoryEntityUsingCategoryIDStatic, true, ref _alreadyFetchedReportCategory, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReportCategoryPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _defaultReportDataFile</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDefaultReportDataFile(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _defaultReportDataFile, new PropertyChangedEventHandler( OnDefaultReportDataFilePropertyChanged ), "DefaultReportDataFile", VarioSL.Entities.RelationClasses.StaticReportRelations.ReportDataFileEntityUsingDataFileIDStatic, true, signalRelatedEntity, "Reports", resetFKFields, new int[] { (int)ReportFieldIndex.DataFileID } );		
			_defaultReportDataFile = null;
		}
		
		/// <summary> setups the sync logic for member _defaultReportDataFile</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDefaultReportDataFile(IEntityCore relatedEntity)
		{
			if(_defaultReportDataFile!=relatedEntity)
			{		
				DesetupSyncDefaultReportDataFile(true, true);
				_defaultReportDataFile = (ReportDataFileEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _defaultReportDataFile, new PropertyChangedEventHandler( OnDefaultReportDataFilePropertyChanged ), "DefaultReportDataFile", VarioSL.Entities.RelationClasses.StaticReportRelations.ReportDataFileEntityUsingDataFileIDStatic, true, ref _alreadyFetchedDefaultReportDataFile, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDefaultReportDataFilePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="reportID">PK value for Report which data should be fetched into this Report object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 reportID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ReportFieldIndex.ReportID].ForcedCurrentValueWrite(reportID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateReportDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ReportEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ReportRelations Relations
		{
			get	{ return new ReportRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportDataFile' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportDataFiles
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportDataFileCollection(), (IEntityRelation)GetRelationsForField("ReportDataFiles")[0], (int)VarioSL.Entities.EntityType.ReportEntity, (int)VarioSL.Entities.EntityType.ReportDataFileEntity, 0, null, null, null, "ReportDataFiles", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportJob' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportJobs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportJobCollection(), (IEntityRelation)GetRelationsForField("ReportJobs")[0], (int)VarioSL.Entities.EntityType.ReportEntity, (int)VarioSL.Entities.EntityType.ReportJobEntity, 0, null, null, null, "ReportJobs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportJobResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportJobResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportJobResultCollection(), (IEntityRelation)GetRelationsForField("ReportJobResults")[0], (int)VarioSL.Entities.EntityType.ReportEntity, (int)VarioSL.Entities.EntityType.ReportJobResultEntity, 0, null, null, null, "ReportJobResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportToClient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportToClients
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportToClientCollection(), (IEntityRelation)GetRelationsForField("ReportToClients")[0], (int)VarioSL.Entities.EntityType.ReportEntity, (int)VarioSL.Entities.EntityType.ReportToClientEntity, 0, null, null, null, "ReportToClients", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClients
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ReportToClientEntityUsingReportID;
				intermediateRelation.SetAliases(string.Empty, "ReportToClient_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.ReportEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, GetRelationsForField("Clients"), "Clients", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserResource'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserResource
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserResourceCollection(), (IEntityRelation)GetRelationsForField("UserResource")[0], (int)VarioSL.Entities.EntityType.ReportEntity, (int)VarioSL.Entities.EntityType.UserResourceEntity, 0, null, null, null, "UserResource", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterListCollection(), (IEntityRelation)GetRelationsForField("FilterList")[0], (int)VarioSL.Entities.EntityType.ReportEntity, (int)VarioSL.Entities.EntityType.FilterListEntity, 0, null, null, null, "FilterList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportCategory
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportCategoryCollection(), (IEntityRelation)GetRelationsForField("ReportCategory")[0], (int)VarioSL.Entities.EntityType.ReportEntity, (int)VarioSL.Entities.EntityType.ReportCategoryEntity, 0, null, null, null, "ReportCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportDataFile'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDefaultReportDataFile
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportDataFileCollection(), (IEntityRelation)GetRelationsForField("DefaultReportDataFile")[0], (int)VarioSL.Entities.EntityType.ReportEntity, (int)VarioSL.Entities.EntityType.ReportDataFileEntity, 0, null, null, null, "DefaultReportDataFile", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CategoryID property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORT"."CATEGORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CategoryID
		{
			get { return (System.Int64)GetValue((int)ReportFieldIndex.CategoryID, true); }
			set	{ SetValue((int)ReportFieldIndex.CategoryID, value, true); }
		}

		/// <summary> The DataFileID property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORT"."DATAFILEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DataFileID
		{
			get { return (System.Int64)GetValue((int)ReportFieldIndex.DataFileID, true); }
			set	{ SetValue((int)ReportFieldIndex.DataFileID, value, true); }
		}

		/// <summary> The Description property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORT"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ReportFieldIndex.Description, true); }
			set	{ SetValue((int)ReportFieldIndex.Description, value, true); }
		}

		/// <summary> The DisplayOrder property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORT"."DISPLAYORDER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DisplayOrder
		{
			get { return (System.Int32)GetValue((int)ReportFieldIndex.DisplayOrder, true); }
			set	{ SetValue((int)ReportFieldIndex.DisplayOrder, value, true); }
		}

		/// <summary> The FilterListID property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORT"."FILTERLISTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FilterListID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ReportFieldIndex.FilterListID, false); }
			set	{ SetValue((int)ReportFieldIndex.FilterListID, value, true); }
		}

		/// <summary> The LastModified property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ReportFieldIndex.LastModified, true); }
			set	{ SetValue((int)ReportFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ReportFieldIndex.LastUser, true); }
			set	{ SetValue((int)ReportFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORT"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ReportFieldIndex.Name, true); }
			set	{ SetValue((int)ReportFieldIndex.Name, value, true); }
		}

		/// <summary> The ReportID property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORT"."REPORTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ReportID
		{
			get { return (System.Int64)GetValue((int)ReportFieldIndex.ReportID, true); }
			set	{ SetValue((int)ReportFieldIndex.ReportID, value, true); }
		}

		/// <summary> The ResourceID property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORT"."RESOURCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResourceID
		{
			get { return (System.String)GetValue((int)ReportFieldIndex.ResourceID, true); }
			set	{ SetValue((int)ReportFieldIndex.ResourceID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ReportFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ReportFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ReportDataFileEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportDataFiles()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportDataFileCollection ReportDataFiles
		{
			get	{ return GetMultiReportDataFiles(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportDataFiles. When set to true, ReportDataFiles is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportDataFiles is accessed. You can always execute/ a forced fetch by calling GetMultiReportDataFiles(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportDataFiles
		{
			get	{ return _alwaysFetchReportDataFiles; }
			set	{ _alwaysFetchReportDataFiles = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportDataFiles already has been fetched. Setting this property to false when ReportDataFiles has been fetched
		/// will clear the ReportDataFiles collection well. Setting this property to true while ReportDataFiles hasn't been fetched disables lazy loading for ReportDataFiles</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportDataFiles
		{
			get { return _alreadyFetchedReportDataFiles;}
			set 
			{
				if(_alreadyFetchedReportDataFiles && !value && (_reportDataFiles != null))
				{
					_reportDataFiles.Clear();
				}
				_alreadyFetchedReportDataFiles = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportJobs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobCollection ReportJobs
		{
			get	{ return GetMultiReportJobs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportJobs. When set to true, ReportJobs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportJobs is accessed. You can always execute/ a forced fetch by calling GetMultiReportJobs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportJobs
		{
			get	{ return _alwaysFetchReportJobs; }
			set	{ _alwaysFetchReportJobs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportJobs already has been fetched. Setting this property to false when ReportJobs has been fetched
		/// will clear the ReportJobs collection well. Setting this property to true while ReportJobs hasn't been fetched disables lazy loading for ReportJobs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportJobs
		{
			get { return _alreadyFetchedReportJobs;}
			set 
			{
				if(_alreadyFetchedReportJobs && !value && (_reportJobs != null))
				{
					_reportJobs.Clear();
				}
				_alreadyFetchedReportJobs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportJobResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobResultCollection ReportJobResults
		{
			get	{ return GetMultiReportJobResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportJobResults. When set to true, ReportJobResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportJobResults is accessed. You can always execute/ a forced fetch by calling GetMultiReportJobResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportJobResults
		{
			get	{ return _alwaysFetchReportJobResults; }
			set	{ _alwaysFetchReportJobResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportJobResults already has been fetched. Setting this property to false when ReportJobResults has been fetched
		/// will clear the ReportJobResults collection well. Setting this property to true while ReportJobResults hasn't been fetched disables lazy loading for ReportJobResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportJobResults
		{
			get { return _alreadyFetchedReportJobResults;}
			set 
			{
				if(_alreadyFetchedReportJobResults && !value && (_reportJobResults != null))
				{
					_reportJobResults.Clear();
				}
				_alreadyFetchedReportJobResults = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportToClients()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportToClientCollection ReportToClients
		{
			get	{ return GetMultiReportToClients(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportToClients. When set to true, ReportToClients is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportToClients is accessed. You can always execute/ a forced fetch by calling GetMultiReportToClients(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportToClients
		{
			get	{ return _alwaysFetchReportToClients; }
			set	{ _alwaysFetchReportToClients = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportToClients already has been fetched. Setting this property to false when ReportToClients has been fetched
		/// will clear the ReportToClients collection well. Setting this property to true while ReportToClients hasn't been fetched disables lazy loading for ReportToClients</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportToClients
		{
			get { return _alreadyFetchedReportToClients;}
			set 
			{
				if(_alreadyFetchedReportToClients && !value && (_reportToClients != null))
				{
					_reportToClients.Clear();
				}
				_alreadyFetchedReportToClients = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClients()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClientCollection Clients
		{
			get { return GetMultiClients(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Clients. When set to true, Clients is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Clients is accessed. You can always execute a forced fetch by calling GetMultiClients(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClients
		{
			get	{ return _alwaysFetchClients; }
			set	{ _alwaysFetchClients = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Clients already has been fetched. Setting this property to false when Clients has been fetched
		/// will clear the Clients collection well. Setting this property to true while Clients hasn't been fetched disables lazy loading for Clients</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClients
		{
			get { return _alreadyFetchedClients;}
			set 
			{
				if(_alreadyFetchedClients && !value && (_clients != null))
				{
					_clients.Clear();
				}
				_alreadyFetchedClients = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'UserResourceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserResource()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserResourceEntity UserResource
		{
			get	{ return GetSingleUserResource(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserResource(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Reports", "UserResource", _userResource, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserResource. When set to true, UserResource is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserResource is accessed. You can always execute a forced fetch by calling GetSingleUserResource(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserResource
		{
			get	{ return _alwaysFetchUserResource; }
			set	{ _alwaysFetchUserResource = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserResource already has been fetched. Setting this property to false when UserResource has been fetched
		/// will set UserResource to null as well. Setting this property to true while UserResource hasn't been fetched disables lazy loading for UserResource</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserResource
		{
			get { return _alreadyFetchedUserResource;}
			set 
			{
				if(_alreadyFetchedUserResource && !value)
				{
					this.UserResource = null;
				}
				_alreadyFetchedUserResource = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserResource is not found
		/// in the database. When set to true, UserResource will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserResourceReturnsNewIfNotFound
		{
			get	{ return _userResourceReturnsNewIfNotFound; }
			set { _userResourceReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FilterListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFilterList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FilterListEntity FilterList
		{
			get	{ return GetSingleFilterList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFilterList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Reports", "FilterList", _filterList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FilterList. When set to true, FilterList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterList is accessed. You can always execute a forced fetch by calling GetSingleFilterList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterList
		{
			get	{ return _alwaysFetchFilterList; }
			set	{ _alwaysFetchFilterList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterList already has been fetched. Setting this property to false when FilterList has been fetched
		/// will set FilterList to null as well. Setting this property to true while FilterList hasn't been fetched disables lazy loading for FilterList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterList
		{
			get { return _alreadyFetchedFilterList;}
			set 
			{
				if(_alreadyFetchedFilterList && !value)
				{
					this.FilterList = null;
				}
				_alreadyFetchedFilterList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FilterList is not found
		/// in the database. When set to true, FilterList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FilterListReturnsNewIfNotFound
		{
			get	{ return _filterListReturnsNewIfNotFound; }
			set { _filterListReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReportCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReportCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ReportCategoryEntity ReportCategory
		{
			get	{ return GetSingleReportCategory(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReportCategory(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Reports", "ReportCategory", _reportCategory, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReportCategory. When set to true, ReportCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportCategory is accessed. You can always execute a forced fetch by calling GetSingleReportCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportCategory
		{
			get	{ return _alwaysFetchReportCategory; }
			set	{ _alwaysFetchReportCategory = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportCategory already has been fetched. Setting this property to false when ReportCategory has been fetched
		/// will set ReportCategory to null as well. Setting this property to true while ReportCategory hasn't been fetched disables lazy loading for ReportCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportCategory
		{
			get { return _alreadyFetchedReportCategory;}
			set 
			{
				if(_alreadyFetchedReportCategory && !value)
				{
					this.ReportCategory = null;
				}
				_alreadyFetchedReportCategory = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReportCategory is not found
		/// in the database. When set to true, ReportCategory will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ReportCategoryReturnsNewIfNotFound
		{
			get	{ return _reportCategoryReturnsNewIfNotFound; }
			set { _reportCategoryReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReportDataFileEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDefaultReportDataFile()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ReportDataFileEntity DefaultReportDataFile
		{
			get	{ return GetSingleDefaultReportDataFile(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDefaultReportDataFile(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Reports", "DefaultReportDataFile", _defaultReportDataFile, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DefaultReportDataFile. When set to true, DefaultReportDataFile is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DefaultReportDataFile is accessed. You can always execute a forced fetch by calling GetSingleDefaultReportDataFile(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDefaultReportDataFile
		{
			get	{ return _alwaysFetchDefaultReportDataFile; }
			set	{ _alwaysFetchDefaultReportDataFile = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DefaultReportDataFile already has been fetched. Setting this property to false when DefaultReportDataFile has been fetched
		/// will set DefaultReportDataFile to null as well. Setting this property to true while DefaultReportDataFile hasn't been fetched disables lazy loading for DefaultReportDataFile</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDefaultReportDataFile
		{
			get { return _alreadyFetchedDefaultReportDataFile;}
			set 
			{
				if(_alreadyFetchedDefaultReportDataFile && !value)
				{
					this.DefaultReportDataFile = null;
				}
				_alreadyFetchedDefaultReportDataFile = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DefaultReportDataFile is not found
		/// in the database. When set to true, DefaultReportDataFile will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DefaultReportDataFileReturnsNewIfNotFound
		{
			get	{ return _defaultReportDataFileReturnsNewIfNotFound; }
			set { _defaultReportDataFileReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ReportEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
