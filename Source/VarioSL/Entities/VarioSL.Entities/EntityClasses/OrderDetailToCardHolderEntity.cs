﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'OrderDetailToCardHolder'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class OrderDetailToCardHolderEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CardHolderEntity _cardHolder;
		private bool	_alwaysFetchCardHolder, _alreadyFetchedCardHolder, _cardHolderReturnsNewIfNotFound;
		private OrderDetailEntity _orderDetail;
		private bool	_alwaysFetchOrderDetail, _alreadyFetchedOrderDetail, _orderDetailReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CardHolder</summary>
			public static readonly string CardHolder = "CardHolder";
			/// <summary>Member name OrderDetail</summary>
			public static readonly string OrderDetail = "OrderDetail";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OrderDetailToCardHolderEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public OrderDetailToCardHolderEntity() :base("OrderDetailToCardHolderEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cardHolderID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="orderDetailID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		public OrderDetailToCardHolderEntity(System.Int64 cardHolderID, System.Int64 orderDetailID):base("OrderDetailToCardHolderEntity")
		{
			InitClassFetch(cardHolderID, orderDetailID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cardHolderID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="orderDetailID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OrderDetailToCardHolderEntity(System.Int64 cardHolderID, System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse):base("OrderDetailToCardHolderEntity")
		{
			InitClassFetch(cardHolderID, orderDetailID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cardHolderID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="orderDetailID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="validator">The custom validator object for this OrderDetailToCardHolderEntity</param>
		public OrderDetailToCardHolderEntity(System.Int64 cardHolderID, System.Int64 orderDetailID, IValidator validator):base("OrderDetailToCardHolderEntity")
		{
			InitClassFetch(cardHolderID, orderDetailID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderDetailToCardHolderEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cardHolder = (CardHolderEntity)info.GetValue("_cardHolder", typeof(CardHolderEntity));
			if(_cardHolder!=null)
			{
				_cardHolder.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardHolderReturnsNewIfNotFound = info.GetBoolean("_cardHolderReturnsNewIfNotFound");
			_alwaysFetchCardHolder = info.GetBoolean("_alwaysFetchCardHolder");
			_alreadyFetchedCardHolder = info.GetBoolean("_alreadyFetchedCardHolder");

			_orderDetail = (OrderDetailEntity)info.GetValue("_orderDetail", typeof(OrderDetailEntity));
			if(_orderDetail!=null)
			{
				_orderDetail.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderDetailReturnsNewIfNotFound = info.GetBoolean("_orderDetailReturnsNewIfNotFound");
			_alwaysFetchOrderDetail = info.GetBoolean("_alwaysFetchOrderDetail");
			_alreadyFetchedOrderDetail = info.GetBoolean("_alreadyFetchedOrderDetail");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((OrderDetailToCardHolderFieldIndex)fieldIndex)
			{
				case OrderDetailToCardHolderFieldIndex.CardHolderID:
					DesetupSyncCardHolder(true, false);
					_alreadyFetchedCardHolder = false;
					break;
				case OrderDetailToCardHolderFieldIndex.OrderDetailID:
					DesetupSyncOrderDetail(true, false);
					_alreadyFetchedOrderDetail = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCardHolder = (_cardHolder != null);
			_alreadyFetchedOrderDetail = (_orderDetail != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CardHolder":
					toReturn.Add(Relations.CardHolderEntityUsingCardHolderID);
					break;
				case "OrderDetail":
					toReturn.Add(Relations.OrderDetailEntityUsingOrderDetailID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cardHolder", (!this.MarkedForDeletion?_cardHolder:null));
			info.AddValue("_cardHolderReturnsNewIfNotFound", _cardHolderReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardHolder", _alwaysFetchCardHolder);
			info.AddValue("_alreadyFetchedCardHolder", _alreadyFetchedCardHolder);
			info.AddValue("_orderDetail", (!this.MarkedForDeletion?_orderDetail:null));
			info.AddValue("_orderDetailReturnsNewIfNotFound", _orderDetailReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrderDetail", _alwaysFetchOrderDetail);
			info.AddValue("_alreadyFetchedOrderDetail", _alreadyFetchedOrderDetail);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CardHolder":
					_alreadyFetchedCardHolder = true;
					this.CardHolder = (CardHolderEntity)entity;
					break;
				case "OrderDetail":
					_alreadyFetchedOrderDetail = true;
					this.OrderDetail = (OrderDetailEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CardHolder":
					SetupSyncCardHolder(relatedEntity);
					break;
				case "OrderDetail":
					SetupSyncOrderDetail(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CardHolder":
					DesetupSyncCardHolder(false, true);
					break;
				case "OrderDetail":
					DesetupSyncOrderDetail(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cardHolder!=null)
			{
				toReturn.Add(_cardHolder);
			}
			if(_orderDetail!=null)
			{
				toReturn.Add(_orderDetail);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardHolderID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="orderDetailID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardHolderID, System.Int64 orderDetailID)
		{
			return FetchUsingPK(cardHolderID, orderDetailID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardHolderID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="orderDetailID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardHolderID, System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cardHolderID, orderDetailID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardHolderID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="orderDetailID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardHolderID, System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cardHolderID, orderDetailID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardHolderID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="orderDetailID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardHolderID, System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cardHolderID, orderDetailID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CardHolderID, this.OrderDetailID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OrderDetailToCardHolderRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CardHolderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardHolderEntity' which is related to this entity.</returns>
		public CardHolderEntity GetSingleCardHolder()
		{
			return GetSingleCardHolder(false);
		}

		/// <summary> Retrieves the related entity of type 'CardHolderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardHolderEntity' which is related to this entity.</returns>
		public virtual CardHolderEntity GetSingleCardHolder(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardHolder || forceFetch || _alwaysFetchCardHolder) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardHolderEntityUsingCardHolderID);
				CardHolderEntity newEntity = (CardHolderEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.CardHolderEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = CardHolderEntity.FetchPolymorphic(this.Transaction, this.CardHolderID, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (CardHolderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardHolderReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardHolder = newEntity;
				_alreadyFetchedCardHolder = fetchResult;
			}
			return _cardHolder;
		}


		/// <summary> Retrieves the related entity of type 'OrderDetailEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderDetailEntity' which is related to this entity.</returns>
		public OrderDetailEntity GetSingleOrderDetail()
		{
			return GetSingleOrderDetail(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderDetailEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderDetailEntity' which is related to this entity.</returns>
		public virtual OrderDetailEntity GetSingleOrderDetail(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrderDetail || forceFetch || _alwaysFetchOrderDetail) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderDetailEntityUsingOrderDetailID);
				OrderDetailEntity newEntity = new OrderDetailEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderDetailID);
				}
				if(fetchResult)
				{
					newEntity = (OrderDetailEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderDetailReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrderDetail = newEntity;
				_alreadyFetchedOrderDetail = fetchResult;
			}
			return _orderDetail;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CardHolder", _cardHolder);
			toReturn.Add("OrderDetail", _orderDetail);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cardHolderID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="orderDetailID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="validator">The validator object for this OrderDetailToCardHolderEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cardHolderID, System.Int64 orderDetailID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cardHolderID, orderDetailID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_cardHolderReturnsNewIfNotFound = false;
			_orderDetailReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardHolderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderDetailID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _cardHolder</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardHolder(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardHolder, new PropertyChangedEventHandler( OnCardHolderPropertyChanged ), "CardHolder", VarioSL.Entities.RelationClasses.StaticOrderDetailToCardHolderRelations.CardHolderEntityUsingCardHolderIDStatic, true, signalRelatedEntity, "OrderDetailToCardHolder", resetFKFields, new int[] { (int)OrderDetailToCardHolderFieldIndex.CardHolderID } );		
			_cardHolder = null;
		}
		
		/// <summary> setups the sync logic for member _cardHolder</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardHolder(IEntityCore relatedEntity)
		{
			if(_cardHolder!=relatedEntity)
			{		
				DesetupSyncCardHolder(true, true);
				_cardHolder = (CardHolderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardHolder, new PropertyChangedEventHandler( OnCardHolderPropertyChanged ), "CardHolder", VarioSL.Entities.RelationClasses.StaticOrderDetailToCardHolderRelations.CardHolderEntityUsingCardHolderIDStatic, true, ref _alreadyFetchedCardHolder, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardHolderPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _orderDetail</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrderDetail(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _orderDetail, new PropertyChangedEventHandler( OnOrderDetailPropertyChanged ), "OrderDetail", VarioSL.Entities.RelationClasses.StaticOrderDetailToCardHolderRelations.OrderDetailEntityUsingOrderDetailIDStatic, true, signalRelatedEntity, "OrderDetailToCardHolder", resetFKFields, new int[] { (int)OrderDetailToCardHolderFieldIndex.OrderDetailID } );		
			_orderDetail = null;
		}
		
		/// <summary> setups the sync logic for member _orderDetail</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrderDetail(IEntityCore relatedEntity)
		{
			if(_orderDetail!=relatedEntity)
			{		
				DesetupSyncOrderDetail(true, true);
				_orderDetail = (OrderDetailEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _orderDetail, new PropertyChangedEventHandler( OnOrderDetailPropertyChanged ), "OrderDetail", VarioSL.Entities.RelationClasses.StaticOrderDetailToCardHolderRelations.OrderDetailEntityUsingOrderDetailIDStatic, true, ref _alreadyFetchedOrderDetail, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderDetailPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cardHolderID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="orderDetailID">PK value for OrderDetailToCardHolder which data should be fetched into this OrderDetailToCardHolder object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cardHolderID, System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OrderDetailToCardHolderFieldIndex.CardHolderID].ForcedCurrentValueWrite(cardHolderID);
				this.Fields[(int)OrderDetailToCardHolderFieldIndex.OrderDetailID].ForcedCurrentValueWrite(orderDetailID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrderDetailToCardHolderDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OrderDetailToCardHolderEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OrderDetailToCardHolderRelations Relations
		{
			get	{ return new OrderDetailToCardHolderRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardHolder'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardHolder
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardHolderCollection(), (IEntityRelation)GetRelationsForField("CardHolder")[0], (int)VarioSL.Entities.EntityType.OrderDetailToCardHolderEntity, (int)VarioSL.Entities.EntityType.CardHolderEntity, 0, null, null, null, "CardHolder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetail'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderDetail
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailCollection(), (IEntityRelation)GetRelationsForField("OrderDetail")[0], (int)VarioSL.Entities.EntityType.OrderDetailToCardHolderEntity, (int)VarioSL.Entities.EntityType.OrderDetailEntity, 0, null, null, null, "OrderDetail", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CardHolderID property of the Entity OrderDetailToCardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAILTOPERSON"."PERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 CardHolderID
		{
			get { return (System.Int64)GetValue((int)OrderDetailToCardHolderFieldIndex.CardHolderID, true); }
			set	{ SetValue((int)OrderDetailToCardHolderFieldIndex.CardHolderID, value, true); }
		}

		/// <summary> The OrderDetailID property of the Entity OrderDetailToCardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAILTOPERSON"."ORDERDETAILID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 OrderDetailID
		{
			get { return (System.Int64)GetValue((int)OrderDetailToCardHolderFieldIndex.OrderDetailID, true); }
			set	{ SetValue((int)OrderDetailToCardHolderFieldIndex.OrderDetailID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CardHolderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardHolder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardHolderEntity CardHolder
		{
			get	{ return GetSingleCardHolder(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCardHolder(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderDetailToCardHolder", "CardHolder", _cardHolder, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardHolder. When set to true, CardHolder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardHolder is accessed. You can always execute a forced fetch by calling GetSingleCardHolder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardHolder
		{
			get	{ return _alwaysFetchCardHolder; }
			set	{ _alwaysFetchCardHolder = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardHolder already has been fetched. Setting this property to false when CardHolder has been fetched
		/// will set CardHolder to null as well. Setting this property to true while CardHolder hasn't been fetched disables lazy loading for CardHolder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardHolder
		{
			get { return _alreadyFetchedCardHolder;}
			set 
			{
				if(_alreadyFetchedCardHolder && !value)
				{
					this.CardHolder = null;
				}
				_alreadyFetchedCardHolder = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardHolder is not found
		/// in the database. When set to true, CardHolder will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardHolderReturnsNewIfNotFound
		{
			get	{ return _cardHolderReturnsNewIfNotFound; }
			set { _cardHolderReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrderDetailEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrderDetail()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OrderDetailEntity OrderDetail
		{
			get	{ return GetSingleOrderDetail(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrderDetail(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderDetailToCardHolder", "OrderDetail", _orderDetail, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrderDetail. When set to true, OrderDetail is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderDetail is accessed. You can always execute a forced fetch by calling GetSingleOrderDetail(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderDetail
		{
			get	{ return _alwaysFetchOrderDetail; }
			set	{ _alwaysFetchOrderDetail = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderDetail already has been fetched. Setting this property to false when OrderDetail has been fetched
		/// will set OrderDetail to null as well. Setting this property to true while OrderDetail hasn't been fetched disables lazy loading for OrderDetail</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderDetail
		{
			get { return _alreadyFetchedOrderDetail;}
			set 
			{
				if(_alreadyFetchedOrderDetail && !value)
				{
					this.OrderDetail = null;
				}
				_alreadyFetchedOrderDetail = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrderDetail is not found
		/// in the database. When set to true, OrderDetail will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OrderDetailReturnsNewIfNotFound
		{
			get	{ return _orderDetailReturnsNewIfNotFound; }
			set { _orderDetailReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.OrderDetailToCardHolderEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
