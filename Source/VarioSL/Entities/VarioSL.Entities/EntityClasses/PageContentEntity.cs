﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PageContent'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PageContentEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.PageContentGroupToContentCollection	_pageContentGroupToContent;
		private bool	_alwaysFetchPageContentGroupToContent, _alreadyFetchedPageContentGroupToContent;
		private VarioSL.Entities.CollectionClasses.ContentItemCollection	_contentItems;
		private bool	_alwaysFetchContentItems, _alreadyFetchedContentItems;
		private VarioSL.Entities.CollectionClasses.PageContentGroupCollection _pageContentGroupCollection;
		private bool	_alwaysFetchPageContentGroupCollection, _alreadyFetchedPageContentGroupCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PageContentGroupToContent</summary>
			public static readonly string PageContentGroupToContent = "PageContentGroupToContent";
			/// <summary>Member name ContentItems</summary>
			public static readonly string ContentItems = "ContentItems";
			/// <summary>Member name PageContentGroupCollection</summary>
			public static readonly string PageContentGroupCollection = "PageContentGroupCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PageContentEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PageContentEntity() :base("PageContentEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="pageContentID">PK value for PageContent which data should be fetched into this PageContent object</param>
		public PageContentEntity(System.Int64 pageContentID):base("PageContentEntity")
		{
			InitClassFetch(pageContentID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="pageContentID">PK value for PageContent which data should be fetched into this PageContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PageContentEntity(System.Int64 pageContentID, IPrefetchPath prefetchPathToUse):base("PageContentEntity")
		{
			InitClassFetch(pageContentID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="pageContentID">PK value for PageContent which data should be fetched into this PageContent object</param>
		/// <param name="validator">The custom validator object for this PageContentEntity</param>
		public PageContentEntity(System.Int64 pageContentID, IValidator validator):base("PageContentEntity")
		{
			InitClassFetch(pageContentID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PageContentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_pageContentGroupToContent = (VarioSL.Entities.CollectionClasses.PageContentGroupToContentCollection)info.GetValue("_pageContentGroupToContent", typeof(VarioSL.Entities.CollectionClasses.PageContentGroupToContentCollection));
			_alwaysFetchPageContentGroupToContent = info.GetBoolean("_alwaysFetchPageContentGroupToContent");
			_alreadyFetchedPageContentGroupToContent = info.GetBoolean("_alreadyFetchedPageContentGroupToContent");

			_contentItems = (VarioSL.Entities.CollectionClasses.ContentItemCollection)info.GetValue("_contentItems", typeof(VarioSL.Entities.CollectionClasses.ContentItemCollection));
			_alwaysFetchContentItems = info.GetBoolean("_alwaysFetchContentItems");
			_alreadyFetchedContentItems = info.GetBoolean("_alreadyFetchedContentItems");
			_pageContentGroupCollection = (VarioSL.Entities.CollectionClasses.PageContentGroupCollection)info.GetValue("_pageContentGroupCollection", typeof(VarioSL.Entities.CollectionClasses.PageContentGroupCollection));
			_alwaysFetchPageContentGroupCollection = info.GetBoolean("_alwaysFetchPageContentGroupCollection");
			_alreadyFetchedPageContentGroupCollection = info.GetBoolean("_alreadyFetchedPageContentGroupCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPageContentGroupToContent = (_pageContentGroupToContent.Count > 0);
			_alreadyFetchedContentItems = (_contentItems.Count > 0);
			_alreadyFetchedPageContentGroupCollection = (_pageContentGroupCollection.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PageContentGroupToContent":
					toReturn.Add(Relations.PageContentGroupToContentEntityUsingPageContentID);
					break;
				case "ContentItems":
					toReturn.Add(Relations.ContentItemEntityUsingPageContentID);
					break;
				case "PageContentGroupCollection":
					toReturn.Add(Relations.PageContentGroupToContentEntityUsingPageContentID, "PageContentEntity__", "PageContentGroupToContent_", JoinHint.None);
					toReturn.Add(PageContentGroupToContentEntity.Relations.PageContentGroupEntityUsingPageContentGroupid, "PageContentGroupToContent_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_pageContentGroupToContent", (!this.MarkedForDeletion?_pageContentGroupToContent:null));
			info.AddValue("_alwaysFetchPageContentGroupToContent", _alwaysFetchPageContentGroupToContent);
			info.AddValue("_alreadyFetchedPageContentGroupToContent", _alreadyFetchedPageContentGroupToContent);
			info.AddValue("_contentItems", (!this.MarkedForDeletion?_contentItems:null));
			info.AddValue("_alwaysFetchContentItems", _alwaysFetchContentItems);
			info.AddValue("_alreadyFetchedContentItems", _alreadyFetchedContentItems);
			info.AddValue("_pageContentGroupCollection", (!this.MarkedForDeletion?_pageContentGroupCollection:null));
			info.AddValue("_alwaysFetchPageContentGroupCollection", _alwaysFetchPageContentGroupCollection);
			info.AddValue("_alreadyFetchedPageContentGroupCollection", _alreadyFetchedPageContentGroupCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PageContentGroupToContent":
					_alreadyFetchedPageContentGroupToContent = true;
					if(entity!=null)
					{
						this.PageContentGroupToContent.Add((PageContentGroupToContentEntity)entity);
					}
					break;
				case "ContentItems":
					_alreadyFetchedContentItems = true;
					if(entity!=null)
					{
						this.ContentItems.Add((ContentItemEntity)entity);
					}
					break;
				case "PageContentGroupCollection":
					_alreadyFetchedPageContentGroupCollection = true;
					if(entity!=null)
					{
						this.PageContentGroupCollection.Add((PageContentGroupEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PageContentGroupToContent":
					_pageContentGroupToContent.Add((PageContentGroupToContentEntity)relatedEntity);
					break;
				case "ContentItems":
					_contentItems.Add((ContentItemEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PageContentGroupToContent":
					this.PerformRelatedEntityRemoval(_pageContentGroupToContent, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ContentItems":
					this.PerformRelatedEntityRemoval(_contentItems, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_pageContentGroupToContent);
			toReturn.Add(_contentItems);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageContentID">PK value for PageContent which data should be fetched into this PageContent object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pageContentID)
		{
			return FetchUsingPK(pageContentID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageContentID">PK value for PageContent which data should be fetched into this PageContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pageContentID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(pageContentID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageContentID">PK value for PageContent which data should be fetched into this PageContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pageContentID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(pageContentID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageContentID">PK value for PageContent which data should be fetched into this PageContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pageContentID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(pageContentID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PageContentID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PageContentRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'PageContentGroupToContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageContentGroupToContentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PageContentGroupToContentCollection GetMultiPageContentGroupToContent(bool forceFetch)
		{
			return GetMultiPageContentGroupToContent(forceFetch, _pageContentGroupToContent.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageContentGroupToContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageContentGroupToContentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PageContentGroupToContentCollection GetMultiPageContentGroupToContent(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageContentGroupToContent(forceFetch, _pageContentGroupToContent.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageContentGroupToContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PageContentGroupToContentCollection GetMultiPageContentGroupToContent(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageContentGroupToContent(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageContentGroupToContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PageContentGroupToContentCollection GetMultiPageContentGroupToContent(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageContentGroupToContent || forceFetch || _alwaysFetchPageContentGroupToContent) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageContentGroupToContent);
				_pageContentGroupToContent.SuppressClearInGetMulti=!forceFetch;
				_pageContentGroupToContent.EntityFactoryToUse = entityFactoryToUse;
				_pageContentGroupToContent.GetMultiManyToOne(null, this, filter);
				_pageContentGroupToContent.SuppressClearInGetMulti=false;
				_alreadyFetchedPageContentGroupToContent = true;
			}
			return _pageContentGroupToContent;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageContentGroupToContent'. These settings will be taken into account
		/// when the property PageContentGroupToContent is requested or GetMultiPageContentGroupToContent is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageContentGroupToContent(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageContentGroupToContent.SortClauses=sortClauses;
			_pageContentGroupToContent.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContentItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContentItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContentItemCollection GetMultiContentItems(bool forceFetch)
		{
			return GetMultiContentItems(forceFetch, _contentItems.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContentItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContentItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContentItemCollection GetMultiContentItems(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContentItems(forceFetch, _contentItems.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContentItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContentItemCollection GetMultiContentItems(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContentItems(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContentItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContentItemCollection GetMultiContentItems(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContentItems || forceFetch || _alwaysFetchContentItems) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contentItems);
				_contentItems.SuppressClearInGetMulti=!forceFetch;
				_contentItems.EntityFactoryToUse = entityFactoryToUse;
				_contentItems.GetMultiManyToOne(this, filter);
				_contentItems.SuppressClearInGetMulti=false;
				_alreadyFetchedContentItems = true;
			}
			return _contentItems;
		}

		/// <summary> Sets the collection parameters for the collection for 'ContentItems'. These settings will be taken into account
		/// when the property ContentItems is requested or GetMultiContentItems is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContentItems(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contentItems.SortClauses=sortClauses;
			_contentItems.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageContentGroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageContentGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PageContentGroupCollection GetMultiPageContentGroupCollection(bool forceFetch)
		{
			return GetMultiPageContentGroupCollection(forceFetch, _pageContentGroupCollection.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PageContentGroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PageContentGroupCollection GetMultiPageContentGroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPageContentGroupCollection || forceFetch || _alwaysFetchPageContentGroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageContentGroupCollection);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PageContentFields.PageContentID, ComparisonOperator.Equal, this.PageContentID, "PageContentEntity__"));
				_pageContentGroupCollection.SuppressClearInGetMulti=!forceFetch;
				_pageContentGroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageContentGroupCollection.GetMulti(filter, GetRelationsForField("PageContentGroupCollection"));
				_pageContentGroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageContentGroupCollection = true;
			}
			return _pageContentGroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageContentGroupCollection'. These settings will be taken into account
		/// when the property PageContentGroupCollection is requested or GetMultiPageContentGroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageContentGroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageContentGroupCollection.SortClauses=sortClauses;
			_pageContentGroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PageContentGroupToContent", _pageContentGroupToContent);
			toReturn.Add("ContentItems", _contentItems);
			toReturn.Add("PageContentGroupCollection", _pageContentGroupCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="pageContentID">PK value for PageContent which data should be fetched into this PageContent object</param>
		/// <param name="validator">The validator object for this PageContentEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 pageContentID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(pageContentID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_pageContentGroupToContent = new VarioSL.Entities.CollectionClasses.PageContentGroupToContentCollection();
			_pageContentGroupToContent.SetContainingEntityInfo(this, "PageContent");

			_contentItems = new VarioSL.Entities.CollectionClasses.ContentItemCollection();
			_contentItems.SetContainingEntityInfo(this, "PageContent");
			_pageContentGroupCollection = new VarioSL.Entities.CollectionClasses.PageContentGroupCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContentType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageContentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Region", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="pageContentID">PK value for PageContent which data should be fetched into this PageContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 pageContentID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PageContentFieldIndex.PageContentID].ForcedCurrentValueWrite(pageContentID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePageContentDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PageContentEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PageContentRelations Relations
		{
			get	{ return new PageContentRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageContentGroupToContent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageContentGroupToContent
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PageContentGroupToContentCollection(), (IEntityRelation)GetRelationsForField("PageContentGroupToContent")[0], (int)VarioSL.Entities.EntityType.PageContentEntity, (int)VarioSL.Entities.EntityType.PageContentGroupToContentEntity, 0, null, null, null, "PageContentGroupToContent", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContentItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContentItems
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContentItemCollection(), (IEntityRelation)GetRelationsForField("ContentItems")[0], (int)VarioSL.Entities.EntityType.PageContentEntity, (int)VarioSL.Entities.EntityType.ContentItemEntity, 0, null, null, null, "ContentItems", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageContentGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageContentGroupCollection
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.PageContentGroupToContentEntityUsingPageContentID;
				intermediateRelation.SetAliases(string.Empty, "PageContentGroupToContent_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PageContentGroupCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.PageContentEntity, (int)VarioSL.Entities.EntityType.PageContentGroupEntity, 0, null, null, GetRelationsForField("PageContentGroupCollection"), "PageContentGroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ContentType property of the Entity PageContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CMS_PAGECONTENT"."CONTENTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.ContentType ContentType
		{
			get { return (VarioSL.Entities.Enumerations.ContentType)GetValue((int)PageContentFieldIndex.ContentType, true); }
			set	{ SetValue((int)PageContentFieldIndex.ContentType, value, true); }
		}

		/// <summary> The Description property of the Entity PageContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CMS_PAGECONTENT"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)PageContentFieldIndex.Description, true); }
			set	{ SetValue((int)PageContentFieldIndex.Description, value, true); }
		}

		/// <summary> The LastModified property of the Entity PageContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CMS_PAGECONTENT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)PageContentFieldIndex.LastModified, true); }
			set	{ SetValue((int)PageContentFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity PageContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CMS_PAGECONTENT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)PageContentFieldIndex.LastUser, true); }
			set	{ SetValue((int)PageContentFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PageContentID property of the Entity PageContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CMS_PAGECONTENT"."PAGECONTENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 PageContentID
		{
			get { return (System.Int64)GetValue((int)PageContentFieldIndex.PageContentID, true); }
			set	{ SetValue((int)PageContentFieldIndex.PageContentID, value, true); }
		}

		/// <summary> The Region property of the Entity PageContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CMS_PAGECONTENT"."REGION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Region
		{
			get { return (System.String)GetValue((int)PageContentFieldIndex.Region, true); }
			set	{ SetValue((int)PageContentFieldIndex.Region, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity PageContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CMS_PAGECONTENT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)PageContentFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)PageContentFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'PageContentGroupToContentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageContentGroupToContent()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PageContentGroupToContentCollection PageContentGroupToContent
		{
			get	{ return GetMultiPageContentGroupToContent(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageContentGroupToContent. When set to true, PageContentGroupToContent is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageContentGroupToContent is accessed. You can always execute/ a forced fetch by calling GetMultiPageContentGroupToContent(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageContentGroupToContent
		{
			get	{ return _alwaysFetchPageContentGroupToContent; }
			set	{ _alwaysFetchPageContentGroupToContent = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageContentGroupToContent already has been fetched. Setting this property to false when PageContentGroupToContent has been fetched
		/// will clear the PageContentGroupToContent collection well. Setting this property to true while PageContentGroupToContent hasn't been fetched disables lazy loading for PageContentGroupToContent</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageContentGroupToContent
		{
			get { return _alreadyFetchedPageContentGroupToContent;}
			set 
			{
				if(_alreadyFetchedPageContentGroupToContent && !value && (_pageContentGroupToContent != null))
				{
					_pageContentGroupToContent.Clear();
				}
				_alreadyFetchedPageContentGroupToContent = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContentItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContentItems()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContentItemCollection ContentItems
		{
			get	{ return GetMultiContentItems(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ContentItems. When set to true, ContentItems is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContentItems is accessed. You can always execute/ a forced fetch by calling GetMultiContentItems(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContentItems
		{
			get	{ return _alwaysFetchContentItems; }
			set	{ _alwaysFetchContentItems = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ContentItems already has been fetched. Setting this property to false when ContentItems has been fetched
		/// will clear the ContentItems collection well. Setting this property to true while ContentItems hasn't been fetched disables lazy loading for ContentItems</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContentItems
		{
			get { return _alreadyFetchedContentItems;}
			set 
			{
				if(_alreadyFetchedContentItems && !value && (_contentItems != null))
				{
					_contentItems.Clear();
				}
				_alreadyFetchedContentItems = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PageContentGroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageContentGroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PageContentGroupCollection PageContentGroupCollection
		{
			get { return GetMultiPageContentGroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageContentGroupCollection. When set to true, PageContentGroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageContentGroupCollection is accessed. You can always execute a forced fetch by calling GetMultiPageContentGroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageContentGroupCollection
		{
			get	{ return _alwaysFetchPageContentGroupCollection; }
			set	{ _alwaysFetchPageContentGroupCollection = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageContentGroupCollection already has been fetched. Setting this property to false when PageContentGroupCollection has been fetched
		/// will clear the PageContentGroupCollection collection well. Setting this property to true while PageContentGroupCollection hasn't been fetched disables lazy loading for PageContentGroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageContentGroupCollection
		{
			get { return _alreadyFetchedPageContentGroupCollection;}
			set 
			{
				if(_alreadyFetchedPageContentGroupCollection && !value && (_pageContentGroupCollection != null))
				{
					_pageContentGroupCollection.Clear();
				}
				_alreadyFetchedPageContentGroupCollection = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PageContentEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
