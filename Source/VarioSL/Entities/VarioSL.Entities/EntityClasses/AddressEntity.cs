﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Address'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AddressEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AddressBookEntryCollection	_addressBookEntries;
		private bool	_alwaysFetchAddressBookEntries, _alreadyFetchedAddressBookEntries;
		private VarioSL.Entities.CollectionClasses.ContractAddressCollection	_contractAddresses;
		private bool	_alwaysFetchContractAddresses, _alreadyFetchedContractAddresses;
		private VarioSL.Entities.CollectionClasses.MobilityProviderCollection	_mobilityProviders;
		private bool	_alwaysFetchMobilityProviders, _alreadyFetchedMobilityProviders;
		private VarioSL.Entities.CollectionClasses.OrderCollection	_invoiceAddressOrders;
		private bool	_alwaysFetchInvoiceAddressOrders, _alreadyFetchedInvoiceAddressOrders;
		private VarioSL.Entities.CollectionClasses.OrderCollection	_shippingAddressOrders;
		private bool	_alwaysFetchShippingAddressOrders, _alreadyFetchedShippingAddressOrders;
		private VarioSL.Entities.CollectionClasses.OrganizationAddressCollection	_organizationAddresses;
		private bool	_alwaysFetchOrganizationAddresses, _alreadyFetchedOrganizationAddresses;
		private VarioSL.Entities.CollectionClasses.PersonAddressCollection	_personAddresses;
		private bool	_alwaysFetchPersonAddresses, _alreadyFetchedPersonAddresses;
		private VarioSL.Entities.CollectionClasses.ProductRelationCollection	_productRelationsFrom;
		private bool	_alwaysFetchProductRelationsFrom, _alreadyFetchedProductRelationsFrom;
		private VarioSL.Entities.CollectionClasses.ProductRelationCollection	_productRelationsTo;
		private bool	_alwaysFetchProductRelationsTo, _alreadyFetchedProductRelationsTo;
		private VarioSL.Entities.CollectionClasses.StorageLocationCollection	_storageLocations;
		private bool	_alwaysFetchStorageLocations, _alreadyFetchedStorageLocations;
		private VarioSL.Entities.CollectionClasses.ContractCollection _contracts;
		private bool	_alwaysFetchContracts, _alreadyFetchedContracts;
		private VarioSL.Entities.CollectionClasses.PersonCollection _persons;
		private bool	_alwaysFetchPersons, _alreadyFetchedPersons;
		private CoordinateEntity _coordinate;
		private bool	_alwaysFetchCoordinate, _alreadyFetchedCoordinate, _coordinateReturnsNewIfNotFound;
		private CardHolderEntity _cardHolder;
		private bool	_alwaysFetchCardHolder, _alreadyFetchedCardHolder, _cardHolderReturnsNewIfNotFound;
		private PersonEntity _person;
		private bool	_alwaysFetchPerson, _alreadyFetchedPerson, _personReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Coordinate</summary>
			public static readonly string Coordinate = "Coordinate";
			/// <summary>Member name AddressBookEntries</summary>
			public static readonly string AddressBookEntries = "AddressBookEntries";
			/// <summary>Member name ContractAddresses</summary>
			public static readonly string ContractAddresses = "ContractAddresses";
			/// <summary>Member name MobilityProviders</summary>
			public static readonly string MobilityProviders = "MobilityProviders";
			/// <summary>Member name InvoiceAddressOrders</summary>
			public static readonly string InvoiceAddressOrders = "InvoiceAddressOrders";
			/// <summary>Member name ShippingAddressOrders</summary>
			public static readonly string ShippingAddressOrders = "ShippingAddressOrders";
			/// <summary>Member name OrganizationAddresses</summary>
			public static readonly string OrganizationAddresses = "OrganizationAddresses";
			/// <summary>Member name PersonAddresses</summary>
			public static readonly string PersonAddresses = "PersonAddresses";
			/// <summary>Member name ProductRelationsFrom</summary>
			public static readonly string ProductRelationsFrom = "ProductRelationsFrom";
			/// <summary>Member name ProductRelationsTo</summary>
			public static readonly string ProductRelationsTo = "ProductRelationsTo";
			/// <summary>Member name StorageLocations</summary>
			public static readonly string StorageLocations = "StorageLocations";
			/// <summary>Member name Contracts</summary>
			public static readonly string Contracts = "Contracts";
			/// <summary>Member name Persons</summary>
			public static readonly string Persons = "Persons";
			/// <summary>Member name CardHolder</summary>
			public static readonly string CardHolder = "CardHolder";
			/// <summary>Member name Person</summary>
			public static readonly string Person = "Person";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AddressEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AddressEntity() :base("AddressEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="addressID">PK value for Address which data should be fetched into this Address object</param>
		public AddressEntity(System.Int64 addressID):base("AddressEntity")
		{
			InitClassFetch(addressID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="addressID">PK value for Address which data should be fetched into this Address object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AddressEntity(System.Int64 addressID, IPrefetchPath prefetchPathToUse):base("AddressEntity")
		{
			InitClassFetch(addressID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="addressID">PK value for Address which data should be fetched into this Address object</param>
		/// <param name="validator">The custom validator object for this AddressEntity</param>
		public AddressEntity(System.Int64 addressID, IValidator validator):base("AddressEntity")
		{
			InitClassFetch(addressID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AddressEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_addressBookEntries = (VarioSL.Entities.CollectionClasses.AddressBookEntryCollection)info.GetValue("_addressBookEntries", typeof(VarioSL.Entities.CollectionClasses.AddressBookEntryCollection));
			_alwaysFetchAddressBookEntries = info.GetBoolean("_alwaysFetchAddressBookEntries");
			_alreadyFetchedAddressBookEntries = info.GetBoolean("_alreadyFetchedAddressBookEntries");

			_contractAddresses = (VarioSL.Entities.CollectionClasses.ContractAddressCollection)info.GetValue("_contractAddresses", typeof(VarioSL.Entities.CollectionClasses.ContractAddressCollection));
			_alwaysFetchContractAddresses = info.GetBoolean("_alwaysFetchContractAddresses");
			_alreadyFetchedContractAddresses = info.GetBoolean("_alreadyFetchedContractAddresses");

			_mobilityProviders = (VarioSL.Entities.CollectionClasses.MobilityProviderCollection)info.GetValue("_mobilityProviders", typeof(VarioSL.Entities.CollectionClasses.MobilityProviderCollection));
			_alwaysFetchMobilityProviders = info.GetBoolean("_alwaysFetchMobilityProviders");
			_alreadyFetchedMobilityProviders = info.GetBoolean("_alreadyFetchedMobilityProviders");

			_invoiceAddressOrders = (VarioSL.Entities.CollectionClasses.OrderCollection)info.GetValue("_invoiceAddressOrders", typeof(VarioSL.Entities.CollectionClasses.OrderCollection));
			_alwaysFetchInvoiceAddressOrders = info.GetBoolean("_alwaysFetchInvoiceAddressOrders");
			_alreadyFetchedInvoiceAddressOrders = info.GetBoolean("_alreadyFetchedInvoiceAddressOrders");

			_shippingAddressOrders = (VarioSL.Entities.CollectionClasses.OrderCollection)info.GetValue("_shippingAddressOrders", typeof(VarioSL.Entities.CollectionClasses.OrderCollection));
			_alwaysFetchShippingAddressOrders = info.GetBoolean("_alwaysFetchShippingAddressOrders");
			_alreadyFetchedShippingAddressOrders = info.GetBoolean("_alreadyFetchedShippingAddressOrders");

			_organizationAddresses = (VarioSL.Entities.CollectionClasses.OrganizationAddressCollection)info.GetValue("_organizationAddresses", typeof(VarioSL.Entities.CollectionClasses.OrganizationAddressCollection));
			_alwaysFetchOrganizationAddresses = info.GetBoolean("_alwaysFetchOrganizationAddresses");
			_alreadyFetchedOrganizationAddresses = info.GetBoolean("_alreadyFetchedOrganizationAddresses");

			_personAddresses = (VarioSL.Entities.CollectionClasses.PersonAddressCollection)info.GetValue("_personAddresses", typeof(VarioSL.Entities.CollectionClasses.PersonAddressCollection));
			_alwaysFetchPersonAddresses = info.GetBoolean("_alwaysFetchPersonAddresses");
			_alreadyFetchedPersonAddresses = info.GetBoolean("_alreadyFetchedPersonAddresses");

			_productRelationsFrom = (VarioSL.Entities.CollectionClasses.ProductRelationCollection)info.GetValue("_productRelationsFrom", typeof(VarioSL.Entities.CollectionClasses.ProductRelationCollection));
			_alwaysFetchProductRelationsFrom = info.GetBoolean("_alwaysFetchProductRelationsFrom");
			_alreadyFetchedProductRelationsFrom = info.GetBoolean("_alreadyFetchedProductRelationsFrom");

			_productRelationsTo = (VarioSL.Entities.CollectionClasses.ProductRelationCollection)info.GetValue("_productRelationsTo", typeof(VarioSL.Entities.CollectionClasses.ProductRelationCollection));
			_alwaysFetchProductRelationsTo = info.GetBoolean("_alwaysFetchProductRelationsTo");
			_alreadyFetchedProductRelationsTo = info.GetBoolean("_alreadyFetchedProductRelationsTo");

			_storageLocations = (VarioSL.Entities.CollectionClasses.StorageLocationCollection)info.GetValue("_storageLocations", typeof(VarioSL.Entities.CollectionClasses.StorageLocationCollection));
			_alwaysFetchStorageLocations = info.GetBoolean("_alwaysFetchStorageLocations");
			_alreadyFetchedStorageLocations = info.GetBoolean("_alreadyFetchedStorageLocations");
			_contracts = (VarioSL.Entities.CollectionClasses.ContractCollection)info.GetValue("_contracts", typeof(VarioSL.Entities.CollectionClasses.ContractCollection));
			_alwaysFetchContracts = info.GetBoolean("_alwaysFetchContracts");
			_alreadyFetchedContracts = info.GetBoolean("_alreadyFetchedContracts");

			_persons = (VarioSL.Entities.CollectionClasses.PersonCollection)info.GetValue("_persons", typeof(VarioSL.Entities.CollectionClasses.PersonCollection));
			_alwaysFetchPersons = info.GetBoolean("_alwaysFetchPersons");
			_alreadyFetchedPersons = info.GetBoolean("_alreadyFetchedPersons");
			_coordinate = (CoordinateEntity)info.GetValue("_coordinate", typeof(CoordinateEntity));
			if(_coordinate!=null)
			{
				_coordinate.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_coordinateReturnsNewIfNotFound = info.GetBoolean("_coordinateReturnsNewIfNotFound");
			_alwaysFetchCoordinate = info.GetBoolean("_alwaysFetchCoordinate");
			_alreadyFetchedCoordinate = info.GetBoolean("_alreadyFetchedCoordinate");
			_cardHolder = (CardHolderEntity)info.GetValue("_cardHolder", typeof(CardHolderEntity));
			if(_cardHolder!=null)
			{
				_cardHolder.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardHolderReturnsNewIfNotFound = info.GetBoolean("_cardHolderReturnsNewIfNotFound");
			_alwaysFetchCardHolder = info.GetBoolean("_alwaysFetchCardHolder");
			_alreadyFetchedCardHolder = info.GetBoolean("_alreadyFetchedCardHolder");

			_person = (PersonEntity)info.GetValue("_person", typeof(PersonEntity));
			if(_person!=null)
			{
				_person.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_personReturnsNewIfNotFound = info.GetBoolean("_personReturnsNewIfNotFound");
			_alwaysFetchPerson = info.GetBoolean("_alwaysFetchPerson");
			_alreadyFetchedPerson = info.GetBoolean("_alreadyFetchedPerson");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AddressFieldIndex)fieldIndex)
			{
				case AddressFieldIndex.CoordinateID:
					DesetupSyncCoordinate(true, false);
					_alreadyFetchedCoordinate = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAddressBookEntries = (_addressBookEntries.Count > 0);
			_alreadyFetchedContractAddresses = (_contractAddresses.Count > 0);
			_alreadyFetchedMobilityProviders = (_mobilityProviders.Count > 0);
			_alreadyFetchedInvoiceAddressOrders = (_invoiceAddressOrders.Count > 0);
			_alreadyFetchedShippingAddressOrders = (_shippingAddressOrders.Count > 0);
			_alreadyFetchedOrganizationAddresses = (_organizationAddresses.Count > 0);
			_alreadyFetchedPersonAddresses = (_personAddresses.Count > 0);
			_alreadyFetchedProductRelationsFrom = (_productRelationsFrom.Count > 0);
			_alreadyFetchedProductRelationsTo = (_productRelationsTo.Count > 0);
			_alreadyFetchedStorageLocations = (_storageLocations.Count > 0);
			_alreadyFetchedContracts = (_contracts.Count > 0);
			_alreadyFetchedPersons = (_persons.Count > 0);
			_alreadyFetchedCoordinate = (_coordinate != null);
			_alreadyFetchedCardHolder = (_cardHolder != null);
			_alreadyFetchedPerson = (_person != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Coordinate":
					toReturn.Add(Relations.CoordinateEntityUsingCoordinateID);
					break;
				case "AddressBookEntries":
					toReturn.Add(Relations.AddressBookEntryEntityUsingAddressID);
					break;
				case "ContractAddresses":
					toReturn.Add(Relations.ContractAddressEntityUsingAddressID);
					break;
				case "MobilityProviders":
					toReturn.Add(Relations.MobilityProviderEntityUsingAddressID);
					break;
				case "InvoiceAddressOrders":
					toReturn.Add(Relations.OrderEntityUsingInvoiceAddressID);
					break;
				case "ShippingAddressOrders":
					toReturn.Add(Relations.OrderEntityUsingShippingAddressID);
					break;
				case "OrganizationAddresses":
					toReturn.Add(Relations.OrganizationAddressEntityUsingAddressID);
					break;
				case "PersonAddresses":
					toReturn.Add(Relations.PersonAddressEntityUsingAddressID);
					break;
				case "ProductRelationsFrom":
					toReturn.Add(Relations.ProductRelationEntityUsingFromAddressID);
					break;
				case "ProductRelationsTo":
					toReturn.Add(Relations.ProductRelationEntityUsingToAddressID);
					break;
				case "StorageLocations":
					toReturn.Add(Relations.StorageLocationEntityUsingAddressID);
					break;
				case "Contracts":
					toReturn.Add(Relations.ContractAddressEntityUsingAddressID, "AddressEntity__", "ContractAddress_", JoinHint.None);
					toReturn.Add(ContractAddressEntity.Relations.ContractEntityUsingContractID, "ContractAddress_", string.Empty, JoinHint.None);
					break;
				case "Persons":
					toReturn.Add(Relations.PersonAddressEntityUsingAddressID, "AddressEntity__", "PersonAddress_", JoinHint.None);
					toReturn.Add(PersonAddressEntity.Relations.PersonEntityUsingPersonID, "PersonAddress_", string.Empty, JoinHint.None);
					break;
				case "CardHolder":
					toReturn.Add(Relations.CardHolderEntityUsingAlternativeAdressID);
					break;
				case "Person":
					toReturn.Add(Relations.PersonEntityUsingAddressID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_addressBookEntries", (!this.MarkedForDeletion?_addressBookEntries:null));
			info.AddValue("_alwaysFetchAddressBookEntries", _alwaysFetchAddressBookEntries);
			info.AddValue("_alreadyFetchedAddressBookEntries", _alreadyFetchedAddressBookEntries);
			info.AddValue("_contractAddresses", (!this.MarkedForDeletion?_contractAddresses:null));
			info.AddValue("_alwaysFetchContractAddresses", _alwaysFetchContractAddresses);
			info.AddValue("_alreadyFetchedContractAddresses", _alreadyFetchedContractAddresses);
			info.AddValue("_mobilityProviders", (!this.MarkedForDeletion?_mobilityProviders:null));
			info.AddValue("_alwaysFetchMobilityProviders", _alwaysFetchMobilityProviders);
			info.AddValue("_alreadyFetchedMobilityProviders", _alreadyFetchedMobilityProviders);
			info.AddValue("_invoiceAddressOrders", (!this.MarkedForDeletion?_invoiceAddressOrders:null));
			info.AddValue("_alwaysFetchInvoiceAddressOrders", _alwaysFetchInvoiceAddressOrders);
			info.AddValue("_alreadyFetchedInvoiceAddressOrders", _alreadyFetchedInvoiceAddressOrders);
			info.AddValue("_shippingAddressOrders", (!this.MarkedForDeletion?_shippingAddressOrders:null));
			info.AddValue("_alwaysFetchShippingAddressOrders", _alwaysFetchShippingAddressOrders);
			info.AddValue("_alreadyFetchedShippingAddressOrders", _alreadyFetchedShippingAddressOrders);
			info.AddValue("_organizationAddresses", (!this.MarkedForDeletion?_organizationAddresses:null));
			info.AddValue("_alwaysFetchOrganizationAddresses", _alwaysFetchOrganizationAddresses);
			info.AddValue("_alreadyFetchedOrganizationAddresses", _alreadyFetchedOrganizationAddresses);
			info.AddValue("_personAddresses", (!this.MarkedForDeletion?_personAddresses:null));
			info.AddValue("_alwaysFetchPersonAddresses", _alwaysFetchPersonAddresses);
			info.AddValue("_alreadyFetchedPersonAddresses", _alreadyFetchedPersonAddresses);
			info.AddValue("_productRelationsFrom", (!this.MarkedForDeletion?_productRelationsFrom:null));
			info.AddValue("_alwaysFetchProductRelationsFrom", _alwaysFetchProductRelationsFrom);
			info.AddValue("_alreadyFetchedProductRelationsFrom", _alreadyFetchedProductRelationsFrom);
			info.AddValue("_productRelationsTo", (!this.MarkedForDeletion?_productRelationsTo:null));
			info.AddValue("_alwaysFetchProductRelationsTo", _alwaysFetchProductRelationsTo);
			info.AddValue("_alreadyFetchedProductRelationsTo", _alreadyFetchedProductRelationsTo);
			info.AddValue("_storageLocations", (!this.MarkedForDeletion?_storageLocations:null));
			info.AddValue("_alwaysFetchStorageLocations", _alwaysFetchStorageLocations);
			info.AddValue("_alreadyFetchedStorageLocations", _alreadyFetchedStorageLocations);
			info.AddValue("_contracts", (!this.MarkedForDeletion?_contracts:null));
			info.AddValue("_alwaysFetchContracts", _alwaysFetchContracts);
			info.AddValue("_alreadyFetchedContracts", _alreadyFetchedContracts);
			info.AddValue("_persons", (!this.MarkedForDeletion?_persons:null));
			info.AddValue("_alwaysFetchPersons", _alwaysFetchPersons);
			info.AddValue("_alreadyFetchedPersons", _alreadyFetchedPersons);
			info.AddValue("_coordinate", (!this.MarkedForDeletion?_coordinate:null));
			info.AddValue("_coordinateReturnsNewIfNotFound", _coordinateReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCoordinate", _alwaysFetchCoordinate);
			info.AddValue("_alreadyFetchedCoordinate", _alreadyFetchedCoordinate);

			info.AddValue("_cardHolder", (!this.MarkedForDeletion?_cardHolder:null));
			info.AddValue("_cardHolderReturnsNewIfNotFound", _cardHolderReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardHolder", _alwaysFetchCardHolder);
			info.AddValue("_alreadyFetchedCardHolder", _alreadyFetchedCardHolder);

			info.AddValue("_person", (!this.MarkedForDeletion?_person:null));
			info.AddValue("_personReturnsNewIfNotFound", _personReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerson", _alwaysFetchPerson);
			info.AddValue("_alreadyFetchedPerson", _alreadyFetchedPerson);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Coordinate":
					_alreadyFetchedCoordinate = true;
					this.Coordinate = (CoordinateEntity)entity;
					break;
				case "AddressBookEntries":
					_alreadyFetchedAddressBookEntries = true;
					if(entity!=null)
					{
						this.AddressBookEntries.Add((AddressBookEntryEntity)entity);
					}
					break;
				case "ContractAddresses":
					_alreadyFetchedContractAddresses = true;
					if(entity!=null)
					{
						this.ContractAddresses.Add((ContractAddressEntity)entity);
					}
					break;
				case "MobilityProviders":
					_alreadyFetchedMobilityProviders = true;
					if(entity!=null)
					{
						this.MobilityProviders.Add((MobilityProviderEntity)entity);
					}
					break;
				case "InvoiceAddressOrders":
					_alreadyFetchedInvoiceAddressOrders = true;
					if(entity!=null)
					{
						this.InvoiceAddressOrders.Add((OrderEntity)entity);
					}
					break;
				case "ShippingAddressOrders":
					_alreadyFetchedShippingAddressOrders = true;
					if(entity!=null)
					{
						this.ShippingAddressOrders.Add((OrderEntity)entity);
					}
					break;
				case "OrganizationAddresses":
					_alreadyFetchedOrganizationAddresses = true;
					if(entity!=null)
					{
						this.OrganizationAddresses.Add((OrganizationAddressEntity)entity);
					}
					break;
				case "PersonAddresses":
					_alreadyFetchedPersonAddresses = true;
					if(entity!=null)
					{
						this.PersonAddresses.Add((PersonAddressEntity)entity);
					}
					break;
				case "ProductRelationsFrom":
					_alreadyFetchedProductRelationsFrom = true;
					if(entity!=null)
					{
						this.ProductRelationsFrom.Add((ProductRelationEntity)entity);
					}
					break;
				case "ProductRelationsTo":
					_alreadyFetchedProductRelationsTo = true;
					if(entity!=null)
					{
						this.ProductRelationsTo.Add((ProductRelationEntity)entity);
					}
					break;
				case "StorageLocations":
					_alreadyFetchedStorageLocations = true;
					if(entity!=null)
					{
						this.StorageLocations.Add((StorageLocationEntity)entity);
					}
					break;
				case "Contracts":
					_alreadyFetchedContracts = true;
					if(entity!=null)
					{
						this.Contracts.Add((ContractEntity)entity);
					}
					break;
				case "Persons":
					_alreadyFetchedPersons = true;
					if(entity!=null)
					{
						this.Persons.Add((PersonEntity)entity);
					}
					break;
				case "CardHolder":
					_alreadyFetchedCardHolder = true;
					this.CardHolder = (CardHolderEntity)entity;
					break;
				case "Person":
					_alreadyFetchedPerson = true;
					this.Person = (PersonEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Coordinate":
					SetupSyncCoordinate(relatedEntity);
					break;
				case "AddressBookEntries":
					_addressBookEntries.Add((AddressBookEntryEntity)relatedEntity);
					break;
				case "ContractAddresses":
					_contractAddresses.Add((ContractAddressEntity)relatedEntity);
					break;
				case "MobilityProviders":
					_mobilityProviders.Add((MobilityProviderEntity)relatedEntity);
					break;
				case "InvoiceAddressOrders":
					_invoiceAddressOrders.Add((OrderEntity)relatedEntity);
					break;
				case "ShippingAddressOrders":
					_shippingAddressOrders.Add((OrderEntity)relatedEntity);
					break;
				case "OrganizationAddresses":
					_organizationAddresses.Add((OrganizationAddressEntity)relatedEntity);
					break;
				case "PersonAddresses":
					_personAddresses.Add((PersonAddressEntity)relatedEntity);
					break;
				case "ProductRelationsFrom":
					_productRelationsFrom.Add((ProductRelationEntity)relatedEntity);
					break;
				case "ProductRelationsTo":
					_productRelationsTo.Add((ProductRelationEntity)relatedEntity);
					break;
				case "StorageLocations":
					_storageLocations.Add((StorageLocationEntity)relatedEntity);
					break;
				case "CardHolder":
					SetupSyncCardHolder(relatedEntity);
					break;
				case "Person":
					SetupSyncPerson(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Coordinate":
					DesetupSyncCoordinate(false, true);
					break;
				case "AddressBookEntries":
					this.PerformRelatedEntityRemoval(_addressBookEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ContractAddresses":
					this.PerformRelatedEntityRemoval(_contractAddresses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MobilityProviders":
					this.PerformRelatedEntityRemoval(_mobilityProviders, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "InvoiceAddressOrders":
					this.PerformRelatedEntityRemoval(_invoiceAddressOrders, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ShippingAddressOrders":
					this.PerformRelatedEntityRemoval(_shippingAddressOrders, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrganizationAddresses":
					this.PerformRelatedEntityRemoval(_organizationAddresses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PersonAddresses":
					this.PerformRelatedEntityRemoval(_personAddresses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductRelationsFrom":
					this.PerformRelatedEntityRemoval(_productRelationsFrom, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductRelationsTo":
					this.PerformRelatedEntityRemoval(_productRelationsTo, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "StorageLocations":
					this.PerformRelatedEntityRemoval(_storageLocations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardHolder":
					DesetupSyncCardHolder(false, true);
					break;
				case "Person":
					DesetupSyncPerson(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cardHolder!=null)
			{
				toReturn.Add(_cardHolder);
			}
			if(_person!=null)
			{
				toReturn.Add(_person);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_coordinate!=null)
			{
				toReturn.Add(_coordinate);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_addressBookEntries);
			toReturn.Add(_contractAddresses);
			toReturn.Add(_mobilityProviders);
			toReturn.Add(_invoiceAddressOrders);
			toReturn.Add(_shippingAddressOrders);
			toReturn.Add(_organizationAddresses);
			toReturn.Add(_personAddresses);
			toReturn.Add(_productRelationsFrom);
			toReturn.Add(_productRelationsTo);
			toReturn.Add(_storageLocations);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="addressID">PK value for Address which data should be fetched into this Address object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 addressID)
		{
			return FetchUsingPK(addressID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="addressID">PK value for Address which data should be fetched into this Address object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 addressID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(addressID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="addressID">PK value for Address which data should be fetched into this Address object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 addressID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(addressID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="addressID">PK value for Address which data should be fetched into this Address object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 addressID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(addressID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AddressID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AddressRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AddressBookEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AddressBookEntryCollection GetMultiAddressBookEntries(bool forceFetch)
		{
			return GetMultiAddressBookEntries(forceFetch, _addressBookEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AddressBookEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AddressBookEntryCollection GetMultiAddressBookEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAddressBookEntries(forceFetch, _addressBookEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AddressBookEntryCollection GetMultiAddressBookEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAddressBookEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AddressBookEntryCollection GetMultiAddressBookEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAddressBookEntries || forceFetch || _alwaysFetchAddressBookEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_addressBookEntries);
				_addressBookEntries.SuppressClearInGetMulti=!forceFetch;
				_addressBookEntries.EntityFactoryToUse = entityFactoryToUse;
				_addressBookEntries.GetMultiManyToOne(null, this, null, filter);
				_addressBookEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedAddressBookEntries = true;
			}
			return _addressBookEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'AddressBookEntries'. These settings will be taken into account
		/// when the property AddressBookEntries is requested or GetMultiAddressBookEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAddressBookEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_addressBookEntries.SortClauses=sortClauses;
			_addressBookEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractAddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractAddressCollection GetMultiContractAddresses(bool forceFetch)
		{
			return GetMultiContractAddresses(forceFetch, _contractAddresses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractAddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractAddressCollection GetMultiContractAddresses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContractAddresses(forceFetch, _contractAddresses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractAddressCollection GetMultiContractAddresses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContractAddresses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractAddressCollection GetMultiContractAddresses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContractAddresses || forceFetch || _alwaysFetchContractAddresses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contractAddresses);
				_contractAddresses.SuppressClearInGetMulti=!forceFetch;
				_contractAddresses.EntityFactoryToUse = entityFactoryToUse;
				_contractAddresses.GetMultiManyToOne(this, null, filter);
				_contractAddresses.SuppressClearInGetMulti=false;
				_alreadyFetchedContractAddresses = true;
			}
			return _contractAddresses;
		}

		/// <summary> Sets the collection parameters for the collection for 'ContractAddresses'. These settings will be taken into account
		/// when the property ContractAddresses is requested or GetMultiContractAddresses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContractAddresses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contractAddresses.SortClauses=sortClauses;
			_contractAddresses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MobilityProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MobilityProviderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.MobilityProviderCollection GetMultiMobilityProviders(bool forceFetch)
		{
			return GetMultiMobilityProviders(forceFetch, _mobilityProviders.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MobilityProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MobilityProviderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.MobilityProviderCollection GetMultiMobilityProviders(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMobilityProviders(forceFetch, _mobilityProviders.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MobilityProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.MobilityProviderCollection GetMultiMobilityProviders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMobilityProviders(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MobilityProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.MobilityProviderCollection GetMultiMobilityProviders(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMobilityProviders || forceFetch || _alwaysFetchMobilityProviders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mobilityProviders);
				_mobilityProviders.SuppressClearInGetMulti=!forceFetch;
				_mobilityProviders.EntityFactoryToUse = entityFactoryToUse;
				_mobilityProviders.GetMultiManyToOne(this, filter);
				_mobilityProviders.SuppressClearInGetMulti=false;
				_alreadyFetchedMobilityProviders = true;
			}
			return _mobilityProviders;
		}

		/// <summary> Sets the collection parameters for the collection for 'MobilityProviders'. These settings will be taken into account
		/// when the property MobilityProviders is requested or GetMultiMobilityProviders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMobilityProviders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mobilityProviders.SortClauses=sortClauses;
			_mobilityProviders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiInvoiceAddressOrders(bool forceFetch)
		{
			return GetMultiInvoiceAddressOrders(forceFetch, _invoiceAddressOrders.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiInvoiceAddressOrders(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiInvoiceAddressOrders(forceFetch, _invoiceAddressOrders.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiInvoiceAddressOrders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiInvoiceAddressOrders(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderCollection GetMultiInvoiceAddressOrders(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedInvoiceAddressOrders || forceFetch || _alwaysFetchInvoiceAddressOrders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_invoiceAddressOrders);
				_invoiceAddressOrders.SuppressClearInGetMulti=!forceFetch;
				_invoiceAddressOrders.EntityFactoryToUse = entityFactoryToUse;
				_invoiceAddressOrders.GetMultiManyToOne(null, this, null, null, null, null, filter);
				_invoiceAddressOrders.SuppressClearInGetMulti=false;
				_alreadyFetchedInvoiceAddressOrders = true;
			}
			return _invoiceAddressOrders;
		}

		/// <summary> Sets the collection parameters for the collection for 'InvoiceAddressOrders'. These settings will be taken into account
		/// when the property InvoiceAddressOrders is requested or GetMultiInvoiceAddressOrders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersInvoiceAddressOrders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_invoiceAddressOrders.SortClauses=sortClauses;
			_invoiceAddressOrders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiShippingAddressOrders(bool forceFetch)
		{
			return GetMultiShippingAddressOrders(forceFetch, _shippingAddressOrders.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiShippingAddressOrders(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiShippingAddressOrders(forceFetch, _shippingAddressOrders.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiShippingAddressOrders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiShippingAddressOrders(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderCollection GetMultiShippingAddressOrders(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedShippingAddressOrders || forceFetch || _alwaysFetchShippingAddressOrders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_shippingAddressOrders);
				_shippingAddressOrders.SuppressClearInGetMulti=!forceFetch;
				_shippingAddressOrders.EntityFactoryToUse = entityFactoryToUse;
				_shippingAddressOrders.GetMultiManyToOne(null, null, this, null, null, null, filter);
				_shippingAddressOrders.SuppressClearInGetMulti=false;
				_alreadyFetchedShippingAddressOrders = true;
			}
			return _shippingAddressOrders;
		}

		/// <summary> Sets the collection parameters for the collection for 'ShippingAddressOrders'. These settings will be taken into account
		/// when the property ShippingAddressOrders is requested or GetMultiShippingAddressOrders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersShippingAddressOrders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_shippingAddressOrders.SortClauses=sortClauses;
			_shippingAddressOrders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrganizationAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationAddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationAddressCollection GetMultiOrganizationAddresses(bool forceFetch)
		{
			return GetMultiOrganizationAddresses(forceFetch, _organizationAddresses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationAddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationAddressCollection GetMultiOrganizationAddresses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrganizationAddresses(forceFetch, _organizationAddresses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationAddressCollection GetMultiOrganizationAddresses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrganizationAddresses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationAddressCollection GetMultiOrganizationAddresses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrganizationAddresses || forceFetch || _alwaysFetchOrganizationAddresses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_organizationAddresses);
				_organizationAddresses.SuppressClearInGetMulti=!forceFetch;
				_organizationAddresses.EntityFactoryToUse = entityFactoryToUse;
				_organizationAddresses.GetMultiManyToOne(this, null, filter);
				_organizationAddresses.SuppressClearInGetMulti=false;
				_alreadyFetchedOrganizationAddresses = true;
			}
			return _organizationAddresses;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrganizationAddresses'. These settings will be taken into account
		/// when the property OrganizationAddresses is requested or GetMultiOrganizationAddresses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrganizationAddresses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_organizationAddresses.SortClauses=sortClauses;
			_organizationAddresses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PersonAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PersonAddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PersonAddressCollection GetMultiPersonAddresses(bool forceFetch)
		{
			return GetMultiPersonAddresses(forceFetch, _personAddresses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PersonAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PersonAddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PersonAddressCollection GetMultiPersonAddresses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPersonAddresses(forceFetch, _personAddresses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PersonAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PersonAddressCollection GetMultiPersonAddresses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPersonAddresses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PersonAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PersonAddressCollection GetMultiPersonAddresses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPersonAddresses || forceFetch || _alwaysFetchPersonAddresses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_personAddresses);
				_personAddresses.SuppressClearInGetMulti=!forceFetch;
				_personAddresses.EntityFactoryToUse = entityFactoryToUse;
				_personAddresses.GetMultiManyToOne(this, null, filter);
				_personAddresses.SuppressClearInGetMulti=false;
				_alreadyFetchedPersonAddresses = true;
			}
			return _personAddresses;
		}

		/// <summary> Sets the collection parameters for the collection for 'PersonAddresses'. These settings will be taken into account
		/// when the property PersonAddresses is requested or GetMultiPersonAddresses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPersonAddresses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_personAddresses.SortClauses=sortClauses;
			_personAddresses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductRelationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductRelationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductRelationCollection GetMultiProductRelationsFrom(bool forceFetch)
		{
			return GetMultiProductRelationsFrom(forceFetch, _productRelationsFrom.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductRelationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductRelationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductRelationCollection GetMultiProductRelationsFrom(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductRelationsFrom(forceFetch, _productRelationsFrom.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductRelationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductRelationCollection GetMultiProductRelationsFrom(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductRelationsFrom(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductRelationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductRelationCollection GetMultiProductRelationsFrom(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductRelationsFrom || forceFetch || _alwaysFetchProductRelationsFrom) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productRelationsFrom);
				_productRelationsFrom.SuppressClearInGetMulti=!forceFetch;
				_productRelationsFrom.EntityFactoryToUse = entityFactoryToUse;
				_productRelationsFrom.GetMultiManyToOne(this, null, filter);
				_productRelationsFrom.SuppressClearInGetMulti=false;
				_alreadyFetchedProductRelationsFrom = true;
			}
			return _productRelationsFrom;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductRelationsFrom'. These settings will be taken into account
		/// when the property ProductRelationsFrom is requested or GetMultiProductRelationsFrom is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductRelationsFrom(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productRelationsFrom.SortClauses=sortClauses;
			_productRelationsFrom.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductRelationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductRelationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductRelationCollection GetMultiProductRelationsTo(bool forceFetch)
		{
			return GetMultiProductRelationsTo(forceFetch, _productRelationsTo.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductRelationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductRelationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductRelationCollection GetMultiProductRelationsTo(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductRelationsTo(forceFetch, _productRelationsTo.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductRelationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductRelationCollection GetMultiProductRelationsTo(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductRelationsTo(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductRelationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductRelationCollection GetMultiProductRelationsTo(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductRelationsTo || forceFetch || _alwaysFetchProductRelationsTo) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productRelationsTo);
				_productRelationsTo.SuppressClearInGetMulti=!forceFetch;
				_productRelationsTo.EntityFactoryToUse = entityFactoryToUse;
				_productRelationsTo.GetMultiManyToOne(null, this, filter);
				_productRelationsTo.SuppressClearInGetMulti=false;
				_alreadyFetchedProductRelationsTo = true;
			}
			return _productRelationsTo;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductRelationsTo'. These settings will be taken into account
		/// when the property ProductRelationsTo is requested or GetMultiProductRelationsTo is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductRelationsTo(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productRelationsTo.SortClauses=sortClauses;
			_productRelationsTo.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'StorageLocationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'StorageLocationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StorageLocationCollection GetMultiStorageLocations(bool forceFetch)
		{
			return GetMultiStorageLocations(forceFetch, _storageLocations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StorageLocationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'StorageLocationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StorageLocationCollection GetMultiStorageLocations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiStorageLocations(forceFetch, _storageLocations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'StorageLocationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.StorageLocationCollection GetMultiStorageLocations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiStorageLocations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StorageLocationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.StorageLocationCollection GetMultiStorageLocations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedStorageLocations || forceFetch || _alwaysFetchStorageLocations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_storageLocations);
				_storageLocations.SuppressClearInGetMulti=!forceFetch;
				_storageLocations.EntityFactoryToUse = entityFactoryToUse;
				_storageLocations.GetMultiManyToOne(null, this, filter);
				_storageLocations.SuppressClearInGetMulti=false;
				_alreadyFetchedStorageLocations = true;
			}
			return _storageLocations;
		}

		/// <summary> Sets the collection parameters for the collection for 'StorageLocations'. These settings will be taken into account
		/// when the property StorageLocations is requested or GetMultiStorageLocations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersStorageLocations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_storageLocations.SortClauses=sortClauses;
			_storageLocations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch)
		{
			return GetMultiContracts(forceFetch, _contracts.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedContracts || forceFetch || _alwaysFetchContracts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contracts);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AddressFields.AddressID, ComparisonOperator.Equal, this.AddressID, "AddressEntity__"));
				_contracts.SuppressClearInGetMulti=!forceFetch;
				_contracts.EntityFactoryToUse = entityFactoryToUse;
				_contracts.GetMulti(filter, GetRelationsForField("Contracts"));
				_contracts.SuppressClearInGetMulti=false;
				_alreadyFetchedContracts = true;
			}
			return _contracts;
		}

		/// <summary> Sets the collection parameters for the collection for 'Contracts'. These settings will be taken into account
		/// when the property Contracts is requested or GetMultiContracts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContracts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contracts.SortClauses=sortClauses;
			_contracts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PersonEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PersonEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PersonCollection GetMultiPersons(bool forceFetch)
		{
			return GetMultiPersons(forceFetch, _persons.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PersonEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PersonCollection GetMultiPersons(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPersons || forceFetch || _alwaysFetchPersons) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_persons);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AddressFields.AddressID, ComparisonOperator.Equal, this.AddressID, "AddressEntity__"));
				_persons.SuppressClearInGetMulti=!forceFetch;
				_persons.EntityFactoryToUse = entityFactoryToUse;
				_persons.GetMulti(filter, GetRelationsForField("Persons"));
				_persons.SuppressClearInGetMulti=false;
				_alreadyFetchedPersons = true;
			}
			return _persons;
		}

		/// <summary> Sets the collection parameters for the collection for 'Persons'. These settings will be taken into account
		/// when the property Persons is requested or GetMultiPersons is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPersons(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_persons.SortClauses=sortClauses;
			_persons.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CoordinateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CoordinateEntity' which is related to this entity.</returns>
		public CoordinateEntity GetSingleCoordinate()
		{
			return GetSingleCoordinate(false);
		}

		/// <summary> Retrieves the related entity of type 'CoordinateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CoordinateEntity' which is related to this entity.</returns>
		public virtual CoordinateEntity GetSingleCoordinate(bool forceFetch)
		{
			if( ( !_alreadyFetchedCoordinate || forceFetch || _alwaysFetchCoordinate) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CoordinateEntityUsingCoordinateID);
				CoordinateEntity newEntity = new CoordinateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CoordinateID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CoordinateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_coordinateReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Coordinate = newEntity;
				_alreadyFetchedCoordinate = fetchResult;
			}
			return _coordinate;
		}

		/// <summary> Retrieves the related entity of type 'CardHolderEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'CardHolderEntity' which is related to this entity.</returns>
		public CardHolderEntity GetSingleCardHolder()
		{
			return GetSingleCardHolder(false);
		}
		
		/// <summary> Retrieves the related entity of type 'CardHolderEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardHolderEntity' which is related to this entity.</returns>
		public virtual CardHolderEntity GetSingleCardHolder(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardHolder || forceFetch || _alwaysFetchCardHolder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardHolderEntityUsingAlternativeAdressID);
				CardHolderEntity newEntity = (CardHolderEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.CardHolderEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = CardHolderEntity.FetchPolymorphicUsingUCAlternativeAdressID(this.Transaction, this.AddressID, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (CardHolderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardHolderReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardHolder = newEntity;
				_alreadyFetchedCardHolder = fetchResult;
			}
			return _cardHolder;
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSinglePerson()
		{
			return GetSinglePerson(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSinglePerson(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerson || forceFetch || _alwaysFetchPerson) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingAddressID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphicUsingUCAddressID(this.Transaction, this.AddressID, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_personReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Person = newEntity;
				_alreadyFetchedPerson = fetchResult;
			}
			return _person;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Coordinate", _coordinate);
			toReturn.Add("AddressBookEntries", _addressBookEntries);
			toReturn.Add("ContractAddresses", _contractAddresses);
			toReturn.Add("MobilityProviders", _mobilityProviders);
			toReturn.Add("InvoiceAddressOrders", _invoiceAddressOrders);
			toReturn.Add("ShippingAddressOrders", _shippingAddressOrders);
			toReturn.Add("OrganizationAddresses", _organizationAddresses);
			toReturn.Add("PersonAddresses", _personAddresses);
			toReturn.Add("ProductRelationsFrom", _productRelationsFrom);
			toReturn.Add("ProductRelationsTo", _productRelationsTo);
			toReturn.Add("StorageLocations", _storageLocations);
			toReturn.Add("Contracts", _contracts);
			toReturn.Add("Persons", _persons);
			toReturn.Add("CardHolder", _cardHolder);
			toReturn.Add("Person", _person);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="addressID">PK value for Address which data should be fetched into this Address object</param>
		/// <param name="validator">The validator object for this AddressEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 addressID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(addressID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_addressBookEntries = new VarioSL.Entities.CollectionClasses.AddressBookEntryCollection();
			_addressBookEntries.SetContainingEntityInfo(this, "Address");

			_contractAddresses = new VarioSL.Entities.CollectionClasses.ContractAddressCollection();
			_contractAddresses.SetContainingEntityInfo(this, "Address");

			_mobilityProviders = new VarioSL.Entities.CollectionClasses.MobilityProviderCollection();
			_mobilityProviders.SetContainingEntityInfo(this, "Address");

			_invoiceAddressOrders = new VarioSL.Entities.CollectionClasses.OrderCollection();
			_invoiceAddressOrders.SetContainingEntityInfo(this, "InvoiceAddress");

			_shippingAddressOrders = new VarioSL.Entities.CollectionClasses.OrderCollection();
			_shippingAddressOrders.SetContainingEntityInfo(this, "ShippingAddress");

			_organizationAddresses = new VarioSL.Entities.CollectionClasses.OrganizationAddressCollection();
			_organizationAddresses.SetContainingEntityInfo(this, "Address");

			_personAddresses = new VarioSL.Entities.CollectionClasses.PersonAddressCollection();
			_personAddresses.SetContainingEntityInfo(this, "Address");

			_productRelationsFrom = new VarioSL.Entities.CollectionClasses.ProductRelationCollection();
			_productRelationsFrom.SetContainingEntityInfo(this, "FromAddress");

			_productRelationsTo = new VarioSL.Entities.CollectionClasses.ProductRelationCollection();
			_productRelationsTo.SetContainingEntityInfo(this, "ToAddress");

			_storageLocations = new VarioSL.Entities.CollectionClasses.StorageLocationCollection();
			_storageLocations.SetContainingEntityInfo(this, "Address");
			_contracts = new VarioSL.Entities.CollectionClasses.ContractCollection();
			_persons = new VarioSL.Entities.CollectionClasses.PersonCollection();
			_coordinateReturnsNewIfNotFound = false;
			_cardHolderReturnsNewIfNotFound = false;
			_personReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressee", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressField1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressField2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("City", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CoordinateID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Country", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostalCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Region", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Salutation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Street", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StreetNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidUntil", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _coordinate</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCoordinate(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _coordinate, new PropertyChangedEventHandler( OnCoordinatePropertyChanged ), "Coordinate", VarioSL.Entities.RelationClasses.StaticAddressRelations.CoordinateEntityUsingCoordinateIDStatic, true, signalRelatedEntity, "Addresses", resetFKFields, new int[] { (int)AddressFieldIndex.CoordinateID } );		
			_coordinate = null;
		}
		
		/// <summary> setups the sync logic for member _coordinate</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCoordinate(IEntityCore relatedEntity)
		{
			if(_coordinate!=relatedEntity)
			{		
				DesetupSyncCoordinate(true, true);
				_coordinate = (CoordinateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _coordinate, new PropertyChangedEventHandler( OnCoordinatePropertyChanged ), "Coordinate", VarioSL.Entities.RelationClasses.StaticAddressRelations.CoordinateEntityUsingCoordinateIDStatic, true, ref _alreadyFetchedCoordinate, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCoordinatePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cardHolder</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardHolder(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardHolder, new PropertyChangedEventHandler( OnCardHolderPropertyChanged ), "CardHolder", VarioSL.Entities.RelationClasses.StaticAddressRelations.CardHolderEntityUsingAlternativeAdressIDStatic, false, signalRelatedEntity, "AlternativeAddress", false, new int[] { (int)AddressFieldIndex.AddressID } );
			_cardHolder = null;
		}
	
		/// <summary> setups the sync logic for member _cardHolder</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardHolder(IEntityCore relatedEntity)
		{
			if(_cardHolder!=relatedEntity)
			{
				DesetupSyncCardHolder(true, true);
				_cardHolder = (CardHolderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardHolder, new PropertyChangedEventHandler( OnCardHolderPropertyChanged ), "CardHolder", VarioSL.Entities.RelationClasses.StaticAddressRelations.CardHolderEntityUsingAlternativeAdressIDStatic, false, ref _alreadyFetchedCardHolder, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardHolderPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _person</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerson(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticAddressRelations.PersonEntityUsingAddressIDStatic, false, signalRelatedEntity, "Address", false, new int[] { (int)AddressFieldIndex.AddressID } );
			_person = null;
		}
	
		/// <summary> setups the sync logic for member _person</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerson(IEntityCore relatedEntity)
		{
			if(_person!=relatedEntity)
			{
				DesetupSyncPerson(true, true);
				_person = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticAddressRelations.PersonEntityUsingAddressIDStatic, false, ref _alreadyFetchedPerson, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPersonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="addressID">PK value for Address which data should be fetched into this Address object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 addressID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AddressFieldIndex.AddressID].ForcedCurrentValueWrite(addressID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAddressDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AddressEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AddressRelations Relations
		{
			get	{ return new AddressRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AddressBookEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAddressBookEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressBookEntryCollection(), (IEntityRelation)GetRelationsForField("AddressBookEntries")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.AddressBookEntryEntity, 0, null, null, null, "AddressBookEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractAddress' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContractAddresses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractAddressCollection(), (IEntityRelation)GetRelationsForField("ContractAddresses")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.ContractAddressEntity, 0, null, null, null, "ContractAddresses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MobilityProvider' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMobilityProviders
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.MobilityProviderCollection(), (IEntityRelation)GetRelationsForField("MobilityProviders")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.MobilityProviderEntity, 0, null, null, null, "MobilityProviders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoiceAddressOrders
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("InvoiceAddressOrders")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.OrderEntity, 0, null, null, null, "InvoiceAddressOrders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathShippingAddressOrders
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("ShippingAddressOrders")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.OrderEntity, 0, null, null, null, "ShippingAddressOrders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrganizationAddress' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganizationAddresses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationAddressCollection(), (IEntityRelation)GetRelationsForField("OrganizationAddresses")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.OrganizationAddressEntity, 0, null, null, null, "OrganizationAddresses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PersonAddress' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPersonAddresses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonAddressCollection(), (IEntityRelation)GetRelationsForField("PersonAddresses")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.PersonAddressEntity, 0, null, null, null, "PersonAddresses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductRelation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductRelationsFrom
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductRelationCollection(), (IEntityRelation)GetRelationsForField("ProductRelationsFrom")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.ProductRelationEntity, 0, null, null, null, "ProductRelationsFrom", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductRelation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductRelationsTo
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductRelationCollection(), (IEntityRelation)GetRelationsForField("ProductRelationsTo")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.ProductRelationEntity, 0, null, null, null, "ProductRelationsTo", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StorageLocation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathStorageLocations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StorageLocationCollection(), (IEntityRelation)GetRelationsForField("StorageLocations")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.StorageLocationEntity, 0, null, null, null, "StorageLocations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContracts
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ContractAddressEntityUsingAddressID;
				intermediateRelation.SetAliases(string.Empty, "ContractAddress_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, GetRelationsForField("Contracts"), "Contracts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPersons
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.PersonAddressEntityUsingAddressID;
				intermediateRelation.SetAliases(string.Empty, "PersonAddress_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, GetRelationsForField("Persons"), "Persons", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Coordinate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCoordinate
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CoordinateCollection(), (IEntityRelation)GetRelationsForField("Coordinate")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.CoordinateEntity, 0, null, null, null, "Coordinate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardHolder'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardHolder
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardHolderCollection(), (IEntityRelation)GetRelationsForField("CardHolder")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.CardHolderEntity, 0, null, null, null, "CardHolder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerson
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("Person")[0], (int)VarioSL.Entities.EntityType.AddressEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "Person", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Addressee property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."ADDRESSEE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressee
		{
			get { return (System.String)GetValue((int)AddressFieldIndex.Addressee, true); }
			set	{ SetValue((int)AddressFieldIndex.Addressee, value, true); }
		}

		/// <summary> The AddressField1 property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."ADDRESSFIELD1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressField1
		{
			get { return (System.String)GetValue((int)AddressFieldIndex.AddressField1, true); }
			set	{ SetValue((int)AddressFieldIndex.AddressField1, value, true); }
		}

		/// <summary> The AddressField2 property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."ADDRESSFIELD2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressField2
		{
			get { return (System.String)GetValue((int)AddressFieldIndex.AddressField2, true); }
			set	{ SetValue((int)AddressFieldIndex.AddressField2, value, true); }
		}

		/// <summary> The AddressID property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."ADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 AddressID
		{
			get { return (System.Int64)GetValue((int)AddressFieldIndex.AddressID, true); }
			set	{ SetValue((int)AddressFieldIndex.AddressID, value, true); }
		}

		/// <summary> The City property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."CITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String City
		{
			get { return (System.String)GetValue((int)AddressFieldIndex.City, true); }
			set	{ SetValue((int)AddressFieldIndex.City, value, true); }
		}

		/// <summary> The CoordinateID property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."COORDINATEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CoordinateID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AddressFieldIndex.CoordinateID, false); }
			set	{ SetValue((int)AddressFieldIndex.CoordinateID, value, true); }
		}

		/// <summary> The Country property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."COUNTRY"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Country
		{
			get { return (System.String)GetValue((int)AddressFieldIndex.Country, true); }
			set	{ SetValue((int)AddressFieldIndex.Country, value, true); }
		}

		/// <summary> The Description property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)AddressFieldIndex.Description, true); }
			set	{ SetValue((int)AddressFieldIndex.Description, value, true); }
		}

		/// <summary> The LastModified property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)AddressFieldIndex.LastModified, true); }
			set	{ SetValue((int)AddressFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)AddressFieldIndex.LastUser, true); }
			set	{ SetValue((int)AddressFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PostalCode property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."POSTALCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostalCode
		{
			get { return (System.String)GetValue((int)AddressFieldIndex.PostalCode, true); }
			set	{ SetValue((int)AddressFieldIndex.PostalCode, value, true); }
		}

		/// <summary> The Region property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."REGION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Region
		{
			get { return (System.String)GetValue((int)AddressFieldIndex.Region, true); }
			set	{ SetValue((int)AddressFieldIndex.Region, value, true); }
		}

		/// <summary> The Salutation property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."SALUTATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.Salutation Salutation
		{
			get { return (VarioSL.Entities.Enumerations.Salutation)GetValue((int)AddressFieldIndex.Salutation, true); }
			set	{ SetValue((int)AddressFieldIndex.Salutation, value, true); }
		}

		/// <summary> The Street property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."STREET"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Street
		{
			get { return (System.String)GetValue((int)AddressFieldIndex.Street, true); }
			set	{ SetValue((int)AddressFieldIndex.Street, value, true); }
		}

		/// <summary> The StreetNumber property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."STREETNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StreetNumber
		{
			get { return (System.String)GetValue((int)AddressFieldIndex.StreetNumber, true); }
			set	{ SetValue((int)AddressFieldIndex.StreetNumber, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)AddressFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)AddressFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AddressFieldIndex.ValidFrom, false); }
			set	{ SetValue((int)AddressFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidUntil property of the Entity Address<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ADDRESS"."VALIDUNTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidUntil
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AddressFieldIndex.ValidUntil, false); }
			set	{ SetValue((int)AddressFieldIndex.ValidUntil, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AddressBookEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAddressBookEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AddressBookEntryCollection AddressBookEntries
		{
			get	{ return GetMultiAddressBookEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AddressBookEntries. When set to true, AddressBookEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AddressBookEntries is accessed. You can always execute/ a forced fetch by calling GetMultiAddressBookEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAddressBookEntries
		{
			get	{ return _alwaysFetchAddressBookEntries; }
			set	{ _alwaysFetchAddressBookEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AddressBookEntries already has been fetched. Setting this property to false when AddressBookEntries has been fetched
		/// will clear the AddressBookEntries collection well. Setting this property to true while AddressBookEntries hasn't been fetched disables lazy loading for AddressBookEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAddressBookEntries
		{
			get { return _alreadyFetchedAddressBookEntries;}
			set 
			{
				if(_alreadyFetchedAddressBookEntries && !value && (_addressBookEntries != null))
				{
					_addressBookEntries.Clear();
				}
				_alreadyFetchedAddressBookEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractAddressEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContractAddresses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractAddressCollection ContractAddresses
		{
			get	{ return GetMultiContractAddresses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ContractAddresses. When set to true, ContractAddresses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContractAddresses is accessed. You can always execute/ a forced fetch by calling GetMultiContractAddresses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContractAddresses
		{
			get	{ return _alwaysFetchContractAddresses; }
			set	{ _alwaysFetchContractAddresses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ContractAddresses already has been fetched. Setting this property to false when ContractAddresses has been fetched
		/// will clear the ContractAddresses collection well. Setting this property to true while ContractAddresses hasn't been fetched disables lazy loading for ContractAddresses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContractAddresses
		{
			get { return _alreadyFetchedContractAddresses;}
			set 
			{
				if(_alreadyFetchedContractAddresses && !value && (_contractAddresses != null))
				{
					_contractAddresses.Clear();
				}
				_alreadyFetchedContractAddresses = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MobilityProviderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMobilityProviders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.MobilityProviderCollection MobilityProviders
		{
			get	{ return GetMultiMobilityProviders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MobilityProviders. When set to true, MobilityProviders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MobilityProviders is accessed. You can always execute/ a forced fetch by calling GetMultiMobilityProviders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMobilityProviders
		{
			get	{ return _alwaysFetchMobilityProviders; }
			set	{ _alwaysFetchMobilityProviders = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MobilityProviders already has been fetched. Setting this property to false when MobilityProviders has been fetched
		/// will clear the MobilityProviders collection well. Setting this property to true while MobilityProviders hasn't been fetched disables lazy loading for MobilityProviders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMobilityProviders
		{
			get { return _alreadyFetchedMobilityProviders;}
			set 
			{
				if(_alreadyFetchedMobilityProviders && !value && (_mobilityProviders != null))
				{
					_mobilityProviders.Clear();
				}
				_alreadyFetchedMobilityProviders = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiInvoiceAddressOrders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderCollection InvoiceAddressOrders
		{
			get	{ return GetMultiInvoiceAddressOrders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for InvoiceAddressOrders. When set to true, InvoiceAddressOrders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time InvoiceAddressOrders is accessed. You can always execute/ a forced fetch by calling GetMultiInvoiceAddressOrders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoiceAddressOrders
		{
			get	{ return _alwaysFetchInvoiceAddressOrders; }
			set	{ _alwaysFetchInvoiceAddressOrders = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property InvoiceAddressOrders already has been fetched. Setting this property to false when InvoiceAddressOrders has been fetched
		/// will clear the InvoiceAddressOrders collection well. Setting this property to true while InvoiceAddressOrders hasn't been fetched disables lazy loading for InvoiceAddressOrders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoiceAddressOrders
		{
			get { return _alreadyFetchedInvoiceAddressOrders;}
			set 
			{
				if(_alreadyFetchedInvoiceAddressOrders && !value && (_invoiceAddressOrders != null))
				{
					_invoiceAddressOrders.Clear();
				}
				_alreadyFetchedInvoiceAddressOrders = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiShippingAddressOrders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderCollection ShippingAddressOrders
		{
			get	{ return GetMultiShippingAddressOrders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ShippingAddressOrders. When set to true, ShippingAddressOrders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ShippingAddressOrders is accessed. You can always execute/ a forced fetch by calling GetMultiShippingAddressOrders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchShippingAddressOrders
		{
			get	{ return _alwaysFetchShippingAddressOrders; }
			set	{ _alwaysFetchShippingAddressOrders = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ShippingAddressOrders already has been fetched. Setting this property to false when ShippingAddressOrders has been fetched
		/// will clear the ShippingAddressOrders collection well. Setting this property to true while ShippingAddressOrders hasn't been fetched disables lazy loading for ShippingAddressOrders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedShippingAddressOrders
		{
			get { return _alreadyFetchedShippingAddressOrders;}
			set 
			{
				if(_alreadyFetchedShippingAddressOrders && !value && (_shippingAddressOrders != null))
				{
					_shippingAddressOrders.Clear();
				}
				_alreadyFetchedShippingAddressOrders = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrganizationAddressEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrganizationAddresses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationAddressCollection OrganizationAddresses
		{
			get	{ return GetMultiOrganizationAddresses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrganizationAddresses. When set to true, OrganizationAddresses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrganizationAddresses is accessed. You can always execute/ a forced fetch by calling GetMultiOrganizationAddresses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganizationAddresses
		{
			get	{ return _alwaysFetchOrganizationAddresses; }
			set	{ _alwaysFetchOrganizationAddresses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrganizationAddresses already has been fetched. Setting this property to false when OrganizationAddresses has been fetched
		/// will clear the OrganizationAddresses collection well. Setting this property to true while OrganizationAddresses hasn't been fetched disables lazy loading for OrganizationAddresses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganizationAddresses
		{
			get { return _alreadyFetchedOrganizationAddresses;}
			set 
			{
				if(_alreadyFetchedOrganizationAddresses && !value && (_organizationAddresses != null))
				{
					_organizationAddresses.Clear();
				}
				_alreadyFetchedOrganizationAddresses = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PersonAddressEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPersonAddresses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PersonAddressCollection PersonAddresses
		{
			get	{ return GetMultiPersonAddresses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PersonAddresses. When set to true, PersonAddresses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PersonAddresses is accessed. You can always execute/ a forced fetch by calling GetMultiPersonAddresses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPersonAddresses
		{
			get	{ return _alwaysFetchPersonAddresses; }
			set	{ _alwaysFetchPersonAddresses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PersonAddresses already has been fetched. Setting this property to false when PersonAddresses has been fetched
		/// will clear the PersonAddresses collection well. Setting this property to true while PersonAddresses hasn't been fetched disables lazy loading for PersonAddresses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPersonAddresses
		{
			get { return _alreadyFetchedPersonAddresses;}
			set 
			{
				if(_alreadyFetchedPersonAddresses && !value && (_personAddresses != null))
				{
					_personAddresses.Clear();
				}
				_alreadyFetchedPersonAddresses = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductRelationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductRelationsFrom()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductRelationCollection ProductRelationsFrom
		{
			get	{ return GetMultiProductRelationsFrom(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductRelationsFrom. When set to true, ProductRelationsFrom is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductRelationsFrom is accessed. You can always execute/ a forced fetch by calling GetMultiProductRelationsFrom(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductRelationsFrom
		{
			get	{ return _alwaysFetchProductRelationsFrom; }
			set	{ _alwaysFetchProductRelationsFrom = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductRelationsFrom already has been fetched. Setting this property to false when ProductRelationsFrom has been fetched
		/// will clear the ProductRelationsFrom collection well. Setting this property to true while ProductRelationsFrom hasn't been fetched disables lazy loading for ProductRelationsFrom</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductRelationsFrom
		{
			get { return _alreadyFetchedProductRelationsFrom;}
			set 
			{
				if(_alreadyFetchedProductRelationsFrom && !value && (_productRelationsFrom != null))
				{
					_productRelationsFrom.Clear();
				}
				_alreadyFetchedProductRelationsFrom = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductRelationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductRelationsTo()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductRelationCollection ProductRelationsTo
		{
			get	{ return GetMultiProductRelationsTo(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductRelationsTo. When set to true, ProductRelationsTo is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductRelationsTo is accessed. You can always execute/ a forced fetch by calling GetMultiProductRelationsTo(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductRelationsTo
		{
			get	{ return _alwaysFetchProductRelationsTo; }
			set	{ _alwaysFetchProductRelationsTo = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductRelationsTo already has been fetched. Setting this property to false when ProductRelationsTo has been fetched
		/// will clear the ProductRelationsTo collection well. Setting this property to true while ProductRelationsTo hasn't been fetched disables lazy loading for ProductRelationsTo</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductRelationsTo
		{
			get { return _alreadyFetchedProductRelationsTo;}
			set 
			{
				if(_alreadyFetchedProductRelationsTo && !value && (_productRelationsTo != null))
				{
					_productRelationsTo.Clear();
				}
				_alreadyFetchedProductRelationsTo = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'StorageLocationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiStorageLocations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.StorageLocationCollection StorageLocations
		{
			get	{ return GetMultiStorageLocations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for StorageLocations. When set to true, StorageLocations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time StorageLocations is accessed. You can always execute/ a forced fetch by calling GetMultiStorageLocations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchStorageLocations
		{
			get	{ return _alwaysFetchStorageLocations; }
			set	{ _alwaysFetchStorageLocations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property StorageLocations already has been fetched. Setting this property to false when StorageLocations has been fetched
		/// will clear the StorageLocations collection well. Setting this property to true while StorageLocations hasn't been fetched disables lazy loading for StorageLocations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedStorageLocations
		{
			get { return _alreadyFetchedStorageLocations;}
			set 
			{
				if(_alreadyFetchedStorageLocations && !value && (_storageLocations != null))
				{
					_storageLocations.Clear();
				}
				_alreadyFetchedStorageLocations = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContracts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractCollection Contracts
		{
			get { return GetMultiContracts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Contracts. When set to true, Contracts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contracts is accessed. You can always execute a forced fetch by calling GetMultiContracts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContracts
		{
			get	{ return _alwaysFetchContracts; }
			set	{ _alwaysFetchContracts = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contracts already has been fetched. Setting this property to false when Contracts has been fetched
		/// will clear the Contracts collection well. Setting this property to true while Contracts hasn't been fetched disables lazy loading for Contracts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContracts
		{
			get { return _alreadyFetchedContracts;}
			set 
			{
				if(_alreadyFetchedContracts && !value && (_contracts != null))
				{
					_contracts.Clear();
				}
				_alreadyFetchedContracts = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PersonEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPersons()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PersonCollection Persons
		{
			get { return GetMultiPersons(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Persons. When set to true, Persons is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Persons is accessed. You can always execute a forced fetch by calling GetMultiPersons(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPersons
		{
			get	{ return _alwaysFetchPersons; }
			set	{ _alwaysFetchPersons = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Persons already has been fetched. Setting this property to false when Persons has been fetched
		/// will clear the Persons collection well. Setting this property to true while Persons hasn't been fetched disables lazy loading for Persons</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPersons
		{
			get { return _alreadyFetchedPersons;}
			set 
			{
				if(_alreadyFetchedPersons && !value && (_persons != null))
				{
					_persons.Clear();
				}
				_alreadyFetchedPersons = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CoordinateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCoordinate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CoordinateEntity Coordinate
		{
			get	{ return GetSingleCoordinate(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCoordinate(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Addresses", "Coordinate", _coordinate, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Coordinate. When set to true, Coordinate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Coordinate is accessed. You can always execute a forced fetch by calling GetSingleCoordinate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCoordinate
		{
			get	{ return _alwaysFetchCoordinate; }
			set	{ _alwaysFetchCoordinate = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Coordinate already has been fetched. Setting this property to false when Coordinate has been fetched
		/// will set Coordinate to null as well. Setting this property to true while Coordinate hasn't been fetched disables lazy loading for Coordinate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCoordinate
		{
			get { return _alreadyFetchedCoordinate;}
			set 
			{
				if(_alreadyFetchedCoordinate && !value)
				{
					this.Coordinate = null;
				}
				_alreadyFetchedCoordinate = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Coordinate is not found
		/// in the database. When set to true, Coordinate will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CoordinateReturnsNewIfNotFound
		{
			get	{ return _coordinateReturnsNewIfNotFound; }
			set { _coordinateReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardHolderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardHolder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardHolderEntity CardHolder
		{
			get	{ return GetSingleCardHolder(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncCardHolder(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_cardHolder !=null);
						DesetupSyncCardHolder(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("CardHolder");
						}
					}
					else
					{
						if(_cardHolder!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "AlternativeAddress");
							SetupSyncCardHolder(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardHolder. When set to true, CardHolder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardHolder is accessed. You can always execute a forced fetch by calling GetSingleCardHolder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardHolder
		{
			get	{ return _alwaysFetchCardHolder; }
			set	{ _alwaysFetchCardHolder = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property CardHolder already has been fetched. Setting this property to false when CardHolder has been fetched
		/// will set CardHolder to null as well. Setting this property to true while CardHolder hasn't been fetched disables lazy loading for CardHolder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardHolder
		{
			get { return _alreadyFetchedCardHolder;}
			set 
			{
				if(_alreadyFetchedCardHolder && !value)
				{
					this.CardHolder = null;
				}
				_alreadyFetchedCardHolder = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardHolder is not found
		/// in the database. When set to true, CardHolder will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardHolderReturnsNewIfNotFound
		{
			get	{ return _cardHolderReturnsNewIfNotFound; }
			set	{ _cardHolderReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity Person
		{
			get	{ return GetSinglePerson(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPerson(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_person !=null);
						DesetupSyncPerson(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("Person");
						}
					}
					else
					{
						if(_person!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Address");
							SetupSyncPerson(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Person. When set to true, Person is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Person is accessed. You can always execute a forced fetch by calling GetSinglePerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerson
		{
			get	{ return _alwaysFetchPerson; }
			set	{ _alwaysFetchPerson = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property Person already has been fetched. Setting this property to false when Person has been fetched
		/// will set Person to null as well. Setting this property to true while Person hasn't been fetched disables lazy loading for Person</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerson
		{
			get { return _alreadyFetchedPerson;}
			set 
			{
				if(_alreadyFetchedPerson && !value)
				{
					this.Person = null;
				}
				_alreadyFetchedPerson = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Person is not found
		/// in the database. When set to true, Person will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PersonReturnsNewIfNotFound
		{
			get	{ return _personReturnsNewIfNotFound; }
			set	{ _personReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AddressEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
