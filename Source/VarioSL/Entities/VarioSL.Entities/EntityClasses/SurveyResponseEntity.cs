﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SurveyResponse'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SurveyResponseEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.SurveyAnswerCollection	_surveyAnswers;
		private bool	_alwaysFetchSurveyAnswers, _alreadyFetchedSurveyAnswers;
		private PersonEntity _person;
		private bool	_alwaysFetchPerson, _alreadyFetchedPerson, _personReturnsNewIfNotFound;
		private SurveyEntity _survey;
		private bool	_alwaysFetchSurvey, _alreadyFetchedSurvey, _surveyReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Person</summary>
			public static readonly string Person = "Person";
			/// <summary>Member name Survey</summary>
			public static readonly string Survey = "Survey";
			/// <summary>Member name SurveyAnswers</summary>
			public static readonly string SurveyAnswers = "SurveyAnswers";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SurveyResponseEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SurveyResponseEntity() :base("SurveyResponseEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="surveyResponseID">PK value for SurveyResponse which data should be fetched into this SurveyResponse object</param>
		public SurveyResponseEntity(System.Int64 surveyResponseID):base("SurveyResponseEntity")
		{
			InitClassFetch(surveyResponseID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyResponseID">PK value for SurveyResponse which data should be fetched into this SurveyResponse object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SurveyResponseEntity(System.Int64 surveyResponseID, IPrefetchPath prefetchPathToUse):base("SurveyResponseEntity")
		{
			InitClassFetch(surveyResponseID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyResponseID">PK value for SurveyResponse which data should be fetched into this SurveyResponse object</param>
		/// <param name="validator">The custom validator object for this SurveyResponseEntity</param>
		public SurveyResponseEntity(System.Int64 surveyResponseID, IValidator validator):base("SurveyResponseEntity")
		{
			InitClassFetch(surveyResponseID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SurveyResponseEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_surveyAnswers = (VarioSL.Entities.CollectionClasses.SurveyAnswerCollection)info.GetValue("_surveyAnswers", typeof(VarioSL.Entities.CollectionClasses.SurveyAnswerCollection));
			_alwaysFetchSurveyAnswers = info.GetBoolean("_alwaysFetchSurveyAnswers");
			_alreadyFetchedSurveyAnswers = info.GetBoolean("_alreadyFetchedSurveyAnswers");
			_person = (PersonEntity)info.GetValue("_person", typeof(PersonEntity));
			if(_person!=null)
			{
				_person.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_personReturnsNewIfNotFound = info.GetBoolean("_personReturnsNewIfNotFound");
			_alwaysFetchPerson = info.GetBoolean("_alwaysFetchPerson");
			_alreadyFetchedPerson = info.GetBoolean("_alreadyFetchedPerson");

			_survey = (SurveyEntity)info.GetValue("_survey", typeof(SurveyEntity));
			if(_survey!=null)
			{
				_survey.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_surveyReturnsNewIfNotFound = info.GetBoolean("_surveyReturnsNewIfNotFound");
			_alwaysFetchSurvey = info.GetBoolean("_alwaysFetchSurvey");
			_alreadyFetchedSurvey = info.GetBoolean("_alreadyFetchedSurvey");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SurveyResponseFieldIndex)fieldIndex)
			{
				case SurveyResponseFieldIndex.PersonID:
					DesetupSyncPerson(true, false);
					_alreadyFetchedPerson = false;
					break;
				case SurveyResponseFieldIndex.SurveyID:
					DesetupSyncSurvey(true, false);
					_alreadyFetchedSurvey = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSurveyAnswers = (_surveyAnswers.Count > 0);
			_alreadyFetchedPerson = (_person != null);
			_alreadyFetchedSurvey = (_survey != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Person":
					toReturn.Add(Relations.PersonEntityUsingPersonID);
					break;
				case "Survey":
					toReturn.Add(Relations.SurveyEntityUsingSurveyID);
					break;
				case "SurveyAnswers":
					toReturn.Add(Relations.SurveyAnswerEntityUsingSurveyResponseID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_surveyAnswers", (!this.MarkedForDeletion?_surveyAnswers:null));
			info.AddValue("_alwaysFetchSurveyAnswers", _alwaysFetchSurveyAnswers);
			info.AddValue("_alreadyFetchedSurveyAnswers", _alreadyFetchedSurveyAnswers);
			info.AddValue("_person", (!this.MarkedForDeletion?_person:null));
			info.AddValue("_personReturnsNewIfNotFound", _personReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerson", _alwaysFetchPerson);
			info.AddValue("_alreadyFetchedPerson", _alreadyFetchedPerson);
			info.AddValue("_survey", (!this.MarkedForDeletion?_survey:null));
			info.AddValue("_surveyReturnsNewIfNotFound", _surveyReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSurvey", _alwaysFetchSurvey);
			info.AddValue("_alreadyFetchedSurvey", _alreadyFetchedSurvey);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Person":
					_alreadyFetchedPerson = true;
					this.Person = (PersonEntity)entity;
					break;
				case "Survey":
					_alreadyFetchedSurvey = true;
					this.Survey = (SurveyEntity)entity;
					break;
				case "SurveyAnswers":
					_alreadyFetchedSurveyAnswers = true;
					if(entity!=null)
					{
						this.SurveyAnswers.Add((SurveyAnswerEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Person":
					SetupSyncPerson(relatedEntity);
					break;
				case "Survey":
					SetupSyncSurvey(relatedEntity);
					break;
				case "SurveyAnswers":
					_surveyAnswers.Add((SurveyAnswerEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Person":
					DesetupSyncPerson(false, true);
					break;
				case "Survey":
					DesetupSyncSurvey(false, true);
					break;
				case "SurveyAnswers":
					this.PerformRelatedEntityRemoval(_surveyAnswers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_person!=null)
			{
				toReturn.Add(_person);
			}
			if(_survey!=null)
			{
				toReturn.Add(_survey);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_surveyAnswers);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyResponseID">PK value for SurveyResponse which data should be fetched into this SurveyResponse object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyResponseID)
		{
			return FetchUsingPK(surveyResponseID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyResponseID">PK value for SurveyResponse which data should be fetched into this SurveyResponse object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyResponseID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(surveyResponseID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyResponseID">PK value for SurveyResponse which data should be fetched into this SurveyResponse object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyResponseID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(surveyResponseID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyResponseID">PK value for SurveyResponse which data should be fetched into this SurveyResponse object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyResponseID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(surveyResponseID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SurveyResponseID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SurveyResponseRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswers(bool forceFetch)
		{
			return GetMultiSurveyAnswers(forceFetch, _surveyAnswers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyAnswers(forceFetch, _surveyAnswers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyAnswers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyAnswers || forceFetch || _alwaysFetchSurveyAnswers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyAnswers);
				_surveyAnswers.SuppressClearInGetMulti=!forceFetch;
				_surveyAnswers.EntityFactoryToUse = entityFactoryToUse;
				_surveyAnswers.GetMultiManyToOne(null, null, this, filter);
				_surveyAnswers.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyAnswers = true;
			}
			return _surveyAnswers;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyAnswers'. These settings will be taken into account
		/// when the property SurveyAnswers is requested or GetMultiSurveyAnswers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyAnswers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyAnswers.SortClauses=sortClauses;
			_surveyAnswers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSinglePerson()
		{
			return GetSinglePerson(false);
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSinglePerson(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerson || forceFetch || _alwaysFetchPerson) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingPersonID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphic(this.Transaction, this.PersonID, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_personReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Person = newEntity;
				_alreadyFetchedPerson = fetchResult;
			}
			return _person;
		}


		/// <summary> Retrieves the related entity of type 'SurveyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyEntity' which is related to this entity.</returns>
		public SurveyEntity GetSingleSurvey()
		{
			return GetSingleSurvey(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyEntity' which is related to this entity.</returns>
		public virtual SurveyEntity GetSingleSurvey(bool forceFetch)
		{
			if( ( !_alreadyFetchedSurvey || forceFetch || _alwaysFetchSurvey) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyEntityUsingSurveyID);
				SurveyEntity newEntity = new SurveyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SurveyID);
				}
				if(fetchResult)
				{
					newEntity = (SurveyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_surveyReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Survey = newEntity;
				_alreadyFetchedSurvey = fetchResult;
			}
			return _survey;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Person", _person);
			toReturn.Add("Survey", _survey);
			toReturn.Add("SurveyAnswers", _surveyAnswers);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="surveyResponseID">PK value for SurveyResponse which data should be fetched into this SurveyResponse object</param>
		/// <param name="validator">The validator object for this SurveyResponseEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 surveyResponseID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(surveyResponseID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_surveyAnswers = new VarioSL.Entities.CollectionClasses.SurveyAnswerCollection();
			_surveyAnswers.SetContainingEntityInfo(this, "SurveyResponse");
			_personReturnsNewIfNotFound = false;
			_surveyReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyResponseID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _person</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerson(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticSurveyResponseRelations.PersonEntityUsingPersonIDStatic, true, signalRelatedEntity, "SurveyResponses", resetFKFields, new int[] { (int)SurveyResponseFieldIndex.PersonID } );		
			_person = null;
		}
		
		/// <summary> setups the sync logic for member _person</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerson(IEntityCore relatedEntity)
		{
			if(_person!=relatedEntity)
			{		
				DesetupSyncPerson(true, true);
				_person = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticSurveyResponseRelations.PersonEntityUsingPersonIDStatic, true, ref _alreadyFetchedPerson, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPersonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _survey</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSurvey(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _survey, new PropertyChangedEventHandler( OnSurveyPropertyChanged ), "Survey", VarioSL.Entities.RelationClasses.StaticSurveyResponseRelations.SurveyEntityUsingSurveyIDStatic, true, signalRelatedEntity, "SurveyResponses", resetFKFields, new int[] { (int)SurveyResponseFieldIndex.SurveyID } );		
			_survey = null;
		}
		
		/// <summary> setups the sync logic for member _survey</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSurvey(IEntityCore relatedEntity)
		{
			if(_survey!=relatedEntity)
			{		
				DesetupSyncSurvey(true, true);
				_survey = (SurveyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _survey, new PropertyChangedEventHandler( OnSurveyPropertyChanged ), "Survey", VarioSL.Entities.RelationClasses.StaticSurveyResponseRelations.SurveyEntityUsingSurveyIDStatic, true, ref _alreadyFetchedSurvey, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSurveyPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="surveyResponseID">PK value for SurveyResponse which data should be fetched into this SurveyResponse object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 surveyResponseID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SurveyResponseFieldIndex.SurveyResponseID].ForcedCurrentValueWrite(surveyResponseID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSurveyResponseDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SurveyResponseEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SurveyResponseRelations Relations
		{
			get	{ return new SurveyResponseRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyAnswer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyAnswers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyAnswerCollection(), (IEntityRelation)GetRelationsForField("SurveyAnswers")[0], (int)VarioSL.Entities.EntityType.SurveyResponseEntity, (int)VarioSL.Entities.EntityType.SurveyAnswerEntity, 0, null, null, null, "SurveyAnswers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerson
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("Person")[0], (int)VarioSL.Entities.EntityType.SurveyResponseEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "Person", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Survey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurvey
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyCollection(), (IEntityRelation)GetRelationsForField("Survey")[0], (int)VarioSL.Entities.EntityType.SurveyResponseEntity, (int)VarioSL.Entities.EntityType.SurveyEntity, 0, null, null, null, "Survey", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LastModified property of the Entity SurveyResponse<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYRESPONSE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SurveyResponseFieldIndex.LastModified, true); }
			set	{ SetValue((int)SurveyResponseFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity SurveyResponse<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYRESPONSE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SurveyResponseFieldIndex.LastUser, true); }
			set	{ SetValue((int)SurveyResponseFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PersonID property of the Entity SurveyResponse<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYRESPONSE"."PERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PersonID
		{
			get { return (System.Int64)GetValue((int)SurveyResponseFieldIndex.PersonID, true); }
			set	{ SetValue((int)SurveyResponseFieldIndex.PersonID, value, true); }
		}

		/// <summary> The SurveyID property of the Entity SurveyResponse<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYRESPONSE"."SURVEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SurveyID
		{
			get { return (System.Int64)GetValue((int)SurveyResponseFieldIndex.SurveyID, true); }
			set	{ SetValue((int)SurveyResponseFieldIndex.SurveyID, value, true); }
		}

		/// <summary> The SurveyResponseID property of the Entity SurveyResponse<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYRESPONSE"."SURVEYRESPONSEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SurveyResponseID
		{
			get { return (System.Int64)GetValue((int)SurveyResponseFieldIndex.SurveyResponseID, true); }
			set	{ SetValue((int)SurveyResponseFieldIndex.SurveyResponseID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity SurveyResponse<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYRESPONSE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SurveyResponseFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SurveyResponseFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyAnswers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SurveyAnswerCollection SurveyAnswers
		{
			get	{ return GetMultiSurveyAnswers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyAnswers. When set to true, SurveyAnswers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyAnswers is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyAnswers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyAnswers
		{
			get	{ return _alwaysFetchSurveyAnswers; }
			set	{ _alwaysFetchSurveyAnswers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyAnswers already has been fetched. Setting this property to false when SurveyAnswers has been fetched
		/// will clear the SurveyAnswers collection well. Setting this property to true while SurveyAnswers hasn't been fetched disables lazy loading for SurveyAnswers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyAnswers
		{
			get { return _alreadyFetchedSurveyAnswers;}
			set 
			{
				if(_alreadyFetchedSurveyAnswers && !value && (_surveyAnswers != null))
				{
					_surveyAnswers.Clear();
				}
				_alreadyFetchedSurveyAnswers = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity Person
		{
			get	{ return GetSinglePerson(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPerson(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyResponses", "Person", _person, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Person. When set to true, Person is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Person is accessed. You can always execute a forced fetch by calling GetSinglePerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerson
		{
			get	{ return _alwaysFetchPerson; }
			set	{ _alwaysFetchPerson = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Person already has been fetched. Setting this property to false when Person has been fetched
		/// will set Person to null as well. Setting this property to true while Person hasn't been fetched disables lazy loading for Person</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerson
		{
			get { return _alreadyFetchedPerson;}
			set 
			{
				if(_alreadyFetchedPerson && !value)
				{
					this.Person = null;
				}
				_alreadyFetchedPerson = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Person is not found
		/// in the database. When set to true, Person will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PersonReturnsNewIfNotFound
		{
			get	{ return _personReturnsNewIfNotFound; }
			set { _personReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SurveyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSurvey()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SurveyEntity Survey
		{
			get	{ return GetSingleSurvey(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSurvey(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyResponses", "Survey", _survey, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Survey. When set to true, Survey is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Survey is accessed. You can always execute a forced fetch by calling GetSingleSurvey(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurvey
		{
			get	{ return _alwaysFetchSurvey; }
			set	{ _alwaysFetchSurvey = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Survey already has been fetched. Setting this property to false when Survey has been fetched
		/// will set Survey to null as well. Setting this property to true while Survey hasn't been fetched disables lazy loading for Survey</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurvey
		{
			get { return _alreadyFetchedSurvey;}
			set 
			{
				if(_alreadyFetchedSurvey && !value)
				{
					this.Survey = null;
				}
				_alreadyFetchedSurvey = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Survey is not found
		/// in the database. When set to true, Survey will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SurveyReturnsNewIfNotFound
		{
			get	{ return _surveyReturnsNewIfNotFound; }
			set { _surveyReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SurveyResponseEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
