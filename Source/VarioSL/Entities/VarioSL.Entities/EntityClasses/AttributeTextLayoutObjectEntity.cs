﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AttributeTextLayoutObject'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AttributeTextLayoutObjectEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AttributeEntity _attribute;
		private bool	_alwaysFetchAttribute, _alreadyFetchedAttribute, _attributeReturnsNewIfNotFound;
		private LayoutEntity _layout;
		private bool	_alwaysFetchLayout, _alreadyFetchedLayout, _layoutReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Attribute</summary>
			public static readonly string Attribute = "Attribute";
			/// <summary>Member name Layout</summary>
			public static readonly string Layout = "Layout";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AttributeTextLayoutObjectEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AttributeTextLayoutObjectEntity() :base("AttributeTextLayoutObjectEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for AttributeTextLayoutObject which data should be fetched into this AttributeTextLayoutObject object</param>
		public AttributeTextLayoutObjectEntity(System.Int64 layoutObjectID):base("AttributeTextLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for AttributeTextLayoutObject which data should be fetched into this AttributeTextLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AttributeTextLayoutObjectEntity(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse):base("AttributeTextLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for AttributeTextLayoutObject which data should be fetched into this AttributeTextLayoutObject object</param>
		/// <param name="validator">The custom validator object for this AttributeTextLayoutObjectEntity</param>
		public AttributeTextLayoutObjectEntity(System.Int64 layoutObjectID, IValidator validator):base("AttributeTextLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AttributeTextLayoutObjectEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_attribute = (AttributeEntity)info.GetValue("_attribute", typeof(AttributeEntity));
			if(_attribute!=null)
			{
				_attribute.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributeReturnsNewIfNotFound = info.GetBoolean("_attributeReturnsNewIfNotFound");
			_alwaysFetchAttribute = info.GetBoolean("_alwaysFetchAttribute");
			_alreadyFetchedAttribute = info.GetBoolean("_alreadyFetchedAttribute");

			_layout = (LayoutEntity)info.GetValue("_layout", typeof(LayoutEntity));
			if(_layout!=null)
			{
				_layout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_layoutReturnsNewIfNotFound = info.GetBoolean("_layoutReturnsNewIfNotFound");
			_alwaysFetchLayout = info.GetBoolean("_alwaysFetchLayout");
			_alreadyFetchedLayout = info.GetBoolean("_alreadyFetchedLayout");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AttributeTextLayoutObjectFieldIndex)fieldIndex)
			{
				case AttributeTextLayoutObjectFieldIndex.AttributeID:
					DesetupSyncAttribute(true, false);
					_alreadyFetchedAttribute = false;
					break;
				case AttributeTextLayoutObjectFieldIndex.LayoutID:
					DesetupSyncLayout(true, false);
					_alreadyFetchedLayout = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAttribute = (_attribute != null);
			_alreadyFetchedLayout = (_layout != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Attribute":
					toReturn.Add(Relations.AttributeEntityUsingAttributeID);
					break;
				case "Layout":
					toReturn.Add(Relations.LayoutEntityUsingLayoutID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_attribute", (!this.MarkedForDeletion?_attribute:null));
			info.AddValue("_attributeReturnsNewIfNotFound", _attributeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttribute", _alwaysFetchAttribute);
			info.AddValue("_alreadyFetchedAttribute", _alreadyFetchedAttribute);
			info.AddValue("_layout", (!this.MarkedForDeletion?_layout:null));
			info.AddValue("_layoutReturnsNewIfNotFound", _layoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLayout", _alwaysFetchLayout);
			info.AddValue("_alreadyFetchedLayout", _alreadyFetchedLayout);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Attribute":
					_alreadyFetchedAttribute = true;
					this.Attribute = (AttributeEntity)entity;
					break;
				case "Layout":
					_alreadyFetchedLayout = true;
					this.Layout = (LayoutEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Attribute":
					SetupSyncAttribute(relatedEntity);
					break;
				case "Layout":
					SetupSyncLayout(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Attribute":
					DesetupSyncAttribute(false, true);
					break;
				case "Layout":
					DesetupSyncLayout(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_attribute!=null)
			{
				toReturn.Add(_attribute);
			}
			if(_layout!=null)
			{
				toReturn.Add(_layout);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for AttributeTextLayoutObject which data should be fetched into this AttributeTextLayoutObject object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID)
		{
			return FetchUsingPK(layoutObjectID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for AttributeTextLayoutObject which data should be fetched into this AttributeTextLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(layoutObjectID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for AttributeTextLayoutObject which data should be fetched into this AttributeTextLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(layoutObjectID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for AttributeTextLayoutObject which data should be fetched into this AttributeTextLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(layoutObjectID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.LayoutObjectID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AttributeTextLayoutObjectRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AttributeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeEntity' which is related to this entity.</returns>
		public AttributeEntity GetSingleAttribute()
		{
			return GetSingleAttribute(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeEntity' which is related to this entity.</returns>
		public virtual AttributeEntity GetSingleAttribute(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttribute || forceFetch || _alwaysFetchAttribute) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeEntityUsingAttributeID);
				AttributeEntity newEntity = new AttributeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AttributeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Attribute = newEntity;
				_alreadyFetchedAttribute = fetchResult;
			}
			return _attribute;
		}


		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSingleLayout()
		{
			return GetSingleLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSingleLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedLayout || forceFetch || _alwaysFetchLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingLayoutID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LayoutID);
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_layoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Layout = newEntity;
				_alreadyFetchedLayout = fetchResult;
			}
			return _layout;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Attribute", _attribute);
			toReturn.Add("Layout", _layout);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="layoutObjectID">PK value for AttributeTextLayoutObject which data should be fetched into this AttributeTextLayoutObject object</param>
		/// <param name="validator">The validator object for this AttributeTextLayoutObjectEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 layoutObjectID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(layoutObjectID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_attributeReturnsNewIfNotFound = false;
			_layoutReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Column", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldWidth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FontSize", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Height", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsBold", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDouble", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsInverse", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsUnderlined", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutObjectID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxLength", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinLength", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoLayoutText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Row", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TextAlignment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Width", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _attribute</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttribute(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attribute, new PropertyChangedEventHandler( OnAttributePropertyChanged ), "Attribute", VarioSL.Entities.RelationClasses.StaticAttributeTextLayoutObjectRelations.AttributeEntityUsingAttributeIDStatic, true, signalRelatedEntity, "AttributeTextLayoutObjects", resetFKFields, new int[] { (int)AttributeTextLayoutObjectFieldIndex.AttributeID } );		
			_attribute = null;
		}
		
		/// <summary> setups the sync logic for member _attribute</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttribute(IEntityCore relatedEntity)
		{
			if(_attribute!=relatedEntity)
			{		
				DesetupSyncAttribute(true, true);
				_attribute = (AttributeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attribute, new PropertyChangedEventHandler( OnAttributePropertyChanged ), "Attribute", VarioSL.Entities.RelationClasses.StaticAttributeTextLayoutObjectRelations.AttributeEntityUsingAttributeIDStatic, true, ref _alreadyFetchedAttribute, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _layout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _layout, new PropertyChangedEventHandler( OnLayoutPropertyChanged ), "Layout", VarioSL.Entities.RelationClasses.StaticAttributeTextLayoutObjectRelations.LayoutEntityUsingLayoutIDStatic, true, signalRelatedEntity, "AttributeTextLayoutObjects", resetFKFields, new int[] { (int)AttributeTextLayoutObjectFieldIndex.LayoutID } );		
			_layout = null;
		}
		
		/// <summary> setups the sync logic for member _layout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLayout(IEntityCore relatedEntity)
		{
			if(_layout!=relatedEntity)
			{		
				DesetupSyncLayout(true, true);
				_layout = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _layout, new PropertyChangedEventHandler( OnLayoutPropertyChanged ), "Layout", VarioSL.Entities.RelationClasses.StaticAttributeTextLayoutObjectRelations.LayoutEntityUsingLayoutIDStatic, true, ref _alreadyFetchedLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="layoutObjectID">PK value for AttributeTextLayoutObject which data should be fetched into this AttributeTextLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AttributeTextLayoutObjectFieldIndex.LayoutObjectID].ForcedCurrentValueWrite(layoutObjectID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAttributeTextLayoutObjectDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AttributeTextLayoutObjectEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AttributeTextLayoutObjectRelations Relations
		{
			get	{ return new AttributeTextLayoutObjectRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Attribute'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttribute
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeCollection(), (IEntityRelation)GetRelationsForField("Attribute")[0], (int)VarioSL.Entities.EntityType.AttributeTextLayoutObjectEntity, (int)VarioSL.Entities.EntityType.AttributeEntity, 0, null, null, null, "Attribute", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("Layout")[0], (int)VarioSL.Entities.EntityType.AttributeTextLayoutObjectEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "Layout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AttributeID property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."ATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AttributeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AttributeTextLayoutObjectFieldIndex.AttributeID, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.AttributeID, value, true); }
		}

		/// <summary> The Column property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."COLUMN_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Column
		{
			get { return (System.Double)GetValue((int)AttributeTextLayoutObjectFieldIndex.Column, true); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.Column, value, true); }
		}

		/// <summary> The FieldWidth property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."FIELDWIDTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> FieldWidth
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeTextLayoutObjectFieldIndex.FieldWidth, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.FieldWidth, value, true); }
		}

		/// <summary> The FontSize property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."FONTSIZE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> FontSize
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeTextLayoutObjectFieldIndex.FontSize, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.FontSize, value, true); }
		}

		/// <summary> The Height property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."HEIGHT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Height
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeTextLayoutObjectFieldIndex.Height, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.Height, value, true); }
		}

		/// <summary> The IsBold property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."ISBOLD"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsBold
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeTextLayoutObjectFieldIndex.IsBold, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.IsBold, value, true); }
		}

		/// <summary> The IsDouble property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."ISDOUBLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsDouble
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeTextLayoutObjectFieldIndex.IsDouble, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.IsDouble, value, true); }
		}

		/// <summary> The IsInverse property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."ISINVERSE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsInverse
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeTextLayoutObjectFieldIndex.IsInverse, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.IsInverse, value, true); }
		}

		/// <summary> The IsUnderlined property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."ISUNDERLINED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsUnderlined
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeTextLayoutObjectFieldIndex.IsUnderlined, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.IsUnderlined, value, true); }
		}

		/// <summary> The LayoutID property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."LAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 LayoutID
		{
			get { return (System.Int64)GetValue((int)AttributeTextLayoutObjectFieldIndex.LayoutID, true); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.LayoutID, value, true); }
		}

		/// <summary> The LayoutObjectID property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."LAYOUTOBJECTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 LayoutObjectID
		{
			get { return (System.Int64)GetValue((int)AttributeTextLayoutObjectFieldIndex.LayoutObjectID, true); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.LayoutObjectID, value, true); }
		}

		/// <summary> The MaxLength property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."MAXLENGTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MaxLength
		{
			get { return (Nullable<System.Int64>)GetValue((int)AttributeTextLayoutObjectFieldIndex.MaxLength, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.MaxLength, value, true); }
		}

		/// <summary> The MinLength property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."MINLENGTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MinLength
		{
			get { return (Nullable<System.Int64>)GetValue((int)AttributeTextLayoutObjectFieldIndex.MinLength, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.MinLength, value, true); }
		}

		/// <summary> The NoLayoutText property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."NOLAYOUTTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NoLayoutText
		{
			get { return (Nullable<System.Int64>)GetValue((int)AttributeTextLayoutObjectFieldIndex.NoLayoutText, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.NoLayoutText, value, true); }
		}

		/// <summary> The Row property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."ROW_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Row
		{
			get { return (System.Double)GetValue((int)AttributeTextLayoutObjectFieldIndex.Row, true); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.Row, value, true); }
		}

		/// <summary> The TextAlignment property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."TEXTALIGNMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> TextAlignment
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeTextLayoutObjectFieldIndex.TextAlignment, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.TextAlignment, value, true); }
		}

		/// <summary> The Width property of the Entity AttributeTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTETEXTLAYOUTOBJECT"."WIDTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Width
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeTextLayoutObjectFieldIndex.Width, false); }
			set	{ SetValue((int)AttributeTextLayoutObjectFieldIndex.Width, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AttributeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttribute()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeEntity Attribute
		{
			get	{ return GetSingleAttribute(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttribute(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AttributeTextLayoutObjects", "Attribute", _attribute, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Attribute. When set to true, Attribute is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Attribute is accessed. You can always execute a forced fetch by calling GetSingleAttribute(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttribute
		{
			get	{ return _alwaysFetchAttribute; }
			set	{ _alwaysFetchAttribute = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Attribute already has been fetched. Setting this property to false when Attribute has been fetched
		/// will set Attribute to null as well. Setting this property to true while Attribute hasn't been fetched disables lazy loading for Attribute</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttribute
		{
			get { return _alreadyFetchedAttribute;}
			set 
			{
				if(_alreadyFetchedAttribute && !value)
				{
					this.Attribute = null;
				}
				_alreadyFetchedAttribute = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Attribute is not found
		/// in the database. When set to true, Attribute will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributeReturnsNewIfNotFound
		{
			get	{ return _attributeReturnsNewIfNotFound; }
			set { _attributeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity Layout
		{
			get	{ return GetSingleLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AttributeTextLayoutObjects", "Layout", _layout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Layout. When set to true, Layout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Layout is accessed. You can always execute a forced fetch by calling GetSingleLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLayout
		{
			get	{ return _alwaysFetchLayout; }
			set	{ _alwaysFetchLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Layout already has been fetched. Setting this property to false when Layout has been fetched
		/// will set Layout to null as well. Setting this property to true while Layout hasn't been fetched disables lazy loading for Layout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLayout
		{
			get { return _alreadyFetchedLayout;}
			set 
			{
				if(_alreadyFetchedLayout && !value)
				{
					this.Layout = null;
				}
				_alreadyFetchedLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Layout is not found
		/// in the database. When set to true, Layout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool LayoutReturnsNewIfNotFound
		{
			get	{ return _layoutReturnsNewIfNotFound; }
			set { _layoutReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AttributeTextLayoutObjectEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
