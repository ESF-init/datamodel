﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DebtorCard'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DebtorCardEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DebtorCardHistoryCollection	_debtorCardHistory;
		private bool	_alwaysFetchDebtorCardHistory, _alreadyFetchedDebtorCardHistory;
		private TypeOfCardEntity _typeOfCard;
		private bool	_alwaysFetchTypeOfCard, _alreadyFetchedTypeOfCard, _typeOfCardReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name TypeOfCard</summary>
			public static readonly string TypeOfCard = "TypeOfCard";
			/// <summary>Member name DebtorCardHistory</summary>
			public static readonly string DebtorCardHistory = "DebtorCardHistory";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DebtorCardEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DebtorCardEntity() :base("DebtorCardEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="debtorCardID">PK value for DebtorCard which data should be fetched into this DebtorCard object</param>
		public DebtorCardEntity(System.Int64 debtorCardID):base("DebtorCardEntity")
		{
			InitClassFetch(debtorCardID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="debtorCardID">PK value for DebtorCard which data should be fetched into this DebtorCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DebtorCardEntity(System.Int64 debtorCardID, IPrefetchPath prefetchPathToUse):base("DebtorCardEntity")
		{
			InitClassFetch(debtorCardID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="debtorCardID">PK value for DebtorCard which data should be fetched into this DebtorCard object</param>
		/// <param name="validator">The custom validator object for this DebtorCardEntity</param>
		public DebtorCardEntity(System.Int64 debtorCardID, IValidator validator):base("DebtorCardEntity")
		{
			InitClassFetch(debtorCardID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DebtorCardEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_debtorCardHistory = (VarioSL.Entities.CollectionClasses.DebtorCardHistoryCollection)info.GetValue("_debtorCardHistory", typeof(VarioSL.Entities.CollectionClasses.DebtorCardHistoryCollection));
			_alwaysFetchDebtorCardHistory = info.GetBoolean("_alwaysFetchDebtorCardHistory");
			_alreadyFetchedDebtorCardHistory = info.GetBoolean("_alreadyFetchedDebtorCardHistory");
			_typeOfCard = (TypeOfCardEntity)info.GetValue("_typeOfCard", typeof(TypeOfCardEntity));
			if(_typeOfCard!=null)
			{
				_typeOfCard.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_typeOfCardReturnsNewIfNotFound = info.GetBoolean("_typeOfCardReturnsNewIfNotFound");
			_alwaysFetchTypeOfCard = info.GetBoolean("_alwaysFetchTypeOfCard");
			_alreadyFetchedTypeOfCard = info.GetBoolean("_alreadyFetchedTypeOfCard");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DebtorCardFieldIndex)fieldIndex)
			{
				case DebtorCardFieldIndex.TypeOfCardID:
					DesetupSyncTypeOfCard(true, false);
					_alreadyFetchedTypeOfCard = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDebtorCardHistory = (_debtorCardHistory.Count > 0);
			_alreadyFetchedTypeOfCard = (_typeOfCard != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "TypeOfCard":
					toReturn.Add(Relations.TypeOfCardEntityUsingTypeOfCardID);
					break;
				case "DebtorCardHistory":
					toReturn.Add(Relations.DebtorCardHistoryEntityUsingDebtorCardID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_debtorCardHistory", (!this.MarkedForDeletion?_debtorCardHistory:null));
			info.AddValue("_alwaysFetchDebtorCardHistory", _alwaysFetchDebtorCardHistory);
			info.AddValue("_alreadyFetchedDebtorCardHistory", _alreadyFetchedDebtorCardHistory);
			info.AddValue("_typeOfCard", (!this.MarkedForDeletion?_typeOfCard:null));
			info.AddValue("_typeOfCardReturnsNewIfNotFound", _typeOfCardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTypeOfCard", _alwaysFetchTypeOfCard);
			info.AddValue("_alreadyFetchedTypeOfCard", _alreadyFetchedTypeOfCard);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "TypeOfCard":
					_alreadyFetchedTypeOfCard = true;
					this.TypeOfCard = (TypeOfCardEntity)entity;
					break;
				case "DebtorCardHistory":
					_alreadyFetchedDebtorCardHistory = true;
					if(entity!=null)
					{
						this.DebtorCardHistory.Add((DebtorCardHistoryEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "TypeOfCard":
					SetupSyncTypeOfCard(relatedEntity);
					break;
				case "DebtorCardHistory":
					_debtorCardHistory.Add((DebtorCardHistoryEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "TypeOfCard":
					DesetupSyncTypeOfCard(false, true);
					break;
				case "DebtorCardHistory":
					this.PerformRelatedEntityRemoval(_debtorCardHistory, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_typeOfCard!=null)
			{
				toReturn.Add(_typeOfCard);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_debtorCardHistory);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="debtorCardID">PK value for DebtorCard which data should be fetched into this DebtorCard object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 debtorCardID)
		{
			return FetchUsingPK(debtorCardID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="debtorCardID">PK value for DebtorCard which data should be fetched into this DebtorCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 debtorCardID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(debtorCardID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="debtorCardID">PK value for DebtorCard which data should be fetched into this DebtorCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 debtorCardID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(debtorCardID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="debtorCardID">PK value for DebtorCard which data should be fetched into this DebtorCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 debtorCardID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(debtorCardID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DebtorCardID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DebtorCardRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DebtorCardHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DebtorCardHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DebtorCardHistoryCollection GetMultiDebtorCardHistory(bool forceFetch)
		{
			return GetMultiDebtorCardHistory(forceFetch, _debtorCardHistory.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DebtorCardHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DebtorCardHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DebtorCardHistoryCollection GetMultiDebtorCardHistory(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDebtorCardHistory(forceFetch, _debtorCardHistory.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DebtorCardHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DebtorCardHistoryCollection GetMultiDebtorCardHistory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDebtorCardHistory(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DebtorCardHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DebtorCardHistoryCollection GetMultiDebtorCardHistory(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDebtorCardHistory || forceFetch || _alwaysFetchDebtorCardHistory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_debtorCardHistory);
				_debtorCardHistory.SuppressClearInGetMulti=!forceFetch;
				_debtorCardHistory.EntityFactoryToUse = entityFactoryToUse;
				_debtorCardHistory.GetMultiManyToOne(this, filter);
				_debtorCardHistory.SuppressClearInGetMulti=false;
				_alreadyFetchedDebtorCardHistory = true;
			}
			return _debtorCardHistory;
		}

		/// <summary> Sets the collection parameters for the collection for 'DebtorCardHistory'. These settings will be taken into account
		/// when the property DebtorCardHistory is requested or GetMultiDebtorCardHistory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDebtorCardHistory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_debtorCardHistory.SortClauses=sortClauses;
			_debtorCardHistory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'TypeOfCardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TypeOfCardEntity' which is related to this entity.</returns>
		public TypeOfCardEntity GetSingleTypeOfCard()
		{
			return GetSingleTypeOfCard(false);
		}

		/// <summary> Retrieves the related entity of type 'TypeOfCardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TypeOfCardEntity' which is related to this entity.</returns>
		public virtual TypeOfCardEntity GetSingleTypeOfCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedTypeOfCard || forceFetch || _alwaysFetchTypeOfCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TypeOfCardEntityUsingTypeOfCardID);
				TypeOfCardEntity newEntity = new TypeOfCardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TypeOfCardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TypeOfCardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_typeOfCardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TypeOfCard = newEntity;
				_alreadyFetchedTypeOfCard = fetchResult;
			}
			return _typeOfCard;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("TypeOfCard", _typeOfCard);
			toReturn.Add("DebtorCardHistory", _debtorCardHistory);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="debtorCardID">PK value for DebtorCard which data should be fetched into this DebtorCard object</param>
		/// <param name="validator">The validator object for this DebtorCardEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 debtorCardID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(debtorCardID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_debtorCardHistory = new VarioSL.Entities.CollectionClasses.DebtorCardHistoryCollection();
			_debtorCardHistory.SetContainingEntityInfo(this, "DebtorCard");
			_typeOfCardReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Blocked", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockingReason", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DateOfNoticeLost", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorCardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTraining", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastReadoutTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PinHash", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintedNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReplaceCardNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SerialNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeOfCardID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _typeOfCard</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTypeOfCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _typeOfCard, new PropertyChangedEventHandler( OnTypeOfCardPropertyChanged ), "TypeOfCard", VarioSL.Entities.RelationClasses.StaticDebtorCardRelations.TypeOfCardEntityUsingTypeOfCardIDStatic, true, signalRelatedEntity, "DebtorCards", resetFKFields, new int[] { (int)DebtorCardFieldIndex.TypeOfCardID } );		
			_typeOfCard = null;
		}
		
		/// <summary> setups the sync logic for member _typeOfCard</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTypeOfCard(IEntityCore relatedEntity)
		{
			if(_typeOfCard!=relatedEntity)
			{		
				DesetupSyncTypeOfCard(true, true);
				_typeOfCard = (TypeOfCardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _typeOfCard, new PropertyChangedEventHandler( OnTypeOfCardPropertyChanged ), "TypeOfCard", VarioSL.Entities.RelationClasses.StaticDebtorCardRelations.TypeOfCardEntityUsingTypeOfCardIDStatic, true, ref _alreadyFetchedTypeOfCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTypeOfCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="debtorCardID">PK value for DebtorCard which data should be fetched into this DebtorCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 debtorCardID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DebtorCardFieldIndex.DebtorCardID].ForcedCurrentValueWrite(debtorCardID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDebtorCardDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DebtorCardEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DebtorCardRelations Relations
		{
			get	{ return new DebtorCardRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DebtorCardHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDebtorCardHistory
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCardHistoryCollection(), (IEntityRelation)GetRelationsForField("DebtorCardHistory")[0], (int)VarioSL.Entities.EntityType.DebtorCardEntity, (int)VarioSL.Entities.EntityType.DebtorCardHistoryEntity, 0, null, null, null, "DebtorCardHistory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TypeOfCard'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTypeOfCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TypeOfCardCollection(), (IEntityRelation)GetRelationsForField("TypeOfCard")[0], (int)VarioSL.Entities.EntityType.DebtorCardEntity, (int)VarioSL.Entities.EntityType.TypeOfCardEntity, 0, null, null, null, "TypeOfCard", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Blocked property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."DATEOFBLOCKING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Blocked
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DebtorCardFieldIndex.Blocked, false); }
			set	{ SetValue((int)DebtorCardFieldIndex.Blocked, value, true); }
		}

		/// <summary> The BlockingReason property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."CAUSEOFBLOCKING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.BlockingReason> BlockingReason
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.BlockingReason>)GetValue((int)DebtorCardFieldIndex.BlockingReason, false); }
			set	{ SetValue((int)DebtorCardFieldIndex.BlockingReason, value, true); }
		}

		/// <summary> The ClientID property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ClientID
		{
			get { return (Nullable<System.Int16>)GetValue((int)DebtorCardFieldIndex.ClientID, false); }
			set	{ SetValue((int)DebtorCardFieldIndex.ClientID, value, true); }
		}

		/// <summary> The DateOfNoticeLost property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."DATEOFNOTICELOST"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DateOfNoticeLost
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DebtorCardFieldIndex.DateOfNoticeLost, false); }
			set	{ SetValue((int)DebtorCardFieldIndex.DateOfNoticeLost, value, true); }
		}

		/// <summary> The DebtorCardID property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 DebtorCardID
		{
			get { return (System.Int64)GetValue((int)DebtorCardFieldIndex.DebtorCardID, true); }
			set	{ SetValue((int)DebtorCardFieldIndex.DebtorCardID, value, true); }
		}

		/// <summary> The DebtorNumber property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."DEPTORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DebtorNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)DebtorCardFieldIndex.DebtorNumber, false); }
			set	{ SetValue((int)DebtorCardFieldIndex.DebtorNumber, value, true); }
		}

		/// <summary> The IsTraining property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."TRAININGMODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsTraining
		{
			get { return (Nullable<System.Boolean>)GetValue((int)DebtorCardFieldIndex.IsTraining, false); }
			set	{ SetValue((int)DebtorCardFieldIndex.IsTraining, value, true); }
		}

		/// <summary> The LastReadoutTime property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."LASTREADOUTTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastReadoutTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DebtorCardFieldIndex.LastReadoutTime, false); }
			set	{ SetValue((int)DebtorCardFieldIndex.LastReadoutTime, value, true); }
		}

		/// <summary> The PinHash property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."PINHASH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PinHash
		{
			get { return (System.String)GetValue((int)DebtorCardFieldIndex.PinHash, true); }
			set	{ SetValue((int)DebtorCardFieldIndex.PinHash, value, true); }
		}

		/// <summary> The PrintedNumber property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."PRINTEDCARDNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrintedNumber
		{
			get { return (System.String)GetValue((int)DebtorCardFieldIndex.PrintedNumber, true); }
			set	{ SetValue((int)DebtorCardFieldIndex.PrintedNumber, value, true); }
		}

		/// <summary> The ReplaceCardNumber property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."REPLACECARDNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReplaceCardNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)DebtorCardFieldIndex.ReplaceCardNumber, false); }
			set	{ SetValue((int)DebtorCardFieldIndex.ReplaceCardNumber, value, true); }
		}

		/// <summary> The SerialNumber property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."CARDNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SerialNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)DebtorCardFieldIndex.SerialNumber, false); }
			set	{ SetValue((int)DebtorCardFieldIndex.SerialNumber, value, true); }
		}

		/// <summary> The ServiceLevel property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."SERVICELEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ServiceLevel
		{
			get { return (Nullable<System.Int64>)GetValue((int)DebtorCardFieldIndex.ServiceLevel, false); }
			set	{ SetValue((int)DebtorCardFieldIndex.ServiceLevel, value, true); }
		}

		/// <summary> The State property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."CARDSTATUSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.CardState> State
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.CardState>)GetValue((int)DebtorCardFieldIndex.State, false); }
			set	{ SetValue((int)DebtorCardFieldIndex.State, value, true); }
		}

		/// <summary> The TypeOfCardID property of the Entity DebtorCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_CARD"."TYPEOFCARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TypeOfCardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DebtorCardFieldIndex.TypeOfCardID, false); }
			set	{ SetValue((int)DebtorCardFieldIndex.TypeOfCardID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DebtorCardHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDebtorCardHistory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DebtorCardHistoryCollection DebtorCardHistory
		{
			get	{ return GetMultiDebtorCardHistory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DebtorCardHistory. When set to true, DebtorCardHistory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DebtorCardHistory is accessed. You can always execute/ a forced fetch by calling GetMultiDebtorCardHistory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDebtorCardHistory
		{
			get	{ return _alwaysFetchDebtorCardHistory; }
			set	{ _alwaysFetchDebtorCardHistory = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DebtorCardHistory already has been fetched. Setting this property to false when DebtorCardHistory has been fetched
		/// will clear the DebtorCardHistory collection well. Setting this property to true while DebtorCardHistory hasn't been fetched disables lazy loading for DebtorCardHistory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDebtorCardHistory
		{
			get { return _alreadyFetchedDebtorCardHistory;}
			set 
			{
				if(_alreadyFetchedDebtorCardHistory && !value && (_debtorCardHistory != null))
				{
					_debtorCardHistory.Clear();
				}
				_alreadyFetchedDebtorCardHistory = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'TypeOfCardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTypeOfCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TypeOfCardEntity TypeOfCard
		{
			get	{ return GetSingleTypeOfCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTypeOfCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DebtorCards", "TypeOfCard", _typeOfCard, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TypeOfCard. When set to true, TypeOfCard is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TypeOfCard is accessed. You can always execute a forced fetch by calling GetSingleTypeOfCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTypeOfCard
		{
			get	{ return _alwaysFetchTypeOfCard; }
			set	{ _alwaysFetchTypeOfCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TypeOfCard already has been fetched. Setting this property to false when TypeOfCard has been fetched
		/// will set TypeOfCard to null as well. Setting this property to true while TypeOfCard hasn't been fetched disables lazy loading for TypeOfCard</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTypeOfCard
		{
			get { return _alreadyFetchedTypeOfCard;}
			set 
			{
				if(_alreadyFetchedTypeOfCard && !value)
				{
					this.TypeOfCard = null;
				}
				_alreadyFetchedTypeOfCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TypeOfCard is not found
		/// in the database. When set to true, TypeOfCard will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TypeOfCardReturnsNewIfNotFound
		{
			get	{ return _typeOfCardReturnsNewIfNotFound; }
			set { _typeOfCardReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DebtorCardEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
