﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Attribute'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AttributeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection	_attributeBitmapLayoutObjects;
		private bool	_alwaysFetchAttributeBitmapLayoutObjects, _alreadyFetchedAttributeBitmapLayoutObjects;
		private VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection	_attributeTextLayoutObjects;
		private bool	_alwaysFetchAttributeTextLayoutObjects, _alreadyFetchedAttributeTextLayoutObjects;
		private VarioSL.Entities.CollectionClasses.AttributeValueCollection	_attributeValues;
		private bool	_alwaysFetchAttributeValues, _alreadyFetchedAttributeValues;
		private VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection	_keyAttributeTransforms;
		private bool	_alwaysFetchKeyAttributeTransforms, _alreadyFetchedKeyAttributeTransforms;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name AttributeBitmapLayoutObjects</summary>
			public static readonly string AttributeBitmapLayoutObjects = "AttributeBitmapLayoutObjects";
			/// <summary>Member name AttributeTextLayoutObjects</summary>
			public static readonly string AttributeTextLayoutObjects = "AttributeTextLayoutObjects";
			/// <summary>Member name AttributeValues</summary>
			public static readonly string AttributeValues = "AttributeValues";
			/// <summary>Member name KeyAttributeTransforms</summary>
			public static readonly string KeyAttributeTransforms = "KeyAttributeTransforms";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AttributeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AttributeEntity() :base("AttributeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="attributeID">PK value for Attribute which data should be fetched into this Attribute object</param>
		public AttributeEntity(System.Int64 attributeID):base("AttributeEntity")
		{
			InitClassFetch(attributeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="attributeID">PK value for Attribute which data should be fetched into this Attribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AttributeEntity(System.Int64 attributeID, IPrefetchPath prefetchPathToUse):base("AttributeEntity")
		{
			InitClassFetch(attributeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="attributeID">PK value for Attribute which data should be fetched into this Attribute object</param>
		/// <param name="validator">The custom validator object for this AttributeEntity</param>
		public AttributeEntity(System.Int64 attributeID, IValidator validator):base("AttributeEntity")
		{
			InitClassFetch(attributeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AttributeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_attributeBitmapLayoutObjects = (VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection)info.GetValue("_attributeBitmapLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection));
			_alwaysFetchAttributeBitmapLayoutObjects = info.GetBoolean("_alwaysFetchAttributeBitmapLayoutObjects");
			_alreadyFetchedAttributeBitmapLayoutObjects = info.GetBoolean("_alreadyFetchedAttributeBitmapLayoutObjects");

			_attributeTextLayoutObjects = (VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection)info.GetValue("_attributeTextLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection));
			_alwaysFetchAttributeTextLayoutObjects = info.GetBoolean("_alwaysFetchAttributeTextLayoutObjects");
			_alreadyFetchedAttributeTextLayoutObjects = info.GetBoolean("_alreadyFetchedAttributeTextLayoutObjects");

			_attributeValues = (VarioSL.Entities.CollectionClasses.AttributeValueCollection)info.GetValue("_attributeValues", typeof(VarioSL.Entities.CollectionClasses.AttributeValueCollection));
			_alwaysFetchAttributeValues = info.GetBoolean("_alwaysFetchAttributeValues");
			_alreadyFetchedAttributeValues = info.GetBoolean("_alreadyFetchedAttributeValues");

			_keyAttributeTransforms = (VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection)info.GetValue("_keyAttributeTransforms", typeof(VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection));
			_alwaysFetchKeyAttributeTransforms = info.GetBoolean("_alwaysFetchKeyAttributeTransforms");
			_alreadyFetchedKeyAttributeTransforms = info.GetBoolean("_alreadyFetchedKeyAttributeTransforms");
			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AttributeFieldIndex)fieldIndex)
			{
				case AttributeFieldIndex.TarifID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAttributeBitmapLayoutObjects = (_attributeBitmapLayoutObjects.Count > 0);
			_alreadyFetchedAttributeTextLayoutObjects = (_attributeTextLayoutObjects.Count > 0);
			_alreadyFetchedAttributeValues = (_attributeValues.Count > 0);
			_alreadyFetchedKeyAttributeTransforms = (_keyAttributeTransforms.Count > 0);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTarifID);
					break;
				case "AttributeBitmapLayoutObjects":
					toReturn.Add(Relations.AttributeBitmapLayoutObjectEntityUsingAttributeID);
					break;
				case "AttributeTextLayoutObjects":
					toReturn.Add(Relations.AttributeTextLayoutObjectEntityUsingAttributeID);
					break;
				case "AttributeValues":
					toReturn.Add(Relations.AttributeValueEntityUsingAttributeID);
					break;
				case "KeyAttributeTransforms":
					toReturn.Add(Relations.KeyAttributeTransfromEntityUsingAttributeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_attributeBitmapLayoutObjects", (!this.MarkedForDeletion?_attributeBitmapLayoutObjects:null));
			info.AddValue("_alwaysFetchAttributeBitmapLayoutObjects", _alwaysFetchAttributeBitmapLayoutObjects);
			info.AddValue("_alreadyFetchedAttributeBitmapLayoutObjects", _alreadyFetchedAttributeBitmapLayoutObjects);
			info.AddValue("_attributeTextLayoutObjects", (!this.MarkedForDeletion?_attributeTextLayoutObjects:null));
			info.AddValue("_alwaysFetchAttributeTextLayoutObjects", _alwaysFetchAttributeTextLayoutObjects);
			info.AddValue("_alreadyFetchedAttributeTextLayoutObjects", _alreadyFetchedAttributeTextLayoutObjects);
			info.AddValue("_attributeValues", (!this.MarkedForDeletion?_attributeValues:null));
			info.AddValue("_alwaysFetchAttributeValues", _alwaysFetchAttributeValues);
			info.AddValue("_alreadyFetchedAttributeValues", _alreadyFetchedAttributeValues);
			info.AddValue("_keyAttributeTransforms", (!this.MarkedForDeletion?_keyAttributeTransforms:null));
			info.AddValue("_alwaysFetchKeyAttributeTransforms", _alwaysFetchKeyAttributeTransforms);
			info.AddValue("_alreadyFetchedKeyAttributeTransforms", _alreadyFetchedKeyAttributeTransforms);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "AttributeBitmapLayoutObjects":
					_alreadyFetchedAttributeBitmapLayoutObjects = true;
					if(entity!=null)
					{
						this.AttributeBitmapLayoutObjects.Add((AttributeBitmapLayoutObjectEntity)entity);
					}
					break;
				case "AttributeTextLayoutObjects":
					_alreadyFetchedAttributeTextLayoutObjects = true;
					if(entity!=null)
					{
						this.AttributeTextLayoutObjects.Add((AttributeTextLayoutObjectEntity)entity);
					}
					break;
				case "AttributeValues":
					_alreadyFetchedAttributeValues = true;
					if(entity!=null)
					{
						this.AttributeValues.Add((AttributeValueEntity)entity);
					}
					break;
				case "KeyAttributeTransforms":
					_alreadyFetchedKeyAttributeTransforms = true;
					if(entity!=null)
					{
						this.KeyAttributeTransforms.Add((KeyAttributeTransfromEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "AttributeBitmapLayoutObjects":
					_attributeBitmapLayoutObjects.Add((AttributeBitmapLayoutObjectEntity)relatedEntity);
					break;
				case "AttributeTextLayoutObjects":
					_attributeTextLayoutObjects.Add((AttributeTextLayoutObjectEntity)relatedEntity);
					break;
				case "AttributeValues":
					_attributeValues.Add((AttributeValueEntity)relatedEntity);
					break;
				case "KeyAttributeTransforms":
					_keyAttributeTransforms.Add((KeyAttributeTransfromEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "AttributeBitmapLayoutObjects":
					this.PerformRelatedEntityRemoval(_attributeBitmapLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AttributeTextLayoutObjects":
					this.PerformRelatedEntityRemoval(_attributeTextLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AttributeValues":
					this.PerformRelatedEntityRemoval(_attributeValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "KeyAttributeTransforms":
					this.PerformRelatedEntityRemoval(_keyAttributeTransforms, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_attributeBitmapLayoutObjects);
			toReturn.Add(_attributeTextLayoutObjects);
			toReturn.Add(_attributeValues);
			toReturn.Add(_keyAttributeTransforms);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="attributeID">PK value for Attribute which data should be fetched into this Attribute object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 attributeID)
		{
			return FetchUsingPK(attributeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="attributeID">PK value for Attribute which data should be fetched into this Attribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 attributeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(attributeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="attributeID">PK value for Attribute which data should be fetched into this Attribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 attributeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(attributeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="attributeID">PK value for Attribute which data should be fetched into this Attribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 attributeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(attributeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AttributeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AttributeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AttributeBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeBitmapLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection GetMultiAttributeBitmapLayoutObjects(bool forceFetch)
		{
			return GetMultiAttributeBitmapLayoutObjects(forceFetch, _attributeBitmapLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttributeBitmapLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection GetMultiAttributeBitmapLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttributeBitmapLayoutObjects(forceFetch, _attributeBitmapLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttributeBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection GetMultiAttributeBitmapLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttributeBitmapLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection GetMultiAttributeBitmapLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttributeBitmapLayoutObjects || forceFetch || _alwaysFetchAttributeBitmapLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributeBitmapLayoutObjects);
				_attributeBitmapLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_attributeBitmapLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_attributeBitmapLayoutObjects.GetMultiManyToOne(this, null, filter);
				_attributeBitmapLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributeBitmapLayoutObjects = true;
			}
			return _attributeBitmapLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttributeBitmapLayoutObjects'. These settings will be taken into account
		/// when the property AttributeBitmapLayoutObjects is requested or GetMultiAttributeBitmapLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributeBitmapLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributeBitmapLayoutObjects.SortClauses=sortClauses;
			_attributeBitmapLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AttributeTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeTextLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection GetMultiAttributeTextLayoutObjects(bool forceFetch)
		{
			return GetMultiAttributeTextLayoutObjects(forceFetch, _attributeTextLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttributeTextLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection GetMultiAttributeTextLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttributeTextLayoutObjects(forceFetch, _attributeTextLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttributeTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection GetMultiAttributeTextLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttributeTextLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection GetMultiAttributeTextLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttributeTextLayoutObjects || forceFetch || _alwaysFetchAttributeTextLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributeTextLayoutObjects);
				_attributeTextLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_attributeTextLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_attributeTextLayoutObjects.GetMultiManyToOne(this, null, filter);
				_attributeTextLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributeTextLayoutObjects = true;
			}
			return _attributeTextLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttributeTextLayoutObjects'. These settings will be taken into account
		/// when the property AttributeTextLayoutObjects is requested or GetMultiAttributeTextLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributeTextLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributeTextLayoutObjects.SortClauses=sortClauses;
			_attributeTextLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributeValues(bool forceFetch)
		{
			return GetMultiAttributeValues(forceFetch, _attributeValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributeValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttributeValues(forceFetch, _attributeValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributeValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttributeValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributeValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttributeValues || forceFetch || _alwaysFetchAttributeValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributeValues);
				_attributeValues.SuppressClearInGetMulti=!forceFetch;
				_attributeValues.EntityFactoryToUse = entityFactoryToUse;
				_attributeValues.GetMultiManyToOne(this, null, null, filter);
				_attributeValues.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributeValues = true;
			}
			return _attributeValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttributeValues'. These settings will be taken into account
		/// when the property AttributeValues is requested or GetMultiAttributeValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributeValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributeValues.SortClauses=sortClauses;
			_attributeValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'KeyAttributeTransfromEntity'</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKeyAttributeTransforms(bool forceFetch)
		{
			return GetMultiKeyAttributeTransforms(forceFetch, _keyAttributeTransforms.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'KeyAttributeTransfromEntity'</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKeyAttributeTransforms(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiKeyAttributeTransforms(forceFetch, _keyAttributeTransforms.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKeyAttributeTransforms(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiKeyAttributeTransforms(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKeyAttributeTransforms(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedKeyAttributeTransforms || forceFetch || _alwaysFetchKeyAttributeTransforms) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_keyAttributeTransforms);
				_keyAttributeTransforms.SuppressClearInGetMulti=!forceFetch;
				_keyAttributeTransforms.EntityFactoryToUse = entityFactoryToUse;
				_keyAttributeTransforms.GetMultiManyToOne(this, null, null, null, null, filter);
				_keyAttributeTransforms.SuppressClearInGetMulti=false;
				_alreadyFetchedKeyAttributeTransforms = true;
			}
			return _keyAttributeTransforms;
		}

		/// <summary> Sets the collection parameters for the collection for 'KeyAttributeTransforms'. These settings will be taken into account
		/// when the property KeyAttributeTransforms is requested or GetMultiKeyAttributeTransforms is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersKeyAttributeTransforms(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_keyAttributeTransforms.SortClauses=sortClauses;
			_keyAttributeTransforms.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTarifID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TarifID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("AttributeBitmapLayoutObjects", _attributeBitmapLayoutObjects);
			toReturn.Add("AttributeTextLayoutObjects", _attributeTextLayoutObjects);
			toReturn.Add("AttributeValues", _attributeValues);
			toReturn.Add("KeyAttributeTransforms", _keyAttributeTransforms);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="attributeID">PK value for Attribute which data should be fetched into this Attribute object</param>
		/// <param name="validator">The validator object for this AttributeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 attributeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(attributeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_attributeBitmapLayoutObjects = new VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection();
			_attributeBitmapLayoutObjects.SetContainingEntityInfo(this, "Attribute");

			_attributeTextLayoutObjects = new VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection();
			_attributeTextLayoutObjects.SetContainingEntityInfo(this, "Attribute");

			_attributeValues = new VarioSL.Entities.CollectionClasses.AttributeValueCollection();
			_attributeValues.SetContainingEntityInfo(this, "Attribute");

			_keyAttributeTransforms = new VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection();
			_keyAttributeTransforms.SetContainingEntityInfo(this, "Attribute");
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttribclassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayColumn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayLength", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayRow", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DotnetTypeOfName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HasMultipleValues", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDefault", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReadOnly", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Showinguixml", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowInMatrix", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowInTickets", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TarifID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticAttributeRelations.TariffEntityUsingTarifIDStatic, true, signalRelatedEntity, "Attributes", resetFKFields, new int[] { (int)AttributeFieldIndex.TarifID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticAttributeRelations.TariffEntityUsingTarifIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="attributeID">PK value for Attribute which data should be fetched into this Attribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 attributeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AttributeFieldIndex.AttributeID].ForcedCurrentValueWrite(attributeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAttributeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AttributeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AttributeRelations Relations
		{
			get	{ return new AttributeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeBitmapLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeBitmapLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("AttributeBitmapLayoutObjects")[0], (int)VarioSL.Entities.EntityType.AttributeEntity, (int)VarioSL.Entities.EntityType.AttributeBitmapLayoutObjectEntity, 0, null, null, null, "AttributeBitmapLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeTextLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeTextLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("AttributeTextLayoutObjects")[0], (int)VarioSL.Entities.EntityType.AttributeEntity, (int)VarioSL.Entities.EntityType.AttributeTextLayoutObjectEntity, 0, null, null, null, "AttributeTextLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("AttributeValues")[0], (int)VarioSL.Entities.EntityType.AttributeEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "AttributeValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'KeyAttributeTransfrom' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathKeyAttributeTransforms
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection(), (IEntityRelation)GetRelationsForField("KeyAttributeTransforms")[0], (int)VarioSL.Entities.EntityType.AttributeEntity, (int)VarioSL.Entities.EntityType.KeyAttributeTransfromEntity, 0, null, null, null, "KeyAttributeTransforms", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.AttributeEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AttribclassID property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."ATTRIBCLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.TariffAttributeClass> AttribclassID
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.TariffAttributeClass>)GetValue((int)AttributeFieldIndex.AttribclassID, false); }
			set	{ SetValue((int)AttributeFieldIndex.AttribclassID, value, true); }
		}

		/// <summary> The AttributeID property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."ATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 AttributeID
		{
			get { return (System.Int64)GetValue((int)AttributeFieldIndex.AttributeID, true); }
			set	{ SetValue((int)AttributeFieldIndex.AttributeID, value, true); }
		}

		/// <summary> The Description property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)AttributeFieldIndex.Description, true); }
			set	{ SetValue((int)AttributeFieldIndex.Description, value, true); }
		}

		/// <summary> The DisplayColumn property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."DISPLAYCOLUMN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> DisplayColumn
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeFieldIndex.DisplayColumn, false); }
			set	{ SetValue((int)AttributeFieldIndex.DisplayColumn, value, true); }
		}

		/// <summary> The DisplayLength property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."DISPLAYLENGTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> DisplayLength
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeFieldIndex.DisplayLength, false); }
			set	{ SetValue((int)AttributeFieldIndex.DisplayLength, value, true); }
		}

		/// <summary> The DisplayRow property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."DISPLAYROW"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> DisplayRow
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeFieldIndex.DisplayRow, false); }
			set	{ SetValue((int)AttributeFieldIndex.DisplayRow, value, true); }
		}

		/// <summary> The DotnetTypeOfName property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."DOTNETTYPEOFNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DotnetTypeOfName
		{
			get { return (System.String)GetValue((int)AttributeFieldIndex.DotnetTypeOfName, true); }
			set	{ SetValue((int)AttributeFieldIndex.DotnetTypeOfName, value, true); }
		}

		/// <summary> The HasMultipleValues property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."HASMULTIPLEVALUES"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> HasMultipleValues
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeFieldIndex.HasMultipleValues, false); }
			set	{ SetValue((int)AttributeFieldIndex.HasMultipleValues, value, true); }
		}

		/// <summary> The IsDefault property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."ISDEFAULT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsDefault
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeFieldIndex.IsDefault, false); }
			set	{ SetValue((int)AttributeFieldIndex.IsDefault, value, true); }
		}

		/// <summary> The Name property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)AttributeFieldIndex.Name, true); }
			set	{ SetValue((int)AttributeFieldIndex.Name, value, true); }
		}

		/// <summary> The Number property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."NUMBER_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Number
		{
			get { return (Nullable<System.Int64>)GetValue((int)AttributeFieldIndex.Number, false); }
			set	{ SetValue((int)AttributeFieldIndex.Number, value, true); }
		}

		/// <summary> The ReadOnly property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."READONLY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 ReadOnly
		{
			get { return (System.Int16)GetValue((int)AttributeFieldIndex.ReadOnly, true); }
			set	{ SetValue((int)AttributeFieldIndex.ReadOnly, value, true); }
		}

		/// <summary> The Showinguixml property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."SHOWINGUIXML"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Showinguixml
		{
			get { return (System.Int16)GetValue((int)AttributeFieldIndex.Showinguixml, true); }
			set	{ SetValue((int)AttributeFieldIndex.Showinguixml, value, true); }
		}

		/// <summary> The ShowInMatrix property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."SHOWINMATRIX"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ShowInMatrix
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeFieldIndex.ShowInMatrix, false); }
			set	{ SetValue((int)AttributeFieldIndex.ShowInMatrix, value, true); }
		}

		/// <summary> The ShowInTickets property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."SHOWINTICKETS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ShowInTickets
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeFieldIndex.ShowInTickets, false); }
			set	{ SetValue((int)AttributeFieldIndex.ShowInTickets, value, true); }
		}

		/// <summary> The TarifID property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TarifID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AttributeFieldIndex.TarifID, false); }
			set	{ SetValue((int)AttributeFieldIndex.TarifID, value, true); }
		}

		/// <summary> The Visible property of the Entity Attribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTE"."VISIBLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Visible
		{
			get { return (Nullable<System.Int16>)GetValue((int)AttributeFieldIndex.Visible, false); }
			set	{ SetValue((int)AttributeFieldIndex.Visible, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AttributeBitmapLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributeBitmapLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection AttributeBitmapLayoutObjects
		{
			get	{ return GetMultiAttributeBitmapLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeBitmapLayoutObjects. When set to true, AttributeBitmapLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeBitmapLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiAttributeBitmapLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeBitmapLayoutObjects
		{
			get	{ return _alwaysFetchAttributeBitmapLayoutObjects; }
			set	{ _alwaysFetchAttributeBitmapLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeBitmapLayoutObjects already has been fetched. Setting this property to false when AttributeBitmapLayoutObjects has been fetched
		/// will clear the AttributeBitmapLayoutObjects collection well. Setting this property to true while AttributeBitmapLayoutObjects hasn't been fetched disables lazy loading for AttributeBitmapLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeBitmapLayoutObjects
		{
			get { return _alreadyFetchedAttributeBitmapLayoutObjects;}
			set 
			{
				if(_alreadyFetchedAttributeBitmapLayoutObjects && !value && (_attributeBitmapLayoutObjects != null))
				{
					_attributeBitmapLayoutObjects.Clear();
				}
				_alreadyFetchedAttributeBitmapLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AttributeTextLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributeTextLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection AttributeTextLayoutObjects
		{
			get	{ return GetMultiAttributeTextLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeTextLayoutObjects. When set to true, AttributeTextLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeTextLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiAttributeTextLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeTextLayoutObjects
		{
			get	{ return _alwaysFetchAttributeTextLayoutObjects; }
			set	{ _alwaysFetchAttributeTextLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeTextLayoutObjects already has been fetched. Setting this property to false when AttributeTextLayoutObjects has been fetched
		/// will clear the AttributeTextLayoutObjects collection well. Setting this property to true while AttributeTextLayoutObjects hasn't been fetched disables lazy loading for AttributeTextLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeTextLayoutObjects
		{
			get { return _alreadyFetchedAttributeTextLayoutObjects;}
			set 
			{
				if(_alreadyFetchedAttributeTextLayoutObjects && !value && (_attributeTextLayoutObjects != null))
				{
					_attributeTextLayoutObjects.Clear();
				}
				_alreadyFetchedAttributeTextLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributeValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeValueCollection AttributeValues
		{
			get	{ return GetMultiAttributeValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValues. When set to true, AttributeValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValues is accessed. You can always execute/ a forced fetch by calling GetMultiAttributeValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValues
		{
			get	{ return _alwaysFetchAttributeValues; }
			set	{ _alwaysFetchAttributeValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValues already has been fetched. Setting this property to false when AttributeValues has been fetched
		/// will clear the AttributeValues collection well. Setting this property to true while AttributeValues hasn't been fetched disables lazy loading for AttributeValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValues
		{
			get { return _alreadyFetchedAttributeValues;}
			set 
			{
				if(_alreadyFetchedAttributeValues && !value && (_attributeValues != null))
				{
					_attributeValues.Clear();
				}
				_alreadyFetchedAttributeValues = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiKeyAttributeTransforms()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection KeyAttributeTransforms
		{
			get	{ return GetMultiKeyAttributeTransforms(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for KeyAttributeTransforms. When set to true, KeyAttributeTransforms is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time KeyAttributeTransforms is accessed. You can always execute/ a forced fetch by calling GetMultiKeyAttributeTransforms(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchKeyAttributeTransforms
		{
			get	{ return _alwaysFetchKeyAttributeTransforms; }
			set	{ _alwaysFetchKeyAttributeTransforms = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property KeyAttributeTransforms already has been fetched. Setting this property to false when KeyAttributeTransforms has been fetched
		/// will clear the KeyAttributeTransforms collection well. Setting this property to true while KeyAttributeTransforms hasn't been fetched disables lazy loading for KeyAttributeTransforms</summary>
		[Browsable(false)]
		public bool AlreadyFetchedKeyAttributeTransforms
		{
			get { return _alreadyFetchedKeyAttributeTransforms;}
			set 
			{
				if(_alreadyFetchedKeyAttributeTransforms && !value && (_keyAttributeTransforms != null))
				{
					_keyAttributeTransforms.Clear();
				}
				_alreadyFetchedKeyAttributeTransforms = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Attributes", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AttributeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
