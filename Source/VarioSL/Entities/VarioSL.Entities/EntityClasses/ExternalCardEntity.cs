﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ExternalCard'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ExternalCardEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private ExternalEffortEntity _externalEffort;
		private bool	_alwaysFetchExternalEffort, _alreadyFetchedExternalEffort, _externalEffortReturnsNewIfNotFound;
		private ExternalPacketEntity _externalPacket;
		private bool	_alwaysFetchExternalPacket, _alreadyFetchedExternalPacket, _externalPacketReturnsNewIfNotFound;
		private ExternalTypeEntity _externalType;
		private bool	_alwaysFetchExternalType, _alreadyFetchedExternalType, _externalTypeReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalEffort</summary>
			public static readonly string ExternalEffort = "ExternalEffort";
			/// <summary>Member name ExternalPacket</summary>
			public static readonly string ExternalPacket = "ExternalPacket";
			/// <summary>Member name ExternalType</summary>
			public static readonly string ExternalType = "ExternalType";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ExternalCardEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ExternalCardEntity() :base("ExternalCardEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cardID">PK value for ExternalCard which data should be fetched into this ExternalCard object</param>
		public ExternalCardEntity(System.Int64 cardID):base("ExternalCardEntity")
		{
			InitClassFetch(cardID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cardID">PK value for ExternalCard which data should be fetched into this ExternalCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ExternalCardEntity(System.Int64 cardID, IPrefetchPath prefetchPathToUse):base("ExternalCardEntity")
		{
			InitClassFetch(cardID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cardID">PK value for ExternalCard which data should be fetched into this ExternalCard object</param>
		/// <param name="validator">The custom validator object for this ExternalCardEntity</param>
		public ExternalCardEntity(System.Int64 cardID, IValidator validator):base("ExternalCardEntity")
		{
			InitClassFetch(cardID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalCardEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_externalEffort = (ExternalEffortEntity)info.GetValue("_externalEffort", typeof(ExternalEffortEntity));
			if(_externalEffort!=null)
			{
				_externalEffort.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalEffortReturnsNewIfNotFound = info.GetBoolean("_externalEffortReturnsNewIfNotFound");
			_alwaysFetchExternalEffort = info.GetBoolean("_alwaysFetchExternalEffort");
			_alreadyFetchedExternalEffort = info.GetBoolean("_alreadyFetchedExternalEffort");

			_externalPacket = (ExternalPacketEntity)info.GetValue("_externalPacket", typeof(ExternalPacketEntity));
			if(_externalPacket!=null)
			{
				_externalPacket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalPacketReturnsNewIfNotFound = info.GetBoolean("_externalPacketReturnsNewIfNotFound");
			_alwaysFetchExternalPacket = info.GetBoolean("_alwaysFetchExternalPacket");
			_alreadyFetchedExternalPacket = info.GetBoolean("_alreadyFetchedExternalPacket");

			_externalType = (ExternalTypeEntity)info.GetValue("_externalType", typeof(ExternalTypeEntity));
			if(_externalType!=null)
			{
				_externalType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalTypeReturnsNewIfNotFound = info.GetBoolean("_externalTypeReturnsNewIfNotFound");
			_alwaysFetchExternalType = info.GetBoolean("_alwaysFetchExternalType");
			_alreadyFetchedExternalType = info.GetBoolean("_alreadyFetchedExternalType");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ExternalCardFieldIndex)fieldIndex)
			{
				case ExternalCardFieldIndex.EffortID:
					DesetupSyncExternalEffort(true, false);
					_alreadyFetchedExternalEffort = false;
					break;
				case ExternalCardFieldIndex.PacketID:
					DesetupSyncExternalPacket(true, false);
					_alreadyFetchedExternalPacket = false;
					break;
				case ExternalCardFieldIndex.TariffId:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				case ExternalCardFieldIndex.Type:
					DesetupSyncExternalType(true, false);
					_alreadyFetchedExternalType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedExternalEffort = (_externalEffort != null);
			_alreadyFetchedExternalPacket = (_externalPacket != null);
			_alreadyFetchedExternalType = (_externalType != null);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ExternalEffort":
					toReturn.Add(Relations.ExternalEffortEntityUsingEffortID);
					break;
				case "ExternalPacket":
					toReturn.Add(Relations.ExternalPacketEntityUsingPacketID);
					break;
				case "ExternalType":
					toReturn.Add(Relations.ExternalTypeEntityUsingType);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_externalEffort", (!this.MarkedForDeletion?_externalEffort:null));
			info.AddValue("_externalEffortReturnsNewIfNotFound", _externalEffortReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalEffort", _alwaysFetchExternalEffort);
			info.AddValue("_alreadyFetchedExternalEffort", _alreadyFetchedExternalEffort);
			info.AddValue("_externalPacket", (!this.MarkedForDeletion?_externalPacket:null));
			info.AddValue("_externalPacketReturnsNewIfNotFound", _externalPacketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalPacket", _alwaysFetchExternalPacket);
			info.AddValue("_alreadyFetchedExternalPacket", _alreadyFetchedExternalPacket);
			info.AddValue("_externalType", (!this.MarkedForDeletion?_externalType:null));
			info.AddValue("_externalTypeReturnsNewIfNotFound", _externalTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalType", _alwaysFetchExternalType);
			info.AddValue("_alreadyFetchedExternalType", _alreadyFetchedExternalType);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ExternalEffort":
					_alreadyFetchedExternalEffort = true;
					this.ExternalEffort = (ExternalEffortEntity)entity;
					break;
				case "ExternalPacket":
					_alreadyFetchedExternalPacket = true;
					this.ExternalPacket = (ExternalPacketEntity)entity;
					break;
				case "ExternalType":
					_alreadyFetchedExternalType = true;
					this.ExternalType = (ExternalTypeEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ExternalEffort":
					SetupSyncExternalEffort(relatedEntity);
					break;
				case "ExternalPacket":
					SetupSyncExternalPacket(relatedEntity);
					break;
				case "ExternalType":
					SetupSyncExternalType(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ExternalEffort":
					DesetupSyncExternalEffort(false, true);
					break;
				case "ExternalPacket":
					DesetupSyncExternalPacket(false, true);
					break;
				case "ExternalType":
					DesetupSyncExternalType(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_externalEffort!=null)
			{
				toReturn.Add(_externalEffort);
			}
			if(_externalPacket!=null)
			{
				toReturn.Add(_externalPacket);
			}
			if(_externalType!=null)
			{
				toReturn.Add(_externalType);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardID">PK value for ExternalCard which data should be fetched into this ExternalCard object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardID)
		{
			return FetchUsingPK(cardID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardID">PK value for ExternalCard which data should be fetched into this ExternalCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cardID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardID">PK value for ExternalCard which data should be fetched into this ExternalCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cardID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardID">PK value for ExternalCard which data should be fetched into this ExternalCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cardID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CardID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ExternalCardRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ExternalEffortEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalEffortEntity' which is related to this entity.</returns>
		public ExternalEffortEntity GetSingleExternalEffort()
		{
			return GetSingleExternalEffort(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalEffortEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalEffortEntity' which is related to this entity.</returns>
		public virtual ExternalEffortEntity GetSingleExternalEffort(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalEffort || forceFetch || _alwaysFetchExternalEffort) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalEffortEntityUsingEffortID);
				ExternalEffortEntity newEntity = new ExternalEffortEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EffortID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ExternalEffortEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalEffortReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalEffort = newEntity;
				_alreadyFetchedExternalEffort = fetchResult;
			}
			return _externalEffort;
		}


		/// <summary> Retrieves the related entity of type 'ExternalPacketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalPacketEntity' which is related to this entity.</returns>
		public ExternalPacketEntity GetSingleExternalPacket()
		{
			return GetSingleExternalPacket(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalPacketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalPacketEntity' which is related to this entity.</returns>
		public virtual ExternalPacketEntity GetSingleExternalPacket(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalPacket || forceFetch || _alwaysFetchExternalPacket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalPacketEntityUsingPacketID);
				ExternalPacketEntity newEntity = new ExternalPacketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PacketID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ExternalPacketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalPacketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalPacket = newEntity;
				_alreadyFetchedExternalPacket = fetchResult;
			}
			return _externalPacket;
		}


		/// <summary> Retrieves the related entity of type 'ExternalTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalTypeEntity' which is related to this entity.</returns>
		public ExternalTypeEntity GetSingleExternalType()
		{
			return GetSingleExternalType(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalTypeEntity' which is related to this entity.</returns>
		public virtual ExternalTypeEntity GetSingleExternalType(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalType || forceFetch || _alwaysFetchExternalType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalTypeEntityUsingType);
				ExternalTypeEntity newEntity = new ExternalTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Type.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ExternalTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalType = newEntity;
				_alreadyFetchedExternalType = fetchResult;
			}
			return _externalType;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffId);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ExternalEffort", _externalEffort);
			toReturn.Add("ExternalPacket", _externalPacket);
			toReturn.Add("ExternalType", _externalType);
			toReturn.Add("Tariff", _tariff);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cardID">PK value for ExternalCard which data should be fetched into this ExternalCard object</param>
		/// <param name="validator">The validator object for this ExternalCardEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cardID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cardID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_externalEffortReturnsNewIfNotFound = false;
			_externalPacketReturnsNewIfNotFound = false;
			_externalTypeReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EffortID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PacketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _externalEffort</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalEffort(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalEffort, new PropertyChangedEventHandler( OnExternalEffortPropertyChanged ), "ExternalEffort", VarioSL.Entities.RelationClasses.StaticExternalCardRelations.ExternalEffortEntityUsingEffortIDStatic, true, signalRelatedEntity, "ExternalCards", resetFKFields, new int[] { (int)ExternalCardFieldIndex.EffortID } );		
			_externalEffort = null;
		}
		
		/// <summary> setups the sync logic for member _externalEffort</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalEffort(IEntityCore relatedEntity)
		{
			if(_externalEffort!=relatedEntity)
			{		
				DesetupSyncExternalEffort(true, true);
				_externalEffort = (ExternalEffortEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalEffort, new PropertyChangedEventHandler( OnExternalEffortPropertyChanged ), "ExternalEffort", VarioSL.Entities.RelationClasses.StaticExternalCardRelations.ExternalEffortEntityUsingEffortIDStatic, true, ref _alreadyFetchedExternalEffort, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalEffortPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _externalPacket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalPacket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalPacket, new PropertyChangedEventHandler( OnExternalPacketPropertyChanged ), "ExternalPacket", VarioSL.Entities.RelationClasses.StaticExternalCardRelations.ExternalPacketEntityUsingPacketIDStatic, true, signalRelatedEntity, "ExternalCards", resetFKFields, new int[] { (int)ExternalCardFieldIndex.PacketID } );		
			_externalPacket = null;
		}
		
		/// <summary> setups the sync logic for member _externalPacket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalPacket(IEntityCore relatedEntity)
		{
			if(_externalPacket!=relatedEntity)
			{		
				DesetupSyncExternalPacket(true, true);
				_externalPacket = (ExternalPacketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalPacket, new PropertyChangedEventHandler( OnExternalPacketPropertyChanged ), "ExternalPacket", VarioSL.Entities.RelationClasses.StaticExternalCardRelations.ExternalPacketEntityUsingPacketIDStatic, true, ref _alreadyFetchedExternalPacket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalPacketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _externalType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalType, new PropertyChangedEventHandler( OnExternalTypePropertyChanged ), "ExternalType", VarioSL.Entities.RelationClasses.StaticExternalCardRelations.ExternalTypeEntityUsingTypeStatic, true, signalRelatedEntity, "ExternalCards", resetFKFields, new int[] { (int)ExternalCardFieldIndex.Type } );		
			_externalType = null;
		}
		
		/// <summary> setups the sync logic for member _externalType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalType(IEntityCore relatedEntity)
		{
			if(_externalType!=relatedEntity)
			{		
				DesetupSyncExternalType(true, true);
				_externalType = (ExternalTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalType, new PropertyChangedEventHandler( OnExternalTypePropertyChanged ), "ExternalType", VarioSL.Entities.RelationClasses.StaticExternalCardRelations.ExternalTypeEntityUsingTypeStatic, true, ref _alreadyFetchedExternalType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticExternalCardRelations.TariffEntityUsingTariffIdStatic, true, signalRelatedEntity, "ExternalCards", resetFKFields, new int[] { (int)ExternalCardFieldIndex.TariffId } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticExternalCardRelations.TariffEntityUsingTariffIdStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cardID">PK value for ExternalCard which data should be fetched into this ExternalCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cardID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ExternalCardFieldIndex.CardID].ForcedCurrentValueWrite(cardID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateExternalCardDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ExternalCardEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ExternalCardRelations Relations
		{
			get	{ return new ExternalCardRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalEffort'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalEffort
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalEffortCollection(), (IEntityRelation)GetRelationsForField("ExternalEffort")[0], (int)VarioSL.Entities.EntityType.ExternalCardEntity, (int)VarioSL.Entities.EntityType.ExternalEffortEntity, 0, null, null, null, "ExternalEffort", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalPacket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalPacket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalPacketCollection(), (IEntityRelation)GetRelationsForField("ExternalPacket")[0], (int)VarioSL.Entities.EntityType.ExternalCardEntity, (int)VarioSL.Entities.EntityType.ExternalPacketEntity, 0, null, null, null, "ExternalPacket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalTypeCollection(), (IEntityRelation)GetRelationsForField("ExternalType")[0], (int)VarioSL.Entities.EntityType.ExternalCardEntity, (int)VarioSL.Entities.EntityType.ExternalTypeEntity, 0, null, null, null, "ExternalType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.ExternalCardEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CardID property of the Entity ExternalCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_CARD"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 CardID
		{
			get { return (System.Int64)GetValue((int)ExternalCardFieldIndex.CardID, true); }
			set	{ SetValue((int)ExternalCardFieldIndex.CardID, value, true); }
		}

		/// <summary> The CardNumber property of the Entity ExternalCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_CARD"."CARDNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalCardFieldIndex.CardNumber, false); }
			set	{ SetValue((int)ExternalCardFieldIndex.CardNumber, value, true); }
		}

		/// <summary> The Description property of the Entity ExternalCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_CARD"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ExternalCardFieldIndex.Description, true); }
			set	{ SetValue((int)ExternalCardFieldIndex.Description, value, true); }
		}

		/// <summary> The EffortID property of the Entity ExternalCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_CARD"."EFFORTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EffortID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalCardFieldIndex.EffortID, false); }
			set	{ SetValue((int)ExternalCardFieldIndex.EffortID, value, true); }
		}

		/// <summary> The PacketID property of the Entity ExternalCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_CARD"."PACKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PacketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalCardFieldIndex.PacketID, false); }
			set	{ SetValue((int)ExternalCardFieldIndex.PacketID, value, true); }
		}

		/// <summary> The TariffId property of the Entity ExternalCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_CARD"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalCardFieldIndex.TariffId, false); }
			set	{ SetValue((int)ExternalCardFieldIndex.TariffId, value, true); }
		}

		/// <summary> The Type property of the Entity ExternalCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_CARD"."TYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Type
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalCardFieldIndex.Type, false); }
			set	{ SetValue((int)ExternalCardFieldIndex.Type, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ExternalEffortEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalEffort()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ExternalEffortEntity ExternalEffort
		{
			get	{ return GetSingleExternalEffort(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalEffort(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalCards", "ExternalEffort", _externalEffort, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalEffort. When set to true, ExternalEffort is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalEffort is accessed. You can always execute a forced fetch by calling GetSingleExternalEffort(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalEffort
		{
			get	{ return _alwaysFetchExternalEffort; }
			set	{ _alwaysFetchExternalEffort = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalEffort already has been fetched. Setting this property to false when ExternalEffort has been fetched
		/// will set ExternalEffort to null as well. Setting this property to true while ExternalEffort hasn't been fetched disables lazy loading for ExternalEffort</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalEffort
		{
			get { return _alreadyFetchedExternalEffort;}
			set 
			{
				if(_alreadyFetchedExternalEffort && !value)
				{
					this.ExternalEffort = null;
				}
				_alreadyFetchedExternalEffort = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalEffort is not found
		/// in the database. When set to true, ExternalEffort will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ExternalEffortReturnsNewIfNotFound
		{
			get	{ return _externalEffortReturnsNewIfNotFound; }
			set { _externalEffortReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ExternalPacketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalPacket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ExternalPacketEntity ExternalPacket
		{
			get	{ return GetSingleExternalPacket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalPacket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalCards", "ExternalPacket", _externalPacket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalPacket. When set to true, ExternalPacket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalPacket is accessed. You can always execute a forced fetch by calling GetSingleExternalPacket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalPacket
		{
			get	{ return _alwaysFetchExternalPacket; }
			set	{ _alwaysFetchExternalPacket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalPacket already has been fetched. Setting this property to false when ExternalPacket has been fetched
		/// will set ExternalPacket to null as well. Setting this property to true while ExternalPacket hasn't been fetched disables lazy loading for ExternalPacket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalPacket
		{
			get { return _alreadyFetchedExternalPacket;}
			set 
			{
				if(_alreadyFetchedExternalPacket && !value)
				{
					this.ExternalPacket = null;
				}
				_alreadyFetchedExternalPacket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalPacket is not found
		/// in the database. When set to true, ExternalPacket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ExternalPacketReturnsNewIfNotFound
		{
			get	{ return _externalPacketReturnsNewIfNotFound; }
			set { _externalPacketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ExternalTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ExternalTypeEntity ExternalType
		{
			get	{ return GetSingleExternalType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalCards", "ExternalType", _externalType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalType. When set to true, ExternalType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalType is accessed. You can always execute a forced fetch by calling GetSingleExternalType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalType
		{
			get	{ return _alwaysFetchExternalType; }
			set	{ _alwaysFetchExternalType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalType already has been fetched. Setting this property to false when ExternalType has been fetched
		/// will set ExternalType to null as well. Setting this property to true while ExternalType hasn't been fetched disables lazy loading for ExternalType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalType
		{
			get { return _alreadyFetchedExternalType;}
			set 
			{
				if(_alreadyFetchedExternalType && !value)
				{
					this.ExternalType = null;
				}
				_alreadyFetchedExternalType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalType is not found
		/// in the database. When set to true, ExternalType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ExternalTypeReturnsNewIfNotFound
		{
			get	{ return _externalTypeReturnsNewIfNotFound; }
			set { _externalTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalCards", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ExternalCardEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
