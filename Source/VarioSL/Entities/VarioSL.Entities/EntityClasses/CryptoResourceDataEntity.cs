﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CryptoResourceData'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CryptoResourceDataEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection	_deviceReaderCryptoResources;
		private bool	_alwaysFetchDeviceReaderCryptoResources, _alreadyFetchedDeviceReaderCryptoResources;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeviceReaderCryptoResources</summary>
			public static readonly string DeviceReaderCryptoResources = "DeviceReaderCryptoResources";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CryptoResourceDataEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CryptoResourceDataEntity() :base("CryptoResourceDataEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cryptoResourceDataID">PK value for CryptoResourceData which data should be fetched into this CryptoResourceData object</param>
		public CryptoResourceDataEntity(System.Int64 cryptoResourceDataID):base("CryptoResourceDataEntity")
		{
			InitClassFetch(cryptoResourceDataID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cryptoResourceDataID">PK value for CryptoResourceData which data should be fetched into this CryptoResourceData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CryptoResourceDataEntity(System.Int64 cryptoResourceDataID, IPrefetchPath prefetchPathToUse):base("CryptoResourceDataEntity")
		{
			InitClassFetch(cryptoResourceDataID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cryptoResourceDataID">PK value for CryptoResourceData which data should be fetched into this CryptoResourceData object</param>
		/// <param name="validator">The custom validator object for this CryptoResourceDataEntity</param>
		public CryptoResourceDataEntity(System.Int64 cryptoResourceDataID, IValidator validator):base("CryptoResourceDataEntity")
		{
			InitClassFetch(cryptoResourceDataID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CryptoResourceDataEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_deviceReaderCryptoResources = (VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection)info.GetValue("_deviceReaderCryptoResources", typeof(VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection));
			_alwaysFetchDeviceReaderCryptoResources = info.GetBoolean("_alwaysFetchDeviceReaderCryptoResources");
			_alreadyFetchedDeviceReaderCryptoResources = info.GetBoolean("_alreadyFetchedDeviceReaderCryptoResources");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDeviceReaderCryptoResources = (_deviceReaderCryptoResources.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeviceReaderCryptoResources":
					toReturn.Add(Relations.DeviceReaderCryptoResourceEntityUsingCryptoResourceDataID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_deviceReaderCryptoResources", (!this.MarkedForDeletion?_deviceReaderCryptoResources:null));
			info.AddValue("_alwaysFetchDeviceReaderCryptoResources", _alwaysFetchDeviceReaderCryptoResources);
			info.AddValue("_alreadyFetchedDeviceReaderCryptoResources", _alreadyFetchedDeviceReaderCryptoResources);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeviceReaderCryptoResources":
					_alreadyFetchedDeviceReaderCryptoResources = true;
					if(entity!=null)
					{
						this.DeviceReaderCryptoResources.Add((DeviceReaderCryptoResourceEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeviceReaderCryptoResources":
					_deviceReaderCryptoResources.Add((DeviceReaderCryptoResourceEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeviceReaderCryptoResources":
					this.PerformRelatedEntityRemoval(_deviceReaderCryptoResources, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_deviceReaderCryptoResources);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="resourceType">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="version">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCResourceTypeVersion(System.Int32 resourceType, System.Int32 version)
		{
			return FetchUsingUCResourceTypeVersion( resourceType,  version, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="resourceType">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="version">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCResourceTypeVersion(System.Int32 resourceType, System.Int32 version, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCResourceTypeVersion( resourceType,  version, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="resourceType">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="version">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCResourceTypeVersion(System.Int32 resourceType, System.Int32 version, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCResourceTypeVersion( resourceType,  version, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="resourceType">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="version">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCResourceTypeVersion(System.Int32 resourceType, System.Int32 version, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((CryptoResourceDataDAO)CreateDAOInstance()).FetchCryptoResourceDataUsingUCResourceTypeVersion(this, this.Transaction, resourceType, version, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cryptoResourceDataID">PK value for CryptoResourceData which data should be fetched into this CryptoResourceData object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cryptoResourceDataID)
		{
			return FetchUsingPK(cryptoResourceDataID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cryptoResourceDataID">PK value for CryptoResourceData which data should be fetched into this CryptoResourceData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cryptoResourceDataID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cryptoResourceDataID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cryptoResourceDataID">PK value for CryptoResourceData which data should be fetched into this CryptoResourceData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cryptoResourceDataID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cryptoResourceDataID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cryptoResourceDataID">PK value for CryptoResourceData which data should be fetched into this CryptoResourceData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cryptoResourceDataID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cryptoResourceDataID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CryptoResourceDataID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CryptoResourceDataRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderCryptoResourceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderCryptoResourceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection GetMultiDeviceReaderCryptoResources(bool forceFetch)
		{
			return GetMultiDeviceReaderCryptoResources(forceFetch, _deviceReaderCryptoResources.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderCryptoResourceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderCryptoResourceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection GetMultiDeviceReaderCryptoResources(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceReaderCryptoResources(forceFetch, _deviceReaderCryptoResources.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderCryptoResourceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection GetMultiDeviceReaderCryptoResources(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceReaderCryptoResources(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderCryptoResourceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection GetMultiDeviceReaderCryptoResources(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceReaderCryptoResources || forceFetch || _alwaysFetchDeviceReaderCryptoResources) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceReaderCryptoResources);
				_deviceReaderCryptoResources.SuppressClearInGetMulti=!forceFetch;
				_deviceReaderCryptoResources.EntityFactoryToUse = entityFactoryToUse;
				_deviceReaderCryptoResources.GetMultiManyToOne(this, null, filter);
				_deviceReaderCryptoResources.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceReaderCryptoResources = true;
			}
			return _deviceReaderCryptoResources;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceReaderCryptoResources'. These settings will be taken into account
		/// when the property DeviceReaderCryptoResources is requested or GetMultiDeviceReaderCryptoResources is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceReaderCryptoResources(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceReaderCryptoResources.SortClauses=sortClauses;
			_deviceReaderCryptoResources.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeviceReaderCryptoResources", _deviceReaderCryptoResources);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cryptoResourceDataID">PK value for CryptoResourceData which data should be fetched into this CryptoResourceData object</param>
		/// <param name="validator">The validator object for this CryptoResourceDataEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cryptoResourceDataID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cryptoResourceDataID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_deviceReaderCryptoResources = new VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection();
			_deviceReaderCryptoResources.SetContainingEntityInfo(this, "CryptoResourceData");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CryptoResourceDataID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUpdate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MetaData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RawData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResourceStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResourceType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cryptoResourceDataID">PK value for CryptoResourceData which data should be fetched into this CryptoResourceData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cryptoResourceDataID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CryptoResourceDataFieldIndex.CryptoResourceDataID].ForcedCurrentValueWrite(cryptoResourceDataID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCryptoResourceDataDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CryptoResourceDataEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CryptoResourceDataRelations Relations
		{
			get	{ return new CryptoResourceDataRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReaderCryptoResource' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceReaderCryptoResources
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection(), (IEntityRelation)GetRelationsForField("DeviceReaderCryptoResources")[0], (int)VarioSL.Entities.EntityType.CryptoResourceDataEntity, (int)VarioSL.Entities.EntityType.DeviceReaderCryptoResourceEntity, 0, null, null, null, "DeviceReaderCryptoResources", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CryptoResourceDataID property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."CRYPTORESOURCEDATAID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CryptoResourceDataID
		{
			get { return (System.Int64)GetValue((int)CryptoResourceDataFieldIndex.CryptoResourceDataID, true); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.CryptoResourceDataID, value, true); }
		}

		/// <summary> The Description property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CryptoResourceDataFieldIndex.Description, true); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.Description, value, true); }
		}

		/// <summary> The LastModified property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CryptoResourceDataFieldIndex.LastModified, true); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUpdate property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."LASTUPDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastUpdate
		{
			get { return (System.DateTime)GetValue((int)CryptoResourceDataFieldIndex.LastUpdate, true); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.LastUpdate, value, true); }
		}

		/// <summary> The LastUser property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CryptoResourceDataFieldIndex.LastUser, true); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MetaData property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."METADATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Blob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.Byte[] MetaData
		{
			get { return (System.Byte[])GetValue((int)CryptoResourceDataFieldIndex.MetaData, true); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.MetaData, value, true); }
		}

		/// <summary> The RawData property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."RAWDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Blob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] RawData
		{
			get { return (System.Byte[])GetValue((int)CryptoResourceDataFieldIndex.RawData, true); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.RawData, value, true); }
		}

		/// <summary> The ResourceStatus property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."RESOURCESTATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ResourceStatus
		{
			get { return (System.Int32)GetValue((int)CryptoResourceDataFieldIndex.ResourceStatus, true); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.ResourceStatus, value, true); }
		}

		/// <summary> The ResourceType property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."RESOURCETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ResourceType
		{
			get { return (System.Int32)GetValue((int)CryptoResourceDataFieldIndex.ResourceType, true); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.ResourceType, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CryptoResourceDataFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)CryptoResourceDataFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidTo property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."VALIDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidTo
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CryptoResourceDataFieldIndex.ValidTo, false); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.ValidTo, value, true); }
		}

		/// <summary> The Version property of the Entity CryptoResourceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CRYPTORESOURCEDATA"."VERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)CryptoResourceDataFieldIndex.Version, true); }
			set	{ SetValue((int)CryptoResourceDataFieldIndex.Version, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderCryptoResourceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceReaderCryptoResources()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection DeviceReaderCryptoResources
		{
			get	{ return GetMultiDeviceReaderCryptoResources(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceReaderCryptoResources. When set to true, DeviceReaderCryptoResources is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceReaderCryptoResources is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceReaderCryptoResources(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceReaderCryptoResources
		{
			get	{ return _alwaysFetchDeviceReaderCryptoResources; }
			set	{ _alwaysFetchDeviceReaderCryptoResources = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceReaderCryptoResources already has been fetched. Setting this property to false when DeviceReaderCryptoResources has been fetched
		/// will clear the DeviceReaderCryptoResources collection well. Setting this property to true while DeviceReaderCryptoResources hasn't been fetched disables lazy loading for DeviceReaderCryptoResources</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceReaderCryptoResources
		{
			get { return _alreadyFetchedDeviceReaderCryptoResources;}
			set 
			{
				if(_alreadyFetchedDeviceReaderCryptoResources && !value && (_deviceReaderCryptoResources != null))
				{
					_deviceReaderCryptoResources.Clear();
				}
				_alreadyFetchedDeviceReaderCryptoResources = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CryptoResourceDataEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
