﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Photograph'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PhotographEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection	_organizationParticipants;
		private bool	_alwaysFetchOrganizationParticipants, _alreadyFetchedOrganizationParticipants;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private PersonEntity _person;
		private bool	_alwaysFetchPerson, _alreadyFetchedPerson, _personReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name Person</summary>
			public static readonly string Person = "Person";
			/// <summary>Member name OrganizationParticipants</summary>
			public static readonly string OrganizationParticipants = "OrganizationParticipants";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PhotographEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PhotographEntity() :base("PhotographEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="photographID">PK value for Photograph which data should be fetched into this Photograph object</param>
		public PhotographEntity(System.Int64 photographID):base("PhotographEntity")
		{
			InitClassFetch(photographID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="photographID">PK value for Photograph which data should be fetched into this Photograph object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PhotographEntity(System.Int64 photographID, IPrefetchPath prefetchPathToUse):base("PhotographEntity")
		{
			InitClassFetch(photographID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="photographID">PK value for Photograph which data should be fetched into this Photograph object</param>
		/// <param name="validator">The custom validator object for this PhotographEntity</param>
		public PhotographEntity(System.Int64 photographID, IValidator validator):base("PhotographEntity")
		{
			InitClassFetch(photographID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PhotographEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_organizationParticipants = (VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection)info.GetValue("_organizationParticipants", typeof(VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection));
			_alwaysFetchOrganizationParticipants = info.GetBoolean("_alwaysFetchOrganizationParticipants");
			_alreadyFetchedOrganizationParticipants = info.GetBoolean("_alreadyFetchedOrganizationParticipants");
			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_person = (PersonEntity)info.GetValue("_person", typeof(PersonEntity));
			if(_person!=null)
			{
				_person.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_personReturnsNewIfNotFound = info.GetBoolean("_personReturnsNewIfNotFound");
			_alwaysFetchPerson = info.GetBoolean("_alwaysFetchPerson");
			_alreadyFetchedPerson = info.GetBoolean("_alreadyFetchedPerson");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PhotographFieldIndex)fieldIndex)
			{
				case PhotographFieldIndex.Contractid:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case PhotographFieldIndex.PersonID:
					DesetupSyncPerson(true, false);
					_alreadyFetchedPerson = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedOrganizationParticipants = (_organizationParticipants.Count > 0);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedPerson = (_person != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractid);
					break;
				case "Person":
					toReturn.Add(Relations.PersonEntityUsingPersonID);
					break;
				case "OrganizationParticipants":
					toReturn.Add(Relations.OrganizationParticipantEntityUsingPhotographID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_organizationParticipants", (!this.MarkedForDeletion?_organizationParticipants:null));
			info.AddValue("_alwaysFetchOrganizationParticipants", _alwaysFetchOrganizationParticipants);
			info.AddValue("_alreadyFetchedOrganizationParticipants", _alreadyFetchedOrganizationParticipants);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_person", (!this.MarkedForDeletion?_person:null));
			info.AddValue("_personReturnsNewIfNotFound", _personReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerson", _alwaysFetchPerson);
			info.AddValue("_alreadyFetchedPerson", _alreadyFetchedPerson);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "Person":
					_alreadyFetchedPerson = true;
					this.Person = (PersonEntity)entity;
					break;
				case "OrganizationParticipants":
					_alreadyFetchedOrganizationParticipants = true;
					if(entity!=null)
					{
						this.OrganizationParticipants.Add((OrganizationParticipantEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "Person":
					SetupSyncPerson(relatedEntity);
					break;
				case "OrganizationParticipants":
					_organizationParticipants.Add((OrganizationParticipantEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "Person":
					DesetupSyncPerson(false, true);
					break;
				case "OrganizationParticipants":
					this.PerformRelatedEntityRemoval(_organizationParticipants, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_person!=null)
			{
				toReturn.Add(_person);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_organizationParticipants);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="photographID">PK value for Photograph which data should be fetched into this Photograph object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 photographID)
		{
			return FetchUsingPK(photographID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="photographID">PK value for Photograph which data should be fetched into this Photograph object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 photographID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(photographID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="photographID">PK value for Photograph which data should be fetched into this Photograph object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 photographID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(photographID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="photographID">PK value for Photograph which data should be fetched into this Photograph object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 photographID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(photographID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PhotographID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PhotographRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationParticipantEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection GetMultiOrganizationParticipants(bool forceFetch)
		{
			return GetMultiOrganizationParticipants(forceFetch, _organizationParticipants.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationParticipantEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection GetMultiOrganizationParticipants(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrganizationParticipants(forceFetch, _organizationParticipants.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection GetMultiOrganizationParticipants(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrganizationParticipants(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection GetMultiOrganizationParticipants(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrganizationParticipants || forceFetch || _alwaysFetchOrganizationParticipants) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_organizationParticipants);
				_organizationParticipants.SuppressClearInGetMulti=!forceFetch;
				_organizationParticipants.EntityFactoryToUse = entityFactoryToUse;
				_organizationParticipants.GetMultiManyToOne(null, null, null, this, filter);
				_organizationParticipants.SuppressClearInGetMulti=false;
				_alreadyFetchedOrganizationParticipants = true;
			}
			return _organizationParticipants;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrganizationParticipants'. These settings will be taken into account
		/// when the property OrganizationParticipants is requested or GetMultiOrganizationParticipants is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrganizationParticipants(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_organizationParticipants.SortClauses=sortClauses;
			_organizationParticipants.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractid);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Contractid.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSinglePerson()
		{
			return GetSinglePerson(false);
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSinglePerson(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerson || forceFetch || _alwaysFetchPerson) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingPersonID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphic(this.Transaction, this.PersonID.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_personReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Person = newEntity;
				_alreadyFetchedPerson = fetchResult;
			}
			return _person;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Contract", _contract);
			toReturn.Add("Person", _person);
			toReturn.Add("OrganizationParticipants", _organizationParticipants);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="photographID">PK value for Photograph which data should be fetched into this Photograph object</param>
		/// <param name="validator">The validator object for this PhotographEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 photographID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(photographID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_organizationParticipants = new VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection();
			_organizationParticipants.SetContainingEntityInfo(this, "Photograph");
			_contractReturnsNewIfNotFound = false;
			_personReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Caption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Contractid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PhotoFile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PhotographID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticPhotographRelations.ContractEntityUsingContractidStatic, true, signalRelatedEntity, "Photographs", resetFKFields, new int[] { (int)PhotographFieldIndex.Contractid } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticPhotographRelations.ContractEntityUsingContractidStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _person</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerson(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticPhotographRelations.PersonEntityUsingPersonIDStatic, true, signalRelatedEntity, "Photographs", resetFKFields, new int[] { (int)PhotographFieldIndex.PersonID } );		
			_person = null;
		}
		
		/// <summary> setups the sync logic for member _person</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerson(IEntityCore relatedEntity)
		{
			if(_person!=relatedEntity)
			{		
				DesetupSyncPerson(true, true);
				_person = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticPhotographRelations.PersonEntityUsingPersonIDStatic, true, ref _alreadyFetchedPerson, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPersonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="photographID">PK value for Photograph which data should be fetched into this Photograph object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 photographID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PhotographFieldIndex.PhotographID].ForcedCurrentValueWrite(photographID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePhotographDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PhotographEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PhotographRelations Relations
		{
			get	{ return new PhotographRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrganizationParticipant' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganizationParticipants
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection(), (IEntityRelation)GetRelationsForField("OrganizationParticipants")[0], (int)VarioSL.Entities.EntityType.PhotographEntity, (int)VarioSL.Entities.EntityType.OrganizationParticipantEntity, 0, null, null, null, "OrganizationParticipants", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.PhotographEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerson
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("Person")[0], (int)VarioSL.Entities.EntityType.PhotographEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "Person", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Caption property of the Entity Photograph<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PHOTOGRAPH"."CAPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Caption
		{
			get { return (System.String)GetValue((int)PhotographFieldIndex.Caption, true); }
			set	{ SetValue((int)PhotographFieldIndex.Caption, value, true); }
		}

		/// <summary> The Contractid property of the Entity Photograph<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PHOTOGRAPH"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Contractid
		{
			get { return (Nullable<System.Int64>)GetValue((int)PhotographFieldIndex.Contractid, false); }
			set	{ SetValue((int)PhotographFieldIndex.Contractid, value, true); }
		}

		/// <summary> The LastModified property of the Entity Photograph<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PHOTOGRAPH"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)PhotographFieldIndex.LastModified, true); }
			set	{ SetValue((int)PhotographFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Photograph<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PHOTOGRAPH"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)PhotographFieldIndex.LastUser, true); }
			set	{ SetValue((int)PhotographFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PersonID property of the Entity Photograph<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PHOTOGRAPH"."PERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PersonID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PhotographFieldIndex.PersonID, false); }
			set	{ SetValue((int)PhotographFieldIndex.PersonID, value, true); }
		}

		/// <summary> The PhotoFile property of the Entity Photograph<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PHOTOGRAPH"."PHOTOFILE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Blob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Drawing.Bitmap PhotoFile
		{
			get { return (System.Drawing.Bitmap)GetValue((int)PhotographFieldIndex.PhotoFile, true); }
			set	{ SetValue((int)PhotographFieldIndex.PhotoFile, value, true); }
		}

		/// <summary> The PhotographID property of the Entity Photograph<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PHOTOGRAPH"."PHOTOGRAPHID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 PhotographID
		{
			get { return (System.Int64)GetValue((int)PhotographFieldIndex.PhotographID, true); }
			set	{ SetValue((int)PhotographFieldIndex.PhotographID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Photograph<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PHOTOGRAPH"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)PhotographFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)PhotographFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrganizationParticipants()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection OrganizationParticipants
		{
			get	{ return GetMultiOrganizationParticipants(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrganizationParticipants. When set to true, OrganizationParticipants is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrganizationParticipants is accessed. You can always execute/ a forced fetch by calling GetMultiOrganizationParticipants(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganizationParticipants
		{
			get	{ return _alwaysFetchOrganizationParticipants; }
			set	{ _alwaysFetchOrganizationParticipants = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrganizationParticipants already has been fetched. Setting this property to false when OrganizationParticipants has been fetched
		/// will clear the OrganizationParticipants collection well. Setting this property to true while OrganizationParticipants hasn't been fetched disables lazy loading for OrganizationParticipants</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganizationParticipants
		{
			get { return _alreadyFetchedOrganizationParticipants;}
			set 
			{
				if(_alreadyFetchedOrganizationParticipants && !value && (_organizationParticipants != null))
				{
					_organizationParticipants.Clear();
				}
				_alreadyFetchedOrganizationParticipants = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Photographs", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity Person
		{
			get	{ return GetSinglePerson(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPerson(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Photographs", "Person", _person, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Person. When set to true, Person is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Person is accessed. You can always execute a forced fetch by calling GetSinglePerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerson
		{
			get	{ return _alwaysFetchPerson; }
			set	{ _alwaysFetchPerson = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Person already has been fetched. Setting this property to false when Person has been fetched
		/// will set Person to null as well. Setting this property to true while Person hasn't been fetched disables lazy loading for Person</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerson
		{
			get { return _alreadyFetchedPerson;}
			set 
			{
				if(_alreadyFetchedPerson && !value)
				{
					this.Person = null;
				}
				_alreadyFetchedPerson = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Person is not found
		/// in the database. When set to true, Person will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PersonReturnsNewIfNotFound
		{
			get	{ return _personReturnsNewIfNotFound; }
			set { _personReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PhotographEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
