﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TenantHistory'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TenantHistoryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private TenantEntity _tenant;
		private bool	_alwaysFetchTenant, _alreadyFetchedTenant, _tenantReturnsNewIfNotFound;
		private TenantPersonEntity _tenantPerson;
		private bool	_alwaysFetchTenantPerson, _alreadyFetchedTenantPerson, _tenantPersonReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Tenant</summary>
			public static readonly string Tenant = "Tenant";
			/// <summary>Member name TenantPerson</summary>
			public static readonly string TenantPerson = "TenantPerson";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TenantHistoryEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TenantHistoryEntity() :base("TenantHistoryEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="tenantHistoryID">PK value for TenantHistory which data should be fetched into this TenantHistory object</param>
		public TenantHistoryEntity(System.Int64 tenantHistoryID):base("TenantHistoryEntity")
		{
			InitClassFetch(tenantHistoryID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="tenantHistoryID">PK value for TenantHistory which data should be fetched into this TenantHistory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TenantHistoryEntity(System.Int64 tenantHistoryID, IPrefetchPath prefetchPathToUse):base("TenantHistoryEntity")
		{
			InitClassFetch(tenantHistoryID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="tenantHistoryID">PK value for TenantHistory which data should be fetched into this TenantHistory object</param>
		/// <param name="validator">The custom validator object for this TenantHistoryEntity</param>
		public TenantHistoryEntity(System.Int64 tenantHistoryID, IValidator validator):base("TenantHistoryEntity")
		{
			InitClassFetch(tenantHistoryID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TenantHistoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_tenant = (TenantEntity)info.GetValue("_tenant", typeof(TenantEntity));
			if(_tenant!=null)
			{
				_tenant.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tenantReturnsNewIfNotFound = info.GetBoolean("_tenantReturnsNewIfNotFound");
			_alwaysFetchTenant = info.GetBoolean("_alwaysFetchTenant");
			_alreadyFetchedTenant = info.GetBoolean("_alreadyFetchedTenant");

			_tenantPerson = (TenantPersonEntity)info.GetValue("_tenantPerson", typeof(TenantPersonEntity));
			if(_tenantPerson!=null)
			{
				_tenantPerson.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tenantPersonReturnsNewIfNotFound = info.GetBoolean("_tenantPersonReturnsNewIfNotFound");
			_alwaysFetchTenantPerson = info.GetBoolean("_alwaysFetchTenantPerson");
			_alreadyFetchedTenantPerson = info.GetBoolean("_alreadyFetchedTenantPerson");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TenantHistoryFieldIndex)fieldIndex)
			{
				case TenantHistoryFieldIndex.TenantID:
					DesetupSyncTenant(true, false);
					_alreadyFetchedTenant = false;
					break;
				case TenantHistoryFieldIndex.TenantPersonID:
					DesetupSyncTenantPerson(true, false);
					_alreadyFetchedTenantPerson = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTenant = (_tenant != null);
			_alreadyFetchedTenantPerson = (_tenantPerson != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Tenant":
					toReturn.Add(Relations.TenantEntityUsingTenantID);
					break;
				case "TenantPerson":
					toReturn.Add(Relations.TenantPersonEntityUsingTenantPersonID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_tenant", (!this.MarkedForDeletion?_tenant:null));
			info.AddValue("_tenantReturnsNewIfNotFound", _tenantReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTenant", _alwaysFetchTenant);
			info.AddValue("_alreadyFetchedTenant", _alreadyFetchedTenant);
			info.AddValue("_tenantPerson", (!this.MarkedForDeletion?_tenantPerson:null));
			info.AddValue("_tenantPersonReturnsNewIfNotFound", _tenantPersonReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTenantPerson", _alwaysFetchTenantPerson);
			info.AddValue("_alreadyFetchedTenantPerson", _alreadyFetchedTenantPerson);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Tenant":
					_alreadyFetchedTenant = true;
					this.Tenant = (TenantEntity)entity;
					break;
				case "TenantPerson":
					_alreadyFetchedTenantPerson = true;
					this.TenantPerson = (TenantPersonEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Tenant":
					SetupSyncTenant(relatedEntity);
					break;
				case "TenantPerson":
					SetupSyncTenantPerson(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Tenant":
					DesetupSyncTenant(false, true);
					break;
				case "TenantPerson":
					DesetupSyncTenantPerson(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_tenant!=null)
			{
				toReturn.Add(_tenant);
			}
			if(_tenantPerson!=null)
			{
				toReturn.Add(_tenantPerson);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tenantHistoryID">PK value for TenantHistory which data should be fetched into this TenantHistory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tenantHistoryID)
		{
			return FetchUsingPK(tenantHistoryID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tenantHistoryID">PK value for TenantHistory which data should be fetched into this TenantHistory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tenantHistoryID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(tenantHistoryID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tenantHistoryID">PK value for TenantHistory which data should be fetched into this TenantHistory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tenantHistoryID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(tenantHistoryID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tenantHistoryID">PK value for TenantHistory which data should be fetched into this TenantHistory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tenantHistoryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(tenantHistoryID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TenantHistoryID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TenantHistoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'TenantEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TenantEntity' which is related to this entity.</returns>
		public TenantEntity GetSingleTenant()
		{
			return GetSingleTenant(false);
		}

		/// <summary> Retrieves the related entity of type 'TenantEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TenantEntity' which is related to this entity.</returns>
		public virtual TenantEntity GetSingleTenant(bool forceFetch)
		{
			if( ( !_alreadyFetchedTenant || forceFetch || _alwaysFetchTenant) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TenantEntityUsingTenantID);
				TenantEntity newEntity = new TenantEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TenantID);
				}
				if(fetchResult)
				{
					newEntity = (TenantEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tenantReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tenant = newEntity;
				_alreadyFetchedTenant = fetchResult;
			}
			return _tenant;
		}


		/// <summary> Retrieves the related entity of type 'TenantPersonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TenantPersonEntity' which is related to this entity.</returns>
		public TenantPersonEntity GetSingleTenantPerson()
		{
			return GetSingleTenantPerson(false);
		}

		/// <summary> Retrieves the related entity of type 'TenantPersonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TenantPersonEntity' which is related to this entity.</returns>
		public virtual TenantPersonEntity GetSingleTenantPerson(bool forceFetch)
		{
			if( ( !_alreadyFetchedTenantPerson || forceFetch || _alwaysFetchTenantPerson) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TenantPersonEntityUsingTenantPersonID);
				TenantPersonEntity newEntity = new TenantPersonEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TenantPersonID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TenantPersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tenantPersonReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TenantPerson = newEntity;
				_alreadyFetchedTenantPerson = fetchResult;
			}
			return _tenantPerson;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Tenant", _tenant);
			toReturn.Add("TenantPerson", _tenantPerson);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="tenantHistoryID">PK value for TenantHistory which data should be fetched into this TenantHistory object</param>
		/// <param name="validator">The validator object for this TenantHistoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 tenantHistoryID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(tenantHistoryID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_tenantReturnsNewIfNotFound = false;
			_tenantPersonReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LogCreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Message", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenantHistoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenantID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenantPersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserName", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _tenant</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTenant(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tenant, new PropertyChangedEventHandler( OnTenantPropertyChanged ), "Tenant", VarioSL.Entities.RelationClasses.StaticTenantHistoryRelations.TenantEntityUsingTenantIDStatic, true, signalRelatedEntity, "TenantHistories", resetFKFields, new int[] { (int)TenantHistoryFieldIndex.TenantID } );		
			_tenant = null;
		}
		
		/// <summary> setups the sync logic for member _tenant</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTenant(IEntityCore relatedEntity)
		{
			if(_tenant!=relatedEntity)
			{		
				DesetupSyncTenant(true, true);
				_tenant = (TenantEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tenant, new PropertyChangedEventHandler( OnTenantPropertyChanged ), "Tenant", VarioSL.Entities.RelationClasses.StaticTenantHistoryRelations.TenantEntityUsingTenantIDStatic, true, ref _alreadyFetchedTenant, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTenantPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tenantPerson</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTenantPerson(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tenantPerson, new PropertyChangedEventHandler( OnTenantPersonPropertyChanged ), "TenantPerson", VarioSL.Entities.RelationClasses.StaticTenantHistoryRelations.TenantPersonEntityUsingTenantPersonIDStatic, true, signalRelatedEntity, "TenantHistories", resetFKFields, new int[] { (int)TenantHistoryFieldIndex.TenantPersonID } );		
			_tenantPerson = null;
		}
		
		/// <summary> setups the sync logic for member _tenantPerson</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTenantPerson(IEntityCore relatedEntity)
		{
			if(_tenantPerson!=relatedEntity)
			{		
				DesetupSyncTenantPerson(true, true);
				_tenantPerson = (TenantPersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tenantPerson, new PropertyChangedEventHandler( OnTenantPersonPropertyChanged ), "TenantPerson", VarioSL.Entities.RelationClasses.StaticTenantHistoryRelations.TenantPersonEntityUsingTenantPersonIDStatic, true, ref _alreadyFetchedTenantPerson, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTenantPersonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="tenantHistoryID">PK value for TenantHistory which data should be fetched into this TenantHistory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 tenantHistoryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TenantHistoryFieldIndex.TenantHistoryID].ForcedCurrentValueWrite(tenantHistoryID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTenantHistoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TenantHistoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TenantHistoryRelations Relations
		{
			get	{ return new TenantHistoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tenant'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTenant
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TenantCollection(), (IEntityRelation)GetRelationsForField("Tenant")[0], (int)VarioSL.Entities.EntityType.TenantHistoryEntity, (int)VarioSL.Entities.EntityType.TenantEntity, 0, null, null, null, "Tenant", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TenantPerson'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTenantPerson
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TenantPersonCollection(), (IEntityRelation)GetRelationsForField("TenantPerson")[0], (int)VarioSL.Entities.EntityType.TenantHistoryEntity, (int)VarioSL.Entities.EntityType.TenantPersonEntity, 0, null, null, null, "TenantPerson", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LogCreationDate property of the Entity TenantHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTHISTORY"."LOGCREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LogCreationDate
		{
			get { return (System.DateTime)GetValue((int)TenantHistoryFieldIndex.LogCreationDate, true); }
			set	{ SetValue((int)TenantHistoryFieldIndex.LogCreationDate, value, true); }
		}

		/// <summary> The Message property of the Entity TenantHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTHISTORY"."MESSAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)TenantHistoryFieldIndex.Message, true); }
			set	{ SetValue((int)TenantHistoryFieldIndex.Message, value, true); }
		}

		/// <summary> The TenantHistoryID property of the Entity TenantHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTHISTORY"."TENANTHISTORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 TenantHistoryID
		{
			get { return (System.Int64)GetValue((int)TenantHistoryFieldIndex.TenantHistoryID, true); }
			set	{ SetValue((int)TenantHistoryFieldIndex.TenantHistoryID, value, true); }
		}

		/// <summary> The TenantID property of the Entity TenantHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTHISTORY"."TENANTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TenantID
		{
			get { return (System.Int64)GetValue((int)TenantHistoryFieldIndex.TenantID, true); }
			set	{ SetValue((int)TenantHistoryFieldIndex.TenantID, value, true); }
		}

		/// <summary> The TenantPersonID property of the Entity TenantHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTHISTORY"."TENANTPERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TenantPersonID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TenantHistoryFieldIndex.TenantPersonID, false); }
			set	{ SetValue((int)TenantHistoryFieldIndex.TenantPersonID, value, true); }
		}

		/// <summary> The UserName property of the Entity TenantHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTHISTORY"."USERNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String UserName
		{
			get { return (System.String)GetValue((int)TenantHistoryFieldIndex.UserName, true); }
			set	{ SetValue((int)TenantHistoryFieldIndex.UserName, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'TenantEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTenant()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TenantEntity Tenant
		{
			get	{ return GetSingleTenant(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTenant(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TenantHistories", "Tenant", _tenant, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tenant. When set to true, Tenant is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tenant is accessed. You can always execute a forced fetch by calling GetSingleTenant(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTenant
		{
			get	{ return _alwaysFetchTenant; }
			set	{ _alwaysFetchTenant = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tenant already has been fetched. Setting this property to false when Tenant has been fetched
		/// will set Tenant to null as well. Setting this property to true while Tenant hasn't been fetched disables lazy loading for Tenant</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTenant
		{
			get { return _alreadyFetchedTenant;}
			set 
			{
				if(_alreadyFetchedTenant && !value)
				{
					this.Tenant = null;
				}
				_alreadyFetchedTenant = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tenant is not found
		/// in the database. When set to true, Tenant will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TenantReturnsNewIfNotFound
		{
			get	{ return _tenantReturnsNewIfNotFound; }
			set { _tenantReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TenantPersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTenantPerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TenantPersonEntity TenantPerson
		{
			get	{ return GetSingleTenantPerson(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTenantPerson(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TenantHistories", "TenantPerson", _tenantPerson, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TenantPerson. When set to true, TenantPerson is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TenantPerson is accessed. You can always execute a forced fetch by calling GetSingleTenantPerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTenantPerson
		{
			get	{ return _alwaysFetchTenantPerson; }
			set	{ _alwaysFetchTenantPerson = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TenantPerson already has been fetched. Setting this property to false when TenantPerson has been fetched
		/// will set TenantPerson to null as well. Setting this property to true while TenantPerson hasn't been fetched disables lazy loading for TenantPerson</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTenantPerson
		{
			get { return _alreadyFetchedTenantPerson;}
			set 
			{
				if(_alreadyFetchedTenantPerson && !value)
				{
					this.TenantPerson = null;
				}
				_alreadyFetchedTenantPerson = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TenantPerson is not found
		/// in the database. When set to true, TenantPerson will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TenantPersonReturnsNewIfNotFound
		{
			get	{ return _tenantPersonReturnsNewIfNotFound; }
			set { _tenantPersonReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TenantHistoryEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
