﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CryptoResource'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CryptoResourceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CryptoContentCollection	_cryptoContentCryptograms;
		private bool	_alwaysFetchCryptoContentCryptograms, _alreadyFetchedCryptoContentCryptograms;
		private VarioSL.Entities.CollectionClasses.CryptoContentCollection	_cryptoContentSignCertificates;
		private bool	_alwaysFetchCryptoContentSignCertificates, _alreadyFetchedCryptoContentSignCertificates;
		private VarioSL.Entities.CollectionClasses.CryptoContentCollection	_cryptoContentSubCertificates;
		private bool	_alwaysFetchCryptoContentSubCertificates, _alreadyFetchedCryptoContentSubCertificates;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CryptoContentCryptograms</summary>
			public static readonly string CryptoContentCryptograms = "CryptoContentCryptograms";
			/// <summary>Member name CryptoContentSignCertificates</summary>
			public static readonly string CryptoContentSignCertificates = "CryptoContentSignCertificates";
			/// <summary>Member name CryptoContentSubCertificates</summary>
			public static readonly string CryptoContentSubCertificates = "CryptoContentSubCertificates";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CryptoResourceEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CryptoResourceEntity() :base("CryptoResourceEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="resourceID">PK value for CryptoResource which data should be fetched into this CryptoResource object</param>
		public CryptoResourceEntity(System.Int64 resourceID):base("CryptoResourceEntity")
		{
			InitClassFetch(resourceID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="resourceID">PK value for CryptoResource which data should be fetched into this CryptoResource object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CryptoResourceEntity(System.Int64 resourceID, IPrefetchPath prefetchPathToUse):base("CryptoResourceEntity")
		{
			InitClassFetch(resourceID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="resourceID">PK value for CryptoResource which data should be fetched into this CryptoResource object</param>
		/// <param name="validator">The custom validator object for this CryptoResourceEntity</param>
		public CryptoResourceEntity(System.Int64 resourceID, IValidator validator):base("CryptoResourceEntity")
		{
			InitClassFetch(resourceID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CryptoResourceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cryptoContentCryptograms = (VarioSL.Entities.CollectionClasses.CryptoContentCollection)info.GetValue("_cryptoContentCryptograms", typeof(VarioSL.Entities.CollectionClasses.CryptoContentCollection));
			_alwaysFetchCryptoContentCryptograms = info.GetBoolean("_alwaysFetchCryptoContentCryptograms");
			_alreadyFetchedCryptoContentCryptograms = info.GetBoolean("_alreadyFetchedCryptoContentCryptograms");

			_cryptoContentSignCertificates = (VarioSL.Entities.CollectionClasses.CryptoContentCollection)info.GetValue("_cryptoContentSignCertificates", typeof(VarioSL.Entities.CollectionClasses.CryptoContentCollection));
			_alwaysFetchCryptoContentSignCertificates = info.GetBoolean("_alwaysFetchCryptoContentSignCertificates");
			_alreadyFetchedCryptoContentSignCertificates = info.GetBoolean("_alreadyFetchedCryptoContentSignCertificates");

			_cryptoContentSubCertificates = (VarioSL.Entities.CollectionClasses.CryptoContentCollection)info.GetValue("_cryptoContentSubCertificates", typeof(VarioSL.Entities.CollectionClasses.CryptoContentCollection));
			_alwaysFetchCryptoContentSubCertificates = info.GetBoolean("_alwaysFetchCryptoContentSubCertificates");
			_alreadyFetchedCryptoContentSubCertificates = info.GetBoolean("_alreadyFetchedCryptoContentSubCertificates");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCryptoContentCryptograms = (_cryptoContentCryptograms.Count > 0);
			_alreadyFetchedCryptoContentSignCertificates = (_cryptoContentSignCertificates.Count > 0);
			_alreadyFetchedCryptoContentSubCertificates = (_cryptoContentSubCertificates.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CryptoContentCryptograms":
					toReturn.Add(Relations.CryptoContentEntityUsingCryptogramResourceID);
					break;
				case "CryptoContentSignCertificates":
					toReturn.Add(Relations.CryptoContentEntityUsingSignCertificateResourceID);
					break;
				case "CryptoContentSubCertificates":
					toReturn.Add(Relations.CryptoContentEntityUsingSubCertificateResourceID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cryptoContentCryptograms", (!this.MarkedForDeletion?_cryptoContentCryptograms:null));
			info.AddValue("_alwaysFetchCryptoContentCryptograms", _alwaysFetchCryptoContentCryptograms);
			info.AddValue("_alreadyFetchedCryptoContentCryptograms", _alreadyFetchedCryptoContentCryptograms);
			info.AddValue("_cryptoContentSignCertificates", (!this.MarkedForDeletion?_cryptoContentSignCertificates:null));
			info.AddValue("_alwaysFetchCryptoContentSignCertificates", _alwaysFetchCryptoContentSignCertificates);
			info.AddValue("_alreadyFetchedCryptoContentSignCertificates", _alreadyFetchedCryptoContentSignCertificates);
			info.AddValue("_cryptoContentSubCertificates", (!this.MarkedForDeletion?_cryptoContentSubCertificates:null));
			info.AddValue("_alwaysFetchCryptoContentSubCertificates", _alwaysFetchCryptoContentSubCertificates);
			info.AddValue("_alreadyFetchedCryptoContentSubCertificates", _alreadyFetchedCryptoContentSubCertificates);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CryptoContentCryptograms":
					_alreadyFetchedCryptoContentCryptograms = true;
					if(entity!=null)
					{
						this.CryptoContentCryptograms.Add((CryptoContentEntity)entity);
					}
					break;
				case "CryptoContentSignCertificates":
					_alreadyFetchedCryptoContentSignCertificates = true;
					if(entity!=null)
					{
						this.CryptoContentSignCertificates.Add((CryptoContentEntity)entity);
					}
					break;
				case "CryptoContentSubCertificates":
					_alreadyFetchedCryptoContentSubCertificates = true;
					if(entity!=null)
					{
						this.CryptoContentSubCertificates.Add((CryptoContentEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CryptoContentCryptograms":
					_cryptoContentCryptograms.Add((CryptoContentEntity)relatedEntity);
					break;
				case "CryptoContentSignCertificates":
					_cryptoContentSignCertificates.Add((CryptoContentEntity)relatedEntity);
					break;
				case "CryptoContentSubCertificates":
					_cryptoContentSubCertificates.Add((CryptoContentEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CryptoContentCryptograms":
					this.PerformRelatedEntityRemoval(_cryptoContentCryptograms, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CryptoContentSignCertificates":
					this.PerformRelatedEntityRemoval(_cryptoContentSignCertificates, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CryptoContentSubCertificates":
					this.PerformRelatedEntityRemoval(_cryptoContentSubCertificates, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cryptoContentCryptograms);
			toReturn.Add(_cryptoContentSignCertificates);
			toReturn.Add(_cryptoContentSubCertificates);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="hashValue">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCHashValue(System.String hashValue)
		{
			return FetchUsingUCHashValue( hashValue, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="hashValue">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCHashValue(System.String hashValue, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCHashValue( hashValue, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="hashValue">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCHashValue(System.String hashValue, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCHashValue( hashValue, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="hashValue">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCHashValue(System.String hashValue, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((CryptoResourceDAO)CreateDAOInstance()).FetchCryptoResourceUsingUCHashValue(this, this.Transaction, hashValue, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="resourceID">PK value for CryptoResource which data should be fetched into this CryptoResource object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 resourceID)
		{
			return FetchUsingPK(resourceID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="resourceID">PK value for CryptoResource which data should be fetched into this CryptoResource object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 resourceID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(resourceID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="resourceID">PK value for CryptoResource which data should be fetched into this CryptoResource object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 resourceID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(resourceID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="resourceID">PK value for CryptoResource which data should be fetched into this CryptoResource object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 resourceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(resourceID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ResourceID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CryptoResourceRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CryptoContentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoContentCollection GetMultiCryptoContentCryptograms(bool forceFetch)
		{
			return GetMultiCryptoContentCryptograms(forceFetch, _cryptoContentCryptograms.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CryptoContentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoContentCollection GetMultiCryptoContentCryptograms(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCryptoContentCryptograms(forceFetch, _cryptoContentCryptograms.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CryptoContentCollection GetMultiCryptoContentCryptograms(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCryptoContentCryptograms(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CryptoContentCollection GetMultiCryptoContentCryptograms(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCryptoContentCryptograms || forceFetch || _alwaysFetchCryptoContentCryptograms) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cryptoContentCryptograms);
				_cryptoContentCryptograms.SuppressClearInGetMulti=!forceFetch;
				_cryptoContentCryptograms.EntityFactoryToUse = entityFactoryToUse;
				_cryptoContentCryptograms.GetMultiManyToOne(null, this, null, null, filter);
				_cryptoContentCryptograms.SuppressClearInGetMulti=false;
				_alreadyFetchedCryptoContentCryptograms = true;
			}
			return _cryptoContentCryptograms;
		}

		/// <summary> Sets the collection parameters for the collection for 'CryptoContentCryptograms'. These settings will be taken into account
		/// when the property CryptoContentCryptograms is requested or GetMultiCryptoContentCryptograms is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCryptoContentCryptograms(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cryptoContentCryptograms.SortClauses=sortClauses;
			_cryptoContentCryptograms.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CryptoContentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoContentCollection GetMultiCryptoContentSignCertificates(bool forceFetch)
		{
			return GetMultiCryptoContentSignCertificates(forceFetch, _cryptoContentSignCertificates.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CryptoContentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoContentCollection GetMultiCryptoContentSignCertificates(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCryptoContentSignCertificates(forceFetch, _cryptoContentSignCertificates.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CryptoContentCollection GetMultiCryptoContentSignCertificates(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCryptoContentSignCertificates(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CryptoContentCollection GetMultiCryptoContentSignCertificates(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCryptoContentSignCertificates || forceFetch || _alwaysFetchCryptoContentSignCertificates) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cryptoContentSignCertificates);
				_cryptoContentSignCertificates.SuppressClearInGetMulti=!forceFetch;
				_cryptoContentSignCertificates.EntityFactoryToUse = entityFactoryToUse;
				_cryptoContentSignCertificates.GetMultiManyToOne(null, null, this, null, filter);
				_cryptoContentSignCertificates.SuppressClearInGetMulti=false;
				_alreadyFetchedCryptoContentSignCertificates = true;
			}
			return _cryptoContentSignCertificates;
		}

		/// <summary> Sets the collection parameters for the collection for 'CryptoContentSignCertificates'. These settings will be taken into account
		/// when the property CryptoContentSignCertificates is requested or GetMultiCryptoContentSignCertificates is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCryptoContentSignCertificates(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cryptoContentSignCertificates.SortClauses=sortClauses;
			_cryptoContentSignCertificates.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CryptoContentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoContentCollection GetMultiCryptoContentSubCertificates(bool forceFetch)
		{
			return GetMultiCryptoContentSubCertificates(forceFetch, _cryptoContentSubCertificates.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CryptoContentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoContentCollection GetMultiCryptoContentSubCertificates(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCryptoContentSubCertificates(forceFetch, _cryptoContentSubCertificates.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CryptoContentCollection GetMultiCryptoContentSubCertificates(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCryptoContentSubCertificates(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CryptoContentCollection GetMultiCryptoContentSubCertificates(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCryptoContentSubCertificates || forceFetch || _alwaysFetchCryptoContentSubCertificates) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cryptoContentSubCertificates);
				_cryptoContentSubCertificates.SuppressClearInGetMulti=!forceFetch;
				_cryptoContentSubCertificates.EntityFactoryToUse = entityFactoryToUse;
				_cryptoContentSubCertificates.GetMultiManyToOne(null, null, null, this, filter);
				_cryptoContentSubCertificates.SuppressClearInGetMulti=false;
				_alreadyFetchedCryptoContentSubCertificates = true;
			}
			return _cryptoContentSubCertificates;
		}

		/// <summary> Sets the collection parameters for the collection for 'CryptoContentSubCertificates'. These settings will be taken into account
		/// when the property CryptoContentSubCertificates is requested or GetMultiCryptoContentSubCertificates is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCryptoContentSubCertificates(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cryptoContentSubCertificates.SortClauses=sortClauses;
			_cryptoContentSubCertificates.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CryptoContentCryptograms", _cryptoContentCryptograms);
			toReturn.Add("CryptoContentSignCertificates", _cryptoContentSignCertificates);
			toReturn.Add("CryptoContentSubCertificates", _cryptoContentSubCertificates);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="resourceID">PK value for CryptoResource which data should be fetched into this CryptoResource object</param>
		/// <param name="validator">The validator object for this CryptoResourceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 resourceID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(resourceID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cryptoContentCryptograms = new VarioSL.Entities.CollectionClasses.CryptoContentCollection();
			_cryptoContentCryptograms.SetContainingEntityInfo(this, "CryptoResourceCryptogram");

			_cryptoContentSignCertificates = new VarioSL.Entities.CollectionClasses.CryptoContentCollection();
			_cryptoContentSignCertificates.SetContainingEntityInfo(this, "CryptoResourceSignCertificate");

			_cryptoContentSubCertificates = new VarioSL.Entities.CollectionClasses.CryptoContentCollection();
			_cryptoContentSubCertificates.SetContainingEntityInfo(this, "CryptoResourceSubCertificate");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BinaryData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HashValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResourceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="resourceID">PK value for CryptoResource which data should be fetched into this CryptoResource object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 resourceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CryptoResourceFieldIndex.ResourceID].ForcedCurrentValueWrite(resourceID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCryptoResourceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CryptoResourceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CryptoResourceRelations Relations
		{
			get	{ return new CryptoResourceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoContent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoContentCryptograms
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoContentCollection(), (IEntityRelation)GetRelationsForField("CryptoContentCryptograms")[0], (int)VarioSL.Entities.EntityType.CryptoResourceEntity, (int)VarioSL.Entities.EntityType.CryptoContentEntity, 0, null, null, null, "CryptoContentCryptograms", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoContent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoContentSignCertificates
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoContentCollection(), (IEntityRelation)GetRelationsForField("CryptoContentSignCertificates")[0], (int)VarioSL.Entities.EntityType.CryptoResourceEntity, (int)VarioSL.Entities.EntityType.CryptoContentEntity, 0, null, null, null, "CryptoContentSignCertificates", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoContent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoContentSubCertificates
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoContentCollection(), (IEntityRelation)GetRelationsForField("CryptoContentSubCertificates")[0], (int)VarioSL.Entities.EntityType.CryptoResourceEntity, (int)VarioSL.Entities.EntityType.CryptoContentEntity, 0, null, null, null, "CryptoContentSubCertificates", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BinaryData property of the Entity CryptoResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_RESOURCE"."BINARYDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Blob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] BinaryData
		{
			get { return (System.Byte[])GetValue((int)CryptoResourceFieldIndex.BinaryData, true); }
			set	{ SetValue((int)CryptoResourceFieldIndex.BinaryData, value, true); }
		}

		/// <summary> The DataType property of the Entity CryptoResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_RESOURCE"."DATATYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CryptogramDataType DataType
		{
			get { return (VarioSL.Entities.Enumerations.CryptogramDataType)GetValue((int)CryptoResourceFieldIndex.DataType, true); }
			set	{ SetValue((int)CryptoResourceFieldIndex.DataType, value, true); }
		}

		/// <summary> The HashValue property of the Entity CryptoResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_RESOURCE"."HASHVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String HashValue
		{
			get { return (System.String)GetValue((int)CryptoResourceFieldIndex.HashValue, true); }
			set	{ SetValue((int)CryptoResourceFieldIndex.HashValue, value, true); }
		}

		/// <summary> The LastModified property of the Entity CryptoResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_RESOURCE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CryptoResourceFieldIndex.LastModified, true); }
			set	{ SetValue((int)CryptoResourceFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CryptoResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_RESOURCE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CryptoResourceFieldIndex.LastUser, true); }
			set	{ SetValue((int)CryptoResourceFieldIndex.LastUser, value, true); }
		}

		/// <summary> The ResourceID property of the Entity CryptoResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_RESOURCE"."RESOURCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ResourceID
		{
			get { return (System.Int64)GetValue((int)CryptoResourceFieldIndex.ResourceID, true); }
			set	{ SetValue((int)CryptoResourceFieldIndex.ResourceID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CryptoResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_RESOURCE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CryptoResourceFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CryptoResourceFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCryptoContentCryptograms()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CryptoContentCollection CryptoContentCryptograms
		{
			get	{ return GetMultiCryptoContentCryptograms(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoContentCryptograms. When set to true, CryptoContentCryptograms is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoContentCryptograms is accessed. You can always execute/ a forced fetch by calling GetMultiCryptoContentCryptograms(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoContentCryptograms
		{
			get	{ return _alwaysFetchCryptoContentCryptograms; }
			set	{ _alwaysFetchCryptoContentCryptograms = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoContentCryptograms already has been fetched. Setting this property to false when CryptoContentCryptograms has been fetched
		/// will clear the CryptoContentCryptograms collection well. Setting this property to true while CryptoContentCryptograms hasn't been fetched disables lazy loading for CryptoContentCryptograms</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoContentCryptograms
		{
			get { return _alreadyFetchedCryptoContentCryptograms;}
			set 
			{
				if(_alreadyFetchedCryptoContentCryptograms && !value && (_cryptoContentCryptograms != null))
				{
					_cryptoContentCryptograms.Clear();
				}
				_alreadyFetchedCryptoContentCryptograms = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCryptoContentSignCertificates()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CryptoContentCollection CryptoContentSignCertificates
		{
			get	{ return GetMultiCryptoContentSignCertificates(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoContentSignCertificates. When set to true, CryptoContentSignCertificates is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoContentSignCertificates is accessed. You can always execute/ a forced fetch by calling GetMultiCryptoContentSignCertificates(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoContentSignCertificates
		{
			get	{ return _alwaysFetchCryptoContentSignCertificates; }
			set	{ _alwaysFetchCryptoContentSignCertificates = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoContentSignCertificates already has been fetched. Setting this property to false when CryptoContentSignCertificates has been fetched
		/// will clear the CryptoContentSignCertificates collection well. Setting this property to true while CryptoContentSignCertificates hasn't been fetched disables lazy loading for CryptoContentSignCertificates</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoContentSignCertificates
		{
			get { return _alreadyFetchedCryptoContentSignCertificates;}
			set 
			{
				if(_alreadyFetchedCryptoContentSignCertificates && !value && (_cryptoContentSignCertificates != null))
				{
					_cryptoContentSignCertificates.Clear();
				}
				_alreadyFetchedCryptoContentSignCertificates = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CryptoContentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCryptoContentSubCertificates()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CryptoContentCollection CryptoContentSubCertificates
		{
			get	{ return GetMultiCryptoContentSubCertificates(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoContentSubCertificates. When set to true, CryptoContentSubCertificates is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoContentSubCertificates is accessed. You can always execute/ a forced fetch by calling GetMultiCryptoContentSubCertificates(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoContentSubCertificates
		{
			get	{ return _alwaysFetchCryptoContentSubCertificates; }
			set	{ _alwaysFetchCryptoContentSubCertificates = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoContentSubCertificates already has been fetched. Setting this property to false when CryptoContentSubCertificates has been fetched
		/// will clear the CryptoContentSubCertificates collection well. Setting this property to true while CryptoContentSubCertificates hasn't been fetched disables lazy loading for CryptoContentSubCertificates</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoContentSubCertificates
		{
			get { return _alreadyFetchedCryptoContentSubCertificates;}
			set 
			{
				if(_alreadyFetchedCryptoContentSubCertificates && !value && (_cryptoContentSubCertificates != null))
				{
					_cryptoContentSubCertificates.Clear();
				}
				_alreadyFetchedCryptoContentSubCertificates = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CryptoResourceEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
