﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SettlementResult'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SettlementResultEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SettlementResultEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SettlementResultEntity() :base("SettlementResultEntity")
		{
			InitClassEmpty(null);
		}
		


		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SettlementResultEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}




				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SettlementResultRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		



		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BadSum", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BadSumTaxed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardIssuerID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearingResultLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CorrectedSum", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CorrectedSumTaxed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DevicePaymentMethodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GoodSum", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GoodSumTaxed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvalidSum", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCancellation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PtomUnit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("QuarantinedSum", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("QuarantinedSumTaxed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResponsibleClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketInternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationSum", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationSumTaxed", fieldHashtable);
		}
		#endregion

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. </summary>
		/// <returns>true</returns>
		public override bool Refetch()
		{
			return true;
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSettlementResultDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SettlementResultEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SettlementResultRelations Relations
		{
			get	{ return new SettlementResultRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BadSum property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."BADSUM"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> BadSum
		{
			get { return (Nullable<System.Decimal>)GetValue((int)SettlementResultFieldIndex.BadSum, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.BadSum, value, true); }
		}

		/// <summary> The BadSumTaxed property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."BADSUMTAXED"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> BadSumTaxed
		{
			get { return (Nullable<System.Decimal>)GetValue((int)SettlementResultFieldIndex.BadSumTaxed, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.BadSumTaxed, value, true); }
		}

		/// <summary> The CardIssuerID property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."CARDISSUERID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardIssuerID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.CardIssuerID, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.CardIssuerID, value, true); }
		}

		/// <summary> The ClearingResultLevel property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."CLEARINGRESULTLEVEL"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ClearingResultLevel
		{
			get { return (System.Int32)GetValue((int)SettlementResultFieldIndex.ClearingResultLevel, true); }
			set	{ SetValue((int)SettlementResultFieldIndex.ClearingResultLevel, value, true); }
		}

		/// <summary> The ClientID property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."CLIENTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.ClientID, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.ClientID, value, true); }
		}

		/// <summary> The ContractorID property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."CONTRACTORID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.ContractorID, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.ContractorID, value, true); }
		}

		/// <summary> The CorrectedSum property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."CORRECTEDSUM"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> CorrectedSum
		{
			get { return (Nullable<System.Decimal>)GetValue((int)SettlementResultFieldIndex.CorrectedSum, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.CorrectedSum, value, true); }
		}

		/// <summary> The CorrectedSumTaxed property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."CORRECTEDSUMTAXED"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> CorrectedSumTaxed
		{
			get { return (Nullable<System.Decimal>)GetValue((int)SettlementResultFieldIndex.CorrectedSumTaxed, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.CorrectedSumTaxed, value, true); }
		}

		/// <summary> The CustomerGroupID property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."CUSTOMERGROUPID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CustomerGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.CustomerGroupID, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.CustomerGroupID, value, true); }
		}

		/// <summary> The DebtorID property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."DEBTORID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DebtorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.DebtorID, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.DebtorID, value, true); }
		}

		/// <summary> The DeviceClassID property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."DEVICECLASSID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceClassID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.DeviceClassID, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The DevicePaymentMethodID property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."DEVICEPAYMENTMETHODID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DevicePaymentMethodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.DevicePaymentMethodID, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.DevicePaymentMethodID, value, true); }
		}

		/// <summary> The GoodSum property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."GOODSUM"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> GoodSum
		{
			get { return (Nullable<System.Decimal>)GetValue((int)SettlementResultFieldIndex.GoodSum, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.GoodSum, value, true); }
		}

		/// <summary> The GoodSumTaxed property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."GOODSUMTAXED"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> GoodSumTaxed
		{
			get { return (Nullable<System.Decimal>)GetValue((int)SettlementResultFieldIndex.GoodSumTaxed, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.GoodSumTaxed, value, true); }
		}

		/// <summary> The InvalidSum property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."GOODSUMTAXED"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> InvalidSum
		{
			get { return (Nullable<System.Decimal>)GetValue((int)SettlementResultFieldIndex.InvalidSum, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.InvalidSum, value, true); }
		}

		/// <summary> The IsCancellation property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."ISCANCELLATION"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsCancellation
		{
			get { return (Nullable<System.Int16>)GetValue((int)SettlementResultFieldIndex.IsCancellation, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.IsCancellation, value, true); }
		}

		/// <summary> The LineGroupID property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."LINEGROUPID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.LineGroupID, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.LineGroupID, value, true); }
		}

		/// <summary> The LineID property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."LINEID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.LineID, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.LineID, value, true); }
		}

		/// <summary> The PtomUnit property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."PTOMUNIT"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PtomUnit
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.PtomUnit, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.PtomUnit, value, true); }
		}

		/// <summary> The QuarantinedSum property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."QUARANTINEDSUM"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> QuarantinedSum
		{
			get { return (Nullable<System.Decimal>)GetValue((int)SettlementResultFieldIndex.QuarantinedSum, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.QuarantinedSum, value, true); }
		}

		/// <summary> The QuarantinedSumTaxed property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."QUARANTINEDSUMTAXED"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> QuarantinedSumTaxed
		{
			get { return (Nullable<System.Decimal>)GetValue((int)SettlementResultFieldIndex.QuarantinedSumTaxed, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.QuarantinedSumTaxed, value, true); }
		}

		/// <summary> The ResponsibleClientID property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."RESPONSIBLECLIENTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ResponsibleClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.ResponsibleClientID, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.ResponsibleClientID, value, true); }
		}

		/// <summary> The SettlementID property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."SETTLEMENTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SettlementID
		{
			get { return (System.Int64)GetValue((int)SettlementResultFieldIndex.SettlementID, true); }
			set	{ SetValue((int)SettlementResultFieldIndex.SettlementID, value, true); }
		}

		/// <summary> The TicketInternalNumber property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."TICKETINTERNALNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketInternalNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.TicketInternalNumber, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.TicketInternalNumber, value, true); }
		}

		/// <summary> The TicketType property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."TICKETTYPE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.TicketType> TicketType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.TicketType>)GetValue((int)SettlementResultFieldIndex.TicketType, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.TicketType, value, true); }
		}

		/// <summary> The TransactionDate property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."TRANSACTIONDATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> TransactionDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SettlementResultFieldIndex.TransactionDate, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.TransactionDate, value, true); }
		}

		/// <summary> The TransactionTypeID property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."TRANSACTIONTYPEID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementResultFieldIndex.TransactionTypeID, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.TransactionTypeID, value, true); }
		}

		/// <summary> The TripCode property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."TRIPCODE"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TripCode
		{
			get { return (System.String)GetValue((int)SettlementResultFieldIndex.TripCode, true); }
			set	{ SetValue((int)SettlementResultFieldIndex.TripCode, value, true); }
		}

		/// <summary> The ValidationSum property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."VALIDATIONSUM"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> ValidationSum
		{
			get { return (Nullable<System.Decimal>)GetValue((int)SettlementResultFieldIndex.ValidationSum, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.ValidationSum, value, true); }
		}

		/// <summary> The ValidationSumTaxed property of the Entity SettlementResult<br/><br/></summary>
		/// <remarks>Mapped on  view field: "CH_SETTLEMENTRESULTS"."VALIDATIONSUMTAXED"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> ValidationSumTaxed
		{
			get { return (Nullable<System.Decimal>)GetValue((int)SettlementResultFieldIndex.ValidationSumTaxed, false); }
			set	{ SetValue((int)SettlementResultFieldIndex.ValidationSumTaxed, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SettlementResultEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
