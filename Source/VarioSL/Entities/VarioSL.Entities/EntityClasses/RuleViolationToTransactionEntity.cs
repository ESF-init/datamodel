﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RuleViolationToTransaction'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RuleViolationToTransactionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private TransactionEntity _revenueTransaction;
		private bool	_alwaysFetchRevenueTransaction, _alreadyFetchedRevenueTransaction, _revenueTransactionReturnsNewIfNotFound;
		private PaymentJournalEntity _paymentJournal;
		private bool	_alwaysFetchPaymentJournal, _alreadyFetchedPaymentJournal, _paymentJournalReturnsNewIfNotFound;
		private RuleViolationEntity _ruleViolation;
		private bool	_alwaysFetchRuleViolation, _alreadyFetchedRuleViolation, _ruleViolationReturnsNewIfNotFound;
		private TransactionJournalEntity _transactionJournal;
		private bool	_alwaysFetchTransactionJournal, _alreadyFetchedTransactionJournal, _transactionJournalReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name RevenueTransaction</summary>
			public static readonly string RevenueTransaction = "RevenueTransaction";
			/// <summary>Member name PaymentJournal</summary>
			public static readonly string PaymentJournal = "PaymentJournal";
			/// <summary>Member name RuleViolation</summary>
			public static readonly string RuleViolation = "RuleViolation";
			/// <summary>Member name TransactionJournal</summary>
			public static readonly string TransactionJournal = "TransactionJournal";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RuleViolationToTransactionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RuleViolationToTransactionEntity() :base("RuleViolationToTransactionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="ruleViolationToTransactionID">PK value for RuleViolationToTransaction which data should be fetched into this RuleViolationToTransaction object</param>
		public RuleViolationToTransactionEntity(System.Int64 ruleViolationToTransactionID):base("RuleViolationToTransactionEntity")
		{
			InitClassFetch(ruleViolationToTransactionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="ruleViolationToTransactionID">PK value for RuleViolationToTransaction which data should be fetched into this RuleViolationToTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RuleViolationToTransactionEntity(System.Int64 ruleViolationToTransactionID, IPrefetchPath prefetchPathToUse):base("RuleViolationToTransactionEntity")
		{
			InitClassFetch(ruleViolationToTransactionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="ruleViolationToTransactionID">PK value for RuleViolationToTransaction which data should be fetched into this RuleViolationToTransaction object</param>
		/// <param name="validator">The custom validator object for this RuleViolationToTransactionEntity</param>
		public RuleViolationToTransactionEntity(System.Int64 ruleViolationToTransactionID, IValidator validator):base("RuleViolationToTransactionEntity")
		{
			InitClassFetch(ruleViolationToTransactionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RuleViolationToTransactionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_revenueTransaction = (TransactionEntity)info.GetValue("_revenueTransaction", typeof(TransactionEntity));
			if(_revenueTransaction!=null)
			{
				_revenueTransaction.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_revenueTransactionReturnsNewIfNotFound = info.GetBoolean("_revenueTransactionReturnsNewIfNotFound");
			_alwaysFetchRevenueTransaction = info.GetBoolean("_alwaysFetchRevenueTransaction");
			_alreadyFetchedRevenueTransaction = info.GetBoolean("_alreadyFetchedRevenueTransaction");

			_paymentJournal = (PaymentJournalEntity)info.GetValue("_paymentJournal", typeof(PaymentJournalEntity));
			if(_paymentJournal!=null)
			{
				_paymentJournal.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentJournalReturnsNewIfNotFound = info.GetBoolean("_paymentJournalReturnsNewIfNotFound");
			_alwaysFetchPaymentJournal = info.GetBoolean("_alwaysFetchPaymentJournal");
			_alreadyFetchedPaymentJournal = info.GetBoolean("_alreadyFetchedPaymentJournal");

			_ruleViolation = (RuleViolationEntity)info.GetValue("_ruleViolation", typeof(RuleViolationEntity));
			if(_ruleViolation!=null)
			{
				_ruleViolation.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ruleViolationReturnsNewIfNotFound = info.GetBoolean("_ruleViolationReturnsNewIfNotFound");
			_alwaysFetchRuleViolation = info.GetBoolean("_alwaysFetchRuleViolation");
			_alreadyFetchedRuleViolation = info.GetBoolean("_alreadyFetchedRuleViolation");

			_transactionJournal = (TransactionJournalEntity)info.GetValue("_transactionJournal", typeof(TransactionJournalEntity));
			if(_transactionJournal!=null)
			{
				_transactionJournal.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_transactionJournalReturnsNewIfNotFound = info.GetBoolean("_transactionJournalReturnsNewIfNotFound");
			_alwaysFetchTransactionJournal = info.GetBoolean("_alwaysFetchTransactionJournal");
			_alreadyFetchedTransactionJournal = info.GetBoolean("_alreadyFetchedTransactionJournal");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RuleViolationToTransactionFieldIndex)fieldIndex)
			{
				case RuleViolationToTransactionFieldIndex.PaymentJournalID:
					DesetupSyncPaymentJournal(true, false);
					_alreadyFetchedPaymentJournal = false;
					break;
				case RuleViolationToTransactionFieldIndex.RuleViolationID:
					DesetupSyncRuleViolation(true, false);
					_alreadyFetchedRuleViolation = false;
					break;
				case RuleViolationToTransactionFieldIndex.TransactionID:
					DesetupSyncRevenueTransaction(true, false);
					_alreadyFetchedRevenueTransaction = false;
					break;
				case RuleViolationToTransactionFieldIndex.TransactionJournalID:
					DesetupSyncTransactionJournal(true, false);
					_alreadyFetchedTransactionJournal = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRevenueTransaction = (_revenueTransaction != null);
			_alreadyFetchedPaymentJournal = (_paymentJournal != null);
			_alreadyFetchedRuleViolation = (_ruleViolation != null);
			_alreadyFetchedTransactionJournal = (_transactionJournal != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "RevenueTransaction":
					toReturn.Add(Relations.TransactionEntityUsingTransactionID);
					break;
				case "PaymentJournal":
					toReturn.Add(Relations.PaymentJournalEntityUsingPaymentJournalID);
					break;
				case "RuleViolation":
					toReturn.Add(Relations.RuleViolationEntityUsingRuleViolationID);
					break;
				case "TransactionJournal":
					toReturn.Add(Relations.TransactionJournalEntityUsingTransactionJournalID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_revenueTransaction", (!this.MarkedForDeletion?_revenueTransaction:null));
			info.AddValue("_revenueTransactionReturnsNewIfNotFound", _revenueTransactionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRevenueTransaction", _alwaysFetchRevenueTransaction);
			info.AddValue("_alreadyFetchedRevenueTransaction", _alreadyFetchedRevenueTransaction);
			info.AddValue("_paymentJournal", (!this.MarkedForDeletion?_paymentJournal:null));
			info.AddValue("_paymentJournalReturnsNewIfNotFound", _paymentJournalReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentJournal", _alwaysFetchPaymentJournal);
			info.AddValue("_alreadyFetchedPaymentJournal", _alreadyFetchedPaymentJournal);
			info.AddValue("_ruleViolation", (!this.MarkedForDeletion?_ruleViolation:null));
			info.AddValue("_ruleViolationReturnsNewIfNotFound", _ruleViolationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRuleViolation", _alwaysFetchRuleViolation);
			info.AddValue("_alreadyFetchedRuleViolation", _alreadyFetchedRuleViolation);
			info.AddValue("_transactionJournal", (!this.MarkedForDeletion?_transactionJournal:null));
			info.AddValue("_transactionJournalReturnsNewIfNotFound", _transactionJournalReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTransactionJournal", _alwaysFetchTransactionJournal);
			info.AddValue("_alreadyFetchedTransactionJournal", _alreadyFetchedTransactionJournal);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "RevenueTransaction":
					_alreadyFetchedRevenueTransaction = true;
					this.RevenueTransaction = (TransactionEntity)entity;
					break;
				case "PaymentJournal":
					_alreadyFetchedPaymentJournal = true;
					this.PaymentJournal = (PaymentJournalEntity)entity;
					break;
				case "RuleViolation":
					_alreadyFetchedRuleViolation = true;
					this.RuleViolation = (RuleViolationEntity)entity;
					break;
				case "TransactionJournal":
					_alreadyFetchedTransactionJournal = true;
					this.TransactionJournal = (TransactionJournalEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "RevenueTransaction":
					SetupSyncRevenueTransaction(relatedEntity);
					break;
				case "PaymentJournal":
					SetupSyncPaymentJournal(relatedEntity);
					break;
				case "RuleViolation":
					SetupSyncRuleViolation(relatedEntity);
					break;
				case "TransactionJournal":
					SetupSyncTransactionJournal(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "RevenueTransaction":
					DesetupSyncRevenueTransaction(false, true);
					break;
				case "PaymentJournal":
					DesetupSyncPaymentJournal(false, true);
					break;
				case "RuleViolation":
					DesetupSyncRuleViolation(false, true);
					break;
				case "TransactionJournal":
					DesetupSyncTransactionJournal(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_revenueTransaction!=null)
			{
				toReturn.Add(_revenueTransaction);
			}
			if(_paymentJournal!=null)
			{
				toReturn.Add(_paymentJournal);
			}
			if(_ruleViolation!=null)
			{
				toReturn.Add(_ruleViolation);
			}
			if(_transactionJournal!=null)
			{
				toReturn.Add(_transactionJournal);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleViolationToTransactionID">PK value for RuleViolationToTransaction which data should be fetched into this RuleViolationToTransaction object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleViolationToTransactionID)
		{
			return FetchUsingPK(ruleViolationToTransactionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleViolationToTransactionID">PK value for RuleViolationToTransaction which data should be fetched into this RuleViolationToTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleViolationToTransactionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(ruleViolationToTransactionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleViolationToTransactionID">PK value for RuleViolationToTransaction which data should be fetched into this RuleViolationToTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleViolationToTransactionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(ruleViolationToTransactionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleViolationToTransactionID">PK value for RuleViolationToTransaction which data should be fetched into this RuleViolationToTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleViolationToTransactionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(ruleViolationToTransactionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RuleViolationToTransactionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RuleViolationToTransactionRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'TransactionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TransactionEntity' which is related to this entity.</returns>
		public TransactionEntity GetSingleRevenueTransaction()
		{
			return GetSingleRevenueTransaction(false);
		}

		/// <summary> Retrieves the related entity of type 'TransactionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TransactionEntity' which is related to this entity.</returns>
		public virtual TransactionEntity GetSingleRevenueTransaction(bool forceFetch)
		{
			if( ( !_alreadyFetchedRevenueTransaction || forceFetch || _alwaysFetchRevenueTransaction) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TransactionEntityUsingTransactionID);
				TransactionEntity newEntity = new TransactionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TransactionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TransactionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_revenueTransactionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RevenueTransaction = newEntity;
				_alreadyFetchedRevenueTransaction = fetchResult;
			}
			return _revenueTransaction;
		}


		/// <summary> Retrieves the related entity of type 'PaymentJournalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentJournalEntity' which is related to this entity.</returns>
		public PaymentJournalEntity GetSinglePaymentJournal()
		{
			return GetSinglePaymentJournal(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentJournalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentJournalEntity' which is related to this entity.</returns>
		public virtual PaymentJournalEntity GetSinglePaymentJournal(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentJournal || forceFetch || _alwaysFetchPaymentJournal) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentJournalEntityUsingPaymentJournalID);
				PaymentJournalEntity newEntity = new PaymentJournalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentJournalID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentJournalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentJournalReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentJournal = newEntity;
				_alreadyFetchedPaymentJournal = fetchResult;
			}
			return _paymentJournal;
		}


		/// <summary> Retrieves the related entity of type 'RuleViolationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RuleViolationEntity' which is related to this entity.</returns>
		public RuleViolationEntity GetSingleRuleViolation()
		{
			return GetSingleRuleViolation(false);
		}

		/// <summary> Retrieves the related entity of type 'RuleViolationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RuleViolationEntity' which is related to this entity.</returns>
		public virtual RuleViolationEntity GetSingleRuleViolation(bool forceFetch)
		{
			if( ( !_alreadyFetchedRuleViolation || forceFetch || _alwaysFetchRuleViolation) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RuleViolationEntityUsingRuleViolationID);
				RuleViolationEntity newEntity = new RuleViolationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RuleViolationID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RuleViolationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ruleViolationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RuleViolation = newEntity;
				_alreadyFetchedRuleViolation = fetchResult;
			}
			return _ruleViolation;
		}


		/// <summary> Retrieves the related entity of type 'TransactionJournalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TransactionJournalEntity' which is related to this entity.</returns>
		public TransactionJournalEntity GetSingleTransactionJournal()
		{
			return GetSingleTransactionJournal(false);
		}

		/// <summary> Retrieves the related entity of type 'TransactionJournalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TransactionJournalEntity' which is related to this entity.</returns>
		public virtual TransactionJournalEntity GetSingleTransactionJournal(bool forceFetch)
		{
			if( ( !_alreadyFetchedTransactionJournal || forceFetch || _alwaysFetchTransactionJournal) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TransactionJournalEntityUsingTransactionJournalID);
				TransactionJournalEntity newEntity = new TransactionJournalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TransactionJournalID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TransactionJournalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_transactionJournalReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TransactionJournal = newEntity;
				_alreadyFetchedTransactionJournal = fetchResult;
			}
			return _transactionJournal;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("RevenueTransaction", _revenueTransaction);
			toReturn.Add("PaymentJournal", _paymentJournal);
			toReturn.Add("RuleViolation", _ruleViolation);
			toReturn.Add("TransactionJournal", _transactionJournal);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="ruleViolationToTransactionID">PK value for RuleViolationToTransaction which data should be fetched into this RuleViolationToTransaction object</param>
		/// <param name="validator">The validator object for this RuleViolationToTransactionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 ruleViolationToTransactionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(ruleViolationToTransactionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_revenueTransactionReturnsNewIfNotFound = false;
			_paymentJournalReturnsNewIfNotFound = false;
			_ruleViolationReturnsNewIfNotFound = false;
			_transactionJournalReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleViolationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleViolationToTransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionJournalID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _revenueTransaction</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRevenueTransaction(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _revenueTransaction, new PropertyChangedEventHandler( OnRevenueTransactionPropertyChanged ), "RevenueTransaction", VarioSL.Entities.RelationClasses.StaticRuleViolationToTransactionRelations.TransactionEntityUsingTransactionIDStatic, true, signalRelatedEntity, "RuleViolationToTransactions", resetFKFields, new int[] { (int)RuleViolationToTransactionFieldIndex.TransactionID } );		
			_revenueTransaction = null;
		}
		
		/// <summary> setups the sync logic for member _revenueTransaction</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRevenueTransaction(IEntityCore relatedEntity)
		{
			if(_revenueTransaction!=relatedEntity)
			{		
				DesetupSyncRevenueTransaction(true, true);
				_revenueTransaction = (TransactionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _revenueTransaction, new PropertyChangedEventHandler( OnRevenueTransactionPropertyChanged ), "RevenueTransaction", VarioSL.Entities.RelationClasses.StaticRuleViolationToTransactionRelations.TransactionEntityUsingTransactionIDStatic, true, ref _alreadyFetchedRevenueTransaction, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRevenueTransactionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentJournal</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentJournal(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentJournal, new PropertyChangedEventHandler( OnPaymentJournalPropertyChanged ), "PaymentJournal", VarioSL.Entities.RelationClasses.StaticRuleViolationToTransactionRelations.PaymentJournalEntityUsingPaymentJournalIDStatic, true, signalRelatedEntity, "RuleViolationToTransactions", resetFKFields, new int[] { (int)RuleViolationToTransactionFieldIndex.PaymentJournalID } );		
			_paymentJournal = null;
		}
		
		/// <summary> setups the sync logic for member _paymentJournal</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentJournal(IEntityCore relatedEntity)
		{
			if(_paymentJournal!=relatedEntity)
			{		
				DesetupSyncPaymentJournal(true, true);
				_paymentJournal = (PaymentJournalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentJournal, new PropertyChangedEventHandler( OnPaymentJournalPropertyChanged ), "PaymentJournal", VarioSL.Entities.RelationClasses.StaticRuleViolationToTransactionRelations.PaymentJournalEntityUsingPaymentJournalIDStatic, true, ref _alreadyFetchedPaymentJournal, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentJournalPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ruleViolation</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRuleViolation(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ruleViolation, new PropertyChangedEventHandler( OnRuleViolationPropertyChanged ), "RuleViolation", VarioSL.Entities.RelationClasses.StaticRuleViolationToTransactionRelations.RuleViolationEntityUsingRuleViolationIDStatic, true, signalRelatedEntity, "RuleViolationToTransactions", resetFKFields, new int[] { (int)RuleViolationToTransactionFieldIndex.RuleViolationID } );		
			_ruleViolation = null;
		}
		
		/// <summary> setups the sync logic for member _ruleViolation</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRuleViolation(IEntityCore relatedEntity)
		{
			if(_ruleViolation!=relatedEntity)
			{		
				DesetupSyncRuleViolation(true, true);
				_ruleViolation = (RuleViolationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ruleViolation, new PropertyChangedEventHandler( OnRuleViolationPropertyChanged ), "RuleViolation", VarioSL.Entities.RelationClasses.StaticRuleViolationToTransactionRelations.RuleViolationEntityUsingRuleViolationIDStatic, true, ref _alreadyFetchedRuleViolation, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRuleViolationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _transactionJournal</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTransactionJournal(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _transactionJournal, new PropertyChangedEventHandler( OnTransactionJournalPropertyChanged ), "TransactionJournal", VarioSL.Entities.RelationClasses.StaticRuleViolationToTransactionRelations.TransactionJournalEntityUsingTransactionJournalIDStatic, true, signalRelatedEntity, "RuleViolationToTransactions", resetFKFields, new int[] { (int)RuleViolationToTransactionFieldIndex.TransactionJournalID } );		
			_transactionJournal = null;
		}
		
		/// <summary> setups the sync logic for member _transactionJournal</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTransactionJournal(IEntityCore relatedEntity)
		{
			if(_transactionJournal!=relatedEntity)
			{		
				DesetupSyncTransactionJournal(true, true);
				_transactionJournal = (TransactionJournalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _transactionJournal, new PropertyChangedEventHandler( OnTransactionJournalPropertyChanged ), "TransactionJournal", VarioSL.Entities.RelationClasses.StaticRuleViolationToTransactionRelations.TransactionJournalEntityUsingTransactionJournalIDStatic, true, ref _alreadyFetchedTransactionJournal, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTransactionJournalPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="ruleViolationToTransactionID">PK value for RuleViolationToTransaction which data should be fetched into this RuleViolationToTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 ruleViolationToTransactionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RuleViolationToTransactionFieldIndex.RuleViolationToTransactionID].ForcedCurrentValueWrite(ruleViolationToTransactionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRuleViolationToTransactionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RuleViolationToTransactionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RuleViolationToTransactionRelations Relations
		{
			get	{ return new RuleViolationToTransactionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Transaction'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRevenueTransaction
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionCollection(), (IEntityRelation)GetRelationsForField("RevenueTransaction")[0], (int)VarioSL.Entities.EntityType.RuleViolationToTransactionEntity, (int)VarioSL.Entities.EntityType.TransactionEntity, 0, null, null, null, "RevenueTransaction", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentJournal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentJournal
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentJournalCollection(), (IEntityRelation)GetRelationsForField("PaymentJournal")[0], (int)VarioSL.Entities.EntityType.RuleViolationToTransactionEntity, (int)VarioSL.Entities.EntityType.PaymentJournalEntity, 0, null, null, null, "PaymentJournal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleViolation'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleViolation
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleViolationCollection(), (IEntityRelation)GetRelationsForField("RuleViolation")[0], (int)VarioSL.Entities.EntityType.RuleViolationToTransactionEntity, (int)VarioSL.Entities.EntityType.RuleViolationEntity, 0, null, null, null, "RuleViolation", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournal
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournal")[0], (int)VarioSL.Entities.EntityType.RuleViolationToTransactionEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LastModified property of the Entity RuleViolationToTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATIONTOTRANSACTION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)RuleViolationToTransactionFieldIndex.LastModified, true); }
			set	{ SetValue((int)RuleViolationToTransactionFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity RuleViolationToTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATIONTOTRANSACTION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)RuleViolationToTransactionFieldIndex.LastUser, true); }
			set	{ SetValue((int)RuleViolationToTransactionFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PaymentJournalID property of the Entity RuleViolationToTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATIONTOTRANSACTION"."PAYMENTJOURNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentJournalID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RuleViolationToTransactionFieldIndex.PaymentJournalID, false); }
			set	{ SetValue((int)RuleViolationToTransactionFieldIndex.PaymentJournalID, value, true); }
		}

		/// <summary> The RuleViolationID property of the Entity RuleViolationToTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATIONTOTRANSACTION"."RULEVIOLATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RuleViolationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RuleViolationToTransactionFieldIndex.RuleViolationID, false); }
			set	{ SetValue((int)RuleViolationToTransactionFieldIndex.RuleViolationID, value, true); }
		}

		/// <summary> The RuleViolationToTransactionID property of the Entity RuleViolationToTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATIONTOTRANSACTION"."RULEVIOLATIONTOTRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 RuleViolationToTransactionID
		{
			get { return (System.Int64)GetValue((int)RuleViolationToTransactionFieldIndex.RuleViolationToTransactionID, true); }
			set	{ SetValue((int)RuleViolationToTransactionFieldIndex.RuleViolationToTransactionID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity RuleViolationToTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATIONTOTRANSACTION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)RuleViolationToTransactionFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)RuleViolationToTransactionFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransactionID property of the Entity RuleViolationToTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATIONTOTRANSACTION"."TRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RuleViolationToTransactionFieldIndex.TransactionID, false); }
			set	{ SetValue((int)RuleViolationToTransactionFieldIndex.TransactionID, value, true); }
		}

		/// <summary> The TransactionJournalID property of the Entity RuleViolationToTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATIONTOTRANSACTION"."TRANSACTIONJOURNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionJournalID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RuleViolationToTransactionFieldIndex.TransactionJournalID, false); }
			set	{ SetValue((int)RuleViolationToTransactionFieldIndex.TransactionJournalID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'TransactionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRevenueTransaction()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TransactionEntity RevenueTransaction
		{
			get	{ return GetSingleRevenueTransaction(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRevenueTransaction(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RuleViolationToTransactions", "RevenueTransaction", _revenueTransaction, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RevenueTransaction. When set to true, RevenueTransaction is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RevenueTransaction is accessed. You can always execute a forced fetch by calling GetSingleRevenueTransaction(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRevenueTransaction
		{
			get	{ return _alwaysFetchRevenueTransaction; }
			set	{ _alwaysFetchRevenueTransaction = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RevenueTransaction already has been fetched. Setting this property to false when RevenueTransaction has been fetched
		/// will set RevenueTransaction to null as well. Setting this property to true while RevenueTransaction hasn't been fetched disables lazy loading for RevenueTransaction</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRevenueTransaction
		{
			get { return _alreadyFetchedRevenueTransaction;}
			set 
			{
				if(_alreadyFetchedRevenueTransaction && !value)
				{
					this.RevenueTransaction = null;
				}
				_alreadyFetchedRevenueTransaction = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RevenueTransaction is not found
		/// in the database. When set to true, RevenueTransaction will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RevenueTransactionReturnsNewIfNotFound
		{
			get	{ return _revenueTransactionReturnsNewIfNotFound; }
			set { _revenueTransactionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentJournalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentJournal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentJournalEntity PaymentJournal
		{
			get	{ return GetSinglePaymentJournal(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentJournal(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RuleViolationToTransactions", "PaymentJournal", _paymentJournal, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentJournal. When set to true, PaymentJournal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentJournal is accessed. You can always execute a forced fetch by calling GetSinglePaymentJournal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentJournal
		{
			get	{ return _alwaysFetchPaymentJournal; }
			set	{ _alwaysFetchPaymentJournal = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentJournal already has been fetched. Setting this property to false when PaymentJournal has been fetched
		/// will set PaymentJournal to null as well. Setting this property to true while PaymentJournal hasn't been fetched disables lazy loading for PaymentJournal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentJournal
		{
			get { return _alreadyFetchedPaymentJournal;}
			set 
			{
				if(_alreadyFetchedPaymentJournal && !value)
				{
					this.PaymentJournal = null;
				}
				_alreadyFetchedPaymentJournal = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentJournal is not found
		/// in the database. When set to true, PaymentJournal will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentJournalReturnsNewIfNotFound
		{
			get	{ return _paymentJournalReturnsNewIfNotFound; }
			set { _paymentJournalReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RuleViolationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRuleViolation()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RuleViolationEntity RuleViolation
		{
			get	{ return GetSingleRuleViolation(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRuleViolation(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RuleViolationToTransactions", "RuleViolation", _ruleViolation, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RuleViolation. When set to true, RuleViolation is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleViolation is accessed. You can always execute a forced fetch by calling GetSingleRuleViolation(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleViolation
		{
			get	{ return _alwaysFetchRuleViolation; }
			set	{ _alwaysFetchRuleViolation = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleViolation already has been fetched. Setting this property to false when RuleViolation has been fetched
		/// will set RuleViolation to null as well. Setting this property to true while RuleViolation hasn't been fetched disables lazy loading for RuleViolation</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleViolation
		{
			get { return _alreadyFetchedRuleViolation;}
			set 
			{
				if(_alreadyFetchedRuleViolation && !value)
				{
					this.RuleViolation = null;
				}
				_alreadyFetchedRuleViolation = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RuleViolation is not found
		/// in the database. When set to true, RuleViolation will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RuleViolationReturnsNewIfNotFound
		{
			get	{ return _ruleViolationReturnsNewIfNotFound; }
			set { _ruleViolationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TransactionJournalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTransactionJournal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TransactionJournalEntity TransactionJournal
		{
			get	{ return GetSingleTransactionJournal(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTransactionJournal(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RuleViolationToTransactions", "TransactionJournal", _transactionJournal, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournal. When set to true, TransactionJournal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournal is accessed. You can always execute a forced fetch by calling GetSingleTransactionJournal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournal
		{
			get	{ return _alwaysFetchTransactionJournal; }
			set	{ _alwaysFetchTransactionJournal = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournal already has been fetched. Setting this property to false when TransactionJournal has been fetched
		/// will set TransactionJournal to null as well. Setting this property to true while TransactionJournal hasn't been fetched disables lazy loading for TransactionJournal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournal
		{
			get { return _alreadyFetchedTransactionJournal;}
			set 
			{
				if(_alreadyFetchedTransactionJournal && !value)
				{
					this.TransactionJournal = null;
				}
				_alreadyFetchedTransactionJournal = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TransactionJournal is not found
		/// in the database. When set to true, TransactionJournal will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TransactionJournalReturnsNewIfNotFound
		{
			get	{ return _transactionJournalReturnsNewIfNotFound; }
			set { _transactionJournalReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.RuleViolationToTransactionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
