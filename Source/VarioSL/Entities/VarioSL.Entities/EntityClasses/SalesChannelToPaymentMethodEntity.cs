﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SalesChannelToPaymentMethod'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SalesChannelToPaymentMethodEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private DeviceClassEntity _deviceClass;
		private bool	_alwaysFetchDeviceClass, _alreadyFetchedDeviceClass, _deviceClassReturnsNewIfNotFound;
		private DevicePaymentMethodEntity _devicePaymentMethod;
		private bool	_alwaysFetchDevicePaymentMethod, _alreadyFetchedDevicePaymentMethod, _devicePaymentMethodReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeviceClass</summary>
			public static readonly string DeviceClass = "DeviceClass";
			/// <summary>Member name DevicePaymentMethod</summary>
			public static readonly string DevicePaymentMethod = "DevicePaymentMethod";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SalesChannelToPaymentMethodEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SalesChannelToPaymentMethodEntity() :base("SalesChannelToPaymentMethodEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="deviceClassID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="devicePaymentMethodID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		public SalesChannelToPaymentMethodEntity(System.Int64 deviceClassID, System.Int64 devicePaymentMethodID):base("SalesChannelToPaymentMethodEntity")
		{
			InitClassFetch(deviceClassID, devicePaymentMethodID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceClassID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="devicePaymentMethodID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SalesChannelToPaymentMethodEntity(System.Int64 deviceClassID, System.Int64 devicePaymentMethodID, IPrefetchPath prefetchPathToUse):base("SalesChannelToPaymentMethodEntity")
		{
			InitClassFetch(deviceClassID, devicePaymentMethodID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceClassID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="devicePaymentMethodID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="validator">The custom validator object for this SalesChannelToPaymentMethodEntity</param>
		public SalesChannelToPaymentMethodEntity(System.Int64 deviceClassID, System.Int64 devicePaymentMethodID, IValidator validator):base("SalesChannelToPaymentMethodEntity")
		{
			InitClassFetch(deviceClassID, devicePaymentMethodID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SalesChannelToPaymentMethodEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_deviceClass = (DeviceClassEntity)info.GetValue("_deviceClass", typeof(DeviceClassEntity));
			if(_deviceClass!=null)
			{
				_deviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceClassReturnsNewIfNotFound = info.GetBoolean("_deviceClassReturnsNewIfNotFound");
			_alwaysFetchDeviceClass = info.GetBoolean("_alwaysFetchDeviceClass");
			_alreadyFetchedDeviceClass = info.GetBoolean("_alreadyFetchedDeviceClass");

			_devicePaymentMethod = (DevicePaymentMethodEntity)info.GetValue("_devicePaymentMethod", typeof(DevicePaymentMethodEntity));
			if(_devicePaymentMethod!=null)
			{
				_devicePaymentMethod.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_devicePaymentMethodReturnsNewIfNotFound = info.GetBoolean("_devicePaymentMethodReturnsNewIfNotFound");
			_alwaysFetchDevicePaymentMethod = info.GetBoolean("_alwaysFetchDevicePaymentMethod");
			_alreadyFetchedDevicePaymentMethod = info.GetBoolean("_alreadyFetchedDevicePaymentMethod");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SalesChannelToPaymentMethodFieldIndex)fieldIndex)
			{
				case SalesChannelToPaymentMethodFieldIndex.DeviceClassID:
					DesetupSyncDeviceClass(true, false);
					_alreadyFetchedDeviceClass = false;
					break;
				case SalesChannelToPaymentMethodFieldIndex.DevicePaymentMethodID:
					DesetupSyncDevicePaymentMethod(true, false);
					_alreadyFetchedDevicePaymentMethod = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDeviceClass = (_deviceClass != null);
			_alreadyFetchedDevicePaymentMethod = (_devicePaymentMethod != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeviceClass":
					toReturn.Add(Relations.DeviceClassEntityUsingDeviceClassID);
					break;
				case "DevicePaymentMethod":
					toReturn.Add(Relations.DevicePaymentMethodEntityUsingDevicePaymentMethodID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_deviceClass", (!this.MarkedForDeletion?_deviceClass:null));
			info.AddValue("_deviceClassReturnsNewIfNotFound", _deviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceClass", _alwaysFetchDeviceClass);
			info.AddValue("_alreadyFetchedDeviceClass", _alreadyFetchedDeviceClass);
			info.AddValue("_devicePaymentMethod", (!this.MarkedForDeletion?_devicePaymentMethod:null));
			info.AddValue("_devicePaymentMethodReturnsNewIfNotFound", _devicePaymentMethodReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDevicePaymentMethod", _alwaysFetchDevicePaymentMethod);
			info.AddValue("_alreadyFetchedDevicePaymentMethod", _alreadyFetchedDevicePaymentMethod);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeviceClass":
					_alreadyFetchedDeviceClass = true;
					this.DeviceClass = (DeviceClassEntity)entity;
					break;
				case "DevicePaymentMethod":
					_alreadyFetchedDevicePaymentMethod = true;
					this.DevicePaymentMethod = (DevicePaymentMethodEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeviceClass":
					SetupSyncDeviceClass(relatedEntity);
					break;
				case "DevicePaymentMethod":
					SetupSyncDevicePaymentMethod(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeviceClass":
					DesetupSyncDeviceClass(false, true);
					break;
				case "DevicePaymentMethod":
					DesetupSyncDevicePaymentMethod(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_deviceClass!=null)
			{
				toReturn.Add(_deviceClass);
			}
			if(_devicePaymentMethod!=null)
			{
				toReturn.Add(_devicePaymentMethod);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceClassID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="devicePaymentMethodID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceClassID, System.Int64 devicePaymentMethodID)
		{
			return FetchUsingPK(deviceClassID, devicePaymentMethodID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceClassID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="devicePaymentMethodID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceClassID, System.Int64 devicePaymentMethodID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deviceClassID, devicePaymentMethodID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceClassID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="devicePaymentMethodID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceClassID, System.Int64 devicePaymentMethodID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deviceClassID, devicePaymentMethodID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceClassID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="devicePaymentMethodID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceClassID, System.Int64 devicePaymentMethodID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deviceClassID, devicePaymentMethodID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeviceClassID, this.DevicePaymentMethodID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SalesChannelToPaymentMethodRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public DeviceClassEntity GetSingleDeviceClass()
		{
			return GetSingleDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public virtual DeviceClassEntity GetSingleDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceClass || forceFetch || _alwaysFetchDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceClassEntityUsingDeviceClassID);
				DeviceClassEntity newEntity = new DeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceClassID);
				}
				if(fetchResult)
				{
					newEntity = (DeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceClass = newEntity;
				_alreadyFetchedDeviceClass = fetchResult;
			}
			return _deviceClass;
		}


		/// <summary> Retrieves the related entity of type 'DevicePaymentMethodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DevicePaymentMethodEntity' which is related to this entity.</returns>
		public DevicePaymentMethodEntity GetSingleDevicePaymentMethod()
		{
			return GetSingleDevicePaymentMethod(false);
		}

		/// <summary> Retrieves the related entity of type 'DevicePaymentMethodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DevicePaymentMethodEntity' which is related to this entity.</returns>
		public virtual DevicePaymentMethodEntity GetSingleDevicePaymentMethod(bool forceFetch)
		{
			if( ( !_alreadyFetchedDevicePaymentMethod || forceFetch || _alwaysFetchDevicePaymentMethod) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DevicePaymentMethodEntityUsingDevicePaymentMethodID);
				DevicePaymentMethodEntity newEntity = new DevicePaymentMethodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DevicePaymentMethodID);
				}
				if(fetchResult)
				{
					newEntity = (DevicePaymentMethodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_devicePaymentMethodReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DevicePaymentMethod = newEntity;
				_alreadyFetchedDevicePaymentMethod = fetchResult;
			}
			return _devicePaymentMethod;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeviceClass", _deviceClass);
			toReturn.Add("DevicePaymentMethod", _devicePaymentMethod);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deviceClassID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="devicePaymentMethodID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="validator">The validator object for this SalesChannelToPaymentMethodEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 deviceClassID, System.Int64 devicePaymentMethodID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deviceClassID, devicePaymentMethodID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_deviceClassReturnsNewIfNotFound = false;
			_devicePaymentMethodReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DevicePaymentMethodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Usage", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _deviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticSalesChannelToPaymentMethodRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, signalRelatedEntity, "SalesChannelToPaymentMethods", resetFKFields, new int[] { (int)SalesChannelToPaymentMethodFieldIndex.DeviceClassID } );		
			_deviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _deviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceClass(IEntityCore relatedEntity)
		{
			if(_deviceClass!=relatedEntity)
			{		
				DesetupSyncDeviceClass(true, true);
				_deviceClass = (DeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticSalesChannelToPaymentMethodRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, ref _alreadyFetchedDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _devicePaymentMethod</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDevicePaymentMethod(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _devicePaymentMethod, new PropertyChangedEventHandler( OnDevicePaymentMethodPropertyChanged ), "DevicePaymentMethod", VarioSL.Entities.RelationClasses.StaticSalesChannelToPaymentMethodRelations.DevicePaymentMethodEntityUsingDevicePaymentMethodIDStatic, true, signalRelatedEntity, "SalesChannelToPaymentMethods", resetFKFields, new int[] { (int)SalesChannelToPaymentMethodFieldIndex.DevicePaymentMethodID } );		
			_devicePaymentMethod = null;
		}
		
		/// <summary> setups the sync logic for member _devicePaymentMethod</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDevicePaymentMethod(IEntityCore relatedEntity)
		{
			if(_devicePaymentMethod!=relatedEntity)
			{		
				DesetupSyncDevicePaymentMethod(true, true);
				_devicePaymentMethod = (DevicePaymentMethodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _devicePaymentMethod, new PropertyChangedEventHandler( OnDevicePaymentMethodPropertyChanged ), "DevicePaymentMethod", VarioSL.Entities.RelationClasses.StaticSalesChannelToPaymentMethodRelations.DevicePaymentMethodEntityUsingDevicePaymentMethodIDStatic, true, ref _alreadyFetchedDevicePaymentMethod, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDevicePaymentMethodPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deviceClassID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="devicePaymentMethodID">PK value for SalesChannelToPaymentMethod which data should be fetched into this SalesChannelToPaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 deviceClassID, System.Int64 devicePaymentMethodID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SalesChannelToPaymentMethodFieldIndex.DeviceClassID].ForcedCurrentValueWrite(deviceClassID);
				this.Fields[(int)SalesChannelToPaymentMethodFieldIndex.DevicePaymentMethodID].ForcedCurrentValueWrite(devicePaymentMethodID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSalesChannelToPaymentMethodDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SalesChannelToPaymentMethodEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SalesChannelToPaymentMethodRelations Relations
		{
			get	{ return new SalesChannelToPaymentMethodRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceClassCollection(), (IEntityRelation)GetRelationsForField("DeviceClass")[0], (int)VarioSL.Entities.EntityType.SalesChannelToPaymentMethodEntity, (int)VarioSL.Entities.EntityType.DeviceClassEntity, 0, null, null, null, "DeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DevicePaymentMethod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDevicePaymentMethod
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DevicePaymentMethodCollection(), (IEntityRelation)GetRelationsForField("DevicePaymentMethod")[0], (int)VarioSL.Entities.EntityType.SalesChannelToPaymentMethodEntity, (int)VarioSL.Entities.EntityType.DevicePaymentMethodEntity, 0, null, null, null, "DevicePaymentMethod", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DeviceClassID property of the Entity SalesChannelToPaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CHANNELTOPAYMENTMETHOD"."DEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 DeviceClassID
		{
			get { return (System.Int64)GetValue((int)SalesChannelToPaymentMethodFieldIndex.DeviceClassID, true); }
			set	{ SetValue((int)SalesChannelToPaymentMethodFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The DevicePaymentMethodID property of the Entity SalesChannelToPaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CHANNELTOPAYMENTMETHOD"."DEVICEPAYMENTMETHODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 DevicePaymentMethodID
		{
			get { return (System.Int64)GetValue((int)SalesChannelToPaymentMethodFieldIndex.DevicePaymentMethodID, true); }
			set	{ SetValue((int)SalesChannelToPaymentMethodFieldIndex.DevicePaymentMethodID, value, true); }
		}

		/// <summary> The Usage property of the Entity SalesChannelToPaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CHANNELTOPAYMENTMETHOD"."USAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.PaymentMethodUsage Usage
		{
			get { return (VarioSL.Entities.Enumerations.PaymentMethodUsage)GetValue((int)SalesChannelToPaymentMethodFieldIndex.Usage, true); }
			set	{ SetValue((int)SalesChannelToPaymentMethodFieldIndex.Usage, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'DeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceClassEntity DeviceClass
		{
			get	{ return GetSingleDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SalesChannelToPaymentMethods", "DeviceClass", _deviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceClass. When set to true, DeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceClass is accessed. You can always execute a forced fetch by calling GetSingleDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceClass
		{
			get	{ return _alwaysFetchDeviceClass; }
			set	{ _alwaysFetchDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceClass already has been fetched. Setting this property to false when DeviceClass has been fetched
		/// will set DeviceClass to null as well. Setting this property to true while DeviceClass hasn't been fetched disables lazy loading for DeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceClass
		{
			get { return _alreadyFetchedDeviceClass;}
			set 
			{
				if(_alreadyFetchedDeviceClass && !value)
				{
					this.DeviceClass = null;
				}
				_alreadyFetchedDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceClass is not found
		/// in the database. When set to true, DeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceClassReturnsNewIfNotFound
		{
			get	{ return _deviceClassReturnsNewIfNotFound; }
			set { _deviceClassReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DevicePaymentMethodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDevicePaymentMethod()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DevicePaymentMethodEntity DevicePaymentMethod
		{
			get	{ return GetSingleDevicePaymentMethod(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDevicePaymentMethod(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SalesChannelToPaymentMethods", "DevicePaymentMethod", _devicePaymentMethod, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DevicePaymentMethod. When set to true, DevicePaymentMethod is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DevicePaymentMethod is accessed. You can always execute a forced fetch by calling GetSingleDevicePaymentMethod(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDevicePaymentMethod
		{
			get	{ return _alwaysFetchDevicePaymentMethod; }
			set	{ _alwaysFetchDevicePaymentMethod = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DevicePaymentMethod already has been fetched. Setting this property to false when DevicePaymentMethod has been fetched
		/// will set DevicePaymentMethod to null as well. Setting this property to true while DevicePaymentMethod hasn't been fetched disables lazy loading for DevicePaymentMethod</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDevicePaymentMethod
		{
			get { return _alreadyFetchedDevicePaymentMethod;}
			set 
			{
				if(_alreadyFetchedDevicePaymentMethod && !value)
				{
					this.DevicePaymentMethod = null;
				}
				_alreadyFetchedDevicePaymentMethod = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DevicePaymentMethod is not found
		/// in the database. When set to true, DevicePaymentMethod will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DevicePaymentMethodReturnsNewIfNotFound
		{
			get	{ return _devicePaymentMethodReturnsNewIfNotFound; }
			set { _devicePaymentMethodReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SalesChannelToPaymentMethodEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
