﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SurveyChoice'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SurveyChoiceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.SurveyAnswerCollection	_surveyAnswers;
		private bool	_alwaysFetchSurveyAnswers, _alreadyFetchedSurveyAnswers;
		private SurveyQuestionEntity _followUpQuestion;
		private bool	_alwaysFetchFollowUpQuestion, _alreadyFetchedFollowUpQuestion, _followUpQuestionReturnsNewIfNotFound;
		private SurveyQuestionEntity _surveyQuestion;
		private bool	_alwaysFetchSurveyQuestion, _alreadyFetchedSurveyQuestion, _surveyQuestionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name FollowUpQuestion</summary>
			public static readonly string FollowUpQuestion = "FollowUpQuestion";
			/// <summary>Member name SurveyQuestion</summary>
			public static readonly string SurveyQuestion = "SurveyQuestion";
			/// <summary>Member name SurveyAnswers</summary>
			public static readonly string SurveyAnswers = "SurveyAnswers";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SurveyChoiceEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SurveyChoiceEntity() :base("SurveyChoiceEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="surveyChoiceID">PK value for SurveyChoice which data should be fetched into this SurveyChoice object</param>
		public SurveyChoiceEntity(System.Int64 surveyChoiceID):base("SurveyChoiceEntity")
		{
			InitClassFetch(surveyChoiceID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyChoiceID">PK value for SurveyChoice which data should be fetched into this SurveyChoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SurveyChoiceEntity(System.Int64 surveyChoiceID, IPrefetchPath prefetchPathToUse):base("SurveyChoiceEntity")
		{
			InitClassFetch(surveyChoiceID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyChoiceID">PK value for SurveyChoice which data should be fetched into this SurveyChoice object</param>
		/// <param name="validator">The custom validator object for this SurveyChoiceEntity</param>
		public SurveyChoiceEntity(System.Int64 surveyChoiceID, IValidator validator):base("SurveyChoiceEntity")
		{
			InitClassFetch(surveyChoiceID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SurveyChoiceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_surveyAnswers = (VarioSL.Entities.CollectionClasses.SurveyAnswerCollection)info.GetValue("_surveyAnswers", typeof(VarioSL.Entities.CollectionClasses.SurveyAnswerCollection));
			_alwaysFetchSurveyAnswers = info.GetBoolean("_alwaysFetchSurveyAnswers");
			_alreadyFetchedSurveyAnswers = info.GetBoolean("_alreadyFetchedSurveyAnswers");
			_followUpQuestion = (SurveyQuestionEntity)info.GetValue("_followUpQuestion", typeof(SurveyQuestionEntity));
			if(_followUpQuestion!=null)
			{
				_followUpQuestion.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_followUpQuestionReturnsNewIfNotFound = info.GetBoolean("_followUpQuestionReturnsNewIfNotFound");
			_alwaysFetchFollowUpQuestion = info.GetBoolean("_alwaysFetchFollowUpQuestion");
			_alreadyFetchedFollowUpQuestion = info.GetBoolean("_alreadyFetchedFollowUpQuestion");

			_surveyQuestion = (SurveyQuestionEntity)info.GetValue("_surveyQuestion", typeof(SurveyQuestionEntity));
			if(_surveyQuestion!=null)
			{
				_surveyQuestion.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_surveyQuestionReturnsNewIfNotFound = info.GetBoolean("_surveyQuestionReturnsNewIfNotFound");
			_alwaysFetchSurveyQuestion = info.GetBoolean("_alwaysFetchSurveyQuestion");
			_alreadyFetchedSurveyQuestion = info.GetBoolean("_alreadyFetchedSurveyQuestion");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SurveyChoiceFieldIndex)fieldIndex)
			{
				case SurveyChoiceFieldIndex.FollowUpQuestionID:
					DesetupSyncFollowUpQuestion(true, false);
					_alreadyFetchedFollowUpQuestion = false;
					break;
				case SurveyChoiceFieldIndex.SurveyQuestionID:
					DesetupSyncSurveyQuestion(true, false);
					_alreadyFetchedSurveyQuestion = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSurveyAnswers = (_surveyAnswers.Count > 0);
			_alreadyFetchedFollowUpQuestion = (_followUpQuestion != null);
			_alreadyFetchedSurveyQuestion = (_surveyQuestion != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "FollowUpQuestion":
					toReturn.Add(Relations.SurveyQuestionEntityUsingFollowUpQuestionID);
					break;
				case "SurveyQuestion":
					toReturn.Add(Relations.SurveyQuestionEntityUsingSurveyQuestionID);
					break;
				case "SurveyAnswers":
					toReturn.Add(Relations.SurveyAnswerEntityUsingSurveyChoiceID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_surveyAnswers", (!this.MarkedForDeletion?_surveyAnswers:null));
			info.AddValue("_alwaysFetchSurveyAnswers", _alwaysFetchSurveyAnswers);
			info.AddValue("_alreadyFetchedSurveyAnswers", _alreadyFetchedSurveyAnswers);
			info.AddValue("_followUpQuestion", (!this.MarkedForDeletion?_followUpQuestion:null));
			info.AddValue("_followUpQuestionReturnsNewIfNotFound", _followUpQuestionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFollowUpQuestion", _alwaysFetchFollowUpQuestion);
			info.AddValue("_alreadyFetchedFollowUpQuestion", _alreadyFetchedFollowUpQuestion);
			info.AddValue("_surveyQuestion", (!this.MarkedForDeletion?_surveyQuestion:null));
			info.AddValue("_surveyQuestionReturnsNewIfNotFound", _surveyQuestionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSurveyQuestion", _alwaysFetchSurveyQuestion);
			info.AddValue("_alreadyFetchedSurveyQuestion", _alreadyFetchedSurveyQuestion);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "FollowUpQuestion":
					_alreadyFetchedFollowUpQuestion = true;
					this.FollowUpQuestion = (SurveyQuestionEntity)entity;
					break;
				case "SurveyQuestion":
					_alreadyFetchedSurveyQuestion = true;
					this.SurveyQuestion = (SurveyQuestionEntity)entity;
					break;
				case "SurveyAnswers":
					_alreadyFetchedSurveyAnswers = true;
					if(entity!=null)
					{
						this.SurveyAnswers.Add((SurveyAnswerEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "FollowUpQuestion":
					SetupSyncFollowUpQuestion(relatedEntity);
					break;
				case "SurveyQuestion":
					SetupSyncSurveyQuestion(relatedEntity);
					break;
				case "SurveyAnswers":
					_surveyAnswers.Add((SurveyAnswerEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "FollowUpQuestion":
					DesetupSyncFollowUpQuestion(false, true);
					break;
				case "SurveyQuestion":
					DesetupSyncSurveyQuestion(false, true);
					break;
				case "SurveyAnswers":
					this.PerformRelatedEntityRemoval(_surveyAnswers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_followUpQuestion!=null)
			{
				toReturn.Add(_followUpQuestion);
			}
			if(_surveyQuestion!=null)
			{
				toReturn.Add(_surveyQuestion);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_surveyAnswers);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyChoiceID">PK value for SurveyChoice which data should be fetched into this SurveyChoice object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyChoiceID)
		{
			return FetchUsingPK(surveyChoiceID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyChoiceID">PK value for SurveyChoice which data should be fetched into this SurveyChoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyChoiceID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(surveyChoiceID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyChoiceID">PK value for SurveyChoice which data should be fetched into this SurveyChoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyChoiceID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(surveyChoiceID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyChoiceID">PK value for SurveyChoice which data should be fetched into this SurveyChoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyChoiceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(surveyChoiceID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SurveyChoiceID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SurveyChoiceRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswers(bool forceFetch)
		{
			return GetMultiSurveyAnswers(forceFetch, _surveyAnswers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyAnswers(forceFetch, _surveyAnswers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyAnswers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyAnswers || forceFetch || _alwaysFetchSurveyAnswers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyAnswers);
				_surveyAnswers.SuppressClearInGetMulti=!forceFetch;
				_surveyAnswers.EntityFactoryToUse = entityFactoryToUse;
				_surveyAnswers.GetMultiManyToOne(this, null, null, filter);
				_surveyAnswers.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyAnswers = true;
			}
			return _surveyAnswers;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyAnswers'. These settings will be taken into account
		/// when the property SurveyAnswers is requested or GetMultiSurveyAnswers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyAnswers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyAnswers.SortClauses=sortClauses;
			_surveyAnswers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'SurveyQuestionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyQuestionEntity' which is related to this entity.</returns>
		public SurveyQuestionEntity GetSingleFollowUpQuestion()
		{
			return GetSingleFollowUpQuestion(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyQuestionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyQuestionEntity' which is related to this entity.</returns>
		public virtual SurveyQuestionEntity GetSingleFollowUpQuestion(bool forceFetch)
		{
			if( ( !_alreadyFetchedFollowUpQuestion || forceFetch || _alwaysFetchFollowUpQuestion) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyQuestionEntityUsingFollowUpQuestionID);
				SurveyQuestionEntity newEntity = new SurveyQuestionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FollowUpQuestionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SurveyQuestionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_followUpQuestionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FollowUpQuestion = newEntity;
				_alreadyFetchedFollowUpQuestion = fetchResult;
			}
			return _followUpQuestion;
		}


		/// <summary> Retrieves the related entity of type 'SurveyQuestionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyQuestionEntity' which is related to this entity.</returns>
		public SurveyQuestionEntity GetSingleSurveyQuestion()
		{
			return GetSingleSurveyQuestion(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyQuestionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyQuestionEntity' which is related to this entity.</returns>
		public virtual SurveyQuestionEntity GetSingleSurveyQuestion(bool forceFetch)
		{
			if( ( !_alreadyFetchedSurveyQuestion || forceFetch || _alwaysFetchSurveyQuestion) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyQuestionEntityUsingSurveyQuestionID);
				SurveyQuestionEntity newEntity = new SurveyQuestionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SurveyQuestionID);
				}
				if(fetchResult)
				{
					newEntity = (SurveyQuestionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_surveyQuestionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SurveyQuestion = newEntity;
				_alreadyFetchedSurveyQuestion = fetchResult;
			}
			return _surveyQuestion;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("FollowUpQuestion", _followUpQuestion);
			toReturn.Add("SurveyQuestion", _surveyQuestion);
			toReturn.Add("SurveyAnswers", _surveyAnswers);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="surveyChoiceID">PK value for SurveyChoice which data should be fetched into this SurveyChoice object</param>
		/// <param name="validator">The validator object for this SurveyChoiceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 surveyChoiceID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(surveyChoiceID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_surveyAnswers = new VarioSL.Entities.CollectionClasses.SurveyAnswerCollection();
			_surveyAnswers.SetContainingEntityInfo(this, "SurveyChoice");
			_followUpQuestionReturnsNewIfNotFound = false;
			_surveyQuestionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FollowUpQuestionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HasFreeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsRetired", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Language", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyChoiceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyQuestionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Text", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _followUpQuestion</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFollowUpQuestion(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _followUpQuestion, new PropertyChangedEventHandler( OnFollowUpQuestionPropertyChanged ), "FollowUpQuestion", VarioSL.Entities.RelationClasses.StaticSurveyChoiceRelations.SurveyQuestionEntityUsingFollowUpQuestionIDStatic, true, signalRelatedEntity, "FollowedUpChoices", resetFKFields, new int[] { (int)SurveyChoiceFieldIndex.FollowUpQuestionID } );		
			_followUpQuestion = null;
		}
		
		/// <summary> setups the sync logic for member _followUpQuestion</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFollowUpQuestion(IEntityCore relatedEntity)
		{
			if(_followUpQuestion!=relatedEntity)
			{		
				DesetupSyncFollowUpQuestion(true, true);
				_followUpQuestion = (SurveyQuestionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _followUpQuestion, new PropertyChangedEventHandler( OnFollowUpQuestionPropertyChanged ), "FollowUpQuestion", VarioSL.Entities.RelationClasses.StaticSurveyChoiceRelations.SurveyQuestionEntityUsingFollowUpQuestionIDStatic, true, ref _alreadyFetchedFollowUpQuestion, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFollowUpQuestionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _surveyQuestion</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSurveyQuestion(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _surveyQuestion, new PropertyChangedEventHandler( OnSurveyQuestionPropertyChanged ), "SurveyQuestion", VarioSL.Entities.RelationClasses.StaticSurveyChoiceRelations.SurveyQuestionEntityUsingSurveyQuestionIDStatic, true, signalRelatedEntity, "SurveyChoices", resetFKFields, new int[] { (int)SurveyChoiceFieldIndex.SurveyQuestionID } );		
			_surveyQuestion = null;
		}
		
		/// <summary> setups the sync logic for member _surveyQuestion</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSurveyQuestion(IEntityCore relatedEntity)
		{
			if(_surveyQuestion!=relatedEntity)
			{		
				DesetupSyncSurveyQuestion(true, true);
				_surveyQuestion = (SurveyQuestionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _surveyQuestion, new PropertyChangedEventHandler( OnSurveyQuestionPropertyChanged ), "SurveyQuestion", VarioSL.Entities.RelationClasses.StaticSurveyChoiceRelations.SurveyQuestionEntityUsingSurveyQuestionIDStatic, true, ref _alreadyFetchedSurveyQuestion, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSurveyQuestionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="surveyChoiceID">PK value for SurveyChoice which data should be fetched into this SurveyChoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 surveyChoiceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SurveyChoiceFieldIndex.SurveyChoiceID].ForcedCurrentValueWrite(surveyChoiceID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSurveyChoiceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SurveyChoiceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SurveyChoiceRelations Relations
		{
			get	{ return new SurveyChoiceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyAnswer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyAnswers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyAnswerCollection(), (IEntityRelation)GetRelationsForField("SurveyAnswers")[0], (int)VarioSL.Entities.EntityType.SurveyChoiceEntity, (int)VarioSL.Entities.EntityType.SurveyAnswerEntity, 0, null, null, null, "SurveyAnswers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestion'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFollowUpQuestion
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyQuestionCollection(), (IEntityRelation)GetRelationsForField("FollowUpQuestion")[0], (int)VarioSL.Entities.EntityType.SurveyChoiceEntity, (int)VarioSL.Entities.EntityType.SurveyQuestionEntity, 0, null, null, null, "FollowUpQuestion", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestion'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyQuestion
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyQuestionCollection(), (IEntityRelation)GetRelationsForField("SurveyQuestion")[0], (int)VarioSL.Entities.EntityType.SurveyChoiceEntity, (int)VarioSL.Entities.EntityType.SurveyQuestionEntity, 0, null, null, null, "SurveyQuestion", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The FollowUpQuestionID property of the Entity SurveyChoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYCHOICE"."FOLLOWUPQUESTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FollowUpQuestionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SurveyChoiceFieldIndex.FollowUpQuestionID, false); }
			set	{ SetValue((int)SurveyChoiceFieldIndex.FollowUpQuestionID, value, true); }
		}

		/// <summary> The HasFreeText property of the Entity SurveyChoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYCHOICE"."HASFREETEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HasFreeText
		{
			get { return (System.Boolean)GetValue((int)SurveyChoiceFieldIndex.HasFreeText, true); }
			set	{ SetValue((int)SurveyChoiceFieldIndex.HasFreeText, value, true); }
		}

		/// <summary> The IsRetired property of the Entity SurveyChoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYCHOICE"."ISRETIRED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsRetired
		{
			get { return (System.Boolean)GetValue((int)SurveyChoiceFieldIndex.IsRetired, true); }
			set	{ SetValue((int)SurveyChoiceFieldIndex.IsRetired, value, true); }
		}

		/// <summary> The Language property of the Entity SurveyChoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYCHOICE"."LANGUAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Language
		{
			get { return (System.String)GetValue((int)SurveyChoiceFieldIndex.Language, true); }
			set	{ SetValue((int)SurveyChoiceFieldIndex.Language, value, true); }
		}

		/// <summary> The LastModified property of the Entity SurveyChoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYCHOICE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SurveyChoiceFieldIndex.LastModified, true); }
			set	{ SetValue((int)SurveyChoiceFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity SurveyChoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYCHOICE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SurveyChoiceFieldIndex.LastUser, true); }
			set	{ SetValue((int)SurveyChoiceFieldIndex.LastUser, value, true); }
		}

		/// <summary> The SurveyChoiceID property of the Entity SurveyChoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYCHOICE"."SURVEYCHOICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SurveyChoiceID
		{
			get { return (System.Int64)GetValue((int)SurveyChoiceFieldIndex.SurveyChoiceID, true); }
			set	{ SetValue((int)SurveyChoiceFieldIndex.SurveyChoiceID, value, true); }
		}

		/// <summary> The SurveyQuestionID property of the Entity SurveyChoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYCHOICE"."SURVEYQUESTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SurveyQuestionID
		{
			get { return (System.Int64)GetValue((int)SurveyChoiceFieldIndex.SurveyQuestionID, true); }
			set	{ SetValue((int)SurveyChoiceFieldIndex.SurveyQuestionID, value, true); }
		}

		/// <summary> The Text property of the Entity SurveyChoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYCHOICE"."TEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Text
		{
			get { return (System.String)GetValue((int)SurveyChoiceFieldIndex.Text, true); }
			set	{ SetValue((int)SurveyChoiceFieldIndex.Text, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity SurveyChoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYCHOICE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SurveyChoiceFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SurveyChoiceFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyAnswers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SurveyAnswerCollection SurveyAnswers
		{
			get	{ return GetMultiSurveyAnswers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyAnswers. When set to true, SurveyAnswers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyAnswers is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyAnswers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyAnswers
		{
			get	{ return _alwaysFetchSurveyAnswers; }
			set	{ _alwaysFetchSurveyAnswers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyAnswers already has been fetched. Setting this property to false when SurveyAnswers has been fetched
		/// will clear the SurveyAnswers collection well. Setting this property to true while SurveyAnswers hasn't been fetched disables lazy loading for SurveyAnswers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyAnswers
		{
			get { return _alreadyFetchedSurveyAnswers;}
			set 
			{
				if(_alreadyFetchedSurveyAnswers && !value && (_surveyAnswers != null))
				{
					_surveyAnswers.Clear();
				}
				_alreadyFetchedSurveyAnswers = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'SurveyQuestionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFollowUpQuestion()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SurveyQuestionEntity FollowUpQuestion
		{
			get	{ return GetSingleFollowUpQuestion(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFollowUpQuestion(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FollowedUpChoices", "FollowUpQuestion", _followUpQuestion, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FollowUpQuestion. When set to true, FollowUpQuestion is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FollowUpQuestion is accessed. You can always execute a forced fetch by calling GetSingleFollowUpQuestion(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFollowUpQuestion
		{
			get	{ return _alwaysFetchFollowUpQuestion; }
			set	{ _alwaysFetchFollowUpQuestion = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FollowUpQuestion already has been fetched. Setting this property to false when FollowUpQuestion has been fetched
		/// will set FollowUpQuestion to null as well. Setting this property to true while FollowUpQuestion hasn't been fetched disables lazy loading for FollowUpQuestion</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFollowUpQuestion
		{
			get { return _alreadyFetchedFollowUpQuestion;}
			set 
			{
				if(_alreadyFetchedFollowUpQuestion && !value)
				{
					this.FollowUpQuestion = null;
				}
				_alreadyFetchedFollowUpQuestion = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FollowUpQuestion is not found
		/// in the database. When set to true, FollowUpQuestion will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FollowUpQuestionReturnsNewIfNotFound
		{
			get	{ return _followUpQuestionReturnsNewIfNotFound; }
			set { _followUpQuestionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SurveyQuestionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSurveyQuestion()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SurveyQuestionEntity SurveyQuestion
		{
			get	{ return GetSingleSurveyQuestion(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSurveyQuestion(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyChoices", "SurveyQuestion", _surveyQuestion, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyQuestion. When set to true, SurveyQuestion is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyQuestion is accessed. You can always execute a forced fetch by calling GetSingleSurveyQuestion(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyQuestion
		{
			get	{ return _alwaysFetchSurveyQuestion; }
			set	{ _alwaysFetchSurveyQuestion = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyQuestion already has been fetched. Setting this property to false when SurveyQuestion has been fetched
		/// will set SurveyQuestion to null as well. Setting this property to true while SurveyQuestion hasn't been fetched disables lazy loading for SurveyQuestion</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyQuestion
		{
			get { return _alreadyFetchedSurveyQuestion;}
			set 
			{
				if(_alreadyFetchedSurveyQuestion && !value)
				{
					this.SurveyQuestion = null;
				}
				_alreadyFetchedSurveyQuestion = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SurveyQuestion is not found
		/// in the database. When set to true, SurveyQuestion will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SurveyQuestionReturnsNewIfNotFound
		{
			get	{ return _surveyQuestionReturnsNewIfNotFound; }
			set { _surveyQuestionReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SurveyChoiceEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
