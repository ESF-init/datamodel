﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FareTableEntry'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FareTableEntryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.FareTableEntryCollection	_fareTableEntries;
		private bool	_alwaysFetchFareTableEntries, _alreadyFetchedFareTableEntries;
		private VarioSL.Entities.CollectionClasses.RuleCappingToFtEntryCollection	_ruleCappingToFtEntry;
		private bool	_alwaysFetchRuleCappingToFtEntry, _alreadyFetchedRuleCappingToFtEntry;
		private AttributeValueEntity _attributeValue;
		private bool	_alwaysFetchAttributeValue, _alreadyFetchedAttributeValue, _attributeValueReturnsNewIfNotFound;
		private FareTableEntity _fareTable;
		private bool	_alwaysFetchFareTable, _alreadyFetchedFareTable, _fareTableReturnsNewIfNotFound;
		private FareTableEntryEntity _fareTableEntry;
		private bool	_alwaysFetchFareTableEntry, _alreadyFetchedFareTableEntry, _fareTableEntryReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AttributeValue</summary>
			public static readonly string AttributeValue = "AttributeValue";
			/// <summary>Member name FareTable</summary>
			public static readonly string FareTable = "FareTable";
			/// <summary>Member name FareTableEntry</summary>
			public static readonly string FareTableEntry = "FareTableEntry";
			/// <summary>Member name FareTableEntries</summary>
			public static readonly string FareTableEntries = "FareTableEntries";
			/// <summary>Member name RuleCappingToFtEntry</summary>
			public static readonly string RuleCappingToFtEntry = "RuleCappingToFtEntry";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FareTableEntryEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FareTableEntryEntity() :base("FareTableEntryEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="fareTableEntryID">PK value for FareTableEntry which data should be fetched into this FareTableEntry object</param>
		public FareTableEntryEntity(System.Int64 fareTableEntryID):base("FareTableEntryEntity")
		{
			InitClassFetch(fareTableEntryID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="fareTableEntryID">PK value for FareTableEntry which data should be fetched into this FareTableEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FareTableEntryEntity(System.Int64 fareTableEntryID, IPrefetchPath prefetchPathToUse):base("FareTableEntryEntity")
		{
			InitClassFetch(fareTableEntryID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="fareTableEntryID">PK value for FareTableEntry which data should be fetched into this FareTableEntry object</param>
		/// <param name="validator">The custom validator object for this FareTableEntryEntity</param>
		public FareTableEntryEntity(System.Int64 fareTableEntryID, IValidator validator):base("FareTableEntryEntity")
		{
			InitClassFetch(fareTableEntryID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FareTableEntryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_fareTableEntries = (VarioSL.Entities.CollectionClasses.FareTableEntryCollection)info.GetValue("_fareTableEntries", typeof(VarioSL.Entities.CollectionClasses.FareTableEntryCollection));
			_alwaysFetchFareTableEntries = info.GetBoolean("_alwaysFetchFareTableEntries");
			_alreadyFetchedFareTableEntries = info.GetBoolean("_alreadyFetchedFareTableEntries");

			_ruleCappingToFtEntry = (VarioSL.Entities.CollectionClasses.RuleCappingToFtEntryCollection)info.GetValue("_ruleCappingToFtEntry", typeof(VarioSL.Entities.CollectionClasses.RuleCappingToFtEntryCollection));
			_alwaysFetchRuleCappingToFtEntry = info.GetBoolean("_alwaysFetchRuleCappingToFtEntry");
			_alreadyFetchedRuleCappingToFtEntry = info.GetBoolean("_alreadyFetchedRuleCappingToFtEntry");
			_attributeValue = (AttributeValueEntity)info.GetValue("_attributeValue", typeof(AttributeValueEntity));
			if(_attributeValue!=null)
			{
				_attributeValue.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributeValueReturnsNewIfNotFound = info.GetBoolean("_attributeValueReturnsNewIfNotFound");
			_alwaysFetchAttributeValue = info.GetBoolean("_alwaysFetchAttributeValue");
			_alreadyFetchedAttributeValue = info.GetBoolean("_alreadyFetchedAttributeValue");

			_fareTable = (FareTableEntity)info.GetValue("_fareTable", typeof(FareTableEntity));
			if(_fareTable!=null)
			{
				_fareTable.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareTableReturnsNewIfNotFound = info.GetBoolean("_fareTableReturnsNewIfNotFound");
			_alwaysFetchFareTable = info.GetBoolean("_alwaysFetchFareTable");
			_alreadyFetchedFareTable = info.GetBoolean("_alreadyFetchedFareTable");

			_fareTableEntry = (FareTableEntryEntity)info.GetValue("_fareTableEntry", typeof(FareTableEntryEntity));
			if(_fareTableEntry!=null)
			{
				_fareTableEntry.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareTableEntryReturnsNewIfNotFound = info.GetBoolean("_fareTableEntryReturnsNewIfNotFound");
			_alwaysFetchFareTableEntry = info.GetBoolean("_alwaysFetchFareTableEntry");
			_alreadyFetchedFareTableEntry = info.GetBoolean("_alreadyFetchedFareTableEntry");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FareTableEntryFieldIndex)fieldIndex)
			{
				case FareTableEntryFieldIndex.AttributeValueID:
					DesetupSyncAttributeValue(true, false);
					_alreadyFetchedAttributeValue = false;
					break;
				case FareTableEntryFieldIndex.FareTableID:
					DesetupSyncFareTable(true, false);
					_alreadyFetchedFareTable = false;
					break;
				case FareTableEntryFieldIndex.MasterFareTableEntryID:
					DesetupSyncFareTableEntry(true, false);
					_alreadyFetchedFareTableEntry = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFareTableEntries = (_fareTableEntries.Count > 0);
			_alreadyFetchedRuleCappingToFtEntry = (_ruleCappingToFtEntry.Count > 0);
			_alreadyFetchedAttributeValue = (_attributeValue != null);
			_alreadyFetchedFareTable = (_fareTable != null);
			_alreadyFetchedFareTableEntry = (_fareTableEntry != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AttributeValue":
					toReturn.Add(Relations.AttributeValueEntityUsingAttributeValueID);
					break;
				case "FareTable":
					toReturn.Add(Relations.FareTableEntityUsingFareTableID);
					break;
				case "FareTableEntry":
					toReturn.Add(Relations.FareTableEntryEntityUsingFareTableEntryIDMasterFareTableEntryID);
					break;
				case "FareTableEntries":
					toReturn.Add(Relations.FareTableEntryEntityUsingMasterFareTableEntryID);
					break;
				case "RuleCappingToFtEntry":
					toReturn.Add(Relations.RuleCappingToFtEntryEntityUsingFareTableEntryID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_fareTableEntries", (!this.MarkedForDeletion?_fareTableEntries:null));
			info.AddValue("_alwaysFetchFareTableEntries", _alwaysFetchFareTableEntries);
			info.AddValue("_alreadyFetchedFareTableEntries", _alreadyFetchedFareTableEntries);
			info.AddValue("_ruleCappingToFtEntry", (!this.MarkedForDeletion?_ruleCappingToFtEntry:null));
			info.AddValue("_alwaysFetchRuleCappingToFtEntry", _alwaysFetchRuleCappingToFtEntry);
			info.AddValue("_alreadyFetchedRuleCappingToFtEntry", _alreadyFetchedRuleCappingToFtEntry);
			info.AddValue("_attributeValue", (!this.MarkedForDeletion?_attributeValue:null));
			info.AddValue("_attributeValueReturnsNewIfNotFound", _attributeValueReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttributeValue", _alwaysFetchAttributeValue);
			info.AddValue("_alreadyFetchedAttributeValue", _alreadyFetchedAttributeValue);
			info.AddValue("_fareTable", (!this.MarkedForDeletion?_fareTable:null));
			info.AddValue("_fareTableReturnsNewIfNotFound", _fareTableReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareTable", _alwaysFetchFareTable);
			info.AddValue("_alreadyFetchedFareTable", _alreadyFetchedFareTable);
			info.AddValue("_fareTableEntry", (!this.MarkedForDeletion?_fareTableEntry:null));
			info.AddValue("_fareTableEntryReturnsNewIfNotFound", _fareTableEntryReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareTableEntry", _alwaysFetchFareTableEntry);
			info.AddValue("_alreadyFetchedFareTableEntry", _alreadyFetchedFareTableEntry);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AttributeValue":
					_alreadyFetchedAttributeValue = true;
					this.AttributeValue = (AttributeValueEntity)entity;
					break;
				case "FareTable":
					_alreadyFetchedFareTable = true;
					this.FareTable = (FareTableEntity)entity;
					break;
				case "FareTableEntry":
					_alreadyFetchedFareTableEntry = true;
					this.FareTableEntry = (FareTableEntryEntity)entity;
					break;
				case "FareTableEntries":
					_alreadyFetchedFareTableEntries = true;
					if(entity!=null)
					{
						this.FareTableEntries.Add((FareTableEntryEntity)entity);
					}
					break;
				case "RuleCappingToFtEntry":
					_alreadyFetchedRuleCappingToFtEntry = true;
					if(entity!=null)
					{
						this.RuleCappingToFtEntry.Add((RuleCappingToFtEntryEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AttributeValue":
					SetupSyncAttributeValue(relatedEntity);
					break;
				case "FareTable":
					SetupSyncFareTable(relatedEntity);
					break;
				case "FareTableEntry":
					SetupSyncFareTableEntry(relatedEntity);
					break;
				case "FareTableEntries":
					_fareTableEntries.Add((FareTableEntryEntity)relatedEntity);
					break;
				case "RuleCappingToFtEntry":
					_ruleCappingToFtEntry.Add((RuleCappingToFtEntryEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AttributeValue":
					DesetupSyncAttributeValue(false, true);
					break;
				case "FareTable":
					DesetupSyncFareTable(false, true);
					break;
				case "FareTableEntry":
					DesetupSyncFareTableEntry(false, true);
					break;
				case "FareTableEntries":
					this.PerformRelatedEntityRemoval(_fareTableEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RuleCappingToFtEntry":
					this.PerformRelatedEntityRemoval(_ruleCappingToFtEntry, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_attributeValue!=null)
			{
				toReturn.Add(_attributeValue);
			}
			if(_fareTable!=null)
			{
				toReturn.Add(_fareTable);
			}
			if(_fareTableEntry!=null)
			{
				toReturn.Add(_fareTableEntry);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_fareTableEntries);
			toReturn.Add(_ruleCappingToFtEntry);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareTableEntryID">PK value for FareTableEntry which data should be fetched into this FareTableEntry object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareTableEntryID)
		{
			return FetchUsingPK(fareTableEntryID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareTableEntryID">PK value for FareTableEntry which data should be fetched into this FareTableEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareTableEntryID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(fareTableEntryID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareTableEntryID">PK value for FareTableEntry which data should be fetched into this FareTableEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareTableEntryID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(fareTableEntryID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareTableEntryID">PK value for FareTableEntry which data should be fetched into this FareTableEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareTableEntryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(fareTableEntryID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FareTableEntryID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FareTableEntryRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareTableEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareTableEntryCollection GetMultiFareTableEntries(bool forceFetch)
		{
			return GetMultiFareTableEntries(forceFetch, _fareTableEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareTableEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareTableEntryCollection GetMultiFareTableEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareTableEntries(forceFetch, _fareTableEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareTableEntryCollection GetMultiFareTableEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareTableEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareTableEntryCollection GetMultiFareTableEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareTableEntries || forceFetch || _alwaysFetchFareTableEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareTableEntries);
				_fareTableEntries.SuppressClearInGetMulti=!forceFetch;
				_fareTableEntries.EntityFactoryToUse = entityFactoryToUse;
				_fareTableEntries.GetMultiManyToOne(null, null, this, filter);
				_fareTableEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedFareTableEntries = true;
			}
			return _fareTableEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareTableEntries'. These settings will be taken into account
		/// when the property FareTableEntries is requested or GetMultiFareTableEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareTableEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareTableEntries.SortClauses=sortClauses;
			_fareTableEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToFtEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingToFtEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingToFtEntryCollection GetMultiRuleCappingToFtEntry(bool forceFetch)
		{
			return GetMultiRuleCappingToFtEntry(forceFetch, _ruleCappingToFtEntry.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToFtEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingToFtEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingToFtEntryCollection GetMultiRuleCappingToFtEntry(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleCappingToFtEntry(forceFetch, _ruleCappingToFtEntry.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToFtEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingToFtEntryCollection GetMultiRuleCappingToFtEntry(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleCappingToFtEntry(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToFtEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingToFtEntryCollection GetMultiRuleCappingToFtEntry(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleCappingToFtEntry || forceFetch || _alwaysFetchRuleCappingToFtEntry) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleCappingToFtEntry);
				_ruleCappingToFtEntry.SuppressClearInGetMulti=!forceFetch;
				_ruleCappingToFtEntry.EntityFactoryToUse = entityFactoryToUse;
				_ruleCappingToFtEntry.GetMultiManyToOne(this, filter);
				_ruleCappingToFtEntry.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleCappingToFtEntry = true;
			}
			return _ruleCappingToFtEntry;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleCappingToFtEntry'. These settings will be taken into account
		/// when the property RuleCappingToFtEntry is requested or GetMultiRuleCappingToFtEntry is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleCappingToFtEntry(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleCappingToFtEntry.SortClauses=sortClauses;
			_ruleCappingToFtEntry.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleAttributeValue()
		{
			return GetSingleAttributeValue(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleAttributeValue(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttributeValue || forceFetch || _alwaysFetchAttributeValue) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingAttributeValueID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AttributeValueID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributeValueReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AttributeValue = newEntity;
				_alreadyFetchedAttributeValue = fetchResult;
			}
			return _attributeValue;
		}


		/// <summary> Retrieves the related entity of type 'FareTableEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareTableEntity' which is related to this entity.</returns>
		public FareTableEntity GetSingleFareTable()
		{
			return GetSingleFareTable(false);
		}

		/// <summary> Retrieves the related entity of type 'FareTableEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareTableEntity' which is related to this entity.</returns>
		public virtual FareTableEntity GetSingleFareTable(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareTable || forceFetch || _alwaysFetchFareTable) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareTableEntityUsingFareTableID);
				FareTableEntity newEntity = new FareTableEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareTableID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareTableEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareTableReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareTable = newEntity;
				_alreadyFetchedFareTable = fetchResult;
			}
			return _fareTable;
		}


		/// <summary> Retrieves the related entity of type 'FareTableEntryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareTableEntryEntity' which is related to this entity.</returns>
		public FareTableEntryEntity GetSingleFareTableEntry()
		{
			return GetSingleFareTableEntry(false);
		}

		/// <summary> Retrieves the related entity of type 'FareTableEntryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareTableEntryEntity' which is related to this entity.</returns>
		public virtual FareTableEntryEntity GetSingleFareTableEntry(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareTableEntry || forceFetch || _alwaysFetchFareTableEntry) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareTableEntryEntityUsingFareTableEntryIDMasterFareTableEntryID);
				FareTableEntryEntity newEntity = new FareTableEntryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MasterFareTableEntryID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareTableEntryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareTableEntryReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareTableEntry = newEntity;
				_alreadyFetchedFareTableEntry = fetchResult;
			}
			return _fareTableEntry;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AttributeValue", _attributeValue);
			toReturn.Add("FareTable", _fareTable);
			toReturn.Add("FareTableEntry", _fareTableEntry);
			toReturn.Add("FareTableEntries", _fareTableEntries);
			toReturn.Add("RuleCappingToFtEntry", _ruleCappingToFtEntry);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="fareTableEntryID">PK value for FareTableEntry which data should be fetched into this FareTableEntry object</param>
		/// <param name="validator">The validator object for this FareTableEntryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 fareTableEntryID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(fareTableEntryID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_fareTableEntries = new VarioSL.Entities.CollectionClasses.FareTableEntryCollection();
			_fareTableEntries.SetContainingEntityInfo(this, "FareTableEntry");

			_ruleCappingToFtEntry = new VarioSL.Entities.CollectionClasses.RuleCappingToFtEntryCollection();
			_ruleCappingToFtEntry.SetContainingEntityInfo(this, "FareTableEntry");
			_attributeValueReturnsNewIfNotFound = false;
			_fareTableReturnsNewIfNotFound = false;
			_fareTableEntryReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttributeValueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BaseFare", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Distance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Fare1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Fare2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareTableEntryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareTableID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MasterFareTableEntryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RulePeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShortCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidityPeriod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidityText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Vat", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _attributeValue</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttributeValue(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attributeValue, new PropertyChangedEventHandler( OnAttributeValuePropertyChanged ), "AttributeValue", VarioSL.Entities.RelationClasses.StaticFareTableEntryRelations.AttributeValueEntityUsingAttributeValueIDStatic, true, signalRelatedEntity, "FareTableEntries", resetFKFields, new int[] { (int)FareTableEntryFieldIndex.AttributeValueID } );		
			_attributeValue = null;
		}
		
		/// <summary> setups the sync logic for member _attributeValue</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttributeValue(IEntityCore relatedEntity)
		{
			if(_attributeValue!=relatedEntity)
			{		
				DesetupSyncAttributeValue(true, true);
				_attributeValue = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attributeValue, new PropertyChangedEventHandler( OnAttributeValuePropertyChanged ), "AttributeValue", VarioSL.Entities.RelationClasses.StaticFareTableEntryRelations.AttributeValueEntityUsingAttributeValueIDStatic, true, ref _alreadyFetchedAttributeValue, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributeValuePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareTable</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareTable(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareTable, new PropertyChangedEventHandler( OnFareTablePropertyChanged ), "FareTable", VarioSL.Entities.RelationClasses.StaticFareTableEntryRelations.FareTableEntityUsingFareTableIDStatic, true, signalRelatedEntity, "FareTableEntries", resetFKFields, new int[] { (int)FareTableEntryFieldIndex.FareTableID } );		
			_fareTable = null;
		}
		
		/// <summary> setups the sync logic for member _fareTable</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareTable(IEntityCore relatedEntity)
		{
			if(_fareTable!=relatedEntity)
			{		
				DesetupSyncFareTable(true, true);
				_fareTable = (FareTableEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareTable, new PropertyChangedEventHandler( OnFareTablePropertyChanged ), "FareTable", VarioSL.Entities.RelationClasses.StaticFareTableEntryRelations.FareTableEntityUsingFareTableIDStatic, true, ref _alreadyFetchedFareTable, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareTablePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareTableEntry</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareTableEntry(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareTableEntry, new PropertyChangedEventHandler( OnFareTableEntryPropertyChanged ), "FareTableEntry", VarioSL.Entities.RelationClasses.StaticFareTableEntryRelations.FareTableEntryEntityUsingFareTableEntryIDMasterFareTableEntryIDStatic, true, signalRelatedEntity, "FareTableEntries", resetFKFields, new int[] { (int)FareTableEntryFieldIndex.MasterFareTableEntryID } );		
			_fareTableEntry = null;
		}
		
		/// <summary> setups the sync logic for member _fareTableEntry</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareTableEntry(IEntityCore relatedEntity)
		{
			if(_fareTableEntry!=relatedEntity)
			{		
				DesetupSyncFareTableEntry(true, true);
				_fareTableEntry = (FareTableEntryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareTableEntry, new PropertyChangedEventHandler( OnFareTableEntryPropertyChanged ), "FareTableEntry", VarioSL.Entities.RelationClasses.StaticFareTableEntryRelations.FareTableEntryEntityUsingFareTableEntryIDMasterFareTableEntryIDStatic, true, ref _alreadyFetchedFareTableEntry, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareTableEntryPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="fareTableEntryID">PK value for FareTableEntry which data should be fetched into this FareTableEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 fareTableEntryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FareTableEntryFieldIndex.FareTableEntryID].ForcedCurrentValueWrite(fareTableEntryID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFareTableEntryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FareTableEntryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FareTableEntryRelations Relations
		{
			get	{ return new FareTableEntryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareTableEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareTableEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareTableEntryCollection(), (IEntityRelation)GetRelationsForField("FareTableEntries")[0], (int)VarioSL.Entities.EntityType.FareTableEntryEntity, (int)VarioSL.Entities.EntityType.FareTableEntryEntity, 0, null, null, null, "FareTableEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleCappingToFtEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleCappingToFtEntry
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleCappingToFtEntryCollection(), (IEntityRelation)GetRelationsForField("RuleCappingToFtEntry")[0], (int)VarioSL.Entities.EntityType.FareTableEntryEntity, (int)VarioSL.Entities.EntityType.RuleCappingToFtEntryEntity, 0, null, null, null, "RuleCappingToFtEntry", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValue
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("AttributeValue")[0], (int)VarioSL.Entities.EntityType.FareTableEntryEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "AttributeValue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareTable'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareTable
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareTableCollection(), (IEntityRelation)GetRelationsForField("FareTable")[0], (int)VarioSL.Entities.EntityType.FareTableEntryEntity, (int)VarioSL.Entities.EntityType.FareTableEntity, 0, null, null, null, "FareTable", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareTableEntry'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareTableEntry
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareTableEntryCollection(), (IEntityRelation)GetRelationsForField("FareTableEntry")[0], (int)VarioSL.Entities.EntityType.FareTableEntryEntity, (int)VarioSL.Entities.EntityType.FareTableEntryEntity, 0, null, null, null, "FareTableEntry", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AttributeValueID property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."ATTRIBUTEVALUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AttributeValueID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareTableEntryFieldIndex.AttributeValueID, false); }
			set	{ SetValue((int)FareTableEntryFieldIndex.AttributeValueID, value, true); }
		}

		/// <summary> The BaseFare property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."BASEFARE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BaseFare
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareTableEntryFieldIndex.BaseFare, false); }
			set	{ SetValue((int)FareTableEntryFieldIndex.BaseFare, value, true); }
		}

		/// <summary> The Distance property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."DISTANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Distance
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareTableEntryFieldIndex.Distance, false); }
			set	{ SetValue((int)FareTableEntryFieldIndex.Distance, value, true); }
		}

		/// <summary> The ExternalName property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."EXTERNALNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalName
		{
			get { return (System.String)GetValue((int)FareTableEntryFieldIndex.ExternalName, true); }
			set	{ SetValue((int)FareTableEntryFieldIndex.ExternalName, value, true); }
		}

		/// <summary> The Fare1 property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."FARE1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Fare1
		{
			get { return (System.Int64)GetValue((int)FareTableEntryFieldIndex.Fare1, true); }
			set	{ SetValue((int)FareTableEntryFieldIndex.Fare1, value, true); }
		}

		/// <summary> The Fare2 property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."FARE2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Fare2
		{
			get { return (System.Int64)GetValue((int)FareTableEntryFieldIndex.Fare2, true); }
			set	{ SetValue((int)FareTableEntryFieldIndex.Fare2, value, true); }
		}

		/// <summary> The FareTableEntryID property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."FARETABLEENTRYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 FareTableEntryID
		{
			get { return (System.Int64)GetValue((int)FareTableEntryFieldIndex.FareTableEntryID, true); }
			set	{ SetValue((int)FareTableEntryFieldIndex.FareTableEntryID, value, true); }
		}

		/// <summary> The FareTableID property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."FARETABLEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareTableID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareTableEntryFieldIndex.FareTableID, false); }
			set	{ SetValue((int)FareTableEntryFieldIndex.FareTableID, value, true); }
		}

		/// <summary> The MasterFareTableEntryID property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."MASTERFARETABLEENTRYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MasterFareTableEntryID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareTableEntryFieldIndex.MasterFareTableEntryID, false); }
			set	{ SetValue((int)FareTableEntryFieldIndex.MasterFareTableEntryID, value, true); }
		}

		/// <summary> The RulePeriodID property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."RULEPERIODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RulePeriodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareTableEntryFieldIndex.RulePeriodID, false); }
			set	{ SetValue((int)FareTableEntryFieldIndex.RulePeriodID, value, true); }
		}

		/// <summary> The ShortCode property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."SHORTCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ShortCode
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareTableEntryFieldIndex.ShortCode, false); }
			set	{ SetValue((int)FareTableEntryFieldIndex.ShortCode, value, true); }
		}

		/// <summary> The ValidityPeriod property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."VALIDITYPERIOD"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ValidityPeriod
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareTableEntryFieldIndex.ValidityPeriod, false); }
			set	{ SetValue((int)FareTableEntryFieldIndex.ValidityPeriod, value, true); }
		}

		/// <summary> The ValidityText property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."VALIDITYTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ValidityText
		{
			get { return (System.String)GetValue((int)FareTableEntryFieldIndex.ValidityText, true); }
			set	{ SetValue((int)FareTableEntryFieldIndex.ValidityText, value, true); }
		}

		/// <summary> The Vat property of the Entity FareTableEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLEENTRY"."VAT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> Vat
		{
			get { return (Nullable<System.Double>)GetValue((int)FareTableEntryFieldIndex.Vat, false); }
			set	{ SetValue((int)FareTableEntryFieldIndex.Vat, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareTableEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareTableEntryCollection FareTableEntries
		{
			get	{ return GetMultiFareTableEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareTableEntries. When set to true, FareTableEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareTableEntries is accessed. You can always execute/ a forced fetch by calling GetMultiFareTableEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareTableEntries
		{
			get	{ return _alwaysFetchFareTableEntries; }
			set	{ _alwaysFetchFareTableEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareTableEntries already has been fetched. Setting this property to false when FareTableEntries has been fetched
		/// will clear the FareTableEntries collection well. Setting this property to true while FareTableEntries hasn't been fetched disables lazy loading for FareTableEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareTableEntries
		{
			get { return _alreadyFetchedFareTableEntries;}
			set 
			{
				if(_alreadyFetchedFareTableEntries && !value && (_fareTableEntries != null))
				{
					_fareTableEntries.Clear();
				}
				_alreadyFetchedFareTableEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RuleCappingToFtEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleCappingToFtEntry()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingToFtEntryCollection RuleCappingToFtEntry
		{
			get	{ return GetMultiRuleCappingToFtEntry(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleCappingToFtEntry. When set to true, RuleCappingToFtEntry is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleCappingToFtEntry is accessed. You can always execute/ a forced fetch by calling GetMultiRuleCappingToFtEntry(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleCappingToFtEntry
		{
			get	{ return _alwaysFetchRuleCappingToFtEntry; }
			set	{ _alwaysFetchRuleCappingToFtEntry = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleCappingToFtEntry already has been fetched. Setting this property to false when RuleCappingToFtEntry has been fetched
		/// will clear the RuleCappingToFtEntry collection well. Setting this property to true while RuleCappingToFtEntry hasn't been fetched disables lazy loading for RuleCappingToFtEntry</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleCappingToFtEntry
		{
			get { return _alreadyFetchedRuleCappingToFtEntry;}
			set 
			{
				if(_alreadyFetchedRuleCappingToFtEntry && !value && (_ruleCappingToFtEntry != null))
				{
					_ruleCappingToFtEntry.Clear();
				}
				_alreadyFetchedRuleCappingToFtEntry = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttributeValue()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity AttributeValue
		{
			get	{ return GetSingleAttributeValue(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttributeValue(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareTableEntries", "AttributeValue", _attributeValue, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValue. When set to true, AttributeValue is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValue is accessed. You can always execute a forced fetch by calling GetSingleAttributeValue(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValue
		{
			get	{ return _alwaysFetchAttributeValue; }
			set	{ _alwaysFetchAttributeValue = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValue already has been fetched. Setting this property to false when AttributeValue has been fetched
		/// will set AttributeValue to null as well. Setting this property to true while AttributeValue hasn't been fetched disables lazy loading for AttributeValue</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValue
		{
			get { return _alreadyFetchedAttributeValue;}
			set 
			{
				if(_alreadyFetchedAttributeValue && !value)
				{
					this.AttributeValue = null;
				}
				_alreadyFetchedAttributeValue = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AttributeValue is not found
		/// in the database. When set to true, AttributeValue will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributeValueReturnsNewIfNotFound
		{
			get	{ return _attributeValueReturnsNewIfNotFound; }
			set { _attributeValueReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareTableEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareTable()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareTableEntity FareTable
		{
			get	{ return GetSingleFareTable(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareTable(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareTableEntries", "FareTable", _fareTable, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareTable. When set to true, FareTable is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareTable is accessed. You can always execute a forced fetch by calling GetSingleFareTable(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareTable
		{
			get	{ return _alwaysFetchFareTable; }
			set	{ _alwaysFetchFareTable = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareTable already has been fetched. Setting this property to false when FareTable has been fetched
		/// will set FareTable to null as well. Setting this property to true while FareTable hasn't been fetched disables lazy loading for FareTable</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareTable
		{
			get { return _alreadyFetchedFareTable;}
			set 
			{
				if(_alreadyFetchedFareTable && !value)
				{
					this.FareTable = null;
				}
				_alreadyFetchedFareTable = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareTable is not found
		/// in the database. When set to true, FareTable will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareTableReturnsNewIfNotFound
		{
			get	{ return _fareTableReturnsNewIfNotFound; }
			set { _fareTableReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareTableEntryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareTableEntry()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareTableEntryEntity FareTableEntry
		{
			get	{ return GetSingleFareTableEntry(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareTableEntry(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareTableEntries", "FareTableEntry", _fareTableEntry, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareTableEntry. When set to true, FareTableEntry is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareTableEntry is accessed. You can always execute a forced fetch by calling GetSingleFareTableEntry(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareTableEntry
		{
			get	{ return _alwaysFetchFareTableEntry; }
			set	{ _alwaysFetchFareTableEntry = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareTableEntry already has been fetched. Setting this property to false when FareTableEntry has been fetched
		/// will set FareTableEntry to null as well. Setting this property to true while FareTableEntry hasn't been fetched disables lazy loading for FareTableEntry</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareTableEntry
		{
			get { return _alreadyFetchedFareTableEntry;}
			set 
			{
				if(_alreadyFetchedFareTableEntry && !value)
				{
					this.FareTableEntry = null;
				}
				_alreadyFetchedFareTableEntry = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareTableEntry is not found
		/// in the database. When set to true, FareTableEntry will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareTableEntryReturnsNewIfNotFound
		{
			get	{ return _fareTableEntryReturnsNewIfNotFound; }
			set { _fareTableEntryReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FareTableEntryEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
