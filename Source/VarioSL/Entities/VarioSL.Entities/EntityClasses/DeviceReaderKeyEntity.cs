﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DeviceReaderKey'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DeviceReaderKeyEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection	_deviceReaderKeyToCerts;
		private bool	_alwaysFetchDeviceReaderKeyToCerts, _alreadyFetchedDeviceReaderKeyToCerts;
		private DeviceReaderEntity _deviceReader;
		private bool	_alwaysFetchDeviceReader, _alreadyFetchedDeviceReader, _deviceReaderReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeviceReader</summary>
			public static readonly string DeviceReader = "DeviceReader";
			/// <summary>Member name DeviceReaderKeyToCerts</summary>
			public static readonly string DeviceReaderKeyToCerts = "DeviceReaderKeyToCerts";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeviceReaderKeyEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DeviceReaderKeyEntity() :base("DeviceReaderKeyEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="deviceReaderKeyID">PK value for DeviceReaderKey which data should be fetched into this DeviceReaderKey object</param>
		public DeviceReaderKeyEntity(System.Int64 deviceReaderKeyID):base("DeviceReaderKeyEntity")
		{
			InitClassFetch(deviceReaderKeyID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceReaderKeyID">PK value for DeviceReaderKey which data should be fetched into this DeviceReaderKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeviceReaderKeyEntity(System.Int64 deviceReaderKeyID, IPrefetchPath prefetchPathToUse):base("DeviceReaderKeyEntity")
		{
			InitClassFetch(deviceReaderKeyID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceReaderKeyID">PK value for DeviceReaderKey which data should be fetched into this DeviceReaderKey object</param>
		/// <param name="validator">The custom validator object for this DeviceReaderKeyEntity</param>
		public DeviceReaderKeyEntity(System.Int64 deviceReaderKeyID, IValidator validator):base("DeviceReaderKeyEntity")
		{
			InitClassFetch(deviceReaderKeyID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeviceReaderKeyEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_deviceReaderKeyToCerts = (VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection)info.GetValue("_deviceReaderKeyToCerts", typeof(VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection));
			_alwaysFetchDeviceReaderKeyToCerts = info.GetBoolean("_alwaysFetchDeviceReaderKeyToCerts");
			_alreadyFetchedDeviceReaderKeyToCerts = info.GetBoolean("_alreadyFetchedDeviceReaderKeyToCerts");
			_deviceReader = (DeviceReaderEntity)info.GetValue("_deviceReader", typeof(DeviceReaderEntity));
			if(_deviceReader!=null)
			{
				_deviceReader.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceReaderReturnsNewIfNotFound = info.GetBoolean("_deviceReaderReturnsNewIfNotFound");
			_alwaysFetchDeviceReader = info.GetBoolean("_alwaysFetchDeviceReader");
			_alreadyFetchedDeviceReader = info.GetBoolean("_alreadyFetchedDeviceReader");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DeviceReaderKeyFieldIndex)fieldIndex)
			{
				case DeviceReaderKeyFieldIndex.DeviceReaderID:
					DesetupSyncDeviceReader(true, false);
					_alreadyFetchedDeviceReader = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDeviceReaderKeyToCerts = (_deviceReaderKeyToCerts.Count > 0);
			_alreadyFetchedDeviceReader = (_deviceReader != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeviceReader":
					toReturn.Add(Relations.DeviceReaderEntityUsingDeviceReaderID);
					break;
				case "DeviceReaderKeyToCerts":
					toReturn.Add(Relations.DeviceReaderKeyToCertEntityUsingDeviceReaderKeyID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_deviceReaderKeyToCerts", (!this.MarkedForDeletion?_deviceReaderKeyToCerts:null));
			info.AddValue("_alwaysFetchDeviceReaderKeyToCerts", _alwaysFetchDeviceReaderKeyToCerts);
			info.AddValue("_alreadyFetchedDeviceReaderKeyToCerts", _alreadyFetchedDeviceReaderKeyToCerts);
			info.AddValue("_deviceReader", (!this.MarkedForDeletion?_deviceReader:null));
			info.AddValue("_deviceReaderReturnsNewIfNotFound", _deviceReaderReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceReader", _alwaysFetchDeviceReader);
			info.AddValue("_alreadyFetchedDeviceReader", _alreadyFetchedDeviceReader);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeviceReader":
					_alreadyFetchedDeviceReader = true;
					this.DeviceReader = (DeviceReaderEntity)entity;
					break;
				case "DeviceReaderKeyToCerts":
					_alreadyFetchedDeviceReaderKeyToCerts = true;
					if(entity!=null)
					{
						this.DeviceReaderKeyToCerts.Add((DeviceReaderKeyToCertEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeviceReader":
					SetupSyncDeviceReader(relatedEntity);
					break;
				case "DeviceReaderKeyToCerts":
					_deviceReaderKeyToCerts.Add((DeviceReaderKeyToCertEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeviceReader":
					DesetupSyncDeviceReader(false, true);
					break;
				case "DeviceReaderKeyToCerts":
					this.PerformRelatedEntityRemoval(_deviceReaderKeyToCerts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_deviceReader!=null)
			{
				toReturn.Add(_deviceReader);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_deviceReaderKeyToCerts);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderKeyID">PK value for DeviceReaderKey which data should be fetched into this DeviceReaderKey object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderKeyID)
		{
			return FetchUsingPK(deviceReaderKeyID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderKeyID">PK value for DeviceReaderKey which data should be fetched into this DeviceReaderKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderKeyID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deviceReaderKeyID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderKeyID">PK value for DeviceReaderKey which data should be fetched into this DeviceReaderKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderKeyID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deviceReaderKeyID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderKeyID">PK value for DeviceReaderKey which data should be fetched into this DeviceReaderKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderKeyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deviceReaderKeyID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeviceReaderKeyID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeviceReaderKeyRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderKeyToCertEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection GetMultiDeviceReaderKeyToCerts(bool forceFetch)
		{
			return GetMultiDeviceReaderKeyToCerts(forceFetch, _deviceReaderKeyToCerts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderKeyToCertEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection GetMultiDeviceReaderKeyToCerts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceReaderKeyToCerts(forceFetch, _deviceReaderKeyToCerts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection GetMultiDeviceReaderKeyToCerts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceReaderKeyToCerts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection GetMultiDeviceReaderKeyToCerts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceReaderKeyToCerts || forceFetch || _alwaysFetchDeviceReaderKeyToCerts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceReaderKeyToCerts);
				_deviceReaderKeyToCerts.SuppressClearInGetMulti=!forceFetch;
				_deviceReaderKeyToCerts.EntityFactoryToUse = entityFactoryToUse;
				_deviceReaderKeyToCerts.GetMultiManyToOne(null, this, filter);
				_deviceReaderKeyToCerts.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceReaderKeyToCerts = true;
			}
			return _deviceReaderKeyToCerts;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceReaderKeyToCerts'. These settings will be taken into account
		/// when the property DeviceReaderKeyToCerts is requested or GetMultiDeviceReaderKeyToCerts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceReaderKeyToCerts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceReaderKeyToCerts.SortClauses=sortClauses;
			_deviceReaderKeyToCerts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'DeviceReaderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceReaderEntity' which is related to this entity.</returns>
		public DeviceReaderEntity GetSingleDeviceReader()
		{
			return GetSingleDeviceReader(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceReaderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceReaderEntity' which is related to this entity.</returns>
		public virtual DeviceReaderEntity GetSingleDeviceReader(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceReader || forceFetch || _alwaysFetchDeviceReader) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceReaderEntityUsingDeviceReaderID);
				DeviceReaderEntity newEntity = new DeviceReaderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceReaderID);
				}
				if(fetchResult)
				{
					newEntity = (DeviceReaderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceReaderReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceReader = newEntity;
				_alreadyFetchedDeviceReader = fetchResult;
			}
			return _deviceReader;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeviceReader", _deviceReader);
			toReturn.Add("DeviceReaderKeyToCerts", _deviceReaderKeyToCerts);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deviceReaderKeyID">PK value for DeviceReaderKey which data should be fetched into this DeviceReaderKey object</param>
		/// <param name="validator">The validator object for this DeviceReaderKeyEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 deviceReaderKeyID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deviceReaderKeyID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_deviceReaderKeyToCerts = new VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection();
			_deviceReaderKeyToCerts.SetContainingEntityInfo(this, "DeviceReaderKey");
			_deviceReaderReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataSignatureKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceReaderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceReaderKeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InstallationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyBlock", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyIndex", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _deviceReader</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceReader(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceReader, new PropertyChangedEventHandler( OnDeviceReaderPropertyChanged ), "DeviceReader", VarioSL.Entities.RelationClasses.StaticDeviceReaderKeyRelations.DeviceReaderEntityUsingDeviceReaderIDStatic, true, signalRelatedEntity, "DeviceReaderKeys", resetFKFields, new int[] { (int)DeviceReaderKeyFieldIndex.DeviceReaderID } );		
			_deviceReader = null;
		}
		
		/// <summary> setups the sync logic for member _deviceReader</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceReader(IEntityCore relatedEntity)
		{
			if(_deviceReader!=relatedEntity)
			{		
				DesetupSyncDeviceReader(true, true);
				_deviceReader = (DeviceReaderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceReader, new PropertyChangedEventHandler( OnDeviceReaderPropertyChanged ), "DeviceReader", VarioSL.Entities.RelationClasses.StaticDeviceReaderKeyRelations.DeviceReaderEntityUsingDeviceReaderIDStatic, true, ref _alreadyFetchedDeviceReader, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceReaderPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deviceReaderKeyID">PK value for DeviceReaderKey which data should be fetched into this DeviceReaderKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 deviceReaderKeyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeviceReaderKeyFieldIndex.DeviceReaderKeyID].ForcedCurrentValueWrite(deviceReaderKeyID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeviceReaderKeyDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeviceReaderKeyEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeviceReaderKeyRelations Relations
		{
			get	{ return new DeviceReaderKeyRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReaderKeyToCert' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceReaderKeyToCerts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection(), (IEntityRelation)GetRelationsForField("DeviceReaderKeyToCerts")[0], (int)VarioSL.Entities.EntityType.DeviceReaderKeyEntity, (int)VarioSL.Entities.EntityType.DeviceReaderKeyToCertEntity, 0, null, null, null, "DeviceReaderKeyToCerts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReader'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceReader
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderCollection(), (IEntityRelation)GetRelationsForField("DeviceReader")[0], (int)VarioSL.Entities.EntityType.DeviceReaderKeyEntity, (int)VarioSL.Entities.EntityType.DeviceReaderEntity, 0, null, null, null, "DeviceReader", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CreationDate property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."CREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreationDate
		{
			get { return (System.DateTime)GetValue((int)DeviceReaderKeyFieldIndex.CreationDate, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.CreationDate, value, true); }
		}

		/// <summary> The DataSignatureKey property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."DATASIGNATUREKEY"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DataSignatureKey
		{
			get { return (System.String)GetValue((int)DeviceReaderKeyFieldIndex.DataSignatureKey, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.DataSignatureKey, value, true); }
		}

		/// <summary> The Description property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)DeviceReaderKeyFieldIndex.Description, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.Description, value, true); }
		}

		/// <summary> The DeviceReaderID property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."DEVICEREADERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DeviceReaderID
		{
			get { return (System.Int64)GetValue((int)DeviceReaderKeyFieldIndex.DeviceReaderID, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.DeviceReaderID, value, true); }
		}

		/// <summary> The DeviceReaderKeyID property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."DEVICEREADERKEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 DeviceReaderKeyID
		{
			get { return (System.Int64)GetValue((int)DeviceReaderKeyFieldIndex.DeviceReaderKeyID, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.DeviceReaderKeyID, value, true); }
		}

		/// <summary> The InstallationDate property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."INSTALLATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime InstallationDate
		{
			get { return (System.DateTime)GetValue((int)DeviceReaderKeyFieldIndex.InstallationDate, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.InstallationDate, value, true); }
		}

		/// <summary> The KeyBlock property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."KEYBLOCK"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String KeyBlock
		{
			get { return (System.String)GetValue((int)DeviceReaderKeyFieldIndex.KeyBlock, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.KeyBlock, value, true); }
		}

		/// <summary> The KeyIndex property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."KEYINDEX"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 KeyIndex
		{
			get { return (System.Int32)GetValue((int)DeviceReaderKeyFieldIndex.KeyIndex, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.KeyIndex, value, true); }
		}

		/// <summary> The KeyStatus property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."KEYSTATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 KeyStatus
		{
			get { return (System.Int32)GetValue((int)DeviceReaderKeyFieldIndex.KeyStatus, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.KeyStatus, value, true); }
		}

		/// <summary> The KeyType property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."KEYTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 KeyType
		{
			get { return (System.Int32)GetValue((int)DeviceReaderKeyFieldIndex.KeyType, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.KeyType, value, true); }
		}

		/// <summary> The LastModified property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)DeviceReaderKeyFieldIndex.LastModified, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)DeviceReaderKeyFieldIndex.LastUser, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)DeviceReaderKeyFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)DeviceReaderKeyFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The Version property of the Entity DeviceReaderKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERKEY"."VERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)DeviceReaderKeyFieldIndex.Version, true); }
			set	{ SetValue((int)DeviceReaderKeyFieldIndex.Version, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyToCertEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceReaderKeyToCerts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection DeviceReaderKeyToCerts
		{
			get	{ return GetMultiDeviceReaderKeyToCerts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceReaderKeyToCerts. When set to true, DeviceReaderKeyToCerts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceReaderKeyToCerts is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceReaderKeyToCerts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceReaderKeyToCerts
		{
			get	{ return _alwaysFetchDeviceReaderKeyToCerts; }
			set	{ _alwaysFetchDeviceReaderKeyToCerts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceReaderKeyToCerts already has been fetched. Setting this property to false when DeviceReaderKeyToCerts has been fetched
		/// will clear the DeviceReaderKeyToCerts collection well. Setting this property to true while DeviceReaderKeyToCerts hasn't been fetched disables lazy loading for DeviceReaderKeyToCerts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceReaderKeyToCerts
		{
			get { return _alreadyFetchedDeviceReaderKeyToCerts;}
			set 
			{
				if(_alreadyFetchedDeviceReaderKeyToCerts && !value && (_deviceReaderKeyToCerts != null))
				{
					_deviceReaderKeyToCerts.Clear();
				}
				_alreadyFetchedDeviceReaderKeyToCerts = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'DeviceReaderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceReader()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceReaderEntity DeviceReader
		{
			get	{ return GetSingleDeviceReader(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceReader(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeviceReaderKeys", "DeviceReader", _deviceReader, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceReader. When set to true, DeviceReader is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceReader is accessed. You can always execute a forced fetch by calling GetSingleDeviceReader(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceReader
		{
			get	{ return _alwaysFetchDeviceReader; }
			set	{ _alwaysFetchDeviceReader = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceReader already has been fetched. Setting this property to false when DeviceReader has been fetched
		/// will set DeviceReader to null as well. Setting this property to true while DeviceReader hasn't been fetched disables lazy loading for DeviceReader</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceReader
		{
			get { return _alreadyFetchedDeviceReader;}
			set 
			{
				if(_alreadyFetchedDeviceReader && !value)
				{
					this.DeviceReader = null;
				}
				_alreadyFetchedDeviceReader = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceReader is not found
		/// in the database. When set to true, DeviceReader will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceReaderReturnsNewIfNotFound
		{
			get	{ return _deviceReaderReturnsNewIfNotFound; }
			set { _deviceReaderReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DeviceReaderKeyEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
