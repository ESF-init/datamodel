﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ListLayoutType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ListLayoutTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection	_listLayoutObjects;
		private bool	_alwaysFetchListLayoutObjects, _alreadyFetchedListLayoutObjects;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ListLayoutObjects</summary>
			public static readonly string ListLayoutObjects = "ListLayoutObjects";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ListLayoutTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ListLayoutTypeEntity() :base("ListLayoutTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="listLayoutID">PK value for ListLayoutType which data should be fetched into this ListLayoutType object</param>
		public ListLayoutTypeEntity(VarioSL.Entities.Enumerations.TariffListLayoutType listLayoutID):base("ListLayoutTypeEntity")
		{
			InitClassFetch(listLayoutID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="listLayoutID">PK value for ListLayoutType which data should be fetched into this ListLayoutType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ListLayoutTypeEntity(VarioSL.Entities.Enumerations.TariffListLayoutType listLayoutID, IPrefetchPath prefetchPathToUse):base("ListLayoutTypeEntity")
		{
			InitClassFetch(listLayoutID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="listLayoutID">PK value for ListLayoutType which data should be fetched into this ListLayoutType object</param>
		/// <param name="validator">The custom validator object for this ListLayoutTypeEntity</param>
		public ListLayoutTypeEntity(VarioSL.Entities.Enumerations.TariffListLayoutType listLayoutID, IValidator validator):base("ListLayoutTypeEntity")
		{
			InitClassFetch(listLayoutID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ListLayoutTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_listLayoutObjects = (VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection)info.GetValue("_listLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection));
			_alwaysFetchListLayoutObjects = info.GetBoolean("_alwaysFetchListLayoutObjects");
			_alreadyFetchedListLayoutObjects = info.GetBoolean("_alreadyFetchedListLayoutObjects");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedListLayoutObjects = (_listLayoutObjects.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ListLayoutObjects":
					toReturn.Add(Relations.ListLayoutObjectEntityUsingLayoutTypeId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_listLayoutObjects", (!this.MarkedForDeletion?_listLayoutObjects:null));
			info.AddValue("_alwaysFetchListLayoutObjects", _alwaysFetchListLayoutObjects);
			info.AddValue("_alreadyFetchedListLayoutObjects", _alreadyFetchedListLayoutObjects);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ListLayoutObjects":
					_alreadyFetchedListLayoutObjects = true;
					if(entity!=null)
					{
						this.ListLayoutObjects.Add((ListLayoutObjectEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ListLayoutObjects":
					_listLayoutObjects.Add((ListLayoutObjectEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ListLayoutObjects":
					this.PerformRelatedEntityRemoval(_listLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_listLayoutObjects);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="listLayoutID">PK value for ListLayoutType which data should be fetched into this ListLayoutType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(VarioSL.Entities.Enumerations.TariffListLayoutType listLayoutID)
		{
			return FetchUsingPK(listLayoutID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="listLayoutID">PK value for ListLayoutType which data should be fetched into this ListLayoutType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(VarioSL.Entities.Enumerations.TariffListLayoutType listLayoutID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(listLayoutID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="listLayoutID">PK value for ListLayoutType which data should be fetched into this ListLayoutType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(VarioSL.Entities.Enumerations.TariffListLayoutType listLayoutID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(listLayoutID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="listLayoutID">PK value for ListLayoutType which data should be fetched into this ListLayoutType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(VarioSL.Entities.Enumerations.TariffListLayoutType listLayoutID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(listLayoutID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ListLayoutID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ListLayoutTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ListLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection GetMultiListLayoutObjects(bool forceFetch)
		{
			return GetMultiListLayoutObjects(forceFetch, _listLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ListLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection GetMultiListLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiListLayoutObjects(forceFetch, _listLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection GetMultiListLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiListLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection GetMultiListLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedListLayoutObjects || forceFetch || _alwaysFetchListLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_listLayoutObjects);
				_listLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_listLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_listLayoutObjects.GetMultiManyToOne(null, null, this, filter);
				_listLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedListLayoutObjects = true;
			}
			return _listLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'ListLayoutObjects'. These settings will be taken into account
		/// when the property ListLayoutObjects is requested or GetMultiListLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersListLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_listLayoutObjects.SortClauses=sortClauses;
			_listLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ListLayoutObjects", _listLayoutObjects);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="listLayoutID">PK value for ListLayoutType which data should be fetched into this ListLayoutType object</param>
		/// <param name="validator">The validator object for this ListLayoutTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(VarioSL.Entities.Enumerations.TariffListLayoutType listLayoutID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(listLayoutID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_listLayoutObjects = new VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection();
			_listLayoutObjects.SetContainingEntityInfo(this, "ListLayoutType");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListLayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NxName", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="listLayoutID">PK value for ListLayoutType which data should be fetched into this ListLayoutType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(VarioSL.Entities.Enumerations.TariffListLayoutType listLayoutID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ListLayoutTypeFieldIndex.ListLayoutID].ForcedCurrentValueWrite(listLayoutID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateListLayoutTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ListLayoutTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ListLayoutTypeRelations Relations
		{
			get	{ return new ListLayoutTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ListLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathListLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("ListLayoutObjects")[0], (int)VarioSL.Entities.EntityType.ListLayoutTypeEntity, (int)VarioSL.Entities.EntityType.ListLayoutObjectEntity, 0, null, null, null, "ListLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ListLayoutID property of the Entity ListLayoutType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LISTLAYOUTTYPE"."LISTLAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TariffListLayoutType ListLayoutID
		{
			get { return (VarioSL.Entities.Enumerations.TariffListLayoutType)GetValue((int)ListLayoutTypeFieldIndex.ListLayoutID, true); }
			set	{ SetValue((int)ListLayoutTypeFieldIndex.ListLayoutID, value, true); }
		}

		/// <summary> The Name property of the Entity ListLayoutType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LISTLAYOUTTYPE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ListLayoutTypeFieldIndex.Name, true); }
			set	{ SetValue((int)ListLayoutTypeFieldIndex.Name, value, true); }
		}

		/// <summary> The NxName property of the Entity ListLayoutType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LISTLAYOUTTYPE"."NXNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NxName
		{
			get { return (System.String)GetValue((int)ListLayoutTypeFieldIndex.NxName, true); }
			set	{ SetValue((int)ListLayoutTypeFieldIndex.NxName, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiListLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection ListLayoutObjects
		{
			get	{ return GetMultiListLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ListLayoutObjects. When set to true, ListLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ListLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiListLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchListLayoutObjects
		{
			get	{ return _alwaysFetchListLayoutObjects; }
			set	{ _alwaysFetchListLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ListLayoutObjects already has been fetched. Setting this property to false when ListLayoutObjects has been fetched
		/// will clear the ListLayoutObjects collection well. Setting this property to true while ListLayoutObjects hasn't been fetched disables lazy loading for ListLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedListLayoutObjects
		{
			get { return _alreadyFetchedListLayoutObjects;}
			set 
			{
				if(_alreadyFetchedListLayoutObjects && !value && (_listLayoutObjects != null))
				{
					_listLayoutObjects.Clear();
				}
				_alreadyFetchedListLayoutObjects = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ListLayoutTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
