﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RuleCapping'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RuleCappingEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection	_ruleCappingToTicket;
		private bool	_alwaysFetchRuleCappingToTicket, _alreadyFetchedRuleCappingToTicket;
		private VarioSL.Entities.CollectionClasses.TicketCollection _ticketCollectionViaRuleCappingToTicket;
		private bool	_alwaysFetchTicketCollectionViaRuleCappingToTicket, _alreadyFetchedTicketCollectionViaRuleCappingToTicket;
		private CalendarEntity _calendar;
		private bool	_alwaysFetchCalendar, _alreadyFetchedCalendar, _calendarReturnsNewIfNotFound;
		private RulePeriodEntity _rulePeriod;
		private bool	_alwaysFetchRulePeriod, _alreadyFetchedRulePeriod, _rulePeriodReturnsNewIfNotFound;
		private RuleTypeEntity _tmRuleType;
		private bool	_alwaysFetchTmRuleType, _alreadyFetchedTmRuleType, _tmRuleTypeReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Calendar</summary>
			public static readonly string Calendar = "Calendar";
			/// <summary>Member name RulePeriod</summary>
			public static readonly string RulePeriod = "RulePeriod";
			/// <summary>Member name TmRuleType</summary>
			public static readonly string TmRuleType = "TmRuleType";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name RuleCappingToTicket</summary>
			public static readonly string RuleCappingToTicket = "RuleCappingToTicket";
			/// <summary>Member name TicketCollectionViaRuleCappingToTicket</summary>
			public static readonly string TicketCollectionViaRuleCappingToTicket = "TicketCollectionViaRuleCappingToTicket";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RuleCappingEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RuleCappingEntity() :base("RuleCappingEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="ruleCappingID">PK value for RuleCapping which data should be fetched into this RuleCapping object</param>
		public RuleCappingEntity(System.Int64 ruleCappingID):base("RuleCappingEntity")
		{
			InitClassFetch(ruleCappingID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="ruleCappingID">PK value for RuleCapping which data should be fetched into this RuleCapping object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RuleCappingEntity(System.Int64 ruleCappingID, IPrefetchPath prefetchPathToUse):base("RuleCappingEntity")
		{
			InitClassFetch(ruleCappingID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="ruleCappingID">PK value for RuleCapping which data should be fetched into this RuleCapping object</param>
		/// <param name="validator">The custom validator object for this RuleCappingEntity</param>
		public RuleCappingEntity(System.Int64 ruleCappingID, IValidator validator):base("RuleCappingEntity")
		{
			InitClassFetch(ruleCappingID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RuleCappingEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_ruleCappingToTicket = (VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection)info.GetValue("_ruleCappingToTicket", typeof(VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection));
			_alwaysFetchRuleCappingToTicket = info.GetBoolean("_alwaysFetchRuleCappingToTicket");
			_alreadyFetchedRuleCappingToTicket = info.GetBoolean("_alreadyFetchedRuleCappingToTicket");
			_ticketCollectionViaRuleCappingToTicket = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketCollectionViaRuleCappingToTicket", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketCollectionViaRuleCappingToTicket = info.GetBoolean("_alwaysFetchTicketCollectionViaRuleCappingToTicket");
			_alreadyFetchedTicketCollectionViaRuleCappingToTicket = info.GetBoolean("_alreadyFetchedTicketCollectionViaRuleCappingToTicket");
			_calendar = (CalendarEntity)info.GetValue("_calendar", typeof(CalendarEntity));
			if(_calendar!=null)
			{
				_calendar.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_calendarReturnsNewIfNotFound = info.GetBoolean("_calendarReturnsNewIfNotFound");
			_alwaysFetchCalendar = info.GetBoolean("_alwaysFetchCalendar");
			_alreadyFetchedCalendar = info.GetBoolean("_alreadyFetchedCalendar");

			_rulePeriod = (RulePeriodEntity)info.GetValue("_rulePeriod", typeof(RulePeriodEntity));
			if(_rulePeriod!=null)
			{
				_rulePeriod.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_rulePeriodReturnsNewIfNotFound = info.GetBoolean("_rulePeriodReturnsNewIfNotFound");
			_alwaysFetchRulePeriod = info.GetBoolean("_alwaysFetchRulePeriod");
			_alreadyFetchedRulePeriod = info.GetBoolean("_alreadyFetchedRulePeriod");

			_tmRuleType = (RuleTypeEntity)info.GetValue("_tmRuleType", typeof(RuleTypeEntity));
			if(_tmRuleType!=null)
			{
				_tmRuleType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tmRuleTypeReturnsNewIfNotFound = info.GetBoolean("_tmRuleTypeReturnsNewIfNotFound");
			_alwaysFetchTmRuleType = info.GetBoolean("_alwaysFetchTmRuleType");
			_alreadyFetchedTmRuleType = info.GetBoolean("_alreadyFetchedTmRuleType");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RuleCappingFieldIndex)fieldIndex)
			{
				case RuleCappingFieldIndex.RulePeriodID:
					DesetupSyncRulePeriod(true, false);
					_alreadyFetchedRulePeriod = false;
					break;
				case RuleCappingFieldIndex.RuleTypeID:
					DesetupSyncTmRuleType(true, false);
					_alreadyFetchedTmRuleType = false;
					break;
				case RuleCappingFieldIndex.CalendarID:
					DesetupSyncCalendar(true, false);
					_alreadyFetchedCalendar = false;
					break;
				case RuleCappingFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRuleCappingToTicket = (_ruleCappingToTicket.Count > 0);
			_alreadyFetchedTicketCollectionViaRuleCappingToTicket = (_ticketCollectionViaRuleCappingToTicket.Count > 0);
			_alreadyFetchedCalendar = (_calendar != null);
			_alreadyFetchedRulePeriod = (_rulePeriod != null);
			_alreadyFetchedTmRuleType = (_tmRuleType != null);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Calendar":
					toReturn.Add(Relations.CalendarEntityUsingCalendarID);
					break;
				case "RulePeriod":
					toReturn.Add(Relations.RulePeriodEntityUsingRulePeriodID);
					break;
				case "TmRuleType":
					toReturn.Add(Relations.RuleTypeEntityUsingRuleTypeID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "RuleCappingToTicket":
					toReturn.Add(Relations.RuleCappingToTicketEntityUsingRuleCappingID);
					break;
				case "TicketCollectionViaRuleCappingToTicket":
					toReturn.Add(Relations.RuleCappingToTicketEntityUsingRuleCappingID, "RuleCappingEntity__", "RuleCappingToTicket_", JoinHint.None);
					toReturn.Add(RuleCappingToTicketEntity.Relations.TicketEntityUsingTicketID, "RuleCappingToTicket_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_ruleCappingToTicket", (!this.MarkedForDeletion?_ruleCappingToTicket:null));
			info.AddValue("_alwaysFetchRuleCappingToTicket", _alwaysFetchRuleCappingToTicket);
			info.AddValue("_alreadyFetchedRuleCappingToTicket", _alreadyFetchedRuleCappingToTicket);
			info.AddValue("_ticketCollectionViaRuleCappingToTicket", (!this.MarkedForDeletion?_ticketCollectionViaRuleCappingToTicket:null));
			info.AddValue("_alwaysFetchTicketCollectionViaRuleCappingToTicket", _alwaysFetchTicketCollectionViaRuleCappingToTicket);
			info.AddValue("_alreadyFetchedTicketCollectionViaRuleCappingToTicket", _alreadyFetchedTicketCollectionViaRuleCappingToTicket);
			info.AddValue("_calendar", (!this.MarkedForDeletion?_calendar:null));
			info.AddValue("_calendarReturnsNewIfNotFound", _calendarReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCalendar", _alwaysFetchCalendar);
			info.AddValue("_alreadyFetchedCalendar", _alreadyFetchedCalendar);
			info.AddValue("_rulePeriod", (!this.MarkedForDeletion?_rulePeriod:null));
			info.AddValue("_rulePeriodReturnsNewIfNotFound", _rulePeriodReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRulePeriod", _alwaysFetchRulePeriod);
			info.AddValue("_alreadyFetchedRulePeriod", _alreadyFetchedRulePeriod);
			info.AddValue("_tmRuleType", (!this.MarkedForDeletion?_tmRuleType:null));
			info.AddValue("_tmRuleTypeReturnsNewIfNotFound", _tmRuleTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTmRuleType", _alwaysFetchTmRuleType);
			info.AddValue("_alreadyFetchedTmRuleType", _alreadyFetchedTmRuleType);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Calendar":
					_alreadyFetchedCalendar = true;
					this.Calendar = (CalendarEntity)entity;
					break;
				case "RulePeriod":
					_alreadyFetchedRulePeriod = true;
					this.RulePeriod = (RulePeriodEntity)entity;
					break;
				case "TmRuleType":
					_alreadyFetchedTmRuleType = true;
					this.TmRuleType = (RuleTypeEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "RuleCappingToTicket":
					_alreadyFetchedRuleCappingToTicket = true;
					if(entity!=null)
					{
						this.RuleCappingToTicket.Add((RuleCappingToTicketEntity)entity);
					}
					break;
				case "TicketCollectionViaRuleCappingToTicket":
					_alreadyFetchedTicketCollectionViaRuleCappingToTicket = true;
					if(entity!=null)
					{
						this.TicketCollectionViaRuleCappingToTicket.Add((TicketEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Calendar":
					SetupSyncCalendar(relatedEntity);
					break;
				case "RulePeriod":
					SetupSyncRulePeriod(relatedEntity);
					break;
				case "TmRuleType":
					SetupSyncTmRuleType(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "RuleCappingToTicket":
					_ruleCappingToTicket.Add((RuleCappingToTicketEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Calendar":
					DesetupSyncCalendar(false, true);
					break;
				case "RulePeriod":
					DesetupSyncRulePeriod(false, true);
					break;
				case "TmRuleType":
					DesetupSyncTmRuleType(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "RuleCappingToTicket":
					this.PerformRelatedEntityRemoval(_ruleCappingToTicket, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_calendar!=null)
			{
				toReturn.Add(_calendar);
			}
			if(_rulePeriod!=null)
			{
				toReturn.Add(_rulePeriod);
			}
			if(_tmRuleType!=null)
			{
				toReturn.Add(_tmRuleType);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_ruleCappingToTicket);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleCappingID">PK value for RuleCapping which data should be fetched into this RuleCapping object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleCappingID)
		{
			return FetchUsingPK(ruleCappingID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleCappingID">PK value for RuleCapping which data should be fetched into this RuleCapping object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleCappingID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(ruleCappingID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleCappingID">PK value for RuleCapping which data should be fetched into this RuleCapping object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleCappingID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(ruleCappingID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleCappingID">PK value for RuleCapping which data should be fetched into this RuleCapping object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleCappingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(ruleCappingID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RuleCappingID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RuleCappingRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection GetMultiRuleCappingToTicket(bool forceFetch)
		{
			return GetMultiRuleCappingToTicket(forceFetch, _ruleCappingToTicket.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection GetMultiRuleCappingToTicket(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleCappingToTicket(forceFetch, _ruleCappingToTicket.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection GetMultiRuleCappingToTicket(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleCappingToTicket(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection GetMultiRuleCappingToTicket(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleCappingToTicket || forceFetch || _alwaysFetchRuleCappingToTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleCappingToTicket);
				_ruleCappingToTicket.SuppressClearInGetMulti=!forceFetch;
				_ruleCappingToTicket.EntityFactoryToUse = entityFactoryToUse;
				_ruleCappingToTicket.GetMultiManyToOne(this, null, filter);
				_ruleCappingToTicket.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleCappingToTicket = true;
			}
			return _ruleCappingToTicket;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleCappingToTicket'. These settings will be taken into account
		/// when the property RuleCappingToTicket is requested or GetMultiRuleCappingToTicket is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleCappingToTicket(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleCappingToTicket.SortClauses=sortClauses;
			_ruleCappingToTicket.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaRuleCappingToTicket(bool forceFetch)
		{
			return GetMultiTicketCollectionViaRuleCappingToTicket(forceFetch, _ticketCollectionViaRuleCappingToTicket.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaRuleCappingToTicket(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTicketCollectionViaRuleCappingToTicket || forceFetch || _alwaysFetchTicketCollectionViaRuleCappingToTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketCollectionViaRuleCappingToTicket);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RuleCappingFields.RuleCappingID, ComparisonOperator.Equal, this.RuleCappingID, "RuleCappingEntity__"));
				_ticketCollectionViaRuleCappingToTicket.SuppressClearInGetMulti=!forceFetch;
				_ticketCollectionViaRuleCappingToTicket.EntityFactoryToUse = entityFactoryToUse;
				_ticketCollectionViaRuleCappingToTicket.GetMulti(filter, GetRelationsForField("TicketCollectionViaRuleCappingToTicket"));
				_ticketCollectionViaRuleCappingToTicket.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketCollectionViaRuleCappingToTicket = true;
			}
			return _ticketCollectionViaRuleCappingToTicket;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketCollectionViaRuleCappingToTicket'. These settings will be taken into account
		/// when the property TicketCollectionViaRuleCappingToTicket is requested or GetMultiTicketCollectionViaRuleCappingToTicket is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketCollectionViaRuleCappingToTicket(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketCollectionViaRuleCappingToTicket.SortClauses=sortClauses;
			_ticketCollectionViaRuleCappingToTicket.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CalendarEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CalendarEntity' which is related to this entity.</returns>
		public CalendarEntity GetSingleCalendar()
		{
			return GetSingleCalendar(false);
		}

		/// <summary> Retrieves the related entity of type 'CalendarEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CalendarEntity' which is related to this entity.</returns>
		public virtual CalendarEntity GetSingleCalendar(bool forceFetch)
		{
			if( ( !_alreadyFetchedCalendar || forceFetch || _alwaysFetchCalendar) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CalendarEntityUsingCalendarID);
				CalendarEntity newEntity = new CalendarEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CalendarID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CalendarEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_calendarReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Calendar = newEntity;
				_alreadyFetchedCalendar = fetchResult;
			}
			return _calendar;
		}


		/// <summary> Retrieves the related entity of type 'RulePeriodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RulePeriodEntity' which is related to this entity.</returns>
		public RulePeriodEntity GetSingleRulePeriod()
		{
			return GetSingleRulePeriod(false);
		}

		/// <summary> Retrieves the related entity of type 'RulePeriodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RulePeriodEntity' which is related to this entity.</returns>
		public virtual RulePeriodEntity GetSingleRulePeriod(bool forceFetch)
		{
			if( ( !_alreadyFetchedRulePeriod || forceFetch || _alwaysFetchRulePeriod) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RulePeriodEntityUsingRulePeriodID);
				RulePeriodEntity newEntity = new RulePeriodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RulePeriodID);
				}
				if(fetchResult)
				{
					newEntity = (RulePeriodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_rulePeriodReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RulePeriod = newEntity;
				_alreadyFetchedRulePeriod = fetchResult;
			}
			return _rulePeriod;
		}


		/// <summary> Retrieves the related entity of type 'RuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RuleTypeEntity' which is related to this entity.</returns>
		public RuleTypeEntity GetSingleTmRuleType()
		{
			return GetSingleTmRuleType(false);
		}

		/// <summary> Retrieves the related entity of type 'RuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RuleTypeEntity' which is related to this entity.</returns>
		public virtual RuleTypeEntity GetSingleTmRuleType(bool forceFetch)
		{
			if( ( !_alreadyFetchedTmRuleType || forceFetch || _alwaysFetchTmRuleType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RuleTypeEntityUsingRuleTypeID);
				RuleTypeEntity newEntity = new RuleTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RuleTypeID);
				}
				if(fetchResult)
				{
					newEntity = (RuleTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tmRuleTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TmRuleType = newEntity;
				_alreadyFetchedTmRuleType = fetchResult;
			}
			return _tmRuleType;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID);
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Calendar", _calendar);
			toReturn.Add("RulePeriod", _rulePeriod);
			toReturn.Add("TmRuleType", _tmRuleType);
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("RuleCappingToTicket", _ruleCappingToTicket);
			toReturn.Add("TicketCollectionViaRuleCappingToTicket", _ticketCollectionViaRuleCappingToTicket);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="ruleCappingID">PK value for RuleCapping which data should be fetched into this RuleCapping object</param>
		/// <param name="validator">The validator object for this RuleCappingEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 ruleCappingID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(ruleCappingID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_ruleCappingToTicket = new VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection();
			_ruleCappingToTicket.SetContainingEntityInfo(this, "RuleCapping");
			_ticketCollectionViaRuleCappingToTicket = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_calendarReturnsNewIfNotFound = false;
			_rulePeriodReturnsNewIfNotFound = false;
			_tmRuleTypeReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleCappingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RulePeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeOfCardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PotNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CalendarID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CappingName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VirtualTicketInternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxChargeAmount", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _calendar</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCalendar(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _calendar, new PropertyChangedEventHandler( OnCalendarPropertyChanged ), "Calendar", VarioSL.Entities.RelationClasses.StaticRuleCappingRelations.CalendarEntityUsingCalendarIDStatic, true, signalRelatedEntity, "RuleCapping", resetFKFields, new int[] { (int)RuleCappingFieldIndex.CalendarID } );		
			_calendar = null;
		}
		
		/// <summary> setups the sync logic for member _calendar</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCalendar(IEntityCore relatedEntity)
		{
			if(_calendar!=relatedEntity)
			{		
				DesetupSyncCalendar(true, true);
				_calendar = (CalendarEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _calendar, new PropertyChangedEventHandler( OnCalendarPropertyChanged ), "Calendar", VarioSL.Entities.RelationClasses.StaticRuleCappingRelations.CalendarEntityUsingCalendarIDStatic, true, ref _alreadyFetchedCalendar, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCalendarPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _rulePeriod</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRulePeriod(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _rulePeriod, new PropertyChangedEventHandler( OnRulePeriodPropertyChanged ), "RulePeriod", VarioSL.Entities.RelationClasses.StaticRuleCappingRelations.RulePeriodEntityUsingRulePeriodIDStatic, true, signalRelatedEntity, "RuleCapping", resetFKFields, new int[] { (int)RuleCappingFieldIndex.RulePeriodID } );		
			_rulePeriod = null;
		}
		
		/// <summary> setups the sync logic for member _rulePeriod</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRulePeriod(IEntityCore relatedEntity)
		{
			if(_rulePeriod!=relatedEntity)
			{		
				DesetupSyncRulePeriod(true, true);
				_rulePeriod = (RulePeriodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _rulePeriod, new PropertyChangedEventHandler( OnRulePeriodPropertyChanged ), "RulePeriod", VarioSL.Entities.RelationClasses.StaticRuleCappingRelations.RulePeriodEntityUsingRulePeriodIDStatic, true, ref _alreadyFetchedRulePeriod, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRulePeriodPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tmRuleType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTmRuleType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tmRuleType, new PropertyChangedEventHandler( OnTmRuleTypePropertyChanged ), "TmRuleType", VarioSL.Entities.RelationClasses.StaticRuleCappingRelations.RuleTypeEntityUsingRuleTypeIDStatic, true, signalRelatedEntity, "TmRuleCappings", resetFKFields, new int[] { (int)RuleCappingFieldIndex.RuleTypeID } );		
			_tmRuleType = null;
		}
		
		/// <summary> setups the sync logic for member _tmRuleType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTmRuleType(IEntityCore relatedEntity)
		{
			if(_tmRuleType!=relatedEntity)
			{		
				DesetupSyncTmRuleType(true, true);
				_tmRuleType = (RuleTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tmRuleType, new PropertyChangedEventHandler( OnTmRuleTypePropertyChanged ), "TmRuleType", VarioSL.Entities.RelationClasses.StaticRuleCappingRelations.RuleTypeEntityUsingRuleTypeIDStatic, true, ref _alreadyFetchedTmRuleType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTmRuleTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticRuleCappingRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "RuleCappings", resetFKFields, new int[] { (int)RuleCappingFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticRuleCappingRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="ruleCappingID">PK value for RuleCapping which data should be fetched into this RuleCapping object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 ruleCappingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RuleCappingFieldIndex.RuleCappingID].ForcedCurrentValueWrite(ruleCappingID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRuleCappingDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RuleCappingEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RuleCappingRelations Relations
		{
			get	{ return new RuleCappingRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleCappingToTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleCappingToTicket
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection(), (IEntityRelation)GetRelationsForField("RuleCappingToTicket")[0], (int)VarioSL.Entities.EntityType.RuleCappingEntity, (int)VarioSL.Entities.EntityType.RuleCappingToTicketEntity, 0, null, null, null, "RuleCappingToTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketCollectionViaRuleCappingToTicket
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.RuleCappingToTicketEntityUsingRuleCappingID;
				intermediateRelation.SetAliases(string.Empty, "RuleCappingToTicket_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.RuleCappingEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, GetRelationsForField("TicketCollectionViaRuleCappingToTicket"), "TicketCollectionViaRuleCappingToTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Calendar'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCalendar
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CalendarCollection(), (IEntityRelation)GetRelationsForField("Calendar")[0], (int)VarioSL.Entities.EntityType.RuleCappingEntity, (int)VarioSL.Entities.EntityType.CalendarEntity, 0, null, null, null, "Calendar", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RulePeriod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRulePeriod
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RulePeriodCollection(), (IEntityRelation)GetRelationsForField("RulePeriod")[0], (int)VarioSL.Entities.EntityType.RuleCappingEntity, (int)VarioSL.Entities.EntityType.RulePeriodEntity, 0, null, null, null, "RulePeriod", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTmRuleType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleTypeCollection(), (IEntityRelation)GetRelationsForField("TmRuleType")[0], (int)VarioSL.Entities.EntityType.RuleCappingEntity, (int)VarioSL.Entities.EntityType.RuleTypeEntity, 0, null, null, null, "TmRuleType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.RuleCappingEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The RuleCappingID property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."RULECAPPINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 RuleCappingID
		{
			get { return (System.Int64)GetValue((int)RuleCappingFieldIndex.RuleCappingID, true); }
			set	{ SetValue((int)RuleCappingFieldIndex.RuleCappingID, value, true); }
		}

		/// <summary> The RulePeriodID property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."RULEPERIODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 RulePeriodID
		{
			get { return (System.Int64)GetValue((int)RuleCappingFieldIndex.RulePeriodID, true); }
			set	{ SetValue((int)RuleCappingFieldIndex.RulePeriodID, value, true); }
		}

		/// <summary> The RuleTypeID property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."RULETYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 RuleTypeID
		{
			get { return (System.Int64)GetValue((int)RuleCappingFieldIndex.RuleTypeID, true); }
			set	{ SetValue((int)RuleCappingFieldIndex.RuleTypeID, value, true); }
		}

		/// <summary> The TypeOfCardID property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."TYPEOFCARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TypeOfCardID
		{
			get { return (System.Int64)GetValue((int)RuleCappingFieldIndex.TypeOfCardID, true); }
			set	{ SetValue((int)RuleCappingFieldIndex.TypeOfCardID, value, true); }
		}

		/// <summary> The PotNumber property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."POTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PotNumber
		{
			get { return (System.Int32)GetValue((int)RuleCappingFieldIndex.PotNumber, true); }
			set	{ SetValue((int)RuleCappingFieldIndex.PotNumber, value, true); }
		}

		/// <summary> The CalendarID property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."CALENDARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Nullable<System.Int64> CalendarID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RuleCappingFieldIndex.CalendarID, false); }
			set	{ SetValue((int)RuleCappingFieldIndex.CalendarID, value, true); }
		}

		/// <summary> The Amount property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Amount
		{
			get { return (System.Int32)GetValue((int)RuleCappingFieldIndex.Amount, true); }
			set	{ SetValue((int)RuleCappingFieldIndex.Amount, value, true); }
		}

		/// <summary> The CappingName property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."CAPPINGNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CappingName
		{
			get { return (System.String)GetValue((int)RuleCappingFieldIndex.CappingName, true); }
			set	{ SetValue((int)RuleCappingFieldIndex.CappingName, value, true); }
		}

		/// <summary> The Description property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)RuleCappingFieldIndex.Description, true); }
			set	{ SetValue((int)RuleCappingFieldIndex.Description, value, true); }
		}

		/// <summary> The TariffID property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)RuleCappingFieldIndex.TariffID, true); }
			set	{ SetValue((int)RuleCappingFieldIndex.TariffID, value, true); }
		}

		/// <summary> The ServiceID property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."SERVICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ServiceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RuleCappingFieldIndex.ServiceID, false); }
			set	{ SetValue((int)RuleCappingFieldIndex.ServiceID, value, true); }
		}

		/// <summary> The VirtualTicketInternalNumber property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."VIRTUALTICKETINTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> VirtualTicketInternalNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)RuleCappingFieldIndex.VirtualTicketInternalNumber, false); }
			set	{ SetValue((int)RuleCappingFieldIndex.VirtualTicketInternalNumber, value, true); }
		}

		/// <summary> The MaxChargeAmount property of the Entity RuleCapping<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPING"."MAXCHARGEAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 MaxChargeAmount
		{
			get { return (System.Int64)GetValue((int)RuleCappingFieldIndex.MaxChargeAmount, true); }
			set	{ SetValue((int)RuleCappingFieldIndex.MaxChargeAmount, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleCappingToTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection RuleCappingToTicket
		{
			get	{ return GetMultiRuleCappingToTicket(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleCappingToTicket. When set to true, RuleCappingToTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleCappingToTicket is accessed. You can always execute/ a forced fetch by calling GetMultiRuleCappingToTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleCappingToTicket
		{
			get	{ return _alwaysFetchRuleCappingToTicket; }
			set	{ _alwaysFetchRuleCappingToTicket = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleCappingToTicket already has been fetched. Setting this property to false when RuleCappingToTicket has been fetched
		/// will clear the RuleCappingToTicket collection well. Setting this property to true while RuleCappingToTicket hasn't been fetched disables lazy loading for RuleCappingToTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleCappingToTicket
		{
			get { return _alreadyFetchedRuleCappingToTicket;}
			set 
			{
				if(_alreadyFetchedRuleCappingToTicket && !value && (_ruleCappingToTicket != null))
				{
					_ruleCappingToTicket.Clear();
				}
				_alreadyFetchedRuleCappingToTicket = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketCollectionViaRuleCappingToTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketCollectionViaRuleCappingToTicket
		{
			get { return GetMultiTicketCollectionViaRuleCappingToTicket(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketCollectionViaRuleCappingToTicket. When set to true, TicketCollectionViaRuleCappingToTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketCollectionViaRuleCappingToTicket is accessed. You can always execute a forced fetch by calling GetMultiTicketCollectionViaRuleCappingToTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketCollectionViaRuleCappingToTicket
		{
			get	{ return _alwaysFetchTicketCollectionViaRuleCappingToTicket; }
			set	{ _alwaysFetchTicketCollectionViaRuleCappingToTicket = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketCollectionViaRuleCappingToTicket already has been fetched. Setting this property to false when TicketCollectionViaRuleCappingToTicket has been fetched
		/// will clear the TicketCollectionViaRuleCappingToTicket collection well. Setting this property to true while TicketCollectionViaRuleCappingToTicket hasn't been fetched disables lazy loading for TicketCollectionViaRuleCappingToTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketCollectionViaRuleCappingToTicket
		{
			get { return _alreadyFetchedTicketCollectionViaRuleCappingToTicket;}
			set 
			{
				if(_alreadyFetchedTicketCollectionViaRuleCappingToTicket && !value && (_ticketCollectionViaRuleCappingToTicket != null))
				{
					_ticketCollectionViaRuleCappingToTicket.Clear();
				}
				_alreadyFetchedTicketCollectionViaRuleCappingToTicket = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CalendarEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCalendar()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CalendarEntity Calendar
		{
			get	{ return GetSingleCalendar(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCalendar(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RuleCapping", "Calendar", _calendar, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Calendar. When set to true, Calendar is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Calendar is accessed. You can always execute a forced fetch by calling GetSingleCalendar(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCalendar
		{
			get	{ return _alwaysFetchCalendar; }
			set	{ _alwaysFetchCalendar = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Calendar already has been fetched. Setting this property to false when Calendar has been fetched
		/// will set Calendar to null as well. Setting this property to true while Calendar hasn't been fetched disables lazy loading for Calendar</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCalendar
		{
			get { return _alreadyFetchedCalendar;}
			set 
			{
				if(_alreadyFetchedCalendar && !value)
				{
					this.Calendar = null;
				}
				_alreadyFetchedCalendar = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Calendar is not found
		/// in the database. When set to true, Calendar will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CalendarReturnsNewIfNotFound
		{
			get	{ return _calendarReturnsNewIfNotFound; }
			set { _calendarReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RulePeriodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRulePeriod()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RulePeriodEntity RulePeriod
		{
			get	{ return GetSingleRulePeriod(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRulePeriod(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RuleCapping", "RulePeriod", _rulePeriod, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RulePeriod. When set to true, RulePeriod is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RulePeriod is accessed. You can always execute a forced fetch by calling GetSingleRulePeriod(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRulePeriod
		{
			get	{ return _alwaysFetchRulePeriod; }
			set	{ _alwaysFetchRulePeriod = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RulePeriod already has been fetched. Setting this property to false when RulePeriod has been fetched
		/// will set RulePeriod to null as well. Setting this property to true while RulePeriod hasn't been fetched disables lazy loading for RulePeriod</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRulePeriod
		{
			get { return _alreadyFetchedRulePeriod;}
			set 
			{
				if(_alreadyFetchedRulePeriod && !value)
				{
					this.RulePeriod = null;
				}
				_alreadyFetchedRulePeriod = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RulePeriod is not found
		/// in the database. When set to true, RulePeriod will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RulePeriodReturnsNewIfNotFound
		{
			get	{ return _rulePeriodReturnsNewIfNotFound; }
			set { _rulePeriodReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RuleTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTmRuleType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RuleTypeEntity TmRuleType
		{
			get	{ return GetSingleTmRuleType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTmRuleType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TmRuleCappings", "TmRuleType", _tmRuleType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TmRuleType. When set to true, TmRuleType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TmRuleType is accessed. You can always execute a forced fetch by calling GetSingleTmRuleType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTmRuleType
		{
			get	{ return _alwaysFetchTmRuleType; }
			set	{ _alwaysFetchTmRuleType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TmRuleType already has been fetched. Setting this property to false when TmRuleType has been fetched
		/// will set TmRuleType to null as well. Setting this property to true while TmRuleType hasn't been fetched disables lazy loading for TmRuleType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTmRuleType
		{
			get { return _alreadyFetchedTmRuleType;}
			set 
			{
				if(_alreadyFetchedTmRuleType && !value)
				{
					this.TmRuleType = null;
				}
				_alreadyFetchedTmRuleType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TmRuleType is not found
		/// in the database. When set to true, TmRuleType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TmRuleTypeReturnsNewIfNotFound
		{
			get	{ return _tmRuleTypeReturnsNewIfNotFound; }
			set { _tmRuleTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RuleCappings", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.RuleCappingEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
