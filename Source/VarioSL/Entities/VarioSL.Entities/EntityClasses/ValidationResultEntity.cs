﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ValidationResult'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ValidationResultEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ValidationResultCollection	_validationResults;
		private bool	_alwaysFetchValidationResults, _alreadyFetchedValidationResults;
		private VarioSL.Entities.CollectionClasses.ValidationValueCollection	_validationValues;
		private bool	_alwaysFetchValidationValues, _alreadyFetchedValidationValues;
		private ValidationResultEntity _validationResult;
		private bool	_alwaysFetchValidationResult, _alreadyFetchedValidationResult, _validationResultReturnsNewIfNotFound;
		private ValidationRuleEntity _validationRule;
		private bool	_alwaysFetchValidationRule, _alreadyFetchedValidationRule, _validationRuleReturnsNewIfNotFound;
		private ValidationRunRuleEntity _validationRunRule;
		private bool	_alwaysFetchValidationRunRule, _alreadyFetchedValidationRunRule, _validationRunRuleReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ValidationResult</summary>
			public static readonly string ValidationResult = "ValidationResult";
			/// <summary>Member name ValidationRule</summary>
			public static readonly string ValidationRule = "ValidationRule";
			/// <summary>Member name ValidationRunRule</summary>
			public static readonly string ValidationRunRule = "ValidationRunRule";
			/// <summary>Member name ValidationResults</summary>
			public static readonly string ValidationResults = "ValidationResults";
			/// <summary>Member name ValidationValues</summary>
			public static readonly string ValidationValues = "ValidationValues";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ValidationResultEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ValidationResultEntity() :base("ValidationResultEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="validationResultID">PK value for ValidationResult which data should be fetched into this ValidationResult object</param>
		public ValidationResultEntity(System.Int64 validationResultID):base("ValidationResultEntity")
		{
			InitClassFetch(validationResultID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="validationResultID">PK value for ValidationResult which data should be fetched into this ValidationResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ValidationResultEntity(System.Int64 validationResultID, IPrefetchPath prefetchPathToUse):base("ValidationResultEntity")
		{
			InitClassFetch(validationResultID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="validationResultID">PK value for ValidationResult which data should be fetched into this ValidationResult object</param>
		/// <param name="validator">The custom validator object for this ValidationResultEntity</param>
		public ValidationResultEntity(System.Int64 validationResultID, IValidator validator):base("ValidationResultEntity")
		{
			InitClassFetch(validationResultID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ValidationResultEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_validationResults = (VarioSL.Entities.CollectionClasses.ValidationResultCollection)info.GetValue("_validationResults", typeof(VarioSL.Entities.CollectionClasses.ValidationResultCollection));
			_alwaysFetchValidationResults = info.GetBoolean("_alwaysFetchValidationResults");
			_alreadyFetchedValidationResults = info.GetBoolean("_alreadyFetchedValidationResults");

			_validationValues = (VarioSL.Entities.CollectionClasses.ValidationValueCollection)info.GetValue("_validationValues", typeof(VarioSL.Entities.CollectionClasses.ValidationValueCollection));
			_alwaysFetchValidationValues = info.GetBoolean("_alwaysFetchValidationValues");
			_alreadyFetchedValidationValues = info.GetBoolean("_alreadyFetchedValidationValues");
			_validationResult = (ValidationResultEntity)info.GetValue("_validationResult", typeof(ValidationResultEntity));
			if(_validationResult!=null)
			{
				_validationResult.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_validationResultReturnsNewIfNotFound = info.GetBoolean("_validationResultReturnsNewIfNotFound");
			_alwaysFetchValidationResult = info.GetBoolean("_alwaysFetchValidationResult");
			_alreadyFetchedValidationResult = info.GetBoolean("_alreadyFetchedValidationResult");

			_validationRule = (ValidationRuleEntity)info.GetValue("_validationRule", typeof(ValidationRuleEntity));
			if(_validationRule!=null)
			{
				_validationRule.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_validationRuleReturnsNewIfNotFound = info.GetBoolean("_validationRuleReturnsNewIfNotFound");
			_alwaysFetchValidationRule = info.GetBoolean("_alwaysFetchValidationRule");
			_alreadyFetchedValidationRule = info.GetBoolean("_alreadyFetchedValidationRule");

			_validationRunRule = (ValidationRunRuleEntity)info.GetValue("_validationRunRule", typeof(ValidationRunRuleEntity));
			if(_validationRunRule!=null)
			{
				_validationRunRule.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_validationRunRuleReturnsNewIfNotFound = info.GetBoolean("_validationRunRuleReturnsNewIfNotFound");
			_alwaysFetchValidationRunRule = info.GetBoolean("_alwaysFetchValidationRunRule");
			_alreadyFetchedValidationRunRule = info.GetBoolean("_alreadyFetchedValidationRunRule");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ValidationResultFieldIndex)fieldIndex)
			{
				case ValidationResultFieldIndex.OriginalValidationResultID:
					DesetupSyncValidationResult(true, false);
					_alreadyFetchedValidationResult = false;
					break;
				case ValidationResultFieldIndex.ValidationRuleID:
					DesetupSyncValidationRule(true, false);
					_alreadyFetchedValidationRule = false;
					break;
				case ValidationResultFieldIndex.ValidationRunRuleID:
					DesetupSyncValidationRunRule(true, false);
					_alreadyFetchedValidationRunRule = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedValidationResults = (_validationResults.Count > 0);
			_alreadyFetchedValidationValues = (_validationValues.Count > 0);
			_alreadyFetchedValidationResult = (_validationResult != null);
			_alreadyFetchedValidationRule = (_validationRule != null);
			_alreadyFetchedValidationRunRule = (_validationRunRule != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ValidationResult":
					toReturn.Add(Relations.ValidationResultEntityUsingValidationResultIDOriginalValidationResultID);
					break;
				case "ValidationRule":
					toReturn.Add(Relations.ValidationRuleEntityUsingValidationRuleID);
					break;
				case "ValidationRunRule":
					toReturn.Add(Relations.ValidationRunRuleEntityUsingValidationRunRuleID);
					break;
				case "ValidationResults":
					toReturn.Add(Relations.ValidationResultEntityUsingOriginalValidationResultID);
					break;
				case "ValidationValues":
					toReturn.Add(Relations.ValidationValueEntityUsingValidationResultID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_validationResults", (!this.MarkedForDeletion?_validationResults:null));
			info.AddValue("_alwaysFetchValidationResults", _alwaysFetchValidationResults);
			info.AddValue("_alreadyFetchedValidationResults", _alreadyFetchedValidationResults);
			info.AddValue("_validationValues", (!this.MarkedForDeletion?_validationValues:null));
			info.AddValue("_alwaysFetchValidationValues", _alwaysFetchValidationValues);
			info.AddValue("_alreadyFetchedValidationValues", _alreadyFetchedValidationValues);
			info.AddValue("_validationResult", (!this.MarkedForDeletion?_validationResult:null));
			info.AddValue("_validationResultReturnsNewIfNotFound", _validationResultReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchValidationResult", _alwaysFetchValidationResult);
			info.AddValue("_alreadyFetchedValidationResult", _alreadyFetchedValidationResult);
			info.AddValue("_validationRule", (!this.MarkedForDeletion?_validationRule:null));
			info.AddValue("_validationRuleReturnsNewIfNotFound", _validationRuleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchValidationRule", _alwaysFetchValidationRule);
			info.AddValue("_alreadyFetchedValidationRule", _alreadyFetchedValidationRule);
			info.AddValue("_validationRunRule", (!this.MarkedForDeletion?_validationRunRule:null));
			info.AddValue("_validationRunRuleReturnsNewIfNotFound", _validationRunRuleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchValidationRunRule", _alwaysFetchValidationRunRule);
			info.AddValue("_alreadyFetchedValidationRunRule", _alreadyFetchedValidationRunRule);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ValidationResult":
					_alreadyFetchedValidationResult = true;
					this.ValidationResult = (ValidationResultEntity)entity;
					break;
				case "ValidationRule":
					_alreadyFetchedValidationRule = true;
					this.ValidationRule = (ValidationRuleEntity)entity;
					break;
				case "ValidationRunRule":
					_alreadyFetchedValidationRunRule = true;
					this.ValidationRunRule = (ValidationRunRuleEntity)entity;
					break;
				case "ValidationResults":
					_alreadyFetchedValidationResults = true;
					if(entity!=null)
					{
						this.ValidationResults.Add((ValidationResultEntity)entity);
					}
					break;
				case "ValidationValues":
					_alreadyFetchedValidationValues = true;
					if(entity!=null)
					{
						this.ValidationValues.Add((ValidationValueEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ValidationResult":
					SetupSyncValidationResult(relatedEntity);
					break;
				case "ValidationRule":
					SetupSyncValidationRule(relatedEntity);
					break;
				case "ValidationRunRule":
					SetupSyncValidationRunRule(relatedEntity);
					break;
				case "ValidationResults":
					_validationResults.Add((ValidationResultEntity)relatedEntity);
					break;
				case "ValidationValues":
					_validationValues.Add((ValidationValueEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ValidationResult":
					DesetupSyncValidationResult(false, true);
					break;
				case "ValidationRule":
					DesetupSyncValidationRule(false, true);
					break;
				case "ValidationRunRule":
					DesetupSyncValidationRunRule(false, true);
					break;
				case "ValidationResults":
					this.PerformRelatedEntityRemoval(_validationResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ValidationValues":
					this.PerformRelatedEntityRemoval(_validationValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_validationResult!=null)
			{
				toReturn.Add(_validationResult);
			}
			if(_validationRule!=null)
			{
				toReturn.Add(_validationRule);
			}
			if(_validationRunRule!=null)
			{
				toReturn.Add(_validationRunRule);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_validationResults);
			toReturn.Add(_validationValues);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="validationResultID">PK value for ValidationResult which data should be fetched into this ValidationResult object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 validationResultID)
		{
			return FetchUsingPK(validationResultID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="validationResultID">PK value for ValidationResult which data should be fetched into this ValidationResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 validationResultID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(validationResultID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="validationResultID">PK value for ValidationResult which data should be fetched into this ValidationResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 validationResultID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(validationResultID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="validationResultID">PK value for ValidationResult which data should be fetched into this ValidationResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 validationResultID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(validationResultID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ValidationResultID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ValidationResultRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ValidationResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ValidationResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ValidationResultCollection GetMultiValidationResults(bool forceFetch)
		{
			return GetMultiValidationResults(forceFetch, _validationResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ValidationResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ValidationResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ValidationResultCollection GetMultiValidationResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiValidationResults(forceFetch, _validationResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ValidationResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ValidationResultCollection GetMultiValidationResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiValidationResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ValidationResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ValidationResultCollection GetMultiValidationResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedValidationResults || forceFetch || _alwaysFetchValidationResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_validationResults);
				_validationResults.SuppressClearInGetMulti=!forceFetch;
				_validationResults.EntityFactoryToUse = entityFactoryToUse;
				_validationResults.GetMultiManyToOne(this, null, null, filter);
				_validationResults.SuppressClearInGetMulti=false;
				_alreadyFetchedValidationResults = true;
			}
			return _validationResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ValidationResults'. These settings will be taken into account
		/// when the property ValidationResults is requested or GetMultiValidationResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersValidationResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_validationResults.SortClauses=sortClauses;
			_validationResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ValidationValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ValidationValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ValidationValueCollection GetMultiValidationValues(bool forceFetch)
		{
			return GetMultiValidationValues(forceFetch, _validationValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ValidationValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ValidationValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ValidationValueCollection GetMultiValidationValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiValidationValues(forceFetch, _validationValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ValidationValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ValidationValueCollection GetMultiValidationValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiValidationValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ValidationValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ValidationValueCollection GetMultiValidationValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedValidationValues || forceFetch || _alwaysFetchValidationValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_validationValues);
				_validationValues.SuppressClearInGetMulti=!forceFetch;
				_validationValues.EntityFactoryToUse = entityFactoryToUse;
				_validationValues.GetMultiManyToOne(this, filter);
				_validationValues.SuppressClearInGetMulti=false;
				_alreadyFetchedValidationValues = true;
			}
			return _validationValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'ValidationValues'. These settings will be taken into account
		/// when the property ValidationValues is requested or GetMultiValidationValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersValidationValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_validationValues.SortClauses=sortClauses;
			_validationValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ValidationResultEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ValidationResultEntity' which is related to this entity.</returns>
		public ValidationResultEntity GetSingleValidationResult()
		{
			return GetSingleValidationResult(false);
		}

		/// <summary> Retrieves the related entity of type 'ValidationResultEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ValidationResultEntity' which is related to this entity.</returns>
		public virtual ValidationResultEntity GetSingleValidationResult(bool forceFetch)
		{
			if( ( !_alreadyFetchedValidationResult || forceFetch || _alwaysFetchValidationResult) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ValidationResultEntityUsingValidationResultIDOriginalValidationResultID);
				ValidationResultEntity newEntity = new ValidationResultEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OriginalValidationResultID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ValidationResultEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_validationResultReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ValidationResult = newEntity;
				_alreadyFetchedValidationResult = fetchResult;
			}
			return _validationResult;
		}


		/// <summary> Retrieves the related entity of type 'ValidationRuleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ValidationRuleEntity' which is related to this entity.</returns>
		public ValidationRuleEntity GetSingleValidationRule()
		{
			return GetSingleValidationRule(false);
		}

		/// <summary> Retrieves the related entity of type 'ValidationRuleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ValidationRuleEntity' which is related to this entity.</returns>
		public virtual ValidationRuleEntity GetSingleValidationRule(bool forceFetch)
		{
			if( ( !_alreadyFetchedValidationRule || forceFetch || _alwaysFetchValidationRule) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ValidationRuleEntityUsingValidationRuleID);
				ValidationRuleEntity newEntity = new ValidationRuleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ValidationRuleID);
				}
				if(fetchResult)
				{
					newEntity = (ValidationRuleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_validationRuleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ValidationRule = newEntity;
				_alreadyFetchedValidationRule = fetchResult;
			}
			return _validationRule;
		}


		/// <summary> Retrieves the related entity of type 'ValidationRunRuleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ValidationRunRuleEntity' which is related to this entity.</returns>
		public ValidationRunRuleEntity GetSingleValidationRunRule()
		{
			return GetSingleValidationRunRule(false);
		}

		/// <summary> Retrieves the related entity of type 'ValidationRunRuleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ValidationRunRuleEntity' which is related to this entity.</returns>
		public virtual ValidationRunRuleEntity GetSingleValidationRunRule(bool forceFetch)
		{
			if( ( !_alreadyFetchedValidationRunRule || forceFetch || _alwaysFetchValidationRunRule) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ValidationRunRuleEntityUsingValidationRunRuleID);
				ValidationRunRuleEntity newEntity = new ValidationRunRuleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ValidationRunRuleID);
				}
				if(fetchResult)
				{
					newEntity = (ValidationRunRuleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_validationRunRuleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ValidationRunRule = newEntity;
				_alreadyFetchedValidationRunRule = fetchResult;
			}
			return _validationRunRule;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ValidationResult", _validationResult);
			toReturn.Add("ValidationRule", _validationRule);
			toReturn.Add("ValidationRunRule", _validationRunRule);
			toReturn.Add("ValidationResults", _validationResults);
			toReturn.Add("ValidationValues", _validationValues);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="validationResultID">PK value for ValidationResult which data should be fetched into this ValidationResult object</param>
		/// <param name="validator">The validator object for this ValidationResultEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 validationResultID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(validationResultID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_validationResults = new VarioSL.Entities.CollectionClasses.ValidationResultCollection();
			_validationResults.SetContainingEntityInfo(this, "ValidationResult");

			_validationValues = new VarioSL.Entities.CollectionClasses.ValidationValueCollection();
			_validationValues.SetContainingEntityInfo(this, "ValidationResult");
			_validationResultReturnsNewIfNotFound = false;
			_validationRuleReturnsNewIfNotFound = false;
			_validationRunRuleReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Checked", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Details", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Info", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OriginalValidationResultID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Resolved", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResolvedComment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResolvedType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResolvedUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationResultID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationRuleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationRunRuleID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _validationResult</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncValidationResult(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _validationResult, new PropertyChangedEventHandler( OnValidationResultPropertyChanged ), "ValidationResult", VarioSL.Entities.RelationClasses.StaticValidationResultRelations.ValidationResultEntityUsingValidationResultIDOriginalValidationResultIDStatic, true, signalRelatedEntity, "ValidationResults", resetFKFields, new int[] { (int)ValidationResultFieldIndex.OriginalValidationResultID } );		
			_validationResult = null;
		}
		
		/// <summary> setups the sync logic for member _validationResult</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncValidationResult(IEntityCore relatedEntity)
		{
			if(_validationResult!=relatedEntity)
			{		
				DesetupSyncValidationResult(true, true);
				_validationResult = (ValidationResultEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _validationResult, new PropertyChangedEventHandler( OnValidationResultPropertyChanged ), "ValidationResult", VarioSL.Entities.RelationClasses.StaticValidationResultRelations.ValidationResultEntityUsingValidationResultIDOriginalValidationResultIDStatic, true, ref _alreadyFetchedValidationResult, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnValidationResultPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _validationRule</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncValidationRule(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _validationRule, new PropertyChangedEventHandler( OnValidationRulePropertyChanged ), "ValidationRule", VarioSL.Entities.RelationClasses.StaticValidationResultRelations.ValidationRuleEntityUsingValidationRuleIDStatic, true, signalRelatedEntity, "ValidationResults", resetFKFields, new int[] { (int)ValidationResultFieldIndex.ValidationRuleID } );		
			_validationRule = null;
		}
		
		/// <summary> setups the sync logic for member _validationRule</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncValidationRule(IEntityCore relatedEntity)
		{
			if(_validationRule!=relatedEntity)
			{		
				DesetupSyncValidationRule(true, true);
				_validationRule = (ValidationRuleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _validationRule, new PropertyChangedEventHandler( OnValidationRulePropertyChanged ), "ValidationRule", VarioSL.Entities.RelationClasses.StaticValidationResultRelations.ValidationRuleEntityUsingValidationRuleIDStatic, true, ref _alreadyFetchedValidationRule, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnValidationRulePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _validationRunRule</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncValidationRunRule(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _validationRunRule, new PropertyChangedEventHandler( OnValidationRunRulePropertyChanged ), "ValidationRunRule", VarioSL.Entities.RelationClasses.StaticValidationResultRelations.ValidationRunRuleEntityUsingValidationRunRuleIDStatic, true, signalRelatedEntity, "ValidationResults", resetFKFields, new int[] { (int)ValidationResultFieldIndex.ValidationRunRuleID } );		
			_validationRunRule = null;
		}
		
		/// <summary> setups the sync logic for member _validationRunRule</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncValidationRunRule(IEntityCore relatedEntity)
		{
			if(_validationRunRule!=relatedEntity)
			{		
				DesetupSyncValidationRunRule(true, true);
				_validationRunRule = (ValidationRunRuleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _validationRunRule, new PropertyChangedEventHandler( OnValidationRunRulePropertyChanged ), "ValidationRunRule", VarioSL.Entities.RelationClasses.StaticValidationResultRelations.ValidationRunRuleEntityUsingValidationRunRuleIDStatic, true, ref _alreadyFetchedValidationRunRule, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnValidationRunRulePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="validationResultID">PK value for ValidationResult which data should be fetched into this ValidationResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 validationResultID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ValidationResultFieldIndex.ValidationResultID].ForcedCurrentValueWrite(validationResultID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateValidationResultDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ValidationResultEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ValidationResultRelations Relations
		{
			get	{ return new ValidationResultRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ValidationResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathValidationResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ValidationResultCollection(), (IEntityRelation)GetRelationsForField("ValidationResults")[0], (int)VarioSL.Entities.EntityType.ValidationResultEntity, (int)VarioSL.Entities.EntityType.ValidationResultEntity, 0, null, null, null, "ValidationResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ValidationValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathValidationValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ValidationValueCollection(), (IEntityRelation)GetRelationsForField("ValidationValues")[0], (int)VarioSL.Entities.EntityType.ValidationResultEntity, (int)VarioSL.Entities.EntityType.ValidationValueEntity, 0, null, null, null, "ValidationValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ValidationResult'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathValidationResult
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ValidationResultCollection(), (IEntityRelation)GetRelationsForField("ValidationResult")[0], (int)VarioSL.Entities.EntityType.ValidationResultEntity, (int)VarioSL.Entities.EntityType.ValidationResultEntity, 0, null, null, null, "ValidationResult", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ValidationRule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathValidationRule
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ValidationRuleCollection(), (IEntityRelation)GetRelationsForField("ValidationRule")[0], (int)VarioSL.Entities.EntityType.ValidationResultEntity, (int)VarioSL.Entities.EntityType.ValidationRuleEntity, 0, null, null, null, "ValidationRule", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ValidationRunRule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathValidationRunRule
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ValidationRunRuleCollection(), (IEntityRelation)GetRelationsForField("ValidationRunRule")[0], (int)VarioSL.Entities.EntityType.ValidationResultEntity, (int)VarioSL.Entities.EntityType.ValidationRunRuleEntity, 0, null, null, null, "ValidationRunRule", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Checked property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."CHECKED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Checked
		{
			get { return (System.DateTime)GetValue((int)ValidationResultFieldIndex.Checked, true); }
			set	{ SetValue((int)ValidationResultFieldIndex.Checked, value, true); }
		}

		/// <summary> The Details property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."DETAILS"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Details
		{
			get { return (System.String)GetValue((int)ValidationResultFieldIndex.Details, true); }
			set	{ SetValue((int)ValidationResultFieldIndex.Details, value, true); }
		}

		/// <summary> The Info property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."INFO"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Info
		{
			get { return (System.String)GetValue((int)ValidationResultFieldIndex.Info, true); }
			set	{ SetValue((int)ValidationResultFieldIndex.Info, value, true); }
		}

		/// <summary> The IsValid property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsValid
		{
			get { return (System.Boolean)GetValue((int)ValidationResultFieldIndex.IsValid, true); }
			set	{ SetValue((int)ValidationResultFieldIndex.IsValid, value, true); }
		}

		/// <summary> The LastModified property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ValidationResultFieldIndex.LastModified, true); }
			set	{ SetValue((int)ValidationResultFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ValidationResultFieldIndex.LastUser, true); }
			set	{ SetValue((int)ValidationResultFieldIndex.LastUser, value, true); }
		}

		/// <summary> The OriginalValidationResultID property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."ORIGINALVALIDATIONRESULTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OriginalValidationResultID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ValidationResultFieldIndex.OriginalValidationResultID, false); }
			set	{ SetValue((int)ValidationResultFieldIndex.OriginalValidationResultID, value, true); }
		}

		/// <summary> The Resolved property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."RESOLVED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Resolved
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ValidationResultFieldIndex.Resolved, false); }
			set	{ SetValue((int)ValidationResultFieldIndex.Resolved, value, true); }
		}

		/// <summary> The ResolvedComment property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."RESOLVEDCOMMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResolvedComment
		{
			get { return (System.String)GetValue((int)ValidationResultFieldIndex.ResolvedComment, true); }
			set	{ SetValue((int)ValidationResultFieldIndex.ResolvedComment, value, true); }
		}

		/// <summary> The ResolvedType property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."RESOLVEDTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.ValidationResultResolveType> ResolvedType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.ValidationResultResolveType>)GetValue((int)ValidationResultFieldIndex.ResolvedType, false); }
			set	{ SetValue((int)ValidationResultFieldIndex.ResolvedType, value, true); }
		}

		/// <summary> The ResolvedUser property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."RESOLVEDUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResolvedUser
		{
			get { return (System.String)GetValue((int)ValidationResultFieldIndex.ResolvedUser, true); }
			set	{ SetValue((int)ValidationResultFieldIndex.ResolvedUser, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ValidationResultFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ValidationResultFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidationResultID property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."VALIDATIONRESULTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ValidationResultID
		{
			get { return (System.Int64)GetValue((int)ValidationResultFieldIndex.ValidationResultID, true); }
			set	{ SetValue((int)ValidationResultFieldIndex.ValidationResultID, value, true); }
		}

		/// <summary> The ValidationRuleID property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."VALIDATIONRULEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ValidationRuleID
		{
			get { return (System.Int64)GetValue((int)ValidationResultFieldIndex.ValidationRuleID, true); }
			set	{ SetValue((int)ValidationResultFieldIndex.ValidationRuleID, value, true); }
		}

		/// <summary> The ValidationRunRuleID property of the Entity ValidationResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRESULT"."VALIDATIONRUNRULEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ValidationRunRuleID
		{
			get { return (System.Int64)GetValue((int)ValidationResultFieldIndex.ValidationRunRuleID, true); }
			set	{ SetValue((int)ValidationResultFieldIndex.ValidationRunRuleID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ValidationResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiValidationResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ValidationResultCollection ValidationResults
		{
			get	{ return GetMultiValidationResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ValidationResults. When set to true, ValidationResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ValidationResults is accessed. You can always execute/ a forced fetch by calling GetMultiValidationResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchValidationResults
		{
			get	{ return _alwaysFetchValidationResults; }
			set	{ _alwaysFetchValidationResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ValidationResults already has been fetched. Setting this property to false when ValidationResults has been fetched
		/// will clear the ValidationResults collection well. Setting this property to true while ValidationResults hasn't been fetched disables lazy loading for ValidationResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedValidationResults
		{
			get { return _alreadyFetchedValidationResults;}
			set 
			{
				if(_alreadyFetchedValidationResults && !value && (_validationResults != null))
				{
					_validationResults.Clear();
				}
				_alreadyFetchedValidationResults = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ValidationValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiValidationValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ValidationValueCollection ValidationValues
		{
			get	{ return GetMultiValidationValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ValidationValues. When set to true, ValidationValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ValidationValues is accessed. You can always execute/ a forced fetch by calling GetMultiValidationValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchValidationValues
		{
			get	{ return _alwaysFetchValidationValues; }
			set	{ _alwaysFetchValidationValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ValidationValues already has been fetched. Setting this property to false when ValidationValues has been fetched
		/// will clear the ValidationValues collection well. Setting this property to true while ValidationValues hasn't been fetched disables lazy loading for ValidationValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedValidationValues
		{
			get { return _alreadyFetchedValidationValues;}
			set 
			{
				if(_alreadyFetchedValidationValues && !value && (_validationValues != null))
				{
					_validationValues.Clear();
				}
				_alreadyFetchedValidationValues = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ValidationResultEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleValidationResult()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ValidationResultEntity ValidationResult
		{
			get	{ return GetSingleValidationResult(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncValidationResult(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ValidationResults", "ValidationResult", _validationResult, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ValidationResult. When set to true, ValidationResult is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ValidationResult is accessed. You can always execute a forced fetch by calling GetSingleValidationResult(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchValidationResult
		{
			get	{ return _alwaysFetchValidationResult; }
			set	{ _alwaysFetchValidationResult = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ValidationResult already has been fetched. Setting this property to false when ValidationResult has been fetched
		/// will set ValidationResult to null as well. Setting this property to true while ValidationResult hasn't been fetched disables lazy loading for ValidationResult</summary>
		[Browsable(false)]
		public bool AlreadyFetchedValidationResult
		{
			get { return _alreadyFetchedValidationResult;}
			set 
			{
				if(_alreadyFetchedValidationResult && !value)
				{
					this.ValidationResult = null;
				}
				_alreadyFetchedValidationResult = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ValidationResult is not found
		/// in the database. When set to true, ValidationResult will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ValidationResultReturnsNewIfNotFound
		{
			get	{ return _validationResultReturnsNewIfNotFound; }
			set { _validationResultReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ValidationRuleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleValidationRule()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ValidationRuleEntity ValidationRule
		{
			get	{ return GetSingleValidationRule(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncValidationRule(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ValidationResults", "ValidationRule", _validationRule, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ValidationRule. When set to true, ValidationRule is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ValidationRule is accessed. You can always execute a forced fetch by calling GetSingleValidationRule(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchValidationRule
		{
			get	{ return _alwaysFetchValidationRule; }
			set	{ _alwaysFetchValidationRule = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ValidationRule already has been fetched. Setting this property to false when ValidationRule has been fetched
		/// will set ValidationRule to null as well. Setting this property to true while ValidationRule hasn't been fetched disables lazy loading for ValidationRule</summary>
		[Browsable(false)]
		public bool AlreadyFetchedValidationRule
		{
			get { return _alreadyFetchedValidationRule;}
			set 
			{
				if(_alreadyFetchedValidationRule && !value)
				{
					this.ValidationRule = null;
				}
				_alreadyFetchedValidationRule = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ValidationRule is not found
		/// in the database. When set to true, ValidationRule will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ValidationRuleReturnsNewIfNotFound
		{
			get	{ return _validationRuleReturnsNewIfNotFound; }
			set { _validationRuleReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ValidationRunRuleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleValidationRunRule()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ValidationRunRuleEntity ValidationRunRule
		{
			get	{ return GetSingleValidationRunRule(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncValidationRunRule(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ValidationResults", "ValidationRunRule", _validationRunRule, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ValidationRunRule. When set to true, ValidationRunRule is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ValidationRunRule is accessed. You can always execute a forced fetch by calling GetSingleValidationRunRule(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchValidationRunRule
		{
			get	{ return _alwaysFetchValidationRunRule; }
			set	{ _alwaysFetchValidationRunRule = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ValidationRunRule already has been fetched. Setting this property to false when ValidationRunRule has been fetched
		/// will set ValidationRunRule to null as well. Setting this property to true while ValidationRunRule hasn't been fetched disables lazy loading for ValidationRunRule</summary>
		[Browsable(false)]
		public bool AlreadyFetchedValidationRunRule
		{
			get { return _alreadyFetchedValidationRunRule;}
			set 
			{
				if(_alreadyFetchedValidationRunRule && !value)
				{
					this.ValidationRunRule = null;
				}
				_alreadyFetchedValidationRunRule = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ValidationRunRule is not found
		/// in the database. When set to true, ValidationRunRule will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ValidationRunRuleReturnsNewIfNotFound
		{
			get	{ return _validationRunRuleReturnsNewIfNotFound; }
			set { _validationRunRuleReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ValidationResultEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
