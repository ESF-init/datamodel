﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FareMatrixEntry'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FareMatrixEntryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AttributeValueEntity _attributeValueDirection;
		private bool	_alwaysFetchAttributeValueDirection, _alreadyFetchedAttributeValueDirection, _attributeValueDirectionReturnsNewIfNotFound;
		private AttributeValueEntity _attributeValueDistance;
		private bool	_alwaysFetchAttributeValueDistance, _alreadyFetchedAttributeValueDistance, _attributeValueDistanceReturnsNewIfNotFound;
		private AttributeValueEntity _attributeValueLineFilter;
		private bool	_alwaysFetchAttributeValueLineFilter, _alreadyFetchedAttributeValueLineFilter, _attributeValueLineFilterReturnsNewIfNotFound;
		private AttributeValueEntity _attributeValueTariff;
		private bool	_alwaysFetchAttributeValueTariff, _alreadyFetchedAttributeValueTariff, _attributeValueTariffReturnsNewIfNotFound;
		private AttributeValueEntity _attributeValueUseDays;
		private bool	_alwaysFetchAttributeValueUseDays, _alreadyFetchedAttributeValueUseDays, _attributeValueUseDaysReturnsNewIfNotFound;
		private FareMatrixEntity _fareMatrix;
		private bool	_alwaysFetchFareMatrix, _alreadyFetchedFareMatrix, _fareMatrixReturnsNewIfNotFound;
		private FareStageEntity _fareStageBoarding;
		private bool	_alwaysFetchFareStageBoarding, _alreadyFetchedFareStageBoarding, _fareStageBoardingReturnsNewIfNotFound;
		private FareStageEntity _fareStageDestination;
		private bool	_alwaysFetchFareStageDestination, _alreadyFetchedFareStageDestination, _fareStageDestinationReturnsNewIfNotFound;
		private FareStageEntity _fareStageVia;
		private bool	_alwaysFetchFareStageVia, _alreadyFetchedFareStageVia, _fareStageViaReturnsNewIfNotFound;
		private RouteNameEntity _routeName;
		private bool	_alwaysFetchRouteName, _alreadyFetchedRouteName, _routeNameReturnsNewIfNotFound;
		private ServiceAllocationEntity _serviceAllocation;
		private bool	_alwaysFetchServiceAllocation, _alreadyFetchedServiceAllocation, _serviceAllocationReturnsNewIfNotFound;
		private TicketGroupEntity _ticketGroup;
		private bool	_alwaysFetchTicketGroup, _alreadyFetchedTicketGroup, _ticketGroupReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AttributeValueDirection</summary>
			public static readonly string AttributeValueDirection = "AttributeValueDirection";
			/// <summary>Member name AttributeValueDistance</summary>
			public static readonly string AttributeValueDistance = "AttributeValueDistance";
			/// <summary>Member name AttributeValueLineFilter</summary>
			public static readonly string AttributeValueLineFilter = "AttributeValueLineFilter";
			/// <summary>Member name AttributeValueTariff</summary>
			public static readonly string AttributeValueTariff = "AttributeValueTariff";
			/// <summary>Member name AttributeValueUseDays</summary>
			public static readonly string AttributeValueUseDays = "AttributeValueUseDays";
			/// <summary>Member name FareMatrix</summary>
			public static readonly string FareMatrix = "FareMatrix";
			/// <summary>Member name FareStageBoarding</summary>
			public static readonly string FareStageBoarding = "FareStageBoarding";
			/// <summary>Member name FareStageDestination</summary>
			public static readonly string FareStageDestination = "FareStageDestination";
			/// <summary>Member name FareStageVia</summary>
			public static readonly string FareStageVia = "FareStageVia";
			/// <summary>Member name RouteName</summary>
			public static readonly string RouteName = "RouteName";
			/// <summary>Member name ServiceAllocation</summary>
			public static readonly string ServiceAllocation = "ServiceAllocation";
			/// <summary>Member name TicketGroup</summary>
			public static readonly string TicketGroup = "TicketGroup";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FareMatrixEntryEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FareMatrixEntryEntity() :base("FareMatrixEntryEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="fareMatrixEntryID">PK value for FareMatrixEntry which data should be fetched into this FareMatrixEntry object</param>
		public FareMatrixEntryEntity(System.Int64 fareMatrixEntryID):base("FareMatrixEntryEntity")
		{
			InitClassFetch(fareMatrixEntryID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="fareMatrixEntryID">PK value for FareMatrixEntry which data should be fetched into this FareMatrixEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FareMatrixEntryEntity(System.Int64 fareMatrixEntryID, IPrefetchPath prefetchPathToUse):base("FareMatrixEntryEntity")
		{
			InitClassFetch(fareMatrixEntryID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="fareMatrixEntryID">PK value for FareMatrixEntry which data should be fetched into this FareMatrixEntry object</param>
		/// <param name="validator">The custom validator object for this FareMatrixEntryEntity</param>
		public FareMatrixEntryEntity(System.Int64 fareMatrixEntryID, IValidator validator):base("FareMatrixEntryEntity")
		{
			InitClassFetch(fareMatrixEntryID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FareMatrixEntryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_attributeValueDirection = (AttributeValueEntity)info.GetValue("_attributeValueDirection", typeof(AttributeValueEntity));
			if(_attributeValueDirection!=null)
			{
				_attributeValueDirection.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributeValueDirectionReturnsNewIfNotFound = info.GetBoolean("_attributeValueDirectionReturnsNewIfNotFound");
			_alwaysFetchAttributeValueDirection = info.GetBoolean("_alwaysFetchAttributeValueDirection");
			_alreadyFetchedAttributeValueDirection = info.GetBoolean("_alreadyFetchedAttributeValueDirection");

			_attributeValueDistance = (AttributeValueEntity)info.GetValue("_attributeValueDistance", typeof(AttributeValueEntity));
			if(_attributeValueDistance!=null)
			{
				_attributeValueDistance.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributeValueDistanceReturnsNewIfNotFound = info.GetBoolean("_attributeValueDistanceReturnsNewIfNotFound");
			_alwaysFetchAttributeValueDistance = info.GetBoolean("_alwaysFetchAttributeValueDistance");
			_alreadyFetchedAttributeValueDistance = info.GetBoolean("_alreadyFetchedAttributeValueDistance");

			_attributeValueLineFilter = (AttributeValueEntity)info.GetValue("_attributeValueLineFilter", typeof(AttributeValueEntity));
			if(_attributeValueLineFilter!=null)
			{
				_attributeValueLineFilter.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributeValueLineFilterReturnsNewIfNotFound = info.GetBoolean("_attributeValueLineFilterReturnsNewIfNotFound");
			_alwaysFetchAttributeValueLineFilter = info.GetBoolean("_alwaysFetchAttributeValueLineFilter");
			_alreadyFetchedAttributeValueLineFilter = info.GetBoolean("_alreadyFetchedAttributeValueLineFilter");

			_attributeValueTariff = (AttributeValueEntity)info.GetValue("_attributeValueTariff", typeof(AttributeValueEntity));
			if(_attributeValueTariff!=null)
			{
				_attributeValueTariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributeValueTariffReturnsNewIfNotFound = info.GetBoolean("_attributeValueTariffReturnsNewIfNotFound");
			_alwaysFetchAttributeValueTariff = info.GetBoolean("_alwaysFetchAttributeValueTariff");
			_alreadyFetchedAttributeValueTariff = info.GetBoolean("_alreadyFetchedAttributeValueTariff");

			_attributeValueUseDays = (AttributeValueEntity)info.GetValue("_attributeValueUseDays", typeof(AttributeValueEntity));
			if(_attributeValueUseDays!=null)
			{
				_attributeValueUseDays.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributeValueUseDaysReturnsNewIfNotFound = info.GetBoolean("_attributeValueUseDaysReturnsNewIfNotFound");
			_alwaysFetchAttributeValueUseDays = info.GetBoolean("_alwaysFetchAttributeValueUseDays");
			_alreadyFetchedAttributeValueUseDays = info.GetBoolean("_alreadyFetchedAttributeValueUseDays");

			_fareMatrix = (FareMatrixEntity)info.GetValue("_fareMatrix", typeof(FareMatrixEntity));
			if(_fareMatrix!=null)
			{
				_fareMatrix.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareMatrixReturnsNewIfNotFound = info.GetBoolean("_fareMatrixReturnsNewIfNotFound");
			_alwaysFetchFareMatrix = info.GetBoolean("_alwaysFetchFareMatrix");
			_alreadyFetchedFareMatrix = info.GetBoolean("_alreadyFetchedFareMatrix");

			_fareStageBoarding = (FareStageEntity)info.GetValue("_fareStageBoarding", typeof(FareStageEntity));
			if(_fareStageBoarding!=null)
			{
				_fareStageBoarding.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareStageBoardingReturnsNewIfNotFound = info.GetBoolean("_fareStageBoardingReturnsNewIfNotFound");
			_alwaysFetchFareStageBoarding = info.GetBoolean("_alwaysFetchFareStageBoarding");
			_alreadyFetchedFareStageBoarding = info.GetBoolean("_alreadyFetchedFareStageBoarding");

			_fareStageDestination = (FareStageEntity)info.GetValue("_fareStageDestination", typeof(FareStageEntity));
			if(_fareStageDestination!=null)
			{
				_fareStageDestination.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareStageDestinationReturnsNewIfNotFound = info.GetBoolean("_fareStageDestinationReturnsNewIfNotFound");
			_alwaysFetchFareStageDestination = info.GetBoolean("_alwaysFetchFareStageDestination");
			_alreadyFetchedFareStageDestination = info.GetBoolean("_alreadyFetchedFareStageDestination");

			_fareStageVia = (FareStageEntity)info.GetValue("_fareStageVia", typeof(FareStageEntity));
			if(_fareStageVia!=null)
			{
				_fareStageVia.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareStageViaReturnsNewIfNotFound = info.GetBoolean("_fareStageViaReturnsNewIfNotFound");
			_alwaysFetchFareStageVia = info.GetBoolean("_alwaysFetchFareStageVia");
			_alreadyFetchedFareStageVia = info.GetBoolean("_alreadyFetchedFareStageVia");

			_routeName = (RouteNameEntity)info.GetValue("_routeName", typeof(RouteNameEntity));
			if(_routeName!=null)
			{
				_routeName.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_routeNameReturnsNewIfNotFound = info.GetBoolean("_routeNameReturnsNewIfNotFound");
			_alwaysFetchRouteName = info.GetBoolean("_alwaysFetchRouteName");
			_alreadyFetchedRouteName = info.GetBoolean("_alreadyFetchedRouteName");

			_serviceAllocation = (ServiceAllocationEntity)info.GetValue("_serviceAllocation", typeof(ServiceAllocationEntity));
			if(_serviceAllocation!=null)
			{
				_serviceAllocation.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_serviceAllocationReturnsNewIfNotFound = info.GetBoolean("_serviceAllocationReturnsNewIfNotFound");
			_alwaysFetchServiceAllocation = info.GetBoolean("_alwaysFetchServiceAllocation");
			_alreadyFetchedServiceAllocation = info.GetBoolean("_alreadyFetchedServiceAllocation");

			_ticketGroup = (TicketGroupEntity)info.GetValue("_ticketGroup", typeof(TicketGroupEntity));
			if(_ticketGroup!=null)
			{
				_ticketGroup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketGroupReturnsNewIfNotFound = info.GetBoolean("_ticketGroupReturnsNewIfNotFound");
			_alwaysFetchTicketGroup = info.GetBoolean("_alwaysFetchTicketGroup");
			_alreadyFetchedTicketGroup = info.GetBoolean("_alreadyFetchedTicketGroup");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FareMatrixEntryFieldIndex)fieldIndex)
			{
				case FareMatrixEntryFieldIndex.BoardingID:
					DesetupSyncFareStageBoarding(true, false);
					_alreadyFetchedFareStageBoarding = false;
					break;
				case FareMatrixEntryFieldIndex.DestinationID:
					DesetupSyncFareStageDestination(true, false);
					_alreadyFetchedFareStageDestination = false;
					break;
				case FareMatrixEntryFieldIndex.DirectionAttributeID:
					DesetupSyncAttributeValueDirection(true, false);
					_alreadyFetchedAttributeValueDirection = false;
					break;
				case FareMatrixEntryFieldIndex.DistanceAttributeID:
					DesetupSyncAttributeValueDistance(true, false);
					_alreadyFetchedAttributeValueDistance = false;
					break;
				case FareMatrixEntryFieldIndex.FareMatrixID:
					DesetupSyncFareMatrix(true, false);
					_alreadyFetchedFareMatrix = false;
					break;
				case FareMatrixEntryFieldIndex.LineFilterAttributeID:
					DesetupSyncAttributeValueLineFilter(true, false);
					_alreadyFetchedAttributeValueLineFilter = false;
					break;
				case FareMatrixEntryFieldIndex.RouteNameID:
					DesetupSyncRouteName(true, false);
					_alreadyFetchedRouteName = false;
					break;
				case FareMatrixEntryFieldIndex.ServiceAllocationID:
					DesetupSyncServiceAllocation(true, false);
					_alreadyFetchedServiceAllocation = false;
					break;
				case FareMatrixEntryFieldIndex.TariffAttributeID:
					DesetupSyncAttributeValueTariff(true, false);
					_alreadyFetchedAttributeValueTariff = false;
					break;
				case FareMatrixEntryFieldIndex.TicketGroupID:
					DesetupSyncTicketGroup(true, false);
					_alreadyFetchedTicketGroup = false;
					break;
				case FareMatrixEntryFieldIndex.UseDaysAttributeID:
					DesetupSyncAttributeValueUseDays(true, false);
					_alreadyFetchedAttributeValueUseDays = false;
					break;
				case FareMatrixEntryFieldIndex.ViaID:
					DesetupSyncFareStageVia(true, false);
					_alreadyFetchedFareStageVia = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAttributeValueDirection = (_attributeValueDirection != null);
			_alreadyFetchedAttributeValueDistance = (_attributeValueDistance != null);
			_alreadyFetchedAttributeValueLineFilter = (_attributeValueLineFilter != null);
			_alreadyFetchedAttributeValueTariff = (_attributeValueTariff != null);
			_alreadyFetchedAttributeValueUseDays = (_attributeValueUseDays != null);
			_alreadyFetchedFareMatrix = (_fareMatrix != null);
			_alreadyFetchedFareStageBoarding = (_fareStageBoarding != null);
			_alreadyFetchedFareStageDestination = (_fareStageDestination != null);
			_alreadyFetchedFareStageVia = (_fareStageVia != null);
			_alreadyFetchedRouteName = (_routeName != null);
			_alreadyFetchedServiceAllocation = (_serviceAllocation != null);
			_alreadyFetchedTicketGroup = (_ticketGroup != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AttributeValueDirection":
					toReturn.Add(Relations.AttributeValueEntityUsingDirectionAttributeID);
					break;
				case "AttributeValueDistance":
					toReturn.Add(Relations.AttributeValueEntityUsingDistanceAttributeID);
					break;
				case "AttributeValueLineFilter":
					toReturn.Add(Relations.AttributeValueEntityUsingLineFilterAttributeID);
					break;
				case "AttributeValueTariff":
					toReturn.Add(Relations.AttributeValueEntityUsingTariffAttributeID);
					break;
				case "AttributeValueUseDays":
					toReturn.Add(Relations.AttributeValueEntityUsingUseDaysAttributeID);
					break;
				case "FareMatrix":
					toReturn.Add(Relations.FareMatrixEntityUsingFareMatrixID);
					break;
				case "FareStageBoarding":
					toReturn.Add(Relations.FareStageEntityUsingBoardingID);
					break;
				case "FareStageDestination":
					toReturn.Add(Relations.FareStageEntityUsingDestinationID);
					break;
				case "FareStageVia":
					toReturn.Add(Relations.FareStageEntityUsingViaID);
					break;
				case "RouteName":
					toReturn.Add(Relations.RouteNameEntityUsingRouteNameID);
					break;
				case "ServiceAllocation":
					toReturn.Add(Relations.ServiceAllocationEntityUsingServiceAllocationID);
					break;
				case "TicketGroup":
					toReturn.Add(Relations.TicketGroupEntityUsingTicketGroupID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_attributeValueDirection", (!this.MarkedForDeletion?_attributeValueDirection:null));
			info.AddValue("_attributeValueDirectionReturnsNewIfNotFound", _attributeValueDirectionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttributeValueDirection", _alwaysFetchAttributeValueDirection);
			info.AddValue("_alreadyFetchedAttributeValueDirection", _alreadyFetchedAttributeValueDirection);
			info.AddValue("_attributeValueDistance", (!this.MarkedForDeletion?_attributeValueDistance:null));
			info.AddValue("_attributeValueDistanceReturnsNewIfNotFound", _attributeValueDistanceReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttributeValueDistance", _alwaysFetchAttributeValueDistance);
			info.AddValue("_alreadyFetchedAttributeValueDistance", _alreadyFetchedAttributeValueDistance);
			info.AddValue("_attributeValueLineFilter", (!this.MarkedForDeletion?_attributeValueLineFilter:null));
			info.AddValue("_attributeValueLineFilterReturnsNewIfNotFound", _attributeValueLineFilterReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttributeValueLineFilter", _alwaysFetchAttributeValueLineFilter);
			info.AddValue("_alreadyFetchedAttributeValueLineFilter", _alreadyFetchedAttributeValueLineFilter);
			info.AddValue("_attributeValueTariff", (!this.MarkedForDeletion?_attributeValueTariff:null));
			info.AddValue("_attributeValueTariffReturnsNewIfNotFound", _attributeValueTariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttributeValueTariff", _alwaysFetchAttributeValueTariff);
			info.AddValue("_alreadyFetchedAttributeValueTariff", _alreadyFetchedAttributeValueTariff);
			info.AddValue("_attributeValueUseDays", (!this.MarkedForDeletion?_attributeValueUseDays:null));
			info.AddValue("_attributeValueUseDaysReturnsNewIfNotFound", _attributeValueUseDaysReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttributeValueUseDays", _alwaysFetchAttributeValueUseDays);
			info.AddValue("_alreadyFetchedAttributeValueUseDays", _alreadyFetchedAttributeValueUseDays);
			info.AddValue("_fareMatrix", (!this.MarkedForDeletion?_fareMatrix:null));
			info.AddValue("_fareMatrixReturnsNewIfNotFound", _fareMatrixReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareMatrix", _alwaysFetchFareMatrix);
			info.AddValue("_alreadyFetchedFareMatrix", _alreadyFetchedFareMatrix);
			info.AddValue("_fareStageBoarding", (!this.MarkedForDeletion?_fareStageBoarding:null));
			info.AddValue("_fareStageBoardingReturnsNewIfNotFound", _fareStageBoardingReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareStageBoarding", _alwaysFetchFareStageBoarding);
			info.AddValue("_alreadyFetchedFareStageBoarding", _alreadyFetchedFareStageBoarding);
			info.AddValue("_fareStageDestination", (!this.MarkedForDeletion?_fareStageDestination:null));
			info.AddValue("_fareStageDestinationReturnsNewIfNotFound", _fareStageDestinationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareStageDestination", _alwaysFetchFareStageDestination);
			info.AddValue("_alreadyFetchedFareStageDestination", _alreadyFetchedFareStageDestination);
			info.AddValue("_fareStageVia", (!this.MarkedForDeletion?_fareStageVia:null));
			info.AddValue("_fareStageViaReturnsNewIfNotFound", _fareStageViaReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareStageVia", _alwaysFetchFareStageVia);
			info.AddValue("_alreadyFetchedFareStageVia", _alreadyFetchedFareStageVia);
			info.AddValue("_routeName", (!this.MarkedForDeletion?_routeName:null));
			info.AddValue("_routeNameReturnsNewIfNotFound", _routeNameReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRouteName", _alwaysFetchRouteName);
			info.AddValue("_alreadyFetchedRouteName", _alreadyFetchedRouteName);
			info.AddValue("_serviceAllocation", (!this.MarkedForDeletion?_serviceAllocation:null));
			info.AddValue("_serviceAllocationReturnsNewIfNotFound", _serviceAllocationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchServiceAllocation", _alwaysFetchServiceAllocation);
			info.AddValue("_alreadyFetchedServiceAllocation", _alreadyFetchedServiceAllocation);
			info.AddValue("_ticketGroup", (!this.MarkedForDeletion?_ticketGroup:null));
			info.AddValue("_ticketGroupReturnsNewIfNotFound", _ticketGroupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicketGroup", _alwaysFetchTicketGroup);
			info.AddValue("_alreadyFetchedTicketGroup", _alreadyFetchedTicketGroup);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AttributeValueDirection":
					_alreadyFetchedAttributeValueDirection = true;
					this.AttributeValueDirection = (AttributeValueEntity)entity;
					break;
				case "AttributeValueDistance":
					_alreadyFetchedAttributeValueDistance = true;
					this.AttributeValueDistance = (AttributeValueEntity)entity;
					break;
				case "AttributeValueLineFilter":
					_alreadyFetchedAttributeValueLineFilter = true;
					this.AttributeValueLineFilter = (AttributeValueEntity)entity;
					break;
				case "AttributeValueTariff":
					_alreadyFetchedAttributeValueTariff = true;
					this.AttributeValueTariff = (AttributeValueEntity)entity;
					break;
				case "AttributeValueUseDays":
					_alreadyFetchedAttributeValueUseDays = true;
					this.AttributeValueUseDays = (AttributeValueEntity)entity;
					break;
				case "FareMatrix":
					_alreadyFetchedFareMatrix = true;
					this.FareMatrix = (FareMatrixEntity)entity;
					break;
				case "FareStageBoarding":
					_alreadyFetchedFareStageBoarding = true;
					this.FareStageBoarding = (FareStageEntity)entity;
					break;
				case "FareStageDestination":
					_alreadyFetchedFareStageDestination = true;
					this.FareStageDestination = (FareStageEntity)entity;
					break;
				case "FareStageVia":
					_alreadyFetchedFareStageVia = true;
					this.FareStageVia = (FareStageEntity)entity;
					break;
				case "RouteName":
					_alreadyFetchedRouteName = true;
					this.RouteName = (RouteNameEntity)entity;
					break;
				case "ServiceAllocation":
					_alreadyFetchedServiceAllocation = true;
					this.ServiceAllocation = (ServiceAllocationEntity)entity;
					break;
				case "TicketGroup":
					_alreadyFetchedTicketGroup = true;
					this.TicketGroup = (TicketGroupEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AttributeValueDirection":
					SetupSyncAttributeValueDirection(relatedEntity);
					break;
				case "AttributeValueDistance":
					SetupSyncAttributeValueDistance(relatedEntity);
					break;
				case "AttributeValueLineFilter":
					SetupSyncAttributeValueLineFilter(relatedEntity);
					break;
				case "AttributeValueTariff":
					SetupSyncAttributeValueTariff(relatedEntity);
					break;
				case "AttributeValueUseDays":
					SetupSyncAttributeValueUseDays(relatedEntity);
					break;
				case "FareMatrix":
					SetupSyncFareMatrix(relatedEntity);
					break;
				case "FareStageBoarding":
					SetupSyncFareStageBoarding(relatedEntity);
					break;
				case "FareStageDestination":
					SetupSyncFareStageDestination(relatedEntity);
					break;
				case "FareStageVia":
					SetupSyncFareStageVia(relatedEntity);
					break;
				case "RouteName":
					SetupSyncRouteName(relatedEntity);
					break;
				case "ServiceAllocation":
					SetupSyncServiceAllocation(relatedEntity);
					break;
				case "TicketGroup":
					SetupSyncTicketGroup(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AttributeValueDirection":
					DesetupSyncAttributeValueDirection(false, true);
					break;
				case "AttributeValueDistance":
					DesetupSyncAttributeValueDistance(false, true);
					break;
				case "AttributeValueLineFilter":
					DesetupSyncAttributeValueLineFilter(false, true);
					break;
				case "AttributeValueTariff":
					DesetupSyncAttributeValueTariff(false, true);
					break;
				case "AttributeValueUseDays":
					DesetupSyncAttributeValueUseDays(false, true);
					break;
				case "FareMatrix":
					DesetupSyncFareMatrix(false, true);
					break;
				case "FareStageBoarding":
					DesetupSyncFareStageBoarding(false, true);
					break;
				case "FareStageDestination":
					DesetupSyncFareStageDestination(false, true);
					break;
				case "FareStageVia":
					DesetupSyncFareStageVia(false, true);
					break;
				case "RouteName":
					DesetupSyncRouteName(false, true);
					break;
				case "ServiceAllocation":
					DesetupSyncServiceAllocation(false, true);
					break;
				case "TicketGroup":
					DesetupSyncTicketGroup(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_attributeValueDirection!=null)
			{
				toReturn.Add(_attributeValueDirection);
			}
			if(_attributeValueDistance!=null)
			{
				toReturn.Add(_attributeValueDistance);
			}
			if(_attributeValueLineFilter!=null)
			{
				toReturn.Add(_attributeValueLineFilter);
			}
			if(_attributeValueTariff!=null)
			{
				toReturn.Add(_attributeValueTariff);
			}
			if(_attributeValueUseDays!=null)
			{
				toReturn.Add(_attributeValueUseDays);
			}
			if(_fareMatrix!=null)
			{
				toReturn.Add(_fareMatrix);
			}
			if(_fareStageBoarding!=null)
			{
				toReturn.Add(_fareStageBoarding);
			}
			if(_fareStageDestination!=null)
			{
				toReturn.Add(_fareStageDestination);
			}
			if(_fareStageVia!=null)
			{
				toReturn.Add(_fareStageVia);
			}
			if(_routeName!=null)
			{
				toReturn.Add(_routeName);
			}
			if(_serviceAllocation!=null)
			{
				toReturn.Add(_serviceAllocation);
			}
			if(_ticketGroup!=null)
			{
				toReturn.Add(_ticketGroup);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareMatrixEntryID">PK value for FareMatrixEntry which data should be fetched into this FareMatrixEntry object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareMatrixEntryID)
		{
			return FetchUsingPK(fareMatrixEntryID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareMatrixEntryID">PK value for FareMatrixEntry which data should be fetched into this FareMatrixEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareMatrixEntryID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(fareMatrixEntryID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareMatrixEntryID">PK value for FareMatrixEntry which data should be fetched into this FareMatrixEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareMatrixEntryID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(fareMatrixEntryID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareMatrixEntryID">PK value for FareMatrixEntry which data should be fetched into this FareMatrixEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareMatrixEntryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(fareMatrixEntryID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FareMatrixEntryID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FareMatrixEntryRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleAttributeValueDirection()
		{
			return GetSingleAttributeValueDirection(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleAttributeValueDirection(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttributeValueDirection || forceFetch || _alwaysFetchAttributeValueDirection) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingDirectionAttributeID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DirectionAttributeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributeValueDirectionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AttributeValueDirection = newEntity;
				_alreadyFetchedAttributeValueDirection = fetchResult;
			}
			return _attributeValueDirection;
		}


		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleAttributeValueDistance()
		{
			return GetSingleAttributeValueDistance(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleAttributeValueDistance(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttributeValueDistance || forceFetch || _alwaysFetchAttributeValueDistance) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingDistanceAttributeID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DistanceAttributeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributeValueDistanceReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AttributeValueDistance = newEntity;
				_alreadyFetchedAttributeValueDistance = fetchResult;
			}
			return _attributeValueDistance;
		}


		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleAttributeValueLineFilter()
		{
			return GetSingleAttributeValueLineFilter(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleAttributeValueLineFilter(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttributeValueLineFilter || forceFetch || _alwaysFetchAttributeValueLineFilter) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingLineFilterAttributeID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LineFilterAttributeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributeValueLineFilterReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AttributeValueLineFilter = newEntity;
				_alreadyFetchedAttributeValueLineFilter = fetchResult;
			}
			return _attributeValueLineFilter;
		}


		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleAttributeValueTariff()
		{
			return GetSingleAttributeValueTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleAttributeValueTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttributeValueTariff || forceFetch || _alwaysFetchAttributeValueTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingTariffAttributeID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffAttributeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributeValueTariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AttributeValueTariff = newEntity;
				_alreadyFetchedAttributeValueTariff = fetchResult;
			}
			return _attributeValueTariff;
		}


		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleAttributeValueUseDays()
		{
			return GetSingleAttributeValueUseDays(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleAttributeValueUseDays(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttributeValueUseDays || forceFetch || _alwaysFetchAttributeValueUseDays) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingUseDaysAttributeID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UseDaysAttributeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributeValueUseDaysReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AttributeValueUseDays = newEntity;
				_alreadyFetchedAttributeValueUseDays = fetchResult;
			}
			return _attributeValueUseDays;
		}


		/// <summary> Retrieves the related entity of type 'FareMatrixEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareMatrixEntity' which is related to this entity.</returns>
		public FareMatrixEntity GetSingleFareMatrix()
		{
			return GetSingleFareMatrix(false);
		}

		/// <summary> Retrieves the related entity of type 'FareMatrixEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareMatrixEntity' which is related to this entity.</returns>
		public virtual FareMatrixEntity GetSingleFareMatrix(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareMatrix || forceFetch || _alwaysFetchFareMatrix) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareMatrixEntityUsingFareMatrixID);
				FareMatrixEntity newEntity = new FareMatrixEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareMatrixID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareMatrixEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareMatrixReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareMatrix = newEntity;
				_alreadyFetchedFareMatrix = fetchResult;
			}
			return _fareMatrix;
		}


		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public FareStageEntity GetSingleFareStageBoarding()
		{
			return GetSingleFareStageBoarding(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public virtual FareStageEntity GetSingleFareStageBoarding(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareStageBoarding || forceFetch || _alwaysFetchFareStageBoarding) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageEntityUsingBoardingID);
				FareStageEntity newEntity = new FareStageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BoardingID);
				}
				if(fetchResult)
				{
					newEntity = (FareStageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareStageBoardingReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareStageBoarding = newEntity;
				_alreadyFetchedFareStageBoarding = fetchResult;
			}
			return _fareStageBoarding;
		}


		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public FareStageEntity GetSingleFareStageDestination()
		{
			return GetSingleFareStageDestination(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public virtual FareStageEntity GetSingleFareStageDestination(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareStageDestination || forceFetch || _alwaysFetchFareStageDestination) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageEntityUsingDestinationID);
				FareStageEntity newEntity = new FareStageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DestinationID);
				}
				if(fetchResult)
				{
					newEntity = (FareStageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareStageDestinationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareStageDestination = newEntity;
				_alreadyFetchedFareStageDestination = fetchResult;
			}
			return _fareStageDestination;
		}


		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public FareStageEntity GetSingleFareStageVia()
		{
			return GetSingleFareStageVia(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public virtual FareStageEntity GetSingleFareStageVia(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareStageVia || forceFetch || _alwaysFetchFareStageVia) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageEntityUsingViaID);
				FareStageEntity newEntity = new FareStageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ViaID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareStageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareStageViaReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareStageVia = newEntity;
				_alreadyFetchedFareStageVia = fetchResult;
			}
			return _fareStageVia;
		}


		/// <summary> Retrieves the related entity of type 'RouteNameEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RouteNameEntity' which is related to this entity.</returns>
		public RouteNameEntity GetSingleRouteName()
		{
			return GetSingleRouteName(false);
		}

		/// <summary> Retrieves the related entity of type 'RouteNameEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RouteNameEntity' which is related to this entity.</returns>
		public virtual RouteNameEntity GetSingleRouteName(bool forceFetch)
		{
			if( ( !_alreadyFetchedRouteName || forceFetch || _alwaysFetchRouteName) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RouteNameEntityUsingRouteNameID);
				RouteNameEntity newEntity = new RouteNameEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RouteNameID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RouteNameEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_routeNameReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RouteName = newEntity;
				_alreadyFetchedRouteName = fetchResult;
			}
			return _routeName;
		}


		/// <summary> Retrieves the related entity of type 'ServiceAllocationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ServiceAllocationEntity' which is related to this entity.</returns>
		public ServiceAllocationEntity GetSingleServiceAllocation()
		{
			return GetSingleServiceAllocation(false);
		}

		/// <summary> Retrieves the related entity of type 'ServiceAllocationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ServiceAllocationEntity' which is related to this entity.</returns>
		public virtual ServiceAllocationEntity GetSingleServiceAllocation(bool forceFetch)
		{
			if( ( !_alreadyFetchedServiceAllocation || forceFetch || _alwaysFetchServiceAllocation) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ServiceAllocationEntityUsingServiceAllocationID);
				ServiceAllocationEntity newEntity = new ServiceAllocationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ServiceAllocationID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ServiceAllocationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_serviceAllocationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ServiceAllocation = newEntity;
				_alreadyFetchedServiceAllocation = fetchResult;
			}
			return _serviceAllocation;
		}


		/// <summary> Retrieves the related entity of type 'TicketGroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketGroupEntity' which is related to this entity.</returns>
		public TicketGroupEntity GetSingleTicketGroup()
		{
			return GetSingleTicketGroup(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketGroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketGroupEntity' which is related to this entity.</returns>
		public virtual TicketGroupEntity GetSingleTicketGroup(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicketGroup || forceFetch || _alwaysFetchTicketGroup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketGroupEntityUsingTicketGroupID);
				TicketGroupEntity newEntity = new TicketGroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketGroupID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketGroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketGroupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TicketGroup = newEntity;
				_alreadyFetchedTicketGroup = fetchResult;
			}
			return _ticketGroup;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AttributeValueDirection", _attributeValueDirection);
			toReturn.Add("AttributeValueDistance", _attributeValueDistance);
			toReturn.Add("AttributeValueLineFilter", _attributeValueLineFilter);
			toReturn.Add("AttributeValueTariff", _attributeValueTariff);
			toReturn.Add("AttributeValueUseDays", _attributeValueUseDays);
			toReturn.Add("FareMatrix", _fareMatrix);
			toReturn.Add("FareStageBoarding", _fareStageBoarding);
			toReturn.Add("FareStageDestination", _fareStageDestination);
			toReturn.Add("FareStageVia", _fareStageVia);
			toReturn.Add("RouteName", _routeName);
			toReturn.Add("ServiceAllocation", _serviceAllocation);
			toReturn.Add("TicketGroup", _ticketGroup);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="fareMatrixEntryID">PK value for FareMatrixEntry which data should be fetched into this FareMatrixEntry object</param>
		/// <param name="validator">The validator object for this FareMatrixEntryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 fareMatrixEntryID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(fareMatrixEntryID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_attributeValueDirectionReturnsNewIfNotFound = false;
			_attributeValueDistanceReturnsNewIfNotFound = false;
			_attributeValueLineFilterReturnsNewIfNotFound = false;
			_attributeValueTariffReturnsNewIfNotFound = false;
			_attributeValueUseDaysReturnsNewIfNotFound = false;
			_fareMatrixReturnsNewIfNotFound = false;
			_fareStageBoardingReturnsNewIfNotFound = false;
			_fareStageDestinationReturnsNewIfNotFound = false;
			_fareStageViaReturnsNewIfNotFound = false;
			_routeNameReturnsNewIfNotFound = false;
			_serviceAllocationReturnsNewIfNotFound = false;
			_ticketGroupReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AreaListKeyBack", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AreaListKeyForth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AverageDistance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoardingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DescriptionReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DestinationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DirectionAttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Distance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DistanceAttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Fare", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareMatrixEntryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareMatrixID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineFilterAttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Locked", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LongName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Priority", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RealDistance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteNameID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceAllocationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShortName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffAttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseDaysAttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ViaID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _attributeValueDirection</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttributeValueDirection(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attributeValueDirection, new PropertyChangedEventHandler( OnAttributeValueDirectionPropertyChanged ), "AttributeValueDirection", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.AttributeValueEntityUsingDirectionAttributeIDStatic, true, signalRelatedEntity, "DirectionAttributeValueFareMatrixEntries", resetFKFields, new int[] { (int)FareMatrixEntryFieldIndex.DirectionAttributeID } );		
			_attributeValueDirection = null;
		}
		
		/// <summary> setups the sync logic for member _attributeValueDirection</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttributeValueDirection(IEntityCore relatedEntity)
		{
			if(_attributeValueDirection!=relatedEntity)
			{		
				DesetupSyncAttributeValueDirection(true, true);
				_attributeValueDirection = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attributeValueDirection, new PropertyChangedEventHandler( OnAttributeValueDirectionPropertyChanged ), "AttributeValueDirection", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.AttributeValueEntityUsingDirectionAttributeIDStatic, true, ref _alreadyFetchedAttributeValueDirection, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributeValueDirectionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _attributeValueDistance</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttributeValueDistance(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attributeValueDistance, new PropertyChangedEventHandler( OnAttributeValueDistancePropertyChanged ), "AttributeValueDistance", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.AttributeValueEntityUsingDistanceAttributeIDStatic, true, signalRelatedEntity, "DistanceAttributeValueFareMatrixEntries", resetFKFields, new int[] { (int)FareMatrixEntryFieldIndex.DistanceAttributeID } );		
			_attributeValueDistance = null;
		}
		
		/// <summary> setups the sync logic for member _attributeValueDistance</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttributeValueDistance(IEntityCore relatedEntity)
		{
			if(_attributeValueDistance!=relatedEntity)
			{		
				DesetupSyncAttributeValueDistance(true, true);
				_attributeValueDistance = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attributeValueDistance, new PropertyChangedEventHandler( OnAttributeValueDistancePropertyChanged ), "AttributeValueDistance", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.AttributeValueEntityUsingDistanceAttributeIDStatic, true, ref _alreadyFetchedAttributeValueDistance, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributeValueDistancePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _attributeValueLineFilter</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttributeValueLineFilter(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attributeValueLineFilter, new PropertyChangedEventHandler( OnAttributeValueLineFilterPropertyChanged ), "AttributeValueLineFilter", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.AttributeValueEntityUsingLineFilterAttributeIDStatic, true, signalRelatedEntity, "LineFilterAttributeValueFareMatrixEntries", resetFKFields, new int[] { (int)FareMatrixEntryFieldIndex.LineFilterAttributeID } );		
			_attributeValueLineFilter = null;
		}
		
		/// <summary> setups the sync logic for member _attributeValueLineFilter</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttributeValueLineFilter(IEntityCore relatedEntity)
		{
			if(_attributeValueLineFilter!=relatedEntity)
			{		
				DesetupSyncAttributeValueLineFilter(true, true);
				_attributeValueLineFilter = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attributeValueLineFilter, new PropertyChangedEventHandler( OnAttributeValueLineFilterPropertyChanged ), "AttributeValueLineFilter", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.AttributeValueEntityUsingLineFilterAttributeIDStatic, true, ref _alreadyFetchedAttributeValueLineFilter, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributeValueLineFilterPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _attributeValueTariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttributeValueTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attributeValueTariff, new PropertyChangedEventHandler( OnAttributeValueTariffPropertyChanged ), "AttributeValueTariff", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.AttributeValueEntityUsingTariffAttributeIDStatic, true, signalRelatedEntity, "TariffAttributeValueFareMatrixEntries", resetFKFields, new int[] { (int)FareMatrixEntryFieldIndex.TariffAttributeID } );		
			_attributeValueTariff = null;
		}
		
		/// <summary> setups the sync logic for member _attributeValueTariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttributeValueTariff(IEntityCore relatedEntity)
		{
			if(_attributeValueTariff!=relatedEntity)
			{		
				DesetupSyncAttributeValueTariff(true, true);
				_attributeValueTariff = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attributeValueTariff, new PropertyChangedEventHandler( OnAttributeValueTariffPropertyChanged ), "AttributeValueTariff", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.AttributeValueEntityUsingTariffAttributeIDStatic, true, ref _alreadyFetchedAttributeValueTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributeValueTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _attributeValueUseDays</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttributeValueUseDays(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attributeValueUseDays, new PropertyChangedEventHandler( OnAttributeValueUseDaysPropertyChanged ), "AttributeValueUseDays", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.AttributeValueEntityUsingUseDaysAttributeIDStatic, true, signalRelatedEntity, "UseDaysAttributeFareMatrixEntries", resetFKFields, new int[] { (int)FareMatrixEntryFieldIndex.UseDaysAttributeID } );		
			_attributeValueUseDays = null;
		}
		
		/// <summary> setups the sync logic for member _attributeValueUseDays</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttributeValueUseDays(IEntityCore relatedEntity)
		{
			if(_attributeValueUseDays!=relatedEntity)
			{		
				DesetupSyncAttributeValueUseDays(true, true);
				_attributeValueUseDays = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attributeValueUseDays, new PropertyChangedEventHandler( OnAttributeValueUseDaysPropertyChanged ), "AttributeValueUseDays", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.AttributeValueEntityUsingUseDaysAttributeIDStatic, true, ref _alreadyFetchedAttributeValueUseDays, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributeValueUseDaysPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareMatrix</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareMatrix(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareMatrix, new PropertyChangedEventHandler( OnFareMatrixPropertyChanged ), "FareMatrix", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.FareMatrixEntityUsingFareMatrixIDStatic, true, signalRelatedEntity, "FareMatrixEntry", resetFKFields, new int[] { (int)FareMatrixEntryFieldIndex.FareMatrixID } );		
			_fareMatrix = null;
		}
		
		/// <summary> setups the sync logic for member _fareMatrix</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareMatrix(IEntityCore relatedEntity)
		{
			if(_fareMatrix!=relatedEntity)
			{		
				DesetupSyncFareMatrix(true, true);
				_fareMatrix = (FareMatrixEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareMatrix, new PropertyChangedEventHandler( OnFareMatrixPropertyChanged ), "FareMatrix", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.FareMatrixEntityUsingFareMatrixIDStatic, true, ref _alreadyFetchedFareMatrix, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareMatrixPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareStageBoarding</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareStageBoarding(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareStageBoarding, new PropertyChangedEventHandler( OnFareStageBoardingPropertyChanged ), "FareStageBoarding", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.FareStageEntityUsingBoardingIDStatic, true, signalRelatedEntity, "FareMatrixEntryBoarding", resetFKFields, new int[] { (int)FareMatrixEntryFieldIndex.BoardingID } );		
			_fareStageBoarding = null;
		}
		
		/// <summary> setups the sync logic for member _fareStageBoarding</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareStageBoarding(IEntityCore relatedEntity)
		{
			if(_fareStageBoarding!=relatedEntity)
			{		
				DesetupSyncFareStageBoarding(true, true);
				_fareStageBoarding = (FareStageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareStageBoarding, new PropertyChangedEventHandler( OnFareStageBoardingPropertyChanged ), "FareStageBoarding", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.FareStageEntityUsingBoardingIDStatic, true, ref _alreadyFetchedFareStageBoarding, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareStageBoardingPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareStageDestination</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareStageDestination(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareStageDestination, new PropertyChangedEventHandler( OnFareStageDestinationPropertyChanged ), "FareStageDestination", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.FareStageEntityUsingDestinationIDStatic, true, signalRelatedEntity, "FareMatrixEntryDestination", resetFKFields, new int[] { (int)FareMatrixEntryFieldIndex.DestinationID } );		
			_fareStageDestination = null;
		}
		
		/// <summary> setups the sync logic for member _fareStageDestination</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareStageDestination(IEntityCore relatedEntity)
		{
			if(_fareStageDestination!=relatedEntity)
			{		
				DesetupSyncFareStageDestination(true, true);
				_fareStageDestination = (FareStageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareStageDestination, new PropertyChangedEventHandler( OnFareStageDestinationPropertyChanged ), "FareStageDestination", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.FareStageEntityUsingDestinationIDStatic, true, ref _alreadyFetchedFareStageDestination, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareStageDestinationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareStageVia</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareStageVia(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareStageVia, new PropertyChangedEventHandler( OnFareStageViaPropertyChanged ), "FareStageVia", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.FareStageEntityUsingViaIDStatic, true, signalRelatedEntity, "FareMatrixEntryVia", resetFKFields, new int[] { (int)FareMatrixEntryFieldIndex.ViaID } );		
			_fareStageVia = null;
		}
		
		/// <summary> setups the sync logic for member _fareStageVia</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareStageVia(IEntityCore relatedEntity)
		{
			if(_fareStageVia!=relatedEntity)
			{		
				DesetupSyncFareStageVia(true, true);
				_fareStageVia = (FareStageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareStageVia, new PropertyChangedEventHandler( OnFareStageViaPropertyChanged ), "FareStageVia", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.FareStageEntityUsingViaIDStatic, true, ref _alreadyFetchedFareStageVia, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareStageViaPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _routeName</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRouteName(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _routeName, new PropertyChangedEventHandler( OnRouteNamePropertyChanged ), "RouteName", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.RouteNameEntityUsingRouteNameIDStatic, true, signalRelatedEntity, "FareMatrixEntry", resetFKFields, new int[] { (int)FareMatrixEntryFieldIndex.RouteNameID } );		
			_routeName = null;
		}
		
		/// <summary> setups the sync logic for member _routeName</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRouteName(IEntityCore relatedEntity)
		{
			if(_routeName!=relatedEntity)
			{		
				DesetupSyncRouteName(true, true);
				_routeName = (RouteNameEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _routeName, new PropertyChangedEventHandler( OnRouteNamePropertyChanged ), "RouteName", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.RouteNameEntityUsingRouteNameIDStatic, true, ref _alreadyFetchedRouteName, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRouteNamePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _serviceAllocation</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncServiceAllocation(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _serviceAllocation, new PropertyChangedEventHandler( OnServiceAllocationPropertyChanged ), "ServiceAllocation", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.ServiceAllocationEntityUsingServiceAllocationIDStatic, true, signalRelatedEntity, "FareMatrixEntry", resetFKFields, new int[] { (int)FareMatrixEntryFieldIndex.ServiceAllocationID } );		
			_serviceAllocation = null;
		}
		
		/// <summary> setups the sync logic for member _serviceAllocation</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncServiceAllocation(IEntityCore relatedEntity)
		{
			if(_serviceAllocation!=relatedEntity)
			{		
				DesetupSyncServiceAllocation(true, true);
				_serviceAllocation = (ServiceAllocationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _serviceAllocation, new PropertyChangedEventHandler( OnServiceAllocationPropertyChanged ), "ServiceAllocation", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.ServiceAllocationEntityUsingServiceAllocationIDStatic, true, ref _alreadyFetchedServiceAllocation, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnServiceAllocationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticketGroup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicketGroup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticketGroup, new PropertyChangedEventHandler( OnTicketGroupPropertyChanged ), "TicketGroup", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.TicketGroupEntityUsingTicketGroupIDStatic, true, signalRelatedEntity, "FareMatrixEntry", resetFKFields, new int[] { (int)FareMatrixEntryFieldIndex.TicketGroupID } );		
			_ticketGroup = null;
		}
		
		/// <summary> setups the sync logic for member _ticketGroup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicketGroup(IEntityCore relatedEntity)
		{
			if(_ticketGroup!=relatedEntity)
			{		
				DesetupSyncTicketGroup(true, true);
				_ticketGroup = (TicketGroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticketGroup, new PropertyChangedEventHandler( OnTicketGroupPropertyChanged ), "TicketGroup", VarioSL.Entities.RelationClasses.StaticFareMatrixEntryRelations.TicketGroupEntityUsingTicketGroupIDStatic, true, ref _alreadyFetchedTicketGroup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketGroupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="fareMatrixEntryID">PK value for FareMatrixEntry which data should be fetched into this FareMatrixEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 fareMatrixEntryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FareMatrixEntryFieldIndex.FareMatrixEntryID].ForcedCurrentValueWrite(fareMatrixEntryID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFareMatrixEntryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FareMatrixEntryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FareMatrixEntryRelations Relations
		{
			get	{ return new FareMatrixEntryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValueDirection
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("AttributeValueDirection")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "AttributeValueDirection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValueDistance
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("AttributeValueDistance")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "AttributeValueDistance", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValueLineFilter
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("AttributeValueLineFilter")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "AttributeValueLineFilter", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValueTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("AttributeValueTariff")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "AttributeValueTariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValueUseDays
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("AttributeValueUseDays")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "AttributeValueUseDays", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrix'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareMatrix
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixCollection(), (IEntityRelation)GetRelationsForField("FareMatrix")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntity, 0, null, null, null, "FareMatrix", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStageBoarding
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageCollection(), (IEntityRelation)GetRelationsForField("FareStageBoarding")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, (int)VarioSL.Entities.EntityType.FareStageEntity, 0, null, null, null, "FareStageBoarding", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStageDestination
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageCollection(), (IEntityRelation)GetRelationsForField("FareStageDestination")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, (int)VarioSL.Entities.EntityType.FareStageEntity, 0, null, null, null, "FareStageDestination", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStageVia
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageCollection(), (IEntityRelation)GetRelationsForField("FareStageVia")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, (int)VarioSL.Entities.EntityType.FareStageEntity, 0, null, null, null, "FareStageVia", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RouteName'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteName
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RouteNameCollection(), (IEntityRelation)GetRelationsForField("RouteName")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, (int)VarioSL.Entities.EntityType.RouteNameEntity, 0, null, null, null, "RouteName", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ServiceAllocation'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathServiceAllocation
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ServiceAllocationCollection(), (IEntityRelation)GetRelationsForField("ServiceAllocation")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, (int)VarioSL.Entities.EntityType.ServiceAllocationEntity, 0, null, null, null, "ServiceAllocation", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketGroup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketGroupCollection(), (IEntityRelation)GetRelationsForField("TicketGroup")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, (int)VarioSL.Entities.EntityType.TicketGroupEntity, 0, null, null, null, "TicketGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AreaListKeyBack property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."AREALISTKEYBACK"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AreaListKeyBack
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.AreaListKeyBack, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.AreaListKeyBack, value, true); }
		}

		/// <summary> The AreaListKeyForth property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."AREALISTKEYFORTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AreaListKeyForth
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.AreaListKeyForth, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.AreaListKeyForth, value, true); }
		}

		/// <summary> The AverageDistance property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."AVERAGEDISTANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> AverageDistance
		{
			get { return (Nullable<System.Double>)GetValue((int)FareMatrixEntryFieldIndex.AverageDistance, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.AverageDistance, value, true); }
		}

		/// <summary> The BoardingID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."BOARDINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 BoardingID
		{
			get { return (System.Int64)GetValue((int)FareMatrixEntryFieldIndex.BoardingID, true); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.BoardingID, value, true); }
		}

		/// <summary> The Description property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)FareMatrixEntryFieldIndex.Description, true); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.Description, value, true); }
		}

		/// <summary> The DescriptionReference property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."DESCRIPTIONREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DescriptionReference
		{
			get { return (System.String)GetValue((int)FareMatrixEntryFieldIndex.DescriptionReference, true); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.DescriptionReference, value, true); }
		}

		/// <summary> The DestinationID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."DESTINATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DestinationID
		{
			get { return (System.Int64)GetValue((int)FareMatrixEntryFieldIndex.DestinationID, true); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.DestinationID, value, true); }
		}

		/// <summary> The DirectionAttributeID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."DIRECTIONATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DirectionAttributeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.DirectionAttributeID, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.DirectionAttributeID, value, true); }
		}

		/// <summary> The Distance property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."DISTANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> Distance
		{
			get { return (Nullable<System.Double>)GetValue((int)FareMatrixEntryFieldIndex.Distance, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.Distance, value, true); }
		}

		/// <summary> The DistanceAttributeID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."DISTANCEATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DistanceAttributeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.DistanceAttributeID, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.DistanceAttributeID, value, true); }
		}

		/// <summary> The Fare property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."FARE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Fare
		{
			get { return (System.Int64)GetValue((int)FareMatrixEntryFieldIndex.Fare, true); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.Fare, value, true); }
		}

		/// <summary> The FareMatrixEntryID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."FAREMATRIXENTRYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 FareMatrixEntryID
		{
			get { return (System.Int64)GetValue((int)FareMatrixEntryFieldIndex.FareMatrixEntryID, true); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.FareMatrixEntryID, value, true); }
		}

		/// <summary> The FareMatrixID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."FAREMATRIXID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareMatrixID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.FareMatrixID, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.FareMatrixID, value, true); }
		}

		/// <summary> The LineFilterAttributeID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."LINEFILTERATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineFilterAttributeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.LineFilterAttributeID, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.LineFilterAttributeID, value, true); }
		}

		/// <summary> The Locked property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."LOCKED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Locked
		{
			get { return (Nullable<System.Boolean>)GetValue((int)FareMatrixEntryFieldIndex.Locked, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.Locked, value, true); }
		}

		/// <summary> The LongName property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."LONGNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LongName
		{
			get { return (System.String)GetValue((int)FareMatrixEntryFieldIndex.LongName, true); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.LongName, value, true); }
		}

		/// <summary> The Priority property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."PRIORITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Priority
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.Priority, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.Priority, value, true); }
		}

		/// <summary> The RealDistance property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."REALDISTANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> RealDistance
		{
			get { return (Nullable<System.Double>)GetValue((int)FareMatrixEntryFieldIndex.RealDistance, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.RealDistance, value, true); }
		}

		/// <summary> The RouteNameID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."ROUTENAMEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RouteNameID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.RouteNameID, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.RouteNameID, value, true); }
		}

		/// <summary> The SalesFlag property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."SALESFLAG"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> SalesFlag
		{
			get { return (Nullable<System.Boolean>)GetValue((int)FareMatrixEntryFieldIndex.SalesFlag, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.SalesFlag, value, true); }
		}

		/// <summary> The ServiceAllocationID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."SERVICEALLOCATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ServiceAllocationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.ServiceAllocationID, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.ServiceAllocationID, value, true); }
		}

		/// <summary> The ShortName property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."SHORTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShortName
		{
			get { return (System.String)GetValue((int)FareMatrixEntryFieldIndex.ShortName, true); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.ShortName, value, true); }
		}

		/// <summary> The TariffAttributeID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."TARIFFATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffAttributeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.TariffAttributeID, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.TariffAttributeID, value, true); }
		}

		/// <summary> The TicketGroupID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."TICKETGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.TicketGroupID, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.TicketGroupID, value, true); }
		}

		/// <summary> The UseDaysAttributeID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."USEDAYSATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UseDaysAttributeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.UseDaysAttributeID, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.UseDaysAttributeID, value, true); }
		}

		/// <summary> The ViaID property of the Entity FareMatrixEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIXENTRY"."VIAID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ViaID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixEntryFieldIndex.ViaID, false); }
			set	{ SetValue((int)FareMatrixEntryFieldIndex.ViaID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttributeValueDirection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity AttributeValueDirection
		{
			get	{ return GetSingleAttributeValueDirection(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttributeValueDirection(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DirectionAttributeValueFareMatrixEntries", "AttributeValueDirection", _attributeValueDirection, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValueDirection. When set to true, AttributeValueDirection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValueDirection is accessed. You can always execute a forced fetch by calling GetSingleAttributeValueDirection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValueDirection
		{
			get	{ return _alwaysFetchAttributeValueDirection; }
			set	{ _alwaysFetchAttributeValueDirection = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValueDirection already has been fetched. Setting this property to false when AttributeValueDirection has been fetched
		/// will set AttributeValueDirection to null as well. Setting this property to true while AttributeValueDirection hasn't been fetched disables lazy loading for AttributeValueDirection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValueDirection
		{
			get { return _alreadyFetchedAttributeValueDirection;}
			set 
			{
				if(_alreadyFetchedAttributeValueDirection && !value)
				{
					this.AttributeValueDirection = null;
				}
				_alreadyFetchedAttributeValueDirection = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AttributeValueDirection is not found
		/// in the database. When set to true, AttributeValueDirection will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributeValueDirectionReturnsNewIfNotFound
		{
			get	{ return _attributeValueDirectionReturnsNewIfNotFound; }
			set { _attributeValueDirectionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttributeValueDistance()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity AttributeValueDistance
		{
			get	{ return GetSingleAttributeValueDistance(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttributeValueDistance(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DistanceAttributeValueFareMatrixEntries", "AttributeValueDistance", _attributeValueDistance, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValueDistance. When set to true, AttributeValueDistance is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValueDistance is accessed. You can always execute a forced fetch by calling GetSingleAttributeValueDistance(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValueDistance
		{
			get	{ return _alwaysFetchAttributeValueDistance; }
			set	{ _alwaysFetchAttributeValueDistance = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValueDistance already has been fetched. Setting this property to false when AttributeValueDistance has been fetched
		/// will set AttributeValueDistance to null as well. Setting this property to true while AttributeValueDistance hasn't been fetched disables lazy loading for AttributeValueDistance</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValueDistance
		{
			get { return _alreadyFetchedAttributeValueDistance;}
			set 
			{
				if(_alreadyFetchedAttributeValueDistance && !value)
				{
					this.AttributeValueDistance = null;
				}
				_alreadyFetchedAttributeValueDistance = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AttributeValueDistance is not found
		/// in the database. When set to true, AttributeValueDistance will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributeValueDistanceReturnsNewIfNotFound
		{
			get	{ return _attributeValueDistanceReturnsNewIfNotFound; }
			set { _attributeValueDistanceReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttributeValueLineFilter()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity AttributeValueLineFilter
		{
			get	{ return GetSingleAttributeValueLineFilter(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttributeValueLineFilter(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "LineFilterAttributeValueFareMatrixEntries", "AttributeValueLineFilter", _attributeValueLineFilter, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValueLineFilter. When set to true, AttributeValueLineFilter is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValueLineFilter is accessed. You can always execute a forced fetch by calling GetSingleAttributeValueLineFilter(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValueLineFilter
		{
			get	{ return _alwaysFetchAttributeValueLineFilter; }
			set	{ _alwaysFetchAttributeValueLineFilter = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValueLineFilter already has been fetched. Setting this property to false when AttributeValueLineFilter has been fetched
		/// will set AttributeValueLineFilter to null as well. Setting this property to true while AttributeValueLineFilter hasn't been fetched disables lazy loading for AttributeValueLineFilter</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValueLineFilter
		{
			get { return _alreadyFetchedAttributeValueLineFilter;}
			set 
			{
				if(_alreadyFetchedAttributeValueLineFilter && !value)
				{
					this.AttributeValueLineFilter = null;
				}
				_alreadyFetchedAttributeValueLineFilter = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AttributeValueLineFilter is not found
		/// in the database. When set to true, AttributeValueLineFilter will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributeValueLineFilterReturnsNewIfNotFound
		{
			get	{ return _attributeValueLineFilterReturnsNewIfNotFound; }
			set { _attributeValueLineFilterReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttributeValueTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity AttributeValueTariff
		{
			get	{ return GetSingleAttributeValueTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttributeValueTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TariffAttributeValueFareMatrixEntries", "AttributeValueTariff", _attributeValueTariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValueTariff. When set to true, AttributeValueTariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValueTariff is accessed. You can always execute a forced fetch by calling GetSingleAttributeValueTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValueTariff
		{
			get	{ return _alwaysFetchAttributeValueTariff; }
			set	{ _alwaysFetchAttributeValueTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValueTariff already has been fetched. Setting this property to false when AttributeValueTariff has been fetched
		/// will set AttributeValueTariff to null as well. Setting this property to true while AttributeValueTariff hasn't been fetched disables lazy loading for AttributeValueTariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValueTariff
		{
			get { return _alreadyFetchedAttributeValueTariff;}
			set 
			{
				if(_alreadyFetchedAttributeValueTariff && !value)
				{
					this.AttributeValueTariff = null;
				}
				_alreadyFetchedAttributeValueTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AttributeValueTariff is not found
		/// in the database. When set to true, AttributeValueTariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributeValueTariffReturnsNewIfNotFound
		{
			get	{ return _attributeValueTariffReturnsNewIfNotFound; }
			set { _attributeValueTariffReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttributeValueUseDays()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity AttributeValueUseDays
		{
			get	{ return GetSingleAttributeValueUseDays(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttributeValueUseDays(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UseDaysAttributeFareMatrixEntries", "AttributeValueUseDays", _attributeValueUseDays, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValueUseDays. When set to true, AttributeValueUseDays is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValueUseDays is accessed. You can always execute a forced fetch by calling GetSingleAttributeValueUseDays(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValueUseDays
		{
			get	{ return _alwaysFetchAttributeValueUseDays; }
			set	{ _alwaysFetchAttributeValueUseDays = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValueUseDays already has been fetched. Setting this property to false when AttributeValueUseDays has been fetched
		/// will set AttributeValueUseDays to null as well. Setting this property to true while AttributeValueUseDays hasn't been fetched disables lazy loading for AttributeValueUseDays</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValueUseDays
		{
			get { return _alreadyFetchedAttributeValueUseDays;}
			set 
			{
				if(_alreadyFetchedAttributeValueUseDays && !value)
				{
					this.AttributeValueUseDays = null;
				}
				_alreadyFetchedAttributeValueUseDays = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AttributeValueUseDays is not found
		/// in the database. When set to true, AttributeValueUseDays will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributeValueUseDaysReturnsNewIfNotFound
		{
			get	{ return _attributeValueUseDaysReturnsNewIfNotFound; }
			set { _attributeValueUseDaysReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareMatrixEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareMatrix()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareMatrixEntity FareMatrix
		{
			get	{ return GetSingleFareMatrix(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareMatrix(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareMatrixEntry", "FareMatrix", _fareMatrix, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareMatrix. When set to true, FareMatrix is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareMatrix is accessed. You can always execute a forced fetch by calling GetSingleFareMatrix(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareMatrix
		{
			get	{ return _alwaysFetchFareMatrix; }
			set	{ _alwaysFetchFareMatrix = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareMatrix already has been fetched. Setting this property to false when FareMatrix has been fetched
		/// will set FareMatrix to null as well. Setting this property to true while FareMatrix hasn't been fetched disables lazy loading for FareMatrix</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareMatrix
		{
			get { return _alreadyFetchedFareMatrix;}
			set 
			{
				if(_alreadyFetchedFareMatrix && !value)
				{
					this.FareMatrix = null;
				}
				_alreadyFetchedFareMatrix = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareMatrix is not found
		/// in the database. When set to true, FareMatrix will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareMatrixReturnsNewIfNotFound
		{
			get	{ return _fareMatrixReturnsNewIfNotFound; }
			set { _fareMatrixReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareStageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareStageBoarding()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageEntity FareStageBoarding
		{
			get	{ return GetSingleFareStageBoarding(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareStageBoarding(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareMatrixEntryBoarding", "FareStageBoarding", _fareStageBoarding, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareStageBoarding. When set to true, FareStageBoarding is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStageBoarding is accessed. You can always execute a forced fetch by calling GetSingleFareStageBoarding(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStageBoarding
		{
			get	{ return _alwaysFetchFareStageBoarding; }
			set	{ _alwaysFetchFareStageBoarding = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStageBoarding already has been fetched. Setting this property to false when FareStageBoarding has been fetched
		/// will set FareStageBoarding to null as well. Setting this property to true while FareStageBoarding hasn't been fetched disables lazy loading for FareStageBoarding</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStageBoarding
		{
			get { return _alreadyFetchedFareStageBoarding;}
			set 
			{
				if(_alreadyFetchedFareStageBoarding && !value)
				{
					this.FareStageBoarding = null;
				}
				_alreadyFetchedFareStageBoarding = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareStageBoarding is not found
		/// in the database. When set to true, FareStageBoarding will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareStageBoardingReturnsNewIfNotFound
		{
			get	{ return _fareStageBoardingReturnsNewIfNotFound; }
			set { _fareStageBoardingReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareStageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareStageDestination()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageEntity FareStageDestination
		{
			get	{ return GetSingleFareStageDestination(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareStageDestination(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareMatrixEntryDestination", "FareStageDestination", _fareStageDestination, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareStageDestination. When set to true, FareStageDestination is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStageDestination is accessed. You can always execute a forced fetch by calling GetSingleFareStageDestination(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStageDestination
		{
			get	{ return _alwaysFetchFareStageDestination; }
			set	{ _alwaysFetchFareStageDestination = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStageDestination already has been fetched. Setting this property to false when FareStageDestination has been fetched
		/// will set FareStageDestination to null as well. Setting this property to true while FareStageDestination hasn't been fetched disables lazy loading for FareStageDestination</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStageDestination
		{
			get { return _alreadyFetchedFareStageDestination;}
			set 
			{
				if(_alreadyFetchedFareStageDestination && !value)
				{
					this.FareStageDestination = null;
				}
				_alreadyFetchedFareStageDestination = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareStageDestination is not found
		/// in the database. When set to true, FareStageDestination will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareStageDestinationReturnsNewIfNotFound
		{
			get	{ return _fareStageDestinationReturnsNewIfNotFound; }
			set { _fareStageDestinationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareStageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareStageVia()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageEntity FareStageVia
		{
			get	{ return GetSingleFareStageVia(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareStageVia(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareMatrixEntryVia", "FareStageVia", _fareStageVia, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareStageVia. When set to true, FareStageVia is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStageVia is accessed. You can always execute a forced fetch by calling GetSingleFareStageVia(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStageVia
		{
			get	{ return _alwaysFetchFareStageVia; }
			set	{ _alwaysFetchFareStageVia = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStageVia already has been fetched. Setting this property to false when FareStageVia has been fetched
		/// will set FareStageVia to null as well. Setting this property to true while FareStageVia hasn't been fetched disables lazy loading for FareStageVia</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStageVia
		{
			get { return _alreadyFetchedFareStageVia;}
			set 
			{
				if(_alreadyFetchedFareStageVia && !value)
				{
					this.FareStageVia = null;
				}
				_alreadyFetchedFareStageVia = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareStageVia is not found
		/// in the database. When set to true, FareStageVia will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareStageViaReturnsNewIfNotFound
		{
			get	{ return _fareStageViaReturnsNewIfNotFound; }
			set { _fareStageViaReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RouteNameEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRouteName()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RouteNameEntity RouteName
		{
			get	{ return GetSingleRouteName(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRouteName(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareMatrixEntry", "RouteName", _routeName, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RouteName. When set to true, RouteName is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteName is accessed. You can always execute a forced fetch by calling GetSingleRouteName(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteName
		{
			get	{ return _alwaysFetchRouteName; }
			set	{ _alwaysFetchRouteName = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteName already has been fetched. Setting this property to false when RouteName has been fetched
		/// will set RouteName to null as well. Setting this property to true while RouteName hasn't been fetched disables lazy loading for RouteName</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteName
		{
			get { return _alreadyFetchedRouteName;}
			set 
			{
				if(_alreadyFetchedRouteName && !value)
				{
					this.RouteName = null;
				}
				_alreadyFetchedRouteName = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RouteName is not found
		/// in the database. When set to true, RouteName will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RouteNameReturnsNewIfNotFound
		{
			get	{ return _routeNameReturnsNewIfNotFound; }
			set { _routeNameReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ServiceAllocationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleServiceAllocation()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ServiceAllocationEntity ServiceAllocation
		{
			get	{ return GetSingleServiceAllocation(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncServiceAllocation(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareMatrixEntry", "ServiceAllocation", _serviceAllocation, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ServiceAllocation. When set to true, ServiceAllocation is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ServiceAllocation is accessed. You can always execute a forced fetch by calling GetSingleServiceAllocation(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchServiceAllocation
		{
			get	{ return _alwaysFetchServiceAllocation; }
			set	{ _alwaysFetchServiceAllocation = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ServiceAllocation already has been fetched. Setting this property to false when ServiceAllocation has been fetched
		/// will set ServiceAllocation to null as well. Setting this property to true while ServiceAllocation hasn't been fetched disables lazy loading for ServiceAllocation</summary>
		[Browsable(false)]
		public bool AlreadyFetchedServiceAllocation
		{
			get { return _alreadyFetchedServiceAllocation;}
			set 
			{
				if(_alreadyFetchedServiceAllocation && !value)
				{
					this.ServiceAllocation = null;
				}
				_alreadyFetchedServiceAllocation = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ServiceAllocation is not found
		/// in the database. When set to true, ServiceAllocation will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ServiceAllocationReturnsNewIfNotFound
		{
			get	{ return _serviceAllocationReturnsNewIfNotFound; }
			set { _serviceAllocationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketGroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicketGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketGroupEntity TicketGroup
		{
			get	{ return GetSingleTicketGroup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicketGroup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareMatrixEntry", "TicketGroup", _ticketGroup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TicketGroup. When set to true, TicketGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketGroup is accessed. You can always execute a forced fetch by calling GetSingleTicketGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketGroup
		{
			get	{ return _alwaysFetchTicketGroup; }
			set	{ _alwaysFetchTicketGroup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketGroup already has been fetched. Setting this property to false when TicketGroup has been fetched
		/// will set TicketGroup to null as well. Setting this property to true while TicketGroup hasn't been fetched disables lazy loading for TicketGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketGroup
		{
			get { return _alreadyFetchedTicketGroup;}
			set 
			{
				if(_alreadyFetchedTicketGroup && !value)
				{
					this.TicketGroup = null;
				}
				_alreadyFetchedTicketGroup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TicketGroup is not found
		/// in the database. When set to true, TicketGroup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketGroupReturnsNewIfNotFound
		{
			get	{ return _ticketGroupReturnsNewIfNotFound; }
			set { _ticketGroupReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
