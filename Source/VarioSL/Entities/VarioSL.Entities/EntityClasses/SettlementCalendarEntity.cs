﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SettlementCalendar'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SettlementCalendarEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.SettlementHolidayCollection	_settlementHolidays;
		private bool	_alwaysFetchSettlementHolidays, _alreadyFetchedSettlementHolidays;
		private VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection	_settlementQuerySettings;
		private bool	_alwaysFetchSettlementQuerySettings, _alreadyFetchedSettlementQuerySettings;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SettlementHolidays</summary>
			public static readonly string SettlementHolidays = "SettlementHolidays";
			/// <summary>Member name SettlementQuerySettings</summary>
			public static readonly string SettlementQuerySettings = "SettlementQuerySettings";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SettlementCalendarEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SettlementCalendarEntity() :base("SettlementCalendarEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="settlementCalendarID">PK value for SettlementCalendar which data should be fetched into this SettlementCalendar object</param>
		public SettlementCalendarEntity(System.Int64 settlementCalendarID):base("SettlementCalendarEntity")
		{
			InitClassFetch(settlementCalendarID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementCalendarID">PK value for SettlementCalendar which data should be fetched into this SettlementCalendar object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SettlementCalendarEntity(System.Int64 settlementCalendarID, IPrefetchPath prefetchPathToUse):base("SettlementCalendarEntity")
		{
			InitClassFetch(settlementCalendarID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementCalendarID">PK value for SettlementCalendar which data should be fetched into this SettlementCalendar object</param>
		/// <param name="validator">The custom validator object for this SettlementCalendarEntity</param>
		public SettlementCalendarEntity(System.Int64 settlementCalendarID, IValidator validator):base("SettlementCalendarEntity")
		{
			InitClassFetch(settlementCalendarID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SettlementCalendarEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_settlementHolidays = (VarioSL.Entities.CollectionClasses.SettlementHolidayCollection)info.GetValue("_settlementHolidays", typeof(VarioSL.Entities.CollectionClasses.SettlementHolidayCollection));
			_alwaysFetchSettlementHolidays = info.GetBoolean("_alwaysFetchSettlementHolidays");
			_alreadyFetchedSettlementHolidays = info.GetBoolean("_alreadyFetchedSettlementHolidays");

			_settlementQuerySettings = (VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection)info.GetValue("_settlementQuerySettings", typeof(VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection));
			_alwaysFetchSettlementQuerySettings = info.GetBoolean("_alwaysFetchSettlementQuerySettings");
			_alreadyFetchedSettlementQuerySettings = info.GetBoolean("_alreadyFetchedSettlementQuerySettings");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSettlementHolidays = (_settlementHolidays.Count > 0);
			_alreadyFetchedSettlementQuerySettings = (_settlementQuerySettings.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SettlementHolidays":
					toReturn.Add(Relations.SettlementHolidayEntityUsingSettlementCalendarID);
					break;
				case "SettlementQuerySettings":
					toReturn.Add(Relations.SettlementQuerySettingEntityUsingSettlementcalendarid);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_settlementHolidays", (!this.MarkedForDeletion?_settlementHolidays:null));
			info.AddValue("_alwaysFetchSettlementHolidays", _alwaysFetchSettlementHolidays);
			info.AddValue("_alreadyFetchedSettlementHolidays", _alreadyFetchedSettlementHolidays);
			info.AddValue("_settlementQuerySettings", (!this.MarkedForDeletion?_settlementQuerySettings:null));
			info.AddValue("_alwaysFetchSettlementQuerySettings", _alwaysFetchSettlementQuerySettings);
			info.AddValue("_alreadyFetchedSettlementQuerySettings", _alreadyFetchedSettlementQuerySettings);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SettlementHolidays":
					_alreadyFetchedSettlementHolidays = true;
					if(entity!=null)
					{
						this.SettlementHolidays.Add((SettlementHolidayEntity)entity);
					}
					break;
				case "SettlementQuerySettings":
					_alreadyFetchedSettlementQuerySettings = true;
					if(entity!=null)
					{
						this.SettlementQuerySettings.Add((SettlementQuerySettingEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SettlementHolidays":
					_settlementHolidays.Add((SettlementHolidayEntity)relatedEntity);
					break;
				case "SettlementQuerySettings":
					_settlementQuerySettings.Add((SettlementQuerySettingEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SettlementHolidays":
					this.PerformRelatedEntityRemoval(_settlementHolidays, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SettlementQuerySettings":
					this.PerformRelatedEntityRemoval(_settlementQuerySettings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_settlementHolidays);
			toReturn.Add(_settlementQuerySettings);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementCalendarID">PK value for SettlementCalendar which data should be fetched into this SettlementCalendar object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementCalendarID)
		{
			return FetchUsingPK(settlementCalendarID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementCalendarID">PK value for SettlementCalendar which data should be fetched into this SettlementCalendar object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementCalendarID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(settlementCalendarID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementCalendarID">PK value for SettlementCalendar which data should be fetched into this SettlementCalendar object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementCalendarID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(settlementCalendarID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementCalendarID">PK value for SettlementCalendar which data should be fetched into this SettlementCalendar object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementCalendarID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(settlementCalendarID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SettlementCalendarID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SettlementCalendarRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SettlementHolidayEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementHolidayEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementHolidayCollection GetMultiSettlementHolidays(bool forceFetch)
		{
			return GetMultiSettlementHolidays(forceFetch, _settlementHolidays.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementHolidayEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementHolidayEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementHolidayCollection GetMultiSettlementHolidays(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlementHolidays(forceFetch, _settlementHolidays.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementHolidayEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementHolidayCollection GetMultiSettlementHolidays(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlementHolidays(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementHolidayEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementHolidayCollection GetMultiSettlementHolidays(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlementHolidays || forceFetch || _alwaysFetchSettlementHolidays) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementHolidays);
				_settlementHolidays.SuppressClearInGetMulti=!forceFetch;
				_settlementHolidays.EntityFactoryToUse = entityFactoryToUse;
				_settlementHolidays.GetMultiManyToOne(this, filter);
				_settlementHolidays.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementHolidays = true;
			}
			return _settlementHolidays;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementHolidays'. These settings will be taken into account
		/// when the property SettlementHolidays is requested or GetMultiSettlementHolidays is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementHolidays(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementHolidays.SortClauses=sortClauses;
			_settlementHolidays.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQuerySettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection GetMultiSettlementQuerySettings(bool forceFetch)
		{
			return GetMultiSettlementQuerySettings(forceFetch, _settlementQuerySettings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQuerySettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection GetMultiSettlementQuerySettings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlementQuerySettings(forceFetch, _settlementQuerySettings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection GetMultiSettlementQuerySettings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlementQuerySettings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection GetMultiSettlementQuerySettings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlementQuerySettings || forceFetch || _alwaysFetchSettlementQuerySettings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementQuerySettings);
				_settlementQuerySettings.SuppressClearInGetMulti=!forceFetch;
				_settlementQuerySettings.EntityFactoryToUse = entityFactoryToUse;
				_settlementQuerySettings.GetMultiManyToOne(this, null, filter);
				_settlementQuerySettings.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementQuerySettings = true;
			}
			return _settlementQuerySettings;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementQuerySettings'. These settings will be taken into account
		/// when the property SettlementQuerySettings is requested or GetMultiSettlementQuerySettings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementQuerySettings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementQuerySettings.SortClauses=sortClauses;
			_settlementQuerySettings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SettlementHolidays", _settlementHolidays);
			toReturn.Add("SettlementQuerySettings", _settlementQuerySettings);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="settlementCalendarID">PK value for SettlementCalendar which data should be fetched into this SettlementCalendar object</param>
		/// <param name="validator">The validator object for this SettlementCalendarEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 settlementCalendarID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(settlementCalendarID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_settlementHolidays = new VarioSL.Entities.CollectionClasses.SettlementHolidayCollection();
			_settlementHolidays.SetContainingEntityInfo(this, "SettlementCalendar");

			_settlementQuerySettings = new VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection();
			_settlementQuerySettings.SetContainingEntityInfo(this, "SettlementCalendar");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementCalendarID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="settlementCalendarID">PK value for SettlementCalendar which data should be fetched into this SettlementCalendar object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 settlementCalendarID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SettlementCalendarFieldIndex.SettlementCalendarID].ForcedCurrentValueWrite(settlementCalendarID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSettlementCalendarDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SettlementCalendarEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SettlementCalendarRelations Relations
		{
			get	{ return new SettlementCalendarRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementHoliday' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementHolidays
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementHolidayCollection(), (IEntityRelation)GetRelationsForField("SettlementHolidays")[0], (int)VarioSL.Entities.EntityType.SettlementCalendarEntity, (int)VarioSL.Entities.EntityType.SettlementHolidayEntity, 0, null, null, null, "SettlementHolidays", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementQuerySetting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementQuerySettings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection(), (IEntityRelation)GetRelationsForField("SettlementQuerySettings")[0], (int)VarioSL.Entities.EntityType.SettlementCalendarEntity, (int)VarioSL.Entities.EntityType.SettlementQuerySettingEntity, 0, null, null, null, "SettlementQuerySettings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity SettlementCalendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTCALENDAR"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)SettlementCalendarFieldIndex.Description, true); }
			set	{ SetValue((int)SettlementCalendarFieldIndex.Description, value, true); }
		}

		/// <summary> The LastModified property of the Entity SettlementCalendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTCALENDAR"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SettlementCalendarFieldIndex.LastModified, true); }
			set	{ SetValue((int)SettlementCalendarFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity SettlementCalendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTCALENDAR"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SettlementCalendarFieldIndex.LastUser, true); }
			set	{ SetValue((int)SettlementCalendarFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity SettlementCalendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTCALENDAR"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SettlementCalendarFieldIndex.Name, true); }
			set	{ SetValue((int)SettlementCalendarFieldIndex.Name, value, true); }
		}

		/// <summary> The SettlementCalendarID property of the Entity SettlementCalendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTCALENDAR"."SETTLEMENTCALENDARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SettlementCalendarID
		{
			get { return (System.Int64)GetValue((int)SettlementCalendarFieldIndex.SettlementCalendarID, true); }
			set	{ SetValue((int)SettlementCalendarFieldIndex.SettlementCalendarID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity SettlementCalendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTCALENDAR"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SettlementCalendarFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SettlementCalendarFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SettlementHolidayEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementHolidays()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementHolidayCollection SettlementHolidays
		{
			get	{ return GetMultiSettlementHolidays(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementHolidays. When set to true, SettlementHolidays is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementHolidays is accessed. You can always execute/ a forced fetch by calling GetMultiSettlementHolidays(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementHolidays
		{
			get	{ return _alwaysFetchSettlementHolidays; }
			set	{ _alwaysFetchSettlementHolidays = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementHolidays already has been fetched. Setting this property to false when SettlementHolidays has been fetched
		/// will clear the SettlementHolidays collection well. Setting this property to true while SettlementHolidays hasn't been fetched disables lazy loading for SettlementHolidays</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementHolidays
		{
			get { return _alreadyFetchedSettlementHolidays;}
			set 
			{
				if(_alreadyFetchedSettlementHolidays && !value && (_settlementHolidays != null))
				{
					_settlementHolidays.Clear();
				}
				_alreadyFetchedSettlementHolidays = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementQuerySettings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection SettlementQuerySettings
		{
			get	{ return GetMultiSettlementQuerySettings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementQuerySettings. When set to true, SettlementQuerySettings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementQuerySettings is accessed. You can always execute/ a forced fetch by calling GetMultiSettlementQuerySettings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementQuerySettings
		{
			get	{ return _alwaysFetchSettlementQuerySettings; }
			set	{ _alwaysFetchSettlementQuerySettings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementQuerySettings already has been fetched. Setting this property to false when SettlementQuerySettings has been fetched
		/// will clear the SettlementQuerySettings collection well. Setting this property to true while SettlementQuerySettings hasn't been fetched disables lazy loading for SettlementQuerySettings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementQuerySettings
		{
			get { return _alreadyFetchedSettlementQuerySettings;}
			set 
			{
				if(_alreadyFetchedSettlementQuerySettings && !value && (_settlementQuerySettings != null))
				{
					_settlementQuerySettings.Clear();
				}
				_alreadyFetchedSettlementQuerySettings = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SettlementCalendarEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
