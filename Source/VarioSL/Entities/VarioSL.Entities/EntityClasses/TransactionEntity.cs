﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Transaction'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TransactionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.PaymentDetailCollection	_paymentDetails;
		private bool	_alwaysFetchPaymentDetails, _alreadyFetchedPaymentDetails;
		private VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection	_ruleViolationToTransactions;
		private bool	_alwaysFetchRuleViolationToTransactions, _alreadyFetchedRuleViolationToTransactions;
		private ClientEntity _cardIssuer;
		private bool	_alwaysFetchCardIssuer, _alreadyFetchedCardIssuer, _cardIssuerReturnsNewIfNotFound;
		private RevenueTransactionTypeEntity _transactionType;
		private bool	_alwaysFetchTransactionType, _alreadyFetchedTransactionType, _transactionTypeReturnsNewIfNotFound;
		private ShiftEntity _shift;
		private bool	_alwaysFetchShift, _alreadyFetchedShift, _shiftReturnsNewIfNotFound;
		private ExternalDataEntity _externalData;
		private bool	_alwaysFetchExternalData, _alreadyFetchedExternalData, _externalDataReturnsNewIfNotFound;
		private TransactionExtensionEntity _transactionExtension;
		private bool	_alwaysFetchTransactionExtension, _alreadyFetchedTransactionExtension, _transactionExtensionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CardIssuer</summary>
			public static readonly string CardIssuer = "CardIssuer";
			/// <summary>Member name TransactionType</summary>
			public static readonly string TransactionType = "TransactionType";
			/// <summary>Member name Shift</summary>
			public static readonly string Shift = "Shift";
			/// <summary>Member name PaymentDetails</summary>
			public static readonly string PaymentDetails = "PaymentDetails";
			/// <summary>Member name RuleViolationToTransactions</summary>
			public static readonly string RuleViolationToTransactions = "RuleViolationToTransactions";
			/// <summary>Member name ExternalData</summary>
			public static readonly string ExternalData = "ExternalData";
			/// <summary>Member name TransactionExtension</summary>
			public static readonly string TransactionExtension = "TransactionExtension";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TransactionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TransactionEntity() :base("TransactionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="transactionID">PK value for Transaction which data should be fetched into this Transaction object</param>
		public TransactionEntity(System.Int64 transactionID):base("TransactionEntity")
		{
			InitClassFetch(transactionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="transactionID">PK value for Transaction which data should be fetched into this Transaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TransactionEntity(System.Int64 transactionID, IPrefetchPath prefetchPathToUse):base("TransactionEntity")
		{
			InitClassFetch(transactionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="transactionID">PK value for Transaction which data should be fetched into this Transaction object</param>
		/// <param name="validator">The custom validator object for this TransactionEntity</param>
		public TransactionEntity(System.Int64 transactionID, IValidator validator):base("TransactionEntity")
		{
			InitClassFetch(transactionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TransactionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_paymentDetails = (VarioSL.Entities.CollectionClasses.PaymentDetailCollection)info.GetValue("_paymentDetails", typeof(VarioSL.Entities.CollectionClasses.PaymentDetailCollection));
			_alwaysFetchPaymentDetails = info.GetBoolean("_alwaysFetchPaymentDetails");
			_alreadyFetchedPaymentDetails = info.GetBoolean("_alreadyFetchedPaymentDetails");

			_ruleViolationToTransactions = (VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection)info.GetValue("_ruleViolationToTransactions", typeof(VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection));
			_alwaysFetchRuleViolationToTransactions = info.GetBoolean("_alwaysFetchRuleViolationToTransactions");
			_alreadyFetchedRuleViolationToTransactions = info.GetBoolean("_alreadyFetchedRuleViolationToTransactions");
			_cardIssuer = (ClientEntity)info.GetValue("_cardIssuer", typeof(ClientEntity));
			if(_cardIssuer!=null)
			{
				_cardIssuer.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardIssuerReturnsNewIfNotFound = info.GetBoolean("_cardIssuerReturnsNewIfNotFound");
			_alwaysFetchCardIssuer = info.GetBoolean("_alwaysFetchCardIssuer");
			_alreadyFetchedCardIssuer = info.GetBoolean("_alreadyFetchedCardIssuer");

			_transactionType = (RevenueTransactionTypeEntity)info.GetValue("_transactionType", typeof(RevenueTransactionTypeEntity));
			if(_transactionType!=null)
			{
				_transactionType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_transactionTypeReturnsNewIfNotFound = info.GetBoolean("_transactionTypeReturnsNewIfNotFound");
			_alwaysFetchTransactionType = info.GetBoolean("_alwaysFetchTransactionType");
			_alreadyFetchedTransactionType = info.GetBoolean("_alreadyFetchedTransactionType");

			_shift = (ShiftEntity)info.GetValue("_shift", typeof(ShiftEntity));
			if(_shift!=null)
			{
				_shift.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_shiftReturnsNewIfNotFound = info.GetBoolean("_shiftReturnsNewIfNotFound");
			_alwaysFetchShift = info.GetBoolean("_alwaysFetchShift");
			_alreadyFetchedShift = info.GetBoolean("_alreadyFetchedShift");
			_externalData = (ExternalDataEntity)info.GetValue("_externalData", typeof(ExternalDataEntity));
			if(_externalData!=null)
			{
				_externalData.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalDataReturnsNewIfNotFound = info.GetBoolean("_externalDataReturnsNewIfNotFound");
			_alwaysFetchExternalData = info.GetBoolean("_alwaysFetchExternalData");
			_alreadyFetchedExternalData = info.GetBoolean("_alreadyFetchedExternalData");

			_transactionExtension = (TransactionExtensionEntity)info.GetValue("_transactionExtension", typeof(TransactionExtensionEntity));
			if(_transactionExtension!=null)
			{
				_transactionExtension.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_transactionExtensionReturnsNewIfNotFound = info.GetBoolean("_transactionExtensionReturnsNewIfNotFound");
			_alwaysFetchTransactionExtension = info.GetBoolean("_alwaysFetchTransactionExtension");
			_alreadyFetchedTransactionExtension = info.GetBoolean("_alreadyFetchedTransactionExtension");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TransactionFieldIndex)fieldIndex)
			{
				case TransactionFieldIndex.CardIssuerID:
					DesetupSyncCardIssuer(true, false);
					_alreadyFetchedCardIssuer = false;
					break;
				case TransactionFieldIndex.ShiftID:
					DesetupSyncShift(true, false);
					_alreadyFetchedShift = false;
					break;
				case TransactionFieldIndex.TypeID:
					DesetupSyncTransactionType(true, false);
					_alreadyFetchedTransactionType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPaymentDetails = (_paymentDetails.Count > 0);
			_alreadyFetchedRuleViolationToTransactions = (_ruleViolationToTransactions.Count > 0);
			_alreadyFetchedCardIssuer = (_cardIssuer != null);
			_alreadyFetchedTransactionType = (_transactionType != null);
			_alreadyFetchedShift = (_shift != null);
			_alreadyFetchedExternalData = (_externalData != null);
			_alreadyFetchedTransactionExtension = (_transactionExtension != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CardIssuer":
					toReturn.Add(Relations.ClientEntityUsingCardIssuerID);
					break;
				case "TransactionType":
					toReturn.Add(Relations.RevenueTransactionTypeEntityUsingTypeID);
					break;
				case "Shift":
					toReturn.Add(Relations.ShiftEntityUsingShiftID);
					break;
				case "PaymentDetails":
					toReturn.Add(Relations.PaymentDetailEntityUsingTransactionID);
					break;
				case "RuleViolationToTransactions":
					toReturn.Add(Relations.RuleViolationToTransactionEntityUsingTransactionID);
					break;
				case "ExternalData":
					toReturn.Add(Relations.ExternalDataEntityUsingTransactionID);
					break;
				case "TransactionExtension":
					toReturn.Add(Relations.TransactionExtensionEntityUsingTransactionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_paymentDetails", (!this.MarkedForDeletion?_paymentDetails:null));
			info.AddValue("_alwaysFetchPaymentDetails", _alwaysFetchPaymentDetails);
			info.AddValue("_alreadyFetchedPaymentDetails", _alreadyFetchedPaymentDetails);
			info.AddValue("_ruleViolationToTransactions", (!this.MarkedForDeletion?_ruleViolationToTransactions:null));
			info.AddValue("_alwaysFetchRuleViolationToTransactions", _alwaysFetchRuleViolationToTransactions);
			info.AddValue("_alreadyFetchedRuleViolationToTransactions", _alreadyFetchedRuleViolationToTransactions);
			info.AddValue("_cardIssuer", (!this.MarkedForDeletion?_cardIssuer:null));
			info.AddValue("_cardIssuerReturnsNewIfNotFound", _cardIssuerReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardIssuer", _alwaysFetchCardIssuer);
			info.AddValue("_alreadyFetchedCardIssuer", _alreadyFetchedCardIssuer);
			info.AddValue("_transactionType", (!this.MarkedForDeletion?_transactionType:null));
			info.AddValue("_transactionTypeReturnsNewIfNotFound", _transactionTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTransactionType", _alwaysFetchTransactionType);
			info.AddValue("_alreadyFetchedTransactionType", _alreadyFetchedTransactionType);
			info.AddValue("_shift", (!this.MarkedForDeletion?_shift:null));
			info.AddValue("_shiftReturnsNewIfNotFound", _shiftReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchShift", _alwaysFetchShift);
			info.AddValue("_alreadyFetchedShift", _alreadyFetchedShift);

			info.AddValue("_externalData", (!this.MarkedForDeletion?_externalData:null));
			info.AddValue("_externalDataReturnsNewIfNotFound", _externalDataReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalData", _alwaysFetchExternalData);
			info.AddValue("_alreadyFetchedExternalData", _alreadyFetchedExternalData);

			info.AddValue("_transactionExtension", (!this.MarkedForDeletion?_transactionExtension:null));
			info.AddValue("_transactionExtensionReturnsNewIfNotFound", _transactionExtensionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTransactionExtension", _alwaysFetchTransactionExtension);
			info.AddValue("_alreadyFetchedTransactionExtension", _alreadyFetchedTransactionExtension);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CardIssuer":
					_alreadyFetchedCardIssuer = true;
					this.CardIssuer = (ClientEntity)entity;
					break;
				case "TransactionType":
					_alreadyFetchedTransactionType = true;
					this.TransactionType = (RevenueTransactionTypeEntity)entity;
					break;
				case "Shift":
					_alreadyFetchedShift = true;
					this.Shift = (ShiftEntity)entity;
					break;
				case "PaymentDetails":
					_alreadyFetchedPaymentDetails = true;
					if(entity!=null)
					{
						this.PaymentDetails.Add((PaymentDetailEntity)entity);
					}
					break;
				case "RuleViolationToTransactions":
					_alreadyFetchedRuleViolationToTransactions = true;
					if(entity!=null)
					{
						this.RuleViolationToTransactions.Add((RuleViolationToTransactionEntity)entity);
					}
					break;
				case "ExternalData":
					_alreadyFetchedExternalData = true;
					this.ExternalData = (ExternalDataEntity)entity;
					break;
				case "TransactionExtension":
					_alreadyFetchedTransactionExtension = true;
					this.TransactionExtension = (TransactionExtensionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CardIssuer":
					SetupSyncCardIssuer(relatedEntity);
					break;
				case "TransactionType":
					SetupSyncTransactionType(relatedEntity);
					break;
				case "Shift":
					SetupSyncShift(relatedEntity);
					break;
				case "PaymentDetails":
					_paymentDetails.Add((PaymentDetailEntity)relatedEntity);
					break;
				case "RuleViolationToTransactions":
					_ruleViolationToTransactions.Add((RuleViolationToTransactionEntity)relatedEntity);
					break;
				case "ExternalData":
					SetupSyncExternalData(relatedEntity);
					break;
				case "TransactionExtension":
					SetupSyncTransactionExtension(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CardIssuer":
					DesetupSyncCardIssuer(false, true);
					break;
				case "TransactionType":
					DesetupSyncTransactionType(false, true);
					break;
				case "Shift":
					DesetupSyncShift(false, true);
					break;
				case "PaymentDetails":
					this.PerformRelatedEntityRemoval(_paymentDetails, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RuleViolationToTransactions":
					this.PerformRelatedEntityRemoval(_ruleViolationToTransactions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalData":
					DesetupSyncExternalData(false, true);
					break;
				case "TransactionExtension":
					DesetupSyncTransactionExtension(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_externalData!=null)
			{
				toReturn.Add(_externalData);
			}
			if(_transactionExtension!=null)
			{
				toReturn.Add(_transactionExtension);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cardIssuer!=null)
			{
				toReturn.Add(_cardIssuer);
			}
			if(_transactionType!=null)
			{
				toReturn.Add(_transactionType);
			}
			if(_shift!=null)
			{
				toReturn.Add(_shift);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_paymentDetails);
			toReturn.Add(_ruleViolationToTransactions);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionID">PK value for Transaction which data should be fetched into this Transaction object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionID)
		{
			return FetchUsingPK(transactionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionID">PK value for Transaction which data should be fetched into this Transaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(transactionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionID">PK value for Transaction which data should be fetched into this Transaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(transactionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionID">PK value for Transaction which data should be fetched into this Transaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(transactionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TransactionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TransactionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'PaymentDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentDetailCollection GetMultiPaymentDetails(bool forceFetch)
		{
			return GetMultiPaymentDetails(forceFetch, _paymentDetails.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentDetailCollection GetMultiPaymentDetails(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentDetails(forceFetch, _paymentDetails.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentDetailCollection GetMultiPaymentDetails(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentDetails(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentDetailCollection GetMultiPaymentDetails(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentDetails || forceFetch || _alwaysFetchPaymentDetails) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentDetails);
				_paymentDetails.SuppressClearInGetMulti=!forceFetch;
				_paymentDetails.EntityFactoryToUse = entityFactoryToUse;
				_paymentDetails.GetMultiManyToOne(this, filter);
				_paymentDetails.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentDetails = true;
			}
			return _paymentDetails;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentDetails'. These settings will be taken into account
		/// when the property PaymentDetails is requested or GetMultiPaymentDetails is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentDetails(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentDetails.SortClauses=sortClauses;
			_paymentDetails.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationToTransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch)
		{
			return GetMultiRuleViolationToTransactions(forceFetch, _ruleViolationToTransactions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationToTransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleViolationToTransactions(forceFetch, _ruleViolationToTransactions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleViolationToTransactions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleViolationToTransactions || forceFetch || _alwaysFetchRuleViolationToTransactions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleViolationToTransactions);
				_ruleViolationToTransactions.SuppressClearInGetMulti=!forceFetch;
				_ruleViolationToTransactions.EntityFactoryToUse = entityFactoryToUse;
				_ruleViolationToTransactions.GetMultiManyToOne(this, null, null, null, filter);
				_ruleViolationToTransactions.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleViolationToTransactions = true;
			}
			return _ruleViolationToTransactions;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleViolationToTransactions'. These settings will be taken into account
		/// when the property RuleViolationToTransactions is requested or GetMultiRuleViolationToTransactions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleViolationToTransactions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleViolationToTransactions.SortClauses=sortClauses;
			_ruleViolationToTransactions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleCardIssuer()
		{
			return GetSingleCardIssuer(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleCardIssuer(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardIssuer || forceFetch || _alwaysFetchCardIssuer) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingCardIssuerID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardIssuerID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardIssuerReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardIssuer = newEntity;
				_alreadyFetchedCardIssuer = fetchResult;
			}
			return _cardIssuer;
		}


		/// <summary> Retrieves the related entity of type 'RevenueTransactionTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RevenueTransactionTypeEntity' which is related to this entity.</returns>
		public RevenueTransactionTypeEntity GetSingleTransactionType()
		{
			return GetSingleTransactionType(false);
		}

		/// <summary> Retrieves the related entity of type 'RevenueTransactionTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RevenueTransactionTypeEntity' which is related to this entity.</returns>
		public virtual RevenueTransactionTypeEntity GetSingleTransactionType(bool forceFetch)
		{
			if( ( !_alreadyFetchedTransactionType || forceFetch || _alwaysFetchTransactionType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RevenueTransactionTypeEntityUsingTypeID);
				RevenueTransactionTypeEntity newEntity = new RevenueTransactionTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TypeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RevenueTransactionTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_transactionTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TransactionType = newEntity;
				_alreadyFetchedTransactionType = fetchResult;
			}
			return _transactionType;
		}


		/// <summary> Retrieves the related entity of type 'ShiftEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ShiftEntity' which is related to this entity.</returns>
		public ShiftEntity GetSingleShift()
		{
			return GetSingleShift(false);
		}

		/// <summary> Retrieves the related entity of type 'ShiftEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ShiftEntity' which is related to this entity.</returns>
		public virtual ShiftEntity GetSingleShift(bool forceFetch)
		{
			if( ( !_alreadyFetchedShift || forceFetch || _alwaysFetchShift) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ShiftEntityUsingShiftID);
				ShiftEntity newEntity = new ShiftEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ShiftID);
				}
				if(fetchResult)
				{
					newEntity = (ShiftEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_shiftReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Shift = newEntity;
				_alreadyFetchedShift = fetchResult;
			}
			return _shift;
		}

		/// <summary> Retrieves the related entity of type 'ExternalDataEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'ExternalDataEntity' which is related to this entity.</returns>
		public ExternalDataEntity GetSingleExternalData()
		{
			return GetSingleExternalData(false);
		}
		
		/// <summary> Retrieves the related entity of type 'ExternalDataEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalDataEntity' which is related to this entity.</returns>
		public virtual ExternalDataEntity GetSingleExternalData(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalData || forceFetch || _alwaysFetchExternalData) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalDataEntityUsingTransactionID);
				ExternalDataEntity newEntity = new ExternalDataEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCTransactionID(this.TransactionID);
				}
				if(fetchResult)
				{
					newEntity = (ExternalDataEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalDataReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalData = newEntity;
				_alreadyFetchedExternalData = fetchResult;
			}
			return _externalData;
		}

		/// <summary> Retrieves the related entity of type 'TransactionExtensionEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'TransactionExtensionEntity' which is related to this entity.</returns>
		public TransactionExtensionEntity GetSingleTransactionExtension()
		{
			return GetSingleTransactionExtension(false);
		}
		
		/// <summary> Retrieves the related entity of type 'TransactionExtensionEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TransactionExtensionEntity' which is related to this entity.</returns>
		public virtual TransactionExtensionEntity GetSingleTransactionExtension(bool forceFetch)
		{
			if( ( !_alreadyFetchedTransactionExtension || forceFetch || _alwaysFetchTransactionExtension) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TransactionExtensionEntityUsingTransactionID);
				TransactionExtensionEntity newEntity = new TransactionExtensionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TransactionID);
				}
				if(fetchResult)
				{
					newEntity = (TransactionExtensionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_transactionExtensionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TransactionExtension = newEntity;
				_alreadyFetchedTransactionExtension = fetchResult;
			}
			return _transactionExtension;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CardIssuer", _cardIssuer);
			toReturn.Add("TransactionType", _transactionType);
			toReturn.Add("Shift", _shift);
			toReturn.Add("PaymentDetails", _paymentDetails);
			toReturn.Add("RuleViolationToTransactions", _ruleViolationToTransactions);
			toReturn.Add("ExternalData", _externalData);
			toReturn.Add("TransactionExtension", _transactionExtension);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="transactionID">PK value for Transaction which data should be fetched into this Transaction object</param>
		/// <param name="validator">The validator object for this TransactionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 transactionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(transactionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_paymentDetails = new VarioSL.Entities.CollectionClasses.PaymentDetailCollection();
			_paymentDetails.SetContainingEntityInfo(this, "SalesTransaction");

			_ruleViolationToTransactions = new VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection();
			_ruleViolationToTransactions.SetContainingEntityInfo(this, "RevenueTransaction");
			_cardIssuerReturnsNewIfNotFound = false;
			_transactionTypeReturnsNewIfNotFound = false;
			_shiftReturnsNewIfNotFound = false;
			_externalDataReturnsNewIfNotFound = false;
			_transactionExtensionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountEntryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AllocatedAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Barcode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BestPricerunID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CappingReason", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardBalance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardChargeTan", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardCredit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Carddebit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardIssuerID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardRefTransactionNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardTransactionNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearingState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CourseNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Deduction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceBookingState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DevicePaymentMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Direction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Distance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ErrorCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExtTikName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Factor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareMatrixEntryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FarePaymentTransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromStopCodeExtern", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromStopNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromTariffZone", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ImportDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JourneyLegCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Line", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxDistance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoAdults", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoChildren", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoUser0", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoUser1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoUser2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoUser3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoUser4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutputDevice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PayCardNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PayrollRelevant", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceCorrection", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RemainingRides", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevokeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesChannel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesPostProcessingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesTransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShoppingCart", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffNetworkNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketInstanceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TikNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToTariffZone", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripSerialNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidUntil", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueOfRide", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VatRatePerMille", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ViaNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ViaTariffZone", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _cardIssuer</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardIssuer(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardIssuer, new PropertyChangedEventHandler( OnCardIssuerPropertyChanged ), "CardIssuer", VarioSL.Entities.RelationClasses.StaticTransactionRelations.ClientEntityUsingCardIssuerIDStatic, true, signalRelatedEntity, "CardIssuerCardTransactions", resetFKFields, new int[] { (int)TransactionFieldIndex.CardIssuerID } );		
			_cardIssuer = null;
		}
		
		/// <summary> setups the sync logic for member _cardIssuer</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardIssuer(IEntityCore relatedEntity)
		{
			if(_cardIssuer!=relatedEntity)
			{		
				DesetupSyncCardIssuer(true, true);
				_cardIssuer = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardIssuer, new PropertyChangedEventHandler( OnCardIssuerPropertyChanged ), "CardIssuer", VarioSL.Entities.RelationClasses.StaticTransactionRelations.ClientEntityUsingCardIssuerIDStatic, true, ref _alreadyFetchedCardIssuer, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardIssuerPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _transactionType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTransactionType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _transactionType, new PropertyChangedEventHandler( OnTransactionTypePropertyChanged ), "TransactionType", VarioSL.Entities.RelationClasses.StaticTransactionRelations.RevenueTransactionTypeEntityUsingTypeIDStatic, true, signalRelatedEntity, "Transactions", resetFKFields, new int[] { (int)TransactionFieldIndex.TypeID } );		
			_transactionType = null;
		}
		
		/// <summary> setups the sync logic for member _transactionType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTransactionType(IEntityCore relatedEntity)
		{
			if(_transactionType!=relatedEntity)
			{		
				DesetupSyncTransactionType(true, true);
				_transactionType = (RevenueTransactionTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _transactionType, new PropertyChangedEventHandler( OnTransactionTypePropertyChanged ), "TransactionType", VarioSL.Entities.RelationClasses.StaticTransactionRelations.RevenueTransactionTypeEntityUsingTypeIDStatic, true, ref _alreadyFetchedTransactionType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTransactionTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _shift</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncShift(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _shift, new PropertyChangedEventHandler( OnShiftPropertyChanged ), "Shift", VarioSL.Entities.RelationClasses.StaticTransactionRelations.ShiftEntityUsingShiftIDStatic, true, signalRelatedEntity, "Transactions", resetFKFields, new int[] { (int)TransactionFieldIndex.ShiftID } );		
			_shift = null;
		}
		
		/// <summary> setups the sync logic for member _shift</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncShift(IEntityCore relatedEntity)
		{
			if(_shift!=relatedEntity)
			{		
				DesetupSyncShift(true, true);
				_shift = (ShiftEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _shift, new PropertyChangedEventHandler( OnShiftPropertyChanged ), "Shift", VarioSL.Entities.RelationClasses.StaticTransactionRelations.ShiftEntityUsingShiftIDStatic, true, ref _alreadyFetchedShift, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnShiftPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _externalData</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalData(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalData, new PropertyChangedEventHandler( OnExternalDataPropertyChanged ), "ExternalData", VarioSL.Entities.RelationClasses.StaticTransactionRelations.ExternalDataEntityUsingTransactionIDStatic, false, signalRelatedEntity, "SalesTransaction", false, new int[] { (int)TransactionFieldIndex.TransactionID } );
			_externalData = null;
		}
	
		/// <summary> setups the sync logic for member _externalData</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalData(IEntityCore relatedEntity)
		{
			if(_externalData!=relatedEntity)
			{
				DesetupSyncExternalData(true, true);
				_externalData = (ExternalDataEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalData, new PropertyChangedEventHandler( OnExternalDataPropertyChanged ), "ExternalData", VarioSL.Entities.RelationClasses.StaticTransactionRelations.ExternalDataEntityUsingTransactionIDStatic, false, ref _alreadyFetchedExternalData, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalDataPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _transactionExtension</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTransactionExtension(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _transactionExtension, new PropertyChangedEventHandler( OnTransactionExtensionPropertyChanged ), "TransactionExtension", VarioSL.Entities.RelationClasses.StaticTransactionRelations.TransactionExtensionEntityUsingTransactionIDStatic, false, signalRelatedEntity, "MainTransaction", false, new int[] { (int)TransactionFieldIndex.TransactionID } );
			_transactionExtension = null;
		}
	
		/// <summary> setups the sync logic for member _transactionExtension</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTransactionExtension(IEntityCore relatedEntity)
		{
			if(_transactionExtension!=relatedEntity)
			{
				DesetupSyncTransactionExtension(true, true);
				_transactionExtension = (TransactionExtensionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _transactionExtension, new PropertyChangedEventHandler( OnTransactionExtensionPropertyChanged ), "TransactionExtension", VarioSL.Entities.RelationClasses.StaticTransactionRelations.TransactionExtensionEntityUsingTransactionIDStatic, false, ref _alreadyFetchedTransactionExtension, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTransactionExtensionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="transactionID">PK value for Transaction which data should be fetched into this Transaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 transactionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TransactionFieldIndex.TransactionID].ForcedCurrentValueWrite(transactionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTransactionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TransactionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TransactionRelations Relations
		{
			get	{ return new TransactionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentDetail' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentDetails
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentDetailCollection(), (IEntityRelation)GetRelationsForField("PaymentDetails")[0], (int)VarioSL.Entities.EntityType.TransactionEntity, (int)VarioSL.Entities.EntityType.PaymentDetailEntity, 0, null, null, null, "PaymentDetails", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleViolationToTransaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleViolationToTransactions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection(), (IEntityRelation)GetRelationsForField("RuleViolationToTransactions")[0], (int)VarioSL.Entities.EntityType.TransactionEntity, (int)VarioSL.Entities.EntityType.RuleViolationToTransactionEntity, 0, null, null, null, "RuleViolationToTransactions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardIssuer
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("CardIssuer")[0], (int)VarioSL.Entities.EntityType.TransactionEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "CardIssuer", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RevenueTransactionType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RevenueTransactionTypeCollection(), (IEntityRelation)GetRelationsForField("TransactionType")[0], (int)VarioSL.Entities.EntityType.TransactionEntity, (int)VarioSL.Entities.EntityType.RevenueTransactionTypeEntity, 0, null, null, null, "TransactionType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Shift'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathShift
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ShiftCollection(), (IEntityRelation)GetRelationsForField("Shift")[0], (int)VarioSL.Entities.EntityType.TransactionEntity, (int)VarioSL.Entities.EntityType.ShiftEntity, 0, null, null, null, "Shift", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalData'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalData
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalDataCollection(), (IEntityRelation)GetRelationsForField("ExternalData")[0], (int)VarioSL.Entities.EntityType.TransactionEntity, (int)VarioSL.Entities.EntityType.ExternalDataEntity, 0, null, null, null, "ExternalData", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionExtension'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionExtension
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionExtensionCollection(), (IEntityRelation)GetRelationsForField("TransactionExtension")[0], (int)VarioSL.Entities.EntityType.TransactionEntity, (int)VarioSL.Entities.EntityType.TransactionExtensionEntity, 0, null, null, null, "TransactionExtension", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccountEntryID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."ACCOUNTENTRYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AccountEntryID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.AccountEntryID, false); }
			set	{ SetValue((int)TransactionFieldIndex.AccountEntryID, value, true); }
		}

		/// <summary> The AllocatedAmount property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."ALLOCATEDAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 20, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> AllocatedAmount
		{
			get { return (Nullable<System.Decimal>)GetValue((int)TransactionFieldIndex.AllocatedAmount, false); }
			set	{ SetValue((int)TransactionFieldIndex.AllocatedAmount, value, true); }
		}

		/// <summary> The Barcode property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."BARCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 13<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Barcode
		{
			get { return (System.String)GetValue((int)TransactionFieldIndex.Barcode, true); }
			set	{ SetValue((int)TransactionFieldIndex.Barcode, value, true); }
		}

		/// <summary> The BestPricerunID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."BESTPRICERUNID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BestPricerunID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.BestPricerunID, false); }
			set	{ SetValue((int)TransactionFieldIndex.BestPricerunID, value, true); }
		}

		/// <summary> The CancellationID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CANCELLATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.CancellationID, false); }
			set	{ SetValue((int)TransactionFieldIndex.CancellationID, value, true); }
		}

		/// <summary> The CappingReason property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CAPPINGREASON"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CappingReason
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.CappingReason, false); }
			set	{ SetValue((int)TransactionFieldIndex.CappingReason, value, true); }
		}

		/// <summary> The CardBalance property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CARDBALANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardBalance
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.CardBalance, false); }
			set	{ SetValue((int)TransactionFieldIndex.CardBalance, value, true); }
		}

		/// <summary> The CardChargeTan property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CARDCHARGETAN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardChargeTan
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.CardChargeTan, false); }
			set	{ SetValue((int)TransactionFieldIndex.CardChargeTan, value, true); }
		}

		/// <summary> The CardCredit property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CARDCREDIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardCredit
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.CardCredit, false); }
			set	{ SetValue((int)TransactionFieldIndex.CardCredit, value, true); }
		}

		/// <summary> The Carddebit property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CARDDEBIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Carddebit
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.Carddebit, false); }
			set	{ SetValue((int)TransactionFieldIndex.Carddebit, value, true); }
		}

		/// <summary> The CardID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CardID
		{
			get { return (System.Int64)GetValue((int)TransactionFieldIndex.CardID, true); }
			set	{ SetValue((int)TransactionFieldIndex.CardID, value, true); }
		}

		/// <summary> The CardIssuerID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CARDISSUERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardIssuerID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.CardIssuerID, false); }
			set	{ SetValue((int)TransactionFieldIndex.CardIssuerID, value, true); }
		}

		/// <summary> The CardRefTransactionNo property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CARDREFTRANSACTIONNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardRefTransactionNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.CardRefTransactionNo, false); }
			set	{ SetValue((int)TransactionFieldIndex.CardRefTransactionNo, value, true); }
		}

		/// <summary> The CardTransactionNo property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CARDTRANSACTIONNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardTransactionNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.CardTransactionNo, false); }
			set	{ SetValue((int)TransactionFieldIndex.CardTransactionNo, value, true); }
		}

		/// <summary> The ClearingID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CLEARINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClearingID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.ClearingID, false); }
			set	{ SetValue((int)TransactionFieldIndex.ClearingID, value, true); }
		}

		/// <summary> The ClearingState property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CLEARINGSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClearingState
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.ClearingState, false); }
			set	{ SetValue((int)TransactionFieldIndex.ClearingState, value, true); }
		}

		/// <summary> The ClientID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ClientID
		{
			get { return (System.Int32)GetValue((int)TransactionFieldIndex.ClientID, true); }
			set	{ SetValue((int)TransactionFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CourseNo property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."COURSENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CourseNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.CourseNo, false); }
			set	{ SetValue((int)TransactionFieldIndex.CourseNo, value, true); }
		}

		/// <summary> The Deduction property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."DEDUCTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Deduction
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.Deduction, false); }
			set	{ SetValue((int)TransactionFieldIndex.Deduction, value, true); }
		}

		/// <summary> The DeviceBookingState property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."DEVICEBOOKINGSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.Int16 DeviceBookingState
		{
			get { return (System.Int16)GetValue((int)TransactionFieldIndex.DeviceBookingState, true); }
			set	{ SetValue((int)TransactionFieldIndex.DeviceBookingState, value, true); }
		}

		/// <summary> The DeviceNo property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."DEVICENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 8, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeviceNo
		{
			get { return (System.Int32)GetValue((int)TransactionFieldIndex.DeviceNo, true); }
			set	{ SetValue((int)TransactionFieldIndex.DeviceNo, value, true); }
		}

		/// <summary> The DevicePaymentMethod property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."DEVICEPAYMENTMETHOD"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> DevicePaymentMethod
		{
			get { return (Nullable<System.Int16>)GetValue((int)TransactionFieldIndex.DevicePaymentMethod, false); }
			set	{ SetValue((int)TransactionFieldIndex.DevicePaymentMethod, value, true); }
		}

		/// <summary> The Direction property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."DIRECTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Direction
		{
			get { return (Nullable<System.Int16>)GetValue((int)TransactionFieldIndex.Direction, false); }
			set	{ SetValue((int)TransactionFieldIndex.Direction, value, true); }
		}

		/// <summary> The Distance property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."DISTANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Distance
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.Distance, false); }
			set	{ SetValue((int)TransactionFieldIndex.Distance, value, true); }
		}

		/// <summary> The ErrorCode property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."ERRORCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ErrorCode
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.ErrorCode, false); }
			set	{ SetValue((int)TransactionFieldIndex.ErrorCode, value, true); }
		}

		/// <summary> The ExtTikName property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."EXTTIKNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExtTikName
		{
			get { return (System.String)GetValue((int)TransactionFieldIndex.ExtTikName, true); }
			set	{ SetValue((int)TransactionFieldIndex.ExtTikName, value, true); }
		}

		/// <summary> The Factor property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."FACTOR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Factor
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.Factor, false); }
			set	{ SetValue((int)TransactionFieldIndex.Factor, value, true); }
		}

		/// <summary> The FareMatrixEntryID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."FAREMATRIXENTRYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareMatrixEntryID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.FareMatrixEntryID, false); }
			set	{ SetValue((int)TransactionFieldIndex.FareMatrixEntryID, value, true); }
		}

		/// <summary> The FarePaymentTransactionID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."FAREPAYMENTTRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FarePaymentTransactionID
		{
			get { return (System.String)GetValue((int)TransactionFieldIndex.FarePaymentTransactionID, true); }
			set	{ SetValue((int)TransactionFieldIndex.FarePaymentTransactionID, value, true); }
		}

		/// <summary> The FareStage property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."FARESTAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareStage
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.FareStage, false); }
			set	{ SetValue((int)TransactionFieldIndex.FareStage, value, true); }
		}

		/// <summary> The FromStopCodeExtern property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."FROMSTOPCODEEXTERN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FromStopCodeExtern
		{
			get { return (System.String)GetValue((int)TransactionFieldIndex.FromStopCodeExtern, true); }
			set	{ SetValue((int)TransactionFieldIndex.FromStopCodeExtern, value, true); }
		}

		/// <summary> The FromStopNo property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."FROMSTOPNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FromStopNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.FromStopNo, false); }
			set	{ SetValue((int)TransactionFieldIndex.FromStopNo, value, true); }
		}

		/// <summary> The FromTariffZone property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."FROMTARIFFZONE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FromTariffZone
		{
			get { return (System.Int64)GetValue((int)TransactionFieldIndex.FromTariffZone, true); }
			set	{ SetValue((int)TransactionFieldIndex.FromTariffZone, value, true); }
		}

		/// <summary> The ImportDateTime property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."IMPORTDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ImportDateTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TransactionFieldIndex.ImportDateTime, false); }
			set	{ SetValue((int)TransactionFieldIndex.ImportDateTime, value, true); }
		}

		/// <summary> The JourneyLegCounter property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."JOURNEYLEGCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> JourneyLegCounter
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.JourneyLegCounter, false); }
			set	{ SetValue((int)TransactionFieldIndex.JourneyLegCounter, value, true); }
		}

		/// <summary> The Line property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."LINE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Line
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.Line, false); }
			set	{ SetValue((int)TransactionFieldIndex.Line, value, true); }
		}

		/// <summary> The LineName property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."LINENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LineName
		{
			get { return (System.String)GetValue((int)TransactionFieldIndex.LineName, true); }
			set	{ SetValue((int)TransactionFieldIndex.LineName, value, true); }
		}

		/// <summary> The MaxDistance property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."MAXDISTANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MaxDistance
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.MaxDistance, false); }
			set	{ SetValue((int)TransactionFieldIndex.MaxDistance, value, true); }
		}

		/// <summary> The NoAdults property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."NOADULTS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 NoAdults
		{
			get { return (System.Int64)GetValue((int)TransactionFieldIndex.NoAdults, true); }
			set	{ SetValue((int)TransactionFieldIndex.NoAdults, value, true); }
		}

		/// <summary> The NoChildren property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."NOCHILDREN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 NoChildren
		{
			get { return (System.Int64)GetValue((int)TransactionFieldIndex.NoChildren, true); }
			set	{ SetValue((int)TransactionFieldIndex.NoChildren, value, true); }
		}

		/// <summary> The NoUser0 property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."NOUSER0"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NoUser0
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.NoUser0, false); }
			set	{ SetValue((int)TransactionFieldIndex.NoUser0, value, true); }
		}

		/// <summary> The NoUser1 property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."NOUSER1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NoUser1
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.NoUser1, false); }
			set	{ SetValue((int)TransactionFieldIndex.NoUser1, value, true); }
		}

		/// <summary> The NoUser2 property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."NOUSER2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NoUser2
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.NoUser2, false); }
			set	{ SetValue((int)TransactionFieldIndex.NoUser2, value, true); }
		}

		/// <summary> The NoUser3 property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."NOUSER3"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NoUser3
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.NoUser3, false); }
			set	{ SetValue((int)TransactionFieldIndex.NoUser3, value, true); }
		}

		/// <summary> The NoUser4 property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."NOUSER4"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NoUser4
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.NoUser4, false); }
			set	{ SetValue((int)TransactionFieldIndex.NoUser4, value, true); }
		}

		/// <summary> The OutputDevice property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."OUTPUTDEVICE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> OutputDevice
		{
			get { return (Nullable<System.Int16>)GetValue((int)TransactionFieldIndex.OutputDevice, false); }
			set	{ SetValue((int)TransactionFieldIndex.OutputDevice, value, true); }
		}

		/// <summary> The PayCardNo property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."PAYCARDNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PayCardNo
		{
			get { return (System.String)GetValue((int)TransactionFieldIndex.PayCardNo, true); }
			set	{ SetValue((int)TransactionFieldIndex.PayCardNo, value, true); }
		}

		/// <summary> The PayrollRelevant property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."PAYROLLRELEVANT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> PayrollRelevant
		{
			get { return (Nullable<System.Int16>)GetValue((int)TransactionFieldIndex.PayrollRelevant, false); }
			set	{ SetValue((int)TransactionFieldIndex.PayrollRelevant, value, true); }
		}

		/// <summary> The Price property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."PRICE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Price
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.Price, false); }
			set	{ SetValue((int)TransactionFieldIndex.Price, value, true); }
		}

		/// <summary> The PriceCorrection property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."PRICECORRECTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PriceCorrection
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionFieldIndex.PriceCorrection, false); }
			set	{ SetValue((int)TransactionFieldIndex.PriceCorrection, value, true); }
		}

		/// <summary> The PriceLevel property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."PRICELEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PriceLevel
		{
			get { return (System.String)GetValue((int)TransactionFieldIndex.PriceLevel, true); }
			set	{ SetValue((int)TransactionFieldIndex.PriceLevel, value, true); }
		}

		/// <summary> The RemainingRides property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."REMAININGRIDES"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RemainingRides
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionFieldIndex.RemainingRides, false); }
			set	{ SetValue((int)TransactionFieldIndex.RemainingRides, value, true); }
		}

		/// <summary> The RevokeID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."REVOKEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RevokeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.RevokeID, false); }
			set	{ SetValue((int)TransactionFieldIndex.RevokeID, value, true); }
		}

		/// <summary> The RouteNo property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."ROUTENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RouteNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.RouteNo, false); }
			set	{ SetValue((int)TransactionFieldIndex.RouteNo, value, true); }
		}

		/// <summary> The SalesChannel property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."SALESCHANNEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 SalesChannel
		{
			get { return (System.Int16)GetValue((int)TransactionFieldIndex.SalesChannel, true); }
			set	{ SetValue((int)TransactionFieldIndex.SalesChannel, value, true); }
		}

		/// <summary> The SalesPostProcessingID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."SALESPOSTPROCESSINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SalesPostProcessingID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.SalesPostProcessingID, false); }
			set	{ SetValue((int)TransactionFieldIndex.SalesPostProcessingID, value, true); }
		}

		/// <summary> The SalesTransactionID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."SALESTRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SalesTransactionID
		{
			get { return (System.String)GetValue((int)TransactionFieldIndex.SalesTransactionID, true); }
			set	{ SetValue((int)TransactionFieldIndex.SalesTransactionID, value, true); }
		}

		/// <summary> The ServiceID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."SERVICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ServiceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.ServiceID, false); }
			set	{ SetValue((int)TransactionFieldIndex.ServiceID, value, true); }
		}

		/// <summary> The ShiftID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."SHIFTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.Int64 ShiftID
		{
			get { return (System.Int64)GetValue((int)TransactionFieldIndex.ShiftID, true); }
			set	{ SetValue((int)TransactionFieldIndex.ShiftID, value, true); }
		}

		/// <summary> The ShoppingCart property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."SHOPPINGCART"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ShoppingCart
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.ShoppingCart, false); }
			set	{ SetValue((int)TransactionFieldIndex.ShoppingCart, value, true); }
		}

		/// <summary> The State property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> State
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.State, false); }
			set	{ SetValue((int)TransactionFieldIndex.State, value, true); }
		}

		/// <summary> The StopFrom property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."STOPFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> StopFrom
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.StopFrom, false); }
			set	{ SetValue((int)TransactionFieldIndex.StopFrom, value, true); }
		}

		/// <summary> The StopTo property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."STOPTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> StopTo
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.StopTo, false); }
			set	{ SetValue((int)TransactionFieldIndex.StopTo, value, true); }
		}

		/// <summary> The TariffID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.TariffID, false); }
			set	{ SetValue((int)TransactionFieldIndex.TariffID, value, true); }
		}

		/// <summary> The TariffNetworkNo property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."TARIFFNETWORKNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffNetworkNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.TariffNetworkNo, false); }
			set	{ SetValue((int)TransactionFieldIndex.TariffNetworkNo, value, true); }
		}

		/// <summary> The TicketInstanceID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."TICKETINSTANCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketInstanceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.TicketInstanceID, false); }
			set	{ SetValue((int)TransactionFieldIndex.TicketInstanceID, value, true); }
		}

		/// <summary> The TikNo property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."TIKNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TikNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.TikNo, false); }
			set	{ SetValue((int)TransactionFieldIndex.TikNo, value, true); }
		}

		/// <summary> The ToTariffZone property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."TOTARIFFZONE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ToTariffZone
		{
			get { return (System.Int64)GetValue((int)TransactionFieldIndex.ToTariffZone, true); }
			set	{ SetValue((int)TransactionFieldIndex.ToTariffZone, value, true); }
		}

		/// <summary> The TransactionID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."TRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 TransactionID
		{
			get { return (System.Int64)GetValue((int)TransactionFieldIndex.TransactionID, true); }
			set	{ SetValue((int)TransactionFieldIndex.TransactionID, value, true); }
		}

		/// <summary> The TripDateTime property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."TRIPDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TripDateTime
		{
			get { return (System.DateTime)GetValue((int)TransactionFieldIndex.TripDateTime, true); }
			set	{ SetValue((int)TransactionFieldIndex.TripDateTime, value, true); }
		}

		/// <summary> The TripSerialNo property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."TRIPSERIALNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 8, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TripSerialNo
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionFieldIndex.TripSerialNo, false); }
			set	{ SetValue((int)TransactionFieldIndex.TripSerialNo, value, true); }
		}

		/// <summary> The TypeID property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."TYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.TypeID, false); }
			set	{ SetValue((int)TransactionFieldIndex.TypeID, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TransactionFieldIndex.ValidFrom, false); }
			set	{ SetValue((int)TransactionFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidUntil property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."VALIDUNTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidUntil
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TransactionFieldIndex.ValidUntil, false); }
			set	{ SetValue((int)TransactionFieldIndex.ValidUntil, value, true); }
		}

		/// <summary> The ValueOfRide property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."VALUEOFRIDE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ValueOfRide
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.ValueOfRide, false); }
			set	{ SetValue((int)TransactionFieldIndex.ValueOfRide, value, true); }
		}

		/// <summary> The VatRatePerMille property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."VATRATEPERMILLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> VatRatePerMille
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionFieldIndex.VatRatePerMille, false); }
			set	{ SetValue((int)TransactionFieldIndex.VatRatePerMille, value, true); }
		}

		/// <summary> The ViaNo property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."VIANO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ViaNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.ViaNo, false); }
			set	{ SetValue((int)TransactionFieldIndex.ViaNo, value, true); }
		}

		/// <summary> The ViaTariffZone property of the Entity Transaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTION"."VIATARIFFZONE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ViaTariffZone
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionFieldIndex.ViaTariffZone, false); }
			set	{ SetValue((int)TransactionFieldIndex.ViaTariffZone, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'PaymentDetailEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentDetails()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentDetailCollection PaymentDetails
		{
			get	{ return GetMultiPaymentDetails(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentDetails. When set to true, PaymentDetails is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentDetails is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentDetails(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentDetails
		{
			get	{ return _alwaysFetchPaymentDetails; }
			set	{ _alwaysFetchPaymentDetails = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentDetails already has been fetched. Setting this property to false when PaymentDetails has been fetched
		/// will clear the PaymentDetails collection well. Setting this property to true while PaymentDetails hasn't been fetched disables lazy loading for PaymentDetails</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentDetails
		{
			get { return _alreadyFetchedPaymentDetails;}
			set 
			{
				if(_alreadyFetchedPaymentDetails && !value && (_paymentDetails != null))
				{
					_paymentDetails.Clear();
				}
				_alreadyFetchedPaymentDetails = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleViolationToTransactions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection RuleViolationToTransactions
		{
			get	{ return GetMultiRuleViolationToTransactions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleViolationToTransactions. When set to true, RuleViolationToTransactions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleViolationToTransactions is accessed. You can always execute/ a forced fetch by calling GetMultiRuleViolationToTransactions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleViolationToTransactions
		{
			get	{ return _alwaysFetchRuleViolationToTransactions; }
			set	{ _alwaysFetchRuleViolationToTransactions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleViolationToTransactions already has been fetched. Setting this property to false when RuleViolationToTransactions has been fetched
		/// will clear the RuleViolationToTransactions collection well. Setting this property to true while RuleViolationToTransactions hasn't been fetched disables lazy loading for RuleViolationToTransactions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleViolationToTransactions
		{
			get { return _alreadyFetchedRuleViolationToTransactions;}
			set 
			{
				if(_alreadyFetchedRuleViolationToTransactions && !value && (_ruleViolationToTransactions != null))
				{
					_ruleViolationToTransactions.Clear();
				}
				_alreadyFetchedRuleViolationToTransactions = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardIssuer()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity CardIssuer
		{
			get	{ return GetSingleCardIssuer(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCardIssuer(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CardIssuerCardTransactions", "CardIssuer", _cardIssuer, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardIssuer. When set to true, CardIssuer is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardIssuer is accessed. You can always execute a forced fetch by calling GetSingleCardIssuer(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardIssuer
		{
			get	{ return _alwaysFetchCardIssuer; }
			set	{ _alwaysFetchCardIssuer = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardIssuer already has been fetched. Setting this property to false when CardIssuer has been fetched
		/// will set CardIssuer to null as well. Setting this property to true while CardIssuer hasn't been fetched disables lazy loading for CardIssuer</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardIssuer
		{
			get { return _alreadyFetchedCardIssuer;}
			set 
			{
				if(_alreadyFetchedCardIssuer && !value)
				{
					this.CardIssuer = null;
				}
				_alreadyFetchedCardIssuer = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardIssuer is not found
		/// in the database. When set to true, CardIssuer will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardIssuerReturnsNewIfNotFound
		{
			get	{ return _cardIssuerReturnsNewIfNotFound; }
			set { _cardIssuerReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RevenueTransactionTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTransactionType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RevenueTransactionTypeEntity TransactionType
		{
			get	{ return GetSingleTransactionType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTransactionType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Transactions", "TransactionType", _transactionType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionType. When set to true, TransactionType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionType is accessed. You can always execute a forced fetch by calling GetSingleTransactionType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionType
		{
			get	{ return _alwaysFetchTransactionType; }
			set	{ _alwaysFetchTransactionType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionType already has been fetched. Setting this property to false when TransactionType has been fetched
		/// will set TransactionType to null as well. Setting this property to true while TransactionType hasn't been fetched disables lazy loading for TransactionType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionType
		{
			get { return _alreadyFetchedTransactionType;}
			set 
			{
				if(_alreadyFetchedTransactionType && !value)
				{
					this.TransactionType = null;
				}
				_alreadyFetchedTransactionType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TransactionType is not found
		/// in the database. When set to true, TransactionType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TransactionTypeReturnsNewIfNotFound
		{
			get	{ return _transactionTypeReturnsNewIfNotFound; }
			set { _transactionTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ShiftEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleShift()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ShiftEntity Shift
		{
			get	{ return GetSingleShift(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncShift(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Transactions", "Shift", _shift, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Shift. When set to true, Shift is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Shift is accessed. You can always execute a forced fetch by calling GetSingleShift(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchShift
		{
			get	{ return _alwaysFetchShift; }
			set	{ _alwaysFetchShift = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Shift already has been fetched. Setting this property to false when Shift has been fetched
		/// will set Shift to null as well. Setting this property to true while Shift hasn't been fetched disables lazy loading for Shift</summary>
		[Browsable(false)]
		public bool AlreadyFetchedShift
		{
			get { return _alreadyFetchedShift;}
			set 
			{
				if(_alreadyFetchedShift && !value)
				{
					this.Shift = null;
				}
				_alreadyFetchedShift = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Shift is not found
		/// in the database. When set to true, Shift will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ShiftReturnsNewIfNotFound
		{
			get	{ return _shiftReturnsNewIfNotFound; }
			set { _shiftReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ExternalDataEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalData()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ExternalDataEntity ExternalData
		{
			get	{ return GetSingleExternalData(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncExternalData(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_externalData !=null);
						DesetupSyncExternalData(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("ExternalData");
						}
					}
					else
					{
						if(_externalData!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "SalesTransaction");
							SetupSyncExternalData(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalData. When set to true, ExternalData is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalData is accessed. You can always execute a forced fetch by calling GetSingleExternalData(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalData
		{
			get	{ return _alwaysFetchExternalData; }
			set	{ _alwaysFetchExternalData = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalData already has been fetched. Setting this property to false when ExternalData has been fetched
		/// will set ExternalData to null as well. Setting this property to true while ExternalData hasn't been fetched disables lazy loading for ExternalData</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalData
		{
			get { return _alreadyFetchedExternalData;}
			set 
			{
				if(_alreadyFetchedExternalData && !value)
				{
					this.ExternalData = null;
				}
				_alreadyFetchedExternalData = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalData is not found
		/// in the database. When set to true, ExternalData will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ExternalDataReturnsNewIfNotFound
		{
			get	{ return _externalDataReturnsNewIfNotFound; }
			set	{ _externalDataReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'TransactionExtensionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTransactionExtension()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TransactionExtensionEntity TransactionExtension
		{
			get	{ return GetSingleTransactionExtension(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncTransactionExtension(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_transactionExtension !=null);
						DesetupSyncTransactionExtension(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("TransactionExtension");
						}
					}
					else
					{
						if(_transactionExtension!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "MainTransaction");
							SetupSyncTransactionExtension(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionExtension. When set to true, TransactionExtension is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionExtension is accessed. You can always execute a forced fetch by calling GetSingleTransactionExtension(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionExtension
		{
			get	{ return _alwaysFetchTransactionExtension; }
			set	{ _alwaysFetchTransactionExtension = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionExtension already has been fetched. Setting this property to false when TransactionExtension has been fetched
		/// will set TransactionExtension to null as well. Setting this property to true while TransactionExtension hasn't been fetched disables lazy loading for TransactionExtension</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionExtension
		{
			get { return _alreadyFetchedTransactionExtension;}
			set 
			{
				if(_alreadyFetchedTransactionExtension && !value)
				{
					this.TransactionExtension = null;
				}
				_alreadyFetchedTransactionExtension = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TransactionExtension is not found
		/// in the database. When set to true, TransactionExtension will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TransactionExtensionReturnsNewIfNotFound
		{
			get	{ return _transactionExtensionReturnsNewIfNotFound; }
			set	{ _transactionExtensionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TransactionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
