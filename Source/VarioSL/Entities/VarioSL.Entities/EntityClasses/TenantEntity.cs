﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Tenant'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TenantEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.TenantHistoryCollection	_tenantHistories;
		private bool	_alwaysFetchTenantHistories, _alreadyFetchedTenantHistories;
		private VarioSL.Entities.CollectionClasses.TenantPersonCollection	_tenantPeople;
		private bool	_alwaysFetchTenantPeople, _alreadyFetchedTenantPeople;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name TenantHistories</summary>
			public static readonly string TenantHistories = "TenantHistories";
			/// <summary>Member name TenantPeople</summary>
			public static readonly string TenantPeople = "TenantPeople";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TenantEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TenantEntity() :base("TenantEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="tenantID">PK value for Tenant which data should be fetched into this Tenant object</param>
		public TenantEntity(System.Int64 tenantID):base("TenantEntity")
		{
			InitClassFetch(tenantID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="tenantID">PK value for Tenant which data should be fetched into this Tenant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TenantEntity(System.Int64 tenantID, IPrefetchPath prefetchPathToUse):base("TenantEntity")
		{
			InitClassFetch(tenantID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="tenantID">PK value for Tenant which data should be fetched into this Tenant object</param>
		/// <param name="validator">The custom validator object for this TenantEntity</param>
		public TenantEntity(System.Int64 tenantID, IValidator validator):base("TenantEntity")
		{
			InitClassFetch(tenantID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TenantEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_tenantHistories = (VarioSL.Entities.CollectionClasses.TenantHistoryCollection)info.GetValue("_tenantHistories", typeof(VarioSL.Entities.CollectionClasses.TenantHistoryCollection));
			_alwaysFetchTenantHistories = info.GetBoolean("_alwaysFetchTenantHistories");
			_alreadyFetchedTenantHistories = info.GetBoolean("_alreadyFetchedTenantHistories");

			_tenantPeople = (VarioSL.Entities.CollectionClasses.TenantPersonCollection)info.GetValue("_tenantPeople", typeof(VarioSL.Entities.CollectionClasses.TenantPersonCollection));
			_alwaysFetchTenantPeople = info.GetBoolean("_alwaysFetchTenantPeople");
			_alreadyFetchedTenantPeople = info.GetBoolean("_alreadyFetchedTenantPeople");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTenantHistories = (_tenantHistories.Count > 0);
			_alreadyFetchedTenantPeople = (_tenantPeople.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "TenantHistories":
					toReturn.Add(Relations.TenantHistoryEntityUsingTenantID);
					break;
				case "TenantPeople":
					toReturn.Add(Relations.TenantPersonEntityUsingTenantID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_tenantHistories", (!this.MarkedForDeletion?_tenantHistories:null));
			info.AddValue("_alwaysFetchTenantHistories", _alwaysFetchTenantHistories);
			info.AddValue("_alreadyFetchedTenantHistories", _alreadyFetchedTenantHistories);
			info.AddValue("_tenantPeople", (!this.MarkedForDeletion?_tenantPeople:null));
			info.AddValue("_alwaysFetchTenantPeople", _alwaysFetchTenantPeople);
			info.AddValue("_alreadyFetchedTenantPeople", _alreadyFetchedTenantPeople);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "TenantHistories":
					_alreadyFetchedTenantHistories = true;
					if(entity!=null)
					{
						this.TenantHistories.Add((TenantHistoryEntity)entity);
					}
					break;
				case "TenantPeople":
					_alreadyFetchedTenantPeople = true;
					if(entity!=null)
					{
						this.TenantPeople.Add((TenantPersonEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "TenantHistories":
					_tenantHistories.Add((TenantHistoryEntity)relatedEntity);
					break;
				case "TenantPeople":
					_tenantPeople.Add((TenantPersonEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "TenantHistories":
					this.PerformRelatedEntityRemoval(_tenantHistories, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TenantPeople":
					this.PerformRelatedEntityRemoval(_tenantPeople, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_tenantHistories);
			toReturn.Add(_tenantPeople);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tenantID">PK value for Tenant which data should be fetched into this Tenant object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tenantID)
		{
			return FetchUsingPK(tenantID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tenantID">PK value for Tenant which data should be fetched into this Tenant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tenantID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(tenantID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tenantID">PK value for Tenant which data should be fetched into this Tenant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tenantID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(tenantID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tenantID">PK value for Tenant which data should be fetched into this Tenant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tenantID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(tenantID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TenantID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TenantRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'TenantHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TenantHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TenantHistoryCollection GetMultiTenantHistories(bool forceFetch)
		{
			return GetMultiTenantHistories(forceFetch, _tenantHistories.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TenantHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TenantHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TenantHistoryCollection GetMultiTenantHistories(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTenantHistories(forceFetch, _tenantHistories.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TenantHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TenantHistoryCollection GetMultiTenantHistories(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTenantHistories(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TenantHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TenantHistoryCollection GetMultiTenantHistories(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTenantHistories || forceFetch || _alwaysFetchTenantHistories) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tenantHistories);
				_tenantHistories.SuppressClearInGetMulti=!forceFetch;
				_tenantHistories.EntityFactoryToUse = entityFactoryToUse;
				_tenantHistories.GetMultiManyToOne(this, null, filter);
				_tenantHistories.SuppressClearInGetMulti=false;
				_alreadyFetchedTenantHistories = true;
			}
			return _tenantHistories;
		}

		/// <summary> Sets the collection parameters for the collection for 'TenantHistories'. These settings will be taken into account
		/// when the property TenantHistories is requested or GetMultiTenantHistories is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTenantHistories(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tenantHistories.SortClauses=sortClauses;
			_tenantHistories.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TenantPersonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TenantPersonEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TenantPersonCollection GetMultiTenantPeople(bool forceFetch)
		{
			return GetMultiTenantPeople(forceFetch, _tenantPeople.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TenantPersonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TenantPersonEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TenantPersonCollection GetMultiTenantPeople(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTenantPeople(forceFetch, _tenantPeople.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TenantPersonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TenantPersonCollection GetMultiTenantPeople(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTenantPeople(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TenantPersonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TenantPersonCollection GetMultiTenantPeople(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTenantPeople || forceFetch || _alwaysFetchTenantPeople) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tenantPeople);
				_tenantPeople.SuppressClearInGetMulti=!forceFetch;
				_tenantPeople.EntityFactoryToUse = entityFactoryToUse;
				_tenantPeople.GetMultiManyToOne(this, filter);
				_tenantPeople.SuppressClearInGetMulti=false;
				_alreadyFetchedTenantPeople = true;
			}
			return _tenantPeople;
		}

		/// <summary> Sets the collection parameters for the collection for 'TenantPeople'. These settings will be taken into account
		/// when the property TenantPeople is requested or GetMultiTenantPeople is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTenantPeople(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tenantPeople.SortClauses=sortClauses;
			_tenantPeople.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("TenantHistories", _tenantHistories);
			toReturn.Add("TenantPeople", _tenantPeople);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="tenantID">PK value for Tenant which data should be fetched into this Tenant object</param>
		/// <param name="validator">The validator object for this TenantEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 tenantID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(tenantID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_tenantHistories = new VarioSL.Entities.CollectionClasses.TenantHistoryCollection();
			_tenantHistories.SetContainingEntityInfo(this, "Tenant");

			_tenantPeople = new VarioSL.Entities.CollectionClasses.TenantPersonCollection();
			_tenantPeople.SetContainingEntityInfo(this, "Tenant");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenantID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenantName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="tenantID">PK value for Tenant which data should be fetched into this Tenant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 tenantID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TenantFieldIndex.TenantID].ForcedCurrentValueWrite(tenantID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTenantDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TenantEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TenantRelations Relations
		{
			get	{ return new TenantRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TenantHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTenantHistories
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TenantHistoryCollection(), (IEntityRelation)GetRelationsForField("TenantHistories")[0], (int)VarioSL.Entities.EntityType.TenantEntity, (int)VarioSL.Entities.EntityType.TenantHistoryEntity, 0, null, null, null, "TenantHistories", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TenantPerson' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTenantPeople
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TenantPersonCollection(), (IEntityRelation)GetRelationsForField("TenantPeople")[0], (int)VarioSL.Entities.EntityType.TenantEntity, (int)VarioSL.Entities.EntityType.TenantPersonEntity, 0, null, null, null, "TenantPeople", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LastModified property of the Entity Tenant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)TenantFieldIndex.LastModified, true); }
			set	{ SetValue((int)TenantFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Tenant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)TenantFieldIndex.LastUser, true); }
			set	{ SetValue((int)TenantFieldIndex.LastUser, value, true); }
		}

		/// <summary> The State property of the Entity Tenant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANT"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TenantState State
		{
			get { return (VarioSL.Entities.Enumerations.TenantState)GetValue((int)TenantFieldIndex.State, true); }
			set	{ SetValue((int)TenantFieldIndex.State, value, true); }
		}

		/// <summary> The TenantID property of the Entity Tenant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANT"."TENANTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 TenantID
		{
			get { return (System.Int64)GetValue((int)TenantFieldIndex.TenantID, true); }
			set	{ SetValue((int)TenantFieldIndex.TenantID, value, true); }
		}

		/// <summary> The TenantName property of the Entity Tenant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANT"."TENANTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TenantName
		{
			get { return (System.String)GetValue((int)TenantFieldIndex.TenantName, true); }
			set	{ SetValue((int)TenantFieldIndex.TenantName, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Tenant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)TenantFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)TenantFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'TenantHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTenantHistories()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TenantHistoryCollection TenantHistories
		{
			get	{ return GetMultiTenantHistories(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TenantHistories. When set to true, TenantHistories is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TenantHistories is accessed. You can always execute/ a forced fetch by calling GetMultiTenantHistories(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTenantHistories
		{
			get	{ return _alwaysFetchTenantHistories; }
			set	{ _alwaysFetchTenantHistories = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TenantHistories already has been fetched. Setting this property to false when TenantHistories has been fetched
		/// will clear the TenantHistories collection well. Setting this property to true while TenantHistories hasn't been fetched disables lazy loading for TenantHistories</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTenantHistories
		{
			get { return _alreadyFetchedTenantHistories;}
			set 
			{
				if(_alreadyFetchedTenantHistories && !value && (_tenantHistories != null))
				{
					_tenantHistories.Clear();
				}
				_alreadyFetchedTenantHistories = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TenantPersonEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTenantPeople()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TenantPersonCollection TenantPeople
		{
			get	{ return GetMultiTenantPeople(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TenantPeople. When set to true, TenantPeople is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TenantPeople is accessed. You can always execute/ a forced fetch by calling GetMultiTenantPeople(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTenantPeople
		{
			get	{ return _alwaysFetchTenantPeople; }
			set	{ _alwaysFetchTenantPeople = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TenantPeople already has been fetched. Setting this property to false when TenantPeople has been fetched
		/// will clear the TenantPeople collection well. Setting this property to true while TenantPeople hasn't been fetched disables lazy loading for TenantPeople</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTenantPeople
		{
			get { return _alreadyFetchedTenantPeople;}
			set 
			{
				if(_alreadyFetchedTenantPeople && !value && (_tenantPeople != null))
				{
					_tenantPeople.Clear();
				}
				_alreadyFetchedTenantPeople = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TenantEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
