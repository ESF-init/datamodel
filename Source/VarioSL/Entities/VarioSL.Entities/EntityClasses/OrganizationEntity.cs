﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Organization'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class OrganizationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.TicketOrganizationCollection	_ticketOrganizations;
		private bool	_alwaysFetchTicketOrganizations, _alreadyFetchedTicketOrganizations;
		private VarioSL.Entities.CollectionClasses.ContractCollection	_contracts;
		private bool	_alwaysFetchContracts, _alreadyFetchedContracts;
		private VarioSL.Entities.CollectionClasses.OrganizationCollection	_organizations;
		private bool	_alwaysFetchOrganizations, _alreadyFetchedOrganizations;
		private VarioSL.Entities.CollectionClasses.OrganizationAddressCollection	_organizationAddresses;
		private bool	_alwaysFetchOrganizationAddresses, _alreadyFetchedOrganizationAddresses;
		private VarioSL.Entities.CollectionClasses.ProductCollection	_products;
		private bool	_alwaysFetchProducts, _alreadyFetchedProducts;
		private VarioSL.Entities.CollectionClasses.ProductSubsidyCollection	_productSubsidies;
		private bool	_alwaysFetchProductSubsidies, _alreadyFetchedProductSubsidies;
		private VarioSL.Entities.CollectionClasses.TicketAssignmentCollection	_ticketAssignments;
		private bool	_alwaysFetchTicketAssignments, _alreadyFetchedTicketAssignments;
		private VarioSL.Entities.CollectionClasses.TicketCollection _ticketCollectionViaTicketOrganization;
		private bool	_alwaysFetchTicketCollectionViaTicketOrganization, _alreadyFetchedTicketCollectionViaTicketOrganization;
		private OrganizationEntity _organization;
		private bool	_alwaysFetchOrganization, _alreadyFetchedOrganization, _organizationReturnsNewIfNotFound;
		private OrganizationTypeEntity _organizationType;
		private bool	_alwaysFetchOrganizationType, _alreadyFetchedOrganizationType, _organizationTypeReturnsNewIfNotFound;
		private CardHolderOrganizationEntity _cardHolderOrganization;
		private bool	_alwaysFetchCardHolderOrganization, _alreadyFetchedCardHolderOrganization, _cardHolderOrganizationReturnsNewIfNotFound;
		private PersonEntity _contactPerson;
		private bool	_alwaysFetchContactPerson, _alreadyFetchedContactPerson, _contactPersonReturnsNewIfNotFound;
		private SubsidyLimitEntity _subsidyLimit;
		private bool	_alwaysFetchSubsidyLimit, _alreadyFetchedSubsidyLimit, _subsidyLimitReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Organization</summary>
			public static readonly string Organization = "Organization";
			/// <summary>Member name OrganizationType</summary>
			public static readonly string OrganizationType = "OrganizationType";
			/// <summary>Member name TicketOrganizations</summary>
			public static readonly string TicketOrganizations = "TicketOrganizations";
			/// <summary>Member name Contracts</summary>
			public static readonly string Contracts = "Contracts";
			/// <summary>Member name Organizations</summary>
			public static readonly string Organizations = "Organizations";
			/// <summary>Member name OrganizationAddresses</summary>
			public static readonly string OrganizationAddresses = "OrganizationAddresses";
			/// <summary>Member name Products</summary>
			public static readonly string Products = "Products";
			/// <summary>Member name ProductSubsidies</summary>
			public static readonly string ProductSubsidies = "ProductSubsidies";
			/// <summary>Member name TicketAssignments</summary>
			public static readonly string TicketAssignments = "TicketAssignments";
			/// <summary>Member name TicketCollectionViaTicketOrganization</summary>
			public static readonly string TicketCollectionViaTicketOrganization = "TicketCollectionViaTicketOrganization";
			/// <summary>Member name CardHolderOrganization</summary>
			public static readonly string CardHolderOrganization = "CardHolderOrganization";
			/// <summary>Member name ContactPerson</summary>
			public static readonly string ContactPerson = "ContactPerson";
			/// <summary>Member name SubsidyLimit</summary>
			public static readonly string SubsidyLimit = "SubsidyLimit";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OrganizationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public OrganizationEntity() :base("OrganizationEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="organizationID">PK value for Organization which data should be fetched into this Organization object</param>
		public OrganizationEntity(System.Int64 organizationID):base("OrganizationEntity")
		{
			InitClassFetch(organizationID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="organizationID">PK value for Organization which data should be fetched into this Organization object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OrganizationEntity(System.Int64 organizationID, IPrefetchPath prefetchPathToUse):base("OrganizationEntity")
		{
			InitClassFetch(organizationID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="organizationID">PK value for Organization which data should be fetched into this Organization object</param>
		/// <param name="validator">The custom validator object for this OrganizationEntity</param>
		public OrganizationEntity(System.Int64 organizationID, IValidator validator):base("OrganizationEntity")
		{
			InitClassFetch(organizationID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrganizationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_ticketOrganizations = (VarioSL.Entities.CollectionClasses.TicketOrganizationCollection)info.GetValue("_ticketOrganizations", typeof(VarioSL.Entities.CollectionClasses.TicketOrganizationCollection));
			_alwaysFetchTicketOrganizations = info.GetBoolean("_alwaysFetchTicketOrganizations");
			_alreadyFetchedTicketOrganizations = info.GetBoolean("_alreadyFetchedTicketOrganizations");

			_contracts = (VarioSL.Entities.CollectionClasses.ContractCollection)info.GetValue("_contracts", typeof(VarioSL.Entities.CollectionClasses.ContractCollection));
			_alwaysFetchContracts = info.GetBoolean("_alwaysFetchContracts");
			_alreadyFetchedContracts = info.GetBoolean("_alreadyFetchedContracts");

			_organizations = (VarioSL.Entities.CollectionClasses.OrganizationCollection)info.GetValue("_organizations", typeof(VarioSL.Entities.CollectionClasses.OrganizationCollection));
			_alwaysFetchOrganizations = info.GetBoolean("_alwaysFetchOrganizations");
			_alreadyFetchedOrganizations = info.GetBoolean("_alreadyFetchedOrganizations");

			_organizationAddresses = (VarioSL.Entities.CollectionClasses.OrganizationAddressCollection)info.GetValue("_organizationAddresses", typeof(VarioSL.Entities.CollectionClasses.OrganizationAddressCollection));
			_alwaysFetchOrganizationAddresses = info.GetBoolean("_alwaysFetchOrganizationAddresses");
			_alreadyFetchedOrganizationAddresses = info.GetBoolean("_alreadyFetchedOrganizationAddresses");

			_products = (VarioSL.Entities.CollectionClasses.ProductCollection)info.GetValue("_products", typeof(VarioSL.Entities.CollectionClasses.ProductCollection));
			_alwaysFetchProducts = info.GetBoolean("_alwaysFetchProducts");
			_alreadyFetchedProducts = info.GetBoolean("_alreadyFetchedProducts");

			_productSubsidies = (VarioSL.Entities.CollectionClasses.ProductSubsidyCollection)info.GetValue("_productSubsidies", typeof(VarioSL.Entities.CollectionClasses.ProductSubsidyCollection));
			_alwaysFetchProductSubsidies = info.GetBoolean("_alwaysFetchProductSubsidies");
			_alreadyFetchedProductSubsidies = info.GetBoolean("_alreadyFetchedProductSubsidies");

			_ticketAssignments = (VarioSL.Entities.CollectionClasses.TicketAssignmentCollection)info.GetValue("_ticketAssignments", typeof(VarioSL.Entities.CollectionClasses.TicketAssignmentCollection));
			_alwaysFetchTicketAssignments = info.GetBoolean("_alwaysFetchTicketAssignments");
			_alreadyFetchedTicketAssignments = info.GetBoolean("_alreadyFetchedTicketAssignments");
			_ticketCollectionViaTicketOrganization = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketCollectionViaTicketOrganization", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketCollectionViaTicketOrganization = info.GetBoolean("_alwaysFetchTicketCollectionViaTicketOrganization");
			_alreadyFetchedTicketCollectionViaTicketOrganization = info.GetBoolean("_alreadyFetchedTicketCollectionViaTicketOrganization");
			_organization = (OrganizationEntity)info.GetValue("_organization", typeof(OrganizationEntity));
			if(_organization!=null)
			{
				_organization.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_organizationReturnsNewIfNotFound = info.GetBoolean("_organizationReturnsNewIfNotFound");
			_alwaysFetchOrganization = info.GetBoolean("_alwaysFetchOrganization");
			_alreadyFetchedOrganization = info.GetBoolean("_alreadyFetchedOrganization");

			_organizationType = (OrganizationTypeEntity)info.GetValue("_organizationType", typeof(OrganizationTypeEntity));
			if(_organizationType!=null)
			{
				_organizationType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_organizationTypeReturnsNewIfNotFound = info.GetBoolean("_organizationTypeReturnsNewIfNotFound");
			_alwaysFetchOrganizationType = info.GetBoolean("_alwaysFetchOrganizationType");
			_alreadyFetchedOrganizationType = info.GetBoolean("_alreadyFetchedOrganizationType");
			_cardHolderOrganization = (CardHolderOrganizationEntity)info.GetValue("_cardHolderOrganization", typeof(CardHolderOrganizationEntity));
			if(_cardHolderOrganization!=null)
			{
				_cardHolderOrganization.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardHolderOrganizationReturnsNewIfNotFound = info.GetBoolean("_cardHolderOrganizationReturnsNewIfNotFound");
			_alwaysFetchCardHolderOrganization = info.GetBoolean("_alwaysFetchCardHolderOrganization");
			_alreadyFetchedCardHolderOrganization = info.GetBoolean("_alreadyFetchedCardHolderOrganization");

			_contactPerson = (PersonEntity)info.GetValue("_contactPerson", typeof(PersonEntity));
			if(_contactPerson!=null)
			{
				_contactPerson.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contactPersonReturnsNewIfNotFound = info.GetBoolean("_contactPersonReturnsNewIfNotFound");
			_alwaysFetchContactPerson = info.GetBoolean("_alwaysFetchContactPerson");
			_alreadyFetchedContactPerson = info.GetBoolean("_alreadyFetchedContactPerson");

			_subsidyLimit = (SubsidyLimitEntity)info.GetValue("_subsidyLimit", typeof(SubsidyLimitEntity));
			if(_subsidyLimit!=null)
			{
				_subsidyLimit.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_subsidyLimitReturnsNewIfNotFound = info.GetBoolean("_subsidyLimitReturnsNewIfNotFound");
			_alwaysFetchSubsidyLimit = info.GetBoolean("_alwaysFetchSubsidyLimit");
			_alreadyFetchedSubsidyLimit = info.GetBoolean("_alreadyFetchedSubsidyLimit");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((OrganizationFieldIndex)fieldIndex)
			{
				case OrganizationFieldIndex.ContactPersonID:
					DesetupSyncContactPerson(true, false);
					_alreadyFetchedContactPerson = false;
					break;
				case OrganizationFieldIndex.FrameOrganizationID:
					DesetupSyncOrganization(true, false);
					_alreadyFetchedOrganization = false;
					break;
				case OrganizationFieldIndex.OrganizationTypeID:
					DesetupSyncOrganizationType(true, false);
					_alreadyFetchedOrganizationType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTicketOrganizations = (_ticketOrganizations.Count > 0);
			_alreadyFetchedContracts = (_contracts.Count > 0);
			_alreadyFetchedOrganizations = (_organizations.Count > 0);
			_alreadyFetchedOrganizationAddresses = (_organizationAddresses.Count > 0);
			_alreadyFetchedProducts = (_products.Count > 0);
			_alreadyFetchedProductSubsidies = (_productSubsidies.Count > 0);
			_alreadyFetchedTicketAssignments = (_ticketAssignments.Count > 0);
			_alreadyFetchedTicketCollectionViaTicketOrganization = (_ticketCollectionViaTicketOrganization.Count > 0);
			_alreadyFetchedOrganization = (_organization != null);
			_alreadyFetchedOrganizationType = (_organizationType != null);
			_alreadyFetchedCardHolderOrganization = (_cardHolderOrganization != null);
			_alreadyFetchedContactPerson = (_contactPerson != null);
			_alreadyFetchedSubsidyLimit = (_subsidyLimit != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Organization":
					toReturn.Add(Relations.OrganizationEntityUsingOrganizationIDFrameOrganizationID);
					break;
				case "OrganizationType":
					toReturn.Add(Relations.OrganizationTypeEntityUsingOrganizationTypeID);
					break;
				case "TicketOrganizations":
					toReturn.Add(Relations.TicketOrganizationEntityUsingOrganizationID);
					break;
				case "Contracts":
					toReturn.Add(Relations.ContractEntityUsingOrganizationID);
					break;
				case "Organizations":
					toReturn.Add(Relations.OrganizationEntityUsingFrameOrganizationID);
					break;
				case "OrganizationAddresses":
					toReturn.Add(Relations.OrganizationAddressEntityUsingOrganizationID);
					break;
				case "Products":
					toReturn.Add(Relations.ProductEntityUsingOrganizationID);
					break;
				case "ProductSubsidies":
					toReturn.Add(Relations.ProductSubsidyEntityUsingOrganizationID);
					break;
				case "TicketAssignments":
					toReturn.Add(Relations.TicketAssignmentEntityUsingInstitutionID);
					break;
				case "TicketCollectionViaTicketOrganization":
					toReturn.Add(Relations.TicketOrganizationEntityUsingOrganizationID, "OrganizationEntity__", "TicketOrganization_", JoinHint.None);
					toReturn.Add(TicketOrganizationEntity.Relations.TicketEntityUsingTicketID, "TicketOrganization_", string.Empty, JoinHint.None);
					break;
				case "CardHolderOrganization":
					toReturn.Add(Relations.CardHolderOrganizationEntityUsingOrganizationID);
					break;
				case "ContactPerson":
					toReturn.Add(Relations.PersonEntityUsingContactPersonID);
					break;
				case "SubsidyLimit":
					toReturn.Add(Relations.SubsidyLimitEntityUsingOrganizationID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_ticketOrganizations", (!this.MarkedForDeletion?_ticketOrganizations:null));
			info.AddValue("_alwaysFetchTicketOrganizations", _alwaysFetchTicketOrganizations);
			info.AddValue("_alreadyFetchedTicketOrganizations", _alreadyFetchedTicketOrganizations);
			info.AddValue("_contracts", (!this.MarkedForDeletion?_contracts:null));
			info.AddValue("_alwaysFetchContracts", _alwaysFetchContracts);
			info.AddValue("_alreadyFetchedContracts", _alreadyFetchedContracts);
			info.AddValue("_organizations", (!this.MarkedForDeletion?_organizations:null));
			info.AddValue("_alwaysFetchOrganizations", _alwaysFetchOrganizations);
			info.AddValue("_alreadyFetchedOrganizations", _alreadyFetchedOrganizations);
			info.AddValue("_organizationAddresses", (!this.MarkedForDeletion?_organizationAddresses:null));
			info.AddValue("_alwaysFetchOrganizationAddresses", _alwaysFetchOrganizationAddresses);
			info.AddValue("_alreadyFetchedOrganizationAddresses", _alreadyFetchedOrganizationAddresses);
			info.AddValue("_products", (!this.MarkedForDeletion?_products:null));
			info.AddValue("_alwaysFetchProducts", _alwaysFetchProducts);
			info.AddValue("_alreadyFetchedProducts", _alreadyFetchedProducts);
			info.AddValue("_productSubsidies", (!this.MarkedForDeletion?_productSubsidies:null));
			info.AddValue("_alwaysFetchProductSubsidies", _alwaysFetchProductSubsidies);
			info.AddValue("_alreadyFetchedProductSubsidies", _alreadyFetchedProductSubsidies);
			info.AddValue("_ticketAssignments", (!this.MarkedForDeletion?_ticketAssignments:null));
			info.AddValue("_alwaysFetchTicketAssignments", _alwaysFetchTicketAssignments);
			info.AddValue("_alreadyFetchedTicketAssignments", _alreadyFetchedTicketAssignments);
			info.AddValue("_ticketCollectionViaTicketOrganization", (!this.MarkedForDeletion?_ticketCollectionViaTicketOrganization:null));
			info.AddValue("_alwaysFetchTicketCollectionViaTicketOrganization", _alwaysFetchTicketCollectionViaTicketOrganization);
			info.AddValue("_alreadyFetchedTicketCollectionViaTicketOrganization", _alreadyFetchedTicketCollectionViaTicketOrganization);
			info.AddValue("_organization", (!this.MarkedForDeletion?_organization:null));
			info.AddValue("_organizationReturnsNewIfNotFound", _organizationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrganization", _alwaysFetchOrganization);
			info.AddValue("_alreadyFetchedOrganization", _alreadyFetchedOrganization);
			info.AddValue("_organizationType", (!this.MarkedForDeletion?_organizationType:null));
			info.AddValue("_organizationTypeReturnsNewIfNotFound", _organizationTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrganizationType", _alwaysFetchOrganizationType);
			info.AddValue("_alreadyFetchedOrganizationType", _alreadyFetchedOrganizationType);

			info.AddValue("_cardHolderOrganization", (!this.MarkedForDeletion?_cardHolderOrganization:null));
			info.AddValue("_cardHolderOrganizationReturnsNewIfNotFound", _cardHolderOrganizationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardHolderOrganization", _alwaysFetchCardHolderOrganization);
			info.AddValue("_alreadyFetchedCardHolderOrganization", _alreadyFetchedCardHolderOrganization);

			info.AddValue("_contactPerson", (!this.MarkedForDeletion?_contactPerson:null));
			info.AddValue("_contactPersonReturnsNewIfNotFound", _contactPersonReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContactPerson", _alwaysFetchContactPerson);
			info.AddValue("_alreadyFetchedContactPerson", _alreadyFetchedContactPerson);

			info.AddValue("_subsidyLimit", (!this.MarkedForDeletion?_subsidyLimit:null));
			info.AddValue("_subsidyLimitReturnsNewIfNotFound", _subsidyLimitReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSubsidyLimit", _alwaysFetchSubsidyLimit);
			info.AddValue("_alreadyFetchedSubsidyLimit", _alreadyFetchedSubsidyLimit);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Organization":
					_alreadyFetchedOrganization = true;
					this.Organization = (OrganizationEntity)entity;
					break;
				case "OrganizationType":
					_alreadyFetchedOrganizationType = true;
					this.OrganizationType = (OrganizationTypeEntity)entity;
					break;
				case "TicketOrganizations":
					_alreadyFetchedTicketOrganizations = true;
					if(entity!=null)
					{
						this.TicketOrganizations.Add((TicketOrganizationEntity)entity);
					}
					break;
				case "Contracts":
					_alreadyFetchedContracts = true;
					if(entity!=null)
					{
						this.Contracts.Add((ContractEntity)entity);
					}
					break;
				case "Organizations":
					_alreadyFetchedOrganizations = true;
					if(entity!=null)
					{
						this.Organizations.Add((OrganizationEntity)entity);
					}
					break;
				case "OrganizationAddresses":
					_alreadyFetchedOrganizationAddresses = true;
					if(entity!=null)
					{
						this.OrganizationAddresses.Add((OrganizationAddressEntity)entity);
					}
					break;
				case "Products":
					_alreadyFetchedProducts = true;
					if(entity!=null)
					{
						this.Products.Add((ProductEntity)entity);
					}
					break;
				case "ProductSubsidies":
					_alreadyFetchedProductSubsidies = true;
					if(entity!=null)
					{
						this.ProductSubsidies.Add((ProductSubsidyEntity)entity);
					}
					break;
				case "TicketAssignments":
					_alreadyFetchedTicketAssignments = true;
					if(entity!=null)
					{
						this.TicketAssignments.Add((TicketAssignmentEntity)entity);
					}
					break;
				case "TicketCollectionViaTicketOrganization":
					_alreadyFetchedTicketCollectionViaTicketOrganization = true;
					if(entity!=null)
					{
						this.TicketCollectionViaTicketOrganization.Add((TicketEntity)entity);
					}
					break;
				case "CardHolderOrganization":
					_alreadyFetchedCardHolderOrganization = true;
					this.CardHolderOrganization = (CardHolderOrganizationEntity)entity;
					break;
				case "ContactPerson":
					_alreadyFetchedContactPerson = true;
					this.ContactPerson = (PersonEntity)entity;
					break;
				case "SubsidyLimit":
					_alreadyFetchedSubsidyLimit = true;
					this.SubsidyLimit = (SubsidyLimitEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Organization":
					SetupSyncOrganization(relatedEntity);
					break;
				case "OrganizationType":
					SetupSyncOrganizationType(relatedEntity);
					break;
				case "TicketOrganizations":
					_ticketOrganizations.Add((TicketOrganizationEntity)relatedEntity);
					break;
				case "Contracts":
					_contracts.Add((ContractEntity)relatedEntity);
					break;
				case "Organizations":
					_organizations.Add((OrganizationEntity)relatedEntity);
					break;
				case "OrganizationAddresses":
					_organizationAddresses.Add((OrganizationAddressEntity)relatedEntity);
					break;
				case "Products":
					_products.Add((ProductEntity)relatedEntity);
					break;
				case "ProductSubsidies":
					_productSubsidies.Add((ProductSubsidyEntity)relatedEntity);
					break;
				case "TicketAssignments":
					_ticketAssignments.Add((TicketAssignmentEntity)relatedEntity);
					break;
				case "CardHolderOrganization":
					SetupSyncCardHolderOrganization(relatedEntity);
					break;
				case "ContactPerson":
					SetupSyncContactPerson(relatedEntity);
					break;
				case "SubsidyLimit":
					SetupSyncSubsidyLimit(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Organization":
					DesetupSyncOrganization(false, true);
					break;
				case "OrganizationType":
					DesetupSyncOrganizationType(false, true);
					break;
				case "TicketOrganizations":
					this.PerformRelatedEntityRemoval(_ticketOrganizations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Contracts":
					this.PerformRelatedEntityRemoval(_contracts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Organizations":
					this.PerformRelatedEntityRemoval(_organizations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrganizationAddresses":
					this.PerformRelatedEntityRemoval(_organizationAddresses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Products":
					this.PerformRelatedEntityRemoval(_products, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductSubsidies":
					this.PerformRelatedEntityRemoval(_productSubsidies, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketAssignments":
					this.PerformRelatedEntityRemoval(_ticketAssignments, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardHolderOrganization":
					DesetupSyncCardHolderOrganization(false, true);
					break;
				case "ContactPerson":
					DesetupSyncContactPerson(false, true);
					break;
				case "SubsidyLimit":
					DesetupSyncSubsidyLimit(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cardHolderOrganization!=null)
			{
				toReturn.Add(_cardHolderOrganization);
			}
			if(_subsidyLimit!=null)
			{
				toReturn.Add(_subsidyLimit);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_organization!=null)
			{
				toReturn.Add(_organization);
			}
			if(_organizationType!=null)
			{
				toReturn.Add(_organizationType);
			}
			if(_contactPerson!=null)
			{
				toReturn.Add(_contactPerson);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_ticketOrganizations);
			toReturn.Add(_contracts);
			toReturn.Add(_organizations);
			toReturn.Add(_organizationAddresses);
			toReturn.Add(_products);
			toReturn.Add(_productSubsidies);
			toReturn.Add(_ticketAssignments);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="contactPersonID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCContactPersonID(System.Int64 contactPersonID)
		{
			return FetchUsingUCContactPersonID( contactPersonID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="contactPersonID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCContactPersonID(System.Int64 contactPersonID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCContactPersonID( contactPersonID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="contactPersonID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCContactPersonID(System.Int64 contactPersonID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCContactPersonID( contactPersonID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="contactPersonID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCContactPersonID(System.Int64 contactPersonID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((OrganizationDAO)CreateDAOInstance()).FetchOrganizationUsingUCContactPersonID(this, this.Transaction, contactPersonID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="organizationID">PK value for Organization which data should be fetched into this Organization object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 organizationID)
		{
			return FetchUsingPK(organizationID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="organizationID">PK value for Organization which data should be fetched into this Organization object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 organizationID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(organizationID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="organizationID">PK value for Organization which data should be fetched into this Organization object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 organizationID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(organizationID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="organizationID">PK value for Organization which data should be fetched into this Organization object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 organizationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(organizationID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OrganizationID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OrganizationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'TicketOrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketOrganizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketOrganizationCollection GetMultiTicketOrganizations(bool forceFetch)
		{
			return GetMultiTicketOrganizations(forceFetch, _ticketOrganizations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketOrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketOrganizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketOrganizationCollection GetMultiTicketOrganizations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketOrganizations(forceFetch, _ticketOrganizations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketOrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketOrganizationCollection GetMultiTicketOrganizations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketOrganizations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketOrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketOrganizationCollection GetMultiTicketOrganizations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketOrganizations || forceFetch || _alwaysFetchTicketOrganizations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketOrganizations);
				_ticketOrganizations.SuppressClearInGetMulti=!forceFetch;
				_ticketOrganizations.EntityFactoryToUse = entityFactoryToUse;
				_ticketOrganizations.GetMultiManyToOne(null, this, filter);
				_ticketOrganizations.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketOrganizations = true;
			}
			return _ticketOrganizations;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketOrganizations'. These settings will be taken into account
		/// when the property TicketOrganizations is requested or GetMultiTicketOrganizations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketOrganizations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketOrganizations.SortClauses=sortClauses;
			_ticketOrganizations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch)
		{
			return GetMultiContracts(forceFetch, _contracts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContracts(forceFetch, _contracts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContracts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContracts || forceFetch || _alwaysFetchContracts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contracts);
				_contracts.SuppressClearInGetMulti=!forceFetch;
				_contracts.EntityFactoryToUse = entityFactoryToUse;
				_contracts.GetMultiManyToOne(null, null, null, this, filter);
				_contracts.SuppressClearInGetMulti=false;
				_alreadyFetchedContracts = true;
			}
			return _contracts;
		}

		/// <summary> Sets the collection parameters for the collection for 'Contracts'. These settings will be taken into account
		/// when the property Contracts is requested or GetMultiContracts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContracts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contracts.SortClauses=sortClauses;
			_contracts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationCollection GetMultiOrganizations(bool forceFetch)
		{
			return GetMultiOrganizations(forceFetch, _organizations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationCollection GetMultiOrganizations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrganizations(forceFetch, _organizations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationCollection GetMultiOrganizations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrganizations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationCollection GetMultiOrganizations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrganizations || forceFetch || _alwaysFetchOrganizations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_organizations);
				_organizations.SuppressClearInGetMulti=!forceFetch;
				_organizations.EntityFactoryToUse = entityFactoryToUse;
				_organizations.GetMultiManyToOne(this, null, filter);
				_organizations.SuppressClearInGetMulti=false;
				_alreadyFetchedOrganizations = true;
			}
			return _organizations;
		}

		/// <summary> Sets the collection parameters for the collection for 'Organizations'. These settings will be taken into account
		/// when the property Organizations is requested or GetMultiOrganizations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrganizations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_organizations.SortClauses=sortClauses;
			_organizations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrganizationAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationAddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationAddressCollection GetMultiOrganizationAddresses(bool forceFetch)
		{
			return GetMultiOrganizationAddresses(forceFetch, _organizationAddresses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationAddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationAddressCollection GetMultiOrganizationAddresses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrganizationAddresses(forceFetch, _organizationAddresses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationAddressCollection GetMultiOrganizationAddresses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrganizationAddresses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationAddressCollection GetMultiOrganizationAddresses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrganizationAddresses || forceFetch || _alwaysFetchOrganizationAddresses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_organizationAddresses);
				_organizationAddresses.SuppressClearInGetMulti=!forceFetch;
				_organizationAddresses.EntityFactoryToUse = entityFactoryToUse;
				_organizationAddresses.GetMultiManyToOne(null, this, filter);
				_organizationAddresses.SuppressClearInGetMulti=false;
				_alreadyFetchedOrganizationAddresses = true;
			}
			return _organizationAddresses;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrganizationAddresses'. These settings will be taken into account
		/// when the property OrganizationAddresses is requested or GetMultiOrganizationAddresses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrganizationAddresses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_organizationAddresses.SortClauses=sortClauses;
			_organizationAddresses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProducts || forceFetch || _alwaysFetchProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_products);
				_products.SuppressClearInGetMulti=!forceFetch;
				_products.EntityFactoryToUse = entityFactoryToUse;
				_products.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, null, filter);
				_products.SuppressClearInGetMulti=false;
				_alreadyFetchedProducts = true;
			}
			return _products;
		}

		/// <summary> Sets the collection parameters for the collection for 'Products'. These settings will be taken into account
		/// when the property Products is requested or GetMultiProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_products.SortClauses=sortClauses;
			_products.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductSubsidyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductSubsidyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductSubsidyCollection GetMultiProductSubsidies(bool forceFetch)
		{
			return GetMultiProductSubsidies(forceFetch, _productSubsidies.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductSubsidyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductSubsidyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductSubsidyCollection GetMultiProductSubsidies(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductSubsidies(forceFetch, _productSubsidies.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductSubsidyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductSubsidyCollection GetMultiProductSubsidies(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductSubsidies(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductSubsidyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductSubsidyCollection GetMultiProductSubsidies(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductSubsidies || forceFetch || _alwaysFetchProductSubsidies) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productSubsidies);
				_productSubsidies.SuppressClearInGetMulti=!forceFetch;
				_productSubsidies.EntityFactoryToUse = entityFactoryToUse;
				_productSubsidies.GetMultiManyToOne(this, filter);
				_productSubsidies.SuppressClearInGetMulti=false;
				_alreadyFetchedProductSubsidies = true;
			}
			return _productSubsidies;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductSubsidies'. These settings will be taken into account
		/// when the property ProductSubsidies is requested or GetMultiProductSubsidies is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductSubsidies(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productSubsidies.SortClauses=sortClauses;
			_productSubsidies.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketAssignmentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch)
		{
			return GetMultiTicketAssignments(forceFetch, _ticketAssignments.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketAssignmentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketAssignments(forceFetch, _ticketAssignments.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketAssignments(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketAssignments || forceFetch || _alwaysFetchTicketAssignments) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketAssignments);
				_ticketAssignments.SuppressClearInGetMulti=!forceFetch;
				_ticketAssignments.EntityFactoryToUse = entityFactoryToUse;
				_ticketAssignments.GetMultiManyToOne(null, null, null, null, this, filter);
				_ticketAssignments.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketAssignments = true;
			}
			return _ticketAssignments;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketAssignments'. These settings will be taken into account
		/// when the property TicketAssignments is requested or GetMultiTicketAssignments is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketAssignments(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketAssignments.SortClauses=sortClauses;
			_ticketAssignments.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaTicketOrganization(bool forceFetch)
		{
			return GetMultiTicketCollectionViaTicketOrganization(forceFetch, _ticketCollectionViaTicketOrganization.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaTicketOrganization(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTicketCollectionViaTicketOrganization || forceFetch || _alwaysFetchTicketCollectionViaTicketOrganization) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketCollectionViaTicketOrganization);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrganizationFields.OrganizationID, ComparisonOperator.Equal, this.OrganizationID, "OrganizationEntity__"));
				_ticketCollectionViaTicketOrganization.SuppressClearInGetMulti=!forceFetch;
				_ticketCollectionViaTicketOrganization.EntityFactoryToUse = entityFactoryToUse;
				_ticketCollectionViaTicketOrganization.GetMulti(filter, GetRelationsForField("TicketCollectionViaTicketOrganization"));
				_ticketCollectionViaTicketOrganization.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketCollectionViaTicketOrganization = true;
			}
			return _ticketCollectionViaTicketOrganization;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketCollectionViaTicketOrganization'. These settings will be taken into account
		/// when the property TicketCollectionViaTicketOrganization is requested or GetMultiTicketCollectionViaTicketOrganization is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketCollectionViaTicketOrganization(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketCollectionViaTicketOrganization.SortClauses=sortClauses;
			_ticketCollectionViaTicketOrganization.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'OrganizationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrganizationEntity' which is related to this entity.</returns>
		public OrganizationEntity GetSingleOrganization()
		{
			return GetSingleOrganization(false);
		}

		/// <summary> Retrieves the related entity of type 'OrganizationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrganizationEntity' which is related to this entity.</returns>
		public virtual OrganizationEntity GetSingleOrganization(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrganization || forceFetch || _alwaysFetchOrganization) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrganizationEntityUsingOrganizationIDFrameOrganizationID);
				OrganizationEntity newEntity = new OrganizationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FrameOrganizationID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OrganizationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_organizationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Organization = newEntity;
				_alreadyFetchedOrganization = fetchResult;
			}
			return _organization;
		}


		/// <summary> Retrieves the related entity of type 'OrganizationTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrganizationTypeEntity' which is related to this entity.</returns>
		public OrganizationTypeEntity GetSingleOrganizationType()
		{
			return GetSingleOrganizationType(false);
		}

		/// <summary> Retrieves the related entity of type 'OrganizationTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrganizationTypeEntity' which is related to this entity.</returns>
		public virtual OrganizationTypeEntity GetSingleOrganizationType(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrganizationType || forceFetch || _alwaysFetchOrganizationType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrganizationTypeEntityUsingOrganizationTypeID);
				OrganizationTypeEntity newEntity = new OrganizationTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrganizationTypeID);
				}
				if(fetchResult)
				{
					newEntity = (OrganizationTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_organizationTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrganizationType = newEntity;
				_alreadyFetchedOrganizationType = fetchResult;
			}
			return _organizationType;
		}

		/// <summary> Retrieves the related entity of type 'CardHolderOrganizationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'CardHolderOrganizationEntity' which is related to this entity.</returns>
		public CardHolderOrganizationEntity GetSingleCardHolderOrganization()
		{
			return GetSingleCardHolderOrganization(false);
		}
		
		/// <summary> Retrieves the related entity of type 'CardHolderOrganizationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardHolderOrganizationEntity' which is related to this entity.</returns>
		public virtual CardHolderOrganizationEntity GetSingleCardHolderOrganization(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardHolderOrganization || forceFetch || _alwaysFetchCardHolderOrganization) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardHolderOrganizationEntityUsingOrganizationID);
				CardHolderOrganizationEntity newEntity = new CardHolderOrganizationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrganizationID);
				}
				if(fetchResult)
				{
					newEntity = (CardHolderOrganizationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardHolderOrganizationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardHolderOrganization = newEntity;
				_alreadyFetchedCardHolderOrganization = fetchResult;
			}
			return _cardHolderOrganization;
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSingleContactPerson()
		{
			return GetSingleContactPerson(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSingleContactPerson(bool forceFetch)
		{
			if( ( !_alreadyFetchedContactPerson || forceFetch || _alwaysFetchContactPerson) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingContactPersonID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphic(this.Transaction, this.ContactPersonID, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contactPersonReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ContactPerson = newEntity;
				_alreadyFetchedContactPerson = fetchResult;
			}
			return _contactPerson;
		}

		/// <summary> Retrieves the related entity of type 'SubsidyLimitEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'SubsidyLimitEntity' which is related to this entity.</returns>
		public SubsidyLimitEntity GetSingleSubsidyLimit()
		{
			return GetSingleSubsidyLimit(false);
		}
		
		/// <summary> Retrieves the related entity of type 'SubsidyLimitEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SubsidyLimitEntity' which is related to this entity.</returns>
		public virtual SubsidyLimitEntity GetSingleSubsidyLimit(bool forceFetch)
		{
			if( ( !_alreadyFetchedSubsidyLimit || forceFetch || _alwaysFetchSubsidyLimit) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SubsidyLimitEntityUsingOrganizationID);
				SubsidyLimitEntity newEntity = new SubsidyLimitEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCOrganizationID(this.OrganizationID);
				}
				if(fetchResult)
				{
					newEntity = (SubsidyLimitEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_subsidyLimitReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SubsidyLimit = newEntity;
				_alreadyFetchedSubsidyLimit = fetchResult;
			}
			return _subsidyLimit;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Organization", _organization);
			toReturn.Add("OrganizationType", _organizationType);
			toReturn.Add("TicketOrganizations", _ticketOrganizations);
			toReturn.Add("Contracts", _contracts);
			toReturn.Add("Organizations", _organizations);
			toReturn.Add("OrganizationAddresses", _organizationAddresses);
			toReturn.Add("Products", _products);
			toReturn.Add("ProductSubsidies", _productSubsidies);
			toReturn.Add("TicketAssignments", _ticketAssignments);
			toReturn.Add("TicketCollectionViaTicketOrganization", _ticketCollectionViaTicketOrganization);
			toReturn.Add("CardHolderOrganization", _cardHolderOrganization);
			toReturn.Add("ContactPerson", _contactPerson);
			toReturn.Add("SubsidyLimit", _subsidyLimit);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="organizationID">PK value for Organization which data should be fetched into this Organization object</param>
		/// <param name="validator">The validator object for this OrganizationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 organizationID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(organizationID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_ticketOrganizations = new VarioSL.Entities.CollectionClasses.TicketOrganizationCollection();
			_ticketOrganizations.SetContainingEntityInfo(this, "Organization");

			_contracts = new VarioSL.Entities.CollectionClasses.ContractCollection();
			_contracts.SetContainingEntityInfo(this, "Organization");

			_organizations = new VarioSL.Entities.CollectionClasses.OrganizationCollection();
			_organizations.SetContainingEntityInfo(this, "Organization");

			_organizationAddresses = new VarioSL.Entities.CollectionClasses.OrganizationAddressCollection();
			_organizationAddresses.SetContainingEntityInfo(this, "Organization");

			_products = new VarioSL.Entities.CollectionClasses.ProductCollection();
			_products.SetContainingEntityInfo(this, "Organization");

			_productSubsidies = new VarioSL.Entities.CollectionClasses.ProductSubsidyCollection();
			_productSubsidies.SetContainingEntityInfo(this, "Organization");

			_ticketAssignments = new VarioSL.Entities.CollectionClasses.TicketAssignmentCollection();
			_ticketAssignments.SetContainingEntityInfo(this, "Organization");
			_ticketCollectionViaTicketOrganization = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_organizationReturnsNewIfNotFound = false;
			_organizationTypeReturnsNewIfNotFound = false;
			_cardHolderOrganizationReturnsNewIfNotFound = false;
			_contactPersonReturnsNewIfNotFound = false;
			_subsidyLimitReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Abbreviation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContactPersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Discount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FrameOrganizationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HasProductPool", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IncludeInExport", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCardOwner", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsPreTaxEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTrusted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrdererNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurchaseOrderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SubType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxIDNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _organization</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrganization(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _organization, new PropertyChangedEventHandler( OnOrganizationPropertyChanged ), "Organization", VarioSL.Entities.RelationClasses.StaticOrganizationRelations.OrganizationEntityUsingOrganizationIDFrameOrganizationIDStatic, true, signalRelatedEntity, "Organizations", resetFKFields, new int[] { (int)OrganizationFieldIndex.FrameOrganizationID } );		
			_organization = null;
		}
		
		/// <summary> setups the sync logic for member _organization</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrganization(IEntityCore relatedEntity)
		{
			if(_organization!=relatedEntity)
			{		
				DesetupSyncOrganization(true, true);
				_organization = (OrganizationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _organization, new PropertyChangedEventHandler( OnOrganizationPropertyChanged ), "Organization", VarioSL.Entities.RelationClasses.StaticOrganizationRelations.OrganizationEntityUsingOrganizationIDFrameOrganizationIDStatic, true, ref _alreadyFetchedOrganization, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrganizationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _organizationType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrganizationType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _organizationType, new PropertyChangedEventHandler( OnOrganizationTypePropertyChanged ), "OrganizationType", VarioSL.Entities.RelationClasses.StaticOrganizationRelations.OrganizationTypeEntityUsingOrganizationTypeIDStatic, true, signalRelatedEntity, "Organizations", resetFKFields, new int[] { (int)OrganizationFieldIndex.OrganizationTypeID } );		
			_organizationType = null;
		}
		
		/// <summary> setups the sync logic for member _organizationType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrganizationType(IEntityCore relatedEntity)
		{
			if(_organizationType!=relatedEntity)
			{		
				DesetupSyncOrganizationType(true, true);
				_organizationType = (OrganizationTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _organizationType, new PropertyChangedEventHandler( OnOrganizationTypePropertyChanged ), "OrganizationType", VarioSL.Entities.RelationClasses.StaticOrganizationRelations.OrganizationTypeEntityUsingOrganizationTypeIDStatic, true, ref _alreadyFetchedOrganizationType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrganizationTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cardHolderOrganization</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardHolderOrganization(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardHolderOrganization, new PropertyChangedEventHandler( OnCardHolderOrganizationPropertyChanged ), "CardHolderOrganization", VarioSL.Entities.RelationClasses.StaticOrganizationRelations.CardHolderOrganizationEntityUsingOrganizationIDStatic, false, signalRelatedEntity, "Organization", false, new int[] { (int)OrganizationFieldIndex.OrganizationID } );
			_cardHolderOrganization = null;
		}
	
		/// <summary> setups the sync logic for member _cardHolderOrganization</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardHolderOrganization(IEntityCore relatedEntity)
		{
			if(_cardHolderOrganization!=relatedEntity)
			{
				DesetupSyncCardHolderOrganization(true, true);
				_cardHolderOrganization = (CardHolderOrganizationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardHolderOrganization, new PropertyChangedEventHandler( OnCardHolderOrganizationPropertyChanged ), "CardHolderOrganization", VarioSL.Entities.RelationClasses.StaticOrganizationRelations.CardHolderOrganizationEntityUsingOrganizationIDStatic, false, ref _alreadyFetchedCardHolderOrganization, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardHolderOrganizationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contactPerson</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContactPerson(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contactPerson, new PropertyChangedEventHandler( OnContactPersonPropertyChanged ), "ContactPerson", VarioSL.Entities.RelationClasses.StaticOrganizationRelations.PersonEntityUsingContactPersonIDStatic, true, signalRelatedEntity, "ContactPersonOnOrganization", resetFKFields, new int[] { (int)OrganizationFieldIndex.ContactPersonID } );
			_contactPerson = null;
		}
	
		/// <summary> setups the sync logic for member _contactPerson</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContactPerson(IEntityCore relatedEntity)
		{
			if(_contactPerson!=relatedEntity)
			{
				DesetupSyncContactPerson(true, true);
				_contactPerson = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contactPerson, new PropertyChangedEventHandler( OnContactPersonPropertyChanged ), "ContactPerson", VarioSL.Entities.RelationClasses.StaticOrganizationRelations.PersonEntityUsingContactPersonIDStatic, true, ref _alreadyFetchedContactPerson, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContactPersonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _subsidyLimit</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSubsidyLimit(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _subsidyLimit, new PropertyChangedEventHandler( OnSubsidyLimitPropertyChanged ), "SubsidyLimit", VarioSL.Entities.RelationClasses.StaticOrganizationRelations.SubsidyLimitEntityUsingOrganizationIDStatic, false, signalRelatedEntity, "Organization", false, new int[] { (int)OrganizationFieldIndex.OrganizationID } );
			_subsidyLimit = null;
		}
	
		/// <summary> setups the sync logic for member _subsidyLimit</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSubsidyLimit(IEntityCore relatedEntity)
		{
			if(_subsidyLimit!=relatedEntity)
			{
				DesetupSyncSubsidyLimit(true, true);
				_subsidyLimit = (SubsidyLimitEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _subsidyLimit, new PropertyChangedEventHandler( OnSubsidyLimitPropertyChanged ), "SubsidyLimit", VarioSL.Entities.RelationClasses.StaticOrganizationRelations.SubsidyLimitEntityUsingOrganizationIDStatic, false, ref _alreadyFetchedSubsidyLimit, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSubsidyLimitPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="organizationID">PK value for Organization which data should be fetched into this Organization object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 organizationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OrganizationFieldIndex.OrganizationID].ForcedCurrentValueWrite(organizationID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrganizationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OrganizationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OrganizationRelations Relations
		{
			get	{ return new OrganizationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketOrganization' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketOrganizations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketOrganizationCollection(), (IEntityRelation)GetRelationsForField("TicketOrganizations")[0], (int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.TicketOrganizationEntity, 0, null, null, null, "TicketOrganizations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContracts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contracts")[0], (int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contracts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Organization' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganizations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationCollection(), (IEntityRelation)GetRelationsForField("Organizations")[0], (int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.OrganizationEntity, 0, null, null, null, "Organizations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrganizationAddress' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganizationAddresses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationAddressCollection(), (IEntityRelation)GetRelationsForField("OrganizationAddresses")[0], (int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.OrganizationAddressEntity, 0, null, null, null, "OrganizationAddresses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Products")[0], (int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Products", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductSubsidy' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductSubsidies
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductSubsidyCollection(), (IEntityRelation)GetRelationsForField("ProductSubsidies")[0], (int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.ProductSubsidyEntity, 0, null, null, null, "ProductSubsidies", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketAssignment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketAssignments
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketAssignmentCollection(), (IEntityRelation)GetRelationsForField("TicketAssignments")[0], (int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.TicketAssignmentEntity, 0, null, null, null, "TicketAssignments", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketCollectionViaTicketOrganization
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketOrganizationEntityUsingOrganizationID;
				intermediateRelation.SetAliases(string.Empty, "TicketOrganization_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, GetRelationsForField("TicketCollectionViaTicketOrganization"), "TicketCollectionViaTicketOrganization", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Organization'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganization
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationCollection(), (IEntityRelation)GetRelationsForField("Organization")[0], (int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.OrganizationEntity, 0, null, null, null, "Organization", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrganizationType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganizationType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationTypeCollection(), (IEntityRelation)GetRelationsForField("OrganizationType")[0], (int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.OrganizationTypeEntity, 0, null, null, null, "OrganizationType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardHolderOrganization'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardHolderOrganization
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardHolderOrganizationCollection(), (IEntityRelation)GetRelationsForField("CardHolderOrganization")[0], (int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.CardHolderOrganizationEntity, 0, null, null, null, "CardHolderOrganization", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContactPerson
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("ContactPerson")[0], (int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "ContactPerson", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SubsidyLimit'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSubsidyLimit
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SubsidyLimitCollection(), (IEntityRelation)GetRelationsForField("SubsidyLimit")[0], (int)VarioSL.Entities.EntityType.OrganizationEntity, (int)VarioSL.Entities.EntityType.SubsidyLimitEntity, 0, null, null, null, "SubsidyLimit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Abbreviation property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."ABBREVIATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Abbreviation
		{
			get { return (System.String)GetValue((int)OrganizationFieldIndex.Abbreviation, true); }
			set	{ SetValue((int)OrganizationFieldIndex.Abbreviation, value, true); }
		}

		/// <summary> The ContactPersonID property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."CONTACTPERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ContactPersonID
		{
			get { return (System.Int64)GetValue((int)OrganizationFieldIndex.ContactPersonID, true); }
			set	{ SetValue((int)OrganizationFieldIndex.ContactPersonID, value, true); }
		}

		/// <summary> The Description property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)OrganizationFieldIndex.Description, true); }
			set	{ SetValue((int)OrganizationFieldIndex.Description, value, true); }
		}

		/// <summary> The Discount property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."DISCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Discount
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrganizationFieldIndex.Discount, false); }
			set	{ SetValue((int)OrganizationFieldIndex.Discount, value, true); }
		}

		/// <summary> The ExternalNumber property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."EXTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalNumber
		{
			get { return (System.String)GetValue((int)OrganizationFieldIndex.ExternalNumber, true); }
			set	{ SetValue((int)OrganizationFieldIndex.ExternalNumber, value, true); }
		}

		/// <summary> The ExternalType property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."EXTERNALTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalType
		{
			get { return (System.String)GetValue((int)OrganizationFieldIndex.ExternalType, true); }
			set	{ SetValue((int)OrganizationFieldIndex.ExternalType, value, true); }
		}

		/// <summary> The FrameOrganizationID property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."FRAMEORGANIZATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FrameOrganizationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrganizationFieldIndex.FrameOrganizationID, false); }
			set	{ SetValue((int)OrganizationFieldIndex.FrameOrganizationID, value, true); }
		}

		/// <summary> The HasProductPool property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."HASPRODUCTPOOL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HasProductPool
		{
			get { return (System.Boolean)GetValue((int)OrganizationFieldIndex.HasProductPool, true); }
			set	{ SetValue((int)OrganizationFieldIndex.HasProductPool, value, true); }
		}

		/// <summary> The IncludeInExport property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."INCLUDEINEXPORT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IncludeInExport
		{
			get { return (System.Boolean)GetValue((int)OrganizationFieldIndex.IncludeInExport, true); }
			set	{ SetValue((int)OrganizationFieldIndex.IncludeInExport, value, true); }
		}

		/// <summary> The IsCardOwner property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."ISCARDOWNER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsCardOwner
		{
			get { return (System.Boolean)GetValue((int)OrganizationFieldIndex.IsCardOwner, true); }
			set	{ SetValue((int)OrganizationFieldIndex.IsCardOwner, value, true); }
		}

		/// <summary> The IsPreTaxEnabled property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."ISPRETAXENABLED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsPreTaxEnabled
		{
			get { return (System.Boolean)GetValue((int)OrganizationFieldIndex.IsPreTaxEnabled, true); }
			set	{ SetValue((int)OrganizationFieldIndex.IsPreTaxEnabled, value, true); }
		}

		/// <summary> The IsTrusted property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."ISTRUSTED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTrusted
		{
			get { return (System.Boolean)GetValue((int)OrganizationFieldIndex.IsTrusted, true); }
			set	{ SetValue((int)OrganizationFieldIndex.IsTrusted, value, true); }
		}

		/// <summary> The LastModified property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)OrganizationFieldIndex.LastModified, true); }
			set	{ SetValue((int)OrganizationFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)OrganizationFieldIndex.LastUser, true); }
			set	{ SetValue((int)OrganizationFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)OrganizationFieldIndex.Name, true); }
			set	{ SetValue((int)OrganizationFieldIndex.Name, value, true); }
		}

		/// <summary> The OrdererNumber property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."ORDERERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrdererNumber
		{
			get { return (System.String)GetValue((int)OrganizationFieldIndex.OrdererNumber, true); }
			set	{ SetValue((int)OrganizationFieldIndex.OrdererNumber, value, true); }
		}

		/// <summary> The OrganizationID property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."ORGANIZATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 OrganizationID
		{
			get { return (System.Int64)GetValue((int)OrganizationFieldIndex.OrganizationID, true); }
			set	{ SetValue((int)OrganizationFieldIndex.OrganizationID, value, true); }
		}

		/// <summary> The OrganizationNumber property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."ORGANIZATIONNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrganizationNumber
		{
			get { return (System.String)GetValue((int)OrganizationFieldIndex.OrganizationNumber, true); }
			set	{ SetValue((int)OrganizationFieldIndex.OrganizationNumber, value, true); }
		}

		/// <summary> The OrganizationTypeID property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."ORGANIZATIONTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 OrganizationTypeID
		{
			get { return (System.Int64)GetValue((int)OrganizationFieldIndex.OrganizationTypeID, true); }
			set	{ SetValue((int)OrganizationFieldIndex.OrganizationTypeID, value, true); }
		}

		/// <summary> The PrintName property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."PRINTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrintName
		{
			get { return (System.String)GetValue((int)OrganizationFieldIndex.PrintName, true); }
			set	{ SetValue((int)OrganizationFieldIndex.PrintName, value, true); }
		}

		/// <summary> The PurchaseOrderNumber property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."PURCHASEORDERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PurchaseOrderNumber
		{
			get { return (System.String)GetValue((int)OrganizationFieldIndex.PurchaseOrderNumber, true); }
			set	{ SetValue((int)OrganizationFieldIndex.PurchaseOrderNumber, value, true); }
		}

		/// <summary> The SubType property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."SUBTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.OrganizationSubType> SubType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.OrganizationSubType>)GetValue((int)OrganizationFieldIndex.SubType, false); }
			set	{ SetValue((int)OrganizationFieldIndex.SubType, value, true); }
		}

		/// <summary> The TaxIDNumber property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."TAXIDNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TaxIDNumber
		{
			get { return (System.String)GetValue((int)OrganizationFieldIndex.TaxIDNumber, true); }
			set	{ SetValue((int)OrganizationFieldIndex.TaxIDNumber, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)OrganizationFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)OrganizationFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The Type property of the Entity Organization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATION"."ORGANIZATIONTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Type
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrganizationFieldIndex.Type, false); }
			set	{ SetValue((int)OrganizationFieldIndex.Type, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'TicketOrganizationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketOrganizations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketOrganizationCollection TicketOrganizations
		{
			get	{ return GetMultiTicketOrganizations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketOrganizations. When set to true, TicketOrganizations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketOrganizations is accessed. You can always execute/ a forced fetch by calling GetMultiTicketOrganizations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketOrganizations
		{
			get	{ return _alwaysFetchTicketOrganizations; }
			set	{ _alwaysFetchTicketOrganizations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketOrganizations already has been fetched. Setting this property to false when TicketOrganizations has been fetched
		/// will clear the TicketOrganizations collection well. Setting this property to true while TicketOrganizations hasn't been fetched disables lazy loading for TicketOrganizations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketOrganizations
		{
			get { return _alreadyFetchedTicketOrganizations;}
			set 
			{
				if(_alreadyFetchedTicketOrganizations && !value && (_ticketOrganizations != null))
				{
					_ticketOrganizations.Clear();
				}
				_alreadyFetchedTicketOrganizations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContracts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractCollection Contracts
		{
			get	{ return GetMultiContracts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Contracts. When set to true, Contracts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contracts is accessed. You can always execute/ a forced fetch by calling GetMultiContracts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContracts
		{
			get	{ return _alwaysFetchContracts; }
			set	{ _alwaysFetchContracts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contracts already has been fetched. Setting this property to false when Contracts has been fetched
		/// will clear the Contracts collection well. Setting this property to true while Contracts hasn't been fetched disables lazy loading for Contracts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContracts
		{
			get { return _alreadyFetchedContracts;}
			set 
			{
				if(_alreadyFetchedContracts && !value && (_contracts != null))
				{
					_contracts.Clear();
				}
				_alreadyFetchedContracts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrganizations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationCollection Organizations
		{
			get	{ return GetMultiOrganizations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Organizations. When set to true, Organizations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Organizations is accessed. You can always execute/ a forced fetch by calling GetMultiOrganizations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganizations
		{
			get	{ return _alwaysFetchOrganizations; }
			set	{ _alwaysFetchOrganizations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Organizations already has been fetched. Setting this property to false when Organizations has been fetched
		/// will clear the Organizations collection well. Setting this property to true while Organizations hasn't been fetched disables lazy loading for Organizations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganizations
		{
			get { return _alreadyFetchedOrganizations;}
			set 
			{
				if(_alreadyFetchedOrganizations && !value && (_organizations != null))
				{
					_organizations.Clear();
				}
				_alreadyFetchedOrganizations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrganizationAddressEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrganizationAddresses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationAddressCollection OrganizationAddresses
		{
			get	{ return GetMultiOrganizationAddresses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrganizationAddresses. When set to true, OrganizationAddresses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrganizationAddresses is accessed. You can always execute/ a forced fetch by calling GetMultiOrganizationAddresses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganizationAddresses
		{
			get	{ return _alwaysFetchOrganizationAddresses; }
			set	{ _alwaysFetchOrganizationAddresses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrganizationAddresses already has been fetched. Setting this property to false when OrganizationAddresses has been fetched
		/// will clear the OrganizationAddresses collection well. Setting this property to true while OrganizationAddresses hasn't been fetched disables lazy loading for OrganizationAddresses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganizationAddresses
		{
			get { return _alreadyFetchedOrganizationAddresses;}
			set 
			{
				if(_alreadyFetchedOrganizationAddresses && !value && (_organizationAddresses != null))
				{
					_organizationAddresses.Clear();
				}
				_alreadyFetchedOrganizationAddresses = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection Products
		{
			get	{ return GetMultiProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Products. When set to true, Products is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Products is accessed. You can always execute/ a forced fetch by calling GetMultiProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProducts
		{
			get	{ return _alwaysFetchProducts; }
			set	{ _alwaysFetchProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Products already has been fetched. Setting this property to false when Products has been fetched
		/// will clear the Products collection well. Setting this property to true while Products hasn't been fetched disables lazy loading for Products</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProducts
		{
			get { return _alreadyFetchedProducts;}
			set 
			{
				if(_alreadyFetchedProducts && !value && (_products != null))
				{
					_products.Clear();
				}
				_alreadyFetchedProducts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductSubsidyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductSubsidies()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductSubsidyCollection ProductSubsidies
		{
			get	{ return GetMultiProductSubsidies(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductSubsidies. When set to true, ProductSubsidies is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductSubsidies is accessed. You can always execute/ a forced fetch by calling GetMultiProductSubsidies(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductSubsidies
		{
			get	{ return _alwaysFetchProductSubsidies; }
			set	{ _alwaysFetchProductSubsidies = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductSubsidies already has been fetched. Setting this property to false when ProductSubsidies has been fetched
		/// will clear the ProductSubsidies collection well. Setting this property to true while ProductSubsidies hasn't been fetched disables lazy loading for ProductSubsidies</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductSubsidies
		{
			get { return _alreadyFetchedProductSubsidies;}
			set 
			{
				if(_alreadyFetchedProductSubsidies && !value && (_productSubsidies != null))
				{
					_productSubsidies.Clear();
				}
				_alreadyFetchedProductSubsidies = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketAssignments()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketAssignmentCollection TicketAssignments
		{
			get	{ return GetMultiTicketAssignments(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketAssignments. When set to true, TicketAssignments is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketAssignments is accessed. You can always execute/ a forced fetch by calling GetMultiTicketAssignments(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketAssignments
		{
			get	{ return _alwaysFetchTicketAssignments; }
			set	{ _alwaysFetchTicketAssignments = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketAssignments already has been fetched. Setting this property to false when TicketAssignments has been fetched
		/// will clear the TicketAssignments collection well. Setting this property to true while TicketAssignments hasn't been fetched disables lazy loading for TicketAssignments</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketAssignments
		{
			get { return _alreadyFetchedTicketAssignments;}
			set 
			{
				if(_alreadyFetchedTicketAssignments && !value && (_ticketAssignments != null))
				{
					_ticketAssignments.Clear();
				}
				_alreadyFetchedTicketAssignments = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketCollectionViaTicketOrganization()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketCollectionViaTicketOrganization
		{
			get { return GetMultiTicketCollectionViaTicketOrganization(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketCollectionViaTicketOrganization. When set to true, TicketCollectionViaTicketOrganization is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketCollectionViaTicketOrganization is accessed. You can always execute a forced fetch by calling GetMultiTicketCollectionViaTicketOrganization(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketCollectionViaTicketOrganization
		{
			get	{ return _alwaysFetchTicketCollectionViaTicketOrganization; }
			set	{ _alwaysFetchTicketCollectionViaTicketOrganization = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketCollectionViaTicketOrganization already has been fetched. Setting this property to false when TicketCollectionViaTicketOrganization has been fetched
		/// will clear the TicketCollectionViaTicketOrganization collection well. Setting this property to true while TicketCollectionViaTicketOrganization hasn't been fetched disables lazy loading for TicketCollectionViaTicketOrganization</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketCollectionViaTicketOrganization
		{
			get { return _alreadyFetchedTicketCollectionViaTicketOrganization;}
			set 
			{
				if(_alreadyFetchedTicketCollectionViaTicketOrganization && !value && (_ticketCollectionViaTicketOrganization != null))
				{
					_ticketCollectionViaTicketOrganization.Clear();
				}
				_alreadyFetchedTicketCollectionViaTicketOrganization = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'OrganizationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrganization()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OrganizationEntity Organization
		{
			get	{ return GetSingleOrganization(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrganization(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Organizations", "Organization", _organization, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Organization. When set to true, Organization is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Organization is accessed. You can always execute a forced fetch by calling GetSingleOrganization(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganization
		{
			get	{ return _alwaysFetchOrganization; }
			set	{ _alwaysFetchOrganization = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Organization already has been fetched. Setting this property to false when Organization has been fetched
		/// will set Organization to null as well. Setting this property to true while Organization hasn't been fetched disables lazy loading for Organization</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganization
		{
			get { return _alreadyFetchedOrganization;}
			set 
			{
				if(_alreadyFetchedOrganization && !value)
				{
					this.Organization = null;
				}
				_alreadyFetchedOrganization = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Organization is not found
		/// in the database. When set to true, Organization will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OrganizationReturnsNewIfNotFound
		{
			get	{ return _organizationReturnsNewIfNotFound; }
			set { _organizationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrganizationTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrganizationType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OrganizationTypeEntity OrganizationType
		{
			get	{ return GetSingleOrganizationType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrganizationType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Organizations", "OrganizationType", _organizationType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrganizationType. When set to true, OrganizationType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrganizationType is accessed. You can always execute a forced fetch by calling GetSingleOrganizationType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganizationType
		{
			get	{ return _alwaysFetchOrganizationType; }
			set	{ _alwaysFetchOrganizationType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrganizationType already has been fetched. Setting this property to false when OrganizationType has been fetched
		/// will set OrganizationType to null as well. Setting this property to true while OrganizationType hasn't been fetched disables lazy loading for OrganizationType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganizationType
		{
			get { return _alreadyFetchedOrganizationType;}
			set 
			{
				if(_alreadyFetchedOrganizationType && !value)
				{
					this.OrganizationType = null;
				}
				_alreadyFetchedOrganizationType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrganizationType is not found
		/// in the database. When set to true, OrganizationType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OrganizationTypeReturnsNewIfNotFound
		{
			get	{ return _organizationTypeReturnsNewIfNotFound; }
			set { _organizationTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardHolderOrganizationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardHolderOrganization()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardHolderOrganizationEntity CardHolderOrganization
		{
			get	{ return GetSingleCardHolderOrganization(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncCardHolderOrganization(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_cardHolderOrganization !=null);
						DesetupSyncCardHolderOrganization(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("CardHolderOrganization");
						}
					}
					else
					{
						if(_cardHolderOrganization!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Organization");
							SetupSyncCardHolderOrganization(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardHolderOrganization. When set to true, CardHolderOrganization is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardHolderOrganization is accessed. You can always execute a forced fetch by calling GetSingleCardHolderOrganization(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardHolderOrganization
		{
			get	{ return _alwaysFetchCardHolderOrganization; }
			set	{ _alwaysFetchCardHolderOrganization = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property CardHolderOrganization already has been fetched. Setting this property to false when CardHolderOrganization has been fetched
		/// will set CardHolderOrganization to null as well. Setting this property to true while CardHolderOrganization hasn't been fetched disables lazy loading for CardHolderOrganization</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardHolderOrganization
		{
			get { return _alreadyFetchedCardHolderOrganization;}
			set 
			{
				if(_alreadyFetchedCardHolderOrganization && !value)
				{
					this.CardHolderOrganization = null;
				}
				_alreadyFetchedCardHolderOrganization = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardHolderOrganization is not found
		/// in the database. When set to true, CardHolderOrganization will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardHolderOrganizationReturnsNewIfNotFound
		{
			get	{ return _cardHolderOrganizationReturnsNewIfNotFound; }
			set	{ _cardHolderOrganizationReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContactPerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity ContactPerson
		{
			get	{ return GetSingleContactPerson(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncContactPerson(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_contactPerson !=null);
						DesetupSyncContactPerson(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("ContactPerson");
						}
					}
					else
					{
						if(_contactPerson!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "ContactPersonOnOrganization");
							SetupSyncContactPerson(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ContactPerson. When set to true, ContactPerson is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContactPerson is accessed. You can always execute a forced fetch by calling GetSingleContactPerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContactPerson
		{
			get	{ return _alwaysFetchContactPerson; }
			set	{ _alwaysFetchContactPerson = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property ContactPerson already has been fetched. Setting this property to false when ContactPerson has been fetched
		/// will set ContactPerson to null as well. Setting this property to true while ContactPerson hasn't been fetched disables lazy loading for ContactPerson</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContactPerson
		{
			get { return _alreadyFetchedContactPerson;}
			set 
			{
				if(_alreadyFetchedContactPerson && !value)
				{
					this.ContactPerson = null;
				}
				_alreadyFetchedContactPerson = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ContactPerson is not found
		/// in the database. When set to true, ContactPerson will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContactPersonReturnsNewIfNotFound
		{
			get	{ return _contactPersonReturnsNewIfNotFound; }
			set	{ _contactPersonReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'SubsidyLimitEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSubsidyLimit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SubsidyLimitEntity SubsidyLimit
		{
			get	{ return GetSingleSubsidyLimit(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncSubsidyLimit(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_subsidyLimit !=null);
						DesetupSyncSubsidyLimit(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("SubsidyLimit");
						}
					}
					else
					{
						if(_subsidyLimit!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Organization");
							SetupSyncSubsidyLimit(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SubsidyLimit. When set to true, SubsidyLimit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SubsidyLimit is accessed. You can always execute a forced fetch by calling GetSingleSubsidyLimit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSubsidyLimit
		{
			get	{ return _alwaysFetchSubsidyLimit; }
			set	{ _alwaysFetchSubsidyLimit = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property SubsidyLimit already has been fetched. Setting this property to false when SubsidyLimit has been fetched
		/// will set SubsidyLimit to null as well. Setting this property to true while SubsidyLimit hasn't been fetched disables lazy loading for SubsidyLimit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSubsidyLimit
		{
			get { return _alreadyFetchedSubsidyLimit;}
			set 
			{
				if(_alreadyFetchedSubsidyLimit && !value)
				{
					this.SubsidyLimit = null;
				}
				_alreadyFetchedSubsidyLimit = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SubsidyLimit is not found
		/// in the database. When set to true, SubsidyLimit will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SubsidyLimitReturnsNewIfNotFound
		{
			get	{ return _subsidyLimitReturnsNewIfNotFound; }
			set	{ _subsidyLimitReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.OrganizationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
