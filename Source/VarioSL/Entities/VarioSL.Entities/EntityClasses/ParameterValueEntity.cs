﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ParameterValue'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ParameterValueEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private DeviceClassEntity _deviceClass;
		private bool	_alwaysFetchDeviceClass, _alreadyFetchedDeviceClass, _deviceClassReturnsNewIfNotFound;
		private UnitEntity _unit;
		private bool	_alwaysFetchUnit, _alreadyFetchedUnit, _unitReturnsNewIfNotFound;
		private ParameterEntity _parameter;
		private bool	_alwaysFetchParameter, _alreadyFetchedParameter, _parameterReturnsNewIfNotFound;
		private ParameterArchiveEntity _parameterArchive;
		private bool	_alwaysFetchParameterArchive, _alreadyFetchedParameterArchive, _parameterArchiveReturnsNewIfNotFound;
		private UnitCollectionEntity _unitCollection;
		private bool	_alwaysFetchUnitCollection, _alreadyFetchedUnitCollection, _unitCollectionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeviceClass</summary>
			public static readonly string DeviceClass = "DeviceClass";
			/// <summary>Member name Unit</summary>
			public static readonly string Unit = "Unit";
			/// <summary>Member name Parameter</summary>
			public static readonly string Parameter = "Parameter";
			/// <summary>Member name ParameterArchive</summary>
			public static readonly string ParameterArchive = "ParameterArchive";
			/// <summary>Member name UnitCollection</summary>
			public static readonly string UnitCollection = "UnitCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ParameterValueEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ParameterValueEntity() :base("ParameterValueEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="parameterValueID">PK value for ParameterValue which data should be fetched into this ParameterValue object</param>
		public ParameterValueEntity(System.Int64 parameterValueID):base("ParameterValueEntity")
		{
			InitClassFetch(parameterValueID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="parameterValueID">PK value for ParameterValue which data should be fetched into this ParameterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ParameterValueEntity(System.Int64 parameterValueID, IPrefetchPath prefetchPathToUse):base("ParameterValueEntity")
		{
			InitClassFetch(parameterValueID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="parameterValueID">PK value for ParameterValue which data should be fetched into this ParameterValue object</param>
		/// <param name="validator">The custom validator object for this ParameterValueEntity</param>
		public ParameterValueEntity(System.Int64 parameterValueID, IValidator validator):base("ParameterValueEntity")
		{
			InitClassFetch(parameterValueID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ParameterValueEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_deviceClass = (DeviceClassEntity)info.GetValue("_deviceClass", typeof(DeviceClassEntity));
			if(_deviceClass!=null)
			{
				_deviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceClassReturnsNewIfNotFound = info.GetBoolean("_deviceClassReturnsNewIfNotFound");
			_alwaysFetchDeviceClass = info.GetBoolean("_alwaysFetchDeviceClass");
			_alreadyFetchedDeviceClass = info.GetBoolean("_alreadyFetchedDeviceClass");

			_unit = (UnitEntity)info.GetValue("_unit", typeof(UnitEntity));
			if(_unit!=null)
			{
				_unit.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_unitReturnsNewIfNotFound = info.GetBoolean("_unitReturnsNewIfNotFound");
			_alwaysFetchUnit = info.GetBoolean("_alwaysFetchUnit");
			_alreadyFetchedUnit = info.GetBoolean("_alreadyFetchedUnit");

			_parameter = (ParameterEntity)info.GetValue("_parameter", typeof(ParameterEntity));
			if(_parameter!=null)
			{
				_parameter.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parameterReturnsNewIfNotFound = info.GetBoolean("_parameterReturnsNewIfNotFound");
			_alwaysFetchParameter = info.GetBoolean("_alwaysFetchParameter");
			_alreadyFetchedParameter = info.GetBoolean("_alreadyFetchedParameter");

			_parameterArchive = (ParameterArchiveEntity)info.GetValue("_parameterArchive", typeof(ParameterArchiveEntity));
			if(_parameterArchive!=null)
			{
				_parameterArchive.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parameterArchiveReturnsNewIfNotFound = info.GetBoolean("_parameterArchiveReturnsNewIfNotFound");
			_alwaysFetchParameterArchive = info.GetBoolean("_alwaysFetchParameterArchive");
			_alreadyFetchedParameterArchive = info.GetBoolean("_alreadyFetchedParameterArchive");

			_unitCollection = (UnitCollectionEntity)info.GetValue("_unitCollection", typeof(UnitCollectionEntity));
			if(_unitCollection!=null)
			{
				_unitCollection.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_unitCollectionReturnsNewIfNotFound = info.GetBoolean("_unitCollectionReturnsNewIfNotFound");
			_alwaysFetchUnitCollection = info.GetBoolean("_alwaysFetchUnitCollection");
			_alreadyFetchedUnitCollection = info.GetBoolean("_alreadyFetchedUnitCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ParameterValueFieldIndex)fieldIndex)
			{
				case ParameterValueFieldIndex.DeviceClassID:
					DesetupSyncDeviceClass(true, false);
					_alreadyFetchedDeviceClass = false;
					break;
				case ParameterValueFieldIndex.ParameterArchiveID:
					DesetupSyncParameterArchive(true, false);
					_alreadyFetchedParameterArchive = false;
					break;
				case ParameterValueFieldIndex.ParameterID:
					DesetupSyncParameter(true, false);
					_alreadyFetchedParameter = false;
					break;
				case ParameterValueFieldIndex.UnitCollectionID:
					DesetupSyncUnitCollection(true, false);
					_alreadyFetchedUnitCollection = false;
					break;
				case ParameterValueFieldIndex.UnitID:
					DesetupSyncUnit(true, false);
					_alreadyFetchedUnit = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDeviceClass = (_deviceClass != null);
			_alreadyFetchedUnit = (_unit != null);
			_alreadyFetchedParameter = (_parameter != null);
			_alreadyFetchedParameterArchive = (_parameterArchive != null);
			_alreadyFetchedUnitCollection = (_unitCollection != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeviceClass":
					toReturn.Add(Relations.DeviceClassEntityUsingDeviceClassID);
					break;
				case "Unit":
					toReturn.Add(Relations.UnitEntityUsingUnitID);
					break;
				case "Parameter":
					toReturn.Add(Relations.ParameterEntityUsingParameterID);
					break;
				case "ParameterArchive":
					toReturn.Add(Relations.ParameterArchiveEntityUsingParameterArchiveID);
					break;
				case "UnitCollection":
					toReturn.Add(Relations.UnitCollectionEntityUsingUnitCollectionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_deviceClass", (!this.MarkedForDeletion?_deviceClass:null));
			info.AddValue("_deviceClassReturnsNewIfNotFound", _deviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceClass", _alwaysFetchDeviceClass);
			info.AddValue("_alreadyFetchedDeviceClass", _alreadyFetchedDeviceClass);
			info.AddValue("_unit", (!this.MarkedForDeletion?_unit:null));
			info.AddValue("_unitReturnsNewIfNotFound", _unitReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUnit", _alwaysFetchUnit);
			info.AddValue("_alreadyFetchedUnit", _alreadyFetchedUnit);
			info.AddValue("_parameter", (!this.MarkedForDeletion?_parameter:null));
			info.AddValue("_parameterReturnsNewIfNotFound", _parameterReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParameter", _alwaysFetchParameter);
			info.AddValue("_alreadyFetchedParameter", _alreadyFetchedParameter);
			info.AddValue("_parameterArchive", (!this.MarkedForDeletion?_parameterArchive:null));
			info.AddValue("_parameterArchiveReturnsNewIfNotFound", _parameterArchiveReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParameterArchive", _alwaysFetchParameterArchive);
			info.AddValue("_alreadyFetchedParameterArchive", _alreadyFetchedParameterArchive);
			info.AddValue("_unitCollection", (!this.MarkedForDeletion?_unitCollection:null));
			info.AddValue("_unitCollectionReturnsNewIfNotFound", _unitCollectionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUnitCollection", _alwaysFetchUnitCollection);
			info.AddValue("_alreadyFetchedUnitCollection", _alreadyFetchedUnitCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeviceClass":
					_alreadyFetchedDeviceClass = true;
					this.DeviceClass = (DeviceClassEntity)entity;
					break;
				case "Unit":
					_alreadyFetchedUnit = true;
					this.Unit = (UnitEntity)entity;
					break;
				case "Parameter":
					_alreadyFetchedParameter = true;
					this.Parameter = (ParameterEntity)entity;
					break;
				case "ParameterArchive":
					_alreadyFetchedParameterArchive = true;
					this.ParameterArchive = (ParameterArchiveEntity)entity;
					break;
				case "UnitCollection":
					_alreadyFetchedUnitCollection = true;
					this.UnitCollection = (UnitCollectionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeviceClass":
					SetupSyncDeviceClass(relatedEntity);
					break;
				case "Unit":
					SetupSyncUnit(relatedEntity);
					break;
				case "Parameter":
					SetupSyncParameter(relatedEntity);
					break;
				case "ParameterArchive":
					SetupSyncParameterArchive(relatedEntity);
					break;
				case "UnitCollection":
					SetupSyncUnitCollection(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeviceClass":
					DesetupSyncDeviceClass(false, true);
					break;
				case "Unit":
					DesetupSyncUnit(false, true);
					break;
				case "Parameter":
					DesetupSyncParameter(false, true);
					break;
				case "ParameterArchive":
					DesetupSyncParameterArchive(false, true);
					break;
				case "UnitCollection":
					DesetupSyncUnitCollection(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_deviceClass!=null)
			{
				toReturn.Add(_deviceClass);
			}
			if(_unit!=null)
			{
				toReturn.Add(_unit);
			}
			if(_parameter!=null)
			{
				toReturn.Add(_parameter);
			}
			if(_parameterArchive!=null)
			{
				toReturn.Add(_parameterArchive);
			}
			if(_unitCollection!=null)
			{
				toReturn.Add(_unitCollection);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterValueID">PK value for ParameterValue which data should be fetched into this ParameterValue object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterValueID)
		{
			return FetchUsingPK(parameterValueID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterValueID">PK value for ParameterValue which data should be fetched into this ParameterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterValueID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(parameterValueID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterValueID">PK value for ParameterValue which data should be fetched into this ParameterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterValueID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(parameterValueID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterValueID">PK value for ParameterValue which data should be fetched into this ParameterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterValueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(parameterValueID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ParameterValueID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ParameterValueRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public DeviceClassEntity GetSingleDeviceClass()
		{
			return GetSingleDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public virtual DeviceClassEntity GetSingleDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceClass || forceFetch || _alwaysFetchDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceClassEntityUsingDeviceClassID);
				DeviceClassEntity newEntity = new DeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceClassID);
				}
				if(fetchResult)
				{
					newEntity = (DeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceClass = newEntity;
				_alreadyFetchedDeviceClass = fetchResult;
			}
			return _deviceClass;
		}


		/// <summary> Retrieves the related entity of type 'UnitEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UnitEntity' which is related to this entity.</returns>
		public UnitEntity GetSingleUnit()
		{
			return GetSingleUnit(false);
		}

		/// <summary> Retrieves the related entity of type 'UnitEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UnitEntity' which is related to this entity.</returns>
		public virtual UnitEntity GetSingleUnit(bool forceFetch)
		{
			if( ( !_alreadyFetchedUnit || forceFetch || _alwaysFetchUnit) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UnitEntityUsingUnitID);
				UnitEntity newEntity = new UnitEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UnitID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UnitEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_unitReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Unit = newEntity;
				_alreadyFetchedUnit = fetchResult;
			}
			return _unit;
		}


		/// <summary> Retrieves the related entity of type 'ParameterEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ParameterEntity' which is related to this entity.</returns>
		public ParameterEntity GetSingleParameter()
		{
			return GetSingleParameter(false);
		}

		/// <summary> Retrieves the related entity of type 'ParameterEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ParameterEntity' which is related to this entity.</returns>
		public virtual ParameterEntity GetSingleParameter(bool forceFetch)
		{
			if( ( !_alreadyFetchedParameter || forceFetch || _alwaysFetchParameter) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ParameterEntityUsingParameterID);
				ParameterEntity newEntity = new ParameterEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParameterID);
				}
				if(fetchResult)
				{
					newEntity = (ParameterEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parameterReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Parameter = newEntity;
				_alreadyFetchedParameter = fetchResult;
			}
			return _parameter;
		}


		/// <summary> Retrieves the related entity of type 'ParameterArchiveEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ParameterArchiveEntity' which is related to this entity.</returns>
		public ParameterArchiveEntity GetSingleParameterArchive()
		{
			return GetSingleParameterArchive(false);
		}

		/// <summary> Retrieves the related entity of type 'ParameterArchiveEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ParameterArchiveEntity' which is related to this entity.</returns>
		public virtual ParameterArchiveEntity GetSingleParameterArchive(bool forceFetch)
		{
			if( ( !_alreadyFetchedParameterArchive || forceFetch || _alwaysFetchParameterArchive) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ParameterArchiveEntityUsingParameterArchiveID);
				ParameterArchiveEntity newEntity = new ParameterArchiveEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParameterArchiveID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ParameterArchiveEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parameterArchiveReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParameterArchive = newEntity;
				_alreadyFetchedParameterArchive = fetchResult;
			}
			return _parameterArchive;
		}


		/// <summary> Retrieves the related entity of type 'UnitCollectionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UnitCollectionEntity' which is related to this entity.</returns>
		public UnitCollectionEntity GetSingleUnitCollection()
		{
			return GetSingleUnitCollection(false);
		}

		/// <summary> Retrieves the related entity of type 'UnitCollectionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UnitCollectionEntity' which is related to this entity.</returns>
		public virtual UnitCollectionEntity GetSingleUnitCollection(bool forceFetch)
		{
			if( ( !_alreadyFetchedUnitCollection || forceFetch || _alwaysFetchUnitCollection) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UnitCollectionEntityUsingUnitCollectionID);
				UnitCollectionEntity newEntity = new UnitCollectionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UnitCollectionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UnitCollectionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_unitCollectionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UnitCollection = newEntity;
				_alreadyFetchedUnitCollection = fetchResult;
			}
			return _unitCollection;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeviceClass", _deviceClass);
			toReturn.Add("Unit", _unit);
			toReturn.Add("Parameter", _parameter);
			toReturn.Add("ParameterArchive", _parameterArchive);
			toReturn.Add("UnitCollection", _unitCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="parameterValueID">PK value for ParameterValue which data should be fetched into this ParameterValue object</param>
		/// <param name="validator">The validator object for this ParameterValueEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 parameterValueID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(parameterValueID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_deviceClassReturnsNewIfNotFound = false;
			_unitReturnsNewIfNotFound = false;
			_parameterReturnsNewIfNotFound = false;
			_parameterArchiveReturnsNewIfNotFound = false;
			_unitCollectionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MountingPlateIndex", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterArchiveID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterValueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnitCollectionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnitID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _deviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticParameterValueRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, signalRelatedEntity, "ParameterValues", resetFKFields, new int[] { (int)ParameterValueFieldIndex.DeviceClassID } );		
			_deviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _deviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceClass(IEntityCore relatedEntity)
		{
			if(_deviceClass!=relatedEntity)
			{		
				DesetupSyncDeviceClass(true, true);
				_deviceClass = (DeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticParameterValueRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, ref _alreadyFetchedDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _unit</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUnit(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _unit, new PropertyChangedEventHandler( OnUnitPropertyChanged ), "Unit", VarioSL.Entities.RelationClasses.StaticParameterValueRelations.UnitEntityUsingUnitIDStatic, true, signalRelatedEntity, "ParameterValues", resetFKFields, new int[] { (int)ParameterValueFieldIndex.UnitID } );		
			_unit = null;
		}
		
		/// <summary> setups the sync logic for member _unit</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUnit(IEntityCore relatedEntity)
		{
			if(_unit!=relatedEntity)
			{		
				DesetupSyncUnit(true, true);
				_unit = (UnitEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _unit, new PropertyChangedEventHandler( OnUnitPropertyChanged ), "Unit", VarioSL.Entities.RelationClasses.StaticParameterValueRelations.UnitEntityUsingUnitIDStatic, true, ref _alreadyFetchedUnit, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUnitPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _parameter</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParameter(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parameter, new PropertyChangedEventHandler( OnParameterPropertyChanged ), "Parameter", VarioSL.Entities.RelationClasses.StaticParameterValueRelations.ParameterEntityUsingParameterIDStatic, true, signalRelatedEntity, "ParameterValues", resetFKFields, new int[] { (int)ParameterValueFieldIndex.ParameterID } );		
			_parameter = null;
		}
		
		/// <summary> setups the sync logic for member _parameter</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParameter(IEntityCore relatedEntity)
		{
			if(_parameter!=relatedEntity)
			{		
				DesetupSyncParameter(true, true);
				_parameter = (ParameterEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parameter, new PropertyChangedEventHandler( OnParameterPropertyChanged ), "Parameter", VarioSL.Entities.RelationClasses.StaticParameterValueRelations.ParameterEntityUsingParameterIDStatic, true, ref _alreadyFetchedParameter, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParameterPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _parameterArchive</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParameterArchive(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parameterArchive, new PropertyChangedEventHandler( OnParameterArchivePropertyChanged ), "ParameterArchive", VarioSL.Entities.RelationClasses.StaticParameterValueRelations.ParameterArchiveEntityUsingParameterArchiveIDStatic, true, signalRelatedEntity, "ParameterValues", resetFKFields, new int[] { (int)ParameterValueFieldIndex.ParameterArchiveID } );		
			_parameterArchive = null;
		}
		
		/// <summary> setups the sync logic for member _parameterArchive</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParameterArchive(IEntityCore relatedEntity)
		{
			if(_parameterArchive!=relatedEntity)
			{		
				DesetupSyncParameterArchive(true, true);
				_parameterArchive = (ParameterArchiveEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parameterArchive, new PropertyChangedEventHandler( OnParameterArchivePropertyChanged ), "ParameterArchive", VarioSL.Entities.RelationClasses.StaticParameterValueRelations.ParameterArchiveEntityUsingParameterArchiveIDStatic, true, ref _alreadyFetchedParameterArchive, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParameterArchivePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _unitCollection</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUnitCollection(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _unitCollection, new PropertyChangedEventHandler( OnUnitCollectionPropertyChanged ), "UnitCollection", VarioSL.Entities.RelationClasses.StaticParameterValueRelations.UnitCollectionEntityUsingUnitCollectionIDStatic, true, signalRelatedEntity, "ParameterValues", resetFKFields, new int[] { (int)ParameterValueFieldIndex.UnitCollectionID } );		
			_unitCollection = null;
		}
		
		/// <summary> setups the sync logic for member _unitCollection</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUnitCollection(IEntityCore relatedEntity)
		{
			if(_unitCollection!=relatedEntity)
			{		
				DesetupSyncUnitCollection(true, true);
				_unitCollection = (UnitCollectionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _unitCollection, new PropertyChangedEventHandler( OnUnitCollectionPropertyChanged ), "UnitCollection", VarioSL.Entities.RelationClasses.StaticParameterValueRelations.UnitCollectionEntityUsingUnitCollectionIDStatic, true, ref _alreadyFetchedUnitCollection, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUnitCollectionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="parameterValueID">PK value for ParameterValue which data should be fetched into this ParameterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 parameterValueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ParameterValueFieldIndex.ParameterValueID].ForcedCurrentValueWrite(parameterValueID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateParameterValueDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ParameterValueEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ParameterValueRelations Relations
		{
			get	{ return new ParameterValueRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceClassCollection(), (IEntityRelation)GetRelationsForField("DeviceClass")[0], (int)VarioSL.Entities.EntityType.ParameterValueEntity, (int)VarioSL.Entities.EntityType.DeviceClassEntity, 0, null, null, null, "DeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Unit'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnit
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollection(), (IEntityRelation)GetRelationsForField("Unit")[0], (int)VarioSL.Entities.EntityType.ParameterValueEntity, (int)VarioSL.Entities.EntityType.UnitEntity, 0, null, null, null, "Unit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Parameter'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameter
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterCollection(), (IEntityRelation)GetRelationsForField("Parameter")[0], (int)VarioSL.Entities.EntityType.ParameterValueEntity, (int)VarioSL.Entities.EntityType.ParameterEntity, 0, null, null, null, "Parameter", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterArchive'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterArchive
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterArchiveCollection(), (IEntityRelation)GetRelationsForField("ParameterArchive")[0], (int)VarioSL.Entities.EntityType.ParameterValueEntity, (int)VarioSL.Entities.EntityType.ParameterArchiveEntity, 0, null, null, null, "ParameterArchive", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UnitCollection'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnitCollection
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollectionCollection(), (IEntityRelation)GetRelationsForField("UnitCollection")[0], (int)VarioSL.Entities.EntityType.ParameterValueEntity, (int)VarioSL.Entities.EntityType.UnitCollectionEntity, 0, null, null, null, "UnitCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DeviceClassID property of the Entity ParameterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERVALUE"."DEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DeviceClassID
		{
			get { return (System.Int64)GetValue((int)ParameterValueFieldIndex.DeviceClassID, true); }
			set	{ SetValue((int)ParameterValueFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The LastModified property of the Entity ParameterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERVALUE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ParameterValueFieldIndex.LastModified, true); }
			set	{ SetValue((int)ParameterValueFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ParameterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERVALUE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ParameterValueFieldIndex.LastUser, true); }
			set	{ SetValue((int)ParameterValueFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MountingPlateIndex property of the Entity ParameterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERVALUE"."MOUNTINGPLATEINDEX"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MountingPlateIndex
		{
			get { return (Nullable<System.Int64>)GetValue((int)ParameterValueFieldIndex.MountingPlateIndex, false); }
			set	{ SetValue((int)ParameterValueFieldIndex.MountingPlateIndex, value, true); }
		}

		/// <summary> The ParameterArchiveID property of the Entity ParameterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERVALUE"."PARAMETERARCHIVEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ParameterArchiveID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ParameterValueFieldIndex.ParameterArchiveID, false); }
			set	{ SetValue((int)ParameterValueFieldIndex.ParameterArchiveID, value, true); }
		}

		/// <summary> The ParameterID property of the Entity ParameterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERVALUE"."PARAMETERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ParameterID
		{
			get { return (System.Int64)GetValue((int)ParameterValueFieldIndex.ParameterID, true); }
			set	{ SetValue((int)ParameterValueFieldIndex.ParameterID, value, true); }
		}

		/// <summary> The ParameterValue property of the Entity ParameterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERVALUE"."PARAMETERVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ParameterValue
		{
			get { return (System.String)GetValue((int)ParameterValueFieldIndex.ParameterValue, true); }
			set	{ SetValue((int)ParameterValueFieldIndex.ParameterValue, value, true); }
		}

		/// <summary> The ParameterValueID property of the Entity ParameterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERVALUE"."PARAMETERVALUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ParameterValueID
		{
			get { return (System.Int64)GetValue((int)ParameterValueFieldIndex.ParameterValueID, true); }
			set	{ SetValue((int)ParameterValueFieldIndex.ParameterValueID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ParameterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERVALUE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ParameterValueFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ParameterValueFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The UnitCollectionID property of the Entity ParameterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERVALUE"."UNITCOLLECTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UnitCollectionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ParameterValueFieldIndex.UnitCollectionID, false); }
			set	{ SetValue((int)ParameterValueFieldIndex.UnitCollectionID, value, true); }
		}

		/// <summary> The UnitID property of the Entity ParameterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERVALUE"."UNITID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UnitID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ParameterValueFieldIndex.UnitID, false); }
			set	{ SetValue((int)ParameterValueFieldIndex.UnitID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'DeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceClassEntity DeviceClass
		{
			get	{ return GetSingleDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ParameterValues", "DeviceClass", _deviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceClass. When set to true, DeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceClass is accessed. You can always execute a forced fetch by calling GetSingleDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceClass
		{
			get	{ return _alwaysFetchDeviceClass; }
			set	{ _alwaysFetchDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceClass already has been fetched. Setting this property to false when DeviceClass has been fetched
		/// will set DeviceClass to null as well. Setting this property to true while DeviceClass hasn't been fetched disables lazy loading for DeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceClass
		{
			get { return _alreadyFetchedDeviceClass;}
			set 
			{
				if(_alreadyFetchedDeviceClass && !value)
				{
					this.DeviceClass = null;
				}
				_alreadyFetchedDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceClass is not found
		/// in the database. When set to true, DeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceClassReturnsNewIfNotFound
		{
			get	{ return _deviceClassReturnsNewIfNotFound; }
			set { _deviceClassReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UnitEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UnitEntity Unit
		{
			get	{ return GetSingleUnit(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUnit(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ParameterValues", "Unit", _unit, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Unit. When set to true, Unit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Unit is accessed. You can always execute a forced fetch by calling GetSingleUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnit
		{
			get	{ return _alwaysFetchUnit; }
			set	{ _alwaysFetchUnit = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Unit already has been fetched. Setting this property to false when Unit has been fetched
		/// will set Unit to null as well. Setting this property to true while Unit hasn't been fetched disables lazy loading for Unit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnit
		{
			get { return _alreadyFetchedUnit;}
			set 
			{
				if(_alreadyFetchedUnit && !value)
				{
					this.Unit = null;
				}
				_alreadyFetchedUnit = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Unit is not found
		/// in the database. When set to true, Unit will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UnitReturnsNewIfNotFound
		{
			get	{ return _unitReturnsNewIfNotFound; }
			set { _unitReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ParameterEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParameter()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ParameterEntity Parameter
		{
			get	{ return GetSingleParameter(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParameter(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ParameterValues", "Parameter", _parameter, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Parameter. When set to true, Parameter is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Parameter is accessed. You can always execute a forced fetch by calling GetSingleParameter(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameter
		{
			get	{ return _alwaysFetchParameter; }
			set	{ _alwaysFetchParameter = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Parameter already has been fetched. Setting this property to false when Parameter has been fetched
		/// will set Parameter to null as well. Setting this property to true while Parameter hasn't been fetched disables lazy loading for Parameter</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameter
		{
			get { return _alreadyFetchedParameter;}
			set 
			{
				if(_alreadyFetchedParameter && !value)
				{
					this.Parameter = null;
				}
				_alreadyFetchedParameter = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Parameter is not found
		/// in the database. When set to true, Parameter will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ParameterReturnsNewIfNotFound
		{
			get	{ return _parameterReturnsNewIfNotFound; }
			set { _parameterReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ParameterArchiveEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParameterArchive()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ParameterArchiveEntity ParameterArchive
		{
			get	{ return GetSingleParameterArchive(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParameterArchive(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ParameterValues", "ParameterArchive", _parameterArchive, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterArchive. When set to true, ParameterArchive is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterArchive is accessed. You can always execute a forced fetch by calling GetSingleParameterArchive(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterArchive
		{
			get	{ return _alwaysFetchParameterArchive; }
			set	{ _alwaysFetchParameterArchive = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterArchive already has been fetched. Setting this property to false when ParameterArchive has been fetched
		/// will set ParameterArchive to null as well. Setting this property to true while ParameterArchive hasn't been fetched disables lazy loading for ParameterArchive</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterArchive
		{
			get { return _alreadyFetchedParameterArchive;}
			set 
			{
				if(_alreadyFetchedParameterArchive && !value)
				{
					this.ParameterArchive = null;
				}
				_alreadyFetchedParameterArchive = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParameterArchive is not found
		/// in the database. When set to true, ParameterArchive will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ParameterArchiveReturnsNewIfNotFound
		{
			get	{ return _parameterArchiveReturnsNewIfNotFound; }
			set { _parameterArchiveReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UnitCollectionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUnitCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UnitCollectionEntity UnitCollection
		{
			get	{ return GetSingleUnitCollection(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUnitCollection(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ParameterValues", "UnitCollection", _unitCollection, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UnitCollection. When set to true, UnitCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UnitCollection is accessed. You can always execute a forced fetch by calling GetSingleUnitCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnitCollection
		{
			get	{ return _alwaysFetchUnitCollection; }
			set	{ _alwaysFetchUnitCollection = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UnitCollection already has been fetched. Setting this property to false when UnitCollection has been fetched
		/// will set UnitCollection to null as well. Setting this property to true while UnitCollection hasn't been fetched disables lazy loading for UnitCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnitCollection
		{
			get { return _alreadyFetchedUnitCollection;}
			set 
			{
				if(_alreadyFetchedUnitCollection && !value)
				{
					this.UnitCollection = null;
				}
				_alreadyFetchedUnitCollection = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UnitCollection is not found
		/// in the database. When set to true, UnitCollection will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UnitCollectionReturnsNewIfNotFound
		{
			get	{ return _unitCollectionReturnsNewIfNotFound; }
			set { _unitCollectionReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ParameterValueEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
