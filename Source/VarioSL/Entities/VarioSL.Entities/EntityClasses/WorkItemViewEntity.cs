﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'WorkItemView'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class WorkItemViewEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static WorkItemViewEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public WorkItemViewEntity() :base("WorkItemViewEntity")
		{
			InitClassEmpty(null);
		}
		


		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WorkItemViewEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}




				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new WorkItemViewRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		



		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Assigned", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Assignee", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Created", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Executed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalOrderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdentificationNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Memo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Source", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Title", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeDiscriminator", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WorkItemID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WorkItemSubjectID", fieldHashtable);
		}
		#endregion

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. </summary>
		/// <returns>true</returns>
		public override bool Refetch()
		{
			return true;
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateWorkItemViewDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new WorkItemViewEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static WorkItemViewRelations Relations
		{
			get	{ return new WorkItemViewRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Assigned property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."ASSIGNED"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Assigned
		{
			get { return (System.DateTime)GetValue((int)WorkItemViewFieldIndex.Assigned, true); }
			set	{ SetValue((int)WorkItemViewFieldIndex.Assigned, value, true); }
		}

		/// <summary> The Assignee property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."ASSIGNEE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Assignee
		{
			get { return (Nullable<System.Int64>)GetValue((int)WorkItemViewFieldIndex.Assignee, false); }
			set	{ SetValue((int)WorkItemViewFieldIndex.Assignee, value, true); }
		}

		/// <summary> The CardID property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."CARDID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)WorkItemViewFieldIndex.CardID, false); }
			set	{ SetValue((int)WorkItemViewFieldIndex.CardID, value, true); }
		}

		/// <summary> The ContractID property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."CONTRACTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractID
		{
			get { return (Nullable<System.Int64>)GetValue((int)WorkItemViewFieldIndex.ContractID, false); }
			set	{ SetValue((int)WorkItemViewFieldIndex.ContractID, value, true); }
		}

		/// <summary> The Created property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."CREATED"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Created
		{
			get { return (System.DateTime)GetValue((int)WorkItemViewFieldIndex.Created, true); }
			set	{ SetValue((int)WorkItemViewFieldIndex.Created, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."CREATEDBY"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CreatedBy
		{
			get { return (System.String)GetValue((int)WorkItemViewFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)WorkItemViewFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The Executed property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."EXECUTED"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Executed
		{
			get { return (System.DateTime)GetValue((int)WorkItemViewFieldIndex.Executed, true); }
			set	{ SetValue((int)WorkItemViewFieldIndex.Executed, value, true); }
		}

		/// <summary> The ExternalOrderNumber property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."EXTERNALORDERNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalOrderNumber
		{
			get { return (System.String)GetValue((int)WorkItemViewFieldIndex.ExternalOrderNumber, true); }
			set	{ SetValue((int)WorkItemViewFieldIndex.ExternalOrderNumber, value, true); }
		}

		/// <summary> The IdentificationNumber property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."IDENTIFICATIONNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String IdentificationNumber
		{
			get { return (System.String)GetValue((int)WorkItemViewFieldIndex.IdentificationNumber, true); }
			set	{ SetValue((int)WorkItemViewFieldIndex.IdentificationNumber, value, true); }
		}

		/// <summary> The LastUser property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."LASTUSER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)WorkItemViewFieldIndex.LastUser, true); }
			set	{ SetValue((int)WorkItemViewFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Memo property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."MEMO"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Memo
		{
			get { return (System.String)GetValue((int)WorkItemViewFieldIndex.Memo, true); }
			set	{ SetValue((int)WorkItemViewFieldIndex.Memo, value, true); }
		}

		/// <summary> The OrderID property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."ORDERID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OrderID
		{
			get { return (Nullable<System.Int64>)GetValue((int)WorkItemViewFieldIndex.OrderID, false); }
			set	{ SetValue((int)WorkItemViewFieldIndex.OrderID, value, true); }
		}

		/// <summary> The Source property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."SOURCE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Source
		{
			get { return (Nullable<System.Int32>)GetValue((int)WorkItemViewFieldIndex.Source, false); }
			set	{ SetValue((int)WorkItemViewFieldIndex.Source, value, true); }
		}

		/// <summary> The State property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."STATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> State
		{
			get { return (Nullable<System.Int32>)GetValue((int)WorkItemViewFieldIndex.State, false); }
			set	{ SetValue((int)WorkItemViewFieldIndex.State, value, true); }
		}

		/// <summary> The Title property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."TITLE"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Title
		{
			get { return (System.String)GetValue((int)WorkItemViewFieldIndex.Title, true); }
			set	{ SetValue((int)WorkItemViewFieldIndex.Title, value, true); }
		}

		/// <summary> The TypeDiscriminator property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."TYPEDISCRIMINATOR"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String TypeDiscriminator
		{
			get { return (System.String)GetValue((int)WorkItemViewFieldIndex.TypeDiscriminator, true); }
			set	{ SetValue((int)WorkItemViewFieldIndex.TypeDiscriminator, value, true); }
		}

		/// <summary> The WorkItemID property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."WORKITEMID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 WorkItemID
		{
			get { return (System.Int64)GetValue((int)WorkItemViewFieldIndex.WorkItemID, true); }
			set	{ SetValue((int)WorkItemViewFieldIndex.WorkItemID, value, true); }
		}

		/// <summary> The WorkItemSubjectID property of the Entity WorkItemView<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_WORKITEMVIEW"."WORKITEMSUBJECTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> WorkItemSubjectID
		{
			get { return (Nullable<System.Int64>)GetValue((int)WorkItemViewFieldIndex.WorkItemSubjectID, false); }
			set	{ SetValue((int)WorkItemViewFieldIndex.WorkItemSubjectID, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.WorkItemViewEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
