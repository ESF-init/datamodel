﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Ticket'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TicketEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ApportionmentResultCollection	_apportionmentResults;
		private bool	_alwaysFetchApportionmentResults, _alreadyFetchedApportionmentResults;
		private VarioSL.Entities.CollectionClasses.AttributeValueListCollection	_attributeValueList;
		private bool	_alwaysFetchAttributeValueList, _alreadyFetchedAttributeValueList;
		private VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection	_cardTicketToTicket;
		private bool	_alwaysFetchCardTicketToTicket, _alreadyFetchedCardTicketToTicket;
		private VarioSL.Entities.CollectionClasses.ChoiceCollection	_choices;
		private bool	_alwaysFetchChoices, _alreadyFetchedChoices;
		private VarioSL.Entities.CollectionClasses.ParameterTicketCollection	_parameterTicket;
		private bool	_alwaysFetchParameterTicket, _alreadyFetchedParameterTicket;
		private VarioSL.Entities.CollectionClasses.PrimalKeyCollection	_primalKeys;
		private bool	_alwaysFetchPrimalKeys, _alreadyFetchedPrimalKeys;
		private VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection	_ruleCappingToTicket;
		private bool	_alwaysFetchRuleCappingToTicket, _alreadyFetchedRuleCappingToTicket;
		private VarioSL.Entities.CollectionClasses.TicketDayTypeCollection	_ticketDayTypes;
		private bool	_alwaysFetchTicketDayTypes, _alreadyFetchedTicketDayTypes;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection	_ticketDeviceClasses;
		private bool	_alwaysFetchTicketDeviceClasses, _alreadyFetchedTicketDeviceClasses;
		private VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection	_ticketDevicePaymentMethods;
		private bool	_alwaysFetchTicketDevicePaymentMethods, _alreadyFetchedTicketDevicePaymentMethods;
		private VarioSL.Entities.CollectionClasses.TicketOrganizationCollection	_ticketOrganizations;
		private bool	_alwaysFetchTicketOrganizations, _alreadyFetchedTicketOrganizations;
		private VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection	_ticketOutputDevices;
		private bool	_alwaysFetchTicketOutputDevices, _alreadyFetchedTicketOutputDevices;
		private VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection	_ticketPaymentIntervals;
		private bool	_alwaysFetchTicketPaymentIntervals, _alreadyFetchedTicketPaymentIntervals;
		private VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection	_ticketPhysicalCardTypes;
		private bool	_alwaysFetchTicketPhysicalCardTypes, _alreadyFetchedTicketPhysicalCardTypes;
		private VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection	_ticketServicesPermitteds;
		private bool	_alwaysFetchTicketServicesPermitteds, _alreadyFetchedTicketServicesPermitteds;
		private VarioSL.Entities.CollectionClasses.TicketToGroupCollection	_ticketToGroups;
		private bool	_alwaysFetchTicketToGroups, _alreadyFetchedTicketToGroups;
		private VarioSL.Entities.CollectionClasses.TicketVendingClientCollection	_ticketVendingClient;
		private bool	_alwaysFetchTicketVendingClient, _alreadyFetchedTicketVendingClient;
		private VarioSL.Entities.CollectionClasses.VdvKeySetCollection	_vdvKeySet;
		private bool	_alwaysFetchVdvKeySet, _alreadyFetchedVdvKeySet;
		private VarioSL.Entities.CollectionClasses.VdvProductCollection	_vdvProduct;
		private bool	_alwaysFetchVdvProduct, _alreadyFetchedVdvProduct;
		private VarioSL.Entities.CollectionClasses.ProductCollection	_taxProducts;
		private bool	_alwaysFetchTaxProducts, _alreadyFetchedTaxProducts;
		private VarioSL.Entities.CollectionClasses.ProductCollection	_products;
		private bool	_alwaysFetchProducts, _alreadyFetchedProducts;
		private VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection	_settlementQuerySettingToTickets;
		private bool	_alwaysFetchSettlementQuerySettingToTickets, _alreadyFetchedSettlementQuerySettingToTickets;
		private VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection	_ticketSerialNumbers;
		private bool	_alwaysFetchTicketSerialNumbers, _alreadyFetchedTicketSerialNumbers;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_transactionJournalsForAppliedPassTicket;
		private bool	_alwaysFetchTransactionJournalsForAppliedPassTicket, _alreadyFetchedTransactionJournalsForAppliedPassTicket;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_transactionJournals;
		private bool	_alwaysFetchTransactionJournals, _alreadyFetchedTransactionJournals;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_transactionJournals_;
		private bool	_alwaysFetchTransactionJournals_, _alreadyFetchedTransactionJournals_;
		private VarioSL.Entities.CollectionClasses.VoucherCollection	_vouchers;
		private bool	_alwaysFetchVouchers, _alreadyFetchedVouchers;
		private VarioSL.Entities.CollectionClasses.AttributeValueCollection _attributevalueCollectionViaAttributeValueList;
		private bool	_alwaysFetchAttributevalueCollectionViaAttributeValueList, _alreadyFetchedAttributevalueCollectionViaAttributeValueList;
		private VarioSL.Entities.CollectionClasses.CardChipTypeCollection _cardChipTypeCollectionViaTicketPhysicalCardType;
		private bool	_alwaysFetchCardChipTypeCollectionViaTicketPhysicalCardType, _alreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType;
		private VarioSL.Entities.CollectionClasses.CardTicketCollection _cardAdditionsTicketIsUsedIn;
		private bool	_alwaysFetchCardAdditionsTicketIsUsedIn, _alreadyFetchedCardAdditionsTicketIsUsedIn;
		private VarioSL.Entities.CollectionClasses.ClientCollection _clientCollectionViaTicketVendingClient;
		private bool	_alwaysFetchClientCollectionViaTicketVendingClient, _alreadyFetchedClientCollectionViaTicketVendingClient;
		private VarioSL.Entities.CollectionClasses.LineCollection _lineCollectionViaTicketServicesPermitted;
		private bool	_alwaysFetchLineCollectionViaTicketServicesPermitted, _alreadyFetchedLineCollectionViaTicketServicesPermitted;
		private VarioSL.Entities.CollectionClasses.PaymentIntervalCollection _paymentIntervalCollectionViaTicketPaymentInterval;
		private bool	_alwaysFetchPaymentIntervalCollectionViaTicketPaymentInterval, _alreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval;
		private VarioSL.Entities.CollectionClasses.RuleCappingCollection _ruleCappingCollectionViaRuleCappingToTicket;
		private bool	_alwaysFetchRuleCappingCollectionViaRuleCappingToTicket, _alreadyFetchedRuleCappingCollectionViaRuleCappingToTicket;
		private VarioSL.Entities.CollectionClasses.TicketGroupCollection _ticketGroupCollectionViaTicketToGroup;
		private bool	_alwaysFetchTicketGroupCollectionViaTicketToGroup, _alreadyFetchedTicketGroupCollectionViaTicketToGroup;
		private VarioSL.Entities.CollectionClasses.OrganizationCollection _organizationCollectionViaTicketOrganization;
		private bool	_alwaysFetchOrganizationCollectionViaTicketOrganization, _alreadyFetchedOrganizationCollectionViaTicketOrganization;
		private VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection _settlementQuerySettings;
		private bool	_alwaysFetchSettlementQuerySettings, _alreadyFetchedSettlementQuerySettings;
		private BusinessRuleEntity _businessRuleInspection;
		private bool	_alwaysFetchBusinessRuleInspection, _alreadyFetchedBusinessRuleInspection, _businessRuleInspectionReturnsNewIfNotFound;
		private BusinessRuleEntity _businessRuleTestBoarding;
		private bool	_alwaysFetchBusinessRuleTestBoarding, _alreadyFetchedBusinessRuleTestBoarding, _businessRuleTestBoardingReturnsNewIfNotFound;
		private BusinessRuleEntity _businessRule;
		private bool	_alwaysFetchBusinessRule, _alreadyFetchedBusinessRule, _businessRuleReturnsNewIfNotFound;
		private BusinessRuleEntity _businessRuleTicketAssignment;
		private bool	_alwaysFetchBusinessRuleTicketAssignment, _alreadyFetchedBusinessRuleTicketAssignment, _businessRuleTicketAssignmentReturnsNewIfNotFound;
		private CalendarEntity _calendar;
		private bool	_alwaysFetchCalendar, _alreadyFetchedCalendar, _calendarReturnsNewIfNotFound;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private DeviceClassEntity _deviceClass;
		private bool	_alwaysFetchDeviceClass, _alreadyFetchedDeviceClass, _deviceClassReturnsNewIfNotFound;
		private EticketEntity _eticket;
		private bool	_alwaysFetchEticket, _alreadyFetchedEticket, _eticketReturnsNewIfNotFound;
		private FareEvasionCategoryEntity _fareEvasionCategory;
		private bool	_alwaysFetchFareEvasionCategory, _alreadyFetchedFareEvasionCategory, _fareEvasionCategoryReturnsNewIfNotFound;
		private FareMatrixEntity _fareMatrix;
		private bool	_alwaysFetchFareMatrix, _alreadyFetchedFareMatrix, _fareMatrixReturnsNewIfNotFound;
		private FareTableEntity _fareTable1;
		private bool	_alwaysFetchFareTable1, _alreadyFetchedFareTable1, _fareTable1ReturnsNewIfNotFound;
		private FareTableEntity _fareTable2;
		private bool	_alwaysFetchFareTable2, _alreadyFetchedFareTable2, _fareTable2ReturnsNewIfNotFound;
		private LayoutEntity _cancellationLayout;
		private bool	_alwaysFetchCancellationLayout, _alreadyFetchedCancellationLayout, _cancellationLayoutReturnsNewIfNotFound;
		private LayoutEntity _groupLayout;
		private bool	_alwaysFetchGroupLayout, _alreadyFetchedGroupLayout, _groupLayoutReturnsNewIfNotFound;
		private LayoutEntity _printLayout;
		private bool	_alwaysFetchPrintLayout, _alreadyFetchedPrintLayout, _printLayoutReturnsNewIfNotFound;
		private LayoutEntity _receiptLayout;
		private bool	_alwaysFetchReceiptLayout, _alreadyFetchedReceiptLayout, _receiptLayoutReturnsNewIfNotFound;
		private LineGroupEntity _lineGroup;
		private bool	_alwaysFetchLineGroup, _alreadyFetchedLineGroup, _lineGroupReturnsNewIfNotFound;
		private PageContentGroupEntity _pageContentGroup;
		private bool	_alwaysFetchPageContentGroup, _alreadyFetchedPageContentGroup, _pageContentGroupReturnsNewIfNotFound;
		private PriceTypeEntity _priceType;
		private bool	_alwaysFetchPriceType, _alreadyFetchedPriceType, _priceTypeReturnsNewIfNotFound;
		private RulePeriodEntity _rulePeriod;
		private bool	_alwaysFetchRulePeriod, _alreadyFetchedRulePeriod, _rulePeriodReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;
		private TemporalTypeEntity _temporalType;
		private bool	_alwaysFetchTemporalType, _alreadyFetchedTemporalType, _temporalTypeReturnsNewIfNotFound;
		private TicketCancellationTypeEntity _ticketCancellationType;
		private bool	_alwaysFetchTicketCancellationType, _alreadyFetchedTicketCancellationType, _ticketCancellationTypeReturnsNewIfNotFound;
		private TicketCategoryEntity _ticketCategory;
		private bool	_alwaysFetchTicketCategory, _alreadyFetchedTicketCategory, _ticketCategoryReturnsNewIfNotFound;
		private TicketSelectionModeEntity _ticketSelectionMode;
		private bool	_alwaysFetchTicketSelectionMode, _alreadyFetchedTicketSelectionMode, _ticketSelectionModeReturnsNewIfNotFound;
		private TicketTypeEntity _ticketType;
		private bool	_alwaysFetchTicketType, _alreadyFetchedTicketType, _ticketTypeReturnsNewIfNotFound;
		private UserKeyEntity _userKey1;
		private bool	_alwaysFetchUserKey1, _alreadyFetchedUserKey1, _userKey1ReturnsNewIfNotFound;
		private UserKeyEntity _userKey2;
		private bool	_alwaysFetchUserKey2, _alreadyFetchedUserKey2, _userKey2ReturnsNewIfNotFound;
		private CardTicketEntity _cardTicketAddition;
		private bool	_alwaysFetchCardTicketAddition, _alreadyFetchedCardTicketAddition, _cardTicketAdditionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BusinessRuleInspection</summary>
			public static readonly string BusinessRuleInspection = "BusinessRuleInspection";
			/// <summary>Member name BusinessRuleTestBoarding</summary>
			public static readonly string BusinessRuleTestBoarding = "BusinessRuleTestBoarding";
			/// <summary>Member name BusinessRule</summary>
			public static readonly string BusinessRule = "BusinessRule";
			/// <summary>Member name BusinessRuleTicketAssignment</summary>
			public static readonly string BusinessRuleTicketAssignment = "BusinessRuleTicketAssignment";
			/// <summary>Member name Calendar</summary>
			public static readonly string Calendar = "Calendar";
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name DeviceClass</summary>
			public static readonly string DeviceClass = "DeviceClass";
			/// <summary>Member name Eticket</summary>
			public static readonly string Eticket = "Eticket";
			/// <summary>Member name FareEvasionCategory</summary>
			public static readonly string FareEvasionCategory = "FareEvasionCategory";
			/// <summary>Member name FareMatrix</summary>
			public static readonly string FareMatrix = "FareMatrix";
			/// <summary>Member name FareTable1</summary>
			public static readonly string FareTable1 = "FareTable1";
			/// <summary>Member name FareTable2</summary>
			public static readonly string FareTable2 = "FareTable2";
			/// <summary>Member name CancellationLayout</summary>
			public static readonly string CancellationLayout = "CancellationLayout";
			/// <summary>Member name GroupLayout</summary>
			public static readonly string GroupLayout = "GroupLayout";
			/// <summary>Member name PrintLayout</summary>
			public static readonly string PrintLayout = "PrintLayout";
			/// <summary>Member name ReceiptLayout</summary>
			public static readonly string ReceiptLayout = "ReceiptLayout";
			/// <summary>Member name LineGroup</summary>
			public static readonly string LineGroup = "LineGroup";
			/// <summary>Member name PageContentGroup</summary>
			public static readonly string PageContentGroup = "PageContentGroup";
			/// <summary>Member name PriceType</summary>
			public static readonly string PriceType = "PriceType";
			/// <summary>Member name RulePeriod</summary>
			public static readonly string RulePeriod = "RulePeriod";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name TemporalType</summary>
			public static readonly string TemporalType = "TemporalType";
			/// <summary>Member name TicketCancellationType</summary>
			public static readonly string TicketCancellationType = "TicketCancellationType";
			/// <summary>Member name TicketCategory</summary>
			public static readonly string TicketCategory = "TicketCategory";
			/// <summary>Member name TicketSelectionMode</summary>
			public static readonly string TicketSelectionMode = "TicketSelectionMode";
			/// <summary>Member name TicketType</summary>
			public static readonly string TicketType = "TicketType";
			/// <summary>Member name UserKey1</summary>
			public static readonly string UserKey1 = "UserKey1";
			/// <summary>Member name UserKey2</summary>
			public static readonly string UserKey2 = "UserKey2";
			/// <summary>Member name ApportionmentResults</summary>
			public static readonly string ApportionmentResults = "ApportionmentResults";
			/// <summary>Member name AttributeValueList</summary>
			public static readonly string AttributeValueList = "AttributeValueList";
			/// <summary>Member name CardTicketToTicket</summary>
			public static readonly string CardTicketToTicket = "CardTicketToTicket";
			/// <summary>Member name Choices</summary>
			public static readonly string Choices = "Choices";
			/// <summary>Member name ParameterTicket</summary>
			public static readonly string ParameterTicket = "ParameterTicket";
			/// <summary>Member name PrimalKeys</summary>
			public static readonly string PrimalKeys = "PrimalKeys";
			/// <summary>Member name RuleCappingToTicket</summary>
			public static readonly string RuleCappingToTicket = "RuleCappingToTicket";
			/// <summary>Member name TicketDayTypes</summary>
			public static readonly string TicketDayTypes = "TicketDayTypes";
			/// <summary>Member name TicketDeviceClasses</summary>
			public static readonly string TicketDeviceClasses = "TicketDeviceClasses";
			/// <summary>Member name TicketDevicePaymentMethods</summary>
			public static readonly string TicketDevicePaymentMethods = "TicketDevicePaymentMethods";
			/// <summary>Member name TicketOrganizations</summary>
			public static readonly string TicketOrganizations = "TicketOrganizations";
			/// <summary>Member name TicketOutputDevices</summary>
			public static readonly string TicketOutputDevices = "TicketOutputDevices";
			/// <summary>Member name TicketPaymentIntervals</summary>
			public static readonly string TicketPaymentIntervals = "TicketPaymentIntervals";
			/// <summary>Member name TicketPhysicalCardTypes</summary>
			public static readonly string TicketPhysicalCardTypes = "TicketPhysicalCardTypes";
			/// <summary>Member name TicketServicesPermitteds</summary>
			public static readonly string TicketServicesPermitteds = "TicketServicesPermitteds";
			/// <summary>Member name TicketToGroups</summary>
			public static readonly string TicketToGroups = "TicketToGroups";
			/// <summary>Member name TicketVendingClient</summary>
			public static readonly string TicketVendingClient = "TicketVendingClient";
			/// <summary>Member name VdvKeySet</summary>
			public static readonly string VdvKeySet = "VdvKeySet";
			/// <summary>Member name VdvProduct</summary>
			public static readonly string VdvProduct = "VdvProduct";
			/// <summary>Member name TaxProducts</summary>
			public static readonly string TaxProducts = "TaxProducts";
			/// <summary>Member name Products</summary>
			public static readonly string Products = "Products";
			/// <summary>Member name SettlementQuerySettingToTickets</summary>
			public static readonly string SettlementQuerySettingToTickets = "SettlementQuerySettingToTickets";
			/// <summary>Member name TicketSerialNumbers</summary>
			public static readonly string TicketSerialNumbers = "TicketSerialNumbers";
			/// <summary>Member name TransactionJournalsForAppliedPassTicket</summary>
			public static readonly string TransactionJournalsForAppliedPassTicket = "TransactionJournalsForAppliedPassTicket";
			/// <summary>Member name TransactionJournals</summary>
			public static readonly string TransactionJournals = "TransactionJournals";
			/// <summary>Member name TransactionJournals_</summary>
			public static readonly string TransactionJournals_ = "TransactionJournals_";
			/// <summary>Member name Vouchers</summary>
			public static readonly string Vouchers = "Vouchers";
			/// <summary>Member name AttributevalueCollectionViaAttributeValueList</summary>
			public static readonly string AttributevalueCollectionViaAttributeValueList = "AttributevalueCollectionViaAttributeValueList";
			/// <summary>Member name CardChipTypeCollectionViaTicketPhysicalCardType</summary>
			public static readonly string CardChipTypeCollectionViaTicketPhysicalCardType = "CardChipTypeCollectionViaTicketPhysicalCardType";
			/// <summary>Member name CardAdditionsTicketIsUsedIn</summary>
			public static readonly string CardAdditionsTicketIsUsedIn = "CardAdditionsTicketIsUsedIn";
			/// <summary>Member name ClientCollectionViaTicketVendingClient</summary>
			public static readonly string ClientCollectionViaTicketVendingClient = "ClientCollectionViaTicketVendingClient";
			/// <summary>Member name LineCollectionViaTicketServicesPermitted</summary>
			public static readonly string LineCollectionViaTicketServicesPermitted = "LineCollectionViaTicketServicesPermitted";
			/// <summary>Member name PaymentIntervalCollectionViaTicketPaymentInterval</summary>
			public static readonly string PaymentIntervalCollectionViaTicketPaymentInterval = "PaymentIntervalCollectionViaTicketPaymentInterval";
			/// <summary>Member name RuleCappingCollectionViaRuleCappingToTicket</summary>
			public static readonly string RuleCappingCollectionViaRuleCappingToTicket = "RuleCappingCollectionViaRuleCappingToTicket";
			/// <summary>Member name TicketGroupCollectionViaTicketToGroup</summary>
			public static readonly string TicketGroupCollectionViaTicketToGroup = "TicketGroupCollectionViaTicketToGroup";
			/// <summary>Member name OrganizationCollectionViaTicketOrganization</summary>
			public static readonly string OrganizationCollectionViaTicketOrganization = "OrganizationCollectionViaTicketOrganization";
			/// <summary>Member name SettlementQuerySettings</summary>
			public static readonly string SettlementQuerySettings = "SettlementQuerySettings";
			/// <summary>Member name CardTicketAddition</summary>
			public static readonly string CardTicketAddition = "CardTicketAddition";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TicketEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TicketEntity() :base("TicketEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="ticketID">PK value for Ticket which data should be fetched into this Ticket object</param>
		public TicketEntity(System.Int64 ticketID):base("TicketEntity")
		{
			InitClassFetch(ticketID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="ticketID">PK value for Ticket which data should be fetched into this Ticket object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TicketEntity(System.Int64 ticketID, IPrefetchPath prefetchPathToUse):base("TicketEntity")
		{
			InitClassFetch(ticketID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="ticketID">PK value for Ticket which data should be fetched into this Ticket object</param>
		/// <param name="validator">The custom validator object for this TicketEntity</param>
		public TicketEntity(System.Int64 ticketID, IValidator validator):base("TicketEntity")
		{
			InitClassFetch(ticketID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TicketEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_apportionmentResults = (VarioSL.Entities.CollectionClasses.ApportionmentResultCollection)info.GetValue("_apportionmentResults", typeof(VarioSL.Entities.CollectionClasses.ApportionmentResultCollection));
			_alwaysFetchApportionmentResults = info.GetBoolean("_alwaysFetchApportionmentResults");
			_alreadyFetchedApportionmentResults = info.GetBoolean("_alreadyFetchedApportionmentResults");

			_attributeValueList = (VarioSL.Entities.CollectionClasses.AttributeValueListCollection)info.GetValue("_attributeValueList", typeof(VarioSL.Entities.CollectionClasses.AttributeValueListCollection));
			_alwaysFetchAttributeValueList = info.GetBoolean("_alwaysFetchAttributeValueList");
			_alreadyFetchedAttributeValueList = info.GetBoolean("_alreadyFetchedAttributeValueList");

			_cardTicketToTicket = (VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection)info.GetValue("_cardTicketToTicket", typeof(VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection));
			_alwaysFetchCardTicketToTicket = info.GetBoolean("_alwaysFetchCardTicketToTicket");
			_alreadyFetchedCardTicketToTicket = info.GetBoolean("_alreadyFetchedCardTicketToTicket");

			_choices = (VarioSL.Entities.CollectionClasses.ChoiceCollection)info.GetValue("_choices", typeof(VarioSL.Entities.CollectionClasses.ChoiceCollection));
			_alwaysFetchChoices = info.GetBoolean("_alwaysFetchChoices");
			_alreadyFetchedChoices = info.GetBoolean("_alreadyFetchedChoices");

			_parameterTicket = (VarioSL.Entities.CollectionClasses.ParameterTicketCollection)info.GetValue("_parameterTicket", typeof(VarioSL.Entities.CollectionClasses.ParameterTicketCollection));
			_alwaysFetchParameterTicket = info.GetBoolean("_alwaysFetchParameterTicket");
			_alreadyFetchedParameterTicket = info.GetBoolean("_alreadyFetchedParameterTicket");

			_primalKeys = (VarioSL.Entities.CollectionClasses.PrimalKeyCollection)info.GetValue("_primalKeys", typeof(VarioSL.Entities.CollectionClasses.PrimalKeyCollection));
			_alwaysFetchPrimalKeys = info.GetBoolean("_alwaysFetchPrimalKeys");
			_alreadyFetchedPrimalKeys = info.GetBoolean("_alreadyFetchedPrimalKeys");

			_ruleCappingToTicket = (VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection)info.GetValue("_ruleCappingToTicket", typeof(VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection));
			_alwaysFetchRuleCappingToTicket = info.GetBoolean("_alwaysFetchRuleCappingToTicket");
			_alreadyFetchedRuleCappingToTicket = info.GetBoolean("_alreadyFetchedRuleCappingToTicket");

			_ticketDayTypes = (VarioSL.Entities.CollectionClasses.TicketDayTypeCollection)info.GetValue("_ticketDayTypes", typeof(VarioSL.Entities.CollectionClasses.TicketDayTypeCollection));
			_alwaysFetchTicketDayTypes = info.GetBoolean("_alwaysFetchTicketDayTypes");
			_alreadyFetchedTicketDayTypes = info.GetBoolean("_alreadyFetchedTicketDayTypes");

			_ticketDeviceClasses = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceClasses", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceClasses = info.GetBoolean("_alwaysFetchTicketDeviceClasses");
			_alreadyFetchedTicketDeviceClasses = info.GetBoolean("_alreadyFetchedTicketDeviceClasses");

			_ticketDevicePaymentMethods = (VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection)info.GetValue("_ticketDevicePaymentMethods", typeof(VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection));
			_alwaysFetchTicketDevicePaymentMethods = info.GetBoolean("_alwaysFetchTicketDevicePaymentMethods");
			_alreadyFetchedTicketDevicePaymentMethods = info.GetBoolean("_alreadyFetchedTicketDevicePaymentMethods");

			_ticketOrganizations = (VarioSL.Entities.CollectionClasses.TicketOrganizationCollection)info.GetValue("_ticketOrganizations", typeof(VarioSL.Entities.CollectionClasses.TicketOrganizationCollection));
			_alwaysFetchTicketOrganizations = info.GetBoolean("_alwaysFetchTicketOrganizations");
			_alreadyFetchedTicketOrganizations = info.GetBoolean("_alreadyFetchedTicketOrganizations");

			_ticketOutputDevices = (VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection)info.GetValue("_ticketOutputDevices", typeof(VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection));
			_alwaysFetchTicketOutputDevices = info.GetBoolean("_alwaysFetchTicketOutputDevices");
			_alreadyFetchedTicketOutputDevices = info.GetBoolean("_alreadyFetchedTicketOutputDevices");

			_ticketPaymentIntervals = (VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection)info.GetValue("_ticketPaymentIntervals", typeof(VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection));
			_alwaysFetchTicketPaymentIntervals = info.GetBoolean("_alwaysFetchTicketPaymentIntervals");
			_alreadyFetchedTicketPaymentIntervals = info.GetBoolean("_alreadyFetchedTicketPaymentIntervals");

			_ticketPhysicalCardTypes = (VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection)info.GetValue("_ticketPhysicalCardTypes", typeof(VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection));
			_alwaysFetchTicketPhysicalCardTypes = info.GetBoolean("_alwaysFetchTicketPhysicalCardTypes");
			_alreadyFetchedTicketPhysicalCardTypes = info.GetBoolean("_alreadyFetchedTicketPhysicalCardTypes");

			_ticketServicesPermitteds = (VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection)info.GetValue("_ticketServicesPermitteds", typeof(VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection));
			_alwaysFetchTicketServicesPermitteds = info.GetBoolean("_alwaysFetchTicketServicesPermitteds");
			_alreadyFetchedTicketServicesPermitteds = info.GetBoolean("_alreadyFetchedTicketServicesPermitteds");

			_ticketToGroups = (VarioSL.Entities.CollectionClasses.TicketToGroupCollection)info.GetValue("_ticketToGroups", typeof(VarioSL.Entities.CollectionClasses.TicketToGroupCollection));
			_alwaysFetchTicketToGroups = info.GetBoolean("_alwaysFetchTicketToGroups");
			_alreadyFetchedTicketToGroups = info.GetBoolean("_alreadyFetchedTicketToGroups");

			_ticketVendingClient = (VarioSL.Entities.CollectionClasses.TicketVendingClientCollection)info.GetValue("_ticketVendingClient", typeof(VarioSL.Entities.CollectionClasses.TicketVendingClientCollection));
			_alwaysFetchTicketVendingClient = info.GetBoolean("_alwaysFetchTicketVendingClient");
			_alreadyFetchedTicketVendingClient = info.GetBoolean("_alreadyFetchedTicketVendingClient");

			_vdvKeySet = (VarioSL.Entities.CollectionClasses.VdvKeySetCollection)info.GetValue("_vdvKeySet", typeof(VarioSL.Entities.CollectionClasses.VdvKeySetCollection));
			_alwaysFetchVdvKeySet = info.GetBoolean("_alwaysFetchVdvKeySet");
			_alreadyFetchedVdvKeySet = info.GetBoolean("_alreadyFetchedVdvKeySet");

			_vdvProduct = (VarioSL.Entities.CollectionClasses.VdvProductCollection)info.GetValue("_vdvProduct", typeof(VarioSL.Entities.CollectionClasses.VdvProductCollection));
			_alwaysFetchVdvProduct = info.GetBoolean("_alwaysFetchVdvProduct");
			_alreadyFetchedVdvProduct = info.GetBoolean("_alreadyFetchedVdvProduct");

			_taxProducts = (VarioSL.Entities.CollectionClasses.ProductCollection)info.GetValue("_taxProducts", typeof(VarioSL.Entities.CollectionClasses.ProductCollection));
			_alwaysFetchTaxProducts = info.GetBoolean("_alwaysFetchTaxProducts");
			_alreadyFetchedTaxProducts = info.GetBoolean("_alreadyFetchedTaxProducts");

			_products = (VarioSL.Entities.CollectionClasses.ProductCollection)info.GetValue("_products", typeof(VarioSL.Entities.CollectionClasses.ProductCollection));
			_alwaysFetchProducts = info.GetBoolean("_alwaysFetchProducts");
			_alreadyFetchedProducts = info.GetBoolean("_alreadyFetchedProducts");

			_settlementQuerySettingToTickets = (VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection)info.GetValue("_settlementQuerySettingToTickets", typeof(VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection));
			_alwaysFetchSettlementQuerySettingToTickets = info.GetBoolean("_alwaysFetchSettlementQuerySettingToTickets");
			_alreadyFetchedSettlementQuerySettingToTickets = info.GetBoolean("_alreadyFetchedSettlementQuerySettingToTickets");

			_ticketSerialNumbers = (VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection)info.GetValue("_ticketSerialNumbers", typeof(VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection));
			_alwaysFetchTicketSerialNumbers = info.GetBoolean("_alwaysFetchTicketSerialNumbers");
			_alreadyFetchedTicketSerialNumbers = info.GetBoolean("_alreadyFetchedTicketSerialNumbers");

			_transactionJournalsForAppliedPassTicket = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_transactionJournalsForAppliedPassTicket", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTransactionJournalsForAppliedPassTicket = info.GetBoolean("_alwaysFetchTransactionJournalsForAppliedPassTicket");
			_alreadyFetchedTransactionJournalsForAppliedPassTicket = info.GetBoolean("_alreadyFetchedTransactionJournalsForAppliedPassTicket");

			_transactionJournals = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_transactionJournals", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTransactionJournals = info.GetBoolean("_alwaysFetchTransactionJournals");
			_alreadyFetchedTransactionJournals = info.GetBoolean("_alreadyFetchedTransactionJournals");

			_transactionJournals_ = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_transactionJournals_", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTransactionJournals_ = info.GetBoolean("_alwaysFetchTransactionJournals_");
			_alreadyFetchedTransactionJournals_ = info.GetBoolean("_alreadyFetchedTransactionJournals_");

			_vouchers = (VarioSL.Entities.CollectionClasses.VoucherCollection)info.GetValue("_vouchers", typeof(VarioSL.Entities.CollectionClasses.VoucherCollection));
			_alwaysFetchVouchers = info.GetBoolean("_alwaysFetchVouchers");
			_alreadyFetchedVouchers = info.GetBoolean("_alreadyFetchedVouchers");
			_attributevalueCollectionViaAttributeValueList = (VarioSL.Entities.CollectionClasses.AttributeValueCollection)info.GetValue("_attributevalueCollectionViaAttributeValueList", typeof(VarioSL.Entities.CollectionClasses.AttributeValueCollection));
			_alwaysFetchAttributevalueCollectionViaAttributeValueList = info.GetBoolean("_alwaysFetchAttributevalueCollectionViaAttributeValueList");
			_alreadyFetchedAttributevalueCollectionViaAttributeValueList = info.GetBoolean("_alreadyFetchedAttributevalueCollectionViaAttributeValueList");

			_cardChipTypeCollectionViaTicketPhysicalCardType = (VarioSL.Entities.CollectionClasses.CardChipTypeCollection)info.GetValue("_cardChipTypeCollectionViaTicketPhysicalCardType", typeof(VarioSL.Entities.CollectionClasses.CardChipTypeCollection));
			_alwaysFetchCardChipTypeCollectionViaTicketPhysicalCardType = info.GetBoolean("_alwaysFetchCardChipTypeCollectionViaTicketPhysicalCardType");
			_alreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType = info.GetBoolean("_alreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType");

			_cardAdditionsTicketIsUsedIn = (VarioSL.Entities.CollectionClasses.CardTicketCollection)info.GetValue("_cardAdditionsTicketIsUsedIn", typeof(VarioSL.Entities.CollectionClasses.CardTicketCollection));
			_alwaysFetchCardAdditionsTicketIsUsedIn = info.GetBoolean("_alwaysFetchCardAdditionsTicketIsUsedIn");
			_alreadyFetchedCardAdditionsTicketIsUsedIn = info.GetBoolean("_alreadyFetchedCardAdditionsTicketIsUsedIn");

			_clientCollectionViaTicketVendingClient = (VarioSL.Entities.CollectionClasses.ClientCollection)info.GetValue("_clientCollectionViaTicketVendingClient", typeof(VarioSL.Entities.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollectionViaTicketVendingClient = info.GetBoolean("_alwaysFetchClientCollectionViaTicketVendingClient");
			_alreadyFetchedClientCollectionViaTicketVendingClient = info.GetBoolean("_alreadyFetchedClientCollectionViaTicketVendingClient");

			_lineCollectionViaTicketServicesPermitted = (VarioSL.Entities.CollectionClasses.LineCollection)info.GetValue("_lineCollectionViaTicketServicesPermitted", typeof(VarioSL.Entities.CollectionClasses.LineCollection));
			_alwaysFetchLineCollectionViaTicketServicesPermitted = info.GetBoolean("_alwaysFetchLineCollectionViaTicketServicesPermitted");
			_alreadyFetchedLineCollectionViaTicketServicesPermitted = info.GetBoolean("_alreadyFetchedLineCollectionViaTicketServicesPermitted");

			_paymentIntervalCollectionViaTicketPaymentInterval = (VarioSL.Entities.CollectionClasses.PaymentIntervalCollection)info.GetValue("_paymentIntervalCollectionViaTicketPaymentInterval", typeof(VarioSL.Entities.CollectionClasses.PaymentIntervalCollection));
			_alwaysFetchPaymentIntervalCollectionViaTicketPaymentInterval = info.GetBoolean("_alwaysFetchPaymentIntervalCollectionViaTicketPaymentInterval");
			_alreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval = info.GetBoolean("_alreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval");

			_ruleCappingCollectionViaRuleCappingToTicket = (VarioSL.Entities.CollectionClasses.RuleCappingCollection)info.GetValue("_ruleCappingCollectionViaRuleCappingToTicket", typeof(VarioSL.Entities.CollectionClasses.RuleCappingCollection));
			_alwaysFetchRuleCappingCollectionViaRuleCappingToTicket = info.GetBoolean("_alwaysFetchRuleCappingCollectionViaRuleCappingToTicket");
			_alreadyFetchedRuleCappingCollectionViaRuleCappingToTicket = info.GetBoolean("_alreadyFetchedRuleCappingCollectionViaRuleCappingToTicket");

			_ticketGroupCollectionViaTicketToGroup = (VarioSL.Entities.CollectionClasses.TicketGroupCollection)info.GetValue("_ticketGroupCollectionViaTicketToGroup", typeof(VarioSL.Entities.CollectionClasses.TicketGroupCollection));
			_alwaysFetchTicketGroupCollectionViaTicketToGroup = info.GetBoolean("_alwaysFetchTicketGroupCollectionViaTicketToGroup");
			_alreadyFetchedTicketGroupCollectionViaTicketToGroup = info.GetBoolean("_alreadyFetchedTicketGroupCollectionViaTicketToGroup");

			_organizationCollectionViaTicketOrganization = (VarioSL.Entities.CollectionClasses.OrganizationCollection)info.GetValue("_organizationCollectionViaTicketOrganization", typeof(VarioSL.Entities.CollectionClasses.OrganizationCollection));
			_alwaysFetchOrganizationCollectionViaTicketOrganization = info.GetBoolean("_alwaysFetchOrganizationCollectionViaTicketOrganization");
			_alreadyFetchedOrganizationCollectionViaTicketOrganization = info.GetBoolean("_alreadyFetchedOrganizationCollectionViaTicketOrganization");

			_settlementQuerySettings = (VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection)info.GetValue("_settlementQuerySettings", typeof(VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection));
			_alwaysFetchSettlementQuerySettings = info.GetBoolean("_alwaysFetchSettlementQuerySettings");
			_alreadyFetchedSettlementQuerySettings = info.GetBoolean("_alreadyFetchedSettlementQuerySettings");
			_businessRuleInspection = (BusinessRuleEntity)info.GetValue("_businessRuleInspection", typeof(BusinessRuleEntity));
			if(_businessRuleInspection!=null)
			{
				_businessRuleInspection.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_businessRuleInspectionReturnsNewIfNotFound = info.GetBoolean("_businessRuleInspectionReturnsNewIfNotFound");
			_alwaysFetchBusinessRuleInspection = info.GetBoolean("_alwaysFetchBusinessRuleInspection");
			_alreadyFetchedBusinessRuleInspection = info.GetBoolean("_alreadyFetchedBusinessRuleInspection");

			_businessRuleTestBoarding = (BusinessRuleEntity)info.GetValue("_businessRuleTestBoarding", typeof(BusinessRuleEntity));
			if(_businessRuleTestBoarding!=null)
			{
				_businessRuleTestBoarding.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_businessRuleTestBoardingReturnsNewIfNotFound = info.GetBoolean("_businessRuleTestBoardingReturnsNewIfNotFound");
			_alwaysFetchBusinessRuleTestBoarding = info.GetBoolean("_alwaysFetchBusinessRuleTestBoarding");
			_alreadyFetchedBusinessRuleTestBoarding = info.GetBoolean("_alreadyFetchedBusinessRuleTestBoarding");

			_businessRule = (BusinessRuleEntity)info.GetValue("_businessRule", typeof(BusinessRuleEntity));
			if(_businessRule!=null)
			{
				_businessRule.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_businessRuleReturnsNewIfNotFound = info.GetBoolean("_businessRuleReturnsNewIfNotFound");
			_alwaysFetchBusinessRule = info.GetBoolean("_alwaysFetchBusinessRule");
			_alreadyFetchedBusinessRule = info.GetBoolean("_alreadyFetchedBusinessRule");

			_businessRuleTicketAssignment = (BusinessRuleEntity)info.GetValue("_businessRuleTicketAssignment", typeof(BusinessRuleEntity));
			if(_businessRuleTicketAssignment!=null)
			{
				_businessRuleTicketAssignment.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_businessRuleTicketAssignmentReturnsNewIfNotFound = info.GetBoolean("_businessRuleTicketAssignmentReturnsNewIfNotFound");
			_alwaysFetchBusinessRuleTicketAssignment = info.GetBoolean("_alwaysFetchBusinessRuleTicketAssignment");
			_alreadyFetchedBusinessRuleTicketAssignment = info.GetBoolean("_alreadyFetchedBusinessRuleTicketAssignment");

			_calendar = (CalendarEntity)info.GetValue("_calendar", typeof(CalendarEntity));
			if(_calendar!=null)
			{
				_calendar.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_calendarReturnsNewIfNotFound = info.GetBoolean("_calendarReturnsNewIfNotFound");
			_alwaysFetchCalendar = info.GetBoolean("_alwaysFetchCalendar");
			_alreadyFetchedCalendar = info.GetBoolean("_alreadyFetchedCalendar");

			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_deviceClass = (DeviceClassEntity)info.GetValue("_deviceClass", typeof(DeviceClassEntity));
			if(_deviceClass!=null)
			{
				_deviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceClassReturnsNewIfNotFound = info.GetBoolean("_deviceClassReturnsNewIfNotFound");
			_alwaysFetchDeviceClass = info.GetBoolean("_alwaysFetchDeviceClass");
			_alreadyFetchedDeviceClass = info.GetBoolean("_alreadyFetchedDeviceClass");

			_eticket = (EticketEntity)info.GetValue("_eticket", typeof(EticketEntity));
			if(_eticket!=null)
			{
				_eticket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_eticketReturnsNewIfNotFound = info.GetBoolean("_eticketReturnsNewIfNotFound");
			_alwaysFetchEticket = info.GetBoolean("_alwaysFetchEticket");
			_alreadyFetchedEticket = info.GetBoolean("_alreadyFetchedEticket");

			_fareEvasionCategory = (FareEvasionCategoryEntity)info.GetValue("_fareEvasionCategory", typeof(FareEvasionCategoryEntity));
			if(_fareEvasionCategory!=null)
			{
				_fareEvasionCategory.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareEvasionCategoryReturnsNewIfNotFound = info.GetBoolean("_fareEvasionCategoryReturnsNewIfNotFound");
			_alwaysFetchFareEvasionCategory = info.GetBoolean("_alwaysFetchFareEvasionCategory");
			_alreadyFetchedFareEvasionCategory = info.GetBoolean("_alreadyFetchedFareEvasionCategory");

			_fareMatrix = (FareMatrixEntity)info.GetValue("_fareMatrix", typeof(FareMatrixEntity));
			if(_fareMatrix!=null)
			{
				_fareMatrix.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareMatrixReturnsNewIfNotFound = info.GetBoolean("_fareMatrixReturnsNewIfNotFound");
			_alwaysFetchFareMatrix = info.GetBoolean("_alwaysFetchFareMatrix");
			_alreadyFetchedFareMatrix = info.GetBoolean("_alreadyFetchedFareMatrix");

			_fareTable1 = (FareTableEntity)info.GetValue("_fareTable1", typeof(FareTableEntity));
			if(_fareTable1!=null)
			{
				_fareTable1.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareTable1ReturnsNewIfNotFound = info.GetBoolean("_fareTable1ReturnsNewIfNotFound");
			_alwaysFetchFareTable1 = info.GetBoolean("_alwaysFetchFareTable1");
			_alreadyFetchedFareTable1 = info.GetBoolean("_alreadyFetchedFareTable1");

			_fareTable2 = (FareTableEntity)info.GetValue("_fareTable2", typeof(FareTableEntity));
			if(_fareTable2!=null)
			{
				_fareTable2.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareTable2ReturnsNewIfNotFound = info.GetBoolean("_fareTable2ReturnsNewIfNotFound");
			_alwaysFetchFareTable2 = info.GetBoolean("_alwaysFetchFareTable2");
			_alreadyFetchedFareTable2 = info.GetBoolean("_alreadyFetchedFareTable2");

			_cancellationLayout = (LayoutEntity)info.GetValue("_cancellationLayout", typeof(LayoutEntity));
			if(_cancellationLayout!=null)
			{
				_cancellationLayout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cancellationLayoutReturnsNewIfNotFound = info.GetBoolean("_cancellationLayoutReturnsNewIfNotFound");
			_alwaysFetchCancellationLayout = info.GetBoolean("_alwaysFetchCancellationLayout");
			_alreadyFetchedCancellationLayout = info.GetBoolean("_alreadyFetchedCancellationLayout");

			_groupLayout = (LayoutEntity)info.GetValue("_groupLayout", typeof(LayoutEntity));
			if(_groupLayout!=null)
			{
				_groupLayout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_groupLayoutReturnsNewIfNotFound = info.GetBoolean("_groupLayoutReturnsNewIfNotFound");
			_alwaysFetchGroupLayout = info.GetBoolean("_alwaysFetchGroupLayout");
			_alreadyFetchedGroupLayout = info.GetBoolean("_alreadyFetchedGroupLayout");

			_printLayout = (LayoutEntity)info.GetValue("_printLayout", typeof(LayoutEntity));
			if(_printLayout!=null)
			{
				_printLayout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_printLayoutReturnsNewIfNotFound = info.GetBoolean("_printLayoutReturnsNewIfNotFound");
			_alwaysFetchPrintLayout = info.GetBoolean("_alwaysFetchPrintLayout");
			_alreadyFetchedPrintLayout = info.GetBoolean("_alreadyFetchedPrintLayout");

			_receiptLayout = (LayoutEntity)info.GetValue("_receiptLayout", typeof(LayoutEntity));
			if(_receiptLayout!=null)
			{
				_receiptLayout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_receiptLayoutReturnsNewIfNotFound = info.GetBoolean("_receiptLayoutReturnsNewIfNotFound");
			_alwaysFetchReceiptLayout = info.GetBoolean("_alwaysFetchReceiptLayout");
			_alreadyFetchedReceiptLayout = info.GetBoolean("_alreadyFetchedReceiptLayout");

			_lineGroup = (LineGroupEntity)info.GetValue("_lineGroup", typeof(LineGroupEntity));
			if(_lineGroup!=null)
			{
				_lineGroup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_lineGroupReturnsNewIfNotFound = info.GetBoolean("_lineGroupReturnsNewIfNotFound");
			_alwaysFetchLineGroup = info.GetBoolean("_alwaysFetchLineGroup");
			_alreadyFetchedLineGroup = info.GetBoolean("_alreadyFetchedLineGroup");

			_pageContentGroup = (PageContentGroupEntity)info.GetValue("_pageContentGroup", typeof(PageContentGroupEntity));
			if(_pageContentGroup!=null)
			{
				_pageContentGroup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageContentGroupReturnsNewIfNotFound = info.GetBoolean("_pageContentGroupReturnsNewIfNotFound");
			_alwaysFetchPageContentGroup = info.GetBoolean("_alwaysFetchPageContentGroup");
			_alreadyFetchedPageContentGroup = info.GetBoolean("_alreadyFetchedPageContentGroup");

			_priceType = (PriceTypeEntity)info.GetValue("_priceType", typeof(PriceTypeEntity));
			if(_priceType!=null)
			{
				_priceType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_priceTypeReturnsNewIfNotFound = info.GetBoolean("_priceTypeReturnsNewIfNotFound");
			_alwaysFetchPriceType = info.GetBoolean("_alwaysFetchPriceType");
			_alreadyFetchedPriceType = info.GetBoolean("_alreadyFetchedPriceType");

			_rulePeriod = (RulePeriodEntity)info.GetValue("_rulePeriod", typeof(RulePeriodEntity));
			if(_rulePeriod!=null)
			{
				_rulePeriod.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_rulePeriodReturnsNewIfNotFound = info.GetBoolean("_rulePeriodReturnsNewIfNotFound");
			_alwaysFetchRulePeriod = info.GetBoolean("_alwaysFetchRulePeriod");
			_alreadyFetchedRulePeriod = info.GetBoolean("_alreadyFetchedRulePeriod");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");

			_temporalType = (TemporalTypeEntity)info.GetValue("_temporalType", typeof(TemporalTypeEntity));
			if(_temporalType!=null)
			{
				_temporalType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_temporalTypeReturnsNewIfNotFound = info.GetBoolean("_temporalTypeReturnsNewIfNotFound");
			_alwaysFetchTemporalType = info.GetBoolean("_alwaysFetchTemporalType");
			_alreadyFetchedTemporalType = info.GetBoolean("_alreadyFetchedTemporalType");

			_ticketCancellationType = (TicketCancellationTypeEntity)info.GetValue("_ticketCancellationType", typeof(TicketCancellationTypeEntity));
			if(_ticketCancellationType!=null)
			{
				_ticketCancellationType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketCancellationTypeReturnsNewIfNotFound = info.GetBoolean("_ticketCancellationTypeReturnsNewIfNotFound");
			_alwaysFetchTicketCancellationType = info.GetBoolean("_alwaysFetchTicketCancellationType");
			_alreadyFetchedTicketCancellationType = info.GetBoolean("_alreadyFetchedTicketCancellationType");

			_ticketCategory = (TicketCategoryEntity)info.GetValue("_ticketCategory", typeof(TicketCategoryEntity));
			if(_ticketCategory!=null)
			{
				_ticketCategory.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketCategoryReturnsNewIfNotFound = info.GetBoolean("_ticketCategoryReturnsNewIfNotFound");
			_alwaysFetchTicketCategory = info.GetBoolean("_alwaysFetchTicketCategory");
			_alreadyFetchedTicketCategory = info.GetBoolean("_alreadyFetchedTicketCategory");

			_ticketSelectionMode = (TicketSelectionModeEntity)info.GetValue("_ticketSelectionMode", typeof(TicketSelectionModeEntity));
			if(_ticketSelectionMode!=null)
			{
				_ticketSelectionMode.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketSelectionModeReturnsNewIfNotFound = info.GetBoolean("_ticketSelectionModeReturnsNewIfNotFound");
			_alwaysFetchTicketSelectionMode = info.GetBoolean("_alwaysFetchTicketSelectionMode");
			_alreadyFetchedTicketSelectionMode = info.GetBoolean("_alreadyFetchedTicketSelectionMode");

			_ticketType = (TicketTypeEntity)info.GetValue("_ticketType", typeof(TicketTypeEntity));
			if(_ticketType!=null)
			{
				_ticketType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketTypeReturnsNewIfNotFound = info.GetBoolean("_ticketTypeReturnsNewIfNotFound");
			_alwaysFetchTicketType = info.GetBoolean("_alwaysFetchTicketType");
			_alreadyFetchedTicketType = info.GetBoolean("_alreadyFetchedTicketType");

			_userKey1 = (UserKeyEntity)info.GetValue("_userKey1", typeof(UserKeyEntity));
			if(_userKey1!=null)
			{
				_userKey1.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userKey1ReturnsNewIfNotFound = info.GetBoolean("_userKey1ReturnsNewIfNotFound");
			_alwaysFetchUserKey1 = info.GetBoolean("_alwaysFetchUserKey1");
			_alreadyFetchedUserKey1 = info.GetBoolean("_alreadyFetchedUserKey1");

			_userKey2 = (UserKeyEntity)info.GetValue("_userKey2", typeof(UserKeyEntity));
			if(_userKey2!=null)
			{
				_userKey2.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userKey2ReturnsNewIfNotFound = info.GetBoolean("_userKey2ReturnsNewIfNotFound");
			_alwaysFetchUserKey2 = info.GetBoolean("_alwaysFetchUserKey2");
			_alreadyFetchedUserKey2 = info.GetBoolean("_alreadyFetchedUserKey2");
			_cardTicketAddition = (CardTicketEntity)info.GetValue("_cardTicketAddition", typeof(CardTicketEntity));
			if(_cardTicketAddition!=null)
			{
				_cardTicketAddition.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardTicketAdditionReturnsNewIfNotFound = info.GetBoolean("_cardTicketAdditionReturnsNewIfNotFound");
			_alwaysFetchCardTicketAddition = info.GetBoolean("_alwaysFetchCardTicketAddition");
			_alreadyFetchedCardTicketAddition = info.GetBoolean("_alreadyFetchedCardTicketAddition");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TicketFieldIndex)fieldIndex)
			{
				case TicketFieldIndex.BrType10ID:
					DesetupSyncBusinessRuleInspection(true, false);
					_alreadyFetchedBusinessRuleInspection = false;
					break;
				case TicketFieldIndex.BrType1ID:
					DesetupSyncBusinessRule(true, false);
					_alreadyFetchedBusinessRule = false;
					break;
				case TicketFieldIndex.BrType2ID:
					DesetupSyncBusinessRuleTestBoarding(true, false);
					_alreadyFetchedBusinessRuleTestBoarding = false;
					break;
				case TicketFieldIndex.BrType9ID:
					DesetupSyncBusinessRuleTicketAssignment(true, false);
					_alreadyFetchedBusinessRuleTicketAssignment = false;
					break;
				case TicketFieldIndex.CalendarID:
					DesetupSyncCalendar(true, false);
					_alreadyFetchedCalendar = false;
					break;
				case TicketFieldIndex.CancellationLayoutID:
					DesetupSyncCancellationLayout(true, false);
					_alreadyFetchedCancellationLayout = false;
					break;
				case TicketFieldIndex.CategoryID:
					DesetupSyncTicketCategory(true, false);
					_alreadyFetchedTicketCategory = false;
					break;
				case TicketFieldIndex.DeviceClassID:
					DesetupSyncDeviceClass(true, false);
					_alreadyFetchedDeviceClass = false;
					break;
				case TicketFieldIndex.EtickID:
					DesetupSyncEticket(true, false);
					_alreadyFetchedEticket = false;
					break;
				case TicketFieldIndex.FareEvasionCategoryID:
					DesetupSyncFareEvasionCategory(true, false);
					_alreadyFetchedFareEvasionCategory = false;
					break;
				case TicketFieldIndex.FareTable2ID:
					DesetupSyncFareTable2(true, false);
					_alreadyFetchedFareTable2 = false;
					break;
				case TicketFieldIndex.FareTableID:
					DesetupSyncFareTable1(true, false);
					_alreadyFetchedFareTable1 = false;
					break;
				case TicketFieldIndex.GroupLayoutID:
					DesetupSyncGroupLayout(true, false);
					_alreadyFetchedGroupLayout = false;
					break;
				case TicketFieldIndex.Key1:
					DesetupSyncUserKey1(true, false);
					_alreadyFetchedUserKey1 = false;
					break;
				case TicketFieldIndex.Key2:
					DesetupSyncUserKey2(true, false);
					_alreadyFetchedUserKey2 = false;
					break;
				case TicketFieldIndex.LineGroupID:
					DesetupSyncLineGroup(true, false);
					_alreadyFetchedLineGroup = false;
					break;
				case TicketFieldIndex.MatrixID:
					DesetupSyncFareMatrix(true, false);
					_alreadyFetchedFareMatrix = false;
					break;
				case TicketFieldIndex.OwnerClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case TicketFieldIndex.PageContentGroupID:
					DesetupSyncPageContentGroup(true, false);
					_alreadyFetchedPageContentGroup = false;
					break;
				case TicketFieldIndex.PriceTypeID:
					DesetupSyncPriceType(true, false);
					_alreadyFetchedPriceType = false;
					break;
				case TicketFieldIndex.PrintLayoutID:
					DesetupSyncPrintLayout(true, false);
					_alreadyFetchedPrintLayout = false;
					break;
				case TicketFieldIndex.ReceiptLayoutID:
					DesetupSyncReceiptLayout(true, false);
					_alreadyFetchedReceiptLayout = false;
					break;
				case TicketFieldIndex.RulePeriodID:
					DesetupSyncRulePeriod(true, false);
					_alreadyFetchedRulePeriod = false;
					break;
				case TicketFieldIndex.SelectionMode:
					DesetupSyncTicketSelectionMode(true, false);
					_alreadyFetchedTicketSelectionMode = false;
					break;
				case TicketFieldIndex.TarifID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				case TicketFieldIndex.TemporalTypeID:
					DesetupSyncTemporalType(true, false);
					_alreadyFetchedTemporalType = false;
					break;
				case TicketFieldIndex.TicketCancellationTypeID:
					DesetupSyncTicketCancellationType(true, false);
					_alreadyFetchedTicketCancellationType = false;
					break;
				case TicketFieldIndex.TicketTypeID:
					DesetupSyncTicketType(true, false);
					_alreadyFetchedTicketType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedApportionmentResults = (_apportionmentResults.Count > 0);
			_alreadyFetchedAttributeValueList = (_attributeValueList.Count > 0);
			_alreadyFetchedCardTicketToTicket = (_cardTicketToTicket.Count > 0);
			_alreadyFetchedChoices = (_choices.Count > 0);
			_alreadyFetchedParameterTicket = (_parameterTicket.Count > 0);
			_alreadyFetchedPrimalKeys = (_primalKeys.Count > 0);
			_alreadyFetchedRuleCappingToTicket = (_ruleCappingToTicket.Count > 0);
			_alreadyFetchedTicketDayTypes = (_ticketDayTypes.Count > 0);
			_alreadyFetchedTicketDeviceClasses = (_ticketDeviceClasses.Count > 0);
			_alreadyFetchedTicketDevicePaymentMethods = (_ticketDevicePaymentMethods.Count > 0);
			_alreadyFetchedTicketOrganizations = (_ticketOrganizations.Count > 0);
			_alreadyFetchedTicketOutputDevices = (_ticketOutputDevices.Count > 0);
			_alreadyFetchedTicketPaymentIntervals = (_ticketPaymentIntervals.Count > 0);
			_alreadyFetchedTicketPhysicalCardTypes = (_ticketPhysicalCardTypes.Count > 0);
			_alreadyFetchedTicketServicesPermitteds = (_ticketServicesPermitteds.Count > 0);
			_alreadyFetchedTicketToGroups = (_ticketToGroups.Count > 0);
			_alreadyFetchedTicketVendingClient = (_ticketVendingClient.Count > 0);
			_alreadyFetchedVdvKeySet = (_vdvKeySet.Count > 0);
			_alreadyFetchedVdvProduct = (_vdvProduct.Count > 0);
			_alreadyFetchedTaxProducts = (_taxProducts.Count > 0);
			_alreadyFetchedProducts = (_products.Count > 0);
			_alreadyFetchedSettlementQuerySettingToTickets = (_settlementQuerySettingToTickets.Count > 0);
			_alreadyFetchedTicketSerialNumbers = (_ticketSerialNumbers.Count > 0);
			_alreadyFetchedTransactionJournalsForAppliedPassTicket = (_transactionJournalsForAppliedPassTicket.Count > 0);
			_alreadyFetchedTransactionJournals = (_transactionJournals.Count > 0);
			_alreadyFetchedTransactionJournals_ = (_transactionJournals_.Count > 0);
			_alreadyFetchedVouchers = (_vouchers.Count > 0);
			_alreadyFetchedAttributevalueCollectionViaAttributeValueList = (_attributevalueCollectionViaAttributeValueList.Count > 0);
			_alreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType = (_cardChipTypeCollectionViaTicketPhysicalCardType.Count > 0);
			_alreadyFetchedCardAdditionsTicketIsUsedIn = (_cardAdditionsTicketIsUsedIn.Count > 0);
			_alreadyFetchedClientCollectionViaTicketVendingClient = (_clientCollectionViaTicketVendingClient.Count > 0);
			_alreadyFetchedLineCollectionViaTicketServicesPermitted = (_lineCollectionViaTicketServicesPermitted.Count > 0);
			_alreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval = (_paymentIntervalCollectionViaTicketPaymentInterval.Count > 0);
			_alreadyFetchedRuleCappingCollectionViaRuleCappingToTicket = (_ruleCappingCollectionViaRuleCappingToTicket.Count > 0);
			_alreadyFetchedTicketGroupCollectionViaTicketToGroup = (_ticketGroupCollectionViaTicketToGroup.Count > 0);
			_alreadyFetchedOrganizationCollectionViaTicketOrganization = (_organizationCollectionViaTicketOrganization.Count > 0);
			_alreadyFetchedSettlementQuerySettings = (_settlementQuerySettings.Count > 0);
			_alreadyFetchedBusinessRuleInspection = (_businessRuleInspection != null);
			_alreadyFetchedBusinessRuleTestBoarding = (_businessRuleTestBoarding != null);
			_alreadyFetchedBusinessRule = (_businessRule != null);
			_alreadyFetchedBusinessRuleTicketAssignment = (_businessRuleTicketAssignment != null);
			_alreadyFetchedCalendar = (_calendar != null);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedDeviceClass = (_deviceClass != null);
			_alreadyFetchedEticket = (_eticket != null);
			_alreadyFetchedFareEvasionCategory = (_fareEvasionCategory != null);
			_alreadyFetchedFareMatrix = (_fareMatrix != null);
			_alreadyFetchedFareTable1 = (_fareTable1 != null);
			_alreadyFetchedFareTable2 = (_fareTable2 != null);
			_alreadyFetchedCancellationLayout = (_cancellationLayout != null);
			_alreadyFetchedGroupLayout = (_groupLayout != null);
			_alreadyFetchedPrintLayout = (_printLayout != null);
			_alreadyFetchedReceiptLayout = (_receiptLayout != null);
			_alreadyFetchedLineGroup = (_lineGroup != null);
			_alreadyFetchedPageContentGroup = (_pageContentGroup != null);
			_alreadyFetchedPriceType = (_priceType != null);
			_alreadyFetchedRulePeriod = (_rulePeriod != null);
			_alreadyFetchedTariff = (_tariff != null);
			_alreadyFetchedTemporalType = (_temporalType != null);
			_alreadyFetchedTicketCancellationType = (_ticketCancellationType != null);
			_alreadyFetchedTicketCategory = (_ticketCategory != null);
			_alreadyFetchedTicketSelectionMode = (_ticketSelectionMode != null);
			_alreadyFetchedTicketType = (_ticketType != null);
			_alreadyFetchedUserKey1 = (_userKey1 != null);
			_alreadyFetchedUserKey2 = (_userKey2 != null);
			_alreadyFetchedCardTicketAddition = (_cardTicketAddition != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BusinessRuleInspection":
					toReturn.Add(Relations.BusinessRuleEntityUsingBrType10ID);
					break;
				case "BusinessRuleTestBoarding":
					toReturn.Add(Relations.BusinessRuleEntityUsingBrType2ID);
					break;
				case "BusinessRule":
					toReturn.Add(Relations.BusinessRuleEntityUsingBrType1ID);
					break;
				case "BusinessRuleTicketAssignment":
					toReturn.Add(Relations.BusinessRuleEntityUsingBrType9ID);
					break;
				case "Calendar":
					toReturn.Add(Relations.CalendarEntityUsingCalendarID);
					break;
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingOwnerClientID);
					break;
				case "DeviceClass":
					toReturn.Add(Relations.DeviceClassEntityUsingDeviceClassID);
					break;
				case "Eticket":
					toReturn.Add(Relations.EticketEntityUsingEtickID);
					break;
				case "FareEvasionCategory":
					toReturn.Add(Relations.FareEvasionCategoryEntityUsingFareEvasionCategoryID);
					break;
				case "FareMatrix":
					toReturn.Add(Relations.FareMatrixEntityUsingMatrixID);
					break;
				case "FareTable1":
					toReturn.Add(Relations.FareTableEntityUsingFareTableID);
					break;
				case "FareTable2":
					toReturn.Add(Relations.FareTableEntityUsingFareTable2ID);
					break;
				case "CancellationLayout":
					toReturn.Add(Relations.LayoutEntityUsingCancellationLayoutID);
					break;
				case "GroupLayout":
					toReturn.Add(Relations.LayoutEntityUsingGroupLayoutID);
					break;
				case "PrintLayout":
					toReturn.Add(Relations.LayoutEntityUsingPrintLayoutID);
					break;
				case "ReceiptLayout":
					toReturn.Add(Relations.LayoutEntityUsingReceiptLayoutID);
					break;
				case "LineGroup":
					toReturn.Add(Relations.LineGroupEntityUsingLineGroupID);
					break;
				case "PageContentGroup":
					toReturn.Add(Relations.PageContentGroupEntityUsingPageContentGroupID);
					break;
				case "PriceType":
					toReturn.Add(Relations.PriceTypeEntityUsingPriceTypeID);
					break;
				case "RulePeriod":
					toReturn.Add(Relations.RulePeriodEntityUsingRulePeriodID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTarifID);
					break;
				case "TemporalType":
					toReturn.Add(Relations.TemporalTypeEntityUsingTemporalTypeID);
					break;
				case "TicketCancellationType":
					toReturn.Add(Relations.TicketCancellationTypeEntityUsingTicketCancellationTypeID);
					break;
				case "TicketCategory":
					toReturn.Add(Relations.TicketCategoryEntityUsingCategoryID);
					break;
				case "TicketSelectionMode":
					toReturn.Add(Relations.TicketSelectionModeEntityUsingSelectionMode);
					break;
				case "TicketType":
					toReturn.Add(Relations.TicketTypeEntityUsingTicketTypeID);
					break;
				case "UserKey1":
					toReturn.Add(Relations.UserKeyEntityUsingKey1);
					break;
				case "UserKey2":
					toReturn.Add(Relations.UserKeyEntityUsingKey2);
					break;
				case "ApportionmentResults":
					toReturn.Add(Relations.ApportionmentResultEntityUsingTicketID);
					break;
				case "AttributeValueList":
					toReturn.Add(Relations.AttributeValueListEntityUsingTicketID);
					break;
				case "CardTicketToTicket":
					toReturn.Add(Relations.CardTicketToTicketEntityUsingTicketID);
					break;
				case "Choices":
					toReturn.Add(Relations.ChoiceEntityUsingTicketID);
					break;
				case "ParameterTicket":
					toReturn.Add(Relations.ParameterTicketEntityUsingTicketID);
					break;
				case "PrimalKeys":
					toReturn.Add(Relations.PrimalKeyEntityUsingTicketID);
					break;
				case "RuleCappingToTicket":
					toReturn.Add(Relations.RuleCappingToTicketEntityUsingTicketID);
					break;
				case "TicketDayTypes":
					toReturn.Add(Relations.TicketDayTypeEntityUsingTicketID);
					break;
				case "TicketDeviceClasses":
					toReturn.Add(Relations.TicketDeviceClassEntityUsingTicketID);
					break;
				case "TicketDevicePaymentMethods":
					toReturn.Add(Relations.TicketDevicePaymentMethodEntityUsingTicketID);
					break;
				case "TicketOrganizations":
					toReturn.Add(Relations.TicketOrganizationEntityUsingTicketID);
					break;
				case "TicketOutputDevices":
					toReturn.Add(Relations.TicketOutputdeviceEntityUsingTicketID);
					break;
				case "TicketPaymentIntervals":
					toReturn.Add(Relations.TicketPaymentIntervalEntityUsingTicketID);
					break;
				case "TicketPhysicalCardTypes":
					toReturn.Add(Relations.TicketPhysicalCardTypeEntityUsingTicketID);
					break;
				case "TicketServicesPermitteds":
					toReturn.Add(Relations.TicketServicesPermittedEntityUsingTicketID);
					break;
				case "TicketToGroups":
					toReturn.Add(Relations.TicketToGroupEntityUsingTicketID);
					break;
				case "TicketVendingClient":
					toReturn.Add(Relations.TicketVendingClientEntityUsingTicketid);
					break;
				case "VdvKeySet":
					toReturn.Add(Relations.VdvKeySetEntityUsingTicketID);
					break;
				case "VdvProduct":
					toReturn.Add(Relations.VdvProductEntityUsingTicketID);
					break;
				case "TaxProducts":
					toReturn.Add(Relations.ProductEntityUsingTaxTicketID);
					break;
				case "Products":
					toReturn.Add(Relations.ProductEntityUsingTicketID);
					break;
				case "SettlementQuerySettingToTickets":
					toReturn.Add(Relations.SettlementQuerySettingToTicketEntityUsingTicketID);
					break;
				case "TicketSerialNumbers":
					toReturn.Add(Relations.TicketSerialNumberEntityUsingTicketID);
					break;
				case "TransactionJournalsForAppliedPassTicket":
					toReturn.Add(Relations.TransactionJournalEntityUsingAppliedPassTicketID);
					break;
				case "TransactionJournals":
					toReturn.Add(Relations.TransactionJournalEntityUsingTicketID);
					break;
				case "TransactionJournals_":
					toReturn.Add(Relations.TransactionJournalEntityUsingTaxTicketID);
					break;
				case "Vouchers":
					toReturn.Add(Relations.VoucherEntityUsingTicketID);
					break;
				case "AttributevalueCollectionViaAttributeValueList":
					toReturn.Add(Relations.AttributeValueListEntityUsingTicketID, "TicketEntity__", "AttributeValueList_", JoinHint.None);
					toReturn.Add(AttributeValueListEntity.Relations.AttributeValueEntityUsingAttributeValueID, "AttributeValueList_", string.Empty, JoinHint.None);
					break;
				case "CardChipTypeCollectionViaTicketPhysicalCardType":
					toReturn.Add(Relations.TicketPhysicalCardTypeEntityUsingTicketID, "TicketEntity__", "TicketPhysicalCardType_", JoinHint.None);
					toReturn.Add(TicketPhysicalCardTypeEntity.Relations.CardChipTypeEntityUsingPhysicalCardTypeID, "TicketPhysicalCardType_", string.Empty, JoinHint.None);
					break;
				case "CardAdditionsTicketIsUsedIn":
					toReturn.Add(Relations.CardTicketToTicketEntityUsingTicketID, "TicketEntity__", "CardTicketToTicket_", JoinHint.None);
					toReturn.Add(CardTicketToTicketEntity.Relations.CardTicketEntityUsingCardTicketID, "CardTicketToTicket_", string.Empty, JoinHint.None);
					break;
				case "ClientCollectionViaTicketVendingClient":
					toReturn.Add(Relations.TicketVendingClientEntityUsingTicketid, "TicketEntity__", "TicketVendingClient_", JoinHint.None);
					toReturn.Add(TicketVendingClientEntity.Relations.ClientEntityUsingClientid, "TicketVendingClient_", string.Empty, JoinHint.None);
					break;
				case "LineCollectionViaTicketServicesPermitted":
					toReturn.Add(Relations.TicketServicesPermittedEntityUsingTicketID, "TicketEntity__", "TicketServicesPermitted_", JoinHint.None);
					toReturn.Add(TicketServicesPermittedEntity.Relations.LineEntityUsingLineID, "TicketServicesPermitted_", string.Empty, JoinHint.None);
					break;
				case "PaymentIntervalCollectionViaTicketPaymentInterval":
					toReturn.Add(Relations.TicketPaymentIntervalEntityUsingTicketID, "TicketEntity__", "TicketPaymentInterval_", JoinHint.None);
					toReturn.Add(TicketPaymentIntervalEntity.Relations.PaymentIntervalEntityUsingPaymentIntervalID, "TicketPaymentInterval_", string.Empty, JoinHint.None);
					break;
				case "RuleCappingCollectionViaRuleCappingToTicket":
					toReturn.Add(Relations.RuleCappingToTicketEntityUsingTicketID, "TicketEntity__", "RuleCappingToTicket_", JoinHint.None);
					toReturn.Add(RuleCappingToTicketEntity.Relations.RuleCappingEntityUsingRuleCappingID, "RuleCappingToTicket_", string.Empty, JoinHint.None);
					break;
				case "TicketGroupCollectionViaTicketToGroup":
					toReturn.Add(Relations.TicketToGroupEntityUsingTicketID, "TicketEntity__", "TicketToGroup_", JoinHint.None);
					toReturn.Add(TicketToGroupEntity.Relations.TicketGroupEntityUsingTicketGroupID, "TicketToGroup_", string.Empty, JoinHint.None);
					break;
				case "OrganizationCollectionViaTicketOrganization":
					toReturn.Add(Relations.TicketOrganizationEntityUsingTicketID, "TicketEntity__", "TicketOrganization_", JoinHint.None);
					toReturn.Add(TicketOrganizationEntity.Relations.OrganizationEntityUsingOrganizationID, "TicketOrganization_", string.Empty, JoinHint.None);
					break;
				case "SettlementQuerySettings":
					toReturn.Add(Relations.SettlementQuerySettingToTicketEntityUsingTicketID, "TicketEntity__", "SettlementQuerySettingToTicket_", JoinHint.None);
					toReturn.Add(SettlementQuerySettingToTicketEntity.Relations.SettlementQuerySettingEntityUsingSettlementQuerySettingID, "SettlementQuerySettingToTicket_", string.Empty, JoinHint.None);
					break;
				case "CardTicketAddition":
					toReturn.Add(Relations.CardTicketEntityUsingTicketID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_apportionmentResults", (!this.MarkedForDeletion?_apportionmentResults:null));
			info.AddValue("_alwaysFetchApportionmentResults", _alwaysFetchApportionmentResults);
			info.AddValue("_alreadyFetchedApportionmentResults", _alreadyFetchedApportionmentResults);
			info.AddValue("_attributeValueList", (!this.MarkedForDeletion?_attributeValueList:null));
			info.AddValue("_alwaysFetchAttributeValueList", _alwaysFetchAttributeValueList);
			info.AddValue("_alreadyFetchedAttributeValueList", _alreadyFetchedAttributeValueList);
			info.AddValue("_cardTicketToTicket", (!this.MarkedForDeletion?_cardTicketToTicket:null));
			info.AddValue("_alwaysFetchCardTicketToTicket", _alwaysFetchCardTicketToTicket);
			info.AddValue("_alreadyFetchedCardTicketToTicket", _alreadyFetchedCardTicketToTicket);
			info.AddValue("_choices", (!this.MarkedForDeletion?_choices:null));
			info.AddValue("_alwaysFetchChoices", _alwaysFetchChoices);
			info.AddValue("_alreadyFetchedChoices", _alreadyFetchedChoices);
			info.AddValue("_parameterTicket", (!this.MarkedForDeletion?_parameterTicket:null));
			info.AddValue("_alwaysFetchParameterTicket", _alwaysFetchParameterTicket);
			info.AddValue("_alreadyFetchedParameterTicket", _alreadyFetchedParameterTicket);
			info.AddValue("_primalKeys", (!this.MarkedForDeletion?_primalKeys:null));
			info.AddValue("_alwaysFetchPrimalKeys", _alwaysFetchPrimalKeys);
			info.AddValue("_alreadyFetchedPrimalKeys", _alreadyFetchedPrimalKeys);
			info.AddValue("_ruleCappingToTicket", (!this.MarkedForDeletion?_ruleCappingToTicket:null));
			info.AddValue("_alwaysFetchRuleCappingToTicket", _alwaysFetchRuleCappingToTicket);
			info.AddValue("_alreadyFetchedRuleCappingToTicket", _alreadyFetchedRuleCappingToTicket);
			info.AddValue("_ticketDayTypes", (!this.MarkedForDeletion?_ticketDayTypes:null));
			info.AddValue("_alwaysFetchTicketDayTypes", _alwaysFetchTicketDayTypes);
			info.AddValue("_alreadyFetchedTicketDayTypes", _alreadyFetchedTicketDayTypes);
			info.AddValue("_ticketDeviceClasses", (!this.MarkedForDeletion?_ticketDeviceClasses:null));
			info.AddValue("_alwaysFetchTicketDeviceClasses", _alwaysFetchTicketDeviceClasses);
			info.AddValue("_alreadyFetchedTicketDeviceClasses", _alreadyFetchedTicketDeviceClasses);
			info.AddValue("_ticketDevicePaymentMethods", (!this.MarkedForDeletion?_ticketDevicePaymentMethods:null));
			info.AddValue("_alwaysFetchTicketDevicePaymentMethods", _alwaysFetchTicketDevicePaymentMethods);
			info.AddValue("_alreadyFetchedTicketDevicePaymentMethods", _alreadyFetchedTicketDevicePaymentMethods);
			info.AddValue("_ticketOrganizations", (!this.MarkedForDeletion?_ticketOrganizations:null));
			info.AddValue("_alwaysFetchTicketOrganizations", _alwaysFetchTicketOrganizations);
			info.AddValue("_alreadyFetchedTicketOrganizations", _alreadyFetchedTicketOrganizations);
			info.AddValue("_ticketOutputDevices", (!this.MarkedForDeletion?_ticketOutputDevices:null));
			info.AddValue("_alwaysFetchTicketOutputDevices", _alwaysFetchTicketOutputDevices);
			info.AddValue("_alreadyFetchedTicketOutputDevices", _alreadyFetchedTicketOutputDevices);
			info.AddValue("_ticketPaymentIntervals", (!this.MarkedForDeletion?_ticketPaymentIntervals:null));
			info.AddValue("_alwaysFetchTicketPaymentIntervals", _alwaysFetchTicketPaymentIntervals);
			info.AddValue("_alreadyFetchedTicketPaymentIntervals", _alreadyFetchedTicketPaymentIntervals);
			info.AddValue("_ticketPhysicalCardTypes", (!this.MarkedForDeletion?_ticketPhysicalCardTypes:null));
			info.AddValue("_alwaysFetchTicketPhysicalCardTypes", _alwaysFetchTicketPhysicalCardTypes);
			info.AddValue("_alreadyFetchedTicketPhysicalCardTypes", _alreadyFetchedTicketPhysicalCardTypes);
			info.AddValue("_ticketServicesPermitteds", (!this.MarkedForDeletion?_ticketServicesPermitteds:null));
			info.AddValue("_alwaysFetchTicketServicesPermitteds", _alwaysFetchTicketServicesPermitteds);
			info.AddValue("_alreadyFetchedTicketServicesPermitteds", _alreadyFetchedTicketServicesPermitteds);
			info.AddValue("_ticketToGroups", (!this.MarkedForDeletion?_ticketToGroups:null));
			info.AddValue("_alwaysFetchTicketToGroups", _alwaysFetchTicketToGroups);
			info.AddValue("_alreadyFetchedTicketToGroups", _alreadyFetchedTicketToGroups);
			info.AddValue("_ticketVendingClient", (!this.MarkedForDeletion?_ticketVendingClient:null));
			info.AddValue("_alwaysFetchTicketVendingClient", _alwaysFetchTicketVendingClient);
			info.AddValue("_alreadyFetchedTicketVendingClient", _alreadyFetchedTicketVendingClient);
			info.AddValue("_vdvKeySet", (!this.MarkedForDeletion?_vdvKeySet:null));
			info.AddValue("_alwaysFetchVdvKeySet", _alwaysFetchVdvKeySet);
			info.AddValue("_alreadyFetchedVdvKeySet", _alreadyFetchedVdvKeySet);
			info.AddValue("_vdvProduct", (!this.MarkedForDeletion?_vdvProduct:null));
			info.AddValue("_alwaysFetchVdvProduct", _alwaysFetchVdvProduct);
			info.AddValue("_alreadyFetchedVdvProduct", _alreadyFetchedVdvProduct);
			info.AddValue("_taxProducts", (!this.MarkedForDeletion?_taxProducts:null));
			info.AddValue("_alwaysFetchTaxProducts", _alwaysFetchTaxProducts);
			info.AddValue("_alreadyFetchedTaxProducts", _alreadyFetchedTaxProducts);
			info.AddValue("_products", (!this.MarkedForDeletion?_products:null));
			info.AddValue("_alwaysFetchProducts", _alwaysFetchProducts);
			info.AddValue("_alreadyFetchedProducts", _alreadyFetchedProducts);
			info.AddValue("_settlementQuerySettingToTickets", (!this.MarkedForDeletion?_settlementQuerySettingToTickets:null));
			info.AddValue("_alwaysFetchSettlementQuerySettingToTickets", _alwaysFetchSettlementQuerySettingToTickets);
			info.AddValue("_alreadyFetchedSettlementQuerySettingToTickets", _alreadyFetchedSettlementQuerySettingToTickets);
			info.AddValue("_ticketSerialNumbers", (!this.MarkedForDeletion?_ticketSerialNumbers:null));
			info.AddValue("_alwaysFetchTicketSerialNumbers", _alwaysFetchTicketSerialNumbers);
			info.AddValue("_alreadyFetchedTicketSerialNumbers", _alreadyFetchedTicketSerialNumbers);
			info.AddValue("_transactionJournalsForAppliedPassTicket", (!this.MarkedForDeletion?_transactionJournalsForAppliedPassTicket:null));
			info.AddValue("_alwaysFetchTransactionJournalsForAppliedPassTicket", _alwaysFetchTransactionJournalsForAppliedPassTicket);
			info.AddValue("_alreadyFetchedTransactionJournalsForAppliedPassTicket", _alreadyFetchedTransactionJournalsForAppliedPassTicket);
			info.AddValue("_transactionJournals", (!this.MarkedForDeletion?_transactionJournals:null));
			info.AddValue("_alwaysFetchTransactionJournals", _alwaysFetchTransactionJournals);
			info.AddValue("_alreadyFetchedTransactionJournals", _alreadyFetchedTransactionJournals);
			info.AddValue("_transactionJournals_", (!this.MarkedForDeletion?_transactionJournals_:null));
			info.AddValue("_alwaysFetchTransactionJournals_", _alwaysFetchTransactionJournals_);
			info.AddValue("_alreadyFetchedTransactionJournals_", _alreadyFetchedTransactionJournals_);
			info.AddValue("_vouchers", (!this.MarkedForDeletion?_vouchers:null));
			info.AddValue("_alwaysFetchVouchers", _alwaysFetchVouchers);
			info.AddValue("_alreadyFetchedVouchers", _alreadyFetchedVouchers);
			info.AddValue("_attributevalueCollectionViaAttributeValueList", (!this.MarkedForDeletion?_attributevalueCollectionViaAttributeValueList:null));
			info.AddValue("_alwaysFetchAttributevalueCollectionViaAttributeValueList", _alwaysFetchAttributevalueCollectionViaAttributeValueList);
			info.AddValue("_alreadyFetchedAttributevalueCollectionViaAttributeValueList", _alreadyFetchedAttributevalueCollectionViaAttributeValueList);
			info.AddValue("_cardChipTypeCollectionViaTicketPhysicalCardType", (!this.MarkedForDeletion?_cardChipTypeCollectionViaTicketPhysicalCardType:null));
			info.AddValue("_alwaysFetchCardChipTypeCollectionViaTicketPhysicalCardType", _alwaysFetchCardChipTypeCollectionViaTicketPhysicalCardType);
			info.AddValue("_alreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType", _alreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType);
			info.AddValue("_cardAdditionsTicketIsUsedIn", (!this.MarkedForDeletion?_cardAdditionsTicketIsUsedIn:null));
			info.AddValue("_alwaysFetchCardAdditionsTicketIsUsedIn", _alwaysFetchCardAdditionsTicketIsUsedIn);
			info.AddValue("_alreadyFetchedCardAdditionsTicketIsUsedIn", _alreadyFetchedCardAdditionsTicketIsUsedIn);
			info.AddValue("_clientCollectionViaTicketVendingClient", (!this.MarkedForDeletion?_clientCollectionViaTicketVendingClient:null));
			info.AddValue("_alwaysFetchClientCollectionViaTicketVendingClient", _alwaysFetchClientCollectionViaTicketVendingClient);
			info.AddValue("_alreadyFetchedClientCollectionViaTicketVendingClient", _alreadyFetchedClientCollectionViaTicketVendingClient);
			info.AddValue("_lineCollectionViaTicketServicesPermitted", (!this.MarkedForDeletion?_lineCollectionViaTicketServicesPermitted:null));
			info.AddValue("_alwaysFetchLineCollectionViaTicketServicesPermitted", _alwaysFetchLineCollectionViaTicketServicesPermitted);
			info.AddValue("_alreadyFetchedLineCollectionViaTicketServicesPermitted", _alreadyFetchedLineCollectionViaTicketServicesPermitted);
			info.AddValue("_paymentIntervalCollectionViaTicketPaymentInterval", (!this.MarkedForDeletion?_paymentIntervalCollectionViaTicketPaymentInterval:null));
			info.AddValue("_alwaysFetchPaymentIntervalCollectionViaTicketPaymentInterval", _alwaysFetchPaymentIntervalCollectionViaTicketPaymentInterval);
			info.AddValue("_alreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval", _alreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval);
			info.AddValue("_ruleCappingCollectionViaRuleCappingToTicket", (!this.MarkedForDeletion?_ruleCappingCollectionViaRuleCappingToTicket:null));
			info.AddValue("_alwaysFetchRuleCappingCollectionViaRuleCappingToTicket", _alwaysFetchRuleCappingCollectionViaRuleCappingToTicket);
			info.AddValue("_alreadyFetchedRuleCappingCollectionViaRuleCappingToTicket", _alreadyFetchedRuleCappingCollectionViaRuleCappingToTicket);
			info.AddValue("_ticketGroupCollectionViaTicketToGroup", (!this.MarkedForDeletion?_ticketGroupCollectionViaTicketToGroup:null));
			info.AddValue("_alwaysFetchTicketGroupCollectionViaTicketToGroup", _alwaysFetchTicketGroupCollectionViaTicketToGroup);
			info.AddValue("_alreadyFetchedTicketGroupCollectionViaTicketToGroup", _alreadyFetchedTicketGroupCollectionViaTicketToGroup);
			info.AddValue("_organizationCollectionViaTicketOrganization", (!this.MarkedForDeletion?_organizationCollectionViaTicketOrganization:null));
			info.AddValue("_alwaysFetchOrganizationCollectionViaTicketOrganization", _alwaysFetchOrganizationCollectionViaTicketOrganization);
			info.AddValue("_alreadyFetchedOrganizationCollectionViaTicketOrganization", _alreadyFetchedOrganizationCollectionViaTicketOrganization);
			info.AddValue("_settlementQuerySettings", (!this.MarkedForDeletion?_settlementQuerySettings:null));
			info.AddValue("_alwaysFetchSettlementQuerySettings", _alwaysFetchSettlementQuerySettings);
			info.AddValue("_alreadyFetchedSettlementQuerySettings", _alreadyFetchedSettlementQuerySettings);
			info.AddValue("_businessRuleInspection", (!this.MarkedForDeletion?_businessRuleInspection:null));
			info.AddValue("_businessRuleInspectionReturnsNewIfNotFound", _businessRuleInspectionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBusinessRuleInspection", _alwaysFetchBusinessRuleInspection);
			info.AddValue("_alreadyFetchedBusinessRuleInspection", _alreadyFetchedBusinessRuleInspection);
			info.AddValue("_businessRuleTestBoarding", (!this.MarkedForDeletion?_businessRuleTestBoarding:null));
			info.AddValue("_businessRuleTestBoardingReturnsNewIfNotFound", _businessRuleTestBoardingReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBusinessRuleTestBoarding", _alwaysFetchBusinessRuleTestBoarding);
			info.AddValue("_alreadyFetchedBusinessRuleTestBoarding", _alreadyFetchedBusinessRuleTestBoarding);
			info.AddValue("_businessRule", (!this.MarkedForDeletion?_businessRule:null));
			info.AddValue("_businessRuleReturnsNewIfNotFound", _businessRuleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBusinessRule", _alwaysFetchBusinessRule);
			info.AddValue("_alreadyFetchedBusinessRule", _alreadyFetchedBusinessRule);
			info.AddValue("_businessRuleTicketAssignment", (!this.MarkedForDeletion?_businessRuleTicketAssignment:null));
			info.AddValue("_businessRuleTicketAssignmentReturnsNewIfNotFound", _businessRuleTicketAssignmentReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBusinessRuleTicketAssignment", _alwaysFetchBusinessRuleTicketAssignment);
			info.AddValue("_alreadyFetchedBusinessRuleTicketAssignment", _alreadyFetchedBusinessRuleTicketAssignment);
			info.AddValue("_calendar", (!this.MarkedForDeletion?_calendar:null));
			info.AddValue("_calendarReturnsNewIfNotFound", _calendarReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCalendar", _alwaysFetchCalendar);
			info.AddValue("_alreadyFetchedCalendar", _alreadyFetchedCalendar);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_deviceClass", (!this.MarkedForDeletion?_deviceClass:null));
			info.AddValue("_deviceClassReturnsNewIfNotFound", _deviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceClass", _alwaysFetchDeviceClass);
			info.AddValue("_alreadyFetchedDeviceClass", _alreadyFetchedDeviceClass);
			info.AddValue("_eticket", (!this.MarkedForDeletion?_eticket:null));
			info.AddValue("_eticketReturnsNewIfNotFound", _eticketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEticket", _alwaysFetchEticket);
			info.AddValue("_alreadyFetchedEticket", _alreadyFetchedEticket);
			info.AddValue("_fareEvasionCategory", (!this.MarkedForDeletion?_fareEvasionCategory:null));
			info.AddValue("_fareEvasionCategoryReturnsNewIfNotFound", _fareEvasionCategoryReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareEvasionCategory", _alwaysFetchFareEvasionCategory);
			info.AddValue("_alreadyFetchedFareEvasionCategory", _alreadyFetchedFareEvasionCategory);
			info.AddValue("_fareMatrix", (!this.MarkedForDeletion?_fareMatrix:null));
			info.AddValue("_fareMatrixReturnsNewIfNotFound", _fareMatrixReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareMatrix", _alwaysFetchFareMatrix);
			info.AddValue("_alreadyFetchedFareMatrix", _alreadyFetchedFareMatrix);
			info.AddValue("_fareTable1", (!this.MarkedForDeletion?_fareTable1:null));
			info.AddValue("_fareTable1ReturnsNewIfNotFound", _fareTable1ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareTable1", _alwaysFetchFareTable1);
			info.AddValue("_alreadyFetchedFareTable1", _alreadyFetchedFareTable1);
			info.AddValue("_fareTable2", (!this.MarkedForDeletion?_fareTable2:null));
			info.AddValue("_fareTable2ReturnsNewIfNotFound", _fareTable2ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareTable2", _alwaysFetchFareTable2);
			info.AddValue("_alreadyFetchedFareTable2", _alreadyFetchedFareTable2);
			info.AddValue("_cancellationLayout", (!this.MarkedForDeletion?_cancellationLayout:null));
			info.AddValue("_cancellationLayoutReturnsNewIfNotFound", _cancellationLayoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCancellationLayout", _alwaysFetchCancellationLayout);
			info.AddValue("_alreadyFetchedCancellationLayout", _alreadyFetchedCancellationLayout);
			info.AddValue("_groupLayout", (!this.MarkedForDeletion?_groupLayout:null));
			info.AddValue("_groupLayoutReturnsNewIfNotFound", _groupLayoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGroupLayout", _alwaysFetchGroupLayout);
			info.AddValue("_alreadyFetchedGroupLayout", _alreadyFetchedGroupLayout);
			info.AddValue("_printLayout", (!this.MarkedForDeletion?_printLayout:null));
			info.AddValue("_printLayoutReturnsNewIfNotFound", _printLayoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPrintLayout", _alwaysFetchPrintLayout);
			info.AddValue("_alreadyFetchedPrintLayout", _alreadyFetchedPrintLayout);
			info.AddValue("_receiptLayout", (!this.MarkedForDeletion?_receiptLayout:null));
			info.AddValue("_receiptLayoutReturnsNewIfNotFound", _receiptLayoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReceiptLayout", _alwaysFetchReceiptLayout);
			info.AddValue("_alreadyFetchedReceiptLayout", _alreadyFetchedReceiptLayout);
			info.AddValue("_lineGroup", (!this.MarkedForDeletion?_lineGroup:null));
			info.AddValue("_lineGroupReturnsNewIfNotFound", _lineGroupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLineGroup", _alwaysFetchLineGroup);
			info.AddValue("_alreadyFetchedLineGroup", _alreadyFetchedLineGroup);
			info.AddValue("_pageContentGroup", (!this.MarkedForDeletion?_pageContentGroup:null));
			info.AddValue("_pageContentGroupReturnsNewIfNotFound", _pageContentGroupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageContentGroup", _alwaysFetchPageContentGroup);
			info.AddValue("_alreadyFetchedPageContentGroup", _alreadyFetchedPageContentGroup);
			info.AddValue("_priceType", (!this.MarkedForDeletion?_priceType:null));
			info.AddValue("_priceTypeReturnsNewIfNotFound", _priceTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPriceType", _alwaysFetchPriceType);
			info.AddValue("_alreadyFetchedPriceType", _alreadyFetchedPriceType);
			info.AddValue("_rulePeriod", (!this.MarkedForDeletion?_rulePeriod:null));
			info.AddValue("_rulePeriodReturnsNewIfNotFound", _rulePeriodReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRulePeriod", _alwaysFetchRulePeriod);
			info.AddValue("_alreadyFetchedRulePeriod", _alreadyFetchedRulePeriod);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);
			info.AddValue("_temporalType", (!this.MarkedForDeletion?_temporalType:null));
			info.AddValue("_temporalTypeReturnsNewIfNotFound", _temporalTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTemporalType", _alwaysFetchTemporalType);
			info.AddValue("_alreadyFetchedTemporalType", _alreadyFetchedTemporalType);
			info.AddValue("_ticketCancellationType", (!this.MarkedForDeletion?_ticketCancellationType:null));
			info.AddValue("_ticketCancellationTypeReturnsNewIfNotFound", _ticketCancellationTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicketCancellationType", _alwaysFetchTicketCancellationType);
			info.AddValue("_alreadyFetchedTicketCancellationType", _alreadyFetchedTicketCancellationType);
			info.AddValue("_ticketCategory", (!this.MarkedForDeletion?_ticketCategory:null));
			info.AddValue("_ticketCategoryReturnsNewIfNotFound", _ticketCategoryReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicketCategory", _alwaysFetchTicketCategory);
			info.AddValue("_alreadyFetchedTicketCategory", _alreadyFetchedTicketCategory);
			info.AddValue("_ticketSelectionMode", (!this.MarkedForDeletion?_ticketSelectionMode:null));
			info.AddValue("_ticketSelectionModeReturnsNewIfNotFound", _ticketSelectionModeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicketSelectionMode", _alwaysFetchTicketSelectionMode);
			info.AddValue("_alreadyFetchedTicketSelectionMode", _alreadyFetchedTicketSelectionMode);
			info.AddValue("_ticketType", (!this.MarkedForDeletion?_ticketType:null));
			info.AddValue("_ticketTypeReturnsNewIfNotFound", _ticketTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicketType", _alwaysFetchTicketType);
			info.AddValue("_alreadyFetchedTicketType", _alreadyFetchedTicketType);
			info.AddValue("_userKey1", (!this.MarkedForDeletion?_userKey1:null));
			info.AddValue("_userKey1ReturnsNewIfNotFound", _userKey1ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserKey1", _alwaysFetchUserKey1);
			info.AddValue("_alreadyFetchedUserKey1", _alreadyFetchedUserKey1);
			info.AddValue("_userKey2", (!this.MarkedForDeletion?_userKey2:null));
			info.AddValue("_userKey2ReturnsNewIfNotFound", _userKey2ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserKey2", _alwaysFetchUserKey2);
			info.AddValue("_alreadyFetchedUserKey2", _alreadyFetchedUserKey2);

			info.AddValue("_cardTicketAddition", (!this.MarkedForDeletion?_cardTicketAddition:null));
			info.AddValue("_cardTicketAdditionReturnsNewIfNotFound", _cardTicketAdditionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardTicketAddition", _alwaysFetchCardTicketAddition);
			info.AddValue("_alreadyFetchedCardTicketAddition", _alreadyFetchedCardTicketAddition);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BusinessRuleInspection":
					_alreadyFetchedBusinessRuleInspection = true;
					this.BusinessRuleInspection = (BusinessRuleEntity)entity;
					break;
				case "BusinessRuleTestBoarding":
					_alreadyFetchedBusinessRuleTestBoarding = true;
					this.BusinessRuleTestBoarding = (BusinessRuleEntity)entity;
					break;
				case "BusinessRule":
					_alreadyFetchedBusinessRule = true;
					this.BusinessRule = (BusinessRuleEntity)entity;
					break;
				case "BusinessRuleTicketAssignment":
					_alreadyFetchedBusinessRuleTicketAssignment = true;
					this.BusinessRuleTicketAssignment = (BusinessRuleEntity)entity;
					break;
				case "Calendar":
					_alreadyFetchedCalendar = true;
					this.Calendar = (CalendarEntity)entity;
					break;
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "DeviceClass":
					_alreadyFetchedDeviceClass = true;
					this.DeviceClass = (DeviceClassEntity)entity;
					break;
				case "Eticket":
					_alreadyFetchedEticket = true;
					this.Eticket = (EticketEntity)entity;
					break;
				case "FareEvasionCategory":
					_alreadyFetchedFareEvasionCategory = true;
					this.FareEvasionCategory = (FareEvasionCategoryEntity)entity;
					break;
				case "FareMatrix":
					_alreadyFetchedFareMatrix = true;
					this.FareMatrix = (FareMatrixEntity)entity;
					break;
				case "FareTable1":
					_alreadyFetchedFareTable1 = true;
					this.FareTable1 = (FareTableEntity)entity;
					break;
				case "FareTable2":
					_alreadyFetchedFareTable2 = true;
					this.FareTable2 = (FareTableEntity)entity;
					break;
				case "CancellationLayout":
					_alreadyFetchedCancellationLayout = true;
					this.CancellationLayout = (LayoutEntity)entity;
					break;
				case "GroupLayout":
					_alreadyFetchedGroupLayout = true;
					this.GroupLayout = (LayoutEntity)entity;
					break;
				case "PrintLayout":
					_alreadyFetchedPrintLayout = true;
					this.PrintLayout = (LayoutEntity)entity;
					break;
				case "ReceiptLayout":
					_alreadyFetchedReceiptLayout = true;
					this.ReceiptLayout = (LayoutEntity)entity;
					break;
				case "LineGroup":
					_alreadyFetchedLineGroup = true;
					this.LineGroup = (LineGroupEntity)entity;
					break;
				case "PageContentGroup":
					_alreadyFetchedPageContentGroup = true;
					this.PageContentGroup = (PageContentGroupEntity)entity;
					break;
				case "PriceType":
					_alreadyFetchedPriceType = true;
					this.PriceType = (PriceTypeEntity)entity;
					break;
				case "RulePeriod":
					_alreadyFetchedRulePeriod = true;
					this.RulePeriod = (RulePeriodEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "TemporalType":
					_alreadyFetchedTemporalType = true;
					this.TemporalType = (TemporalTypeEntity)entity;
					break;
				case "TicketCancellationType":
					_alreadyFetchedTicketCancellationType = true;
					this.TicketCancellationType = (TicketCancellationTypeEntity)entity;
					break;
				case "TicketCategory":
					_alreadyFetchedTicketCategory = true;
					this.TicketCategory = (TicketCategoryEntity)entity;
					break;
				case "TicketSelectionMode":
					_alreadyFetchedTicketSelectionMode = true;
					this.TicketSelectionMode = (TicketSelectionModeEntity)entity;
					break;
				case "TicketType":
					_alreadyFetchedTicketType = true;
					this.TicketType = (TicketTypeEntity)entity;
					break;
				case "UserKey1":
					_alreadyFetchedUserKey1 = true;
					this.UserKey1 = (UserKeyEntity)entity;
					break;
				case "UserKey2":
					_alreadyFetchedUserKey2 = true;
					this.UserKey2 = (UserKeyEntity)entity;
					break;
				case "ApportionmentResults":
					_alreadyFetchedApportionmentResults = true;
					if(entity!=null)
					{
						this.ApportionmentResults.Add((ApportionmentResultEntity)entity);
					}
					break;
				case "AttributeValueList":
					_alreadyFetchedAttributeValueList = true;
					if(entity!=null)
					{
						this.AttributeValueList.Add((AttributeValueListEntity)entity);
					}
					break;
				case "CardTicketToTicket":
					_alreadyFetchedCardTicketToTicket = true;
					if(entity!=null)
					{
						this.CardTicketToTicket.Add((CardTicketToTicketEntity)entity);
					}
					break;
				case "Choices":
					_alreadyFetchedChoices = true;
					if(entity!=null)
					{
						this.Choices.Add((ChoiceEntity)entity);
					}
					break;
				case "ParameterTicket":
					_alreadyFetchedParameterTicket = true;
					if(entity!=null)
					{
						this.ParameterTicket.Add((ParameterTicketEntity)entity);
					}
					break;
				case "PrimalKeys":
					_alreadyFetchedPrimalKeys = true;
					if(entity!=null)
					{
						this.PrimalKeys.Add((PrimalKeyEntity)entity);
					}
					break;
				case "RuleCappingToTicket":
					_alreadyFetchedRuleCappingToTicket = true;
					if(entity!=null)
					{
						this.RuleCappingToTicket.Add((RuleCappingToTicketEntity)entity);
					}
					break;
				case "TicketDayTypes":
					_alreadyFetchedTicketDayTypes = true;
					if(entity!=null)
					{
						this.TicketDayTypes.Add((TicketDayTypeEntity)entity);
					}
					break;
				case "TicketDeviceClasses":
					_alreadyFetchedTicketDeviceClasses = true;
					if(entity!=null)
					{
						this.TicketDeviceClasses.Add((TicketDeviceClassEntity)entity);
					}
					break;
				case "TicketDevicePaymentMethods":
					_alreadyFetchedTicketDevicePaymentMethods = true;
					if(entity!=null)
					{
						this.TicketDevicePaymentMethods.Add((TicketDevicePaymentMethodEntity)entity);
					}
					break;
				case "TicketOrganizations":
					_alreadyFetchedTicketOrganizations = true;
					if(entity!=null)
					{
						this.TicketOrganizations.Add((TicketOrganizationEntity)entity);
					}
					break;
				case "TicketOutputDevices":
					_alreadyFetchedTicketOutputDevices = true;
					if(entity!=null)
					{
						this.TicketOutputDevices.Add((TicketOutputdeviceEntity)entity);
					}
					break;
				case "TicketPaymentIntervals":
					_alreadyFetchedTicketPaymentIntervals = true;
					if(entity!=null)
					{
						this.TicketPaymentIntervals.Add((TicketPaymentIntervalEntity)entity);
					}
					break;
				case "TicketPhysicalCardTypes":
					_alreadyFetchedTicketPhysicalCardTypes = true;
					if(entity!=null)
					{
						this.TicketPhysicalCardTypes.Add((TicketPhysicalCardTypeEntity)entity);
					}
					break;
				case "TicketServicesPermitteds":
					_alreadyFetchedTicketServicesPermitteds = true;
					if(entity!=null)
					{
						this.TicketServicesPermitteds.Add((TicketServicesPermittedEntity)entity);
					}
					break;
				case "TicketToGroups":
					_alreadyFetchedTicketToGroups = true;
					if(entity!=null)
					{
						this.TicketToGroups.Add((TicketToGroupEntity)entity);
					}
					break;
				case "TicketVendingClient":
					_alreadyFetchedTicketVendingClient = true;
					if(entity!=null)
					{
						this.TicketVendingClient.Add((TicketVendingClientEntity)entity);
					}
					break;
				case "VdvKeySet":
					_alreadyFetchedVdvKeySet = true;
					if(entity!=null)
					{
						this.VdvKeySet.Add((VdvKeySetEntity)entity);
					}
					break;
				case "VdvProduct":
					_alreadyFetchedVdvProduct = true;
					if(entity!=null)
					{
						this.VdvProduct.Add((VdvProductEntity)entity);
					}
					break;
				case "TaxProducts":
					_alreadyFetchedTaxProducts = true;
					if(entity!=null)
					{
						this.TaxProducts.Add((ProductEntity)entity);
					}
					break;
				case "Products":
					_alreadyFetchedProducts = true;
					if(entity!=null)
					{
						this.Products.Add((ProductEntity)entity);
					}
					break;
				case "SettlementQuerySettingToTickets":
					_alreadyFetchedSettlementQuerySettingToTickets = true;
					if(entity!=null)
					{
						this.SettlementQuerySettingToTickets.Add((SettlementQuerySettingToTicketEntity)entity);
					}
					break;
				case "TicketSerialNumbers":
					_alreadyFetchedTicketSerialNumbers = true;
					if(entity!=null)
					{
						this.TicketSerialNumbers.Add((TicketSerialNumberEntity)entity);
					}
					break;
				case "TransactionJournalsForAppliedPassTicket":
					_alreadyFetchedTransactionJournalsForAppliedPassTicket = true;
					if(entity!=null)
					{
						this.TransactionJournalsForAppliedPassTicket.Add((TransactionJournalEntity)entity);
					}
					break;
				case "TransactionJournals":
					_alreadyFetchedTransactionJournals = true;
					if(entity!=null)
					{
						this.TransactionJournals.Add((TransactionJournalEntity)entity);
					}
					break;
				case "TransactionJournals_":
					_alreadyFetchedTransactionJournals_ = true;
					if(entity!=null)
					{
						this.TransactionJournals_.Add((TransactionJournalEntity)entity);
					}
					break;
				case "Vouchers":
					_alreadyFetchedVouchers = true;
					if(entity!=null)
					{
						this.Vouchers.Add((VoucherEntity)entity);
					}
					break;
				case "AttributevalueCollectionViaAttributeValueList":
					_alreadyFetchedAttributevalueCollectionViaAttributeValueList = true;
					if(entity!=null)
					{
						this.AttributevalueCollectionViaAttributeValueList.Add((AttributeValueEntity)entity);
					}
					break;
				case "CardChipTypeCollectionViaTicketPhysicalCardType":
					_alreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType = true;
					if(entity!=null)
					{
						this.CardChipTypeCollectionViaTicketPhysicalCardType.Add((CardChipTypeEntity)entity);
					}
					break;
				case "CardAdditionsTicketIsUsedIn":
					_alreadyFetchedCardAdditionsTicketIsUsedIn = true;
					if(entity!=null)
					{
						this.CardAdditionsTicketIsUsedIn.Add((CardTicketEntity)entity);
					}
					break;
				case "ClientCollectionViaTicketVendingClient":
					_alreadyFetchedClientCollectionViaTicketVendingClient = true;
					if(entity!=null)
					{
						this.ClientCollectionViaTicketVendingClient.Add((ClientEntity)entity);
					}
					break;
				case "LineCollectionViaTicketServicesPermitted":
					_alreadyFetchedLineCollectionViaTicketServicesPermitted = true;
					if(entity!=null)
					{
						this.LineCollectionViaTicketServicesPermitted.Add((LineEntity)entity);
					}
					break;
				case "PaymentIntervalCollectionViaTicketPaymentInterval":
					_alreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval = true;
					if(entity!=null)
					{
						this.PaymentIntervalCollectionViaTicketPaymentInterval.Add((PaymentIntervalEntity)entity);
					}
					break;
				case "RuleCappingCollectionViaRuleCappingToTicket":
					_alreadyFetchedRuleCappingCollectionViaRuleCappingToTicket = true;
					if(entity!=null)
					{
						this.RuleCappingCollectionViaRuleCappingToTicket.Add((RuleCappingEntity)entity);
					}
					break;
				case "TicketGroupCollectionViaTicketToGroup":
					_alreadyFetchedTicketGroupCollectionViaTicketToGroup = true;
					if(entity!=null)
					{
						this.TicketGroupCollectionViaTicketToGroup.Add((TicketGroupEntity)entity);
					}
					break;
				case "OrganizationCollectionViaTicketOrganization":
					_alreadyFetchedOrganizationCollectionViaTicketOrganization = true;
					if(entity!=null)
					{
						this.OrganizationCollectionViaTicketOrganization.Add((OrganizationEntity)entity);
					}
					break;
				case "SettlementQuerySettings":
					_alreadyFetchedSettlementQuerySettings = true;
					if(entity!=null)
					{
						this.SettlementQuerySettings.Add((SettlementQuerySettingEntity)entity);
					}
					break;
				case "CardTicketAddition":
					_alreadyFetchedCardTicketAddition = true;
					this.CardTicketAddition = (CardTicketEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BusinessRuleInspection":
					SetupSyncBusinessRuleInspection(relatedEntity);
					break;
				case "BusinessRuleTestBoarding":
					SetupSyncBusinessRuleTestBoarding(relatedEntity);
					break;
				case "BusinessRule":
					SetupSyncBusinessRule(relatedEntity);
					break;
				case "BusinessRuleTicketAssignment":
					SetupSyncBusinessRuleTicketAssignment(relatedEntity);
					break;
				case "Calendar":
					SetupSyncCalendar(relatedEntity);
					break;
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "DeviceClass":
					SetupSyncDeviceClass(relatedEntity);
					break;
				case "Eticket":
					SetupSyncEticket(relatedEntity);
					break;
				case "FareEvasionCategory":
					SetupSyncFareEvasionCategory(relatedEntity);
					break;
				case "FareMatrix":
					SetupSyncFareMatrix(relatedEntity);
					break;
				case "FareTable1":
					SetupSyncFareTable1(relatedEntity);
					break;
				case "FareTable2":
					SetupSyncFareTable2(relatedEntity);
					break;
				case "CancellationLayout":
					SetupSyncCancellationLayout(relatedEntity);
					break;
				case "GroupLayout":
					SetupSyncGroupLayout(relatedEntity);
					break;
				case "PrintLayout":
					SetupSyncPrintLayout(relatedEntity);
					break;
				case "ReceiptLayout":
					SetupSyncReceiptLayout(relatedEntity);
					break;
				case "LineGroup":
					SetupSyncLineGroup(relatedEntity);
					break;
				case "PageContentGroup":
					SetupSyncPageContentGroup(relatedEntity);
					break;
				case "PriceType":
					SetupSyncPriceType(relatedEntity);
					break;
				case "RulePeriod":
					SetupSyncRulePeriod(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "TemporalType":
					SetupSyncTemporalType(relatedEntity);
					break;
				case "TicketCancellationType":
					SetupSyncTicketCancellationType(relatedEntity);
					break;
				case "TicketCategory":
					SetupSyncTicketCategory(relatedEntity);
					break;
				case "TicketSelectionMode":
					SetupSyncTicketSelectionMode(relatedEntity);
					break;
				case "TicketType":
					SetupSyncTicketType(relatedEntity);
					break;
				case "UserKey1":
					SetupSyncUserKey1(relatedEntity);
					break;
				case "UserKey2":
					SetupSyncUserKey2(relatedEntity);
					break;
				case "ApportionmentResults":
					_apportionmentResults.Add((ApportionmentResultEntity)relatedEntity);
					break;
				case "AttributeValueList":
					_attributeValueList.Add((AttributeValueListEntity)relatedEntity);
					break;
				case "CardTicketToTicket":
					_cardTicketToTicket.Add((CardTicketToTicketEntity)relatedEntity);
					break;
				case "Choices":
					_choices.Add((ChoiceEntity)relatedEntity);
					break;
				case "ParameterTicket":
					_parameterTicket.Add((ParameterTicketEntity)relatedEntity);
					break;
				case "PrimalKeys":
					_primalKeys.Add((PrimalKeyEntity)relatedEntity);
					break;
				case "RuleCappingToTicket":
					_ruleCappingToTicket.Add((RuleCappingToTicketEntity)relatedEntity);
					break;
				case "TicketDayTypes":
					_ticketDayTypes.Add((TicketDayTypeEntity)relatedEntity);
					break;
				case "TicketDeviceClasses":
					_ticketDeviceClasses.Add((TicketDeviceClassEntity)relatedEntity);
					break;
				case "TicketDevicePaymentMethods":
					_ticketDevicePaymentMethods.Add((TicketDevicePaymentMethodEntity)relatedEntity);
					break;
				case "TicketOrganizations":
					_ticketOrganizations.Add((TicketOrganizationEntity)relatedEntity);
					break;
				case "TicketOutputDevices":
					_ticketOutputDevices.Add((TicketOutputdeviceEntity)relatedEntity);
					break;
				case "TicketPaymentIntervals":
					_ticketPaymentIntervals.Add((TicketPaymentIntervalEntity)relatedEntity);
					break;
				case "TicketPhysicalCardTypes":
					_ticketPhysicalCardTypes.Add((TicketPhysicalCardTypeEntity)relatedEntity);
					break;
				case "TicketServicesPermitteds":
					_ticketServicesPermitteds.Add((TicketServicesPermittedEntity)relatedEntity);
					break;
				case "TicketToGroups":
					_ticketToGroups.Add((TicketToGroupEntity)relatedEntity);
					break;
				case "TicketVendingClient":
					_ticketVendingClient.Add((TicketVendingClientEntity)relatedEntity);
					break;
				case "VdvKeySet":
					_vdvKeySet.Add((VdvKeySetEntity)relatedEntity);
					break;
				case "VdvProduct":
					_vdvProduct.Add((VdvProductEntity)relatedEntity);
					break;
				case "TaxProducts":
					_taxProducts.Add((ProductEntity)relatedEntity);
					break;
				case "Products":
					_products.Add((ProductEntity)relatedEntity);
					break;
				case "SettlementQuerySettingToTickets":
					_settlementQuerySettingToTickets.Add((SettlementQuerySettingToTicketEntity)relatedEntity);
					break;
				case "TicketSerialNumbers":
					_ticketSerialNumbers.Add((TicketSerialNumberEntity)relatedEntity);
					break;
				case "TransactionJournalsForAppliedPassTicket":
					_transactionJournalsForAppliedPassTicket.Add((TransactionJournalEntity)relatedEntity);
					break;
				case "TransactionJournals":
					_transactionJournals.Add((TransactionJournalEntity)relatedEntity);
					break;
				case "TransactionJournals_":
					_transactionJournals_.Add((TransactionJournalEntity)relatedEntity);
					break;
				case "Vouchers":
					_vouchers.Add((VoucherEntity)relatedEntity);
					break;
				case "CardTicketAddition":
					SetupSyncCardTicketAddition(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BusinessRuleInspection":
					DesetupSyncBusinessRuleInspection(false, true);
					break;
				case "BusinessRuleTestBoarding":
					DesetupSyncBusinessRuleTestBoarding(false, true);
					break;
				case "BusinessRule":
					DesetupSyncBusinessRule(false, true);
					break;
				case "BusinessRuleTicketAssignment":
					DesetupSyncBusinessRuleTicketAssignment(false, true);
					break;
				case "Calendar":
					DesetupSyncCalendar(false, true);
					break;
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "DeviceClass":
					DesetupSyncDeviceClass(false, true);
					break;
				case "Eticket":
					DesetupSyncEticket(false, true);
					break;
				case "FareEvasionCategory":
					DesetupSyncFareEvasionCategory(false, true);
					break;
				case "FareMatrix":
					DesetupSyncFareMatrix(false, true);
					break;
				case "FareTable1":
					DesetupSyncFareTable1(false, true);
					break;
				case "FareTable2":
					DesetupSyncFareTable2(false, true);
					break;
				case "CancellationLayout":
					DesetupSyncCancellationLayout(false, true);
					break;
				case "GroupLayout":
					DesetupSyncGroupLayout(false, true);
					break;
				case "PrintLayout":
					DesetupSyncPrintLayout(false, true);
					break;
				case "ReceiptLayout":
					DesetupSyncReceiptLayout(false, true);
					break;
				case "LineGroup":
					DesetupSyncLineGroup(false, true);
					break;
				case "PageContentGroup":
					DesetupSyncPageContentGroup(false, true);
					break;
				case "PriceType":
					DesetupSyncPriceType(false, true);
					break;
				case "RulePeriod":
					DesetupSyncRulePeriod(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "TemporalType":
					DesetupSyncTemporalType(false, true);
					break;
				case "TicketCancellationType":
					DesetupSyncTicketCancellationType(false, true);
					break;
				case "TicketCategory":
					DesetupSyncTicketCategory(false, true);
					break;
				case "TicketSelectionMode":
					DesetupSyncTicketSelectionMode(false, true);
					break;
				case "TicketType":
					DesetupSyncTicketType(false, true);
					break;
				case "UserKey1":
					DesetupSyncUserKey1(false, true);
					break;
				case "UserKey2":
					DesetupSyncUserKey2(false, true);
					break;
				case "ApportionmentResults":
					this.PerformRelatedEntityRemoval(_apportionmentResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AttributeValueList":
					this.PerformRelatedEntityRemoval(_attributeValueList, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardTicketToTicket":
					this.PerformRelatedEntityRemoval(_cardTicketToTicket, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Choices":
					this.PerformRelatedEntityRemoval(_choices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterTicket":
					this.PerformRelatedEntityRemoval(_parameterTicket, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PrimalKeys":
					this.PerformRelatedEntityRemoval(_primalKeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RuleCappingToTicket":
					this.PerformRelatedEntityRemoval(_ruleCappingToTicket, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDayTypes":
					this.PerformRelatedEntityRemoval(_ticketDayTypes, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceClasses":
					this.PerformRelatedEntityRemoval(_ticketDeviceClasses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDevicePaymentMethods":
					this.PerformRelatedEntityRemoval(_ticketDevicePaymentMethods, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketOrganizations":
					this.PerformRelatedEntityRemoval(_ticketOrganizations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketOutputDevices":
					this.PerformRelatedEntityRemoval(_ticketOutputDevices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketPaymentIntervals":
					this.PerformRelatedEntityRemoval(_ticketPaymentIntervals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketPhysicalCardTypes":
					this.PerformRelatedEntityRemoval(_ticketPhysicalCardTypes, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketServicesPermitteds":
					this.PerformRelatedEntityRemoval(_ticketServicesPermitteds, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketToGroups":
					this.PerformRelatedEntityRemoval(_ticketToGroups, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketVendingClient":
					this.PerformRelatedEntityRemoval(_ticketVendingClient, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvKeySet":
					this.PerformRelatedEntityRemoval(_vdvKeySet, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvProduct":
					this.PerformRelatedEntityRemoval(_vdvProduct, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TaxProducts":
					this.PerformRelatedEntityRemoval(_taxProducts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Products":
					this.PerformRelatedEntityRemoval(_products, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SettlementQuerySettingToTickets":
					this.PerformRelatedEntityRemoval(_settlementQuerySettingToTickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketSerialNumbers":
					this.PerformRelatedEntityRemoval(_ticketSerialNumbers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournalsForAppliedPassTicket":
					this.PerformRelatedEntityRemoval(_transactionJournalsForAppliedPassTicket, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournals":
					this.PerformRelatedEntityRemoval(_transactionJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournals_":
					this.PerformRelatedEntityRemoval(_transactionJournals_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Vouchers":
					this.PerformRelatedEntityRemoval(_vouchers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardTicketAddition":
					DesetupSyncCardTicketAddition(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cardTicketAddition!=null)
			{
				toReturn.Add(_cardTicketAddition);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_businessRuleInspection!=null)
			{
				toReturn.Add(_businessRuleInspection);
			}
			if(_businessRuleTestBoarding!=null)
			{
				toReturn.Add(_businessRuleTestBoarding);
			}
			if(_businessRule!=null)
			{
				toReturn.Add(_businessRule);
			}
			if(_businessRuleTicketAssignment!=null)
			{
				toReturn.Add(_businessRuleTicketAssignment);
			}
			if(_calendar!=null)
			{
				toReturn.Add(_calendar);
			}
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_deviceClass!=null)
			{
				toReturn.Add(_deviceClass);
			}
			if(_eticket!=null)
			{
				toReturn.Add(_eticket);
			}
			if(_fareEvasionCategory!=null)
			{
				toReturn.Add(_fareEvasionCategory);
			}
			if(_fareMatrix!=null)
			{
				toReturn.Add(_fareMatrix);
			}
			if(_fareTable1!=null)
			{
				toReturn.Add(_fareTable1);
			}
			if(_fareTable2!=null)
			{
				toReturn.Add(_fareTable2);
			}
			if(_cancellationLayout!=null)
			{
				toReturn.Add(_cancellationLayout);
			}
			if(_groupLayout!=null)
			{
				toReturn.Add(_groupLayout);
			}
			if(_printLayout!=null)
			{
				toReturn.Add(_printLayout);
			}
			if(_receiptLayout!=null)
			{
				toReturn.Add(_receiptLayout);
			}
			if(_lineGroup!=null)
			{
				toReturn.Add(_lineGroup);
			}
			if(_pageContentGroup!=null)
			{
				toReturn.Add(_pageContentGroup);
			}
			if(_priceType!=null)
			{
				toReturn.Add(_priceType);
			}
			if(_rulePeriod!=null)
			{
				toReturn.Add(_rulePeriod);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			if(_temporalType!=null)
			{
				toReturn.Add(_temporalType);
			}
			if(_ticketCancellationType!=null)
			{
				toReturn.Add(_ticketCancellationType);
			}
			if(_ticketCategory!=null)
			{
				toReturn.Add(_ticketCategory);
			}
			if(_ticketSelectionMode!=null)
			{
				toReturn.Add(_ticketSelectionMode);
			}
			if(_ticketType!=null)
			{
				toReturn.Add(_ticketType);
			}
			if(_userKey1!=null)
			{
				toReturn.Add(_userKey1);
			}
			if(_userKey2!=null)
			{
				toReturn.Add(_userKey2);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_apportionmentResults);
			toReturn.Add(_attributeValueList);
			toReturn.Add(_cardTicketToTicket);
			toReturn.Add(_choices);
			toReturn.Add(_parameterTicket);
			toReturn.Add(_primalKeys);
			toReturn.Add(_ruleCappingToTicket);
			toReturn.Add(_ticketDayTypes);
			toReturn.Add(_ticketDeviceClasses);
			toReturn.Add(_ticketDevicePaymentMethods);
			toReturn.Add(_ticketOrganizations);
			toReturn.Add(_ticketOutputDevices);
			toReturn.Add(_ticketPaymentIntervals);
			toReturn.Add(_ticketPhysicalCardTypes);
			toReturn.Add(_ticketServicesPermitteds);
			toReturn.Add(_ticketToGroups);
			toReturn.Add(_ticketVendingClient);
			toReturn.Add(_vdvKeySet);
			toReturn.Add(_vdvProduct);
			toReturn.Add(_taxProducts);
			toReturn.Add(_products);
			toReturn.Add(_settlementQuerySettingToTickets);
			toReturn.Add(_ticketSerialNumbers);
			toReturn.Add(_transactionJournalsForAppliedPassTicket);
			toReturn.Add(_transactionJournals);
			toReturn.Add(_transactionJournals_);
			toReturn.Add(_vouchers);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketID">PK value for Ticket which data should be fetched into this Ticket object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketID)
		{
			return FetchUsingPK(ticketID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketID">PK value for Ticket which data should be fetched into this Ticket object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(ticketID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketID">PK value for Ticket which data should be fetched into this Ticket object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(ticketID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketID">PK value for Ticket which data should be fetched into this Ticket object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(ticketID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TicketID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TicketRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ApportionmentResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults(bool forceFetch)
		{
			return GetMultiApportionmentResults(forceFetch, _apportionmentResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ApportionmentResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiApportionmentResults(forceFetch, _apportionmentResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiApportionmentResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedApportionmentResults || forceFetch || _alwaysFetchApportionmentResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_apportionmentResults);
				_apportionmentResults.SuppressClearInGetMulti=!forceFetch;
				_apportionmentResults.EntityFactoryToUse = entityFactoryToUse;
				_apportionmentResults.GetMultiManyToOne(null, null, null, null, this, null, filter);
				_apportionmentResults.SuppressClearInGetMulti=false;
				_alreadyFetchedApportionmentResults = true;
			}
			return _apportionmentResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ApportionmentResults'. These settings will be taken into account
		/// when the property ApportionmentResults is requested or GetMultiApportionmentResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersApportionmentResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_apportionmentResults.SortClauses=sortClauses;
			_apportionmentResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeValueListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueListCollection GetMultiAttributeValueList(bool forceFetch)
		{
			return GetMultiAttributeValueList(forceFetch, _attributeValueList.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttributeValueListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueListCollection GetMultiAttributeValueList(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttributeValueList(forceFetch, _attributeValueList.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueListCollection GetMultiAttributeValueList(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttributeValueList(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AttributeValueListCollection GetMultiAttributeValueList(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttributeValueList || forceFetch || _alwaysFetchAttributeValueList) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributeValueList);
				_attributeValueList.SuppressClearInGetMulti=!forceFetch;
				_attributeValueList.EntityFactoryToUse = entityFactoryToUse;
				_attributeValueList.GetMultiManyToOne(null, this, filter);
				_attributeValueList.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributeValueList = true;
			}
			return _attributeValueList;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttributeValueList'. These settings will be taken into account
		/// when the property AttributeValueList is requested or GetMultiAttributeValueList is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributeValueList(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributeValueList.SortClauses=sortClauses;
			_attributeValueList.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardTicketToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardTicketToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection GetMultiCardTicketToTicket(bool forceFetch)
		{
			return GetMultiCardTicketToTicket(forceFetch, _cardTicketToTicket.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardTicketToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection GetMultiCardTicketToTicket(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardTicketToTicket(forceFetch, _cardTicketToTicket.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection GetMultiCardTicketToTicket(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardTicketToTicket(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection GetMultiCardTicketToTicket(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardTicketToTicket || forceFetch || _alwaysFetchCardTicketToTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardTicketToTicket);
				_cardTicketToTicket.SuppressClearInGetMulti=!forceFetch;
				_cardTicketToTicket.EntityFactoryToUse = entityFactoryToUse;
				_cardTicketToTicket.GetMultiManyToOne(null, this, filter);
				_cardTicketToTicket.SuppressClearInGetMulti=false;
				_alreadyFetchedCardTicketToTicket = true;
			}
			return _cardTicketToTicket;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardTicketToTicket'. These settings will be taken into account
		/// when the property CardTicketToTicket is requested or GetMultiCardTicketToTicket is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardTicketToTicket(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardTicketToTicket.SortClauses=sortClauses;
			_cardTicketToTicket.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ChoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ChoiceCollection GetMultiChoices(bool forceFetch)
		{
			return GetMultiChoices(forceFetch, _choices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ChoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ChoiceCollection GetMultiChoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiChoices(forceFetch, _choices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ChoiceCollection GetMultiChoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiChoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ChoiceCollection GetMultiChoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedChoices || forceFetch || _alwaysFetchChoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_choices);
				_choices.SuppressClearInGetMulti=!forceFetch;
				_choices.EntityFactoryToUse = entityFactoryToUse;
				_choices.GetMultiManyToOne(null, null, null, this, filter);
				_choices.SuppressClearInGetMulti=false;
				_alreadyFetchedChoices = true;
			}
			return _choices;
		}

		/// <summary> Sets the collection parameters for the collection for 'Choices'. These settings will be taken into account
		/// when the property Choices is requested or GetMultiChoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersChoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_choices.SortClauses=sortClauses;
			_choices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTicketCollection GetMultiParameterTicket(bool forceFetch)
		{
			return GetMultiParameterTicket(forceFetch, _parameterTicket.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTicketCollection GetMultiParameterTicket(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterTicket(forceFetch, _parameterTicket.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTicketCollection GetMultiParameterTicket(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterTicket(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterTicketCollection GetMultiParameterTicket(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterTicket || forceFetch || _alwaysFetchParameterTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterTicket);
				_parameterTicket.SuppressClearInGetMulti=!forceFetch;
				_parameterTicket.EntityFactoryToUse = entityFactoryToUse;
				_parameterTicket.GetMultiManyToOne(null, null, this, filter);
				_parameterTicket.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterTicket = true;
			}
			return _parameterTicket;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterTicket'. These settings will be taken into account
		/// when the property ParameterTicket is requested or GetMultiParameterTicket is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterTicket(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterTicket.SortClauses=sortClauses;
			_parameterTicket.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PrimalKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PrimalKeyCollection GetMultiPrimalKeys(bool forceFetch)
		{
			return GetMultiPrimalKeys(forceFetch, _primalKeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PrimalKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PrimalKeyCollection GetMultiPrimalKeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPrimalKeys(forceFetch, _primalKeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PrimalKeyCollection GetMultiPrimalKeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPrimalKeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PrimalKeyCollection GetMultiPrimalKeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPrimalKeys || forceFetch || _alwaysFetchPrimalKeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_primalKeys);
				_primalKeys.SuppressClearInGetMulti=!forceFetch;
				_primalKeys.EntityFactoryToUse = entityFactoryToUse;
				_primalKeys.GetMultiManyToOne(null, null, this, filter);
				_primalKeys.SuppressClearInGetMulti=false;
				_alreadyFetchedPrimalKeys = true;
			}
			return _primalKeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'PrimalKeys'. These settings will be taken into account
		/// when the property PrimalKeys is requested or GetMultiPrimalKeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPrimalKeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_primalKeys.SortClauses=sortClauses;
			_primalKeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection GetMultiRuleCappingToTicket(bool forceFetch)
		{
			return GetMultiRuleCappingToTicket(forceFetch, _ruleCappingToTicket.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection GetMultiRuleCappingToTicket(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleCappingToTicket(forceFetch, _ruleCappingToTicket.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection GetMultiRuleCappingToTicket(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleCappingToTicket(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection GetMultiRuleCappingToTicket(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleCappingToTicket || forceFetch || _alwaysFetchRuleCappingToTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleCappingToTicket);
				_ruleCappingToTicket.SuppressClearInGetMulti=!forceFetch;
				_ruleCappingToTicket.EntityFactoryToUse = entityFactoryToUse;
				_ruleCappingToTicket.GetMultiManyToOne(null, this, filter);
				_ruleCappingToTicket.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleCappingToTicket = true;
			}
			return _ruleCappingToTicket;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleCappingToTicket'. These settings will be taken into account
		/// when the property RuleCappingToTicket is requested or GetMultiRuleCappingToTicket is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleCappingToTicket(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleCappingToTicket.SortClauses=sortClauses;
			_ruleCappingToTicket.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDayTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDayTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDayTypeCollection GetMultiTicketDayTypes(bool forceFetch)
		{
			return GetMultiTicketDayTypes(forceFetch, _ticketDayTypes.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDayTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDayTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDayTypeCollection GetMultiTicketDayTypes(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDayTypes(forceFetch, _ticketDayTypes.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDayTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDayTypeCollection GetMultiTicketDayTypes(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDayTypes(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDayTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDayTypeCollection GetMultiTicketDayTypes(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDayTypes || forceFetch || _alwaysFetchTicketDayTypes) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDayTypes);
				_ticketDayTypes.SuppressClearInGetMulti=!forceFetch;
				_ticketDayTypes.EntityFactoryToUse = entityFactoryToUse;
				_ticketDayTypes.GetMultiManyToOne(null, this, filter);
				_ticketDayTypes.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDayTypes = true;
			}
			return _ticketDayTypes;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDayTypes'. These settings will be taken into account
		/// when the property TicketDayTypes is requested or GetMultiTicketDayTypes is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDayTypes(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDayTypes.SortClauses=sortClauses;
			_ticketDayTypes.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClasses(bool forceFetch)
		{
			return GetMultiTicketDeviceClasses(forceFetch, _ticketDeviceClasses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClasses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceClasses(forceFetch, _ticketDeviceClasses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClasses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceClasses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClasses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceClasses || forceFetch || _alwaysFetchTicketDeviceClasses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceClasses);
				_ticketDeviceClasses.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceClasses.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceClasses.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, null, null, filter);
				_ticketDeviceClasses.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceClasses = true;
			}
			return _ticketDeviceClasses;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceClasses'. These settings will be taken into account
		/// when the property TicketDeviceClasses is requested or GetMultiTicketDeviceClasses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceClasses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceClasses.SortClauses=sortClauses;
			_ticketDeviceClasses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDevicePaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDevicePaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection GetMultiTicketDevicePaymentMethods(bool forceFetch)
		{
			return GetMultiTicketDevicePaymentMethods(forceFetch, _ticketDevicePaymentMethods.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDevicePaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDevicePaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection GetMultiTicketDevicePaymentMethods(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDevicePaymentMethods(forceFetch, _ticketDevicePaymentMethods.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDevicePaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection GetMultiTicketDevicePaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDevicePaymentMethods(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDevicePaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection GetMultiTicketDevicePaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDevicePaymentMethods || forceFetch || _alwaysFetchTicketDevicePaymentMethods) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDevicePaymentMethods);
				_ticketDevicePaymentMethods.SuppressClearInGetMulti=!forceFetch;
				_ticketDevicePaymentMethods.EntityFactoryToUse = entityFactoryToUse;
				_ticketDevicePaymentMethods.GetMultiManyToOne(null, this, filter);
				_ticketDevicePaymentMethods.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDevicePaymentMethods = true;
			}
			return _ticketDevicePaymentMethods;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDevicePaymentMethods'. These settings will be taken into account
		/// when the property TicketDevicePaymentMethods is requested or GetMultiTicketDevicePaymentMethods is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDevicePaymentMethods(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDevicePaymentMethods.SortClauses=sortClauses;
			_ticketDevicePaymentMethods.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketOrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketOrganizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketOrganizationCollection GetMultiTicketOrganizations(bool forceFetch)
		{
			return GetMultiTicketOrganizations(forceFetch, _ticketOrganizations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketOrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketOrganizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketOrganizationCollection GetMultiTicketOrganizations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketOrganizations(forceFetch, _ticketOrganizations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketOrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketOrganizationCollection GetMultiTicketOrganizations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketOrganizations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketOrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketOrganizationCollection GetMultiTicketOrganizations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketOrganizations || forceFetch || _alwaysFetchTicketOrganizations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketOrganizations);
				_ticketOrganizations.SuppressClearInGetMulti=!forceFetch;
				_ticketOrganizations.EntityFactoryToUse = entityFactoryToUse;
				_ticketOrganizations.GetMultiManyToOne(this, null, filter);
				_ticketOrganizations.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketOrganizations = true;
			}
			return _ticketOrganizations;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketOrganizations'. These settings will be taken into account
		/// when the property TicketOrganizations is requested or GetMultiTicketOrganizations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketOrganizations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketOrganizations.SortClauses=sortClauses;
			_ticketOrganizations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketOutputdeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketOutputdeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection GetMultiTicketOutputDevices(bool forceFetch)
		{
			return GetMultiTicketOutputDevices(forceFetch, _ticketOutputDevices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketOutputdeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketOutputdeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection GetMultiTicketOutputDevices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketOutputDevices(forceFetch, _ticketOutputDevices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketOutputdeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection GetMultiTicketOutputDevices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketOutputDevices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketOutputdeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection GetMultiTicketOutputDevices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketOutputDevices || forceFetch || _alwaysFetchTicketOutputDevices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketOutputDevices);
				_ticketOutputDevices.SuppressClearInGetMulti=!forceFetch;
				_ticketOutputDevices.EntityFactoryToUse = entityFactoryToUse;
				_ticketOutputDevices.GetMultiManyToOne(null, this, filter);
				_ticketOutputDevices.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketOutputDevices = true;
			}
			return _ticketOutputDevices;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketOutputDevices'. These settings will be taken into account
		/// when the property TicketOutputDevices is requested or GetMultiTicketOutputDevices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketOutputDevices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketOutputDevices.SortClauses=sortClauses;
			_ticketOutputDevices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketPaymentIntervalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketPaymentIntervalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection GetMultiTicketPaymentIntervals(bool forceFetch)
		{
			return GetMultiTicketPaymentIntervals(forceFetch, _ticketPaymentIntervals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketPaymentIntervalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketPaymentIntervalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection GetMultiTicketPaymentIntervals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketPaymentIntervals(forceFetch, _ticketPaymentIntervals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketPaymentIntervalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection GetMultiTicketPaymentIntervals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketPaymentIntervals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketPaymentIntervalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection GetMultiTicketPaymentIntervals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketPaymentIntervals || forceFetch || _alwaysFetchTicketPaymentIntervals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketPaymentIntervals);
				_ticketPaymentIntervals.SuppressClearInGetMulti=!forceFetch;
				_ticketPaymentIntervals.EntityFactoryToUse = entityFactoryToUse;
				_ticketPaymentIntervals.GetMultiManyToOne(null, this, filter);
				_ticketPaymentIntervals.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketPaymentIntervals = true;
			}
			return _ticketPaymentIntervals;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketPaymentIntervals'. These settings will be taken into account
		/// when the property TicketPaymentIntervals is requested or GetMultiTicketPaymentIntervals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketPaymentIntervals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketPaymentIntervals.SortClauses=sortClauses;
			_ticketPaymentIntervals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketPhysicalCardTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketPhysicalCardTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection GetMultiTicketPhysicalCardTypes(bool forceFetch)
		{
			return GetMultiTicketPhysicalCardTypes(forceFetch, _ticketPhysicalCardTypes.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketPhysicalCardTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketPhysicalCardTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection GetMultiTicketPhysicalCardTypes(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketPhysicalCardTypes(forceFetch, _ticketPhysicalCardTypes.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketPhysicalCardTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection GetMultiTicketPhysicalCardTypes(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketPhysicalCardTypes(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketPhysicalCardTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection GetMultiTicketPhysicalCardTypes(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketPhysicalCardTypes || forceFetch || _alwaysFetchTicketPhysicalCardTypes) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketPhysicalCardTypes);
				_ticketPhysicalCardTypes.SuppressClearInGetMulti=!forceFetch;
				_ticketPhysicalCardTypes.EntityFactoryToUse = entityFactoryToUse;
				_ticketPhysicalCardTypes.GetMultiManyToOne(null, this, filter);
				_ticketPhysicalCardTypes.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketPhysicalCardTypes = true;
			}
			return _ticketPhysicalCardTypes;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketPhysicalCardTypes'. These settings will be taken into account
		/// when the property TicketPhysicalCardTypes is requested or GetMultiTicketPhysicalCardTypes is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketPhysicalCardTypes(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketPhysicalCardTypes.SortClauses=sortClauses;
			_ticketPhysicalCardTypes.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketServicesPermittedEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketServicesPermittedEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection GetMultiTicketServicesPermitteds(bool forceFetch)
		{
			return GetMultiTicketServicesPermitteds(forceFetch, _ticketServicesPermitteds.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketServicesPermittedEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketServicesPermittedEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection GetMultiTicketServicesPermitteds(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketServicesPermitteds(forceFetch, _ticketServicesPermitteds.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketServicesPermittedEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection GetMultiTicketServicesPermitteds(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketServicesPermitteds(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketServicesPermittedEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection GetMultiTicketServicesPermitteds(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketServicesPermitteds || forceFetch || _alwaysFetchTicketServicesPermitteds) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketServicesPermitteds);
				_ticketServicesPermitteds.SuppressClearInGetMulti=!forceFetch;
				_ticketServicesPermitteds.EntityFactoryToUse = entityFactoryToUse;
				_ticketServicesPermitteds.GetMultiManyToOne(null, this, filter);
				_ticketServicesPermitteds.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketServicesPermitteds = true;
			}
			return _ticketServicesPermitteds;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketServicesPermitteds'. These settings will be taken into account
		/// when the property TicketServicesPermitteds is requested or GetMultiTicketServicesPermitteds is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketServicesPermitteds(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketServicesPermitteds.SortClauses=sortClauses;
			_ticketServicesPermitteds.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketToGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketToGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketToGroupCollection GetMultiTicketToGroups(bool forceFetch)
		{
			return GetMultiTicketToGroups(forceFetch, _ticketToGroups.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketToGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketToGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketToGroupCollection GetMultiTicketToGroups(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketToGroups(forceFetch, _ticketToGroups.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketToGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketToGroupCollection GetMultiTicketToGroups(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketToGroups(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketToGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketToGroupCollection GetMultiTicketToGroups(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketToGroups || forceFetch || _alwaysFetchTicketToGroups) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketToGroups);
				_ticketToGroups.SuppressClearInGetMulti=!forceFetch;
				_ticketToGroups.EntityFactoryToUse = entityFactoryToUse;
				_ticketToGroups.GetMultiManyToOne(this, null, filter);
				_ticketToGroups.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketToGroups = true;
			}
			return _ticketToGroups;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketToGroups'. These settings will be taken into account
		/// when the property TicketToGroups is requested or GetMultiTicketToGroups is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketToGroups(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketToGroups.SortClauses=sortClauses;
			_ticketToGroups.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketVendingClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketVendingClientEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketVendingClientCollection GetMultiTicketVendingClient(bool forceFetch)
		{
			return GetMultiTicketVendingClient(forceFetch, _ticketVendingClient.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketVendingClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketVendingClientEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketVendingClientCollection GetMultiTicketVendingClient(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketVendingClient(forceFetch, _ticketVendingClient.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketVendingClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketVendingClientCollection GetMultiTicketVendingClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketVendingClient(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketVendingClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketVendingClientCollection GetMultiTicketVendingClient(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketVendingClient || forceFetch || _alwaysFetchTicketVendingClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketVendingClient);
				_ticketVendingClient.SuppressClearInGetMulti=!forceFetch;
				_ticketVendingClient.EntityFactoryToUse = entityFactoryToUse;
				_ticketVendingClient.GetMultiManyToOne(null, this, filter);
				_ticketVendingClient.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketVendingClient = true;
			}
			return _ticketVendingClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketVendingClient'. These settings will be taken into account
		/// when the property TicketVendingClient is requested or GetMultiTicketVendingClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketVendingClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketVendingClient.SortClauses=sortClauses;
			_ticketVendingClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvKeySetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvKeySetCollection GetMultiVdvKeySet(bool forceFetch)
		{
			return GetMultiVdvKeySet(forceFetch, _vdvKeySet.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvKeySetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvKeySetCollection GetMultiVdvKeySet(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvKeySet(forceFetch, _vdvKeySet.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvKeySetCollection GetMultiVdvKeySet(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvKeySet(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvKeySetCollection GetMultiVdvKeySet(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvKeySet || forceFetch || _alwaysFetchVdvKeySet) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvKeySet);
				_vdvKeySet.SuppressClearInGetMulti=!forceFetch;
				_vdvKeySet.EntityFactoryToUse = entityFactoryToUse;
				_vdvKeySet.GetMultiManyToOne(null, null, this, filter);
				_vdvKeySet.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvKeySet = true;
			}
			return _vdvKeySet;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvKeySet'. These settings will be taken into account
		/// when the property VdvKeySet is requested or GetMultiVdvKeySet is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvKeySet(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvKeySet.SortClauses=sortClauses;
			_vdvKeySet.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch)
		{
			return GetMultiVdvProduct(forceFetch, _vdvProduct.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvProduct(forceFetch, _vdvProduct.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvProduct(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvProduct || forceFetch || _alwaysFetchVdvProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvProduct);
				_vdvProduct.SuppressClearInGetMulti=!forceFetch;
				_vdvProduct.EntityFactoryToUse = entityFactoryToUse;
				_vdvProduct.GetMultiManyToOne(null, null, null, this, null, null, null, null, filter);
				_vdvProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvProduct = true;
			}
			return _vdvProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvProduct'. These settings will be taken into account
		/// when the property VdvProduct is requested or GetMultiVdvProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvProduct.SortClauses=sortClauses;
			_vdvProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiTaxProducts(bool forceFetch)
		{
			return GetMultiTaxProducts(forceFetch, _taxProducts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiTaxProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTaxProducts(forceFetch, _taxProducts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiTaxProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTaxProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection GetMultiTaxProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTaxProducts || forceFetch || _alwaysFetchTaxProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_taxProducts);
				_taxProducts.SuppressClearInGetMulti=!forceFetch;
				_taxProducts.EntityFactoryToUse = entityFactoryToUse;
				_taxProducts.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, null, filter);
				_taxProducts.SuppressClearInGetMulti=false;
				_alreadyFetchedTaxProducts = true;
			}
			return _taxProducts;
		}

		/// <summary> Sets the collection parameters for the collection for 'TaxProducts'. These settings will be taken into account
		/// when the property TaxProducts is requested or GetMultiTaxProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTaxProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_taxProducts.SortClauses=sortClauses;
			_taxProducts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProducts || forceFetch || _alwaysFetchProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_products);
				_products.SuppressClearInGetMulti=!forceFetch;
				_products.EntityFactoryToUse = entityFactoryToUse;
				_products.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, filter);
				_products.SuppressClearInGetMulti=false;
				_alreadyFetchedProducts = true;
			}
			return _products;
		}

		/// <summary> Sets the collection parameters for the collection for 'Products'. These settings will be taken into account
		/// when the property Products is requested or GetMultiProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_products.SortClauses=sortClauses;
			_products.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQuerySettingToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection GetMultiSettlementQuerySettingToTickets(bool forceFetch)
		{
			return GetMultiSettlementQuerySettingToTickets(forceFetch, _settlementQuerySettingToTickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQuerySettingToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection GetMultiSettlementQuerySettingToTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlementQuerySettingToTickets(forceFetch, _settlementQuerySettingToTickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection GetMultiSettlementQuerySettingToTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlementQuerySettingToTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection GetMultiSettlementQuerySettingToTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlementQuerySettingToTickets || forceFetch || _alwaysFetchSettlementQuerySettingToTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementQuerySettingToTickets);
				_settlementQuerySettingToTickets.SuppressClearInGetMulti=!forceFetch;
				_settlementQuerySettingToTickets.EntityFactoryToUse = entityFactoryToUse;
				_settlementQuerySettingToTickets.GetMultiManyToOne(this, null, filter);
				_settlementQuerySettingToTickets.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementQuerySettingToTickets = true;
			}
			return _settlementQuerySettingToTickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementQuerySettingToTickets'. These settings will be taken into account
		/// when the property SettlementQuerySettingToTickets is requested or GetMultiSettlementQuerySettingToTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementQuerySettingToTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementQuerySettingToTickets.SortClauses=sortClauses;
			_settlementQuerySettingToTickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketSerialNumberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection GetMultiTicketSerialNumbers(bool forceFetch)
		{
			return GetMultiTicketSerialNumbers(forceFetch, _ticketSerialNumbers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketSerialNumberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection GetMultiTicketSerialNumbers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketSerialNumbers(forceFetch, _ticketSerialNumbers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection GetMultiTicketSerialNumbers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketSerialNumbers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection GetMultiTicketSerialNumbers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketSerialNumbers || forceFetch || _alwaysFetchTicketSerialNumbers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketSerialNumbers);
				_ticketSerialNumbers.SuppressClearInGetMulti=!forceFetch;
				_ticketSerialNumbers.EntityFactoryToUse = entityFactoryToUse;
				_ticketSerialNumbers.GetMultiManyToOne(this, null, null, filter);
				_ticketSerialNumbers.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketSerialNumbers = true;
			}
			return _ticketSerialNumbers;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketSerialNumbers'. These settings will be taken into account
		/// when the property TicketSerialNumbers is requested or GetMultiTicketSerialNumbers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketSerialNumbers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketSerialNumbers.SortClauses=sortClauses;
			_ticketSerialNumbers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournalsForAppliedPassTicket(bool forceFetch)
		{
			return GetMultiTransactionJournalsForAppliedPassTicket(forceFetch, _transactionJournalsForAppliedPassTicket.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournalsForAppliedPassTicket(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournalsForAppliedPassTicket(forceFetch, _transactionJournalsForAppliedPassTicket.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournalsForAppliedPassTicket(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournalsForAppliedPassTicket(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournalsForAppliedPassTicket(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournalsForAppliedPassTicket || forceFetch || _alwaysFetchTransactionJournalsForAppliedPassTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournalsForAppliedPassTicket);
				_transactionJournalsForAppliedPassTicket.SuppressClearInGetMulti=!forceFetch;
				_transactionJournalsForAppliedPassTicket.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournalsForAppliedPassTicket.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, null, null, null, null, null, filter);
				_transactionJournalsForAppliedPassTicket.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournalsForAppliedPassTicket = true;
			}
			return _transactionJournalsForAppliedPassTicket;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournalsForAppliedPassTicket'. These settings will be taken into account
		/// when the property TransactionJournalsForAppliedPassTicket is requested or GetMultiTransactionJournalsForAppliedPassTicket is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournalsForAppliedPassTicket(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournalsForAppliedPassTicket.SortClauses=sortClauses;
			_transactionJournalsForAppliedPassTicket.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournals || forceFetch || _alwaysFetchTransactionJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournals);
				_transactionJournals.SuppressClearInGetMulti=!forceFetch;
				_transactionJournals.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournals.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, null, null, null, null, filter);
				_transactionJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournals = true;
			}
			return _transactionJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournals'. These settings will be taken into account
		/// when the property TransactionJournals is requested or GetMultiTransactionJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournals.SortClauses=sortClauses;
			_transactionJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals_(bool forceFetch)
		{
			return GetMultiTransactionJournals_(forceFetch, _transactionJournals_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournals_(forceFetch, _transactionJournals_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournals_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournals_ || forceFetch || _alwaysFetchTransactionJournals_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournals_);
				_transactionJournals_.SuppressClearInGetMulti=!forceFetch;
				_transactionJournals_.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournals_.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, null, null, null, filter);
				_transactionJournals_.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournals_ = true;
			}
			return _transactionJournals_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournals_'. These settings will be taken into account
		/// when the property TransactionJournals_ is requested or GetMultiTransactionJournals_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournals_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournals_.SortClauses=sortClauses;
			_transactionJournals_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VoucherEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VoucherEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VoucherCollection GetMultiVouchers(bool forceFetch)
		{
			return GetMultiVouchers(forceFetch, _vouchers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VoucherEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VoucherEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VoucherCollection GetMultiVouchers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVouchers(forceFetch, _vouchers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VoucherEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VoucherCollection GetMultiVouchers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVouchers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VoucherEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VoucherCollection GetMultiVouchers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVouchers || forceFetch || _alwaysFetchVouchers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vouchers);
				_vouchers.SuppressClearInGetMulti=!forceFetch;
				_vouchers.EntityFactoryToUse = entityFactoryToUse;
				_vouchers.GetMultiManyToOne(this, null, filter);
				_vouchers.SuppressClearInGetMulti=false;
				_alreadyFetchedVouchers = true;
			}
			return _vouchers;
		}

		/// <summary> Sets the collection parameters for the collection for 'Vouchers'. These settings will be taken into account
		/// when the property Vouchers is requested or GetMultiVouchers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVouchers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vouchers.SortClauses=sortClauses;
			_vouchers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributevalueCollectionViaAttributeValueList(bool forceFetch)
		{
			return GetMultiAttributevalueCollectionViaAttributeValueList(forceFetch, _attributevalueCollectionViaAttributeValueList.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributevalueCollectionViaAttributeValueList(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAttributevalueCollectionViaAttributeValueList || forceFetch || _alwaysFetchAttributevalueCollectionViaAttributeValueList) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributevalueCollectionViaAttributeValueList);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketFields.TicketID, ComparisonOperator.Equal, this.TicketID, "TicketEntity__"));
				_attributevalueCollectionViaAttributeValueList.SuppressClearInGetMulti=!forceFetch;
				_attributevalueCollectionViaAttributeValueList.EntityFactoryToUse = entityFactoryToUse;
				_attributevalueCollectionViaAttributeValueList.GetMulti(filter, GetRelationsForField("AttributevalueCollectionViaAttributeValueList"));
				_attributevalueCollectionViaAttributeValueList.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributevalueCollectionViaAttributeValueList = true;
			}
			return _attributevalueCollectionViaAttributeValueList;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttributevalueCollectionViaAttributeValueList'. These settings will be taken into account
		/// when the property AttributevalueCollectionViaAttributeValueList is requested or GetMultiAttributevalueCollectionViaAttributeValueList is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributevalueCollectionViaAttributeValueList(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributevalueCollectionViaAttributeValueList.SortClauses=sortClauses;
			_attributevalueCollectionViaAttributeValueList.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardChipTypeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardChipTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardChipTypeCollection GetMultiCardChipTypeCollectionViaTicketPhysicalCardType(bool forceFetch)
		{
			return GetMultiCardChipTypeCollectionViaTicketPhysicalCardType(forceFetch, _cardChipTypeCollectionViaTicketPhysicalCardType.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CardChipTypeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardChipTypeCollection GetMultiCardChipTypeCollectionViaTicketPhysicalCardType(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType || forceFetch || _alwaysFetchCardChipTypeCollectionViaTicketPhysicalCardType) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardChipTypeCollectionViaTicketPhysicalCardType);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketFields.TicketID, ComparisonOperator.Equal, this.TicketID, "TicketEntity__"));
				_cardChipTypeCollectionViaTicketPhysicalCardType.SuppressClearInGetMulti=!forceFetch;
				_cardChipTypeCollectionViaTicketPhysicalCardType.EntityFactoryToUse = entityFactoryToUse;
				_cardChipTypeCollectionViaTicketPhysicalCardType.GetMulti(filter, GetRelationsForField("CardChipTypeCollectionViaTicketPhysicalCardType"));
				_cardChipTypeCollectionViaTicketPhysicalCardType.SuppressClearInGetMulti=false;
				_alreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType = true;
			}
			return _cardChipTypeCollectionViaTicketPhysicalCardType;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardChipTypeCollectionViaTicketPhysicalCardType'. These settings will be taken into account
		/// when the property CardChipTypeCollectionViaTicketPhysicalCardType is requested or GetMultiCardChipTypeCollectionViaTicketPhysicalCardType is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardChipTypeCollectionViaTicketPhysicalCardType(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardChipTypeCollectionViaTicketPhysicalCardType.SortClauses=sortClauses;
			_cardChipTypeCollectionViaTicketPhysicalCardType.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardAdditionsTicketIsUsedIn(bool forceFetch)
		{
			return GetMultiCardAdditionsTicketIsUsedIn(forceFetch, _cardAdditionsTicketIsUsedIn.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardAdditionsTicketIsUsedIn(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCardAdditionsTicketIsUsedIn || forceFetch || _alwaysFetchCardAdditionsTicketIsUsedIn) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardAdditionsTicketIsUsedIn);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketFields.TicketID, ComparisonOperator.Equal, this.TicketID, "TicketEntity__"));
				_cardAdditionsTicketIsUsedIn.SuppressClearInGetMulti=!forceFetch;
				_cardAdditionsTicketIsUsedIn.EntityFactoryToUse = entityFactoryToUse;
				_cardAdditionsTicketIsUsedIn.GetMulti(filter, GetRelationsForField("CardAdditionsTicketIsUsedIn"));
				_cardAdditionsTicketIsUsedIn.SuppressClearInGetMulti=false;
				_alreadyFetchedCardAdditionsTicketIsUsedIn = true;
			}
			return _cardAdditionsTicketIsUsedIn;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardAdditionsTicketIsUsedIn'. These settings will be taken into account
		/// when the property CardAdditionsTicketIsUsedIn is requested or GetMultiCardAdditionsTicketIsUsedIn is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardAdditionsTicketIsUsedIn(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardAdditionsTicketIsUsedIn.SortClauses=sortClauses;
			_cardAdditionsTicketIsUsedIn.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClientCollection GetMultiClientCollectionViaTicketVendingClient(bool forceFetch)
		{
			return GetMultiClientCollectionViaTicketVendingClient(forceFetch, _clientCollectionViaTicketVendingClient.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClientCollection GetMultiClientCollectionViaTicketVendingClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedClientCollectionViaTicketVendingClient || forceFetch || _alwaysFetchClientCollectionViaTicketVendingClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollectionViaTicketVendingClient);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketFields.TicketID, ComparisonOperator.Equal, this.TicketID, "TicketEntity__"));
				_clientCollectionViaTicketVendingClient.SuppressClearInGetMulti=!forceFetch;
				_clientCollectionViaTicketVendingClient.EntityFactoryToUse = entityFactoryToUse;
				_clientCollectionViaTicketVendingClient.GetMulti(filter, GetRelationsForField("ClientCollectionViaTicketVendingClient"));
				_clientCollectionViaTicketVendingClient.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollectionViaTicketVendingClient = true;
			}
			return _clientCollectionViaTicketVendingClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollectionViaTicketVendingClient'. These settings will be taken into account
		/// when the property ClientCollectionViaTicketVendingClient is requested or GetMultiClientCollectionViaTicketVendingClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollectionViaTicketVendingClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollectionViaTicketVendingClient.SortClauses=sortClauses;
			_clientCollectionViaTicketVendingClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LineEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LineEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LineCollection GetMultiLineCollectionViaTicketServicesPermitted(bool forceFetch)
		{
			return GetMultiLineCollectionViaTicketServicesPermitted(forceFetch, _lineCollectionViaTicketServicesPermitted.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'LineEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.LineCollection GetMultiLineCollectionViaTicketServicesPermitted(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedLineCollectionViaTicketServicesPermitted || forceFetch || _alwaysFetchLineCollectionViaTicketServicesPermitted) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_lineCollectionViaTicketServicesPermitted);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketFields.TicketID, ComparisonOperator.Equal, this.TicketID, "TicketEntity__"));
				_lineCollectionViaTicketServicesPermitted.SuppressClearInGetMulti=!forceFetch;
				_lineCollectionViaTicketServicesPermitted.EntityFactoryToUse = entityFactoryToUse;
				_lineCollectionViaTicketServicesPermitted.GetMulti(filter, GetRelationsForField("LineCollectionViaTicketServicesPermitted"));
				_lineCollectionViaTicketServicesPermitted.SuppressClearInGetMulti=false;
				_alreadyFetchedLineCollectionViaTicketServicesPermitted = true;
			}
			return _lineCollectionViaTicketServicesPermitted;
		}

		/// <summary> Sets the collection parameters for the collection for 'LineCollectionViaTicketServicesPermitted'. These settings will be taken into account
		/// when the property LineCollectionViaTicketServicesPermitted is requested or GetMultiLineCollectionViaTicketServicesPermitted is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLineCollectionViaTicketServicesPermitted(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_lineCollectionViaTicketServicesPermitted.SortClauses=sortClauses;
			_lineCollectionViaTicketServicesPermitted.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentIntervalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentIntervalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentIntervalCollection GetMultiPaymentIntervalCollectionViaTicketPaymentInterval(bool forceFetch)
		{
			return GetMultiPaymentIntervalCollectionViaTicketPaymentInterval(forceFetch, _paymentIntervalCollectionViaTicketPaymentInterval.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PaymentIntervalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentIntervalCollection GetMultiPaymentIntervalCollectionViaTicketPaymentInterval(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval || forceFetch || _alwaysFetchPaymentIntervalCollectionViaTicketPaymentInterval) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentIntervalCollectionViaTicketPaymentInterval);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketFields.TicketID, ComparisonOperator.Equal, this.TicketID, "TicketEntity__"));
				_paymentIntervalCollectionViaTicketPaymentInterval.SuppressClearInGetMulti=!forceFetch;
				_paymentIntervalCollectionViaTicketPaymentInterval.EntityFactoryToUse = entityFactoryToUse;
				_paymentIntervalCollectionViaTicketPaymentInterval.GetMulti(filter, GetRelationsForField("PaymentIntervalCollectionViaTicketPaymentInterval"));
				_paymentIntervalCollectionViaTicketPaymentInterval.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval = true;
			}
			return _paymentIntervalCollectionViaTicketPaymentInterval;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentIntervalCollectionViaTicketPaymentInterval'. These settings will be taken into account
		/// when the property PaymentIntervalCollectionViaTicketPaymentInterval is requested or GetMultiPaymentIntervalCollectionViaTicketPaymentInterval is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentIntervalCollectionViaTicketPaymentInterval(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentIntervalCollectionViaTicketPaymentInterval.SortClauses=sortClauses;
			_paymentIntervalCollectionViaTicketPaymentInterval.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCappingCollectionViaRuleCappingToTicket(bool forceFetch)
		{
			return GetMultiRuleCappingCollectionViaRuleCappingToTicket(forceFetch, _ruleCappingCollectionViaRuleCappingToTicket.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCappingCollectionViaRuleCappingToTicket(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRuleCappingCollectionViaRuleCappingToTicket || forceFetch || _alwaysFetchRuleCappingCollectionViaRuleCappingToTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleCappingCollectionViaRuleCappingToTicket);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketFields.TicketID, ComparisonOperator.Equal, this.TicketID, "TicketEntity__"));
				_ruleCappingCollectionViaRuleCappingToTicket.SuppressClearInGetMulti=!forceFetch;
				_ruleCappingCollectionViaRuleCappingToTicket.EntityFactoryToUse = entityFactoryToUse;
				_ruleCappingCollectionViaRuleCappingToTicket.GetMulti(filter, GetRelationsForField("RuleCappingCollectionViaRuleCappingToTicket"));
				_ruleCappingCollectionViaRuleCappingToTicket.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleCappingCollectionViaRuleCappingToTicket = true;
			}
			return _ruleCappingCollectionViaRuleCappingToTicket;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleCappingCollectionViaRuleCappingToTicket'. These settings will be taken into account
		/// when the property RuleCappingCollectionViaRuleCappingToTicket is requested or GetMultiRuleCappingCollectionViaRuleCappingToTicket is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleCappingCollectionViaRuleCappingToTicket(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleCappingCollectionViaRuleCappingToTicket.SortClauses=sortClauses;
			_ruleCappingCollectionViaRuleCappingToTicket.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketGroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketGroupCollection GetMultiTicketGroupCollectionViaTicketToGroup(bool forceFetch)
		{
			return GetMultiTicketGroupCollectionViaTicketToGroup(forceFetch, _ticketGroupCollectionViaTicketToGroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketGroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketGroupCollection GetMultiTicketGroupCollectionViaTicketToGroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTicketGroupCollectionViaTicketToGroup || forceFetch || _alwaysFetchTicketGroupCollectionViaTicketToGroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketGroupCollectionViaTicketToGroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketFields.TicketID, ComparisonOperator.Equal, this.TicketID, "TicketEntity__"));
				_ticketGroupCollectionViaTicketToGroup.SuppressClearInGetMulti=!forceFetch;
				_ticketGroupCollectionViaTicketToGroup.EntityFactoryToUse = entityFactoryToUse;
				_ticketGroupCollectionViaTicketToGroup.GetMulti(filter, GetRelationsForField("TicketGroupCollectionViaTicketToGroup"));
				_ticketGroupCollectionViaTicketToGroup.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketGroupCollectionViaTicketToGroup = true;
			}
			return _ticketGroupCollectionViaTicketToGroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketGroupCollectionViaTicketToGroup'. These settings will be taken into account
		/// when the property TicketGroupCollectionViaTicketToGroup is requested or GetMultiTicketGroupCollectionViaTicketToGroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketGroupCollectionViaTicketToGroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketGroupCollectionViaTicketToGroup.SortClauses=sortClauses;
			_ticketGroupCollectionViaTicketToGroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationCollection GetMultiOrganizationCollectionViaTicketOrganization(bool forceFetch)
		{
			return GetMultiOrganizationCollectionViaTicketOrganization(forceFetch, _organizationCollectionViaTicketOrganization.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationCollection GetMultiOrganizationCollectionViaTicketOrganization(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrganizationCollectionViaTicketOrganization || forceFetch || _alwaysFetchOrganizationCollectionViaTicketOrganization) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_organizationCollectionViaTicketOrganization);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketFields.TicketID, ComparisonOperator.Equal, this.TicketID, "TicketEntity__"));
				_organizationCollectionViaTicketOrganization.SuppressClearInGetMulti=!forceFetch;
				_organizationCollectionViaTicketOrganization.EntityFactoryToUse = entityFactoryToUse;
				_organizationCollectionViaTicketOrganization.GetMulti(filter, GetRelationsForField("OrganizationCollectionViaTicketOrganization"));
				_organizationCollectionViaTicketOrganization.SuppressClearInGetMulti=false;
				_alreadyFetchedOrganizationCollectionViaTicketOrganization = true;
			}
			return _organizationCollectionViaTicketOrganization;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrganizationCollectionViaTicketOrganization'. These settings will be taken into account
		/// when the property OrganizationCollectionViaTicketOrganization is requested or GetMultiOrganizationCollectionViaTicketOrganization is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrganizationCollectionViaTicketOrganization(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_organizationCollectionViaTicketOrganization.SortClauses=sortClauses;
			_organizationCollectionViaTicketOrganization.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQuerySettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection GetMultiSettlementQuerySettings(bool forceFetch)
		{
			return GetMultiSettlementQuerySettings(forceFetch, _settlementQuerySettings.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection GetMultiSettlementQuerySettings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSettlementQuerySettings || forceFetch || _alwaysFetchSettlementQuerySettings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementQuerySettings);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketFields.TicketID, ComparisonOperator.Equal, this.TicketID, "TicketEntity__"));
				_settlementQuerySettings.SuppressClearInGetMulti=!forceFetch;
				_settlementQuerySettings.EntityFactoryToUse = entityFactoryToUse;
				_settlementQuerySettings.GetMulti(filter, GetRelationsForField("SettlementQuerySettings"));
				_settlementQuerySettings.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementQuerySettings = true;
			}
			return _settlementQuerySettings;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementQuerySettings'. These settings will be taken into account
		/// when the property SettlementQuerySettings is requested or GetMultiSettlementQuerySettings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementQuerySettings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementQuerySettings.SortClauses=sortClauses;
			_settlementQuerySettings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BusinessRuleEntity' which is related to this entity.</returns>
		public BusinessRuleEntity GetSingleBusinessRuleInspection()
		{
			return GetSingleBusinessRuleInspection(false);
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BusinessRuleEntity' which is related to this entity.</returns>
		public virtual BusinessRuleEntity GetSingleBusinessRuleInspection(bool forceFetch)
		{
			if( ( !_alreadyFetchedBusinessRuleInspection || forceFetch || _alwaysFetchBusinessRuleInspection) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BusinessRuleEntityUsingBrType10ID);
				BusinessRuleEntity newEntity = new BusinessRuleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BrType10ID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BusinessRuleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_businessRuleInspectionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BusinessRuleInspection = newEntity;
				_alreadyFetchedBusinessRuleInspection = fetchResult;
			}
			return _businessRuleInspection;
		}


		/// <summary> Retrieves the related entity of type 'BusinessRuleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BusinessRuleEntity' which is related to this entity.</returns>
		public BusinessRuleEntity GetSingleBusinessRuleTestBoarding()
		{
			return GetSingleBusinessRuleTestBoarding(false);
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BusinessRuleEntity' which is related to this entity.</returns>
		public virtual BusinessRuleEntity GetSingleBusinessRuleTestBoarding(bool forceFetch)
		{
			if( ( !_alreadyFetchedBusinessRuleTestBoarding || forceFetch || _alwaysFetchBusinessRuleTestBoarding) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BusinessRuleEntityUsingBrType2ID);
				BusinessRuleEntity newEntity = new BusinessRuleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BrType2ID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BusinessRuleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_businessRuleTestBoardingReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BusinessRuleTestBoarding = newEntity;
				_alreadyFetchedBusinessRuleTestBoarding = fetchResult;
			}
			return _businessRuleTestBoarding;
		}


		/// <summary> Retrieves the related entity of type 'BusinessRuleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BusinessRuleEntity' which is related to this entity.</returns>
		public BusinessRuleEntity GetSingleBusinessRule()
		{
			return GetSingleBusinessRule(false);
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BusinessRuleEntity' which is related to this entity.</returns>
		public virtual BusinessRuleEntity GetSingleBusinessRule(bool forceFetch)
		{
			if( ( !_alreadyFetchedBusinessRule || forceFetch || _alwaysFetchBusinessRule) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BusinessRuleEntityUsingBrType1ID);
				BusinessRuleEntity newEntity = new BusinessRuleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BrType1ID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BusinessRuleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_businessRuleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BusinessRule = newEntity;
				_alreadyFetchedBusinessRule = fetchResult;
			}
			return _businessRule;
		}


		/// <summary> Retrieves the related entity of type 'BusinessRuleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BusinessRuleEntity' which is related to this entity.</returns>
		public BusinessRuleEntity GetSingleBusinessRuleTicketAssignment()
		{
			return GetSingleBusinessRuleTicketAssignment(false);
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BusinessRuleEntity' which is related to this entity.</returns>
		public virtual BusinessRuleEntity GetSingleBusinessRuleTicketAssignment(bool forceFetch)
		{
			if( ( !_alreadyFetchedBusinessRuleTicketAssignment || forceFetch || _alwaysFetchBusinessRuleTicketAssignment) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BusinessRuleEntityUsingBrType9ID);
				BusinessRuleEntity newEntity = new BusinessRuleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BrType9ID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BusinessRuleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_businessRuleTicketAssignmentReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BusinessRuleTicketAssignment = newEntity;
				_alreadyFetchedBusinessRuleTicketAssignment = fetchResult;
			}
			return _businessRuleTicketAssignment;
		}


		/// <summary> Retrieves the related entity of type 'CalendarEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CalendarEntity' which is related to this entity.</returns>
		public CalendarEntity GetSingleCalendar()
		{
			return GetSingleCalendar(false);
		}

		/// <summary> Retrieves the related entity of type 'CalendarEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CalendarEntity' which is related to this entity.</returns>
		public virtual CalendarEntity GetSingleCalendar(bool forceFetch)
		{
			if( ( !_alreadyFetchedCalendar || forceFetch || _alwaysFetchCalendar) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CalendarEntityUsingCalendarID);
				CalendarEntity newEntity = new CalendarEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CalendarID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CalendarEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_calendarReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Calendar = newEntity;
				_alreadyFetchedCalendar = fetchResult;
			}
			return _calendar;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingOwnerClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OwnerClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public DeviceClassEntity GetSingleDeviceClass()
		{
			return GetSingleDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public virtual DeviceClassEntity GetSingleDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceClass || forceFetch || _alwaysFetchDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceClassEntityUsingDeviceClassID);
				DeviceClassEntity newEntity = new DeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceClassID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceClass = newEntity;
				_alreadyFetchedDeviceClass = fetchResult;
			}
			return _deviceClass;
		}


		/// <summary> Retrieves the related entity of type 'EticketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EticketEntity' which is related to this entity.</returns>
		public EticketEntity GetSingleEticket()
		{
			return GetSingleEticket(false);
		}

		/// <summary> Retrieves the related entity of type 'EticketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EticketEntity' which is related to this entity.</returns>
		public virtual EticketEntity GetSingleEticket(bool forceFetch)
		{
			if( ( !_alreadyFetchedEticket || forceFetch || _alwaysFetchEticket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EticketEntityUsingEtickID);
				EticketEntity newEntity = new EticketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EtickID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EticketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_eticketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Eticket = newEntity;
				_alreadyFetchedEticket = fetchResult;
			}
			return _eticket;
		}


		/// <summary> Retrieves the related entity of type 'FareEvasionCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareEvasionCategoryEntity' which is related to this entity.</returns>
		public FareEvasionCategoryEntity GetSingleFareEvasionCategory()
		{
			return GetSingleFareEvasionCategory(false);
		}

		/// <summary> Retrieves the related entity of type 'FareEvasionCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareEvasionCategoryEntity' which is related to this entity.</returns>
		public virtual FareEvasionCategoryEntity GetSingleFareEvasionCategory(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareEvasionCategory || forceFetch || _alwaysFetchFareEvasionCategory) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareEvasionCategoryEntityUsingFareEvasionCategoryID);
				FareEvasionCategoryEntity newEntity = new FareEvasionCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareEvasionCategoryID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareEvasionCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareEvasionCategoryReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareEvasionCategory = newEntity;
				_alreadyFetchedFareEvasionCategory = fetchResult;
			}
			return _fareEvasionCategory;
		}


		/// <summary> Retrieves the related entity of type 'FareMatrixEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareMatrixEntity' which is related to this entity.</returns>
		public FareMatrixEntity GetSingleFareMatrix()
		{
			return GetSingleFareMatrix(false);
		}

		/// <summary> Retrieves the related entity of type 'FareMatrixEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareMatrixEntity' which is related to this entity.</returns>
		public virtual FareMatrixEntity GetSingleFareMatrix(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareMatrix || forceFetch || _alwaysFetchFareMatrix) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareMatrixEntityUsingMatrixID);
				FareMatrixEntity newEntity = new FareMatrixEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MatrixID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareMatrixEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareMatrixReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareMatrix = newEntity;
				_alreadyFetchedFareMatrix = fetchResult;
			}
			return _fareMatrix;
		}


		/// <summary> Retrieves the related entity of type 'FareTableEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareTableEntity' which is related to this entity.</returns>
		public FareTableEntity GetSingleFareTable1()
		{
			return GetSingleFareTable1(false);
		}

		/// <summary> Retrieves the related entity of type 'FareTableEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareTableEntity' which is related to this entity.</returns>
		public virtual FareTableEntity GetSingleFareTable1(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareTable1 || forceFetch || _alwaysFetchFareTable1) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareTableEntityUsingFareTableID);
				FareTableEntity newEntity = new FareTableEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareTableID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareTableEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareTable1ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareTable1 = newEntity;
				_alreadyFetchedFareTable1 = fetchResult;
			}
			return _fareTable1;
		}


		/// <summary> Retrieves the related entity of type 'FareTableEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareTableEntity' which is related to this entity.</returns>
		public FareTableEntity GetSingleFareTable2()
		{
			return GetSingleFareTable2(false);
		}

		/// <summary> Retrieves the related entity of type 'FareTableEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareTableEntity' which is related to this entity.</returns>
		public virtual FareTableEntity GetSingleFareTable2(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareTable2 || forceFetch || _alwaysFetchFareTable2) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareTableEntityUsingFareTable2ID);
				FareTableEntity newEntity = new FareTableEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareTable2ID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareTableEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareTable2ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareTable2 = newEntity;
				_alreadyFetchedFareTable2 = fetchResult;
			}
			return _fareTable2;
		}


		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSingleCancellationLayout()
		{
			return GetSingleCancellationLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSingleCancellationLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedCancellationLayout || forceFetch || _alwaysFetchCancellationLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingCancellationLayoutID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CancellationLayoutID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cancellationLayoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CancellationLayout = newEntity;
				_alreadyFetchedCancellationLayout = fetchResult;
			}
			return _cancellationLayout;
		}


		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSingleGroupLayout()
		{
			return GetSingleGroupLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSingleGroupLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedGroupLayout || forceFetch || _alwaysFetchGroupLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingGroupLayoutID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GroupLayoutID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_groupLayoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GroupLayout = newEntity;
				_alreadyFetchedGroupLayout = fetchResult;
			}
			return _groupLayout;
		}


		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSinglePrintLayout()
		{
			return GetSinglePrintLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSinglePrintLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedPrintLayout || forceFetch || _alwaysFetchPrintLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingPrintLayoutID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PrintLayoutID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_printLayoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PrintLayout = newEntity;
				_alreadyFetchedPrintLayout = fetchResult;
			}
			return _printLayout;
		}


		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSingleReceiptLayout()
		{
			return GetSingleReceiptLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSingleReceiptLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedReceiptLayout || forceFetch || _alwaysFetchReceiptLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingReceiptLayoutID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReceiptLayoutID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_receiptLayoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReceiptLayout = newEntity;
				_alreadyFetchedReceiptLayout = fetchResult;
			}
			return _receiptLayout;
		}


		/// <summary> Retrieves the related entity of type 'LineGroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LineGroupEntity' which is related to this entity.</returns>
		public LineGroupEntity GetSingleLineGroup()
		{
			return GetSingleLineGroup(false);
		}

		/// <summary> Retrieves the related entity of type 'LineGroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LineGroupEntity' which is related to this entity.</returns>
		public virtual LineGroupEntity GetSingleLineGroup(bool forceFetch)
		{
			if( ( !_alreadyFetchedLineGroup || forceFetch || _alwaysFetchLineGroup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LineGroupEntityUsingLineGroupID);
				LineGroupEntity newEntity = new LineGroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LineGroupID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LineGroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_lineGroupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.LineGroup = newEntity;
				_alreadyFetchedLineGroup = fetchResult;
			}
			return _lineGroup;
		}


		/// <summary> Retrieves the related entity of type 'PageContentGroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageContentGroupEntity' which is related to this entity.</returns>
		public PageContentGroupEntity GetSinglePageContentGroup()
		{
			return GetSinglePageContentGroup(false);
		}

		/// <summary> Retrieves the related entity of type 'PageContentGroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageContentGroupEntity' which is related to this entity.</returns>
		public virtual PageContentGroupEntity GetSinglePageContentGroup(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageContentGroup || forceFetch || _alwaysFetchPageContentGroup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageContentGroupEntityUsingPageContentGroupID);
				PageContentGroupEntity newEntity = new PageContentGroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageContentGroupID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageContentGroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageContentGroupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageContentGroup = newEntity;
				_alreadyFetchedPageContentGroup = fetchResult;
			}
			return _pageContentGroup;
		}


		/// <summary> Retrieves the related entity of type 'PriceTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PriceTypeEntity' which is related to this entity.</returns>
		public PriceTypeEntity GetSinglePriceType()
		{
			return GetSinglePriceType(false);
		}

		/// <summary> Retrieves the related entity of type 'PriceTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PriceTypeEntity' which is related to this entity.</returns>
		public virtual PriceTypeEntity GetSinglePriceType(bool forceFetch)
		{
			if( ( !_alreadyFetchedPriceType || forceFetch || _alwaysFetchPriceType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PriceTypeEntityUsingPriceTypeID);
				PriceTypeEntity newEntity = new PriceTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PriceTypeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PriceTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_priceTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PriceType = newEntity;
				_alreadyFetchedPriceType = fetchResult;
			}
			return _priceType;
		}


		/// <summary> Retrieves the related entity of type 'RulePeriodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RulePeriodEntity' which is related to this entity.</returns>
		public RulePeriodEntity GetSingleRulePeriod()
		{
			return GetSingleRulePeriod(false);
		}

		/// <summary> Retrieves the related entity of type 'RulePeriodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RulePeriodEntity' which is related to this entity.</returns>
		public virtual RulePeriodEntity GetSingleRulePeriod(bool forceFetch)
		{
			if( ( !_alreadyFetchedRulePeriod || forceFetch || _alwaysFetchRulePeriod) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RulePeriodEntityUsingRulePeriodID);
				RulePeriodEntity newEntity = new RulePeriodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RulePeriodID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RulePeriodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_rulePeriodReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RulePeriod = newEntity;
				_alreadyFetchedRulePeriod = fetchResult;
			}
			return _rulePeriod;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTarifID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TarifID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary> Retrieves the related entity of type 'TemporalTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TemporalTypeEntity' which is related to this entity.</returns>
		public TemporalTypeEntity GetSingleTemporalType()
		{
			return GetSingleTemporalType(false);
		}

		/// <summary> Retrieves the related entity of type 'TemporalTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TemporalTypeEntity' which is related to this entity.</returns>
		public virtual TemporalTypeEntity GetSingleTemporalType(bool forceFetch)
		{
			if( ( !_alreadyFetchedTemporalType || forceFetch || _alwaysFetchTemporalType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TemporalTypeEntityUsingTemporalTypeID);
				TemporalTypeEntity newEntity = new TemporalTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TemporalTypeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TemporalTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_temporalTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TemporalType = newEntity;
				_alreadyFetchedTemporalType = fetchResult;
			}
			return _temporalType;
		}


		/// <summary> Retrieves the related entity of type 'TicketCancellationTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketCancellationTypeEntity' which is related to this entity.</returns>
		public TicketCancellationTypeEntity GetSingleTicketCancellationType()
		{
			return GetSingleTicketCancellationType(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketCancellationTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketCancellationTypeEntity' which is related to this entity.</returns>
		public virtual TicketCancellationTypeEntity GetSingleTicketCancellationType(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicketCancellationType || forceFetch || _alwaysFetchTicketCancellationType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketCancellationTypeEntityUsingTicketCancellationTypeID);
				TicketCancellationTypeEntity newEntity = new TicketCancellationTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketCancellationTypeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketCancellationTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketCancellationTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TicketCancellationType = newEntity;
				_alreadyFetchedTicketCancellationType = fetchResult;
			}
			return _ticketCancellationType;
		}


		/// <summary> Retrieves the related entity of type 'TicketCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketCategoryEntity' which is related to this entity.</returns>
		public TicketCategoryEntity GetSingleTicketCategory()
		{
			return GetSingleTicketCategory(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketCategoryEntity' which is related to this entity.</returns>
		public virtual TicketCategoryEntity GetSingleTicketCategory(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicketCategory || forceFetch || _alwaysFetchTicketCategory) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketCategoryEntityUsingCategoryID);
				TicketCategoryEntity newEntity = new TicketCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CategoryID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketCategoryReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TicketCategory = newEntity;
				_alreadyFetchedTicketCategory = fetchResult;
			}
			return _ticketCategory;
		}


		/// <summary> Retrieves the related entity of type 'TicketSelectionModeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketSelectionModeEntity' which is related to this entity.</returns>
		public TicketSelectionModeEntity GetSingleTicketSelectionMode()
		{
			return GetSingleTicketSelectionMode(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketSelectionModeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketSelectionModeEntity' which is related to this entity.</returns>
		public virtual TicketSelectionModeEntity GetSingleTicketSelectionMode(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicketSelectionMode || forceFetch || _alwaysFetchTicketSelectionMode) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketSelectionModeEntityUsingSelectionMode);
				TicketSelectionModeEntity newEntity = new TicketSelectionModeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SelectionMode.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketSelectionModeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketSelectionModeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TicketSelectionMode = newEntity;
				_alreadyFetchedTicketSelectionMode = fetchResult;
			}
			return _ticketSelectionMode;
		}


		/// <summary> Retrieves the related entity of type 'TicketTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketTypeEntity' which is related to this entity.</returns>
		public TicketTypeEntity GetSingleTicketType()
		{
			return GetSingleTicketType(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketTypeEntity' which is related to this entity.</returns>
		public virtual TicketTypeEntity GetSingleTicketType(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicketType || forceFetch || _alwaysFetchTicketType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketTypeEntityUsingTicketTypeID);
				TicketTypeEntity newEntity = new TicketTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketTypeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TicketType = newEntity;
				_alreadyFetchedTicketType = fetchResult;
			}
			return _ticketType;
		}


		/// <summary> Retrieves the related entity of type 'UserKeyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserKeyEntity' which is related to this entity.</returns>
		public UserKeyEntity GetSingleUserKey1()
		{
			return GetSingleUserKey1(false);
		}

		/// <summary> Retrieves the related entity of type 'UserKeyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserKeyEntity' which is related to this entity.</returns>
		public virtual UserKeyEntity GetSingleUserKey1(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserKey1 || forceFetch || _alwaysFetchUserKey1) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserKeyEntityUsingKey1);
				UserKeyEntity newEntity = new UserKeyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Key1.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserKeyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userKey1ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserKey1 = newEntity;
				_alreadyFetchedUserKey1 = fetchResult;
			}
			return _userKey1;
		}


		/// <summary> Retrieves the related entity of type 'UserKeyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserKeyEntity' which is related to this entity.</returns>
		public UserKeyEntity GetSingleUserKey2()
		{
			return GetSingleUserKey2(false);
		}

		/// <summary> Retrieves the related entity of type 'UserKeyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserKeyEntity' which is related to this entity.</returns>
		public virtual UserKeyEntity GetSingleUserKey2(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserKey2 || forceFetch || _alwaysFetchUserKey2) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserKeyEntityUsingKey2);
				UserKeyEntity newEntity = new UserKeyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Key2.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserKeyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userKey2ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserKey2 = newEntity;
				_alreadyFetchedUserKey2 = fetchResult;
			}
			return _userKey2;
		}

		/// <summary> Retrieves the related entity of type 'CardTicketEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'CardTicketEntity' which is related to this entity.</returns>
		public CardTicketEntity GetSingleCardTicketAddition()
		{
			return GetSingleCardTicketAddition(false);
		}
		
		/// <summary> Retrieves the related entity of type 'CardTicketEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardTicketEntity' which is related to this entity.</returns>
		public virtual CardTicketEntity GetSingleCardTicketAddition(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardTicketAddition || forceFetch || _alwaysFetchCardTicketAddition) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardTicketEntityUsingTicketID);
				CardTicketEntity newEntity = new CardTicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCTicketID(this.TicketID);
				}
				if(fetchResult)
				{
					newEntity = (CardTicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardTicketAdditionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardTicketAddition = newEntity;
				_alreadyFetchedCardTicketAddition = fetchResult;
			}
			return _cardTicketAddition;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BusinessRuleInspection", _businessRuleInspection);
			toReturn.Add("BusinessRuleTestBoarding", _businessRuleTestBoarding);
			toReturn.Add("BusinessRule", _businessRule);
			toReturn.Add("BusinessRuleTicketAssignment", _businessRuleTicketAssignment);
			toReturn.Add("Calendar", _calendar);
			toReturn.Add("Client", _client);
			toReturn.Add("DeviceClass", _deviceClass);
			toReturn.Add("Eticket", _eticket);
			toReturn.Add("FareEvasionCategory", _fareEvasionCategory);
			toReturn.Add("FareMatrix", _fareMatrix);
			toReturn.Add("FareTable1", _fareTable1);
			toReturn.Add("FareTable2", _fareTable2);
			toReturn.Add("CancellationLayout", _cancellationLayout);
			toReturn.Add("GroupLayout", _groupLayout);
			toReturn.Add("PrintLayout", _printLayout);
			toReturn.Add("ReceiptLayout", _receiptLayout);
			toReturn.Add("LineGroup", _lineGroup);
			toReturn.Add("PageContentGroup", _pageContentGroup);
			toReturn.Add("PriceType", _priceType);
			toReturn.Add("RulePeriod", _rulePeriod);
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("TemporalType", _temporalType);
			toReturn.Add("TicketCancellationType", _ticketCancellationType);
			toReturn.Add("TicketCategory", _ticketCategory);
			toReturn.Add("TicketSelectionMode", _ticketSelectionMode);
			toReturn.Add("TicketType", _ticketType);
			toReturn.Add("UserKey1", _userKey1);
			toReturn.Add("UserKey2", _userKey2);
			toReturn.Add("ApportionmentResults", _apportionmentResults);
			toReturn.Add("AttributeValueList", _attributeValueList);
			toReturn.Add("CardTicketToTicket", _cardTicketToTicket);
			toReturn.Add("Choices", _choices);
			toReturn.Add("ParameterTicket", _parameterTicket);
			toReturn.Add("PrimalKeys", _primalKeys);
			toReturn.Add("RuleCappingToTicket", _ruleCappingToTicket);
			toReturn.Add("TicketDayTypes", _ticketDayTypes);
			toReturn.Add("TicketDeviceClasses", _ticketDeviceClasses);
			toReturn.Add("TicketDevicePaymentMethods", _ticketDevicePaymentMethods);
			toReturn.Add("TicketOrganizations", _ticketOrganizations);
			toReturn.Add("TicketOutputDevices", _ticketOutputDevices);
			toReturn.Add("TicketPaymentIntervals", _ticketPaymentIntervals);
			toReturn.Add("TicketPhysicalCardTypes", _ticketPhysicalCardTypes);
			toReturn.Add("TicketServicesPermitteds", _ticketServicesPermitteds);
			toReturn.Add("TicketToGroups", _ticketToGroups);
			toReturn.Add("TicketVendingClient", _ticketVendingClient);
			toReturn.Add("VdvKeySet", _vdvKeySet);
			toReturn.Add("VdvProduct", _vdvProduct);
			toReturn.Add("TaxProducts", _taxProducts);
			toReturn.Add("Products", _products);
			toReturn.Add("SettlementQuerySettingToTickets", _settlementQuerySettingToTickets);
			toReturn.Add("TicketSerialNumbers", _ticketSerialNumbers);
			toReturn.Add("TransactionJournalsForAppliedPassTicket", _transactionJournalsForAppliedPassTicket);
			toReturn.Add("TransactionJournals", _transactionJournals);
			toReturn.Add("TransactionJournals_", _transactionJournals_);
			toReturn.Add("Vouchers", _vouchers);
			toReturn.Add("AttributevalueCollectionViaAttributeValueList", _attributevalueCollectionViaAttributeValueList);
			toReturn.Add("CardChipTypeCollectionViaTicketPhysicalCardType", _cardChipTypeCollectionViaTicketPhysicalCardType);
			toReturn.Add("CardAdditionsTicketIsUsedIn", _cardAdditionsTicketIsUsedIn);
			toReturn.Add("ClientCollectionViaTicketVendingClient", _clientCollectionViaTicketVendingClient);
			toReturn.Add("LineCollectionViaTicketServicesPermitted", _lineCollectionViaTicketServicesPermitted);
			toReturn.Add("PaymentIntervalCollectionViaTicketPaymentInterval", _paymentIntervalCollectionViaTicketPaymentInterval);
			toReturn.Add("RuleCappingCollectionViaRuleCappingToTicket", _ruleCappingCollectionViaRuleCappingToTicket);
			toReturn.Add("TicketGroupCollectionViaTicketToGroup", _ticketGroupCollectionViaTicketToGroup);
			toReturn.Add("OrganizationCollectionViaTicketOrganization", _organizationCollectionViaTicketOrganization);
			toReturn.Add("SettlementQuerySettings", _settlementQuerySettings);
			toReturn.Add("CardTicketAddition", _cardTicketAddition);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="ticketID">PK value for Ticket which data should be fetched into this Ticket object</param>
		/// <param name="validator">The validator object for this TicketEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 ticketID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(ticketID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_apportionmentResults = new VarioSL.Entities.CollectionClasses.ApportionmentResultCollection();
			_apportionmentResults.SetContainingEntityInfo(this, "Ticket");

			_attributeValueList = new VarioSL.Entities.CollectionClasses.AttributeValueListCollection();
			_attributeValueList.SetContainingEntityInfo(this, "Ticket");

			_cardTicketToTicket = new VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection();
			_cardTicketToTicket.SetContainingEntityInfo(this, "Ticket");

			_choices = new VarioSL.Entities.CollectionClasses.ChoiceCollection();
			_choices.SetContainingEntityInfo(this, "Ticket");

			_parameterTicket = new VarioSL.Entities.CollectionClasses.ParameterTicketCollection();
			_parameterTicket.SetContainingEntityInfo(this, "Ticket");

			_primalKeys = new VarioSL.Entities.CollectionClasses.PrimalKeyCollection();
			_primalKeys.SetContainingEntityInfo(this, "Ticket");

			_ruleCappingToTicket = new VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection();
			_ruleCappingToTicket.SetContainingEntityInfo(this, "Ticket");

			_ticketDayTypes = new VarioSL.Entities.CollectionClasses.TicketDayTypeCollection();
			_ticketDayTypes.SetContainingEntityInfo(this, "Ticket");

			_ticketDeviceClasses = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			_ticketDeviceClasses.SetContainingEntityInfo(this, "Ticket");

			_ticketDevicePaymentMethods = new VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection();
			_ticketDevicePaymentMethods.SetContainingEntityInfo(this, "Ticket");

			_ticketOrganizations = new VarioSL.Entities.CollectionClasses.TicketOrganizationCollection();
			_ticketOrganizations.SetContainingEntityInfo(this, "Ticket");

			_ticketOutputDevices = new VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection();
			_ticketOutputDevices.SetContainingEntityInfo(this, "Ticket");

			_ticketPaymentIntervals = new VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection();
			_ticketPaymentIntervals.SetContainingEntityInfo(this, "Ticket");

			_ticketPhysicalCardTypes = new VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection();
			_ticketPhysicalCardTypes.SetContainingEntityInfo(this, "Ticket");

			_ticketServicesPermitteds = new VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection();
			_ticketServicesPermitteds.SetContainingEntityInfo(this, "Ticket");

			_ticketToGroups = new VarioSL.Entities.CollectionClasses.TicketToGroupCollection();
			_ticketToGroups.SetContainingEntityInfo(this, "Ticket");

			_ticketVendingClient = new VarioSL.Entities.CollectionClasses.TicketVendingClientCollection();
			_ticketVendingClient.SetContainingEntityInfo(this, "Ticket");

			_vdvKeySet = new VarioSL.Entities.CollectionClasses.VdvKeySetCollection();
			_vdvKeySet.SetContainingEntityInfo(this, "Ticket");

			_vdvProduct = new VarioSL.Entities.CollectionClasses.VdvProductCollection();
			_vdvProduct.SetContainingEntityInfo(this, "Ticket");

			_taxProducts = new VarioSL.Entities.CollectionClasses.ProductCollection();
			_taxProducts.SetContainingEntityInfo(this, "TaxTicket");

			_products = new VarioSL.Entities.CollectionClasses.ProductCollection();
			_products.SetContainingEntityInfo(this, "Ticket");

			_settlementQuerySettingToTickets = new VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection();
			_settlementQuerySettingToTickets.SetContainingEntityInfo(this, "Ticket");

			_ticketSerialNumbers = new VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection();
			_ticketSerialNumbers.SetContainingEntityInfo(this, "Ticket");

			_transactionJournalsForAppliedPassTicket = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_transactionJournalsForAppliedPassTicket.SetContainingEntityInfo(this, "AppliedPassTicket");

			_transactionJournals = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_transactionJournals.SetContainingEntityInfo(this, "Ticket");

			_transactionJournals_ = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_transactionJournals_.SetContainingEntityInfo(this, "Ticket_");

			_vouchers = new VarioSL.Entities.CollectionClasses.VoucherCollection();
			_vouchers.SetContainingEntityInfo(this, "Ticket");
			_attributevalueCollectionViaAttributeValueList = new VarioSL.Entities.CollectionClasses.AttributeValueCollection();
			_cardChipTypeCollectionViaTicketPhysicalCardType = new VarioSL.Entities.CollectionClasses.CardChipTypeCollection();
			_cardAdditionsTicketIsUsedIn = new VarioSL.Entities.CollectionClasses.CardTicketCollection();
			_clientCollectionViaTicketVendingClient = new VarioSL.Entities.CollectionClasses.ClientCollection();
			_lineCollectionViaTicketServicesPermitted = new VarioSL.Entities.CollectionClasses.LineCollection();
			_paymentIntervalCollectionViaTicketPaymentInterval = new VarioSL.Entities.CollectionClasses.PaymentIntervalCollection();
			_ruleCappingCollectionViaRuleCappingToTicket = new VarioSL.Entities.CollectionClasses.RuleCappingCollection();
			_ticketGroupCollectionViaTicketToGroup = new VarioSL.Entities.CollectionClasses.TicketGroupCollection();
			_organizationCollectionViaTicketOrganization = new VarioSL.Entities.CollectionClasses.OrganizationCollection();
			_settlementQuerySettings = new VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection();
			_businessRuleInspectionReturnsNewIfNotFound = false;
			_businessRuleTestBoardingReturnsNewIfNotFound = false;
			_businessRuleReturnsNewIfNotFound = false;
			_businessRuleTicketAssignmentReturnsNewIfNotFound = false;
			_calendarReturnsNewIfNotFound = false;
			_clientReturnsNewIfNotFound = false;
			_deviceClassReturnsNewIfNotFound = false;
			_eticketReturnsNewIfNotFound = false;
			_fareEvasionCategoryReturnsNewIfNotFound = false;
			_fareMatrixReturnsNewIfNotFound = false;
			_fareTable1ReturnsNewIfNotFound = false;
			_fareTable2ReturnsNewIfNotFound = false;
			_cancellationLayoutReturnsNewIfNotFound = false;
			_groupLayoutReturnsNewIfNotFound = false;
			_printLayoutReturnsNewIfNotFound = false;
			_receiptLayoutReturnsNewIfNotFound = false;
			_lineGroupReturnsNewIfNotFound = false;
			_pageContentGroupReturnsNewIfNotFound = false;
			_priceTypeReturnsNewIfNotFound = false;
			_rulePeriodReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			_temporalTypeReturnsNewIfNotFound = false;
			_ticketCancellationTypeReturnsNewIfNotFound = false;
			_ticketCategoryReturnsNewIfNotFound = false;
			_ticketSelectionModeReturnsNewIfNotFound = false;
			_ticketTypeReturnsNewIfNotFound = false;
			_userKey1ReturnsNewIfNotFound = false;
			_userKey2ReturnsNewIfNotFound = false;
			_cardTicketAdditionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttributeValueListID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Barcode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BaseFare", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrType10ID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrType1ID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrType2ID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrType9ID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CalendarID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationLayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationQuantity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Discount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EtickCancellationQuantity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EtickEmission", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EtickID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalName2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalName3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalName4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalName5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalName6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalName7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalName8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalName9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareEvasionCategoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageListID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareTable2ID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareTableID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FormularName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GroupLayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InfoText1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InfoText2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsOfflineAvailable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsSupplement", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Key1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Key2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MatrixID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxFactor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinFactor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumberOfPrintouts", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OwnerClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageContentGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Price2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintLayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptLayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RulePeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SelectionMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShortCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TarifID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TemporalTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketCancellationTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketPropertyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripFrequency", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValuePassCredit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Vat", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _businessRuleInspection</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBusinessRuleInspection(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _businessRuleInspection, new PropertyChangedEventHandler( OnBusinessRuleInspectionPropertyChanged ), "BusinessRuleInspection", VarioSL.Entities.RelationClasses.StaticTicketRelations.BusinessRuleEntityUsingBrType10IDStatic, true, signalRelatedEntity, "TicketsInspection", resetFKFields, new int[] { (int)TicketFieldIndex.BrType10ID } );		
			_businessRuleInspection = null;
		}
		
		/// <summary> setups the sync logic for member _businessRuleInspection</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBusinessRuleInspection(IEntityCore relatedEntity)
		{
			if(_businessRuleInspection!=relatedEntity)
			{		
				DesetupSyncBusinessRuleInspection(true, true);
				_businessRuleInspection = (BusinessRuleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _businessRuleInspection, new PropertyChangedEventHandler( OnBusinessRuleInspectionPropertyChanged ), "BusinessRuleInspection", VarioSL.Entities.RelationClasses.StaticTicketRelations.BusinessRuleEntityUsingBrType10IDStatic, true, ref _alreadyFetchedBusinessRuleInspection, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBusinessRuleInspectionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _businessRuleTestBoarding</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBusinessRuleTestBoarding(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _businessRuleTestBoarding, new PropertyChangedEventHandler( OnBusinessRuleTestBoardingPropertyChanged ), "BusinessRuleTestBoarding", VarioSL.Entities.RelationClasses.StaticTicketRelations.BusinessRuleEntityUsingBrType2IDStatic, true, signalRelatedEntity, "TicketsTestBoarding", resetFKFields, new int[] { (int)TicketFieldIndex.BrType2ID } );		
			_businessRuleTestBoarding = null;
		}
		
		/// <summary> setups the sync logic for member _businessRuleTestBoarding</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBusinessRuleTestBoarding(IEntityCore relatedEntity)
		{
			if(_businessRuleTestBoarding!=relatedEntity)
			{		
				DesetupSyncBusinessRuleTestBoarding(true, true);
				_businessRuleTestBoarding = (BusinessRuleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _businessRuleTestBoarding, new PropertyChangedEventHandler( OnBusinessRuleTestBoardingPropertyChanged ), "BusinessRuleTestBoarding", VarioSL.Entities.RelationClasses.StaticTicketRelations.BusinessRuleEntityUsingBrType2IDStatic, true, ref _alreadyFetchedBusinessRuleTestBoarding, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBusinessRuleTestBoardingPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _businessRule</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBusinessRule(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _businessRule, new PropertyChangedEventHandler( OnBusinessRulePropertyChanged ), "BusinessRule", VarioSL.Entities.RelationClasses.StaticTicketRelations.BusinessRuleEntityUsingBrType1IDStatic, true, signalRelatedEntity, "TicketsTestSale", resetFKFields, new int[] { (int)TicketFieldIndex.BrType1ID } );		
			_businessRule = null;
		}
		
		/// <summary> setups the sync logic for member _businessRule</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBusinessRule(IEntityCore relatedEntity)
		{
			if(_businessRule!=relatedEntity)
			{		
				DesetupSyncBusinessRule(true, true);
				_businessRule = (BusinessRuleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _businessRule, new PropertyChangedEventHandler( OnBusinessRulePropertyChanged ), "BusinessRule", VarioSL.Entities.RelationClasses.StaticTicketRelations.BusinessRuleEntityUsingBrType1IDStatic, true, ref _alreadyFetchedBusinessRule, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBusinessRulePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _businessRuleTicketAssignment</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBusinessRuleTicketAssignment(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _businessRuleTicketAssignment, new PropertyChangedEventHandler( OnBusinessRuleTicketAssignmentPropertyChanged ), "BusinessRuleTicketAssignment", VarioSL.Entities.RelationClasses.StaticTicketRelations.BusinessRuleEntityUsingBrType9IDStatic, true, signalRelatedEntity, "TicketsTicketAssignment", resetFKFields, new int[] { (int)TicketFieldIndex.BrType9ID } );		
			_businessRuleTicketAssignment = null;
		}
		
		/// <summary> setups the sync logic for member _businessRuleTicketAssignment</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBusinessRuleTicketAssignment(IEntityCore relatedEntity)
		{
			if(_businessRuleTicketAssignment!=relatedEntity)
			{		
				DesetupSyncBusinessRuleTicketAssignment(true, true);
				_businessRuleTicketAssignment = (BusinessRuleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _businessRuleTicketAssignment, new PropertyChangedEventHandler( OnBusinessRuleTicketAssignmentPropertyChanged ), "BusinessRuleTicketAssignment", VarioSL.Entities.RelationClasses.StaticTicketRelations.BusinessRuleEntityUsingBrType9IDStatic, true, ref _alreadyFetchedBusinessRuleTicketAssignment, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBusinessRuleTicketAssignmentPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _calendar</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCalendar(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _calendar, new PropertyChangedEventHandler( OnCalendarPropertyChanged ), "Calendar", VarioSL.Entities.RelationClasses.StaticTicketRelations.CalendarEntityUsingCalendarIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.CalendarID } );		
			_calendar = null;
		}
		
		/// <summary> setups the sync logic for member _calendar</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCalendar(IEntityCore relatedEntity)
		{
			if(_calendar!=relatedEntity)
			{		
				DesetupSyncCalendar(true, true);
				_calendar = (CalendarEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _calendar, new PropertyChangedEventHandler( OnCalendarPropertyChanged ), "Calendar", VarioSL.Entities.RelationClasses.StaticTicketRelations.CalendarEntityUsingCalendarIDStatic, true, ref _alreadyFetchedCalendar, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCalendarPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticTicketRelations.ClientEntityUsingOwnerClientIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.OwnerClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticTicketRelations.ClientEntityUsingOwnerClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticTicketRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.DeviceClassID } );		
			_deviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _deviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceClass(IEntityCore relatedEntity)
		{
			if(_deviceClass!=relatedEntity)
			{		
				DesetupSyncDeviceClass(true, true);
				_deviceClass = (DeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticTicketRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, ref _alreadyFetchedDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _eticket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEticket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _eticket, new PropertyChangedEventHandler( OnEticketPropertyChanged ), "Eticket", VarioSL.Entities.RelationClasses.StaticTicketRelations.EticketEntityUsingEtickIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.EtickID } );		
			_eticket = null;
		}
		
		/// <summary> setups the sync logic for member _eticket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEticket(IEntityCore relatedEntity)
		{
			if(_eticket!=relatedEntity)
			{		
				DesetupSyncEticket(true, true);
				_eticket = (EticketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _eticket, new PropertyChangedEventHandler( OnEticketPropertyChanged ), "Eticket", VarioSL.Entities.RelationClasses.StaticTicketRelations.EticketEntityUsingEtickIDStatic, true, ref _alreadyFetchedEticket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEticketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareEvasionCategory</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareEvasionCategory(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareEvasionCategory, new PropertyChangedEventHandler( OnFareEvasionCategoryPropertyChanged ), "FareEvasionCategory", VarioSL.Entities.RelationClasses.StaticTicketRelations.FareEvasionCategoryEntityUsingFareEvasionCategoryIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.FareEvasionCategoryID } );		
			_fareEvasionCategory = null;
		}
		
		/// <summary> setups the sync logic for member _fareEvasionCategory</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareEvasionCategory(IEntityCore relatedEntity)
		{
			if(_fareEvasionCategory!=relatedEntity)
			{		
				DesetupSyncFareEvasionCategory(true, true);
				_fareEvasionCategory = (FareEvasionCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareEvasionCategory, new PropertyChangedEventHandler( OnFareEvasionCategoryPropertyChanged ), "FareEvasionCategory", VarioSL.Entities.RelationClasses.StaticTicketRelations.FareEvasionCategoryEntityUsingFareEvasionCategoryIDStatic, true, ref _alreadyFetchedFareEvasionCategory, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareEvasionCategoryPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareMatrix</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareMatrix(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareMatrix, new PropertyChangedEventHandler( OnFareMatrixPropertyChanged ), "FareMatrix", VarioSL.Entities.RelationClasses.StaticTicketRelations.FareMatrixEntityUsingMatrixIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.MatrixID } );		
			_fareMatrix = null;
		}
		
		/// <summary> setups the sync logic for member _fareMatrix</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareMatrix(IEntityCore relatedEntity)
		{
			if(_fareMatrix!=relatedEntity)
			{		
				DesetupSyncFareMatrix(true, true);
				_fareMatrix = (FareMatrixEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareMatrix, new PropertyChangedEventHandler( OnFareMatrixPropertyChanged ), "FareMatrix", VarioSL.Entities.RelationClasses.StaticTicketRelations.FareMatrixEntityUsingMatrixIDStatic, true, ref _alreadyFetchedFareMatrix, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareMatrixPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareTable1</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareTable1(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareTable1, new PropertyChangedEventHandler( OnFareTable1PropertyChanged ), "FareTable1", VarioSL.Entities.RelationClasses.StaticTicketRelations.FareTableEntityUsingFareTableIDStatic, true, signalRelatedEntity, "TicketsFareTable1", resetFKFields, new int[] { (int)TicketFieldIndex.FareTableID } );		
			_fareTable1 = null;
		}
		
		/// <summary> setups the sync logic for member _fareTable1</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareTable1(IEntityCore relatedEntity)
		{
			if(_fareTable1!=relatedEntity)
			{		
				DesetupSyncFareTable1(true, true);
				_fareTable1 = (FareTableEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareTable1, new PropertyChangedEventHandler( OnFareTable1PropertyChanged ), "FareTable1", VarioSL.Entities.RelationClasses.StaticTicketRelations.FareTableEntityUsingFareTableIDStatic, true, ref _alreadyFetchedFareTable1, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareTable1PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareTable2</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareTable2(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareTable2, new PropertyChangedEventHandler( OnFareTable2PropertyChanged ), "FareTable2", VarioSL.Entities.RelationClasses.StaticTicketRelations.FareTableEntityUsingFareTable2IDStatic, true, signalRelatedEntity, "TicketsFareTable2", resetFKFields, new int[] { (int)TicketFieldIndex.FareTable2ID } );		
			_fareTable2 = null;
		}
		
		/// <summary> setups the sync logic for member _fareTable2</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareTable2(IEntityCore relatedEntity)
		{
			if(_fareTable2!=relatedEntity)
			{		
				DesetupSyncFareTable2(true, true);
				_fareTable2 = (FareTableEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareTable2, new PropertyChangedEventHandler( OnFareTable2PropertyChanged ), "FareTable2", VarioSL.Entities.RelationClasses.StaticTicketRelations.FareTableEntityUsingFareTable2IDStatic, true, ref _alreadyFetchedFareTable2, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareTable2PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cancellationLayout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCancellationLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cancellationLayout, new PropertyChangedEventHandler( OnCancellationLayoutPropertyChanged ), "CancellationLayout", VarioSL.Entities.RelationClasses.StaticTicketRelations.LayoutEntityUsingCancellationLayoutIDStatic, true, signalRelatedEntity, "TicketsCancellationLayout", resetFKFields, new int[] { (int)TicketFieldIndex.CancellationLayoutID } );		
			_cancellationLayout = null;
		}
		
		/// <summary> setups the sync logic for member _cancellationLayout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCancellationLayout(IEntityCore relatedEntity)
		{
			if(_cancellationLayout!=relatedEntity)
			{		
				DesetupSyncCancellationLayout(true, true);
				_cancellationLayout = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cancellationLayout, new PropertyChangedEventHandler( OnCancellationLayoutPropertyChanged ), "CancellationLayout", VarioSL.Entities.RelationClasses.StaticTicketRelations.LayoutEntityUsingCancellationLayoutIDStatic, true, ref _alreadyFetchedCancellationLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCancellationLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _groupLayout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGroupLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _groupLayout, new PropertyChangedEventHandler( OnGroupLayoutPropertyChanged ), "GroupLayout", VarioSL.Entities.RelationClasses.StaticTicketRelations.LayoutEntityUsingGroupLayoutIDStatic, true, signalRelatedEntity, "TicketsGroupLayout", resetFKFields, new int[] { (int)TicketFieldIndex.GroupLayoutID } );		
			_groupLayout = null;
		}
		
		/// <summary> setups the sync logic for member _groupLayout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGroupLayout(IEntityCore relatedEntity)
		{
			if(_groupLayout!=relatedEntity)
			{		
				DesetupSyncGroupLayout(true, true);
				_groupLayout = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _groupLayout, new PropertyChangedEventHandler( OnGroupLayoutPropertyChanged ), "GroupLayout", VarioSL.Entities.RelationClasses.StaticTicketRelations.LayoutEntityUsingGroupLayoutIDStatic, true, ref _alreadyFetchedGroupLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGroupLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _printLayout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPrintLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _printLayout, new PropertyChangedEventHandler( OnPrintLayoutPropertyChanged ), "PrintLayout", VarioSL.Entities.RelationClasses.StaticTicketRelations.LayoutEntityUsingPrintLayoutIDStatic, true, signalRelatedEntity, "TicketsPrintLayout", resetFKFields, new int[] { (int)TicketFieldIndex.PrintLayoutID } );		
			_printLayout = null;
		}
		
		/// <summary> setups the sync logic for member _printLayout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPrintLayout(IEntityCore relatedEntity)
		{
			if(_printLayout!=relatedEntity)
			{		
				DesetupSyncPrintLayout(true, true);
				_printLayout = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _printLayout, new PropertyChangedEventHandler( OnPrintLayoutPropertyChanged ), "PrintLayout", VarioSL.Entities.RelationClasses.StaticTicketRelations.LayoutEntityUsingPrintLayoutIDStatic, true, ref _alreadyFetchedPrintLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPrintLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _receiptLayout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReceiptLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _receiptLayout, new PropertyChangedEventHandler( OnReceiptLayoutPropertyChanged ), "ReceiptLayout", VarioSL.Entities.RelationClasses.StaticTicketRelations.LayoutEntityUsingReceiptLayoutIDStatic, true, signalRelatedEntity, "TicketsReceiptLayout", resetFKFields, new int[] { (int)TicketFieldIndex.ReceiptLayoutID } );		
			_receiptLayout = null;
		}
		
		/// <summary> setups the sync logic for member _receiptLayout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReceiptLayout(IEntityCore relatedEntity)
		{
			if(_receiptLayout!=relatedEntity)
			{		
				DesetupSyncReceiptLayout(true, true);
				_receiptLayout = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _receiptLayout, new PropertyChangedEventHandler( OnReceiptLayoutPropertyChanged ), "ReceiptLayout", VarioSL.Entities.RelationClasses.StaticTicketRelations.LayoutEntityUsingReceiptLayoutIDStatic, true, ref _alreadyFetchedReceiptLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReceiptLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _lineGroup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLineGroup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _lineGroup, new PropertyChangedEventHandler( OnLineGroupPropertyChanged ), "LineGroup", VarioSL.Entities.RelationClasses.StaticTicketRelations.LineGroupEntityUsingLineGroupIDStatic, true, signalRelatedEntity, "Ticket", resetFKFields, new int[] { (int)TicketFieldIndex.LineGroupID } );		
			_lineGroup = null;
		}
		
		/// <summary> setups the sync logic for member _lineGroup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLineGroup(IEntityCore relatedEntity)
		{
			if(_lineGroup!=relatedEntity)
			{		
				DesetupSyncLineGroup(true, true);
				_lineGroup = (LineGroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _lineGroup, new PropertyChangedEventHandler( OnLineGroupPropertyChanged ), "LineGroup", VarioSL.Entities.RelationClasses.StaticTicketRelations.LineGroupEntityUsingLineGroupIDStatic, true, ref _alreadyFetchedLineGroup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLineGroupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageContentGroup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageContentGroup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageContentGroup, new PropertyChangedEventHandler( OnPageContentGroupPropertyChanged ), "PageContentGroup", VarioSL.Entities.RelationClasses.StaticTicketRelations.PageContentGroupEntityUsingPageContentGroupIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.PageContentGroupID } );		
			_pageContentGroup = null;
		}
		
		/// <summary> setups the sync logic for member _pageContentGroup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageContentGroup(IEntityCore relatedEntity)
		{
			if(_pageContentGroup!=relatedEntity)
			{		
				DesetupSyncPageContentGroup(true, true);
				_pageContentGroup = (PageContentGroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageContentGroup, new PropertyChangedEventHandler( OnPageContentGroupPropertyChanged ), "PageContentGroup", VarioSL.Entities.RelationClasses.StaticTicketRelations.PageContentGroupEntityUsingPageContentGroupIDStatic, true, ref _alreadyFetchedPageContentGroup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageContentGroupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _priceType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPriceType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _priceType, new PropertyChangedEventHandler( OnPriceTypePropertyChanged ), "PriceType", VarioSL.Entities.RelationClasses.StaticTicketRelations.PriceTypeEntityUsingPriceTypeIDStatic, true, signalRelatedEntity, "Ticket", resetFKFields, new int[] { (int)TicketFieldIndex.PriceTypeID } );		
			_priceType = null;
		}
		
		/// <summary> setups the sync logic for member _priceType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPriceType(IEntityCore relatedEntity)
		{
			if(_priceType!=relatedEntity)
			{		
				DesetupSyncPriceType(true, true);
				_priceType = (PriceTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _priceType, new PropertyChangedEventHandler( OnPriceTypePropertyChanged ), "PriceType", VarioSL.Entities.RelationClasses.StaticTicketRelations.PriceTypeEntityUsingPriceTypeIDStatic, true, ref _alreadyFetchedPriceType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPriceTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _rulePeriod</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRulePeriod(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _rulePeriod, new PropertyChangedEventHandler( OnRulePeriodPropertyChanged ), "RulePeriod", VarioSL.Entities.RelationClasses.StaticTicketRelations.RulePeriodEntityUsingRulePeriodIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.RulePeriodID } );		
			_rulePeriod = null;
		}
		
		/// <summary> setups the sync logic for member _rulePeriod</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRulePeriod(IEntityCore relatedEntity)
		{
			if(_rulePeriod!=relatedEntity)
			{		
				DesetupSyncRulePeriod(true, true);
				_rulePeriod = (RulePeriodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _rulePeriod, new PropertyChangedEventHandler( OnRulePeriodPropertyChanged ), "RulePeriod", VarioSL.Entities.RelationClasses.StaticTicketRelations.RulePeriodEntityUsingRulePeriodIDStatic, true, ref _alreadyFetchedRulePeriod, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRulePeriodPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticTicketRelations.TariffEntityUsingTarifIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.TarifID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticTicketRelations.TariffEntityUsingTarifIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _temporalType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTemporalType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _temporalType, new PropertyChangedEventHandler( OnTemporalTypePropertyChanged ), "TemporalType", VarioSL.Entities.RelationClasses.StaticTicketRelations.TemporalTypeEntityUsingTemporalTypeIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.TemporalTypeID } );		
			_temporalType = null;
		}
		
		/// <summary> setups the sync logic for member _temporalType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTemporalType(IEntityCore relatedEntity)
		{
			if(_temporalType!=relatedEntity)
			{		
				DesetupSyncTemporalType(true, true);
				_temporalType = (TemporalTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _temporalType, new PropertyChangedEventHandler( OnTemporalTypePropertyChanged ), "TemporalType", VarioSL.Entities.RelationClasses.StaticTicketRelations.TemporalTypeEntityUsingTemporalTypeIDStatic, true, ref _alreadyFetchedTemporalType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTemporalTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticketCancellationType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicketCancellationType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticketCancellationType, new PropertyChangedEventHandler( OnTicketCancellationTypePropertyChanged ), "TicketCancellationType", VarioSL.Entities.RelationClasses.StaticTicketRelations.TicketCancellationTypeEntityUsingTicketCancellationTypeIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.TicketCancellationTypeID } );		
			_ticketCancellationType = null;
		}
		
		/// <summary> setups the sync logic for member _ticketCancellationType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicketCancellationType(IEntityCore relatedEntity)
		{
			if(_ticketCancellationType!=relatedEntity)
			{		
				DesetupSyncTicketCancellationType(true, true);
				_ticketCancellationType = (TicketCancellationTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticketCancellationType, new PropertyChangedEventHandler( OnTicketCancellationTypePropertyChanged ), "TicketCancellationType", VarioSL.Entities.RelationClasses.StaticTicketRelations.TicketCancellationTypeEntityUsingTicketCancellationTypeIDStatic, true, ref _alreadyFetchedTicketCancellationType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketCancellationTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticketCategory</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicketCategory(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticketCategory, new PropertyChangedEventHandler( OnTicketCategoryPropertyChanged ), "TicketCategory", VarioSL.Entities.RelationClasses.StaticTicketRelations.TicketCategoryEntityUsingCategoryIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.CategoryID } );		
			_ticketCategory = null;
		}
		
		/// <summary> setups the sync logic for member _ticketCategory</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicketCategory(IEntityCore relatedEntity)
		{
			if(_ticketCategory!=relatedEntity)
			{		
				DesetupSyncTicketCategory(true, true);
				_ticketCategory = (TicketCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticketCategory, new PropertyChangedEventHandler( OnTicketCategoryPropertyChanged ), "TicketCategory", VarioSL.Entities.RelationClasses.StaticTicketRelations.TicketCategoryEntityUsingCategoryIDStatic, true, ref _alreadyFetchedTicketCategory, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketCategoryPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticketSelectionMode</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicketSelectionMode(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticketSelectionMode, new PropertyChangedEventHandler( OnTicketSelectionModePropertyChanged ), "TicketSelectionMode", VarioSL.Entities.RelationClasses.StaticTicketRelations.TicketSelectionModeEntityUsingSelectionModeStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.SelectionMode } );		
			_ticketSelectionMode = null;
		}
		
		/// <summary> setups the sync logic for member _ticketSelectionMode</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicketSelectionMode(IEntityCore relatedEntity)
		{
			if(_ticketSelectionMode!=relatedEntity)
			{		
				DesetupSyncTicketSelectionMode(true, true);
				_ticketSelectionMode = (TicketSelectionModeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticketSelectionMode, new PropertyChangedEventHandler( OnTicketSelectionModePropertyChanged ), "TicketSelectionMode", VarioSL.Entities.RelationClasses.StaticTicketRelations.TicketSelectionModeEntityUsingSelectionModeStatic, true, ref _alreadyFetchedTicketSelectionMode, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketSelectionModePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticketType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicketType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticketType, new PropertyChangedEventHandler( OnTicketTypePropertyChanged ), "TicketType", VarioSL.Entities.RelationClasses.StaticTicketRelations.TicketTypeEntityUsingTicketTypeIDStatic, true, signalRelatedEntity, "Tickets", resetFKFields, new int[] { (int)TicketFieldIndex.TicketTypeID } );		
			_ticketType = null;
		}
		
		/// <summary> setups the sync logic for member _ticketType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicketType(IEntityCore relatedEntity)
		{
			if(_ticketType!=relatedEntity)
			{		
				DesetupSyncTicketType(true, true);
				_ticketType = (TicketTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticketType, new PropertyChangedEventHandler( OnTicketTypePropertyChanged ), "TicketType", VarioSL.Entities.RelationClasses.StaticTicketRelations.TicketTypeEntityUsingTicketTypeIDStatic, true, ref _alreadyFetchedTicketType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _userKey1</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserKey1(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userKey1, new PropertyChangedEventHandler( OnUserKey1PropertyChanged ), "UserKey1", VarioSL.Entities.RelationClasses.StaticTicketRelations.UserKeyEntityUsingKey1Static, true, signalRelatedEntity, "TicketsKey1", resetFKFields, new int[] { (int)TicketFieldIndex.Key1 } );		
			_userKey1 = null;
		}
		
		/// <summary> setups the sync logic for member _userKey1</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserKey1(IEntityCore relatedEntity)
		{
			if(_userKey1!=relatedEntity)
			{		
				DesetupSyncUserKey1(true, true);
				_userKey1 = (UserKeyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userKey1, new PropertyChangedEventHandler( OnUserKey1PropertyChanged ), "UserKey1", VarioSL.Entities.RelationClasses.StaticTicketRelations.UserKeyEntityUsingKey1Static, true, ref _alreadyFetchedUserKey1, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserKey1PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _userKey2</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserKey2(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userKey2, new PropertyChangedEventHandler( OnUserKey2PropertyChanged ), "UserKey2", VarioSL.Entities.RelationClasses.StaticTicketRelations.UserKeyEntityUsingKey2Static, true, signalRelatedEntity, "TicketsKey2", resetFKFields, new int[] { (int)TicketFieldIndex.Key2 } );		
			_userKey2 = null;
		}
		
		/// <summary> setups the sync logic for member _userKey2</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserKey2(IEntityCore relatedEntity)
		{
			if(_userKey2!=relatedEntity)
			{		
				DesetupSyncUserKey2(true, true);
				_userKey2 = (UserKeyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userKey2, new PropertyChangedEventHandler( OnUserKey2PropertyChanged ), "UserKey2", VarioSL.Entities.RelationClasses.StaticTicketRelations.UserKeyEntityUsingKey2Static, true, ref _alreadyFetchedUserKey2, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserKey2PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cardTicketAddition</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardTicketAddition(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardTicketAddition, new PropertyChangedEventHandler( OnCardTicketAdditionPropertyChanged ), "CardTicketAddition", VarioSL.Entities.RelationClasses.StaticTicketRelations.CardTicketEntityUsingTicketIDStatic, false, signalRelatedEntity, "AssignedCardTicket", false, new int[] { (int)TicketFieldIndex.TicketID } );
			_cardTicketAddition = null;
		}
	
		/// <summary> setups the sync logic for member _cardTicketAddition</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardTicketAddition(IEntityCore relatedEntity)
		{
			if(_cardTicketAddition!=relatedEntity)
			{
				DesetupSyncCardTicketAddition(true, true);
				_cardTicketAddition = (CardTicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardTicketAddition, new PropertyChangedEventHandler( OnCardTicketAdditionPropertyChanged ), "CardTicketAddition", VarioSL.Entities.RelationClasses.StaticTicketRelations.CardTicketEntityUsingTicketIDStatic, false, ref _alreadyFetchedCardTicketAddition, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardTicketAdditionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="ticketID">PK value for Ticket which data should be fetched into this Ticket object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 ticketID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TicketFieldIndex.TicketID].ForcedCurrentValueWrite(ticketID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTicketDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TicketEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TicketRelations Relations
		{
			get	{ return new TicketRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ApportionmentResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApportionmentResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ApportionmentResultCollection(), (IEntityRelation)GetRelationsForField("ApportionmentResults")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.ApportionmentResultEntity, 0, null, null, null, "ApportionmentResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValueList' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValueList
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueListCollection(), (IEntityRelation)GetRelationsForField("AttributeValueList")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.AttributeValueListEntity, 0, null, null, null, "AttributeValueList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardTicketToTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardTicketToTicket
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection(), (IEntityRelation)GetRelationsForField("CardTicketToTicket")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.CardTicketToTicketEntity, 0, null, null, null, "CardTicketToTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Choice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathChoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ChoiceCollection(), (IEntityRelation)GetRelationsForField("Choices")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.ChoiceEntity, 0, null, null, null, "Choices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterTicket
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterTicketCollection(), (IEntityRelation)GetRelationsForField("ParameterTicket")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.ParameterTicketEntity, 0, null, null, null, "ParameterTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PrimalKey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPrimalKeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PrimalKeyCollection(), (IEntityRelation)GetRelationsForField("PrimalKeys")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.PrimalKeyEntity, 0, null, null, null, "PrimalKeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleCappingToTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleCappingToTicket
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection(), (IEntityRelation)GetRelationsForField("RuleCappingToTicket")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.RuleCappingToTicketEntity, 0, null, null, null, "RuleCappingToTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDayType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDayTypes
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDayTypeCollection(), (IEntityRelation)GetRelationsForField("TicketDayTypes")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketDayTypeEntity, 0, null, null, null, "TicketDayTypes", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceClasses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceClasses")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, null, "TicketDeviceClasses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDevicePaymentMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDevicePaymentMethods
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection(), (IEntityRelation)GetRelationsForField("TicketDevicePaymentMethods")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketDevicePaymentMethodEntity, 0, null, null, null, "TicketDevicePaymentMethods", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketOrganization' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketOrganizations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketOrganizationCollection(), (IEntityRelation)GetRelationsForField("TicketOrganizations")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketOrganizationEntity, 0, null, null, null, "TicketOrganizations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketOutputdevice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketOutputDevices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection(), (IEntityRelation)GetRelationsForField("TicketOutputDevices")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketOutputdeviceEntity, 0, null, null, null, "TicketOutputDevices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketPaymentInterval' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketPaymentIntervals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection(), (IEntityRelation)GetRelationsForField("TicketPaymentIntervals")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketPaymentIntervalEntity, 0, null, null, null, "TicketPaymentIntervals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketPhysicalCardType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketPhysicalCardTypes
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection(), (IEntityRelation)GetRelationsForField("TicketPhysicalCardTypes")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketPhysicalCardTypeEntity, 0, null, null, null, "TicketPhysicalCardTypes", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketServicesPermitted' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketServicesPermitteds
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection(), (IEntityRelation)GetRelationsForField("TicketServicesPermitteds")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketServicesPermittedEntity, 0, null, null, null, "TicketServicesPermitteds", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketToGroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketToGroups
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketToGroupCollection(), (IEntityRelation)GetRelationsForField("TicketToGroups")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketToGroupEntity, 0, null, null, null, "TicketToGroups", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketVendingClient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketVendingClient
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketVendingClientCollection(), (IEntityRelation)GetRelationsForField("TicketVendingClient")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketVendingClientEntity, 0, null, null, null, "TicketVendingClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvKeySet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvKeySet
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvKeySetCollection(), (IEntityRelation)GetRelationsForField("VdvKeySet")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.VdvKeySetEntity, 0, null, null, null, "VdvKeySet", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvProduct
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvProductCollection(), (IEntityRelation)GetRelationsForField("VdvProduct")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.VdvProductEntity, 0, null, null, null, "VdvProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTaxProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("TaxProducts")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "TaxProducts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Products")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Products", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementQuerySettingToTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementQuerySettingToTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection(), (IEntityRelation)GetRelationsForField("SettlementQuerySettingToTickets")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.SettlementQuerySettingToTicketEntity, 0, null, null, null, "SettlementQuerySettingToTickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketSerialNumber' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketSerialNumbers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection(), (IEntityRelation)GetRelationsForField("TicketSerialNumbers")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketSerialNumberEntity, 0, null, null, null, "TicketSerialNumbers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournalsForAppliedPassTicket
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournalsForAppliedPassTicket")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournalsForAppliedPassTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournals")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournals_
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournals_")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournals_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Voucher' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVouchers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VoucherCollection(), (IEntityRelation)GetRelationsForField("Vouchers")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.VoucherEntity, 0, null, null, null, "Vouchers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributevalueCollectionViaAttributeValueList
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AttributeValueListEntityUsingTicketID;
				intermediateRelation.SetAliases(string.Empty, "AttributeValueList_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, GetRelationsForField("AttributevalueCollectionViaAttributeValueList"), "AttributevalueCollectionViaAttributeValueList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardChipType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardChipTypeCollectionViaTicketPhysicalCardType
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketPhysicalCardTypeEntityUsingTicketID;
				intermediateRelation.SetAliases(string.Empty, "TicketPhysicalCardType_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardChipTypeCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.CardChipTypeEntity, 0, null, null, GetRelationsForField("CardChipTypeCollectionViaTicketPhysicalCardType"), "CardChipTypeCollectionViaTicketPhysicalCardType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardTicket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardAdditionsTicketIsUsedIn
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CardTicketToTicketEntityUsingTicketID;
				intermediateRelation.SetAliases(string.Empty, "CardTicketToTicket_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardTicketCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.CardTicketEntity, 0, null, null, GetRelationsForField("CardAdditionsTicketIsUsedIn"), "CardAdditionsTicketIsUsedIn", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollectionViaTicketVendingClient
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketVendingClientEntityUsingTicketid;
				intermediateRelation.SetAliases(string.Empty, "TicketVendingClient_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, GetRelationsForField("ClientCollectionViaTicketVendingClient"), "ClientCollectionViaTicketVendingClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Line'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLineCollectionViaTicketServicesPermitted
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketServicesPermittedEntityUsingTicketID;
				intermediateRelation.SetAliases(string.Empty, "TicketServicesPermitted_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LineCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.LineEntity, 0, null, null, GetRelationsForField("LineCollectionViaTicketServicesPermitted"), "LineCollectionViaTicketServicesPermitted", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentInterval'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentIntervalCollectionViaTicketPaymentInterval
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketPaymentIntervalEntityUsingTicketID;
				intermediateRelation.SetAliases(string.Empty, "TicketPaymentInterval_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentIntervalCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.PaymentIntervalEntity, 0, null, null, GetRelationsForField("PaymentIntervalCollectionViaTicketPaymentInterval"), "PaymentIntervalCollectionViaTicketPaymentInterval", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleCapping'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleCappingCollectionViaRuleCappingToTicket
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.RuleCappingToTicketEntityUsingTicketID;
				intermediateRelation.SetAliases(string.Empty, "RuleCappingToTicket_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleCappingCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.RuleCappingEntity, 0, null, null, GetRelationsForField("RuleCappingCollectionViaRuleCappingToTicket"), "RuleCappingCollectionViaRuleCappingToTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketGroupCollectionViaTicketToGroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketToGroupEntityUsingTicketID;
				intermediateRelation.SetAliases(string.Empty, "TicketToGroup_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketGroupCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketGroupEntity, 0, null, null, GetRelationsForField("TicketGroupCollectionViaTicketToGroup"), "TicketGroupCollectionViaTicketToGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Organization'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganizationCollectionViaTicketOrganization
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketOrganizationEntityUsingTicketID;
				intermediateRelation.SetAliases(string.Empty, "TicketOrganization_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.OrganizationEntity, 0, null, null, GetRelationsForField("OrganizationCollectionViaTicketOrganization"), "OrganizationCollectionViaTicketOrganization", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementQuerySetting'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementQuerySettings
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.SettlementQuerySettingToTicketEntityUsingTicketID;
				intermediateRelation.SetAliases(string.Empty, "SettlementQuerySettingToTicket_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.SettlementQuerySettingEntity, 0, null, null, GetRelationsForField("SettlementQuerySettings"), "SettlementQuerySettings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleInspection
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleInspection")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.BusinessRuleEntity, 0, null, null, null, "BusinessRuleInspection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleTestBoarding
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleTestBoarding")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.BusinessRuleEntity, 0, null, null, null, "BusinessRuleTestBoarding", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRule
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleCollection(), (IEntityRelation)GetRelationsForField("BusinessRule")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.BusinessRuleEntity, 0, null, null, null, "BusinessRule", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleTicketAssignment
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleTicketAssignment")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.BusinessRuleEntity, 0, null, null, null, "BusinessRuleTicketAssignment", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Calendar'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCalendar
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CalendarCollection(), (IEntityRelation)GetRelationsForField("Calendar")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.CalendarEntity, 0, null, null, null, "Calendar", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceClassCollection(), (IEntityRelation)GetRelationsForField("DeviceClass")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.DeviceClassEntity, 0, null, null, null, "DeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Eticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEticket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EticketCollection(), (IEntityRelation)GetRelationsForField("Eticket")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.EticketEntity, 0, null, null, null, "Eticket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionCategory
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection(), (IEntityRelation)GetRelationsForField("FareEvasionCategory")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.FareEvasionCategoryEntity, 0, null, null, null, "FareEvasionCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrix'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareMatrix
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixCollection(), (IEntityRelation)GetRelationsForField("FareMatrix")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntity, 0, null, null, null, "FareMatrix", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareTable'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareTable1
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareTableCollection(), (IEntityRelation)GetRelationsForField("FareTable1")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.FareTableEntity, 0, null, null, null, "FareTable1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareTable'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareTable2
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareTableCollection(), (IEntityRelation)GetRelationsForField("FareTable2")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.FareTableEntity, 0, null, null, null, "FareTable2", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCancellationLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("CancellationLayout")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "CancellationLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGroupLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("GroupLayout")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "GroupLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPrintLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("PrintLayout")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "PrintLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceiptLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("ReceiptLayout")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "ReceiptLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LineGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLineGroup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LineGroupCollection(), (IEntityRelation)GetRelationsForField("LineGroup")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.LineGroupEntity, 0, null, null, null, "LineGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageContentGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageContentGroup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PageContentGroupCollection(), (IEntityRelation)GetRelationsForField("PageContentGroup")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.PageContentGroupEntity, 0, null, null, null, "PageContentGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PriceTypeCollection(), (IEntityRelation)GetRelationsForField("PriceType")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.PriceTypeEntity, 0, null, null, null, "PriceType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RulePeriod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRulePeriod
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RulePeriodCollection(), (IEntityRelation)GetRelationsForField("RulePeriod")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.RulePeriodEntity, 0, null, null, null, "RulePeriod", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TemporalType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTemporalType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TemporalTypeCollection(), (IEntityRelation)GetRelationsForField("TemporalType")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TemporalTypeEntity, 0, null, null, null, "TemporalType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketCancellationType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketCancellationType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCancellationTypeCollection(), (IEntityRelation)GetRelationsForField("TicketCancellationType")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketCancellationTypeEntity, 0, null, null, null, "TicketCancellationType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketCategory
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCategoryCollection(), (IEntityRelation)GetRelationsForField("TicketCategory")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketCategoryEntity, 0, null, null, null, "TicketCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketSelectionMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketSelectionMode
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketSelectionModeCollection(), (IEntityRelation)GetRelationsForField("TicketSelectionMode")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketSelectionModeEntity, 0, null, null, null, "TicketSelectionMode", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketTypeCollection(), (IEntityRelation)GetRelationsForField("TicketType")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.TicketTypeEntity, 0, null, null, null, "TicketType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserKey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserKey1
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserKeyCollection(), (IEntityRelation)GetRelationsForField("UserKey1")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.UserKeyEntity, 0, null, null, null, "UserKey1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserKey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserKey2
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserKeyCollection(), (IEntityRelation)GetRelationsForField("UserKey2")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.UserKeyEntity, 0, null, null, null, "UserKey2", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardTicket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardTicketAddition
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardTicketCollection(), (IEntityRelation)GetRelationsForField("CardTicketAddition")[0], (int)VarioSL.Entities.EntityType.TicketEntity, (int)VarioSL.Entities.EntityType.CardTicketEntity, 0, null, null, null, "CardTicketAddition", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AttributeValueListID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."ATTRIBUTEVALUELISTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AttributeValueListID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.AttributeValueListID, false); }
			set	{ SetValue((int)TicketFieldIndex.AttributeValueListID, value, true); }
		}

		/// <summary> The Barcode property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."BARCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Barcode
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.Barcode, true); }
			set	{ SetValue((int)TicketFieldIndex.Barcode, value, true); }
		}

		/// <summary> The BaseFare property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."BASEFARE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BaseFare
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.BaseFare, false); }
			set	{ SetValue((int)TicketFieldIndex.BaseFare, value, true); }
		}

		/// <summary> The BrType10ID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."BRTYPE10ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BrType10ID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.BrType10ID, false); }
			set	{ SetValue((int)TicketFieldIndex.BrType10ID, value, true); }
		}

		/// <summary> The BrType1ID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."BRTYPE1ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BrType1ID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.BrType1ID, false); }
			set	{ SetValue((int)TicketFieldIndex.BrType1ID, value, true); }
		}

		/// <summary> The BrType2ID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."BRTYPE2ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BrType2ID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.BrType2ID, false); }
			set	{ SetValue((int)TicketFieldIndex.BrType2ID, value, true); }
		}

		/// <summary> The BrType9ID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."BRTYPE9ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BrType9ID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.BrType9ID, false); }
			set	{ SetValue((int)TicketFieldIndex.BrType9ID, value, true); }
		}

		/// <summary> The CalendarID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."CALENDARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CalendarID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.CalendarID, false); }
			set	{ SetValue((int)TicketFieldIndex.CalendarID, value, true); }
		}

		/// <summary> The CancellationLayoutID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."CANCELLATIONLAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationLayoutID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.CancellationLayoutID, false); }
			set	{ SetValue((int)TicketFieldIndex.CancellationLayoutID, value, true); }
		}

		/// <summary> The CancellationQuantity property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."CANCELLATIONQUANTITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationQuantity
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.CancellationQuantity, false); }
			set	{ SetValue((int)TicketFieldIndex.CancellationQuantity, value, true); }
		}

		/// <summary> The CategoryID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."CATEGORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CategoryID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.CategoryID, false); }
			set	{ SetValue((int)TicketFieldIndex.CategoryID, value, true); }
		}

		/// <summary> The Description property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.Description, true); }
			set	{ SetValue((int)TicketFieldIndex.Description, value, true); }
		}

		/// <summary> The DeviceClassID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."DEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceClassID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.DeviceClassID, false); }
			set	{ SetValue((int)TicketFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The Discount property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."DISCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Discount
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.Discount, false); }
			set	{ SetValue((int)TicketFieldIndex.Discount, value, true); }
		}

		/// <summary> The EtickCancellationQuantity property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."ETICKCANCELLATIONQUANTITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EtickCancellationQuantity
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.EtickCancellationQuantity, false); }
			set	{ SetValue((int)TicketFieldIndex.EtickCancellationQuantity, value, true); }
		}

		/// <summary> The EtickEmission property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."ETICKEMISSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> EtickEmission
		{
			get { return (Nullable<System.Int16>)GetValue((int)TicketFieldIndex.EtickEmission, false); }
			set	{ SetValue((int)TicketFieldIndex.EtickEmission, value, true); }
		}

		/// <summary> The EtickID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."ETICKID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EtickID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.EtickID, false); }
			set	{ SetValue((int)TicketFieldIndex.EtickID, value, true); }
		}

		/// <summary> The ExternalName property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."EXTERNALNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalName
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.ExternalName, true); }
			set	{ SetValue((int)TicketFieldIndex.ExternalName, value, true); }
		}

		/// <summary> The ExternalName2 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."EXTERNALNAME2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalName2
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.ExternalName2, true); }
			set	{ SetValue((int)TicketFieldIndex.ExternalName2, value, true); }
		}

		/// <summary> The ExternalName3 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."EXTERNALNAME3"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalName3
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.ExternalName3, true); }
			set	{ SetValue((int)TicketFieldIndex.ExternalName3, value, true); }
		}

		/// <summary> The ExternalName4 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."EXTERNALNAME4"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalName4
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.ExternalName4, true); }
			set	{ SetValue((int)TicketFieldIndex.ExternalName4, value, true); }
		}

		/// <summary> The ExternalName5 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."EXTERNALNAME5"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalName5
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.ExternalName5, true); }
			set	{ SetValue((int)TicketFieldIndex.ExternalName5, value, true); }
		}

		/// <summary> The ExternalName6 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."EXTERNALNAME6"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalName6
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.ExternalName6, true); }
			set	{ SetValue((int)TicketFieldIndex.ExternalName6, value, true); }
		}

		/// <summary> The ExternalName7 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."EXTERNALNAME7"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalName7
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.ExternalName7, true); }
			set	{ SetValue((int)TicketFieldIndex.ExternalName7, value, true); }
		}

		/// <summary> The ExternalName8 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."EXTERNALNAME8"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalName8
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.ExternalName8, true); }
			set	{ SetValue((int)TicketFieldIndex.ExternalName8, value, true); }
		}

		/// <summary> The ExternalName9 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."EXTERNALNAME9"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalName9
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.ExternalName9, true); }
			set	{ SetValue((int)TicketFieldIndex.ExternalName9, value, true); }
		}

		/// <summary> The ExternalNumber property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."EXTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ExternalNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.ExternalNumber, false); }
			set	{ SetValue((int)TicketFieldIndex.ExternalNumber, value, true); }
		}

		/// <summary> The FareEvasionCategoryID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."FAREEVASIONCATEGORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareEvasionCategoryID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.FareEvasionCategoryID, false); }
			set	{ SetValue((int)TicketFieldIndex.FareEvasionCategoryID, value, true); }
		}

		/// <summary> The FareStageListID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."FARESTAGELISTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareStageListID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.FareStageListID, false); }
			set	{ SetValue((int)TicketFieldIndex.FareStageListID, value, true); }
		}

		/// <summary> The FareTable2ID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."FARETABLE2ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareTable2ID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.FareTable2ID, false); }
			set	{ SetValue((int)TicketFieldIndex.FareTable2ID, value, true); }
		}

		/// <summary> The FareTableID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."FARETABLEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareTableID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.FareTableID, false); }
			set	{ SetValue((int)TicketFieldIndex.FareTableID, value, true); }
		}

		/// <summary> The FormularName property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."FORMULARNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FormularName
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.FormularName, true); }
			set	{ SetValue((int)TicketFieldIndex.FormularName, value, true); }
		}

		/// <summary> The GroupLayoutID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."GROUPLAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> GroupLayoutID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.GroupLayoutID, false); }
			set	{ SetValue((int)TicketFieldIndex.GroupLayoutID, value, true); }
		}

		/// <summary> The InfoText1 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."INFOTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String InfoText1
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.InfoText1, true); }
			set	{ SetValue((int)TicketFieldIndex.InfoText1, value, true); }
		}

		/// <summary> The InfoText2 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."INFOTEXT2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String InfoText2
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.InfoText2, true); }
			set	{ SetValue((int)TicketFieldIndex.InfoText2, value, true); }
		}

		/// <summary> The InternalNumber property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."INTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 InternalNumber
		{
			get { return (System.Int64)GetValue((int)TicketFieldIndex.InternalNumber, true); }
			set	{ SetValue((int)TicketFieldIndex.InternalNumber, value, true); }
		}

		/// <summary> The IsOfflineAvailable property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."ISOFFLINEAVAILABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsOfflineAvailable
		{
			get { return (Nullable<System.Boolean>)GetValue((int)TicketFieldIndex.IsOfflineAvailable, false); }
			set	{ SetValue((int)TicketFieldIndex.IsOfflineAvailable, value, true); }
		}

		/// <summary> The IsSupplement property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."ISSUPPLEMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsSupplement
		{
			get { return (System.Boolean)GetValue((int)TicketFieldIndex.IsSupplement, true); }
			set	{ SetValue((int)TicketFieldIndex.IsSupplement, value, true); }
		}

		/// <summary> The Key1 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."KEY1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Key1
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.Key1, false); }
			set	{ SetValue((int)TicketFieldIndex.Key1, value, true); }
		}

		/// <summary> The Key2 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."KEY2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Key2
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.Key2, false); }
			set	{ SetValue((int)TicketFieldIndex.Key2, value, true); }
		}

		/// <summary> The LineGroupID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."LINEGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.LineGroupID, false); }
			set	{ SetValue((int)TicketFieldIndex.LineGroupID, value, true); }
		}

		/// <summary> The MatrixID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."MATRIXID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MatrixID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.MatrixID, false); }
			set	{ SetValue((int)TicketFieldIndex.MatrixID, value, true); }
		}

		/// <summary> The MaxFactor property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."MAXFACTOR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MaxFactor
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.MaxFactor, false); }
			set	{ SetValue((int)TicketFieldIndex.MaxFactor, value, true); }
		}

		/// <summary> The MinFactor property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."MINFACTOR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MinFactor
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.MinFactor, false); }
			set	{ SetValue((int)TicketFieldIndex.MinFactor, value, true); }
		}

		/// <summary> The Name property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.Name, true); }
			set	{ SetValue((int)TicketFieldIndex.Name, value, true); }
		}

		/// <summary> The NumberOfPrintouts property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."NUMBEROFPRINTOUTS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> NumberOfPrintouts
		{
			get { return (Nullable<System.Int16>)GetValue((int)TicketFieldIndex.NumberOfPrintouts, false); }
			set	{ SetValue((int)TicketFieldIndex.NumberOfPrintouts, value, true); }
		}

		/// <summary> The OrderNumber property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."ORDERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 OrderNumber
		{
			get { return (System.Int64)GetValue((int)TicketFieldIndex.OrderNumber, true); }
			set	{ SetValue((int)TicketFieldIndex.OrderNumber, value, true); }
		}

		/// <summary> The OwnerClientID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."OWNERCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OwnerClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.OwnerClientID, false); }
			set	{ SetValue((int)TicketFieldIndex.OwnerClientID, value, true); }
		}

		/// <summary> The PageContentGroupID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."PAGECONTENTGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PageContentGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.PageContentGroupID, false); }
			set	{ SetValue((int)TicketFieldIndex.PageContentGroupID, value, true); }
		}

		/// <summary> The Price property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."PRICE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Price
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.Price, false); }
			set	{ SetValue((int)TicketFieldIndex.Price, value, true); }
		}

		/// <summary> The Price2 property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."PRICE2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Price2
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.Price2, false); }
			set	{ SetValue((int)TicketFieldIndex.Price2, value, true); }
		}

		/// <summary> The PriceTypeID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."PRICETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PriceTypeID
		{
			get { return (Nullable<System.Int32>)GetValue((int)TicketFieldIndex.PriceTypeID, false); }
			set	{ SetValue((int)TicketFieldIndex.PriceTypeID, value, true); }
		}

		/// <summary> The PrintLayoutID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."PRINTLAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PrintLayoutID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.PrintLayoutID, false); }
			set	{ SetValue((int)TicketFieldIndex.PrintLayoutID, value, true); }
		}

		/// <summary> The PrintText property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."PRINTTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrintText
		{
			get { return (System.String)GetValue((int)TicketFieldIndex.PrintText, true); }
			set	{ SetValue((int)TicketFieldIndex.PrintText, value, true); }
		}

		/// <summary> The ReceiptLayoutID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."RECEIPTLAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReceiptLayoutID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.ReceiptLayoutID, false); }
			set	{ SetValue((int)TicketFieldIndex.ReceiptLayoutID, value, true); }
		}

		/// <summary> The RulePeriodID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."RULEPERIODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RulePeriodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.RulePeriodID, false); }
			set	{ SetValue((int)TicketFieldIndex.RulePeriodID, value, true); }
		}

		/// <summary> The SelectionMode property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."SELECTIONMODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SelectionMode
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.SelectionMode, false); }
			set	{ SetValue((int)TicketFieldIndex.SelectionMode, value, true); }
		}

		/// <summary> The ServiceID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."SERVICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ServiceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.ServiceID, false); }
			set	{ SetValue((int)TicketFieldIndex.ServiceID, value, true); }
		}

		/// <summary> The ShortCode property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."SHORTCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ShortCode
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.ShortCode, false); }
			set	{ SetValue((int)TicketFieldIndex.ShortCode, value, true); }
		}

		/// <summary> The TarifID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TarifID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.TarifID, false); }
			set	{ SetValue((int)TicketFieldIndex.TarifID, value, true); }
		}

		/// <summary> The TemporalTypeID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."TEMPORALTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TemporalTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.TemporalTypeID, false); }
			set	{ SetValue((int)TicketFieldIndex.TemporalTypeID, value, true); }
		}

		/// <summary> The TicketCancellationTypeID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."TICKETCANCELLATIONTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketCancellationTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.TicketCancellationTypeID, false); }
			set	{ SetValue((int)TicketFieldIndex.TicketCancellationTypeID, value, true); }
		}

		/// <summary> The TicketID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."TICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 TicketID
		{
			get { return (System.Int64)GetValue((int)TicketFieldIndex.TicketID, true); }
			set	{ SetValue((int)TicketFieldIndex.TicketID, value, true); }
		}

		/// <summary> The TicketPropertyID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."TICKETPROPERTYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketPropertyID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.TicketPropertyID, false); }
			set	{ SetValue((int)TicketFieldIndex.TicketPropertyID, value, true); }
		}

		/// <summary> The TicketTypeID property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."TICKETTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.TicketTypeID, false); }
			set	{ SetValue((int)TicketFieldIndex.TicketTypeID, value, true); }
		}

		/// <summary> The TripFrequency property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."TRIPFREQUENCY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> TripFrequency
		{
			get { return (Nullable<System.Double>)GetValue((int)TicketFieldIndex.TripFrequency, false); }
			set	{ SetValue((int)TicketFieldIndex.TripFrequency, value, true); }
		}

		/// <summary> The ValuePassCredit property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."VALUEPASSCREDIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ValuePassCredit
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketFieldIndex.ValuePassCredit, false); }
			set	{ SetValue((int)TicketFieldIndex.ValuePassCredit, value, true); }
		}

		/// <summary> The Vat property of the Entity Ticket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET"."VAT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> Vat
		{
			get { return (Nullable<System.Double>)GetValue((int)TicketFieldIndex.Vat, false); }
			set	{ SetValue((int)TicketFieldIndex.Vat, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiApportionmentResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ApportionmentResultCollection ApportionmentResults
		{
			get	{ return GetMultiApportionmentResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ApportionmentResults. When set to true, ApportionmentResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApportionmentResults is accessed. You can always execute/ a forced fetch by calling GetMultiApportionmentResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApportionmentResults
		{
			get	{ return _alwaysFetchApportionmentResults; }
			set	{ _alwaysFetchApportionmentResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApportionmentResults already has been fetched. Setting this property to false when ApportionmentResults has been fetched
		/// will clear the ApportionmentResults collection well. Setting this property to true while ApportionmentResults hasn't been fetched disables lazy loading for ApportionmentResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApportionmentResults
		{
			get { return _alreadyFetchedApportionmentResults;}
			set 
			{
				if(_alreadyFetchedApportionmentResults && !value && (_apportionmentResults != null))
				{
					_apportionmentResults.Clear();
				}
				_alreadyFetchedApportionmentResults = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AttributeValueListEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributeValueList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeValueListCollection AttributeValueList
		{
			get	{ return GetMultiAttributeValueList(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValueList. When set to true, AttributeValueList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValueList is accessed. You can always execute/ a forced fetch by calling GetMultiAttributeValueList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValueList
		{
			get	{ return _alwaysFetchAttributeValueList; }
			set	{ _alwaysFetchAttributeValueList = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValueList already has been fetched. Setting this property to false when AttributeValueList has been fetched
		/// will clear the AttributeValueList collection well. Setting this property to true while AttributeValueList hasn't been fetched disables lazy loading for AttributeValueList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValueList
		{
			get { return _alreadyFetchedAttributeValueList;}
			set 
			{
				if(_alreadyFetchedAttributeValueList && !value && (_attributeValueList != null))
				{
					_attributeValueList.Clear();
				}
				_alreadyFetchedAttributeValueList = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardTicketToTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardTicketToTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection CardTicketToTicket
		{
			get	{ return GetMultiCardTicketToTicket(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardTicketToTicket. When set to true, CardTicketToTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardTicketToTicket is accessed. You can always execute/ a forced fetch by calling GetMultiCardTicketToTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardTicketToTicket
		{
			get	{ return _alwaysFetchCardTicketToTicket; }
			set	{ _alwaysFetchCardTicketToTicket = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardTicketToTicket already has been fetched. Setting this property to false when CardTicketToTicket has been fetched
		/// will clear the CardTicketToTicket collection well. Setting this property to true while CardTicketToTicket hasn't been fetched disables lazy loading for CardTicketToTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardTicketToTicket
		{
			get { return _alreadyFetchedCardTicketToTicket;}
			set 
			{
				if(_alreadyFetchedCardTicketToTicket && !value && (_cardTicketToTicket != null))
				{
					_cardTicketToTicket.Clear();
				}
				_alreadyFetchedCardTicketToTicket = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiChoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ChoiceCollection Choices
		{
			get	{ return GetMultiChoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Choices. When set to true, Choices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Choices is accessed. You can always execute/ a forced fetch by calling GetMultiChoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchChoices
		{
			get	{ return _alwaysFetchChoices; }
			set	{ _alwaysFetchChoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Choices already has been fetched. Setting this property to false when Choices has been fetched
		/// will clear the Choices collection well. Setting this property to true while Choices hasn't been fetched disables lazy loading for Choices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedChoices
		{
			get { return _alreadyFetchedChoices;}
			set 
			{
				if(_alreadyFetchedChoices && !value && (_choices != null))
				{
					_choices.Clear();
				}
				_alreadyFetchedChoices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterTicketCollection ParameterTicket
		{
			get	{ return GetMultiParameterTicket(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterTicket. When set to true, ParameterTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterTicket is accessed. You can always execute/ a forced fetch by calling GetMultiParameterTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterTicket
		{
			get	{ return _alwaysFetchParameterTicket; }
			set	{ _alwaysFetchParameterTicket = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterTicket already has been fetched. Setting this property to false when ParameterTicket has been fetched
		/// will clear the ParameterTicket collection well. Setting this property to true while ParameterTicket hasn't been fetched disables lazy loading for ParameterTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterTicket
		{
			get { return _alreadyFetchedParameterTicket;}
			set 
			{
				if(_alreadyFetchedParameterTicket && !value && (_parameterTicket != null))
				{
					_parameterTicket.Clear();
				}
				_alreadyFetchedParameterTicket = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPrimalKeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PrimalKeyCollection PrimalKeys
		{
			get	{ return GetMultiPrimalKeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PrimalKeys. When set to true, PrimalKeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PrimalKeys is accessed. You can always execute/ a forced fetch by calling GetMultiPrimalKeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPrimalKeys
		{
			get	{ return _alwaysFetchPrimalKeys; }
			set	{ _alwaysFetchPrimalKeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PrimalKeys already has been fetched. Setting this property to false when PrimalKeys has been fetched
		/// will clear the PrimalKeys collection well. Setting this property to true while PrimalKeys hasn't been fetched disables lazy loading for PrimalKeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPrimalKeys
		{
			get { return _alreadyFetchedPrimalKeys;}
			set 
			{
				if(_alreadyFetchedPrimalKeys && !value && (_primalKeys != null))
				{
					_primalKeys.Clear();
				}
				_alreadyFetchedPrimalKeys = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RuleCappingToTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleCappingToTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingToTicketCollection RuleCappingToTicket
		{
			get	{ return GetMultiRuleCappingToTicket(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleCappingToTicket. When set to true, RuleCappingToTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleCappingToTicket is accessed. You can always execute/ a forced fetch by calling GetMultiRuleCappingToTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleCappingToTicket
		{
			get	{ return _alwaysFetchRuleCappingToTicket; }
			set	{ _alwaysFetchRuleCappingToTicket = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleCappingToTicket already has been fetched. Setting this property to false when RuleCappingToTicket has been fetched
		/// will clear the RuleCappingToTicket collection well. Setting this property to true while RuleCappingToTicket hasn't been fetched disables lazy loading for RuleCappingToTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleCappingToTicket
		{
			get { return _alreadyFetchedRuleCappingToTicket;}
			set 
			{
				if(_alreadyFetchedRuleCappingToTicket && !value && (_ruleCappingToTicket != null))
				{
					_ruleCappingToTicket.Clear();
				}
				_alreadyFetchedRuleCappingToTicket = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDayTypeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDayTypes()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDayTypeCollection TicketDayTypes
		{
			get	{ return GetMultiTicketDayTypes(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDayTypes. When set to true, TicketDayTypes is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDayTypes is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDayTypes(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDayTypes
		{
			get	{ return _alwaysFetchTicketDayTypes; }
			set	{ _alwaysFetchTicketDayTypes = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDayTypes already has been fetched. Setting this property to false when TicketDayTypes has been fetched
		/// will clear the TicketDayTypes collection well. Setting this property to true while TicketDayTypes hasn't been fetched disables lazy loading for TicketDayTypes</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDayTypes
		{
			get { return _alreadyFetchedTicketDayTypes;}
			set 
			{
				if(_alreadyFetchedTicketDayTypes && !value && (_ticketDayTypes != null))
				{
					_ticketDayTypes.Clear();
				}
				_alreadyFetchedTicketDayTypes = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceClasses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceClasses
		{
			get	{ return GetMultiTicketDeviceClasses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceClasses. When set to true, TicketDeviceClasses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceClasses is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceClasses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceClasses
		{
			get	{ return _alwaysFetchTicketDeviceClasses; }
			set	{ _alwaysFetchTicketDeviceClasses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceClasses already has been fetched. Setting this property to false when TicketDeviceClasses has been fetched
		/// will clear the TicketDeviceClasses collection well. Setting this property to true while TicketDeviceClasses hasn't been fetched disables lazy loading for TicketDeviceClasses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceClasses
		{
			get { return _alreadyFetchedTicketDeviceClasses;}
			set 
			{
				if(_alreadyFetchedTicketDeviceClasses && !value && (_ticketDeviceClasses != null))
				{
					_ticketDeviceClasses.Clear();
				}
				_alreadyFetchedTicketDeviceClasses = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDevicePaymentMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDevicePaymentMethods()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDevicePaymentMethodCollection TicketDevicePaymentMethods
		{
			get	{ return GetMultiTicketDevicePaymentMethods(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDevicePaymentMethods. When set to true, TicketDevicePaymentMethods is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDevicePaymentMethods is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDevicePaymentMethods(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDevicePaymentMethods
		{
			get	{ return _alwaysFetchTicketDevicePaymentMethods; }
			set	{ _alwaysFetchTicketDevicePaymentMethods = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDevicePaymentMethods already has been fetched. Setting this property to false when TicketDevicePaymentMethods has been fetched
		/// will clear the TicketDevicePaymentMethods collection well. Setting this property to true while TicketDevicePaymentMethods hasn't been fetched disables lazy loading for TicketDevicePaymentMethods</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDevicePaymentMethods
		{
			get { return _alreadyFetchedTicketDevicePaymentMethods;}
			set 
			{
				if(_alreadyFetchedTicketDevicePaymentMethods && !value && (_ticketDevicePaymentMethods != null))
				{
					_ticketDevicePaymentMethods.Clear();
				}
				_alreadyFetchedTicketDevicePaymentMethods = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketOrganizationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketOrganizations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketOrganizationCollection TicketOrganizations
		{
			get	{ return GetMultiTicketOrganizations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketOrganizations. When set to true, TicketOrganizations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketOrganizations is accessed. You can always execute/ a forced fetch by calling GetMultiTicketOrganizations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketOrganizations
		{
			get	{ return _alwaysFetchTicketOrganizations; }
			set	{ _alwaysFetchTicketOrganizations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketOrganizations already has been fetched. Setting this property to false when TicketOrganizations has been fetched
		/// will clear the TicketOrganizations collection well. Setting this property to true while TicketOrganizations hasn't been fetched disables lazy loading for TicketOrganizations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketOrganizations
		{
			get { return _alreadyFetchedTicketOrganizations;}
			set 
			{
				if(_alreadyFetchedTicketOrganizations && !value && (_ticketOrganizations != null))
				{
					_ticketOrganizations.Clear();
				}
				_alreadyFetchedTicketOrganizations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketOutputdeviceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketOutputDevices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection TicketOutputDevices
		{
			get	{ return GetMultiTicketOutputDevices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketOutputDevices. When set to true, TicketOutputDevices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketOutputDevices is accessed. You can always execute/ a forced fetch by calling GetMultiTicketOutputDevices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketOutputDevices
		{
			get	{ return _alwaysFetchTicketOutputDevices; }
			set	{ _alwaysFetchTicketOutputDevices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketOutputDevices already has been fetched. Setting this property to false when TicketOutputDevices has been fetched
		/// will clear the TicketOutputDevices collection well. Setting this property to true while TicketOutputDevices hasn't been fetched disables lazy loading for TicketOutputDevices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketOutputDevices
		{
			get { return _alreadyFetchedTicketOutputDevices;}
			set 
			{
				if(_alreadyFetchedTicketOutputDevices && !value && (_ticketOutputDevices != null))
				{
					_ticketOutputDevices.Clear();
				}
				_alreadyFetchedTicketOutputDevices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketPaymentIntervalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketPaymentIntervals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection TicketPaymentIntervals
		{
			get	{ return GetMultiTicketPaymentIntervals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketPaymentIntervals. When set to true, TicketPaymentIntervals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketPaymentIntervals is accessed. You can always execute/ a forced fetch by calling GetMultiTicketPaymentIntervals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketPaymentIntervals
		{
			get	{ return _alwaysFetchTicketPaymentIntervals; }
			set	{ _alwaysFetchTicketPaymentIntervals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketPaymentIntervals already has been fetched. Setting this property to false when TicketPaymentIntervals has been fetched
		/// will clear the TicketPaymentIntervals collection well. Setting this property to true while TicketPaymentIntervals hasn't been fetched disables lazy loading for TicketPaymentIntervals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketPaymentIntervals
		{
			get { return _alreadyFetchedTicketPaymentIntervals;}
			set 
			{
				if(_alreadyFetchedTicketPaymentIntervals && !value && (_ticketPaymentIntervals != null))
				{
					_ticketPaymentIntervals.Clear();
				}
				_alreadyFetchedTicketPaymentIntervals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketPhysicalCardTypeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketPhysicalCardTypes()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection TicketPhysicalCardTypes
		{
			get	{ return GetMultiTicketPhysicalCardTypes(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketPhysicalCardTypes. When set to true, TicketPhysicalCardTypes is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketPhysicalCardTypes is accessed. You can always execute/ a forced fetch by calling GetMultiTicketPhysicalCardTypes(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketPhysicalCardTypes
		{
			get	{ return _alwaysFetchTicketPhysicalCardTypes; }
			set	{ _alwaysFetchTicketPhysicalCardTypes = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketPhysicalCardTypes already has been fetched. Setting this property to false when TicketPhysicalCardTypes has been fetched
		/// will clear the TicketPhysicalCardTypes collection well. Setting this property to true while TicketPhysicalCardTypes hasn't been fetched disables lazy loading for TicketPhysicalCardTypes</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketPhysicalCardTypes
		{
			get { return _alreadyFetchedTicketPhysicalCardTypes;}
			set 
			{
				if(_alreadyFetchedTicketPhysicalCardTypes && !value && (_ticketPhysicalCardTypes != null))
				{
					_ticketPhysicalCardTypes.Clear();
				}
				_alreadyFetchedTicketPhysicalCardTypes = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketServicesPermittedEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketServicesPermitteds()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection TicketServicesPermitteds
		{
			get	{ return GetMultiTicketServicesPermitteds(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketServicesPermitteds. When set to true, TicketServicesPermitteds is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketServicesPermitteds is accessed. You can always execute/ a forced fetch by calling GetMultiTicketServicesPermitteds(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketServicesPermitteds
		{
			get	{ return _alwaysFetchTicketServicesPermitteds; }
			set	{ _alwaysFetchTicketServicesPermitteds = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketServicesPermitteds already has been fetched. Setting this property to false when TicketServicesPermitteds has been fetched
		/// will clear the TicketServicesPermitteds collection well. Setting this property to true while TicketServicesPermitteds hasn't been fetched disables lazy loading for TicketServicesPermitteds</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketServicesPermitteds
		{
			get { return _alreadyFetchedTicketServicesPermitteds;}
			set 
			{
				if(_alreadyFetchedTicketServicesPermitteds && !value && (_ticketServicesPermitteds != null))
				{
					_ticketServicesPermitteds.Clear();
				}
				_alreadyFetchedTicketServicesPermitteds = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketToGroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketToGroups()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketToGroupCollection TicketToGroups
		{
			get	{ return GetMultiTicketToGroups(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketToGroups. When set to true, TicketToGroups is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketToGroups is accessed. You can always execute/ a forced fetch by calling GetMultiTicketToGroups(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketToGroups
		{
			get	{ return _alwaysFetchTicketToGroups; }
			set	{ _alwaysFetchTicketToGroups = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketToGroups already has been fetched. Setting this property to false when TicketToGroups has been fetched
		/// will clear the TicketToGroups collection well. Setting this property to true while TicketToGroups hasn't been fetched disables lazy loading for TicketToGroups</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketToGroups
		{
			get { return _alreadyFetchedTicketToGroups;}
			set 
			{
				if(_alreadyFetchedTicketToGroups && !value && (_ticketToGroups != null))
				{
					_ticketToGroups.Clear();
				}
				_alreadyFetchedTicketToGroups = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketVendingClientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketVendingClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketVendingClientCollection TicketVendingClient
		{
			get	{ return GetMultiTicketVendingClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketVendingClient. When set to true, TicketVendingClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketVendingClient is accessed. You can always execute/ a forced fetch by calling GetMultiTicketVendingClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketVendingClient
		{
			get	{ return _alwaysFetchTicketVendingClient; }
			set	{ _alwaysFetchTicketVendingClient = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketVendingClient already has been fetched. Setting this property to false when TicketVendingClient has been fetched
		/// will clear the TicketVendingClient collection well. Setting this property to true while TicketVendingClient hasn't been fetched disables lazy loading for TicketVendingClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketVendingClient
		{
			get { return _alreadyFetchedTicketVendingClient;}
			set 
			{
				if(_alreadyFetchedTicketVendingClient && !value && (_ticketVendingClient != null))
				{
					_ticketVendingClient.Clear();
				}
				_alreadyFetchedTicketVendingClient = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvKeySet()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvKeySetCollection VdvKeySet
		{
			get	{ return GetMultiVdvKeySet(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvKeySet. When set to true, VdvKeySet is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvKeySet is accessed. You can always execute/ a forced fetch by calling GetMultiVdvKeySet(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvKeySet
		{
			get	{ return _alwaysFetchVdvKeySet; }
			set	{ _alwaysFetchVdvKeySet = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvKeySet already has been fetched. Setting this property to false when VdvKeySet has been fetched
		/// will clear the VdvKeySet collection well. Setting this property to true while VdvKeySet hasn't been fetched disables lazy loading for VdvKeySet</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvKeySet
		{
			get { return _alreadyFetchedVdvKeySet;}
			set 
			{
				if(_alreadyFetchedVdvKeySet && !value && (_vdvKeySet != null))
				{
					_vdvKeySet.Clear();
				}
				_alreadyFetchedVdvKeySet = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection VdvProduct
		{
			get	{ return GetMultiVdvProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvProduct. When set to true, VdvProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvProduct is accessed. You can always execute/ a forced fetch by calling GetMultiVdvProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvProduct
		{
			get	{ return _alwaysFetchVdvProduct; }
			set	{ _alwaysFetchVdvProduct = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvProduct already has been fetched. Setting this property to false when VdvProduct has been fetched
		/// will clear the VdvProduct collection well. Setting this property to true while VdvProduct hasn't been fetched disables lazy loading for VdvProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvProduct
		{
			get { return _alreadyFetchedVdvProduct;}
			set 
			{
				if(_alreadyFetchedVdvProduct && !value && (_vdvProduct != null))
				{
					_vdvProduct.Clear();
				}
				_alreadyFetchedVdvProduct = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTaxProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection TaxProducts
		{
			get	{ return GetMultiTaxProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TaxProducts. When set to true, TaxProducts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TaxProducts is accessed. You can always execute/ a forced fetch by calling GetMultiTaxProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTaxProducts
		{
			get	{ return _alwaysFetchTaxProducts; }
			set	{ _alwaysFetchTaxProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TaxProducts already has been fetched. Setting this property to false when TaxProducts has been fetched
		/// will clear the TaxProducts collection well. Setting this property to true while TaxProducts hasn't been fetched disables lazy loading for TaxProducts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTaxProducts
		{
			get { return _alreadyFetchedTaxProducts;}
			set 
			{
				if(_alreadyFetchedTaxProducts && !value && (_taxProducts != null))
				{
					_taxProducts.Clear();
				}
				_alreadyFetchedTaxProducts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection Products
		{
			get	{ return GetMultiProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Products. When set to true, Products is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Products is accessed. You can always execute/ a forced fetch by calling GetMultiProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProducts
		{
			get	{ return _alwaysFetchProducts; }
			set	{ _alwaysFetchProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Products already has been fetched. Setting this property to false when Products has been fetched
		/// will clear the Products collection well. Setting this property to true while Products hasn't been fetched disables lazy loading for Products</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProducts
		{
			get { return _alreadyFetchedProducts;}
			set 
			{
				if(_alreadyFetchedProducts && !value && (_products != null))
				{
					_products.Clear();
				}
				_alreadyFetchedProducts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingToTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementQuerySettingToTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection SettlementQuerySettingToTickets
		{
			get	{ return GetMultiSettlementQuerySettingToTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementQuerySettingToTickets. When set to true, SettlementQuerySettingToTickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementQuerySettingToTickets is accessed. You can always execute/ a forced fetch by calling GetMultiSettlementQuerySettingToTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementQuerySettingToTickets
		{
			get	{ return _alwaysFetchSettlementQuerySettingToTickets; }
			set	{ _alwaysFetchSettlementQuerySettingToTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementQuerySettingToTickets already has been fetched. Setting this property to false when SettlementQuerySettingToTickets has been fetched
		/// will clear the SettlementQuerySettingToTickets collection well. Setting this property to true while SettlementQuerySettingToTickets hasn't been fetched disables lazy loading for SettlementQuerySettingToTickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementQuerySettingToTickets
		{
			get { return _alreadyFetchedSettlementQuerySettingToTickets;}
			set 
			{
				if(_alreadyFetchedSettlementQuerySettingToTickets && !value && (_settlementQuerySettingToTickets != null))
				{
					_settlementQuerySettingToTickets.Clear();
				}
				_alreadyFetchedSettlementQuerySettingToTickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketSerialNumbers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection TicketSerialNumbers
		{
			get	{ return GetMultiTicketSerialNumbers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketSerialNumbers. When set to true, TicketSerialNumbers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketSerialNumbers is accessed. You can always execute/ a forced fetch by calling GetMultiTicketSerialNumbers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketSerialNumbers
		{
			get	{ return _alwaysFetchTicketSerialNumbers; }
			set	{ _alwaysFetchTicketSerialNumbers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketSerialNumbers already has been fetched. Setting this property to false when TicketSerialNumbers has been fetched
		/// will clear the TicketSerialNumbers collection well. Setting this property to true while TicketSerialNumbers hasn't been fetched disables lazy loading for TicketSerialNumbers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketSerialNumbers
		{
			get { return _alreadyFetchedTicketSerialNumbers;}
			set 
			{
				if(_alreadyFetchedTicketSerialNumbers && !value && (_ticketSerialNumbers != null))
				{
					_ticketSerialNumbers.Clear();
				}
				_alreadyFetchedTicketSerialNumbers = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournalsForAppliedPassTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TransactionJournalsForAppliedPassTicket
		{
			get	{ return GetMultiTransactionJournalsForAppliedPassTicket(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournalsForAppliedPassTicket. When set to true, TransactionJournalsForAppliedPassTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournalsForAppliedPassTicket is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournalsForAppliedPassTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournalsForAppliedPassTicket
		{
			get	{ return _alwaysFetchTransactionJournalsForAppliedPassTicket; }
			set	{ _alwaysFetchTransactionJournalsForAppliedPassTicket = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournalsForAppliedPassTicket already has been fetched. Setting this property to false when TransactionJournalsForAppliedPassTicket has been fetched
		/// will clear the TransactionJournalsForAppliedPassTicket collection well. Setting this property to true while TransactionJournalsForAppliedPassTicket hasn't been fetched disables lazy loading for TransactionJournalsForAppliedPassTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournalsForAppliedPassTicket
		{
			get { return _alreadyFetchedTransactionJournalsForAppliedPassTicket;}
			set 
			{
				if(_alreadyFetchedTransactionJournalsForAppliedPassTicket && !value && (_transactionJournalsForAppliedPassTicket != null))
				{
					_transactionJournalsForAppliedPassTicket.Clear();
				}
				_alreadyFetchedTransactionJournalsForAppliedPassTicket = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TransactionJournals
		{
			get	{ return GetMultiTransactionJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournals. When set to true, TransactionJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournals is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournals
		{
			get	{ return _alwaysFetchTransactionJournals; }
			set	{ _alwaysFetchTransactionJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournals already has been fetched. Setting this property to false when TransactionJournals has been fetched
		/// will clear the TransactionJournals collection well. Setting this property to true while TransactionJournals hasn't been fetched disables lazy loading for TransactionJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournals
		{
			get { return _alreadyFetchedTransactionJournals;}
			set 
			{
				if(_alreadyFetchedTransactionJournals && !value && (_transactionJournals != null))
				{
					_transactionJournals.Clear();
				}
				_alreadyFetchedTransactionJournals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournals_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TransactionJournals_
		{
			get	{ return GetMultiTransactionJournals_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournals_. When set to true, TransactionJournals_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournals_ is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournals_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournals_
		{
			get	{ return _alwaysFetchTransactionJournals_; }
			set	{ _alwaysFetchTransactionJournals_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournals_ already has been fetched. Setting this property to false when TransactionJournals_ has been fetched
		/// will clear the TransactionJournals_ collection well. Setting this property to true while TransactionJournals_ hasn't been fetched disables lazy loading for TransactionJournals_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournals_
		{
			get { return _alreadyFetchedTransactionJournals_;}
			set 
			{
				if(_alreadyFetchedTransactionJournals_ && !value && (_transactionJournals_ != null))
				{
					_transactionJournals_.Clear();
				}
				_alreadyFetchedTransactionJournals_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VoucherEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVouchers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VoucherCollection Vouchers
		{
			get	{ return GetMultiVouchers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Vouchers. When set to true, Vouchers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Vouchers is accessed. You can always execute/ a forced fetch by calling GetMultiVouchers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVouchers
		{
			get	{ return _alwaysFetchVouchers; }
			set	{ _alwaysFetchVouchers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Vouchers already has been fetched. Setting this property to false when Vouchers has been fetched
		/// will clear the Vouchers collection well. Setting this property to true while Vouchers hasn't been fetched disables lazy loading for Vouchers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVouchers
		{
			get { return _alreadyFetchedVouchers;}
			set 
			{
				if(_alreadyFetchedVouchers && !value && (_vouchers != null))
				{
					_vouchers.Clear();
				}
				_alreadyFetchedVouchers = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributevalueCollectionViaAttributeValueList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeValueCollection AttributevalueCollectionViaAttributeValueList
		{
			get { return GetMultiAttributevalueCollectionViaAttributeValueList(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttributevalueCollectionViaAttributeValueList. When set to true, AttributevalueCollectionViaAttributeValueList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributevalueCollectionViaAttributeValueList is accessed. You can always execute a forced fetch by calling GetMultiAttributevalueCollectionViaAttributeValueList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributevalueCollectionViaAttributeValueList
		{
			get	{ return _alwaysFetchAttributevalueCollectionViaAttributeValueList; }
			set	{ _alwaysFetchAttributevalueCollectionViaAttributeValueList = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributevalueCollectionViaAttributeValueList already has been fetched. Setting this property to false when AttributevalueCollectionViaAttributeValueList has been fetched
		/// will clear the AttributevalueCollectionViaAttributeValueList collection well. Setting this property to true while AttributevalueCollectionViaAttributeValueList hasn't been fetched disables lazy loading for AttributevalueCollectionViaAttributeValueList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributevalueCollectionViaAttributeValueList
		{
			get { return _alreadyFetchedAttributevalueCollectionViaAttributeValueList;}
			set 
			{
				if(_alreadyFetchedAttributevalueCollectionViaAttributeValueList && !value && (_attributevalueCollectionViaAttributeValueList != null))
				{
					_attributevalueCollectionViaAttributeValueList.Clear();
				}
				_alreadyFetchedAttributevalueCollectionViaAttributeValueList = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CardChipTypeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardChipTypeCollectionViaTicketPhysicalCardType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardChipTypeCollection CardChipTypeCollectionViaTicketPhysicalCardType
		{
			get { return GetMultiCardChipTypeCollectionViaTicketPhysicalCardType(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardChipTypeCollectionViaTicketPhysicalCardType. When set to true, CardChipTypeCollectionViaTicketPhysicalCardType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardChipTypeCollectionViaTicketPhysicalCardType is accessed. You can always execute a forced fetch by calling GetMultiCardChipTypeCollectionViaTicketPhysicalCardType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardChipTypeCollectionViaTicketPhysicalCardType
		{
			get	{ return _alwaysFetchCardChipTypeCollectionViaTicketPhysicalCardType; }
			set	{ _alwaysFetchCardChipTypeCollectionViaTicketPhysicalCardType = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardChipTypeCollectionViaTicketPhysicalCardType already has been fetched. Setting this property to false when CardChipTypeCollectionViaTicketPhysicalCardType has been fetched
		/// will clear the CardChipTypeCollectionViaTicketPhysicalCardType collection well. Setting this property to true while CardChipTypeCollectionViaTicketPhysicalCardType hasn't been fetched disables lazy loading for CardChipTypeCollectionViaTicketPhysicalCardType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType
		{
			get { return _alreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType;}
			set 
			{
				if(_alreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType && !value && (_cardChipTypeCollectionViaTicketPhysicalCardType != null))
				{
					_cardChipTypeCollectionViaTicketPhysicalCardType.Clear();
				}
				_alreadyFetchedCardChipTypeCollectionViaTicketPhysicalCardType = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardAdditionsTicketIsUsedIn()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardTicketCollection CardAdditionsTicketIsUsedIn
		{
			get { return GetMultiCardAdditionsTicketIsUsedIn(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardAdditionsTicketIsUsedIn. When set to true, CardAdditionsTicketIsUsedIn is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardAdditionsTicketIsUsedIn is accessed. You can always execute a forced fetch by calling GetMultiCardAdditionsTicketIsUsedIn(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardAdditionsTicketIsUsedIn
		{
			get	{ return _alwaysFetchCardAdditionsTicketIsUsedIn; }
			set	{ _alwaysFetchCardAdditionsTicketIsUsedIn = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardAdditionsTicketIsUsedIn already has been fetched. Setting this property to false when CardAdditionsTicketIsUsedIn has been fetched
		/// will clear the CardAdditionsTicketIsUsedIn collection well. Setting this property to true while CardAdditionsTicketIsUsedIn hasn't been fetched disables lazy loading for CardAdditionsTicketIsUsedIn</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardAdditionsTicketIsUsedIn
		{
			get { return _alreadyFetchedCardAdditionsTicketIsUsedIn;}
			set 
			{
				if(_alreadyFetchedCardAdditionsTicketIsUsedIn && !value && (_cardAdditionsTicketIsUsedIn != null))
				{
					_cardAdditionsTicketIsUsedIn.Clear();
				}
				_alreadyFetchedCardAdditionsTicketIsUsedIn = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollectionViaTicketVendingClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClientCollection ClientCollectionViaTicketVendingClient
		{
			get { return GetMultiClientCollectionViaTicketVendingClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollectionViaTicketVendingClient. When set to true, ClientCollectionViaTicketVendingClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollectionViaTicketVendingClient is accessed. You can always execute a forced fetch by calling GetMultiClientCollectionViaTicketVendingClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollectionViaTicketVendingClient
		{
			get	{ return _alwaysFetchClientCollectionViaTicketVendingClient; }
			set	{ _alwaysFetchClientCollectionViaTicketVendingClient = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollectionViaTicketVendingClient already has been fetched. Setting this property to false when ClientCollectionViaTicketVendingClient has been fetched
		/// will clear the ClientCollectionViaTicketVendingClient collection well. Setting this property to true while ClientCollectionViaTicketVendingClient hasn't been fetched disables lazy loading for ClientCollectionViaTicketVendingClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollectionViaTicketVendingClient
		{
			get { return _alreadyFetchedClientCollectionViaTicketVendingClient;}
			set 
			{
				if(_alreadyFetchedClientCollectionViaTicketVendingClient && !value && (_clientCollectionViaTicketVendingClient != null))
				{
					_clientCollectionViaTicketVendingClient.Clear();
				}
				_alreadyFetchedClientCollectionViaTicketVendingClient = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'LineEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLineCollectionViaTicketServicesPermitted()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.LineCollection LineCollectionViaTicketServicesPermitted
		{
			get { return GetMultiLineCollectionViaTicketServicesPermitted(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for LineCollectionViaTicketServicesPermitted. When set to true, LineCollectionViaTicketServicesPermitted is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LineCollectionViaTicketServicesPermitted is accessed. You can always execute a forced fetch by calling GetMultiLineCollectionViaTicketServicesPermitted(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLineCollectionViaTicketServicesPermitted
		{
			get	{ return _alwaysFetchLineCollectionViaTicketServicesPermitted; }
			set	{ _alwaysFetchLineCollectionViaTicketServicesPermitted = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LineCollectionViaTicketServicesPermitted already has been fetched. Setting this property to false when LineCollectionViaTicketServicesPermitted has been fetched
		/// will clear the LineCollectionViaTicketServicesPermitted collection well. Setting this property to true while LineCollectionViaTicketServicesPermitted hasn't been fetched disables lazy loading for LineCollectionViaTicketServicesPermitted</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLineCollectionViaTicketServicesPermitted
		{
			get { return _alreadyFetchedLineCollectionViaTicketServicesPermitted;}
			set 
			{
				if(_alreadyFetchedLineCollectionViaTicketServicesPermitted && !value && (_lineCollectionViaTicketServicesPermitted != null))
				{
					_lineCollectionViaTicketServicesPermitted.Clear();
				}
				_alreadyFetchedLineCollectionViaTicketServicesPermitted = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PaymentIntervalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentIntervalCollectionViaTicketPaymentInterval()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentIntervalCollection PaymentIntervalCollectionViaTicketPaymentInterval
		{
			get { return GetMultiPaymentIntervalCollectionViaTicketPaymentInterval(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentIntervalCollectionViaTicketPaymentInterval. When set to true, PaymentIntervalCollectionViaTicketPaymentInterval is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentIntervalCollectionViaTicketPaymentInterval is accessed. You can always execute a forced fetch by calling GetMultiPaymentIntervalCollectionViaTicketPaymentInterval(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentIntervalCollectionViaTicketPaymentInterval
		{
			get	{ return _alwaysFetchPaymentIntervalCollectionViaTicketPaymentInterval; }
			set	{ _alwaysFetchPaymentIntervalCollectionViaTicketPaymentInterval = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentIntervalCollectionViaTicketPaymentInterval already has been fetched. Setting this property to false when PaymentIntervalCollectionViaTicketPaymentInterval has been fetched
		/// will clear the PaymentIntervalCollectionViaTicketPaymentInterval collection well. Setting this property to true while PaymentIntervalCollectionViaTicketPaymentInterval hasn't been fetched disables lazy loading for PaymentIntervalCollectionViaTicketPaymentInterval</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval
		{
			get { return _alreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval;}
			set 
			{
				if(_alreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval && !value && (_paymentIntervalCollectionViaTicketPaymentInterval != null))
				{
					_paymentIntervalCollectionViaTicketPaymentInterval.Clear();
				}
				_alreadyFetchedPaymentIntervalCollectionViaTicketPaymentInterval = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleCappingCollectionViaRuleCappingToTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingCollection RuleCappingCollectionViaRuleCappingToTicket
		{
			get { return GetMultiRuleCappingCollectionViaRuleCappingToTicket(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleCappingCollectionViaRuleCappingToTicket. When set to true, RuleCappingCollectionViaRuleCappingToTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleCappingCollectionViaRuleCappingToTicket is accessed. You can always execute a forced fetch by calling GetMultiRuleCappingCollectionViaRuleCappingToTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleCappingCollectionViaRuleCappingToTicket
		{
			get	{ return _alwaysFetchRuleCappingCollectionViaRuleCappingToTicket; }
			set	{ _alwaysFetchRuleCappingCollectionViaRuleCappingToTicket = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleCappingCollectionViaRuleCappingToTicket already has been fetched. Setting this property to false when RuleCappingCollectionViaRuleCappingToTicket has been fetched
		/// will clear the RuleCappingCollectionViaRuleCappingToTicket collection well. Setting this property to true while RuleCappingCollectionViaRuleCappingToTicket hasn't been fetched disables lazy loading for RuleCappingCollectionViaRuleCappingToTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleCappingCollectionViaRuleCappingToTicket
		{
			get { return _alreadyFetchedRuleCappingCollectionViaRuleCappingToTicket;}
			set 
			{
				if(_alreadyFetchedRuleCappingCollectionViaRuleCappingToTicket && !value && (_ruleCappingCollectionViaRuleCappingToTicket != null))
				{
					_ruleCappingCollectionViaRuleCappingToTicket.Clear();
				}
				_alreadyFetchedRuleCappingCollectionViaRuleCappingToTicket = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketGroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketGroupCollectionViaTicketToGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketGroupCollection TicketGroupCollectionViaTicketToGroup
		{
			get { return GetMultiTicketGroupCollectionViaTicketToGroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketGroupCollectionViaTicketToGroup. When set to true, TicketGroupCollectionViaTicketToGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketGroupCollectionViaTicketToGroup is accessed. You can always execute a forced fetch by calling GetMultiTicketGroupCollectionViaTicketToGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketGroupCollectionViaTicketToGroup
		{
			get	{ return _alwaysFetchTicketGroupCollectionViaTicketToGroup; }
			set	{ _alwaysFetchTicketGroupCollectionViaTicketToGroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketGroupCollectionViaTicketToGroup already has been fetched. Setting this property to false when TicketGroupCollectionViaTicketToGroup has been fetched
		/// will clear the TicketGroupCollectionViaTicketToGroup collection well. Setting this property to true while TicketGroupCollectionViaTicketToGroup hasn't been fetched disables lazy loading for TicketGroupCollectionViaTicketToGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketGroupCollectionViaTicketToGroup
		{
			get { return _alreadyFetchedTicketGroupCollectionViaTicketToGroup;}
			set 
			{
				if(_alreadyFetchedTicketGroupCollectionViaTicketToGroup && !value && (_ticketGroupCollectionViaTicketToGroup != null))
				{
					_ticketGroupCollectionViaTicketToGroup.Clear();
				}
				_alreadyFetchedTicketGroupCollectionViaTicketToGroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrganizationCollectionViaTicketOrganization()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationCollection OrganizationCollectionViaTicketOrganization
		{
			get { return GetMultiOrganizationCollectionViaTicketOrganization(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrganizationCollectionViaTicketOrganization. When set to true, OrganizationCollectionViaTicketOrganization is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrganizationCollectionViaTicketOrganization is accessed. You can always execute a forced fetch by calling GetMultiOrganizationCollectionViaTicketOrganization(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganizationCollectionViaTicketOrganization
		{
			get	{ return _alwaysFetchOrganizationCollectionViaTicketOrganization; }
			set	{ _alwaysFetchOrganizationCollectionViaTicketOrganization = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrganizationCollectionViaTicketOrganization already has been fetched. Setting this property to false when OrganizationCollectionViaTicketOrganization has been fetched
		/// will clear the OrganizationCollectionViaTicketOrganization collection well. Setting this property to true while OrganizationCollectionViaTicketOrganization hasn't been fetched disables lazy loading for OrganizationCollectionViaTicketOrganization</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganizationCollectionViaTicketOrganization
		{
			get { return _alreadyFetchedOrganizationCollectionViaTicketOrganization;}
			set 
			{
				if(_alreadyFetchedOrganizationCollectionViaTicketOrganization && !value && (_organizationCollectionViaTicketOrganization != null))
				{
					_organizationCollectionViaTicketOrganization.Clear();
				}
				_alreadyFetchedOrganizationCollectionViaTicketOrganization = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementQuerySettings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection SettlementQuerySettings
		{
			get { return GetMultiSettlementQuerySettings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementQuerySettings. When set to true, SettlementQuerySettings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementQuerySettings is accessed. You can always execute a forced fetch by calling GetMultiSettlementQuerySettings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementQuerySettings
		{
			get	{ return _alwaysFetchSettlementQuerySettings; }
			set	{ _alwaysFetchSettlementQuerySettings = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementQuerySettings already has been fetched. Setting this property to false when SettlementQuerySettings has been fetched
		/// will clear the SettlementQuerySettings collection well. Setting this property to true while SettlementQuerySettings hasn't been fetched disables lazy loading for SettlementQuerySettings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementQuerySettings
		{
			get { return _alreadyFetchedSettlementQuerySettings;}
			set 
			{
				if(_alreadyFetchedSettlementQuerySettings && !value && (_settlementQuerySettings != null))
				{
					_settlementQuerySettings.Clear();
				}
				_alreadyFetchedSettlementQuerySettings = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'BusinessRuleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBusinessRuleInspection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BusinessRuleEntity BusinessRuleInspection
		{
			get	{ return GetSingleBusinessRuleInspection(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBusinessRuleInspection(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketsInspection", "BusinessRuleInspection", _businessRuleInspection, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleInspection. When set to true, BusinessRuleInspection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleInspection is accessed. You can always execute a forced fetch by calling GetSingleBusinessRuleInspection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleInspection
		{
			get	{ return _alwaysFetchBusinessRuleInspection; }
			set	{ _alwaysFetchBusinessRuleInspection = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleInspection already has been fetched. Setting this property to false when BusinessRuleInspection has been fetched
		/// will set BusinessRuleInspection to null as well. Setting this property to true while BusinessRuleInspection hasn't been fetched disables lazy loading for BusinessRuleInspection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleInspection
		{
			get { return _alreadyFetchedBusinessRuleInspection;}
			set 
			{
				if(_alreadyFetchedBusinessRuleInspection && !value)
				{
					this.BusinessRuleInspection = null;
				}
				_alreadyFetchedBusinessRuleInspection = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BusinessRuleInspection is not found
		/// in the database. When set to true, BusinessRuleInspection will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BusinessRuleInspectionReturnsNewIfNotFound
		{
			get	{ return _businessRuleInspectionReturnsNewIfNotFound; }
			set { _businessRuleInspectionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BusinessRuleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBusinessRuleTestBoarding()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BusinessRuleEntity BusinessRuleTestBoarding
		{
			get	{ return GetSingleBusinessRuleTestBoarding(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBusinessRuleTestBoarding(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketsTestBoarding", "BusinessRuleTestBoarding", _businessRuleTestBoarding, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleTestBoarding. When set to true, BusinessRuleTestBoarding is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleTestBoarding is accessed. You can always execute a forced fetch by calling GetSingleBusinessRuleTestBoarding(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleTestBoarding
		{
			get	{ return _alwaysFetchBusinessRuleTestBoarding; }
			set	{ _alwaysFetchBusinessRuleTestBoarding = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleTestBoarding already has been fetched. Setting this property to false when BusinessRuleTestBoarding has been fetched
		/// will set BusinessRuleTestBoarding to null as well. Setting this property to true while BusinessRuleTestBoarding hasn't been fetched disables lazy loading for BusinessRuleTestBoarding</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleTestBoarding
		{
			get { return _alreadyFetchedBusinessRuleTestBoarding;}
			set 
			{
				if(_alreadyFetchedBusinessRuleTestBoarding && !value)
				{
					this.BusinessRuleTestBoarding = null;
				}
				_alreadyFetchedBusinessRuleTestBoarding = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BusinessRuleTestBoarding is not found
		/// in the database. When set to true, BusinessRuleTestBoarding will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BusinessRuleTestBoardingReturnsNewIfNotFound
		{
			get	{ return _businessRuleTestBoardingReturnsNewIfNotFound; }
			set { _businessRuleTestBoardingReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BusinessRuleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBusinessRule()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BusinessRuleEntity BusinessRule
		{
			get	{ return GetSingleBusinessRule(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBusinessRule(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketsTestSale", "BusinessRule", _businessRule, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRule. When set to true, BusinessRule is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRule is accessed. You can always execute a forced fetch by calling GetSingleBusinessRule(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRule
		{
			get	{ return _alwaysFetchBusinessRule; }
			set	{ _alwaysFetchBusinessRule = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRule already has been fetched. Setting this property to false when BusinessRule has been fetched
		/// will set BusinessRule to null as well. Setting this property to true while BusinessRule hasn't been fetched disables lazy loading for BusinessRule</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRule
		{
			get { return _alreadyFetchedBusinessRule;}
			set 
			{
				if(_alreadyFetchedBusinessRule && !value)
				{
					this.BusinessRule = null;
				}
				_alreadyFetchedBusinessRule = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BusinessRule is not found
		/// in the database. When set to true, BusinessRule will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BusinessRuleReturnsNewIfNotFound
		{
			get	{ return _businessRuleReturnsNewIfNotFound; }
			set { _businessRuleReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BusinessRuleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBusinessRuleTicketAssignment()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BusinessRuleEntity BusinessRuleTicketAssignment
		{
			get	{ return GetSingleBusinessRuleTicketAssignment(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBusinessRuleTicketAssignment(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketsTicketAssignment", "BusinessRuleTicketAssignment", _businessRuleTicketAssignment, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleTicketAssignment. When set to true, BusinessRuleTicketAssignment is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleTicketAssignment is accessed. You can always execute a forced fetch by calling GetSingleBusinessRuleTicketAssignment(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleTicketAssignment
		{
			get	{ return _alwaysFetchBusinessRuleTicketAssignment; }
			set	{ _alwaysFetchBusinessRuleTicketAssignment = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleTicketAssignment already has been fetched. Setting this property to false when BusinessRuleTicketAssignment has been fetched
		/// will set BusinessRuleTicketAssignment to null as well. Setting this property to true while BusinessRuleTicketAssignment hasn't been fetched disables lazy loading for BusinessRuleTicketAssignment</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleTicketAssignment
		{
			get { return _alreadyFetchedBusinessRuleTicketAssignment;}
			set 
			{
				if(_alreadyFetchedBusinessRuleTicketAssignment && !value)
				{
					this.BusinessRuleTicketAssignment = null;
				}
				_alreadyFetchedBusinessRuleTicketAssignment = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BusinessRuleTicketAssignment is not found
		/// in the database. When set to true, BusinessRuleTicketAssignment will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BusinessRuleTicketAssignmentReturnsNewIfNotFound
		{
			get	{ return _businessRuleTicketAssignmentReturnsNewIfNotFound; }
			set { _businessRuleTicketAssignmentReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CalendarEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCalendar()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CalendarEntity Calendar
		{
			get	{ return GetSingleCalendar(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCalendar(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "Calendar", _calendar, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Calendar. When set to true, Calendar is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Calendar is accessed. You can always execute a forced fetch by calling GetSingleCalendar(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCalendar
		{
			get	{ return _alwaysFetchCalendar; }
			set	{ _alwaysFetchCalendar = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Calendar already has been fetched. Setting this property to false when Calendar has been fetched
		/// will set Calendar to null as well. Setting this property to true while Calendar hasn't been fetched disables lazy loading for Calendar</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCalendar
		{
			get { return _alreadyFetchedCalendar;}
			set 
			{
				if(_alreadyFetchedCalendar && !value)
				{
					this.Calendar = null;
				}
				_alreadyFetchedCalendar = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Calendar is not found
		/// in the database. When set to true, Calendar will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CalendarReturnsNewIfNotFound
		{
			get	{ return _calendarReturnsNewIfNotFound; }
			set { _calendarReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceClassEntity DeviceClass
		{
			get	{ return GetSingleDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "DeviceClass", _deviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceClass. When set to true, DeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceClass is accessed. You can always execute a forced fetch by calling GetSingleDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceClass
		{
			get	{ return _alwaysFetchDeviceClass; }
			set	{ _alwaysFetchDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceClass already has been fetched. Setting this property to false when DeviceClass has been fetched
		/// will set DeviceClass to null as well. Setting this property to true while DeviceClass hasn't been fetched disables lazy loading for DeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceClass
		{
			get { return _alreadyFetchedDeviceClass;}
			set 
			{
				if(_alreadyFetchedDeviceClass && !value)
				{
					this.DeviceClass = null;
				}
				_alreadyFetchedDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceClass is not found
		/// in the database. When set to true, DeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceClassReturnsNewIfNotFound
		{
			get	{ return _deviceClassReturnsNewIfNotFound; }
			set { _deviceClassReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EticketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEticket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual EticketEntity Eticket
		{
			get	{ return GetSingleEticket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEticket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "Eticket", _eticket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Eticket. When set to true, Eticket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Eticket is accessed. You can always execute a forced fetch by calling GetSingleEticket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEticket
		{
			get	{ return _alwaysFetchEticket; }
			set	{ _alwaysFetchEticket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Eticket already has been fetched. Setting this property to false when Eticket has been fetched
		/// will set Eticket to null as well. Setting this property to true while Eticket hasn't been fetched disables lazy loading for Eticket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEticket
		{
			get { return _alreadyFetchedEticket;}
			set 
			{
				if(_alreadyFetchedEticket && !value)
				{
					this.Eticket = null;
				}
				_alreadyFetchedEticket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Eticket is not found
		/// in the database. When set to true, Eticket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool EticketReturnsNewIfNotFound
		{
			get	{ return _eticketReturnsNewIfNotFound; }
			set { _eticketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareEvasionCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareEvasionCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareEvasionCategoryEntity FareEvasionCategory
		{
			get	{ return GetSingleFareEvasionCategory(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareEvasionCategory(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "FareEvasionCategory", _fareEvasionCategory, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionCategory. When set to true, FareEvasionCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionCategory is accessed. You can always execute a forced fetch by calling GetSingleFareEvasionCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionCategory
		{
			get	{ return _alwaysFetchFareEvasionCategory; }
			set	{ _alwaysFetchFareEvasionCategory = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionCategory already has been fetched. Setting this property to false when FareEvasionCategory has been fetched
		/// will set FareEvasionCategory to null as well. Setting this property to true while FareEvasionCategory hasn't been fetched disables lazy loading for FareEvasionCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionCategory
		{
			get { return _alreadyFetchedFareEvasionCategory;}
			set 
			{
				if(_alreadyFetchedFareEvasionCategory && !value)
				{
					this.FareEvasionCategory = null;
				}
				_alreadyFetchedFareEvasionCategory = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareEvasionCategory is not found
		/// in the database. When set to true, FareEvasionCategory will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareEvasionCategoryReturnsNewIfNotFound
		{
			get	{ return _fareEvasionCategoryReturnsNewIfNotFound; }
			set { _fareEvasionCategoryReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareMatrixEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareMatrix()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareMatrixEntity FareMatrix
		{
			get	{ return GetSingleFareMatrix(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareMatrix(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "FareMatrix", _fareMatrix, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareMatrix. When set to true, FareMatrix is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareMatrix is accessed. You can always execute a forced fetch by calling GetSingleFareMatrix(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareMatrix
		{
			get	{ return _alwaysFetchFareMatrix; }
			set	{ _alwaysFetchFareMatrix = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareMatrix already has been fetched. Setting this property to false when FareMatrix has been fetched
		/// will set FareMatrix to null as well. Setting this property to true while FareMatrix hasn't been fetched disables lazy loading for FareMatrix</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareMatrix
		{
			get { return _alreadyFetchedFareMatrix;}
			set 
			{
				if(_alreadyFetchedFareMatrix && !value)
				{
					this.FareMatrix = null;
				}
				_alreadyFetchedFareMatrix = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareMatrix is not found
		/// in the database. When set to true, FareMatrix will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareMatrixReturnsNewIfNotFound
		{
			get	{ return _fareMatrixReturnsNewIfNotFound; }
			set { _fareMatrixReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareTableEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareTable1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareTableEntity FareTable1
		{
			get	{ return GetSingleFareTable1(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareTable1(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketsFareTable1", "FareTable1", _fareTable1, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareTable1. When set to true, FareTable1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareTable1 is accessed. You can always execute a forced fetch by calling GetSingleFareTable1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareTable1
		{
			get	{ return _alwaysFetchFareTable1; }
			set	{ _alwaysFetchFareTable1 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareTable1 already has been fetched. Setting this property to false when FareTable1 has been fetched
		/// will set FareTable1 to null as well. Setting this property to true while FareTable1 hasn't been fetched disables lazy loading for FareTable1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareTable1
		{
			get { return _alreadyFetchedFareTable1;}
			set 
			{
				if(_alreadyFetchedFareTable1 && !value)
				{
					this.FareTable1 = null;
				}
				_alreadyFetchedFareTable1 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareTable1 is not found
		/// in the database. When set to true, FareTable1 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareTable1ReturnsNewIfNotFound
		{
			get	{ return _fareTable1ReturnsNewIfNotFound; }
			set { _fareTable1ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareTableEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareTable2()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareTableEntity FareTable2
		{
			get	{ return GetSingleFareTable2(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareTable2(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketsFareTable2", "FareTable2", _fareTable2, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareTable2. When set to true, FareTable2 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareTable2 is accessed. You can always execute a forced fetch by calling GetSingleFareTable2(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareTable2
		{
			get	{ return _alwaysFetchFareTable2; }
			set	{ _alwaysFetchFareTable2 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareTable2 already has been fetched. Setting this property to false when FareTable2 has been fetched
		/// will set FareTable2 to null as well. Setting this property to true while FareTable2 hasn't been fetched disables lazy loading for FareTable2</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareTable2
		{
			get { return _alreadyFetchedFareTable2;}
			set 
			{
				if(_alreadyFetchedFareTable2 && !value)
				{
					this.FareTable2 = null;
				}
				_alreadyFetchedFareTable2 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareTable2 is not found
		/// in the database. When set to true, FareTable2 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareTable2ReturnsNewIfNotFound
		{
			get	{ return _fareTable2ReturnsNewIfNotFound; }
			set { _fareTable2ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCancellationLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity CancellationLayout
		{
			get	{ return GetSingleCancellationLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCancellationLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketsCancellationLayout", "CancellationLayout", _cancellationLayout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CancellationLayout. When set to true, CancellationLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CancellationLayout is accessed. You can always execute a forced fetch by calling GetSingleCancellationLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCancellationLayout
		{
			get	{ return _alwaysFetchCancellationLayout; }
			set	{ _alwaysFetchCancellationLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CancellationLayout already has been fetched. Setting this property to false when CancellationLayout has been fetched
		/// will set CancellationLayout to null as well. Setting this property to true while CancellationLayout hasn't been fetched disables lazy loading for CancellationLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCancellationLayout
		{
			get { return _alreadyFetchedCancellationLayout;}
			set 
			{
				if(_alreadyFetchedCancellationLayout && !value)
				{
					this.CancellationLayout = null;
				}
				_alreadyFetchedCancellationLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CancellationLayout is not found
		/// in the database. When set to true, CancellationLayout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CancellationLayoutReturnsNewIfNotFound
		{
			get	{ return _cancellationLayoutReturnsNewIfNotFound; }
			set { _cancellationLayoutReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGroupLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity GroupLayout
		{
			get	{ return GetSingleGroupLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGroupLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketsGroupLayout", "GroupLayout", _groupLayout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GroupLayout. When set to true, GroupLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GroupLayout is accessed. You can always execute a forced fetch by calling GetSingleGroupLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGroupLayout
		{
			get	{ return _alwaysFetchGroupLayout; }
			set	{ _alwaysFetchGroupLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GroupLayout already has been fetched. Setting this property to false when GroupLayout has been fetched
		/// will set GroupLayout to null as well. Setting this property to true while GroupLayout hasn't been fetched disables lazy loading for GroupLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGroupLayout
		{
			get { return _alreadyFetchedGroupLayout;}
			set 
			{
				if(_alreadyFetchedGroupLayout && !value)
				{
					this.GroupLayout = null;
				}
				_alreadyFetchedGroupLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GroupLayout is not found
		/// in the database. When set to true, GroupLayout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool GroupLayoutReturnsNewIfNotFound
		{
			get	{ return _groupLayoutReturnsNewIfNotFound; }
			set { _groupLayoutReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePrintLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity PrintLayout
		{
			get	{ return GetSinglePrintLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPrintLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketsPrintLayout", "PrintLayout", _printLayout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PrintLayout. When set to true, PrintLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PrintLayout is accessed. You can always execute a forced fetch by calling GetSinglePrintLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPrintLayout
		{
			get	{ return _alwaysFetchPrintLayout; }
			set	{ _alwaysFetchPrintLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PrintLayout already has been fetched. Setting this property to false when PrintLayout has been fetched
		/// will set PrintLayout to null as well. Setting this property to true while PrintLayout hasn't been fetched disables lazy loading for PrintLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPrintLayout
		{
			get { return _alreadyFetchedPrintLayout;}
			set 
			{
				if(_alreadyFetchedPrintLayout && !value)
				{
					this.PrintLayout = null;
				}
				_alreadyFetchedPrintLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PrintLayout is not found
		/// in the database. When set to true, PrintLayout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PrintLayoutReturnsNewIfNotFound
		{
			get	{ return _printLayoutReturnsNewIfNotFound; }
			set { _printLayoutReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReceiptLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity ReceiptLayout
		{
			get	{ return GetSingleReceiptLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReceiptLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketsReceiptLayout", "ReceiptLayout", _receiptLayout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReceiptLayout. When set to true, ReceiptLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceiptLayout is accessed. You can always execute a forced fetch by calling GetSingleReceiptLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceiptLayout
		{
			get	{ return _alwaysFetchReceiptLayout; }
			set	{ _alwaysFetchReceiptLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceiptLayout already has been fetched. Setting this property to false when ReceiptLayout has been fetched
		/// will set ReceiptLayout to null as well. Setting this property to true while ReceiptLayout hasn't been fetched disables lazy loading for ReceiptLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceiptLayout
		{
			get { return _alreadyFetchedReceiptLayout;}
			set 
			{
				if(_alreadyFetchedReceiptLayout && !value)
				{
					this.ReceiptLayout = null;
				}
				_alreadyFetchedReceiptLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReceiptLayout is not found
		/// in the database. When set to true, ReceiptLayout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ReceiptLayoutReturnsNewIfNotFound
		{
			get	{ return _receiptLayoutReturnsNewIfNotFound; }
			set { _receiptLayoutReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LineGroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLineGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LineGroupEntity LineGroup
		{
			get	{ return GetSingleLineGroup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLineGroup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Ticket", "LineGroup", _lineGroup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for LineGroup. When set to true, LineGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LineGroup is accessed. You can always execute a forced fetch by calling GetSingleLineGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLineGroup
		{
			get	{ return _alwaysFetchLineGroup; }
			set	{ _alwaysFetchLineGroup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LineGroup already has been fetched. Setting this property to false when LineGroup has been fetched
		/// will set LineGroup to null as well. Setting this property to true while LineGroup hasn't been fetched disables lazy loading for LineGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLineGroup
		{
			get { return _alreadyFetchedLineGroup;}
			set 
			{
				if(_alreadyFetchedLineGroup && !value)
				{
					this.LineGroup = null;
				}
				_alreadyFetchedLineGroup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property LineGroup is not found
		/// in the database. When set to true, LineGroup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool LineGroupReturnsNewIfNotFound
		{
			get	{ return _lineGroupReturnsNewIfNotFound; }
			set { _lineGroupReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageContentGroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageContentGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PageContentGroupEntity PageContentGroup
		{
			get	{ return GetSinglePageContentGroup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageContentGroup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "PageContentGroup", _pageContentGroup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageContentGroup. When set to true, PageContentGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageContentGroup is accessed. You can always execute a forced fetch by calling GetSinglePageContentGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageContentGroup
		{
			get	{ return _alwaysFetchPageContentGroup; }
			set	{ _alwaysFetchPageContentGroup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageContentGroup already has been fetched. Setting this property to false when PageContentGroup has been fetched
		/// will set PageContentGroup to null as well. Setting this property to true while PageContentGroup hasn't been fetched disables lazy loading for PageContentGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageContentGroup
		{
			get { return _alreadyFetchedPageContentGroup;}
			set 
			{
				if(_alreadyFetchedPageContentGroup && !value)
				{
					this.PageContentGroup = null;
				}
				_alreadyFetchedPageContentGroup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageContentGroup is not found
		/// in the database. When set to true, PageContentGroup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PageContentGroupReturnsNewIfNotFound
		{
			get	{ return _pageContentGroupReturnsNewIfNotFound; }
			set { _pageContentGroupReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PriceTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePriceType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PriceTypeEntity PriceType
		{
			get	{ return GetSinglePriceType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPriceType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Ticket", "PriceType", _priceType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PriceType. When set to true, PriceType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceType is accessed. You can always execute a forced fetch by calling GetSinglePriceType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceType
		{
			get	{ return _alwaysFetchPriceType; }
			set	{ _alwaysFetchPriceType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceType already has been fetched. Setting this property to false when PriceType has been fetched
		/// will set PriceType to null as well. Setting this property to true while PriceType hasn't been fetched disables lazy loading for PriceType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceType
		{
			get { return _alreadyFetchedPriceType;}
			set 
			{
				if(_alreadyFetchedPriceType && !value)
				{
					this.PriceType = null;
				}
				_alreadyFetchedPriceType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PriceType is not found
		/// in the database. When set to true, PriceType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PriceTypeReturnsNewIfNotFound
		{
			get	{ return _priceTypeReturnsNewIfNotFound; }
			set { _priceTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RulePeriodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRulePeriod()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RulePeriodEntity RulePeriod
		{
			get	{ return GetSingleRulePeriod(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRulePeriod(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "RulePeriod", _rulePeriod, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RulePeriod. When set to true, RulePeriod is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RulePeriod is accessed. You can always execute a forced fetch by calling GetSingleRulePeriod(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRulePeriod
		{
			get	{ return _alwaysFetchRulePeriod; }
			set	{ _alwaysFetchRulePeriod = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RulePeriod already has been fetched. Setting this property to false when RulePeriod has been fetched
		/// will set RulePeriod to null as well. Setting this property to true while RulePeriod hasn't been fetched disables lazy loading for RulePeriod</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRulePeriod
		{
			get { return _alreadyFetchedRulePeriod;}
			set 
			{
				if(_alreadyFetchedRulePeriod && !value)
				{
					this.RulePeriod = null;
				}
				_alreadyFetchedRulePeriod = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RulePeriod is not found
		/// in the database. When set to true, RulePeriod will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RulePeriodReturnsNewIfNotFound
		{
			get	{ return _rulePeriodReturnsNewIfNotFound; }
			set { _rulePeriodReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TemporalTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTemporalType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TemporalTypeEntity TemporalType
		{
			get	{ return GetSingleTemporalType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTemporalType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "TemporalType", _temporalType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TemporalType. When set to true, TemporalType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TemporalType is accessed. You can always execute a forced fetch by calling GetSingleTemporalType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTemporalType
		{
			get	{ return _alwaysFetchTemporalType; }
			set	{ _alwaysFetchTemporalType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TemporalType already has been fetched. Setting this property to false when TemporalType has been fetched
		/// will set TemporalType to null as well. Setting this property to true while TemporalType hasn't been fetched disables lazy loading for TemporalType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTemporalType
		{
			get { return _alreadyFetchedTemporalType;}
			set 
			{
				if(_alreadyFetchedTemporalType && !value)
				{
					this.TemporalType = null;
				}
				_alreadyFetchedTemporalType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TemporalType is not found
		/// in the database. When set to true, TemporalType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TemporalTypeReturnsNewIfNotFound
		{
			get	{ return _temporalTypeReturnsNewIfNotFound; }
			set { _temporalTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketCancellationTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicketCancellationType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketCancellationTypeEntity TicketCancellationType
		{
			get	{ return GetSingleTicketCancellationType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicketCancellationType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "TicketCancellationType", _ticketCancellationType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TicketCancellationType. When set to true, TicketCancellationType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketCancellationType is accessed. You can always execute a forced fetch by calling GetSingleTicketCancellationType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketCancellationType
		{
			get	{ return _alwaysFetchTicketCancellationType; }
			set	{ _alwaysFetchTicketCancellationType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketCancellationType already has been fetched. Setting this property to false when TicketCancellationType has been fetched
		/// will set TicketCancellationType to null as well. Setting this property to true while TicketCancellationType hasn't been fetched disables lazy loading for TicketCancellationType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketCancellationType
		{
			get { return _alreadyFetchedTicketCancellationType;}
			set 
			{
				if(_alreadyFetchedTicketCancellationType && !value)
				{
					this.TicketCancellationType = null;
				}
				_alreadyFetchedTicketCancellationType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TicketCancellationType is not found
		/// in the database. When set to true, TicketCancellationType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketCancellationTypeReturnsNewIfNotFound
		{
			get	{ return _ticketCancellationTypeReturnsNewIfNotFound; }
			set { _ticketCancellationTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicketCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketCategoryEntity TicketCategory
		{
			get	{ return GetSingleTicketCategory(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicketCategory(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "TicketCategory", _ticketCategory, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TicketCategory. When set to true, TicketCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketCategory is accessed. You can always execute a forced fetch by calling GetSingleTicketCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketCategory
		{
			get	{ return _alwaysFetchTicketCategory; }
			set	{ _alwaysFetchTicketCategory = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketCategory already has been fetched. Setting this property to false when TicketCategory has been fetched
		/// will set TicketCategory to null as well. Setting this property to true while TicketCategory hasn't been fetched disables lazy loading for TicketCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketCategory
		{
			get { return _alreadyFetchedTicketCategory;}
			set 
			{
				if(_alreadyFetchedTicketCategory && !value)
				{
					this.TicketCategory = null;
				}
				_alreadyFetchedTicketCategory = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TicketCategory is not found
		/// in the database. When set to true, TicketCategory will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketCategoryReturnsNewIfNotFound
		{
			get	{ return _ticketCategoryReturnsNewIfNotFound; }
			set { _ticketCategoryReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketSelectionModeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicketSelectionMode()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketSelectionModeEntity TicketSelectionMode
		{
			get	{ return GetSingleTicketSelectionMode(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicketSelectionMode(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "TicketSelectionMode", _ticketSelectionMode, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TicketSelectionMode. When set to true, TicketSelectionMode is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketSelectionMode is accessed. You can always execute a forced fetch by calling GetSingleTicketSelectionMode(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketSelectionMode
		{
			get	{ return _alwaysFetchTicketSelectionMode; }
			set	{ _alwaysFetchTicketSelectionMode = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketSelectionMode already has been fetched. Setting this property to false when TicketSelectionMode has been fetched
		/// will set TicketSelectionMode to null as well. Setting this property to true while TicketSelectionMode hasn't been fetched disables lazy loading for TicketSelectionMode</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketSelectionMode
		{
			get { return _alreadyFetchedTicketSelectionMode;}
			set 
			{
				if(_alreadyFetchedTicketSelectionMode && !value)
				{
					this.TicketSelectionMode = null;
				}
				_alreadyFetchedTicketSelectionMode = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TicketSelectionMode is not found
		/// in the database. When set to true, TicketSelectionMode will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketSelectionModeReturnsNewIfNotFound
		{
			get	{ return _ticketSelectionModeReturnsNewIfNotFound; }
			set { _ticketSelectionModeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicketType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketTypeEntity TicketType
		{
			get	{ return GetSingleTicketType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicketType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tickets", "TicketType", _ticketType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TicketType. When set to true, TicketType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketType is accessed. You can always execute a forced fetch by calling GetSingleTicketType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketType
		{
			get	{ return _alwaysFetchTicketType; }
			set	{ _alwaysFetchTicketType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketType already has been fetched. Setting this property to false when TicketType has been fetched
		/// will set TicketType to null as well. Setting this property to true while TicketType hasn't been fetched disables lazy loading for TicketType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketType
		{
			get { return _alreadyFetchedTicketType;}
			set 
			{
				if(_alreadyFetchedTicketType && !value)
				{
					this.TicketType = null;
				}
				_alreadyFetchedTicketType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TicketType is not found
		/// in the database. When set to true, TicketType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketTypeReturnsNewIfNotFound
		{
			get	{ return _ticketTypeReturnsNewIfNotFound; }
			set { _ticketTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserKeyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserKey1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserKeyEntity UserKey1
		{
			get	{ return GetSingleUserKey1(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserKey1(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketsKey1", "UserKey1", _userKey1, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserKey1. When set to true, UserKey1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserKey1 is accessed. You can always execute a forced fetch by calling GetSingleUserKey1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserKey1
		{
			get	{ return _alwaysFetchUserKey1; }
			set	{ _alwaysFetchUserKey1 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserKey1 already has been fetched. Setting this property to false when UserKey1 has been fetched
		/// will set UserKey1 to null as well. Setting this property to true while UserKey1 hasn't been fetched disables lazy loading for UserKey1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserKey1
		{
			get { return _alreadyFetchedUserKey1;}
			set 
			{
				if(_alreadyFetchedUserKey1 && !value)
				{
					this.UserKey1 = null;
				}
				_alreadyFetchedUserKey1 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserKey1 is not found
		/// in the database. When set to true, UserKey1 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserKey1ReturnsNewIfNotFound
		{
			get	{ return _userKey1ReturnsNewIfNotFound; }
			set { _userKey1ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserKeyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserKey2()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserKeyEntity UserKey2
		{
			get	{ return GetSingleUserKey2(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserKey2(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketsKey2", "UserKey2", _userKey2, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserKey2. When set to true, UserKey2 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserKey2 is accessed. You can always execute a forced fetch by calling GetSingleUserKey2(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserKey2
		{
			get	{ return _alwaysFetchUserKey2; }
			set	{ _alwaysFetchUserKey2 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserKey2 already has been fetched. Setting this property to false when UserKey2 has been fetched
		/// will set UserKey2 to null as well. Setting this property to true while UserKey2 hasn't been fetched disables lazy loading for UserKey2</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserKey2
		{
			get { return _alreadyFetchedUserKey2;}
			set 
			{
				if(_alreadyFetchedUserKey2 && !value)
				{
					this.UserKey2 = null;
				}
				_alreadyFetchedUserKey2 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserKey2 is not found
		/// in the database. When set to true, UserKey2 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserKey2ReturnsNewIfNotFound
		{
			get	{ return _userKey2ReturnsNewIfNotFound; }
			set { _userKey2ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardTicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardTicketAddition()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardTicketEntity CardTicketAddition
		{
			get	{ return GetSingleCardTicketAddition(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncCardTicketAddition(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_cardTicketAddition !=null);
						DesetupSyncCardTicketAddition(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("CardTicketAddition");
						}
					}
					else
					{
						if(_cardTicketAddition!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "AssignedCardTicket");
							SetupSyncCardTicketAddition(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardTicketAddition. When set to true, CardTicketAddition is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardTicketAddition is accessed. You can always execute a forced fetch by calling GetSingleCardTicketAddition(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardTicketAddition
		{
			get	{ return _alwaysFetchCardTicketAddition; }
			set	{ _alwaysFetchCardTicketAddition = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property CardTicketAddition already has been fetched. Setting this property to false when CardTicketAddition has been fetched
		/// will set CardTicketAddition to null as well. Setting this property to true while CardTicketAddition hasn't been fetched disables lazy loading for CardTicketAddition</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardTicketAddition
		{
			get { return _alreadyFetchedCardTicketAddition;}
			set 
			{
				if(_alreadyFetchedCardTicketAddition && !value)
				{
					this.CardTicketAddition = null;
				}
				_alreadyFetchedCardTicketAddition = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardTicketAddition is not found
		/// in the database. When set to true, CardTicketAddition will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardTicketAdditionReturnsNewIfNotFound
		{
			get	{ return _cardTicketAdditionReturnsNewIfNotFound; }
			set	{ _cardTicketAdditionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TicketEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
