﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Component'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ComponentEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ComponentClearingCollection	_componentClearings;
		private bool	_alwaysFetchComponentClearings, _alreadyFetchedComponentClearings;
		private VarioSL.Entities.CollectionClasses.ComponentFillingCollection	_componentFillings;
		private bool	_alwaysFetchComponentFillings, _alreadyFetchedComponentFillings;
		private VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection	_paymentJournalToComponent;
		private bool	_alwaysFetchPaymentJournalToComponent, _alreadyFetchedPaymentJournalToComponent;
		private VarioSL.Entities.CollectionClasses.SaleToComponentCollection	_saleToComponents;
		private bool	_alwaysFetchSaleToComponents, _alreadyFetchedSaleToComponents;
		private AutomatEntity _automat;
		private bool	_alwaysFetchAutomat, _alreadyFetchedAutomat, _automatReturnsNewIfNotFound;
		private ComponentStateEntity _componentState;
		private bool	_alwaysFetchComponentState, _alreadyFetchedComponentState, _componentStateReturnsNewIfNotFound;
		private ComponentTypeEntity _componentType;
		private bool	_alwaysFetchComponentType, _alreadyFetchedComponentType, _componentTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Automat</summary>
			public static readonly string Automat = "Automat";
			/// <summary>Member name ComponentState</summary>
			public static readonly string ComponentState = "ComponentState";
			/// <summary>Member name ComponentType</summary>
			public static readonly string ComponentType = "ComponentType";
			/// <summary>Member name ComponentClearings</summary>
			public static readonly string ComponentClearings = "ComponentClearings";
			/// <summary>Member name ComponentFillings</summary>
			public static readonly string ComponentFillings = "ComponentFillings";
			/// <summary>Member name PaymentJournalToComponent</summary>
			public static readonly string PaymentJournalToComponent = "PaymentJournalToComponent";
			/// <summary>Member name SaleToComponents</summary>
			public static readonly string SaleToComponents = "SaleToComponents";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ComponentEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ComponentEntity() :base("ComponentEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="componentID">PK value for Component which data should be fetched into this Component object</param>
		public ComponentEntity(System.Int64 componentID):base("ComponentEntity")
		{
			InitClassFetch(componentID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="componentID">PK value for Component which data should be fetched into this Component object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ComponentEntity(System.Int64 componentID, IPrefetchPath prefetchPathToUse):base("ComponentEntity")
		{
			InitClassFetch(componentID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="componentID">PK value for Component which data should be fetched into this Component object</param>
		/// <param name="validator">The custom validator object for this ComponentEntity</param>
		public ComponentEntity(System.Int64 componentID, IValidator validator):base("ComponentEntity")
		{
			InitClassFetch(componentID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ComponentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_componentClearings = (VarioSL.Entities.CollectionClasses.ComponentClearingCollection)info.GetValue("_componentClearings", typeof(VarioSL.Entities.CollectionClasses.ComponentClearingCollection));
			_alwaysFetchComponentClearings = info.GetBoolean("_alwaysFetchComponentClearings");
			_alreadyFetchedComponentClearings = info.GetBoolean("_alreadyFetchedComponentClearings");

			_componentFillings = (VarioSL.Entities.CollectionClasses.ComponentFillingCollection)info.GetValue("_componentFillings", typeof(VarioSL.Entities.CollectionClasses.ComponentFillingCollection));
			_alwaysFetchComponentFillings = info.GetBoolean("_alwaysFetchComponentFillings");
			_alreadyFetchedComponentFillings = info.GetBoolean("_alreadyFetchedComponentFillings");

			_paymentJournalToComponent = (VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection)info.GetValue("_paymentJournalToComponent", typeof(VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection));
			_alwaysFetchPaymentJournalToComponent = info.GetBoolean("_alwaysFetchPaymentJournalToComponent");
			_alreadyFetchedPaymentJournalToComponent = info.GetBoolean("_alreadyFetchedPaymentJournalToComponent");

			_saleToComponents = (VarioSL.Entities.CollectionClasses.SaleToComponentCollection)info.GetValue("_saleToComponents", typeof(VarioSL.Entities.CollectionClasses.SaleToComponentCollection));
			_alwaysFetchSaleToComponents = info.GetBoolean("_alwaysFetchSaleToComponents");
			_alreadyFetchedSaleToComponents = info.GetBoolean("_alreadyFetchedSaleToComponents");
			_automat = (AutomatEntity)info.GetValue("_automat", typeof(AutomatEntity));
			if(_automat!=null)
			{
				_automat.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_automatReturnsNewIfNotFound = info.GetBoolean("_automatReturnsNewIfNotFound");
			_alwaysFetchAutomat = info.GetBoolean("_alwaysFetchAutomat");
			_alreadyFetchedAutomat = info.GetBoolean("_alreadyFetchedAutomat");

			_componentState = (ComponentStateEntity)info.GetValue("_componentState", typeof(ComponentStateEntity));
			if(_componentState!=null)
			{
				_componentState.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_componentStateReturnsNewIfNotFound = info.GetBoolean("_componentStateReturnsNewIfNotFound");
			_alwaysFetchComponentState = info.GetBoolean("_alwaysFetchComponentState");
			_alreadyFetchedComponentState = info.GetBoolean("_alreadyFetchedComponentState");

			_componentType = (ComponentTypeEntity)info.GetValue("_componentType", typeof(ComponentTypeEntity));
			if(_componentType!=null)
			{
				_componentType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_componentTypeReturnsNewIfNotFound = info.GetBoolean("_componentTypeReturnsNewIfNotFound");
			_alwaysFetchComponentType = info.GetBoolean("_alwaysFetchComponentType");
			_alreadyFetchedComponentType = info.GetBoolean("_alreadyFetchedComponentType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ComponentFieldIndex)fieldIndex)
			{
				case ComponentFieldIndex.AutomatID:
					DesetupSyncAutomat(true, false);
					_alreadyFetchedAutomat = false;
					break;
				case ComponentFieldIndex.ComponentTypeID:
					DesetupSyncComponentType(true, false);
					_alreadyFetchedComponentType = false;
					break;
				case ComponentFieldIndex.StatusID:
					DesetupSyncComponentState(true, false);
					_alreadyFetchedComponentState = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedComponentClearings = (_componentClearings.Count > 0);
			_alreadyFetchedComponentFillings = (_componentFillings.Count > 0);
			_alreadyFetchedPaymentJournalToComponent = (_paymentJournalToComponent.Count > 0);
			_alreadyFetchedSaleToComponents = (_saleToComponents.Count > 0);
			_alreadyFetchedAutomat = (_automat != null);
			_alreadyFetchedComponentState = (_componentState != null);
			_alreadyFetchedComponentType = (_componentType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Automat":
					toReturn.Add(Relations.AutomatEntityUsingAutomatID);
					break;
				case "ComponentState":
					toReturn.Add(Relations.ComponentStateEntityUsingStatusID);
					break;
				case "ComponentType":
					toReturn.Add(Relations.ComponentTypeEntityUsingComponentTypeID);
					break;
				case "ComponentClearings":
					toReturn.Add(Relations.ComponentClearingEntityUsingComponentID);
					break;
				case "ComponentFillings":
					toReturn.Add(Relations.ComponentFillingEntityUsingComponentID);
					break;
				case "PaymentJournalToComponent":
					toReturn.Add(Relations.PaymentJournalToComponentEntityUsingComponentID);
					break;
				case "SaleToComponents":
					toReturn.Add(Relations.SaleToComponentEntityUsingComponentID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_componentClearings", (!this.MarkedForDeletion?_componentClearings:null));
			info.AddValue("_alwaysFetchComponentClearings", _alwaysFetchComponentClearings);
			info.AddValue("_alreadyFetchedComponentClearings", _alreadyFetchedComponentClearings);
			info.AddValue("_componentFillings", (!this.MarkedForDeletion?_componentFillings:null));
			info.AddValue("_alwaysFetchComponentFillings", _alwaysFetchComponentFillings);
			info.AddValue("_alreadyFetchedComponentFillings", _alreadyFetchedComponentFillings);
			info.AddValue("_paymentJournalToComponent", (!this.MarkedForDeletion?_paymentJournalToComponent:null));
			info.AddValue("_alwaysFetchPaymentJournalToComponent", _alwaysFetchPaymentJournalToComponent);
			info.AddValue("_alreadyFetchedPaymentJournalToComponent", _alreadyFetchedPaymentJournalToComponent);
			info.AddValue("_saleToComponents", (!this.MarkedForDeletion?_saleToComponents:null));
			info.AddValue("_alwaysFetchSaleToComponents", _alwaysFetchSaleToComponents);
			info.AddValue("_alreadyFetchedSaleToComponents", _alreadyFetchedSaleToComponents);
			info.AddValue("_automat", (!this.MarkedForDeletion?_automat:null));
			info.AddValue("_automatReturnsNewIfNotFound", _automatReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAutomat", _alwaysFetchAutomat);
			info.AddValue("_alreadyFetchedAutomat", _alreadyFetchedAutomat);
			info.AddValue("_componentState", (!this.MarkedForDeletion?_componentState:null));
			info.AddValue("_componentStateReturnsNewIfNotFound", _componentStateReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchComponentState", _alwaysFetchComponentState);
			info.AddValue("_alreadyFetchedComponentState", _alreadyFetchedComponentState);
			info.AddValue("_componentType", (!this.MarkedForDeletion?_componentType:null));
			info.AddValue("_componentTypeReturnsNewIfNotFound", _componentTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchComponentType", _alwaysFetchComponentType);
			info.AddValue("_alreadyFetchedComponentType", _alreadyFetchedComponentType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Automat":
					_alreadyFetchedAutomat = true;
					this.Automat = (AutomatEntity)entity;
					break;
				case "ComponentState":
					_alreadyFetchedComponentState = true;
					this.ComponentState = (ComponentStateEntity)entity;
					break;
				case "ComponentType":
					_alreadyFetchedComponentType = true;
					this.ComponentType = (ComponentTypeEntity)entity;
					break;
				case "ComponentClearings":
					_alreadyFetchedComponentClearings = true;
					if(entity!=null)
					{
						this.ComponentClearings.Add((ComponentClearingEntity)entity);
					}
					break;
				case "ComponentFillings":
					_alreadyFetchedComponentFillings = true;
					if(entity!=null)
					{
						this.ComponentFillings.Add((ComponentFillingEntity)entity);
					}
					break;
				case "PaymentJournalToComponent":
					_alreadyFetchedPaymentJournalToComponent = true;
					if(entity!=null)
					{
						this.PaymentJournalToComponent.Add((PaymentJournalToComponentEntity)entity);
					}
					break;
				case "SaleToComponents":
					_alreadyFetchedSaleToComponents = true;
					if(entity!=null)
					{
						this.SaleToComponents.Add((SaleToComponentEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Automat":
					SetupSyncAutomat(relatedEntity);
					break;
				case "ComponentState":
					SetupSyncComponentState(relatedEntity);
					break;
				case "ComponentType":
					SetupSyncComponentType(relatedEntity);
					break;
				case "ComponentClearings":
					_componentClearings.Add((ComponentClearingEntity)relatedEntity);
					break;
				case "ComponentFillings":
					_componentFillings.Add((ComponentFillingEntity)relatedEntity);
					break;
				case "PaymentJournalToComponent":
					_paymentJournalToComponent.Add((PaymentJournalToComponentEntity)relatedEntity);
					break;
				case "SaleToComponents":
					_saleToComponents.Add((SaleToComponentEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Automat":
					DesetupSyncAutomat(false, true);
					break;
				case "ComponentState":
					DesetupSyncComponentState(false, true);
					break;
				case "ComponentType":
					DesetupSyncComponentType(false, true);
					break;
				case "ComponentClearings":
					this.PerformRelatedEntityRemoval(_componentClearings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ComponentFillings":
					this.PerformRelatedEntityRemoval(_componentFillings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentJournalToComponent":
					this.PerformRelatedEntityRemoval(_paymentJournalToComponent, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SaleToComponents":
					this.PerformRelatedEntityRemoval(_saleToComponents, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_automat!=null)
			{
				toReturn.Add(_automat);
			}
			if(_componentState!=null)
			{
				toReturn.Add(_componentState);
			}
			if(_componentType!=null)
			{
				toReturn.Add(_componentType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_componentClearings);
			toReturn.Add(_componentFillings);
			toReturn.Add(_paymentJournalToComponent);
			toReturn.Add(_saleToComponents);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentID">PK value for Component which data should be fetched into this Component object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentID)
		{
			return FetchUsingPK(componentID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentID">PK value for Component which data should be fetched into this Component object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(componentID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentID">PK value for Component which data should be fetched into this Component object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(componentID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentID">PK value for Component which data should be fetched into this Component object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(componentID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ComponentID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ComponentRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ComponentClearingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearings(bool forceFetch)
		{
			return GetMultiComponentClearings(forceFetch, _componentClearings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ComponentClearingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiComponentClearings(forceFetch, _componentClearings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiComponentClearings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedComponentClearings || forceFetch || _alwaysFetchComponentClearings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_componentClearings);
				_componentClearings.SuppressClearInGetMulti=!forceFetch;
				_componentClearings.EntityFactoryToUse = entityFactoryToUse;
				_componentClearings.GetMultiManyToOne(null, this, null, null, filter);
				_componentClearings.SuppressClearInGetMulti=false;
				_alreadyFetchedComponentClearings = true;
			}
			return _componentClearings;
		}

		/// <summary> Sets the collection parameters for the collection for 'ComponentClearings'. These settings will be taken into account
		/// when the property ComponentClearings is requested or GetMultiComponentClearings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersComponentClearings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_componentClearings.SortClauses=sortClauses;
			_componentClearings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ComponentFillingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillings(bool forceFetch)
		{
			return GetMultiComponentFillings(forceFetch, _componentFillings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ComponentFillingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiComponentFillings(forceFetch, _componentFillings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiComponentFillings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedComponentFillings || forceFetch || _alwaysFetchComponentFillings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_componentFillings);
				_componentFillings.SuppressClearInGetMulti=!forceFetch;
				_componentFillings.EntityFactoryToUse = entityFactoryToUse;
				_componentFillings.GetMultiManyToOne(null, null, this, null, null, filter);
				_componentFillings.SuppressClearInGetMulti=false;
				_alreadyFetchedComponentFillings = true;
			}
			return _componentFillings;
		}

		/// <summary> Sets the collection parameters for the collection for 'ComponentFillings'. These settings will be taken into account
		/// when the property ComponentFillings is requested or GetMultiComponentFillings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersComponentFillings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_componentFillings.SortClauses=sortClauses;
			_componentFillings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalToComponentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection GetMultiPaymentJournalToComponent(bool forceFetch)
		{
			return GetMultiPaymentJournalToComponent(forceFetch, _paymentJournalToComponent.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalToComponentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection GetMultiPaymentJournalToComponent(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentJournalToComponent(forceFetch, _paymentJournalToComponent.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection GetMultiPaymentJournalToComponent(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentJournalToComponent(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection GetMultiPaymentJournalToComponent(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentJournalToComponent || forceFetch || _alwaysFetchPaymentJournalToComponent) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentJournalToComponent);
				_paymentJournalToComponent.SuppressClearInGetMulti=!forceFetch;
				_paymentJournalToComponent.EntityFactoryToUse = entityFactoryToUse;
				_paymentJournalToComponent.GetMultiManyToOne(this, null, filter);
				_paymentJournalToComponent.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentJournalToComponent = true;
			}
			return _paymentJournalToComponent;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentJournalToComponent'. These settings will be taken into account
		/// when the property PaymentJournalToComponent is requested or GetMultiPaymentJournalToComponent is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentJournalToComponent(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentJournalToComponent.SortClauses=sortClauses;
			_paymentJournalToComponent.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SaleToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SaleToComponentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleToComponentCollection GetMultiSaleToComponents(bool forceFetch)
		{
			return GetMultiSaleToComponents(forceFetch, _saleToComponents.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SaleToComponentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleToComponentCollection GetMultiSaleToComponents(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSaleToComponents(forceFetch, _saleToComponents.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SaleToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SaleToComponentCollection GetMultiSaleToComponents(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSaleToComponents(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleToComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SaleToComponentCollection GetMultiSaleToComponents(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSaleToComponents || forceFetch || _alwaysFetchSaleToComponents) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_saleToComponents);
				_saleToComponents.SuppressClearInGetMulti=!forceFetch;
				_saleToComponents.EntityFactoryToUse = entityFactoryToUse;
				_saleToComponents.GetMultiManyToOne(this, null, filter);
				_saleToComponents.SuppressClearInGetMulti=false;
				_alreadyFetchedSaleToComponents = true;
			}
			return _saleToComponents;
		}

		/// <summary> Sets the collection parameters for the collection for 'SaleToComponents'. These settings will be taken into account
		/// when the property SaleToComponents is requested or GetMultiSaleToComponents is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSaleToComponents(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_saleToComponents.SortClauses=sortClauses;
			_saleToComponents.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AutomatEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AutomatEntity' which is related to this entity.</returns>
		public AutomatEntity GetSingleAutomat()
		{
			return GetSingleAutomat(false);
		}

		/// <summary> Retrieves the related entity of type 'AutomatEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AutomatEntity' which is related to this entity.</returns>
		public virtual AutomatEntity GetSingleAutomat(bool forceFetch)
		{
			if( ( !_alreadyFetchedAutomat || forceFetch || _alwaysFetchAutomat) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AutomatEntityUsingAutomatID);
				AutomatEntity newEntity = new AutomatEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AutomatID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AutomatEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_automatReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Automat = newEntity;
				_alreadyFetchedAutomat = fetchResult;
			}
			return _automat;
		}


		/// <summary> Retrieves the related entity of type 'ComponentStateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ComponentStateEntity' which is related to this entity.</returns>
		public ComponentStateEntity GetSingleComponentState()
		{
			return GetSingleComponentState(false);
		}

		/// <summary> Retrieves the related entity of type 'ComponentStateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ComponentStateEntity' which is related to this entity.</returns>
		public virtual ComponentStateEntity GetSingleComponentState(bool forceFetch)
		{
			if( ( !_alreadyFetchedComponentState || forceFetch || _alwaysFetchComponentState) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ComponentStateEntityUsingStatusID);
				ComponentStateEntity newEntity = new ComponentStateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.StatusID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ComponentStateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_componentStateReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ComponentState = newEntity;
				_alreadyFetchedComponentState = fetchResult;
			}
			return _componentState;
		}


		/// <summary> Retrieves the related entity of type 'ComponentTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ComponentTypeEntity' which is related to this entity.</returns>
		public ComponentTypeEntity GetSingleComponentType()
		{
			return GetSingleComponentType(false);
		}

		/// <summary> Retrieves the related entity of type 'ComponentTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ComponentTypeEntity' which is related to this entity.</returns>
		public virtual ComponentTypeEntity GetSingleComponentType(bool forceFetch)
		{
			if( ( !_alreadyFetchedComponentType || forceFetch || _alwaysFetchComponentType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ComponentTypeEntityUsingComponentTypeID);
				ComponentTypeEntity newEntity = new ComponentTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ComponentTypeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ComponentTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_componentTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ComponentType = newEntity;
				_alreadyFetchedComponentType = fetchResult;
			}
			return _componentType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Automat", _automat);
			toReturn.Add("ComponentState", _componentState);
			toReturn.Add("ComponentType", _componentType);
			toReturn.Add("ComponentClearings", _componentClearings);
			toReturn.Add("ComponentFillings", _componentFillings);
			toReturn.Add("PaymentJournalToComponent", _paymentJournalToComponent);
			toReturn.Add("SaleToComponents", _saleToComponents);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="componentID">PK value for Component which data should be fetched into this Component object</param>
		/// <param name="validator">The validator object for this ComponentEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 componentID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(componentID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_componentClearings = new VarioSL.Entities.CollectionClasses.ComponentClearingCollection();
			_componentClearings.SetContainingEntityInfo(this, "Component");

			_componentFillings = new VarioSL.Entities.CollectionClasses.ComponentFillingCollection();
			_componentFillings.SetContainingEntityInfo(this, "Component");

			_paymentJournalToComponent = new VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection();
			_paymentJournalToComponent.SetContainingEntityInfo(this, "Component");

			_saleToComponents = new VarioSL.Entities.CollectionClasses.SaleToComponentCollection();
			_saleToComponents.SetContainingEntityInfo(this, "Component");
			_automatReturnsNewIfNotFound = false;
			_componentStateReturnsNewIfNotFound = false;
			_componentTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutomatID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintedComponentNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResetDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Slot", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StatusID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StockWarning", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueDate", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _automat</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAutomat(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _automat, new PropertyChangedEventHandler( OnAutomatPropertyChanged ), "Automat", VarioSL.Entities.RelationClasses.StaticComponentRelations.AutomatEntityUsingAutomatIDStatic, true, signalRelatedEntity, "Components", resetFKFields, new int[] { (int)ComponentFieldIndex.AutomatID } );		
			_automat = null;
		}
		
		/// <summary> setups the sync logic for member _automat</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAutomat(IEntityCore relatedEntity)
		{
			if(_automat!=relatedEntity)
			{		
				DesetupSyncAutomat(true, true);
				_automat = (AutomatEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _automat, new PropertyChangedEventHandler( OnAutomatPropertyChanged ), "Automat", VarioSL.Entities.RelationClasses.StaticComponentRelations.AutomatEntityUsingAutomatIDStatic, true, ref _alreadyFetchedAutomat, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAutomatPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _componentState</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncComponentState(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _componentState, new PropertyChangedEventHandler( OnComponentStatePropertyChanged ), "ComponentState", VarioSL.Entities.RelationClasses.StaticComponentRelations.ComponentStateEntityUsingStatusIDStatic, true, signalRelatedEntity, "Component", resetFKFields, new int[] { (int)ComponentFieldIndex.StatusID } );		
			_componentState = null;
		}
		
		/// <summary> setups the sync logic for member _componentState</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncComponentState(IEntityCore relatedEntity)
		{
			if(_componentState!=relatedEntity)
			{		
				DesetupSyncComponentState(true, true);
				_componentState = (ComponentStateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _componentState, new PropertyChangedEventHandler( OnComponentStatePropertyChanged ), "ComponentState", VarioSL.Entities.RelationClasses.StaticComponentRelations.ComponentStateEntityUsingStatusIDStatic, true, ref _alreadyFetchedComponentState, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnComponentStatePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _componentType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncComponentType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _componentType, new PropertyChangedEventHandler( OnComponentTypePropertyChanged ), "ComponentType", VarioSL.Entities.RelationClasses.StaticComponentRelations.ComponentTypeEntityUsingComponentTypeIDStatic, true, signalRelatedEntity, "Components", resetFKFields, new int[] { (int)ComponentFieldIndex.ComponentTypeID } );		
			_componentType = null;
		}
		
		/// <summary> setups the sync logic for member _componentType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncComponentType(IEntityCore relatedEntity)
		{
			if(_componentType!=relatedEntity)
			{		
				DesetupSyncComponentType(true, true);
				_componentType = (ComponentTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _componentType, new PropertyChangedEventHandler( OnComponentTypePropertyChanged ), "ComponentType", VarioSL.Entities.RelationClasses.StaticComponentRelations.ComponentTypeEntityUsingComponentTypeIDStatic, true, ref _alreadyFetchedComponentType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnComponentTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="componentID">PK value for Component which data should be fetched into this Component object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 componentID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ComponentFieldIndex.ComponentID].ForcedCurrentValueWrite(componentID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateComponentDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ComponentEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ComponentRelations Relations
		{
			get	{ return new ComponentRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ComponentClearing' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponentClearings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentClearingCollection(), (IEntityRelation)GetRelationsForField("ComponentClearings")[0], (int)VarioSL.Entities.EntityType.ComponentEntity, (int)VarioSL.Entities.EntityType.ComponentClearingEntity, 0, null, null, null, "ComponentClearings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ComponentFilling' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponentFillings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentFillingCollection(), (IEntityRelation)GetRelationsForField("ComponentFillings")[0], (int)VarioSL.Entities.EntityType.ComponentEntity, (int)VarioSL.Entities.EntityType.ComponentFillingEntity, 0, null, null, null, "ComponentFillings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentJournalToComponent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentJournalToComponent
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection(), (IEntityRelation)GetRelationsForField("PaymentJournalToComponent")[0], (int)VarioSL.Entities.EntityType.ComponentEntity, (int)VarioSL.Entities.EntityType.PaymentJournalToComponentEntity, 0, null, null, null, "PaymentJournalToComponent", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SaleToComponent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSaleToComponents
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleToComponentCollection(), (IEntityRelation)GetRelationsForField("SaleToComponents")[0], (int)VarioSL.Entities.EntityType.ComponentEntity, (int)VarioSL.Entities.EntityType.SaleToComponentEntity, 0, null, null, null, "SaleToComponents", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Automat'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutomat
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AutomatCollection(), (IEntityRelation)GetRelationsForField("Automat")[0], (int)VarioSL.Entities.EntityType.ComponentEntity, (int)VarioSL.Entities.EntityType.AutomatEntity, 0, null, null, null, "Automat", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ComponentState'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponentState
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentStateCollection(), (IEntityRelation)GetRelationsForField("ComponentState")[0], (int)VarioSL.Entities.EntityType.ComponentEntity, (int)VarioSL.Entities.EntityType.ComponentStateEntity, 0, null, null, null, "ComponentState", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ComponentType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponentType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentTypeCollection(), (IEntityRelation)GetRelationsForField("ComponentType")[0], (int)VarioSL.Entities.EntityType.ComponentEntity, (int)VarioSL.Entities.EntityType.ComponentTypeEntity, 0, null, null, null, "ComponentType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AutomatID property of the Entity Component<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENT"."AUTOMATID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AutomatID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ComponentFieldIndex.AutomatID, false); }
			set	{ SetValue((int)ComponentFieldIndex.AutomatID, value, true); }
		}

		/// <summary> The ComponentID property of the Entity Component<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENT"."COMPONENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ComponentID
		{
			get { return (System.Int64)GetValue((int)ComponentFieldIndex.ComponentID, true); }
			set	{ SetValue((int)ComponentFieldIndex.ComponentID, value, true); }
		}

		/// <summary> The ComponentNo property of the Entity Component<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENT"."COMPONENTNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 14, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ComponentNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ComponentFieldIndex.ComponentNo, false); }
			set	{ SetValue((int)ComponentFieldIndex.ComponentNo, value, true); }
		}

		/// <summary> The ComponentTypeID property of the Entity Component<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENT"."COMPONENTTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ComponentTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ComponentFieldIndex.ComponentTypeID, false); }
			set	{ SetValue((int)ComponentFieldIndex.ComponentTypeID, value, true); }
		}

		/// <summary> The PrintedComponentNo property of the Entity Component<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENT"."PRINTEDCOMPONENTNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrintedComponentNo
		{
			get { return (System.String)GetValue((int)ComponentFieldIndex.PrintedComponentNo, true); }
			set	{ SetValue((int)ComponentFieldIndex.PrintedComponentNo, value, true); }
		}

		/// <summary> The ResetDate property of the Entity Component<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENT"."RESETDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ResetDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ComponentFieldIndex.ResetDate, false); }
			set	{ SetValue((int)ComponentFieldIndex.ResetDate, value, true); }
		}

		/// <summary> The Slot property of the Entity Component<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENT"."SLOT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Slot
		{
			get { return (Nullable<System.Int32>)GetValue((int)ComponentFieldIndex.Slot, false); }
			set	{ SetValue((int)ComponentFieldIndex.Slot, value, true); }
		}

		/// <summary> The StatusID property of the Entity Component<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENT"."STATUSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> StatusID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ComponentFieldIndex.StatusID, false); }
			set	{ SetValue((int)ComponentFieldIndex.StatusID, value, true); }
		}

		/// <summary> The StockWarning property of the Entity Component<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENT"."STOCKWARNING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> StockWarning
		{
			get { return (Nullable<System.Int16>)GetValue((int)ComponentFieldIndex.StockWarning, false); }
			set	{ SetValue((int)ComponentFieldIndex.StockWarning, value, true); }
		}

		/// <summary> The ValueDate property of the Entity Component<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENT"."VALUEDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValueDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ComponentFieldIndex.ValueDate, false); }
			set	{ SetValue((int)ComponentFieldIndex.ValueDate, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiComponentClearings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ComponentClearingCollection ComponentClearings
		{
			get	{ return GetMultiComponentClearings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ComponentClearings. When set to true, ComponentClearings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ComponentClearings is accessed. You can always execute/ a forced fetch by calling GetMultiComponentClearings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponentClearings
		{
			get	{ return _alwaysFetchComponentClearings; }
			set	{ _alwaysFetchComponentClearings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ComponentClearings already has been fetched. Setting this property to false when ComponentClearings has been fetched
		/// will clear the ComponentClearings collection well. Setting this property to true while ComponentClearings hasn't been fetched disables lazy loading for ComponentClearings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponentClearings
		{
			get { return _alreadyFetchedComponentClearings;}
			set 
			{
				if(_alreadyFetchedComponentClearings && !value && (_componentClearings != null))
				{
					_componentClearings.Clear();
				}
				_alreadyFetchedComponentClearings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiComponentFillings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ComponentFillingCollection ComponentFillings
		{
			get	{ return GetMultiComponentFillings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ComponentFillings. When set to true, ComponentFillings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ComponentFillings is accessed. You can always execute/ a forced fetch by calling GetMultiComponentFillings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponentFillings
		{
			get	{ return _alwaysFetchComponentFillings; }
			set	{ _alwaysFetchComponentFillings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ComponentFillings already has been fetched. Setting this property to false when ComponentFillings has been fetched
		/// will clear the ComponentFillings collection well. Setting this property to true while ComponentFillings hasn't been fetched disables lazy loading for ComponentFillings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponentFillings
		{
			get { return _alreadyFetchedComponentFillings;}
			set 
			{
				if(_alreadyFetchedComponentFillings && !value && (_componentFillings != null))
				{
					_componentFillings.Clear();
				}
				_alreadyFetchedComponentFillings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentJournalToComponentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentJournalToComponent()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalToComponentCollection PaymentJournalToComponent
		{
			get	{ return GetMultiPaymentJournalToComponent(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentJournalToComponent. When set to true, PaymentJournalToComponent is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentJournalToComponent is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentJournalToComponent(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentJournalToComponent
		{
			get	{ return _alwaysFetchPaymentJournalToComponent; }
			set	{ _alwaysFetchPaymentJournalToComponent = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentJournalToComponent already has been fetched. Setting this property to false when PaymentJournalToComponent has been fetched
		/// will clear the PaymentJournalToComponent collection well. Setting this property to true while PaymentJournalToComponent hasn't been fetched disables lazy loading for PaymentJournalToComponent</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentJournalToComponent
		{
			get { return _alreadyFetchedPaymentJournalToComponent;}
			set 
			{
				if(_alreadyFetchedPaymentJournalToComponent && !value && (_paymentJournalToComponent != null))
				{
					_paymentJournalToComponent.Clear();
				}
				_alreadyFetchedPaymentJournalToComponent = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SaleToComponentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSaleToComponents()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SaleToComponentCollection SaleToComponents
		{
			get	{ return GetMultiSaleToComponents(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SaleToComponents. When set to true, SaleToComponents is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SaleToComponents is accessed. You can always execute/ a forced fetch by calling GetMultiSaleToComponents(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSaleToComponents
		{
			get	{ return _alwaysFetchSaleToComponents; }
			set	{ _alwaysFetchSaleToComponents = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SaleToComponents already has been fetched. Setting this property to false when SaleToComponents has been fetched
		/// will clear the SaleToComponents collection well. Setting this property to true while SaleToComponents hasn't been fetched disables lazy loading for SaleToComponents</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSaleToComponents
		{
			get { return _alreadyFetchedSaleToComponents;}
			set 
			{
				if(_alreadyFetchedSaleToComponents && !value && (_saleToComponents != null))
				{
					_saleToComponents.Clear();
				}
				_alreadyFetchedSaleToComponents = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AutomatEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAutomat()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AutomatEntity Automat
		{
			get	{ return GetSingleAutomat(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAutomat(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Components", "Automat", _automat, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Automat. When set to true, Automat is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Automat is accessed. You can always execute a forced fetch by calling GetSingleAutomat(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutomat
		{
			get	{ return _alwaysFetchAutomat; }
			set	{ _alwaysFetchAutomat = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Automat already has been fetched. Setting this property to false when Automat has been fetched
		/// will set Automat to null as well. Setting this property to true while Automat hasn't been fetched disables lazy loading for Automat</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutomat
		{
			get { return _alreadyFetchedAutomat;}
			set 
			{
				if(_alreadyFetchedAutomat && !value)
				{
					this.Automat = null;
				}
				_alreadyFetchedAutomat = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Automat is not found
		/// in the database. When set to true, Automat will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AutomatReturnsNewIfNotFound
		{
			get	{ return _automatReturnsNewIfNotFound; }
			set { _automatReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ComponentStateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleComponentState()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ComponentStateEntity ComponentState
		{
			get	{ return GetSingleComponentState(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncComponentState(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Component", "ComponentState", _componentState, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ComponentState. When set to true, ComponentState is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ComponentState is accessed. You can always execute a forced fetch by calling GetSingleComponentState(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponentState
		{
			get	{ return _alwaysFetchComponentState; }
			set	{ _alwaysFetchComponentState = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ComponentState already has been fetched. Setting this property to false when ComponentState has been fetched
		/// will set ComponentState to null as well. Setting this property to true while ComponentState hasn't been fetched disables lazy loading for ComponentState</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponentState
		{
			get { return _alreadyFetchedComponentState;}
			set 
			{
				if(_alreadyFetchedComponentState && !value)
				{
					this.ComponentState = null;
				}
				_alreadyFetchedComponentState = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ComponentState is not found
		/// in the database. When set to true, ComponentState will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ComponentStateReturnsNewIfNotFound
		{
			get	{ return _componentStateReturnsNewIfNotFound; }
			set { _componentStateReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ComponentTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleComponentType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ComponentTypeEntity ComponentType
		{
			get	{ return GetSingleComponentType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncComponentType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Components", "ComponentType", _componentType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ComponentType. When set to true, ComponentType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ComponentType is accessed. You can always execute a forced fetch by calling GetSingleComponentType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponentType
		{
			get	{ return _alwaysFetchComponentType; }
			set	{ _alwaysFetchComponentType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ComponentType already has been fetched. Setting this property to false when ComponentType has been fetched
		/// will set ComponentType to null as well. Setting this property to true while ComponentType hasn't been fetched disables lazy loading for ComponentType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponentType
		{
			get { return _alreadyFetchedComponentType;}
			set 
			{
				if(_alreadyFetchedComponentType && !value)
				{
					this.ComponentType = null;
				}
				_alreadyFetchedComponentType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ComponentType is not found
		/// in the database. When set to true, ComponentType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ComponentTypeReturnsNewIfNotFound
		{
			get	{ return _componentTypeReturnsNewIfNotFound; }
			set { _componentTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ComponentEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
