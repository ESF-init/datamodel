﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TypeOfCard'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TypeOfCardEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DebtorCardCollection	_debtorCards;
		private bool	_alwaysFetchDebtorCards, _alreadyFetchedDebtorCards;
		private VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection	_externalPacketefforts;
		private bool	_alwaysFetchExternalPacketefforts, _alreadyFetchedExternalPacketefforts;
		private DefaultPinEntity _defaultPin;
		private bool	_alwaysFetchDefaultPin, _alreadyFetchedDefaultPin, _defaultPinReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DebtorCards</summary>
			public static readonly string DebtorCards = "DebtorCards";
			/// <summary>Member name ExternalPacketefforts</summary>
			public static readonly string ExternalPacketefforts = "ExternalPacketefforts";
			/// <summary>Member name DefaultPin</summary>
			public static readonly string DefaultPin = "DefaultPin";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TypeOfCardEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TypeOfCardEntity() :base("TypeOfCardEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="typeOfCardID">PK value for TypeOfCard which data should be fetched into this TypeOfCard object</param>
		public TypeOfCardEntity(System.Int64 typeOfCardID):base("TypeOfCardEntity")
		{
			InitClassFetch(typeOfCardID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="typeOfCardID">PK value for TypeOfCard which data should be fetched into this TypeOfCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TypeOfCardEntity(System.Int64 typeOfCardID, IPrefetchPath prefetchPathToUse):base("TypeOfCardEntity")
		{
			InitClassFetch(typeOfCardID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="typeOfCardID">PK value for TypeOfCard which data should be fetched into this TypeOfCard object</param>
		/// <param name="validator">The custom validator object for this TypeOfCardEntity</param>
		public TypeOfCardEntity(System.Int64 typeOfCardID, IValidator validator):base("TypeOfCardEntity")
		{
			InitClassFetch(typeOfCardID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TypeOfCardEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_debtorCards = (VarioSL.Entities.CollectionClasses.DebtorCardCollection)info.GetValue("_debtorCards", typeof(VarioSL.Entities.CollectionClasses.DebtorCardCollection));
			_alwaysFetchDebtorCards = info.GetBoolean("_alwaysFetchDebtorCards");
			_alreadyFetchedDebtorCards = info.GetBoolean("_alreadyFetchedDebtorCards");

			_externalPacketefforts = (VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection)info.GetValue("_externalPacketefforts", typeof(VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection));
			_alwaysFetchExternalPacketefforts = info.GetBoolean("_alwaysFetchExternalPacketefforts");
			_alreadyFetchedExternalPacketefforts = info.GetBoolean("_alreadyFetchedExternalPacketefforts");
			_defaultPin = (DefaultPinEntity)info.GetValue("_defaultPin", typeof(DefaultPinEntity));
			if(_defaultPin!=null)
			{
				_defaultPin.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_defaultPinReturnsNewIfNotFound = info.GetBoolean("_defaultPinReturnsNewIfNotFound");
			_alwaysFetchDefaultPin = info.GetBoolean("_alwaysFetchDefaultPin");
			_alreadyFetchedDefaultPin = info.GetBoolean("_alreadyFetchedDefaultPin");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDebtorCards = (_debtorCards.Count > 0);
			_alreadyFetchedExternalPacketefforts = (_externalPacketefforts.Count > 0);
			_alreadyFetchedDefaultPin = (_defaultPin != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DebtorCards":
					toReturn.Add(Relations.DebtorCardEntityUsingTypeOfCardID);
					break;
				case "ExternalPacketefforts":
					toReturn.Add(Relations.ExternalPacketEffortEntityUsingCardTypeId);
					break;
				case "DefaultPin":
					toReturn.Add(Relations.DefaultPinEntityUsingTypeOfCardID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_debtorCards", (!this.MarkedForDeletion?_debtorCards:null));
			info.AddValue("_alwaysFetchDebtorCards", _alwaysFetchDebtorCards);
			info.AddValue("_alreadyFetchedDebtorCards", _alreadyFetchedDebtorCards);
			info.AddValue("_externalPacketefforts", (!this.MarkedForDeletion?_externalPacketefforts:null));
			info.AddValue("_alwaysFetchExternalPacketefforts", _alwaysFetchExternalPacketefforts);
			info.AddValue("_alreadyFetchedExternalPacketefforts", _alreadyFetchedExternalPacketefforts);

			info.AddValue("_defaultPin", (!this.MarkedForDeletion?_defaultPin:null));
			info.AddValue("_defaultPinReturnsNewIfNotFound", _defaultPinReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDefaultPin", _alwaysFetchDefaultPin);
			info.AddValue("_alreadyFetchedDefaultPin", _alreadyFetchedDefaultPin);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DebtorCards":
					_alreadyFetchedDebtorCards = true;
					if(entity!=null)
					{
						this.DebtorCards.Add((DebtorCardEntity)entity);
					}
					break;
				case "ExternalPacketefforts":
					_alreadyFetchedExternalPacketefforts = true;
					if(entity!=null)
					{
						this.ExternalPacketefforts.Add((ExternalPacketEffortEntity)entity);
					}
					break;
				case "DefaultPin":
					_alreadyFetchedDefaultPin = true;
					this.DefaultPin = (DefaultPinEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DebtorCards":
					_debtorCards.Add((DebtorCardEntity)relatedEntity);
					break;
				case "ExternalPacketefforts":
					_externalPacketefforts.Add((ExternalPacketEffortEntity)relatedEntity);
					break;
				case "DefaultPin":
					SetupSyncDefaultPin(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DebtorCards":
					this.PerformRelatedEntityRemoval(_debtorCards, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalPacketefforts":
					this.PerformRelatedEntityRemoval(_externalPacketefforts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DefaultPin":
					DesetupSyncDefaultPin(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_defaultPin!=null)
			{
				toReturn.Add(_defaultPin);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_debtorCards);
			toReturn.Add(_externalPacketefforts);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeOfCardID">PK value for TypeOfCard which data should be fetched into this TypeOfCard object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeOfCardID)
		{
			return FetchUsingPK(typeOfCardID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeOfCardID">PK value for TypeOfCard which data should be fetched into this TypeOfCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeOfCardID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(typeOfCardID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeOfCardID">PK value for TypeOfCard which data should be fetched into this TypeOfCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeOfCardID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(typeOfCardID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeOfCardID">PK value for TypeOfCard which data should be fetched into this TypeOfCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeOfCardID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(typeOfCardID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TypeOfCardID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TypeOfCardRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DebtorCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DebtorCardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DebtorCardCollection GetMultiDebtorCards(bool forceFetch)
		{
			return GetMultiDebtorCards(forceFetch, _debtorCards.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DebtorCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DebtorCardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DebtorCardCollection GetMultiDebtorCards(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDebtorCards(forceFetch, _debtorCards.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DebtorCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DebtorCardCollection GetMultiDebtorCards(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDebtorCards(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DebtorCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DebtorCardCollection GetMultiDebtorCards(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDebtorCards || forceFetch || _alwaysFetchDebtorCards) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_debtorCards);
				_debtorCards.SuppressClearInGetMulti=!forceFetch;
				_debtorCards.EntityFactoryToUse = entityFactoryToUse;
				_debtorCards.GetMultiManyToOne(this, filter);
				_debtorCards.SuppressClearInGetMulti=false;
				_alreadyFetchedDebtorCards = true;
			}
			return _debtorCards;
		}

		/// <summary> Sets the collection parameters for the collection for 'DebtorCards'. These settings will be taken into account
		/// when the property DebtorCards is requested or GetMultiDebtorCards is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDebtorCards(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_debtorCards.SortClauses=sortClauses;
			_debtorCards.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalPacketEffortEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketefforts(bool forceFetch)
		{
			return GetMultiExternalPacketefforts(forceFetch, _externalPacketefforts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalPacketEffortEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketefforts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalPacketefforts(forceFetch, _externalPacketefforts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketefforts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalPacketefforts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketefforts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalPacketefforts || forceFetch || _alwaysFetchExternalPacketefforts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalPacketefforts);
				_externalPacketefforts.SuppressClearInGetMulti=!forceFetch;
				_externalPacketefforts.EntityFactoryToUse = entityFactoryToUse;
				_externalPacketefforts.GetMultiManyToOne(null, null, null, null, null, null, this, filter);
				_externalPacketefforts.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalPacketefforts = true;
			}
			return _externalPacketefforts;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalPacketefforts'. These settings will be taken into account
		/// when the property ExternalPacketefforts is requested or GetMultiExternalPacketefforts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalPacketefforts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalPacketefforts.SortClauses=sortClauses;
			_externalPacketefforts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'DefaultPinEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'DefaultPinEntity' which is related to this entity.</returns>
		public DefaultPinEntity GetSingleDefaultPin()
		{
			return GetSingleDefaultPin(false);
		}
		
		/// <summary> Retrieves the related entity of type 'DefaultPinEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DefaultPinEntity' which is related to this entity.</returns>
		public virtual DefaultPinEntity GetSingleDefaultPin(bool forceFetch)
		{
			if( ( !_alreadyFetchedDefaultPin || forceFetch || _alwaysFetchDefaultPin) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DefaultPinEntityUsingTypeOfCardID);
				DefaultPinEntity newEntity = new DefaultPinEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCTypeOfCardID(this.TypeOfCardID);
				}
				if(fetchResult)
				{
					newEntity = (DefaultPinEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_defaultPinReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DefaultPin = newEntity;
				_alreadyFetchedDefaultPin = fetchResult;
			}
			return _defaultPin;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DebtorCards", _debtorCards);
			toReturn.Add("ExternalPacketefforts", _externalPacketefforts);
			toReturn.Add("DefaultPin", _defaultPin);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="typeOfCardID">PK value for TypeOfCard which data should be fetched into this TypeOfCard object</param>
		/// <param name="validator">The validator object for this TypeOfCardEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 typeOfCardID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(typeOfCardID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_debtorCards = new VarioSL.Entities.CollectionClasses.DebtorCardCollection();
			_debtorCards.SetContainingEntityInfo(this, "TypeOfCard");

			_externalPacketefforts = new VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection();
			_externalPacketefforts.SetContainingEntityInfo(this, "TypeOfCard");
			_defaultPinReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CodeNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EnumerationValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FormularID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Language", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Literal", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxCappingPotCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MocaFileSystem", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeOfCardID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _defaultPin</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDefaultPin(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _defaultPin, new PropertyChangedEventHandler( OnDefaultPinPropertyChanged ), "DefaultPin", VarioSL.Entities.RelationClasses.StaticTypeOfCardRelations.DefaultPinEntityUsingTypeOfCardIDStatic, false, signalRelatedEntity, "TypeOfCard", false, new int[] { (int)TypeOfCardFieldIndex.TypeOfCardID } );
			_defaultPin = null;
		}
	
		/// <summary> setups the sync logic for member _defaultPin</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDefaultPin(IEntityCore relatedEntity)
		{
			if(_defaultPin!=relatedEntity)
			{
				DesetupSyncDefaultPin(true, true);
				_defaultPin = (DefaultPinEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _defaultPin, new PropertyChangedEventHandler( OnDefaultPinPropertyChanged ), "DefaultPin", VarioSL.Entities.RelationClasses.StaticTypeOfCardRelations.DefaultPinEntityUsingTypeOfCardIDStatic, false, ref _alreadyFetchedDefaultPin, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDefaultPinPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="typeOfCardID">PK value for TypeOfCard which data should be fetched into this TypeOfCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 typeOfCardID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TypeOfCardFieldIndex.TypeOfCardID].ForcedCurrentValueWrite(typeOfCardID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTypeOfCardDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TypeOfCardEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TypeOfCardRelations Relations
		{
			get	{ return new TypeOfCardRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DebtorCard' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDebtorCards
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCardCollection(), (IEntityRelation)GetRelationsForField("DebtorCards")[0], (int)VarioSL.Entities.EntityType.TypeOfCardEntity, (int)VarioSL.Entities.EntityType.DebtorCardEntity, 0, null, null, null, "DebtorCards", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalPacketEffort' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalPacketefforts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection(), (IEntityRelation)GetRelationsForField("ExternalPacketefforts")[0], (int)VarioSL.Entities.EntityType.TypeOfCardEntity, (int)VarioSL.Entities.EntityType.ExternalPacketEffortEntity, 0, null, null, null, "ExternalPacketefforts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DefaultPin'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDefaultPin
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DefaultPinCollection(), (IEntityRelation)GetRelationsForField("DefaultPin")[0], (int)VarioSL.Entities.EntityType.TypeOfCardEntity, (int)VarioSL.Entities.EntityType.DefaultPinEntity, 0, null, null, null, "DefaultPin", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CodeNo property of the Entity TypeOfCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_TYPEOFCARD"."CODENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> CodeNo
		{
			get { return (Nullable<System.Int16>)GetValue((int)TypeOfCardFieldIndex.CodeNo, false); }
			set	{ SetValue((int)TypeOfCardFieldIndex.CodeNo, value, true); }
		}

		/// <summary> The Description property of the Entity TypeOfCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_TYPEOFCARD"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)TypeOfCardFieldIndex.Description, true); }
			set	{ SetValue((int)TypeOfCardFieldIndex.Description, value, true); }
		}

		/// <summary> The EnumerationValue property of the Entity TypeOfCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_TYPEOFCARD"."CODENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.Int32 EnumerationValue
		{
			get { return (System.Int32)GetValue((int)TypeOfCardFieldIndex.EnumerationValue, true); }
			set	{ SetValue((int)TypeOfCardFieldIndex.EnumerationValue, value, true); }
		}

		/// <summary> The FormularID property of the Entity TypeOfCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_TYPEOFCARD"."FORMULARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> FormularID
		{
			get { return (Nullable<System.Int32>)GetValue((int)TypeOfCardFieldIndex.FormularID, false); }
			set	{ SetValue((int)TypeOfCardFieldIndex.FormularID, value, true); }
		}

		/// <summary> The Language property of the Entity TypeOfCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_TYPEOFCARD"."LANGUAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Language
		{
			get { return (System.String)GetValue((int)TypeOfCardFieldIndex.Language, true); }
			set	{ SetValue((int)TypeOfCardFieldIndex.Language, value, true); }
		}

		/// <summary> The Literal property of the Entity TypeOfCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_TYPEOFCARD"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Literal
		{
			get { return (System.String)GetValue((int)TypeOfCardFieldIndex.Literal, true); }
			set	{ SetValue((int)TypeOfCardFieldIndex.Literal, value, true); }
		}

		/// <summary> The MaxCappingPotCount property of the Entity TypeOfCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_TYPEOFCARD"."MAXCAPPINGPOTCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MaxCappingPotCount
		{
			get { return (Nullable<System.Int32>)GetValue((int)TypeOfCardFieldIndex.MaxCappingPotCount, false); }
			set	{ SetValue((int)TypeOfCardFieldIndex.MaxCappingPotCount, value, true); }
		}

		/// <summary> The MocaFileSystem property of the Entity TypeOfCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_TYPEOFCARD"."MOCAFILESYSTEM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MocaFileSystem
		{
			get { return (Nullable<System.Int64>)GetValue((int)TypeOfCardFieldIndex.MocaFileSystem, false); }
			set	{ SetValue((int)TypeOfCardFieldIndex.MocaFileSystem, value, true); }
		}

		/// <summary> The TypeOfCardID property of the Entity TypeOfCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_TYPEOFCARD"."TYPEOFCARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 TypeOfCardID
		{
			get { return (System.Int64)GetValue((int)TypeOfCardFieldIndex.TypeOfCardID, true); }
			set	{ SetValue((int)TypeOfCardFieldIndex.TypeOfCardID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DebtorCardEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDebtorCards()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DebtorCardCollection DebtorCards
		{
			get	{ return GetMultiDebtorCards(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DebtorCards. When set to true, DebtorCards is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DebtorCards is accessed. You can always execute/ a forced fetch by calling GetMultiDebtorCards(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDebtorCards
		{
			get	{ return _alwaysFetchDebtorCards; }
			set	{ _alwaysFetchDebtorCards = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DebtorCards already has been fetched. Setting this property to false when DebtorCards has been fetched
		/// will clear the DebtorCards collection well. Setting this property to true while DebtorCards hasn't been fetched disables lazy loading for DebtorCards</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDebtorCards
		{
			get { return _alreadyFetchedDebtorCards;}
			set 
			{
				if(_alreadyFetchedDebtorCards && !value && (_debtorCards != null))
				{
					_debtorCards.Clear();
				}
				_alreadyFetchedDebtorCards = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalPacketefforts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection ExternalPacketefforts
		{
			get	{ return GetMultiExternalPacketefforts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalPacketefforts. When set to true, ExternalPacketefforts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalPacketefforts is accessed. You can always execute/ a forced fetch by calling GetMultiExternalPacketefforts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalPacketefforts
		{
			get	{ return _alwaysFetchExternalPacketefforts; }
			set	{ _alwaysFetchExternalPacketefforts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalPacketefforts already has been fetched. Setting this property to false when ExternalPacketefforts has been fetched
		/// will clear the ExternalPacketefforts collection well. Setting this property to true while ExternalPacketefforts hasn't been fetched disables lazy loading for ExternalPacketefforts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalPacketefforts
		{
			get { return _alreadyFetchedExternalPacketefforts;}
			set 
			{
				if(_alreadyFetchedExternalPacketefforts && !value && (_externalPacketefforts != null))
				{
					_externalPacketefforts.Clear();
				}
				_alreadyFetchedExternalPacketefforts = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'DefaultPinEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDefaultPin()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DefaultPinEntity DefaultPin
		{
			get	{ return GetSingleDefaultPin(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncDefaultPin(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_defaultPin !=null);
						DesetupSyncDefaultPin(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("DefaultPin");
						}
					}
					else
					{
						if(_defaultPin!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TypeOfCard");
							SetupSyncDefaultPin(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DefaultPin. When set to true, DefaultPin is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DefaultPin is accessed. You can always execute a forced fetch by calling GetSingleDefaultPin(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDefaultPin
		{
			get	{ return _alwaysFetchDefaultPin; }
			set	{ _alwaysFetchDefaultPin = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property DefaultPin already has been fetched. Setting this property to false when DefaultPin has been fetched
		/// will set DefaultPin to null as well. Setting this property to true while DefaultPin hasn't been fetched disables lazy loading for DefaultPin</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDefaultPin
		{
			get { return _alreadyFetchedDefaultPin;}
			set 
			{
				if(_alreadyFetchedDefaultPin && !value)
				{
					this.DefaultPin = null;
				}
				_alreadyFetchedDefaultPin = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DefaultPin is not found
		/// in the database. When set to true, DefaultPin will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DefaultPinReturnsNewIfNotFound
		{
			get	{ return _defaultPinReturnsNewIfNotFound; }
			set	{ _defaultPinReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TypeOfCardEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
