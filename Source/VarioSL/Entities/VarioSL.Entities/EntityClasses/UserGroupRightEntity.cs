﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UserGroupRight'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class UserGroupRightEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private UserGroupEntity _userGroup;
		private bool	_alwaysFetchUserGroup, _alreadyFetchedUserGroup, _userGroupReturnsNewIfNotFound;
		private UserResourceEntity _userResource;
		private bool	_alwaysFetchUserResource, _alreadyFetchedUserResource, _userResourceReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserGroup</summary>
			public static readonly string UserGroup = "UserGroup";
			/// <summary>Member name UserResource</summary>
			public static readonly string UserResource = "UserResource";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UserGroupRightEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public UserGroupRightEntity() :base("UserGroupRightEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="userGroupRightID">PK value for UserGroupRight which data should be fetched into this UserGroupRight object</param>
		public UserGroupRightEntity(System.Int64 userGroupRightID):base("UserGroupRightEntity")
		{
			InitClassFetch(userGroupRightID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="userGroupRightID">PK value for UserGroupRight which data should be fetched into this UserGroupRight object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UserGroupRightEntity(System.Int64 userGroupRightID, IPrefetchPath prefetchPathToUse):base("UserGroupRightEntity")
		{
			InitClassFetch(userGroupRightID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="userGroupRightID">PK value for UserGroupRight which data should be fetched into this UserGroupRight object</param>
		/// <param name="validator">The custom validator object for this UserGroupRightEntity</param>
		public UserGroupRightEntity(System.Int64 userGroupRightID, IValidator validator):base("UserGroupRightEntity")
		{
			InitClassFetch(userGroupRightID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UserGroupRightEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_userGroup = (UserGroupEntity)info.GetValue("_userGroup", typeof(UserGroupEntity));
			if(_userGroup!=null)
			{
				_userGroup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userGroupReturnsNewIfNotFound = info.GetBoolean("_userGroupReturnsNewIfNotFound");
			_alwaysFetchUserGroup = info.GetBoolean("_alwaysFetchUserGroup");
			_alreadyFetchedUserGroup = info.GetBoolean("_alreadyFetchedUserGroup");

			_userResource = (UserResourceEntity)info.GetValue("_userResource", typeof(UserResourceEntity));
			if(_userResource!=null)
			{
				_userResource.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userResourceReturnsNewIfNotFound = info.GetBoolean("_userResourceReturnsNewIfNotFound");
			_alwaysFetchUserResource = info.GetBoolean("_alwaysFetchUserResource");
			_alreadyFetchedUserResource = info.GetBoolean("_alreadyFetchedUserResource");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UserGroupRightFieldIndex)fieldIndex)
			{
				case UserGroupRightFieldIndex.ResourceID:
					DesetupSyncUserResource(true, false);
					_alreadyFetchedUserResource = false;
					break;
				case UserGroupRightFieldIndex.UserGroupID:
					DesetupSyncUserGroup(true, false);
					_alreadyFetchedUserGroup = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedUserGroup = (_userGroup != null);
			_alreadyFetchedUserResource = (_userResource != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "UserGroup":
					toReturn.Add(Relations.UserGroupEntityUsingUserGroupID);
					break;
				case "UserResource":
					toReturn.Add(Relations.UserResourceEntityUsingResourceID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_userGroup", (!this.MarkedForDeletion?_userGroup:null));
			info.AddValue("_userGroupReturnsNewIfNotFound", _userGroupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserGroup", _alwaysFetchUserGroup);
			info.AddValue("_alreadyFetchedUserGroup", _alreadyFetchedUserGroup);
			info.AddValue("_userResource", (!this.MarkedForDeletion?_userResource:null));
			info.AddValue("_userResourceReturnsNewIfNotFound", _userResourceReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserResource", _alwaysFetchUserResource);
			info.AddValue("_alreadyFetchedUserResource", _alreadyFetchedUserResource);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "UserGroup":
					_alreadyFetchedUserGroup = true;
					this.UserGroup = (UserGroupEntity)entity;
					break;
				case "UserResource":
					_alreadyFetchedUserResource = true;
					this.UserResource = (UserResourceEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "UserGroup":
					SetupSyncUserGroup(relatedEntity);
					break;
				case "UserResource":
					SetupSyncUserResource(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "UserGroup":
					DesetupSyncUserGroup(false, true);
					break;
				case "UserResource":
					DesetupSyncUserResource(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_userGroup!=null)
			{
				toReturn.Add(_userGroup);
			}
			if(_userResource!=null)
			{
				toReturn.Add(_userResource);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userGroupRightID">PK value for UserGroupRight which data should be fetched into this UserGroupRight object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 userGroupRightID)
		{
			return FetchUsingPK(userGroupRightID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userGroupRightID">PK value for UserGroupRight which data should be fetched into this UserGroupRight object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 userGroupRightID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(userGroupRightID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userGroupRightID">PK value for UserGroupRight which data should be fetched into this UserGroupRight object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 userGroupRightID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(userGroupRightID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userGroupRightID">PK value for UserGroupRight which data should be fetched into this UserGroupRight object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 userGroupRightID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(userGroupRightID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UserGroupRightID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UserGroupRightRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'UserGroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserGroupEntity' which is related to this entity.</returns>
		public UserGroupEntity GetSingleUserGroup()
		{
			return GetSingleUserGroup(false);
		}

		/// <summary> Retrieves the related entity of type 'UserGroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserGroupEntity' which is related to this entity.</returns>
		public virtual UserGroupEntity GetSingleUserGroup(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserGroup || forceFetch || _alwaysFetchUserGroup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserGroupEntityUsingUserGroupID);
				UserGroupEntity newEntity = new UserGroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UserGroupID);
				}
				if(fetchResult)
				{
					newEntity = (UserGroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userGroupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserGroup = newEntity;
				_alreadyFetchedUserGroup = fetchResult;
			}
			return _userGroup;
		}


		/// <summary> Retrieves the related entity of type 'UserResourceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserResourceEntity' which is related to this entity.</returns>
		public UserResourceEntity GetSingleUserResource()
		{
			return GetSingleUserResource(false);
		}

		/// <summary> Retrieves the related entity of type 'UserResourceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserResourceEntity' which is related to this entity.</returns>
		public virtual UserResourceEntity GetSingleUserResource(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserResource || forceFetch || _alwaysFetchUserResource) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserResourceEntityUsingResourceID);
				UserResourceEntity newEntity = new UserResourceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ResourceID);
				}
				if(fetchResult)
				{
					newEntity = (UserResourceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userResourceReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserResource = newEntity;
				_alreadyFetchedUserResource = fetchResult;
			}
			return _userResource;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("UserGroup", _userGroup);
			toReturn.Add("UserResource", _userResource);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="userGroupRightID">PK value for UserGroupRight which data should be fetched into this UserGroupRight object</param>
		/// <param name="validator">The validator object for this UserGroupRightEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 userGroupRightID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(userGroupRightID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_userGroupReturnsNewIfNotFound = false;
			_userResourceReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LevelOfRight", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResourceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserGroupRightID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _userGroup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserGroup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userGroup, new PropertyChangedEventHandler( OnUserGroupPropertyChanged ), "UserGroup", VarioSL.Entities.RelationClasses.StaticUserGroupRightRelations.UserGroupEntityUsingUserGroupIDStatic, true, signalRelatedEntity, "UserGroupRights", resetFKFields, new int[] { (int)UserGroupRightFieldIndex.UserGroupID } );		
			_userGroup = null;
		}
		
		/// <summary> setups the sync logic for member _userGroup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserGroup(IEntityCore relatedEntity)
		{
			if(_userGroup!=relatedEntity)
			{		
				DesetupSyncUserGroup(true, true);
				_userGroup = (UserGroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userGroup, new PropertyChangedEventHandler( OnUserGroupPropertyChanged ), "UserGroup", VarioSL.Entities.RelationClasses.StaticUserGroupRightRelations.UserGroupEntityUsingUserGroupIDStatic, true, ref _alreadyFetchedUserGroup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserGroupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _userResource</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserResource(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userResource, new PropertyChangedEventHandler( OnUserResourcePropertyChanged ), "UserResource", VarioSL.Entities.RelationClasses.StaticUserGroupRightRelations.UserResourceEntityUsingResourceIDStatic, true, signalRelatedEntity, "UserGroupRights", resetFKFields, new int[] { (int)UserGroupRightFieldIndex.ResourceID } );		
			_userResource = null;
		}
		
		/// <summary> setups the sync logic for member _userResource</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserResource(IEntityCore relatedEntity)
		{
			if(_userResource!=relatedEntity)
			{		
				DesetupSyncUserResource(true, true);
				_userResource = (UserResourceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userResource, new PropertyChangedEventHandler( OnUserResourcePropertyChanged ), "UserResource", VarioSL.Entities.RelationClasses.StaticUserGroupRightRelations.UserResourceEntityUsingResourceIDStatic, true, ref _alreadyFetchedUserResource, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserResourcePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="userGroupRightID">PK value for UserGroupRight which data should be fetched into this UserGroupRight object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 userGroupRightID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UserGroupRightFieldIndex.UserGroupRightID].ForcedCurrentValueWrite(userGroupRightID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUserGroupRightDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UserGroupRightEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UserGroupRightRelations Relations
		{
			get	{ return new UserGroupRightRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserGroup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserGroupCollection(), (IEntityRelation)GetRelationsForField("UserGroup")[0], (int)VarioSL.Entities.EntityType.UserGroupRightEntity, (int)VarioSL.Entities.EntityType.UserGroupEntity, 0, null, null, null, "UserGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserResource'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserResource
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserResourceCollection(), (IEntityRelation)GetRelationsForField("UserResource")[0], (int)VarioSL.Entities.EntityType.UserGroupRightEntity, (int)VarioSL.Entities.EntityType.UserResourceEntity, 0, null, null, null, "UserResource", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LevelOfRight property of the Entity UserGroupRight<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_GROUPRIGHT"."LEVELOFRIGHT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 LevelOfRight
		{
			get { return (System.Int16)GetValue((int)UserGroupRightFieldIndex.LevelOfRight, true); }
			set	{ SetValue((int)UserGroupRightFieldIndex.LevelOfRight, value, true); }
		}

		/// <summary> The ResourceID property of the Entity UserGroupRight<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_GROUPRIGHT"."RESOURCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ResourceID
		{
			get { return (System.String)GetValue((int)UserGroupRightFieldIndex.ResourceID, true); }
			set	{ SetValue((int)UserGroupRightFieldIndex.ResourceID, value, true); }
		}

		/// <summary> The UserGroupID property of the Entity UserGroupRight<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_GROUPRIGHT"."USERGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 UserGroupID
		{
			get { return (System.Int64)GetValue((int)UserGroupRightFieldIndex.UserGroupID, true); }
			set	{ SetValue((int)UserGroupRightFieldIndex.UserGroupID, value, true); }
		}

		/// <summary> The UserGroupRightID property of the Entity UserGroupRight<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_GROUPRIGHT"."USERGROUPRIGHTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 UserGroupRightID
		{
			get { return (System.Int64)GetValue((int)UserGroupRightFieldIndex.UserGroupRightID, true); }
			set	{ SetValue((int)UserGroupRightFieldIndex.UserGroupRightID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'UserGroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserGroupEntity UserGroup
		{
			get	{ return GetSingleUserGroup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserGroup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UserGroupRights", "UserGroup", _userGroup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserGroup. When set to true, UserGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserGroup is accessed. You can always execute a forced fetch by calling GetSingleUserGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserGroup
		{
			get	{ return _alwaysFetchUserGroup; }
			set	{ _alwaysFetchUserGroup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserGroup already has been fetched. Setting this property to false when UserGroup has been fetched
		/// will set UserGroup to null as well. Setting this property to true while UserGroup hasn't been fetched disables lazy loading for UserGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserGroup
		{
			get { return _alreadyFetchedUserGroup;}
			set 
			{
				if(_alreadyFetchedUserGroup && !value)
				{
					this.UserGroup = null;
				}
				_alreadyFetchedUserGroup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserGroup is not found
		/// in the database. When set to true, UserGroup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserGroupReturnsNewIfNotFound
		{
			get	{ return _userGroupReturnsNewIfNotFound; }
			set { _userGroupReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserResourceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserResource()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserResourceEntity UserResource
		{
			get	{ return GetSingleUserResource(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserResource(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UserGroupRights", "UserResource", _userResource, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserResource. When set to true, UserResource is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserResource is accessed. You can always execute a forced fetch by calling GetSingleUserResource(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserResource
		{
			get	{ return _alwaysFetchUserResource; }
			set	{ _alwaysFetchUserResource = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserResource already has been fetched. Setting this property to false when UserResource has been fetched
		/// will set UserResource to null as well. Setting this property to true while UserResource hasn't been fetched disables lazy loading for UserResource</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserResource
		{
			get { return _alreadyFetchedUserResource;}
			set 
			{
				if(_alreadyFetchedUserResource && !value)
				{
					this.UserResource = null;
				}
				_alreadyFetchedUserResource = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserResource is not found
		/// in the database. When set to true, UserResource will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserResourceReturnsNewIfNotFound
		{
			get	{ return _userResourceReturnsNewIfNotFound; }
			set { _userResourceReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.UserGroupRightEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
