﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'EmailEvent'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class EmailEventEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection	_accountMessageSettings;
		private bool	_alwaysFetchAccountMessageSettings, _alreadyFetchedAccountMessageSettings;
		private VarioSL.Entities.CollectionClasses.EmailMessageCollection	_emailMessages;
		private bool	_alwaysFetchEmailMessages, _alreadyFetchedEmailMessages;
		private VarioSL.Entities.CollectionClasses.EventSettingCollection	_eventSettings;
		private bool	_alwaysFetchEventSettings, _alreadyFetchedEventSettings;
		private VarioSL.Entities.CollectionClasses.MessageCollection	_messages;
		private bool	_alwaysFetchMessages, _alreadyFetchedMessages;
		private VarioSL.Entities.CollectionClasses.NotificationMessageCollection	_notificationMessages;
		private bool	_alwaysFetchNotificationMessages, _alreadyFetchedNotificationMessages;
		private VarioSL.Entities.CollectionClasses.FormCollection _formCollectionViaEmailMessage;
		private bool	_alwaysFetchFormCollectionViaEmailMessage, _alreadyFetchedFormCollectionViaEmailMessage;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AccountMessageSettings</summary>
			public static readonly string AccountMessageSettings = "AccountMessageSettings";
			/// <summary>Member name EmailMessages</summary>
			public static readonly string EmailMessages = "EmailMessages";
			/// <summary>Member name EventSettings</summary>
			public static readonly string EventSettings = "EventSettings";
			/// <summary>Member name Messages</summary>
			public static readonly string Messages = "Messages";
			/// <summary>Member name NotificationMessages</summary>
			public static readonly string NotificationMessages = "NotificationMessages";
			/// <summary>Member name FormCollectionViaEmailMessage</summary>
			public static readonly string FormCollectionViaEmailMessage = "FormCollectionViaEmailMessage";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static EmailEventEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public EmailEventEntity() :base("EmailEventEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="emailEventID">PK value for EmailEvent which data should be fetched into this EmailEvent object</param>
		public EmailEventEntity(System.Int64 emailEventID):base("EmailEventEntity")
		{
			InitClassFetch(emailEventID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="emailEventID">PK value for EmailEvent which data should be fetched into this EmailEvent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public EmailEventEntity(System.Int64 emailEventID, IPrefetchPath prefetchPathToUse):base("EmailEventEntity")
		{
			InitClassFetch(emailEventID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="emailEventID">PK value for EmailEvent which data should be fetched into this EmailEvent object</param>
		/// <param name="validator">The custom validator object for this EmailEventEntity</param>
		public EmailEventEntity(System.Int64 emailEventID, IValidator validator):base("EmailEventEntity")
		{
			InitClassFetch(emailEventID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EmailEventEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_accountMessageSettings = (VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection)info.GetValue("_accountMessageSettings", typeof(VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection));
			_alwaysFetchAccountMessageSettings = info.GetBoolean("_alwaysFetchAccountMessageSettings");
			_alreadyFetchedAccountMessageSettings = info.GetBoolean("_alreadyFetchedAccountMessageSettings");

			_emailMessages = (VarioSL.Entities.CollectionClasses.EmailMessageCollection)info.GetValue("_emailMessages", typeof(VarioSL.Entities.CollectionClasses.EmailMessageCollection));
			_alwaysFetchEmailMessages = info.GetBoolean("_alwaysFetchEmailMessages");
			_alreadyFetchedEmailMessages = info.GetBoolean("_alreadyFetchedEmailMessages");

			_eventSettings = (VarioSL.Entities.CollectionClasses.EventSettingCollection)info.GetValue("_eventSettings", typeof(VarioSL.Entities.CollectionClasses.EventSettingCollection));
			_alwaysFetchEventSettings = info.GetBoolean("_alwaysFetchEventSettings");
			_alreadyFetchedEventSettings = info.GetBoolean("_alreadyFetchedEventSettings");

			_messages = (VarioSL.Entities.CollectionClasses.MessageCollection)info.GetValue("_messages", typeof(VarioSL.Entities.CollectionClasses.MessageCollection));
			_alwaysFetchMessages = info.GetBoolean("_alwaysFetchMessages");
			_alreadyFetchedMessages = info.GetBoolean("_alreadyFetchedMessages");

			_notificationMessages = (VarioSL.Entities.CollectionClasses.NotificationMessageCollection)info.GetValue("_notificationMessages", typeof(VarioSL.Entities.CollectionClasses.NotificationMessageCollection));
			_alwaysFetchNotificationMessages = info.GetBoolean("_alwaysFetchNotificationMessages");
			_alreadyFetchedNotificationMessages = info.GetBoolean("_alreadyFetchedNotificationMessages");
			_formCollectionViaEmailMessage = (VarioSL.Entities.CollectionClasses.FormCollection)info.GetValue("_formCollectionViaEmailMessage", typeof(VarioSL.Entities.CollectionClasses.FormCollection));
			_alwaysFetchFormCollectionViaEmailMessage = info.GetBoolean("_alwaysFetchFormCollectionViaEmailMessage");
			_alreadyFetchedFormCollectionViaEmailMessage = info.GetBoolean("_alreadyFetchedFormCollectionViaEmailMessage");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAccountMessageSettings = (_accountMessageSettings.Count > 0);
			_alreadyFetchedEmailMessages = (_emailMessages.Count > 0);
			_alreadyFetchedEventSettings = (_eventSettings.Count > 0);
			_alreadyFetchedMessages = (_messages.Count > 0);
			_alreadyFetchedNotificationMessages = (_notificationMessages.Count > 0);
			_alreadyFetchedFormCollectionViaEmailMessage = (_formCollectionViaEmailMessage.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AccountMessageSettings":
					toReturn.Add(Relations.AccountMessageSettingEntityUsingEmailEventID);
					break;
				case "EmailMessages":
					toReturn.Add(Relations.EmailMessageEntityUsingEmailEventID);
					break;
				case "EventSettings":
					toReturn.Add(Relations.EventSettingEntityUsingEmailEventID);
					break;
				case "Messages":
					toReturn.Add(Relations.MessageEntityUsingEmailEventID);
					break;
				case "NotificationMessages":
					toReturn.Add(Relations.NotificationMessageEntityUsingEmailEventID);
					break;
				case "FormCollectionViaEmailMessage":
					toReturn.Add(Relations.EmailMessageEntityUsingEmailEventID, "EmailEventEntity__", "EmailMessage_", JoinHint.None);
					toReturn.Add(EmailMessageEntity.Relations.FormEntityUsingFormID, "EmailMessage_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_accountMessageSettings", (!this.MarkedForDeletion?_accountMessageSettings:null));
			info.AddValue("_alwaysFetchAccountMessageSettings", _alwaysFetchAccountMessageSettings);
			info.AddValue("_alreadyFetchedAccountMessageSettings", _alreadyFetchedAccountMessageSettings);
			info.AddValue("_emailMessages", (!this.MarkedForDeletion?_emailMessages:null));
			info.AddValue("_alwaysFetchEmailMessages", _alwaysFetchEmailMessages);
			info.AddValue("_alreadyFetchedEmailMessages", _alreadyFetchedEmailMessages);
			info.AddValue("_eventSettings", (!this.MarkedForDeletion?_eventSettings:null));
			info.AddValue("_alwaysFetchEventSettings", _alwaysFetchEventSettings);
			info.AddValue("_alreadyFetchedEventSettings", _alreadyFetchedEventSettings);
			info.AddValue("_messages", (!this.MarkedForDeletion?_messages:null));
			info.AddValue("_alwaysFetchMessages", _alwaysFetchMessages);
			info.AddValue("_alreadyFetchedMessages", _alreadyFetchedMessages);
			info.AddValue("_notificationMessages", (!this.MarkedForDeletion?_notificationMessages:null));
			info.AddValue("_alwaysFetchNotificationMessages", _alwaysFetchNotificationMessages);
			info.AddValue("_alreadyFetchedNotificationMessages", _alreadyFetchedNotificationMessages);
			info.AddValue("_formCollectionViaEmailMessage", (!this.MarkedForDeletion?_formCollectionViaEmailMessage:null));
			info.AddValue("_alwaysFetchFormCollectionViaEmailMessage", _alwaysFetchFormCollectionViaEmailMessage);
			info.AddValue("_alreadyFetchedFormCollectionViaEmailMessage", _alreadyFetchedFormCollectionViaEmailMessage);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AccountMessageSettings":
					_alreadyFetchedAccountMessageSettings = true;
					if(entity!=null)
					{
						this.AccountMessageSettings.Add((AccountMessageSettingEntity)entity);
					}
					break;
				case "EmailMessages":
					_alreadyFetchedEmailMessages = true;
					if(entity!=null)
					{
						this.EmailMessages.Add((EmailMessageEntity)entity);
					}
					break;
				case "EventSettings":
					_alreadyFetchedEventSettings = true;
					if(entity!=null)
					{
						this.EventSettings.Add((EventSettingEntity)entity);
					}
					break;
				case "Messages":
					_alreadyFetchedMessages = true;
					if(entity!=null)
					{
						this.Messages.Add((MessageEntity)entity);
					}
					break;
				case "NotificationMessages":
					_alreadyFetchedNotificationMessages = true;
					if(entity!=null)
					{
						this.NotificationMessages.Add((NotificationMessageEntity)entity);
					}
					break;
				case "FormCollectionViaEmailMessage":
					_alreadyFetchedFormCollectionViaEmailMessage = true;
					if(entity!=null)
					{
						this.FormCollectionViaEmailMessage.Add((FormEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AccountMessageSettings":
					_accountMessageSettings.Add((AccountMessageSettingEntity)relatedEntity);
					break;
				case "EmailMessages":
					_emailMessages.Add((EmailMessageEntity)relatedEntity);
					break;
				case "EventSettings":
					_eventSettings.Add((EventSettingEntity)relatedEntity);
					break;
				case "Messages":
					_messages.Add((MessageEntity)relatedEntity);
					break;
				case "NotificationMessages":
					_notificationMessages.Add((NotificationMessageEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AccountMessageSettings":
					this.PerformRelatedEntityRemoval(_accountMessageSettings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EmailMessages":
					this.PerformRelatedEntityRemoval(_emailMessages, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EventSettings":
					this.PerformRelatedEntityRemoval(_eventSettings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Messages":
					this.PerformRelatedEntityRemoval(_messages, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "NotificationMessages":
					this.PerformRelatedEntityRemoval(_notificationMessages, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_accountMessageSettings);
			toReturn.Add(_emailMessages);
			toReturn.Add(_eventSettings);
			toReturn.Add(_messages);
			toReturn.Add(_notificationMessages);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="eventName">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEventName(System.String eventName)
		{
			return FetchUsingUCEventName( eventName, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="eventName">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEventName(System.String eventName, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCEventName( eventName, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="eventName">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEventName(System.String eventName, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCEventName( eventName, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="eventName">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEventName(System.String eventName, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((EmailEventDAO)CreateDAOInstance()).FetchEmailEventUsingUCEventName(this, this.Transaction, eventName, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="enumerationValue">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEnumerationValue(VarioSL.Entities.Enumerations.EventType enumerationValue)
		{
			return FetchUsingUCEnumerationValue( enumerationValue, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="enumerationValue">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEnumerationValue(VarioSL.Entities.Enumerations.EventType enumerationValue, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCEnumerationValue( enumerationValue, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="enumerationValue">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEnumerationValue(VarioSL.Entities.Enumerations.EventType enumerationValue, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCEnumerationValue( enumerationValue, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="enumerationValue">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEnumerationValue(VarioSL.Entities.Enumerations.EventType enumerationValue, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((EmailEventDAO)CreateDAOInstance()).FetchEmailEventUsingUCEnumerationValue(this, this.Transaction, enumerationValue, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventID">PK value for EmailEvent which data should be fetched into this EmailEvent object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventID)
		{
			return FetchUsingPK(emailEventID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventID">PK value for EmailEvent which data should be fetched into this EmailEvent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(emailEventID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventID">PK value for EmailEvent which data should be fetched into this EmailEvent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(emailEventID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventID">PK value for EmailEvent which data should be fetched into this EmailEvent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(emailEventID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EmailEventID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new EmailEventRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AccountMessageSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AccountMessageSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection GetMultiAccountMessageSettings(bool forceFetch)
		{
			return GetMultiAccountMessageSettings(forceFetch, _accountMessageSettings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccountMessageSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AccountMessageSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection GetMultiAccountMessageSettings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAccountMessageSettings(forceFetch, _accountMessageSettings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AccountMessageSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection GetMultiAccountMessageSettings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAccountMessageSettings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccountMessageSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection GetMultiAccountMessageSettings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAccountMessageSettings || forceFetch || _alwaysFetchAccountMessageSettings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_accountMessageSettings);
				_accountMessageSettings.SuppressClearInGetMulti=!forceFetch;
				_accountMessageSettings.EntityFactoryToUse = entityFactoryToUse;
				_accountMessageSettings.GetMultiManyToOne(null, this, filter);
				_accountMessageSettings.SuppressClearInGetMulti=false;
				_alreadyFetchedAccountMessageSettings = true;
			}
			return _accountMessageSettings;
		}

		/// <summary> Sets the collection parameters for the collection for 'AccountMessageSettings'. These settings will be taken into account
		/// when the property AccountMessageSettings is requested or GetMultiAccountMessageSettings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAccountMessageSettings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_accountMessageSettings.SortClauses=sortClauses;
			_accountMessageSettings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EmailMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailMessageCollection GetMultiEmailMessages(bool forceFetch)
		{
			return GetMultiEmailMessages(forceFetch, _emailMessages.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EmailMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailMessageCollection GetMultiEmailMessages(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEmailMessages(forceFetch, _emailMessages.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EmailMessageCollection GetMultiEmailMessages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEmailMessages(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EmailMessageCollection GetMultiEmailMessages(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEmailMessages || forceFetch || _alwaysFetchEmailMessages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_emailMessages);
				_emailMessages.SuppressClearInGetMulti=!forceFetch;
				_emailMessages.EntityFactoryToUse = entityFactoryToUse;
				_emailMessages.GetMultiManyToOne(this, null, null, filter);
				_emailMessages.SuppressClearInGetMulti=false;
				_alreadyFetchedEmailMessages = true;
			}
			return _emailMessages;
		}

		/// <summary> Sets the collection parameters for the collection for 'EmailMessages'. These settings will be taken into account
		/// when the property EmailMessages is requested or GetMultiEmailMessages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEmailMessages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_emailMessages.SortClauses=sortClauses;
			_emailMessages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EventSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EventSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EventSettingCollection GetMultiEventSettings(bool forceFetch)
		{
			return GetMultiEventSettings(forceFetch, _eventSettings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EventSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EventSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EventSettingCollection GetMultiEventSettings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEventSettings(forceFetch, _eventSettings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EventSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EventSettingCollection GetMultiEventSettings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEventSettings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EventSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EventSettingCollection GetMultiEventSettings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEventSettings || forceFetch || _alwaysFetchEventSettings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_eventSettings);
				_eventSettings.SuppressClearInGetMulti=!forceFetch;
				_eventSettings.EntityFactoryToUse = entityFactoryToUse;
				_eventSettings.GetMultiManyToOne(this, null, filter);
				_eventSettings.SuppressClearInGetMulti=false;
				_alreadyFetchedEventSettings = true;
			}
			return _eventSettings;
		}

		/// <summary> Sets the collection parameters for the collection for 'EventSettings'. These settings will be taken into account
		/// when the property EventSettings is requested or GetMultiEventSettings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEventSettings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_eventSettings.SortClauses=sortClauses;
			_eventSettings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.MessageCollection GetMultiMessages(bool forceFetch)
		{
			return GetMultiMessages(forceFetch, _messages.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.MessageCollection GetMultiMessages(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessages(forceFetch, _messages.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.MessageCollection GetMultiMessages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessages(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.MessageCollection GetMultiMessages(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessages || forceFetch || _alwaysFetchMessages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messages);
				_messages.SuppressClearInGetMulti=!forceFetch;
				_messages.EntityFactoryToUse = entityFactoryToUse;
				_messages.GetMultiManyToOne(this, null, filter);
				_messages.SuppressClearInGetMulti=false;
				_alreadyFetchedMessages = true;
			}
			return _messages;
		}

		/// <summary> Sets the collection parameters for the collection for 'Messages'. These settings will be taken into account
		/// when the property Messages is requested or GetMultiMessages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messages.SortClauses=sortClauses;
			_messages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageCollection GetMultiNotificationMessages(bool forceFetch)
		{
			return GetMultiNotificationMessages(forceFetch, _notificationMessages.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageCollection GetMultiNotificationMessages(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNotificationMessages(forceFetch, _notificationMessages.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageCollection GetMultiNotificationMessages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNotificationMessages(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageCollection GetMultiNotificationMessages(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNotificationMessages || forceFetch || _alwaysFetchNotificationMessages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_notificationMessages);
				_notificationMessages.SuppressClearInGetMulti=!forceFetch;
				_notificationMessages.EntityFactoryToUse = entityFactoryToUse;
				_notificationMessages.GetMultiManyToOne(this, null, filter);
				_notificationMessages.SuppressClearInGetMulti=false;
				_alreadyFetchedNotificationMessages = true;
			}
			return _notificationMessages;
		}

		/// <summary> Sets the collection parameters for the collection for 'NotificationMessages'. These settings will be taken into account
		/// when the property NotificationMessages is requested or GetMultiNotificationMessages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNotificationMessages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_notificationMessages.SortClauses=sortClauses;
			_notificationMessages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FormEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FormEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FormCollection GetMultiFormCollectionViaEmailMessage(bool forceFetch)
		{
			return GetMultiFormCollectionViaEmailMessage(forceFetch, _formCollectionViaEmailMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'FormEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FormCollection GetMultiFormCollectionViaEmailMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedFormCollectionViaEmailMessage || forceFetch || _alwaysFetchFormCollectionViaEmailMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_formCollectionViaEmailMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EmailEventFields.EmailEventID, ComparisonOperator.Equal, this.EmailEventID, "EmailEventEntity__"));
				_formCollectionViaEmailMessage.SuppressClearInGetMulti=!forceFetch;
				_formCollectionViaEmailMessage.EntityFactoryToUse = entityFactoryToUse;
				_formCollectionViaEmailMessage.GetMulti(filter, GetRelationsForField("FormCollectionViaEmailMessage"));
				_formCollectionViaEmailMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedFormCollectionViaEmailMessage = true;
			}
			return _formCollectionViaEmailMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'FormCollectionViaEmailMessage'. These settings will be taken into account
		/// when the property FormCollectionViaEmailMessage is requested or GetMultiFormCollectionViaEmailMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFormCollectionViaEmailMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_formCollectionViaEmailMessage.SortClauses=sortClauses;
			_formCollectionViaEmailMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AccountMessageSettings", _accountMessageSettings);
			toReturn.Add("EmailMessages", _emailMessages);
			toReturn.Add("EventSettings", _eventSettings);
			toReturn.Add("Messages", _messages);
			toReturn.Add("NotificationMessages", _notificationMessages);
			toReturn.Add("FormCollectionViaEmailMessage", _formCollectionViaEmailMessage);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="emailEventID">PK value for EmailEvent which data should be fetched into this EmailEvent object</param>
		/// <param name="validator">The validator object for this EmailEventEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 emailEventID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(emailEventID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_accountMessageSettings = new VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection();
			_accountMessageSettings.SetContainingEntityInfo(this, "EmailEvent");

			_emailMessages = new VarioSL.Entities.CollectionClasses.EmailMessageCollection();
			_emailMessages.SetContainingEntityInfo(this, "EmailEvent");

			_eventSettings = new VarioSL.Entities.CollectionClasses.EventSettingCollection();
			_eventSettings.SetContainingEntityInfo(this, "EmailEvent");

			_messages = new VarioSL.Entities.CollectionClasses.MessageCollection();
			_messages.SetContainingEntityInfo(this, "EmailEvent");

			_notificationMessages = new VarioSL.Entities.CollectionClasses.NotificationMessageCollection();
			_notificationMessages.SetContainingEntityInfo(this, "EmailEvent");
			_formCollectionViaEmailMessage = new VarioSL.Entities.CollectionClasses.FormCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmailEventID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EnumerationValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EventName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsConfigurable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SendToAnonymousCustomerAccounts", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SendToCardHolder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SendToExecutiveCustomerAccount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SendToLoadOnlyCustomerAccounts", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SendToPrimaryCustomerAccount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFormSchemaName", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="emailEventID">PK value for EmailEvent which data should be fetched into this EmailEvent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 emailEventID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)EmailEventFieldIndex.EmailEventID].ForcedCurrentValueWrite(emailEventID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateEmailEventDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new EmailEventEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static EmailEventRelations Relations
		{
			get	{ return new EmailEventRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AccountMessageSetting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccountMessageSettings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection(), (IEntityRelation)GetRelationsForField("AccountMessageSettings")[0], (int)VarioSL.Entities.EntityType.EmailEventEntity, (int)VarioSL.Entities.EntityType.AccountMessageSettingEntity, 0, null, null, null, "AccountMessageSettings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailMessages
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailMessageCollection(), (IEntityRelation)GetRelationsForField("EmailMessages")[0], (int)VarioSL.Entities.EntityType.EmailEventEntity, (int)VarioSL.Entities.EntityType.EmailMessageEntity, 0, null, null, null, "EmailMessages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EventSetting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEventSettings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EventSettingCollection(), (IEntityRelation)GetRelationsForField("EventSettings")[0], (int)VarioSL.Entities.EntityType.EmailEventEntity, (int)VarioSL.Entities.EntityType.EventSettingEntity, 0, null, null, null, "EventSettings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Message' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessages
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.MessageCollection(), (IEntityRelation)GetRelationsForField("Messages")[0], (int)VarioSL.Entities.EntityType.EmailEventEntity, (int)VarioSL.Entities.EntityType.MessageEntity, 0, null, null, null, "Messages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NotificationMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNotificationMessages
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.NotificationMessageCollection(), (IEntityRelation)GetRelationsForField("NotificationMessages")[0], (int)VarioSL.Entities.EntityType.EmailEventEntity, (int)VarioSL.Entities.EntityType.NotificationMessageEntity, 0, null, null, null, "NotificationMessages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Form'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFormCollectionViaEmailMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.EmailMessageEntityUsingEmailEventID;
				intermediateRelation.SetAliases(string.Empty, "EmailMessage_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FormCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.EmailEventEntity, (int)VarioSL.Entities.EntityType.FormEntity, 0, null, null, GetRelationsForField("FormCollectionViaEmailMessage"), "FormCollectionViaEmailMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)EmailEventFieldIndex.Description, true); }
			set	{ SetValue((int)EmailEventFieldIndex.Description, value, true); }
		}

		/// <summary> The EmailEventID property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."EMAILEVENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 EmailEventID
		{
			get { return (System.Int64)GetValue((int)EmailEventFieldIndex.EmailEventID, true); }
			set	{ SetValue((int)EmailEventFieldIndex.EmailEventID, value, true); }
		}

		/// <summary> The EnumerationValue property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."ENUMERATIONVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.EventType EnumerationValue
		{
			get { return (VarioSL.Entities.Enumerations.EventType)GetValue((int)EmailEventFieldIndex.EnumerationValue, true); }
			set	{ SetValue((int)EmailEventFieldIndex.EnumerationValue, value, true); }
		}

		/// <summary> The EventName property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."EVENTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String EventName
		{
			get { return (System.String)GetValue((int)EmailEventFieldIndex.EventName, true); }
			set	{ SetValue((int)EmailEventFieldIndex.EventName, value, true); }
		}

		/// <summary> The IsActive property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."ISACTIVE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsActive
		{
			get { return (System.Boolean)GetValue((int)EmailEventFieldIndex.IsActive, true); }
			set	{ SetValue((int)EmailEventFieldIndex.IsActive, value, true); }
		}

		/// <summary> The IsConfigurable property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."ISCONFIGURABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsConfigurable
		{
			get { return (System.Boolean)GetValue((int)EmailEventFieldIndex.IsConfigurable, true); }
			set	{ SetValue((int)EmailEventFieldIndex.IsConfigurable, value, true); }
		}

		/// <summary> The LastModified property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)EmailEventFieldIndex.LastModified, true); }
			set	{ SetValue((int)EmailEventFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)EmailEventFieldIndex.LastUser, true); }
			set	{ SetValue((int)EmailEventFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MessageType property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."MESSAGETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.MessageType> MessageType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.MessageType>)GetValue((int)EmailEventFieldIndex.MessageType, false); }
			set	{ SetValue((int)EmailEventFieldIndex.MessageType, value, true); }
		}

		/// <summary> The SendToAnonymousCustomerAccounts property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."SENDTOANONYMOUSCUSTOMERACCS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SendToAnonymousCustomerAccounts
		{
			get { return (System.Boolean)GetValue((int)EmailEventFieldIndex.SendToAnonymousCustomerAccounts, true); }
			set	{ SetValue((int)EmailEventFieldIndex.SendToAnonymousCustomerAccounts, value, true); }
		}

		/// <summary> The SendToCardHolder property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."SENDTOCARDHOLDER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SendToCardHolder
		{
			get { return (System.Boolean)GetValue((int)EmailEventFieldIndex.SendToCardHolder, true); }
			set	{ SetValue((int)EmailEventFieldIndex.SendToCardHolder, value, true); }
		}

		/// <summary> The SendToExecutiveCustomerAccount property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."SENDTOEXECUTIVECUSTOMERACCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SendToExecutiveCustomerAccount
		{
			get { return (System.Boolean)GetValue((int)EmailEventFieldIndex.SendToExecutiveCustomerAccount, true); }
			set	{ SetValue((int)EmailEventFieldIndex.SendToExecutiveCustomerAccount, value, true); }
		}

		/// <summary> The SendToLoadOnlyCustomerAccounts property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."SENDTOLOADONLYCUSTOMERACCOUNTS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SendToLoadOnlyCustomerAccounts
		{
			get { return (System.Boolean)GetValue((int)EmailEventFieldIndex.SendToLoadOnlyCustomerAccounts, true); }
			set	{ SetValue((int)EmailEventFieldIndex.SendToLoadOnlyCustomerAccounts, value, true); }
		}

		/// <summary> The SendToPrimaryCustomerAccount property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."SENDTOPRIMARYCUSTOMERACCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SendToPrimaryCustomerAccount
		{
			get { return (System.Boolean)GetValue((int)EmailEventFieldIndex.SendToPrimaryCustomerAccount, true); }
			set	{ SetValue((int)EmailEventFieldIndex.SendToPrimaryCustomerAccount, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)EmailEventFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)EmailEventFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFormSchemaName property of the Entity EmailEvent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENT"."VALIDFORMSCHEMANAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ValidFormSchemaName
		{
			get { return (System.String)GetValue((int)EmailEventFieldIndex.ValidFormSchemaName, true); }
			set	{ SetValue((int)EmailEventFieldIndex.ValidFormSchemaName, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AccountMessageSettingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAccountMessageSettings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AccountMessageSettingCollection AccountMessageSettings
		{
			get	{ return GetMultiAccountMessageSettings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AccountMessageSettings. When set to true, AccountMessageSettings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccountMessageSettings is accessed. You can always execute/ a forced fetch by calling GetMultiAccountMessageSettings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccountMessageSettings
		{
			get	{ return _alwaysFetchAccountMessageSettings; }
			set	{ _alwaysFetchAccountMessageSettings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AccountMessageSettings already has been fetched. Setting this property to false when AccountMessageSettings has been fetched
		/// will clear the AccountMessageSettings collection well. Setting this property to true while AccountMessageSettings hasn't been fetched disables lazy loading for AccountMessageSettings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccountMessageSettings
		{
			get { return _alreadyFetchedAccountMessageSettings;}
			set 
			{
				if(_alreadyFetchedAccountMessageSettings && !value && (_accountMessageSettings != null))
				{
					_accountMessageSettings.Clear();
				}
				_alreadyFetchedAccountMessageSettings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEmailMessages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EmailMessageCollection EmailMessages
		{
			get	{ return GetMultiEmailMessages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EmailMessages. When set to true, EmailMessages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailMessages is accessed. You can always execute/ a forced fetch by calling GetMultiEmailMessages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailMessages
		{
			get	{ return _alwaysFetchEmailMessages; }
			set	{ _alwaysFetchEmailMessages = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailMessages already has been fetched. Setting this property to false when EmailMessages has been fetched
		/// will clear the EmailMessages collection well. Setting this property to true while EmailMessages hasn't been fetched disables lazy loading for EmailMessages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailMessages
		{
			get { return _alreadyFetchedEmailMessages;}
			set 
			{
				if(_alreadyFetchedEmailMessages && !value && (_emailMessages != null))
				{
					_emailMessages.Clear();
				}
				_alreadyFetchedEmailMessages = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EventSettingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEventSettings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EventSettingCollection EventSettings
		{
			get	{ return GetMultiEventSettings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EventSettings. When set to true, EventSettings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EventSettings is accessed. You can always execute/ a forced fetch by calling GetMultiEventSettings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEventSettings
		{
			get	{ return _alwaysFetchEventSettings; }
			set	{ _alwaysFetchEventSettings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EventSettings already has been fetched. Setting this property to false when EventSettings has been fetched
		/// will clear the EventSettings collection well. Setting this property to true while EventSettings hasn't been fetched disables lazy loading for EventSettings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEventSettings
		{
			get { return _alreadyFetchedEventSettings;}
			set 
			{
				if(_alreadyFetchedEventSettings && !value && (_eventSettings != null))
				{
					_eventSettings.Clear();
				}
				_alreadyFetchedEventSettings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.MessageCollection Messages
		{
			get	{ return GetMultiMessages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Messages. When set to true, Messages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Messages is accessed. You can always execute/ a forced fetch by calling GetMultiMessages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessages
		{
			get	{ return _alwaysFetchMessages; }
			set	{ _alwaysFetchMessages = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Messages already has been fetched. Setting this property to false when Messages has been fetched
		/// will clear the Messages collection well. Setting this property to true while Messages hasn't been fetched disables lazy loading for Messages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessages
		{
			get { return _alreadyFetchedMessages;}
			set 
			{
				if(_alreadyFetchedMessages && !value && (_messages != null))
				{
					_messages.Clear();
				}
				_alreadyFetchedMessages = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNotificationMessages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageCollection NotificationMessages
		{
			get	{ return GetMultiNotificationMessages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NotificationMessages. When set to true, NotificationMessages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NotificationMessages is accessed. You can always execute/ a forced fetch by calling GetMultiNotificationMessages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNotificationMessages
		{
			get	{ return _alwaysFetchNotificationMessages; }
			set	{ _alwaysFetchNotificationMessages = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NotificationMessages already has been fetched. Setting this property to false when NotificationMessages has been fetched
		/// will clear the NotificationMessages collection well. Setting this property to true while NotificationMessages hasn't been fetched disables lazy loading for NotificationMessages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNotificationMessages
		{
			get { return _alreadyFetchedNotificationMessages;}
			set 
			{
				if(_alreadyFetchedNotificationMessages && !value && (_notificationMessages != null))
				{
					_notificationMessages.Clear();
				}
				_alreadyFetchedNotificationMessages = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'FormEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFormCollectionViaEmailMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FormCollection FormCollectionViaEmailMessage
		{
			get { return GetMultiFormCollectionViaEmailMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FormCollectionViaEmailMessage. When set to true, FormCollectionViaEmailMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FormCollectionViaEmailMessage is accessed. You can always execute a forced fetch by calling GetMultiFormCollectionViaEmailMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFormCollectionViaEmailMessage
		{
			get	{ return _alwaysFetchFormCollectionViaEmailMessage; }
			set	{ _alwaysFetchFormCollectionViaEmailMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FormCollectionViaEmailMessage already has been fetched. Setting this property to false when FormCollectionViaEmailMessage has been fetched
		/// will clear the FormCollectionViaEmailMessage collection well. Setting this property to true while FormCollectionViaEmailMessage hasn't been fetched disables lazy loading for FormCollectionViaEmailMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFormCollectionViaEmailMessage
		{
			get { return _alreadyFetchedFormCollectionViaEmailMessage;}
			set 
			{
				if(_alreadyFetchedFormCollectionViaEmailMessage && !value && (_formCollectionViaEmailMessage != null))
				{
					_formCollectionViaEmailMessage.Clear();
				}
				_alreadyFetchedFormCollectionViaEmailMessage = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.EmailEventEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
