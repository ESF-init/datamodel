﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AreaListElement'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AreaListElementEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AreaListElementGroupEntity _areaListElementGroup;
		private bool	_alwaysFetchAreaListElementGroup, _alreadyFetchedAreaListElementGroup, _areaListElementGroupReturnsNewIfNotFound;
		private AreaTypeEntity _areaTypeElement;
		private bool	_alwaysFetchAreaTypeElement, _alreadyFetchedAreaTypeElement, _areaTypeElementReturnsNewIfNotFound;
		private FareStageEntity _fareStage;
		private bool	_alwaysFetchFareStage, _alreadyFetchedFareStage, _fareStageReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AreaListElementGroup</summary>
			public static readonly string AreaListElementGroup = "AreaListElementGroup";
			/// <summary>Member name AreaTypeElement</summary>
			public static readonly string AreaTypeElement = "AreaTypeElement";
			/// <summary>Member name FareStage</summary>
			public static readonly string FareStage = "FareStage";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AreaListElementEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AreaListElementEntity() :base("AreaListElementEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="areaListElementID">PK value for AreaListElement which data should be fetched into this AreaListElement object</param>
		public AreaListElementEntity(System.Int64 areaListElementID):base("AreaListElementEntity")
		{
			InitClassFetch(areaListElementID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="areaListElementID">PK value for AreaListElement which data should be fetched into this AreaListElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AreaListElementEntity(System.Int64 areaListElementID, IPrefetchPath prefetchPathToUse):base("AreaListElementEntity")
		{
			InitClassFetch(areaListElementID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="areaListElementID">PK value for AreaListElement which data should be fetched into this AreaListElement object</param>
		/// <param name="validator">The custom validator object for this AreaListElementEntity</param>
		public AreaListElementEntity(System.Int64 areaListElementID, IValidator validator):base("AreaListElementEntity")
		{
			InitClassFetch(areaListElementID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AreaListElementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_areaListElementGroup = (AreaListElementGroupEntity)info.GetValue("_areaListElementGroup", typeof(AreaListElementGroupEntity));
			if(_areaListElementGroup!=null)
			{
				_areaListElementGroup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_areaListElementGroupReturnsNewIfNotFound = info.GetBoolean("_areaListElementGroupReturnsNewIfNotFound");
			_alwaysFetchAreaListElementGroup = info.GetBoolean("_alwaysFetchAreaListElementGroup");
			_alreadyFetchedAreaListElementGroup = info.GetBoolean("_alreadyFetchedAreaListElementGroup");

			_areaTypeElement = (AreaTypeEntity)info.GetValue("_areaTypeElement", typeof(AreaTypeEntity));
			if(_areaTypeElement!=null)
			{
				_areaTypeElement.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_areaTypeElementReturnsNewIfNotFound = info.GetBoolean("_areaTypeElementReturnsNewIfNotFound");
			_alwaysFetchAreaTypeElement = info.GetBoolean("_alwaysFetchAreaTypeElement");
			_alreadyFetchedAreaTypeElement = info.GetBoolean("_alreadyFetchedAreaTypeElement");

			_fareStage = (FareStageEntity)info.GetValue("_fareStage", typeof(FareStageEntity));
			if(_fareStage!=null)
			{
				_fareStage.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareStageReturnsNewIfNotFound = info.GetBoolean("_fareStageReturnsNewIfNotFound");
			_alwaysFetchFareStage = info.GetBoolean("_alwaysFetchFareStage");
			_alreadyFetchedFareStage = info.GetBoolean("_alreadyFetchedFareStage");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AreaListElementFieldIndex)fieldIndex)
			{
				case AreaListElementFieldIndex.AreaInstanceTypeID:
					DesetupSyncAreaTypeElement(true, false);
					_alreadyFetchedAreaTypeElement = false;
					break;
				case AreaListElementFieldIndex.AreaListID:
					DesetupSyncAreaListElementGroup(true, false);
					_alreadyFetchedAreaListElementGroup = false;
					break;
				case AreaListElementFieldIndex.FareStageID:
					DesetupSyncFareStage(true, false);
					_alreadyFetchedFareStage = false;
					break;
				case AreaListElementFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAreaListElementGroup = (_areaListElementGroup != null);
			_alreadyFetchedAreaTypeElement = (_areaTypeElement != null);
			_alreadyFetchedFareStage = (_fareStage != null);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AreaListElementGroup":
					toReturn.Add(Relations.AreaListElementGroupEntityUsingAreaListID);
					break;
				case "AreaTypeElement":
					toReturn.Add(Relations.AreaTypeEntityUsingAreaInstanceTypeID);
					break;
				case "FareStage":
					toReturn.Add(Relations.FareStageEntityUsingFareStageID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_areaListElementGroup", (!this.MarkedForDeletion?_areaListElementGroup:null));
			info.AddValue("_areaListElementGroupReturnsNewIfNotFound", _areaListElementGroupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAreaListElementGroup", _alwaysFetchAreaListElementGroup);
			info.AddValue("_alreadyFetchedAreaListElementGroup", _alreadyFetchedAreaListElementGroup);
			info.AddValue("_areaTypeElement", (!this.MarkedForDeletion?_areaTypeElement:null));
			info.AddValue("_areaTypeElementReturnsNewIfNotFound", _areaTypeElementReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAreaTypeElement", _alwaysFetchAreaTypeElement);
			info.AddValue("_alreadyFetchedAreaTypeElement", _alreadyFetchedAreaTypeElement);
			info.AddValue("_fareStage", (!this.MarkedForDeletion?_fareStage:null));
			info.AddValue("_fareStageReturnsNewIfNotFound", _fareStageReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareStage", _alwaysFetchFareStage);
			info.AddValue("_alreadyFetchedFareStage", _alreadyFetchedFareStage);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AreaListElementGroup":
					_alreadyFetchedAreaListElementGroup = true;
					this.AreaListElementGroup = (AreaListElementGroupEntity)entity;
					break;
				case "AreaTypeElement":
					_alreadyFetchedAreaTypeElement = true;
					this.AreaTypeElement = (AreaTypeEntity)entity;
					break;
				case "FareStage":
					_alreadyFetchedFareStage = true;
					this.FareStage = (FareStageEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AreaListElementGroup":
					SetupSyncAreaListElementGroup(relatedEntity);
					break;
				case "AreaTypeElement":
					SetupSyncAreaTypeElement(relatedEntity);
					break;
				case "FareStage":
					SetupSyncFareStage(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AreaListElementGroup":
					DesetupSyncAreaListElementGroup(false, true);
					break;
				case "AreaTypeElement":
					DesetupSyncAreaTypeElement(false, true);
					break;
				case "FareStage":
					DesetupSyncFareStage(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_areaListElementGroup!=null)
			{
				toReturn.Add(_areaListElementGroup);
			}
			if(_areaTypeElement!=null)
			{
				toReturn.Add(_areaTypeElement);
			}
			if(_fareStage!=null)
			{
				toReturn.Add(_fareStage);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="areaListElementID">PK value for AreaListElement which data should be fetched into this AreaListElement object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 areaListElementID)
		{
			return FetchUsingPK(areaListElementID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="areaListElementID">PK value for AreaListElement which data should be fetched into this AreaListElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 areaListElementID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(areaListElementID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="areaListElementID">PK value for AreaListElement which data should be fetched into this AreaListElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 areaListElementID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(areaListElementID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="areaListElementID">PK value for AreaListElement which data should be fetched into this AreaListElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 areaListElementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(areaListElementID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AreaListElementID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AreaListElementRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AreaListElementGroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AreaListElementGroupEntity' which is related to this entity.</returns>
		public AreaListElementGroupEntity GetSingleAreaListElementGroup()
		{
			return GetSingleAreaListElementGroup(false);
		}

		/// <summary> Retrieves the related entity of type 'AreaListElementGroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AreaListElementGroupEntity' which is related to this entity.</returns>
		public virtual AreaListElementGroupEntity GetSingleAreaListElementGroup(bool forceFetch)
		{
			if( ( !_alreadyFetchedAreaListElementGroup || forceFetch || _alwaysFetchAreaListElementGroup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AreaListElementGroupEntityUsingAreaListID);
				AreaListElementGroupEntity newEntity = new AreaListElementGroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AreaListID);
				}
				if(fetchResult)
				{
					newEntity = (AreaListElementGroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_areaListElementGroupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AreaListElementGroup = newEntity;
				_alreadyFetchedAreaListElementGroup = fetchResult;
			}
			return _areaListElementGroup;
		}


		/// <summary> Retrieves the related entity of type 'AreaTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AreaTypeEntity' which is related to this entity.</returns>
		public AreaTypeEntity GetSingleAreaTypeElement()
		{
			return GetSingleAreaTypeElement(false);
		}

		/// <summary> Retrieves the related entity of type 'AreaTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AreaTypeEntity' which is related to this entity.</returns>
		public virtual AreaTypeEntity GetSingleAreaTypeElement(bool forceFetch)
		{
			if( ( !_alreadyFetchedAreaTypeElement || forceFetch || _alwaysFetchAreaTypeElement) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AreaTypeEntityUsingAreaInstanceTypeID);
				AreaTypeEntity newEntity = new AreaTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AreaInstanceTypeID);
				}
				if(fetchResult)
				{
					newEntity = (AreaTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_areaTypeElementReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AreaTypeElement = newEntity;
				_alreadyFetchedAreaTypeElement = fetchResult;
			}
			return _areaTypeElement;
		}


		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public FareStageEntity GetSingleFareStage()
		{
			return GetSingleFareStage(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public virtual FareStageEntity GetSingleFareStage(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareStage || forceFetch || _alwaysFetchFareStage) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageEntityUsingFareStageID);
				FareStageEntity newEntity = new FareStageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareStageID);
				}
				if(fetchResult)
				{
					newEntity = (FareStageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareStageReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareStage = newEntity;
				_alreadyFetchedFareStage = fetchResult;
			}
			return _fareStage;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID);
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AreaListElementGroup", _areaListElementGroup);
			toReturn.Add("AreaTypeElement", _areaTypeElement);
			toReturn.Add("FareStage", _fareStage);
			toReturn.Add("Tariff", _tariff);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="areaListElementID">PK value for AreaListElement which data should be fetched into this AreaListElement object</param>
		/// <param name="validator">The validator object for this AreaListElementEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 areaListElementID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(areaListElementID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_areaListElementGroupReturnsNewIfNotFound = false;
			_areaTypeElementReturnsNewIfNotFound = false;
			_fareStageReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AreaInstanceTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AreaListElementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AreaListID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _areaListElementGroup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAreaListElementGroup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _areaListElementGroup, new PropertyChangedEventHandler( OnAreaListElementGroupPropertyChanged ), "AreaListElementGroup", VarioSL.Entities.RelationClasses.StaticAreaListElementRelations.AreaListElementGroupEntityUsingAreaListIDStatic, true, signalRelatedEntity, "AreaListElement", resetFKFields, new int[] { (int)AreaListElementFieldIndex.AreaListID } );		
			_areaListElementGroup = null;
		}
		
		/// <summary> setups the sync logic for member _areaListElementGroup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAreaListElementGroup(IEntityCore relatedEntity)
		{
			if(_areaListElementGroup!=relatedEntity)
			{		
				DesetupSyncAreaListElementGroup(true, true);
				_areaListElementGroup = (AreaListElementGroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _areaListElementGroup, new PropertyChangedEventHandler( OnAreaListElementGroupPropertyChanged ), "AreaListElementGroup", VarioSL.Entities.RelationClasses.StaticAreaListElementRelations.AreaListElementGroupEntityUsingAreaListIDStatic, true, ref _alreadyFetchedAreaListElementGroup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAreaListElementGroupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _areaTypeElement</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAreaTypeElement(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _areaTypeElement, new PropertyChangedEventHandler( OnAreaTypeElementPropertyChanged ), "AreaTypeElement", VarioSL.Entities.RelationClasses.StaticAreaListElementRelations.AreaTypeEntityUsingAreaInstanceTypeIDStatic, true, signalRelatedEntity, "AreaListElement", resetFKFields, new int[] { (int)AreaListElementFieldIndex.AreaInstanceTypeID } );		
			_areaTypeElement = null;
		}
		
		/// <summary> setups the sync logic for member _areaTypeElement</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAreaTypeElement(IEntityCore relatedEntity)
		{
			if(_areaTypeElement!=relatedEntity)
			{		
				DesetupSyncAreaTypeElement(true, true);
				_areaTypeElement = (AreaTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _areaTypeElement, new PropertyChangedEventHandler( OnAreaTypeElementPropertyChanged ), "AreaTypeElement", VarioSL.Entities.RelationClasses.StaticAreaListElementRelations.AreaTypeEntityUsingAreaInstanceTypeIDStatic, true, ref _alreadyFetchedAreaTypeElement, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAreaTypeElementPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareStage</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareStage(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareStage, new PropertyChangedEventHandler( OnFareStagePropertyChanged ), "FareStage", VarioSL.Entities.RelationClasses.StaticAreaListElementRelations.FareStageEntityUsingFareStageIDStatic, true, signalRelatedEntity, "AreaListElement", resetFKFields, new int[] { (int)AreaListElementFieldIndex.FareStageID } );		
			_fareStage = null;
		}
		
		/// <summary> setups the sync logic for member _fareStage</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareStage(IEntityCore relatedEntity)
		{
			if(_fareStage!=relatedEntity)
			{		
				DesetupSyncFareStage(true, true);
				_fareStage = (FareStageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareStage, new PropertyChangedEventHandler( OnFareStagePropertyChanged ), "FareStage", VarioSL.Entities.RelationClasses.StaticAreaListElementRelations.FareStageEntityUsingFareStageIDStatic, true, ref _alreadyFetchedFareStage, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareStagePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticAreaListElementRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "AreaListElements", resetFKFields, new int[] { (int)AreaListElementFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticAreaListElementRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="areaListElementID">PK value for AreaListElement which data should be fetched into this AreaListElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 areaListElementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AreaListElementFieldIndex.AreaListElementID].ForcedCurrentValueWrite(areaListElementID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAreaListElementDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AreaListElementEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AreaListElementRelations Relations
		{
			get	{ return new AreaListElementRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AreaListElementGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAreaListElementGroup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AreaListElementGroupCollection(), (IEntityRelation)GetRelationsForField("AreaListElementGroup")[0], (int)VarioSL.Entities.EntityType.AreaListElementEntity, (int)VarioSL.Entities.EntityType.AreaListElementGroupEntity, 0, null, null, null, "AreaListElementGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AreaType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAreaTypeElement
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AreaTypeCollection(), (IEntityRelation)GetRelationsForField("AreaTypeElement")[0], (int)VarioSL.Entities.EntityType.AreaListElementEntity, (int)VarioSL.Entities.EntityType.AreaTypeEntity, 0, null, null, null, "AreaTypeElement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStage
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageCollection(), (IEntityRelation)GetRelationsForField("FareStage")[0], (int)VarioSL.Entities.EntityType.AreaListElementEntity, (int)VarioSL.Entities.EntityType.FareStageEntity, 0, null, null, null, "FareStage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.AreaListElementEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AreaInstanceTypeID property of the Entity AreaListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALISTELEMENT"."AREAINSTANCETYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 AreaInstanceTypeID
		{
			get { return (System.Int64)GetValue((int)AreaListElementFieldIndex.AreaInstanceTypeID, true); }
			set	{ SetValue((int)AreaListElementFieldIndex.AreaInstanceTypeID, value, true); }
		}

		/// <summary> The AreaListElementID property of the Entity AreaListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALISTELEMENT"."AREALISTELEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 AreaListElementID
		{
			get { return (System.Int64)GetValue((int)AreaListElementFieldIndex.AreaListElementID, true); }
			set	{ SetValue((int)AreaListElementFieldIndex.AreaListElementID, value, true); }
		}

		/// <summary> The AreaListID property of the Entity AreaListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALISTELEMENT"."AREALISTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 AreaListID
		{
			get { return (System.Int64)GetValue((int)AreaListElementFieldIndex.AreaListID, true); }
			set	{ SetValue((int)AreaListElementFieldIndex.AreaListID, value, true); }
		}

		/// <summary> The ExternalNumber property of the Entity AreaListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALISTELEMENT"."EXTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ExternalNumber
		{
			get { return (System.Int64)GetValue((int)AreaListElementFieldIndex.ExternalNumber, true); }
			set	{ SetValue((int)AreaListElementFieldIndex.ExternalNumber, value, true); }
		}

		/// <summary> The FareStageID property of the Entity AreaListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALISTELEMENT"."FARESTAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FareStageID
		{
			get { return (System.Int64)GetValue((int)AreaListElementFieldIndex.FareStageID, true); }
			set	{ SetValue((int)AreaListElementFieldIndex.FareStageID, value, true); }
		}

		/// <summary> The OrderNumber property of the Entity AreaListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALISTELEMENT"."ORDERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 OrderNumber
		{
			get { return (System.Int64)GetValue((int)AreaListElementFieldIndex.OrderNumber, true); }
			set	{ SetValue((int)AreaListElementFieldIndex.OrderNumber, value, true); }
		}

		/// <summary> The TariffID property of the Entity AreaListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALISTELEMENT"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)AreaListElementFieldIndex.TariffID, true); }
			set	{ SetValue((int)AreaListElementFieldIndex.TariffID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AreaListElementGroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAreaListElementGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AreaListElementGroupEntity AreaListElementGroup
		{
			get	{ return GetSingleAreaListElementGroup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAreaListElementGroup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AreaListElement", "AreaListElementGroup", _areaListElementGroup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AreaListElementGroup. When set to true, AreaListElementGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AreaListElementGroup is accessed. You can always execute a forced fetch by calling GetSingleAreaListElementGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAreaListElementGroup
		{
			get	{ return _alwaysFetchAreaListElementGroup; }
			set	{ _alwaysFetchAreaListElementGroup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AreaListElementGroup already has been fetched. Setting this property to false when AreaListElementGroup has been fetched
		/// will set AreaListElementGroup to null as well. Setting this property to true while AreaListElementGroup hasn't been fetched disables lazy loading for AreaListElementGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAreaListElementGroup
		{
			get { return _alreadyFetchedAreaListElementGroup;}
			set 
			{
				if(_alreadyFetchedAreaListElementGroup && !value)
				{
					this.AreaListElementGroup = null;
				}
				_alreadyFetchedAreaListElementGroup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AreaListElementGroup is not found
		/// in the database. When set to true, AreaListElementGroup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AreaListElementGroupReturnsNewIfNotFound
		{
			get	{ return _areaListElementGroupReturnsNewIfNotFound; }
			set { _areaListElementGroupReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AreaTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAreaTypeElement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AreaTypeEntity AreaTypeElement
		{
			get	{ return GetSingleAreaTypeElement(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAreaTypeElement(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AreaListElement", "AreaTypeElement", _areaTypeElement, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AreaTypeElement. When set to true, AreaTypeElement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AreaTypeElement is accessed. You can always execute a forced fetch by calling GetSingleAreaTypeElement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAreaTypeElement
		{
			get	{ return _alwaysFetchAreaTypeElement; }
			set	{ _alwaysFetchAreaTypeElement = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AreaTypeElement already has been fetched. Setting this property to false when AreaTypeElement has been fetched
		/// will set AreaTypeElement to null as well. Setting this property to true while AreaTypeElement hasn't been fetched disables lazy loading for AreaTypeElement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAreaTypeElement
		{
			get { return _alreadyFetchedAreaTypeElement;}
			set 
			{
				if(_alreadyFetchedAreaTypeElement && !value)
				{
					this.AreaTypeElement = null;
				}
				_alreadyFetchedAreaTypeElement = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AreaTypeElement is not found
		/// in the database. When set to true, AreaTypeElement will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AreaTypeElementReturnsNewIfNotFound
		{
			get	{ return _areaTypeElementReturnsNewIfNotFound; }
			set { _areaTypeElementReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareStageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareStage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageEntity FareStage
		{
			get	{ return GetSingleFareStage(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareStage(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AreaListElement", "FareStage", _fareStage, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareStage. When set to true, FareStage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStage is accessed. You can always execute a forced fetch by calling GetSingleFareStage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStage
		{
			get	{ return _alwaysFetchFareStage; }
			set	{ _alwaysFetchFareStage = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStage already has been fetched. Setting this property to false when FareStage has been fetched
		/// will set FareStage to null as well. Setting this property to true while FareStage hasn't been fetched disables lazy loading for FareStage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStage
		{
			get { return _alreadyFetchedFareStage;}
			set 
			{
				if(_alreadyFetchedFareStage && !value)
				{
					this.FareStage = null;
				}
				_alreadyFetchedFareStage = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareStage is not found
		/// in the database. When set to true, FareStage will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareStageReturnsNewIfNotFound
		{
			get	{ return _fareStageReturnsNewIfNotFound; }
			set { _fareStageReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AreaListElements", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AreaListElementEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
