﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SettlementRunValueToVarioSettlement'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SettlementRunValueToVarioSettlementEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSettlementEntity _varioSettlement;
		private bool	_alwaysFetchVarioSettlement, _alreadyFetchedVarioSettlement, _varioSettlementReturnsNewIfNotFound;
		private SettlementRunValueEntity _settlementRunValue;
		private bool	_alwaysFetchSettlementRunValue, _alreadyFetchedSettlementRunValue, _settlementRunValueReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name VarioSettlement</summary>
			public static readonly string VarioSettlement = "VarioSettlement";
			/// <summary>Member name SettlementRunValue</summary>
			public static readonly string SettlementRunValue = "SettlementRunValue";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SettlementRunValueToVarioSettlementEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SettlementRunValueToVarioSettlementEntity() :base("SettlementRunValueToVarioSettlementEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="varioSettlementID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		public SettlementRunValueToVarioSettlementEntity(System.Int64 settlementRunValueID, System.Int64 varioSettlementID):base("SettlementRunValueToVarioSettlementEntity")
		{
			InitClassFetch(settlementRunValueID, varioSettlementID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="varioSettlementID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SettlementRunValueToVarioSettlementEntity(System.Int64 settlementRunValueID, System.Int64 varioSettlementID, IPrefetchPath prefetchPathToUse):base("SettlementRunValueToVarioSettlementEntity")
		{
			InitClassFetch(settlementRunValueID, varioSettlementID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="varioSettlementID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="validator">The custom validator object for this SettlementRunValueToVarioSettlementEntity</param>
		public SettlementRunValueToVarioSettlementEntity(System.Int64 settlementRunValueID, System.Int64 varioSettlementID, IValidator validator):base("SettlementRunValueToVarioSettlementEntity")
		{
			InitClassFetch(settlementRunValueID, varioSettlementID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SettlementRunValueToVarioSettlementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_varioSettlement = (VarioSettlementEntity)info.GetValue("_varioSettlement", typeof(VarioSettlementEntity));
			if(_varioSettlement!=null)
			{
				_varioSettlement.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_varioSettlementReturnsNewIfNotFound = info.GetBoolean("_varioSettlementReturnsNewIfNotFound");
			_alwaysFetchVarioSettlement = info.GetBoolean("_alwaysFetchVarioSettlement");
			_alreadyFetchedVarioSettlement = info.GetBoolean("_alreadyFetchedVarioSettlement");

			_settlementRunValue = (SettlementRunValueEntity)info.GetValue("_settlementRunValue", typeof(SettlementRunValueEntity));
			if(_settlementRunValue!=null)
			{
				_settlementRunValue.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_settlementRunValueReturnsNewIfNotFound = info.GetBoolean("_settlementRunValueReturnsNewIfNotFound");
			_alwaysFetchSettlementRunValue = info.GetBoolean("_alwaysFetchSettlementRunValue");
			_alreadyFetchedSettlementRunValue = info.GetBoolean("_alreadyFetchedSettlementRunValue");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SettlementRunValueToVarioSettlementFieldIndex)fieldIndex)
			{
				case SettlementRunValueToVarioSettlementFieldIndex.SettlementRunValueID:
					DesetupSyncSettlementRunValue(true, false);
					_alreadyFetchedSettlementRunValue = false;
					break;
				case SettlementRunValueToVarioSettlementFieldIndex.VarioSettlementID:
					DesetupSyncVarioSettlement(true, false);
					_alreadyFetchedVarioSettlement = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedVarioSettlement = (_varioSettlement != null);
			_alreadyFetchedSettlementRunValue = (_settlementRunValue != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "VarioSettlement":
					toReturn.Add(Relations.VarioSettlementEntityUsingVarioSettlementID);
					break;
				case "SettlementRunValue":
					toReturn.Add(Relations.SettlementRunValueEntityUsingSettlementRunValueID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_varioSettlement", (!this.MarkedForDeletion?_varioSettlement:null));
			info.AddValue("_varioSettlementReturnsNewIfNotFound", _varioSettlementReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVarioSettlement", _alwaysFetchVarioSettlement);
			info.AddValue("_alreadyFetchedVarioSettlement", _alreadyFetchedVarioSettlement);
			info.AddValue("_settlementRunValue", (!this.MarkedForDeletion?_settlementRunValue:null));
			info.AddValue("_settlementRunValueReturnsNewIfNotFound", _settlementRunValueReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSettlementRunValue", _alwaysFetchSettlementRunValue);
			info.AddValue("_alreadyFetchedSettlementRunValue", _alreadyFetchedSettlementRunValue);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "VarioSettlement":
					_alreadyFetchedVarioSettlement = true;
					this.VarioSettlement = (VarioSettlementEntity)entity;
					break;
				case "SettlementRunValue":
					_alreadyFetchedSettlementRunValue = true;
					this.SettlementRunValue = (SettlementRunValueEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "VarioSettlement":
					SetupSyncVarioSettlement(relatedEntity);
					break;
				case "SettlementRunValue":
					SetupSyncSettlementRunValue(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "VarioSettlement":
					DesetupSyncVarioSettlement(false, true);
					break;
				case "SettlementRunValue":
					DesetupSyncSettlementRunValue(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_varioSettlement!=null)
			{
				toReturn.Add(_varioSettlement);
			}
			if(_settlementRunValue!=null)
			{
				toReturn.Add(_settlementRunValue);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="varioSettlementID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementRunValueID, System.Int64 varioSettlementID)
		{
			return FetchUsingPK(settlementRunValueID, varioSettlementID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="varioSettlementID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementRunValueID, System.Int64 varioSettlementID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(settlementRunValueID, varioSettlementID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="varioSettlementID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementRunValueID, System.Int64 varioSettlementID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(settlementRunValueID, varioSettlementID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="varioSettlementID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementRunValueID, System.Int64 varioSettlementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(settlementRunValueID, varioSettlementID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SettlementRunValueID, this.VarioSettlementID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SettlementRunValueToVarioSettlementRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'VarioSettlementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VarioSettlementEntity' which is related to this entity.</returns>
		public VarioSettlementEntity GetSingleVarioSettlement()
		{
			return GetSingleVarioSettlement(false);
		}

		/// <summary> Retrieves the related entity of type 'VarioSettlementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VarioSettlementEntity' which is related to this entity.</returns>
		public virtual VarioSettlementEntity GetSingleVarioSettlement(bool forceFetch)
		{
			if( ( !_alreadyFetchedVarioSettlement || forceFetch || _alwaysFetchVarioSettlement) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VarioSettlementEntityUsingVarioSettlementID);
				VarioSettlementEntity newEntity = new VarioSettlementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.VarioSettlementID);
				}
				if(fetchResult)
				{
					newEntity = (VarioSettlementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_varioSettlementReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VarioSettlement = newEntity;
				_alreadyFetchedVarioSettlement = fetchResult;
			}
			return _varioSettlement;
		}


		/// <summary> Retrieves the related entity of type 'SettlementRunValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SettlementRunValueEntity' which is related to this entity.</returns>
		public SettlementRunValueEntity GetSingleSettlementRunValue()
		{
			return GetSingleSettlementRunValue(false);
		}

		/// <summary> Retrieves the related entity of type 'SettlementRunValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SettlementRunValueEntity' which is related to this entity.</returns>
		public virtual SettlementRunValueEntity GetSingleSettlementRunValue(bool forceFetch)
		{
			if( ( !_alreadyFetchedSettlementRunValue || forceFetch || _alwaysFetchSettlementRunValue) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SettlementRunValueEntityUsingSettlementRunValueID);
				SettlementRunValueEntity newEntity = new SettlementRunValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SettlementRunValueID);
				}
				if(fetchResult)
				{
					newEntity = (SettlementRunValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_settlementRunValueReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SettlementRunValue = newEntity;
				_alreadyFetchedSettlementRunValue = fetchResult;
			}
			return _settlementRunValue;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("VarioSettlement", _varioSettlement);
			toReturn.Add("SettlementRunValue", _settlementRunValue);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="varioSettlementID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="validator">The validator object for this SettlementRunValueToVarioSettlementEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 settlementRunValueID, System.Int64 varioSettlementID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(settlementRunValueID, varioSettlementID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_varioSettlementReturnsNewIfNotFound = false;
			_settlementRunValueReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementRunValueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VarioSettlementID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _varioSettlement</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVarioSettlement(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _varioSettlement, new PropertyChangedEventHandler( OnVarioSettlementPropertyChanged ), "VarioSettlement", VarioSL.Entities.RelationClasses.StaticSettlementRunValueToVarioSettlementRelations.VarioSettlementEntityUsingVarioSettlementIDStatic, true, signalRelatedEntity, "SettlementRunValueToVarioSettlements", resetFKFields, new int[] { (int)SettlementRunValueToVarioSettlementFieldIndex.VarioSettlementID } );		
			_varioSettlement = null;
		}
		
		/// <summary> setups the sync logic for member _varioSettlement</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVarioSettlement(IEntityCore relatedEntity)
		{
			if(_varioSettlement!=relatedEntity)
			{		
				DesetupSyncVarioSettlement(true, true);
				_varioSettlement = (VarioSettlementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _varioSettlement, new PropertyChangedEventHandler( OnVarioSettlementPropertyChanged ), "VarioSettlement", VarioSL.Entities.RelationClasses.StaticSettlementRunValueToVarioSettlementRelations.VarioSettlementEntityUsingVarioSettlementIDStatic, true, ref _alreadyFetchedVarioSettlement, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVarioSettlementPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _settlementRunValue</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSettlementRunValue(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _settlementRunValue, new PropertyChangedEventHandler( OnSettlementRunValuePropertyChanged ), "SettlementRunValue", VarioSL.Entities.RelationClasses.StaticSettlementRunValueToVarioSettlementRelations.SettlementRunValueEntityUsingSettlementRunValueIDStatic, true, signalRelatedEntity, "SettlementRunValueToVarioSettlements", resetFKFields, new int[] { (int)SettlementRunValueToVarioSettlementFieldIndex.SettlementRunValueID } );		
			_settlementRunValue = null;
		}
		
		/// <summary> setups the sync logic for member _settlementRunValue</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSettlementRunValue(IEntityCore relatedEntity)
		{
			if(_settlementRunValue!=relatedEntity)
			{		
				DesetupSyncSettlementRunValue(true, true);
				_settlementRunValue = (SettlementRunValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _settlementRunValue, new PropertyChangedEventHandler( OnSettlementRunValuePropertyChanged ), "SettlementRunValue", VarioSL.Entities.RelationClasses.StaticSettlementRunValueToVarioSettlementRelations.SettlementRunValueEntityUsingSettlementRunValueIDStatic, true, ref _alreadyFetchedSettlementRunValue, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSettlementRunValuePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="settlementRunValueID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="varioSettlementID">PK value for SettlementRunValueToVarioSettlement which data should be fetched into this SettlementRunValueToVarioSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 settlementRunValueID, System.Int64 varioSettlementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SettlementRunValueToVarioSettlementFieldIndex.SettlementRunValueID].ForcedCurrentValueWrite(settlementRunValueID);
				this.Fields[(int)SettlementRunValueToVarioSettlementFieldIndex.VarioSettlementID].ForcedCurrentValueWrite(varioSettlementID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSettlementRunValueToVarioSettlementDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SettlementRunValueToVarioSettlementEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SettlementRunValueToVarioSettlementRelations Relations
		{
			get	{ return new SettlementRunValueToVarioSettlementRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VarioSettlement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVarioSettlement
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VarioSettlementCollection(), (IEntityRelation)GetRelationsForField("VarioSettlement")[0], (int)VarioSL.Entities.EntityType.SettlementRunValueToVarioSettlementEntity, (int)VarioSL.Entities.EntityType.VarioSettlementEntity, 0, null, null, null, "VarioSettlement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementRunValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementRunValue
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementRunValueCollection(), (IEntityRelation)GetRelationsForField("SettlementRunValue")[0], (int)VarioSL.Entities.EntityType.SettlementRunValueToVarioSettlementEntity, (int)VarioSL.Entities.EntityType.SettlementRunValueEntity, 0, null, null, null, "SettlementRunValue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SettlementRunValueID property of the Entity SettlementRunValueToVarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUNVALUETOVARIO"."SETTLEMENTRUNVALUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 SettlementRunValueID
		{
			get { return (System.Int64)GetValue((int)SettlementRunValueToVarioSettlementFieldIndex.SettlementRunValueID, true); }
			set	{ SetValue((int)SettlementRunValueToVarioSettlementFieldIndex.SettlementRunValueID, value, true); }
		}

		/// <summary> The VarioSettlementID property of the Entity SettlementRunValueToVarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUNVALUETOVARIO"."VARIOSETTLEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 VarioSettlementID
		{
			get { return (System.Int64)GetValue((int)SettlementRunValueToVarioSettlementFieldIndex.VarioSettlementID, true); }
			set	{ SetValue((int)SettlementRunValueToVarioSettlementFieldIndex.VarioSettlementID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'VarioSettlementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVarioSettlement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VarioSettlementEntity VarioSettlement
		{
			get	{ return GetSingleVarioSettlement(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVarioSettlement(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SettlementRunValueToVarioSettlements", "VarioSettlement", _varioSettlement, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VarioSettlement. When set to true, VarioSettlement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VarioSettlement is accessed. You can always execute a forced fetch by calling GetSingleVarioSettlement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVarioSettlement
		{
			get	{ return _alwaysFetchVarioSettlement; }
			set	{ _alwaysFetchVarioSettlement = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VarioSettlement already has been fetched. Setting this property to false when VarioSettlement has been fetched
		/// will set VarioSettlement to null as well. Setting this property to true while VarioSettlement hasn't been fetched disables lazy loading for VarioSettlement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVarioSettlement
		{
			get { return _alreadyFetchedVarioSettlement;}
			set 
			{
				if(_alreadyFetchedVarioSettlement && !value)
				{
					this.VarioSettlement = null;
				}
				_alreadyFetchedVarioSettlement = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VarioSettlement is not found
		/// in the database. When set to true, VarioSettlement will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool VarioSettlementReturnsNewIfNotFound
		{
			get	{ return _varioSettlementReturnsNewIfNotFound; }
			set { _varioSettlementReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SettlementRunValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSettlementRunValue()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SettlementRunValueEntity SettlementRunValue
		{
			get	{ return GetSingleSettlementRunValue(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSettlementRunValue(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SettlementRunValueToVarioSettlements", "SettlementRunValue", _settlementRunValue, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementRunValue. When set to true, SettlementRunValue is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementRunValue is accessed. You can always execute a forced fetch by calling GetSingleSettlementRunValue(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementRunValue
		{
			get	{ return _alwaysFetchSettlementRunValue; }
			set	{ _alwaysFetchSettlementRunValue = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementRunValue already has been fetched. Setting this property to false when SettlementRunValue has been fetched
		/// will set SettlementRunValue to null as well. Setting this property to true while SettlementRunValue hasn't been fetched disables lazy loading for SettlementRunValue</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementRunValue
		{
			get { return _alreadyFetchedSettlementRunValue;}
			set 
			{
				if(_alreadyFetchedSettlementRunValue && !value)
				{
					this.SettlementRunValue = null;
				}
				_alreadyFetchedSettlementRunValue = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SettlementRunValue is not found
		/// in the database. When set to true, SettlementRunValue will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SettlementRunValueReturnsNewIfNotFound
		{
			get	{ return _settlementRunValueReturnsNewIfNotFound; }
			set { _settlementRunValueReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SettlementRunValueToVarioSettlementEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
