﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'BankStatementVerification'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class BankStatementVerificationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private BankStatementEntity _bankStatement;
		private bool	_alwaysFetchBankStatement, _alreadyFetchedBankStatement, _bankStatementReturnsNewIfNotFound;
		private PaymentJournalEntity _paymentJournal;
		private bool	_alwaysFetchPaymentJournal, _alreadyFetchedPaymentJournal, _paymentJournalReturnsNewIfNotFound;
		private SaleEntity _sale;
		private bool	_alwaysFetchSale, _alreadyFetchedSale, _saleReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BankStatement</summary>
			public static readonly string BankStatement = "BankStatement";
			/// <summary>Member name PaymentJournal</summary>
			public static readonly string PaymentJournal = "PaymentJournal";
			/// <summary>Member name Sale</summary>
			public static readonly string Sale = "Sale";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static BankStatementVerificationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public BankStatementVerificationEntity() :base("BankStatementVerificationEntity")
		{
			InitClassEmpty(null);
		}
		


		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BankStatementVerificationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_bankStatement = (BankStatementEntity)info.GetValue("_bankStatement", typeof(BankStatementEntity));
			if(_bankStatement!=null)
			{
				_bankStatement.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_bankStatementReturnsNewIfNotFound = info.GetBoolean("_bankStatementReturnsNewIfNotFound");
			_alwaysFetchBankStatement = info.GetBoolean("_alwaysFetchBankStatement");
			_alreadyFetchedBankStatement = info.GetBoolean("_alreadyFetchedBankStatement");

			_paymentJournal = (PaymentJournalEntity)info.GetValue("_paymentJournal", typeof(PaymentJournalEntity));
			if(_paymentJournal!=null)
			{
				_paymentJournal.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentJournalReturnsNewIfNotFound = info.GetBoolean("_paymentJournalReturnsNewIfNotFound");
			_alwaysFetchPaymentJournal = info.GetBoolean("_alwaysFetchPaymentJournal");
			_alreadyFetchedPaymentJournal = info.GetBoolean("_alreadyFetchedPaymentJournal");

			_sale = (SaleEntity)info.GetValue("_sale", typeof(SaleEntity));
			if(_sale!=null)
			{
				_sale.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_saleReturnsNewIfNotFound = info.GetBoolean("_saleReturnsNewIfNotFound");
			_alwaysFetchSale = info.GetBoolean("_alwaysFetchSale");
			_alreadyFetchedSale = info.GetBoolean("_alreadyFetchedSale");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((BankStatementVerificationFieldIndex)fieldIndex)
			{
				case BankStatementVerificationFieldIndex.BankStatementID:
					DesetupSyncBankStatement(true, false);
					_alreadyFetchedBankStatement = false;
					break;
				case BankStatementVerificationFieldIndex.PaymentJournalID:
					DesetupSyncPaymentJournal(true, false);
					_alreadyFetchedPaymentJournal = false;
					break;
				case BankStatementVerificationFieldIndex.SaleID:
					DesetupSyncSale(true, false);
					_alreadyFetchedSale = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedBankStatement = (_bankStatement != null);
			_alreadyFetchedPaymentJournal = (_paymentJournal != null);
			_alreadyFetchedSale = (_sale != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BankStatement":
					toReturn.Add(Relations.BankStatementEntityUsingBankStatementID);
					break;
				case "PaymentJournal":
					toReturn.Add(Relations.PaymentJournalEntityUsingPaymentJournalID);
					break;
				case "Sale":
					toReturn.Add(Relations.SaleEntityUsingSaleID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			info.AddValue("_bankStatement", (!this.MarkedForDeletion?_bankStatement:null));
			info.AddValue("_bankStatementReturnsNewIfNotFound", _bankStatementReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBankStatement", _alwaysFetchBankStatement);
			info.AddValue("_alreadyFetchedBankStatement", _alreadyFetchedBankStatement);

			info.AddValue("_paymentJournal", (!this.MarkedForDeletion?_paymentJournal:null));
			info.AddValue("_paymentJournalReturnsNewIfNotFound", _paymentJournalReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentJournal", _alwaysFetchPaymentJournal);
			info.AddValue("_alreadyFetchedPaymentJournal", _alreadyFetchedPaymentJournal);

			info.AddValue("_sale", (!this.MarkedForDeletion?_sale:null));
			info.AddValue("_saleReturnsNewIfNotFound", _saleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSale", _alwaysFetchSale);
			info.AddValue("_alreadyFetchedSale", _alreadyFetchedSale);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BankStatement":
					_alreadyFetchedBankStatement = true;
					this.BankStatement = (BankStatementEntity)entity;
					break;
				case "PaymentJournal":
					_alreadyFetchedPaymentJournal = true;
					this.PaymentJournal = (PaymentJournalEntity)entity;
					break;
				case "Sale":
					_alreadyFetchedSale = true;
					this.Sale = (SaleEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BankStatement":
					SetupSyncBankStatement(relatedEntity);
					break;
				case "PaymentJournal":
					SetupSyncPaymentJournal(relatedEntity);
					break;
				case "Sale":
					SetupSyncSale(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BankStatement":
					DesetupSyncBankStatement(false, true);
					break;
				case "PaymentJournal":
					DesetupSyncPaymentJournal(false, true);
					break;
				case "Sale":
					DesetupSyncSale(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_bankStatement!=null)
			{
				toReturn.Add(_bankStatement);
			}
			if(_paymentJournal!=null)
			{
				toReturn.Add(_paymentJournal);
			}
			if(_sale!=null)
			{
				toReturn.Add(_sale);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="bankStatementID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCBankStatementID(Nullable<System.Int64> bankStatementID)
		{
			return FetchUsingUCBankStatementID( bankStatementID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="bankStatementID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCBankStatementID(Nullable<System.Int64> bankStatementID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCBankStatementID( bankStatementID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="bankStatementID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCBankStatementID(Nullable<System.Int64> bankStatementID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCBankStatementID( bankStatementID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="bankStatementID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCBankStatementID(Nullable<System.Int64> bankStatementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((BankStatementVerificationDAO)CreateDAOInstance()).FetchBankStatementVerificationUsingUCBankStatementID(this, this.Transaction, bankStatementID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="paymentJournalID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPaymentJournalID(Nullable<System.Int64> paymentJournalID)
		{
			return FetchUsingUCPaymentJournalID( paymentJournalID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="paymentJournalID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPaymentJournalID(Nullable<System.Int64> paymentJournalID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCPaymentJournalID( paymentJournalID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="paymentJournalID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPaymentJournalID(Nullable<System.Int64> paymentJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCPaymentJournalID( paymentJournalID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="paymentJournalID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPaymentJournalID(Nullable<System.Int64> paymentJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((BankStatementVerificationDAO)CreateDAOInstance()).FetchBankStatementVerificationUsingUCPaymentJournalID(this, this.Transaction, paymentJournalID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="saleID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCSaleID(Nullable<System.Int64> saleID)
		{
			return FetchUsingUCSaleID( saleID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="saleID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCSaleID(Nullable<System.Int64> saleID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCSaleID( saleID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="saleID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCSaleID(Nullable<System.Int64> saleID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCSaleID( saleID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="saleID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCSaleID(Nullable<System.Int64> saleID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((BankStatementVerificationDAO)CreateDAOInstance()).FetchBankStatementVerificationUsingUCSaleID(this, this.Transaction, saleID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}




				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new BankStatementVerificationRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'BankStatementEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'BankStatementEntity' which is related to this entity.</returns>
		public BankStatementEntity GetSingleBankStatement()
		{
			return GetSingleBankStatement(false);
		}
		
		/// <summary> Retrieves the related entity of type 'BankStatementEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BankStatementEntity' which is related to this entity.</returns>
		public virtual BankStatementEntity GetSingleBankStatement(bool forceFetch)
		{
			if( ( !_alreadyFetchedBankStatement || forceFetch || _alwaysFetchBankStatement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BankStatementEntityUsingBankStatementID);
				BankStatementEntity newEntity = new BankStatementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BankStatementID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BankStatementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_bankStatementReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BankStatement = newEntity;
				_alreadyFetchedBankStatement = fetchResult;
			}
			return _bankStatement;
		}

		/// <summary> Retrieves the related entity of type 'PaymentJournalEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PaymentJournalEntity' which is related to this entity.</returns>
		public PaymentJournalEntity GetSinglePaymentJournal()
		{
			return GetSinglePaymentJournal(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PaymentJournalEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentJournalEntity' which is related to this entity.</returns>
		public virtual PaymentJournalEntity GetSinglePaymentJournal(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentJournal || forceFetch || _alwaysFetchPaymentJournal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentJournalEntityUsingPaymentJournalID);
				PaymentJournalEntity newEntity = new PaymentJournalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentJournalID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentJournalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentJournalReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentJournal = newEntity;
				_alreadyFetchedPaymentJournal = fetchResult;
			}
			return _paymentJournal;
		}

		/// <summary> Retrieves the related entity of type 'SaleEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'SaleEntity' which is related to this entity.</returns>
		public SaleEntity GetSingleSale()
		{
			return GetSingleSale(false);
		}
		
		/// <summary> Retrieves the related entity of type 'SaleEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SaleEntity' which is related to this entity.</returns>
		public virtual SaleEntity GetSingleSale(bool forceFetch)
		{
			if( ( !_alreadyFetchedSale || forceFetch || _alwaysFetchSale) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SaleEntityUsingSaleID);
				SaleEntity newEntity = new SaleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SaleID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SaleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_saleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Sale = newEntity;
				_alreadyFetchedSale = fetchResult;
			}
			return _sale;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BankStatement", _bankStatement);
			toReturn.Add("PaymentJournal", _paymentJournal);
			toReturn.Add("Sale", _sale);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		



		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_bankStatementReturnsNewIfNotFound = false;
			_paymentJournalReturnsNewIfNotFound = false;
			_saleReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankStatementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankStatementState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaskedPan", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MatchedStatements", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReconciliationState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecordType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SaleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShortPan", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionDate", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _bankStatement</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBankStatement(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _bankStatement, new PropertyChangedEventHandler( OnBankStatementPropertyChanged ), "BankStatement", VarioSL.Entities.RelationClasses.StaticBankStatementVerificationRelations.BankStatementEntityUsingBankStatementIDStatic, true, signalRelatedEntity, "BankStatementVerification", resetFKFields, new int[] { (int)BankStatementVerificationFieldIndex.BankStatementID } );
			_bankStatement = null;
		}
	
		/// <summary> setups the sync logic for member _bankStatement</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBankStatement(IEntityCore relatedEntity)
		{
			if(_bankStatement!=relatedEntity)
			{
				DesetupSyncBankStatement(true, true);
				_bankStatement = (BankStatementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _bankStatement, new PropertyChangedEventHandler( OnBankStatementPropertyChanged ), "BankStatement", VarioSL.Entities.RelationClasses.StaticBankStatementVerificationRelations.BankStatementEntityUsingBankStatementIDStatic, true, ref _alreadyFetchedBankStatement, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBankStatementPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentJournal</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentJournal(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentJournal, new PropertyChangedEventHandler( OnPaymentJournalPropertyChanged ), "PaymentJournal", VarioSL.Entities.RelationClasses.StaticBankStatementVerificationRelations.PaymentJournalEntityUsingPaymentJournalIDStatic, true, signalRelatedEntity, "BankStatementVerification", resetFKFields, new int[] { (int)BankStatementVerificationFieldIndex.PaymentJournalID } );
			_paymentJournal = null;
		}
	
		/// <summary> setups the sync logic for member _paymentJournal</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentJournal(IEntityCore relatedEntity)
		{
			if(_paymentJournal!=relatedEntity)
			{
				DesetupSyncPaymentJournal(true, true);
				_paymentJournal = (PaymentJournalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentJournal, new PropertyChangedEventHandler( OnPaymentJournalPropertyChanged ), "PaymentJournal", VarioSL.Entities.RelationClasses.StaticBankStatementVerificationRelations.PaymentJournalEntityUsingPaymentJournalIDStatic, true, ref _alreadyFetchedPaymentJournal, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentJournalPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _sale</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSale(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _sale, new PropertyChangedEventHandler( OnSalePropertyChanged ), "Sale", VarioSL.Entities.RelationClasses.StaticBankStatementVerificationRelations.SaleEntityUsingSaleIDStatic, true, signalRelatedEntity, "BankStatementVerification", resetFKFields, new int[] { (int)BankStatementVerificationFieldIndex.SaleID } );
			_sale = null;
		}
	
		/// <summary> setups the sync logic for member _sale</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSale(IEntityCore relatedEntity)
		{
			if(_sale!=relatedEntity)
			{
				DesetupSyncSale(true, true);
				_sale = (SaleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _sale, new PropertyChangedEventHandler( OnSalePropertyChanged ), "Sale", VarioSL.Entities.RelationClasses.StaticBankStatementVerificationRelations.SaleEntityUsingSaleIDStatic, true, ref _alreadyFetchedSale, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSalePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. </summary>
		/// <returns>true</returns>
		public override bool Refetch()
		{
			return true;
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateBankStatementVerificationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new BankStatementVerificationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static BankStatementVerificationRelations Relations
		{
			get	{ return new BankStatementVerificationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BankStatement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBankStatement
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BankStatementCollection(), (IEntityRelation)GetRelationsForField("BankStatement")[0], (int)VarioSL.Entities.EntityType.BankStatementVerificationEntity, (int)VarioSL.Entities.EntityType.BankStatementEntity, 0, null, null, null, "BankStatement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentJournal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentJournal
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentJournalCollection(), (IEntityRelation)GetRelationsForField("PaymentJournal")[0], (int)VarioSL.Entities.EntityType.BankStatementVerificationEntity, (int)VarioSL.Entities.EntityType.PaymentJournalEntity, 0, null, null, null, "PaymentJournal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSale
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("Sale")[0], (int)VarioSL.Entities.EntityType.BankStatementVerificationEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "Sale", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."AMOUNT"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Amount
		{
			get { return (Nullable<System.Int64>)GetValue((int)BankStatementVerificationFieldIndex.Amount, false); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.Amount, value, true); }
		}

		/// <summary> The BankStatementID property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."BANKSTATEMENTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Nullable<System.Int64> BankStatementID
		{
			get { return (Nullable<System.Int64>)GetValue((int)BankStatementVerificationFieldIndex.BankStatementID, false); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.BankStatementID, value, true); }
		}

		/// <summary> The BankStatementState property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."BANKSTATEMENTSTATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.BankStatementState> BankStatementState
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.BankStatementState>)GetValue((int)BankStatementVerificationFieldIndex.BankStatementState, false); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.BankStatementState, value, true); }
		}

		/// <summary> The MaskedPan property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."MASKEDPAN"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MaskedPan
		{
			get { return (System.String)GetValue((int)BankStatementVerificationFieldIndex.MaskedPan, true); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.MaskedPan, value, true); }
		}

		/// <summary> The MatchedStatements property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."MATCHEDSTATEMENTS"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> MatchedStatements
		{
			get { return (Nullable<System.Decimal>)GetValue((int)BankStatementVerificationFieldIndex.MatchedStatements, false); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.MatchedStatements, value, true); }
		}

		/// <summary> The PaymentJournalID property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."PAYMENTJOURNALID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentJournalID
		{
			get { return (Nullable<System.Int64>)GetValue((int)BankStatementVerificationFieldIndex.PaymentJournalID, false); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.PaymentJournalID, value, true); }
		}

		/// <summary> The PaymentReference property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."PAYMENTREFERENCE"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 300<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentReference
		{
			get { return (System.String)GetValue((int)BankStatementVerificationFieldIndex.PaymentReference, true); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.PaymentReference, value, true); }
		}

		/// <summary> The PaymentState property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."PAYMENTSTATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.PaymentState> PaymentState
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.PaymentState>)GetValue((int)BankStatementVerificationFieldIndex.PaymentState, false); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.PaymentState, value, true); }
		}

		/// <summary> The ReconciliationState property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."RECONCILIATIONSTATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.PaymentReconciliationState> ReconciliationState
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.PaymentReconciliationState>)GetValue((int)BankStatementVerificationFieldIndex.ReconciliationState, false); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.ReconciliationState, value, true); }
		}

		/// <summary> The RecordType property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."RECORDTYPE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RecordType
		{
			get { return (Nullable<System.Int32>)GetValue((int)BankStatementVerificationFieldIndex.RecordType, false); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.RecordType, value, true); }
		}

		/// <summary> The SaleID property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."SALEID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SaleID
		{
			get { return (Nullable<System.Int64>)GetValue((int)BankStatementVerificationFieldIndex.SaleID, false); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.SaleID, value, true); }
		}

		/// <summary> The ShortPan property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."SHORTPAN"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 4<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShortPan
		{
			get { return (System.String)GetValue((int)BankStatementVerificationFieldIndex.ShortPan, true); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.ShortPan, value, true); }
		}

		/// <summary> The TransactionDate property of the Entity BankStatementVerification<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_BANKSTATEMENTVERIFICATION"."TRANSACTIONDATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> TransactionDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BankStatementVerificationFieldIndex.TransactionDate, false); }
			set	{ SetValue((int)BankStatementVerificationFieldIndex.TransactionDate, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'BankStatementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBankStatement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BankStatementEntity BankStatement
		{
			get	{ return GetSingleBankStatement(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncBankStatement(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_bankStatement !=null);
						DesetupSyncBankStatement(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("BankStatement");
						}
					}
					else
					{
						if(_bankStatement!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "BankStatementVerification");
							SetupSyncBankStatement(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BankStatement. When set to true, BankStatement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BankStatement is accessed. You can always execute a forced fetch by calling GetSingleBankStatement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBankStatement
		{
			get	{ return _alwaysFetchBankStatement; }
			set	{ _alwaysFetchBankStatement = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property BankStatement already has been fetched. Setting this property to false when BankStatement has been fetched
		/// will set BankStatement to null as well. Setting this property to true while BankStatement hasn't been fetched disables lazy loading for BankStatement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBankStatement
		{
			get { return _alreadyFetchedBankStatement;}
			set 
			{
				if(_alreadyFetchedBankStatement && !value)
				{
					this.BankStatement = null;
				}
				_alreadyFetchedBankStatement = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BankStatement is not found
		/// in the database. When set to true, BankStatement will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BankStatementReturnsNewIfNotFound
		{
			get	{ return _bankStatementReturnsNewIfNotFound; }
			set	{ _bankStatementReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'PaymentJournalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentJournal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentJournalEntity PaymentJournal
		{
			get	{ return GetSinglePaymentJournal(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPaymentJournal(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_paymentJournal !=null);
						DesetupSyncPaymentJournal(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("PaymentJournal");
						}
					}
					else
					{
						if(_paymentJournal!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "BankStatementVerification");
							SetupSyncPaymentJournal(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentJournal. When set to true, PaymentJournal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentJournal is accessed. You can always execute a forced fetch by calling GetSinglePaymentJournal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentJournal
		{
			get	{ return _alwaysFetchPaymentJournal; }
			set	{ _alwaysFetchPaymentJournal = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentJournal already has been fetched. Setting this property to false when PaymentJournal has been fetched
		/// will set PaymentJournal to null as well. Setting this property to true while PaymentJournal hasn't been fetched disables lazy loading for PaymentJournal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentJournal
		{
			get { return _alreadyFetchedPaymentJournal;}
			set 
			{
				if(_alreadyFetchedPaymentJournal && !value)
				{
					this.PaymentJournal = null;
				}
				_alreadyFetchedPaymentJournal = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentJournal is not found
		/// in the database. When set to true, PaymentJournal will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentJournalReturnsNewIfNotFound
		{
			get	{ return _paymentJournalReturnsNewIfNotFound; }
			set	{ _paymentJournalReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'SaleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSale()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SaleEntity Sale
		{
			get	{ return GetSingleSale(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncSale(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_sale !=null);
						DesetupSyncSale(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("Sale");
						}
					}
					else
					{
						if(_sale!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "BankStatementVerification");
							SetupSyncSale(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Sale. When set to true, Sale is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Sale is accessed. You can always execute a forced fetch by calling GetSingleSale(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSale
		{
			get	{ return _alwaysFetchSale; }
			set	{ _alwaysFetchSale = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property Sale already has been fetched. Setting this property to false when Sale has been fetched
		/// will set Sale to null as well. Setting this property to true while Sale hasn't been fetched disables lazy loading for Sale</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSale
		{
			get { return _alreadyFetchedSale;}
			set 
			{
				if(_alreadyFetchedSale && !value)
				{
					this.Sale = null;
				}
				_alreadyFetchedSale = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Sale is not found
		/// in the database. When set to true, Sale will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SaleReturnsNewIfNotFound
		{
			get	{ return _saleReturnsNewIfNotFound; }
			set	{ _saleReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.BankStatementVerificationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
