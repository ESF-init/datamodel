﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ProductTermination'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ProductTerminationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private UserListEntity _userList;
		private bool	_alwaysFetchUserList, _alreadyFetchedUserList, _userListReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private InvoiceEntity _invoice;
		private bool	_alwaysFetchInvoice, _alreadyFetchedInvoice, _invoiceReturnsNewIfNotFound;
		private ProductEntity _product;
		private bool	_alwaysFetchProduct, _alreadyFetchedProduct, _productReturnsNewIfNotFound;
		private ProductTerminationTypeEntity _productTerminationType;
		private bool	_alwaysFetchProductTerminationType, _alreadyFetchedProductTerminationType, _productTerminationTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserList</summary>
			public static readonly string UserList = "UserList";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name Invoice</summary>
			public static readonly string Invoice = "Invoice";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name ProductTerminationType</summary>
			public static readonly string ProductTerminationType = "ProductTerminationType";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ProductTerminationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ProductTerminationEntity() :base("ProductTerminationEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="productTerminationID">PK value for ProductTermination which data should be fetched into this ProductTermination object</param>
		public ProductTerminationEntity(System.Int64 productTerminationID):base("ProductTerminationEntity")
		{
			InitClassFetch(productTerminationID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="productTerminationID">PK value for ProductTermination which data should be fetched into this ProductTermination object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProductTerminationEntity(System.Int64 productTerminationID, IPrefetchPath prefetchPathToUse):base("ProductTerminationEntity")
		{
			InitClassFetch(productTerminationID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="productTerminationID">PK value for ProductTermination which data should be fetched into this ProductTermination object</param>
		/// <param name="validator">The custom validator object for this ProductTerminationEntity</param>
		public ProductTerminationEntity(System.Int64 productTerminationID, IValidator validator):base("ProductTerminationEntity")
		{
			InitClassFetch(productTerminationID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductTerminationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_userList = (UserListEntity)info.GetValue("_userList", typeof(UserListEntity));
			if(_userList!=null)
			{
				_userList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userListReturnsNewIfNotFound = info.GetBoolean("_userListReturnsNewIfNotFound");
			_alwaysFetchUserList = info.GetBoolean("_alwaysFetchUserList");
			_alreadyFetchedUserList = info.GetBoolean("_alreadyFetchedUserList");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_invoice = (InvoiceEntity)info.GetValue("_invoice", typeof(InvoiceEntity));
			if(_invoice!=null)
			{
				_invoice.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_invoiceReturnsNewIfNotFound = info.GetBoolean("_invoiceReturnsNewIfNotFound");
			_alwaysFetchInvoice = info.GetBoolean("_alwaysFetchInvoice");
			_alreadyFetchedInvoice = info.GetBoolean("_alreadyFetchedInvoice");

			_product = (ProductEntity)info.GetValue("_product", typeof(ProductEntity));
			if(_product!=null)
			{
				_product.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productReturnsNewIfNotFound = info.GetBoolean("_productReturnsNewIfNotFound");
			_alwaysFetchProduct = info.GetBoolean("_alwaysFetchProduct");
			_alreadyFetchedProduct = info.GetBoolean("_alreadyFetchedProduct");

			_productTerminationType = (ProductTerminationTypeEntity)info.GetValue("_productTerminationType", typeof(ProductTerminationTypeEntity));
			if(_productTerminationType!=null)
			{
				_productTerminationType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productTerminationTypeReturnsNewIfNotFound = info.GetBoolean("_productTerminationTypeReturnsNewIfNotFound");
			_alwaysFetchProductTerminationType = info.GetBoolean("_alwaysFetchProductTerminationType");
			_alreadyFetchedProductTerminationType = info.GetBoolean("_alreadyFetchedProductTerminationType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ProductTerminationFieldIndex)fieldIndex)
			{
				case ProductTerminationFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case ProductTerminationFieldIndex.EmployeeID:
					DesetupSyncUserList(true, false);
					_alreadyFetchedUserList = false;
					break;
				case ProductTerminationFieldIndex.InvoiceID:
					DesetupSyncInvoice(true, false);
					_alreadyFetchedInvoice = false;
					break;
				case ProductTerminationFieldIndex.ProductID:
					DesetupSyncProduct(true, false);
					_alreadyFetchedProduct = false;
					break;
				case ProductTerminationFieldIndex.ProductTerminationTypeID:
					DesetupSyncProductTerminationType(true, false);
					_alreadyFetchedProductTerminationType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedUserList = (_userList != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedInvoice = (_invoice != null);
			_alreadyFetchedProduct = (_product != null);
			_alreadyFetchedProductTerminationType = (_productTerminationType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "UserList":
					toReturn.Add(Relations.UserListEntityUsingEmployeeID);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "Invoice":
					toReturn.Add(Relations.InvoiceEntityUsingInvoiceID);
					break;
				case "Product":
					toReturn.Add(Relations.ProductEntityUsingProductID);
					break;
				case "ProductTerminationType":
					toReturn.Add(Relations.ProductTerminationTypeEntityUsingProductTerminationTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_userList", (!this.MarkedForDeletion?_userList:null));
			info.AddValue("_userListReturnsNewIfNotFound", _userListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserList", _alwaysFetchUserList);
			info.AddValue("_alreadyFetchedUserList", _alreadyFetchedUserList);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_invoice", (!this.MarkedForDeletion?_invoice:null));
			info.AddValue("_invoiceReturnsNewIfNotFound", _invoiceReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInvoice", _alwaysFetchInvoice);
			info.AddValue("_alreadyFetchedInvoice", _alreadyFetchedInvoice);
			info.AddValue("_product", (!this.MarkedForDeletion?_product:null));
			info.AddValue("_productReturnsNewIfNotFound", _productReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProduct", _alwaysFetchProduct);
			info.AddValue("_alreadyFetchedProduct", _alreadyFetchedProduct);
			info.AddValue("_productTerminationType", (!this.MarkedForDeletion?_productTerminationType:null));
			info.AddValue("_productTerminationTypeReturnsNewIfNotFound", _productTerminationTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductTerminationType", _alwaysFetchProductTerminationType);
			info.AddValue("_alreadyFetchedProductTerminationType", _alreadyFetchedProductTerminationType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "UserList":
					_alreadyFetchedUserList = true;
					this.UserList = (UserListEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "Invoice":
					_alreadyFetchedInvoice = true;
					this.Invoice = (InvoiceEntity)entity;
					break;
				case "Product":
					_alreadyFetchedProduct = true;
					this.Product = (ProductEntity)entity;
					break;
				case "ProductTerminationType":
					_alreadyFetchedProductTerminationType = true;
					this.ProductTerminationType = (ProductTerminationTypeEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "UserList":
					SetupSyncUserList(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "Invoice":
					SetupSyncInvoice(relatedEntity);
					break;
				case "Product":
					SetupSyncProduct(relatedEntity);
					break;
				case "ProductTerminationType":
					SetupSyncProductTerminationType(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "UserList":
					DesetupSyncUserList(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "Invoice":
					DesetupSyncInvoice(false, true);
					break;
				case "Product":
					DesetupSyncProduct(false, true);
					break;
				case "ProductTerminationType":
					DesetupSyncProductTerminationType(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_userList!=null)
			{
				toReturn.Add(_userList);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_invoice!=null)
			{
				toReturn.Add(_invoice);
			}
			if(_product!=null)
			{
				toReturn.Add(_product);
			}
			if(_productTerminationType!=null)
			{
				toReturn.Add(_productTerminationType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productTerminationID">PK value for ProductTermination which data should be fetched into this ProductTermination object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 productTerminationID)
		{
			return FetchUsingPK(productTerminationID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productTerminationID">PK value for ProductTermination which data should be fetched into this ProductTermination object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 productTerminationID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(productTerminationID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productTerminationID">PK value for ProductTermination which data should be fetched into this ProductTermination object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 productTerminationID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(productTerminationID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productTerminationID">PK value for ProductTermination which data should be fetched into this ProductTermination object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 productTerminationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(productTerminationID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ProductTerminationID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ProductTerminationRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public UserListEntity GetSingleUserList()
		{
			return GetSingleUserList(false);
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public virtual UserListEntity GetSingleUserList(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserList || forceFetch || _alwaysFetchUserList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserListEntityUsingEmployeeID);
				UserListEntity newEntity = new UserListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EmployeeID);
				}
				if(fetchResult)
				{
					newEntity = (UserListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserList = newEntity;
				_alreadyFetchedUserList = fetchResult;
			}
			return _userList;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID);
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'InvoiceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'InvoiceEntity' which is related to this entity.</returns>
		public InvoiceEntity GetSingleInvoice()
		{
			return GetSingleInvoice(false);
		}

		/// <summary> Retrieves the related entity of type 'InvoiceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InvoiceEntity' which is related to this entity.</returns>
		public virtual InvoiceEntity GetSingleInvoice(bool forceFetch)
		{
			if( ( !_alreadyFetchedInvoice || forceFetch || _alwaysFetchInvoice) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InvoiceEntityUsingInvoiceID);
				InvoiceEntity newEntity = new InvoiceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InvoiceID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (InvoiceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_invoiceReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Invoice = newEntity;
				_alreadyFetchedInvoice = fetchResult;
			}
			return _invoice;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProduct()
		{
			return GetSingleProduct(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProduct(bool forceFetch)
		{
			if( ( !_alreadyFetchedProduct || forceFetch || _alwaysFetchProduct) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductID);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductID);
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Product = newEntity;
				_alreadyFetchedProduct = fetchResult;
			}
			return _product;
		}


		/// <summary> Retrieves the related entity of type 'ProductTerminationTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductTerminationTypeEntity' which is related to this entity.</returns>
		public ProductTerminationTypeEntity GetSingleProductTerminationType()
		{
			return GetSingleProductTerminationType(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductTerminationTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductTerminationTypeEntity' which is related to this entity.</returns>
		public virtual ProductTerminationTypeEntity GetSingleProductTerminationType(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductTerminationType || forceFetch || _alwaysFetchProductTerminationType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductTerminationTypeEntityUsingProductTerminationTypeID);
				ProductTerminationTypeEntity newEntity = new ProductTerminationTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductTerminationTypeID);
				}
				if(fetchResult)
				{
					newEntity = (ProductTerminationTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productTerminationTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductTerminationType = newEntity;
				_alreadyFetchedProductTerminationType = fetchResult;
			}
			return _productTerminationType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("UserList", _userList);
			toReturn.Add("Contract", _contract);
			toReturn.Add("Invoice", _invoice);
			toReturn.Add("Product", _product);
			toReturn.Add("ProductTerminationType", _productTerminationType);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="productTerminationID">PK value for ProductTermination which data should be fetched into this ProductTermination object</param>
		/// <param name="validator">The validator object for this ProductTerminationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 productTerminationID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(productTerminationID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_userListReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_invoiceReturnsNewIfNotFound = false;
			_productReturnsNewIfNotFound = false;
			_productTerminationTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankingCost", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CapReached", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DemandDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DifferenceCost", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmployeeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MonthinUse", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProcessingCost", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductTerminationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductTerminationTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReferencedTicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminationStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TotalCost", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _userList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticProductTerminationRelations.UserListEntityUsingEmployeeIDStatic, true, signalRelatedEntity, "ProductTerminations", resetFKFields, new int[] { (int)ProductTerminationFieldIndex.EmployeeID } );		
			_userList = null;
		}
		
		/// <summary> setups the sync logic for member _userList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserList(IEntityCore relatedEntity)
		{
			if(_userList!=relatedEntity)
			{		
				DesetupSyncUserList(true, true);
				_userList = (UserListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticProductTerminationRelations.UserListEntityUsingEmployeeIDStatic, true, ref _alreadyFetchedUserList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticProductTerminationRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "ProductTerminations", resetFKFields, new int[] { (int)ProductTerminationFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticProductTerminationRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _invoice</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInvoice(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _invoice, new PropertyChangedEventHandler( OnInvoicePropertyChanged ), "Invoice", VarioSL.Entities.RelationClasses.StaticProductTerminationRelations.InvoiceEntityUsingInvoiceIDStatic, true, signalRelatedEntity, "ProductTerminations", resetFKFields, new int[] { (int)ProductTerminationFieldIndex.InvoiceID } );		
			_invoice = null;
		}
		
		/// <summary> setups the sync logic for member _invoice</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInvoice(IEntityCore relatedEntity)
		{
			if(_invoice!=relatedEntity)
			{		
				DesetupSyncInvoice(true, true);
				_invoice = (InvoiceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _invoice, new PropertyChangedEventHandler( OnInvoicePropertyChanged ), "Invoice", VarioSL.Entities.RelationClasses.StaticProductTerminationRelations.InvoiceEntityUsingInvoiceIDStatic, true, ref _alreadyFetchedInvoice, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInvoicePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _product</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProduct(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticProductTerminationRelations.ProductEntityUsingProductIDStatic, true, signalRelatedEntity, "ProductTerminations", resetFKFields, new int[] { (int)ProductTerminationFieldIndex.ProductID } );		
			_product = null;
		}
		
		/// <summary> setups the sync logic for member _product</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProduct(IEntityCore relatedEntity)
		{
			if(_product!=relatedEntity)
			{		
				DesetupSyncProduct(true, true);
				_product = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticProductTerminationRelations.ProductEntityUsingProductIDStatic, true, ref _alreadyFetchedProduct, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productTerminationType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductTerminationType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productTerminationType, new PropertyChangedEventHandler( OnProductTerminationTypePropertyChanged ), "ProductTerminationType", VarioSL.Entities.RelationClasses.StaticProductTerminationRelations.ProductTerminationTypeEntityUsingProductTerminationTypeIDStatic, true, signalRelatedEntity, "ProductTerminations", resetFKFields, new int[] { (int)ProductTerminationFieldIndex.ProductTerminationTypeID } );		
			_productTerminationType = null;
		}
		
		/// <summary> setups the sync logic for member _productTerminationType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductTerminationType(IEntityCore relatedEntity)
		{
			if(_productTerminationType!=relatedEntity)
			{		
				DesetupSyncProductTerminationType(true, true);
				_productTerminationType = (ProductTerminationTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productTerminationType, new PropertyChangedEventHandler( OnProductTerminationTypePropertyChanged ), "ProductTerminationType", VarioSL.Entities.RelationClasses.StaticProductTerminationRelations.ProductTerminationTypeEntityUsingProductTerminationTypeIDStatic, true, ref _alreadyFetchedProductTerminationType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductTerminationTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="productTerminationID">PK value for ProductTermination which data should be fetched into this ProductTermination object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 productTerminationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ProductTerminationFieldIndex.ProductTerminationID].ForcedCurrentValueWrite(productTerminationID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateProductTerminationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ProductTerminationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ProductTerminationRelations Relations
		{
			get	{ return new ProductTerminationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("UserList")[0], (int)VarioSL.Entities.EntityType.ProductTerminationEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "UserList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.ProductTerminationEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoice'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoice
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoiceCollection(), (IEntityRelation)GetRelationsForField("Invoice")[0], (int)VarioSL.Entities.EntityType.ProductTerminationEntity, (int)VarioSL.Entities.EntityType.InvoiceEntity, 0, null, null, null, "Invoice", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProduct
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Product")[0], (int)VarioSL.Entities.EntityType.ProductTerminationEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Product", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductTerminationType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductTerminationType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductTerminationTypeCollection(), (IEntityRelation)GetRelationsForField("ProductTerminationType")[0], (int)VarioSL.Entities.EntityType.ProductTerminationEntity, (int)VarioSL.Entities.EntityType.ProductTerminationTypeEntity, 0, null, null, null, "ProductTerminationType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BankingCost property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."BANKINGCOST"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BankingCost
		{
			get { return (System.Int32)GetValue((int)ProductTerminationFieldIndex.BankingCost, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.BankingCost, value, true); }
		}

		/// <summary> The CapReached property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."CAPREACHED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> CapReached
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ProductTerminationFieldIndex.CapReached, false); }
			set	{ SetValue((int)ProductTerminationFieldIndex.CapReached, value, true); }
		}

		/// <summary> The ContractID property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ContractID
		{
			get { return (System.Int64)GetValue((int)ProductTerminationFieldIndex.ContractID, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.ContractID, value, true); }
		}

		/// <summary> The DemandDate property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."DEMANDDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DemandDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductTerminationFieldIndex.DemandDate, false); }
			set	{ SetValue((int)ProductTerminationFieldIndex.DemandDate, value, true); }
		}

		/// <summary> The Description property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ProductTerminationFieldIndex.Description, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.Description, value, true); }
		}

		/// <summary> The DifferenceCost property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."DIFFERENCECOST"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DifferenceCost
		{
			get { return (System.Int32)GetValue((int)ProductTerminationFieldIndex.DifferenceCost, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.DifferenceCost, value, true); }
		}

		/// <summary> The EmployeeID property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."EMPLOYEEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 EmployeeID
		{
			get { return (System.Int64)GetValue((int)ProductTerminationFieldIndex.EmployeeID, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.EmployeeID, value, true); }
		}

		/// <summary> The InvoiceID property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."INVOICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> InvoiceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductTerminationFieldIndex.InvoiceID, false); }
			set	{ SetValue((int)ProductTerminationFieldIndex.InvoiceID, value, true); }
		}

		/// <summary> The LastModified property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ProductTerminationFieldIndex.LastModified, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ProductTerminationFieldIndex.LastUser, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MonthinUse property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."MONTHINUSE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> MonthinUse
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ProductTerminationFieldIndex.MonthinUse, false); }
			set	{ SetValue((int)ProductTerminationFieldIndex.MonthinUse, value, true); }
		}

		/// <summary> The ProcessingCost property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."PROCESSINGCOST"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> ProcessingCost
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ProductTerminationFieldIndex.ProcessingCost, false); }
			set	{ SetValue((int)ProductTerminationFieldIndex.ProcessingCost, value, true); }
		}

		/// <summary> The ProductID property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."PRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ProductID
		{
			get { return (System.Int64)GetValue((int)ProductTerminationFieldIndex.ProductID, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.ProductID, value, true); }
		}

		/// <summary> The ProductTerminationID property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."PRODUCTTERMINATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ProductTerminationID
		{
			get { return (System.Int64)GetValue((int)ProductTerminationFieldIndex.ProductTerminationID, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.ProductTerminationID, value, true); }
		}

		/// <summary> The ProductTerminationTypeID property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."PRODUCTTERMINATIONTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ProductTerminationTypeID
		{
			get { return (System.Int64)GetValue((int)ProductTerminationFieldIndex.ProductTerminationTypeID, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.ProductTerminationTypeID, value, true); }
		}

		/// <summary> The ReferencedTicketID property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."REFERENCEDTICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReferencedTicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductTerminationFieldIndex.ReferencedTicketID, false); }
			set	{ SetValue((int)ProductTerminationFieldIndex.ReferencedTicketID, value, true); }
		}

		/// <summary> The TerminationDate property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."TERMINATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TerminationDate
		{
			get { return (System.DateTime)GetValue((int)ProductTerminationFieldIndex.TerminationDate, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.TerminationDate, value, true); }
		}

		/// <summary> The TerminationStatus property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."TERMINATIONSTATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TerminationStatus
		{
			get { return (System.Int32)GetValue((int)ProductTerminationFieldIndex.TerminationStatus, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.TerminationStatus, value, true); }
		}

		/// <summary> The TotalCost property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."TOTALCOST"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TotalCost
		{
			get { return (System.Int32)GetValue((int)ProductTerminationFieldIndex.TotalCost, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.TotalCost, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ProductTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCTTERMINATION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ProductTerminationFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ProductTerminationFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'UserListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserListEntity UserList
		{
			get	{ return GetSingleUserList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductTerminations", "UserList", _userList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserList. When set to true, UserList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserList is accessed. You can always execute a forced fetch by calling GetSingleUserList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserList
		{
			get	{ return _alwaysFetchUserList; }
			set	{ _alwaysFetchUserList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserList already has been fetched. Setting this property to false when UserList has been fetched
		/// will set UserList to null as well. Setting this property to true while UserList hasn't been fetched disables lazy loading for UserList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserList
		{
			get { return _alreadyFetchedUserList;}
			set 
			{
				if(_alreadyFetchedUserList && !value)
				{
					this.UserList = null;
				}
				_alreadyFetchedUserList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserList is not found
		/// in the database. When set to true, UserList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserListReturnsNewIfNotFound
		{
			get	{ return _userListReturnsNewIfNotFound; }
			set { _userListReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductTerminations", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'InvoiceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInvoice()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual InvoiceEntity Invoice
		{
			get	{ return GetSingleInvoice(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInvoice(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductTerminations", "Invoice", _invoice, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Invoice. When set to true, Invoice is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoice is accessed. You can always execute a forced fetch by calling GetSingleInvoice(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoice
		{
			get	{ return _alwaysFetchInvoice; }
			set	{ _alwaysFetchInvoice = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoice already has been fetched. Setting this property to false when Invoice has been fetched
		/// will set Invoice to null as well. Setting this property to true while Invoice hasn't been fetched disables lazy loading for Invoice</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoice
		{
			get { return _alreadyFetchedInvoice;}
			set 
			{
				if(_alreadyFetchedInvoice && !value)
				{
					this.Invoice = null;
				}
				_alreadyFetchedInvoice = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Invoice is not found
		/// in the database. When set to true, Invoice will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool InvoiceReturnsNewIfNotFound
		{
			get	{ return _invoiceReturnsNewIfNotFound; }
			set { _invoiceReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get	{ return GetSingleProduct(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProduct(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductTerminations", "Product", _product, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Product. When set to true, Product is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Product is accessed. You can always execute a forced fetch by calling GetSingleProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProduct
		{
			get	{ return _alwaysFetchProduct; }
			set	{ _alwaysFetchProduct = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Product already has been fetched. Setting this property to false when Product has been fetched
		/// will set Product to null as well. Setting this property to true while Product hasn't been fetched disables lazy loading for Product</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProduct
		{
			get { return _alreadyFetchedProduct;}
			set 
			{
				if(_alreadyFetchedProduct && !value)
				{
					this.Product = null;
				}
				_alreadyFetchedProduct = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Product is not found
		/// in the database. When set to true, Product will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProductReturnsNewIfNotFound
		{
			get	{ return _productReturnsNewIfNotFound; }
			set { _productReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductTerminationTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductTerminationType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductTerminationTypeEntity ProductTerminationType
		{
			get	{ return GetSingleProductTerminationType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductTerminationType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductTerminations", "ProductTerminationType", _productTerminationType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductTerminationType. When set to true, ProductTerminationType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductTerminationType is accessed. You can always execute a forced fetch by calling GetSingleProductTerminationType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductTerminationType
		{
			get	{ return _alwaysFetchProductTerminationType; }
			set	{ _alwaysFetchProductTerminationType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductTerminationType already has been fetched. Setting this property to false when ProductTerminationType has been fetched
		/// will set ProductTerminationType to null as well. Setting this property to true while ProductTerminationType hasn't been fetched disables lazy loading for ProductTerminationType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductTerminationType
		{
			get { return _alreadyFetchedProductTerminationType;}
			set 
			{
				if(_alreadyFetchedProductTerminationType && !value)
				{
					this.ProductTerminationType = null;
				}
				_alreadyFetchedProductTerminationType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductTerminationType is not found
		/// in the database. When set to true, ProductTerminationType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProductTerminationTypeReturnsNewIfNotFound
		{
			get	{ return _productTerminationTypeReturnsNewIfNotFound; }
			set { _productTerminationTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ProductTerminationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
