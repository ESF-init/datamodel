﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Panel'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PanelEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.GuiDefCollection	_guiDefs;
		private bool	_alwaysFetchGuiDefs, _alreadyFetchedGuiDefs;
		private VarioSL.Entities.CollectionClasses.PredefinedKeyCollection	_predefinedKeys;
		private bool	_alwaysFetchPredefinedKeys, _alreadyFetchedPredefinedKeys;
		private VarioSL.Entities.CollectionClasses.UserKeyCollection	_originKeys;
		private bool	_alwaysFetchOriginKeys, _alreadyFetchedOriginKeys;
		private VarioSL.Entities.CollectionClasses.UserKeyCollection	_userKeys;
		private bool	_alwaysFetchUserKeys, _alreadyFetchedUserKeys;
		private DeviceClassEntity _deviceClass;
		private bool	_alwaysFetchDeviceClass, _alreadyFetchedDeviceClass, _deviceClassReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeviceClass</summary>
			public static readonly string DeviceClass = "DeviceClass";
			/// <summary>Member name GuiDefs</summary>
			public static readonly string GuiDefs = "GuiDefs";
			/// <summary>Member name PredefinedKeys</summary>
			public static readonly string PredefinedKeys = "PredefinedKeys";
			/// <summary>Member name OriginKeys</summary>
			public static readonly string OriginKeys = "OriginKeys";
			/// <summary>Member name UserKeys</summary>
			public static readonly string UserKeys = "UserKeys";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PanelEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PanelEntity() :base("PanelEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="panelID">PK value for Panel which data should be fetched into this Panel object</param>
		public PanelEntity(System.Int64 panelID):base("PanelEntity")
		{
			InitClassFetch(panelID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="panelID">PK value for Panel which data should be fetched into this Panel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PanelEntity(System.Int64 panelID, IPrefetchPath prefetchPathToUse):base("PanelEntity")
		{
			InitClassFetch(panelID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="panelID">PK value for Panel which data should be fetched into this Panel object</param>
		/// <param name="validator">The custom validator object for this PanelEntity</param>
		public PanelEntity(System.Int64 panelID, IValidator validator):base("PanelEntity")
		{
			InitClassFetch(panelID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PanelEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_guiDefs = (VarioSL.Entities.CollectionClasses.GuiDefCollection)info.GetValue("_guiDefs", typeof(VarioSL.Entities.CollectionClasses.GuiDefCollection));
			_alwaysFetchGuiDefs = info.GetBoolean("_alwaysFetchGuiDefs");
			_alreadyFetchedGuiDefs = info.GetBoolean("_alreadyFetchedGuiDefs");

			_predefinedKeys = (VarioSL.Entities.CollectionClasses.PredefinedKeyCollection)info.GetValue("_predefinedKeys", typeof(VarioSL.Entities.CollectionClasses.PredefinedKeyCollection));
			_alwaysFetchPredefinedKeys = info.GetBoolean("_alwaysFetchPredefinedKeys");
			_alreadyFetchedPredefinedKeys = info.GetBoolean("_alreadyFetchedPredefinedKeys");

			_originKeys = (VarioSL.Entities.CollectionClasses.UserKeyCollection)info.GetValue("_originKeys", typeof(VarioSL.Entities.CollectionClasses.UserKeyCollection));
			_alwaysFetchOriginKeys = info.GetBoolean("_alwaysFetchOriginKeys");
			_alreadyFetchedOriginKeys = info.GetBoolean("_alreadyFetchedOriginKeys");

			_userKeys = (VarioSL.Entities.CollectionClasses.UserKeyCollection)info.GetValue("_userKeys", typeof(VarioSL.Entities.CollectionClasses.UserKeyCollection));
			_alwaysFetchUserKeys = info.GetBoolean("_alwaysFetchUserKeys");
			_alreadyFetchedUserKeys = info.GetBoolean("_alreadyFetchedUserKeys");
			_deviceClass = (DeviceClassEntity)info.GetValue("_deviceClass", typeof(DeviceClassEntity));
			if(_deviceClass!=null)
			{
				_deviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceClassReturnsNewIfNotFound = info.GetBoolean("_deviceClassReturnsNewIfNotFound");
			_alwaysFetchDeviceClass = info.GetBoolean("_alwaysFetchDeviceClass");
			_alreadyFetchedDeviceClass = info.GetBoolean("_alreadyFetchedDeviceClass");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PanelFieldIndex)fieldIndex)
			{
				case PanelFieldIndex.DeviceClassID:
					DesetupSyncDeviceClass(true, false);
					_alreadyFetchedDeviceClass = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedGuiDefs = (_guiDefs.Count > 0);
			_alreadyFetchedPredefinedKeys = (_predefinedKeys.Count > 0);
			_alreadyFetchedOriginKeys = (_originKeys.Count > 0);
			_alreadyFetchedUserKeys = (_userKeys.Count > 0);
			_alreadyFetchedDeviceClass = (_deviceClass != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeviceClass":
					toReturn.Add(Relations.DeviceClassEntityUsingDeviceClassID);
					break;
				case "GuiDefs":
					toReturn.Add(Relations.GuiDefEntityUsingPanelID);
					break;
				case "PredefinedKeys":
					toReturn.Add(Relations.PredefinedKeyEntityUsingPanelID);
					break;
				case "OriginKeys":
					toReturn.Add(Relations.UserKeyEntityUsingDestinationPanelID);
					break;
				case "UserKeys":
					toReturn.Add(Relations.UserKeyEntityUsingPanelID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_guiDefs", (!this.MarkedForDeletion?_guiDefs:null));
			info.AddValue("_alwaysFetchGuiDefs", _alwaysFetchGuiDefs);
			info.AddValue("_alreadyFetchedGuiDefs", _alreadyFetchedGuiDefs);
			info.AddValue("_predefinedKeys", (!this.MarkedForDeletion?_predefinedKeys:null));
			info.AddValue("_alwaysFetchPredefinedKeys", _alwaysFetchPredefinedKeys);
			info.AddValue("_alreadyFetchedPredefinedKeys", _alreadyFetchedPredefinedKeys);
			info.AddValue("_originKeys", (!this.MarkedForDeletion?_originKeys:null));
			info.AddValue("_alwaysFetchOriginKeys", _alwaysFetchOriginKeys);
			info.AddValue("_alreadyFetchedOriginKeys", _alreadyFetchedOriginKeys);
			info.AddValue("_userKeys", (!this.MarkedForDeletion?_userKeys:null));
			info.AddValue("_alwaysFetchUserKeys", _alwaysFetchUserKeys);
			info.AddValue("_alreadyFetchedUserKeys", _alreadyFetchedUserKeys);
			info.AddValue("_deviceClass", (!this.MarkedForDeletion?_deviceClass:null));
			info.AddValue("_deviceClassReturnsNewIfNotFound", _deviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceClass", _alwaysFetchDeviceClass);
			info.AddValue("_alreadyFetchedDeviceClass", _alreadyFetchedDeviceClass);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeviceClass":
					_alreadyFetchedDeviceClass = true;
					this.DeviceClass = (DeviceClassEntity)entity;
					break;
				case "GuiDefs":
					_alreadyFetchedGuiDefs = true;
					if(entity!=null)
					{
						this.GuiDefs.Add((GuiDefEntity)entity);
					}
					break;
				case "PredefinedKeys":
					_alreadyFetchedPredefinedKeys = true;
					if(entity!=null)
					{
						this.PredefinedKeys.Add((PredefinedKeyEntity)entity);
					}
					break;
				case "OriginKeys":
					_alreadyFetchedOriginKeys = true;
					if(entity!=null)
					{
						this.OriginKeys.Add((UserKeyEntity)entity);
					}
					break;
				case "UserKeys":
					_alreadyFetchedUserKeys = true;
					if(entity!=null)
					{
						this.UserKeys.Add((UserKeyEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeviceClass":
					SetupSyncDeviceClass(relatedEntity);
					break;
				case "GuiDefs":
					_guiDefs.Add((GuiDefEntity)relatedEntity);
					break;
				case "PredefinedKeys":
					_predefinedKeys.Add((PredefinedKeyEntity)relatedEntity);
					break;
				case "OriginKeys":
					_originKeys.Add((UserKeyEntity)relatedEntity);
					break;
				case "UserKeys":
					_userKeys.Add((UserKeyEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeviceClass":
					DesetupSyncDeviceClass(false, true);
					break;
				case "GuiDefs":
					this.PerformRelatedEntityRemoval(_guiDefs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PredefinedKeys":
					this.PerformRelatedEntityRemoval(_predefinedKeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OriginKeys":
					this.PerformRelatedEntityRemoval(_originKeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UserKeys":
					this.PerformRelatedEntityRemoval(_userKeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_deviceClass!=null)
			{
				toReturn.Add(_deviceClass);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_guiDefs);
			toReturn.Add(_predefinedKeys);
			toReturn.Add(_originKeys);
			toReturn.Add(_userKeys);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="panelID">PK value for Panel which data should be fetched into this Panel object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 panelID)
		{
			return FetchUsingPK(panelID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="panelID">PK value for Panel which data should be fetched into this Panel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 panelID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(panelID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="panelID">PK value for Panel which data should be fetched into this Panel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 panelID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(panelID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="panelID">PK value for Panel which data should be fetched into this Panel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 panelID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(panelID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PanelID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PanelRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GuiDefEntity'</returns>
		public VarioSL.Entities.CollectionClasses.GuiDefCollection GetMultiGuiDefs(bool forceFetch)
		{
			return GetMultiGuiDefs(forceFetch, _guiDefs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GuiDefEntity'</returns>
		public VarioSL.Entities.CollectionClasses.GuiDefCollection GetMultiGuiDefs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGuiDefs(forceFetch, _guiDefs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.GuiDefCollection GetMultiGuiDefs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGuiDefs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.GuiDefCollection GetMultiGuiDefs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGuiDefs || forceFetch || _alwaysFetchGuiDefs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_guiDefs);
				_guiDefs.SuppressClearInGetMulti=!forceFetch;
				_guiDefs.EntityFactoryToUse = entityFactoryToUse;
				_guiDefs.GetMultiManyToOne(null, this, null, filter);
				_guiDefs.SuppressClearInGetMulti=false;
				_alreadyFetchedGuiDefs = true;
			}
			return _guiDefs;
		}

		/// <summary> Sets the collection parameters for the collection for 'GuiDefs'. These settings will be taken into account
		/// when the property GuiDefs is requested or GetMultiGuiDefs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGuiDefs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_guiDefs.SortClauses=sortClauses;
			_guiDefs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PredefinedKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PredefinedKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PredefinedKeyCollection GetMultiPredefinedKeys(bool forceFetch)
		{
			return GetMultiPredefinedKeys(forceFetch, _predefinedKeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PredefinedKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PredefinedKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PredefinedKeyCollection GetMultiPredefinedKeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPredefinedKeys(forceFetch, _predefinedKeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PredefinedKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PredefinedKeyCollection GetMultiPredefinedKeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPredefinedKeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PredefinedKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PredefinedKeyCollection GetMultiPredefinedKeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPredefinedKeys || forceFetch || _alwaysFetchPredefinedKeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_predefinedKeys);
				_predefinedKeys.SuppressClearInGetMulti=!forceFetch;
				_predefinedKeys.EntityFactoryToUse = entityFactoryToUse;
				_predefinedKeys.GetMultiManyToOne(this, null, filter);
				_predefinedKeys.SuppressClearInGetMulti=false;
				_alreadyFetchedPredefinedKeys = true;
			}
			return _predefinedKeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'PredefinedKeys'. These settings will be taken into account
		/// when the property PredefinedKeys is requested or GetMultiPredefinedKeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPredefinedKeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_predefinedKeys.SortClauses=sortClauses;
			_predefinedKeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserKeyCollection GetMultiOriginKeys(bool forceFetch)
		{
			return GetMultiOriginKeys(forceFetch, _originKeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserKeyCollection GetMultiOriginKeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOriginKeys(forceFetch, _originKeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UserKeyCollection GetMultiOriginKeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOriginKeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UserKeyCollection GetMultiOriginKeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOriginKeys || forceFetch || _alwaysFetchOriginKeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_originKeys);
				_originKeys.SuppressClearInGetMulti=!forceFetch;
				_originKeys.EntityFactoryToUse = entityFactoryToUse;
				_originKeys.GetMultiManyToOne(this, null, null, filter);
				_originKeys.SuppressClearInGetMulti=false;
				_alreadyFetchedOriginKeys = true;
			}
			return _originKeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'OriginKeys'. These settings will be taken into account
		/// when the property OriginKeys is requested or GetMultiOriginKeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOriginKeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_originKeys.SortClauses=sortClauses;
			_originKeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserKeyCollection GetMultiUserKeys(bool forceFetch)
		{
			return GetMultiUserKeys(forceFetch, _userKeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserKeyCollection GetMultiUserKeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserKeys(forceFetch, _userKeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UserKeyCollection GetMultiUserKeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserKeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UserKeyCollection GetMultiUserKeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserKeys || forceFetch || _alwaysFetchUserKeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userKeys);
				_userKeys.SuppressClearInGetMulti=!forceFetch;
				_userKeys.EntityFactoryToUse = entityFactoryToUse;
				_userKeys.GetMultiManyToOne(null, this, null, filter);
				_userKeys.SuppressClearInGetMulti=false;
				_alreadyFetchedUserKeys = true;
			}
			return _userKeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserKeys'. These settings will be taken into account
		/// when the property UserKeys is requested or GetMultiUserKeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserKeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userKeys.SortClauses=sortClauses;
			_userKeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public DeviceClassEntity GetSingleDeviceClass()
		{
			return GetSingleDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public virtual DeviceClassEntity GetSingleDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceClass || forceFetch || _alwaysFetchDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceClassEntityUsingDeviceClassID);
				DeviceClassEntity newEntity = new DeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceClassID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceClass = newEntity;
				_alreadyFetchedDeviceClass = fetchResult;
			}
			return _deviceClass;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeviceClass", _deviceClass);
			toReturn.Add("GuiDefs", _guiDefs);
			toReturn.Add("PredefinedKeys", _predefinedKeys);
			toReturn.Add("OriginKeys", _originKeys);
			toReturn.Add("UserKeys", _userKeys);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="panelID">PK value for Panel which data should be fetched into this Panel object</param>
		/// <param name="validator">The validator object for this PanelEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 panelID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(panelID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_guiDefs = new VarioSL.Entities.CollectionClasses.GuiDefCollection();
			_guiDefs.SetContainingEntityInfo(this, "Panel");

			_predefinedKeys = new VarioSL.Entities.CollectionClasses.PredefinedKeyCollection();
			_predefinedKeys.SetContainingEntityInfo(this, "Panel");

			_originKeys = new VarioSL.Entities.CollectionClasses.UserKeyCollection();
			_originKeys.SetContainingEntityInfo(this, "DestinationPanel");

			_userKeys = new VarioSL.Entities.CollectionClasses.UserKeyCollection();
			_userKeys.SetContainingEntityInfo(this, "Panel");
			_deviceClassReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplyCardTicketFilter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EvendNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PanelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _deviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticPanelRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, signalRelatedEntity, "Panels", resetFKFields, new int[] { (int)PanelFieldIndex.DeviceClassID } );		
			_deviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _deviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceClass(IEntityCore relatedEntity)
		{
			if(_deviceClass!=relatedEntity)
			{		
				DesetupSyncDeviceClass(true, true);
				_deviceClass = (DeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticPanelRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, ref _alreadyFetchedDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="panelID">PK value for Panel which data should be fetched into this Panel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 panelID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PanelFieldIndex.PanelID].ForcedCurrentValueWrite(panelID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePanelDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PanelEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PanelRelations Relations
		{
			get	{ return new PanelRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'GuiDef' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGuiDefs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.GuiDefCollection(), (IEntityRelation)GetRelationsForField("GuiDefs")[0], (int)VarioSL.Entities.EntityType.PanelEntity, (int)VarioSL.Entities.EntityType.GuiDefEntity, 0, null, null, null, "GuiDefs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PredefinedKey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPredefinedKeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PredefinedKeyCollection(), (IEntityRelation)GetRelationsForField("PredefinedKeys")[0], (int)VarioSL.Entities.EntityType.PanelEntity, (int)VarioSL.Entities.EntityType.PredefinedKeyEntity, 0, null, null, null, "PredefinedKeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserKey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOriginKeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserKeyCollection(), (IEntityRelation)GetRelationsForField("OriginKeys")[0], (int)VarioSL.Entities.EntityType.PanelEntity, (int)VarioSL.Entities.EntityType.UserKeyEntity, 0, null, null, null, "OriginKeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserKey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserKeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserKeyCollection(), (IEntityRelation)GetRelationsForField("UserKeys")[0], (int)VarioSL.Entities.EntityType.PanelEntity, (int)VarioSL.Entities.EntityType.UserKeyEntity, 0, null, null, null, "UserKeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceClassCollection(), (IEntityRelation)GetRelationsForField("DeviceClass")[0], (int)VarioSL.Entities.EntityType.PanelEntity, (int)VarioSL.Entities.EntityType.DeviceClassEntity, 0, null, null, null, "DeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ApplyCardTicketFilter property of the Entity Panel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PANELS"."APPLYCARDTICKETFILTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ApplyCardTicketFilter
		{
			get { return (System.Boolean)GetValue((int)PanelFieldIndex.ApplyCardTicketFilter, true); }
			set	{ SetValue((int)PanelFieldIndex.ApplyCardTicketFilter, value, true); }
		}

		/// <summary> The Description property of the Entity Panel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PANELS"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)PanelFieldIndex.Description, true); }
			set	{ SetValue((int)PanelFieldIndex.Description, value, true); }
		}

		/// <summary> The DeviceClassID property of the Entity Panel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PANELS"."DEVICECLASS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceClassID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PanelFieldIndex.DeviceClassID, false); }
			set	{ SetValue((int)PanelFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The EvendNumber property of the Entity Panel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PANELS"."EVENDNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EvendNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)PanelFieldIndex.EvendNumber, false); }
			set	{ SetValue((int)PanelFieldIndex.EvendNumber, value, true); }
		}

		/// <summary> The Name property of the Entity Panel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PANELS"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PanelFieldIndex.Name, true); }
			set	{ SetValue((int)PanelFieldIndex.Name, value, true); }
		}

		/// <summary> The Number property of the Entity Panel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PANELS"."NUMBER_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Number
		{
			get { return (Nullable<System.Int64>)GetValue((int)PanelFieldIndex.Number, false); }
			set	{ SetValue((int)PanelFieldIndex.Number, value, true); }
		}

		/// <summary> The PanelID property of the Entity Panel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PANELS"."PANELID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 PanelID
		{
			get { return (System.Int64)GetValue((int)PanelFieldIndex.PanelID, true); }
			set	{ SetValue((int)PanelFieldIndex.PanelID, value, true); }
		}

		/// <summary> The TariffID property of the Entity Panel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PANELS"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PanelFieldIndex.TariffID, false); }
			set	{ SetValue((int)PanelFieldIndex.TariffID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGuiDefs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.GuiDefCollection GuiDefs
		{
			get	{ return GetMultiGuiDefs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GuiDefs. When set to true, GuiDefs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GuiDefs is accessed. You can always execute/ a forced fetch by calling GetMultiGuiDefs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGuiDefs
		{
			get	{ return _alwaysFetchGuiDefs; }
			set	{ _alwaysFetchGuiDefs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GuiDefs already has been fetched. Setting this property to false when GuiDefs has been fetched
		/// will clear the GuiDefs collection well. Setting this property to true while GuiDefs hasn't been fetched disables lazy loading for GuiDefs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGuiDefs
		{
			get { return _alreadyFetchedGuiDefs;}
			set 
			{
				if(_alreadyFetchedGuiDefs && !value && (_guiDefs != null))
				{
					_guiDefs.Clear();
				}
				_alreadyFetchedGuiDefs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PredefinedKeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPredefinedKeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PredefinedKeyCollection PredefinedKeys
		{
			get	{ return GetMultiPredefinedKeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PredefinedKeys. When set to true, PredefinedKeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PredefinedKeys is accessed. You can always execute/ a forced fetch by calling GetMultiPredefinedKeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPredefinedKeys
		{
			get	{ return _alwaysFetchPredefinedKeys; }
			set	{ _alwaysFetchPredefinedKeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PredefinedKeys already has been fetched. Setting this property to false when PredefinedKeys has been fetched
		/// will clear the PredefinedKeys collection well. Setting this property to true while PredefinedKeys hasn't been fetched disables lazy loading for PredefinedKeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPredefinedKeys
		{
			get { return _alreadyFetchedPredefinedKeys;}
			set 
			{
				if(_alreadyFetchedPredefinedKeys && !value && (_predefinedKeys != null))
				{
					_predefinedKeys.Clear();
				}
				_alreadyFetchedPredefinedKeys = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOriginKeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UserKeyCollection OriginKeys
		{
			get	{ return GetMultiOriginKeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OriginKeys. When set to true, OriginKeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OriginKeys is accessed. You can always execute/ a forced fetch by calling GetMultiOriginKeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOriginKeys
		{
			get	{ return _alwaysFetchOriginKeys; }
			set	{ _alwaysFetchOriginKeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OriginKeys already has been fetched. Setting this property to false when OriginKeys has been fetched
		/// will clear the OriginKeys collection well. Setting this property to true while OriginKeys hasn't been fetched disables lazy loading for OriginKeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOriginKeys
		{
			get { return _alreadyFetchedOriginKeys;}
			set 
			{
				if(_alreadyFetchedOriginKeys && !value && (_originKeys != null))
				{
					_originKeys.Clear();
				}
				_alreadyFetchedOriginKeys = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserKeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UserKeyCollection UserKeys
		{
			get	{ return GetMultiUserKeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserKeys. When set to true, UserKeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserKeys is accessed. You can always execute/ a forced fetch by calling GetMultiUserKeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserKeys
		{
			get	{ return _alwaysFetchUserKeys; }
			set	{ _alwaysFetchUserKeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserKeys already has been fetched. Setting this property to false when UserKeys has been fetched
		/// will clear the UserKeys collection well. Setting this property to true while UserKeys hasn't been fetched disables lazy loading for UserKeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserKeys
		{
			get { return _alreadyFetchedUserKeys;}
			set 
			{
				if(_alreadyFetchedUserKeys && !value && (_userKeys != null))
				{
					_userKeys.Clear();
				}
				_alreadyFetchedUserKeys = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'DeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceClassEntity DeviceClass
		{
			get	{ return GetSingleDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Panels", "DeviceClass", _deviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceClass. When set to true, DeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceClass is accessed. You can always execute a forced fetch by calling GetSingleDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceClass
		{
			get	{ return _alwaysFetchDeviceClass; }
			set	{ _alwaysFetchDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceClass already has been fetched. Setting this property to false when DeviceClass has been fetched
		/// will set DeviceClass to null as well. Setting this property to true while DeviceClass hasn't been fetched disables lazy loading for DeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceClass
		{
			get { return _alreadyFetchedDeviceClass;}
			set 
			{
				if(_alreadyFetchedDeviceClass && !value)
				{
					this.DeviceClass = null;
				}
				_alreadyFetchedDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceClass is not found
		/// in the database. When set to true, DeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceClassReturnsNewIfNotFound
		{
			get	{ return _deviceClassReturnsNewIfNotFound; }
			set { _deviceClassReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PanelEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
