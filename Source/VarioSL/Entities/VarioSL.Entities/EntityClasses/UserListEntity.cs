﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UserList'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class UserListEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AccountEntryCollection	_accountEntries;
		private bool	_alwaysFetchAccountEntries, _alreadyFetchedAccountEntries;
		private VarioSL.Entities.CollectionClasses.UserIsMemberCollection	_userIsMembers;
		private bool	_alwaysFetchUserIsMembers, _alreadyFetchedUserIsMembers;
		private VarioSL.Entities.CollectionClasses.UserToWorkstationCollection	_usersToWorkstations;
		private bool	_alwaysFetchUsersToWorkstations, _alreadyFetchedUsersToWorkstations;
		private VarioSL.Entities.CollectionClasses.VarioSettlementCollection	_settlements;
		private bool	_alwaysFetchSettlements, _alreadyFetchedSettlements;
		private VarioSL.Entities.CollectionClasses.ContractTerminationCollection	_contractTerminations;
		private bool	_alwaysFetchContractTerminations, _alreadyFetchedContractTerminations;
		private VarioSL.Entities.CollectionClasses.FilterValueSetCollection	_filterValueSets;
		private bool	_alwaysFetchFilterValueSets, _alreadyFetchedFilterValueSets;
		private VarioSL.Entities.CollectionClasses.FilterValueSetCollection	_filterValueSets_;
		private bool	_alwaysFetchFilterValueSets_, _alreadyFetchedFilterValueSets_;
		private VarioSL.Entities.CollectionClasses.InvoiceCollection	_invoices;
		private bool	_alwaysFetchInvoices, _alreadyFetchedInvoices;
		private VarioSL.Entities.CollectionClasses.InvoicingCollection	_invoicings;
		private bool	_alwaysFetchInvoicings, _alreadyFetchedInvoicings;
		private VarioSL.Entities.CollectionClasses.OrderCollection	_orders;
		private bool	_alwaysFetchOrders, _alreadyFetchedOrders;
		private VarioSL.Entities.CollectionClasses.PostingCollection	_postings;
		private bool	_alwaysFetchPostings, _alreadyFetchedPostings;
		private VarioSL.Entities.CollectionClasses.ProductTerminationCollection	_productTerminations;
		private bool	_alwaysFetchProductTerminations, _alreadyFetchedProductTerminations;
		private VarioSL.Entities.CollectionClasses.ReportJobCollection	_reportJobs;
		private bool	_alwaysFetchReportJobs, _alreadyFetchedReportJobs;
		private VarioSL.Entities.CollectionClasses.WorkItemCollection	_workItems;
		private bool	_alwaysFetchWorkItems, _alreadyFetchedWorkItems;
		private VarioSL.Entities.CollectionClasses.WorkstationCollection _workstations;
		private bool	_alwaysFetchWorkstations, _alreadyFetchedWorkstations;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private DebtorEntity _debtor;
		private bool	_alwaysFetchDebtor, _alreadyFetchedDebtor, _debtorReturnsNewIfNotFound;
		private DepotEntity _varioDepot;
		private bool	_alwaysFetchVarioDepot, _alreadyFetchedVarioDepot, _varioDepotReturnsNewIfNotFound;
		private LanguageEntity _language;
		private bool	_alwaysFetchLanguage, _alreadyFetchedLanguage, _languageReturnsNewIfNotFound;
		private UserLanguageEntity _userLanguage;
		private bool	_alwaysFetchUserLanguage, _alreadyFetchedUserLanguage, _userLanguageReturnsNewIfNotFound;
		private VarioAddressEntity _varioAddress;
		private bool	_alwaysFetchVarioAddress, _alreadyFetchedVarioAddress, _varioAddressReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name Debtor</summary>
			public static readonly string Debtor = "Debtor";
			/// <summary>Member name VarioDepot</summary>
			public static readonly string VarioDepot = "VarioDepot";
			/// <summary>Member name Language</summary>
			public static readonly string Language = "Language";
			/// <summary>Member name UserLanguage</summary>
			public static readonly string UserLanguage = "UserLanguage";
			/// <summary>Member name VarioAddress</summary>
			public static readonly string VarioAddress = "VarioAddress";
			/// <summary>Member name AccountEntries</summary>
			public static readonly string AccountEntries = "AccountEntries";
			/// <summary>Member name UserIsMembers</summary>
			public static readonly string UserIsMembers = "UserIsMembers";
			/// <summary>Member name UsersToWorkstations</summary>
			public static readonly string UsersToWorkstations = "UsersToWorkstations";
			/// <summary>Member name Settlements</summary>
			public static readonly string Settlements = "Settlements";
			/// <summary>Member name ContractTerminations</summary>
			public static readonly string ContractTerminations = "ContractTerminations";
			/// <summary>Member name FilterValueSets</summary>
			public static readonly string FilterValueSets = "FilterValueSets";
			/// <summary>Member name FilterValueSets_</summary>
			public static readonly string FilterValueSets_ = "FilterValueSets_";
			/// <summary>Member name Invoices</summary>
			public static readonly string Invoices = "Invoices";
			/// <summary>Member name Invoicings</summary>
			public static readonly string Invoicings = "Invoicings";
			/// <summary>Member name Orders</summary>
			public static readonly string Orders = "Orders";
			/// <summary>Member name Postings</summary>
			public static readonly string Postings = "Postings";
			/// <summary>Member name ProductTerminations</summary>
			public static readonly string ProductTerminations = "ProductTerminations";
			/// <summary>Member name ReportJobs</summary>
			public static readonly string ReportJobs = "ReportJobs";
			/// <summary>Member name WorkItems</summary>
			public static readonly string WorkItems = "WorkItems";
			/// <summary>Member name Workstations</summary>
			public static readonly string Workstations = "Workstations";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UserListEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public UserListEntity() :base("UserListEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="userID">PK value for UserList which data should be fetched into this UserList object</param>
		public UserListEntity(System.Int64 userID):base("UserListEntity")
		{
			InitClassFetch(userID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="userID">PK value for UserList which data should be fetched into this UserList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UserListEntity(System.Int64 userID, IPrefetchPath prefetchPathToUse):base("UserListEntity")
		{
			InitClassFetch(userID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="userID">PK value for UserList which data should be fetched into this UserList object</param>
		/// <param name="validator">The custom validator object for this UserListEntity</param>
		public UserListEntity(System.Int64 userID, IValidator validator):base("UserListEntity")
		{
			InitClassFetch(userID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UserListEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_accountEntries = (VarioSL.Entities.CollectionClasses.AccountEntryCollection)info.GetValue("_accountEntries", typeof(VarioSL.Entities.CollectionClasses.AccountEntryCollection));
			_alwaysFetchAccountEntries = info.GetBoolean("_alwaysFetchAccountEntries");
			_alreadyFetchedAccountEntries = info.GetBoolean("_alreadyFetchedAccountEntries");

			_userIsMembers = (VarioSL.Entities.CollectionClasses.UserIsMemberCollection)info.GetValue("_userIsMembers", typeof(VarioSL.Entities.CollectionClasses.UserIsMemberCollection));
			_alwaysFetchUserIsMembers = info.GetBoolean("_alwaysFetchUserIsMembers");
			_alreadyFetchedUserIsMembers = info.GetBoolean("_alreadyFetchedUserIsMembers");

			_usersToWorkstations = (VarioSL.Entities.CollectionClasses.UserToWorkstationCollection)info.GetValue("_usersToWorkstations", typeof(VarioSL.Entities.CollectionClasses.UserToWorkstationCollection));
			_alwaysFetchUsersToWorkstations = info.GetBoolean("_alwaysFetchUsersToWorkstations");
			_alreadyFetchedUsersToWorkstations = info.GetBoolean("_alreadyFetchedUsersToWorkstations");

			_settlements = (VarioSL.Entities.CollectionClasses.VarioSettlementCollection)info.GetValue("_settlements", typeof(VarioSL.Entities.CollectionClasses.VarioSettlementCollection));
			_alwaysFetchSettlements = info.GetBoolean("_alwaysFetchSettlements");
			_alreadyFetchedSettlements = info.GetBoolean("_alreadyFetchedSettlements");

			_contractTerminations = (VarioSL.Entities.CollectionClasses.ContractTerminationCollection)info.GetValue("_contractTerminations", typeof(VarioSL.Entities.CollectionClasses.ContractTerminationCollection));
			_alwaysFetchContractTerminations = info.GetBoolean("_alwaysFetchContractTerminations");
			_alreadyFetchedContractTerminations = info.GetBoolean("_alreadyFetchedContractTerminations");

			_filterValueSets = (VarioSL.Entities.CollectionClasses.FilterValueSetCollection)info.GetValue("_filterValueSets", typeof(VarioSL.Entities.CollectionClasses.FilterValueSetCollection));
			_alwaysFetchFilterValueSets = info.GetBoolean("_alwaysFetchFilterValueSets");
			_alreadyFetchedFilterValueSets = info.GetBoolean("_alreadyFetchedFilterValueSets");

			_filterValueSets_ = (VarioSL.Entities.CollectionClasses.FilterValueSetCollection)info.GetValue("_filterValueSets_", typeof(VarioSL.Entities.CollectionClasses.FilterValueSetCollection));
			_alwaysFetchFilterValueSets_ = info.GetBoolean("_alwaysFetchFilterValueSets_");
			_alreadyFetchedFilterValueSets_ = info.GetBoolean("_alreadyFetchedFilterValueSets_");

			_invoices = (VarioSL.Entities.CollectionClasses.InvoiceCollection)info.GetValue("_invoices", typeof(VarioSL.Entities.CollectionClasses.InvoiceCollection));
			_alwaysFetchInvoices = info.GetBoolean("_alwaysFetchInvoices");
			_alreadyFetchedInvoices = info.GetBoolean("_alreadyFetchedInvoices");

			_invoicings = (VarioSL.Entities.CollectionClasses.InvoicingCollection)info.GetValue("_invoicings", typeof(VarioSL.Entities.CollectionClasses.InvoicingCollection));
			_alwaysFetchInvoicings = info.GetBoolean("_alwaysFetchInvoicings");
			_alreadyFetchedInvoicings = info.GetBoolean("_alreadyFetchedInvoicings");

			_orders = (VarioSL.Entities.CollectionClasses.OrderCollection)info.GetValue("_orders", typeof(VarioSL.Entities.CollectionClasses.OrderCollection));
			_alwaysFetchOrders = info.GetBoolean("_alwaysFetchOrders");
			_alreadyFetchedOrders = info.GetBoolean("_alreadyFetchedOrders");

			_postings = (VarioSL.Entities.CollectionClasses.PostingCollection)info.GetValue("_postings", typeof(VarioSL.Entities.CollectionClasses.PostingCollection));
			_alwaysFetchPostings = info.GetBoolean("_alwaysFetchPostings");
			_alreadyFetchedPostings = info.GetBoolean("_alreadyFetchedPostings");

			_productTerminations = (VarioSL.Entities.CollectionClasses.ProductTerminationCollection)info.GetValue("_productTerminations", typeof(VarioSL.Entities.CollectionClasses.ProductTerminationCollection));
			_alwaysFetchProductTerminations = info.GetBoolean("_alwaysFetchProductTerminations");
			_alreadyFetchedProductTerminations = info.GetBoolean("_alreadyFetchedProductTerminations");

			_reportJobs = (VarioSL.Entities.CollectionClasses.ReportJobCollection)info.GetValue("_reportJobs", typeof(VarioSL.Entities.CollectionClasses.ReportJobCollection));
			_alwaysFetchReportJobs = info.GetBoolean("_alwaysFetchReportJobs");
			_alreadyFetchedReportJobs = info.GetBoolean("_alreadyFetchedReportJobs");

			_workItems = (VarioSL.Entities.CollectionClasses.WorkItemCollection)info.GetValue("_workItems", typeof(VarioSL.Entities.CollectionClasses.WorkItemCollection));
			_alwaysFetchWorkItems = info.GetBoolean("_alwaysFetchWorkItems");
			_alreadyFetchedWorkItems = info.GetBoolean("_alreadyFetchedWorkItems");
			_workstations = (VarioSL.Entities.CollectionClasses.WorkstationCollection)info.GetValue("_workstations", typeof(VarioSL.Entities.CollectionClasses.WorkstationCollection));
			_alwaysFetchWorkstations = info.GetBoolean("_alwaysFetchWorkstations");
			_alreadyFetchedWorkstations = info.GetBoolean("_alreadyFetchedWorkstations");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_debtor = (DebtorEntity)info.GetValue("_debtor", typeof(DebtorEntity));
			if(_debtor!=null)
			{
				_debtor.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_debtorReturnsNewIfNotFound = info.GetBoolean("_debtorReturnsNewIfNotFound");
			_alwaysFetchDebtor = info.GetBoolean("_alwaysFetchDebtor");
			_alreadyFetchedDebtor = info.GetBoolean("_alreadyFetchedDebtor");

			_varioDepot = (DepotEntity)info.GetValue("_varioDepot", typeof(DepotEntity));
			if(_varioDepot!=null)
			{
				_varioDepot.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_varioDepotReturnsNewIfNotFound = info.GetBoolean("_varioDepotReturnsNewIfNotFound");
			_alwaysFetchVarioDepot = info.GetBoolean("_alwaysFetchVarioDepot");
			_alreadyFetchedVarioDepot = info.GetBoolean("_alreadyFetchedVarioDepot");

			_language = (LanguageEntity)info.GetValue("_language", typeof(LanguageEntity));
			if(_language!=null)
			{
				_language.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_languageReturnsNewIfNotFound = info.GetBoolean("_languageReturnsNewIfNotFound");
			_alwaysFetchLanguage = info.GetBoolean("_alwaysFetchLanguage");
			_alreadyFetchedLanguage = info.GetBoolean("_alreadyFetchedLanguage");

			_userLanguage = (UserLanguageEntity)info.GetValue("_userLanguage", typeof(UserLanguageEntity));
			if(_userLanguage!=null)
			{
				_userLanguage.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userLanguageReturnsNewIfNotFound = info.GetBoolean("_userLanguageReturnsNewIfNotFound");
			_alwaysFetchUserLanguage = info.GetBoolean("_alwaysFetchUserLanguage");
			_alreadyFetchedUserLanguage = info.GetBoolean("_alreadyFetchedUserLanguage");

			_varioAddress = (VarioAddressEntity)info.GetValue("_varioAddress", typeof(VarioAddressEntity));
			if(_varioAddress!=null)
			{
				_varioAddress.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_varioAddressReturnsNewIfNotFound = info.GetBoolean("_varioAddressReturnsNewIfNotFound");
			_alwaysFetchVarioAddress = info.GetBoolean("_alwaysFetchVarioAddress");
			_alreadyFetchedVarioAddress = info.GetBoolean("_alreadyFetchedVarioAddress");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UserListFieldIndex)fieldIndex)
			{
				case UserListFieldIndex.AddressID:
					DesetupSyncVarioAddress(true, false);
					_alreadyFetchedVarioAddress = false;
					break;
				case UserListFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case UserListFieldIndex.DebtorID:
					DesetupSyncDebtor(true, false);
					_alreadyFetchedDebtor = false;
					break;
				case UserListFieldIndex.DepotID:
					DesetupSyncVarioDepot(true, false);
					_alreadyFetchedVarioDepot = false;
					break;
				case UserListFieldIndex.LanguageID:
					DesetupSyncLanguage(true, false);
					_alreadyFetchedLanguage = false;
					DesetupSyncUserLanguage(true, false);
					_alreadyFetchedUserLanguage = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAccountEntries = (_accountEntries.Count > 0);
			_alreadyFetchedUserIsMembers = (_userIsMembers.Count > 0);
			_alreadyFetchedUsersToWorkstations = (_usersToWorkstations.Count > 0);
			_alreadyFetchedSettlements = (_settlements.Count > 0);
			_alreadyFetchedContractTerminations = (_contractTerminations.Count > 0);
			_alreadyFetchedFilterValueSets = (_filterValueSets.Count > 0);
			_alreadyFetchedFilterValueSets_ = (_filterValueSets_.Count > 0);
			_alreadyFetchedInvoices = (_invoices.Count > 0);
			_alreadyFetchedInvoicings = (_invoicings.Count > 0);
			_alreadyFetchedOrders = (_orders.Count > 0);
			_alreadyFetchedPostings = (_postings.Count > 0);
			_alreadyFetchedProductTerminations = (_productTerminations.Count > 0);
			_alreadyFetchedReportJobs = (_reportJobs.Count > 0);
			_alreadyFetchedWorkItems = (_workItems.Count > 0);
			_alreadyFetchedWorkstations = (_workstations.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedDebtor = (_debtor != null);
			_alreadyFetchedVarioDepot = (_varioDepot != null);
			_alreadyFetchedLanguage = (_language != null);
			_alreadyFetchedUserLanguage = (_userLanguage != null);
			_alreadyFetchedVarioAddress = (_varioAddress != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "Debtor":
					toReturn.Add(Relations.DebtorEntityUsingDebtorID);
					break;
				case "VarioDepot":
					toReturn.Add(Relations.DepotEntityUsingDepotID);
					break;
				case "Language":
					toReturn.Add(Relations.LanguageEntityUsingLanguageID);
					break;
				case "UserLanguage":
					toReturn.Add(Relations.UserLanguageEntityUsingLanguageID);
					break;
				case "VarioAddress":
					toReturn.Add(Relations.VarioAddressEntityUsingAddressID);
					break;
				case "AccountEntries":
					toReturn.Add(Relations.AccountEntryEntityUsingUserID);
					break;
				case "UserIsMembers":
					toReturn.Add(Relations.UserIsMemberEntityUsingUserID);
					break;
				case "UsersToWorkstations":
					toReturn.Add(Relations.UserToWorkstationEntityUsingUserID);
					break;
				case "Settlements":
					toReturn.Add(Relations.VarioSettlementEntityUsingUserID);
					break;
				case "ContractTerminations":
					toReturn.Add(Relations.ContractTerminationEntityUsingEmployeeID);
					break;
				case "FilterValueSets":
					toReturn.Add(Relations.FilterValueSetEntityUsingOwnerID);
					break;
				case "FilterValueSets_":
					toReturn.Add(Relations.FilterValueSetEntityUsingUserID);
					break;
				case "Invoices":
					toReturn.Add(Relations.InvoiceEntityUsingUserID);
					break;
				case "Invoicings":
					toReturn.Add(Relations.InvoicingEntityUsingUserID);
					break;
				case "Orders":
					toReturn.Add(Relations.OrderEntityUsingAssignee);
					break;
				case "Postings":
					toReturn.Add(Relations.PostingEntityUsingUserID);
					break;
				case "ProductTerminations":
					toReturn.Add(Relations.ProductTerminationEntityUsingEmployeeID);
					break;
				case "ReportJobs":
					toReturn.Add(Relations.ReportJobEntityUsingUserID);
					break;
				case "WorkItems":
					toReturn.Add(Relations.WorkItemEntityUsingAssignee);
					break;
				case "Workstations":
					toReturn.Add(Relations.UserToWorkstationEntityUsingUserID, "UserListEntity__", "UserToWorkstation_", JoinHint.None);
					toReturn.Add(UserToWorkstationEntity.Relations.WorkstationEntityUsingWorkstationID, "UserToWorkstation_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_accountEntries", (!this.MarkedForDeletion?_accountEntries:null));
			info.AddValue("_alwaysFetchAccountEntries", _alwaysFetchAccountEntries);
			info.AddValue("_alreadyFetchedAccountEntries", _alreadyFetchedAccountEntries);
			info.AddValue("_userIsMembers", (!this.MarkedForDeletion?_userIsMembers:null));
			info.AddValue("_alwaysFetchUserIsMembers", _alwaysFetchUserIsMembers);
			info.AddValue("_alreadyFetchedUserIsMembers", _alreadyFetchedUserIsMembers);
			info.AddValue("_usersToWorkstations", (!this.MarkedForDeletion?_usersToWorkstations:null));
			info.AddValue("_alwaysFetchUsersToWorkstations", _alwaysFetchUsersToWorkstations);
			info.AddValue("_alreadyFetchedUsersToWorkstations", _alreadyFetchedUsersToWorkstations);
			info.AddValue("_settlements", (!this.MarkedForDeletion?_settlements:null));
			info.AddValue("_alwaysFetchSettlements", _alwaysFetchSettlements);
			info.AddValue("_alreadyFetchedSettlements", _alreadyFetchedSettlements);
			info.AddValue("_contractTerminations", (!this.MarkedForDeletion?_contractTerminations:null));
			info.AddValue("_alwaysFetchContractTerminations", _alwaysFetchContractTerminations);
			info.AddValue("_alreadyFetchedContractTerminations", _alreadyFetchedContractTerminations);
			info.AddValue("_filterValueSets", (!this.MarkedForDeletion?_filterValueSets:null));
			info.AddValue("_alwaysFetchFilterValueSets", _alwaysFetchFilterValueSets);
			info.AddValue("_alreadyFetchedFilterValueSets", _alreadyFetchedFilterValueSets);
			info.AddValue("_filterValueSets_", (!this.MarkedForDeletion?_filterValueSets_:null));
			info.AddValue("_alwaysFetchFilterValueSets_", _alwaysFetchFilterValueSets_);
			info.AddValue("_alreadyFetchedFilterValueSets_", _alreadyFetchedFilterValueSets_);
			info.AddValue("_invoices", (!this.MarkedForDeletion?_invoices:null));
			info.AddValue("_alwaysFetchInvoices", _alwaysFetchInvoices);
			info.AddValue("_alreadyFetchedInvoices", _alreadyFetchedInvoices);
			info.AddValue("_invoicings", (!this.MarkedForDeletion?_invoicings:null));
			info.AddValue("_alwaysFetchInvoicings", _alwaysFetchInvoicings);
			info.AddValue("_alreadyFetchedInvoicings", _alreadyFetchedInvoicings);
			info.AddValue("_orders", (!this.MarkedForDeletion?_orders:null));
			info.AddValue("_alwaysFetchOrders", _alwaysFetchOrders);
			info.AddValue("_alreadyFetchedOrders", _alreadyFetchedOrders);
			info.AddValue("_postings", (!this.MarkedForDeletion?_postings:null));
			info.AddValue("_alwaysFetchPostings", _alwaysFetchPostings);
			info.AddValue("_alreadyFetchedPostings", _alreadyFetchedPostings);
			info.AddValue("_productTerminations", (!this.MarkedForDeletion?_productTerminations:null));
			info.AddValue("_alwaysFetchProductTerminations", _alwaysFetchProductTerminations);
			info.AddValue("_alreadyFetchedProductTerminations", _alreadyFetchedProductTerminations);
			info.AddValue("_reportJobs", (!this.MarkedForDeletion?_reportJobs:null));
			info.AddValue("_alwaysFetchReportJobs", _alwaysFetchReportJobs);
			info.AddValue("_alreadyFetchedReportJobs", _alreadyFetchedReportJobs);
			info.AddValue("_workItems", (!this.MarkedForDeletion?_workItems:null));
			info.AddValue("_alwaysFetchWorkItems", _alwaysFetchWorkItems);
			info.AddValue("_alreadyFetchedWorkItems", _alreadyFetchedWorkItems);
			info.AddValue("_workstations", (!this.MarkedForDeletion?_workstations:null));
			info.AddValue("_alwaysFetchWorkstations", _alwaysFetchWorkstations);
			info.AddValue("_alreadyFetchedWorkstations", _alreadyFetchedWorkstations);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_debtor", (!this.MarkedForDeletion?_debtor:null));
			info.AddValue("_debtorReturnsNewIfNotFound", _debtorReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDebtor", _alwaysFetchDebtor);
			info.AddValue("_alreadyFetchedDebtor", _alreadyFetchedDebtor);
			info.AddValue("_varioDepot", (!this.MarkedForDeletion?_varioDepot:null));
			info.AddValue("_varioDepotReturnsNewIfNotFound", _varioDepotReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVarioDepot", _alwaysFetchVarioDepot);
			info.AddValue("_alreadyFetchedVarioDepot", _alreadyFetchedVarioDepot);
			info.AddValue("_language", (!this.MarkedForDeletion?_language:null));
			info.AddValue("_languageReturnsNewIfNotFound", _languageReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLanguage", _alwaysFetchLanguage);
			info.AddValue("_alreadyFetchedLanguage", _alreadyFetchedLanguage);
			info.AddValue("_userLanguage", (!this.MarkedForDeletion?_userLanguage:null));
			info.AddValue("_userLanguageReturnsNewIfNotFound", _userLanguageReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserLanguage", _alwaysFetchUserLanguage);
			info.AddValue("_alreadyFetchedUserLanguage", _alreadyFetchedUserLanguage);
			info.AddValue("_varioAddress", (!this.MarkedForDeletion?_varioAddress:null));
			info.AddValue("_varioAddressReturnsNewIfNotFound", _varioAddressReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVarioAddress", _alwaysFetchVarioAddress);
			info.AddValue("_alreadyFetchedVarioAddress", _alreadyFetchedVarioAddress);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "Debtor":
					_alreadyFetchedDebtor = true;
					this.Debtor = (DebtorEntity)entity;
					break;
				case "VarioDepot":
					_alreadyFetchedVarioDepot = true;
					this.VarioDepot = (DepotEntity)entity;
					break;
				case "Language":
					_alreadyFetchedLanguage = true;
					this.Language = (LanguageEntity)entity;
					break;
				case "UserLanguage":
					_alreadyFetchedUserLanguage = true;
					this.UserLanguage = (UserLanguageEntity)entity;
					break;
				case "VarioAddress":
					_alreadyFetchedVarioAddress = true;
					this.VarioAddress = (VarioAddressEntity)entity;
					break;
				case "AccountEntries":
					_alreadyFetchedAccountEntries = true;
					if(entity!=null)
					{
						this.AccountEntries.Add((AccountEntryEntity)entity);
					}
					break;
				case "UserIsMembers":
					_alreadyFetchedUserIsMembers = true;
					if(entity!=null)
					{
						this.UserIsMembers.Add((UserIsMemberEntity)entity);
					}
					break;
				case "UsersToWorkstations":
					_alreadyFetchedUsersToWorkstations = true;
					if(entity!=null)
					{
						this.UsersToWorkstations.Add((UserToWorkstationEntity)entity);
					}
					break;
				case "Settlements":
					_alreadyFetchedSettlements = true;
					if(entity!=null)
					{
						this.Settlements.Add((VarioSettlementEntity)entity);
					}
					break;
				case "ContractTerminations":
					_alreadyFetchedContractTerminations = true;
					if(entity!=null)
					{
						this.ContractTerminations.Add((ContractTerminationEntity)entity);
					}
					break;
				case "FilterValueSets":
					_alreadyFetchedFilterValueSets = true;
					if(entity!=null)
					{
						this.FilterValueSets.Add((FilterValueSetEntity)entity);
					}
					break;
				case "FilterValueSets_":
					_alreadyFetchedFilterValueSets_ = true;
					if(entity!=null)
					{
						this.FilterValueSets_.Add((FilterValueSetEntity)entity);
					}
					break;
				case "Invoices":
					_alreadyFetchedInvoices = true;
					if(entity!=null)
					{
						this.Invoices.Add((InvoiceEntity)entity);
					}
					break;
				case "Invoicings":
					_alreadyFetchedInvoicings = true;
					if(entity!=null)
					{
						this.Invoicings.Add((InvoicingEntity)entity);
					}
					break;
				case "Orders":
					_alreadyFetchedOrders = true;
					if(entity!=null)
					{
						this.Orders.Add((OrderEntity)entity);
					}
					break;
				case "Postings":
					_alreadyFetchedPostings = true;
					if(entity!=null)
					{
						this.Postings.Add((PostingEntity)entity);
					}
					break;
				case "ProductTerminations":
					_alreadyFetchedProductTerminations = true;
					if(entity!=null)
					{
						this.ProductTerminations.Add((ProductTerminationEntity)entity);
					}
					break;
				case "ReportJobs":
					_alreadyFetchedReportJobs = true;
					if(entity!=null)
					{
						this.ReportJobs.Add((ReportJobEntity)entity);
					}
					break;
				case "WorkItems":
					_alreadyFetchedWorkItems = true;
					if(entity!=null)
					{
						this.WorkItems.Add((WorkItemEntity)entity);
					}
					break;
				case "Workstations":
					_alreadyFetchedWorkstations = true;
					if(entity!=null)
					{
						this.Workstations.Add((WorkstationEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "Debtor":
					SetupSyncDebtor(relatedEntity);
					break;
				case "VarioDepot":
					SetupSyncVarioDepot(relatedEntity);
					break;
				case "Language":
					SetupSyncLanguage(relatedEntity);
					break;
				case "UserLanguage":
					SetupSyncUserLanguage(relatedEntity);
					break;
				case "VarioAddress":
					SetupSyncVarioAddress(relatedEntity);
					break;
				case "AccountEntries":
					_accountEntries.Add((AccountEntryEntity)relatedEntity);
					break;
				case "UserIsMembers":
					_userIsMembers.Add((UserIsMemberEntity)relatedEntity);
					break;
				case "UsersToWorkstations":
					_usersToWorkstations.Add((UserToWorkstationEntity)relatedEntity);
					break;
				case "Settlements":
					_settlements.Add((VarioSettlementEntity)relatedEntity);
					break;
				case "ContractTerminations":
					_contractTerminations.Add((ContractTerminationEntity)relatedEntity);
					break;
				case "FilterValueSets":
					_filterValueSets.Add((FilterValueSetEntity)relatedEntity);
					break;
				case "FilterValueSets_":
					_filterValueSets_.Add((FilterValueSetEntity)relatedEntity);
					break;
				case "Invoices":
					_invoices.Add((InvoiceEntity)relatedEntity);
					break;
				case "Invoicings":
					_invoicings.Add((InvoicingEntity)relatedEntity);
					break;
				case "Orders":
					_orders.Add((OrderEntity)relatedEntity);
					break;
				case "Postings":
					_postings.Add((PostingEntity)relatedEntity);
					break;
				case "ProductTerminations":
					_productTerminations.Add((ProductTerminationEntity)relatedEntity);
					break;
				case "ReportJobs":
					_reportJobs.Add((ReportJobEntity)relatedEntity);
					break;
				case "WorkItems":
					_workItems.Add((WorkItemEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "Debtor":
					DesetupSyncDebtor(false, true);
					break;
				case "VarioDepot":
					DesetupSyncVarioDepot(false, true);
					break;
				case "Language":
					DesetupSyncLanguage(false, true);
					break;
				case "UserLanguage":
					DesetupSyncUserLanguage(false, true);
					break;
				case "VarioAddress":
					DesetupSyncVarioAddress(false, true);
					break;
				case "AccountEntries":
					this.PerformRelatedEntityRemoval(_accountEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UserIsMembers":
					this.PerformRelatedEntityRemoval(_userIsMembers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UsersToWorkstations":
					this.PerformRelatedEntityRemoval(_usersToWorkstations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Settlements":
					this.PerformRelatedEntityRemoval(_settlements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ContractTerminations":
					this.PerformRelatedEntityRemoval(_contractTerminations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FilterValueSets":
					this.PerformRelatedEntityRemoval(_filterValueSets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FilterValueSets_":
					this.PerformRelatedEntityRemoval(_filterValueSets_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Invoices":
					this.PerformRelatedEntityRemoval(_invoices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Invoicings":
					this.PerformRelatedEntityRemoval(_invoicings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Orders":
					this.PerformRelatedEntityRemoval(_orders, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Postings":
					this.PerformRelatedEntityRemoval(_postings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductTerminations":
					this.PerformRelatedEntityRemoval(_productTerminations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReportJobs":
					this.PerformRelatedEntityRemoval(_reportJobs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "WorkItems":
					this.PerformRelatedEntityRemoval(_workItems, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_debtor!=null)
			{
				toReturn.Add(_debtor);
			}
			if(_varioDepot!=null)
			{
				toReturn.Add(_varioDepot);
			}
			if(_language!=null)
			{
				toReturn.Add(_language);
			}
			if(_userLanguage!=null)
			{
				toReturn.Add(_userLanguage);
			}
			if(_varioAddress!=null)
			{
				toReturn.Add(_varioAddress);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_accountEntries);
			toReturn.Add(_userIsMembers);
			toReturn.Add(_usersToWorkstations);
			toReturn.Add(_settlements);
			toReturn.Add(_contractTerminations);
			toReturn.Add(_filterValueSets);
			toReturn.Add(_filterValueSets_);
			toReturn.Add(_invoices);
			toReturn.Add(_invoicings);
			toReturn.Add(_orders);
			toReturn.Add(_postings);
			toReturn.Add(_productTerminations);
			toReturn.Add(_reportJobs);
			toReturn.Add(_workItems);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userID">PK value for UserList which data should be fetched into this UserList object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 userID)
		{
			return FetchUsingPK(userID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userID">PK value for UserList which data should be fetched into this UserList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 userID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(userID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userID">PK value for UserList which data should be fetched into this UserList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 userID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(userID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userID">PK value for UserList which data should be fetched into this UserList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 userID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(userID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UserID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UserListRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AccountEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AccountEntryCollection GetMultiAccountEntries(bool forceFetch)
		{
			return GetMultiAccountEntries(forceFetch, _accountEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AccountEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AccountEntryCollection GetMultiAccountEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAccountEntries(forceFetch, _accountEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AccountEntryCollection GetMultiAccountEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAccountEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AccountEntryCollection GetMultiAccountEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAccountEntries || forceFetch || _alwaysFetchAccountEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_accountEntries);
				_accountEntries.SuppressClearInGetMulti=!forceFetch;
				_accountEntries.EntityFactoryToUse = entityFactoryToUse;
				_accountEntries.GetMultiManyToOne(null, null, this, filter);
				_accountEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedAccountEntries = true;
			}
			return _accountEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'AccountEntries'. These settings will be taken into account
		/// when the property AccountEntries is requested or GetMultiAccountEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAccountEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_accountEntries.SortClauses=sortClauses;
			_accountEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserIsMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserIsMemberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserIsMemberCollection GetMultiUserIsMembers(bool forceFetch)
		{
			return GetMultiUserIsMembers(forceFetch, _userIsMembers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserIsMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserIsMemberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserIsMemberCollection GetMultiUserIsMembers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserIsMembers(forceFetch, _userIsMembers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserIsMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UserIsMemberCollection GetMultiUserIsMembers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserIsMembers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserIsMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UserIsMemberCollection GetMultiUserIsMembers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserIsMembers || forceFetch || _alwaysFetchUserIsMembers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userIsMembers);
				_userIsMembers.SuppressClearInGetMulti=!forceFetch;
				_userIsMembers.EntityFactoryToUse = entityFactoryToUse;
				_userIsMembers.GetMultiManyToOne(null, this, filter);
				_userIsMembers.SuppressClearInGetMulti=false;
				_alreadyFetchedUserIsMembers = true;
			}
			return _userIsMembers;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserIsMembers'. These settings will be taken into account
		/// when the property UserIsMembers is requested or GetMultiUserIsMembers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserIsMembers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userIsMembers.SortClauses=sortClauses;
			_userIsMembers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserToWorkstationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserToWorkstationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserToWorkstationCollection GetMultiUsersToWorkstations(bool forceFetch)
		{
			return GetMultiUsersToWorkstations(forceFetch, _usersToWorkstations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserToWorkstationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserToWorkstationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserToWorkstationCollection GetMultiUsersToWorkstations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUsersToWorkstations(forceFetch, _usersToWorkstations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserToWorkstationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UserToWorkstationCollection GetMultiUsersToWorkstations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUsersToWorkstations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserToWorkstationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UserToWorkstationCollection GetMultiUsersToWorkstations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUsersToWorkstations || forceFetch || _alwaysFetchUsersToWorkstations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_usersToWorkstations);
				_usersToWorkstations.SuppressClearInGetMulti=!forceFetch;
				_usersToWorkstations.EntityFactoryToUse = entityFactoryToUse;
				_usersToWorkstations.GetMultiManyToOne(this, null, filter);
				_usersToWorkstations.SuppressClearInGetMulti=false;
				_alreadyFetchedUsersToWorkstations = true;
			}
			return _usersToWorkstations;
		}

		/// <summary> Sets the collection parameters for the collection for 'UsersToWorkstations'. These settings will be taken into account
		/// when the property UsersToWorkstations is requested or GetMultiUsersToWorkstations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUsersToWorkstations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_usersToWorkstations.SortClauses=sortClauses;
			_usersToWorkstations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VarioSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiSettlements(bool forceFetch)
		{
			return GetMultiSettlements(forceFetch, _settlements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VarioSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiSettlements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlements(forceFetch, _settlements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiSettlements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VarioSettlementCollection GetMultiSettlements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlements || forceFetch || _alwaysFetchSettlements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlements);
				_settlements.SuppressClearInGetMulti=!forceFetch;
				_settlements.EntityFactoryToUse = entityFactoryToUse;
				_settlements.GetMultiManyToOne(null, null, this, filter);
				_settlements.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlements = true;
			}
			return _settlements;
		}

		/// <summary> Sets the collection parameters for the collection for 'Settlements'. These settings will be taken into account
		/// when the property Settlements is requested or GetMultiSettlements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlements.SortClauses=sortClauses;
			_settlements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractTerminationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractTerminationCollection GetMultiContractTerminations(bool forceFetch)
		{
			return GetMultiContractTerminations(forceFetch, _contractTerminations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractTerminationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractTerminationCollection GetMultiContractTerminations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContractTerminations(forceFetch, _contractTerminations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractTerminationCollection GetMultiContractTerminations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContractTerminations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractTerminationCollection GetMultiContractTerminations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContractTerminations || forceFetch || _alwaysFetchContractTerminations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contractTerminations);
				_contractTerminations.SuppressClearInGetMulti=!forceFetch;
				_contractTerminations.EntityFactoryToUse = entityFactoryToUse;
				_contractTerminations.GetMultiManyToOne(this, null, null, filter);
				_contractTerminations.SuppressClearInGetMulti=false;
				_alreadyFetchedContractTerminations = true;
			}
			return _contractTerminations;
		}

		/// <summary> Sets the collection parameters for the collection for 'ContractTerminations'. These settings will be taken into account
		/// when the property ContractTerminations is requested or GetMultiContractTerminations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContractTerminations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contractTerminations.SortClauses=sortClauses;
			_contractTerminations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FilterValueSetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets(bool forceFetch)
		{
			return GetMultiFilterValueSets(forceFetch, _filterValueSets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FilterValueSetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFilterValueSets(forceFetch, _filterValueSets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFilterValueSets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFilterValueSets || forceFetch || _alwaysFetchFilterValueSets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_filterValueSets);
				_filterValueSets.SuppressClearInGetMulti=!forceFetch;
				_filterValueSets.EntityFactoryToUse = entityFactoryToUse;
				_filterValueSets.GetMultiManyToOne(null, this, null, null, filter);
				_filterValueSets.SuppressClearInGetMulti=false;
				_alreadyFetchedFilterValueSets = true;
			}
			return _filterValueSets;
		}

		/// <summary> Sets the collection parameters for the collection for 'FilterValueSets'. These settings will be taken into account
		/// when the property FilterValueSets is requested or GetMultiFilterValueSets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFilterValueSets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_filterValueSets.SortClauses=sortClauses;
			_filterValueSets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FilterValueSetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets_(bool forceFetch)
		{
			return GetMultiFilterValueSets_(forceFetch, _filterValueSets_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FilterValueSetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFilterValueSets_(forceFetch, _filterValueSets_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFilterValueSets_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFilterValueSets_ || forceFetch || _alwaysFetchFilterValueSets_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_filterValueSets_);
				_filterValueSets_.SuppressClearInGetMulti=!forceFetch;
				_filterValueSets_.EntityFactoryToUse = entityFactoryToUse;
				_filterValueSets_.GetMultiManyToOne(null, null, this, null, filter);
				_filterValueSets_.SuppressClearInGetMulti=false;
				_alreadyFetchedFilterValueSets_ = true;
			}
			return _filterValueSets_;
		}

		/// <summary> Sets the collection parameters for the collection for 'FilterValueSets_'. These settings will be taken into account
		/// when the property FilterValueSets_ is requested or GetMultiFilterValueSets_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFilterValueSets_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_filterValueSets_.SortClauses=sortClauses;
			_filterValueSets_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch)
		{
			return GetMultiInvoices(forceFetch, _invoices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiInvoices(forceFetch, _invoices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiInvoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedInvoices || forceFetch || _alwaysFetchInvoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_invoices);
				_invoices.SuppressClearInGetMulti=!forceFetch;
				_invoices.EntityFactoryToUse = entityFactoryToUse;
				_invoices.GetMultiManyToOne(null, this, null, null, null, null, filter);
				_invoices.SuppressClearInGetMulti=false;
				_alreadyFetchedInvoices = true;
			}
			return _invoices;
		}

		/// <summary> Sets the collection parameters for the collection for 'Invoices'. These settings will be taken into account
		/// when the property Invoices is requested or GetMultiInvoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersInvoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_invoices.SortClauses=sortClauses;
			_invoices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'InvoicingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'InvoicingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoicingCollection GetMultiInvoicings(bool forceFetch)
		{
			return GetMultiInvoicings(forceFetch, _invoicings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoicingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'InvoicingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoicingCollection GetMultiInvoicings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiInvoicings(forceFetch, _invoicings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'InvoicingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.InvoicingCollection GetMultiInvoicings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiInvoicings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoicingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.InvoicingCollection GetMultiInvoicings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedInvoicings || forceFetch || _alwaysFetchInvoicings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_invoicings);
				_invoicings.SuppressClearInGetMulti=!forceFetch;
				_invoicings.EntityFactoryToUse = entityFactoryToUse;
				_invoicings.GetMultiManyToOne(null, this, filter);
				_invoicings.SuppressClearInGetMulti=false;
				_alreadyFetchedInvoicings = true;
			}
			return _invoicings;
		}

		/// <summary> Sets the collection parameters for the collection for 'Invoicings'. These settings will be taken into account
		/// when the property Invoicings is requested or GetMultiInvoicings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersInvoicings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_invoicings.SortClauses=sortClauses;
			_invoicings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch)
		{
			return GetMultiOrders(forceFetch, _orders.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrders(forceFetch, _orders.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrders(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrders || forceFetch || _alwaysFetchOrders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orders);
				_orders.SuppressClearInGetMulti=!forceFetch;
				_orders.EntityFactoryToUse = entityFactoryToUse;
				_orders.GetMultiManyToOne(this, null, null, null, null, null, filter);
				_orders.SuppressClearInGetMulti=false;
				_alreadyFetchedOrders = true;
			}
			return _orders;
		}

		/// <summary> Sets the collection parameters for the collection for 'Orders'. These settings will be taken into account
		/// when the property Orders is requested or GetMultiOrders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orders.SortClauses=sortClauses;
			_orders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPostings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPostings || forceFetch || _alwaysFetchPostings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_postings);
				_postings.SuppressClearInGetMulti=!forceFetch;
				_postings.EntityFactoryToUse = entityFactoryToUse;
				_postings.GetMultiManyToOne(this, null, null, null, null, null, null, filter);
				_postings.SuppressClearInGetMulti=false;
				_alreadyFetchedPostings = true;
			}
			return _postings;
		}

		/// <summary> Sets the collection parameters for the collection for 'Postings'. These settings will be taken into account
		/// when the property Postings is requested or GetMultiPostings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPostings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_postings.SortClauses=sortClauses;
			_postings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductTerminationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch)
		{
			return GetMultiProductTerminations(forceFetch, _productTerminations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductTerminationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductTerminations(forceFetch, _productTerminations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductTerminations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductTerminations || forceFetch || _alwaysFetchProductTerminations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productTerminations);
				_productTerminations.SuppressClearInGetMulti=!forceFetch;
				_productTerminations.EntityFactoryToUse = entityFactoryToUse;
				_productTerminations.GetMultiManyToOne(this, null, null, null, null, filter);
				_productTerminations.SuppressClearInGetMulti=false;
				_alreadyFetchedProductTerminations = true;
			}
			return _productTerminations;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductTerminations'. These settings will be taken into account
		/// when the property ProductTerminations is requested or GetMultiProductTerminations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductTerminations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productTerminations.SortClauses=sortClauses;
			_productTerminations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch)
		{
			return GetMultiReportJobs(forceFetch, _reportJobs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportJobs(forceFetch, _reportJobs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportJobs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportJobs || forceFetch || _alwaysFetchReportJobs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportJobs);
				_reportJobs.SuppressClearInGetMulti=!forceFetch;
				_reportJobs.EntityFactoryToUse = entityFactoryToUse;
				_reportJobs.GetMultiManyToOne(null, this, null, null, filter);
				_reportJobs.SuppressClearInGetMulti=false;
				_alreadyFetchedReportJobs = true;
			}
			return _reportJobs;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportJobs'. These settings will be taken into account
		/// when the property ReportJobs is requested or GetMultiReportJobs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportJobs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportJobs.SortClauses=sortClauses;
			_reportJobs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'WorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'WorkItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.WorkItemCollection GetMultiWorkItems(bool forceFetch)
		{
			return GetMultiWorkItems(forceFetch, _workItems.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'WorkItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.WorkItemCollection GetMultiWorkItems(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiWorkItems(forceFetch, _workItems.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'WorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.WorkItemCollection GetMultiWorkItems(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiWorkItems(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.WorkItemCollection GetMultiWorkItems(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedWorkItems || forceFetch || _alwaysFetchWorkItems) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_workItems);
				_workItems.SuppressClearInGetMulti=!forceFetch;
				_workItems.EntityFactoryToUse = entityFactoryToUse;
				_workItems.GetMultiManyToOne(this, null, filter);
				_workItems.SuppressClearInGetMulti=false;
				_alreadyFetchedWorkItems = true;
			}
			return _workItems;
		}

		/// <summary> Sets the collection parameters for the collection for 'WorkItems'. These settings will be taken into account
		/// when the property WorkItems is requested or GetMultiWorkItems is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWorkItems(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_workItems.SortClauses=sortClauses;
			_workItems.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'WorkstationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'WorkstationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.WorkstationCollection GetMultiWorkstations(bool forceFetch)
		{
			return GetMultiWorkstations(forceFetch, _workstations.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'WorkstationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.WorkstationCollection GetMultiWorkstations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedWorkstations || forceFetch || _alwaysFetchWorkstations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_workstations);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserListFields.UserID, ComparisonOperator.Equal, this.UserID, "UserListEntity__"));
				_workstations.SuppressClearInGetMulti=!forceFetch;
				_workstations.EntityFactoryToUse = entityFactoryToUse;
				_workstations.GetMulti(filter, GetRelationsForField("Workstations"));
				_workstations.SuppressClearInGetMulti=false;
				_alreadyFetchedWorkstations = true;
			}
			return _workstations;
		}

		/// <summary> Sets the collection parameters for the collection for 'Workstations'. These settings will be taken into account
		/// when the property Workstations is requested or GetMultiWorkstations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWorkstations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_workstations.SortClauses=sortClauses;
			_workstations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public DebtorEntity GetSingleDebtor()
		{
			return GetSingleDebtor(false);
		}

		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public virtual DebtorEntity GetSingleDebtor(bool forceFetch)
		{
			if( ( !_alreadyFetchedDebtor || forceFetch || _alwaysFetchDebtor) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DebtorEntityUsingDebtorID);
				DebtorEntity newEntity = new DebtorEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DebtorID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DebtorEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_debtorReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Debtor = newEntity;
				_alreadyFetchedDebtor = fetchResult;
			}
			return _debtor;
		}


		/// <summary> Retrieves the related entity of type 'DepotEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DepotEntity' which is related to this entity.</returns>
		public DepotEntity GetSingleVarioDepot()
		{
			return GetSingleVarioDepot(false);
		}

		/// <summary> Retrieves the related entity of type 'DepotEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DepotEntity' which is related to this entity.</returns>
		public virtual DepotEntity GetSingleVarioDepot(bool forceFetch)
		{
			if( ( !_alreadyFetchedVarioDepot || forceFetch || _alwaysFetchVarioDepot) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DepotEntityUsingDepotID);
				DepotEntity newEntity = new DepotEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DepotID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DepotEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_varioDepotReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VarioDepot = newEntity;
				_alreadyFetchedVarioDepot = fetchResult;
			}
			return _varioDepot;
		}


		/// <summary> Retrieves the related entity of type 'LanguageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LanguageEntity' which is related to this entity.</returns>
		public LanguageEntity GetSingleLanguage()
		{
			return GetSingleLanguage(false);
		}

		/// <summary> Retrieves the related entity of type 'LanguageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LanguageEntity' which is related to this entity.</returns>
		public virtual LanguageEntity GetSingleLanguage(bool forceFetch)
		{
			if( ( !_alreadyFetchedLanguage || forceFetch || _alwaysFetchLanguage) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LanguageEntityUsingLanguageID);
				LanguageEntity newEntity = new LanguageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LanguageID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LanguageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_languageReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Language = newEntity;
				_alreadyFetchedLanguage = fetchResult;
			}
			return _language;
		}


		/// <summary> Retrieves the related entity of type 'UserLanguageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserLanguageEntity' which is related to this entity.</returns>
		public UserLanguageEntity GetSingleUserLanguage()
		{
			return GetSingleUserLanguage(false);
		}

		/// <summary> Retrieves the related entity of type 'UserLanguageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserLanguageEntity' which is related to this entity.</returns>
		public virtual UserLanguageEntity GetSingleUserLanguage(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserLanguage || forceFetch || _alwaysFetchUserLanguage) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserLanguageEntityUsingLanguageID);
				UserLanguageEntity newEntity = new UserLanguageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LanguageID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserLanguageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userLanguageReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserLanguage = newEntity;
				_alreadyFetchedUserLanguage = fetchResult;
			}
			return _userLanguage;
		}


		/// <summary> Retrieves the related entity of type 'VarioAddressEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VarioAddressEntity' which is related to this entity.</returns>
		public VarioAddressEntity GetSingleVarioAddress()
		{
			return GetSingleVarioAddress(false);
		}

		/// <summary> Retrieves the related entity of type 'VarioAddressEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VarioAddressEntity' which is related to this entity.</returns>
		public virtual VarioAddressEntity GetSingleVarioAddress(bool forceFetch)
		{
			if( ( !_alreadyFetchedVarioAddress || forceFetch || _alwaysFetchVarioAddress) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VarioAddressEntityUsingAddressID);
				VarioAddressEntity newEntity = new VarioAddressEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AddressID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (VarioAddressEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_varioAddressReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VarioAddress = newEntity;
				_alreadyFetchedVarioAddress = fetchResult;
			}
			return _varioAddress;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("Debtor", _debtor);
			toReturn.Add("VarioDepot", _varioDepot);
			toReturn.Add("Language", _language);
			toReturn.Add("UserLanguage", _userLanguage);
			toReturn.Add("VarioAddress", _varioAddress);
			toReturn.Add("AccountEntries", _accountEntries);
			toReturn.Add("UserIsMembers", _userIsMembers);
			toReturn.Add("UsersToWorkstations", _usersToWorkstations);
			toReturn.Add("Settlements", _settlements);
			toReturn.Add("ContractTerminations", _contractTerminations);
			toReturn.Add("FilterValueSets", _filterValueSets);
			toReturn.Add("FilterValueSets_", _filterValueSets_);
			toReturn.Add("Invoices", _invoices);
			toReturn.Add("Invoicings", _invoicings);
			toReturn.Add("Orders", _orders);
			toReturn.Add("Postings", _postings);
			toReturn.Add("ProductTerminations", _productTerminations);
			toReturn.Add("ReportJobs", _reportJobs);
			toReturn.Add("WorkItems", _workItems);
			toReturn.Add("Workstations", _workstations);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="userID">PK value for UserList which data should be fetched into this UserList object</param>
		/// <param name="validator">The validator object for this UserListEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 userID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(userID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_accountEntries = new VarioSL.Entities.CollectionClasses.AccountEntryCollection();
			_accountEntries.SetContainingEntityInfo(this, "User");

			_userIsMembers = new VarioSL.Entities.CollectionClasses.UserIsMemberCollection();
			_userIsMembers.SetContainingEntityInfo(this, "UserList");

			_usersToWorkstations = new VarioSL.Entities.CollectionClasses.UserToWorkstationCollection();
			_usersToWorkstations.SetContainingEntityInfo(this, "User");

			_settlements = new VarioSL.Entities.CollectionClasses.VarioSettlementCollection();
			_settlements.SetContainingEntityInfo(this, "User");

			_contractTerminations = new VarioSL.Entities.CollectionClasses.ContractTerminationCollection();
			_contractTerminations.SetContainingEntityInfo(this, "UserList");

			_filterValueSets = new VarioSL.Entities.CollectionClasses.FilterValueSetCollection();
			_filterValueSets.SetContainingEntityInfo(this, "UserList");

			_filterValueSets_ = new VarioSL.Entities.CollectionClasses.FilterValueSetCollection();
			_filterValueSets_.SetContainingEntityInfo(this, "UserList_");

			_invoices = new VarioSL.Entities.CollectionClasses.InvoiceCollection();
			_invoices.SetContainingEntityInfo(this, "UserList");

			_invoicings = new VarioSL.Entities.CollectionClasses.InvoicingCollection();
			_invoicings.SetContainingEntityInfo(this, "UserList");

			_orders = new VarioSL.Entities.CollectionClasses.OrderCollection();
			_orders.SetContainingEntityInfo(this, "AssignedUser");

			_postings = new VarioSL.Entities.CollectionClasses.PostingCollection();
			_postings.SetContainingEntityInfo(this, "UserList");

			_productTerminations = new VarioSL.Entities.CollectionClasses.ProductTerminationCollection();
			_productTerminations.SetContainingEntityInfo(this, "UserList");

			_reportJobs = new VarioSL.Entities.CollectionClasses.ReportJobCollection();
			_reportJobs.SetContainingEntityInfo(this, "UserList");

			_workItems = new VarioSL.Entities.CollectionClasses.WorkItemCollection();
			_workItems.SetContainingEntityInfo(this, "UserList");
			_workstations = new VarioSL.Entities.CollectionClasses.WorkstationCollection();
			_clientReturnsNewIfNotFound = false;
			_debtorReturnsNewIfNotFound = false;
			_varioDepotReturnsNewIfNotFound = false;
			_languageReturnsNewIfNotFound = false;
			_userLanguageReturnsNewIfNotFound = false;
			_varioAddressReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessEmail", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessFaxNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessPhoneNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepotID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Division", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FirstName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LanguageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Password", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PasswordChanged", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Shortname", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserName", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticUserListRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "Users", resetFKFields, new int[] { (int)UserListFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticUserListRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _debtor</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDebtor(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _debtor, new PropertyChangedEventHandler( OnDebtorPropertyChanged ), "Debtor", VarioSL.Entities.RelationClasses.StaticUserListRelations.DebtorEntityUsingDebtorIDStatic, true, signalRelatedEntity, "Users", resetFKFields, new int[] { (int)UserListFieldIndex.DebtorID } );		
			_debtor = null;
		}
		
		/// <summary> setups the sync logic for member _debtor</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDebtor(IEntityCore relatedEntity)
		{
			if(_debtor!=relatedEntity)
			{		
				DesetupSyncDebtor(true, true);
				_debtor = (DebtorEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _debtor, new PropertyChangedEventHandler( OnDebtorPropertyChanged ), "Debtor", VarioSL.Entities.RelationClasses.StaticUserListRelations.DebtorEntityUsingDebtorIDStatic, true, ref _alreadyFetchedDebtor, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDebtorPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _varioDepot</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVarioDepot(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _varioDepot, new PropertyChangedEventHandler( OnVarioDepotPropertyChanged ), "VarioDepot", VarioSL.Entities.RelationClasses.StaticUserListRelations.DepotEntityUsingDepotIDStatic, true, signalRelatedEntity, "UserLists", resetFKFields, new int[] { (int)UserListFieldIndex.DepotID } );		
			_varioDepot = null;
		}
		
		/// <summary> setups the sync logic for member _varioDepot</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVarioDepot(IEntityCore relatedEntity)
		{
			if(_varioDepot!=relatedEntity)
			{		
				DesetupSyncVarioDepot(true, true);
				_varioDepot = (DepotEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _varioDepot, new PropertyChangedEventHandler( OnVarioDepotPropertyChanged ), "VarioDepot", VarioSL.Entities.RelationClasses.StaticUserListRelations.DepotEntityUsingDepotIDStatic, true, ref _alreadyFetchedVarioDepot, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVarioDepotPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _language</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLanguage(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _language, new PropertyChangedEventHandler( OnLanguagePropertyChanged ), "Language", VarioSL.Entities.RelationClasses.StaticUserListRelations.LanguageEntityUsingLanguageIDStatic, true, signalRelatedEntity, "Users", resetFKFields, new int[] { (int)UserListFieldIndex.LanguageID } );		
			_language = null;
		}
		
		/// <summary> setups the sync logic for member _language</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLanguage(IEntityCore relatedEntity)
		{
			if(_language!=relatedEntity)
			{		
				DesetupSyncLanguage(true, true);
				_language = (LanguageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _language, new PropertyChangedEventHandler( OnLanguagePropertyChanged ), "Language", VarioSL.Entities.RelationClasses.StaticUserListRelations.LanguageEntityUsingLanguageIDStatic, true, ref _alreadyFetchedLanguage, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLanguagePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _userLanguage</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserLanguage(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userLanguage, new PropertyChangedEventHandler( OnUserLanguagePropertyChanged ), "UserLanguage", VarioSL.Entities.RelationClasses.StaticUserListRelations.UserLanguageEntityUsingLanguageIDStatic, true, signalRelatedEntity, "UserLists", resetFKFields, new int[] { (int)UserListFieldIndex.LanguageID } );		
			_userLanguage = null;
		}
		
		/// <summary> setups the sync logic for member _userLanguage</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserLanguage(IEntityCore relatedEntity)
		{
			if(_userLanguage!=relatedEntity)
			{		
				DesetupSyncUserLanguage(true, true);
				_userLanguage = (UserLanguageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userLanguage, new PropertyChangedEventHandler( OnUserLanguagePropertyChanged ), "UserLanguage", VarioSL.Entities.RelationClasses.StaticUserListRelations.UserLanguageEntityUsingLanguageIDStatic, true, ref _alreadyFetchedUserLanguage, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserLanguagePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _varioAddress</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVarioAddress(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _varioAddress, new PropertyChangedEventHandler( OnVarioAddressPropertyChanged ), "VarioAddress", VarioSL.Entities.RelationClasses.StaticUserListRelations.VarioAddressEntityUsingAddressIDStatic, true, signalRelatedEntity, "UserLists", resetFKFields, new int[] { (int)UserListFieldIndex.AddressID } );		
			_varioAddress = null;
		}
		
		/// <summary> setups the sync logic for member _varioAddress</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVarioAddress(IEntityCore relatedEntity)
		{
			if(_varioAddress!=relatedEntity)
			{		
				DesetupSyncVarioAddress(true, true);
				_varioAddress = (VarioAddressEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _varioAddress, new PropertyChangedEventHandler( OnVarioAddressPropertyChanged ), "VarioAddress", VarioSL.Entities.RelationClasses.StaticUserListRelations.VarioAddressEntityUsingAddressIDStatic, true, ref _alreadyFetchedVarioAddress, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVarioAddressPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="userID">PK value for UserList which data should be fetched into this UserList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 userID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UserListFieldIndex.UserID].ForcedCurrentValueWrite(userID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUserListDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UserListEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UserListRelations Relations
		{
			get	{ return new UserListRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AccountEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccountEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AccountEntryCollection(), (IEntityRelation)GetRelationsForField("AccountEntries")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.AccountEntryEntity, 0, null, null, null, "AccountEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserIsMember' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserIsMembers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserIsMemberCollection(), (IEntityRelation)GetRelationsForField("UserIsMembers")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.UserIsMemberEntity, 0, null, null, null, "UserIsMembers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserToWorkstation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUsersToWorkstations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserToWorkstationCollection(), (IEntityRelation)GetRelationsForField("UsersToWorkstations")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.UserToWorkstationEntity, 0, null, null, null, "UsersToWorkstations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VarioSettlement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VarioSettlementCollection(), (IEntityRelation)GetRelationsForField("Settlements")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.VarioSettlementEntity, 0, null, null, null, "Settlements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractTermination' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContractTerminations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractTerminationCollection(), (IEntityRelation)GetRelationsForField("ContractTerminations")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.ContractTerminationEntity, 0, null, null, null, "ContractTerminations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterValueSet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterValueSets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterValueSetCollection(), (IEntityRelation)GetRelationsForField("FilterValueSets")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.FilterValueSetEntity, 0, null, null, null, "FilterValueSets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterValueSet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterValueSets_
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterValueSetCollection(), (IEntityRelation)GetRelationsForField("FilterValueSets_")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.FilterValueSetEntity, 0, null, null, null, "FilterValueSets_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoiceCollection(), (IEntityRelation)GetRelationsForField("Invoices")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.InvoiceEntity, 0, null, null, null, "Invoices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoicing' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoicings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoicingCollection(), (IEntityRelation)GetRelationsForField("Invoicings")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.InvoicingEntity, 0, null, null, null, "Invoicings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrders
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("Orders")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.OrderEntity, 0, null, null, null, "Orders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPostings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PostingCollection(), (IEntityRelation)GetRelationsForField("Postings")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.PostingEntity, 0, null, null, null, "Postings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductTermination' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductTerminations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductTerminationCollection(), (IEntityRelation)GetRelationsForField("ProductTerminations")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.ProductTerminationEntity, 0, null, null, null, "ProductTerminations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportJob' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportJobs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportJobCollection(), (IEntityRelation)GetRelationsForField("ReportJobs")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.ReportJobEntity, 0, null, null, null, "ReportJobs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'WorkItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWorkItems
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.WorkItemCollection(), (IEntityRelation)GetRelationsForField("WorkItems")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.WorkItemEntity, 0, null, null, null, "WorkItems", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Workstation'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWorkstations
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.UserToWorkstationEntityUsingUserID;
				intermediateRelation.SetAliases(string.Empty, "UserToWorkstation_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.WorkstationCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.WorkstationEntity, 0, null, null, GetRelationsForField("Workstations"), "Workstations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Debtor'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDebtor
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCollection(), (IEntityRelation)GetRelationsForField("Debtor")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.DebtorEntity, 0, null, null, null, "Debtor", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Depot'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVarioDepot
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DepotCollection(), (IEntityRelation)GetRelationsForField("VarioDepot")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.DepotEntity, 0, null, null, null, "VarioDepot", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Language'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLanguage
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LanguageCollection(), (IEntityRelation)GetRelationsForField("Language")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.LanguageEntity, 0, null, null, null, "Language", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserLanguage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserLanguage
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserLanguageCollection(), (IEntityRelation)GetRelationsForField("UserLanguage")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.UserLanguageEntity, 0, null, null, null, "UserLanguage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VarioAddress'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVarioAddress
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VarioAddressCollection(), (IEntityRelation)GetRelationsForField("VarioAddress")[0], (int)VarioSL.Entities.EntityType.UserListEntity, (int)VarioSL.Entities.EntityType.VarioAddressEntity, 0, null, null, null, "VarioAddress", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AddressID property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."ADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AddressID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UserListFieldIndex.AddressID, false); }
			set	{ SetValue((int)UserListFieldIndex.AddressID, value, true); }
		}

		/// <summary> The BusinessEmail property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."BUSINESS_EMAIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BusinessEmail
		{
			get { return (System.String)GetValue((int)UserListFieldIndex.BusinessEmail, true); }
			set	{ SetValue((int)UserListFieldIndex.BusinessEmail, value, true); }
		}

		/// <summary> The BusinessFaxNo property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."BUSINESS_FAXNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BusinessFaxNo
		{
			get { return (System.String)GetValue((int)UserListFieldIndex.BusinessFaxNo, true); }
			set	{ SetValue((int)UserListFieldIndex.BusinessFaxNo, value, true); }
		}

		/// <summary> The BusinessPhoneNo property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."BUSINESS_PHONENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BusinessPhoneNo
		{
			get { return (System.String)GetValue((int)UserListFieldIndex.BusinessPhoneNo, true); }
			set	{ SetValue((int)UserListFieldIndex.BusinessPhoneNo, value, true); }
		}

		/// <summary> The ClientID property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UserListFieldIndex.ClientID, false); }
			set	{ SetValue((int)UserListFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."USERCREATEDBY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CreatedBy
		{
			get { return (System.String)GetValue((int)UserListFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)UserListFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The CreationDate property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."USERCREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UserListFieldIndex.CreationDate, false); }
			set	{ SetValue((int)UserListFieldIndex.CreationDate, value, true); }
		}

		/// <summary> The CreationTime property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."USERCREATIONTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreationTime
		{
			get { return (Nullable<System.Int32>)GetValue((int)UserListFieldIndex.CreationTime, false); }
			set	{ SetValue((int)UserListFieldIndex.CreationTime, value, true); }
		}

		/// <summary> The DebtorID property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."DEBTORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DebtorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UserListFieldIndex.DebtorID, false); }
			set	{ SetValue((int)UserListFieldIndex.DebtorID, value, true); }
		}

		/// <summary> The DepotID property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."DEPOTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DepotID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UserListFieldIndex.DepotID, false); }
			set	{ SetValue((int)UserListFieldIndex.DepotID, value, true); }
		}

		/// <summary> The Description property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."USERDESCR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)UserListFieldIndex.Description, true); }
			set	{ SetValue((int)UserListFieldIndex.Description, value, true); }
		}

		/// <summary> The Division property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."DIVISION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Division
		{
			get { return (System.String)GetValue((int)UserListFieldIndex.Division, true); }
			set	{ SetValue((int)UserListFieldIndex.Division, value, true); }
		}

		/// <summary> The FirstName property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."USERFIRSTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FirstName
		{
			get { return (System.String)GetValue((int)UserListFieldIndex.FirstName, true); }
			set	{ SetValue((int)UserListFieldIndex.FirstName, value, true); }
		}

		/// <summary> The LanguageID property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."LANGUAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LanguageID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UserListFieldIndex.LanguageID, false); }
			set	{ SetValue((int)UserListFieldIndex.LanguageID, value, true); }
		}

		/// <summary> The Password property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."USERPASSWORD"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 32<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Password
		{
			get { return (System.String)GetValue((int)UserListFieldIndex.Password, true); }
			set	{ SetValue((int)UserListFieldIndex.Password, value, true); }
		}

		/// <summary> The PasswordChanged property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."PASSWORDCHANGED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PasswordChanged
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UserListFieldIndex.PasswordChanged, false); }
			set	{ SetValue((int)UserListFieldIndex.PasswordChanged, value, true); }
		}

		/// <summary> The Shortname property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."USERSHORTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Shortname
		{
			get { return (System.String)GetValue((int)UserListFieldIndex.Shortname, true); }
			set	{ SetValue((int)UserListFieldIndex.Shortname, value, true); }
		}

		/// <summary> The Status property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."STATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.UserStatus Status
		{
			get { return (VarioSL.Entities.Enumerations.UserStatus)GetValue((int)UserListFieldIndex.Status, true); }
			set	{ SetValue((int)UserListFieldIndex.Status, value, true); }
		}

		/// <summary> The UserID property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."USERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 UserID
		{
			get { return (System.Int64)GetValue((int)UserListFieldIndex.UserID, true); }
			set	{ SetValue((int)UserListFieldIndex.UserID, value, true); }
		}

		/// <summary> The UserName property of the Entity UserList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_LIST"."USERNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String UserName
		{
			get { return (System.String)GetValue((int)UserListFieldIndex.UserName, true); }
			set	{ SetValue((int)UserListFieldIndex.UserName, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAccountEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AccountEntryCollection AccountEntries
		{
			get	{ return GetMultiAccountEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AccountEntries. When set to true, AccountEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccountEntries is accessed. You can always execute/ a forced fetch by calling GetMultiAccountEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccountEntries
		{
			get	{ return _alwaysFetchAccountEntries; }
			set	{ _alwaysFetchAccountEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AccountEntries already has been fetched. Setting this property to false when AccountEntries has been fetched
		/// will clear the AccountEntries collection well. Setting this property to true while AccountEntries hasn't been fetched disables lazy loading for AccountEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccountEntries
		{
			get { return _alreadyFetchedAccountEntries;}
			set 
			{
				if(_alreadyFetchedAccountEntries && !value && (_accountEntries != null))
				{
					_accountEntries.Clear();
				}
				_alreadyFetchedAccountEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserIsMemberEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserIsMembers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UserIsMemberCollection UserIsMembers
		{
			get	{ return GetMultiUserIsMembers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserIsMembers. When set to true, UserIsMembers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserIsMembers is accessed. You can always execute/ a forced fetch by calling GetMultiUserIsMembers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserIsMembers
		{
			get	{ return _alwaysFetchUserIsMembers; }
			set	{ _alwaysFetchUserIsMembers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserIsMembers already has been fetched. Setting this property to false when UserIsMembers has been fetched
		/// will clear the UserIsMembers collection well. Setting this property to true while UserIsMembers hasn't been fetched disables lazy loading for UserIsMembers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserIsMembers
		{
			get { return _alreadyFetchedUserIsMembers;}
			set 
			{
				if(_alreadyFetchedUserIsMembers && !value && (_userIsMembers != null))
				{
					_userIsMembers.Clear();
				}
				_alreadyFetchedUserIsMembers = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserToWorkstationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUsersToWorkstations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UserToWorkstationCollection UsersToWorkstations
		{
			get	{ return GetMultiUsersToWorkstations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UsersToWorkstations. When set to true, UsersToWorkstations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UsersToWorkstations is accessed. You can always execute/ a forced fetch by calling GetMultiUsersToWorkstations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUsersToWorkstations
		{
			get	{ return _alwaysFetchUsersToWorkstations; }
			set	{ _alwaysFetchUsersToWorkstations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UsersToWorkstations already has been fetched. Setting this property to false when UsersToWorkstations has been fetched
		/// will clear the UsersToWorkstations collection well. Setting this property to true while UsersToWorkstations hasn't been fetched disables lazy loading for UsersToWorkstations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUsersToWorkstations
		{
			get { return _alreadyFetchedUsersToWorkstations;}
			set 
			{
				if(_alreadyFetchedUsersToWorkstations && !value && (_usersToWorkstations != null))
				{
					_usersToWorkstations.Clear();
				}
				_alreadyFetchedUsersToWorkstations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VarioSettlementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VarioSettlementCollection Settlements
		{
			get	{ return GetMultiSettlements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Settlements. When set to true, Settlements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Settlements is accessed. You can always execute/ a forced fetch by calling GetMultiSettlements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlements
		{
			get	{ return _alwaysFetchSettlements; }
			set	{ _alwaysFetchSettlements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Settlements already has been fetched. Setting this property to false when Settlements has been fetched
		/// will clear the Settlements collection well. Setting this property to true while Settlements hasn't been fetched disables lazy loading for Settlements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlements
		{
			get { return _alreadyFetchedSettlements;}
			set 
			{
				if(_alreadyFetchedSettlements && !value && (_settlements != null))
				{
					_settlements.Clear();
				}
				_alreadyFetchedSettlements = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractTerminationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContractTerminations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractTerminationCollection ContractTerminations
		{
			get	{ return GetMultiContractTerminations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ContractTerminations. When set to true, ContractTerminations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContractTerminations is accessed. You can always execute/ a forced fetch by calling GetMultiContractTerminations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContractTerminations
		{
			get	{ return _alwaysFetchContractTerminations; }
			set	{ _alwaysFetchContractTerminations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ContractTerminations already has been fetched. Setting this property to false when ContractTerminations has been fetched
		/// will clear the ContractTerminations collection well. Setting this property to true while ContractTerminations hasn't been fetched disables lazy loading for ContractTerminations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContractTerminations
		{
			get { return _alreadyFetchedContractTerminations;}
			set 
			{
				if(_alreadyFetchedContractTerminations && !value && (_contractTerminations != null))
				{
					_contractTerminations.Clear();
				}
				_alreadyFetchedContractTerminations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFilterValueSets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FilterValueSetCollection FilterValueSets
		{
			get	{ return GetMultiFilterValueSets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FilterValueSets. When set to true, FilterValueSets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterValueSets is accessed. You can always execute/ a forced fetch by calling GetMultiFilterValueSets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterValueSets
		{
			get	{ return _alwaysFetchFilterValueSets; }
			set	{ _alwaysFetchFilterValueSets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterValueSets already has been fetched. Setting this property to false when FilterValueSets has been fetched
		/// will clear the FilterValueSets collection well. Setting this property to true while FilterValueSets hasn't been fetched disables lazy loading for FilterValueSets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterValueSets
		{
			get { return _alreadyFetchedFilterValueSets;}
			set 
			{
				if(_alreadyFetchedFilterValueSets && !value && (_filterValueSets != null))
				{
					_filterValueSets.Clear();
				}
				_alreadyFetchedFilterValueSets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFilterValueSets_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FilterValueSetCollection FilterValueSets_
		{
			get	{ return GetMultiFilterValueSets_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FilterValueSets_. When set to true, FilterValueSets_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterValueSets_ is accessed. You can always execute/ a forced fetch by calling GetMultiFilterValueSets_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterValueSets_
		{
			get	{ return _alwaysFetchFilterValueSets_; }
			set	{ _alwaysFetchFilterValueSets_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterValueSets_ already has been fetched. Setting this property to false when FilterValueSets_ has been fetched
		/// will clear the FilterValueSets_ collection well. Setting this property to true while FilterValueSets_ hasn't been fetched disables lazy loading for FilterValueSets_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterValueSets_
		{
			get { return _alreadyFetchedFilterValueSets_;}
			set 
			{
				if(_alreadyFetchedFilterValueSets_ && !value && (_filterValueSets_ != null))
				{
					_filterValueSets_.Clear();
				}
				_alreadyFetchedFilterValueSets_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiInvoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceCollection Invoices
		{
			get	{ return GetMultiInvoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Invoices. When set to true, Invoices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoices is accessed. You can always execute/ a forced fetch by calling GetMultiInvoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoices
		{
			get	{ return _alwaysFetchInvoices; }
			set	{ _alwaysFetchInvoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoices already has been fetched. Setting this property to false when Invoices has been fetched
		/// will clear the Invoices collection well. Setting this property to true while Invoices hasn't been fetched disables lazy loading for Invoices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoices
		{
			get { return _alreadyFetchedInvoices;}
			set 
			{
				if(_alreadyFetchedInvoices && !value && (_invoices != null))
				{
					_invoices.Clear();
				}
				_alreadyFetchedInvoices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'InvoicingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiInvoicings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.InvoicingCollection Invoicings
		{
			get	{ return GetMultiInvoicings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Invoicings. When set to true, Invoicings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoicings is accessed. You can always execute/ a forced fetch by calling GetMultiInvoicings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoicings
		{
			get	{ return _alwaysFetchInvoicings; }
			set	{ _alwaysFetchInvoicings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoicings already has been fetched. Setting this property to false when Invoicings has been fetched
		/// will clear the Invoicings collection well. Setting this property to true while Invoicings hasn't been fetched disables lazy loading for Invoicings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoicings
		{
			get { return _alreadyFetchedInvoicings;}
			set 
			{
				if(_alreadyFetchedInvoicings && !value && (_invoicings != null))
				{
					_invoicings.Clear();
				}
				_alreadyFetchedInvoicings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderCollection Orders
		{
			get	{ return GetMultiOrders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Orders. When set to true, Orders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Orders is accessed. You can always execute/ a forced fetch by calling GetMultiOrders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrders
		{
			get	{ return _alwaysFetchOrders; }
			set	{ _alwaysFetchOrders = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Orders already has been fetched. Setting this property to false when Orders has been fetched
		/// will clear the Orders collection well. Setting this property to true while Orders hasn't been fetched disables lazy loading for Orders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrders
		{
			get { return _alreadyFetchedOrders;}
			set 
			{
				if(_alreadyFetchedOrders && !value && (_orders != null))
				{
					_orders.Clear();
				}
				_alreadyFetchedOrders = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPostings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection Postings
		{
			get	{ return GetMultiPostings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Postings. When set to true, Postings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Postings is accessed. You can always execute/ a forced fetch by calling GetMultiPostings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPostings
		{
			get	{ return _alwaysFetchPostings; }
			set	{ _alwaysFetchPostings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Postings already has been fetched. Setting this property to false when Postings has been fetched
		/// will clear the Postings collection well. Setting this property to true while Postings hasn't been fetched disables lazy loading for Postings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPostings
		{
			get { return _alreadyFetchedPostings;}
			set 
			{
				if(_alreadyFetchedPostings && !value && (_postings != null))
				{
					_postings.Clear();
				}
				_alreadyFetchedPostings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductTerminations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductTerminationCollection ProductTerminations
		{
			get	{ return GetMultiProductTerminations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductTerminations. When set to true, ProductTerminations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductTerminations is accessed. You can always execute/ a forced fetch by calling GetMultiProductTerminations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductTerminations
		{
			get	{ return _alwaysFetchProductTerminations; }
			set	{ _alwaysFetchProductTerminations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductTerminations already has been fetched. Setting this property to false when ProductTerminations has been fetched
		/// will clear the ProductTerminations collection well. Setting this property to true while ProductTerminations hasn't been fetched disables lazy loading for ProductTerminations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductTerminations
		{
			get { return _alreadyFetchedProductTerminations;}
			set 
			{
				if(_alreadyFetchedProductTerminations && !value && (_productTerminations != null))
				{
					_productTerminations.Clear();
				}
				_alreadyFetchedProductTerminations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportJobs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobCollection ReportJobs
		{
			get	{ return GetMultiReportJobs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportJobs. When set to true, ReportJobs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportJobs is accessed. You can always execute/ a forced fetch by calling GetMultiReportJobs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportJobs
		{
			get	{ return _alwaysFetchReportJobs; }
			set	{ _alwaysFetchReportJobs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportJobs already has been fetched. Setting this property to false when ReportJobs has been fetched
		/// will clear the ReportJobs collection well. Setting this property to true while ReportJobs hasn't been fetched disables lazy loading for ReportJobs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportJobs
		{
			get { return _alreadyFetchedReportJobs;}
			set 
			{
				if(_alreadyFetchedReportJobs && !value && (_reportJobs != null))
				{
					_reportJobs.Clear();
				}
				_alreadyFetchedReportJobs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'WorkItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWorkItems()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.WorkItemCollection WorkItems
		{
			get	{ return GetMultiWorkItems(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for WorkItems. When set to true, WorkItems is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WorkItems is accessed. You can always execute/ a forced fetch by calling GetMultiWorkItems(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWorkItems
		{
			get	{ return _alwaysFetchWorkItems; }
			set	{ _alwaysFetchWorkItems = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property WorkItems already has been fetched. Setting this property to false when WorkItems has been fetched
		/// will clear the WorkItems collection well. Setting this property to true while WorkItems hasn't been fetched disables lazy loading for WorkItems</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWorkItems
		{
			get { return _alreadyFetchedWorkItems;}
			set 
			{
				if(_alreadyFetchedWorkItems && !value && (_workItems != null))
				{
					_workItems.Clear();
				}
				_alreadyFetchedWorkItems = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'WorkstationEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWorkstations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.WorkstationCollection Workstations
		{
			get { return GetMultiWorkstations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Workstations. When set to true, Workstations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Workstations is accessed. You can always execute a forced fetch by calling GetMultiWorkstations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWorkstations
		{
			get	{ return _alwaysFetchWorkstations; }
			set	{ _alwaysFetchWorkstations = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Workstations already has been fetched. Setting this property to false when Workstations has been fetched
		/// will clear the Workstations collection well. Setting this property to true while Workstations hasn't been fetched disables lazy loading for Workstations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWorkstations
		{
			get { return _alreadyFetchedWorkstations;}
			set 
			{
				if(_alreadyFetchedWorkstations && !value && (_workstations != null))
				{
					_workstations.Clear();
				}
				_alreadyFetchedWorkstations = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Users", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DebtorEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDebtor()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DebtorEntity Debtor
		{
			get	{ return GetSingleDebtor(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDebtor(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Users", "Debtor", _debtor, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Debtor. When set to true, Debtor is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Debtor is accessed. You can always execute a forced fetch by calling GetSingleDebtor(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDebtor
		{
			get	{ return _alwaysFetchDebtor; }
			set	{ _alwaysFetchDebtor = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Debtor already has been fetched. Setting this property to false when Debtor has been fetched
		/// will set Debtor to null as well. Setting this property to true while Debtor hasn't been fetched disables lazy loading for Debtor</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDebtor
		{
			get { return _alreadyFetchedDebtor;}
			set 
			{
				if(_alreadyFetchedDebtor && !value)
				{
					this.Debtor = null;
				}
				_alreadyFetchedDebtor = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Debtor is not found
		/// in the database. When set to true, Debtor will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DebtorReturnsNewIfNotFound
		{
			get	{ return _debtorReturnsNewIfNotFound; }
			set { _debtorReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DepotEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVarioDepot()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DepotEntity VarioDepot
		{
			get	{ return GetSingleVarioDepot(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVarioDepot(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UserLists", "VarioDepot", _varioDepot, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VarioDepot. When set to true, VarioDepot is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VarioDepot is accessed. You can always execute a forced fetch by calling GetSingleVarioDepot(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVarioDepot
		{
			get	{ return _alwaysFetchVarioDepot; }
			set	{ _alwaysFetchVarioDepot = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VarioDepot already has been fetched. Setting this property to false when VarioDepot has been fetched
		/// will set VarioDepot to null as well. Setting this property to true while VarioDepot hasn't been fetched disables lazy loading for VarioDepot</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVarioDepot
		{
			get { return _alreadyFetchedVarioDepot;}
			set 
			{
				if(_alreadyFetchedVarioDepot && !value)
				{
					this.VarioDepot = null;
				}
				_alreadyFetchedVarioDepot = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VarioDepot is not found
		/// in the database. When set to true, VarioDepot will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool VarioDepotReturnsNewIfNotFound
		{
			get	{ return _varioDepotReturnsNewIfNotFound; }
			set { _varioDepotReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LanguageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLanguage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LanguageEntity Language
		{
			get	{ return GetSingleLanguage(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLanguage(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Users", "Language", _language, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Language. When set to true, Language is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Language is accessed. You can always execute a forced fetch by calling GetSingleLanguage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLanguage
		{
			get	{ return _alwaysFetchLanguage; }
			set	{ _alwaysFetchLanguage = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Language already has been fetched. Setting this property to false when Language has been fetched
		/// will set Language to null as well. Setting this property to true while Language hasn't been fetched disables lazy loading for Language</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLanguage
		{
			get { return _alreadyFetchedLanguage;}
			set 
			{
				if(_alreadyFetchedLanguage && !value)
				{
					this.Language = null;
				}
				_alreadyFetchedLanguage = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Language is not found
		/// in the database. When set to true, Language will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool LanguageReturnsNewIfNotFound
		{
			get	{ return _languageReturnsNewIfNotFound; }
			set { _languageReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserLanguageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserLanguage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserLanguageEntity UserLanguage
		{
			get	{ return GetSingleUserLanguage(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserLanguage(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UserLists", "UserLanguage", _userLanguage, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserLanguage. When set to true, UserLanguage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserLanguage is accessed. You can always execute a forced fetch by calling GetSingleUserLanguage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserLanguage
		{
			get	{ return _alwaysFetchUserLanguage; }
			set	{ _alwaysFetchUserLanguage = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserLanguage already has been fetched. Setting this property to false when UserLanguage has been fetched
		/// will set UserLanguage to null as well. Setting this property to true while UserLanguage hasn't been fetched disables lazy loading for UserLanguage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserLanguage
		{
			get { return _alreadyFetchedUserLanguage;}
			set 
			{
				if(_alreadyFetchedUserLanguage && !value)
				{
					this.UserLanguage = null;
				}
				_alreadyFetchedUserLanguage = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserLanguage is not found
		/// in the database. When set to true, UserLanguage will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserLanguageReturnsNewIfNotFound
		{
			get	{ return _userLanguageReturnsNewIfNotFound; }
			set { _userLanguageReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VarioAddressEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVarioAddress()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VarioAddressEntity VarioAddress
		{
			get	{ return GetSingleVarioAddress(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVarioAddress(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UserLists", "VarioAddress", _varioAddress, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VarioAddress. When set to true, VarioAddress is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VarioAddress is accessed. You can always execute a forced fetch by calling GetSingleVarioAddress(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVarioAddress
		{
			get	{ return _alwaysFetchVarioAddress; }
			set	{ _alwaysFetchVarioAddress = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VarioAddress already has been fetched. Setting this property to false when VarioAddress has been fetched
		/// will set VarioAddress to null as well. Setting this property to true while VarioAddress hasn't been fetched disables lazy loading for VarioAddress</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVarioAddress
		{
			get { return _alreadyFetchedVarioAddress;}
			set 
			{
				if(_alreadyFetchedVarioAddress && !value)
				{
					this.VarioAddress = null;
				}
				_alreadyFetchedVarioAddress = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VarioAddress is not found
		/// in the database. When set to true, VarioAddress will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool VarioAddressReturnsNewIfNotFound
		{
			get	{ return _varioAddressReturnsNewIfNotFound; }
			set { _varioAddressReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.UserListEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
