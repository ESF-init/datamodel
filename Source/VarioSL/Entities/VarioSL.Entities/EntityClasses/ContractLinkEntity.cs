﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ContractLink'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ContractLinkEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private ContractEntity _sourceContract;
		private bool	_alwaysFetchSourceContract, _alreadyFetchedSourceContract, _sourceContractReturnsNewIfNotFound;
		private ContractEntity _targetContract;
		private bool	_alwaysFetchTargetContract, _alreadyFetchedTargetContract, _targetContractReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SourceContract</summary>
			public static readonly string SourceContract = "SourceContract";
			/// <summary>Member name TargetContract</summary>
			public static readonly string TargetContract = "TargetContract";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ContractLinkEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ContractLinkEntity() :base("ContractLinkEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="sourceContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="targetContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		public ContractLinkEntity(System.Int64 sourceContractID, System.Int64 targetContractID):base("ContractLinkEntity")
		{
			InitClassFetch(sourceContractID, targetContractID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="sourceContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="targetContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ContractLinkEntity(System.Int64 sourceContractID, System.Int64 targetContractID, IPrefetchPath prefetchPathToUse):base("ContractLinkEntity")
		{
			InitClassFetch(sourceContractID, targetContractID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="sourceContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="targetContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="validator">The custom validator object for this ContractLinkEntity</param>
		public ContractLinkEntity(System.Int64 sourceContractID, System.Int64 targetContractID, IValidator validator):base("ContractLinkEntity")
		{
			InitClassFetch(sourceContractID, targetContractID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ContractLinkEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_sourceContract = (ContractEntity)info.GetValue("_sourceContract", typeof(ContractEntity));
			if(_sourceContract!=null)
			{
				_sourceContract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_sourceContractReturnsNewIfNotFound = info.GetBoolean("_sourceContractReturnsNewIfNotFound");
			_alwaysFetchSourceContract = info.GetBoolean("_alwaysFetchSourceContract");
			_alreadyFetchedSourceContract = info.GetBoolean("_alreadyFetchedSourceContract");

			_targetContract = (ContractEntity)info.GetValue("_targetContract", typeof(ContractEntity));
			if(_targetContract!=null)
			{
				_targetContract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_targetContractReturnsNewIfNotFound = info.GetBoolean("_targetContractReturnsNewIfNotFound");
			_alwaysFetchTargetContract = info.GetBoolean("_alwaysFetchTargetContract");
			_alreadyFetchedTargetContract = info.GetBoolean("_alreadyFetchedTargetContract");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ContractLinkFieldIndex)fieldIndex)
			{
				case ContractLinkFieldIndex.SourceContractID:
					DesetupSyncSourceContract(true, false);
					_alreadyFetchedSourceContract = false;
					break;
				case ContractLinkFieldIndex.TargetContractID:
					DesetupSyncTargetContract(true, false);
					_alreadyFetchedTargetContract = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSourceContract = (_sourceContract != null);
			_alreadyFetchedTargetContract = (_targetContract != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SourceContract":
					toReturn.Add(Relations.ContractEntityUsingSourceContractID);
					break;
				case "TargetContract":
					toReturn.Add(Relations.ContractEntityUsingTargetContractID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_sourceContract", (!this.MarkedForDeletion?_sourceContract:null));
			info.AddValue("_sourceContractReturnsNewIfNotFound", _sourceContractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSourceContract", _alwaysFetchSourceContract);
			info.AddValue("_alreadyFetchedSourceContract", _alreadyFetchedSourceContract);
			info.AddValue("_targetContract", (!this.MarkedForDeletion?_targetContract:null));
			info.AddValue("_targetContractReturnsNewIfNotFound", _targetContractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTargetContract", _alwaysFetchTargetContract);
			info.AddValue("_alreadyFetchedTargetContract", _alreadyFetchedTargetContract);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SourceContract":
					_alreadyFetchedSourceContract = true;
					this.SourceContract = (ContractEntity)entity;
					break;
				case "TargetContract":
					_alreadyFetchedTargetContract = true;
					this.TargetContract = (ContractEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SourceContract":
					SetupSyncSourceContract(relatedEntity);
					break;
				case "TargetContract":
					SetupSyncTargetContract(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SourceContract":
					DesetupSyncSourceContract(false, true);
					break;
				case "TargetContract":
					DesetupSyncTargetContract(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_sourceContract!=null)
			{
				toReturn.Add(_sourceContract);
			}
			if(_targetContract!=null)
			{
				toReturn.Add(_targetContract);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="sourceContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="targetContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 sourceContractID, System.Int64 targetContractID)
		{
			return FetchUsingPK(sourceContractID, targetContractID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="sourceContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="targetContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 sourceContractID, System.Int64 targetContractID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(sourceContractID, targetContractID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="sourceContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="targetContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 sourceContractID, System.Int64 targetContractID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(sourceContractID, targetContractID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="sourceContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="targetContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 sourceContractID, System.Int64 targetContractID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(sourceContractID, targetContractID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SourceContractID, this.TargetContractID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ContractLinkRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleSourceContract()
		{
			return GetSingleSourceContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleSourceContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedSourceContract || forceFetch || _alwaysFetchSourceContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingSourceContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SourceContractID);
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_sourceContractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SourceContract = newEntity;
				_alreadyFetchedSourceContract = fetchResult;
			}
			return _sourceContract;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleTargetContract()
		{
			return GetSingleTargetContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleTargetContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedTargetContract || forceFetch || _alwaysFetchTargetContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingTargetContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TargetContractID);
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_targetContractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TargetContract = newEntity;
				_alreadyFetchedTargetContract = fetchResult;
			}
			return _targetContract;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SourceContract", _sourceContract);
			toReturn.Add("TargetContract", _targetContract);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="sourceContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="targetContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="validator">The validator object for this ContractLinkEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 sourceContractID, System.Int64 targetContractID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(sourceContractID, targetContractID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_sourceContractReturnsNewIfNotFound = false;
			_targetContractReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataRowCreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SourceContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TargetContractID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _sourceContract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSourceContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _sourceContract, new PropertyChangedEventHandler( OnSourceContractPropertyChanged ), "SourceContract", VarioSL.Entities.RelationClasses.StaticContractLinkRelations.ContractEntityUsingSourceContractIDStatic, true, signalRelatedEntity, "TargetContracts", resetFKFields, new int[] { (int)ContractLinkFieldIndex.SourceContractID } );		
			_sourceContract = null;
		}
		
		/// <summary> setups the sync logic for member _sourceContract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSourceContract(IEntityCore relatedEntity)
		{
			if(_sourceContract!=relatedEntity)
			{		
				DesetupSyncSourceContract(true, true);
				_sourceContract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _sourceContract, new PropertyChangedEventHandler( OnSourceContractPropertyChanged ), "SourceContract", VarioSL.Entities.RelationClasses.StaticContractLinkRelations.ContractEntityUsingSourceContractIDStatic, true, ref _alreadyFetchedSourceContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSourceContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _targetContract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTargetContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _targetContract, new PropertyChangedEventHandler( OnTargetContractPropertyChanged ), "TargetContract", VarioSL.Entities.RelationClasses.StaticContractLinkRelations.ContractEntityUsingTargetContractIDStatic, true, signalRelatedEntity, "SourceContracts", resetFKFields, new int[] { (int)ContractLinkFieldIndex.TargetContractID } );		
			_targetContract = null;
		}
		
		/// <summary> setups the sync logic for member _targetContract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTargetContract(IEntityCore relatedEntity)
		{
			if(_targetContract!=relatedEntity)
			{		
				DesetupSyncTargetContract(true, true);
				_targetContract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _targetContract, new PropertyChangedEventHandler( OnTargetContractPropertyChanged ), "TargetContract", VarioSL.Entities.RelationClasses.StaticContractLinkRelations.ContractEntityUsingTargetContractIDStatic, true, ref _alreadyFetchedTargetContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTargetContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="sourceContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="targetContractID">PK value for ContractLink which data should be fetched into this ContractLink object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 sourceContractID, System.Int64 targetContractID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ContractLinkFieldIndex.SourceContractID].ForcedCurrentValueWrite(sourceContractID);
				this.Fields[(int)ContractLinkFieldIndex.TargetContractID].ForcedCurrentValueWrite(targetContractID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateContractLinkDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ContractLinkEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ContractLinkRelations Relations
		{
			get	{ return new ContractLinkRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSourceContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("SourceContract")[0], (int)VarioSL.Entities.EntityType.ContractLinkEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "SourceContract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTargetContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("TargetContract")[0], (int)VarioSL.Entities.EntityType.ContractLinkEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "TargetContract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DataRowCreationDate property of the Entity ContractLink<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACTLINK"."DATAROWCREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataRowCreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ContractLinkFieldIndex.DataRowCreationDate, false); }
			set	{ SetValue((int)ContractLinkFieldIndex.DataRowCreationDate, value, true); }
		}

		/// <summary> The SourceContractID property of the Entity ContractLink<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACTLINK"."SOURCECONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 SourceContractID
		{
			get { return (System.Int64)GetValue((int)ContractLinkFieldIndex.SourceContractID, true); }
			set	{ SetValue((int)ContractLinkFieldIndex.SourceContractID, value, true); }
		}

		/// <summary> The TargetContractID property of the Entity ContractLink<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACTLINK"."TARGETCONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 TargetContractID
		{
			get { return (System.Int64)GetValue((int)ContractLinkFieldIndex.TargetContractID, true); }
			set	{ SetValue((int)ContractLinkFieldIndex.TargetContractID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSourceContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity SourceContract
		{
			get	{ return GetSingleSourceContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSourceContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TargetContracts", "SourceContract", _sourceContract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SourceContract. When set to true, SourceContract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SourceContract is accessed. You can always execute a forced fetch by calling GetSingleSourceContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSourceContract
		{
			get	{ return _alwaysFetchSourceContract; }
			set	{ _alwaysFetchSourceContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SourceContract already has been fetched. Setting this property to false when SourceContract has been fetched
		/// will set SourceContract to null as well. Setting this property to true while SourceContract hasn't been fetched disables lazy loading for SourceContract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSourceContract
		{
			get { return _alreadyFetchedSourceContract;}
			set 
			{
				if(_alreadyFetchedSourceContract && !value)
				{
					this.SourceContract = null;
				}
				_alreadyFetchedSourceContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SourceContract is not found
		/// in the database. When set to true, SourceContract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SourceContractReturnsNewIfNotFound
		{
			get	{ return _sourceContractReturnsNewIfNotFound; }
			set { _sourceContractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTargetContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity TargetContract
		{
			get	{ return GetSingleTargetContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTargetContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SourceContracts", "TargetContract", _targetContract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TargetContract. When set to true, TargetContract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TargetContract is accessed. You can always execute a forced fetch by calling GetSingleTargetContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTargetContract
		{
			get	{ return _alwaysFetchTargetContract; }
			set	{ _alwaysFetchTargetContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TargetContract already has been fetched. Setting this property to false when TargetContract has been fetched
		/// will set TargetContract to null as well. Setting this property to true while TargetContract hasn't been fetched disables lazy loading for TargetContract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTargetContract
		{
			get { return _alreadyFetchedTargetContract;}
			set 
			{
				if(_alreadyFetchedTargetContract && !value)
				{
					this.TargetContract = null;
				}
				_alreadyFetchedTargetContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TargetContract is not found
		/// in the database. When set to true, TargetContract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TargetContractReturnsNewIfNotFound
		{
			get	{ return _targetContractReturnsNewIfNotFound; }
			set { _targetContractReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ContractLinkEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
