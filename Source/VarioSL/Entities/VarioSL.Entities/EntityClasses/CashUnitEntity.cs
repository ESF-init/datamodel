﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CashUnit'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CashUnitEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CashServiceDataCollection	_amCashservicedatas;
		private bool	_alwaysFetchAmCashservicedatas, _alreadyFetchedAmCashservicedatas;
		private VarioSL.Entities.CollectionClasses.ComponentFillingCollection	_componentFillings;
		private bool	_alwaysFetchComponentFillings, _alreadyFetchedComponentFillings;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AmCashservicedatas</summary>
			public static readonly string AmCashservicedatas = "AmCashservicedatas";
			/// <summary>Member name ComponentFillings</summary>
			public static readonly string ComponentFillings = "ComponentFillings";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CashUnitEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CashUnitEntity() :base("CashUnitEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cashUnitID">PK value for CashUnit which data should be fetched into this CashUnit object</param>
		public CashUnitEntity(System.Int64 cashUnitID):base("CashUnitEntity")
		{
			InitClassFetch(cashUnitID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cashUnitID">PK value for CashUnit which data should be fetched into this CashUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CashUnitEntity(System.Int64 cashUnitID, IPrefetchPath prefetchPathToUse):base("CashUnitEntity")
		{
			InitClassFetch(cashUnitID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cashUnitID">PK value for CashUnit which data should be fetched into this CashUnit object</param>
		/// <param name="validator">The custom validator object for this CashUnitEntity</param>
		public CashUnitEntity(System.Int64 cashUnitID, IValidator validator):base("CashUnitEntity")
		{
			InitClassFetch(cashUnitID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CashUnitEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_amCashservicedatas = (VarioSL.Entities.CollectionClasses.CashServiceDataCollection)info.GetValue("_amCashservicedatas", typeof(VarioSL.Entities.CollectionClasses.CashServiceDataCollection));
			_alwaysFetchAmCashservicedatas = info.GetBoolean("_alwaysFetchAmCashservicedatas");
			_alreadyFetchedAmCashservicedatas = info.GetBoolean("_alreadyFetchedAmCashservicedatas");

			_componentFillings = (VarioSL.Entities.CollectionClasses.ComponentFillingCollection)info.GetValue("_componentFillings", typeof(VarioSL.Entities.CollectionClasses.ComponentFillingCollection));
			_alwaysFetchComponentFillings = info.GetBoolean("_alwaysFetchComponentFillings");
			_alreadyFetchedComponentFillings = info.GetBoolean("_alreadyFetchedComponentFillings");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAmCashservicedatas = (_amCashservicedatas.Count > 0);
			_alreadyFetchedComponentFillings = (_componentFillings.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AmCashservicedatas":
					toReturn.Add(Relations.CashServiceDataEntityUsingCashUnitID);
					break;
				case "ComponentFillings":
					toReturn.Add(Relations.ComponentFillingEntityUsingCashUnitID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_amCashservicedatas", (!this.MarkedForDeletion?_amCashservicedatas:null));
			info.AddValue("_alwaysFetchAmCashservicedatas", _alwaysFetchAmCashservicedatas);
			info.AddValue("_alreadyFetchedAmCashservicedatas", _alreadyFetchedAmCashservicedatas);
			info.AddValue("_componentFillings", (!this.MarkedForDeletion?_componentFillings:null));
			info.AddValue("_alwaysFetchComponentFillings", _alwaysFetchComponentFillings);
			info.AddValue("_alreadyFetchedComponentFillings", _alreadyFetchedComponentFillings);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AmCashservicedatas":
					_alreadyFetchedAmCashservicedatas = true;
					if(entity!=null)
					{
						this.AmCashservicedatas.Add((CashServiceDataEntity)entity);
					}
					break;
				case "ComponentFillings":
					_alreadyFetchedComponentFillings = true;
					if(entity!=null)
					{
						this.ComponentFillings.Add((ComponentFillingEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AmCashservicedatas":
					_amCashservicedatas.Add((CashServiceDataEntity)relatedEntity);
					break;
				case "ComponentFillings":
					_componentFillings.Add((ComponentFillingEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AmCashservicedatas":
					this.PerformRelatedEntityRemoval(_amCashservicedatas, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ComponentFillings":
					this.PerformRelatedEntityRemoval(_componentFillings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_amCashservicedatas);
			toReturn.Add(_componentFillings);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cashUnitID">PK value for CashUnit which data should be fetched into this CashUnit object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cashUnitID)
		{
			return FetchUsingPK(cashUnitID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cashUnitID">PK value for CashUnit which data should be fetched into this CashUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cashUnitID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cashUnitID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cashUnitID">PK value for CashUnit which data should be fetched into this CashUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cashUnitID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cashUnitID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cashUnitID">PK value for CashUnit which data should be fetched into this CashUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cashUnitID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cashUnitID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CashUnitID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CashUnitRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CashServiceDataEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CashServiceDataEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceDataCollection GetMultiAmCashservicedatas(bool forceFetch)
		{
			return GetMultiAmCashservicedatas(forceFetch, _amCashservicedatas.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceDataEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CashServiceDataEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceDataCollection GetMultiAmCashservicedatas(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAmCashservicedatas(forceFetch, _amCashservicedatas.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceDataEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceDataCollection GetMultiAmCashservicedatas(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAmCashservicedatas(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceDataEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CashServiceDataCollection GetMultiAmCashservicedatas(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAmCashservicedatas || forceFetch || _alwaysFetchAmCashservicedatas) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_amCashservicedatas);
				_amCashservicedatas.SuppressClearInGetMulti=!forceFetch;
				_amCashservicedatas.EntityFactoryToUse = entityFactoryToUse;
				_amCashservicedatas.GetMultiManyToOne(null, this, filter);
				_amCashservicedatas.SuppressClearInGetMulti=false;
				_alreadyFetchedAmCashservicedatas = true;
			}
			return _amCashservicedatas;
		}

		/// <summary> Sets the collection parameters for the collection for 'AmCashservicedatas'. These settings will be taken into account
		/// when the property AmCashservicedatas is requested or GetMultiAmCashservicedatas is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAmCashservicedatas(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_amCashservicedatas.SortClauses=sortClauses;
			_amCashservicedatas.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ComponentFillingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillings(bool forceFetch)
		{
			return GetMultiComponentFillings(forceFetch, _componentFillings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ComponentFillingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiComponentFillings(forceFetch, _componentFillings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiComponentFillings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedComponentFillings || forceFetch || _alwaysFetchComponentFillings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_componentFillings);
				_componentFillings.SuppressClearInGetMulti=!forceFetch;
				_componentFillings.EntityFactoryToUse = entityFactoryToUse;
				_componentFillings.GetMultiManyToOne(null, this, null, null, null, filter);
				_componentFillings.SuppressClearInGetMulti=false;
				_alreadyFetchedComponentFillings = true;
			}
			return _componentFillings;
		}

		/// <summary> Sets the collection parameters for the collection for 'ComponentFillings'. These settings will be taken into account
		/// when the property ComponentFillings is requested or GetMultiComponentFillings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersComponentFillings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_componentFillings.SortClauses=sortClauses;
			_componentFillings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AmCashservicedatas", _amCashservicedatas);
			toReturn.Add("ComponentFillings", _componentFillings);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cashUnitID">PK value for CashUnit which data should be fetched into this CashUnit object</param>
		/// <param name="validator">The validator object for this CashUnitEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cashUnitID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cashUnitID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_amCashservicedatas = new VarioSL.Entities.CollectionClasses.CashServiceDataCollection();
			_amCashservicedatas.SetContainingEntityInfo(this, "CashUnit");

			_componentFillings = new VarioSL.Entities.CollectionClasses.ComponentFillingCollection();
			_componentFillings.SetContainingEntityInfo(this, "CashUnit");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashUnitID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashUnitName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashUnitWorth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCoin", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Literal", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cashUnitID">PK value for CashUnit which data should be fetched into this CashUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cashUnitID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CashUnitFieldIndex.CashUnitID].ForcedCurrentValueWrite(cashUnitID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCashUnitDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CashUnitEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CashUnitRelations Relations
		{
			get	{ return new CashUnitRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CashServiceData' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAmCashservicedatas
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CashServiceDataCollection(), (IEntityRelation)GetRelationsForField("AmCashservicedatas")[0], (int)VarioSL.Entities.EntityType.CashUnitEntity, (int)VarioSL.Entities.EntityType.CashServiceDataEntity, 0, null, null, null, "AmCashservicedatas", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ComponentFilling' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponentFillings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentFillingCollection(), (IEntityRelation)GetRelationsForField("ComponentFillings")[0], (int)VarioSL.Entities.EntityType.CashUnitEntity, (int)VarioSL.Entities.EntityType.ComponentFillingEntity, 0, null, null, null, "ComponentFillings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CashUnitID property of the Entity CashUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHUNIT"."CASHUNITID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CashUnitID
		{
			get { return (System.Int64)GetValue((int)CashUnitFieldIndex.CashUnitID, true); }
			set	{ SetValue((int)CashUnitFieldIndex.CashUnitID, value, true); }
		}

		/// <summary> The CashUnitName property of the Entity CashUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHUNIT"."CASHUNITNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CashUnitName
		{
			get { return (System.String)GetValue((int)CashUnitFieldIndex.CashUnitName, true); }
			set	{ SetValue((int)CashUnitFieldIndex.CashUnitName, value, true); }
		}

		/// <summary> The CashUnitWorth property of the Entity CashUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHUNIT"."CASHUNITWORTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CashUnitWorth
		{
			get { return (System.Int32)GetValue((int)CashUnitFieldIndex.CashUnitWorth, true); }
			set	{ SetValue((int)CashUnitFieldIndex.CashUnitWorth, value, true); }
		}

		/// <summary> The Description property of the Entity CashUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHUNIT"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CashUnitFieldIndex.Description, true); }
			set	{ SetValue((int)CashUnitFieldIndex.Description, value, true); }
		}

		/// <summary> The IsCoin property of the Entity CashUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHUNIT"."ISCOIN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsCoin
		{
			get { return (Nullable<System.Int16>)GetValue((int)CashUnitFieldIndex.IsCoin, false); }
			set	{ SetValue((int)CashUnitFieldIndex.IsCoin, value, true); }
		}

		/// <summary> The Literal property of the Entity CashUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHUNIT"."CASHUNITNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Literal
		{
			get { return (System.String)GetValue((int)CashUnitFieldIndex.Literal, true); }

		}

		/// <summary> The MaxCount property of the Entity CashUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHUNIT"."MAXCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MaxCount
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashUnitFieldIndex.MaxCount, false); }
			set	{ SetValue((int)CashUnitFieldIndex.MaxCount, value, true); }
		}

		/// <summary> The MinCount property of the Entity CashUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHUNIT"."MINCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MinCount
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashUnitFieldIndex.MinCount, false); }
			set	{ SetValue((int)CashUnitFieldIndex.MinCount, value, true); }
		}

		/// <summary> The Visible property of the Entity CashUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHUNIT"."VISIBLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Visible
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CashUnitFieldIndex.Visible, false); }
			set	{ SetValue((int)CashUnitFieldIndex.Visible, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CashServiceDataEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAmCashservicedatas()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CashServiceDataCollection AmCashservicedatas
		{
			get	{ return GetMultiAmCashservicedatas(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AmCashservicedatas. When set to true, AmCashservicedatas is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AmCashservicedatas is accessed. You can always execute/ a forced fetch by calling GetMultiAmCashservicedatas(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAmCashservicedatas
		{
			get	{ return _alwaysFetchAmCashservicedatas; }
			set	{ _alwaysFetchAmCashservicedatas = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AmCashservicedatas already has been fetched. Setting this property to false when AmCashservicedatas has been fetched
		/// will clear the AmCashservicedatas collection well. Setting this property to true while AmCashservicedatas hasn't been fetched disables lazy loading for AmCashservicedatas</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAmCashservicedatas
		{
			get { return _alreadyFetchedAmCashservicedatas;}
			set 
			{
				if(_alreadyFetchedAmCashservicedatas && !value && (_amCashservicedatas != null))
				{
					_amCashservicedatas.Clear();
				}
				_alreadyFetchedAmCashservicedatas = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiComponentFillings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ComponentFillingCollection ComponentFillings
		{
			get	{ return GetMultiComponentFillings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ComponentFillings. When set to true, ComponentFillings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ComponentFillings is accessed. You can always execute/ a forced fetch by calling GetMultiComponentFillings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponentFillings
		{
			get	{ return _alwaysFetchComponentFillings; }
			set	{ _alwaysFetchComponentFillings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ComponentFillings already has been fetched. Setting this property to false when ComponentFillings has been fetched
		/// will clear the ComponentFillings collection well. Setting this property to true while ComponentFillings hasn't been fetched disables lazy loading for ComponentFillings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponentFillings
		{
			get { return _alreadyFetchedComponentFillings;}
			set 
			{
				if(_alreadyFetchedComponentFillings && !value && (_componentFillings != null))
				{
					_componentFillings.Clear();
				}
				_alreadyFetchedComponentFillings = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CashUnitEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
