﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Unit'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class UnitEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DeviceCollection	_devices;
		private bool	_alwaysFetchDevices, _alreadyFetchedDevices;
		private VarioSL.Entities.CollectionClasses.ParameterValueCollection	_parameterValues;
		private bool	_alwaysFetchParameterValues, _alreadyFetchedParameterValues;
		private VarioSL.Entities.CollectionClasses.PrinterToUnitCollection	_printerToUnits;
		private bool	_alwaysFetchPrinterToUnits, _alreadyFetchedPrinterToUnits;
		private VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection	_unitCollectionToUnits;
		private bool	_alwaysFetchUnitCollectionToUnits, _alreadyFetchedUnitCollectionToUnits;
		private VarioSL.Entities.CollectionClasses.UnitCollectionCollection _unitCollections;
		private bool	_alwaysFetchUnitCollections, _alreadyFetchedUnitCollections;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private DepotEntity _varioDepot;
		private bool	_alwaysFetchVarioDepot, _alreadyFetchedVarioDepot, _varioDepotReturnsNewIfNotFound;
		private TypeOfUnitEntity _typeOfUnit;
		private bool	_alwaysFetchTypeOfUnit, _alreadyFetchedTypeOfUnit, _typeOfUnitReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name VarioDepot</summary>
			public static readonly string VarioDepot = "VarioDepot";
			/// <summary>Member name TypeOfUnit</summary>
			public static readonly string TypeOfUnit = "TypeOfUnit";
			/// <summary>Member name Devices</summary>
			public static readonly string Devices = "Devices";
			/// <summary>Member name ParameterValues</summary>
			public static readonly string ParameterValues = "ParameterValues";
			/// <summary>Member name PrinterToUnits</summary>
			public static readonly string PrinterToUnits = "PrinterToUnits";
			/// <summary>Member name UnitCollectionToUnits</summary>
			public static readonly string UnitCollectionToUnits = "UnitCollectionToUnits";
			/// <summary>Member name UnitCollections</summary>
			public static readonly string UnitCollections = "UnitCollections";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UnitEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public UnitEntity() :base("UnitEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="unitID">PK value for Unit which data should be fetched into this Unit object</param>
		public UnitEntity(System.Int64 unitID):base("UnitEntity")
		{
			InitClassFetch(unitID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="unitID">PK value for Unit which data should be fetched into this Unit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UnitEntity(System.Int64 unitID, IPrefetchPath prefetchPathToUse):base("UnitEntity")
		{
			InitClassFetch(unitID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="unitID">PK value for Unit which data should be fetched into this Unit object</param>
		/// <param name="validator">The custom validator object for this UnitEntity</param>
		public UnitEntity(System.Int64 unitID, IValidator validator):base("UnitEntity")
		{
			InitClassFetch(unitID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UnitEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_devices = (VarioSL.Entities.CollectionClasses.DeviceCollection)info.GetValue("_devices", typeof(VarioSL.Entities.CollectionClasses.DeviceCollection));
			_alwaysFetchDevices = info.GetBoolean("_alwaysFetchDevices");
			_alreadyFetchedDevices = info.GetBoolean("_alreadyFetchedDevices");

			_parameterValues = (VarioSL.Entities.CollectionClasses.ParameterValueCollection)info.GetValue("_parameterValues", typeof(VarioSL.Entities.CollectionClasses.ParameterValueCollection));
			_alwaysFetchParameterValues = info.GetBoolean("_alwaysFetchParameterValues");
			_alreadyFetchedParameterValues = info.GetBoolean("_alreadyFetchedParameterValues");

			_printerToUnits = (VarioSL.Entities.CollectionClasses.PrinterToUnitCollection)info.GetValue("_printerToUnits", typeof(VarioSL.Entities.CollectionClasses.PrinterToUnitCollection));
			_alwaysFetchPrinterToUnits = info.GetBoolean("_alwaysFetchPrinterToUnits");
			_alreadyFetchedPrinterToUnits = info.GetBoolean("_alreadyFetchedPrinterToUnits");

			_unitCollectionToUnits = (VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection)info.GetValue("_unitCollectionToUnits", typeof(VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection));
			_alwaysFetchUnitCollectionToUnits = info.GetBoolean("_alwaysFetchUnitCollectionToUnits");
			_alreadyFetchedUnitCollectionToUnits = info.GetBoolean("_alreadyFetchedUnitCollectionToUnits");
			_unitCollections = (VarioSL.Entities.CollectionClasses.UnitCollectionCollection)info.GetValue("_unitCollections", typeof(VarioSL.Entities.CollectionClasses.UnitCollectionCollection));
			_alwaysFetchUnitCollections = info.GetBoolean("_alwaysFetchUnitCollections");
			_alreadyFetchedUnitCollections = info.GetBoolean("_alreadyFetchedUnitCollections");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_varioDepot = (DepotEntity)info.GetValue("_varioDepot", typeof(DepotEntity));
			if(_varioDepot!=null)
			{
				_varioDepot.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_varioDepotReturnsNewIfNotFound = info.GetBoolean("_varioDepotReturnsNewIfNotFound");
			_alwaysFetchVarioDepot = info.GetBoolean("_alwaysFetchVarioDepot");
			_alreadyFetchedVarioDepot = info.GetBoolean("_alreadyFetchedVarioDepot");

			_typeOfUnit = (TypeOfUnitEntity)info.GetValue("_typeOfUnit", typeof(TypeOfUnitEntity));
			if(_typeOfUnit!=null)
			{
				_typeOfUnit.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_typeOfUnitReturnsNewIfNotFound = info.GetBoolean("_typeOfUnitReturnsNewIfNotFound");
			_alwaysFetchTypeOfUnit = info.GetBoolean("_alwaysFetchTypeOfUnit");
			_alreadyFetchedTypeOfUnit = info.GetBoolean("_alreadyFetchedTypeOfUnit");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UnitFieldIndex)fieldIndex)
			{
				case UnitFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case UnitFieldIndex.DepotID:
					DesetupSyncVarioDepot(true, false);
					_alreadyFetchedVarioDepot = false;
					break;
				case UnitFieldIndex.TypeOfUnitID:
					DesetupSyncTypeOfUnit(true, false);
					_alreadyFetchedTypeOfUnit = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDevices = (_devices.Count > 0);
			_alreadyFetchedParameterValues = (_parameterValues.Count > 0);
			_alreadyFetchedPrinterToUnits = (_printerToUnits.Count > 0);
			_alreadyFetchedUnitCollectionToUnits = (_unitCollectionToUnits.Count > 0);
			_alreadyFetchedUnitCollections = (_unitCollections.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedVarioDepot = (_varioDepot != null);
			_alreadyFetchedTypeOfUnit = (_typeOfUnit != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "VarioDepot":
					toReturn.Add(Relations.DepotEntityUsingDepotID);
					break;
				case "TypeOfUnit":
					toReturn.Add(Relations.TypeOfUnitEntityUsingTypeOfUnitID);
					break;
				case "Devices":
					toReturn.Add(Relations.DeviceEntityUsingUnitID);
					break;
				case "ParameterValues":
					toReturn.Add(Relations.ParameterValueEntityUsingUnitID);
					break;
				case "PrinterToUnits":
					toReturn.Add(Relations.PrinterToUnitEntityUsingUnitID);
					break;
				case "UnitCollectionToUnits":
					toReturn.Add(Relations.UnitCollectionToUnitEntityUsingUnitID);
					break;
				case "UnitCollections":
					toReturn.Add(Relations.UnitCollectionToUnitEntityUsingUnitID, "UnitEntity__", "UnitCollectionToUnit_", JoinHint.None);
					toReturn.Add(UnitCollectionToUnitEntity.Relations.UnitCollectionEntityUsingUnitCollectionID, "UnitCollectionToUnit_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_devices", (!this.MarkedForDeletion?_devices:null));
			info.AddValue("_alwaysFetchDevices", _alwaysFetchDevices);
			info.AddValue("_alreadyFetchedDevices", _alreadyFetchedDevices);
			info.AddValue("_parameterValues", (!this.MarkedForDeletion?_parameterValues:null));
			info.AddValue("_alwaysFetchParameterValues", _alwaysFetchParameterValues);
			info.AddValue("_alreadyFetchedParameterValues", _alreadyFetchedParameterValues);
			info.AddValue("_printerToUnits", (!this.MarkedForDeletion?_printerToUnits:null));
			info.AddValue("_alwaysFetchPrinterToUnits", _alwaysFetchPrinterToUnits);
			info.AddValue("_alreadyFetchedPrinterToUnits", _alreadyFetchedPrinterToUnits);
			info.AddValue("_unitCollectionToUnits", (!this.MarkedForDeletion?_unitCollectionToUnits:null));
			info.AddValue("_alwaysFetchUnitCollectionToUnits", _alwaysFetchUnitCollectionToUnits);
			info.AddValue("_alreadyFetchedUnitCollectionToUnits", _alreadyFetchedUnitCollectionToUnits);
			info.AddValue("_unitCollections", (!this.MarkedForDeletion?_unitCollections:null));
			info.AddValue("_alwaysFetchUnitCollections", _alwaysFetchUnitCollections);
			info.AddValue("_alreadyFetchedUnitCollections", _alreadyFetchedUnitCollections);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_varioDepot", (!this.MarkedForDeletion?_varioDepot:null));
			info.AddValue("_varioDepotReturnsNewIfNotFound", _varioDepotReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVarioDepot", _alwaysFetchVarioDepot);
			info.AddValue("_alreadyFetchedVarioDepot", _alreadyFetchedVarioDepot);
			info.AddValue("_typeOfUnit", (!this.MarkedForDeletion?_typeOfUnit:null));
			info.AddValue("_typeOfUnitReturnsNewIfNotFound", _typeOfUnitReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTypeOfUnit", _alwaysFetchTypeOfUnit);
			info.AddValue("_alreadyFetchedTypeOfUnit", _alreadyFetchedTypeOfUnit);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "VarioDepot":
					_alreadyFetchedVarioDepot = true;
					this.VarioDepot = (DepotEntity)entity;
					break;
				case "TypeOfUnit":
					_alreadyFetchedTypeOfUnit = true;
					this.TypeOfUnit = (TypeOfUnitEntity)entity;
					break;
				case "Devices":
					_alreadyFetchedDevices = true;
					if(entity!=null)
					{
						this.Devices.Add((DeviceEntity)entity);
					}
					break;
				case "ParameterValues":
					_alreadyFetchedParameterValues = true;
					if(entity!=null)
					{
						this.ParameterValues.Add((ParameterValueEntity)entity);
					}
					break;
				case "PrinterToUnits":
					_alreadyFetchedPrinterToUnits = true;
					if(entity!=null)
					{
						this.PrinterToUnits.Add((PrinterToUnitEntity)entity);
					}
					break;
				case "UnitCollectionToUnits":
					_alreadyFetchedUnitCollectionToUnits = true;
					if(entity!=null)
					{
						this.UnitCollectionToUnits.Add((UnitCollectionToUnitEntity)entity);
					}
					break;
				case "UnitCollections":
					_alreadyFetchedUnitCollections = true;
					if(entity!=null)
					{
						this.UnitCollections.Add((UnitCollectionEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "VarioDepot":
					SetupSyncVarioDepot(relatedEntity);
					break;
				case "TypeOfUnit":
					SetupSyncTypeOfUnit(relatedEntity);
					break;
				case "Devices":
					_devices.Add((DeviceEntity)relatedEntity);
					break;
				case "ParameterValues":
					_parameterValues.Add((ParameterValueEntity)relatedEntity);
					break;
				case "PrinterToUnits":
					_printerToUnits.Add((PrinterToUnitEntity)relatedEntity);
					break;
				case "UnitCollectionToUnits":
					_unitCollectionToUnits.Add((UnitCollectionToUnitEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "VarioDepot":
					DesetupSyncVarioDepot(false, true);
					break;
				case "TypeOfUnit":
					DesetupSyncTypeOfUnit(false, true);
					break;
				case "Devices":
					this.PerformRelatedEntityRemoval(_devices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterValues":
					this.PerformRelatedEntityRemoval(_parameterValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PrinterToUnits":
					this.PerformRelatedEntityRemoval(_printerToUnits, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UnitCollectionToUnits":
					this.PerformRelatedEntityRemoval(_unitCollectionToUnits, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_varioDepot!=null)
			{
				toReturn.Add(_varioDepot);
			}
			if(_typeOfUnit!=null)
			{
				toReturn.Add(_typeOfUnit);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_devices);
			toReturn.Add(_parameterValues);
			toReturn.Add(_printerToUnits);
			toReturn.Add(_unitCollectionToUnits);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="unitID">PK value for Unit which data should be fetched into this Unit object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 unitID)
		{
			return FetchUsingPK(unitID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="unitID">PK value for Unit which data should be fetched into this Unit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 unitID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(unitID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="unitID">PK value for Unit which data should be fetched into this Unit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 unitID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(unitID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="unitID">PK value for Unit which data should be fetched into this Unit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 unitID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(unitID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UnitID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UnitRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceCollection GetMultiDevices(bool forceFetch)
		{
			return GetMultiDevices(forceFetch, _devices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceCollection GetMultiDevices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDevices(forceFetch, _devices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DeviceCollection GetMultiDevices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDevices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DeviceCollection GetMultiDevices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDevices || forceFetch || _alwaysFetchDevices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_devices);
				_devices.SuppressClearInGetMulti=!forceFetch;
				_devices.EntityFactoryToUse = entityFactoryToUse;
				_devices.GetMultiManyToOne(null, this, filter);
				_devices.SuppressClearInGetMulti=false;
				_alreadyFetchedDevices = true;
			}
			return _devices;
		}

		/// <summary> Sets the collection parameters for the collection for 'Devices'. These settings will be taken into account
		/// when the property Devices is requested or GetMultiDevices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDevices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_devices.SortClauses=sortClauses;
			_devices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch)
		{
			return GetMultiParameterValues(forceFetch, _parameterValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterValues(forceFetch, _parameterValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterValues || forceFetch || _alwaysFetchParameterValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterValues);
				_parameterValues.SuppressClearInGetMulti=!forceFetch;
				_parameterValues.EntityFactoryToUse = entityFactoryToUse;
				_parameterValues.GetMultiManyToOne(null, this, null, null, null, filter);
				_parameterValues.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterValues = true;
			}
			return _parameterValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterValues'. These settings will be taken into account
		/// when the property ParameterValues is requested or GetMultiParameterValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterValues.SortClauses=sortClauses;
			_parameterValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PrinterToUnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PrinterToUnitEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PrinterToUnitCollection GetMultiPrinterToUnits(bool forceFetch)
		{
			return GetMultiPrinterToUnits(forceFetch, _printerToUnits.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PrinterToUnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PrinterToUnitEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PrinterToUnitCollection GetMultiPrinterToUnits(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPrinterToUnits(forceFetch, _printerToUnits.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PrinterToUnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PrinterToUnitCollection GetMultiPrinterToUnits(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPrinterToUnits(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PrinterToUnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PrinterToUnitCollection GetMultiPrinterToUnits(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPrinterToUnits || forceFetch || _alwaysFetchPrinterToUnits) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_printerToUnits);
				_printerToUnits.SuppressClearInGetMulti=!forceFetch;
				_printerToUnits.EntityFactoryToUse = entityFactoryToUse;
				_printerToUnits.GetMultiManyToOne(this, null, filter);
				_printerToUnits.SuppressClearInGetMulti=false;
				_alreadyFetchedPrinterToUnits = true;
			}
			return _printerToUnits;
		}

		/// <summary> Sets the collection parameters for the collection for 'PrinterToUnits'. These settings will be taken into account
		/// when the property PrinterToUnits is requested or GetMultiPrinterToUnits is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPrinterToUnits(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_printerToUnits.SortClauses=sortClauses;
			_printerToUnits.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionToUnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UnitCollectionToUnitEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection GetMultiUnitCollectionToUnits(bool forceFetch)
		{
			return GetMultiUnitCollectionToUnits(forceFetch, _unitCollectionToUnits.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionToUnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UnitCollectionToUnitEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection GetMultiUnitCollectionToUnits(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUnitCollectionToUnits(forceFetch, _unitCollectionToUnits.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionToUnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection GetMultiUnitCollectionToUnits(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUnitCollectionToUnits(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionToUnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection GetMultiUnitCollectionToUnits(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUnitCollectionToUnits || forceFetch || _alwaysFetchUnitCollectionToUnits) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_unitCollectionToUnits);
				_unitCollectionToUnits.SuppressClearInGetMulti=!forceFetch;
				_unitCollectionToUnits.EntityFactoryToUse = entityFactoryToUse;
				_unitCollectionToUnits.GetMultiManyToOne(this, null, filter);
				_unitCollectionToUnits.SuppressClearInGetMulti=false;
				_alreadyFetchedUnitCollectionToUnits = true;
			}
			return _unitCollectionToUnits;
		}

		/// <summary> Sets the collection parameters for the collection for 'UnitCollectionToUnits'. These settings will be taken into account
		/// when the property UnitCollectionToUnits is requested or GetMultiUnitCollectionToUnits is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUnitCollectionToUnits(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_unitCollectionToUnits.SortClauses=sortClauses;
			_unitCollectionToUnits.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UnitCollectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionCollection GetMultiUnitCollections(bool forceFetch)
		{
			return GetMultiUnitCollections(forceFetch, _unitCollections.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionCollection GetMultiUnitCollections(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUnitCollections || forceFetch || _alwaysFetchUnitCollections) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_unitCollections);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UnitFields.UnitID, ComparisonOperator.Equal, this.UnitID, "UnitEntity__"));
				_unitCollections.SuppressClearInGetMulti=!forceFetch;
				_unitCollections.EntityFactoryToUse = entityFactoryToUse;
				_unitCollections.GetMulti(filter, GetRelationsForField("UnitCollections"));
				_unitCollections.SuppressClearInGetMulti=false;
				_alreadyFetchedUnitCollections = true;
			}
			return _unitCollections;
		}

		/// <summary> Sets the collection parameters for the collection for 'UnitCollections'. These settings will be taken into account
		/// when the property UnitCollections is requested or GetMultiUnitCollections is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUnitCollections(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_unitCollections.SortClauses=sortClauses;
			_unitCollections.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'DepotEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DepotEntity' which is related to this entity.</returns>
		public DepotEntity GetSingleVarioDepot()
		{
			return GetSingleVarioDepot(false);
		}

		/// <summary> Retrieves the related entity of type 'DepotEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DepotEntity' which is related to this entity.</returns>
		public virtual DepotEntity GetSingleVarioDepot(bool forceFetch)
		{
			if( ( !_alreadyFetchedVarioDepot || forceFetch || _alwaysFetchVarioDepot) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DepotEntityUsingDepotID);
				DepotEntity newEntity = new DepotEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DepotID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DepotEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_varioDepotReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VarioDepot = newEntity;
				_alreadyFetchedVarioDepot = fetchResult;
			}
			return _varioDepot;
		}


		/// <summary> Retrieves the related entity of type 'TypeOfUnitEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TypeOfUnitEntity' which is related to this entity.</returns>
		public TypeOfUnitEntity GetSingleTypeOfUnit()
		{
			return GetSingleTypeOfUnit(false);
		}

		/// <summary> Retrieves the related entity of type 'TypeOfUnitEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TypeOfUnitEntity' which is related to this entity.</returns>
		public virtual TypeOfUnitEntity GetSingleTypeOfUnit(bool forceFetch)
		{
			if( ( !_alreadyFetchedTypeOfUnit || forceFetch || _alwaysFetchTypeOfUnit) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TypeOfUnitEntityUsingTypeOfUnitID);
				TypeOfUnitEntity newEntity = new TypeOfUnitEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TypeOfUnitID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TypeOfUnitEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_typeOfUnitReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TypeOfUnit = newEntity;
				_alreadyFetchedTypeOfUnit = fetchResult;
			}
			return _typeOfUnit;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("VarioDepot", _varioDepot);
			toReturn.Add("TypeOfUnit", _typeOfUnit);
			toReturn.Add("Devices", _devices);
			toReturn.Add("ParameterValues", _parameterValues);
			toReturn.Add("PrinterToUnits", _printerToUnits);
			toReturn.Add("UnitCollectionToUnits", _unitCollectionToUnits);
			toReturn.Add("UnitCollections", _unitCollections);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="unitID">PK value for Unit which data should be fetched into this Unit object</param>
		/// <param name="validator">The validator object for this UnitEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 unitID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(unitID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_devices = new VarioSL.Entities.CollectionClasses.DeviceCollection();
			_devices.SetContainingEntityInfo(this, "Unit");

			_parameterValues = new VarioSL.Entities.CollectionClasses.ParameterValueCollection();
			_parameterValues.SetContainingEntityInfo(this, "Unit");

			_printerToUnits = new VarioSL.Entities.CollectionClasses.PrinterToUnitCollection();
			_printerToUnits.SetContainingEntityInfo(this, "Unit");

			_unitCollectionToUnits = new VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection();
			_unitCollectionToUnits.SetContainingEntityInfo(this, "Unit");
			_unitCollections = new VarioSL.Entities.CollectionClasses.UnitCollectionCollection();
			_clientReturnsNewIfNotFound = false;
			_varioDepotReturnsNewIfNotFound = false;
			_typeOfUnitReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepotID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EquipmentConfigurationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GarageCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsAutoGenerated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastGprsConnection", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastWlanConnection", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("License", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotificationAddress", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeOfUnitID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnitID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnitName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnitNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnitState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VisibleUnitNumber", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticUnitRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "UmUnits", resetFKFields, new int[] { (int)UnitFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticUnitRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _varioDepot</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVarioDepot(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _varioDepot, new PropertyChangedEventHandler( OnVarioDepotPropertyChanged ), "VarioDepot", VarioSL.Entities.RelationClasses.StaticUnitRelations.DepotEntityUsingDepotIDStatic, true, signalRelatedEntity, "Units", resetFKFields, new int[] { (int)UnitFieldIndex.DepotID } );		
			_varioDepot = null;
		}
		
		/// <summary> setups the sync logic for member _varioDepot</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVarioDepot(IEntityCore relatedEntity)
		{
			if(_varioDepot!=relatedEntity)
			{		
				DesetupSyncVarioDepot(true, true);
				_varioDepot = (DepotEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _varioDepot, new PropertyChangedEventHandler( OnVarioDepotPropertyChanged ), "VarioDepot", VarioSL.Entities.RelationClasses.StaticUnitRelations.DepotEntityUsingDepotIDStatic, true, ref _alreadyFetchedVarioDepot, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVarioDepotPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _typeOfUnit</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTypeOfUnit(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _typeOfUnit, new PropertyChangedEventHandler( OnTypeOfUnitPropertyChanged ), "TypeOfUnit", VarioSL.Entities.RelationClasses.StaticUnitRelations.TypeOfUnitEntityUsingTypeOfUnitIDStatic, true, signalRelatedEntity, "Units", resetFKFields, new int[] { (int)UnitFieldIndex.TypeOfUnitID } );		
			_typeOfUnit = null;
		}
		
		/// <summary> setups the sync logic for member _typeOfUnit</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTypeOfUnit(IEntityCore relatedEntity)
		{
			if(_typeOfUnit!=relatedEntity)
			{		
				DesetupSyncTypeOfUnit(true, true);
				_typeOfUnit = (TypeOfUnitEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _typeOfUnit, new PropertyChangedEventHandler( OnTypeOfUnitPropertyChanged ), "TypeOfUnit", VarioSL.Entities.RelationClasses.StaticUnitRelations.TypeOfUnitEntityUsingTypeOfUnitIDStatic, true, ref _alreadyFetchedTypeOfUnit, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTypeOfUnitPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="unitID">PK value for Unit which data should be fetched into this Unit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 unitID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UnitFieldIndex.UnitID].ForcedCurrentValueWrite(unitID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUnitDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UnitEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UnitRelations Relations
		{
			get	{ return new UnitRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDevices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceCollection(), (IEntityRelation)GetRelationsForField("Devices")[0], (int)VarioSL.Entities.EntityType.UnitEntity, (int)VarioSL.Entities.EntityType.DeviceEntity, 0, null, null, null, "Devices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterValueCollection(), (IEntityRelation)GetRelationsForField("ParameterValues")[0], (int)VarioSL.Entities.EntityType.UnitEntity, (int)VarioSL.Entities.EntityType.ParameterValueEntity, 0, null, null, null, "ParameterValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PrinterToUnit' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPrinterToUnits
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PrinterToUnitCollection(), (IEntityRelation)GetRelationsForField("PrinterToUnits")[0], (int)VarioSL.Entities.EntityType.UnitEntity, (int)VarioSL.Entities.EntityType.PrinterToUnitEntity, 0, null, null, null, "PrinterToUnits", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UnitCollectionToUnit' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnitCollectionToUnits
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection(), (IEntityRelation)GetRelationsForField("UnitCollectionToUnits")[0], (int)VarioSL.Entities.EntityType.UnitEntity, (int)VarioSL.Entities.EntityType.UnitCollectionToUnitEntity, 0, null, null, null, "UnitCollectionToUnits", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UnitCollection'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnitCollections
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.UnitCollectionToUnitEntityUsingUnitID;
				intermediateRelation.SetAliases(string.Empty, "UnitCollectionToUnit_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollectionCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.UnitEntity, (int)VarioSL.Entities.EntityType.UnitCollectionEntity, 0, null, null, GetRelationsForField("UnitCollections"), "UnitCollections", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.UnitEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Depot'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVarioDepot
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DepotCollection(), (IEntityRelation)GetRelationsForField("VarioDepot")[0], (int)VarioSL.Entities.EntityType.UnitEntity, (int)VarioSL.Entities.EntityType.DepotEntity, 0, null, null, null, "VarioDepot", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TypeOfUnit'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTypeOfUnit
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TypeOfUnitCollection(), (IEntityRelation)GetRelationsForField("TypeOfUnit")[0], (int)VarioSL.Entities.EntityType.UnitEntity, (int)VarioSL.Entities.EntityType.TypeOfUnitEntity, 0, null, null, null, "TypeOfUnit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientID property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UnitFieldIndex.ClientID, false); }
			set	{ SetValue((int)UnitFieldIndex.ClientID, value, true); }
		}

		/// <summary> The DepotID property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."DEPOTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DepotID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UnitFieldIndex.DepotID, false); }
			set	{ SetValue((int)UnitFieldIndex.DepotID, value, true); }
		}

		/// <summary> The Description property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)UnitFieldIndex.Description, true); }
			set	{ SetValue((int)UnitFieldIndex.Description, value, true); }
		}

		/// <summary> The EquipmentConfigurationID property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."EQUIPMENTCONFIGURATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EquipmentConfigurationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UnitFieldIndex.EquipmentConfigurationID, false); }
			set	{ SetValue((int)UnitFieldIndex.EquipmentConfigurationID, value, true); }
		}

		/// <summary> The GarageCode property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."GARAGECODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GarageCode
		{
			get { return (System.String)GetValue((int)UnitFieldIndex.GarageCode, true); }
			set	{ SetValue((int)UnitFieldIndex.GarageCode, value, true); }
		}

		/// <summary> The IsAutoGenerated property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."AUTOGENERATED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAutoGenerated
		{
			get { return (System.Boolean)GetValue((int)UnitFieldIndex.IsAutoGenerated, true); }
			set	{ SetValue((int)UnitFieldIndex.IsAutoGenerated, value, true); }
		}

		/// <summary> The LastGprsConnection property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."LASTGPRSCONNECTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastGprsConnection
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UnitFieldIndex.LastGprsConnection, false); }
			set	{ SetValue((int)UnitFieldIndex.LastGprsConnection, value, true); }
		}

		/// <summary> The LastWlanConnection property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."LASTWLANCONNECTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastWlanConnection
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UnitFieldIndex.LastWlanConnection, false); }
			set	{ SetValue((int)UnitFieldIndex.LastWlanConnection, value, true); }
		}

		/// <summary> The License property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."LICENSE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String License
		{
			get { return (System.String)GetValue((int)UnitFieldIndex.License, true); }
			set	{ SetValue((int)UnitFieldIndex.License, value, true); }
		}

		/// <summary> The NotificationAddress property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."NOTIFICATIONADDRESS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NotificationAddress
		{
			get { return (System.String)GetValue((int)UnitFieldIndex.NotificationAddress, true); }
			set	{ SetValue((int)UnitFieldIndex.NotificationAddress, value, true); }
		}

		/// <summary> The TypeOfUnitID property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."TYPEOFUNITID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TypeOfUnitID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UnitFieldIndex.TypeOfUnitID, false); }
			set	{ SetValue((int)UnitFieldIndex.TypeOfUnitID, value, true); }
		}

		/// <summary> The UnitID property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."UNITID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 UnitID
		{
			get { return (System.Int64)GetValue((int)UnitFieldIndex.UnitID, true); }
			set	{ SetValue((int)UnitFieldIndex.UnitID, value, true); }
		}

		/// <summary> The UnitName property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."UNITNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String UnitName
		{
			get { return (System.String)GetValue((int)UnitFieldIndex.UnitName, true); }
			set	{ SetValue((int)UnitFieldIndex.UnitName, value, true); }
		}

		/// <summary> The UnitNumber property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."UNITNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UnitNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)UnitFieldIndex.UnitNumber, false); }
			set	{ SetValue((int)UnitFieldIndex.UnitNumber, value, true); }
		}

		/// <summary> The UnitState property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."UNITSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UnitState
		{
			get { return (Nullable<System.Int64>)GetValue((int)UnitFieldIndex.UnitState, false); }
			set	{ SetValue((int)UnitFieldIndex.UnitState, value, true); }
		}

		/// <summary> The VisibleUnitNumber property of the Entity Unit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_UNIT"."VISIBLEUNITNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> VisibleUnitNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)UnitFieldIndex.VisibleUnitNumber, false); }
			set	{ SetValue((int)UnitFieldIndex.VisibleUnitNumber, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDevices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DeviceCollection Devices
		{
			get	{ return GetMultiDevices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Devices. When set to true, Devices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Devices is accessed. You can always execute/ a forced fetch by calling GetMultiDevices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDevices
		{
			get	{ return _alwaysFetchDevices; }
			set	{ _alwaysFetchDevices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Devices already has been fetched. Setting this property to false when Devices has been fetched
		/// will clear the Devices collection well. Setting this property to true while Devices hasn't been fetched disables lazy loading for Devices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDevices
		{
			get { return _alreadyFetchedDevices;}
			set 
			{
				if(_alreadyFetchedDevices && !value && (_devices != null))
				{
					_devices.Clear();
				}
				_alreadyFetchedDevices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterValueCollection ParameterValues
		{
			get	{ return GetMultiParameterValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterValues. When set to true, ParameterValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterValues is accessed. You can always execute/ a forced fetch by calling GetMultiParameterValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterValues
		{
			get	{ return _alwaysFetchParameterValues; }
			set	{ _alwaysFetchParameterValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterValues already has been fetched. Setting this property to false when ParameterValues has been fetched
		/// will clear the ParameterValues collection well. Setting this property to true while ParameterValues hasn't been fetched disables lazy loading for ParameterValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterValues
		{
			get { return _alreadyFetchedParameterValues;}
			set 
			{
				if(_alreadyFetchedParameterValues && !value && (_parameterValues != null))
				{
					_parameterValues.Clear();
				}
				_alreadyFetchedParameterValues = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PrinterToUnitEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPrinterToUnits()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PrinterToUnitCollection PrinterToUnits
		{
			get	{ return GetMultiPrinterToUnits(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PrinterToUnits. When set to true, PrinterToUnits is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PrinterToUnits is accessed. You can always execute/ a forced fetch by calling GetMultiPrinterToUnits(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPrinterToUnits
		{
			get	{ return _alwaysFetchPrinterToUnits; }
			set	{ _alwaysFetchPrinterToUnits = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PrinterToUnits already has been fetched. Setting this property to false when PrinterToUnits has been fetched
		/// will clear the PrinterToUnits collection well. Setting this property to true while PrinterToUnits hasn't been fetched disables lazy loading for PrinterToUnits</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPrinterToUnits
		{
			get { return _alreadyFetchedPrinterToUnits;}
			set 
			{
				if(_alreadyFetchedPrinterToUnits && !value && (_printerToUnits != null))
				{
					_printerToUnits.Clear();
				}
				_alreadyFetchedPrinterToUnits = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UnitCollectionToUnitEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUnitCollectionToUnits()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection UnitCollectionToUnits
		{
			get	{ return GetMultiUnitCollectionToUnits(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UnitCollectionToUnits. When set to true, UnitCollectionToUnits is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UnitCollectionToUnits is accessed. You can always execute/ a forced fetch by calling GetMultiUnitCollectionToUnits(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnitCollectionToUnits
		{
			get	{ return _alwaysFetchUnitCollectionToUnits; }
			set	{ _alwaysFetchUnitCollectionToUnits = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UnitCollectionToUnits already has been fetched. Setting this property to false when UnitCollectionToUnits has been fetched
		/// will clear the UnitCollectionToUnits collection well. Setting this property to true while UnitCollectionToUnits hasn't been fetched disables lazy loading for UnitCollectionToUnits</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnitCollectionToUnits
		{
			get { return _alreadyFetchedUnitCollectionToUnits;}
			set 
			{
				if(_alreadyFetchedUnitCollectionToUnits && !value && (_unitCollectionToUnits != null))
				{
					_unitCollectionToUnits.Clear();
				}
				_alreadyFetchedUnitCollectionToUnits = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUnitCollections()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollectionCollection UnitCollections
		{
			get { return GetMultiUnitCollections(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UnitCollections. When set to true, UnitCollections is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UnitCollections is accessed. You can always execute a forced fetch by calling GetMultiUnitCollections(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnitCollections
		{
			get	{ return _alwaysFetchUnitCollections; }
			set	{ _alwaysFetchUnitCollections = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UnitCollections already has been fetched. Setting this property to false when UnitCollections has been fetched
		/// will clear the UnitCollections collection well. Setting this property to true while UnitCollections hasn't been fetched disables lazy loading for UnitCollections</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnitCollections
		{
			get { return _alreadyFetchedUnitCollections;}
			set 
			{
				if(_alreadyFetchedUnitCollections && !value && (_unitCollections != null))
				{
					_unitCollections.Clear();
				}
				_alreadyFetchedUnitCollections = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UmUnits", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DepotEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVarioDepot()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DepotEntity VarioDepot
		{
			get	{ return GetSingleVarioDepot(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVarioDepot(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Units", "VarioDepot", _varioDepot, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VarioDepot. When set to true, VarioDepot is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VarioDepot is accessed. You can always execute a forced fetch by calling GetSingleVarioDepot(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVarioDepot
		{
			get	{ return _alwaysFetchVarioDepot; }
			set	{ _alwaysFetchVarioDepot = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VarioDepot already has been fetched. Setting this property to false when VarioDepot has been fetched
		/// will set VarioDepot to null as well. Setting this property to true while VarioDepot hasn't been fetched disables lazy loading for VarioDepot</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVarioDepot
		{
			get { return _alreadyFetchedVarioDepot;}
			set 
			{
				if(_alreadyFetchedVarioDepot && !value)
				{
					this.VarioDepot = null;
				}
				_alreadyFetchedVarioDepot = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VarioDepot is not found
		/// in the database. When set to true, VarioDepot will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool VarioDepotReturnsNewIfNotFound
		{
			get	{ return _varioDepotReturnsNewIfNotFound; }
			set { _varioDepotReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TypeOfUnitEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTypeOfUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TypeOfUnitEntity TypeOfUnit
		{
			get	{ return GetSingleTypeOfUnit(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTypeOfUnit(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Units", "TypeOfUnit", _typeOfUnit, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TypeOfUnit. When set to true, TypeOfUnit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TypeOfUnit is accessed. You can always execute a forced fetch by calling GetSingleTypeOfUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTypeOfUnit
		{
			get	{ return _alwaysFetchTypeOfUnit; }
			set	{ _alwaysFetchTypeOfUnit = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TypeOfUnit already has been fetched. Setting this property to false when TypeOfUnit has been fetched
		/// will set TypeOfUnit to null as well. Setting this property to true while TypeOfUnit hasn't been fetched disables lazy loading for TypeOfUnit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTypeOfUnit
		{
			get { return _alreadyFetchedTypeOfUnit;}
			set 
			{
				if(_alreadyFetchedTypeOfUnit && !value)
				{
					this.TypeOfUnit = null;
				}
				_alreadyFetchedTypeOfUnit = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TypeOfUnit is not found
		/// in the database. When set to true, TypeOfUnit will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TypeOfUnitReturnsNewIfNotFound
		{
			get	{ return _typeOfUnitReturnsNewIfNotFound; }
			set { _typeOfUnitReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.UnitEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
