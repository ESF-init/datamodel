﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'NotificationMessageOwner'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class NotificationMessageOwnerEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private NotificationMessageEntity _notificationMessage;
		private bool	_alwaysFetchNotificationMessage, _alreadyFetchedNotificationMessage, _notificationMessageReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name NotificationMessage</summary>
			public static readonly string NotificationMessage = "NotificationMessage";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static NotificationMessageOwnerEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public NotificationMessageOwnerEntity() :base("NotificationMessageOwnerEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="contractID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="notificationMessageID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		public NotificationMessageOwnerEntity(System.Int64 contractID, System.Int64 notificationMessageID):base("NotificationMessageOwnerEntity")
		{
			InitClassFetch(contractID, notificationMessageID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="contractID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="notificationMessageID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public NotificationMessageOwnerEntity(System.Int64 contractID, System.Int64 notificationMessageID, IPrefetchPath prefetchPathToUse):base("NotificationMessageOwnerEntity")
		{
			InitClassFetch(contractID, notificationMessageID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="contractID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="notificationMessageID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="validator">The custom validator object for this NotificationMessageOwnerEntity</param>
		public NotificationMessageOwnerEntity(System.Int64 contractID, System.Int64 notificationMessageID, IValidator validator):base("NotificationMessageOwnerEntity")
		{
			InitClassFetch(contractID, notificationMessageID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected NotificationMessageOwnerEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_notificationMessage = (NotificationMessageEntity)info.GetValue("_notificationMessage", typeof(NotificationMessageEntity));
			if(_notificationMessage!=null)
			{
				_notificationMessage.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_notificationMessageReturnsNewIfNotFound = info.GetBoolean("_notificationMessageReturnsNewIfNotFound");
			_alwaysFetchNotificationMessage = info.GetBoolean("_alwaysFetchNotificationMessage");
			_alreadyFetchedNotificationMessage = info.GetBoolean("_alreadyFetchedNotificationMessage");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((NotificationMessageOwnerFieldIndex)fieldIndex)
			{
				case NotificationMessageOwnerFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case NotificationMessageOwnerFieldIndex.NotificationMessageID:
					DesetupSyncNotificationMessage(true, false);
					_alreadyFetchedNotificationMessage = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedNotificationMessage = (_notificationMessage != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "NotificationMessage":
					toReturn.Add(Relations.NotificationMessageEntityUsingNotificationMessageID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_notificationMessage", (!this.MarkedForDeletion?_notificationMessage:null));
			info.AddValue("_notificationMessageReturnsNewIfNotFound", _notificationMessageReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchNotificationMessage", _alwaysFetchNotificationMessage);
			info.AddValue("_alreadyFetchedNotificationMessage", _alreadyFetchedNotificationMessage);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "NotificationMessage":
					_alreadyFetchedNotificationMessage = true;
					this.NotificationMessage = (NotificationMessageEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "NotificationMessage":
					SetupSyncNotificationMessage(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "NotificationMessage":
					DesetupSyncNotificationMessage(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_notificationMessage!=null)
			{
				toReturn.Add(_notificationMessage);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contractID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="notificationMessageID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contractID, System.Int64 notificationMessageID)
		{
			return FetchUsingPK(contractID, notificationMessageID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contractID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="notificationMessageID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contractID, System.Int64 notificationMessageID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(contractID, notificationMessageID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contractID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="notificationMessageID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contractID, System.Int64 notificationMessageID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(contractID, notificationMessageID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contractID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="notificationMessageID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contractID, System.Int64 notificationMessageID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(contractID, notificationMessageID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ContractID, this.NotificationMessageID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new NotificationMessageOwnerRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID);
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'NotificationMessageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'NotificationMessageEntity' which is related to this entity.</returns>
		public NotificationMessageEntity GetSingleNotificationMessage()
		{
			return GetSingleNotificationMessage(false);
		}

		/// <summary> Retrieves the related entity of type 'NotificationMessageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'NotificationMessageEntity' which is related to this entity.</returns>
		public virtual NotificationMessageEntity GetSingleNotificationMessage(bool forceFetch)
		{
			if( ( !_alreadyFetchedNotificationMessage || forceFetch || _alwaysFetchNotificationMessage) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.NotificationMessageEntityUsingNotificationMessageID);
				NotificationMessageEntity newEntity = new NotificationMessageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.NotificationMessageID);
				}
				if(fetchResult)
				{
					newEntity = (NotificationMessageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_notificationMessageReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.NotificationMessage = newEntity;
				_alreadyFetchedNotificationMessage = fetchResult;
			}
			return _notificationMessage;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Contract", _contract);
			toReturn.Add("NotificationMessage", _notificationMessage);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="contractID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="notificationMessageID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="validator">The validator object for this NotificationMessageOwnerEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 contractID, System.Int64 notificationMessageID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(contractID, notificationMessageID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_contractReturnsNewIfNotFound = false;
			_notificationMessageReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsAcknowledged", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDeleted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotificationMessageID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticNotificationMessageOwnerRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "NotificationMessageOwners", resetFKFields, new int[] { (int)NotificationMessageOwnerFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticNotificationMessageOwnerRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _notificationMessage</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncNotificationMessage(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _notificationMessage, new PropertyChangedEventHandler( OnNotificationMessagePropertyChanged ), "NotificationMessage", VarioSL.Entities.RelationClasses.StaticNotificationMessageOwnerRelations.NotificationMessageEntityUsingNotificationMessageIDStatic, true, signalRelatedEntity, "NotificationMessageOwners", resetFKFields, new int[] { (int)NotificationMessageOwnerFieldIndex.NotificationMessageID } );		
			_notificationMessage = null;
		}
		
		/// <summary> setups the sync logic for member _notificationMessage</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncNotificationMessage(IEntityCore relatedEntity)
		{
			if(_notificationMessage!=relatedEntity)
			{		
				DesetupSyncNotificationMessage(true, true);
				_notificationMessage = (NotificationMessageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _notificationMessage, new PropertyChangedEventHandler( OnNotificationMessagePropertyChanged ), "NotificationMessage", VarioSL.Entities.RelationClasses.StaticNotificationMessageOwnerRelations.NotificationMessageEntityUsingNotificationMessageIDStatic, true, ref _alreadyFetchedNotificationMessage, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnNotificationMessagePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="contractID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="notificationMessageID">PK value for NotificationMessageOwner which data should be fetched into this NotificationMessageOwner object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 contractID, System.Int64 notificationMessageID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)NotificationMessageOwnerFieldIndex.ContractID].ForcedCurrentValueWrite(contractID);
				this.Fields[(int)NotificationMessageOwnerFieldIndex.NotificationMessageID].ForcedCurrentValueWrite(notificationMessageID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateNotificationMessageOwnerDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new NotificationMessageOwnerEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static NotificationMessageOwnerRelations Relations
		{
			get	{ return new NotificationMessageOwnerRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.NotificationMessageOwnerEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NotificationMessage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNotificationMessage
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.NotificationMessageCollection(), (IEntityRelation)GetRelationsForField("NotificationMessage")[0], (int)VarioSL.Entities.EntityType.NotificationMessageOwnerEntity, (int)VarioSL.Entities.EntityType.NotificationMessageEntity, 0, null, null, null, "NotificationMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ContractID property of the Entity NotificationMessageOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGEOWNER"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 ContractID
		{
			get { return (System.Int64)GetValue((int)NotificationMessageOwnerFieldIndex.ContractID, true); }
			set	{ SetValue((int)NotificationMessageOwnerFieldIndex.ContractID, value, true); }
		}

		/// <summary> The IsAcknowledged property of the Entity NotificationMessageOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGEOWNER"."ISACKNOWLEDGED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAcknowledged
		{
			get { return (System.Boolean)GetValue((int)NotificationMessageOwnerFieldIndex.IsAcknowledged, true); }
			set	{ SetValue((int)NotificationMessageOwnerFieldIndex.IsAcknowledged, value, true); }
		}

		/// <summary> The IsDeleted property of the Entity NotificationMessageOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGEOWNER"."ISDELETED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsDeleted
		{
			get { return (Nullable<System.Boolean>)GetValue((int)NotificationMessageOwnerFieldIndex.IsDeleted, false); }
			set	{ SetValue((int)NotificationMessageOwnerFieldIndex.IsDeleted, value, true); }
		}

		/// <summary> The NotificationMessageID property of the Entity NotificationMessageOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGEOWNER"."NOTIFICATIONMESSAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 NotificationMessageID
		{
			get { return (System.Int64)GetValue((int)NotificationMessageOwnerFieldIndex.NotificationMessageID, true); }
			set	{ SetValue((int)NotificationMessageOwnerFieldIndex.NotificationMessageID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "NotificationMessageOwners", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'NotificationMessageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleNotificationMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual NotificationMessageEntity NotificationMessage
		{
			get	{ return GetSingleNotificationMessage(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncNotificationMessage(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "NotificationMessageOwners", "NotificationMessage", _notificationMessage, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for NotificationMessage. When set to true, NotificationMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NotificationMessage is accessed. You can always execute a forced fetch by calling GetSingleNotificationMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNotificationMessage
		{
			get	{ return _alwaysFetchNotificationMessage; }
			set	{ _alwaysFetchNotificationMessage = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property NotificationMessage already has been fetched. Setting this property to false when NotificationMessage has been fetched
		/// will set NotificationMessage to null as well. Setting this property to true while NotificationMessage hasn't been fetched disables lazy loading for NotificationMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNotificationMessage
		{
			get { return _alreadyFetchedNotificationMessage;}
			set 
			{
				if(_alreadyFetchedNotificationMessage && !value)
				{
					this.NotificationMessage = null;
				}
				_alreadyFetchedNotificationMessage = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property NotificationMessage is not found
		/// in the database. When set to true, NotificationMessage will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool NotificationMessageReturnsNewIfNotFound
		{
			get	{ return _notificationMessageReturnsNewIfNotFound; }
			set { _notificationMessageReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.NotificationMessageOwnerEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
