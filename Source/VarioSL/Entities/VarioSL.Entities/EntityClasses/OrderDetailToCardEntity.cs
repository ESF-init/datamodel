﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'OrderDetailToCard'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class OrderDetailToCardEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.OrderDetailToCardCollection	_orderDetailToCards;
		private bool	_alwaysFetchOrderDetailToCards, _alreadyFetchedOrderDetailToCards;
		private OrderDetailToCardEntity _orderDetailToCard;
		private bool	_alwaysFetchOrderDetailToCard, _alreadyFetchedOrderDetailToCard, _orderDetailToCardReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name OrderDetailToCard</summary>
			public static readonly string OrderDetailToCard = "OrderDetailToCard";
			/// <summary>Member name OrderDetailToCards</summary>
			public static readonly string OrderDetailToCards = "OrderDetailToCards";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OrderDetailToCardEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public OrderDetailToCardEntity() :base("OrderDetailToCardEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="orderDetailToCardID">PK value for OrderDetailToCard which data should be fetched into this OrderDetailToCard object</param>
		public OrderDetailToCardEntity(System.Int64 orderDetailToCardID):base("OrderDetailToCardEntity")
		{
			InitClassFetch(orderDetailToCardID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderDetailToCardID">PK value for OrderDetailToCard which data should be fetched into this OrderDetailToCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OrderDetailToCardEntity(System.Int64 orderDetailToCardID, IPrefetchPath prefetchPathToUse):base("OrderDetailToCardEntity")
		{
			InitClassFetch(orderDetailToCardID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="orderDetailToCardID">PK value for OrderDetailToCard which data should be fetched into this OrderDetailToCard object</param>
		/// <param name="validator">The custom validator object for this OrderDetailToCardEntity</param>
		public OrderDetailToCardEntity(System.Int64 orderDetailToCardID, IValidator validator):base("OrderDetailToCardEntity")
		{
			InitClassFetch(orderDetailToCardID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderDetailToCardEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_orderDetailToCards = (VarioSL.Entities.CollectionClasses.OrderDetailToCardCollection)info.GetValue("_orderDetailToCards", typeof(VarioSL.Entities.CollectionClasses.OrderDetailToCardCollection));
			_alwaysFetchOrderDetailToCards = info.GetBoolean("_alwaysFetchOrderDetailToCards");
			_alreadyFetchedOrderDetailToCards = info.GetBoolean("_alreadyFetchedOrderDetailToCards");
			_orderDetailToCard = (OrderDetailToCardEntity)info.GetValue("_orderDetailToCard", typeof(OrderDetailToCardEntity));
			if(_orderDetailToCard!=null)
			{
				_orderDetailToCard.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderDetailToCardReturnsNewIfNotFound = info.GetBoolean("_orderDetailToCardReturnsNewIfNotFound");
			_alwaysFetchOrderDetailToCard = info.GetBoolean("_alwaysFetchOrderDetailToCard");
			_alreadyFetchedOrderDetailToCard = info.GetBoolean("_alreadyFetchedOrderDetailToCard");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((OrderDetailToCardFieldIndex)fieldIndex)
			{
				case OrderDetailToCardFieldIndex.ReplacedOrderDetailToCardID:
					DesetupSyncOrderDetailToCard(true, false);
					_alreadyFetchedOrderDetailToCard = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedOrderDetailToCards = (_orderDetailToCards.Count > 0);
			_alreadyFetchedOrderDetailToCard = (_orderDetailToCard != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "OrderDetailToCard":
					toReturn.Add(Relations.OrderDetailToCardEntityUsingOrderDetailToCardIDReplacedOrderDetailToCardID);
					break;
				case "OrderDetailToCards":
					toReturn.Add(Relations.OrderDetailToCardEntityUsingReplacedOrderDetailToCardID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_orderDetailToCards", (!this.MarkedForDeletion?_orderDetailToCards:null));
			info.AddValue("_alwaysFetchOrderDetailToCards", _alwaysFetchOrderDetailToCards);
			info.AddValue("_alreadyFetchedOrderDetailToCards", _alreadyFetchedOrderDetailToCards);
			info.AddValue("_orderDetailToCard", (!this.MarkedForDeletion?_orderDetailToCard:null));
			info.AddValue("_orderDetailToCardReturnsNewIfNotFound", _orderDetailToCardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrderDetailToCard", _alwaysFetchOrderDetailToCard);
			info.AddValue("_alreadyFetchedOrderDetailToCard", _alreadyFetchedOrderDetailToCard);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "OrderDetailToCard":
					_alreadyFetchedOrderDetailToCard = true;
					this.OrderDetailToCard = (OrderDetailToCardEntity)entity;
					break;
				case "OrderDetailToCards":
					_alreadyFetchedOrderDetailToCards = true;
					if(entity!=null)
					{
						this.OrderDetailToCards.Add((OrderDetailToCardEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "OrderDetailToCard":
					SetupSyncOrderDetailToCard(relatedEntity);
					break;
				case "OrderDetailToCards":
					_orderDetailToCards.Add((OrderDetailToCardEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "OrderDetailToCard":
					DesetupSyncOrderDetailToCard(false, true);
					break;
				case "OrderDetailToCards":
					this.PerformRelatedEntityRemoval(_orderDetailToCards, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_orderDetailToCard!=null)
			{
				toReturn.Add(_orderDetailToCard);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_orderDetailToCards);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="cardID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="jobID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="orderDetailID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCCardIDJobIDOrderDetailID(Nullable<System.Int64> cardID, Nullable<System.Int64> jobID, System.Int64 orderDetailID)
		{
			return FetchUsingUCCardIDJobIDOrderDetailID( cardID,  jobID,  orderDetailID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="cardID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="jobID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="orderDetailID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCCardIDJobIDOrderDetailID(Nullable<System.Int64> cardID, Nullable<System.Int64> jobID, System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCCardIDJobIDOrderDetailID( cardID,  jobID,  orderDetailID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="cardID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="jobID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="orderDetailID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCCardIDJobIDOrderDetailID(Nullable<System.Int64> cardID, Nullable<System.Int64> jobID, System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCCardIDJobIDOrderDetailID( cardID,  jobID,  orderDetailID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="cardID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="jobID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="orderDetailID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCCardIDJobIDOrderDetailID(Nullable<System.Int64> cardID, Nullable<System.Int64> jobID, System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((OrderDetailToCardDAO)CreateDAOInstance()).FetchOrderDetailToCardUsingUCCardIDJobIDOrderDetailID(this, this.Transaction, cardID, jobID, orderDetailID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderDetailToCardID">PK value for OrderDetailToCard which data should be fetched into this OrderDetailToCard object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 orderDetailToCardID)
		{
			return FetchUsingPK(orderDetailToCardID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderDetailToCardID">PK value for OrderDetailToCard which data should be fetched into this OrderDetailToCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 orderDetailToCardID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(orderDetailToCardID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderDetailToCardID">PK value for OrderDetailToCard which data should be fetched into this OrderDetailToCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 orderDetailToCardID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(orderDetailToCardID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderDetailToCardID">PK value for OrderDetailToCard which data should be fetched into this OrderDetailToCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 orderDetailToCardID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(orderDetailToCardID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OrderDetailToCardID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OrderDetailToCardRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailToCardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailToCardCollection GetMultiOrderDetailToCards(bool forceFetch)
		{
			return GetMultiOrderDetailToCards(forceFetch, _orderDetailToCards.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailToCardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailToCardCollection GetMultiOrderDetailToCards(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderDetailToCards(forceFetch, _orderDetailToCards.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailToCardCollection GetMultiOrderDetailToCards(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderDetailToCards(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailToCardCollection GetMultiOrderDetailToCards(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderDetailToCards || forceFetch || _alwaysFetchOrderDetailToCards) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderDetailToCards);
				_orderDetailToCards.SuppressClearInGetMulti=!forceFetch;
				_orderDetailToCards.EntityFactoryToUse = entityFactoryToUse;
				_orderDetailToCards.GetMultiManyToOne(this, filter);
				_orderDetailToCards.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderDetailToCards = true;
			}
			return _orderDetailToCards;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderDetailToCards'. These settings will be taken into account
		/// when the property OrderDetailToCards is requested or GetMultiOrderDetailToCards is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderDetailToCards(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderDetailToCards.SortClauses=sortClauses;
			_orderDetailToCards.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'OrderDetailToCardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderDetailToCardEntity' which is related to this entity.</returns>
		public OrderDetailToCardEntity GetSingleOrderDetailToCard()
		{
			return GetSingleOrderDetailToCard(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderDetailToCardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderDetailToCardEntity' which is related to this entity.</returns>
		public virtual OrderDetailToCardEntity GetSingleOrderDetailToCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrderDetailToCard || forceFetch || _alwaysFetchOrderDetailToCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderDetailToCardEntityUsingOrderDetailToCardIDReplacedOrderDetailToCardID);
				OrderDetailToCardEntity newEntity = new OrderDetailToCardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReplacedOrderDetailToCardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OrderDetailToCardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderDetailToCardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrderDetailToCard = newEntity;
				_alreadyFetchedOrderDetailToCard = fetchResult;
			}
			return _orderDetailToCard;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("OrderDetailToCard", _orderDetailToCard);
			toReturn.Add("OrderDetailToCards", _orderDetailToCards);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="orderDetailToCardID">PK value for OrderDetailToCard which data should be fetched into this OrderDetailToCard object</param>
		/// <param name="validator">The validator object for this OrderDetailToCardEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 orderDetailToCardID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(orderDetailToCardID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_orderDetailToCards = new VarioSL.Entities.CollectionClasses.OrderDetailToCardCollection();
			_orderDetailToCards.SetContainingEntityInfo(this, "OrderDetailToCard");
			_orderDetailToCardReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderDetailID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderDetailToCardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReplacedOrderDetailToCardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _orderDetailToCard</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrderDetailToCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _orderDetailToCard, new PropertyChangedEventHandler( OnOrderDetailToCardPropertyChanged ), "OrderDetailToCard", VarioSL.Entities.RelationClasses.StaticOrderDetailToCardRelations.OrderDetailToCardEntityUsingOrderDetailToCardIDReplacedOrderDetailToCardIDStatic, true, signalRelatedEntity, "OrderDetailToCards", resetFKFields, new int[] { (int)OrderDetailToCardFieldIndex.ReplacedOrderDetailToCardID } );		
			_orderDetailToCard = null;
		}
		
		/// <summary> setups the sync logic for member _orderDetailToCard</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrderDetailToCard(IEntityCore relatedEntity)
		{
			if(_orderDetailToCard!=relatedEntity)
			{		
				DesetupSyncOrderDetailToCard(true, true);
				_orderDetailToCard = (OrderDetailToCardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _orderDetailToCard, new PropertyChangedEventHandler( OnOrderDetailToCardPropertyChanged ), "OrderDetailToCard", VarioSL.Entities.RelationClasses.StaticOrderDetailToCardRelations.OrderDetailToCardEntityUsingOrderDetailToCardIDReplacedOrderDetailToCardIDStatic, true, ref _alreadyFetchedOrderDetailToCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderDetailToCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="orderDetailToCardID">PK value for OrderDetailToCard which data should be fetched into this OrderDetailToCard object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 orderDetailToCardID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OrderDetailToCardFieldIndex.OrderDetailToCardID].ForcedCurrentValueWrite(orderDetailToCardID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrderDetailToCardDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OrderDetailToCardEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OrderDetailToCardRelations Relations
		{
			get	{ return new OrderDetailToCardRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetailToCard' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderDetailToCards
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailToCardCollection(), (IEntityRelation)GetRelationsForField("OrderDetailToCards")[0], (int)VarioSL.Entities.EntityType.OrderDetailToCardEntity, (int)VarioSL.Entities.EntityType.OrderDetailToCardEntity, 0, null, null, null, "OrderDetailToCards", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetailToCard'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderDetailToCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailToCardCollection(), (IEntityRelation)GetRelationsForField("OrderDetailToCard")[0], (int)VarioSL.Entities.EntityType.OrderDetailToCardEntity, (int)VarioSL.Entities.EntityType.OrderDetailToCardEntity, 0, null, null, null, "OrderDetailToCard", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CardID property of the Entity OrderDetailToCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAILTOCARD"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrderDetailToCardFieldIndex.CardID, false); }
			set	{ SetValue((int)OrderDetailToCardFieldIndex.CardID, value, true); }
		}

		/// <summary> The JobID property of the Entity OrderDetailToCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAILTOCARD"."JOBID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> JobID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrderDetailToCardFieldIndex.JobID, false); }
			set	{ SetValue((int)OrderDetailToCardFieldIndex.JobID, value, true); }
		}

		/// <summary> The OrderDetailID property of the Entity OrderDetailToCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAILTOCARD"."ORDERDETAILID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 OrderDetailID
		{
			get { return (System.Int64)GetValue((int)OrderDetailToCardFieldIndex.OrderDetailID, true); }
			set	{ SetValue((int)OrderDetailToCardFieldIndex.OrderDetailID, value, true); }
		}

		/// <summary> The OrderDetailToCardID property of the Entity OrderDetailToCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAILTOCARD"."ORDERDETAILTOCARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 OrderDetailToCardID
		{
			get { return (System.Int64)GetValue((int)OrderDetailToCardFieldIndex.OrderDetailToCardID, true); }
			set	{ SetValue((int)OrderDetailToCardFieldIndex.OrderDetailToCardID, value, true); }
		}

		/// <summary> The ReplacedOrderDetailToCardID property of the Entity OrderDetailToCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAILTOCARD"."REPLACEDORDERDETAILTOCARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReplacedOrderDetailToCardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrderDetailToCardFieldIndex.ReplacedOrderDetailToCardID, false); }
			set	{ SetValue((int)OrderDetailToCardFieldIndex.ReplacedOrderDetailToCardID, value, true); }
		}

		/// <summary> The Status property of the Entity OrderDetailToCard<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAILTOCARD"."STATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CardFulfillmentStatus Status
		{
			get { return (VarioSL.Entities.Enumerations.CardFulfillmentStatus)GetValue((int)OrderDetailToCardFieldIndex.Status, true); }
			set	{ SetValue((int)OrderDetailToCardFieldIndex.Status, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderDetailToCards()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailToCardCollection OrderDetailToCards
		{
			get	{ return GetMultiOrderDetailToCards(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderDetailToCards. When set to true, OrderDetailToCards is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderDetailToCards is accessed. You can always execute/ a forced fetch by calling GetMultiOrderDetailToCards(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderDetailToCards
		{
			get	{ return _alwaysFetchOrderDetailToCards; }
			set	{ _alwaysFetchOrderDetailToCards = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderDetailToCards already has been fetched. Setting this property to false when OrderDetailToCards has been fetched
		/// will clear the OrderDetailToCards collection well. Setting this property to true while OrderDetailToCards hasn't been fetched disables lazy loading for OrderDetailToCards</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderDetailToCards
		{
			get { return _alreadyFetchedOrderDetailToCards;}
			set 
			{
				if(_alreadyFetchedOrderDetailToCards && !value && (_orderDetailToCards != null))
				{
					_orderDetailToCards.Clear();
				}
				_alreadyFetchedOrderDetailToCards = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'OrderDetailToCardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrderDetailToCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OrderDetailToCardEntity OrderDetailToCard
		{
			get	{ return GetSingleOrderDetailToCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrderDetailToCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderDetailToCards", "OrderDetailToCard", _orderDetailToCard, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrderDetailToCard. When set to true, OrderDetailToCard is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderDetailToCard is accessed. You can always execute a forced fetch by calling GetSingleOrderDetailToCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderDetailToCard
		{
			get	{ return _alwaysFetchOrderDetailToCard; }
			set	{ _alwaysFetchOrderDetailToCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderDetailToCard already has been fetched. Setting this property to false when OrderDetailToCard has been fetched
		/// will set OrderDetailToCard to null as well. Setting this property to true while OrderDetailToCard hasn't been fetched disables lazy loading for OrderDetailToCard</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderDetailToCard
		{
			get { return _alreadyFetchedOrderDetailToCard;}
			set 
			{
				if(_alreadyFetchedOrderDetailToCard && !value)
				{
					this.OrderDetailToCard = null;
				}
				_alreadyFetchedOrderDetailToCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrderDetailToCard is not found
		/// in the database. When set to true, OrderDetailToCard will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OrderDetailToCardReturnsNewIfNotFound
		{
			get	{ return _orderDetailToCardReturnsNewIfNotFound; }
			set { _orderDetailToCardReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.OrderDetailToCardEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
