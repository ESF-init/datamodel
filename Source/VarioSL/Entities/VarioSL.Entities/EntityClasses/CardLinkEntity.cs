﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CardLink'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CardLinkEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CardEntity _sourceCard;
		private bool	_alwaysFetchSourceCard, _alreadyFetchedSourceCard, _sourceCardReturnsNewIfNotFound;
		private CardEntity _targetCard;
		private bool	_alwaysFetchTargetCard, _alreadyFetchedTargetCard, _targetCardReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SourceCard</summary>
			public static readonly string SourceCard = "SourceCard";
			/// <summary>Member name TargetCard</summary>
			public static readonly string TargetCard = "TargetCard";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CardLinkEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CardLinkEntity() :base("CardLinkEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="targetCardID">PK value for CardLink which data should be fetched into this CardLink object</param>
		public CardLinkEntity(System.Int64 targetCardID):base("CardLinkEntity")
		{
			InitClassFetch(targetCardID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="targetCardID">PK value for CardLink which data should be fetched into this CardLink object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CardLinkEntity(System.Int64 targetCardID, IPrefetchPath prefetchPathToUse):base("CardLinkEntity")
		{
			InitClassFetch(targetCardID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="targetCardID">PK value for CardLink which data should be fetched into this CardLink object</param>
		/// <param name="validator">The custom validator object for this CardLinkEntity</param>
		public CardLinkEntity(System.Int64 targetCardID, IValidator validator):base("CardLinkEntity")
		{
			InitClassFetch(targetCardID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CardLinkEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_sourceCard = (CardEntity)info.GetValue("_sourceCard", typeof(CardEntity));
			if(_sourceCard!=null)
			{
				_sourceCard.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_sourceCardReturnsNewIfNotFound = info.GetBoolean("_sourceCardReturnsNewIfNotFound");
			_alwaysFetchSourceCard = info.GetBoolean("_alwaysFetchSourceCard");
			_alreadyFetchedSourceCard = info.GetBoolean("_alreadyFetchedSourceCard");
			_targetCard = (CardEntity)info.GetValue("_targetCard", typeof(CardEntity));
			if(_targetCard!=null)
			{
				_targetCard.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_targetCardReturnsNewIfNotFound = info.GetBoolean("_targetCardReturnsNewIfNotFound");
			_alwaysFetchTargetCard = info.GetBoolean("_alwaysFetchTargetCard");
			_alreadyFetchedTargetCard = info.GetBoolean("_alreadyFetchedTargetCard");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CardLinkFieldIndex)fieldIndex)
			{
				case CardLinkFieldIndex.SourceCardID:
					DesetupSyncSourceCard(true, false);
					_alreadyFetchedSourceCard = false;
					break;
				case CardLinkFieldIndex.TargetCardID:
					DesetupSyncTargetCard(true, false);
					_alreadyFetchedTargetCard = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSourceCard = (_sourceCard != null);
			_alreadyFetchedTargetCard = (_targetCard != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SourceCard":
					toReturn.Add(Relations.CardEntityUsingSourceCardID);
					break;
				case "TargetCard":
					toReturn.Add(Relations.CardEntityUsingTargetCardID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_sourceCard", (!this.MarkedForDeletion?_sourceCard:null));
			info.AddValue("_sourceCardReturnsNewIfNotFound", _sourceCardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSourceCard", _alwaysFetchSourceCard);
			info.AddValue("_alreadyFetchedSourceCard", _alreadyFetchedSourceCard);

			info.AddValue("_targetCard", (!this.MarkedForDeletion?_targetCard:null));
			info.AddValue("_targetCardReturnsNewIfNotFound", _targetCardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTargetCard", _alwaysFetchTargetCard);
			info.AddValue("_alreadyFetchedTargetCard", _alreadyFetchedTargetCard);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SourceCard":
					_alreadyFetchedSourceCard = true;
					this.SourceCard = (CardEntity)entity;
					break;
				case "TargetCard":
					_alreadyFetchedTargetCard = true;
					this.TargetCard = (CardEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SourceCard":
					SetupSyncSourceCard(relatedEntity);
					break;
				case "TargetCard":
					SetupSyncTargetCard(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SourceCard":
					DesetupSyncSourceCard(false, true);
					break;
				case "TargetCard":
					DesetupSyncTargetCard(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_sourceCard!=null)
			{
				toReturn.Add(_sourceCard);
			}
			if(_targetCard!=null)
			{
				toReturn.Add(_targetCard);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="targetCardID">PK value for CardLink which data should be fetched into this CardLink object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 targetCardID)
		{
			return FetchUsingPK(targetCardID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="targetCardID">PK value for CardLink which data should be fetched into this CardLink object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 targetCardID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(targetCardID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="targetCardID">PK value for CardLink which data should be fetched into this CardLink object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 targetCardID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(targetCardID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="targetCardID">PK value for CardLink which data should be fetched into this CardLink object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 targetCardID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(targetCardID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TargetCardID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CardLinkRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleSourceCard()
		{
			return GetSingleSourceCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleSourceCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedSourceCard || forceFetch || _alwaysFetchSourceCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingSourceCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SourceCardID);
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_sourceCardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SourceCard = newEntity;
				_alreadyFetchedSourceCard = fetchResult;
			}
			return _sourceCard;
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleTargetCard()
		{
			return GetSingleTargetCard(false);
		}
		
		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleTargetCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedTargetCard || forceFetch || _alwaysFetchTargetCard) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingTargetCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TargetCardID);
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_targetCardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TargetCard = newEntity;
				_alreadyFetchedTargetCard = fetchResult;
			}
			return _targetCard;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SourceCard", _sourceCard);
			toReturn.Add("TargetCard", _targetCard);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="targetCardID">PK value for CardLink which data should be fetched into this CardLink object</param>
		/// <param name="validator">The validator object for this CardLinkEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 targetCardID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(targetCardID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_sourceCardReturnsNewIfNotFound = false;
			_targetCardReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SourceCardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TargetCardID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _sourceCard</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSourceCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _sourceCard, new PropertyChangedEventHandler( OnSourceCardPropertyChanged ), "SourceCard", VarioSL.Entities.RelationClasses.StaticCardLinkRelations.CardEntityUsingSourceCardIDStatic, true, signalRelatedEntity, "CardLinksSource", resetFKFields, new int[] { (int)CardLinkFieldIndex.SourceCardID } );		
			_sourceCard = null;
		}
		
		/// <summary> setups the sync logic for member _sourceCard</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSourceCard(IEntityCore relatedEntity)
		{
			if(_sourceCard!=relatedEntity)
			{		
				DesetupSyncSourceCard(true, true);
				_sourceCard = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _sourceCard, new PropertyChangedEventHandler( OnSourceCardPropertyChanged ), "SourceCard", VarioSL.Entities.RelationClasses.StaticCardLinkRelations.CardEntityUsingSourceCardIDStatic, true, ref _alreadyFetchedSourceCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSourceCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _targetCard</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTargetCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _targetCard, new PropertyChangedEventHandler( OnTargetCardPropertyChanged ), "TargetCard", VarioSL.Entities.RelationClasses.StaticCardLinkRelations.CardEntityUsingTargetCardIDStatic, true, signalRelatedEntity, "CardLinkTarget", false, new int[] { (int)CardLinkFieldIndex.TargetCardID } );
			_targetCard = null;
		}
	
		/// <summary> setups the sync logic for member _targetCard</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTargetCard(IEntityCore relatedEntity)
		{
			if(_targetCard!=relatedEntity)
			{
				DesetupSyncTargetCard(true, true);
				_targetCard = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _targetCard, new PropertyChangedEventHandler( OnTargetCardPropertyChanged ), "TargetCard", VarioSL.Entities.RelationClasses.StaticCardLinkRelations.CardEntityUsingTargetCardIDStatic, true, ref _alreadyFetchedTargetCard, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTargetCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="targetCardID">PK value for CardLink which data should be fetched into this CardLink object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 targetCardID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CardLinkFieldIndex.TargetCardID].ForcedCurrentValueWrite(targetCardID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCardLinkDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CardLinkEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CardLinkRelations Relations
		{
			get	{ return new CardLinkRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSourceCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("SourceCard")[0], (int)VarioSL.Entities.EntityType.CardLinkEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "SourceCard", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTargetCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("TargetCard")[0], (int)VarioSL.Entities.EntityType.CardLinkEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "TargetCard", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SourceCardID property of the Entity CardLink<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDLINK"."SOURCECARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SourceCardID
		{
			get { return (System.Int64)GetValue((int)CardLinkFieldIndex.SourceCardID, true); }
			set	{ SetValue((int)CardLinkFieldIndex.SourceCardID, value, true); }
		}

		/// <summary> The TargetCardID property of the Entity CardLink<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDLINK"."TARGETCARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 TargetCardID
		{
			get { return (System.Int64)GetValue((int)CardLinkFieldIndex.TargetCardID, true); }
			set	{ SetValue((int)CardLinkFieldIndex.TargetCardID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSourceCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity SourceCard
		{
			get	{ return GetSingleSourceCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSourceCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CardLinksSource", "SourceCard", _sourceCard, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SourceCard. When set to true, SourceCard is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SourceCard is accessed. You can always execute a forced fetch by calling GetSingleSourceCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSourceCard
		{
			get	{ return _alwaysFetchSourceCard; }
			set	{ _alwaysFetchSourceCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SourceCard already has been fetched. Setting this property to false when SourceCard has been fetched
		/// will set SourceCard to null as well. Setting this property to true while SourceCard hasn't been fetched disables lazy loading for SourceCard</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSourceCard
		{
			get { return _alreadyFetchedSourceCard;}
			set 
			{
				if(_alreadyFetchedSourceCard && !value)
				{
					this.SourceCard = null;
				}
				_alreadyFetchedSourceCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SourceCard is not found
		/// in the database. When set to true, SourceCard will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SourceCardReturnsNewIfNotFound
		{
			get	{ return _sourceCardReturnsNewIfNotFound; }
			set { _sourceCardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTargetCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity TargetCard
		{
			get	{ return GetSingleTargetCard(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncTargetCard(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_targetCard !=null);
						DesetupSyncTargetCard(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("TargetCard");
						}
					}
					else
					{
						if(_targetCard!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "CardLinkTarget");
							SetupSyncTargetCard(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TargetCard. When set to true, TargetCard is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TargetCard is accessed. You can always execute a forced fetch by calling GetSingleTargetCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTargetCard
		{
			get	{ return _alwaysFetchTargetCard; }
			set	{ _alwaysFetchTargetCard = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property TargetCard already has been fetched. Setting this property to false when TargetCard has been fetched
		/// will set TargetCard to null as well. Setting this property to true while TargetCard hasn't been fetched disables lazy loading for TargetCard</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTargetCard
		{
			get { return _alreadyFetchedTargetCard;}
			set 
			{
				if(_alreadyFetchedTargetCard && !value)
				{
					this.TargetCard = null;
				}
				_alreadyFetchedTargetCard = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TargetCard is not found
		/// in the database. When set to true, TargetCard will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TargetCardReturnsNewIfNotFound
		{
			get	{ return _targetCardReturnsNewIfNotFound; }
			set	{ _targetCardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CardLinkEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
