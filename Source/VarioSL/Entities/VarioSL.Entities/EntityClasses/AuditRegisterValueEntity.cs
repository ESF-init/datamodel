﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AuditRegisterValue'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AuditRegisterValueEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AuditRegisterEntity _auditRegister;
		private bool	_alwaysFetchAuditRegister, _alreadyFetchedAuditRegister, _auditRegisterReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AuditRegister</summary>
			public static readonly string AuditRegister = "AuditRegister";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AuditRegisterValueEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AuditRegisterValueEntity() :base("AuditRegisterValueEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="auditRegisterValueID">PK value for AuditRegisterValue which data should be fetched into this AuditRegisterValue object</param>
		public AuditRegisterValueEntity(System.Int64 auditRegisterValueID):base("AuditRegisterValueEntity")
		{
			InitClassFetch(auditRegisterValueID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="auditRegisterValueID">PK value for AuditRegisterValue which data should be fetched into this AuditRegisterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AuditRegisterValueEntity(System.Int64 auditRegisterValueID, IPrefetchPath prefetchPathToUse):base("AuditRegisterValueEntity")
		{
			InitClassFetch(auditRegisterValueID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="auditRegisterValueID">PK value for AuditRegisterValue which data should be fetched into this AuditRegisterValue object</param>
		/// <param name="validator">The custom validator object for this AuditRegisterValueEntity</param>
		public AuditRegisterValueEntity(System.Int64 auditRegisterValueID, IValidator validator):base("AuditRegisterValueEntity")
		{
			InitClassFetch(auditRegisterValueID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AuditRegisterValueEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_auditRegister = (AuditRegisterEntity)info.GetValue("_auditRegister", typeof(AuditRegisterEntity));
			if(_auditRegister!=null)
			{
				_auditRegister.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_auditRegisterReturnsNewIfNotFound = info.GetBoolean("_auditRegisterReturnsNewIfNotFound");
			_alwaysFetchAuditRegister = info.GetBoolean("_alwaysFetchAuditRegister");
			_alreadyFetchedAuditRegister = info.GetBoolean("_alreadyFetchedAuditRegister");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AuditRegisterValueFieldIndex)fieldIndex)
			{
				case AuditRegisterValueFieldIndex.AuditRegisterID:
					DesetupSyncAuditRegister(true, false);
					_alreadyFetchedAuditRegister = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAuditRegister = (_auditRegister != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AuditRegister":
					toReturn.Add(Relations.AuditRegisterEntityUsingAuditRegisterID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_auditRegister", (!this.MarkedForDeletion?_auditRegister:null));
			info.AddValue("_auditRegisterReturnsNewIfNotFound", _auditRegisterReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAuditRegister", _alwaysFetchAuditRegister);
			info.AddValue("_alreadyFetchedAuditRegister", _alreadyFetchedAuditRegister);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AuditRegister":
					_alreadyFetchedAuditRegister = true;
					this.AuditRegister = (AuditRegisterEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AuditRegister":
					SetupSyncAuditRegister(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AuditRegister":
					DesetupSyncAuditRegister(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_auditRegister!=null)
			{
				toReturn.Add(_auditRegister);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="auditRegisterValueID">PK value for AuditRegisterValue which data should be fetched into this AuditRegisterValue object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 auditRegisterValueID)
		{
			return FetchUsingPK(auditRegisterValueID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="auditRegisterValueID">PK value for AuditRegisterValue which data should be fetched into this AuditRegisterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 auditRegisterValueID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(auditRegisterValueID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="auditRegisterValueID">PK value for AuditRegisterValue which data should be fetched into this AuditRegisterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 auditRegisterValueID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(auditRegisterValueID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="auditRegisterValueID">PK value for AuditRegisterValue which data should be fetched into this AuditRegisterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 auditRegisterValueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(auditRegisterValueID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AuditRegisterValueID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AuditRegisterValueRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AuditRegisterEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AuditRegisterEntity' which is related to this entity.</returns>
		public AuditRegisterEntity GetSingleAuditRegister()
		{
			return GetSingleAuditRegister(false);
		}

		/// <summary> Retrieves the related entity of type 'AuditRegisterEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AuditRegisterEntity' which is related to this entity.</returns>
		public virtual AuditRegisterEntity GetSingleAuditRegister(bool forceFetch)
		{
			if( ( !_alreadyFetchedAuditRegister || forceFetch || _alwaysFetchAuditRegister) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AuditRegisterEntityUsingAuditRegisterID);
				AuditRegisterEntity newEntity = new AuditRegisterEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AuditRegisterID);
				}
				if(fetchResult)
				{
					newEntity = (AuditRegisterEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_auditRegisterReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AuditRegister = newEntity;
				_alreadyFetchedAuditRegister = fetchResult;
			}
			return _auditRegister;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AuditRegister", _auditRegister);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="auditRegisterValueID">PK value for AuditRegisterValue which data should be fetched into this AuditRegisterValue object</param>
		/// <param name="validator">The validator object for this AuditRegisterValueEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 auditRegisterValueID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(auditRegisterValueID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_auditRegisterReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AuditRegisterID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AuditRegisterValueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RegisterValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RegisterValueType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _auditRegister</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAuditRegister(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _auditRegister, new PropertyChangedEventHandler( OnAuditRegisterPropertyChanged ), "AuditRegister", VarioSL.Entities.RelationClasses.StaticAuditRegisterValueRelations.AuditRegisterEntityUsingAuditRegisterIDStatic, true, signalRelatedEntity, "AuditRegisterValues", resetFKFields, new int[] { (int)AuditRegisterValueFieldIndex.AuditRegisterID } );		
			_auditRegister = null;
		}
		
		/// <summary> setups the sync logic for member _auditRegister</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAuditRegister(IEntityCore relatedEntity)
		{
			if(_auditRegister!=relatedEntity)
			{		
				DesetupSyncAuditRegister(true, true);
				_auditRegister = (AuditRegisterEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _auditRegister, new PropertyChangedEventHandler( OnAuditRegisterPropertyChanged ), "AuditRegister", VarioSL.Entities.RelationClasses.StaticAuditRegisterValueRelations.AuditRegisterEntityUsingAuditRegisterIDStatic, true, ref _alreadyFetchedAuditRegister, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAuditRegisterPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="auditRegisterValueID">PK value for AuditRegisterValue which data should be fetched into this AuditRegisterValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 auditRegisterValueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AuditRegisterValueFieldIndex.AuditRegisterValueID].ForcedCurrentValueWrite(auditRegisterValueID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAuditRegisterValueDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AuditRegisterValueEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AuditRegisterValueRelations Relations
		{
			get	{ return new AuditRegisterValueRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AuditRegister'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAuditRegister
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AuditRegisterCollection(), (IEntityRelation)GetRelationsForField("AuditRegister")[0], (int)VarioSL.Entities.EntityType.AuditRegisterValueEntity, (int)VarioSL.Entities.EntityType.AuditRegisterEntity, 0, null, null, null, "AuditRegister", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AuditRegisterID property of the Entity AuditRegisterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTERVALUE"."AUDITREGISTERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 AuditRegisterID
		{
			get { return (System.Int64)GetValue((int)AuditRegisterValueFieldIndex.AuditRegisterID, true); }
			set	{ SetValue((int)AuditRegisterValueFieldIndex.AuditRegisterID, value, true); }
		}

		/// <summary> The AuditRegisterValueID property of the Entity AuditRegisterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTERVALUE"."AUDITREGISTERVALUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 AuditRegisterValueID
		{
			get { return (System.Int64)GetValue((int)AuditRegisterValueFieldIndex.AuditRegisterValueID, true); }
			set	{ SetValue((int)AuditRegisterValueFieldIndex.AuditRegisterValueID, value, true); }
		}

		/// <summary> The LastModified property of the Entity AuditRegisterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTERVALUE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)AuditRegisterValueFieldIndex.LastModified, true); }
			set	{ SetValue((int)AuditRegisterValueFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity AuditRegisterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTERVALUE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)AuditRegisterValueFieldIndex.LastUser, true); }
			set	{ SetValue((int)AuditRegisterValueFieldIndex.LastUser, value, true); }
		}

		/// <summary> The RegisterValue property of the Entity AuditRegisterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTERVALUE"."REGISTERVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String RegisterValue
		{
			get { return (System.String)GetValue((int)AuditRegisterValueFieldIndex.RegisterValue, true); }
			set	{ SetValue((int)AuditRegisterValueFieldIndex.RegisterValue, value, true); }
		}

		/// <summary> The RegisterValueType property of the Entity AuditRegisterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTERVALUE"."REGISTERVALUETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.AuditRegisterValueType RegisterValueType
		{
			get { return (VarioSL.Entities.Enumerations.AuditRegisterValueType)GetValue((int)AuditRegisterValueFieldIndex.RegisterValueType, true); }
			set	{ SetValue((int)AuditRegisterValueFieldIndex.RegisterValueType, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity AuditRegisterValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTERVALUE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)AuditRegisterValueFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)AuditRegisterValueFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AuditRegisterEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAuditRegister()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AuditRegisterEntity AuditRegister
		{
			get	{ return GetSingleAuditRegister(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAuditRegister(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AuditRegisterValues", "AuditRegister", _auditRegister, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AuditRegister. When set to true, AuditRegister is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AuditRegister is accessed. You can always execute a forced fetch by calling GetSingleAuditRegister(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAuditRegister
		{
			get	{ return _alwaysFetchAuditRegister; }
			set	{ _alwaysFetchAuditRegister = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AuditRegister already has been fetched. Setting this property to false when AuditRegister has been fetched
		/// will set AuditRegister to null as well. Setting this property to true while AuditRegister hasn't been fetched disables lazy loading for AuditRegister</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAuditRegister
		{
			get { return _alreadyFetchedAuditRegister;}
			set 
			{
				if(_alreadyFetchedAuditRegister && !value)
				{
					this.AuditRegister = null;
				}
				_alreadyFetchedAuditRegister = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AuditRegister is not found
		/// in the database. When set to true, AuditRegister will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AuditRegisterReturnsNewIfNotFound
		{
			get	{ return _auditRegisterReturnsNewIfNotFound; }
			set { _auditRegisterReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AuditRegisterValueEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
