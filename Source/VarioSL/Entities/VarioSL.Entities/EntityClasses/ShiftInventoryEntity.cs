﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ShiftInventory'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ShiftInventoryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AuditRegisterCollection	_auditRegisters;
		private bool	_alwaysFetchAuditRegisters, _alreadyFetchedAuditRegisters;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AuditRegisters</summary>
			public static readonly string AuditRegisters = "AuditRegisters";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ShiftInventoryEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ShiftInventoryEntity() :base("ShiftInventoryEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="hashValue">PK value for ShiftInventory which data should be fetched into this ShiftInventory object</param>
		public ShiftInventoryEntity(System.String hashValue):base("ShiftInventoryEntity")
		{
			InitClassFetch(hashValue, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="hashValue">PK value for ShiftInventory which data should be fetched into this ShiftInventory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ShiftInventoryEntity(System.String hashValue, IPrefetchPath prefetchPathToUse):base("ShiftInventoryEntity")
		{
			InitClassFetch(hashValue, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="hashValue">PK value for ShiftInventory which data should be fetched into this ShiftInventory object</param>
		/// <param name="validator">The custom validator object for this ShiftInventoryEntity</param>
		public ShiftInventoryEntity(System.String hashValue, IValidator validator):base("ShiftInventoryEntity")
		{
			InitClassFetch(hashValue, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ShiftInventoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_auditRegisters = (VarioSL.Entities.CollectionClasses.AuditRegisterCollection)info.GetValue("_auditRegisters", typeof(VarioSL.Entities.CollectionClasses.AuditRegisterCollection));
			_alwaysFetchAuditRegisters = info.GetBoolean("_alwaysFetchAuditRegisters");
			_alreadyFetchedAuditRegisters = info.GetBoolean("_alreadyFetchedAuditRegisters");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAuditRegisters = (_auditRegisters.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AuditRegisters":
					toReturn.Add(Relations.AuditRegisterEntityUsingShiftID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_auditRegisters", (!this.MarkedForDeletion?_auditRegisters:null));
			info.AddValue("_alwaysFetchAuditRegisters", _alwaysFetchAuditRegisters);
			info.AddValue("_alreadyFetchedAuditRegisters", _alreadyFetchedAuditRegisters);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AuditRegisters":
					_alreadyFetchedAuditRegisters = true;
					if(entity!=null)
					{
						this.AuditRegisters.Add((AuditRegisterEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AuditRegisters":
					_auditRegisters.Add((AuditRegisterEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AuditRegisters":
					this.PerformRelatedEntityRemoval(_auditRegisters, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_auditRegisters);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="hashValue">PK value for ShiftInventory which data should be fetched into this ShiftInventory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.String hashValue)
		{
			return FetchUsingPK(hashValue, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="hashValue">PK value for ShiftInventory which data should be fetched into this ShiftInventory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.String hashValue, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(hashValue, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="hashValue">PK value for ShiftInventory which data should be fetched into this ShiftInventory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.String hashValue, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(hashValue, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="hashValue">PK value for ShiftInventory which data should be fetched into this ShiftInventory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.String hashValue, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(hashValue, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.HashValue, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ShiftInventoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AuditRegisterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AuditRegisterEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AuditRegisterCollection GetMultiAuditRegisters(bool forceFetch)
		{
			return GetMultiAuditRegisters(forceFetch, _auditRegisters.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AuditRegisterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AuditRegisterEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AuditRegisterCollection GetMultiAuditRegisters(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAuditRegisters(forceFetch, _auditRegisters.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AuditRegisterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AuditRegisterCollection GetMultiAuditRegisters(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAuditRegisters(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AuditRegisterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AuditRegisterCollection GetMultiAuditRegisters(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAuditRegisters || forceFetch || _alwaysFetchAuditRegisters) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_auditRegisters);
				_auditRegisters.SuppressClearInGetMulti=!forceFetch;
				_auditRegisters.EntityFactoryToUse = entityFactoryToUse;
				_auditRegisters.GetMultiManyToOne(this, filter);
				_auditRegisters.SuppressClearInGetMulti=false;
				_alreadyFetchedAuditRegisters = true;
			}
			return _auditRegisters;
		}

		/// <summary> Sets the collection parameters for the collection for 'AuditRegisters'. These settings will be taken into account
		/// when the property AuditRegisters is requested or GetMultiAuditRegisters is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAuditRegisters(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_auditRegisters.SortClauses=sortClauses;
			_auditRegisters.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AuditRegisters", _auditRegisters);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="hashValue">PK value for ShiftInventory which data should be fetched into this ShiftInventory object</param>
		/// <param name="validator">The validator object for this ShiftInventoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.String hashValue, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(hashValue, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_auditRegisters = new VarioSL.Entities.CollectionClasses.AuditRegisterCollection();
			_auditRegisters.SetContainingEntityInfo(this, "ShiftInventory");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankCardRevenue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashReceipt", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerCardRevenue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClass", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FirstSequentialNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HashValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDst", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTraining", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastSequentialNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesVolume", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftBegin", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftEnd", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Shiftid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VehicleNo", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="hashValue">PK value for ShiftInventory which data should be fetched into this ShiftInventory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.String hashValue, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ShiftInventoryFieldIndex.HashValue].ForcedCurrentValueWrite(hashValue);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateShiftInventoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ShiftInventoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ShiftInventoryRelations Relations
		{
			get	{ return new ShiftInventoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AuditRegister' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAuditRegisters
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AuditRegisterCollection(), (IEntityRelation)GetRelationsForField("AuditRegisters")[0], (int)VarioSL.Entities.EntityType.ShiftInventoryEntity, (int)VarioSL.Entities.EntityType.AuditRegisterEntity, 0, null, null, null, "AuditRegisters", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BankCardRevenue property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."BANKCARDREVENUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BankCardRevenue
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftInventoryFieldIndex.BankCardRevenue, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.BankCardRevenue, value, true); }
		}

		/// <summary> The CardNo property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."CARDNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftInventoryFieldIndex.CardNo, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.CardNo, value, true); }
		}

		/// <summary> The CashReceipt property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."CASHRECEIPT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashReceipt
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftInventoryFieldIndex.CashReceipt, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.CashReceipt, value, true); }
		}

		/// <summary> The CustomerCardRevenue property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."CUSTOMERCARDREVENUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CustomerCardRevenue
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftInventoryFieldIndex.CustomerCardRevenue, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.CustomerCardRevenue, value, true); }
		}

		/// <summary> The DebtorNo property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."DEBTORNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DebtorNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftInventoryFieldIndex.DebtorNo, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.DebtorNo, value, true); }
		}

		/// <summary> The DeviceClass property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."DEVICECLASS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> DeviceClass
		{
			get { return (Nullable<System.Int16>)GetValue((int)ShiftInventoryFieldIndex.DeviceClass, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.DeviceClass, value, true); }
		}

		/// <summary> The DeviceNo property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."DEVICENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftInventoryFieldIndex.DeviceNo, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.DeviceNo, value, true); }
		}

		/// <summary> The FirstSequentialNo property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."FIRSTSEQUENTTIALNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FirstSequentialNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftInventoryFieldIndex.FirstSequentialNo, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.FirstSequentialNo, value, true); }
		}

		/// <summary> The HashValue property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."HASHVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 32<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.String HashValue
		{
			get { return (System.String)GetValue((int)ShiftInventoryFieldIndex.HashValue, true); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.HashValue, value, true); }
		}

		/// <summary> The IsDst property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."ISDST"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsDst
		{
			get { return (Nullable<System.Int16>)GetValue((int)ShiftInventoryFieldIndex.IsDst, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.IsDst, value, true); }
		}

		/// <summary> The IsTraining property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."ISTRAINING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsTraining
		{
			get { return (Nullable<System.Int16>)GetValue((int)ShiftInventoryFieldIndex.IsTraining, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.IsTraining, value, true); }
		}

		/// <summary> The LastSequentialNo property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."LASTSEQUENTTIALNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastSequentialNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftInventoryFieldIndex.LastSequentialNo, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.LastSequentialNo, value, true); }
		}

		/// <summary> The SalesVolume property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."SALESVOLUME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SalesVolume
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftInventoryFieldIndex.SalesVolume, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.SalesVolume, value, true); }
		}

		/// <summary> The ShiftBegin property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."SHIFTBEGIN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ShiftBegin
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ShiftInventoryFieldIndex.ShiftBegin, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.ShiftBegin, value, true); }
		}

		/// <summary> The ShiftEnd property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."SHIFTEND"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ShiftEnd
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ShiftInventoryFieldIndex.ShiftEnd, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.ShiftEnd, value, true); }
		}

		/// <summary> The Shiftid property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."SHIFTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Shiftid
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftInventoryFieldIndex.Shiftid, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.Shiftid, value, true); }
		}

		/// <summary> The VehicleNo property of the Entity ShiftInventory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFTINVENTORY"."VEHICLENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> VehicleNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftInventoryFieldIndex.VehicleNo, false); }
			set	{ SetValue((int)ShiftInventoryFieldIndex.VehicleNo, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AuditRegisterEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAuditRegisters()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AuditRegisterCollection AuditRegisters
		{
			get	{ return GetMultiAuditRegisters(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AuditRegisters. When set to true, AuditRegisters is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AuditRegisters is accessed. You can always execute/ a forced fetch by calling GetMultiAuditRegisters(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAuditRegisters
		{
			get	{ return _alwaysFetchAuditRegisters; }
			set	{ _alwaysFetchAuditRegisters = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AuditRegisters already has been fetched. Setting this property to false when AuditRegisters has been fetched
		/// will clear the AuditRegisters collection well. Setting this property to true while AuditRegisters hasn't been fetched disables lazy loading for AuditRegisters</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAuditRegisters
		{
			get { return _alreadyFetchedAuditRegisters;}
			set 
			{
				if(_alreadyFetchedAuditRegisters && !value && (_auditRegisters != null))
				{
					_auditRegisters.Clear();
				}
				_alreadyFetchedAuditRegisters = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ShiftInventoryEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
