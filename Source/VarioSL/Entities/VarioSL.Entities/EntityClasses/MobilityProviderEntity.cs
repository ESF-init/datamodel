﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'MobilityProvider'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class MobilityProviderEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection	_attributeToMobilityProviders;
		private bool	_alwaysFetchAttributeToMobilityProviders, _alreadyFetchedAttributeToMobilityProviders;
		private VarioSL.Entities.CollectionClasses.BookingItemCollection	_bookingItems;
		private bool	_alwaysFetchBookingItems, _alreadyFetchedBookingItems;
		private VarioSL.Entities.CollectionClasses.ContractToProviderCollection	_contractToProviders;
		private bool	_alwaysFetchContractToProviders, _alreadyFetchedContractToProviders;
		private AddressEntity _address;
		private bool	_alwaysFetchAddress, _alreadyFetchedAddress, _addressReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Address</summary>
			public static readonly string Address = "Address";
			/// <summary>Member name AttributeToMobilityProviders</summary>
			public static readonly string AttributeToMobilityProviders = "AttributeToMobilityProviders";
			/// <summary>Member name BookingItems</summary>
			public static readonly string BookingItems = "BookingItems";
			/// <summary>Member name ContractToProviders</summary>
			public static readonly string ContractToProviders = "ContractToProviders";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MobilityProviderEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public MobilityProviderEntity() :base("MobilityProviderEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="mobilityProviderID">PK value for MobilityProvider which data should be fetched into this MobilityProvider object</param>
		public MobilityProviderEntity(System.Int64 mobilityProviderID):base("MobilityProviderEntity")
		{
			InitClassFetch(mobilityProviderID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="mobilityProviderID">PK value for MobilityProvider which data should be fetched into this MobilityProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public MobilityProviderEntity(System.Int64 mobilityProviderID, IPrefetchPath prefetchPathToUse):base("MobilityProviderEntity")
		{
			InitClassFetch(mobilityProviderID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="mobilityProviderID">PK value for MobilityProvider which data should be fetched into this MobilityProvider object</param>
		/// <param name="validator">The custom validator object for this MobilityProviderEntity</param>
		public MobilityProviderEntity(System.Int64 mobilityProviderID, IValidator validator):base("MobilityProviderEntity")
		{
			InitClassFetch(mobilityProviderID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MobilityProviderEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_attributeToMobilityProviders = (VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection)info.GetValue("_attributeToMobilityProviders", typeof(VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection));
			_alwaysFetchAttributeToMobilityProviders = info.GetBoolean("_alwaysFetchAttributeToMobilityProviders");
			_alreadyFetchedAttributeToMobilityProviders = info.GetBoolean("_alreadyFetchedAttributeToMobilityProviders");

			_bookingItems = (VarioSL.Entities.CollectionClasses.BookingItemCollection)info.GetValue("_bookingItems", typeof(VarioSL.Entities.CollectionClasses.BookingItemCollection));
			_alwaysFetchBookingItems = info.GetBoolean("_alwaysFetchBookingItems");
			_alreadyFetchedBookingItems = info.GetBoolean("_alreadyFetchedBookingItems");

			_contractToProviders = (VarioSL.Entities.CollectionClasses.ContractToProviderCollection)info.GetValue("_contractToProviders", typeof(VarioSL.Entities.CollectionClasses.ContractToProviderCollection));
			_alwaysFetchContractToProviders = info.GetBoolean("_alwaysFetchContractToProviders");
			_alreadyFetchedContractToProviders = info.GetBoolean("_alreadyFetchedContractToProviders");
			_address = (AddressEntity)info.GetValue("_address", typeof(AddressEntity));
			if(_address!=null)
			{
				_address.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_addressReturnsNewIfNotFound = info.GetBoolean("_addressReturnsNewIfNotFound");
			_alwaysFetchAddress = info.GetBoolean("_alwaysFetchAddress");
			_alreadyFetchedAddress = info.GetBoolean("_alreadyFetchedAddress");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((MobilityProviderFieldIndex)fieldIndex)
			{
				case MobilityProviderFieldIndex.AddressID:
					DesetupSyncAddress(true, false);
					_alreadyFetchedAddress = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAttributeToMobilityProviders = (_attributeToMobilityProviders.Count > 0);
			_alreadyFetchedBookingItems = (_bookingItems.Count > 0);
			_alreadyFetchedContractToProviders = (_contractToProviders.Count > 0);
			_alreadyFetchedAddress = (_address != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Address":
					toReturn.Add(Relations.AddressEntityUsingAddressID);
					break;
				case "AttributeToMobilityProviders":
					toReturn.Add(Relations.AttributeToMobilityProviderEntityUsingMobilityProviderID);
					break;
				case "BookingItems":
					toReturn.Add(Relations.BookingItemEntityUsingMobilityProviderID);
					break;
				case "ContractToProviders":
					toReturn.Add(Relations.ContractToProviderEntityUsingMobilityProviderID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_attributeToMobilityProviders", (!this.MarkedForDeletion?_attributeToMobilityProviders:null));
			info.AddValue("_alwaysFetchAttributeToMobilityProviders", _alwaysFetchAttributeToMobilityProviders);
			info.AddValue("_alreadyFetchedAttributeToMobilityProviders", _alreadyFetchedAttributeToMobilityProviders);
			info.AddValue("_bookingItems", (!this.MarkedForDeletion?_bookingItems:null));
			info.AddValue("_alwaysFetchBookingItems", _alwaysFetchBookingItems);
			info.AddValue("_alreadyFetchedBookingItems", _alreadyFetchedBookingItems);
			info.AddValue("_contractToProviders", (!this.MarkedForDeletion?_contractToProviders:null));
			info.AddValue("_alwaysFetchContractToProviders", _alwaysFetchContractToProviders);
			info.AddValue("_alreadyFetchedContractToProviders", _alreadyFetchedContractToProviders);
			info.AddValue("_address", (!this.MarkedForDeletion?_address:null));
			info.AddValue("_addressReturnsNewIfNotFound", _addressReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAddress", _alwaysFetchAddress);
			info.AddValue("_alreadyFetchedAddress", _alreadyFetchedAddress);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Address":
					_alreadyFetchedAddress = true;
					this.Address = (AddressEntity)entity;
					break;
				case "AttributeToMobilityProviders":
					_alreadyFetchedAttributeToMobilityProviders = true;
					if(entity!=null)
					{
						this.AttributeToMobilityProviders.Add((AttributeToMobilityProviderEntity)entity);
					}
					break;
				case "BookingItems":
					_alreadyFetchedBookingItems = true;
					if(entity!=null)
					{
						this.BookingItems.Add((BookingItemEntity)entity);
					}
					break;
				case "ContractToProviders":
					_alreadyFetchedContractToProviders = true;
					if(entity!=null)
					{
						this.ContractToProviders.Add((ContractToProviderEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Address":
					SetupSyncAddress(relatedEntity);
					break;
				case "AttributeToMobilityProviders":
					_attributeToMobilityProviders.Add((AttributeToMobilityProviderEntity)relatedEntity);
					break;
				case "BookingItems":
					_bookingItems.Add((BookingItemEntity)relatedEntity);
					break;
				case "ContractToProviders":
					_contractToProviders.Add((ContractToProviderEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Address":
					DesetupSyncAddress(false, true);
					break;
				case "AttributeToMobilityProviders":
					this.PerformRelatedEntityRemoval(_attributeToMobilityProviders, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BookingItems":
					this.PerformRelatedEntityRemoval(_bookingItems, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ContractToProviders":
					this.PerformRelatedEntityRemoval(_contractToProviders, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_address!=null)
			{
				toReturn.Add(_address);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_attributeToMobilityProviders);
			toReturn.Add(_bookingItems);
			toReturn.Add(_contractToProviders);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mobilityProviderID">PK value for MobilityProvider which data should be fetched into this MobilityProvider object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 mobilityProviderID)
		{
			return FetchUsingPK(mobilityProviderID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mobilityProviderID">PK value for MobilityProvider which data should be fetched into this MobilityProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 mobilityProviderID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(mobilityProviderID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mobilityProviderID">PK value for MobilityProvider which data should be fetched into this MobilityProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 mobilityProviderID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(mobilityProviderID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mobilityProviderID">PK value for MobilityProvider which data should be fetched into this MobilityProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 mobilityProviderID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(mobilityProviderID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.MobilityProviderID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MobilityProviderRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AttributeToMobilityProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeToMobilityProviderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection GetMultiAttributeToMobilityProviders(bool forceFetch)
		{
			return GetMultiAttributeToMobilityProviders(forceFetch, _attributeToMobilityProviders.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeToMobilityProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttributeToMobilityProviderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection GetMultiAttributeToMobilityProviders(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttributeToMobilityProviders(forceFetch, _attributeToMobilityProviders.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttributeToMobilityProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection GetMultiAttributeToMobilityProviders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttributeToMobilityProviders(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeToMobilityProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection GetMultiAttributeToMobilityProviders(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttributeToMobilityProviders || forceFetch || _alwaysFetchAttributeToMobilityProviders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributeToMobilityProviders);
				_attributeToMobilityProviders.SuppressClearInGetMulti=!forceFetch;
				_attributeToMobilityProviders.EntityFactoryToUse = entityFactoryToUse;
				_attributeToMobilityProviders.GetMultiManyToOne(null, this, filter);
				_attributeToMobilityProviders.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributeToMobilityProviders = true;
			}
			return _attributeToMobilityProviders;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttributeToMobilityProviders'. These settings will be taken into account
		/// when the property AttributeToMobilityProviders is requested or GetMultiAttributeToMobilityProviders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributeToMobilityProviders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributeToMobilityProviders.SortClauses=sortClauses;
			_attributeToMobilityProviders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BookingItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BookingItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BookingItemCollection GetMultiBookingItems(bool forceFetch)
		{
			return GetMultiBookingItems(forceFetch, _bookingItems.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BookingItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BookingItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BookingItemCollection GetMultiBookingItems(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBookingItems(forceFetch, _bookingItems.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BookingItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BookingItemCollection GetMultiBookingItems(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBookingItems(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BookingItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BookingItemCollection GetMultiBookingItems(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBookingItems || forceFetch || _alwaysFetchBookingItems) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_bookingItems);
				_bookingItems.SuppressClearInGetMulti=!forceFetch;
				_bookingItems.EntityFactoryToUse = entityFactoryToUse;
				_bookingItems.GetMultiManyToOne(null, null, this, filter);
				_bookingItems.SuppressClearInGetMulti=false;
				_alreadyFetchedBookingItems = true;
			}
			return _bookingItems;
		}

		/// <summary> Sets the collection parameters for the collection for 'BookingItems'. These settings will be taken into account
		/// when the property BookingItems is requested or GetMultiBookingItems is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBookingItems(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_bookingItems.SortClauses=sortClauses;
			_bookingItems.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractToProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractToProviderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractToProviderCollection GetMultiContractToProviders(bool forceFetch)
		{
			return GetMultiContractToProviders(forceFetch, _contractToProviders.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractToProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractToProviderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractToProviderCollection GetMultiContractToProviders(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContractToProviders(forceFetch, _contractToProviders.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractToProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractToProviderCollection GetMultiContractToProviders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContractToProviders(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractToProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractToProviderCollection GetMultiContractToProviders(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContractToProviders || forceFetch || _alwaysFetchContractToProviders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contractToProviders);
				_contractToProviders.SuppressClearInGetMulti=!forceFetch;
				_contractToProviders.EntityFactoryToUse = entityFactoryToUse;
				_contractToProviders.GetMultiManyToOne(null, this, filter);
				_contractToProviders.SuppressClearInGetMulti=false;
				_alreadyFetchedContractToProviders = true;
			}
			return _contractToProviders;
		}

		/// <summary> Sets the collection parameters for the collection for 'ContractToProviders'. These settings will be taken into account
		/// when the property ContractToProviders is requested or GetMultiContractToProviders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContractToProviders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contractToProviders.SortClauses=sortClauses;
			_contractToProviders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public AddressEntity GetSingleAddress()
		{
			return GetSingleAddress(false);
		}

		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public virtual AddressEntity GetSingleAddress(bool forceFetch)
		{
			if( ( !_alreadyFetchedAddress || forceFetch || _alwaysFetchAddress) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AddressEntityUsingAddressID);
				AddressEntity newEntity = new AddressEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AddressID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AddressEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_addressReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Address = newEntity;
				_alreadyFetchedAddress = fetchResult;
			}
			return _address;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Address", _address);
			toReturn.Add("AttributeToMobilityProviders", _attributeToMobilityProviders);
			toReturn.Add("BookingItems", _bookingItems);
			toReturn.Add("ContractToProviders", _contractToProviders);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="mobilityProviderID">PK value for MobilityProvider which data should be fetched into this MobilityProvider object</param>
		/// <param name="validator">The validator object for this MobilityProviderEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 mobilityProviderID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(mobilityProviderID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_attributeToMobilityProviders = new VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection();
			_attributeToMobilityProviders.SetContainingEntityInfo(this, "MobilityProvider");

			_bookingItems = new VarioSL.Entities.CollectionClasses.BookingItemCollection();
			_bookingItems.SetContainingEntityInfo(this, "MobilityProvider");

			_contractToProviders = new VarioSL.Entities.CollectionClasses.ContractToProviderCollection();
			_contractToProviders.SetContainingEntityInfo(this, "MobilityProvider");
			_addressReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataRowCreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DrivingLicenceRequired", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FixedPriceProvider", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MobilityProviderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MobilityProviderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProviderName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _address</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAddress(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _address, new PropertyChangedEventHandler( OnAddressPropertyChanged ), "Address", VarioSL.Entities.RelationClasses.StaticMobilityProviderRelations.AddressEntityUsingAddressIDStatic, true, signalRelatedEntity, "MobilityProviders", resetFKFields, new int[] { (int)MobilityProviderFieldIndex.AddressID } );		
			_address = null;
		}
		
		/// <summary> setups the sync logic for member _address</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAddress(IEntityCore relatedEntity)
		{
			if(_address!=relatedEntity)
			{		
				DesetupSyncAddress(true, true);
				_address = (AddressEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _address, new PropertyChangedEventHandler( OnAddressPropertyChanged ), "Address", VarioSL.Entities.RelationClasses.StaticMobilityProviderRelations.AddressEntityUsingAddressIDStatic, true, ref _alreadyFetchedAddress, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAddressPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="mobilityProviderID">PK value for MobilityProvider which data should be fetched into this MobilityProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 mobilityProviderID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)MobilityProviderFieldIndex.MobilityProviderID].ForcedCurrentValueWrite(mobilityProviderID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateMobilityProviderDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new MobilityProviderEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MobilityProviderRelations Relations
		{
			get	{ return new MobilityProviderRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeToMobilityProvider' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeToMobilityProviders
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection(), (IEntityRelation)GetRelationsForField("AttributeToMobilityProviders")[0], (int)VarioSL.Entities.EntityType.MobilityProviderEntity, (int)VarioSL.Entities.EntityType.AttributeToMobilityProviderEntity, 0, null, null, null, "AttributeToMobilityProviders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BookingItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBookingItems
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BookingItemCollection(), (IEntityRelation)GetRelationsForField("BookingItems")[0], (int)VarioSL.Entities.EntityType.MobilityProviderEntity, (int)VarioSL.Entities.EntityType.BookingItemEntity, 0, null, null, null, "BookingItems", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractToProvider' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContractToProviders
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractToProviderCollection(), (IEntityRelation)GetRelationsForField("ContractToProviders")[0], (int)VarioSL.Entities.EntityType.MobilityProviderEntity, (int)VarioSL.Entities.EntityType.ContractToProviderEntity, 0, null, null, null, "ContractToProviders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Address'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAddress
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressCollection(), (IEntityRelation)GetRelationsForField("Address")[0], (int)VarioSL.Entities.EntityType.MobilityProviderEntity, (int)VarioSL.Entities.EntityType.AddressEntity, 0, null, null, null, "Address", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AddressID property of the Entity MobilityProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MOBILITYPROVIDER"."ADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AddressID
		{
			get { return (Nullable<System.Int64>)GetValue((int)MobilityProviderFieldIndex.AddressID, false); }
			set	{ SetValue((int)MobilityProviderFieldIndex.AddressID, value, true); }
		}

		/// <summary> The DataRowCreationDate property of the Entity MobilityProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MOBILITYPROVIDER"."DATAROWCREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataRowCreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MobilityProviderFieldIndex.DataRowCreationDate, false); }
			set	{ SetValue((int)MobilityProviderFieldIndex.DataRowCreationDate, value, true); }
		}

		/// <summary> The DrivingLicenceRequired property of the Entity MobilityProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MOBILITYPROVIDER"."DRIVINGLICENCEREQUIRED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> DrivingLicenceRequired
		{
			get { return (Nullable<System.Boolean>)GetValue((int)MobilityProviderFieldIndex.DrivingLicenceRequired, false); }
			set	{ SetValue((int)MobilityProviderFieldIndex.DrivingLicenceRequired, value, true); }
		}

		/// <summary> The FixedPriceProvider property of the Entity MobilityProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MOBILITYPROVIDER"."FIXEDPRICEPROVIDER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> FixedPriceProvider
		{
			get { return (Nullable<System.Boolean>)GetValue((int)MobilityProviderFieldIndex.FixedPriceProvider, false); }
			set	{ SetValue((int)MobilityProviderFieldIndex.FixedPriceProvider, value, true); }
		}

		/// <summary> The LastModified property of the Entity MobilityProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MOBILITYPROVIDER"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)MobilityProviderFieldIndex.LastModified, true); }
			set	{ SetValue((int)MobilityProviderFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity MobilityProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MOBILITYPROVIDER"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)MobilityProviderFieldIndex.LastUser, true); }
			set	{ SetValue((int)MobilityProviderFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MobilityProviderID property of the Entity MobilityProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MOBILITYPROVIDER"."MOBILITYPROVIDERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 MobilityProviderID
		{
			get { return (System.Int64)GetValue((int)MobilityProviderFieldIndex.MobilityProviderID, true); }
			set	{ SetValue((int)MobilityProviderFieldIndex.MobilityProviderID, value, true); }
		}

		/// <summary> The MobilityProviderNumber property of the Entity MobilityProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MOBILITYPROVIDER"."MOBILITYPROVIDERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MobilityProviderNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)MobilityProviderFieldIndex.MobilityProviderNumber, false); }
			set	{ SetValue((int)MobilityProviderFieldIndex.MobilityProviderNumber, value, true); }
		}

		/// <summary> The ProviderName property of the Entity MobilityProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MOBILITYPROVIDER"."PROVIDERNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ProviderName
		{
			get { return (System.String)GetValue((int)MobilityProviderFieldIndex.ProviderName, true); }
			set	{ SetValue((int)MobilityProviderFieldIndex.ProviderName, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity MobilityProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MOBILITYPROVIDER"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)MobilityProviderFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)MobilityProviderFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AttributeToMobilityProviderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributeToMobilityProviders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection AttributeToMobilityProviders
		{
			get	{ return GetMultiAttributeToMobilityProviders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeToMobilityProviders. When set to true, AttributeToMobilityProviders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeToMobilityProviders is accessed. You can always execute/ a forced fetch by calling GetMultiAttributeToMobilityProviders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeToMobilityProviders
		{
			get	{ return _alwaysFetchAttributeToMobilityProviders; }
			set	{ _alwaysFetchAttributeToMobilityProviders = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeToMobilityProviders already has been fetched. Setting this property to false when AttributeToMobilityProviders has been fetched
		/// will clear the AttributeToMobilityProviders collection well. Setting this property to true while AttributeToMobilityProviders hasn't been fetched disables lazy loading for AttributeToMobilityProviders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeToMobilityProviders
		{
			get { return _alreadyFetchedAttributeToMobilityProviders;}
			set 
			{
				if(_alreadyFetchedAttributeToMobilityProviders && !value && (_attributeToMobilityProviders != null))
				{
					_attributeToMobilityProviders.Clear();
				}
				_alreadyFetchedAttributeToMobilityProviders = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BookingItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBookingItems()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BookingItemCollection BookingItems
		{
			get	{ return GetMultiBookingItems(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BookingItems. When set to true, BookingItems is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BookingItems is accessed. You can always execute/ a forced fetch by calling GetMultiBookingItems(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBookingItems
		{
			get	{ return _alwaysFetchBookingItems; }
			set	{ _alwaysFetchBookingItems = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BookingItems already has been fetched. Setting this property to false when BookingItems has been fetched
		/// will clear the BookingItems collection well. Setting this property to true while BookingItems hasn't been fetched disables lazy loading for BookingItems</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBookingItems
		{
			get { return _alreadyFetchedBookingItems;}
			set 
			{
				if(_alreadyFetchedBookingItems && !value && (_bookingItems != null))
				{
					_bookingItems.Clear();
				}
				_alreadyFetchedBookingItems = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractToProviderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContractToProviders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractToProviderCollection ContractToProviders
		{
			get	{ return GetMultiContractToProviders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ContractToProviders. When set to true, ContractToProviders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContractToProviders is accessed. You can always execute/ a forced fetch by calling GetMultiContractToProviders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContractToProviders
		{
			get	{ return _alwaysFetchContractToProviders; }
			set	{ _alwaysFetchContractToProviders = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ContractToProviders already has been fetched. Setting this property to false when ContractToProviders has been fetched
		/// will clear the ContractToProviders collection well. Setting this property to true while ContractToProviders hasn't been fetched disables lazy loading for ContractToProviders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContractToProviders
		{
			get { return _alreadyFetchedContractToProviders;}
			set 
			{
				if(_alreadyFetchedContractToProviders && !value && (_contractToProviders != null))
				{
					_contractToProviders.Clear();
				}
				_alreadyFetchedContractToProviders = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AddressEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAddress()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AddressEntity Address
		{
			get	{ return GetSingleAddress(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAddress(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MobilityProviders", "Address", _address, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Address. When set to true, Address is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Address is accessed. You can always execute a forced fetch by calling GetSingleAddress(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAddress
		{
			get	{ return _alwaysFetchAddress; }
			set	{ _alwaysFetchAddress = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Address already has been fetched. Setting this property to false when Address has been fetched
		/// will set Address to null as well. Setting this property to true while Address hasn't been fetched disables lazy loading for Address</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAddress
		{
			get { return _alreadyFetchedAddress;}
			set 
			{
				if(_alreadyFetchedAddress && !value)
				{
					this.Address = null;
				}
				_alreadyFetchedAddress = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Address is not found
		/// in the database. When set to true, Address will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AddressReturnsNewIfNotFound
		{
			get	{ return _addressReturnsNewIfNotFound; }
			set { _addressReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.MobilityProviderEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
