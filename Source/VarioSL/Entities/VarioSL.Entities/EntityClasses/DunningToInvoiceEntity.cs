﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DunningToInvoice'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DunningToInvoiceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private DunningLevelConfigurationEntity _dunningLevelConfiguration;
		private bool	_alwaysFetchDunningLevelConfiguration, _alreadyFetchedDunningLevelConfiguration, _dunningLevelConfigurationReturnsNewIfNotFound;
		private DunningProcessEntity _dunningProcess;
		private bool	_alwaysFetchDunningProcess, _alreadyFetchedDunningProcess, _dunningProcessReturnsNewIfNotFound;
		private InvoiceEntity _invoice;
		private bool	_alwaysFetchInvoice, _alreadyFetchedInvoice, _invoiceReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DunningLevelConfiguration</summary>
			public static readonly string DunningLevelConfiguration = "DunningLevelConfiguration";
			/// <summary>Member name DunningProcess</summary>
			public static readonly string DunningProcess = "DunningProcess";
			/// <summary>Member name Invoice</summary>
			public static readonly string Invoice = "Invoice";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DunningToInvoiceEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DunningToInvoiceEntity() :base("DunningToInvoiceEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="dunningToInvoiceID">PK value for DunningToInvoice which data should be fetched into this DunningToInvoice object</param>
		public DunningToInvoiceEntity(System.Int64 dunningToInvoiceID):base("DunningToInvoiceEntity")
		{
			InitClassFetch(dunningToInvoiceID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="dunningToInvoiceID">PK value for DunningToInvoice which data should be fetched into this DunningToInvoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DunningToInvoiceEntity(System.Int64 dunningToInvoiceID, IPrefetchPath prefetchPathToUse):base("DunningToInvoiceEntity")
		{
			InitClassFetch(dunningToInvoiceID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="dunningToInvoiceID">PK value for DunningToInvoice which data should be fetched into this DunningToInvoice object</param>
		/// <param name="validator">The custom validator object for this DunningToInvoiceEntity</param>
		public DunningToInvoiceEntity(System.Int64 dunningToInvoiceID, IValidator validator):base("DunningToInvoiceEntity")
		{
			InitClassFetch(dunningToInvoiceID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DunningToInvoiceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_dunningLevelConfiguration = (DunningLevelConfigurationEntity)info.GetValue("_dunningLevelConfiguration", typeof(DunningLevelConfigurationEntity));
			if(_dunningLevelConfiguration!=null)
			{
				_dunningLevelConfiguration.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_dunningLevelConfigurationReturnsNewIfNotFound = info.GetBoolean("_dunningLevelConfigurationReturnsNewIfNotFound");
			_alwaysFetchDunningLevelConfiguration = info.GetBoolean("_alwaysFetchDunningLevelConfiguration");
			_alreadyFetchedDunningLevelConfiguration = info.GetBoolean("_alreadyFetchedDunningLevelConfiguration");

			_dunningProcess = (DunningProcessEntity)info.GetValue("_dunningProcess", typeof(DunningProcessEntity));
			if(_dunningProcess!=null)
			{
				_dunningProcess.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_dunningProcessReturnsNewIfNotFound = info.GetBoolean("_dunningProcessReturnsNewIfNotFound");
			_alwaysFetchDunningProcess = info.GetBoolean("_alwaysFetchDunningProcess");
			_alreadyFetchedDunningProcess = info.GetBoolean("_alreadyFetchedDunningProcess");

			_invoice = (InvoiceEntity)info.GetValue("_invoice", typeof(InvoiceEntity));
			if(_invoice!=null)
			{
				_invoice.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_invoiceReturnsNewIfNotFound = info.GetBoolean("_invoiceReturnsNewIfNotFound");
			_alwaysFetchInvoice = info.GetBoolean("_alwaysFetchInvoice");
			_alreadyFetchedInvoice = info.GetBoolean("_alreadyFetchedInvoice");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DunningToInvoiceFieldIndex)fieldIndex)
			{
				case DunningToInvoiceFieldIndex.DunningLevelID:
					DesetupSyncDunningLevelConfiguration(true, false);
					_alreadyFetchedDunningLevelConfiguration = false;
					break;
				case DunningToInvoiceFieldIndex.DunningProcessID:
					DesetupSyncDunningProcess(true, false);
					_alreadyFetchedDunningProcess = false;
					break;
				case DunningToInvoiceFieldIndex.InvoiceID:
					DesetupSyncInvoice(true, false);
					_alreadyFetchedInvoice = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDunningLevelConfiguration = (_dunningLevelConfiguration != null);
			_alreadyFetchedDunningProcess = (_dunningProcess != null);
			_alreadyFetchedInvoice = (_invoice != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DunningLevelConfiguration":
					toReturn.Add(Relations.DunningLevelConfigurationEntityUsingDunningLevelID);
					break;
				case "DunningProcess":
					toReturn.Add(Relations.DunningProcessEntityUsingDunningProcessID);
					break;
				case "Invoice":
					toReturn.Add(Relations.InvoiceEntityUsingInvoiceID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_dunningLevelConfiguration", (!this.MarkedForDeletion?_dunningLevelConfiguration:null));
			info.AddValue("_dunningLevelConfigurationReturnsNewIfNotFound", _dunningLevelConfigurationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDunningLevelConfiguration", _alwaysFetchDunningLevelConfiguration);
			info.AddValue("_alreadyFetchedDunningLevelConfiguration", _alreadyFetchedDunningLevelConfiguration);
			info.AddValue("_dunningProcess", (!this.MarkedForDeletion?_dunningProcess:null));
			info.AddValue("_dunningProcessReturnsNewIfNotFound", _dunningProcessReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDunningProcess", _alwaysFetchDunningProcess);
			info.AddValue("_alreadyFetchedDunningProcess", _alreadyFetchedDunningProcess);
			info.AddValue("_invoice", (!this.MarkedForDeletion?_invoice:null));
			info.AddValue("_invoiceReturnsNewIfNotFound", _invoiceReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInvoice", _alwaysFetchInvoice);
			info.AddValue("_alreadyFetchedInvoice", _alreadyFetchedInvoice);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DunningLevelConfiguration":
					_alreadyFetchedDunningLevelConfiguration = true;
					this.DunningLevelConfiguration = (DunningLevelConfigurationEntity)entity;
					break;
				case "DunningProcess":
					_alreadyFetchedDunningProcess = true;
					this.DunningProcess = (DunningProcessEntity)entity;
					break;
				case "Invoice":
					_alreadyFetchedInvoice = true;
					this.Invoice = (InvoiceEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DunningLevelConfiguration":
					SetupSyncDunningLevelConfiguration(relatedEntity);
					break;
				case "DunningProcess":
					SetupSyncDunningProcess(relatedEntity);
					break;
				case "Invoice":
					SetupSyncInvoice(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DunningLevelConfiguration":
					DesetupSyncDunningLevelConfiguration(false, true);
					break;
				case "DunningProcess":
					DesetupSyncDunningProcess(false, true);
					break;
				case "Invoice":
					DesetupSyncInvoice(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_dunningLevelConfiguration!=null)
			{
				toReturn.Add(_dunningLevelConfiguration);
			}
			if(_dunningProcess!=null)
			{
				toReturn.Add(_dunningProcess);
			}
			if(_invoice!=null)
			{
				toReturn.Add(_invoice);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningToInvoiceID">PK value for DunningToInvoice which data should be fetched into this DunningToInvoice object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningToInvoiceID)
		{
			return FetchUsingPK(dunningToInvoiceID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningToInvoiceID">PK value for DunningToInvoice which data should be fetched into this DunningToInvoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningToInvoiceID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(dunningToInvoiceID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningToInvoiceID">PK value for DunningToInvoice which data should be fetched into this DunningToInvoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningToInvoiceID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(dunningToInvoiceID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningToInvoiceID">PK value for DunningToInvoice which data should be fetched into this DunningToInvoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningToInvoiceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(dunningToInvoiceID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DunningToInvoiceID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DunningToInvoiceRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'DunningLevelConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DunningLevelConfigurationEntity' which is related to this entity.</returns>
		public DunningLevelConfigurationEntity GetSingleDunningLevelConfiguration()
		{
			return GetSingleDunningLevelConfiguration(false);
		}

		/// <summary> Retrieves the related entity of type 'DunningLevelConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DunningLevelConfigurationEntity' which is related to this entity.</returns>
		public virtual DunningLevelConfigurationEntity GetSingleDunningLevelConfiguration(bool forceFetch)
		{
			if( ( !_alreadyFetchedDunningLevelConfiguration || forceFetch || _alwaysFetchDunningLevelConfiguration) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DunningLevelConfigurationEntityUsingDunningLevelID);
				DunningLevelConfigurationEntity newEntity = new DunningLevelConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DunningLevelID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DunningLevelConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_dunningLevelConfigurationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DunningLevelConfiguration = newEntity;
				_alreadyFetchedDunningLevelConfiguration = fetchResult;
			}
			return _dunningLevelConfiguration;
		}


		/// <summary> Retrieves the related entity of type 'DunningProcessEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DunningProcessEntity' which is related to this entity.</returns>
		public DunningProcessEntity GetSingleDunningProcess()
		{
			return GetSingleDunningProcess(false);
		}

		/// <summary> Retrieves the related entity of type 'DunningProcessEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DunningProcessEntity' which is related to this entity.</returns>
		public virtual DunningProcessEntity GetSingleDunningProcess(bool forceFetch)
		{
			if( ( !_alreadyFetchedDunningProcess || forceFetch || _alwaysFetchDunningProcess) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DunningProcessEntityUsingDunningProcessID);
				DunningProcessEntity newEntity = new DunningProcessEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DunningProcessID);
				}
				if(fetchResult)
				{
					newEntity = (DunningProcessEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_dunningProcessReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DunningProcess = newEntity;
				_alreadyFetchedDunningProcess = fetchResult;
			}
			return _dunningProcess;
		}


		/// <summary> Retrieves the related entity of type 'InvoiceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'InvoiceEntity' which is related to this entity.</returns>
		public InvoiceEntity GetSingleInvoice()
		{
			return GetSingleInvoice(false);
		}

		/// <summary> Retrieves the related entity of type 'InvoiceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InvoiceEntity' which is related to this entity.</returns>
		public virtual InvoiceEntity GetSingleInvoice(bool forceFetch)
		{
			if( ( !_alreadyFetchedInvoice || forceFetch || _alwaysFetchInvoice) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InvoiceEntityUsingInvoiceID);
				InvoiceEntity newEntity = new InvoiceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InvoiceID);
				}
				if(fetchResult)
				{
					newEntity = (InvoiceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_invoiceReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Invoice = newEntity;
				_alreadyFetchedInvoice = fetchResult;
			}
			return _invoice;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DunningLevelConfiguration", _dunningLevelConfiguration);
			toReturn.Add("DunningProcess", _dunningProcess);
			toReturn.Add("Invoice", _invoice);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="dunningToInvoiceID">PK value for DunningToInvoice which data should be fetched into this DunningToInvoice object</param>
		/// <param name="validator">The validator object for this DunningToInvoiceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 dunningToInvoiceID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(dunningToInvoiceID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_dunningLevelConfigurationReturnsNewIfNotFound = false;
			_dunningProcessReturnsNewIfNotFound = false;
			_invoiceReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DunningLevelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DunningProcessID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DunningToInvoiceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactioCcounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _dunningLevelConfiguration</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDunningLevelConfiguration(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _dunningLevelConfiguration, new PropertyChangedEventHandler( OnDunningLevelConfigurationPropertyChanged ), "DunningLevelConfiguration", VarioSL.Entities.RelationClasses.StaticDunningToInvoiceRelations.DunningLevelConfigurationEntityUsingDunningLevelIDStatic, true, signalRelatedEntity, "DunningToInvoices", resetFKFields, new int[] { (int)DunningToInvoiceFieldIndex.DunningLevelID } );		
			_dunningLevelConfiguration = null;
		}
		
		/// <summary> setups the sync logic for member _dunningLevelConfiguration</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDunningLevelConfiguration(IEntityCore relatedEntity)
		{
			if(_dunningLevelConfiguration!=relatedEntity)
			{		
				DesetupSyncDunningLevelConfiguration(true, true);
				_dunningLevelConfiguration = (DunningLevelConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _dunningLevelConfiguration, new PropertyChangedEventHandler( OnDunningLevelConfigurationPropertyChanged ), "DunningLevelConfiguration", VarioSL.Entities.RelationClasses.StaticDunningToInvoiceRelations.DunningLevelConfigurationEntityUsingDunningLevelIDStatic, true, ref _alreadyFetchedDunningLevelConfiguration, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDunningLevelConfigurationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _dunningProcess</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDunningProcess(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _dunningProcess, new PropertyChangedEventHandler( OnDunningProcessPropertyChanged ), "DunningProcess", VarioSL.Entities.RelationClasses.StaticDunningToInvoiceRelations.DunningProcessEntityUsingDunningProcessIDStatic, true, signalRelatedEntity, "DunningToInvoices", resetFKFields, new int[] { (int)DunningToInvoiceFieldIndex.DunningProcessID } );		
			_dunningProcess = null;
		}
		
		/// <summary> setups the sync logic for member _dunningProcess</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDunningProcess(IEntityCore relatedEntity)
		{
			if(_dunningProcess!=relatedEntity)
			{		
				DesetupSyncDunningProcess(true, true);
				_dunningProcess = (DunningProcessEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _dunningProcess, new PropertyChangedEventHandler( OnDunningProcessPropertyChanged ), "DunningProcess", VarioSL.Entities.RelationClasses.StaticDunningToInvoiceRelations.DunningProcessEntityUsingDunningProcessIDStatic, true, ref _alreadyFetchedDunningProcess, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDunningProcessPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _invoice</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInvoice(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _invoice, new PropertyChangedEventHandler( OnInvoicePropertyChanged ), "Invoice", VarioSL.Entities.RelationClasses.StaticDunningToInvoiceRelations.InvoiceEntityUsingInvoiceIDStatic, true, signalRelatedEntity, "DunningToInvoices", resetFKFields, new int[] { (int)DunningToInvoiceFieldIndex.InvoiceID } );		
			_invoice = null;
		}
		
		/// <summary> setups the sync logic for member _invoice</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInvoice(IEntityCore relatedEntity)
		{
			if(_invoice!=relatedEntity)
			{		
				DesetupSyncInvoice(true, true);
				_invoice = (InvoiceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _invoice, new PropertyChangedEventHandler( OnInvoicePropertyChanged ), "Invoice", VarioSL.Entities.RelationClasses.StaticDunningToInvoiceRelations.InvoiceEntityUsingInvoiceIDStatic, true, ref _alreadyFetchedInvoice, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInvoicePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="dunningToInvoiceID">PK value for DunningToInvoice which data should be fetched into this DunningToInvoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 dunningToInvoiceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DunningToInvoiceFieldIndex.DunningToInvoiceID].ForcedCurrentValueWrite(dunningToInvoiceID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDunningToInvoiceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DunningToInvoiceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DunningToInvoiceRelations Relations
		{
			get	{ return new DunningToInvoiceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningLevelConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningLevelConfiguration
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection(), (IEntityRelation)GetRelationsForField("DunningLevelConfiguration")[0], (int)VarioSL.Entities.EntityType.DunningToInvoiceEntity, (int)VarioSL.Entities.EntityType.DunningLevelConfigurationEntity, 0, null, null, null, "DunningLevelConfiguration", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningProcess'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningProcess
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningProcessCollection(), (IEntityRelation)GetRelationsForField("DunningProcess")[0], (int)VarioSL.Entities.EntityType.DunningToInvoiceEntity, (int)VarioSL.Entities.EntityType.DunningProcessEntity, 0, null, null, null, "DunningProcess", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoice'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoice
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoiceCollection(), (IEntityRelation)GetRelationsForField("Invoice")[0], (int)VarioSL.Entities.EntityType.DunningToInvoiceEntity, (int)VarioSL.Entities.EntityType.InvoiceEntity, 0, null, null, null, "Invoice", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity DunningToInvoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGTOINVOICE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)DunningToInvoiceFieldIndex.Description, true); }
			set	{ SetValue((int)DunningToInvoiceFieldIndex.Description, value, true); }
		}

		/// <summary> The DunningLevelID property of the Entity DunningToInvoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGTOINVOICE"."DUNNINGLEVELID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DunningLevelID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DunningToInvoiceFieldIndex.DunningLevelID, false); }
			set	{ SetValue((int)DunningToInvoiceFieldIndex.DunningLevelID, value, true); }
		}

		/// <summary> The DunningProcessID property of the Entity DunningToInvoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGTOINVOICE"."DUNNINGPROCESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DunningProcessID
		{
			get { return (System.Int64)GetValue((int)DunningToInvoiceFieldIndex.DunningProcessID, true); }
			set	{ SetValue((int)DunningToInvoiceFieldIndex.DunningProcessID, value, true); }
		}

		/// <summary> The DunningToInvoiceID property of the Entity DunningToInvoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGTOINVOICE"."DUNNINGTOINVOICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 DunningToInvoiceID
		{
			get { return (System.Int64)GetValue((int)DunningToInvoiceFieldIndex.DunningToInvoiceID, true); }
			set	{ SetValue((int)DunningToInvoiceFieldIndex.DunningToInvoiceID, value, true); }
		}

		/// <summary> The InvoiceID property of the Entity DunningToInvoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGTOINVOICE"."INVOICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 InvoiceID
		{
			get { return (System.Int64)GetValue((int)DunningToInvoiceFieldIndex.InvoiceID, true); }
			set	{ SetValue((int)DunningToInvoiceFieldIndex.InvoiceID, value, true); }
		}

		/// <summary> The LastModified property of the Entity DunningToInvoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGTOINVOICE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)DunningToInvoiceFieldIndex.LastModified, true); }
			set	{ SetValue((int)DunningToInvoiceFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity DunningToInvoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGTOINVOICE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)DunningToInvoiceFieldIndex.LastUser, true); }
			set	{ SetValue((int)DunningToInvoiceFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TransactioCcounter property of the Entity DunningToInvoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGTOINVOICE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactioCcounter
		{
			get { return (System.Decimal)GetValue((int)DunningToInvoiceFieldIndex.TransactioCcounter, true); }
			set	{ SetValue((int)DunningToInvoiceFieldIndex.TransactioCcounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'DunningLevelConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDunningLevelConfiguration()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DunningLevelConfigurationEntity DunningLevelConfiguration
		{
			get	{ return GetSingleDunningLevelConfiguration(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDunningLevelConfiguration(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DunningToInvoices", "DunningLevelConfiguration", _dunningLevelConfiguration, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DunningLevelConfiguration. When set to true, DunningLevelConfiguration is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningLevelConfiguration is accessed. You can always execute a forced fetch by calling GetSingleDunningLevelConfiguration(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningLevelConfiguration
		{
			get	{ return _alwaysFetchDunningLevelConfiguration; }
			set	{ _alwaysFetchDunningLevelConfiguration = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningLevelConfiguration already has been fetched. Setting this property to false when DunningLevelConfiguration has been fetched
		/// will set DunningLevelConfiguration to null as well. Setting this property to true while DunningLevelConfiguration hasn't been fetched disables lazy loading for DunningLevelConfiguration</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningLevelConfiguration
		{
			get { return _alreadyFetchedDunningLevelConfiguration;}
			set 
			{
				if(_alreadyFetchedDunningLevelConfiguration && !value)
				{
					this.DunningLevelConfiguration = null;
				}
				_alreadyFetchedDunningLevelConfiguration = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DunningLevelConfiguration is not found
		/// in the database. When set to true, DunningLevelConfiguration will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DunningLevelConfigurationReturnsNewIfNotFound
		{
			get	{ return _dunningLevelConfigurationReturnsNewIfNotFound; }
			set { _dunningLevelConfigurationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DunningProcessEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDunningProcess()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DunningProcessEntity DunningProcess
		{
			get	{ return GetSingleDunningProcess(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDunningProcess(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DunningToInvoices", "DunningProcess", _dunningProcess, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DunningProcess. When set to true, DunningProcess is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningProcess is accessed. You can always execute a forced fetch by calling GetSingleDunningProcess(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningProcess
		{
			get	{ return _alwaysFetchDunningProcess; }
			set	{ _alwaysFetchDunningProcess = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningProcess already has been fetched. Setting this property to false when DunningProcess has been fetched
		/// will set DunningProcess to null as well. Setting this property to true while DunningProcess hasn't been fetched disables lazy loading for DunningProcess</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningProcess
		{
			get { return _alreadyFetchedDunningProcess;}
			set 
			{
				if(_alreadyFetchedDunningProcess && !value)
				{
					this.DunningProcess = null;
				}
				_alreadyFetchedDunningProcess = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DunningProcess is not found
		/// in the database. When set to true, DunningProcess will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DunningProcessReturnsNewIfNotFound
		{
			get	{ return _dunningProcessReturnsNewIfNotFound; }
			set { _dunningProcessReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'InvoiceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInvoice()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual InvoiceEntity Invoice
		{
			get	{ return GetSingleInvoice(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInvoice(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DunningToInvoices", "Invoice", _invoice, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Invoice. When set to true, Invoice is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoice is accessed. You can always execute a forced fetch by calling GetSingleInvoice(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoice
		{
			get	{ return _alwaysFetchInvoice; }
			set	{ _alwaysFetchInvoice = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoice already has been fetched. Setting this property to false when Invoice has been fetched
		/// will set Invoice to null as well. Setting this property to true while Invoice hasn't been fetched disables lazy loading for Invoice</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoice
		{
			get { return _alreadyFetchedInvoice;}
			set 
			{
				if(_alreadyFetchedInvoice && !value)
				{
					this.Invoice = null;
				}
				_alreadyFetchedInvoice = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Invoice is not found
		/// in the database. When set to true, Invoice will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool InvoiceReturnsNewIfNotFound
		{
			get	{ return _invoiceReturnsNewIfNotFound; }
			set { _invoiceReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DunningToInvoiceEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
