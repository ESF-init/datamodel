﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Line'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class LineEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection	_externalPacketEfforts;
		private bool	_alwaysFetchExternalPacketEfforts, _alreadyFetchedExternalPacketEfforts;
		private VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection	_ticketServicesPermitted;
		private bool	_alwaysFetchTicketServicesPermitted, _alreadyFetchedTicketServicesPermitted;
		private VarioSL.Entities.CollectionClasses.TicketCollection _ticketCollectionViaTicketServicesPermitted;
		private bool	_alwaysFetchTicketCollectionViaTicketServicesPermitted, _alreadyFetchedTicketCollectionViaTicketServicesPermitted;
		private NetEntity _net;
		private bool	_alwaysFetchNet, _alreadyFetchedNet, _netReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Net</summary>
			public static readonly string Net = "Net";
			/// <summary>Member name ExternalPacketEfforts</summary>
			public static readonly string ExternalPacketEfforts = "ExternalPacketEfforts";
			/// <summary>Member name TicketServicesPermitted</summary>
			public static readonly string TicketServicesPermitted = "TicketServicesPermitted";
			/// <summary>Member name TicketCollectionViaTicketServicesPermitted</summary>
			public static readonly string TicketCollectionViaTicketServicesPermitted = "TicketCollectionViaTicketServicesPermitted";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static LineEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public LineEntity() :base("LineEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="lineID">PK value for Line which data should be fetched into this Line object</param>
		public LineEntity(System.Int64 lineID):base("LineEntity")
		{
			InitClassFetch(lineID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="lineID">PK value for Line which data should be fetched into this Line object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public LineEntity(System.Int64 lineID, IPrefetchPath prefetchPathToUse):base("LineEntity")
		{
			InitClassFetch(lineID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="lineID">PK value for Line which data should be fetched into this Line object</param>
		/// <param name="validator">The custom validator object for this LineEntity</param>
		public LineEntity(System.Int64 lineID, IValidator validator):base("LineEntity")
		{
			InitClassFetch(lineID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected LineEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_externalPacketEfforts = (VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection)info.GetValue("_externalPacketEfforts", typeof(VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection));
			_alwaysFetchExternalPacketEfforts = info.GetBoolean("_alwaysFetchExternalPacketEfforts");
			_alreadyFetchedExternalPacketEfforts = info.GetBoolean("_alreadyFetchedExternalPacketEfforts");

			_ticketServicesPermitted = (VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection)info.GetValue("_ticketServicesPermitted", typeof(VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection));
			_alwaysFetchTicketServicesPermitted = info.GetBoolean("_alwaysFetchTicketServicesPermitted");
			_alreadyFetchedTicketServicesPermitted = info.GetBoolean("_alreadyFetchedTicketServicesPermitted");
			_ticketCollectionViaTicketServicesPermitted = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketCollectionViaTicketServicesPermitted", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketCollectionViaTicketServicesPermitted = info.GetBoolean("_alwaysFetchTicketCollectionViaTicketServicesPermitted");
			_alreadyFetchedTicketCollectionViaTicketServicesPermitted = info.GetBoolean("_alreadyFetchedTicketCollectionViaTicketServicesPermitted");
			_net = (NetEntity)info.GetValue("_net", typeof(NetEntity));
			if(_net!=null)
			{
				_net.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_netReturnsNewIfNotFound = info.GetBoolean("_netReturnsNewIfNotFound");
			_alwaysFetchNet = info.GetBoolean("_alwaysFetchNet");
			_alreadyFetchedNet = info.GetBoolean("_alreadyFetchedNet");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((LineFieldIndex)fieldIndex)
			{
				case LineFieldIndex.NetID:
					DesetupSyncNet(true, false);
					_alreadyFetchedNet = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedExternalPacketEfforts = (_externalPacketEfforts.Count > 0);
			_alreadyFetchedTicketServicesPermitted = (_ticketServicesPermitted.Count > 0);
			_alreadyFetchedTicketCollectionViaTicketServicesPermitted = (_ticketCollectionViaTicketServicesPermitted.Count > 0);
			_alreadyFetchedNet = (_net != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Net":
					toReturn.Add(Relations.NetEntityUsingNetID);
					break;
				case "ExternalPacketEfforts":
					toReturn.Add(Relations.ExternalPacketEffortEntityUsingLineID);
					break;
				case "TicketServicesPermitted":
					toReturn.Add(Relations.TicketServicesPermittedEntityUsingLineID);
					break;
				case "TicketCollectionViaTicketServicesPermitted":
					toReturn.Add(Relations.TicketServicesPermittedEntityUsingLineID, "LineEntity__", "TicketServicesPermitted_", JoinHint.None);
					toReturn.Add(TicketServicesPermittedEntity.Relations.TicketEntityUsingTicketID, "TicketServicesPermitted_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_externalPacketEfforts", (!this.MarkedForDeletion?_externalPacketEfforts:null));
			info.AddValue("_alwaysFetchExternalPacketEfforts", _alwaysFetchExternalPacketEfforts);
			info.AddValue("_alreadyFetchedExternalPacketEfforts", _alreadyFetchedExternalPacketEfforts);
			info.AddValue("_ticketServicesPermitted", (!this.MarkedForDeletion?_ticketServicesPermitted:null));
			info.AddValue("_alwaysFetchTicketServicesPermitted", _alwaysFetchTicketServicesPermitted);
			info.AddValue("_alreadyFetchedTicketServicesPermitted", _alreadyFetchedTicketServicesPermitted);
			info.AddValue("_ticketCollectionViaTicketServicesPermitted", (!this.MarkedForDeletion?_ticketCollectionViaTicketServicesPermitted:null));
			info.AddValue("_alwaysFetchTicketCollectionViaTicketServicesPermitted", _alwaysFetchTicketCollectionViaTicketServicesPermitted);
			info.AddValue("_alreadyFetchedTicketCollectionViaTicketServicesPermitted", _alreadyFetchedTicketCollectionViaTicketServicesPermitted);
			info.AddValue("_net", (!this.MarkedForDeletion?_net:null));
			info.AddValue("_netReturnsNewIfNotFound", _netReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchNet", _alwaysFetchNet);
			info.AddValue("_alreadyFetchedNet", _alreadyFetchedNet);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Net":
					_alreadyFetchedNet = true;
					this.Net = (NetEntity)entity;
					break;
				case "ExternalPacketEfforts":
					_alreadyFetchedExternalPacketEfforts = true;
					if(entity!=null)
					{
						this.ExternalPacketEfforts.Add((ExternalPacketEffortEntity)entity);
					}
					break;
				case "TicketServicesPermitted":
					_alreadyFetchedTicketServicesPermitted = true;
					if(entity!=null)
					{
						this.TicketServicesPermitted.Add((TicketServicesPermittedEntity)entity);
					}
					break;
				case "TicketCollectionViaTicketServicesPermitted":
					_alreadyFetchedTicketCollectionViaTicketServicesPermitted = true;
					if(entity!=null)
					{
						this.TicketCollectionViaTicketServicesPermitted.Add((TicketEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Net":
					SetupSyncNet(relatedEntity);
					break;
				case "ExternalPacketEfforts":
					_externalPacketEfforts.Add((ExternalPacketEffortEntity)relatedEntity);
					break;
				case "TicketServicesPermitted":
					_ticketServicesPermitted.Add((TicketServicesPermittedEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Net":
					DesetupSyncNet(false, true);
					break;
				case "ExternalPacketEfforts":
					this.PerformRelatedEntityRemoval(_externalPacketEfforts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketServicesPermitted":
					this.PerformRelatedEntityRemoval(_ticketServicesPermitted, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_net!=null)
			{
				toReturn.Add(_net);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_externalPacketEfforts);
			toReturn.Add(_ticketServicesPermitted);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="lineID">PK value for Line which data should be fetched into this Line object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 lineID)
		{
			return FetchUsingPK(lineID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="lineID">PK value for Line which data should be fetched into this Line object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 lineID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(lineID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="lineID">PK value for Line which data should be fetched into this Line object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 lineID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(lineID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="lineID">PK value for Line which data should be fetched into this Line object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 lineID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(lineID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.LineID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new LineRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalPacketEffortEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketEfforts(bool forceFetch)
		{
			return GetMultiExternalPacketEfforts(forceFetch, _externalPacketEfforts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalPacketEffortEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketEfforts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalPacketEfforts(forceFetch, _externalPacketEfforts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketEfforts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalPacketEfforts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketEfforts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalPacketEfforts || forceFetch || _alwaysFetchExternalPacketEfforts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalPacketEfforts);
				_externalPacketEfforts.SuppressClearInGetMulti=!forceFetch;
				_externalPacketEfforts.EntityFactoryToUse = entityFactoryToUse;
				_externalPacketEfforts.GetMultiManyToOne(null, null, null, this, null, null, null, filter);
				_externalPacketEfforts.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalPacketEfforts = true;
			}
			return _externalPacketEfforts;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalPacketEfforts'. These settings will be taken into account
		/// when the property ExternalPacketEfforts is requested or GetMultiExternalPacketEfforts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalPacketEfforts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalPacketEfforts.SortClauses=sortClauses;
			_externalPacketEfforts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketServicesPermittedEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketServicesPermittedEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection GetMultiTicketServicesPermitted(bool forceFetch)
		{
			return GetMultiTicketServicesPermitted(forceFetch, _ticketServicesPermitted.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketServicesPermittedEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketServicesPermittedEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection GetMultiTicketServicesPermitted(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketServicesPermitted(forceFetch, _ticketServicesPermitted.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketServicesPermittedEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection GetMultiTicketServicesPermitted(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketServicesPermitted(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketServicesPermittedEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection GetMultiTicketServicesPermitted(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketServicesPermitted || forceFetch || _alwaysFetchTicketServicesPermitted) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketServicesPermitted);
				_ticketServicesPermitted.SuppressClearInGetMulti=!forceFetch;
				_ticketServicesPermitted.EntityFactoryToUse = entityFactoryToUse;
				_ticketServicesPermitted.GetMultiManyToOne(this, null, filter);
				_ticketServicesPermitted.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketServicesPermitted = true;
			}
			return _ticketServicesPermitted;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketServicesPermitted'. These settings will be taken into account
		/// when the property TicketServicesPermitted is requested or GetMultiTicketServicesPermitted is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketServicesPermitted(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketServicesPermitted.SortClauses=sortClauses;
			_ticketServicesPermitted.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaTicketServicesPermitted(bool forceFetch)
		{
			return GetMultiTicketCollectionViaTicketServicesPermitted(forceFetch, _ticketCollectionViaTicketServicesPermitted.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaTicketServicesPermitted(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTicketCollectionViaTicketServicesPermitted || forceFetch || _alwaysFetchTicketCollectionViaTicketServicesPermitted) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketCollectionViaTicketServicesPermitted);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(LineFields.LineID, ComparisonOperator.Equal, this.LineID, "LineEntity__"));
				_ticketCollectionViaTicketServicesPermitted.SuppressClearInGetMulti=!forceFetch;
				_ticketCollectionViaTicketServicesPermitted.EntityFactoryToUse = entityFactoryToUse;
				_ticketCollectionViaTicketServicesPermitted.GetMulti(filter, GetRelationsForField("TicketCollectionViaTicketServicesPermitted"));
				_ticketCollectionViaTicketServicesPermitted.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketCollectionViaTicketServicesPermitted = true;
			}
			return _ticketCollectionViaTicketServicesPermitted;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketCollectionViaTicketServicesPermitted'. These settings will be taken into account
		/// when the property TicketCollectionViaTicketServicesPermitted is requested or GetMultiTicketCollectionViaTicketServicesPermitted is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketCollectionViaTicketServicesPermitted(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketCollectionViaTicketServicesPermitted.SortClauses=sortClauses;
			_ticketCollectionViaTicketServicesPermitted.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'NetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'NetEntity' which is related to this entity.</returns>
		public NetEntity GetSingleNet()
		{
			return GetSingleNet(false);
		}

		/// <summary> Retrieves the related entity of type 'NetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'NetEntity' which is related to this entity.</returns>
		public virtual NetEntity GetSingleNet(bool forceFetch)
		{
			if( ( !_alreadyFetchedNet || forceFetch || _alwaysFetchNet) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.NetEntityUsingNetID);
				NetEntity newEntity = new NetEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.NetID);
				}
				if(fetchResult)
				{
					newEntity = (NetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_netReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Net = newEntity;
				_alreadyFetchedNet = fetchResult;
			}
			return _net;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Net", _net);
			toReturn.Add("ExternalPacketEfforts", _externalPacketEfforts);
			toReturn.Add("TicketServicesPermitted", _ticketServicesPermitted);
			toReturn.Add("TicketCollectionViaTicketServicesPermitted", _ticketCollectionViaTicketServicesPermitted);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="lineID">PK value for Line which data should be fetched into this Line object</param>
		/// <param name="validator">The validator object for this LineEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 lineID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(lineID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_externalPacketEfforts = new VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection();
			_externalPacketEfforts.SetContainingEntityInfo(this, "Line");

			_ticketServicesPermitted = new VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection();
			_ticketServicesPermitted.SetContainingEntityInfo(this, "Line");
			_ticketCollectionViaTicketServicesPermitted = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_netReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearingLine", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineCodeExtern", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NetID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NetworkLine", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PayRollRelevant", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RaillingSchedule", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SystemLine", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidUntil", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _net</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncNet(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _net, new PropertyChangedEventHandler( OnNetPropertyChanged ), "Net", VarioSL.Entities.RelationClasses.StaticLineRelations.NetEntityUsingNetIDStatic, true, signalRelatedEntity, "Lines", resetFKFields, new int[] { (int)LineFieldIndex.NetID } );		
			_net = null;
		}
		
		/// <summary> setups the sync logic for member _net</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncNet(IEntityCore relatedEntity)
		{
			if(_net!=relatedEntity)
			{		
				DesetupSyncNet(true, true);
				_net = (NetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _net, new PropertyChangedEventHandler( OnNetPropertyChanged ), "Net", VarioSL.Entities.RelationClasses.StaticLineRelations.NetEntityUsingNetIDStatic, true, ref _alreadyFetchedNet, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnNetPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="lineID">PK value for Line which data should be fetched into this Line object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 lineID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)LineFieldIndex.LineID].ForcedCurrentValueWrite(lineID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateLineDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new LineEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static LineRelations Relations
		{
			get	{ return new LineRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalPacketEffort' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalPacketEfforts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection(), (IEntityRelation)GetRelationsForField("ExternalPacketEfforts")[0], (int)VarioSL.Entities.EntityType.LineEntity, (int)VarioSL.Entities.EntityType.ExternalPacketEffortEntity, 0, null, null, null, "ExternalPacketEfforts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketServicesPermitted' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketServicesPermitted
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection(), (IEntityRelation)GetRelationsForField("TicketServicesPermitted")[0], (int)VarioSL.Entities.EntityType.LineEntity, (int)VarioSL.Entities.EntityType.TicketServicesPermittedEntity, 0, null, null, null, "TicketServicesPermitted", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketCollectionViaTicketServicesPermitted
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketServicesPermittedEntityUsingLineID;
				intermediateRelation.SetAliases(string.Empty, "TicketServicesPermitted_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.LineEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, GetRelationsForField("TicketCollectionViaTicketServicesPermitted"), "TicketCollectionViaTicketServicesPermitted", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Net'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNet
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.NetCollection(), (IEntityRelation)GetRelationsForField("Net")[0], (int)VarioSL.Entities.EntityType.LineEntity, (int)VarioSL.Entities.EntityType.NetEntity, 0, null, null, null, "Net", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClearingLine property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."CLEARINGLINE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClearingLine
		{
			get { return (System.String)GetValue((int)LineFieldIndex.ClearingLine, true); }
			set	{ SetValue((int)LineFieldIndex.ClearingLine, value, true); }
		}

		/// <summary> The ClientID property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)LineFieldIndex.ClientID, true); }
			set	{ SetValue((int)LineFieldIndex.ClientID, value, true); }
		}

		/// <summary> The IsActive property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."ISACTIVE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsActive
		{
			get { return (Nullable<System.Boolean>)GetValue((int)LineFieldIndex.IsActive, false); }
			set	{ SetValue((int)LineFieldIndex.IsActive, value, true); }
		}

		/// <summary> The LineCode property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."LINECODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LineCode
		{
			get { return (System.String)GetValue((int)LineFieldIndex.LineCode, true); }
			set	{ SetValue((int)LineFieldIndex.LineCode, value, true); }
		}

		/// <summary> The LineCodeExtern property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."LINECODEEXTERN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LineCodeExtern
		{
			get { return (System.String)GetValue((int)LineFieldIndex.LineCodeExtern, true); }
			set	{ SetValue((int)LineFieldIndex.LineCodeExtern, value, true); }
		}

		/// <summary> The LineID property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."LINEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 LineID
		{
			get { return (System.Int64)GetValue((int)LineFieldIndex.LineID, true); }
			set	{ SetValue((int)LineFieldIndex.LineID, value, true); }
		}

		/// <summary> The LineName property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."LINENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LineName
		{
			get { return (System.String)GetValue((int)LineFieldIndex.LineName, true); }
			set	{ SetValue((int)LineFieldIndex.LineName, value, true); }
		}

		/// <summary> The LineNo property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."LINENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 LineNo
		{
			get { return (System.Int64)GetValue((int)LineFieldIndex.LineNo, true); }
			set	{ SetValue((int)LineFieldIndex.LineNo, value, true); }
		}

		/// <summary> The NetID property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."NETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 NetID
		{
			get { return (System.Int64)GetValue((int)LineFieldIndex.NetID, true); }
			set	{ SetValue((int)LineFieldIndex.NetID, value, true); }
		}

		/// <summary> The NetworkLine property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."NETWORKLINE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NetworkLine
		{
			get { return (System.String)GetValue((int)LineFieldIndex.NetworkLine, true); }
			set	{ SetValue((int)LineFieldIndex.NetworkLine, value, true); }
		}

		/// <summary> The PayRollRelevant property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."PAYROLLRELEVANT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 PayRollRelevant
		{
			get { return (System.Int16)GetValue((int)LineFieldIndex.PayRollRelevant, true); }
			set	{ SetValue((int)LineFieldIndex.PayRollRelevant, value, true); }
		}

		/// <summary> The RaillingSchedule property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."RAILLINESCHEDULE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RaillingSchedule
		{
			get { return (System.String)GetValue((int)LineFieldIndex.RaillingSchedule, true); }
			set	{ SetValue((int)LineFieldIndex.RaillingSchedule, value, true); }
		}

		/// <summary> The SystemLine property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."SYSTEMLINE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SystemLine
		{
			get { return (System.String)GetValue((int)LineFieldIndex.SystemLine, true); }
			set	{ SetValue((int)LineFieldIndex.SystemLine, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)LineFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)LineFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidUntil property of the Entity Line<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_LINE"."VALIDUNTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidUntil
		{
			get { return (System.DateTime)GetValue((int)LineFieldIndex.ValidUntil, true); }
			set	{ SetValue((int)LineFieldIndex.ValidUntil, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalPacketEfforts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection ExternalPacketEfforts
		{
			get	{ return GetMultiExternalPacketEfforts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalPacketEfforts. When set to true, ExternalPacketEfforts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalPacketEfforts is accessed. You can always execute/ a forced fetch by calling GetMultiExternalPacketEfforts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalPacketEfforts
		{
			get	{ return _alwaysFetchExternalPacketEfforts; }
			set	{ _alwaysFetchExternalPacketEfforts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalPacketEfforts already has been fetched. Setting this property to false when ExternalPacketEfforts has been fetched
		/// will clear the ExternalPacketEfforts collection well. Setting this property to true while ExternalPacketEfforts hasn't been fetched disables lazy loading for ExternalPacketEfforts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalPacketEfforts
		{
			get { return _alreadyFetchedExternalPacketEfforts;}
			set 
			{
				if(_alreadyFetchedExternalPacketEfforts && !value && (_externalPacketEfforts != null))
				{
					_externalPacketEfforts.Clear();
				}
				_alreadyFetchedExternalPacketEfforts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketServicesPermittedEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketServicesPermitted()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketServicesPermittedCollection TicketServicesPermitted
		{
			get	{ return GetMultiTicketServicesPermitted(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketServicesPermitted. When set to true, TicketServicesPermitted is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketServicesPermitted is accessed. You can always execute/ a forced fetch by calling GetMultiTicketServicesPermitted(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketServicesPermitted
		{
			get	{ return _alwaysFetchTicketServicesPermitted; }
			set	{ _alwaysFetchTicketServicesPermitted = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketServicesPermitted already has been fetched. Setting this property to false when TicketServicesPermitted has been fetched
		/// will clear the TicketServicesPermitted collection well. Setting this property to true while TicketServicesPermitted hasn't been fetched disables lazy loading for TicketServicesPermitted</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketServicesPermitted
		{
			get { return _alreadyFetchedTicketServicesPermitted;}
			set 
			{
				if(_alreadyFetchedTicketServicesPermitted && !value && (_ticketServicesPermitted != null))
				{
					_ticketServicesPermitted.Clear();
				}
				_alreadyFetchedTicketServicesPermitted = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketCollectionViaTicketServicesPermitted()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketCollectionViaTicketServicesPermitted
		{
			get { return GetMultiTicketCollectionViaTicketServicesPermitted(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketCollectionViaTicketServicesPermitted. When set to true, TicketCollectionViaTicketServicesPermitted is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketCollectionViaTicketServicesPermitted is accessed. You can always execute a forced fetch by calling GetMultiTicketCollectionViaTicketServicesPermitted(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketCollectionViaTicketServicesPermitted
		{
			get	{ return _alwaysFetchTicketCollectionViaTicketServicesPermitted; }
			set	{ _alwaysFetchTicketCollectionViaTicketServicesPermitted = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketCollectionViaTicketServicesPermitted already has been fetched. Setting this property to false when TicketCollectionViaTicketServicesPermitted has been fetched
		/// will clear the TicketCollectionViaTicketServicesPermitted collection well. Setting this property to true while TicketCollectionViaTicketServicesPermitted hasn't been fetched disables lazy loading for TicketCollectionViaTicketServicesPermitted</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketCollectionViaTicketServicesPermitted
		{
			get { return _alreadyFetchedTicketCollectionViaTicketServicesPermitted;}
			set 
			{
				if(_alreadyFetchedTicketCollectionViaTicketServicesPermitted && !value && (_ticketCollectionViaTicketServicesPermitted != null))
				{
					_ticketCollectionViaTicketServicesPermitted.Clear();
				}
				_alreadyFetchedTicketCollectionViaTicketServicesPermitted = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'NetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleNet()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual NetEntity Net
		{
			get	{ return GetSingleNet(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncNet(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Lines", "Net", _net, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Net. When set to true, Net is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Net is accessed. You can always execute a forced fetch by calling GetSingleNet(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNet
		{
			get	{ return _alwaysFetchNet; }
			set	{ _alwaysFetchNet = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Net already has been fetched. Setting this property to false when Net has been fetched
		/// will set Net to null as well. Setting this property to true while Net hasn't been fetched disables lazy loading for Net</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNet
		{
			get { return _alreadyFetchedNet;}
			set 
			{
				if(_alreadyFetchedNet && !value)
				{
					this.Net = null;
				}
				_alreadyFetchedNet = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Net is not found
		/// in the database. When set to true, Net will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool NetReturnsNewIfNotFound
		{
			get	{ return _netReturnsNewIfNotFound; }
			set { _netReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.LineEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
