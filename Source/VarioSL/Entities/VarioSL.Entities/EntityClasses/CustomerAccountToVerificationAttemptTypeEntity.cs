﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CustomerAccountToVerificationAttemptType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CustomerAccountToVerificationAttemptTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CustomerAccountEntity _customerAccount;
		private bool	_alwaysFetchCustomerAccount, _alreadyFetchedCustomerAccount, _customerAccountReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CustomerAccount</summary>
			public static readonly string CustomerAccount = "CustomerAccount";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CustomerAccountToVerificationAttemptTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CustomerAccountToVerificationAttemptTypeEntity() :base("CustomerAccountToVerificationAttemptTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="customerAccountID">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="verificationAttemptType">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		public CustomerAccountToVerificationAttemptTypeEntity(System.Int64 customerAccountID, VarioSL.Entities.Enumerations.VerificationAttemptType verificationAttemptType):base("CustomerAccountToVerificationAttemptTypeEntity")
		{
			InitClassFetch(customerAccountID, verificationAttemptType, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="customerAccountID">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="verificationAttemptType">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CustomerAccountToVerificationAttemptTypeEntity(System.Int64 customerAccountID, VarioSL.Entities.Enumerations.VerificationAttemptType verificationAttemptType, IPrefetchPath prefetchPathToUse):base("CustomerAccountToVerificationAttemptTypeEntity")
		{
			InitClassFetch(customerAccountID, verificationAttemptType, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="customerAccountID">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="verificationAttemptType">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="validator">The custom validator object for this CustomerAccountToVerificationAttemptTypeEntity</param>
		public CustomerAccountToVerificationAttemptTypeEntity(System.Int64 customerAccountID, VarioSL.Entities.Enumerations.VerificationAttemptType verificationAttemptType, IValidator validator):base("CustomerAccountToVerificationAttemptTypeEntity")
		{
			InitClassFetch(customerAccountID, verificationAttemptType, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CustomerAccountToVerificationAttemptTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customerAccount = (CustomerAccountEntity)info.GetValue("_customerAccount", typeof(CustomerAccountEntity));
			if(_customerAccount!=null)
			{
				_customerAccount.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_customerAccountReturnsNewIfNotFound = info.GetBoolean("_customerAccountReturnsNewIfNotFound");
			_alwaysFetchCustomerAccount = info.GetBoolean("_alwaysFetchCustomerAccount");
			_alreadyFetchedCustomerAccount = info.GetBoolean("_alreadyFetchedCustomerAccount");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CustomerAccountToVerificationAttemptTypeFieldIndex)fieldIndex)
			{
				case CustomerAccountToVerificationAttemptTypeFieldIndex.CustomerAccountID:
					DesetupSyncCustomerAccount(true, false);
					_alreadyFetchedCustomerAccount = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomerAccount = (_customerAccount != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CustomerAccount":
					toReturn.Add(Relations.CustomerAccountEntityUsingCustomerAccountID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customerAccount", (!this.MarkedForDeletion?_customerAccount:null));
			info.AddValue("_customerAccountReturnsNewIfNotFound", _customerAccountReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCustomerAccount", _alwaysFetchCustomerAccount);
			info.AddValue("_alreadyFetchedCustomerAccount", _alreadyFetchedCustomerAccount);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CustomerAccount":
					_alreadyFetchedCustomerAccount = true;
					this.CustomerAccount = (CustomerAccountEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CustomerAccount":
					SetupSyncCustomerAccount(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CustomerAccount":
					DesetupSyncCustomerAccount(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_customerAccount!=null)
			{
				toReturn.Add(_customerAccount);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerAccountID">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="verificationAttemptType">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customerAccountID, VarioSL.Entities.Enumerations.VerificationAttemptType verificationAttemptType)
		{
			return FetchUsingPK(customerAccountID, verificationAttemptType, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerAccountID">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="verificationAttemptType">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customerAccountID, VarioSL.Entities.Enumerations.VerificationAttemptType verificationAttemptType, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(customerAccountID, verificationAttemptType, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerAccountID">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="verificationAttemptType">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customerAccountID, VarioSL.Entities.Enumerations.VerificationAttemptType verificationAttemptType, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(customerAccountID, verificationAttemptType, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerAccountID">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="verificationAttemptType">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customerAccountID, VarioSL.Entities.Enumerations.VerificationAttemptType verificationAttemptType, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(customerAccountID, verificationAttemptType, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CustomerAccountID, this.VerificationAttemptType, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CustomerAccountToVerificationAttemptTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CustomerAccountEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomerAccountEntity' which is related to this entity.</returns>
		public CustomerAccountEntity GetSingleCustomerAccount()
		{
			return GetSingleCustomerAccount(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomerAccountEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomerAccountEntity' which is related to this entity.</returns>
		public virtual CustomerAccountEntity GetSingleCustomerAccount(bool forceFetch)
		{
			if( ( !_alreadyFetchedCustomerAccount || forceFetch || _alwaysFetchCustomerAccount) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomerAccountEntityUsingCustomerAccountID);
				CustomerAccountEntity newEntity = new CustomerAccountEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CustomerAccountID);
				}
				if(fetchResult)
				{
					newEntity = (CustomerAccountEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_customerAccountReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CustomerAccount = newEntity;
				_alreadyFetchedCustomerAccount = fetchResult;
			}
			return _customerAccount;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CustomerAccount", _customerAccount);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="customerAccountID">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="verificationAttemptType">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="validator">The validator object for this CustomerAccountToVerificationAttemptTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 customerAccountID, VarioSL.Entities.Enumerations.VerificationAttemptType verificationAttemptType, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(customerAccountID, verificationAttemptType, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_customerAccountReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerAccountID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TotalAttempts", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VerificationAttemptType", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _customerAccount</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCustomerAccount(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _customerAccount, new PropertyChangedEventHandler( OnCustomerAccountPropertyChanged ), "CustomerAccount", VarioSL.Entities.RelationClasses.StaticCustomerAccountToVerificationAttemptTypeRelations.CustomerAccountEntityUsingCustomerAccountIDStatic, true, signalRelatedEntity, "CustomerAccountToVerificationAttemptTypes", resetFKFields, new int[] { (int)CustomerAccountToVerificationAttemptTypeFieldIndex.CustomerAccountID } );		
			_customerAccount = null;
		}
		
		/// <summary> setups the sync logic for member _customerAccount</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCustomerAccount(IEntityCore relatedEntity)
		{
			if(_customerAccount!=relatedEntity)
			{		
				DesetupSyncCustomerAccount(true, true);
				_customerAccount = (CustomerAccountEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _customerAccount, new PropertyChangedEventHandler( OnCustomerAccountPropertyChanged ), "CustomerAccount", VarioSL.Entities.RelationClasses.StaticCustomerAccountToVerificationAttemptTypeRelations.CustomerAccountEntityUsingCustomerAccountIDStatic, true, ref _alreadyFetchedCustomerAccount, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCustomerAccountPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="customerAccountID">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="verificationAttemptType">PK value for CustomerAccountToVerificationAttemptType which data should be fetched into this CustomerAccountToVerificationAttemptType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 customerAccountID, VarioSL.Entities.Enumerations.VerificationAttemptType verificationAttemptType, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CustomerAccountToVerificationAttemptTypeFieldIndex.CustomerAccountID].ForcedCurrentValueWrite(customerAccountID);
				this.Fields[(int)CustomerAccountToVerificationAttemptTypeFieldIndex.VerificationAttemptType].ForcedCurrentValueWrite(verificationAttemptType);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCustomerAccountToVerificationAttemptTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CustomerAccountToVerificationAttemptTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CustomerAccountToVerificationAttemptTypeRelations Relations
		{
			get	{ return new CustomerAccountToVerificationAttemptTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomerAccount'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerAccount
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomerAccountCollection(), (IEntityRelation)GetRelationsForField("CustomerAccount")[0], (int)VarioSL.Entities.EntityType.CustomerAccountToVerificationAttemptTypeEntity, (int)VarioSL.Entities.EntityType.CustomerAccountEntity, 0, null, null, null, "CustomerAccount", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CustomerAccountID property of the Entity CustomerAccountToVerificationAttemptType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCTOVERATTEMPTTYPE"."CUSTOMERACCOUNTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 CustomerAccountID
		{
			get { return (System.Int64)GetValue((int)CustomerAccountToVerificationAttemptTypeFieldIndex.CustomerAccountID, true); }
			set	{ SetValue((int)CustomerAccountToVerificationAttemptTypeFieldIndex.CustomerAccountID, value, true); }
		}

		/// <summary> The LastModified property of the Entity CustomerAccountToVerificationAttemptType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCTOVERATTEMPTTYPE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CustomerAccountToVerificationAttemptTypeFieldIndex.LastModified, true); }
			set	{ SetValue((int)CustomerAccountToVerificationAttemptTypeFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CustomerAccountToVerificationAttemptType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCTOVERATTEMPTTYPE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CustomerAccountToVerificationAttemptTypeFieldIndex.LastUser, true); }
			set	{ SetValue((int)CustomerAccountToVerificationAttemptTypeFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TotalAttempts property of the Entity CustomerAccountToVerificationAttemptType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCTOVERATTEMPTTYPE"."TOTALATTEMPTS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TotalAttempts
		{
			get { return (System.Int32)GetValue((int)CustomerAccountToVerificationAttemptTypeFieldIndex.TotalAttempts, true); }
			set	{ SetValue((int)CustomerAccountToVerificationAttemptTypeFieldIndex.TotalAttempts, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CustomerAccountToVerificationAttemptType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCTOVERATTEMPTTYPE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CustomerAccountToVerificationAttemptTypeFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CustomerAccountToVerificationAttemptTypeFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The VerificationAttemptType property of the Entity CustomerAccountToVerificationAttemptType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCTOVERATTEMPTTYPE"."VERIFICATIONATTEMPTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual VarioSL.Entities.Enumerations.VerificationAttemptType VerificationAttemptType
		{
			get { return (VarioSL.Entities.Enumerations.VerificationAttemptType)GetValue((int)CustomerAccountToVerificationAttemptTypeFieldIndex.VerificationAttemptType, true); }
			set	{ SetValue((int)CustomerAccountToVerificationAttemptTypeFieldIndex.VerificationAttemptType, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CustomerAccountEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCustomerAccount()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CustomerAccountEntity CustomerAccount
		{
			get	{ return GetSingleCustomerAccount(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCustomerAccount(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomerAccountToVerificationAttemptTypes", "CustomerAccount", _customerAccount, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerAccount. When set to true, CustomerAccount is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerAccount is accessed. You can always execute a forced fetch by calling GetSingleCustomerAccount(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerAccount
		{
			get	{ return _alwaysFetchCustomerAccount; }
			set	{ _alwaysFetchCustomerAccount = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerAccount already has been fetched. Setting this property to false when CustomerAccount has been fetched
		/// will set CustomerAccount to null as well. Setting this property to true while CustomerAccount hasn't been fetched disables lazy loading for CustomerAccount</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerAccount
		{
			get { return _alreadyFetchedCustomerAccount;}
			set 
			{
				if(_alreadyFetchedCustomerAccount && !value)
				{
					this.CustomerAccount = null;
				}
				_alreadyFetchedCustomerAccount = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CustomerAccount is not found
		/// in the database. When set to true, CustomerAccount will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CustomerAccountReturnsNewIfNotFound
		{
			get	{ return _customerAccountReturnsNewIfNotFound; }
			set { _customerAccountReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CustomerAccountToVerificationAttemptTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
