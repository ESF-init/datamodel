﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FilterElement'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FilterElementEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.FilterListElementCollection	_filterListElements;
		private bool	_alwaysFetchFilterListElements, _alreadyFetchedFilterListElements;
		private ConfigurationEntity _configuration;
		private bool	_alwaysFetchConfiguration, _alreadyFetchedConfiguration, _configurationReturnsNewIfNotFound;
		private ConfigurationDefinitionEntity _configurationDefinition;
		private bool	_alwaysFetchConfigurationDefinition, _alreadyFetchedConfigurationDefinition, _configurationDefinitionReturnsNewIfNotFound;
		private FilterTypeEntity _filterType;
		private bool	_alwaysFetchFilterType, _alreadyFetchedFilterType, _filterTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Configuration</summary>
			public static readonly string Configuration = "Configuration";
			/// <summary>Member name ConfigurationDefinition</summary>
			public static readonly string ConfigurationDefinition = "ConfigurationDefinition";
			/// <summary>Member name FilterType</summary>
			public static readonly string FilterType = "FilterType";
			/// <summary>Member name FilterListElements</summary>
			public static readonly string FilterListElements = "FilterListElements";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FilterElementEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FilterElementEntity() :base("FilterElementEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="filterElementID">PK value for FilterElement which data should be fetched into this FilterElement object</param>
		public FilterElementEntity(System.Int64 filterElementID):base("FilterElementEntity")
		{
			InitClassFetch(filterElementID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="filterElementID">PK value for FilterElement which data should be fetched into this FilterElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FilterElementEntity(System.Int64 filterElementID, IPrefetchPath prefetchPathToUse):base("FilterElementEntity")
		{
			InitClassFetch(filterElementID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="filterElementID">PK value for FilterElement which data should be fetched into this FilterElement object</param>
		/// <param name="validator">The custom validator object for this FilterElementEntity</param>
		public FilterElementEntity(System.Int64 filterElementID, IValidator validator):base("FilterElementEntity")
		{
			InitClassFetch(filterElementID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FilterElementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_filterListElements = (VarioSL.Entities.CollectionClasses.FilterListElementCollection)info.GetValue("_filterListElements", typeof(VarioSL.Entities.CollectionClasses.FilterListElementCollection));
			_alwaysFetchFilterListElements = info.GetBoolean("_alwaysFetchFilterListElements");
			_alreadyFetchedFilterListElements = info.GetBoolean("_alreadyFetchedFilterListElements");
			_configuration = (ConfigurationEntity)info.GetValue("_configuration", typeof(ConfigurationEntity));
			if(_configuration!=null)
			{
				_configuration.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_configurationReturnsNewIfNotFound = info.GetBoolean("_configurationReturnsNewIfNotFound");
			_alwaysFetchConfiguration = info.GetBoolean("_alwaysFetchConfiguration");
			_alreadyFetchedConfiguration = info.GetBoolean("_alreadyFetchedConfiguration");

			_configurationDefinition = (ConfigurationDefinitionEntity)info.GetValue("_configurationDefinition", typeof(ConfigurationDefinitionEntity));
			if(_configurationDefinition!=null)
			{
				_configurationDefinition.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_configurationDefinitionReturnsNewIfNotFound = info.GetBoolean("_configurationDefinitionReturnsNewIfNotFound");
			_alwaysFetchConfigurationDefinition = info.GetBoolean("_alwaysFetchConfigurationDefinition");
			_alreadyFetchedConfigurationDefinition = info.GetBoolean("_alreadyFetchedConfigurationDefinition");

			_filterType = (FilterTypeEntity)info.GetValue("_filterType", typeof(FilterTypeEntity));
			if(_filterType!=null)
			{
				_filterType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_filterTypeReturnsNewIfNotFound = info.GetBoolean("_filterTypeReturnsNewIfNotFound");
			_alwaysFetchFilterType = info.GetBoolean("_alwaysFetchFilterType");
			_alreadyFetchedFilterType = info.GetBoolean("_alreadyFetchedFilterType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FilterElementFieldIndex)fieldIndex)
			{
				case FilterElementFieldIndex.FilterTypeID:
					DesetupSyncFilterType(true, false);
					_alreadyFetchedFilterType = false;
					break;
				case FilterElementFieldIndex.ValueListDataSourceID:
					DesetupSyncConfiguration(true, false);
					_alreadyFetchedConfiguration = false;
					DesetupSyncConfigurationDefinition(true, false);
					_alreadyFetchedConfigurationDefinition = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFilterListElements = (_filterListElements.Count > 0);
			_alreadyFetchedConfiguration = (_configuration != null);
			_alreadyFetchedConfigurationDefinition = (_configurationDefinition != null);
			_alreadyFetchedFilterType = (_filterType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Configuration":
					toReturn.Add(Relations.ConfigurationEntityUsingValueListDataSourceID);
					break;
				case "ConfigurationDefinition":
					toReturn.Add(Relations.ConfigurationDefinitionEntityUsingValueListDataSourceID);
					break;
				case "FilterType":
					toReturn.Add(Relations.FilterTypeEntityUsingFilterTypeID);
					break;
				case "FilterListElements":
					toReturn.Add(Relations.FilterListElementEntityUsingFilterElementID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_filterListElements", (!this.MarkedForDeletion?_filterListElements:null));
			info.AddValue("_alwaysFetchFilterListElements", _alwaysFetchFilterListElements);
			info.AddValue("_alreadyFetchedFilterListElements", _alreadyFetchedFilterListElements);
			info.AddValue("_configuration", (!this.MarkedForDeletion?_configuration:null));
			info.AddValue("_configurationReturnsNewIfNotFound", _configurationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchConfiguration", _alwaysFetchConfiguration);
			info.AddValue("_alreadyFetchedConfiguration", _alreadyFetchedConfiguration);
			info.AddValue("_configurationDefinition", (!this.MarkedForDeletion?_configurationDefinition:null));
			info.AddValue("_configurationDefinitionReturnsNewIfNotFound", _configurationDefinitionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchConfigurationDefinition", _alwaysFetchConfigurationDefinition);
			info.AddValue("_alreadyFetchedConfigurationDefinition", _alreadyFetchedConfigurationDefinition);
			info.AddValue("_filterType", (!this.MarkedForDeletion?_filterType:null));
			info.AddValue("_filterTypeReturnsNewIfNotFound", _filterTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFilterType", _alwaysFetchFilterType);
			info.AddValue("_alreadyFetchedFilterType", _alreadyFetchedFilterType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Configuration":
					_alreadyFetchedConfiguration = true;
					this.Configuration = (ConfigurationEntity)entity;
					break;
				case "ConfigurationDefinition":
					_alreadyFetchedConfigurationDefinition = true;
					this.ConfigurationDefinition = (ConfigurationDefinitionEntity)entity;
					break;
				case "FilterType":
					_alreadyFetchedFilterType = true;
					this.FilterType = (FilterTypeEntity)entity;
					break;
				case "FilterListElements":
					_alreadyFetchedFilterListElements = true;
					if(entity!=null)
					{
						this.FilterListElements.Add((FilterListElementEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Configuration":
					SetupSyncConfiguration(relatedEntity);
					break;
				case "ConfigurationDefinition":
					SetupSyncConfigurationDefinition(relatedEntity);
					break;
				case "FilterType":
					SetupSyncFilterType(relatedEntity);
					break;
				case "FilterListElements":
					_filterListElements.Add((FilterListElementEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Configuration":
					DesetupSyncConfiguration(false, true);
					break;
				case "ConfigurationDefinition":
					DesetupSyncConfigurationDefinition(false, true);
					break;
				case "FilterType":
					DesetupSyncFilterType(false, true);
					break;
				case "FilterListElements":
					this.PerformRelatedEntityRemoval(_filterListElements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_configuration!=null)
			{
				toReturn.Add(_configuration);
			}
			if(_configurationDefinition!=null)
			{
				toReturn.Add(_configurationDefinition);
			}
			if(_filterType!=null)
			{
				toReturn.Add(_filterType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_filterListElements);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterElementID">PK value for FilterElement which data should be fetched into this FilterElement object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterElementID)
		{
			return FetchUsingPK(filterElementID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterElementID">PK value for FilterElement which data should be fetched into this FilterElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterElementID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(filterElementID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterElementID">PK value for FilterElement which data should be fetched into this FilterElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterElementID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(filterElementID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterElementID">PK value for FilterElement which data should be fetched into this FilterElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterElementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(filterElementID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FilterElementID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FilterElementRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'FilterListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FilterListElementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterListElementCollection GetMultiFilterListElements(bool forceFetch)
		{
			return GetMultiFilterListElements(forceFetch, _filterListElements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FilterListElementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterListElementCollection GetMultiFilterListElements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFilterListElements(forceFetch, _filterListElements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FilterListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FilterListElementCollection GetMultiFilterListElements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFilterListElements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FilterListElementCollection GetMultiFilterListElements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFilterListElements || forceFetch || _alwaysFetchFilterListElements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_filterListElements);
				_filterListElements.SuppressClearInGetMulti=!forceFetch;
				_filterListElements.EntityFactoryToUse = entityFactoryToUse;
				_filterListElements.GetMultiManyToOne(null, this, null, filter);
				_filterListElements.SuppressClearInGetMulti=false;
				_alreadyFetchedFilterListElements = true;
			}
			return _filterListElements;
		}

		/// <summary> Sets the collection parameters for the collection for 'FilterListElements'. These settings will be taken into account
		/// when the property FilterListElements is requested or GetMultiFilterListElements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFilterListElements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_filterListElements.SortClauses=sortClauses;
			_filterListElements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ConfigurationEntity' which is related to this entity.</returns>
		public ConfigurationEntity GetSingleConfiguration()
		{
			return GetSingleConfiguration(false);
		}

		/// <summary> Retrieves the related entity of type 'ConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ConfigurationEntity' which is related to this entity.</returns>
		public virtual ConfigurationEntity GetSingleConfiguration(bool forceFetch)
		{
			if( ( !_alreadyFetchedConfiguration || forceFetch || _alwaysFetchConfiguration) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ConfigurationEntityUsingValueListDataSourceID);
				ConfigurationEntity newEntity = new ConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ValueListDataSourceID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_configurationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Configuration = newEntity;
				_alreadyFetchedConfiguration = fetchResult;
			}
			return _configuration;
		}


		/// <summary> Retrieves the related entity of type 'ConfigurationDefinitionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ConfigurationDefinitionEntity' which is related to this entity.</returns>
		public ConfigurationDefinitionEntity GetSingleConfigurationDefinition()
		{
			return GetSingleConfigurationDefinition(false);
		}

		/// <summary> Retrieves the related entity of type 'ConfigurationDefinitionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ConfigurationDefinitionEntity' which is related to this entity.</returns>
		public virtual ConfigurationDefinitionEntity GetSingleConfigurationDefinition(bool forceFetch)
		{
			if( ( !_alreadyFetchedConfigurationDefinition || forceFetch || _alwaysFetchConfigurationDefinition) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ConfigurationDefinitionEntityUsingValueListDataSourceID);
				ConfigurationDefinitionEntity newEntity = new ConfigurationDefinitionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ValueListDataSourceID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ConfigurationDefinitionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_configurationDefinitionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ConfigurationDefinition = newEntity;
				_alreadyFetchedConfigurationDefinition = fetchResult;
			}
			return _configurationDefinition;
		}


		/// <summary> Retrieves the related entity of type 'FilterTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FilterTypeEntity' which is related to this entity.</returns>
		public FilterTypeEntity GetSingleFilterType()
		{
			return GetSingleFilterType(false);
		}

		/// <summary> Retrieves the related entity of type 'FilterTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FilterTypeEntity' which is related to this entity.</returns>
		public virtual FilterTypeEntity GetSingleFilterType(bool forceFetch)
		{
			if( ( !_alreadyFetchedFilterType || forceFetch || _alwaysFetchFilterType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FilterTypeEntityUsingFilterTypeID);
				FilterTypeEntity newEntity = new FilterTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FilterTypeID);
				}
				if(fetchResult)
				{
					newEntity = (FilterTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_filterTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FilterType = newEntity;
				_alreadyFetchedFilterType = fetchResult;
			}
			return _filterType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Configuration", _configuration);
			toReturn.Add("ConfigurationDefinition", _configurationDefinition);
			toReturn.Add("FilterType", _filterType);
			toReturn.Add("FilterListElements", _filterListElements);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="filterElementID">PK value for FilterElement which data should be fetched into this FilterElement object</param>
		/// <param name="validator">The validator object for this FilterElementEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 filterElementID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(filterElementID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_filterListElements = new VarioSL.Entities.CollectionClasses.FilterListElementCollection();
			_filterListElements.SetContainingEntityInfo(this, "FilterElement");
			_configurationReturnsNewIfNotFound = false;
			_configurationDefinitionReturnsNewIfNotFound = false;
			_filterTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CrystalReportName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EditMask", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterElementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HelpText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseFlexValues", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseMixedValues", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseMultipleValues", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseRangeValues", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueListClientFilter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueListColumn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueListDataSourceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueListShown", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueListTable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueListWhere", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _configuration</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncConfiguration(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _configuration, new PropertyChangedEventHandler( OnConfigurationPropertyChanged ), "Configuration", VarioSL.Entities.RelationClasses.StaticFilterElementRelations.ConfigurationEntityUsingValueListDataSourceIDStatic, true, signalRelatedEntity, "FilterElements", resetFKFields, new int[] { (int)FilterElementFieldIndex.ValueListDataSourceID } );		
			_configuration = null;
		}
		
		/// <summary> setups the sync logic for member _configuration</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncConfiguration(IEntityCore relatedEntity)
		{
			if(_configuration!=relatedEntity)
			{		
				DesetupSyncConfiguration(true, true);
				_configuration = (ConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _configuration, new PropertyChangedEventHandler( OnConfigurationPropertyChanged ), "Configuration", VarioSL.Entities.RelationClasses.StaticFilterElementRelations.ConfigurationEntityUsingValueListDataSourceIDStatic, true, ref _alreadyFetchedConfiguration, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnConfigurationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _configurationDefinition</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncConfigurationDefinition(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _configurationDefinition, new PropertyChangedEventHandler( OnConfigurationDefinitionPropertyChanged ), "ConfigurationDefinition", VarioSL.Entities.RelationClasses.StaticFilterElementRelations.ConfigurationDefinitionEntityUsingValueListDataSourceIDStatic, true, signalRelatedEntity, "FilterElements", resetFKFields, new int[] { (int)FilterElementFieldIndex.ValueListDataSourceID } );		
			_configurationDefinition = null;
		}
		
		/// <summary> setups the sync logic for member _configurationDefinition</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncConfigurationDefinition(IEntityCore relatedEntity)
		{
			if(_configurationDefinition!=relatedEntity)
			{		
				DesetupSyncConfigurationDefinition(true, true);
				_configurationDefinition = (ConfigurationDefinitionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _configurationDefinition, new PropertyChangedEventHandler( OnConfigurationDefinitionPropertyChanged ), "ConfigurationDefinition", VarioSL.Entities.RelationClasses.StaticFilterElementRelations.ConfigurationDefinitionEntityUsingValueListDataSourceIDStatic, true, ref _alreadyFetchedConfigurationDefinition, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnConfigurationDefinitionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _filterType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFilterType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _filterType, new PropertyChangedEventHandler( OnFilterTypePropertyChanged ), "FilterType", VarioSL.Entities.RelationClasses.StaticFilterElementRelations.FilterTypeEntityUsingFilterTypeIDStatic, true, signalRelatedEntity, "FilterElements", resetFKFields, new int[] { (int)FilterElementFieldIndex.FilterTypeID } );		
			_filterType = null;
		}
		
		/// <summary> setups the sync logic for member _filterType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFilterType(IEntityCore relatedEntity)
		{
			if(_filterType!=relatedEntity)
			{		
				DesetupSyncFilterType(true, true);
				_filterType = (FilterTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _filterType, new PropertyChangedEventHandler( OnFilterTypePropertyChanged ), "FilterType", VarioSL.Entities.RelationClasses.StaticFilterElementRelations.FilterTypeEntityUsingFilterTypeIDStatic, true, ref _alreadyFetchedFilterType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFilterTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="filterElementID">PK value for FilterElement which data should be fetched into this FilterElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 filterElementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FilterElementFieldIndex.FilterElementID].ForcedCurrentValueWrite(filterElementID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFilterElementDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FilterElementEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FilterElementRelations Relations
		{
			get	{ return new FilterElementRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterListElement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterListElements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterListElementCollection(), (IEntityRelation)GetRelationsForField("FilterListElements")[0], (int)VarioSL.Entities.EntityType.FilterElementEntity, (int)VarioSL.Entities.EntityType.FilterListElementEntity, 0, null, null, null, "FilterListElements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Configuration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathConfiguration
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ConfigurationCollection(), (IEntityRelation)GetRelationsForField("Configuration")[0], (int)VarioSL.Entities.EntityType.FilterElementEntity, (int)VarioSL.Entities.EntityType.ConfigurationEntity, 0, null, null, null, "Configuration", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ConfigurationDefinition'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathConfigurationDefinition
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ConfigurationDefinitionCollection(), (IEntityRelation)GetRelationsForField("ConfigurationDefinition")[0], (int)VarioSL.Entities.EntityType.FilterElementEntity, (int)VarioSL.Entities.EntityType.ConfigurationDefinitionEntity, 0, null, null, null, "ConfigurationDefinition", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterTypeCollection(), (IEntityRelation)GetRelationsForField("FilterType")[0], (int)VarioSL.Entities.EntityType.FilterElementEntity, (int)VarioSL.Entities.EntityType.FilterTypeEntity, 0, null, null, null, "FilterType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CrystalReportName property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."CRYSTALREPORTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CrystalReportName
		{
			get { return (System.String)GetValue((int)FilterElementFieldIndex.CrystalReportName, true); }
			set	{ SetValue((int)FilterElementFieldIndex.CrystalReportName, value, true); }
		}

		/// <summary> The Description property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)FilterElementFieldIndex.Description, true); }
			set	{ SetValue((int)FilterElementFieldIndex.Description, value, true); }
		}

		/// <summary> The EditMask property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."EDITMASK"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EditMask
		{
			get { return (System.String)GetValue((int)FilterElementFieldIndex.EditMask, true); }
			set	{ SetValue((int)FilterElementFieldIndex.EditMask, value, true); }
		}

		/// <summary> The FilterElementID property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."FILTERELEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 FilterElementID
		{
			get { return (System.Int64)GetValue((int)FilterElementFieldIndex.FilterElementID, true); }
			set	{ SetValue((int)FilterElementFieldIndex.FilterElementID, value, true); }
		}

		/// <summary> The FilterTypeID property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."FILTERTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FilterTypeID
		{
			get { return (System.Int64)GetValue((int)FilterElementFieldIndex.FilterTypeID, true); }
			set	{ SetValue((int)FilterElementFieldIndex.FilterTypeID, value, true); }
		}

		/// <summary> The HelpText property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."HELPTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HelpText
		{
			get { return (System.String)GetValue((int)FilterElementFieldIndex.HelpText, true); }
			set	{ SetValue((int)FilterElementFieldIndex.HelpText, value, true); }
		}

		/// <summary> The LastModified property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)FilterElementFieldIndex.LastModified, true); }
			set	{ SetValue((int)FilterElementFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)FilterElementFieldIndex.LastUser, true); }
			set	{ SetValue((int)FilterElementFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MaxValue property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."MAXVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MaxValue
		{
			get { return (Nullable<System.Int32>)GetValue((int)FilterElementFieldIndex.MaxValue, false); }
			set	{ SetValue((int)FilterElementFieldIndex.MaxValue, value, true); }
		}

		/// <summary> The MinValue property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."MINVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MinValue
		{
			get { return (Nullable<System.Int32>)GetValue((int)FilterElementFieldIndex.MinValue, false); }
			set	{ SetValue((int)FilterElementFieldIndex.MinValue, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)FilterElementFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)FilterElementFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The UseFlexValues property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."USEFLEXVALUES"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseFlexValues
		{
			get { return (System.Boolean)GetValue((int)FilterElementFieldIndex.UseFlexValues, true); }
			set	{ SetValue((int)FilterElementFieldIndex.UseFlexValues, value, true); }
		}

		/// <summary> The UseMixedValues property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."USEMIXEDVALUES"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseMixedValues
		{
			get { return (System.Boolean)GetValue((int)FilterElementFieldIndex.UseMixedValues, true); }
			set	{ SetValue((int)FilterElementFieldIndex.UseMixedValues, value, true); }
		}

		/// <summary> The UseMultipleValues property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."USEMULTIPLEVALUES"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseMultipleValues
		{
			get { return (System.Boolean)GetValue((int)FilterElementFieldIndex.UseMultipleValues, true); }
			set	{ SetValue((int)FilterElementFieldIndex.UseMultipleValues, value, true); }
		}

		/// <summary> The UseRangeValues property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."USERANGEVALUES"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseRangeValues
		{
			get { return (System.Boolean)GetValue((int)FilterElementFieldIndex.UseRangeValues, true); }
			set	{ SetValue((int)FilterElementFieldIndex.UseRangeValues, value, true); }
		}

		/// <summary> The ValueListClientFilter property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."VALUELISTCLIENTFILTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.ClientFilterType ValueListClientFilter
		{
			get { return (VarioSL.Entities.Enumerations.ClientFilterType)GetValue((int)FilterElementFieldIndex.ValueListClientFilter, true); }
			set	{ SetValue((int)FilterElementFieldIndex.ValueListClientFilter, value, true); }
		}

		/// <summary> The ValueListColumn property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."VALUELISTCOLUMN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ValueListColumn
		{
			get { return (System.String)GetValue((int)FilterElementFieldIndex.ValueListColumn, true); }
			set	{ SetValue((int)FilterElementFieldIndex.ValueListColumn, value, true); }
		}

		/// <summary> The ValueListDataSourceID property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."VALUELISTDATASOURCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ValueListDataSourceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FilterElementFieldIndex.ValueListDataSourceID, false); }
			set	{ SetValue((int)FilterElementFieldIndex.ValueListDataSourceID, value, true); }
		}

		/// <summary> The ValueListShown property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."VALUELISTSHOWN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ValueListShown
		{
			get { return (System.String)GetValue((int)FilterElementFieldIndex.ValueListShown, true); }
			set	{ SetValue((int)FilterElementFieldIndex.ValueListShown, value, true); }
		}

		/// <summary> The ValueListTable property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."VALUELISTTABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ValueListTable
		{
			get { return (System.String)GetValue((int)FilterElementFieldIndex.ValueListTable, true); }
			set	{ SetValue((int)FilterElementFieldIndex.ValueListTable, value, true); }
		}

		/// <summary> The ValueListWhere property of the Entity FilterElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERELEMENT"."VALUELISTWHERE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ValueListWhere
		{
			get { return (System.String)GetValue((int)FilterElementFieldIndex.ValueListWhere, true); }
			set	{ SetValue((int)FilterElementFieldIndex.ValueListWhere, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'FilterListElementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFilterListElements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FilterListElementCollection FilterListElements
		{
			get	{ return GetMultiFilterListElements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FilterListElements. When set to true, FilterListElements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterListElements is accessed. You can always execute/ a forced fetch by calling GetMultiFilterListElements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterListElements
		{
			get	{ return _alwaysFetchFilterListElements; }
			set	{ _alwaysFetchFilterListElements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterListElements already has been fetched. Setting this property to false when FilterListElements has been fetched
		/// will clear the FilterListElements collection well. Setting this property to true while FilterListElements hasn't been fetched disables lazy loading for FilterListElements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterListElements
		{
			get { return _alreadyFetchedFilterListElements;}
			set 
			{
				if(_alreadyFetchedFilterListElements && !value && (_filterListElements != null))
				{
					_filterListElements.Clear();
				}
				_alreadyFetchedFilterListElements = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleConfiguration()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ConfigurationEntity Configuration
		{
			get	{ return GetSingleConfiguration(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncConfiguration(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FilterElements", "Configuration", _configuration, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Configuration. When set to true, Configuration is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Configuration is accessed. You can always execute a forced fetch by calling GetSingleConfiguration(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchConfiguration
		{
			get	{ return _alwaysFetchConfiguration; }
			set	{ _alwaysFetchConfiguration = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Configuration already has been fetched. Setting this property to false when Configuration has been fetched
		/// will set Configuration to null as well. Setting this property to true while Configuration hasn't been fetched disables lazy loading for Configuration</summary>
		[Browsable(false)]
		public bool AlreadyFetchedConfiguration
		{
			get { return _alreadyFetchedConfiguration;}
			set 
			{
				if(_alreadyFetchedConfiguration && !value)
				{
					this.Configuration = null;
				}
				_alreadyFetchedConfiguration = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Configuration is not found
		/// in the database. When set to true, Configuration will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ConfigurationReturnsNewIfNotFound
		{
			get	{ return _configurationReturnsNewIfNotFound; }
			set { _configurationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ConfigurationDefinitionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleConfigurationDefinition()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ConfigurationDefinitionEntity ConfigurationDefinition
		{
			get	{ return GetSingleConfigurationDefinition(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncConfigurationDefinition(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FilterElements", "ConfigurationDefinition", _configurationDefinition, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ConfigurationDefinition. When set to true, ConfigurationDefinition is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ConfigurationDefinition is accessed. You can always execute a forced fetch by calling GetSingleConfigurationDefinition(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchConfigurationDefinition
		{
			get	{ return _alwaysFetchConfigurationDefinition; }
			set	{ _alwaysFetchConfigurationDefinition = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ConfigurationDefinition already has been fetched. Setting this property to false when ConfigurationDefinition has been fetched
		/// will set ConfigurationDefinition to null as well. Setting this property to true while ConfigurationDefinition hasn't been fetched disables lazy loading for ConfigurationDefinition</summary>
		[Browsable(false)]
		public bool AlreadyFetchedConfigurationDefinition
		{
			get { return _alreadyFetchedConfigurationDefinition;}
			set 
			{
				if(_alreadyFetchedConfigurationDefinition && !value)
				{
					this.ConfigurationDefinition = null;
				}
				_alreadyFetchedConfigurationDefinition = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ConfigurationDefinition is not found
		/// in the database. When set to true, ConfigurationDefinition will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ConfigurationDefinitionReturnsNewIfNotFound
		{
			get	{ return _configurationDefinitionReturnsNewIfNotFound; }
			set { _configurationDefinitionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FilterTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFilterType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FilterTypeEntity FilterType
		{
			get	{ return GetSingleFilterType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFilterType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FilterElements", "FilterType", _filterType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FilterType. When set to true, FilterType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterType is accessed. You can always execute a forced fetch by calling GetSingleFilterType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterType
		{
			get	{ return _alwaysFetchFilterType; }
			set	{ _alwaysFetchFilterType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterType already has been fetched. Setting this property to false when FilterType has been fetched
		/// will set FilterType to null as well. Setting this property to true while FilterType hasn't been fetched disables lazy loading for FilterType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterType
		{
			get { return _alreadyFetchedFilterType;}
			set 
			{
				if(_alreadyFetchedFilterType && !value)
				{
					this.FilterType = null;
				}
				_alreadyFetchedFilterType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FilterType is not found
		/// in the database. When set to true, FilterType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FilterTypeReturnsNewIfNotFound
		{
			get	{ return _filterTypeReturnsNewIfNotFound; }
			set { _filterTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FilterElementEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
