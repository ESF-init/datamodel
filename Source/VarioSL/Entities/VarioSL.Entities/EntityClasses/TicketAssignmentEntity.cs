﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TicketAssignment'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TicketAssignmentEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private RuleTypeEntity _ruleType;
		private bool	_alwaysFetchRuleType, _alreadyFetchedRuleType, _ruleTypeReturnsNewIfNotFound;
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private EntitlementEntity _entitlement;
		private bool	_alwaysFetchEntitlement, _alreadyFetchedEntitlement, _entitlementReturnsNewIfNotFound;
		private OrganizationEntity _organization;
		private bool	_alwaysFetchOrganization, _alreadyFetchedOrganization, _organizationReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name RuleType</summary>
			public static readonly string RuleType = "RuleType";
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name Entitlement</summary>
			public static readonly string Entitlement = "Entitlement";
			/// <summary>Member name Organization</summary>
			public static readonly string Organization = "Organization";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TicketAssignmentEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TicketAssignmentEntity() :base("TicketAssignmentEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="ticketAssignmentId">PK value for TicketAssignment which data should be fetched into this TicketAssignment object</param>
		public TicketAssignmentEntity(System.Int64 ticketAssignmentId):base("TicketAssignmentEntity")
		{
			InitClassFetch(ticketAssignmentId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="ticketAssignmentId">PK value for TicketAssignment which data should be fetched into this TicketAssignment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TicketAssignmentEntity(System.Int64 ticketAssignmentId, IPrefetchPath prefetchPathToUse):base("TicketAssignmentEntity")
		{
			InitClassFetch(ticketAssignmentId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="ticketAssignmentId">PK value for TicketAssignment which data should be fetched into this TicketAssignment object</param>
		/// <param name="validator">The custom validator object for this TicketAssignmentEntity</param>
		public TicketAssignmentEntity(System.Int64 ticketAssignmentId, IValidator validator):base("TicketAssignmentEntity")
		{
			InitClassFetch(ticketAssignmentId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TicketAssignmentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_ruleType = (RuleTypeEntity)info.GetValue("_ruleType", typeof(RuleTypeEntity));
			if(_ruleType!=null)
			{
				_ruleType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ruleTypeReturnsNewIfNotFound = info.GetBoolean("_ruleTypeReturnsNewIfNotFound");
			_alwaysFetchRuleType = info.GetBoolean("_alwaysFetchRuleType");
			_alreadyFetchedRuleType = info.GetBoolean("_alreadyFetchedRuleType");

			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_entitlement = (EntitlementEntity)info.GetValue("_entitlement", typeof(EntitlementEntity));
			if(_entitlement!=null)
			{
				_entitlement.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entitlementReturnsNewIfNotFound = info.GetBoolean("_entitlementReturnsNewIfNotFound");
			_alwaysFetchEntitlement = info.GetBoolean("_alwaysFetchEntitlement");
			_alreadyFetchedEntitlement = info.GetBoolean("_alreadyFetchedEntitlement");

			_organization = (OrganizationEntity)info.GetValue("_organization", typeof(OrganizationEntity));
			if(_organization!=null)
			{
				_organization.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_organizationReturnsNewIfNotFound = info.GetBoolean("_organizationReturnsNewIfNotFound");
			_alwaysFetchOrganization = info.GetBoolean("_alwaysFetchOrganization");
			_alreadyFetchedOrganization = info.GetBoolean("_alreadyFetchedOrganization");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TicketAssignmentFieldIndex)fieldIndex)
			{
				case TicketAssignmentFieldIndex.CardID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				case TicketAssignmentFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case TicketAssignmentFieldIndex.EntitlementID:
					DesetupSyncEntitlement(true, false);
					_alreadyFetchedEntitlement = false;
					break;
				case TicketAssignmentFieldIndex.InstitutionID:
					DesetupSyncOrganization(true, false);
					_alreadyFetchedOrganization = false;
					break;
				case TicketAssignmentFieldIndex.PeriodicTypeID:
					DesetupSyncRuleType(true, false);
					_alreadyFetchedRuleType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRuleType = (_ruleType != null);
			_alreadyFetchedCard = (_card != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedEntitlement = (_entitlement != null);
			_alreadyFetchedOrganization = (_organization != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "RuleType":
					toReturn.Add(Relations.RuleTypeEntityUsingPeriodicTypeID);
					break;
				case "Card":
					toReturn.Add(Relations.CardEntityUsingCardID);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "Entitlement":
					toReturn.Add(Relations.EntitlementEntityUsingEntitlementID);
					break;
				case "Organization":
					toReturn.Add(Relations.OrganizationEntityUsingInstitutionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_ruleType", (!this.MarkedForDeletion?_ruleType:null));
			info.AddValue("_ruleTypeReturnsNewIfNotFound", _ruleTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRuleType", _alwaysFetchRuleType);
			info.AddValue("_alreadyFetchedRuleType", _alreadyFetchedRuleType);
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_entitlement", (!this.MarkedForDeletion?_entitlement:null));
			info.AddValue("_entitlementReturnsNewIfNotFound", _entitlementReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntitlement", _alwaysFetchEntitlement);
			info.AddValue("_alreadyFetchedEntitlement", _alreadyFetchedEntitlement);
			info.AddValue("_organization", (!this.MarkedForDeletion?_organization:null));
			info.AddValue("_organizationReturnsNewIfNotFound", _organizationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrganization", _alwaysFetchOrganization);
			info.AddValue("_alreadyFetchedOrganization", _alreadyFetchedOrganization);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "RuleType":
					_alreadyFetchedRuleType = true;
					this.RuleType = (RuleTypeEntity)entity;
					break;
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "Entitlement":
					_alreadyFetchedEntitlement = true;
					this.Entitlement = (EntitlementEntity)entity;
					break;
				case "Organization":
					_alreadyFetchedOrganization = true;
					this.Organization = (OrganizationEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "RuleType":
					SetupSyncRuleType(relatedEntity);
					break;
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "Entitlement":
					SetupSyncEntitlement(relatedEntity);
					break;
				case "Organization":
					SetupSyncOrganization(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "RuleType":
					DesetupSyncRuleType(false, true);
					break;
				case "Card":
					DesetupSyncCard(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "Entitlement":
					DesetupSyncEntitlement(false, true);
					break;
				case "Organization":
					DesetupSyncOrganization(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_ruleType!=null)
			{
				toReturn.Add(_ruleType);
			}
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_entitlement!=null)
			{
				toReturn.Add(_entitlement);
			}
			if(_organization!=null)
			{
				toReturn.Add(_organization);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketAssignmentId">PK value for TicketAssignment which data should be fetched into this TicketAssignment object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketAssignmentId)
		{
			return FetchUsingPK(ticketAssignmentId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketAssignmentId">PK value for TicketAssignment which data should be fetched into this TicketAssignment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketAssignmentId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(ticketAssignmentId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketAssignmentId">PK value for TicketAssignment which data should be fetched into this TicketAssignment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketAssignmentId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(ticketAssignmentId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketAssignmentId">PK value for TicketAssignment which data should be fetched into this TicketAssignment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketAssignmentId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(ticketAssignmentId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TicketAssignmentId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TicketAssignmentRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'RuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RuleTypeEntity' which is related to this entity.</returns>
		public RuleTypeEntity GetSingleRuleType()
		{
			return GetSingleRuleType(false);
		}

		/// <summary> Retrieves the related entity of type 'RuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RuleTypeEntity' which is related to this entity.</returns>
		public virtual RuleTypeEntity GetSingleRuleType(bool forceFetch)
		{
			if( ( !_alreadyFetchedRuleType || forceFetch || _alwaysFetchRuleType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RuleTypeEntityUsingPeriodicTypeID);
				RuleTypeEntity newEntity = new RuleTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PeriodicTypeID);
				}
				if(fetchResult)
				{
					newEntity = (RuleTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ruleTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RuleType = newEntity;
				_alreadyFetchedRuleType = fetchResult;
			}
			return _ruleType;
		}


		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardID);
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'EntitlementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntitlementEntity' which is related to this entity.</returns>
		public EntitlementEntity GetSingleEntitlement()
		{
			return GetSingleEntitlement(false);
		}

		/// <summary> Retrieves the related entity of type 'EntitlementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntitlementEntity' which is related to this entity.</returns>
		public virtual EntitlementEntity GetSingleEntitlement(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntitlement || forceFetch || _alwaysFetchEntitlement) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntitlementEntityUsingEntitlementID);
				EntitlementEntity newEntity = new EntitlementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntitlementID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntitlementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entitlementReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Entitlement = newEntity;
				_alreadyFetchedEntitlement = fetchResult;
			}
			return _entitlement;
		}


		/// <summary> Retrieves the related entity of type 'OrganizationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrganizationEntity' which is related to this entity.</returns>
		public OrganizationEntity GetSingleOrganization()
		{
			return GetSingleOrganization(false);
		}

		/// <summary> Retrieves the related entity of type 'OrganizationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrganizationEntity' which is related to this entity.</returns>
		public virtual OrganizationEntity GetSingleOrganization(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrganization || forceFetch || _alwaysFetchOrganization) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrganizationEntityUsingInstitutionID);
				OrganizationEntity newEntity = new OrganizationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InstitutionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OrganizationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_organizationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Organization = newEntity;
				_alreadyFetchedOrganization = fetchResult;
			}
			return _organization;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("RuleType", _ruleType);
			toReturn.Add("Card", _card);
			toReturn.Add("Contract", _contract);
			toReturn.Add("Entitlement", _entitlement);
			toReturn.Add("Organization", _organization);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="ticketAssignmentId">PK value for TicketAssignment which data should be fetched into this TicketAssignment object</param>
		/// <param name="validator">The validator object for this TicketAssignmentEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 ticketAssignmentId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(ticketAssignmentId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_ruleTypeReturnsNewIfNotFound = false;
			_cardReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_entitlementReturnsNewIfNotFound = false;
			_organizationReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AssignmentName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataRowCreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Destination", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntitlementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InstitutionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsAutoRenewEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastTransaction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Origin", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodicAllowance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodicTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Quantity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketAssignmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketInternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketsRemaining", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _ruleType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRuleType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ruleType, new PropertyChangedEventHandler( OnRuleTypePropertyChanged ), "RuleType", VarioSL.Entities.RelationClasses.StaticTicketAssignmentRelations.RuleTypeEntityUsingPeriodicTypeIDStatic, true, signalRelatedEntity, "TicketAssignments", resetFKFields, new int[] { (int)TicketAssignmentFieldIndex.PeriodicTypeID } );		
			_ruleType = null;
		}
		
		/// <summary> setups the sync logic for member _ruleType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRuleType(IEntityCore relatedEntity)
		{
			if(_ruleType!=relatedEntity)
			{		
				DesetupSyncRuleType(true, true);
				_ruleType = (RuleTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ruleType, new PropertyChangedEventHandler( OnRuleTypePropertyChanged ), "RuleType", VarioSL.Entities.RelationClasses.StaticTicketAssignmentRelations.RuleTypeEntityUsingPeriodicTypeIDStatic, true, ref _alreadyFetchedRuleType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRuleTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticTicketAssignmentRelations.CardEntityUsingCardIDStatic, true, signalRelatedEntity, "TicketAssignments", resetFKFields, new int[] { (int)TicketAssignmentFieldIndex.CardID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticTicketAssignmentRelations.CardEntityUsingCardIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticTicketAssignmentRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "TicketAssignments", resetFKFields, new int[] { (int)TicketAssignmentFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticTicketAssignmentRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entitlement</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntitlement(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entitlement, new PropertyChangedEventHandler( OnEntitlementPropertyChanged ), "Entitlement", VarioSL.Entities.RelationClasses.StaticTicketAssignmentRelations.EntitlementEntityUsingEntitlementIDStatic, true, signalRelatedEntity, "TicketAssignments", resetFKFields, new int[] { (int)TicketAssignmentFieldIndex.EntitlementID } );		
			_entitlement = null;
		}
		
		/// <summary> setups the sync logic for member _entitlement</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntitlement(IEntityCore relatedEntity)
		{
			if(_entitlement!=relatedEntity)
			{		
				DesetupSyncEntitlement(true, true);
				_entitlement = (EntitlementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entitlement, new PropertyChangedEventHandler( OnEntitlementPropertyChanged ), "Entitlement", VarioSL.Entities.RelationClasses.StaticTicketAssignmentRelations.EntitlementEntityUsingEntitlementIDStatic, true, ref _alreadyFetchedEntitlement, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntitlementPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _organization</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrganization(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _organization, new PropertyChangedEventHandler( OnOrganizationPropertyChanged ), "Organization", VarioSL.Entities.RelationClasses.StaticTicketAssignmentRelations.OrganizationEntityUsingInstitutionIDStatic, true, signalRelatedEntity, "TicketAssignments", resetFKFields, new int[] { (int)TicketAssignmentFieldIndex.InstitutionID } );		
			_organization = null;
		}
		
		/// <summary> setups the sync logic for member _organization</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrganization(IEntityCore relatedEntity)
		{
			if(_organization!=relatedEntity)
			{		
				DesetupSyncOrganization(true, true);
				_organization = (OrganizationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _organization, new PropertyChangedEventHandler( OnOrganizationPropertyChanged ), "Organization", VarioSL.Entities.RelationClasses.StaticTicketAssignmentRelations.OrganizationEntityUsingInstitutionIDStatic, true, ref _alreadyFetchedOrganization, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrganizationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="ticketAssignmentId">PK value for TicketAssignment which data should be fetched into this TicketAssignment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 ticketAssignmentId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TicketAssignmentFieldIndex.TicketAssignmentId].ForcedCurrentValueWrite(ticketAssignmentId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTicketAssignmentDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TicketAssignmentEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TicketAssignmentRelations Relations
		{
			get	{ return new TicketAssignmentRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleTypeCollection(), (IEntityRelation)GetRelationsForField("RuleType")[0], (int)VarioSL.Entities.EntityType.TicketAssignmentEntity, (int)VarioSL.Entities.EntityType.RuleTypeEntity, 0, null, null, null, "RuleType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.TicketAssignmentEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.TicketAssignmentEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entitlement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntitlement
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EntitlementCollection(), (IEntityRelation)GetRelationsForField("Entitlement")[0], (int)VarioSL.Entities.EntityType.TicketAssignmentEntity, (int)VarioSL.Entities.EntityType.EntitlementEntity, 0, null, null, null, "Entitlement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Organization'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganization
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationCollection(), (IEntityRelation)GetRelationsForField("Organization")[0], (int)VarioSL.Entities.EntityType.TicketAssignmentEntity, (int)VarioSL.Entities.EntityType.OrganizationEntity, 0, null, null, null, "Organization", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AssignmentName property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."ASSIGNMENTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AssignmentName
		{
			get { return (System.String)GetValue((int)TicketAssignmentFieldIndex.AssignmentName, true); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.AssignmentName, value, true); }
		}

		/// <summary> The CardID property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CardID
		{
			get { return (System.Int64)GetValue((int)TicketAssignmentFieldIndex.CardID, true); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.CardID, value, true); }
		}

		/// <summary> The ContractID property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketAssignmentFieldIndex.ContractID, false); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.ContractID, value, true); }
		}

		/// <summary> The DataRowCreationDate property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."DATAROWCREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataRowCreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TicketAssignmentFieldIndex.DataRowCreationDate, false); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.DataRowCreationDate, value, true); }
		}

		/// <summary> The Destination property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."DESTINATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Destination
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketAssignmentFieldIndex.Destination, false); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.Destination, value, true); }
		}

		/// <summary> The EntitlementID property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."ENTITLEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EntitlementID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketAssignmentFieldIndex.EntitlementID, false); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.EntitlementID, value, true); }
		}

		/// <summary> The InstitutionID property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."INSTITUTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> InstitutionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketAssignmentFieldIndex.InstitutionID, false); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.InstitutionID, value, true); }
		}

		/// <summary> The IsAutoRenewEnabled property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."ISAUTORENEWENABLED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAutoRenewEnabled
		{
			get { return (System.Boolean)GetValue((int)TicketAssignmentFieldIndex.IsAutoRenewEnabled, true); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.IsAutoRenewEnabled, value, true); }
		}

		/// <summary> The LastModified property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)TicketAssignmentFieldIndex.LastModified, true); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastTransaction property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."LASTTRANSACTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastTransaction
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TicketAssignmentFieldIndex.LastTransaction, false); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.LastTransaction, value, true); }
		}

		/// <summary> The LastUser property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)TicketAssignmentFieldIndex.LastUser, true); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Origin property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."ORIGIN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Origin
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketAssignmentFieldIndex.Origin, false); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.Origin, value, true); }
		}

		/// <summary> The PeriodicAllowance property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."PERIODICALLOWANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PeriodicAllowance
		{
			get { return (System.Decimal)GetValue((int)TicketAssignmentFieldIndex.PeriodicAllowance, true); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.PeriodicAllowance, value, true); }
		}

		/// <summary> The PeriodicTypeID property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."PERIODICTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PeriodicTypeID
		{
			get { return (System.Int64)GetValue((int)TicketAssignmentFieldIndex.PeriodicTypeID, true); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.PeriodicTypeID, value, true); }
		}

		/// <summary> The Quantity property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."QUANTITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Quantity
		{
			get { return (System.Int64)GetValue((int)TicketAssignmentFieldIndex.Quantity, true); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.Quantity, value, true); }
		}

		/// <summary> The Status property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."STATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TicketAssignmentStatus Status
		{
			get { return (VarioSL.Entities.Enumerations.TicketAssignmentStatus)GetValue((int)TicketAssignmentFieldIndex.Status, true); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.Status, value, true); }
		}

		/// <summary> The TicketAssignmentId property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."TICKETASSIGNMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 TicketAssignmentId
		{
			get { return (System.Int64)GetValue((int)TicketAssignmentFieldIndex.TicketAssignmentId, true); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.TicketAssignmentId, value, true); }
		}

		/// <summary> The TicketInternalNumber property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."TICKETINTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketInternalNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketAssignmentFieldIndex.TicketInternalNumber, false); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.TicketInternalNumber, value, true); }
		}

		/// <summary> The TicketsRemaining property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."TICKETSREMAINING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TicketsRemaining
		{
			get { return (System.Int64)GetValue((int)TicketAssignmentFieldIndex.TicketsRemaining, true); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.TicketsRemaining, value, true); }
		}

		/// <summary> The TicketTypeID property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."TICKETTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketAssignmentFieldIndex.TicketTypeID, false); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.TicketTypeID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity TicketAssignment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETASSIGNMENT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)TicketAssignmentFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)TicketAssignmentFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'RuleTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRuleType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RuleTypeEntity RuleType
		{
			get	{ return GetSingleRuleType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRuleType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketAssignments", "RuleType", _ruleType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RuleType. When set to true, RuleType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleType is accessed. You can always execute a forced fetch by calling GetSingleRuleType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleType
		{
			get	{ return _alwaysFetchRuleType; }
			set	{ _alwaysFetchRuleType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleType already has been fetched. Setting this property to false when RuleType has been fetched
		/// will set RuleType to null as well. Setting this property to true while RuleType hasn't been fetched disables lazy loading for RuleType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleType
		{
			get { return _alreadyFetchedRuleType;}
			set 
			{
				if(_alreadyFetchedRuleType && !value)
				{
					this.RuleType = null;
				}
				_alreadyFetchedRuleType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RuleType is not found
		/// in the database. When set to true, RuleType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RuleTypeReturnsNewIfNotFound
		{
			get	{ return _ruleTypeReturnsNewIfNotFound; }
			set { _ruleTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketAssignments", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketAssignments", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntitlementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntitlement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual EntitlementEntity Entitlement
		{
			get	{ return GetSingleEntitlement(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntitlement(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketAssignments", "Entitlement", _entitlement, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Entitlement. When set to true, Entitlement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Entitlement is accessed. You can always execute a forced fetch by calling GetSingleEntitlement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntitlement
		{
			get	{ return _alwaysFetchEntitlement; }
			set	{ _alwaysFetchEntitlement = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Entitlement already has been fetched. Setting this property to false when Entitlement has been fetched
		/// will set Entitlement to null as well. Setting this property to true while Entitlement hasn't been fetched disables lazy loading for Entitlement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntitlement
		{
			get { return _alreadyFetchedEntitlement;}
			set 
			{
				if(_alreadyFetchedEntitlement && !value)
				{
					this.Entitlement = null;
				}
				_alreadyFetchedEntitlement = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Entitlement is not found
		/// in the database. When set to true, Entitlement will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool EntitlementReturnsNewIfNotFound
		{
			get	{ return _entitlementReturnsNewIfNotFound; }
			set { _entitlementReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrganizationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrganization()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OrganizationEntity Organization
		{
			get	{ return GetSingleOrganization(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrganization(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketAssignments", "Organization", _organization, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Organization. When set to true, Organization is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Organization is accessed. You can always execute a forced fetch by calling GetSingleOrganization(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganization
		{
			get	{ return _alwaysFetchOrganization; }
			set	{ _alwaysFetchOrganization = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Organization already has been fetched. Setting this property to false when Organization has been fetched
		/// will set Organization to null as well. Setting this property to true while Organization hasn't been fetched disables lazy loading for Organization</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganization
		{
			get { return _alreadyFetchedOrganization;}
			set 
			{
				if(_alreadyFetchedOrganization && !value)
				{
					this.Organization = null;
				}
				_alreadyFetchedOrganization = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Organization is not found
		/// in the database. When set to true, Organization will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OrganizationReturnsNewIfNotFound
		{
			get	{ return _organizationReturnsNewIfNotFound; }
			set { _organizationReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TicketAssignmentEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
