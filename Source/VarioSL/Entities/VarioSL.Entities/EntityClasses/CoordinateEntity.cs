﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Coordinate'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CoordinateEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.StopCollection	_stops;
		private bool	_alwaysFetchStops, _alreadyFetchedStops;
		private VarioSL.Entities.CollectionClasses.AddressCollection	_addresses;
		private bool	_alwaysFetchAddresses, _alreadyFetchedAddresses;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Stops</summary>
			public static readonly string Stops = "Stops";
			/// <summary>Member name Addresses</summary>
			public static readonly string Addresses = "Addresses";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CoordinateEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CoordinateEntity() :base("CoordinateEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="coordinateID">PK value for Coordinate which data should be fetched into this Coordinate object</param>
		public CoordinateEntity(System.Int64 coordinateID):base("CoordinateEntity")
		{
			InitClassFetch(coordinateID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="coordinateID">PK value for Coordinate which data should be fetched into this Coordinate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CoordinateEntity(System.Int64 coordinateID, IPrefetchPath prefetchPathToUse):base("CoordinateEntity")
		{
			InitClassFetch(coordinateID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="coordinateID">PK value for Coordinate which data should be fetched into this Coordinate object</param>
		/// <param name="validator">The custom validator object for this CoordinateEntity</param>
		public CoordinateEntity(System.Int64 coordinateID, IValidator validator):base("CoordinateEntity")
		{
			InitClassFetch(coordinateID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CoordinateEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_stops = (VarioSL.Entities.CollectionClasses.StopCollection)info.GetValue("_stops", typeof(VarioSL.Entities.CollectionClasses.StopCollection));
			_alwaysFetchStops = info.GetBoolean("_alwaysFetchStops");
			_alreadyFetchedStops = info.GetBoolean("_alreadyFetchedStops");

			_addresses = (VarioSL.Entities.CollectionClasses.AddressCollection)info.GetValue("_addresses", typeof(VarioSL.Entities.CollectionClasses.AddressCollection));
			_alwaysFetchAddresses = info.GetBoolean("_alwaysFetchAddresses");
			_alreadyFetchedAddresses = info.GetBoolean("_alreadyFetchedAddresses");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedStops = (_stops.Count > 0);
			_alreadyFetchedAddresses = (_addresses.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Stops":
					toReturn.Add(Relations.StopEntityUsingCoordinateID);
					break;
				case "Addresses":
					toReturn.Add(Relations.AddressEntityUsingCoordinateID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_stops", (!this.MarkedForDeletion?_stops:null));
			info.AddValue("_alwaysFetchStops", _alwaysFetchStops);
			info.AddValue("_alreadyFetchedStops", _alreadyFetchedStops);
			info.AddValue("_addresses", (!this.MarkedForDeletion?_addresses:null));
			info.AddValue("_alwaysFetchAddresses", _alwaysFetchAddresses);
			info.AddValue("_alreadyFetchedAddresses", _alreadyFetchedAddresses);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Stops":
					_alreadyFetchedStops = true;
					if(entity!=null)
					{
						this.Stops.Add((StopEntity)entity);
					}
					break;
				case "Addresses":
					_alreadyFetchedAddresses = true;
					if(entity!=null)
					{
						this.Addresses.Add((AddressEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Stops":
					_stops.Add((StopEntity)relatedEntity);
					break;
				case "Addresses":
					_addresses.Add((AddressEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Stops":
					this.PerformRelatedEntityRemoval(_stops, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Addresses":
					this.PerformRelatedEntityRemoval(_addresses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_stops);
			toReturn.Add(_addresses);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="coordinateID">PK value for Coordinate which data should be fetched into this Coordinate object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 coordinateID)
		{
			return FetchUsingPK(coordinateID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="coordinateID">PK value for Coordinate which data should be fetched into this Coordinate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 coordinateID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(coordinateID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="coordinateID">PK value for Coordinate which data should be fetched into this Coordinate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 coordinateID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(coordinateID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="coordinateID">PK value for Coordinate which data should be fetched into this Coordinate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 coordinateID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(coordinateID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CoordinateID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CoordinateRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'StopEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'StopEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StopCollection GetMultiStops(bool forceFetch)
		{
			return GetMultiStops(forceFetch, _stops.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StopEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'StopEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StopCollection GetMultiStops(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiStops(forceFetch, _stops.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'StopEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.StopCollection GetMultiStops(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiStops(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StopEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.StopCollection GetMultiStops(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedStops || forceFetch || _alwaysFetchStops) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_stops);
				_stops.SuppressClearInGetMulti=!forceFetch;
				_stops.EntityFactoryToUse = entityFactoryToUse;
				_stops.GetMultiManyToOne(null, this, filter);
				_stops.SuppressClearInGetMulti=false;
				_alreadyFetchedStops = true;
			}
			return _stops;
		}

		/// <summary> Sets the collection parameters for the collection for 'Stops'. These settings will be taken into account
		/// when the property Stops is requested or GetMultiStops is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersStops(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_stops.SortClauses=sortClauses;
			_stops.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AddressCollection GetMultiAddresses(bool forceFetch)
		{
			return GetMultiAddresses(forceFetch, _addresses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AddressCollection GetMultiAddresses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAddresses(forceFetch, _addresses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AddressCollection GetMultiAddresses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAddresses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AddressCollection GetMultiAddresses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAddresses || forceFetch || _alwaysFetchAddresses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_addresses);
				_addresses.SuppressClearInGetMulti=!forceFetch;
				_addresses.EntityFactoryToUse = entityFactoryToUse;
				_addresses.GetMultiManyToOne(this, filter);
				_addresses.SuppressClearInGetMulti=false;
				_alreadyFetchedAddresses = true;
			}
			return _addresses;
		}

		/// <summary> Sets the collection parameters for the collection for 'Addresses'. These settings will be taken into account
		/// when the property Addresses is requested or GetMultiAddresses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAddresses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_addresses.SortClauses=sortClauses;
			_addresses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Stops", _stops);
			toReturn.Add("Addresses", _addresses);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="coordinateID">PK value for Coordinate which data should be fetched into this Coordinate object</param>
		/// <param name="validator">The validator object for this CoordinateEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 coordinateID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(coordinateID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_stops = new VarioSL.Entities.CollectionClasses.StopCollection();
			_stops.SetContainingEntityInfo(this, "Coordinate");

			_addresses = new VarioSL.Entities.CollectionClasses.AddressCollection();
			_addresses.SetContainingEntityInfo(this, "Coordinate");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CoordinateID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Easting", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Northing", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ZoneName", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="coordinateID">PK value for Coordinate which data should be fetched into this Coordinate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 coordinateID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CoordinateFieldIndex.CoordinateID].ForcedCurrentValueWrite(coordinateID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCoordinateDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CoordinateEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CoordinateRelations Relations
		{
			get	{ return new CoordinateRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Stop' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathStops
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StopCollection(), (IEntityRelation)GetRelationsForField("Stops")[0], (int)VarioSL.Entities.EntityType.CoordinateEntity, (int)VarioSL.Entities.EntityType.StopEntity, 0, null, null, null, "Stops", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Address' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAddresses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressCollection(), (IEntityRelation)GetRelationsForField("Addresses")[0], (int)VarioSL.Entities.EntityType.CoordinateEntity, (int)VarioSL.Entities.EntityType.AddressEntity, 0, null, null, null, "Addresses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CoordinateID property of the Entity Coordinate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_COORDINATE"."COORDINATEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CoordinateID
		{
			get { return (System.Int64)GetValue((int)CoordinateFieldIndex.CoordinateID, true); }
			set	{ SetValue((int)CoordinateFieldIndex.CoordinateID, value, true); }
		}

		/// <summary> The Easting property of the Entity Coordinate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_COORDINATE"."EASTING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 20, 10, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Easting
		{
			get { return (System.Decimal)GetValue((int)CoordinateFieldIndex.Easting, true); }
			set	{ SetValue((int)CoordinateFieldIndex.Easting, value, true); }
		}

		/// <summary> The LastModified property of the Entity Coordinate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_COORDINATE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CoordinateFieldIndex.LastModified, true); }
			set	{ SetValue((int)CoordinateFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Coordinate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_COORDINATE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CoordinateFieldIndex.LastUser, true); }
			set	{ SetValue((int)CoordinateFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Northing property of the Entity Coordinate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_COORDINATE"."NORTHING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 20, 10, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Northing
		{
			get { return (System.Decimal)GetValue((int)CoordinateFieldIndex.Northing, true); }
			set	{ SetValue((int)CoordinateFieldIndex.Northing, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Coordinate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_COORDINATE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CoordinateFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CoordinateFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ZoneName property of the Entity Coordinate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_COORDINATE"."ZONENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 5<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ZoneName
		{
			get { return (System.String)GetValue((int)CoordinateFieldIndex.ZoneName, true); }
			set	{ SetValue((int)CoordinateFieldIndex.ZoneName, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'StopEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiStops()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.StopCollection Stops
		{
			get	{ return GetMultiStops(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Stops. When set to true, Stops is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Stops is accessed. You can always execute/ a forced fetch by calling GetMultiStops(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchStops
		{
			get	{ return _alwaysFetchStops; }
			set	{ _alwaysFetchStops = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Stops already has been fetched. Setting this property to false when Stops has been fetched
		/// will clear the Stops collection well. Setting this property to true while Stops hasn't been fetched disables lazy loading for Stops</summary>
		[Browsable(false)]
		public bool AlreadyFetchedStops
		{
			get { return _alreadyFetchedStops;}
			set 
			{
				if(_alreadyFetchedStops && !value && (_stops != null))
				{
					_stops.Clear();
				}
				_alreadyFetchedStops = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AddressEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAddresses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AddressCollection Addresses
		{
			get	{ return GetMultiAddresses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Addresses. When set to true, Addresses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Addresses is accessed. You can always execute/ a forced fetch by calling GetMultiAddresses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAddresses
		{
			get	{ return _alwaysFetchAddresses; }
			set	{ _alwaysFetchAddresses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Addresses already has been fetched. Setting this property to false when Addresses has been fetched
		/// will clear the Addresses collection well. Setting this property to true while Addresses hasn't been fetched disables lazy loading for Addresses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAddresses
		{
			get { return _alreadyFetchedAddresses;}
			set 
			{
				if(_alreadyFetchedAddresses && !value && (_addresses != null))
				{
					_addresses.Clear();
				}
				_alreadyFetchedAddresses = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CoordinateEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
