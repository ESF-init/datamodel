﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AccountingCancelRecTapTabSettled'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AccountingCancelRecTapTabSettledEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AccountingCancelRecTapTabSettledEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AccountingCancelRecTapTabSettledEntity() :base("AccountingCancelRecTapTabSettledEntity")
		{
			InitClassEmpty(null);
		}
		


		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AccountingCancelRecTapTabSettledEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}




				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AccountingCancelRecTapTabSettledRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		



		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BaseFare", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationFeferenceGuid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloseoutType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CostCenterKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerGroup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebitAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IncludeInSettlement", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Line", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Premium", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurseBalance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurseCredit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResultType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevenueRecognitionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevenueSettlementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesChannelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketInternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionOperatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransitAccountID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripTicketInternalNumber", fieldHashtable);
		}
		#endregion

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. </summary>
		/// <returns>true</returns>
		public override bool Refetch()
		{
			return true;
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAccountingCancelRecTapTabSettledDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AccountingCancelRecTapTabSettledEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AccountingCancelRecTapTabSettledRelations Relations
		{
			get	{ return new AccountingCancelRecTapTabSettledRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."AMOUNT"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Amount
		{
			get { return (System.Int64)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.Amount, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.Amount, value, true); }
		}

		/// <summary> The BaseFare property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."BASEFARE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BaseFare
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.BaseFare, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.BaseFare, value, true); }
		}

		/// <summary> The CancellationFeferenceGuid property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."CANCELLATIONREFERENCEGUID"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CancellationFeferenceGuid
		{
			get { return (System.String)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CancellationFeferenceGuid, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CancellationFeferenceGuid, value, true); }
		}

		/// <summary> The CancellationReference property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."CANCELLATIONREFERENCE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationReference
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CancellationReference, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CancellationReference, value, true); }
		}

		/// <summary> The ClientID property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."CLIENTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.ClientID, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CloseoutPeriodID property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."CLOSEOUTPERIODID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CloseoutPeriodID
		{
			get { return (System.Int64)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CloseoutPeriodID, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CloseoutPeriodID, value, true); }
		}

		/// <summary> The CloseoutType property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."CLOSEOUTTYPE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CloseoutType
		{
			get { return (System.Int32)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CloseoutType, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CloseoutType, value, true); }
		}

		/// <summary> The CostCenterKey property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."COSTCENTERKEY"<br/>
		/// View field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CostCenterKey
		{
			get { return (System.String)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CostCenterKey, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CostCenterKey, value, true); }
		}

		/// <summary> The CreditAccountNumber property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."CREDITACCOUNTNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CreditAccountNumber
		{
			get { return (System.String)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CreditAccountNumber, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CreditAccountNumber, value, true); }
		}

		/// <summary> The CustomerGroup property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."CUSTOMERGROUP"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CustomerGroup
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CustomerGroup, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.CustomerGroup, value, true); }
		}

		/// <summary> The DebitAccountNumber property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."DEBITACCOUNTNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DebitAccountNumber
		{
			get { return (System.String)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.DebitAccountNumber, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.DebitAccountNumber, value, true); }
		}

		/// <summary> The DeviceTime property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."DEVICETIME"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DeviceTime
		{
			get { return (System.DateTime)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.DeviceTime, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.DeviceTime, value, true); }
		}

		/// <summary> The FareAmount property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."FAREAMOUNT"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FareAmount
		{
			get { return (System.Int32)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.FareAmount, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.FareAmount, value, true); }
		}

		/// <summary> The IncludeInSettlement property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."INCLUDEINSETTLEMENT"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IncludeInSettlement
		{
			get { return (Nullable<System.Boolean>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.IncludeInSettlement, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.IncludeInSettlement, value, true); }
		}

		/// <summary> The LastModified property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."LASTMODIFIED"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.LastModified, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.LastModified, value, true); }
		}

		/// <summary> The Line property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."LINE"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Line
		{
			get { return (System.String)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.Line, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.Line, value, true); }
		}

		/// <summary> The LineGroupID property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."LINEGROUPID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.LineGroupID, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.LineGroupID, value, true); }
		}

		/// <summary> The OperatorID property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."OPERATORID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OperatorID
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.OperatorID, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.OperatorID, value, true); }
		}

		/// <summary> The PeriodFrom property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."PERIODFROM"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PeriodFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.PeriodFrom, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.PeriodFrom, value, true); }
		}

		/// <summary> The PeriodTo property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."PERIODTO"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PeriodTo
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.PeriodTo, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.PeriodTo, value, true); }
		}

		/// <summary> The PostingDate property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."POSTINGDATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PostingDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.PostingDate, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.PostingDate, value, true); }
		}

		/// <summary> The PostingReference property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."POSTINGREFERENCE"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostingReference
		{
			get { return (System.String)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.PostingReference, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.PostingReference, value, true); }
		}

		/// <summary> The Premium property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."PREMIUM"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Premium
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.Premium, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.Premium, value, true); }
		}

		/// <summary> The ProductID property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."PRODUCTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ProductID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.ProductID, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.ProductID, value, true); }
		}

		/// <summary> The PurseBalance property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."PURSEBALANCE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PurseBalance
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.PurseBalance, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.PurseBalance, value, true); }
		}

		/// <summary> The PurseCredit property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."PURSECREDIT"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PurseCredit
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.PurseCredit, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.PurseCredit, value, true); }
		}

		/// <summary> The ResultType property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."RESULTTYPE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ResultType
		{
			get { return (System.Int32)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.ResultType, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.ResultType, value, true); }
		}

		/// <summary> The RevenueRecognitionID property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."REVENUERECOGNITIONID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 RevenueRecognitionID
		{
			get { return (System.Int64)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.RevenueRecognitionID, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.RevenueRecognitionID, value, true); }
		}

		/// <summary> The RevenueSettlementID property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."REVENUESETTLEMENTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RevenueSettlementID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.RevenueSettlementID, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.RevenueSettlementID, value, true); }
		}

		/// <summary> The SalesChannelID property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."SALESCHANNELID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SalesChannelID
		{
			get { return (System.Int64)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.SalesChannelID, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.SalesChannelID, value, true); }
		}

		/// <summary> The SettlementType property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."SETTLEMENTTYPE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SettlementType
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.SettlementType, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.SettlementType, value, true); }
		}

		/// <summary> The State property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."STATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 State
		{
			get { return (System.Int32)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.State, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.State, value, true); }
		}

		/// <summary> The TariffDate property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."TARIFFDATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TariffDate
		{
			get { return (System.DateTime)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TariffDate, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TariffDate, value, true); }
		}

		/// <summary> The TariffVersion property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."TARIFFVERSION"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TariffVersion
		{
			get { return (System.Int32)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TariffVersion, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TariffVersion, value, true); }
		}

		/// <summary> The TicketID property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."TICKETID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TicketID, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TicketID, value, true); }
		}

		/// <summary> The TicketInternalNumber property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."TICKETINTERNALNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TicketInternalNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TicketInternalNumber, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TicketInternalNumber, value, true); }
		}

		/// <summary> The TicketNumber property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."TICKETNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TicketNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TicketNumber, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TicketNumber, value, true); }
		}

		/// <summary> The TransactionJournalID property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."TRANSACTIONJOURNALID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TransactionJournalID
		{
			get { return (System.Int64)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TransactionJournalID, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TransactionJournalID, value, true); }
		}

		/// <summary> The TransactionOperatorID property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."TRANSACTIONOPERATORID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionOperatorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TransactionOperatorID, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TransactionOperatorID, value, true); }
		}

		/// <summary> The TransactionType property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."TRANSACTIONTYPE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TransactionType
		{
			get { return (System.Int32)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TransactionType, true); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TransactionType, value, true); }
		}

		/// <summary> The TransitAccountID property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."TRANSITACCOUNTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransitAccountID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TransitAccountID, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TransitAccountID, value, true); }
		}

		/// <summary> The TripTicketInternalNumber property of the Entity AccountingCancelRecTapTabSettled<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_CANCEL_REC_TAP_TAB_SETTLED"."TRIPTICKETINTERNALNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TripTicketInternalNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TripTicketInternalNumber, false); }
			set	{ SetValue((int)AccountingCancelRecTapTabSettledFieldIndex.TripTicketInternalNumber, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AccountingCancelRecTapTabSettledEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
