﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CardActionAttribute'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CardActionAttributeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CardActionRequestEntity _cardActionRequest;
		private bool	_alwaysFetchCardActionRequest, _alreadyFetchedCardActionRequest, _cardActionRequestReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CardActionRequest</summary>
			public static readonly string CardActionRequest = "CardActionRequest";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CardActionAttributeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CardActionAttributeEntity() :base("CardActionAttributeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cardActionAttributeID">PK value for CardActionAttribute which data should be fetched into this CardActionAttribute object</param>
		public CardActionAttributeEntity(System.Int64 cardActionAttributeID):base("CardActionAttributeEntity")
		{
			InitClassFetch(cardActionAttributeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cardActionAttributeID">PK value for CardActionAttribute which data should be fetched into this CardActionAttribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CardActionAttributeEntity(System.Int64 cardActionAttributeID, IPrefetchPath prefetchPathToUse):base("CardActionAttributeEntity")
		{
			InitClassFetch(cardActionAttributeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cardActionAttributeID">PK value for CardActionAttribute which data should be fetched into this CardActionAttribute object</param>
		/// <param name="validator">The custom validator object for this CardActionAttributeEntity</param>
		public CardActionAttributeEntity(System.Int64 cardActionAttributeID, IValidator validator):base("CardActionAttributeEntity")
		{
			InitClassFetch(cardActionAttributeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CardActionAttributeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cardActionRequest = (CardActionRequestEntity)info.GetValue("_cardActionRequest", typeof(CardActionRequestEntity));
			if(_cardActionRequest!=null)
			{
				_cardActionRequest.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardActionRequestReturnsNewIfNotFound = info.GetBoolean("_cardActionRequestReturnsNewIfNotFound");
			_alwaysFetchCardActionRequest = info.GetBoolean("_alwaysFetchCardActionRequest");
			_alreadyFetchedCardActionRequest = info.GetBoolean("_alreadyFetchedCardActionRequest");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CardActionAttributeFieldIndex)fieldIndex)
			{
				case CardActionAttributeFieldIndex.CardActionRequestID:
					DesetupSyncCardActionRequest(true, false);
					_alreadyFetchedCardActionRequest = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCardActionRequest = (_cardActionRequest != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CardActionRequest":
					toReturn.Add(Relations.CardActionRequestEntityUsingCardActionRequestID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cardActionRequest", (!this.MarkedForDeletion?_cardActionRequest:null));
			info.AddValue("_cardActionRequestReturnsNewIfNotFound", _cardActionRequestReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardActionRequest", _alwaysFetchCardActionRequest);
			info.AddValue("_alreadyFetchedCardActionRequest", _alreadyFetchedCardActionRequest);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CardActionRequest":
					_alreadyFetchedCardActionRequest = true;
					this.CardActionRequest = (CardActionRequestEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CardActionRequest":
					SetupSyncCardActionRequest(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CardActionRequest":
					DesetupSyncCardActionRequest(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cardActionRequest!=null)
			{
				toReturn.Add(_cardActionRequest);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardActionAttributeID">PK value for CardActionAttribute which data should be fetched into this CardActionAttribute object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardActionAttributeID)
		{
			return FetchUsingPK(cardActionAttributeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardActionAttributeID">PK value for CardActionAttribute which data should be fetched into this CardActionAttribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardActionAttributeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cardActionAttributeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardActionAttributeID">PK value for CardActionAttribute which data should be fetched into this CardActionAttribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardActionAttributeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cardActionAttributeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardActionAttributeID">PK value for CardActionAttribute which data should be fetched into this CardActionAttribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardActionAttributeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cardActionAttributeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CardActionAttributeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CardActionAttributeRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CardActionRequestEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardActionRequestEntity' which is related to this entity.</returns>
		public CardActionRequestEntity GetSingleCardActionRequest()
		{
			return GetSingleCardActionRequest(false);
		}

		/// <summary> Retrieves the related entity of type 'CardActionRequestEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardActionRequestEntity' which is related to this entity.</returns>
		public virtual CardActionRequestEntity GetSingleCardActionRequest(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardActionRequest || forceFetch || _alwaysFetchCardActionRequest) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardActionRequestEntityUsingCardActionRequestID);
				CardActionRequestEntity newEntity = new CardActionRequestEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardActionRequestID);
				}
				if(fetchResult)
				{
					newEntity = (CardActionRequestEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardActionRequestReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardActionRequest = newEntity;
				_alreadyFetchedCardActionRequest = fetchResult;
			}
			return _cardActionRequest;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CardActionRequest", _cardActionRequest);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cardActionAttributeID">PK value for CardActionAttribute which data should be fetched into this CardActionAttribute object</param>
		/// <param name="validator">The validator object for this CardActionAttributeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cardActionAttributeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cardActionAttributeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_cardActionRequestReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardActionAttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardActionRequestID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DateOfBirth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Deposit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExpiryDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GroupExpiry", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IssueFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IssuerID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintedCardNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Registration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RemoveBirthday", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VoiceFlag", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _cardActionRequest</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardActionRequest(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardActionRequest, new PropertyChangedEventHandler( OnCardActionRequestPropertyChanged ), "CardActionRequest", VarioSL.Entities.RelationClasses.StaticCardActionAttributeRelations.CardActionRequestEntityUsingCardActionRequestIDStatic, true, signalRelatedEntity, "CardActionAttributes", resetFKFields, new int[] { (int)CardActionAttributeFieldIndex.CardActionRequestID } );		
			_cardActionRequest = null;
		}
		
		/// <summary> setups the sync logic for member _cardActionRequest</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardActionRequest(IEntityCore relatedEntity)
		{
			if(_cardActionRequest!=relatedEntity)
			{		
				DesetupSyncCardActionRequest(true, true);
				_cardActionRequest = (CardActionRequestEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardActionRequest, new PropertyChangedEventHandler( OnCardActionRequestPropertyChanged ), "CardActionRequest", VarioSL.Entities.RelationClasses.StaticCardActionAttributeRelations.CardActionRequestEntityUsingCardActionRequestIDStatic, true, ref _alreadyFetchedCardActionRequest, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardActionRequestPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cardActionAttributeID">PK value for CardActionAttribute which data should be fetched into this CardActionAttribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cardActionAttributeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CardActionAttributeFieldIndex.CardActionAttributeID].ForcedCurrentValueWrite(cardActionAttributeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCardActionAttributeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CardActionAttributeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CardActionAttributeRelations Relations
		{
			get	{ return new CardActionAttributeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardActionRequest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardActionRequest
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardActionRequestCollection(), (IEntityRelation)GetRelationsForField("CardActionRequest")[0], (int)VarioSL.Entities.EntityType.CardActionAttributeEntity, (int)VarioSL.Entities.EntityType.CardActionRequestEntity, 0, null, null, null, "CardActionRequest", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CardActionAttributeID property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CardActionAttributeID
		{
			get { return (System.Int64)GetValue((int)CardActionAttributeFieldIndex.CardActionAttributeID, true); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.CardActionAttributeID, value, true); }
		}

		/// <summary> The CardActionRequestID property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."CARDACTIONREQUESTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CardActionRequestID
		{
			get { return (System.Int64)GetValue((int)CardActionAttributeFieldIndex.CardActionRequestID, true); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.CardActionRequestID, value, true); }
		}

		/// <summary> The DateOfBirth property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."DATEOFBIRTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DateOfBirth
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CardActionAttributeFieldIndex.DateOfBirth, false); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.DateOfBirth, value, true); }
		}

		/// <summary> The Deposit property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."DEPOSIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Deposit
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardActionAttributeFieldIndex.Deposit, false); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.Deposit, value, true); }
		}

		/// <summary> The ExpiryDate property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."EXPIRY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ExpiryDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CardActionAttributeFieldIndex.ExpiryDate, false); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.ExpiryDate, value, true); }
		}

		/// <summary> The GroupExpiry property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."GROUPEXPIRY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> GroupExpiry
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CardActionAttributeFieldIndex.GroupExpiry, false); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.GroupExpiry, value, true); }
		}

		/// <summary> The IssueFlag property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."ISSUEFLAG"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IssueFlag
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CardActionAttributeFieldIndex.IssueFlag, false); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.IssueFlag, value, true); }
		}

		/// <summary> The IssuerID property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."ISSUERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> IssuerID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardActionAttributeFieldIndex.IssuerID, false); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.IssuerID, value, true); }
		}

		/// <summary> The PrintedCardNo property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."PRINTEDCARDNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrintedCardNo
		{
			get { return (System.String)GetValue((int)CardActionAttributeFieldIndex.PrintedCardNo, true); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.PrintedCardNo, value, true); }
		}

		/// <summary> The Registration property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."REGISTRATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Registration
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardActionAttributeFieldIndex.Registration, false); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.Registration, value, true); }
		}

		/// <summary> The RemoveBirthday property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."REMOVEBIRTHDAY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> RemoveBirthday
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CardActionAttributeFieldIndex.RemoveBirthday, false); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.RemoveBirthday, value, true); }
		}

		/// <summary> The UserGroupID property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."USERGROUP"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UserGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardActionAttributeFieldIndex.UserGroupID, false); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.UserGroupID, value, true); }
		}

		/// <summary> The VoiceFlag property of the Entity CardActionAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONATTRIBUTE"."VOICEFLAG"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> VoiceFlag
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CardActionAttributeFieldIndex.VoiceFlag, false); }
			set	{ SetValue((int)CardActionAttributeFieldIndex.VoiceFlag, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CardActionRequestEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardActionRequest()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardActionRequestEntity CardActionRequest
		{
			get	{ return GetSingleCardActionRequest(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCardActionRequest(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CardActionAttributes", "CardActionRequest", _cardActionRequest, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardActionRequest. When set to true, CardActionRequest is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardActionRequest is accessed. You can always execute a forced fetch by calling GetSingleCardActionRequest(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardActionRequest
		{
			get	{ return _alwaysFetchCardActionRequest; }
			set	{ _alwaysFetchCardActionRequest = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardActionRequest already has been fetched. Setting this property to false when CardActionRequest has been fetched
		/// will set CardActionRequest to null as well. Setting this property to true while CardActionRequest hasn't been fetched disables lazy loading for CardActionRequest</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardActionRequest
		{
			get { return _alreadyFetchedCardActionRequest;}
			set 
			{
				if(_alreadyFetchedCardActionRequest && !value)
				{
					this.CardActionRequest = null;
				}
				_alreadyFetchedCardActionRequest = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardActionRequest is not found
		/// in the database. When set to true, CardActionRequest will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardActionRequestReturnsNewIfNotFound
		{
			get	{ return _cardActionRequestReturnsNewIfNotFound; }
			set { _cardActionRequestReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CardActionAttributeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
