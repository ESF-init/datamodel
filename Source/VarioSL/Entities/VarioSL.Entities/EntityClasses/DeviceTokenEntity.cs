﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DeviceToken'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DeviceTokenEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection	_customerAccountNotifications;
		private bool	_alwaysFetchCustomerAccountNotifications, _alreadyFetchedCustomerAccountNotifications;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CustomerAccountNotifications</summary>
			public static readonly string CustomerAccountNotifications = "CustomerAccountNotifications";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeviceTokenEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DeviceTokenEntity() :base("DeviceTokenEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="deviceTokenID">PK value for DeviceToken which data should be fetched into this DeviceToken object</param>
		public DeviceTokenEntity(System.Int64 deviceTokenID):base("DeviceTokenEntity")
		{
			InitClassFetch(deviceTokenID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceTokenID">PK value for DeviceToken which data should be fetched into this DeviceToken object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeviceTokenEntity(System.Int64 deviceTokenID, IPrefetchPath prefetchPathToUse):base("DeviceTokenEntity")
		{
			InitClassFetch(deviceTokenID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceTokenID">PK value for DeviceToken which data should be fetched into this DeviceToken object</param>
		/// <param name="validator">The custom validator object for this DeviceTokenEntity</param>
		public DeviceTokenEntity(System.Int64 deviceTokenID, IValidator validator):base("DeviceTokenEntity")
		{
			InitClassFetch(deviceTokenID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeviceTokenEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customerAccountNotifications = (VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection)info.GetValue("_customerAccountNotifications", typeof(VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection));
			_alwaysFetchCustomerAccountNotifications = info.GetBoolean("_alwaysFetchCustomerAccountNotifications");
			_alreadyFetchedCustomerAccountNotifications = info.GetBoolean("_alreadyFetchedCustomerAccountNotifications");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomerAccountNotifications = (_customerAccountNotifications.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CustomerAccountNotifications":
					toReturn.Add(Relations.CustomerAccountNotificationEntityUsingDeviceTokenID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customerAccountNotifications", (!this.MarkedForDeletion?_customerAccountNotifications:null));
			info.AddValue("_alwaysFetchCustomerAccountNotifications", _alwaysFetchCustomerAccountNotifications);
			info.AddValue("_alreadyFetchedCustomerAccountNotifications", _alreadyFetchedCustomerAccountNotifications);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CustomerAccountNotifications":
					_alreadyFetchedCustomerAccountNotifications = true;
					if(entity!=null)
					{
						this.CustomerAccountNotifications.Add((CustomerAccountNotificationEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CustomerAccountNotifications":
					_customerAccountNotifications.Add((CustomerAccountNotificationEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CustomerAccountNotifications":
					this.PerformRelatedEntityRemoval(_customerAccountNotifications, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customerAccountNotifications);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceTokenID">PK value for DeviceToken which data should be fetched into this DeviceToken object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceTokenID)
		{
			return FetchUsingPK(deviceTokenID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceTokenID">PK value for DeviceToken which data should be fetched into this DeviceToken object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceTokenID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deviceTokenID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceTokenID">PK value for DeviceToken which data should be fetched into this DeviceToken object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceTokenID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deviceTokenID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceTokenID">PK value for DeviceToken which data should be fetched into this DeviceToken object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceTokenID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deviceTokenID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeviceTokenID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeviceTokenRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerAccountNotificationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection GetMultiCustomerAccountNotifications(bool forceFetch)
		{
			return GetMultiCustomerAccountNotifications(forceFetch, _customerAccountNotifications.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomerAccountNotificationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection GetMultiCustomerAccountNotifications(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomerAccountNotifications(forceFetch, _customerAccountNotifications.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection GetMultiCustomerAccountNotifications(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomerAccountNotifications(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection GetMultiCustomerAccountNotifications(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomerAccountNotifications || forceFetch || _alwaysFetchCustomerAccountNotifications) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerAccountNotifications);
				_customerAccountNotifications.SuppressClearInGetMulti=!forceFetch;
				_customerAccountNotifications.EntityFactoryToUse = entityFactoryToUse;
				_customerAccountNotifications.GetMultiManyToOne(null, this, null, filter);
				_customerAccountNotifications.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerAccountNotifications = true;
			}
			return _customerAccountNotifications;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerAccountNotifications'. These settings will be taken into account
		/// when the property CustomerAccountNotifications is requested or GetMultiCustomerAccountNotifications is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerAccountNotifications(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerAccountNotifications.SortClauses=sortClauses;
			_customerAccountNotifications.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CustomerAccountNotifications", _customerAccountNotifications);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deviceTokenID">PK value for DeviceToken which data should be fetched into this DeviceToken object</param>
		/// <param name="validator">The validator object for this DeviceTokenEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 deviceTokenID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deviceTokenID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customerAccountNotifications = new VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection();
			_customerAccountNotifications.SetContainingEntityInfo(this, "DeviceToken");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceToken", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceTokenID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceTokenState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExpirationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deviceTokenID">PK value for DeviceToken which data should be fetched into this DeviceToken object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 deviceTokenID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeviceTokenFieldIndex.DeviceTokenID].ForcedCurrentValueWrite(deviceTokenID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeviceTokenDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeviceTokenEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeviceTokenRelations Relations
		{
			get	{ return new DeviceTokenRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomerAccountNotification' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerAccountNotifications
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection(), (IEntityRelation)GetRelationsForField("CustomerAccountNotifications")[0], (int)VarioSL.Entities.EntityType.DeviceTokenEntity, (int)VarioSL.Entities.EntityType.CustomerAccountNotificationEntity, 0, null, null, null, "CustomerAccountNotifications", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DeviceToken property of the Entity DeviceToken<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICETOKEN"."DEVICETOKEN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DeviceToken
		{
			get { return (System.String)GetValue((int)DeviceTokenFieldIndex.DeviceToken, true); }
			set	{ SetValue((int)DeviceTokenFieldIndex.DeviceToken, value, true); }
		}

		/// <summary> The DeviceTokenID property of the Entity DeviceToken<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICETOKEN"."DEVICETOKENID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 DeviceTokenID
		{
			get { return (System.Int64)GetValue((int)DeviceTokenFieldIndex.DeviceTokenID, true); }
			set	{ SetValue((int)DeviceTokenFieldIndex.DeviceTokenID, value, true); }
		}

		/// <summary> The DeviceTokenState property of the Entity DeviceToken<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICETOKEN"."DEVICETOKENSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.DeviceTokenState DeviceTokenState
		{
			get { return (VarioSL.Entities.Enumerations.DeviceTokenState)GetValue((int)DeviceTokenFieldIndex.DeviceTokenState, true); }
			set	{ SetValue((int)DeviceTokenFieldIndex.DeviceTokenState, value, true); }
		}

		/// <summary> The ExpirationDate property of the Entity DeviceToken<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICETOKEN"."EXPIRATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ExpirationDate
		{
			get { return (System.DateTime)GetValue((int)DeviceTokenFieldIndex.ExpirationDate, true); }
			set	{ SetValue((int)DeviceTokenFieldIndex.ExpirationDate, value, true); }
		}

		/// <summary> The LastModified property of the Entity DeviceToken<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICETOKEN"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)DeviceTokenFieldIndex.LastModified, true); }
			set	{ SetValue((int)DeviceTokenFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity DeviceToken<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICETOKEN"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)DeviceTokenFieldIndex.LastUser, true); }
			set	{ SetValue((int)DeviceTokenFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity DeviceToken<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICETOKEN"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)DeviceTokenFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)DeviceTokenFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerAccountNotifications()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection CustomerAccountNotifications
		{
			get	{ return GetMultiCustomerAccountNotifications(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerAccountNotifications. When set to true, CustomerAccountNotifications is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerAccountNotifications is accessed. You can always execute/ a forced fetch by calling GetMultiCustomerAccountNotifications(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerAccountNotifications
		{
			get	{ return _alwaysFetchCustomerAccountNotifications; }
			set	{ _alwaysFetchCustomerAccountNotifications = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerAccountNotifications already has been fetched. Setting this property to false when CustomerAccountNotifications has been fetched
		/// will clear the CustomerAccountNotifications collection well. Setting this property to true while CustomerAccountNotifications hasn't been fetched disables lazy loading for CustomerAccountNotifications</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerAccountNotifications
		{
			get { return _alreadyFetchedCustomerAccountNotifications;}
			set 
			{
				if(_alreadyFetchedCustomerAccountNotifications && !value && (_customerAccountNotifications != null))
				{
					_customerAccountNotifications.Clear();
				}
				_alreadyFetchedCustomerAccountNotifications = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DeviceTokenEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
