﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Job'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class JobEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.JobCardImportCollection	_jobCardImports;
		private bool	_alwaysFetchJobCardImports, _alreadyFetchedJobCardImports;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private InvoicingEntity _invoicing;
		private bool	_alwaysFetchInvoicing, _alreadyFetchedInvoicing, _invoicingReturnsNewIfNotFound;
		private JobBulkImportEntity _jobBulkImport;
		private bool	_alwaysFetchJobBulkImport, _alreadyFetchedJobBulkImport, _jobBulkImportReturnsNewIfNotFound;
		private MessageEntity _message;
		private bool	_alwaysFetchMessage, _alreadyFetchedMessage, _messageReturnsNewIfNotFound;
		private OrderDetailEntity _orderDetail;
		private bool	_alwaysFetchOrderDetail, _alreadyFetchedOrderDetail, _orderDetailReturnsNewIfNotFound;
		private PrinterEntity _printer;
		private bool	_alwaysFetchPrinter, _alreadyFetchedPrinter, _printerReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name Invoicing</summary>
			public static readonly string Invoicing = "Invoicing";
			/// <summary>Member name JobBulkImport</summary>
			public static readonly string JobBulkImport = "JobBulkImport";
			/// <summary>Member name Message</summary>
			public static readonly string Message = "Message";
			/// <summary>Member name OrderDetail</summary>
			public static readonly string OrderDetail = "OrderDetail";
			/// <summary>Member name Printer</summary>
			public static readonly string Printer = "Printer";
			/// <summary>Member name JobCardImports</summary>
			public static readonly string JobCardImports = "JobCardImports";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static JobEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public JobEntity() :base("JobEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="jobID">PK value for Job which data should be fetched into this Job object</param>
		public JobEntity(System.Int64 jobID):base("JobEntity")
		{
			InitClassFetch(jobID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="jobID">PK value for Job which data should be fetched into this Job object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public JobEntity(System.Int64 jobID, IPrefetchPath prefetchPathToUse):base("JobEntity")
		{
			InitClassFetch(jobID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="jobID">PK value for Job which data should be fetched into this Job object</param>
		/// <param name="validator">The custom validator object for this JobEntity</param>
		public JobEntity(System.Int64 jobID, IValidator validator):base("JobEntity")
		{
			InitClassFetch(jobID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected JobEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_jobCardImports = (VarioSL.Entities.CollectionClasses.JobCardImportCollection)info.GetValue("_jobCardImports", typeof(VarioSL.Entities.CollectionClasses.JobCardImportCollection));
			_alwaysFetchJobCardImports = info.GetBoolean("_alwaysFetchJobCardImports");
			_alreadyFetchedJobCardImports = info.GetBoolean("_alreadyFetchedJobCardImports");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_invoicing = (InvoicingEntity)info.GetValue("_invoicing", typeof(InvoicingEntity));
			if(_invoicing!=null)
			{
				_invoicing.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_invoicingReturnsNewIfNotFound = info.GetBoolean("_invoicingReturnsNewIfNotFound");
			_alwaysFetchInvoicing = info.GetBoolean("_alwaysFetchInvoicing");
			_alreadyFetchedInvoicing = info.GetBoolean("_alreadyFetchedInvoicing");

			_jobBulkImport = (JobBulkImportEntity)info.GetValue("_jobBulkImport", typeof(JobBulkImportEntity));
			if(_jobBulkImport!=null)
			{
				_jobBulkImport.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_jobBulkImportReturnsNewIfNotFound = info.GetBoolean("_jobBulkImportReturnsNewIfNotFound");
			_alwaysFetchJobBulkImport = info.GetBoolean("_alwaysFetchJobBulkImport");
			_alreadyFetchedJobBulkImport = info.GetBoolean("_alreadyFetchedJobBulkImport");

			_message = (MessageEntity)info.GetValue("_message", typeof(MessageEntity));
			if(_message!=null)
			{
				_message.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_messageReturnsNewIfNotFound = info.GetBoolean("_messageReturnsNewIfNotFound");
			_alwaysFetchMessage = info.GetBoolean("_alwaysFetchMessage");
			_alreadyFetchedMessage = info.GetBoolean("_alreadyFetchedMessage");

			_orderDetail = (OrderDetailEntity)info.GetValue("_orderDetail", typeof(OrderDetailEntity));
			if(_orderDetail!=null)
			{
				_orderDetail.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderDetailReturnsNewIfNotFound = info.GetBoolean("_orderDetailReturnsNewIfNotFound");
			_alwaysFetchOrderDetail = info.GetBoolean("_alwaysFetchOrderDetail");
			_alreadyFetchedOrderDetail = info.GetBoolean("_alreadyFetchedOrderDetail");

			_printer = (PrinterEntity)info.GetValue("_printer", typeof(PrinterEntity));
			if(_printer!=null)
			{
				_printer.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_printerReturnsNewIfNotFound = info.GetBoolean("_printerReturnsNewIfNotFound");
			_alwaysFetchPrinter = info.GetBoolean("_alwaysFetchPrinter");
			_alreadyFetchedPrinter = info.GetBoolean("_alreadyFetchedPrinter");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((JobFieldIndex)fieldIndex)
			{
				case JobFieldIndex.BulkImportID:
					DesetupSyncJobBulkImport(true, false);
					_alreadyFetchedJobBulkImport = false;
					break;
				case JobFieldIndex.CardID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				case JobFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case JobFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case JobFieldIndex.InvoicingID:
					DesetupSyncInvoicing(true, false);
					_alreadyFetchedInvoicing = false;
					break;
				case JobFieldIndex.MessageID:
					DesetupSyncMessage(true, false);
					_alreadyFetchedMessage = false;
					break;
				case JobFieldIndex.OrderDetailID:
					DesetupSyncOrderDetail(true, false);
					_alreadyFetchedOrderDetail = false;
					break;
				case JobFieldIndex.PrinterID:
					DesetupSyncPrinter(true, false);
					_alreadyFetchedPrinter = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedJobCardImports = (_jobCardImports.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedCard = (_card != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedInvoicing = (_invoicing != null);
			_alreadyFetchedJobBulkImport = (_jobBulkImport != null);
			_alreadyFetchedMessage = (_message != null);
			_alreadyFetchedOrderDetail = (_orderDetail != null);
			_alreadyFetchedPrinter = (_printer != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "Card":
					toReturn.Add(Relations.CardEntityUsingCardID);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "Invoicing":
					toReturn.Add(Relations.InvoicingEntityUsingInvoicingID);
					break;
				case "JobBulkImport":
					toReturn.Add(Relations.JobBulkImportEntityUsingBulkImportID);
					break;
				case "Message":
					toReturn.Add(Relations.MessageEntityUsingMessageID);
					break;
				case "OrderDetail":
					toReturn.Add(Relations.OrderDetailEntityUsingOrderDetailID);
					break;
				case "Printer":
					toReturn.Add(Relations.PrinterEntityUsingPrinterID);
					break;
				case "JobCardImports":
					toReturn.Add(Relations.JobCardImportEntityUsingJobID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_jobCardImports", (!this.MarkedForDeletion?_jobCardImports:null));
			info.AddValue("_alwaysFetchJobCardImports", _alwaysFetchJobCardImports);
			info.AddValue("_alreadyFetchedJobCardImports", _alreadyFetchedJobCardImports);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_invoicing", (!this.MarkedForDeletion?_invoicing:null));
			info.AddValue("_invoicingReturnsNewIfNotFound", _invoicingReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInvoicing", _alwaysFetchInvoicing);
			info.AddValue("_alreadyFetchedInvoicing", _alreadyFetchedInvoicing);
			info.AddValue("_jobBulkImport", (!this.MarkedForDeletion?_jobBulkImport:null));
			info.AddValue("_jobBulkImportReturnsNewIfNotFound", _jobBulkImportReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchJobBulkImport", _alwaysFetchJobBulkImport);
			info.AddValue("_alreadyFetchedJobBulkImport", _alreadyFetchedJobBulkImport);
			info.AddValue("_message", (!this.MarkedForDeletion?_message:null));
			info.AddValue("_messageReturnsNewIfNotFound", _messageReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMessage", _alwaysFetchMessage);
			info.AddValue("_alreadyFetchedMessage", _alreadyFetchedMessage);
			info.AddValue("_orderDetail", (!this.MarkedForDeletion?_orderDetail:null));
			info.AddValue("_orderDetailReturnsNewIfNotFound", _orderDetailReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrderDetail", _alwaysFetchOrderDetail);
			info.AddValue("_alreadyFetchedOrderDetail", _alreadyFetchedOrderDetail);
			info.AddValue("_printer", (!this.MarkedForDeletion?_printer:null));
			info.AddValue("_printerReturnsNewIfNotFound", _printerReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPrinter", _alwaysFetchPrinter);
			info.AddValue("_alreadyFetchedPrinter", _alreadyFetchedPrinter);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "Invoicing":
					_alreadyFetchedInvoicing = true;
					this.Invoicing = (InvoicingEntity)entity;
					break;
				case "JobBulkImport":
					_alreadyFetchedJobBulkImport = true;
					this.JobBulkImport = (JobBulkImportEntity)entity;
					break;
				case "Message":
					_alreadyFetchedMessage = true;
					this.Message = (MessageEntity)entity;
					break;
				case "OrderDetail":
					_alreadyFetchedOrderDetail = true;
					this.OrderDetail = (OrderDetailEntity)entity;
					break;
				case "Printer":
					_alreadyFetchedPrinter = true;
					this.Printer = (PrinterEntity)entity;
					break;
				case "JobCardImports":
					_alreadyFetchedJobCardImports = true;
					if(entity!=null)
					{
						this.JobCardImports.Add((JobCardImportEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "Invoicing":
					SetupSyncInvoicing(relatedEntity);
					break;
				case "JobBulkImport":
					SetupSyncJobBulkImport(relatedEntity);
					break;
				case "Message":
					SetupSyncMessage(relatedEntity);
					break;
				case "OrderDetail":
					SetupSyncOrderDetail(relatedEntity);
					break;
				case "Printer":
					SetupSyncPrinter(relatedEntity);
					break;
				case "JobCardImports":
					_jobCardImports.Add((JobCardImportEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "Card":
					DesetupSyncCard(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "Invoicing":
					DesetupSyncInvoicing(false, true);
					break;
				case "JobBulkImport":
					DesetupSyncJobBulkImport(false, true);
					break;
				case "Message":
					DesetupSyncMessage(false, true);
					break;
				case "OrderDetail":
					DesetupSyncOrderDetail(false, true);
					break;
				case "Printer":
					DesetupSyncPrinter(false, true);
					break;
				case "JobCardImports":
					this.PerformRelatedEntityRemoval(_jobCardImports, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_invoicing!=null)
			{
				toReturn.Add(_invoicing);
			}
			if(_jobBulkImport!=null)
			{
				toReturn.Add(_jobBulkImport);
			}
			if(_message!=null)
			{
				toReturn.Add(_message);
			}
			if(_orderDetail!=null)
			{
				toReturn.Add(_orderDetail);
			}
			if(_printer!=null)
			{
				toReturn.Add(_printer);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_jobCardImports);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="jobID">PK value for Job which data should be fetched into this Job object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 jobID)
		{
			return FetchUsingPK(jobID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="jobID">PK value for Job which data should be fetched into this Job object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 jobID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(jobID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="jobID">PK value for Job which data should be fetched into this Job object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 jobID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(jobID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="jobID">PK value for Job which data should be fetched into this Job object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 jobID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(jobID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.JobID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new JobRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'JobCardImportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCardImportCollection GetMultiJobCardImports(bool forceFetch)
		{
			return GetMultiJobCardImports(forceFetch, _jobCardImports.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'JobCardImportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCardImportCollection GetMultiJobCardImports(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiJobCardImports(forceFetch, _jobCardImports.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.JobCardImportCollection GetMultiJobCardImports(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiJobCardImports(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.JobCardImportCollection GetMultiJobCardImports(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedJobCardImports || forceFetch || _alwaysFetchJobCardImports) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_jobCardImports);
				_jobCardImports.SuppressClearInGetMulti=!forceFetch;
				_jobCardImports.EntityFactoryToUse = entityFactoryToUse;
				_jobCardImports.GetMultiManyToOne(null, this, null, filter);
				_jobCardImports.SuppressClearInGetMulti=false;
				_alreadyFetchedJobCardImports = true;
			}
			return _jobCardImports;
		}

		/// <summary> Sets the collection parameters for the collection for 'JobCardImports'. These settings will be taken into account
		/// when the property JobCardImports is requested or GetMultiJobCardImports is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersJobCardImports(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_jobCardImports.SortClauses=sortClauses;
			_jobCardImports.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'InvoicingEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'InvoicingEntity' which is related to this entity.</returns>
		public InvoicingEntity GetSingleInvoicing()
		{
			return GetSingleInvoicing(false);
		}

		/// <summary> Retrieves the related entity of type 'InvoicingEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InvoicingEntity' which is related to this entity.</returns>
		public virtual InvoicingEntity GetSingleInvoicing(bool forceFetch)
		{
			if( ( !_alreadyFetchedInvoicing || forceFetch || _alwaysFetchInvoicing) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InvoicingEntityUsingInvoicingID);
				InvoicingEntity newEntity = new InvoicingEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InvoicingID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (InvoicingEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_invoicingReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Invoicing = newEntity;
				_alreadyFetchedInvoicing = fetchResult;
			}
			return _invoicing;
		}


		/// <summary> Retrieves the related entity of type 'JobBulkImportEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'JobBulkImportEntity' which is related to this entity.</returns>
		public JobBulkImportEntity GetSingleJobBulkImport()
		{
			return GetSingleJobBulkImport(false);
		}

		/// <summary> Retrieves the related entity of type 'JobBulkImportEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'JobBulkImportEntity' which is related to this entity.</returns>
		public virtual JobBulkImportEntity GetSingleJobBulkImport(bool forceFetch)
		{
			if( ( !_alreadyFetchedJobBulkImport || forceFetch || _alwaysFetchJobBulkImport) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.JobBulkImportEntityUsingBulkImportID);
				JobBulkImportEntity newEntity = new JobBulkImportEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BulkImportID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (JobBulkImportEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_jobBulkImportReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.JobBulkImport = newEntity;
				_alreadyFetchedJobBulkImport = fetchResult;
			}
			return _jobBulkImport;
		}


		/// <summary> Retrieves the related entity of type 'MessageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MessageEntity' which is related to this entity.</returns>
		public MessageEntity GetSingleMessage()
		{
			return GetSingleMessage(false);
		}

		/// <summary> Retrieves the related entity of type 'MessageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MessageEntity' which is related to this entity.</returns>
		public virtual MessageEntity GetSingleMessage(bool forceFetch)
		{
			if( ( !_alreadyFetchedMessage || forceFetch || _alwaysFetchMessage) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MessageEntityUsingMessageID);
				MessageEntity newEntity = new MessageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MessageID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MessageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_messageReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Message = newEntity;
				_alreadyFetchedMessage = fetchResult;
			}
			return _message;
		}


		/// <summary> Retrieves the related entity of type 'OrderDetailEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderDetailEntity' which is related to this entity.</returns>
		public OrderDetailEntity GetSingleOrderDetail()
		{
			return GetSingleOrderDetail(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderDetailEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderDetailEntity' which is related to this entity.</returns>
		public virtual OrderDetailEntity GetSingleOrderDetail(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrderDetail || forceFetch || _alwaysFetchOrderDetail) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderDetailEntityUsingOrderDetailID);
				OrderDetailEntity newEntity = new OrderDetailEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderDetailID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OrderDetailEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderDetailReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrderDetail = newEntity;
				_alreadyFetchedOrderDetail = fetchResult;
			}
			return _orderDetail;
		}


		/// <summary> Retrieves the related entity of type 'PrinterEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PrinterEntity' which is related to this entity.</returns>
		public PrinterEntity GetSinglePrinter()
		{
			return GetSinglePrinter(false);
		}

		/// <summary> Retrieves the related entity of type 'PrinterEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PrinterEntity' which is related to this entity.</returns>
		public virtual PrinterEntity GetSinglePrinter(bool forceFetch)
		{
			if( ( !_alreadyFetchedPrinter || forceFetch || _alwaysFetchPrinter) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PrinterEntityUsingPrinterID);
				PrinterEntity newEntity = new PrinterEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PrinterID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PrinterEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_printerReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Printer = newEntity;
				_alreadyFetchedPrinter = fetchResult;
			}
			return _printer;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("Card", _card);
			toReturn.Add("Contract", _contract);
			toReturn.Add("Invoicing", _invoicing);
			toReturn.Add("JobBulkImport", _jobBulkImport);
			toReturn.Add("Message", _message);
			toReturn.Add("OrderDetail", _orderDetail);
			toReturn.Add("Printer", _printer);
			toReturn.Add("JobCardImports", _jobCardImports);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="jobID">PK value for Job which data should be fetched into this Job object</param>
		/// <param name="validator">The validator object for this JobEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 jobID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(jobID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_jobCardImports = new VarioSL.Entities.CollectionClasses.JobCardImportCollection();
			_jobCardImports.SetContainingEntityInfo(this, "Job");
			_clientReturnsNewIfNotFound = false;
			_cardReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_invoicingReturnsNewIfNotFound = false;
			_jobBulkImportReturnsNewIfNotFound = false;
			_messageReturnsNewIfNotFound = false;
			_orderDetailReturnsNewIfNotFound = false;
			_printerReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BulkImportID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Data", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExceptionMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HasInputFile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HasOutputFile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoicingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderDetailID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutputMessages", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrinterID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticJobRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "Jobs", resetFKFields, new int[] { (int)JobFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticJobRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticJobRelations.CardEntityUsingCardIDStatic, true, signalRelatedEntity, "Jobs", resetFKFields, new int[] { (int)JobFieldIndex.CardID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticJobRelations.CardEntityUsingCardIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticJobRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "Jobs", resetFKFields, new int[] { (int)JobFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticJobRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _invoicing</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInvoicing(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _invoicing, new PropertyChangedEventHandler( OnInvoicingPropertyChanged ), "Invoicing", VarioSL.Entities.RelationClasses.StaticJobRelations.InvoicingEntityUsingInvoicingIDStatic, true, signalRelatedEntity, "Jobs", resetFKFields, new int[] { (int)JobFieldIndex.InvoicingID } );		
			_invoicing = null;
		}
		
		/// <summary> setups the sync logic for member _invoicing</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInvoicing(IEntityCore relatedEntity)
		{
			if(_invoicing!=relatedEntity)
			{		
				DesetupSyncInvoicing(true, true);
				_invoicing = (InvoicingEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _invoicing, new PropertyChangedEventHandler( OnInvoicingPropertyChanged ), "Invoicing", VarioSL.Entities.RelationClasses.StaticJobRelations.InvoicingEntityUsingInvoicingIDStatic, true, ref _alreadyFetchedInvoicing, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInvoicingPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _jobBulkImport</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncJobBulkImport(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _jobBulkImport, new PropertyChangedEventHandler( OnJobBulkImportPropertyChanged ), "JobBulkImport", VarioSL.Entities.RelationClasses.StaticJobRelations.JobBulkImportEntityUsingBulkImportIDStatic, true, signalRelatedEntity, "Jobs", resetFKFields, new int[] { (int)JobFieldIndex.BulkImportID } );		
			_jobBulkImport = null;
		}
		
		/// <summary> setups the sync logic for member _jobBulkImport</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncJobBulkImport(IEntityCore relatedEntity)
		{
			if(_jobBulkImport!=relatedEntity)
			{		
				DesetupSyncJobBulkImport(true, true);
				_jobBulkImport = (JobBulkImportEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _jobBulkImport, new PropertyChangedEventHandler( OnJobBulkImportPropertyChanged ), "JobBulkImport", VarioSL.Entities.RelationClasses.StaticJobRelations.JobBulkImportEntityUsingBulkImportIDStatic, true, ref _alreadyFetchedJobBulkImport, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnJobBulkImportPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _message</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMessage(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _message, new PropertyChangedEventHandler( OnMessagePropertyChanged ), "Message", VarioSL.Entities.RelationClasses.StaticJobRelations.MessageEntityUsingMessageIDStatic, true, signalRelatedEntity, "Jobs", resetFKFields, new int[] { (int)JobFieldIndex.MessageID } );		
			_message = null;
		}
		
		/// <summary> setups the sync logic for member _message</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMessage(IEntityCore relatedEntity)
		{
			if(_message!=relatedEntity)
			{		
				DesetupSyncMessage(true, true);
				_message = (MessageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _message, new PropertyChangedEventHandler( OnMessagePropertyChanged ), "Message", VarioSL.Entities.RelationClasses.StaticJobRelations.MessageEntityUsingMessageIDStatic, true, ref _alreadyFetchedMessage, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMessagePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _orderDetail</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrderDetail(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _orderDetail, new PropertyChangedEventHandler( OnOrderDetailPropertyChanged ), "OrderDetail", VarioSL.Entities.RelationClasses.StaticJobRelations.OrderDetailEntityUsingOrderDetailIDStatic, true, signalRelatedEntity, "Jobs", resetFKFields, new int[] { (int)JobFieldIndex.OrderDetailID } );		
			_orderDetail = null;
		}
		
		/// <summary> setups the sync logic for member _orderDetail</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrderDetail(IEntityCore relatedEntity)
		{
			if(_orderDetail!=relatedEntity)
			{		
				DesetupSyncOrderDetail(true, true);
				_orderDetail = (OrderDetailEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _orderDetail, new PropertyChangedEventHandler( OnOrderDetailPropertyChanged ), "OrderDetail", VarioSL.Entities.RelationClasses.StaticJobRelations.OrderDetailEntityUsingOrderDetailIDStatic, true, ref _alreadyFetchedOrderDetail, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderDetailPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _printer</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPrinter(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _printer, new PropertyChangedEventHandler( OnPrinterPropertyChanged ), "Printer", VarioSL.Entities.RelationClasses.StaticJobRelations.PrinterEntityUsingPrinterIDStatic, true, signalRelatedEntity, "Jobs", resetFKFields, new int[] { (int)JobFieldIndex.PrinterID } );		
			_printer = null;
		}
		
		/// <summary> setups the sync logic for member _printer</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPrinter(IEntityCore relatedEntity)
		{
			if(_printer!=relatedEntity)
			{		
				DesetupSyncPrinter(true, true);
				_printer = (PrinterEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _printer, new PropertyChangedEventHandler( OnPrinterPropertyChanged ), "Printer", VarioSL.Entities.RelationClasses.StaticJobRelations.PrinterEntityUsingPrinterIDStatic, true, ref _alreadyFetchedPrinter, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPrinterPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="jobID">PK value for Job which data should be fetched into this Job object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 jobID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)JobFieldIndex.JobID].ForcedCurrentValueWrite(jobID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateJobDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new JobEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static JobRelations Relations
		{
			get	{ return new JobRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'JobCardImport' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathJobCardImports
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.JobCardImportCollection(), (IEntityRelation)GetRelationsForField("JobCardImports")[0], (int)VarioSL.Entities.EntityType.JobEntity, (int)VarioSL.Entities.EntityType.JobCardImportEntity, 0, null, null, null, "JobCardImports", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.JobEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.JobEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.JobEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoicing'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoicing
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoicingCollection(), (IEntityRelation)GetRelationsForField("Invoicing")[0], (int)VarioSL.Entities.EntityType.JobEntity, (int)VarioSL.Entities.EntityType.InvoicingEntity, 0, null, null, null, "Invoicing", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'JobBulkImport'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathJobBulkImport
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.JobBulkImportCollection(), (IEntityRelation)GetRelationsForField("JobBulkImport")[0], (int)VarioSL.Entities.EntityType.JobEntity, (int)VarioSL.Entities.EntityType.JobBulkImportEntity, 0, null, null, null, "JobBulkImport", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Message'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessage
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.MessageCollection(), (IEntityRelation)GetRelationsForField("Message")[0], (int)VarioSL.Entities.EntityType.JobEntity, (int)VarioSL.Entities.EntityType.MessageEntity, 0, null, null, null, "Message", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetail'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderDetail
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailCollection(), (IEntityRelation)GetRelationsForField("OrderDetail")[0], (int)VarioSL.Entities.EntityType.JobEntity, (int)VarioSL.Entities.EntityType.OrderDetailEntity, 0, null, null, null, "OrderDetail", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Printer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPrinter
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PrinterCollection(), (IEntityRelation)GetRelationsForField("Printer")[0], (int)VarioSL.Entities.EntityType.JobEntity, (int)VarioSL.Entities.EntityType.PrinterEntity, 0, null, null, null, "Printer", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BulkImportID property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."BULKIMPORTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BulkImportID
		{
			get { return (Nullable<System.Int64>)GetValue((int)JobFieldIndex.BulkImportID, false); }
			set	{ SetValue((int)JobFieldIndex.BulkImportID, value, true); }
		}

		/// <summary> The CardID property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)JobFieldIndex.CardID, false); }
			set	{ SetValue((int)JobFieldIndex.CardID, value, true); }
		}

		/// <summary> The ClientID property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)JobFieldIndex.ClientID, false); }
			set	{ SetValue((int)JobFieldIndex.ClientID, value, true); }
		}

		/// <summary> The ContractID property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractID
		{
			get { return (Nullable<System.Int64>)GetValue((int)JobFieldIndex.ContractID, false); }
			set	{ SetValue((int)JobFieldIndex.ContractID, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."CREATEDBY"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CreatedBy
		{
			get { return (System.String)GetValue((int)JobFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)JobFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The CreateTime property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."CREATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreateTime
		{
			get { return (System.DateTime)GetValue((int)JobFieldIndex.CreateTime, true); }
			set	{ SetValue((int)JobFieldIndex.CreateTime, value, true); }
		}

		/// <summary> The Data property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."DATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): NClob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Data
		{
			get { return (System.String)GetValue((int)JobFieldIndex.Data, true); }
			set	{ SetValue((int)JobFieldIndex.Data, value, true); }
		}

		/// <summary> The EndTime property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."ENDTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)JobFieldIndex.EndTime, false); }
			set	{ SetValue((int)JobFieldIndex.EndTime, value, true); }
		}

		/// <summary> The ExceptionMessage property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."EXCEPTIONMESSAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NClob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExceptionMessage
		{
			get { return (System.String)GetValue((int)JobFieldIndex.ExceptionMessage, true); }
			set	{ SetValue((int)JobFieldIndex.ExceptionMessage, value, true); }
		}

		/// <summary> The HasInputFile property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."HASINPUTFILE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HasInputFile
		{
			get { return (System.Boolean)GetValue((int)JobFieldIndex.HasInputFile, true); }
			set	{ SetValue((int)JobFieldIndex.HasInputFile, value, true); }
		}

		/// <summary> The HasOutputFile property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."HASOUTPUTFILE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HasOutputFile
		{
			get { return (System.Boolean)GetValue((int)JobFieldIndex.HasOutputFile, true); }
			set	{ SetValue((int)JobFieldIndex.HasOutputFile, value, true); }
		}

		/// <summary> The InvoicingID property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."INVOICINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> InvoicingID
		{
			get { return (Nullable<System.Int64>)GetValue((int)JobFieldIndex.InvoicingID, false); }
			set	{ SetValue((int)JobFieldIndex.InvoicingID, value, true); }
		}

		/// <summary> The JobID property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."JOBID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 JobID
		{
			get { return (System.Int64)GetValue((int)JobFieldIndex.JobID, true); }
			set	{ SetValue((int)JobFieldIndex.JobID, value, true); }
		}

		/// <summary> The JobState property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."JOBSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.JobState JobState
		{
			get { return (VarioSL.Entities.Enumerations.JobState)GetValue((int)JobFieldIndex.JobState, true); }
			set	{ SetValue((int)JobFieldIndex.JobState, value, true); }
		}

		/// <summary> The JobType property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."JOBTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.JobType JobType
		{
			get { return (VarioSL.Entities.Enumerations.JobType)GetValue((int)JobFieldIndex.JobType, true); }
			set	{ SetValue((int)JobFieldIndex.JobType, value, true); }
		}

		/// <summary> The LastModified property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)JobFieldIndex.LastModified, true); }
			set	{ SetValue((int)JobFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)JobFieldIndex.LastUser, true); }
			set	{ SetValue((int)JobFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MessageID property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."MESSAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MessageID
		{
			get { return (Nullable<System.Int64>)GetValue((int)JobFieldIndex.MessageID, false); }
			set	{ SetValue((int)JobFieldIndex.MessageID, value, true); }
		}

		/// <summary> The OrderDetailID property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."ORDERDETAILID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OrderDetailID
		{
			get { return (Nullable<System.Int64>)GetValue((int)JobFieldIndex.OrderDetailID, false); }
			set	{ SetValue((int)JobFieldIndex.OrderDetailID, value, true); }
		}

		/// <summary> The OutputMessages property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."OUTPUTMESSAGES"<br/>
		/// Table field type characteristics (type, precision, scale, length): NClob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OutputMessages
		{
			get { return (System.String)GetValue((int)JobFieldIndex.OutputMessages, true); }
			set	{ SetValue((int)JobFieldIndex.OutputMessages, value, true); }
		}

		/// <summary> The PrinterID property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."PRINTERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PrinterID
		{
			get { return (Nullable<System.Int64>)GetValue((int)JobFieldIndex.PrinterID, false); }
			set	{ SetValue((int)JobFieldIndex.PrinterID, value, true); }
		}

		/// <summary> The StartTime property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."STARTTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)JobFieldIndex.StartTime, false); }
			set	{ SetValue((int)JobFieldIndex.StartTime, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Job<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOB"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)JobFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)JobFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiJobCardImports()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.JobCardImportCollection JobCardImports
		{
			get	{ return GetMultiJobCardImports(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for JobCardImports. When set to true, JobCardImports is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time JobCardImports is accessed. You can always execute/ a forced fetch by calling GetMultiJobCardImports(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchJobCardImports
		{
			get	{ return _alwaysFetchJobCardImports; }
			set	{ _alwaysFetchJobCardImports = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property JobCardImports already has been fetched. Setting this property to false when JobCardImports has been fetched
		/// will clear the JobCardImports collection well. Setting this property to true while JobCardImports hasn't been fetched disables lazy loading for JobCardImports</summary>
		[Browsable(false)]
		public bool AlreadyFetchedJobCardImports
		{
			get { return _alreadyFetchedJobCardImports;}
			set 
			{
				if(_alreadyFetchedJobCardImports && !value && (_jobCardImports != null))
				{
					_jobCardImports.Clear();
				}
				_alreadyFetchedJobCardImports = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Jobs", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Jobs", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Jobs", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'InvoicingEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInvoicing()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual InvoicingEntity Invoicing
		{
			get	{ return GetSingleInvoicing(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInvoicing(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Jobs", "Invoicing", _invoicing, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Invoicing. When set to true, Invoicing is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoicing is accessed. You can always execute a forced fetch by calling GetSingleInvoicing(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoicing
		{
			get	{ return _alwaysFetchInvoicing; }
			set	{ _alwaysFetchInvoicing = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoicing already has been fetched. Setting this property to false when Invoicing has been fetched
		/// will set Invoicing to null as well. Setting this property to true while Invoicing hasn't been fetched disables lazy loading for Invoicing</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoicing
		{
			get { return _alreadyFetchedInvoicing;}
			set 
			{
				if(_alreadyFetchedInvoicing && !value)
				{
					this.Invoicing = null;
				}
				_alreadyFetchedInvoicing = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Invoicing is not found
		/// in the database. When set to true, Invoicing will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool InvoicingReturnsNewIfNotFound
		{
			get	{ return _invoicingReturnsNewIfNotFound; }
			set { _invoicingReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'JobBulkImportEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleJobBulkImport()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual JobBulkImportEntity JobBulkImport
		{
			get	{ return GetSingleJobBulkImport(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncJobBulkImport(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Jobs", "JobBulkImport", _jobBulkImport, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for JobBulkImport. When set to true, JobBulkImport is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time JobBulkImport is accessed. You can always execute a forced fetch by calling GetSingleJobBulkImport(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchJobBulkImport
		{
			get	{ return _alwaysFetchJobBulkImport; }
			set	{ _alwaysFetchJobBulkImport = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property JobBulkImport already has been fetched. Setting this property to false when JobBulkImport has been fetched
		/// will set JobBulkImport to null as well. Setting this property to true while JobBulkImport hasn't been fetched disables lazy loading for JobBulkImport</summary>
		[Browsable(false)]
		public bool AlreadyFetchedJobBulkImport
		{
			get { return _alreadyFetchedJobBulkImport;}
			set 
			{
				if(_alreadyFetchedJobBulkImport && !value)
				{
					this.JobBulkImport = null;
				}
				_alreadyFetchedJobBulkImport = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property JobBulkImport is not found
		/// in the database. When set to true, JobBulkImport will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool JobBulkImportReturnsNewIfNotFound
		{
			get	{ return _jobBulkImportReturnsNewIfNotFound; }
			set { _jobBulkImportReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MessageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual MessageEntity Message
		{
			get	{ return GetSingleMessage(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMessage(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Jobs", "Message", _message, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Message. When set to true, Message is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Message is accessed. You can always execute a forced fetch by calling GetSingleMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessage
		{
			get	{ return _alwaysFetchMessage; }
			set	{ _alwaysFetchMessage = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Message already has been fetched. Setting this property to false when Message has been fetched
		/// will set Message to null as well. Setting this property to true while Message hasn't been fetched disables lazy loading for Message</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessage
		{
			get { return _alreadyFetchedMessage;}
			set 
			{
				if(_alreadyFetchedMessage && !value)
				{
					this.Message = null;
				}
				_alreadyFetchedMessage = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Message is not found
		/// in the database. When set to true, Message will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool MessageReturnsNewIfNotFound
		{
			get	{ return _messageReturnsNewIfNotFound; }
			set { _messageReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrderDetailEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrderDetail()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OrderDetailEntity OrderDetail
		{
			get	{ return GetSingleOrderDetail(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrderDetail(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Jobs", "OrderDetail", _orderDetail, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrderDetail. When set to true, OrderDetail is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderDetail is accessed. You can always execute a forced fetch by calling GetSingleOrderDetail(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderDetail
		{
			get	{ return _alwaysFetchOrderDetail; }
			set	{ _alwaysFetchOrderDetail = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderDetail already has been fetched. Setting this property to false when OrderDetail has been fetched
		/// will set OrderDetail to null as well. Setting this property to true while OrderDetail hasn't been fetched disables lazy loading for OrderDetail</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderDetail
		{
			get { return _alreadyFetchedOrderDetail;}
			set 
			{
				if(_alreadyFetchedOrderDetail && !value)
				{
					this.OrderDetail = null;
				}
				_alreadyFetchedOrderDetail = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrderDetail is not found
		/// in the database. When set to true, OrderDetail will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OrderDetailReturnsNewIfNotFound
		{
			get	{ return _orderDetailReturnsNewIfNotFound; }
			set { _orderDetailReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PrinterEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePrinter()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PrinterEntity Printer
		{
			get	{ return GetSinglePrinter(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPrinter(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Jobs", "Printer", _printer, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Printer. When set to true, Printer is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Printer is accessed. You can always execute a forced fetch by calling GetSinglePrinter(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPrinter
		{
			get	{ return _alwaysFetchPrinter; }
			set	{ _alwaysFetchPrinter = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Printer already has been fetched. Setting this property to false when Printer has been fetched
		/// will set Printer to null as well. Setting this property to true while Printer hasn't been fetched disables lazy loading for Printer</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPrinter
		{
			get { return _alreadyFetchedPrinter;}
			set 
			{
				if(_alreadyFetchedPrinter && !value)
				{
					this.Printer = null;
				}
				_alreadyFetchedPrinter = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Printer is not found
		/// in the database. When set to true, Printer will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PrinterReturnsNewIfNotFound
		{
			get	{ return _printerReturnsNewIfNotFound; }
			set { _printerReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.JobEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
