﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TicketSerialNumber'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TicketSerialNumberEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private TicketEntity _ticket;
		private bool	_alwaysFetchTicket, _alreadyFetchedTicket, _ticketReturnsNewIfNotFound;
		private TicketTypeEntity _ticketType;
		private bool	_alwaysFetchTicketType, _alreadyFetchedTicketType, _ticketTypeReturnsNewIfNotFound;
		private ProductEntity _product;
		private bool	_alwaysFetchProduct, _alreadyFetchedProduct, _productReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Ticket</summary>
			public static readonly string Ticket = "Ticket";
			/// <summary>Member name TicketType</summary>
			public static readonly string TicketType = "TicketType";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TicketSerialNumberEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TicketSerialNumberEntity() :base("TicketSerialNumberEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="ticketSerialnumberID">PK value for TicketSerialNumber which data should be fetched into this TicketSerialNumber object</param>
		public TicketSerialNumberEntity(System.Int64 ticketSerialnumberID):base("TicketSerialNumberEntity")
		{
			InitClassFetch(ticketSerialnumberID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="ticketSerialnumberID">PK value for TicketSerialNumber which data should be fetched into this TicketSerialNumber object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TicketSerialNumberEntity(System.Int64 ticketSerialnumberID, IPrefetchPath prefetchPathToUse):base("TicketSerialNumberEntity")
		{
			InitClassFetch(ticketSerialnumberID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="ticketSerialnumberID">PK value for TicketSerialNumber which data should be fetched into this TicketSerialNumber object</param>
		/// <param name="validator">The custom validator object for this TicketSerialNumberEntity</param>
		public TicketSerialNumberEntity(System.Int64 ticketSerialnumberID, IValidator validator):base("TicketSerialNumberEntity")
		{
			InitClassFetch(ticketSerialnumberID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TicketSerialNumberEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_ticket = (TicketEntity)info.GetValue("_ticket", typeof(TicketEntity));
			if(_ticket!=null)
			{
				_ticket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketReturnsNewIfNotFound = info.GetBoolean("_ticketReturnsNewIfNotFound");
			_alwaysFetchTicket = info.GetBoolean("_alwaysFetchTicket");
			_alreadyFetchedTicket = info.GetBoolean("_alreadyFetchedTicket");

			_ticketType = (TicketTypeEntity)info.GetValue("_ticketType", typeof(TicketTypeEntity));
			if(_ticketType!=null)
			{
				_ticketType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketTypeReturnsNewIfNotFound = info.GetBoolean("_ticketTypeReturnsNewIfNotFound");
			_alwaysFetchTicketType = info.GetBoolean("_alwaysFetchTicketType");
			_alreadyFetchedTicketType = info.GetBoolean("_alreadyFetchedTicketType");

			_product = (ProductEntity)info.GetValue("_product", typeof(ProductEntity));
			if(_product!=null)
			{
				_product.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productReturnsNewIfNotFound = info.GetBoolean("_productReturnsNewIfNotFound");
			_alwaysFetchProduct = info.GetBoolean("_alwaysFetchProduct");
			_alreadyFetchedProduct = info.GetBoolean("_alreadyFetchedProduct");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TicketSerialNumberFieldIndex)fieldIndex)
			{
				case TicketSerialNumberFieldIndex.ProductID:
					DesetupSyncProduct(true, false);
					_alreadyFetchedProduct = false;
					break;
				case TicketSerialNumberFieldIndex.TicketID:
					DesetupSyncTicket(true, false);
					_alreadyFetchedTicket = false;
					break;
				case TicketSerialNumberFieldIndex.TicketTypeID:
					DesetupSyncTicketType(true, false);
					_alreadyFetchedTicketType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTicket = (_ticket != null);
			_alreadyFetchedTicketType = (_ticketType != null);
			_alreadyFetchedProduct = (_product != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Ticket":
					toReturn.Add(Relations.TicketEntityUsingTicketID);
					break;
				case "TicketType":
					toReturn.Add(Relations.TicketTypeEntityUsingTicketTypeID);
					break;
				case "Product":
					toReturn.Add(Relations.ProductEntityUsingProductID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_ticket", (!this.MarkedForDeletion?_ticket:null));
			info.AddValue("_ticketReturnsNewIfNotFound", _ticketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicket", _alwaysFetchTicket);
			info.AddValue("_alreadyFetchedTicket", _alreadyFetchedTicket);
			info.AddValue("_ticketType", (!this.MarkedForDeletion?_ticketType:null));
			info.AddValue("_ticketTypeReturnsNewIfNotFound", _ticketTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicketType", _alwaysFetchTicketType);
			info.AddValue("_alreadyFetchedTicketType", _alreadyFetchedTicketType);
			info.AddValue("_product", (!this.MarkedForDeletion?_product:null));
			info.AddValue("_productReturnsNewIfNotFound", _productReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProduct", _alwaysFetchProduct);
			info.AddValue("_alreadyFetchedProduct", _alreadyFetchedProduct);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Ticket":
					_alreadyFetchedTicket = true;
					this.Ticket = (TicketEntity)entity;
					break;
				case "TicketType":
					_alreadyFetchedTicketType = true;
					this.TicketType = (TicketTypeEntity)entity;
					break;
				case "Product":
					_alreadyFetchedProduct = true;
					this.Product = (ProductEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Ticket":
					SetupSyncTicket(relatedEntity);
					break;
				case "TicketType":
					SetupSyncTicketType(relatedEntity);
					break;
				case "Product":
					SetupSyncProduct(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Ticket":
					DesetupSyncTicket(false, true);
					break;
				case "TicketType":
					DesetupSyncTicketType(false, true);
					break;
				case "Product":
					DesetupSyncProduct(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_ticket!=null)
			{
				toReturn.Add(_ticket);
			}
			if(_ticketType!=null)
			{
				toReturn.Add(_ticketType);
			}
			if(_product!=null)
			{
				toReturn.Add(_product);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketSerialnumberID">PK value for TicketSerialNumber which data should be fetched into this TicketSerialNumber object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketSerialnumberID)
		{
			return FetchUsingPK(ticketSerialnumberID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketSerialnumberID">PK value for TicketSerialNumber which data should be fetched into this TicketSerialNumber object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketSerialnumberID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(ticketSerialnumberID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketSerialnumberID">PK value for TicketSerialNumber which data should be fetched into this TicketSerialNumber object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketSerialnumberID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(ticketSerialnumberID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketSerialnumberID">PK value for TicketSerialNumber which data should be fetched into this TicketSerialNumber object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketSerialnumberID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(ticketSerialnumberID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TicketSerialnumberID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TicketSerialNumberRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public TicketEntity GetSingleTicket()
		{
			return GetSingleTicket(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public virtual TicketEntity GetSingleTicket(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicket || forceFetch || _alwaysFetchTicket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketEntityUsingTicketID);
				TicketEntity newEntity = new TicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Ticket = newEntity;
				_alreadyFetchedTicket = fetchResult;
			}
			return _ticket;
		}


		/// <summary> Retrieves the related entity of type 'TicketTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketTypeEntity' which is related to this entity.</returns>
		public TicketTypeEntity GetSingleTicketType()
		{
			return GetSingleTicketType(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketTypeEntity' which is related to this entity.</returns>
		public virtual TicketTypeEntity GetSingleTicketType(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicketType || forceFetch || _alwaysFetchTicketType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketTypeEntityUsingTicketTypeID);
				TicketTypeEntity newEntity = new TicketTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketTypeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TicketType = newEntity;
				_alreadyFetchedTicketType = fetchResult;
			}
			return _ticketType;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProduct()
		{
			return GetSingleProduct(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProduct(bool forceFetch)
		{
			if( ( !_alreadyFetchedProduct || forceFetch || _alwaysFetchProduct) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductID);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Product = newEntity;
				_alreadyFetchedProduct = fetchResult;
			}
			return _product;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Ticket", _ticket);
			toReturn.Add("TicketType", _ticketType);
			toReturn.Add("Product", _product);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="ticketSerialnumberID">PK value for TicketSerialNumber which data should be fetched into this TicketSerialNumber object</param>
		/// <param name="validator">The validator object for this TicketSerialNumberEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 ticketSerialnumberID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(ticketSerialnumberID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_ticketReturnsNewIfNotFound = false;
			_ticketTypeReturnsNewIfNotFound = false;
			_productReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SerialNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SoldBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SoldDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketSerialnumberID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _ticket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticTicketSerialNumberRelations.TicketEntityUsingTicketIDStatic, true, signalRelatedEntity, "TicketSerialNumbers", resetFKFields, new int[] { (int)TicketSerialNumberFieldIndex.TicketID } );		
			_ticket = null;
		}
		
		/// <summary> setups the sync logic for member _ticket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicket(IEntityCore relatedEntity)
		{
			if(_ticket!=relatedEntity)
			{		
				DesetupSyncTicket(true, true);
				_ticket = (TicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticTicketSerialNumberRelations.TicketEntityUsingTicketIDStatic, true, ref _alreadyFetchedTicket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticketType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicketType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticketType, new PropertyChangedEventHandler( OnTicketTypePropertyChanged ), "TicketType", VarioSL.Entities.RelationClasses.StaticTicketSerialNumberRelations.TicketTypeEntityUsingTicketTypeIDStatic, true, signalRelatedEntity, "TicketSerialNumbers", resetFKFields, new int[] { (int)TicketSerialNumberFieldIndex.TicketTypeID } );		
			_ticketType = null;
		}
		
		/// <summary> setups the sync logic for member _ticketType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicketType(IEntityCore relatedEntity)
		{
			if(_ticketType!=relatedEntity)
			{		
				DesetupSyncTicketType(true, true);
				_ticketType = (TicketTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticketType, new PropertyChangedEventHandler( OnTicketTypePropertyChanged ), "TicketType", VarioSL.Entities.RelationClasses.StaticTicketSerialNumberRelations.TicketTypeEntityUsingTicketTypeIDStatic, true, ref _alreadyFetchedTicketType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _product</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProduct(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticTicketSerialNumberRelations.ProductEntityUsingProductIDStatic, true, signalRelatedEntity, "TicketSerialNumbers", resetFKFields, new int[] { (int)TicketSerialNumberFieldIndex.ProductID } );		
			_product = null;
		}
		
		/// <summary> setups the sync logic for member _product</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProduct(IEntityCore relatedEntity)
		{
			if(_product!=relatedEntity)
			{		
				DesetupSyncProduct(true, true);
				_product = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticTicketSerialNumberRelations.ProductEntityUsingProductIDStatic, true, ref _alreadyFetchedProduct, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="ticketSerialnumberID">PK value for TicketSerialNumber which data should be fetched into this TicketSerialNumber object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 ticketSerialnumberID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TicketSerialNumberFieldIndex.TicketSerialnumberID].ForcedCurrentValueWrite(ticketSerialnumberID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTicketSerialNumberDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TicketSerialNumberEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TicketSerialNumberRelations Relations
		{
			get	{ return new TicketSerialNumberRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Ticket")[0], (int)VarioSL.Entities.EntityType.TicketSerialNumberEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Ticket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketTypeCollection(), (IEntityRelation)GetRelationsForField("TicketType")[0], (int)VarioSL.Entities.EntityType.TicketSerialNumberEntity, (int)VarioSL.Entities.EntityType.TicketTypeEntity, 0, null, null, null, "TicketType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProduct
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Product")[0], (int)VarioSL.Entities.EntityType.TicketSerialNumberEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Product", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LastModified property of the Entity TicketSerialNumber<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETSERIALNUMBER"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)TicketSerialNumberFieldIndex.LastModified, true); }
			set	{ SetValue((int)TicketSerialNumberFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity TicketSerialNumber<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETSERIALNUMBER"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)TicketSerialNumberFieldIndex.LastUser, true); }
			set	{ SetValue((int)TicketSerialNumberFieldIndex.LastUser, value, true); }
		}

		/// <summary> The ProductID property of the Entity TicketSerialNumber<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETSERIALNUMBER"."PRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ProductID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketSerialNumberFieldIndex.ProductID, false); }
			set	{ SetValue((int)TicketSerialNumberFieldIndex.ProductID, value, true); }
		}

		/// <summary> The SerialNumber property of the Entity TicketSerialNumber<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETSERIALNUMBER"."SERIALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SerialNumber
		{
			get { return (System.Int64)GetValue((int)TicketSerialNumberFieldIndex.SerialNumber, true); }
			set	{ SetValue((int)TicketSerialNumberFieldIndex.SerialNumber, value, true); }
		}

		/// <summary> The SoldBy property of the Entity TicketSerialNumber<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETSERIALNUMBER"."SOLDBY"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SoldBy
		{
			get { return (System.String)GetValue((int)TicketSerialNumberFieldIndex.SoldBy, true); }
			set	{ SetValue((int)TicketSerialNumberFieldIndex.SoldBy, value, true); }
		}

		/// <summary> The SoldDate property of the Entity TicketSerialNumber<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETSERIALNUMBER"."SOLDDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> SoldDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TicketSerialNumberFieldIndex.SoldDate, false); }
			set	{ SetValue((int)TicketSerialNumberFieldIndex.SoldDate, value, true); }
		}

		/// <summary> The Status property of the Entity TicketSerialNumber<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETSERIALNUMBER"."STATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Status
		{
			get { return (System.Int32)GetValue((int)TicketSerialNumberFieldIndex.Status, true); }
			set	{ SetValue((int)TicketSerialNumberFieldIndex.Status, value, true); }
		}

		/// <summary> The TicketID property of the Entity TicketSerialNumber<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETSERIALNUMBER"."TICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketSerialNumberFieldIndex.TicketID, false); }
			set	{ SetValue((int)TicketSerialNumberFieldIndex.TicketID, value, true); }
		}

		/// <summary> The TicketSerialnumberID property of the Entity TicketSerialNumber<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETSERIALNUMBER"."TICKETSERIALNUMBERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 TicketSerialnumberID
		{
			get { return (System.Int64)GetValue((int)TicketSerialNumberFieldIndex.TicketSerialnumberID, true); }
			set	{ SetValue((int)TicketSerialNumberFieldIndex.TicketSerialnumberID, value, true); }
		}

		/// <summary> The TicketTypeID property of the Entity TicketSerialNumber<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETSERIALNUMBER"."TICKETTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketSerialNumberFieldIndex.TicketTypeID, false); }
			set	{ SetValue((int)TicketSerialNumberFieldIndex.TicketTypeID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity TicketSerialNumber<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TICKETSERIALNUMBER"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)TicketSerialNumberFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)TicketSerialNumberFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'TicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketEntity Ticket
		{
			get	{ return GetSingleTicket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketSerialNumbers", "Ticket", _ticket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Ticket. When set to true, Ticket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Ticket is accessed. You can always execute a forced fetch by calling GetSingleTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicket
		{
			get	{ return _alwaysFetchTicket; }
			set	{ _alwaysFetchTicket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Ticket already has been fetched. Setting this property to false when Ticket has been fetched
		/// will set Ticket to null as well. Setting this property to true while Ticket hasn't been fetched disables lazy loading for Ticket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicket
		{
			get { return _alreadyFetchedTicket;}
			set 
			{
				if(_alreadyFetchedTicket && !value)
				{
					this.Ticket = null;
				}
				_alreadyFetchedTicket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Ticket is not found
		/// in the database. When set to true, Ticket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketReturnsNewIfNotFound
		{
			get	{ return _ticketReturnsNewIfNotFound; }
			set { _ticketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicketType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketTypeEntity TicketType
		{
			get	{ return GetSingleTicketType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicketType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketSerialNumbers", "TicketType", _ticketType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TicketType. When set to true, TicketType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketType is accessed. You can always execute a forced fetch by calling GetSingleTicketType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketType
		{
			get	{ return _alwaysFetchTicketType; }
			set	{ _alwaysFetchTicketType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketType already has been fetched. Setting this property to false when TicketType has been fetched
		/// will set TicketType to null as well. Setting this property to true while TicketType hasn't been fetched disables lazy loading for TicketType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketType
		{
			get { return _alreadyFetchedTicketType;}
			set 
			{
				if(_alreadyFetchedTicketType && !value)
				{
					this.TicketType = null;
				}
				_alreadyFetchedTicketType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TicketType is not found
		/// in the database. When set to true, TicketType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketTypeReturnsNewIfNotFound
		{
			get	{ return _ticketTypeReturnsNewIfNotFound; }
			set { _ticketTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get	{ return GetSingleProduct(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProduct(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketSerialNumbers", "Product", _product, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Product. When set to true, Product is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Product is accessed. You can always execute a forced fetch by calling GetSingleProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProduct
		{
			get	{ return _alwaysFetchProduct; }
			set	{ _alwaysFetchProduct = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Product already has been fetched. Setting this property to false when Product has been fetched
		/// will set Product to null as well. Setting this property to true while Product hasn't been fetched disables lazy loading for Product</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProduct
		{
			get { return _alreadyFetchedProduct;}
			set 
			{
				if(_alreadyFetchedProduct && !value)
				{
					this.Product = null;
				}
				_alreadyFetchedProduct = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Product is not found
		/// in the database. When set to true, Product will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProductReturnsNewIfNotFound
		{
			get	{ return _productReturnsNewIfNotFound; }
			set { _productReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TicketSerialNumberEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
