﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Card'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CardEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ApportionmentResultCollection	_apportionmentResults;
		private bool	_alwaysFetchApportionmentResults, _alreadyFetchedApportionmentResults;
		private VarioSL.Entities.CollectionClasses.BadCardCollection	_badCards;
		private bool	_alwaysFetchBadCards, _alreadyFetchedBadCards;
		private VarioSL.Entities.CollectionClasses.BookingCollection	_bookings;
		private bool	_alwaysFetchBookings, _alreadyFetchedBookings;
		private VarioSL.Entities.CollectionClasses.CardCollection	_replacementCard;
		private bool	_alwaysFetchReplacementCard, _alreadyFetchedReplacementCard;
		private VarioSL.Entities.CollectionClasses.CardEventCollection	_cardEvents;
		private bool	_alwaysFetchCardEvents, _alreadyFetchedCardEvents;
		private VarioSL.Entities.CollectionClasses.CardLinkCollection	_cardLinksSource;
		private bool	_alwaysFetchCardLinksSource, _alreadyFetchedCardLinksSource;
		private VarioSL.Entities.CollectionClasses.CardStockTransferCollection	_cardStockTransfers;
		private bool	_alwaysFetchCardStockTransfers, _alreadyFetchedCardStockTransfers;
		private VarioSL.Entities.CollectionClasses.CardToContractCollection	_cardsToContracts;
		private bool	_alwaysFetchCardsToContracts, _alreadyFetchedCardsToContracts;
		private VarioSL.Entities.CollectionClasses.CardToRuleViolationCollection	_cardToRuleViolations;
		private bool	_alwaysFetchCardToRuleViolations, _alreadyFetchedCardToRuleViolations;
		private VarioSL.Entities.CollectionClasses.CardWorkItemCollection	_workItems;
		private bool	_alwaysFetchWorkItems, _alreadyFetchedWorkItems;
		private VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection	_creditCardAuthorizations;
		private bool	_alwaysFetchCreditCardAuthorizations, _alreadyFetchedCreditCardAuthorizations;
		private VarioSL.Entities.CollectionClasses.DormancyNotificationCollection	_dormancyNotifications;
		private bool	_alwaysFetchDormancyNotifications, _alreadyFetchedDormancyNotifications;
		private VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection	_emailEventResendLocks;
		private bool	_alwaysFetchEmailEventResendLocks, _alreadyFetchedEmailEventResendLocks;
		private VarioSL.Entities.CollectionClasses.JobCollection	_jobs;
		private bool	_alwaysFetchJobs, _alreadyFetchedJobs;
		private VarioSL.Entities.CollectionClasses.OrderDetailCollection	_orderDetails;
		private bool	_alwaysFetchOrderDetails, _alreadyFetchedOrderDetails;
		private VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection	_organizationParticipants;
		private bool	_alwaysFetchOrganizationParticipants, _alreadyFetchedOrganizationParticipants;
		private VarioSL.Entities.CollectionClasses.PaymentJournalCollection	_paymentJournals;
		private bool	_alwaysFetchPaymentJournals, _alreadyFetchedPaymentJournals;
		private VarioSL.Entities.CollectionClasses.PaymentOptionCollection	_paymentOptions;
		private bool	_alwaysFetchPaymentOptions, _alreadyFetchedPaymentOptions;
		private VarioSL.Entities.CollectionClasses.ProductCollection	_products;
		private bool	_alwaysFetchProducts, _alreadyFetchedProducts;
		private VarioSL.Entities.CollectionClasses.RuleViolationCollection	_ruleViolations;
		private bool	_alwaysFetchRuleViolations, _alreadyFetchedRuleViolations;
		private VarioSL.Entities.CollectionClasses.TicketAssignmentCollection	_ticketAssignments;
		private bool	_alwaysFetchTicketAssignments, _alreadyFetchedTicketAssignments;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_transactionJournals;
		private bool	_alwaysFetchTransactionJournals, _alreadyFetchedTransactionJournals;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_transactionJournals_;
		private bool	_alwaysFetchTransactionJournals_, _alreadyFetchedTransactionJournals_;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_tokenTransactionJournals;
		private bool	_alwaysFetchTokenTransactionJournals, _alreadyFetchedTokenTransactionJournals;
		private VarioSL.Entities.CollectionClasses.VirtualCardCollection	_virtualCards;
		private bool	_alwaysFetchVirtualCards, _alreadyFetchedVirtualCards;
		private VarioSL.Entities.CollectionClasses.VoucherCollection	_vouchers;
		private bool	_alwaysFetchVouchers, _alreadyFetchedVouchers;
		private VarioSL.Entities.CollectionClasses.WhitelistCollection	_whitelists;
		private bool	_alwaysFetchWhitelists, _alreadyFetchedWhitelists;
		private VarioSL.Entities.CollectionClasses.WhitelistJournalCollection	_whitelistJournals;
		private bool	_alwaysFetchWhitelistJournals, _alreadyFetchedWhitelistJournals;
		private VarioSL.Entities.CollectionClasses.ContractCollection _contracts;
		private bool	_alwaysFetchContracts, _alreadyFetchedContracts;
		private VarioSL.Entities.CollectionClasses.StockTransferCollection _stockTransferCollectionViaCardStockTransfer;
		private bool	_alwaysFetchStockTransferCollectionViaCardStockTransfer, _alreadyFetchedStockTransferCollectionViaCardStockTransfer;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private CardEntity _replacedCard;
		private bool	_alwaysFetchReplacedCard, _alreadyFetchedReplacedCard, _replacedCardReturnsNewIfNotFound;
		private CardHolderEntity _cardHolder;
		private bool	_alwaysFetchCardHolder, _alreadyFetchedCardHolder, _cardHolderReturnsNewIfNotFound;
		private CardHolderEntity _participant;
		private bool	_alwaysFetchParticipant, _alreadyFetchedParticipant, _participantReturnsNewIfNotFound;
		private CardKeyEntity _cardKey;
		private bool	_alwaysFetchCardKey, _alreadyFetchedCardKey, _cardKeyReturnsNewIfNotFound;
		private CardPhysicalDetailEntity _cardPhysicalDetail;
		private bool	_alwaysFetchCardPhysicalDetail, _alreadyFetchedCardPhysicalDetail, _cardPhysicalDetailReturnsNewIfNotFound;
		private StorageLocationEntity _storageLocation;
		private bool	_alwaysFetchStorageLocation, _alreadyFetchedStorageLocation, _storageLocationReturnsNewIfNotFound;
		private CardLinkEntity _cardLinkTarget;
		private bool	_alwaysFetchCardLinkTarget, _alreadyFetchedCardLinkTarget, _cardLinkTargetReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name ReplacedCard</summary>
			public static readonly string ReplacedCard = "ReplacedCard";
			/// <summary>Member name CardHolder</summary>
			public static readonly string CardHolder = "CardHolder";
			/// <summary>Member name Participant</summary>
			public static readonly string Participant = "Participant";
			/// <summary>Member name CardKey</summary>
			public static readonly string CardKey = "CardKey";
			/// <summary>Member name CardPhysicalDetail</summary>
			public static readonly string CardPhysicalDetail = "CardPhysicalDetail";
			/// <summary>Member name StorageLocation</summary>
			public static readonly string StorageLocation = "StorageLocation";
			/// <summary>Member name ApportionmentResults</summary>
			public static readonly string ApportionmentResults = "ApportionmentResults";
			/// <summary>Member name BadCards</summary>
			public static readonly string BadCards = "BadCards";
			/// <summary>Member name Bookings</summary>
			public static readonly string Bookings = "Bookings";
			/// <summary>Member name ReplacementCard</summary>
			public static readonly string ReplacementCard = "ReplacementCard";
			/// <summary>Member name CardEvents</summary>
			public static readonly string CardEvents = "CardEvents";
			/// <summary>Member name CardLinksSource</summary>
			public static readonly string CardLinksSource = "CardLinksSource";
			/// <summary>Member name CardStockTransfers</summary>
			public static readonly string CardStockTransfers = "CardStockTransfers";
			/// <summary>Member name CardsToContracts</summary>
			public static readonly string CardsToContracts = "CardsToContracts";
			/// <summary>Member name CardToRuleViolations</summary>
			public static readonly string CardToRuleViolations = "CardToRuleViolations";
			/// <summary>Member name WorkItems</summary>
			public static readonly string WorkItems = "WorkItems";
			/// <summary>Member name CreditCardAuthorizations</summary>
			public static readonly string CreditCardAuthorizations = "CreditCardAuthorizations";
			/// <summary>Member name DormancyNotifications</summary>
			public static readonly string DormancyNotifications = "DormancyNotifications";
			/// <summary>Member name EmailEventResendLocks</summary>
			public static readonly string EmailEventResendLocks = "EmailEventResendLocks";
			/// <summary>Member name Jobs</summary>
			public static readonly string Jobs = "Jobs";
			/// <summary>Member name OrderDetails</summary>
			public static readonly string OrderDetails = "OrderDetails";
			/// <summary>Member name OrganizationParticipants</summary>
			public static readonly string OrganizationParticipants = "OrganizationParticipants";
			/// <summary>Member name PaymentJournals</summary>
			public static readonly string PaymentJournals = "PaymentJournals";
			/// <summary>Member name PaymentOptions</summary>
			public static readonly string PaymentOptions = "PaymentOptions";
			/// <summary>Member name Products</summary>
			public static readonly string Products = "Products";
			/// <summary>Member name RuleViolations</summary>
			public static readonly string RuleViolations = "RuleViolations";
			/// <summary>Member name TicketAssignments</summary>
			public static readonly string TicketAssignments = "TicketAssignments";
			/// <summary>Member name TransactionJournals</summary>
			public static readonly string TransactionJournals = "TransactionJournals";
			/// <summary>Member name TransactionJournals_</summary>
			public static readonly string TransactionJournals_ = "TransactionJournals_";
			/// <summary>Member name TokenTransactionJournals</summary>
			public static readonly string TokenTransactionJournals = "TokenTransactionJournals";
			/// <summary>Member name VirtualCards</summary>
			public static readonly string VirtualCards = "VirtualCards";
			/// <summary>Member name Vouchers</summary>
			public static readonly string Vouchers = "Vouchers";
			/// <summary>Member name Whitelists</summary>
			public static readonly string Whitelists = "Whitelists";
			/// <summary>Member name WhitelistJournals</summary>
			public static readonly string WhitelistJournals = "WhitelistJournals";
			/// <summary>Member name Contracts</summary>
			public static readonly string Contracts = "Contracts";
			/// <summary>Member name StockTransferCollectionViaCardStockTransfer</summary>
			public static readonly string StockTransferCollectionViaCardStockTransfer = "StockTransferCollectionViaCardStockTransfer";
			/// <summary>Member name CardLinkTarget</summary>
			public static readonly string CardLinkTarget = "CardLinkTarget";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CardEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CardEntity() :base("CardEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cardID">PK value for Card which data should be fetched into this Card object</param>
		public CardEntity(System.Int64 cardID):base("CardEntity")
		{
			InitClassFetch(cardID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cardID">PK value for Card which data should be fetched into this Card object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CardEntity(System.Int64 cardID, IPrefetchPath prefetchPathToUse):base("CardEntity")
		{
			InitClassFetch(cardID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cardID">PK value for Card which data should be fetched into this Card object</param>
		/// <param name="validator">The custom validator object for this CardEntity</param>
		public CardEntity(System.Int64 cardID, IValidator validator):base("CardEntity")
		{
			InitClassFetch(cardID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CardEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_apportionmentResults = (VarioSL.Entities.CollectionClasses.ApportionmentResultCollection)info.GetValue("_apportionmentResults", typeof(VarioSL.Entities.CollectionClasses.ApportionmentResultCollection));
			_alwaysFetchApportionmentResults = info.GetBoolean("_alwaysFetchApportionmentResults");
			_alreadyFetchedApportionmentResults = info.GetBoolean("_alreadyFetchedApportionmentResults");

			_badCards = (VarioSL.Entities.CollectionClasses.BadCardCollection)info.GetValue("_badCards", typeof(VarioSL.Entities.CollectionClasses.BadCardCollection));
			_alwaysFetchBadCards = info.GetBoolean("_alwaysFetchBadCards");
			_alreadyFetchedBadCards = info.GetBoolean("_alreadyFetchedBadCards");

			_bookings = (VarioSL.Entities.CollectionClasses.BookingCollection)info.GetValue("_bookings", typeof(VarioSL.Entities.CollectionClasses.BookingCollection));
			_alwaysFetchBookings = info.GetBoolean("_alwaysFetchBookings");
			_alreadyFetchedBookings = info.GetBoolean("_alreadyFetchedBookings");

			_replacementCard = (VarioSL.Entities.CollectionClasses.CardCollection)info.GetValue("_replacementCard", typeof(VarioSL.Entities.CollectionClasses.CardCollection));
			_alwaysFetchReplacementCard = info.GetBoolean("_alwaysFetchReplacementCard");
			_alreadyFetchedReplacementCard = info.GetBoolean("_alreadyFetchedReplacementCard");

			_cardEvents = (VarioSL.Entities.CollectionClasses.CardEventCollection)info.GetValue("_cardEvents", typeof(VarioSL.Entities.CollectionClasses.CardEventCollection));
			_alwaysFetchCardEvents = info.GetBoolean("_alwaysFetchCardEvents");
			_alreadyFetchedCardEvents = info.GetBoolean("_alreadyFetchedCardEvents");

			_cardLinksSource = (VarioSL.Entities.CollectionClasses.CardLinkCollection)info.GetValue("_cardLinksSource", typeof(VarioSL.Entities.CollectionClasses.CardLinkCollection));
			_alwaysFetchCardLinksSource = info.GetBoolean("_alwaysFetchCardLinksSource");
			_alreadyFetchedCardLinksSource = info.GetBoolean("_alreadyFetchedCardLinksSource");

			_cardStockTransfers = (VarioSL.Entities.CollectionClasses.CardStockTransferCollection)info.GetValue("_cardStockTransfers", typeof(VarioSL.Entities.CollectionClasses.CardStockTransferCollection));
			_alwaysFetchCardStockTransfers = info.GetBoolean("_alwaysFetchCardStockTransfers");
			_alreadyFetchedCardStockTransfers = info.GetBoolean("_alreadyFetchedCardStockTransfers");

			_cardsToContracts = (VarioSL.Entities.CollectionClasses.CardToContractCollection)info.GetValue("_cardsToContracts", typeof(VarioSL.Entities.CollectionClasses.CardToContractCollection));
			_alwaysFetchCardsToContracts = info.GetBoolean("_alwaysFetchCardsToContracts");
			_alreadyFetchedCardsToContracts = info.GetBoolean("_alreadyFetchedCardsToContracts");

			_cardToRuleViolations = (VarioSL.Entities.CollectionClasses.CardToRuleViolationCollection)info.GetValue("_cardToRuleViolations", typeof(VarioSL.Entities.CollectionClasses.CardToRuleViolationCollection));
			_alwaysFetchCardToRuleViolations = info.GetBoolean("_alwaysFetchCardToRuleViolations");
			_alreadyFetchedCardToRuleViolations = info.GetBoolean("_alreadyFetchedCardToRuleViolations");

			_workItems = (VarioSL.Entities.CollectionClasses.CardWorkItemCollection)info.GetValue("_workItems", typeof(VarioSL.Entities.CollectionClasses.CardWorkItemCollection));
			_alwaysFetchWorkItems = info.GetBoolean("_alwaysFetchWorkItems");
			_alreadyFetchedWorkItems = info.GetBoolean("_alreadyFetchedWorkItems");

			_creditCardAuthorizations = (VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection)info.GetValue("_creditCardAuthorizations", typeof(VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection));
			_alwaysFetchCreditCardAuthorizations = info.GetBoolean("_alwaysFetchCreditCardAuthorizations");
			_alreadyFetchedCreditCardAuthorizations = info.GetBoolean("_alreadyFetchedCreditCardAuthorizations");

			_dormancyNotifications = (VarioSL.Entities.CollectionClasses.DormancyNotificationCollection)info.GetValue("_dormancyNotifications", typeof(VarioSL.Entities.CollectionClasses.DormancyNotificationCollection));
			_alwaysFetchDormancyNotifications = info.GetBoolean("_alwaysFetchDormancyNotifications");
			_alreadyFetchedDormancyNotifications = info.GetBoolean("_alreadyFetchedDormancyNotifications");

			_emailEventResendLocks = (VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection)info.GetValue("_emailEventResendLocks", typeof(VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection));
			_alwaysFetchEmailEventResendLocks = info.GetBoolean("_alwaysFetchEmailEventResendLocks");
			_alreadyFetchedEmailEventResendLocks = info.GetBoolean("_alreadyFetchedEmailEventResendLocks");

			_jobs = (VarioSL.Entities.CollectionClasses.JobCollection)info.GetValue("_jobs", typeof(VarioSL.Entities.CollectionClasses.JobCollection));
			_alwaysFetchJobs = info.GetBoolean("_alwaysFetchJobs");
			_alreadyFetchedJobs = info.GetBoolean("_alreadyFetchedJobs");

			_orderDetails = (VarioSL.Entities.CollectionClasses.OrderDetailCollection)info.GetValue("_orderDetails", typeof(VarioSL.Entities.CollectionClasses.OrderDetailCollection));
			_alwaysFetchOrderDetails = info.GetBoolean("_alwaysFetchOrderDetails");
			_alreadyFetchedOrderDetails = info.GetBoolean("_alreadyFetchedOrderDetails");

			_organizationParticipants = (VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection)info.GetValue("_organizationParticipants", typeof(VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection));
			_alwaysFetchOrganizationParticipants = info.GetBoolean("_alwaysFetchOrganizationParticipants");
			_alreadyFetchedOrganizationParticipants = info.GetBoolean("_alreadyFetchedOrganizationParticipants");

			_paymentJournals = (VarioSL.Entities.CollectionClasses.PaymentJournalCollection)info.GetValue("_paymentJournals", typeof(VarioSL.Entities.CollectionClasses.PaymentJournalCollection));
			_alwaysFetchPaymentJournals = info.GetBoolean("_alwaysFetchPaymentJournals");
			_alreadyFetchedPaymentJournals = info.GetBoolean("_alreadyFetchedPaymentJournals");

			_paymentOptions = (VarioSL.Entities.CollectionClasses.PaymentOptionCollection)info.GetValue("_paymentOptions", typeof(VarioSL.Entities.CollectionClasses.PaymentOptionCollection));
			_alwaysFetchPaymentOptions = info.GetBoolean("_alwaysFetchPaymentOptions");
			_alreadyFetchedPaymentOptions = info.GetBoolean("_alreadyFetchedPaymentOptions");

			_products = (VarioSL.Entities.CollectionClasses.ProductCollection)info.GetValue("_products", typeof(VarioSL.Entities.CollectionClasses.ProductCollection));
			_alwaysFetchProducts = info.GetBoolean("_alwaysFetchProducts");
			_alreadyFetchedProducts = info.GetBoolean("_alreadyFetchedProducts");

			_ruleViolations = (VarioSL.Entities.CollectionClasses.RuleViolationCollection)info.GetValue("_ruleViolations", typeof(VarioSL.Entities.CollectionClasses.RuleViolationCollection));
			_alwaysFetchRuleViolations = info.GetBoolean("_alwaysFetchRuleViolations");
			_alreadyFetchedRuleViolations = info.GetBoolean("_alreadyFetchedRuleViolations");

			_ticketAssignments = (VarioSL.Entities.CollectionClasses.TicketAssignmentCollection)info.GetValue("_ticketAssignments", typeof(VarioSL.Entities.CollectionClasses.TicketAssignmentCollection));
			_alwaysFetchTicketAssignments = info.GetBoolean("_alwaysFetchTicketAssignments");
			_alreadyFetchedTicketAssignments = info.GetBoolean("_alreadyFetchedTicketAssignments");

			_transactionJournals = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_transactionJournals", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTransactionJournals = info.GetBoolean("_alwaysFetchTransactionJournals");
			_alreadyFetchedTransactionJournals = info.GetBoolean("_alreadyFetchedTransactionJournals");

			_transactionJournals_ = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_transactionJournals_", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTransactionJournals_ = info.GetBoolean("_alwaysFetchTransactionJournals_");
			_alreadyFetchedTransactionJournals_ = info.GetBoolean("_alreadyFetchedTransactionJournals_");

			_tokenTransactionJournals = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_tokenTransactionJournals", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTokenTransactionJournals = info.GetBoolean("_alwaysFetchTokenTransactionJournals");
			_alreadyFetchedTokenTransactionJournals = info.GetBoolean("_alreadyFetchedTokenTransactionJournals");

			_virtualCards = (VarioSL.Entities.CollectionClasses.VirtualCardCollection)info.GetValue("_virtualCards", typeof(VarioSL.Entities.CollectionClasses.VirtualCardCollection));
			_alwaysFetchVirtualCards = info.GetBoolean("_alwaysFetchVirtualCards");
			_alreadyFetchedVirtualCards = info.GetBoolean("_alreadyFetchedVirtualCards");

			_vouchers = (VarioSL.Entities.CollectionClasses.VoucherCollection)info.GetValue("_vouchers", typeof(VarioSL.Entities.CollectionClasses.VoucherCollection));
			_alwaysFetchVouchers = info.GetBoolean("_alwaysFetchVouchers");
			_alreadyFetchedVouchers = info.GetBoolean("_alreadyFetchedVouchers");

			_whitelists = (VarioSL.Entities.CollectionClasses.WhitelistCollection)info.GetValue("_whitelists", typeof(VarioSL.Entities.CollectionClasses.WhitelistCollection));
			_alwaysFetchWhitelists = info.GetBoolean("_alwaysFetchWhitelists");
			_alreadyFetchedWhitelists = info.GetBoolean("_alreadyFetchedWhitelists");

			_whitelistJournals = (VarioSL.Entities.CollectionClasses.WhitelistJournalCollection)info.GetValue("_whitelistJournals", typeof(VarioSL.Entities.CollectionClasses.WhitelistJournalCollection));
			_alwaysFetchWhitelistJournals = info.GetBoolean("_alwaysFetchWhitelistJournals");
			_alreadyFetchedWhitelistJournals = info.GetBoolean("_alreadyFetchedWhitelistJournals");
			_contracts = (VarioSL.Entities.CollectionClasses.ContractCollection)info.GetValue("_contracts", typeof(VarioSL.Entities.CollectionClasses.ContractCollection));
			_alwaysFetchContracts = info.GetBoolean("_alwaysFetchContracts");
			_alreadyFetchedContracts = info.GetBoolean("_alreadyFetchedContracts");

			_stockTransferCollectionViaCardStockTransfer = (VarioSL.Entities.CollectionClasses.StockTransferCollection)info.GetValue("_stockTransferCollectionViaCardStockTransfer", typeof(VarioSL.Entities.CollectionClasses.StockTransferCollection));
			_alwaysFetchStockTransferCollectionViaCardStockTransfer = info.GetBoolean("_alwaysFetchStockTransferCollectionViaCardStockTransfer");
			_alreadyFetchedStockTransferCollectionViaCardStockTransfer = info.GetBoolean("_alreadyFetchedStockTransferCollectionViaCardStockTransfer");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_replacedCard = (CardEntity)info.GetValue("_replacedCard", typeof(CardEntity));
			if(_replacedCard!=null)
			{
				_replacedCard.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_replacedCardReturnsNewIfNotFound = info.GetBoolean("_replacedCardReturnsNewIfNotFound");
			_alwaysFetchReplacedCard = info.GetBoolean("_alwaysFetchReplacedCard");
			_alreadyFetchedReplacedCard = info.GetBoolean("_alreadyFetchedReplacedCard");

			_cardHolder = (CardHolderEntity)info.GetValue("_cardHolder", typeof(CardHolderEntity));
			if(_cardHolder!=null)
			{
				_cardHolder.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardHolderReturnsNewIfNotFound = info.GetBoolean("_cardHolderReturnsNewIfNotFound");
			_alwaysFetchCardHolder = info.GetBoolean("_alwaysFetchCardHolder");
			_alreadyFetchedCardHolder = info.GetBoolean("_alreadyFetchedCardHolder");

			_participant = (CardHolderEntity)info.GetValue("_participant", typeof(CardHolderEntity));
			if(_participant!=null)
			{
				_participant.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_participantReturnsNewIfNotFound = info.GetBoolean("_participantReturnsNewIfNotFound");
			_alwaysFetchParticipant = info.GetBoolean("_alwaysFetchParticipant");
			_alreadyFetchedParticipant = info.GetBoolean("_alreadyFetchedParticipant");

			_cardKey = (CardKeyEntity)info.GetValue("_cardKey", typeof(CardKeyEntity));
			if(_cardKey!=null)
			{
				_cardKey.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardKeyReturnsNewIfNotFound = info.GetBoolean("_cardKeyReturnsNewIfNotFound");
			_alwaysFetchCardKey = info.GetBoolean("_alwaysFetchCardKey");
			_alreadyFetchedCardKey = info.GetBoolean("_alreadyFetchedCardKey");

			_cardPhysicalDetail = (CardPhysicalDetailEntity)info.GetValue("_cardPhysicalDetail", typeof(CardPhysicalDetailEntity));
			if(_cardPhysicalDetail!=null)
			{
				_cardPhysicalDetail.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardPhysicalDetailReturnsNewIfNotFound = info.GetBoolean("_cardPhysicalDetailReturnsNewIfNotFound");
			_alwaysFetchCardPhysicalDetail = info.GetBoolean("_alwaysFetchCardPhysicalDetail");
			_alreadyFetchedCardPhysicalDetail = info.GetBoolean("_alreadyFetchedCardPhysicalDetail");

			_storageLocation = (StorageLocationEntity)info.GetValue("_storageLocation", typeof(StorageLocationEntity));
			if(_storageLocation!=null)
			{
				_storageLocation.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_storageLocationReturnsNewIfNotFound = info.GetBoolean("_storageLocationReturnsNewIfNotFound");
			_alwaysFetchStorageLocation = info.GetBoolean("_alwaysFetchStorageLocation");
			_alreadyFetchedStorageLocation = info.GetBoolean("_alreadyFetchedStorageLocation");
			_cardLinkTarget = (CardLinkEntity)info.GetValue("_cardLinkTarget", typeof(CardLinkEntity));
			if(_cardLinkTarget!=null)
			{
				_cardLinkTarget.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardLinkTargetReturnsNewIfNotFound = info.GetBoolean("_cardLinkTargetReturnsNewIfNotFound");
			_alwaysFetchCardLinkTarget = info.GetBoolean("_alwaysFetchCardLinkTarget");
			_alreadyFetchedCardLinkTarget = info.GetBoolean("_alreadyFetchedCardLinkTarget");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CardFieldIndex)fieldIndex)
			{
				case CardFieldIndex.CardHolderID:
					DesetupSyncCardHolder(true, false);
					_alreadyFetchedCardHolder = false;
					break;
				case CardFieldIndex.CardKeyID:
					DesetupSyncCardKey(true, false);
					_alreadyFetchedCardKey = false;
					break;
				case CardFieldIndex.CardPhysicalDetailID:
					DesetupSyncCardPhysicalDetail(true, false);
					_alreadyFetchedCardPhysicalDetail = false;
					break;
				case CardFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case CardFieldIndex.ParticipantID:
					DesetupSyncParticipant(true, false);
					_alreadyFetchedParticipant = false;
					break;
				case CardFieldIndex.ReplacedCardID:
					DesetupSyncReplacedCard(true, false);
					_alreadyFetchedReplacedCard = false;
					break;
				case CardFieldIndex.StorageLocationID:
					DesetupSyncStorageLocation(true, false);
					_alreadyFetchedStorageLocation = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedApportionmentResults = (_apportionmentResults.Count > 0);
			_alreadyFetchedBadCards = (_badCards.Count > 0);
			_alreadyFetchedBookings = (_bookings.Count > 0);
			_alreadyFetchedReplacementCard = (_replacementCard.Count > 0);
			_alreadyFetchedCardEvents = (_cardEvents.Count > 0);
			_alreadyFetchedCardLinksSource = (_cardLinksSource.Count > 0);
			_alreadyFetchedCardStockTransfers = (_cardStockTransfers.Count > 0);
			_alreadyFetchedCardsToContracts = (_cardsToContracts.Count > 0);
			_alreadyFetchedCardToRuleViolations = (_cardToRuleViolations.Count > 0);
			_alreadyFetchedWorkItems = (_workItems.Count > 0);
			_alreadyFetchedCreditCardAuthorizations = (_creditCardAuthorizations.Count > 0);
			_alreadyFetchedDormancyNotifications = (_dormancyNotifications.Count > 0);
			_alreadyFetchedEmailEventResendLocks = (_emailEventResendLocks.Count > 0);
			_alreadyFetchedJobs = (_jobs.Count > 0);
			_alreadyFetchedOrderDetails = (_orderDetails.Count > 0);
			_alreadyFetchedOrganizationParticipants = (_organizationParticipants.Count > 0);
			_alreadyFetchedPaymentJournals = (_paymentJournals.Count > 0);
			_alreadyFetchedPaymentOptions = (_paymentOptions.Count > 0);
			_alreadyFetchedProducts = (_products.Count > 0);
			_alreadyFetchedRuleViolations = (_ruleViolations.Count > 0);
			_alreadyFetchedTicketAssignments = (_ticketAssignments.Count > 0);
			_alreadyFetchedTransactionJournals = (_transactionJournals.Count > 0);
			_alreadyFetchedTransactionJournals_ = (_transactionJournals_.Count > 0);
			_alreadyFetchedTokenTransactionJournals = (_tokenTransactionJournals.Count > 0);
			_alreadyFetchedVirtualCards = (_virtualCards.Count > 0);
			_alreadyFetchedVouchers = (_vouchers.Count > 0);
			_alreadyFetchedWhitelists = (_whitelists.Count > 0);
			_alreadyFetchedWhitelistJournals = (_whitelistJournals.Count > 0);
			_alreadyFetchedContracts = (_contracts.Count > 0);
			_alreadyFetchedStockTransferCollectionViaCardStockTransfer = (_stockTransferCollectionViaCardStockTransfer.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedReplacedCard = (_replacedCard != null);
			_alreadyFetchedCardHolder = (_cardHolder != null);
			_alreadyFetchedParticipant = (_participant != null);
			_alreadyFetchedCardKey = (_cardKey != null);
			_alreadyFetchedCardPhysicalDetail = (_cardPhysicalDetail != null);
			_alreadyFetchedStorageLocation = (_storageLocation != null);
			_alreadyFetchedCardLinkTarget = (_cardLinkTarget != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "ReplacedCard":
					toReturn.Add(Relations.CardEntityUsingCardIDReplacedCardID);
					break;
				case "CardHolder":
					toReturn.Add(Relations.CardHolderEntityUsingCardHolderID);
					break;
				case "Participant":
					toReturn.Add(Relations.CardHolderEntityUsingParticipantID);
					break;
				case "CardKey":
					toReturn.Add(Relations.CardKeyEntityUsingCardKeyID);
					break;
				case "CardPhysicalDetail":
					toReturn.Add(Relations.CardPhysicalDetailEntityUsingCardPhysicalDetailID);
					break;
				case "StorageLocation":
					toReturn.Add(Relations.StorageLocationEntityUsingStorageLocationID);
					break;
				case "ApportionmentResults":
					toReturn.Add(Relations.ApportionmentResultEntityUsingCardID);
					break;
				case "BadCards":
					toReturn.Add(Relations.BadCardEntityUsingCardID);
					break;
				case "Bookings":
					toReturn.Add(Relations.BookingEntityUsingCardId);
					break;
				case "ReplacementCard":
					toReturn.Add(Relations.CardEntityUsingReplacedCardID);
					break;
				case "CardEvents":
					toReturn.Add(Relations.CardEventEntityUsingCardID);
					break;
				case "CardLinksSource":
					toReturn.Add(Relations.CardLinkEntityUsingSourceCardID);
					break;
				case "CardStockTransfers":
					toReturn.Add(Relations.CardStockTransferEntityUsingCardID);
					break;
				case "CardsToContracts":
					toReturn.Add(Relations.CardToContractEntityUsingCardID);
					break;
				case "CardToRuleViolations":
					toReturn.Add(Relations.CardToRuleViolationEntityUsingCardID);
					break;
				case "WorkItems":
					toReturn.Add(Relations.CardWorkItemEntityUsingCardID);
					break;
				case "CreditCardAuthorizations":
					toReturn.Add(Relations.CreditCardAuthorizationEntityUsingCardID);
					break;
				case "DormancyNotifications":
					toReturn.Add(Relations.DormancyNotificationEntityUsingCardID);
					break;
				case "EmailEventResendLocks":
					toReturn.Add(Relations.EmailEventResendLockEntityUsingCardID);
					break;
				case "Jobs":
					toReturn.Add(Relations.JobEntityUsingCardID);
					break;
				case "OrderDetails":
					toReturn.Add(Relations.OrderDetailEntityUsingReplacementCardID);
					break;
				case "OrganizationParticipants":
					toReturn.Add(Relations.OrganizationParticipantEntityUsingCardID);
					break;
				case "PaymentJournals":
					toReturn.Add(Relations.PaymentJournalEntityUsingCardID);
					break;
				case "PaymentOptions":
					toReturn.Add(Relations.PaymentOptionEntityUsingCardID);
					break;
				case "Products":
					toReturn.Add(Relations.ProductEntityUsingCardID);
					break;
				case "RuleViolations":
					toReturn.Add(Relations.RuleViolationEntityUsingCardID);
					break;
				case "TicketAssignments":
					toReturn.Add(Relations.TicketAssignmentEntityUsingCardID);
					break;
				case "TransactionJournals":
					toReturn.Add(Relations.TransactionJournalEntityUsingTransitAccountID);
					break;
				case "TransactionJournals_":
					toReturn.Add(Relations.TransactionJournalEntityUsingTransferredfromcardid);
					break;
				case "TokenTransactionJournals":
					toReturn.Add(Relations.TransactionJournalEntityUsingTokenCardID);
					break;
				case "VirtualCards":
					toReturn.Add(Relations.VirtualCardEntityUsingCardID);
					break;
				case "Vouchers":
					toReturn.Add(Relations.VoucherEntityUsingRedeemedForCardID);
					break;
				case "Whitelists":
					toReturn.Add(Relations.WhitelistEntityUsingCardID);
					break;
				case "WhitelistJournals":
					toReturn.Add(Relations.WhitelistJournalEntityUsingCardID);
					break;
				case "Contracts":
					toReturn.Add(Relations.CardToContractEntityUsingCardID, "CardEntity__", "CardToContract_", JoinHint.None);
					toReturn.Add(CardToContractEntity.Relations.ContractEntityUsingContractID, "CardToContract_", string.Empty, JoinHint.None);
					break;
				case "StockTransferCollectionViaCardStockTransfer":
					toReturn.Add(Relations.CardStockTransferEntityUsingCardID, "CardEntity__", "CardStockTransfer_", JoinHint.None);
					toReturn.Add(CardStockTransferEntity.Relations.StockTransferEntityUsingStockTransferID, "CardStockTransfer_", string.Empty, JoinHint.None);
					break;
				case "CardLinkTarget":
					toReturn.Add(Relations.CardLinkEntityUsingTargetCardID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_apportionmentResults", (!this.MarkedForDeletion?_apportionmentResults:null));
			info.AddValue("_alwaysFetchApportionmentResults", _alwaysFetchApportionmentResults);
			info.AddValue("_alreadyFetchedApportionmentResults", _alreadyFetchedApportionmentResults);
			info.AddValue("_badCards", (!this.MarkedForDeletion?_badCards:null));
			info.AddValue("_alwaysFetchBadCards", _alwaysFetchBadCards);
			info.AddValue("_alreadyFetchedBadCards", _alreadyFetchedBadCards);
			info.AddValue("_bookings", (!this.MarkedForDeletion?_bookings:null));
			info.AddValue("_alwaysFetchBookings", _alwaysFetchBookings);
			info.AddValue("_alreadyFetchedBookings", _alreadyFetchedBookings);
			info.AddValue("_replacementCard", (!this.MarkedForDeletion?_replacementCard:null));
			info.AddValue("_alwaysFetchReplacementCard", _alwaysFetchReplacementCard);
			info.AddValue("_alreadyFetchedReplacementCard", _alreadyFetchedReplacementCard);
			info.AddValue("_cardEvents", (!this.MarkedForDeletion?_cardEvents:null));
			info.AddValue("_alwaysFetchCardEvents", _alwaysFetchCardEvents);
			info.AddValue("_alreadyFetchedCardEvents", _alreadyFetchedCardEvents);
			info.AddValue("_cardLinksSource", (!this.MarkedForDeletion?_cardLinksSource:null));
			info.AddValue("_alwaysFetchCardLinksSource", _alwaysFetchCardLinksSource);
			info.AddValue("_alreadyFetchedCardLinksSource", _alreadyFetchedCardLinksSource);
			info.AddValue("_cardStockTransfers", (!this.MarkedForDeletion?_cardStockTransfers:null));
			info.AddValue("_alwaysFetchCardStockTransfers", _alwaysFetchCardStockTransfers);
			info.AddValue("_alreadyFetchedCardStockTransfers", _alreadyFetchedCardStockTransfers);
			info.AddValue("_cardsToContracts", (!this.MarkedForDeletion?_cardsToContracts:null));
			info.AddValue("_alwaysFetchCardsToContracts", _alwaysFetchCardsToContracts);
			info.AddValue("_alreadyFetchedCardsToContracts", _alreadyFetchedCardsToContracts);
			info.AddValue("_cardToRuleViolations", (!this.MarkedForDeletion?_cardToRuleViolations:null));
			info.AddValue("_alwaysFetchCardToRuleViolations", _alwaysFetchCardToRuleViolations);
			info.AddValue("_alreadyFetchedCardToRuleViolations", _alreadyFetchedCardToRuleViolations);
			info.AddValue("_workItems", (!this.MarkedForDeletion?_workItems:null));
			info.AddValue("_alwaysFetchWorkItems", _alwaysFetchWorkItems);
			info.AddValue("_alreadyFetchedWorkItems", _alreadyFetchedWorkItems);
			info.AddValue("_creditCardAuthorizations", (!this.MarkedForDeletion?_creditCardAuthorizations:null));
			info.AddValue("_alwaysFetchCreditCardAuthorizations", _alwaysFetchCreditCardAuthorizations);
			info.AddValue("_alreadyFetchedCreditCardAuthorizations", _alreadyFetchedCreditCardAuthorizations);
			info.AddValue("_dormancyNotifications", (!this.MarkedForDeletion?_dormancyNotifications:null));
			info.AddValue("_alwaysFetchDormancyNotifications", _alwaysFetchDormancyNotifications);
			info.AddValue("_alreadyFetchedDormancyNotifications", _alreadyFetchedDormancyNotifications);
			info.AddValue("_emailEventResendLocks", (!this.MarkedForDeletion?_emailEventResendLocks:null));
			info.AddValue("_alwaysFetchEmailEventResendLocks", _alwaysFetchEmailEventResendLocks);
			info.AddValue("_alreadyFetchedEmailEventResendLocks", _alreadyFetchedEmailEventResendLocks);
			info.AddValue("_jobs", (!this.MarkedForDeletion?_jobs:null));
			info.AddValue("_alwaysFetchJobs", _alwaysFetchJobs);
			info.AddValue("_alreadyFetchedJobs", _alreadyFetchedJobs);
			info.AddValue("_orderDetails", (!this.MarkedForDeletion?_orderDetails:null));
			info.AddValue("_alwaysFetchOrderDetails", _alwaysFetchOrderDetails);
			info.AddValue("_alreadyFetchedOrderDetails", _alreadyFetchedOrderDetails);
			info.AddValue("_organizationParticipants", (!this.MarkedForDeletion?_organizationParticipants:null));
			info.AddValue("_alwaysFetchOrganizationParticipants", _alwaysFetchOrganizationParticipants);
			info.AddValue("_alreadyFetchedOrganizationParticipants", _alreadyFetchedOrganizationParticipants);
			info.AddValue("_paymentJournals", (!this.MarkedForDeletion?_paymentJournals:null));
			info.AddValue("_alwaysFetchPaymentJournals", _alwaysFetchPaymentJournals);
			info.AddValue("_alreadyFetchedPaymentJournals", _alreadyFetchedPaymentJournals);
			info.AddValue("_paymentOptions", (!this.MarkedForDeletion?_paymentOptions:null));
			info.AddValue("_alwaysFetchPaymentOptions", _alwaysFetchPaymentOptions);
			info.AddValue("_alreadyFetchedPaymentOptions", _alreadyFetchedPaymentOptions);
			info.AddValue("_products", (!this.MarkedForDeletion?_products:null));
			info.AddValue("_alwaysFetchProducts", _alwaysFetchProducts);
			info.AddValue("_alreadyFetchedProducts", _alreadyFetchedProducts);
			info.AddValue("_ruleViolations", (!this.MarkedForDeletion?_ruleViolations:null));
			info.AddValue("_alwaysFetchRuleViolations", _alwaysFetchRuleViolations);
			info.AddValue("_alreadyFetchedRuleViolations", _alreadyFetchedRuleViolations);
			info.AddValue("_ticketAssignments", (!this.MarkedForDeletion?_ticketAssignments:null));
			info.AddValue("_alwaysFetchTicketAssignments", _alwaysFetchTicketAssignments);
			info.AddValue("_alreadyFetchedTicketAssignments", _alreadyFetchedTicketAssignments);
			info.AddValue("_transactionJournals", (!this.MarkedForDeletion?_transactionJournals:null));
			info.AddValue("_alwaysFetchTransactionJournals", _alwaysFetchTransactionJournals);
			info.AddValue("_alreadyFetchedTransactionJournals", _alreadyFetchedTransactionJournals);
			info.AddValue("_transactionJournals_", (!this.MarkedForDeletion?_transactionJournals_:null));
			info.AddValue("_alwaysFetchTransactionJournals_", _alwaysFetchTransactionJournals_);
			info.AddValue("_alreadyFetchedTransactionJournals_", _alreadyFetchedTransactionJournals_);
			info.AddValue("_tokenTransactionJournals", (!this.MarkedForDeletion?_tokenTransactionJournals:null));
			info.AddValue("_alwaysFetchTokenTransactionJournals", _alwaysFetchTokenTransactionJournals);
			info.AddValue("_alreadyFetchedTokenTransactionJournals", _alreadyFetchedTokenTransactionJournals);
			info.AddValue("_virtualCards", (!this.MarkedForDeletion?_virtualCards:null));
			info.AddValue("_alwaysFetchVirtualCards", _alwaysFetchVirtualCards);
			info.AddValue("_alreadyFetchedVirtualCards", _alreadyFetchedVirtualCards);
			info.AddValue("_vouchers", (!this.MarkedForDeletion?_vouchers:null));
			info.AddValue("_alwaysFetchVouchers", _alwaysFetchVouchers);
			info.AddValue("_alreadyFetchedVouchers", _alreadyFetchedVouchers);
			info.AddValue("_whitelists", (!this.MarkedForDeletion?_whitelists:null));
			info.AddValue("_alwaysFetchWhitelists", _alwaysFetchWhitelists);
			info.AddValue("_alreadyFetchedWhitelists", _alreadyFetchedWhitelists);
			info.AddValue("_whitelistJournals", (!this.MarkedForDeletion?_whitelistJournals:null));
			info.AddValue("_alwaysFetchWhitelistJournals", _alwaysFetchWhitelistJournals);
			info.AddValue("_alreadyFetchedWhitelistJournals", _alreadyFetchedWhitelistJournals);
			info.AddValue("_contracts", (!this.MarkedForDeletion?_contracts:null));
			info.AddValue("_alwaysFetchContracts", _alwaysFetchContracts);
			info.AddValue("_alreadyFetchedContracts", _alreadyFetchedContracts);
			info.AddValue("_stockTransferCollectionViaCardStockTransfer", (!this.MarkedForDeletion?_stockTransferCollectionViaCardStockTransfer:null));
			info.AddValue("_alwaysFetchStockTransferCollectionViaCardStockTransfer", _alwaysFetchStockTransferCollectionViaCardStockTransfer);
			info.AddValue("_alreadyFetchedStockTransferCollectionViaCardStockTransfer", _alreadyFetchedStockTransferCollectionViaCardStockTransfer);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_replacedCard", (!this.MarkedForDeletion?_replacedCard:null));
			info.AddValue("_replacedCardReturnsNewIfNotFound", _replacedCardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReplacedCard", _alwaysFetchReplacedCard);
			info.AddValue("_alreadyFetchedReplacedCard", _alreadyFetchedReplacedCard);
			info.AddValue("_cardHolder", (!this.MarkedForDeletion?_cardHolder:null));
			info.AddValue("_cardHolderReturnsNewIfNotFound", _cardHolderReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardHolder", _alwaysFetchCardHolder);
			info.AddValue("_alreadyFetchedCardHolder", _alreadyFetchedCardHolder);
			info.AddValue("_participant", (!this.MarkedForDeletion?_participant:null));
			info.AddValue("_participantReturnsNewIfNotFound", _participantReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParticipant", _alwaysFetchParticipant);
			info.AddValue("_alreadyFetchedParticipant", _alreadyFetchedParticipant);
			info.AddValue("_cardKey", (!this.MarkedForDeletion?_cardKey:null));
			info.AddValue("_cardKeyReturnsNewIfNotFound", _cardKeyReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardKey", _alwaysFetchCardKey);
			info.AddValue("_alreadyFetchedCardKey", _alreadyFetchedCardKey);
			info.AddValue("_cardPhysicalDetail", (!this.MarkedForDeletion?_cardPhysicalDetail:null));
			info.AddValue("_cardPhysicalDetailReturnsNewIfNotFound", _cardPhysicalDetailReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardPhysicalDetail", _alwaysFetchCardPhysicalDetail);
			info.AddValue("_alreadyFetchedCardPhysicalDetail", _alreadyFetchedCardPhysicalDetail);
			info.AddValue("_storageLocation", (!this.MarkedForDeletion?_storageLocation:null));
			info.AddValue("_storageLocationReturnsNewIfNotFound", _storageLocationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchStorageLocation", _alwaysFetchStorageLocation);
			info.AddValue("_alreadyFetchedStorageLocation", _alreadyFetchedStorageLocation);

			info.AddValue("_cardLinkTarget", (!this.MarkedForDeletion?_cardLinkTarget:null));
			info.AddValue("_cardLinkTargetReturnsNewIfNotFound", _cardLinkTargetReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardLinkTarget", _alwaysFetchCardLinkTarget);
			info.AddValue("_alreadyFetchedCardLinkTarget", _alreadyFetchedCardLinkTarget);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "ReplacedCard":
					_alreadyFetchedReplacedCard = true;
					this.ReplacedCard = (CardEntity)entity;
					break;
				case "CardHolder":
					_alreadyFetchedCardHolder = true;
					this.CardHolder = (CardHolderEntity)entity;
					break;
				case "Participant":
					_alreadyFetchedParticipant = true;
					this.Participant = (CardHolderEntity)entity;
					break;
				case "CardKey":
					_alreadyFetchedCardKey = true;
					this.CardKey = (CardKeyEntity)entity;
					break;
				case "CardPhysicalDetail":
					_alreadyFetchedCardPhysicalDetail = true;
					this.CardPhysicalDetail = (CardPhysicalDetailEntity)entity;
					break;
				case "StorageLocation":
					_alreadyFetchedStorageLocation = true;
					this.StorageLocation = (StorageLocationEntity)entity;
					break;
				case "ApportionmentResults":
					_alreadyFetchedApportionmentResults = true;
					if(entity!=null)
					{
						this.ApportionmentResults.Add((ApportionmentResultEntity)entity);
					}
					break;
				case "BadCards":
					_alreadyFetchedBadCards = true;
					if(entity!=null)
					{
						this.BadCards.Add((BadCardEntity)entity);
					}
					break;
				case "Bookings":
					_alreadyFetchedBookings = true;
					if(entity!=null)
					{
						this.Bookings.Add((BookingEntity)entity);
					}
					break;
				case "ReplacementCard":
					_alreadyFetchedReplacementCard = true;
					if(entity!=null)
					{
						this.ReplacementCard.Add((CardEntity)entity);
					}
					break;
				case "CardEvents":
					_alreadyFetchedCardEvents = true;
					if(entity!=null)
					{
						this.CardEvents.Add((CardEventEntity)entity);
					}
					break;
				case "CardLinksSource":
					_alreadyFetchedCardLinksSource = true;
					if(entity!=null)
					{
						this.CardLinksSource.Add((CardLinkEntity)entity);
					}
					break;
				case "CardStockTransfers":
					_alreadyFetchedCardStockTransfers = true;
					if(entity!=null)
					{
						this.CardStockTransfers.Add((CardStockTransferEntity)entity);
					}
					break;
				case "CardsToContracts":
					_alreadyFetchedCardsToContracts = true;
					if(entity!=null)
					{
						this.CardsToContracts.Add((CardToContractEntity)entity);
					}
					break;
				case "CardToRuleViolations":
					_alreadyFetchedCardToRuleViolations = true;
					if(entity!=null)
					{
						this.CardToRuleViolations.Add((CardToRuleViolationEntity)entity);
					}
					break;
				case "WorkItems":
					_alreadyFetchedWorkItems = true;
					if(entity!=null)
					{
						this.WorkItems.Add((CardWorkItemEntity)entity);
					}
					break;
				case "CreditCardAuthorizations":
					_alreadyFetchedCreditCardAuthorizations = true;
					if(entity!=null)
					{
						this.CreditCardAuthorizations.Add((CreditCardAuthorizationEntity)entity);
					}
					break;
				case "DormancyNotifications":
					_alreadyFetchedDormancyNotifications = true;
					if(entity!=null)
					{
						this.DormancyNotifications.Add((DormancyNotificationEntity)entity);
					}
					break;
				case "EmailEventResendLocks":
					_alreadyFetchedEmailEventResendLocks = true;
					if(entity!=null)
					{
						this.EmailEventResendLocks.Add((EmailEventResendLockEntity)entity);
					}
					break;
				case "Jobs":
					_alreadyFetchedJobs = true;
					if(entity!=null)
					{
						this.Jobs.Add((JobEntity)entity);
					}
					break;
				case "OrderDetails":
					_alreadyFetchedOrderDetails = true;
					if(entity!=null)
					{
						this.OrderDetails.Add((OrderDetailEntity)entity);
					}
					break;
				case "OrganizationParticipants":
					_alreadyFetchedOrganizationParticipants = true;
					if(entity!=null)
					{
						this.OrganizationParticipants.Add((OrganizationParticipantEntity)entity);
					}
					break;
				case "PaymentJournals":
					_alreadyFetchedPaymentJournals = true;
					if(entity!=null)
					{
						this.PaymentJournals.Add((PaymentJournalEntity)entity);
					}
					break;
				case "PaymentOptions":
					_alreadyFetchedPaymentOptions = true;
					if(entity!=null)
					{
						this.PaymentOptions.Add((PaymentOptionEntity)entity);
					}
					break;
				case "Products":
					_alreadyFetchedProducts = true;
					if(entity!=null)
					{
						this.Products.Add((ProductEntity)entity);
					}
					break;
				case "RuleViolations":
					_alreadyFetchedRuleViolations = true;
					if(entity!=null)
					{
						this.RuleViolations.Add((RuleViolationEntity)entity);
					}
					break;
				case "TicketAssignments":
					_alreadyFetchedTicketAssignments = true;
					if(entity!=null)
					{
						this.TicketAssignments.Add((TicketAssignmentEntity)entity);
					}
					break;
				case "TransactionJournals":
					_alreadyFetchedTransactionJournals = true;
					if(entity!=null)
					{
						this.TransactionJournals.Add((TransactionJournalEntity)entity);
					}
					break;
				case "TransactionJournals_":
					_alreadyFetchedTransactionJournals_ = true;
					if(entity!=null)
					{
						this.TransactionJournals_.Add((TransactionJournalEntity)entity);
					}
					break;
				case "TokenTransactionJournals":
					_alreadyFetchedTokenTransactionJournals = true;
					if(entity!=null)
					{
						this.TokenTransactionJournals.Add((TransactionJournalEntity)entity);
					}
					break;
				case "VirtualCards":
					_alreadyFetchedVirtualCards = true;
					if(entity!=null)
					{
						this.VirtualCards.Add((VirtualCardEntity)entity);
					}
					break;
				case "Vouchers":
					_alreadyFetchedVouchers = true;
					if(entity!=null)
					{
						this.Vouchers.Add((VoucherEntity)entity);
					}
					break;
				case "Whitelists":
					_alreadyFetchedWhitelists = true;
					if(entity!=null)
					{
						this.Whitelists.Add((WhitelistEntity)entity);
					}
					break;
				case "WhitelistJournals":
					_alreadyFetchedWhitelistJournals = true;
					if(entity!=null)
					{
						this.WhitelistJournals.Add((WhitelistJournalEntity)entity);
					}
					break;
				case "Contracts":
					_alreadyFetchedContracts = true;
					if(entity!=null)
					{
						this.Contracts.Add((ContractEntity)entity);
					}
					break;
				case "StockTransferCollectionViaCardStockTransfer":
					_alreadyFetchedStockTransferCollectionViaCardStockTransfer = true;
					if(entity!=null)
					{
						this.StockTransferCollectionViaCardStockTransfer.Add((StockTransferEntity)entity);
					}
					break;
				case "CardLinkTarget":
					_alreadyFetchedCardLinkTarget = true;
					this.CardLinkTarget = (CardLinkEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "ReplacedCard":
					SetupSyncReplacedCard(relatedEntity);
					break;
				case "CardHolder":
					SetupSyncCardHolder(relatedEntity);
					break;
				case "Participant":
					SetupSyncParticipant(relatedEntity);
					break;
				case "CardKey":
					SetupSyncCardKey(relatedEntity);
					break;
				case "CardPhysicalDetail":
					SetupSyncCardPhysicalDetail(relatedEntity);
					break;
				case "StorageLocation":
					SetupSyncStorageLocation(relatedEntity);
					break;
				case "ApportionmentResults":
					_apportionmentResults.Add((ApportionmentResultEntity)relatedEntity);
					break;
				case "BadCards":
					_badCards.Add((BadCardEntity)relatedEntity);
					break;
				case "Bookings":
					_bookings.Add((BookingEntity)relatedEntity);
					break;
				case "ReplacementCard":
					_replacementCard.Add((CardEntity)relatedEntity);
					break;
				case "CardEvents":
					_cardEvents.Add((CardEventEntity)relatedEntity);
					break;
				case "CardLinksSource":
					_cardLinksSource.Add((CardLinkEntity)relatedEntity);
					break;
				case "CardStockTransfers":
					_cardStockTransfers.Add((CardStockTransferEntity)relatedEntity);
					break;
				case "CardsToContracts":
					_cardsToContracts.Add((CardToContractEntity)relatedEntity);
					break;
				case "CardToRuleViolations":
					_cardToRuleViolations.Add((CardToRuleViolationEntity)relatedEntity);
					break;
				case "WorkItems":
					_workItems.Add((CardWorkItemEntity)relatedEntity);
					break;
				case "CreditCardAuthorizations":
					_creditCardAuthorizations.Add((CreditCardAuthorizationEntity)relatedEntity);
					break;
				case "DormancyNotifications":
					_dormancyNotifications.Add((DormancyNotificationEntity)relatedEntity);
					break;
				case "EmailEventResendLocks":
					_emailEventResendLocks.Add((EmailEventResendLockEntity)relatedEntity);
					break;
				case "Jobs":
					_jobs.Add((JobEntity)relatedEntity);
					break;
				case "OrderDetails":
					_orderDetails.Add((OrderDetailEntity)relatedEntity);
					break;
				case "OrganizationParticipants":
					_organizationParticipants.Add((OrganizationParticipantEntity)relatedEntity);
					break;
				case "PaymentJournals":
					_paymentJournals.Add((PaymentJournalEntity)relatedEntity);
					break;
				case "PaymentOptions":
					_paymentOptions.Add((PaymentOptionEntity)relatedEntity);
					break;
				case "Products":
					_products.Add((ProductEntity)relatedEntity);
					break;
				case "RuleViolations":
					_ruleViolations.Add((RuleViolationEntity)relatedEntity);
					break;
				case "TicketAssignments":
					_ticketAssignments.Add((TicketAssignmentEntity)relatedEntity);
					break;
				case "TransactionJournals":
					_transactionJournals.Add((TransactionJournalEntity)relatedEntity);
					break;
				case "TransactionJournals_":
					_transactionJournals_.Add((TransactionJournalEntity)relatedEntity);
					break;
				case "TokenTransactionJournals":
					_tokenTransactionJournals.Add((TransactionJournalEntity)relatedEntity);
					break;
				case "VirtualCards":
					_virtualCards.Add((VirtualCardEntity)relatedEntity);
					break;
				case "Vouchers":
					_vouchers.Add((VoucherEntity)relatedEntity);
					break;
				case "Whitelists":
					_whitelists.Add((WhitelistEntity)relatedEntity);
					break;
				case "WhitelistJournals":
					_whitelistJournals.Add((WhitelistJournalEntity)relatedEntity);
					break;
				case "CardLinkTarget":
					SetupSyncCardLinkTarget(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "ReplacedCard":
					DesetupSyncReplacedCard(false, true);
					break;
				case "CardHolder":
					DesetupSyncCardHolder(false, true);
					break;
				case "Participant":
					DesetupSyncParticipant(false, true);
					break;
				case "CardKey":
					DesetupSyncCardKey(false, true);
					break;
				case "CardPhysicalDetail":
					DesetupSyncCardPhysicalDetail(false, true);
					break;
				case "StorageLocation":
					DesetupSyncStorageLocation(false, true);
					break;
				case "ApportionmentResults":
					this.PerformRelatedEntityRemoval(_apportionmentResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BadCards":
					this.PerformRelatedEntityRemoval(_badCards, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Bookings":
					this.PerformRelatedEntityRemoval(_bookings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReplacementCard":
					this.PerformRelatedEntityRemoval(_replacementCard, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardEvents":
					this.PerformRelatedEntityRemoval(_cardEvents, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardLinksSource":
					this.PerformRelatedEntityRemoval(_cardLinksSource, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardStockTransfers":
					this.PerformRelatedEntityRemoval(_cardStockTransfers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardsToContracts":
					this.PerformRelatedEntityRemoval(_cardsToContracts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardToRuleViolations":
					this.PerformRelatedEntityRemoval(_cardToRuleViolations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "WorkItems":
					this.PerformRelatedEntityRemoval(_workItems, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CreditCardAuthorizations":
					this.PerformRelatedEntityRemoval(_creditCardAuthorizations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DormancyNotifications":
					this.PerformRelatedEntityRemoval(_dormancyNotifications, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EmailEventResendLocks":
					this.PerformRelatedEntityRemoval(_emailEventResendLocks, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Jobs":
					this.PerformRelatedEntityRemoval(_jobs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderDetails":
					this.PerformRelatedEntityRemoval(_orderDetails, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrganizationParticipants":
					this.PerformRelatedEntityRemoval(_organizationParticipants, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentJournals":
					this.PerformRelatedEntityRemoval(_paymentJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentOptions":
					this.PerformRelatedEntityRemoval(_paymentOptions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Products":
					this.PerformRelatedEntityRemoval(_products, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RuleViolations":
					this.PerformRelatedEntityRemoval(_ruleViolations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketAssignments":
					this.PerformRelatedEntityRemoval(_ticketAssignments, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournals":
					this.PerformRelatedEntityRemoval(_transactionJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournals_":
					this.PerformRelatedEntityRemoval(_transactionJournals_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TokenTransactionJournals":
					this.PerformRelatedEntityRemoval(_tokenTransactionJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VirtualCards":
					this.PerformRelatedEntityRemoval(_virtualCards, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Vouchers":
					this.PerformRelatedEntityRemoval(_vouchers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Whitelists":
					this.PerformRelatedEntityRemoval(_whitelists, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "WhitelistJournals":
					this.PerformRelatedEntityRemoval(_whitelistJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardLinkTarget":
					DesetupSyncCardLinkTarget(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cardLinkTarget!=null)
			{
				toReturn.Add(_cardLinkTarget);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_replacedCard!=null)
			{
				toReturn.Add(_replacedCard);
			}
			if(_cardHolder!=null)
			{
				toReturn.Add(_cardHolder);
			}
			if(_participant!=null)
			{
				toReturn.Add(_participant);
			}
			if(_cardKey!=null)
			{
				toReturn.Add(_cardKey);
			}
			if(_cardPhysicalDetail!=null)
			{
				toReturn.Add(_cardPhysicalDetail);
			}
			if(_storageLocation!=null)
			{
				toReturn.Add(_storageLocation);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_apportionmentResults);
			toReturn.Add(_badCards);
			toReturn.Add(_bookings);
			toReturn.Add(_replacementCard);
			toReturn.Add(_cardEvents);
			toReturn.Add(_cardLinksSource);
			toReturn.Add(_cardStockTransfers);
			toReturn.Add(_cardsToContracts);
			toReturn.Add(_cardToRuleViolations);
			toReturn.Add(_workItems);
			toReturn.Add(_creditCardAuthorizations);
			toReturn.Add(_dormancyNotifications);
			toReturn.Add(_emailEventResendLocks);
			toReturn.Add(_jobs);
			toReturn.Add(_orderDetails);
			toReturn.Add(_organizationParticipants);
			toReturn.Add(_paymentJournals);
			toReturn.Add(_paymentOptions);
			toReturn.Add(_products);
			toReturn.Add(_ruleViolations);
			toReturn.Add(_ticketAssignments);
			toReturn.Add(_transactionJournals);
			toReturn.Add(_transactionJournals_);
			toReturn.Add(_tokenTransactionJournals);
			toReturn.Add(_virtualCards);
			toReturn.Add(_vouchers);
			toReturn.Add(_whitelists);
			toReturn.Add(_whitelistJournals);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardID">PK value for Card which data should be fetched into this Card object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardID)
		{
			return FetchUsingPK(cardID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardID">PK value for Card which data should be fetched into this Card object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cardID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardID">PK value for Card which data should be fetched into this Card object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cardID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardID">PK value for Card which data should be fetched into this Card object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cardID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CardID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CardRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ApportionmentResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults(bool forceFetch)
		{
			return GetMultiApportionmentResults(forceFetch, _apportionmentResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ApportionmentResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiApportionmentResults(forceFetch, _apportionmentResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiApportionmentResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ApportionmentResultCollection GetMultiApportionmentResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedApportionmentResults || forceFetch || _alwaysFetchApportionmentResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_apportionmentResults);
				_apportionmentResults.SuppressClearInGetMulti=!forceFetch;
				_apportionmentResults.EntityFactoryToUse = entityFactoryToUse;
				_apportionmentResults.GetMultiManyToOne(null, null, null, null, null, this, filter);
				_apportionmentResults.SuppressClearInGetMulti=false;
				_alreadyFetchedApportionmentResults = true;
			}
			return _apportionmentResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ApportionmentResults'. These settings will be taken into account
		/// when the property ApportionmentResults is requested or GetMultiApportionmentResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersApportionmentResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_apportionmentResults.SortClauses=sortClauses;
			_apportionmentResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BadCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BadCardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BadCardCollection GetMultiBadCards(bool forceFetch)
		{
			return GetMultiBadCards(forceFetch, _badCards.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BadCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BadCardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BadCardCollection GetMultiBadCards(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBadCards(forceFetch, _badCards.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BadCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BadCardCollection GetMultiBadCards(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBadCards(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BadCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BadCardCollection GetMultiBadCards(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBadCards || forceFetch || _alwaysFetchBadCards) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_badCards);
				_badCards.SuppressClearInGetMulti=!forceFetch;
				_badCards.EntityFactoryToUse = entityFactoryToUse;
				_badCards.GetMultiManyToOne(this, filter);
				_badCards.SuppressClearInGetMulti=false;
				_alreadyFetchedBadCards = true;
			}
			return _badCards;
		}

		/// <summary> Sets the collection parameters for the collection for 'BadCards'. These settings will be taken into account
		/// when the property BadCards is requested or GetMultiBadCards is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBadCards(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_badCards.SortClauses=sortClauses;
			_badCards.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BookingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BookingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BookingCollection GetMultiBookings(bool forceFetch)
		{
			return GetMultiBookings(forceFetch, _bookings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BookingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BookingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BookingCollection GetMultiBookings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBookings(forceFetch, _bookings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BookingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BookingCollection GetMultiBookings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBookings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BookingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BookingCollection GetMultiBookings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBookings || forceFetch || _alwaysFetchBookings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_bookings);
				_bookings.SuppressClearInGetMulti=!forceFetch;
				_bookings.EntityFactoryToUse = entityFactoryToUse;
				_bookings.GetMultiManyToOne(this, filter);
				_bookings.SuppressClearInGetMulti=false;
				_alreadyFetchedBookings = true;
			}
			return _bookings;
		}

		/// <summary> Sets the collection parameters for the collection for 'Bookings'. These settings will be taken into account
		/// when the property Bookings is requested or GetMultiBookings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBookings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_bookings.SortClauses=sortClauses;
			_bookings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiReplacementCard(bool forceFetch)
		{
			return GetMultiReplacementCard(forceFetch, _replacementCard.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiReplacementCard(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReplacementCard(forceFetch, _replacementCard.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiReplacementCard(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReplacementCard(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection GetMultiReplacementCard(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReplacementCard || forceFetch || _alwaysFetchReplacementCard) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_replacementCard);
				_replacementCard.SuppressClearInGetMulti=!forceFetch;
				_replacementCard.EntityFactoryToUse = entityFactoryToUse;
				_replacementCard.GetMultiManyToOne(null, this, null, null, null, null, null, filter);
				_replacementCard.SuppressClearInGetMulti=false;
				_alreadyFetchedReplacementCard = true;
			}
			return _replacementCard;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReplacementCard'. These settings will be taken into account
		/// when the property ReplacementCard is requested or GetMultiReplacementCard is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReplacementCard(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_replacementCard.SortClauses=sortClauses;
			_replacementCard.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardEventEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardEventEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardEventCollection GetMultiCardEvents(bool forceFetch)
		{
			return GetMultiCardEvents(forceFetch, _cardEvents.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEventEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardEventEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardEventCollection GetMultiCardEvents(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardEvents(forceFetch, _cardEvents.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardEventEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardEventCollection GetMultiCardEvents(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardEvents(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEventEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardEventCollection GetMultiCardEvents(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardEvents || forceFetch || _alwaysFetchCardEvents) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardEvents);
				_cardEvents.SuppressClearInGetMulti=!forceFetch;
				_cardEvents.EntityFactoryToUse = entityFactoryToUse;
				_cardEvents.GetMultiManyToOne(this, null, filter);
				_cardEvents.SuppressClearInGetMulti=false;
				_alreadyFetchedCardEvents = true;
			}
			return _cardEvents;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardEvents'. These settings will be taken into account
		/// when the property CardEvents is requested or GetMultiCardEvents is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardEvents(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardEvents.SortClauses=sortClauses;
			_cardEvents.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardLinkEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardLinkEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardLinkCollection GetMultiCardLinksSource(bool forceFetch)
		{
			return GetMultiCardLinksSource(forceFetch, _cardLinksSource.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardLinkEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardLinkEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardLinkCollection GetMultiCardLinksSource(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardLinksSource(forceFetch, _cardLinksSource.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardLinkEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardLinkCollection GetMultiCardLinksSource(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardLinksSource(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardLinkEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardLinkCollection GetMultiCardLinksSource(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardLinksSource || forceFetch || _alwaysFetchCardLinksSource) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardLinksSource);
				_cardLinksSource.SuppressClearInGetMulti=!forceFetch;
				_cardLinksSource.EntityFactoryToUse = entityFactoryToUse;
				_cardLinksSource.GetMultiManyToOne(this, filter);
				_cardLinksSource.SuppressClearInGetMulti=false;
				_alreadyFetchedCardLinksSource = true;
			}
			return _cardLinksSource;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardLinksSource'. These settings will be taken into account
		/// when the property CardLinksSource is requested or GetMultiCardLinksSource is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardLinksSource(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardLinksSource.SortClauses=sortClauses;
			_cardLinksSource.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardStockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardStockTransferEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardStockTransferCollection GetMultiCardStockTransfers(bool forceFetch)
		{
			return GetMultiCardStockTransfers(forceFetch, _cardStockTransfers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardStockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardStockTransferEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardStockTransferCollection GetMultiCardStockTransfers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardStockTransfers(forceFetch, _cardStockTransfers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardStockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardStockTransferCollection GetMultiCardStockTransfers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardStockTransfers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardStockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardStockTransferCollection GetMultiCardStockTransfers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardStockTransfers || forceFetch || _alwaysFetchCardStockTransfers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardStockTransfers);
				_cardStockTransfers.SuppressClearInGetMulti=!forceFetch;
				_cardStockTransfers.EntityFactoryToUse = entityFactoryToUse;
				_cardStockTransfers.GetMultiManyToOne(this, null, filter);
				_cardStockTransfers.SuppressClearInGetMulti=false;
				_alreadyFetchedCardStockTransfers = true;
			}
			return _cardStockTransfers;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardStockTransfers'. These settings will be taken into account
		/// when the property CardStockTransfers is requested or GetMultiCardStockTransfers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardStockTransfers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardStockTransfers.SortClauses=sortClauses;
			_cardStockTransfers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardToContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardToContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardToContractCollection GetMultiCardsToContracts(bool forceFetch)
		{
			return GetMultiCardsToContracts(forceFetch, _cardsToContracts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardToContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardToContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardToContractCollection GetMultiCardsToContracts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardsToContracts(forceFetch, _cardsToContracts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardToContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardToContractCollection GetMultiCardsToContracts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardsToContracts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardToContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardToContractCollection GetMultiCardsToContracts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardsToContracts || forceFetch || _alwaysFetchCardsToContracts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardsToContracts);
				_cardsToContracts.SuppressClearInGetMulti=!forceFetch;
				_cardsToContracts.EntityFactoryToUse = entityFactoryToUse;
				_cardsToContracts.GetMultiManyToOne(this, null, filter);
				_cardsToContracts.SuppressClearInGetMulti=false;
				_alreadyFetchedCardsToContracts = true;
			}
			return _cardsToContracts;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardsToContracts'. These settings will be taken into account
		/// when the property CardsToContracts is requested or GetMultiCardsToContracts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardsToContracts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardsToContracts.SortClauses=sortClauses;
			_cardsToContracts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardToRuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardToRuleViolationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardToRuleViolationCollection GetMultiCardToRuleViolations(bool forceFetch)
		{
			return GetMultiCardToRuleViolations(forceFetch, _cardToRuleViolations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardToRuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardToRuleViolationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardToRuleViolationCollection GetMultiCardToRuleViolations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardToRuleViolations(forceFetch, _cardToRuleViolations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardToRuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardToRuleViolationCollection GetMultiCardToRuleViolations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardToRuleViolations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardToRuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardToRuleViolationCollection GetMultiCardToRuleViolations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardToRuleViolations || forceFetch || _alwaysFetchCardToRuleViolations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardToRuleViolations);
				_cardToRuleViolations.SuppressClearInGetMulti=!forceFetch;
				_cardToRuleViolations.EntityFactoryToUse = entityFactoryToUse;
				_cardToRuleViolations.GetMultiManyToOne(this, filter);
				_cardToRuleViolations.SuppressClearInGetMulti=false;
				_alreadyFetchedCardToRuleViolations = true;
			}
			return _cardToRuleViolations;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardToRuleViolations'. These settings will be taken into account
		/// when the property CardToRuleViolations is requested or GetMultiCardToRuleViolations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardToRuleViolations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardToRuleViolations.SortClauses=sortClauses;
			_cardToRuleViolations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardWorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardWorkItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardWorkItemCollection GetMultiWorkItems(bool forceFetch)
		{
			return GetMultiWorkItems(forceFetch, _workItems.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardWorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardWorkItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardWorkItemCollection GetMultiWorkItems(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiWorkItems(forceFetch, _workItems.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardWorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardWorkItemCollection GetMultiWorkItems(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiWorkItems(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardWorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardWorkItemCollection GetMultiWorkItems(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedWorkItems || forceFetch || _alwaysFetchWorkItems) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_workItems);
				_workItems.SuppressClearInGetMulti=!forceFetch;
				_workItems.EntityFactoryToUse = entityFactoryToUse;
				_workItems.GetMultiManyToOne(null, this, null, filter);
				_workItems.SuppressClearInGetMulti=false;
				_alreadyFetchedWorkItems = true;
			}
			return _workItems;
		}

		/// <summary> Sets the collection parameters for the collection for 'WorkItems'. These settings will be taken into account
		/// when the property WorkItems is requested or GetMultiWorkItems is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWorkItems(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_workItems.SortClauses=sortClauses;
			_workItems.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CreditCardAuthorizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection GetMultiCreditCardAuthorizations(bool forceFetch)
		{
			return GetMultiCreditCardAuthorizations(forceFetch, _creditCardAuthorizations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CreditCardAuthorizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection GetMultiCreditCardAuthorizations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCreditCardAuthorizations(forceFetch, _creditCardAuthorizations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection GetMultiCreditCardAuthorizations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCreditCardAuthorizations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection GetMultiCreditCardAuthorizations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCreditCardAuthorizations || forceFetch || _alwaysFetchCreditCardAuthorizations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_creditCardAuthorizations);
				_creditCardAuthorizations.SuppressClearInGetMulti=!forceFetch;
				_creditCardAuthorizations.EntityFactoryToUse = entityFactoryToUse;
				_creditCardAuthorizations.GetMultiManyToOne(this, null, filter);
				_creditCardAuthorizations.SuppressClearInGetMulti=false;
				_alreadyFetchedCreditCardAuthorizations = true;
			}
			return _creditCardAuthorizations;
		}

		/// <summary> Sets the collection parameters for the collection for 'CreditCardAuthorizations'. These settings will be taken into account
		/// when the property CreditCardAuthorizations is requested or GetMultiCreditCardAuthorizations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCreditCardAuthorizations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_creditCardAuthorizations.SortClauses=sortClauses;
			_creditCardAuthorizations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DormancyNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DormancyNotificationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DormancyNotificationCollection GetMultiDormancyNotifications(bool forceFetch)
		{
			return GetMultiDormancyNotifications(forceFetch, _dormancyNotifications.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DormancyNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DormancyNotificationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DormancyNotificationCollection GetMultiDormancyNotifications(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDormancyNotifications(forceFetch, _dormancyNotifications.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DormancyNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DormancyNotificationCollection GetMultiDormancyNotifications(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDormancyNotifications(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DormancyNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DormancyNotificationCollection GetMultiDormancyNotifications(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDormancyNotifications || forceFetch || _alwaysFetchDormancyNotifications) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dormancyNotifications);
				_dormancyNotifications.SuppressClearInGetMulti=!forceFetch;
				_dormancyNotifications.EntityFactoryToUse = entityFactoryToUse;
				_dormancyNotifications.GetMultiManyToOne(this, filter);
				_dormancyNotifications.SuppressClearInGetMulti=false;
				_alreadyFetchedDormancyNotifications = true;
			}
			return _dormancyNotifications;
		}

		/// <summary> Sets the collection parameters for the collection for 'DormancyNotifications'. These settings will be taken into account
		/// when the property DormancyNotifications is requested or GetMultiDormancyNotifications is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDormancyNotifications(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dormancyNotifications.SortClauses=sortClauses;
			_dormancyNotifications.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EmailEventResendLockEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch)
		{
			return GetMultiEmailEventResendLocks(forceFetch, _emailEventResendLocks.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EmailEventResendLockEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEmailEventResendLocks(forceFetch, _emailEventResendLocks.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEmailEventResendLocks(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEmailEventResendLocks || forceFetch || _alwaysFetchEmailEventResendLocks) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_emailEventResendLocks);
				_emailEventResendLocks.SuppressClearInGetMulti=!forceFetch;
				_emailEventResendLocks.EntityFactoryToUse = entityFactoryToUse;
				_emailEventResendLocks.GetMultiManyToOne(null, null, this, null, null, filter);
				_emailEventResendLocks.SuppressClearInGetMulti=false;
				_alreadyFetchedEmailEventResendLocks = true;
			}
			return _emailEventResendLocks;
		}

		/// <summary> Sets the collection parameters for the collection for 'EmailEventResendLocks'. These settings will be taken into account
		/// when the property EmailEventResendLocks is requested or GetMultiEmailEventResendLocks is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEmailEventResendLocks(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_emailEventResendLocks.SortClauses=sortClauses;
			_emailEventResendLocks.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'JobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch)
		{
			return GetMultiJobs(forceFetch, _jobs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'JobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiJobs(forceFetch, _jobs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiJobs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedJobs || forceFetch || _alwaysFetchJobs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_jobs);
				_jobs.SuppressClearInGetMulti=!forceFetch;
				_jobs.EntityFactoryToUse = entityFactoryToUse;
				_jobs.GetMultiManyToOne(null, this, null, null, null, null, null, null, filter);
				_jobs.SuppressClearInGetMulti=false;
				_alreadyFetchedJobs = true;
			}
			return _jobs;
		}

		/// <summary> Sets the collection parameters for the collection for 'Jobs'. These settings will be taken into account
		/// when the property Jobs is requested or GetMultiJobs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersJobs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_jobs.SortClauses=sortClauses;
			_jobs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch)
		{
			return GetMultiOrderDetails(forceFetch, _orderDetails.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderDetails(forceFetch, _orderDetails.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderDetails(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderDetails || forceFetch || _alwaysFetchOrderDetails) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderDetails);
				_orderDetails.SuppressClearInGetMulti=!forceFetch;
				_orderDetails.EntityFactoryToUse = entityFactoryToUse;
				_orderDetails.GetMultiManyToOne(this, null, null, null, null, null, filter);
				_orderDetails.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderDetails = true;
			}
			return _orderDetails;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderDetails'. These settings will be taken into account
		/// when the property OrderDetails is requested or GetMultiOrderDetails is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderDetails(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderDetails.SortClauses=sortClauses;
			_orderDetails.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationParticipantEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection GetMultiOrganizationParticipants(bool forceFetch)
		{
			return GetMultiOrganizationParticipants(forceFetch, _organizationParticipants.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationParticipantEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection GetMultiOrganizationParticipants(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrganizationParticipants(forceFetch, _organizationParticipants.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection GetMultiOrganizationParticipants(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrganizationParticipants(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection GetMultiOrganizationParticipants(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrganizationParticipants || forceFetch || _alwaysFetchOrganizationParticipants) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_organizationParticipants);
				_organizationParticipants.SuppressClearInGetMulti=!forceFetch;
				_organizationParticipants.EntityFactoryToUse = entityFactoryToUse;
				_organizationParticipants.GetMultiManyToOne(this, null, null, null, filter);
				_organizationParticipants.SuppressClearInGetMulti=false;
				_alreadyFetchedOrganizationParticipants = true;
			}
			return _organizationParticipants;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrganizationParticipants'. These settings will be taken into account
		/// when the property OrganizationParticipants is requested or GetMultiOrganizationParticipants is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrganizationParticipants(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_organizationParticipants.SortClauses=sortClauses;
			_organizationParticipants.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch)
		{
			return GetMultiPaymentJournals(forceFetch, _paymentJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentJournals(forceFetch, _paymentJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentJournals || forceFetch || _alwaysFetchPaymentJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentJournals);
				_paymentJournals.SuppressClearInGetMulti=!forceFetch;
				_paymentJournals.EntityFactoryToUse = entityFactoryToUse;
				_paymentJournals.GetMultiManyToOne(this, null, null, null, null, null, filter);
				_paymentJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentJournals = true;
			}
			return _paymentJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentJournals'. These settings will be taken into account
		/// when the property PaymentJournals is requested or GetMultiPaymentJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentJournals.SortClauses=sortClauses;
			_paymentJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentOptionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentOptionCollection GetMultiPaymentOptions(bool forceFetch)
		{
			return GetMultiPaymentOptions(forceFetch, _paymentOptions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentOptionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentOptionCollection GetMultiPaymentOptions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentOptions(forceFetch, _paymentOptions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentOptionCollection GetMultiPaymentOptions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentOptions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentOptionCollection GetMultiPaymentOptions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentOptions || forceFetch || _alwaysFetchPaymentOptions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentOptions);
				_paymentOptions.SuppressClearInGetMulti=!forceFetch;
				_paymentOptions.EntityFactoryToUse = entityFactoryToUse;
				_paymentOptions.GetMultiManyToOne(this, null, filter);
				_paymentOptions.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentOptions = true;
			}
			return _paymentOptions;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentOptions'. These settings will be taken into account
		/// when the property PaymentOptions is requested or GetMultiPaymentOptions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentOptions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentOptions.SortClauses=sortClauses;
			_paymentOptions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProducts || forceFetch || _alwaysFetchProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_products);
				_products.SuppressClearInGetMulti=!forceFetch;
				_products.EntityFactoryToUse = entityFactoryToUse;
				_products.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, filter);
				_products.SuppressClearInGetMulti=false;
				_alreadyFetchedProducts = true;
			}
			return _products;
		}

		/// <summary> Sets the collection parameters for the collection for 'Products'. These settings will be taken into account
		/// when the property Products is requested or GetMultiProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_products.SortClauses=sortClauses;
			_products.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationCollection GetMultiRuleViolations(bool forceFetch)
		{
			return GetMultiRuleViolations(forceFetch, _ruleViolations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationCollection GetMultiRuleViolations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleViolations(forceFetch, _ruleViolations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationCollection GetMultiRuleViolations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleViolations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationCollection GetMultiRuleViolations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleViolations || forceFetch || _alwaysFetchRuleViolations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleViolations);
				_ruleViolations.SuppressClearInGetMulti=!forceFetch;
				_ruleViolations.EntityFactoryToUse = entityFactoryToUse;
				_ruleViolations.GetMultiManyToOne(this, null, null, filter);
				_ruleViolations.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleViolations = true;
			}
			return _ruleViolations;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleViolations'. These settings will be taken into account
		/// when the property RuleViolations is requested or GetMultiRuleViolations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleViolations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleViolations.SortClauses=sortClauses;
			_ruleViolations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketAssignmentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch)
		{
			return GetMultiTicketAssignments(forceFetch, _ticketAssignments.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketAssignmentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketAssignments(forceFetch, _ticketAssignments.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketAssignments(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketAssignments || forceFetch || _alwaysFetchTicketAssignments) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketAssignments);
				_ticketAssignments.SuppressClearInGetMulti=!forceFetch;
				_ticketAssignments.EntityFactoryToUse = entityFactoryToUse;
				_ticketAssignments.GetMultiManyToOne(null, this, null, null, null, filter);
				_ticketAssignments.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketAssignments = true;
			}
			return _ticketAssignments;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketAssignments'. These settings will be taken into account
		/// when the property TicketAssignments is requested or GetMultiTicketAssignments is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketAssignments(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketAssignments.SortClauses=sortClauses;
			_ticketAssignments.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournals || forceFetch || _alwaysFetchTransactionJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournals);
				_transactionJournals.SuppressClearInGetMulti=!forceFetch;
				_transactionJournals.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournals.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, null, null, null, null, filter);
				_transactionJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournals = true;
			}
			return _transactionJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournals'. These settings will be taken into account
		/// when the property TransactionJournals is requested or GetMultiTransactionJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournals.SortClauses=sortClauses;
			_transactionJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals_(bool forceFetch)
		{
			return GetMultiTransactionJournals_(forceFetch, _transactionJournals_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournals_(forceFetch, _transactionJournals_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournals_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournals_ || forceFetch || _alwaysFetchTransactionJournals_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournals_);
				_transactionJournals_.SuppressClearInGetMulti=!forceFetch;
				_transactionJournals_.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournals_.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, null, null, null, null, null, filter);
				_transactionJournals_.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournals_ = true;
			}
			return _transactionJournals_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournals_'. These settings will be taken into account
		/// when the property TransactionJournals_ is requested or GetMultiTransactionJournals_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournals_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournals_.SortClauses=sortClauses;
			_transactionJournals_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTokenTransactionJournals(bool forceFetch)
		{
			return GetMultiTokenTransactionJournals(forceFetch, _tokenTransactionJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTokenTransactionJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTokenTransactionJournals(forceFetch, _tokenTransactionJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTokenTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTokenTransactionJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTokenTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTokenTransactionJournals || forceFetch || _alwaysFetchTokenTransactionJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tokenTransactionJournals);
				_tokenTransactionJournals.SuppressClearInGetMulti=!forceFetch;
				_tokenTransactionJournals.EntityFactoryToUse = entityFactoryToUse;
				_tokenTransactionJournals.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, null, null, null, null, null, filter);
				_tokenTransactionJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedTokenTransactionJournals = true;
			}
			return _tokenTransactionJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'TokenTransactionJournals'. These settings will be taken into account
		/// when the property TokenTransactionJournals is requested or GetMultiTokenTransactionJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTokenTransactionJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tokenTransactionJournals.SortClauses=sortClauses;
			_tokenTransactionJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VirtualCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VirtualCardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VirtualCardCollection GetMultiVirtualCards(bool forceFetch)
		{
			return GetMultiVirtualCards(forceFetch, _virtualCards.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VirtualCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VirtualCardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VirtualCardCollection GetMultiVirtualCards(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVirtualCards(forceFetch, _virtualCards.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VirtualCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VirtualCardCollection GetMultiVirtualCards(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVirtualCards(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VirtualCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VirtualCardCollection GetMultiVirtualCards(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVirtualCards || forceFetch || _alwaysFetchVirtualCards) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_virtualCards);
				_virtualCards.SuppressClearInGetMulti=!forceFetch;
				_virtualCards.EntityFactoryToUse = entityFactoryToUse;
				_virtualCards.GetMultiManyToOne(this, null, filter);
				_virtualCards.SuppressClearInGetMulti=false;
				_alreadyFetchedVirtualCards = true;
			}
			return _virtualCards;
		}

		/// <summary> Sets the collection parameters for the collection for 'VirtualCards'. These settings will be taken into account
		/// when the property VirtualCards is requested or GetMultiVirtualCards is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVirtualCards(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_virtualCards.SortClauses=sortClauses;
			_virtualCards.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VoucherEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VoucherEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VoucherCollection GetMultiVouchers(bool forceFetch)
		{
			return GetMultiVouchers(forceFetch, _vouchers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VoucherEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VoucherEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VoucherCollection GetMultiVouchers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVouchers(forceFetch, _vouchers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VoucherEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VoucherCollection GetMultiVouchers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVouchers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VoucherEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VoucherCollection GetMultiVouchers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVouchers || forceFetch || _alwaysFetchVouchers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vouchers);
				_vouchers.SuppressClearInGetMulti=!forceFetch;
				_vouchers.EntityFactoryToUse = entityFactoryToUse;
				_vouchers.GetMultiManyToOne(null, this, filter);
				_vouchers.SuppressClearInGetMulti=false;
				_alreadyFetchedVouchers = true;
			}
			return _vouchers;
		}

		/// <summary> Sets the collection parameters for the collection for 'Vouchers'. These settings will be taken into account
		/// when the property Vouchers is requested or GetMultiVouchers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVouchers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vouchers.SortClauses=sortClauses;
			_vouchers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'WhitelistEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'WhitelistEntity'</returns>
		public VarioSL.Entities.CollectionClasses.WhitelistCollection GetMultiWhitelists(bool forceFetch)
		{
			return GetMultiWhitelists(forceFetch, _whitelists.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WhitelistEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'WhitelistEntity'</returns>
		public VarioSL.Entities.CollectionClasses.WhitelistCollection GetMultiWhitelists(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiWhitelists(forceFetch, _whitelists.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'WhitelistEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.WhitelistCollection GetMultiWhitelists(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiWhitelists(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WhitelistEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.WhitelistCollection GetMultiWhitelists(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedWhitelists || forceFetch || _alwaysFetchWhitelists) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_whitelists);
				_whitelists.SuppressClearInGetMulti=!forceFetch;
				_whitelists.EntityFactoryToUse = entityFactoryToUse;
				_whitelists.GetMultiManyToOne(this, filter);
				_whitelists.SuppressClearInGetMulti=false;
				_alreadyFetchedWhitelists = true;
			}
			return _whitelists;
		}

		/// <summary> Sets the collection parameters for the collection for 'Whitelists'. These settings will be taken into account
		/// when the property Whitelists is requested or GetMultiWhitelists is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWhitelists(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_whitelists.SortClauses=sortClauses;
			_whitelists.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'WhitelistJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'WhitelistJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.WhitelistJournalCollection GetMultiWhitelistJournals(bool forceFetch)
		{
			return GetMultiWhitelistJournals(forceFetch, _whitelistJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WhitelistJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'WhitelistJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.WhitelistJournalCollection GetMultiWhitelistJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiWhitelistJournals(forceFetch, _whitelistJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'WhitelistJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.WhitelistJournalCollection GetMultiWhitelistJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiWhitelistJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WhitelistJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.WhitelistJournalCollection GetMultiWhitelistJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedWhitelistJournals || forceFetch || _alwaysFetchWhitelistJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_whitelistJournals);
				_whitelistJournals.SuppressClearInGetMulti=!forceFetch;
				_whitelistJournals.EntityFactoryToUse = entityFactoryToUse;
				_whitelistJournals.GetMultiManyToOne(this, filter);
				_whitelistJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedWhitelistJournals = true;
			}
			return _whitelistJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'WhitelistJournals'. These settings will be taken into account
		/// when the property WhitelistJournals is requested or GetMultiWhitelistJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWhitelistJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_whitelistJournals.SortClauses=sortClauses;
			_whitelistJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch)
		{
			return GetMultiContracts(forceFetch, _contracts.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedContracts || forceFetch || _alwaysFetchContracts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contracts);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CardFields.CardID, ComparisonOperator.Equal, this.CardID, "CardEntity__"));
				_contracts.SuppressClearInGetMulti=!forceFetch;
				_contracts.EntityFactoryToUse = entityFactoryToUse;
				_contracts.GetMulti(filter, GetRelationsForField("Contracts"));
				_contracts.SuppressClearInGetMulti=false;
				_alreadyFetchedContracts = true;
			}
			return _contracts;
		}

		/// <summary> Sets the collection parameters for the collection for 'Contracts'. These settings will be taken into account
		/// when the property Contracts is requested or GetMultiContracts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContracts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contracts.SortClauses=sortClauses;
			_contracts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'StockTransferEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StockTransferCollection GetMultiStockTransferCollectionViaCardStockTransfer(bool forceFetch)
		{
			return GetMultiStockTransferCollectionViaCardStockTransfer(forceFetch, _stockTransferCollectionViaCardStockTransfer.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.StockTransferCollection GetMultiStockTransferCollectionViaCardStockTransfer(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedStockTransferCollectionViaCardStockTransfer || forceFetch || _alwaysFetchStockTransferCollectionViaCardStockTransfer) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_stockTransferCollectionViaCardStockTransfer);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CardFields.CardID, ComparisonOperator.Equal, this.CardID, "CardEntity__"));
				_stockTransferCollectionViaCardStockTransfer.SuppressClearInGetMulti=!forceFetch;
				_stockTransferCollectionViaCardStockTransfer.EntityFactoryToUse = entityFactoryToUse;
				_stockTransferCollectionViaCardStockTransfer.GetMulti(filter, GetRelationsForField("StockTransferCollectionViaCardStockTransfer"));
				_stockTransferCollectionViaCardStockTransfer.SuppressClearInGetMulti=false;
				_alreadyFetchedStockTransferCollectionViaCardStockTransfer = true;
			}
			return _stockTransferCollectionViaCardStockTransfer;
		}

		/// <summary> Sets the collection parameters for the collection for 'StockTransferCollectionViaCardStockTransfer'. These settings will be taken into account
		/// when the property StockTransferCollectionViaCardStockTransfer is requested or GetMultiStockTransferCollectionViaCardStockTransfer is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersStockTransferCollectionViaCardStockTransfer(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_stockTransferCollectionViaCardStockTransfer.SortClauses=sortClauses;
			_stockTransferCollectionViaCardStockTransfer.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleReplacedCard()
		{
			return GetSingleReplacedCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleReplacedCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedReplacedCard || forceFetch || _alwaysFetchReplacedCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingCardIDReplacedCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReplacedCardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_replacedCardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReplacedCard = newEntity;
				_alreadyFetchedReplacedCard = fetchResult;
			}
			return _replacedCard;
		}


		/// <summary> Retrieves the related entity of type 'CardHolderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardHolderEntity' which is related to this entity.</returns>
		public CardHolderEntity GetSingleCardHolder()
		{
			return GetSingleCardHolder(false);
		}

		/// <summary> Retrieves the related entity of type 'CardHolderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardHolderEntity' which is related to this entity.</returns>
		public virtual CardHolderEntity GetSingleCardHolder(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardHolder || forceFetch || _alwaysFetchCardHolder) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardHolderEntityUsingCardHolderID);
				CardHolderEntity newEntity = (CardHolderEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.CardHolderEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = CardHolderEntity.FetchPolymorphic(this.Transaction, this.CardHolderID.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (CardHolderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardHolderReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardHolder = newEntity;
				_alreadyFetchedCardHolder = fetchResult;
			}
			return _cardHolder;
		}


		/// <summary> Retrieves the related entity of type 'CardHolderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardHolderEntity' which is related to this entity.</returns>
		public CardHolderEntity GetSingleParticipant()
		{
			return GetSingleParticipant(false);
		}

		/// <summary> Retrieves the related entity of type 'CardHolderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardHolderEntity' which is related to this entity.</returns>
		public virtual CardHolderEntity GetSingleParticipant(bool forceFetch)
		{
			if( ( !_alreadyFetchedParticipant || forceFetch || _alwaysFetchParticipant) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardHolderEntityUsingParticipantID);
				CardHolderEntity newEntity = (CardHolderEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.CardHolderEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = CardHolderEntity.FetchPolymorphic(this.Transaction, this.ParticipantID.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (CardHolderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_participantReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Participant = newEntity;
				_alreadyFetchedParticipant = fetchResult;
			}
			return _participant;
		}


		/// <summary> Retrieves the related entity of type 'CardKeyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardKeyEntity' which is related to this entity.</returns>
		public CardKeyEntity GetSingleCardKey()
		{
			return GetSingleCardKey(false);
		}

		/// <summary> Retrieves the related entity of type 'CardKeyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardKeyEntity' which is related to this entity.</returns>
		public virtual CardKeyEntity GetSingleCardKey(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardKey || forceFetch || _alwaysFetchCardKey) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardKeyEntityUsingCardKeyID);
				CardKeyEntity newEntity = new CardKeyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardKeyID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardKeyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardKeyReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardKey = newEntity;
				_alreadyFetchedCardKey = fetchResult;
			}
			return _cardKey;
		}


		/// <summary> Retrieves the related entity of type 'CardPhysicalDetailEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardPhysicalDetailEntity' which is related to this entity.</returns>
		public CardPhysicalDetailEntity GetSingleCardPhysicalDetail()
		{
			return GetSingleCardPhysicalDetail(false);
		}

		/// <summary> Retrieves the related entity of type 'CardPhysicalDetailEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardPhysicalDetailEntity' which is related to this entity.</returns>
		public virtual CardPhysicalDetailEntity GetSingleCardPhysicalDetail(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardPhysicalDetail || forceFetch || _alwaysFetchCardPhysicalDetail) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardPhysicalDetailEntityUsingCardPhysicalDetailID);
				CardPhysicalDetailEntity newEntity = new CardPhysicalDetailEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardPhysicalDetailID);
				}
				if(fetchResult)
				{
					newEntity = (CardPhysicalDetailEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardPhysicalDetailReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardPhysicalDetail = newEntity;
				_alreadyFetchedCardPhysicalDetail = fetchResult;
			}
			return _cardPhysicalDetail;
		}


		/// <summary> Retrieves the related entity of type 'StorageLocationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'StorageLocationEntity' which is related to this entity.</returns>
		public StorageLocationEntity GetSingleStorageLocation()
		{
			return GetSingleStorageLocation(false);
		}

		/// <summary> Retrieves the related entity of type 'StorageLocationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'StorageLocationEntity' which is related to this entity.</returns>
		public virtual StorageLocationEntity GetSingleStorageLocation(bool forceFetch)
		{
			if( ( !_alreadyFetchedStorageLocation || forceFetch || _alwaysFetchStorageLocation) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.StorageLocationEntityUsingStorageLocationID);
				StorageLocationEntity newEntity = new StorageLocationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.StorageLocationID);
				}
				if(fetchResult)
				{
					newEntity = (StorageLocationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_storageLocationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.StorageLocation = newEntity;
				_alreadyFetchedStorageLocation = fetchResult;
			}
			return _storageLocation;
		}

		/// <summary> Retrieves the related entity of type 'CardLinkEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'CardLinkEntity' which is related to this entity.</returns>
		public CardLinkEntity GetSingleCardLinkTarget()
		{
			return GetSingleCardLinkTarget(false);
		}
		
		/// <summary> Retrieves the related entity of type 'CardLinkEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardLinkEntity' which is related to this entity.</returns>
		public virtual CardLinkEntity GetSingleCardLinkTarget(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardLinkTarget || forceFetch || _alwaysFetchCardLinkTarget) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardLinkEntityUsingTargetCardID);
				CardLinkEntity newEntity = new CardLinkEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardID);
				}
				if(fetchResult)
				{
					newEntity = (CardLinkEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardLinkTargetReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardLinkTarget = newEntity;
				_alreadyFetchedCardLinkTarget = fetchResult;
			}
			return _cardLinkTarget;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("ReplacedCard", _replacedCard);
			toReturn.Add("CardHolder", _cardHolder);
			toReturn.Add("Participant", _participant);
			toReturn.Add("CardKey", _cardKey);
			toReturn.Add("CardPhysicalDetail", _cardPhysicalDetail);
			toReturn.Add("StorageLocation", _storageLocation);
			toReturn.Add("ApportionmentResults", _apportionmentResults);
			toReturn.Add("BadCards", _badCards);
			toReturn.Add("Bookings", _bookings);
			toReturn.Add("ReplacementCard", _replacementCard);
			toReturn.Add("CardEvents", _cardEvents);
			toReturn.Add("CardLinksSource", _cardLinksSource);
			toReturn.Add("CardStockTransfers", _cardStockTransfers);
			toReturn.Add("CardsToContracts", _cardsToContracts);
			toReturn.Add("CardToRuleViolations", _cardToRuleViolations);
			toReturn.Add("WorkItems", _workItems);
			toReturn.Add("CreditCardAuthorizations", _creditCardAuthorizations);
			toReturn.Add("DormancyNotifications", _dormancyNotifications);
			toReturn.Add("EmailEventResendLocks", _emailEventResendLocks);
			toReturn.Add("Jobs", _jobs);
			toReturn.Add("OrderDetails", _orderDetails);
			toReturn.Add("OrganizationParticipants", _organizationParticipants);
			toReturn.Add("PaymentJournals", _paymentJournals);
			toReturn.Add("PaymentOptions", _paymentOptions);
			toReturn.Add("Products", _products);
			toReturn.Add("RuleViolations", _ruleViolations);
			toReturn.Add("TicketAssignments", _ticketAssignments);
			toReturn.Add("TransactionJournals", _transactionJournals);
			toReturn.Add("TransactionJournals_", _transactionJournals_);
			toReturn.Add("TokenTransactionJournals", _tokenTransactionJournals);
			toReturn.Add("VirtualCards", _virtualCards);
			toReturn.Add("Vouchers", _vouchers);
			toReturn.Add("Whitelists", _whitelists);
			toReturn.Add("WhitelistJournals", _whitelistJournals);
			toReturn.Add("Contracts", _contracts);
			toReturn.Add("StockTransferCollectionViaCardStockTransfer", _stockTransferCollectionViaCardStockTransfer);
			toReturn.Add("CardLinkTarget", _cardLinkTarget);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cardID">PK value for Card which data should be fetched into this Card object</param>
		/// <param name="validator">The validator object for this CardEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cardID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cardID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_apportionmentResults = new VarioSL.Entities.CollectionClasses.ApportionmentResultCollection();
			_apportionmentResults.SetContainingEntityInfo(this, "Card");

			_badCards = new VarioSL.Entities.CollectionClasses.BadCardCollection();
			_badCards.SetContainingEntityInfo(this, "Card");

			_bookings = new VarioSL.Entities.CollectionClasses.BookingCollection();
			_bookings.SetContainingEntityInfo(this, "Card");

			_replacementCard = new VarioSL.Entities.CollectionClasses.CardCollection();
			_replacementCard.SetContainingEntityInfo(this, "ReplacedCard");

			_cardEvents = new VarioSL.Entities.CollectionClasses.CardEventCollection();
			_cardEvents.SetContainingEntityInfo(this, "Card");

			_cardLinksSource = new VarioSL.Entities.CollectionClasses.CardLinkCollection();
			_cardLinksSource.SetContainingEntityInfo(this, "SourceCard");

			_cardStockTransfers = new VarioSL.Entities.CollectionClasses.CardStockTransferCollection();
			_cardStockTransfers.SetContainingEntityInfo(this, "Card");

			_cardsToContracts = new VarioSL.Entities.CollectionClasses.CardToContractCollection();
			_cardsToContracts.SetContainingEntityInfo(this, "Card");

			_cardToRuleViolations = new VarioSL.Entities.CollectionClasses.CardToRuleViolationCollection();
			_cardToRuleViolations.SetContainingEntityInfo(this, "Card");

			_workItems = new VarioSL.Entities.CollectionClasses.CardWorkItemCollection();
			_workItems.SetContainingEntityInfo(this, "Card");

			_creditCardAuthorizations = new VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection();
			_creditCardAuthorizations.SetContainingEntityInfo(this, "Card");

			_dormancyNotifications = new VarioSL.Entities.CollectionClasses.DormancyNotificationCollection();
			_dormancyNotifications.SetContainingEntityInfo(this, "Card");

			_emailEventResendLocks = new VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection();
			_emailEventResendLocks.SetContainingEntityInfo(this, "Card");

			_jobs = new VarioSL.Entities.CollectionClasses.JobCollection();
			_jobs.SetContainingEntityInfo(this, "Card");

			_orderDetails = new VarioSL.Entities.CollectionClasses.OrderDetailCollection();
			_orderDetails.SetContainingEntityInfo(this, "Card");

			_organizationParticipants = new VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection();
			_organizationParticipants.SetContainingEntityInfo(this, "Card");

			_paymentJournals = new VarioSL.Entities.CollectionClasses.PaymentJournalCollection();
			_paymentJournals.SetContainingEntityInfo(this, "Card");

			_paymentOptions = new VarioSL.Entities.CollectionClasses.PaymentOptionCollection();
			_paymentOptions.SetContainingEntityInfo(this, "Card");

			_products = new VarioSL.Entities.CollectionClasses.ProductCollection();
			_products.SetContainingEntityInfo(this, "Card");

			_ruleViolations = new VarioSL.Entities.CollectionClasses.RuleViolationCollection();
			_ruleViolations.SetContainingEntityInfo(this, "Card");

			_ticketAssignments = new VarioSL.Entities.CollectionClasses.TicketAssignmentCollection();
			_ticketAssignments.SetContainingEntityInfo(this, "Card");

			_transactionJournals = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_transactionJournals.SetContainingEntityInfo(this, "Card");

			_transactionJournals_ = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_transactionJournals_.SetContainingEntityInfo(this, "Card_");

			_tokenTransactionJournals = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_tokenTransactionJournals.SetContainingEntityInfo(this, "TokenCard");

			_virtualCards = new VarioSL.Entities.CollectionClasses.VirtualCardCollection();
			_virtualCards.SetContainingEntityInfo(this, "Card");

			_vouchers = new VarioSL.Entities.CollectionClasses.VoucherCollection();
			_vouchers.SetContainingEntityInfo(this, "Card");

			_whitelists = new VarioSL.Entities.CollectionClasses.WhitelistCollection();
			_whitelists.SetContainingEntityInfo(this, "Card");

			_whitelistJournals = new VarioSL.Entities.CollectionClasses.WhitelistJournalCollection();
			_whitelistJournals.SetContainingEntityInfo(this, "Card");
			_contracts = new VarioSL.Entities.CollectionClasses.ContractCollection();
			_stockTransferCollectionViaCardStockTransfer = new VarioSL.Entities.CollectionClasses.StockTransferCollection();
			_clientReturnsNewIfNotFound = false;
			_replacedCardReturnsNewIfNotFound = false;
			_cardHolderReturnsNewIfNotFound = false;
			_participantReturnsNewIfNotFound = false;
			_cardKeyReturnsNewIfNotFound = false;
			_cardPhysicalDetailReturnsNewIfNotFound = false;
			_storageLocationReturnsNewIfNotFound = false;
			_cardLinkTargetReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BatchNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Blocked", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockingComment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockingReason", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardHolderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardKeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardPhysicalDetailID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargeCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Expiration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExportState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareMediaID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Imported", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InventoryState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsProductPool", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsShared", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTraining", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsVoiceEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUsed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoadCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumberOfPrints", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParticipantID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentAccountReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PinCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintedDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintedNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductionDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProjectIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurseBalance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReplacedCardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Revision", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SecurityCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SequentialNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SerialNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShortCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StorageLocationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SubsidyLimit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WrongPinAttempt", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticCardRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "Cards", resetFKFields, new int[] { (int)CardFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticCardRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _replacedCard</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReplacedCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _replacedCard, new PropertyChangedEventHandler( OnReplacedCardPropertyChanged ), "ReplacedCard", VarioSL.Entities.RelationClasses.StaticCardRelations.CardEntityUsingCardIDReplacedCardIDStatic, true, signalRelatedEntity, "ReplacementCard", resetFKFields, new int[] { (int)CardFieldIndex.ReplacedCardID } );		
			_replacedCard = null;
		}
		
		/// <summary> setups the sync logic for member _replacedCard</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReplacedCard(IEntityCore relatedEntity)
		{
			if(_replacedCard!=relatedEntity)
			{		
				DesetupSyncReplacedCard(true, true);
				_replacedCard = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _replacedCard, new PropertyChangedEventHandler( OnReplacedCardPropertyChanged ), "ReplacedCard", VarioSL.Entities.RelationClasses.StaticCardRelations.CardEntityUsingCardIDReplacedCardIDStatic, true, ref _alreadyFetchedReplacedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReplacedCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cardHolder</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardHolder(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardHolder, new PropertyChangedEventHandler( OnCardHolderPropertyChanged ), "CardHolder", VarioSL.Entities.RelationClasses.StaticCardRelations.CardHolderEntityUsingCardHolderIDStatic, true, signalRelatedEntity, "Cards", resetFKFields, new int[] { (int)CardFieldIndex.CardHolderID } );		
			_cardHolder = null;
		}
		
		/// <summary> setups the sync logic for member _cardHolder</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardHolder(IEntityCore relatedEntity)
		{
			if(_cardHolder!=relatedEntity)
			{		
				DesetupSyncCardHolder(true, true);
				_cardHolder = (CardHolderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardHolder, new PropertyChangedEventHandler( OnCardHolderPropertyChanged ), "CardHolder", VarioSL.Entities.RelationClasses.StaticCardRelations.CardHolderEntityUsingCardHolderIDStatic, true, ref _alreadyFetchedCardHolder, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardHolderPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _participant</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParticipant(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _participant, new PropertyChangedEventHandler( OnParticipantPropertyChanged ), "Participant", VarioSL.Entities.RelationClasses.StaticCardRelations.CardHolderEntityUsingParticipantIDStatic, true, signalRelatedEntity, "ParticipantCards", resetFKFields, new int[] { (int)CardFieldIndex.ParticipantID } );		
			_participant = null;
		}
		
		/// <summary> setups the sync logic for member _participant</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParticipant(IEntityCore relatedEntity)
		{
			if(_participant!=relatedEntity)
			{		
				DesetupSyncParticipant(true, true);
				_participant = (CardHolderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _participant, new PropertyChangedEventHandler( OnParticipantPropertyChanged ), "Participant", VarioSL.Entities.RelationClasses.StaticCardRelations.CardHolderEntityUsingParticipantIDStatic, true, ref _alreadyFetchedParticipant, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParticipantPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cardKey</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardKey(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardKey, new PropertyChangedEventHandler( OnCardKeyPropertyChanged ), "CardKey", VarioSL.Entities.RelationClasses.StaticCardRelations.CardKeyEntityUsingCardKeyIDStatic, true, signalRelatedEntity, "Cards", resetFKFields, new int[] { (int)CardFieldIndex.CardKeyID } );		
			_cardKey = null;
		}
		
		/// <summary> setups the sync logic for member _cardKey</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardKey(IEntityCore relatedEntity)
		{
			if(_cardKey!=relatedEntity)
			{		
				DesetupSyncCardKey(true, true);
				_cardKey = (CardKeyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardKey, new PropertyChangedEventHandler( OnCardKeyPropertyChanged ), "CardKey", VarioSL.Entities.RelationClasses.StaticCardRelations.CardKeyEntityUsingCardKeyIDStatic, true, ref _alreadyFetchedCardKey, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardKeyPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cardPhysicalDetail</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardPhysicalDetail(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardPhysicalDetail, new PropertyChangedEventHandler( OnCardPhysicalDetailPropertyChanged ), "CardPhysicalDetail", VarioSL.Entities.RelationClasses.StaticCardRelations.CardPhysicalDetailEntityUsingCardPhysicalDetailIDStatic, true, signalRelatedEntity, "Cards", resetFKFields, new int[] { (int)CardFieldIndex.CardPhysicalDetailID } );		
			_cardPhysicalDetail = null;
		}
		
		/// <summary> setups the sync logic for member _cardPhysicalDetail</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardPhysicalDetail(IEntityCore relatedEntity)
		{
			if(_cardPhysicalDetail!=relatedEntity)
			{		
				DesetupSyncCardPhysicalDetail(true, true);
				_cardPhysicalDetail = (CardPhysicalDetailEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardPhysicalDetail, new PropertyChangedEventHandler( OnCardPhysicalDetailPropertyChanged ), "CardPhysicalDetail", VarioSL.Entities.RelationClasses.StaticCardRelations.CardPhysicalDetailEntityUsingCardPhysicalDetailIDStatic, true, ref _alreadyFetchedCardPhysicalDetail, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPhysicalDetailPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _storageLocation</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncStorageLocation(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _storageLocation, new PropertyChangedEventHandler( OnStorageLocationPropertyChanged ), "StorageLocation", VarioSL.Entities.RelationClasses.StaticCardRelations.StorageLocationEntityUsingStorageLocationIDStatic, true, signalRelatedEntity, "Cards", resetFKFields, new int[] { (int)CardFieldIndex.StorageLocationID } );		
			_storageLocation = null;
		}
		
		/// <summary> setups the sync logic for member _storageLocation</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncStorageLocation(IEntityCore relatedEntity)
		{
			if(_storageLocation!=relatedEntity)
			{		
				DesetupSyncStorageLocation(true, true);
				_storageLocation = (StorageLocationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _storageLocation, new PropertyChangedEventHandler( OnStorageLocationPropertyChanged ), "StorageLocation", VarioSL.Entities.RelationClasses.StaticCardRelations.StorageLocationEntityUsingStorageLocationIDStatic, true, ref _alreadyFetchedStorageLocation, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnStorageLocationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cardLinkTarget</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardLinkTarget(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardLinkTarget, new PropertyChangedEventHandler( OnCardLinkTargetPropertyChanged ), "CardLinkTarget", VarioSL.Entities.RelationClasses.StaticCardRelations.CardLinkEntityUsingTargetCardIDStatic, false, signalRelatedEntity, "TargetCard", false, new int[] { (int)CardFieldIndex.CardID } );
			_cardLinkTarget = null;
		}
	
		/// <summary> setups the sync logic for member _cardLinkTarget</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardLinkTarget(IEntityCore relatedEntity)
		{
			if(_cardLinkTarget!=relatedEntity)
			{
				DesetupSyncCardLinkTarget(true, true);
				_cardLinkTarget = (CardLinkEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardLinkTarget, new PropertyChangedEventHandler( OnCardLinkTargetPropertyChanged ), "CardLinkTarget", VarioSL.Entities.RelationClasses.StaticCardRelations.CardLinkEntityUsingTargetCardIDStatic, false, ref _alreadyFetchedCardLinkTarget, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardLinkTargetPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cardID">PK value for Card which data should be fetched into this Card object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cardID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CardFieldIndex.CardID].ForcedCurrentValueWrite(cardID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCardDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CardEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CardRelations Relations
		{
			get	{ return new CardRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ApportionmentResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApportionmentResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ApportionmentResultCollection(), (IEntityRelation)GetRelationsForField("ApportionmentResults")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.ApportionmentResultEntity, 0, null, null, null, "ApportionmentResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BadCard' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBadCards
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BadCardCollection(), (IEntityRelation)GetRelationsForField("BadCards")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.BadCardEntity, 0, null, null, null, "BadCards", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Booking' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBookings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BookingCollection(), (IEntityRelation)GetRelationsForField("Bookings")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.BookingEntity, 0, null, null, null, "Bookings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReplacementCard
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("ReplacementCard")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "ReplacementCard", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardEvent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardEvents
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardEventCollection(), (IEntityRelation)GetRelationsForField("CardEvents")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardEventEntity, 0, null, null, null, "CardEvents", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardLink' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardLinksSource
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardLinkCollection(), (IEntityRelation)GetRelationsForField("CardLinksSource")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardLinkEntity, 0, null, null, null, "CardLinksSource", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardStockTransfer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardStockTransfers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardStockTransferCollection(), (IEntityRelation)GetRelationsForField("CardStockTransfers")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardStockTransferEntity, 0, null, null, null, "CardStockTransfers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardToContract' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardsToContracts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardToContractCollection(), (IEntityRelation)GetRelationsForField("CardsToContracts")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardToContractEntity, 0, null, null, null, "CardsToContracts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardToRuleViolation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardToRuleViolations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardToRuleViolationCollection(), (IEntityRelation)GetRelationsForField("CardToRuleViolations")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardToRuleViolationEntity, 0, null, null, null, "CardToRuleViolations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardWorkItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWorkItems
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardWorkItemCollection(), (IEntityRelation)GetRelationsForField("WorkItems")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardWorkItemEntity, 0, null, null, null, "WorkItems", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CreditCardAuthorization' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCreditCardAuthorizations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection(), (IEntityRelation)GetRelationsForField("CreditCardAuthorizations")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CreditCardAuthorizationEntity, 0, null, null, null, "CreditCardAuthorizations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DormancyNotification' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDormancyNotifications
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DormancyNotificationCollection(), (IEntityRelation)GetRelationsForField("DormancyNotifications")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.DormancyNotificationEntity, 0, null, null, null, "DormancyNotifications", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailEventResendLock' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailEventResendLocks
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection(), (IEntityRelation)GetRelationsForField("EmailEventResendLocks")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.EmailEventResendLockEntity, 0, null, null, null, "EmailEventResendLocks", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Job' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathJobs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.JobCollection(), (IEntityRelation)GetRelationsForField("Jobs")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.JobEntity, 0, null, null, null, "Jobs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetail' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderDetails
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailCollection(), (IEntityRelation)GetRelationsForField("OrderDetails")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.OrderDetailEntity, 0, null, null, null, "OrderDetails", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrganizationParticipant' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganizationParticipants
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection(), (IEntityRelation)GetRelationsForField("OrganizationParticipants")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.OrganizationParticipantEntity, 0, null, null, null, "OrganizationParticipants", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentJournalCollection(), (IEntityRelation)GetRelationsForField("PaymentJournals")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.PaymentJournalEntity, 0, null, null, null, "PaymentJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentOptions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("PaymentOptions")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "PaymentOptions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Products")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Products", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleViolation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleViolations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleViolationCollection(), (IEntityRelation)GetRelationsForField("RuleViolations")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.RuleViolationEntity, 0, null, null, null, "RuleViolations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketAssignment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketAssignments
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketAssignmentCollection(), (IEntityRelation)GetRelationsForField("TicketAssignments")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.TicketAssignmentEntity, 0, null, null, null, "TicketAssignments", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournals")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournals_
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournals_")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournals_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTokenTransactionJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TokenTransactionJournals")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TokenTransactionJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VirtualCard' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVirtualCards
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VirtualCardCollection(), (IEntityRelation)GetRelationsForField("VirtualCards")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.VirtualCardEntity, 0, null, null, null, "VirtualCards", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Voucher' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVouchers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VoucherCollection(), (IEntityRelation)GetRelationsForField("Vouchers")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.VoucherEntity, 0, null, null, null, "Vouchers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Whitelist' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWhitelists
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.WhitelistCollection(), (IEntityRelation)GetRelationsForField("Whitelists")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.WhitelistEntity, 0, null, null, null, "Whitelists", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'WhitelistJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWhitelistJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.WhitelistJournalCollection(), (IEntityRelation)GetRelationsForField("WhitelistJournals")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.WhitelistJournalEntity, 0, null, null, null, "WhitelistJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContracts
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CardToContractEntityUsingCardID;
				intermediateRelation.SetAliases(string.Empty, "CardToContract_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, GetRelationsForField("Contracts"), "Contracts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StockTransfer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathStockTransferCollectionViaCardStockTransfer
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CardStockTransferEntityUsingCardID;
				intermediateRelation.SetAliases(string.Empty, "CardStockTransfer_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StockTransferCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.StockTransferEntity, 0, null, null, GetRelationsForField("StockTransferCollectionViaCardStockTransfer"), "StockTransferCollectionViaCardStockTransfer", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReplacedCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("ReplacedCard")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "ReplacedCard", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardHolder'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardHolder
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardHolderCollection(), (IEntityRelation)GetRelationsForField("CardHolder")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardHolderEntity, 0, null, null, null, "CardHolder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardHolder'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParticipant
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardHolderCollection(), (IEntityRelation)GetRelationsForField("Participant")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardHolderEntity, 0, null, null, null, "Participant", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardKey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardKey
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardKeyCollection(), (IEntityRelation)GetRelationsForField("CardKey")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardKeyEntity, 0, null, null, null, "CardKey", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardPhysicalDetail'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardPhysicalDetail
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection(), (IEntityRelation)GetRelationsForField("CardPhysicalDetail")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardPhysicalDetailEntity, 0, null, null, null, "CardPhysicalDetail", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StorageLocation'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathStorageLocation
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StorageLocationCollection(), (IEntityRelation)GetRelationsForField("StorageLocation")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.StorageLocationEntity, 0, null, null, null, "StorageLocation", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardLink'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardLinkTarget
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardLinkCollection(), (IEntityRelation)GetRelationsForField("CardLinkTarget")[0], (int)VarioSL.Entities.EntityType.CardEntity, (int)VarioSL.Entities.EntityType.CardLinkEntity, 0, null, null, null, "CardLinkTarget", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BatchNumber property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."BATCHNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String BatchNumber
		{
			get { return (System.String)GetValue((int)CardFieldIndex.BatchNumber, true); }
			set	{ SetValue((int)CardFieldIndex.BatchNumber, value, true); }
		}

		/// <summary> The Blocked property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."BLOCKED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Blocked
		{
			get { return (System.DateTime)GetValue((int)CardFieldIndex.Blocked, true); }
			set	{ SetValue((int)CardFieldIndex.Blocked, value, true); }
		}

		/// <summary> The BlockingComment property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."BLOCKINGCOMMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BlockingComment
		{
			get { return (System.String)GetValue((int)CardFieldIndex.BlockingComment, true); }
			set	{ SetValue((int)CardFieldIndex.BlockingComment, value, true); }
		}

		/// <summary> The BlockingReason property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."BLOCKINGREASON"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.BlockingReason BlockingReason
		{
			get { return (VarioSL.Entities.Enumerations.BlockingReason)GetValue((int)CardFieldIndex.BlockingReason, true); }
			set	{ SetValue((int)CardFieldIndex.BlockingReason, value, true); }
		}

		/// <summary> The CardHolderID property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."CARDHOLDERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardHolderID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardFieldIndex.CardHolderID, false); }
			set	{ SetValue((int)CardFieldIndex.CardHolderID, value, true); }
		}

		/// <summary> The CardID property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CardID
		{
			get { return (System.Int64)GetValue((int)CardFieldIndex.CardID, true); }
			set	{ SetValue((int)CardFieldIndex.CardID, value, true); }
		}

		/// <summary> The CardKeyID property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."CARDKEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardKeyID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardFieldIndex.CardKeyID, false); }
			set	{ SetValue((int)CardFieldIndex.CardKeyID, value, true); }
		}

		/// <summary> The CardPhysicalDetailID property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."CARDPHYSICALDETAILID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CardPhysicalDetailID
		{
			get { return (System.Int64)GetValue((int)CardFieldIndex.CardPhysicalDetailID, true); }
			set	{ SetValue((int)CardFieldIndex.CardPhysicalDetailID, value, true); }
		}

		/// <summary> The CardType property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."CARDTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CardType
		{
			get { return (System.Int64)GetValue((int)CardFieldIndex.CardType, true); }
			set	{ SetValue((int)CardFieldIndex.CardType, value, true); }
		}

		/// <summary> The ChargeCounter property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."CHARGECOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ChargeCounter
		{
			get { return (System.Int32)GetValue((int)CardFieldIndex.ChargeCounter, true); }
			set	{ SetValue((int)CardFieldIndex.ChargeCounter, value, true); }
		}

		/// <summary> The ClientID property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardFieldIndex.ClientID, false); }
			set	{ SetValue((int)CardFieldIndex.ClientID, value, true); }
		}

		/// <summary> The Description property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CardFieldIndex.Description, true); }
			set	{ SetValue((int)CardFieldIndex.Description, value, true); }
		}

		/// <summary> The Expiration property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."EXPIRATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Expiration
		{
			get { return (System.DateTime)GetValue((int)CardFieldIndex.Expiration, true); }
			set	{ SetValue((int)CardFieldIndex.Expiration, value, true); }
		}

		/// <summary> The ExportState property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."EXPORTSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 ExportState
		{
			get { return (System.Int16)GetValue((int)CardFieldIndex.ExportState, true); }
			set	{ SetValue((int)CardFieldIndex.ExportState, value, true); }
		}

		/// <summary> The FareMediaID property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."FAREMEDIAID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FareMediaID
		{
			get { return (System.String)GetValue((int)CardFieldIndex.FareMediaID, true); }
			set	{ SetValue((int)CardFieldIndex.FareMediaID, value, true); }
		}

		/// <summary> The Imported property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."IMPORTED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Imported
		{
			get { return (System.DateTime)GetValue((int)CardFieldIndex.Imported, true); }
			set	{ SetValue((int)CardFieldIndex.Imported, value, true); }
		}

		/// <summary> The InventoryState property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."INVENTORYSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.InventoryState InventoryState
		{
			get { return (VarioSL.Entities.Enumerations.InventoryState)GetValue((int)CardFieldIndex.InventoryState, true); }
			set	{ SetValue((int)CardFieldIndex.InventoryState, value, true); }
		}

		/// <summary> The IsProductPool property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."ISPRODUCTPOOL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsProductPool
		{
			get { return (System.Boolean)GetValue((int)CardFieldIndex.IsProductPool, true); }
			set	{ SetValue((int)CardFieldIndex.IsProductPool, value, true); }
		}

		/// <summary> The IsShared property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."ISSHARED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsShared
		{
			get { return (System.Boolean)GetValue((int)CardFieldIndex.IsShared, true); }
			set	{ SetValue((int)CardFieldIndex.IsShared, value, true); }
		}

		/// <summary> The IsTraining property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."ISTRAINING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTraining
		{
			get { return (System.Boolean)GetValue((int)CardFieldIndex.IsTraining, true); }
			set	{ SetValue((int)CardFieldIndex.IsTraining, value, true); }
		}

		/// <summary> The IsVoiceEnabled property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."ISVOICEENABLED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsVoiceEnabled
		{
			get { return (System.Boolean)GetValue((int)CardFieldIndex.IsVoiceEnabled, true); }
			set	{ SetValue((int)CardFieldIndex.IsVoiceEnabled, value, true); }
		}

		/// <summary> The LastModified property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CardFieldIndex.LastModified, true); }
			set	{ SetValue((int)CardFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUsed property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."LASTUSED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastUsed
		{
			get { return (System.DateTime)GetValue((int)CardFieldIndex.LastUsed, true); }
			set	{ SetValue((int)CardFieldIndex.LastUsed, value, true); }
		}

		/// <summary> The LastUser property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CardFieldIndex.LastUser, true); }
			set	{ SetValue((int)CardFieldIndex.LastUser, value, true); }
		}

		/// <summary> The LoadCounter property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."LOADCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LoadCounter
		{
			get { return (System.Int32)GetValue((int)CardFieldIndex.LoadCounter, true); }
			set	{ SetValue((int)CardFieldIndex.LoadCounter, value, true); }
		}

		/// <summary> The NumberOfPrints property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."NUMBEROFPRINTS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NumberOfPrints
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardFieldIndex.NumberOfPrints, false); }
			set	{ SetValue((int)CardFieldIndex.NumberOfPrints, value, true); }
		}

		/// <summary> The OrderIdentifier property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."ORDERIDENTIFIER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderIdentifier
		{
			get { return (System.String)GetValue((int)CardFieldIndex.OrderIdentifier, true); }
			set	{ SetValue((int)CardFieldIndex.OrderIdentifier, value, true); }
		}

		/// <summary> The ParticipantID property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."PARTICIPANTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ParticipantID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardFieldIndex.ParticipantID, false); }
			set	{ SetValue((int)CardFieldIndex.ParticipantID, value, true); }
		}

		/// <summary> The PaymentAccountReference property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."PAYMENTACCOUNTREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentAccountReference
		{
			get { return (System.String)GetValue((int)CardFieldIndex.PaymentAccountReference, true); }
			set	{ SetValue((int)CardFieldIndex.PaymentAccountReference, value, true); }
		}

		/// <summary> The PinCode property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."PINCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 70<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PinCode
		{
			get { return (System.String)GetValue((int)CardFieldIndex.PinCode, true); }
			set	{ SetValue((int)CardFieldIndex.PinCode, value, true); }
		}

		/// <summary> The PrintedDate property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."PRINTEDDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PrintedDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CardFieldIndex.PrintedDate, false); }
			set	{ SetValue((int)CardFieldIndex.PrintedDate, value, true); }
		}

		/// <summary> The PrintedNumber property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."PRINTEDNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String PrintedNumber
		{
			get { return (System.String)GetValue((int)CardFieldIndex.PrintedNumber, true); }
			set	{ SetValue((int)CardFieldIndex.PrintedNumber, value, true); }
		}

		/// <summary> The ProductionDate property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."PRODUCTIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ProductionDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CardFieldIndex.ProductionDate, false); }
			set	{ SetValue((int)CardFieldIndex.ProductionDate, value, true); }
		}

		/// <summary> The ProjectIdentifier property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."PROJECTIDENTIFIER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProjectIdentifier
		{
			get { return (System.String)GetValue((int)CardFieldIndex.ProjectIdentifier, true); }
			set	{ SetValue((int)CardFieldIndex.ProjectIdentifier, value, true); }
		}

		/// <summary> The PurseBalance property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."PURSEBALANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PurseBalance
		{
			get { return (System.Int32)GetValue((int)CardFieldIndex.PurseBalance, true); }
			set	{ SetValue((int)CardFieldIndex.PurseBalance, value, true); }
		}

		/// <summary> The ReplacedCardID property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."REPLACEDCARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReplacedCardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardFieldIndex.ReplacedCardID, false); }
			set	{ SetValue((int)CardFieldIndex.ReplacedCardID, value, true); }
		}

		/// <summary> The Revision property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."REVISION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Revision
		{
			get { return (Nullable<System.Int32>)GetValue((int)CardFieldIndex.Revision, false); }
			set	{ SetValue((int)CardFieldIndex.Revision, value, true); }
		}

		/// <summary> The SecurityCode property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."SECURITYCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 3<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SecurityCode
		{
			get { return (System.String)GetValue((int)CardFieldIndex.SecurityCode, true); }
			set	{ SetValue((int)CardFieldIndex.SecurityCode, value, true); }
		}

		/// <summary> The SequentialNumber property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."SEQUENTIALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SequentialNumber
		{
			get { return (System.String)GetValue((int)CardFieldIndex.SequentialNumber, true); }
			set	{ SetValue((int)CardFieldIndex.SequentialNumber, value, true); }
		}

		/// <summary> The SerialNumber property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."SERIALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SerialNumber
		{
			get { return (System.String)GetValue((int)CardFieldIndex.SerialNumber, true); }
			set	{ SetValue((int)CardFieldIndex.SerialNumber, value, true); }
		}

		/// <summary> The ShortCode property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."SHORTCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShortCode
		{
			get { return (System.String)GetValue((int)CardFieldIndex.ShortCode, true); }
			set	{ SetValue((int)CardFieldIndex.ShortCode, value, true); }
		}

		/// <summary> The State property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CardState State
		{
			get { return (VarioSL.Entities.Enumerations.CardState)GetValue((int)CardFieldIndex.State, true); }
			set	{ SetValue((int)CardFieldIndex.State, value, true); }
		}

		/// <summary> The StorageLocationID property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."STORAGELOCATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 StorageLocationID
		{
			get { return (System.Int64)GetValue((int)CardFieldIndex.StorageLocationID, true); }
			set	{ SetValue((int)CardFieldIndex.StorageLocationID, value, true); }
		}

		/// <summary> The SubsidyLimit property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."SUBSIDYLIMIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SubsidyLimit
		{
			get { return (Nullable<System.Int32>)GetValue((int)CardFieldIndex.SubsidyLimit, false); }
			set	{ SetValue((int)CardFieldIndex.SubsidyLimit, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CardFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CardFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The UserGroupID property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."USERGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UserGroupID
		{
			get { return (Nullable<System.Int32>)GetValue((int)CardFieldIndex.UserGroupID, false); }
			set	{ SetValue((int)CardFieldIndex.UserGroupID, value, true); }
		}

		/// <summary> The WrongPinAttempt property of the Entity Card<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARD"."WRONGPINATTEMPT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 8, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 WrongPinAttempt
		{
			get { return (System.Int32)GetValue((int)CardFieldIndex.WrongPinAttempt, true); }
			set	{ SetValue((int)CardFieldIndex.WrongPinAttempt, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ApportionmentResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiApportionmentResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ApportionmentResultCollection ApportionmentResults
		{
			get	{ return GetMultiApportionmentResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ApportionmentResults. When set to true, ApportionmentResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApportionmentResults is accessed. You can always execute/ a forced fetch by calling GetMultiApportionmentResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApportionmentResults
		{
			get	{ return _alwaysFetchApportionmentResults; }
			set	{ _alwaysFetchApportionmentResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApportionmentResults already has been fetched. Setting this property to false when ApportionmentResults has been fetched
		/// will clear the ApportionmentResults collection well. Setting this property to true while ApportionmentResults hasn't been fetched disables lazy loading for ApportionmentResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApportionmentResults
		{
			get { return _alreadyFetchedApportionmentResults;}
			set 
			{
				if(_alreadyFetchedApportionmentResults && !value && (_apportionmentResults != null))
				{
					_apportionmentResults.Clear();
				}
				_alreadyFetchedApportionmentResults = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BadCardEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBadCards()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BadCardCollection BadCards
		{
			get	{ return GetMultiBadCards(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BadCards. When set to true, BadCards is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BadCards is accessed. You can always execute/ a forced fetch by calling GetMultiBadCards(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBadCards
		{
			get	{ return _alwaysFetchBadCards; }
			set	{ _alwaysFetchBadCards = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BadCards already has been fetched. Setting this property to false when BadCards has been fetched
		/// will clear the BadCards collection well. Setting this property to true while BadCards hasn't been fetched disables lazy loading for BadCards</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBadCards
		{
			get { return _alreadyFetchedBadCards;}
			set 
			{
				if(_alreadyFetchedBadCards && !value && (_badCards != null))
				{
					_badCards.Clear();
				}
				_alreadyFetchedBadCards = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BookingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBookings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BookingCollection Bookings
		{
			get	{ return GetMultiBookings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Bookings. When set to true, Bookings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Bookings is accessed. You can always execute/ a forced fetch by calling GetMultiBookings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBookings
		{
			get	{ return _alwaysFetchBookings; }
			set	{ _alwaysFetchBookings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Bookings already has been fetched. Setting this property to false when Bookings has been fetched
		/// will clear the Bookings collection well. Setting this property to true while Bookings hasn't been fetched disables lazy loading for Bookings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBookings
		{
			get { return _alreadyFetchedBookings;}
			set 
			{
				if(_alreadyFetchedBookings && !value && (_bookings != null))
				{
					_bookings.Clear();
				}
				_alreadyFetchedBookings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReplacementCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection ReplacementCard
		{
			get	{ return GetMultiReplacementCard(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReplacementCard. When set to true, ReplacementCard is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReplacementCard is accessed. You can always execute/ a forced fetch by calling GetMultiReplacementCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReplacementCard
		{
			get	{ return _alwaysFetchReplacementCard; }
			set	{ _alwaysFetchReplacementCard = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReplacementCard already has been fetched. Setting this property to false when ReplacementCard has been fetched
		/// will clear the ReplacementCard collection well. Setting this property to true while ReplacementCard hasn't been fetched disables lazy loading for ReplacementCard</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReplacementCard
		{
			get { return _alreadyFetchedReplacementCard;}
			set 
			{
				if(_alreadyFetchedReplacementCard && !value && (_replacementCard != null))
				{
					_replacementCard.Clear();
				}
				_alreadyFetchedReplacementCard = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardEventEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardEvents()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardEventCollection CardEvents
		{
			get	{ return GetMultiCardEvents(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardEvents. When set to true, CardEvents is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardEvents is accessed. You can always execute/ a forced fetch by calling GetMultiCardEvents(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardEvents
		{
			get	{ return _alwaysFetchCardEvents; }
			set	{ _alwaysFetchCardEvents = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardEvents already has been fetched. Setting this property to false when CardEvents has been fetched
		/// will clear the CardEvents collection well. Setting this property to true while CardEvents hasn't been fetched disables lazy loading for CardEvents</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardEvents
		{
			get { return _alreadyFetchedCardEvents;}
			set 
			{
				if(_alreadyFetchedCardEvents && !value && (_cardEvents != null))
				{
					_cardEvents.Clear();
				}
				_alreadyFetchedCardEvents = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardLinkEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardLinksSource()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardLinkCollection CardLinksSource
		{
			get	{ return GetMultiCardLinksSource(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardLinksSource. When set to true, CardLinksSource is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardLinksSource is accessed. You can always execute/ a forced fetch by calling GetMultiCardLinksSource(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardLinksSource
		{
			get	{ return _alwaysFetchCardLinksSource; }
			set	{ _alwaysFetchCardLinksSource = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardLinksSource already has been fetched. Setting this property to false when CardLinksSource has been fetched
		/// will clear the CardLinksSource collection well. Setting this property to true while CardLinksSource hasn't been fetched disables lazy loading for CardLinksSource</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardLinksSource
		{
			get { return _alreadyFetchedCardLinksSource;}
			set 
			{
				if(_alreadyFetchedCardLinksSource && !value && (_cardLinksSource != null))
				{
					_cardLinksSource.Clear();
				}
				_alreadyFetchedCardLinksSource = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardStockTransferEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardStockTransfers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardStockTransferCollection CardStockTransfers
		{
			get	{ return GetMultiCardStockTransfers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardStockTransfers. When set to true, CardStockTransfers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardStockTransfers is accessed. You can always execute/ a forced fetch by calling GetMultiCardStockTransfers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardStockTransfers
		{
			get	{ return _alwaysFetchCardStockTransfers; }
			set	{ _alwaysFetchCardStockTransfers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardStockTransfers already has been fetched. Setting this property to false when CardStockTransfers has been fetched
		/// will clear the CardStockTransfers collection well. Setting this property to true while CardStockTransfers hasn't been fetched disables lazy loading for CardStockTransfers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardStockTransfers
		{
			get { return _alreadyFetchedCardStockTransfers;}
			set 
			{
				if(_alreadyFetchedCardStockTransfers && !value && (_cardStockTransfers != null))
				{
					_cardStockTransfers.Clear();
				}
				_alreadyFetchedCardStockTransfers = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardToContractEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardsToContracts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardToContractCollection CardsToContracts
		{
			get	{ return GetMultiCardsToContracts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardsToContracts. When set to true, CardsToContracts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardsToContracts is accessed. You can always execute/ a forced fetch by calling GetMultiCardsToContracts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardsToContracts
		{
			get	{ return _alwaysFetchCardsToContracts; }
			set	{ _alwaysFetchCardsToContracts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardsToContracts already has been fetched. Setting this property to false when CardsToContracts has been fetched
		/// will clear the CardsToContracts collection well. Setting this property to true while CardsToContracts hasn't been fetched disables lazy loading for CardsToContracts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardsToContracts
		{
			get { return _alreadyFetchedCardsToContracts;}
			set 
			{
				if(_alreadyFetchedCardsToContracts && !value && (_cardsToContracts != null))
				{
					_cardsToContracts.Clear();
				}
				_alreadyFetchedCardsToContracts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardToRuleViolationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardToRuleViolations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardToRuleViolationCollection CardToRuleViolations
		{
			get	{ return GetMultiCardToRuleViolations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardToRuleViolations. When set to true, CardToRuleViolations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardToRuleViolations is accessed. You can always execute/ a forced fetch by calling GetMultiCardToRuleViolations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardToRuleViolations
		{
			get	{ return _alwaysFetchCardToRuleViolations; }
			set	{ _alwaysFetchCardToRuleViolations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardToRuleViolations already has been fetched. Setting this property to false when CardToRuleViolations has been fetched
		/// will clear the CardToRuleViolations collection well. Setting this property to true while CardToRuleViolations hasn't been fetched disables lazy loading for CardToRuleViolations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardToRuleViolations
		{
			get { return _alreadyFetchedCardToRuleViolations;}
			set 
			{
				if(_alreadyFetchedCardToRuleViolations && !value && (_cardToRuleViolations != null))
				{
					_cardToRuleViolations.Clear();
				}
				_alreadyFetchedCardToRuleViolations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardWorkItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWorkItems()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardWorkItemCollection WorkItems
		{
			get	{ return GetMultiWorkItems(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for WorkItems. When set to true, WorkItems is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WorkItems is accessed. You can always execute/ a forced fetch by calling GetMultiWorkItems(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWorkItems
		{
			get	{ return _alwaysFetchWorkItems; }
			set	{ _alwaysFetchWorkItems = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property WorkItems already has been fetched. Setting this property to false when WorkItems has been fetched
		/// will clear the WorkItems collection well. Setting this property to true while WorkItems hasn't been fetched disables lazy loading for WorkItems</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWorkItems
		{
			get { return _alreadyFetchedWorkItems;}
			set 
			{
				if(_alreadyFetchedWorkItems && !value && (_workItems != null))
				{
					_workItems.Clear();
				}
				_alreadyFetchedWorkItems = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCreditCardAuthorizations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection CreditCardAuthorizations
		{
			get	{ return GetMultiCreditCardAuthorizations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CreditCardAuthorizations. When set to true, CreditCardAuthorizations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CreditCardAuthorizations is accessed. You can always execute/ a forced fetch by calling GetMultiCreditCardAuthorizations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCreditCardAuthorizations
		{
			get	{ return _alwaysFetchCreditCardAuthorizations; }
			set	{ _alwaysFetchCreditCardAuthorizations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CreditCardAuthorizations already has been fetched. Setting this property to false when CreditCardAuthorizations has been fetched
		/// will clear the CreditCardAuthorizations collection well. Setting this property to true while CreditCardAuthorizations hasn't been fetched disables lazy loading for CreditCardAuthorizations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCreditCardAuthorizations
		{
			get { return _alreadyFetchedCreditCardAuthorizations;}
			set 
			{
				if(_alreadyFetchedCreditCardAuthorizations && !value && (_creditCardAuthorizations != null))
				{
					_creditCardAuthorizations.Clear();
				}
				_alreadyFetchedCreditCardAuthorizations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DormancyNotificationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDormancyNotifications()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DormancyNotificationCollection DormancyNotifications
		{
			get	{ return GetMultiDormancyNotifications(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DormancyNotifications. When set to true, DormancyNotifications is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DormancyNotifications is accessed. You can always execute/ a forced fetch by calling GetMultiDormancyNotifications(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDormancyNotifications
		{
			get	{ return _alwaysFetchDormancyNotifications; }
			set	{ _alwaysFetchDormancyNotifications = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DormancyNotifications already has been fetched. Setting this property to false when DormancyNotifications has been fetched
		/// will clear the DormancyNotifications collection well. Setting this property to true while DormancyNotifications hasn't been fetched disables lazy loading for DormancyNotifications</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDormancyNotifications
		{
			get { return _alreadyFetchedDormancyNotifications;}
			set 
			{
				if(_alreadyFetchedDormancyNotifications && !value && (_dormancyNotifications != null))
				{
					_dormancyNotifications.Clear();
				}
				_alreadyFetchedDormancyNotifications = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEmailEventResendLocks()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection EmailEventResendLocks
		{
			get	{ return GetMultiEmailEventResendLocks(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EmailEventResendLocks. When set to true, EmailEventResendLocks is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailEventResendLocks is accessed. You can always execute/ a forced fetch by calling GetMultiEmailEventResendLocks(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailEventResendLocks
		{
			get	{ return _alwaysFetchEmailEventResendLocks; }
			set	{ _alwaysFetchEmailEventResendLocks = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailEventResendLocks already has been fetched. Setting this property to false when EmailEventResendLocks has been fetched
		/// will clear the EmailEventResendLocks collection well. Setting this property to true while EmailEventResendLocks hasn't been fetched disables lazy loading for EmailEventResendLocks</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailEventResendLocks
		{
			get { return _alreadyFetchedEmailEventResendLocks;}
			set 
			{
				if(_alreadyFetchedEmailEventResendLocks && !value && (_emailEventResendLocks != null))
				{
					_emailEventResendLocks.Clear();
				}
				_alreadyFetchedEmailEventResendLocks = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiJobs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.JobCollection Jobs
		{
			get	{ return GetMultiJobs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Jobs. When set to true, Jobs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Jobs is accessed. You can always execute/ a forced fetch by calling GetMultiJobs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchJobs
		{
			get	{ return _alwaysFetchJobs; }
			set	{ _alwaysFetchJobs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Jobs already has been fetched. Setting this property to false when Jobs has been fetched
		/// will clear the Jobs collection well. Setting this property to true while Jobs hasn't been fetched disables lazy loading for Jobs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedJobs
		{
			get { return _alreadyFetchedJobs;}
			set 
			{
				if(_alreadyFetchedJobs && !value && (_jobs != null))
				{
					_jobs.Clear();
				}
				_alreadyFetchedJobs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderDetails()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailCollection OrderDetails
		{
			get	{ return GetMultiOrderDetails(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderDetails. When set to true, OrderDetails is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderDetails is accessed. You can always execute/ a forced fetch by calling GetMultiOrderDetails(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderDetails
		{
			get	{ return _alwaysFetchOrderDetails; }
			set	{ _alwaysFetchOrderDetails = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderDetails already has been fetched. Setting this property to false when OrderDetails has been fetched
		/// will clear the OrderDetails collection well. Setting this property to true while OrderDetails hasn't been fetched disables lazy loading for OrderDetails</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderDetails
		{
			get { return _alreadyFetchedOrderDetails;}
			set 
			{
				if(_alreadyFetchedOrderDetails && !value && (_orderDetails != null))
				{
					_orderDetails.Clear();
				}
				_alreadyFetchedOrderDetails = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrganizationParticipants()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection OrganizationParticipants
		{
			get	{ return GetMultiOrganizationParticipants(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrganizationParticipants. When set to true, OrganizationParticipants is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrganizationParticipants is accessed. You can always execute/ a forced fetch by calling GetMultiOrganizationParticipants(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganizationParticipants
		{
			get	{ return _alwaysFetchOrganizationParticipants; }
			set	{ _alwaysFetchOrganizationParticipants = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrganizationParticipants already has been fetched. Setting this property to false when OrganizationParticipants has been fetched
		/// will clear the OrganizationParticipants collection well. Setting this property to true while OrganizationParticipants hasn't been fetched disables lazy loading for OrganizationParticipants</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganizationParticipants
		{
			get { return _alreadyFetchedOrganizationParticipants;}
			set 
			{
				if(_alreadyFetchedOrganizationParticipants && !value && (_organizationParticipants != null))
				{
					_organizationParticipants.Clear();
				}
				_alreadyFetchedOrganizationParticipants = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalCollection PaymentJournals
		{
			get	{ return GetMultiPaymentJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentJournals. When set to true, PaymentJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentJournals is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentJournals
		{
			get	{ return _alwaysFetchPaymentJournals; }
			set	{ _alwaysFetchPaymentJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentJournals already has been fetched. Setting this property to false when PaymentJournals has been fetched
		/// will clear the PaymentJournals collection well. Setting this property to true while PaymentJournals hasn't been fetched disables lazy loading for PaymentJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentJournals
		{
			get { return _alreadyFetchedPaymentJournals;}
			set 
			{
				if(_alreadyFetchedPaymentJournals && !value && (_paymentJournals != null))
				{
					_paymentJournals.Clear();
				}
				_alreadyFetchedPaymentJournals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentOptionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentOptions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentOptionCollection PaymentOptions
		{
			get	{ return GetMultiPaymentOptions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentOptions. When set to true, PaymentOptions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentOptions is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentOptions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentOptions
		{
			get	{ return _alwaysFetchPaymentOptions; }
			set	{ _alwaysFetchPaymentOptions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentOptions already has been fetched. Setting this property to false when PaymentOptions has been fetched
		/// will clear the PaymentOptions collection well. Setting this property to true while PaymentOptions hasn't been fetched disables lazy loading for PaymentOptions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentOptions
		{
			get { return _alreadyFetchedPaymentOptions;}
			set 
			{
				if(_alreadyFetchedPaymentOptions && !value && (_paymentOptions != null))
				{
					_paymentOptions.Clear();
				}
				_alreadyFetchedPaymentOptions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection Products
		{
			get	{ return GetMultiProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Products. When set to true, Products is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Products is accessed. You can always execute/ a forced fetch by calling GetMultiProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProducts
		{
			get	{ return _alwaysFetchProducts; }
			set	{ _alwaysFetchProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Products already has been fetched. Setting this property to false when Products has been fetched
		/// will clear the Products collection well. Setting this property to true while Products hasn't been fetched disables lazy loading for Products</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProducts
		{
			get { return _alreadyFetchedProducts;}
			set 
			{
				if(_alreadyFetchedProducts && !value && (_products != null))
				{
					_products.Clear();
				}
				_alreadyFetchedProducts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleViolations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationCollection RuleViolations
		{
			get	{ return GetMultiRuleViolations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleViolations. When set to true, RuleViolations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleViolations is accessed. You can always execute/ a forced fetch by calling GetMultiRuleViolations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleViolations
		{
			get	{ return _alwaysFetchRuleViolations; }
			set	{ _alwaysFetchRuleViolations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleViolations already has been fetched. Setting this property to false when RuleViolations has been fetched
		/// will clear the RuleViolations collection well. Setting this property to true while RuleViolations hasn't been fetched disables lazy loading for RuleViolations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleViolations
		{
			get { return _alreadyFetchedRuleViolations;}
			set 
			{
				if(_alreadyFetchedRuleViolations && !value && (_ruleViolations != null))
				{
					_ruleViolations.Clear();
				}
				_alreadyFetchedRuleViolations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketAssignments()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketAssignmentCollection TicketAssignments
		{
			get	{ return GetMultiTicketAssignments(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketAssignments. When set to true, TicketAssignments is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketAssignments is accessed. You can always execute/ a forced fetch by calling GetMultiTicketAssignments(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketAssignments
		{
			get	{ return _alwaysFetchTicketAssignments; }
			set	{ _alwaysFetchTicketAssignments = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketAssignments already has been fetched. Setting this property to false when TicketAssignments has been fetched
		/// will clear the TicketAssignments collection well. Setting this property to true while TicketAssignments hasn't been fetched disables lazy loading for TicketAssignments</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketAssignments
		{
			get { return _alreadyFetchedTicketAssignments;}
			set 
			{
				if(_alreadyFetchedTicketAssignments && !value && (_ticketAssignments != null))
				{
					_ticketAssignments.Clear();
				}
				_alreadyFetchedTicketAssignments = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TransactionJournals
		{
			get	{ return GetMultiTransactionJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournals. When set to true, TransactionJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournals is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournals
		{
			get	{ return _alwaysFetchTransactionJournals; }
			set	{ _alwaysFetchTransactionJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournals already has been fetched. Setting this property to false when TransactionJournals has been fetched
		/// will clear the TransactionJournals collection well. Setting this property to true while TransactionJournals hasn't been fetched disables lazy loading for TransactionJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournals
		{
			get { return _alreadyFetchedTransactionJournals;}
			set 
			{
				if(_alreadyFetchedTransactionJournals && !value && (_transactionJournals != null))
				{
					_transactionJournals.Clear();
				}
				_alreadyFetchedTransactionJournals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournals_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TransactionJournals_
		{
			get	{ return GetMultiTransactionJournals_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournals_. When set to true, TransactionJournals_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournals_ is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournals_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournals_
		{
			get	{ return _alwaysFetchTransactionJournals_; }
			set	{ _alwaysFetchTransactionJournals_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournals_ already has been fetched. Setting this property to false when TransactionJournals_ has been fetched
		/// will clear the TransactionJournals_ collection well. Setting this property to true while TransactionJournals_ hasn't been fetched disables lazy loading for TransactionJournals_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournals_
		{
			get { return _alreadyFetchedTransactionJournals_;}
			set 
			{
				if(_alreadyFetchedTransactionJournals_ && !value && (_transactionJournals_ != null))
				{
					_transactionJournals_.Clear();
				}
				_alreadyFetchedTransactionJournals_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTokenTransactionJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TokenTransactionJournals
		{
			get	{ return GetMultiTokenTransactionJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TokenTransactionJournals. When set to true, TokenTransactionJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TokenTransactionJournals is accessed. You can always execute/ a forced fetch by calling GetMultiTokenTransactionJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTokenTransactionJournals
		{
			get	{ return _alwaysFetchTokenTransactionJournals; }
			set	{ _alwaysFetchTokenTransactionJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TokenTransactionJournals already has been fetched. Setting this property to false when TokenTransactionJournals has been fetched
		/// will clear the TokenTransactionJournals collection well. Setting this property to true while TokenTransactionJournals hasn't been fetched disables lazy loading for TokenTransactionJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTokenTransactionJournals
		{
			get { return _alreadyFetchedTokenTransactionJournals;}
			set 
			{
				if(_alreadyFetchedTokenTransactionJournals && !value && (_tokenTransactionJournals != null))
				{
					_tokenTransactionJournals.Clear();
				}
				_alreadyFetchedTokenTransactionJournals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VirtualCardEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVirtualCards()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VirtualCardCollection VirtualCards
		{
			get	{ return GetMultiVirtualCards(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VirtualCards. When set to true, VirtualCards is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VirtualCards is accessed. You can always execute/ a forced fetch by calling GetMultiVirtualCards(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVirtualCards
		{
			get	{ return _alwaysFetchVirtualCards; }
			set	{ _alwaysFetchVirtualCards = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VirtualCards already has been fetched. Setting this property to false when VirtualCards has been fetched
		/// will clear the VirtualCards collection well. Setting this property to true while VirtualCards hasn't been fetched disables lazy loading for VirtualCards</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVirtualCards
		{
			get { return _alreadyFetchedVirtualCards;}
			set 
			{
				if(_alreadyFetchedVirtualCards && !value && (_virtualCards != null))
				{
					_virtualCards.Clear();
				}
				_alreadyFetchedVirtualCards = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VoucherEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVouchers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VoucherCollection Vouchers
		{
			get	{ return GetMultiVouchers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Vouchers. When set to true, Vouchers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Vouchers is accessed. You can always execute/ a forced fetch by calling GetMultiVouchers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVouchers
		{
			get	{ return _alwaysFetchVouchers; }
			set	{ _alwaysFetchVouchers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Vouchers already has been fetched. Setting this property to false when Vouchers has been fetched
		/// will clear the Vouchers collection well. Setting this property to true while Vouchers hasn't been fetched disables lazy loading for Vouchers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVouchers
		{
			get { return _alreadyFetchedVouchers;}
			set 
			{
				if(_alreadyFetchedVouchers && !value && (_vouchers != null))
				{
					_vouchers.Clear();
				}
				_alreadyFetchedVouchers = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'WhitelistEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWhitelists()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.WhitelistCollection Whitelists
		{
			get	{ return GetMultiWhitelists(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Whitelists. When set to true, Whitelists is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Whitelists is accessed. You can always execute/ a forced fetch by calling GetMultiWhitelists(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWhitelists
		{
			get	{ return _alwaysFetchWhitelists; }
			set	{ _alwaysFetchWhitelists = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Whitelists already has been fetched. Setting this property to false when Whitelists has been fetched
		/// will clear the Whitelists collection well. Setting this property to true while Whitelists hasn't been fetched disables lazy loading for Whitelists</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWhitelists
		{
			get { return _alreadyFetchedWhitelists;}
			set 
			{
				if(_alreadyFetchedWhitelists && !value && (_whitelists != null))
				{
					_whitelists.Clear();
				}
				_alreadyFetchedWhitelists = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'WhitelistJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWhitelistJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.WhitelistJournalCollection WhitelistJournals
		{
			get	{ return GetMultiWhitelistJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for WhitelistJournals. When set to true, WhitelistJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WhitelistJournals is accessed. You can always execute/ a forced fetch by calling GetMultiWhitelistJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWhitelistJournals
		{
			get	{ return _alwaysFetchWhitelistJournals; }
			set	{ _alwaysFetchWhitelistJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property WhitelistJournals already has been fetched. Setting this property to false when WhitelistJournals has been fetched
		/// will clear the WhitelistJournals collection well. Setting this property to true while WhitelistJournals hasn't been fetched disables lazy loading for WhitelistJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWhitelistJournals
		{
			get { return _alreadyFetchedWhitelistJournals;}
			set 
			{
				if(_alreadyFetchedWhitelistJournals && !value && (_whitelistJournals != null))
				{
					_whitelistJournals.Clear();
				}
				_alreadyFetchedWhitelistJournals = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContracts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractCollection Contracts
		{
			get { return GetMultiContracts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Contracts. When set to true, Contracts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contracts is accessed. You can always execute a forced fetch by calling GetMultiContracts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContracts
		{
			get	{ return _alwaysFetchContracts; }
			set	{ _alwaysFetchContracts = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contracts already has been fetched. Setting this property to false when Contracts has been fetched
		/// will clear the Contracts collection well. Setting this property to true while Contracts hasn't been fetched disables lazy loading for Contracts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContracts
		{
			get { return _alreadyFetchedContracts;}
			set 
			{
				if(_alreadyFetchedContracts && !value && (_contracts != null))
				{
					_contracts.Clear();
				}
				_alreadyFetchedContracts = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiStockTransferCollectionViaCardStockTransfer()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.StockTransferCollection StockTransferCollectionViaCardStockTransfer
		{
			get { return GetMultiStockTransferCollectionViaCardStockTransfer(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for StockTransferCollectionViaCardStockTransfer. When set to true, StockTransferCollectionViaCardStockTransfer is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time StockTransferCollectionViaCardStockTransfer is accessed. You can always execute a forced fetch by calling GetMultiStockTransferCollectionViaCardStockTransfer(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchStockTransferCollectionViaCardStockTransfer
		{
			get	{ return _alwaysFetchStockTransferCollectionViaCardStockTransfer; }
			set	{ _alwaysFetchStockTransferCollectionViaCardStockTransfer = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property StockTransferCollectionViaCardStockTransfer already has been fetched. Setting this property to false when StockTransferCollectionViaCardStockTransfer has been fetched
		/// will clear the StockTransferCollectionViaCardStockTransfer collection well. Setting this property to true while StockTransferCollectionViaCardStockTransfer hasn't been fetched disables lazy loading for StockTransferCollectionViaCardStockTransfer</summary>
		[Browsable(false)]
		public bool AlreadyFetchedStockTransferCollectionViaCardStockTransfer
		{
			get { return _alreadyFetchedStockTransferCollectionViaCardStockTransfer;}
			set 
			{
				if(_alreadyFetchedStockTransferCollectionViaCardStockTransfer && !value && (_stockTransferCollectionViaCardStockTransfer != null))
				{
					_stockTransferCollectionViaCardStockTransfer.Clear();
				}
				_alreadyFetchedStockTransferCollectionViaCardStockTransfer = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Cards", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReplacedCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity ReplacedCard
		{
			get	{ return GetSingleReplacedCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReplacedCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReplacementCard", "ReplacedCard", _replacedCard, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReplacedCard. When set to true, ReplacedCard is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReplacedCard is accessed. You can always execute a forced fetch by calling GetSingleReplacedCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReplacedCard
		{
			get	{ return _alwaysFetchReplacedCard; }
			set	{ _alwaysFetchReplacedCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReplacedCard already has been fetched. Setting this property to false when ReplacedCard has been fetched
		/// will set ReplacedCard to null as well. Setting this property to true while ReplacedCard hasn't been fetched disables lazy loading for ReplacedCard</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReplacedCard
		{
			get { return _alreadyFetchedReplacedCard;}
			set 
			{
				if(_alreadyFetchedReplacedCard && !value)
				{
					this.ReplacedCard = null;
				}
				_alreadyFetchedReplacedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReplacedCard is not found
		/// in the database. When set to true, ReplacedCard will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ReplacedCardReturnsNewIfNotFound
		{
			get	{ return _replacedCardReturnsNewIfNotFound; }
			set { _replacedCardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardHolderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardHolder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardHolderEntity CardHolder
		{
			get	{ return GetSingleCardHolder(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCardHolder(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Cards", "CardHolder", _cardHolder, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardHolder. When set to true, CardHolder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardHolder is accessed. You can always execute a forced fetch by calling GetSingleCardHolder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardHolder
		{
			get	{ return _alwaysFetchCardHolder; }
			set	{ _alwaysFetchCardHolder = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardHolder already has been fetched. Setting this property to false when CardHolder has been fetched
		/// will set CardHolder to null as well. Setting this property to true while CardHolder hasn't been fetched disables lazy loading for CardHolder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardHolder
		{
			get { return _alreadyFetchedCardHolder;}
			set 
			{
				if(_alreadyFetchedCardHolder && !value)
				{
					this.CardHolder = null;
				}
				_alreadyFetchedCardHolder = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardHolder is not found
		/// in the database. When set to true, CardHolder will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardHolderReturnsNewIfNotFound
		{
			get	{ return _cardHolderReturnsNewIfNotFound; }
			set { _cardHolderReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardHolderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParticipant()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardHolderEntity Participant
		{
			get	{ return GetSingleParticipant(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParticipant(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ParticipantCards", "Participant", _participant, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Participant. When set to true, Participant is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Participant is accessed. You can always execute a forced fetch by calling GetSingleParticipant(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParticipant
		{
			get	{ return _alwaysFetchParticipant; }
			set	{ _alwaysFetchParticipant = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Participant already has been fetched. Setting this property to false when Participant has been fetched
		/// will set Participant to null as well. Setting this property to true while Participant hasn't been fetched disables lazy loading for Participant</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParticipant
		{
			get { return _alreadyFetchedParticipant;}
			set 
			{
				if(_alreadyFetchedParticipant && !value)
				{
					this.Participant = null;
				}
				_alreadyFetchedParticipant = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Participant is not found
		/// in the database. When set to true, Participant will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ParticipantReturnsNewIfNotFound
		{
			get	{ return _participantReturnsNewIfNotFound; }
			set { _participantReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardKeyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardKey()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardKeyEntity CardKey
		{
			get	{ return GetSingleCardKey(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCardKey(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Cards", "CardKey", _cardKey, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardKey. When set to true, CardKey is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardKey is accessed. You can always execute a forced fetch by calling GetSingleCardKey(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardKey
		{
			get	{ return _alwaysFetchCardKey; }
			set	{ _alwaysFetchCardKey = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardKey already has been fetched. Setting this property to false when CardKey has been fetched
		/// will set CardKey to null as well. Setting this property to true while CardKey hasn't been fetched disables lazy loading for CardKey</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardKey
		{
			get { return _alreadyFetchedCardKey;}
			set 
			{
				if(_alreadyFetchedCardKey && !value)
				{
					this.CardKey = null;
				}
				_alreadyFetchedCardKey = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardKey is not found
		/// in the database. When set to true, CardKey will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardKeyReturnsNewIfNotFound
		{
			get	{ return _cardKeyReturnsNewIfNotFound; }
			set { _cardKeyReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardPhysicalDetailEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardPhysicalDetail()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardPhysicalDetailEntity CardPhysicalDetail
		{
			get	{ return GetSingleCardPhysicalDetail(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCardPhysicalDetail(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Cards", "CardPhysicalDetail", _cardPhysicalDetail, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardPhysicalDetail. When set to true, CardPhysicalDetail is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardPhysicalDetail is accessed. You can always execute a forced fetch by calling GetSingleCardPhysicalDetail(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardPhysicalDetail
		{
			get	{ return _alwaysFetchCardPhysicalDetail; }
			set	{ _alwaysFetchCardPhysicalDetail = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardPhysicalDetail already has been fetched. Setting this property to false when CardPhysicalDetail has been fetched
		/// will set CardPhysicalDetail to null as well. Setting this property to true while CardPhysicalDetail hasn't been fetched disables lazy loading for CardPhysicalDetail</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardPhysicalDetail
		{
			get { return _alreadyFetchedCardPhysicalDetail;}
			set 
			{
				if(_alreadyFetchedCardPhysicalDetail && !value)
				{
					this.CardPhysicalDetail = null;
				}
				_alreadyFetchedCardPhysicalDetail = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardPhysicalDetail is not found
		/// in the database. When set to true, CardPhysicalDetail will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardPhysicalDetailReturnsNewIfNotFound
		{
			get	{ return _cardPhysicalDetailReturnsNewIfNotFound; }
			set { _cardPhysicalDetailReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'StorageLocationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleStorageLocation()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual StorageLocationEntity StorageLocation
		{
			get	{ return GetSingleStorageLocation(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncStorageLocation(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Cards", "StorageLocation", _storageLocation, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for StorageLocation. When set to true, StorageLocation is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time StorageLocation is accessed. You can always execute a forced fetch by calling GetSingleStorageLocation(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchStorageLocation
		{
			get	{ return _alwaysFetchStorageLocation; }
			set	{ _alwaysFetchStorageLocation = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property StorageLocation already has been fetched. Setting this property to false when StorageLocation has been fetched
		/// will set StorageLocation to null as well. Setting this property to true while StorageLocation hasn't been fetched disables lazy loading for StorageLocation</summary>
		[Browsable(false)]
		public bool AlreadyFetchedStorageLocation
		{
			get { return _alreadyFetchedStorageLocation;}
			set 
			{
				if(_alreadyFetchedStorageLocation && !value)
				{
					this.StorageLocation = null;
				}
				_alreadyFetchedStorageLocation = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property StorageLocation is not found
		/// in the database. When set to true, StorageLocation will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool StorageLocationReturnsNewIfNotFound
		{
			get	{ return _storageLocationReturnsNewIfNotFound; }
			set { _storageLocationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardLinkEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardLinkTarget()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardLinkEntity CardLinkTarget
		{
			get	{ return GetSingleCardLinkTarget(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncCardLinkTarget(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_cardLinkTarget !=null);
						DesetupSyncCardLinkTarget(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("CardLinkTarget");
						}
					}
					else
					{
						if(_cardLinkTarget!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TargetCard");
							SetupSyncCardLinkTarget(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardLinkTarget. When set to true, CardLinkTarget is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardLinkTarget is accessed. You can always execute a forced fetch by calling GetSingleCardLinkTarget(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardLinkTarget
		{
			get	{ return _alwaysFetchCardLinkTarget; }
			set	{ _alwaysFetchCardLinkTarget = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property CardLinkTarget already has been fetched. Setting this property to false when CardLinkTarget has been fetched
		/// will set CardLinkTarget to null as well. Setting this property to true while CardLinkTarget hasn't been fetched disables lazy loading for CardLinkTarget</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardLinkTarget
		{
			get { return _alreadyFetchedCardLinkTarget;}
			set 
			{
				if(_alreadyFetchedCardLinkTarget && !value)
				{
					this.CardLinkTarget = null;
				}
				_alreadyFetchedCardLinkTarget = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardLinkTarget is not found
		/// in the database. When set to true, CardLinkTarget will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardLinkTargetReturnsNewIfNotFound
		{
			get	{ return _cardLinkTargetReturnsNewIfNotFound; }
			set	{ _cardLinkTargetReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CardEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
