﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'BookingItemPayment'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class BookingItemPaymentEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private BookingItemEntity _bookingItem;
		private bool	_alwaysFetchBookingItem, _alreadyFetchedBookingItem, _bookingItemReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BookingItem</summary>
			public static readonly string BookingItem = "BookingItem";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static BookingItemPaymentEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public BookingItemPaymentEntity() :base("BookingItemPaymentEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="bookingItemPaymentID">PK value for BookingItemPayment which data should be fetched into this BookingItemPayment object</param>
		public BookingItemPaymentEntity(System.Int64 bookingItemPaymentID):base("BookingItemPaymentEntity")
		{
			InitClassFetch(bookingItemPaymentID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="bookingItemPaymentID">PK value for BookingItemPayment which data should be fetched into this BookingItemPayment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public BookingItemPaymentEntity(System.Int64 bookingItemPaymentID, IPrefetchPath prefetchPathToUse):base("BookingItemPaymentEntity")
		{
			InitClassFetch(bookingItemPaymentID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="bookingItemPaymentID">PK value for BookingItemPayment which data should be fetched into this BookingItemPayment object</param>
		/// <param name="validator">The custom validator object for this BookingItemPaymentEntity</param>
		public BookingItemPaymentEntity(System.Int64 bookingItemPaymentID, IValidator validator):base("BookingItemPaymentEntity")
		{
			InitClassFetch(bookingItemPaymentID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BookingItemPaymentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_bookingItem = (BookingItemEntity)info.GetValue("_bookingItem", typeof(BookingItemEntity));
			if(_bookingItem!=null)
			{
				_bookingItem.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_bookingItemReturnsNewIfNotFound = info.GetBoolean("_bookingItemReturnsNewIfNotFound");
			_alwaysFetchBookingItem = info.GetBoolean("_alwaysFetchBookingItem");
			_alreadyFetchedBookingItem = info.GetBoolean("_alreadyFetchedBookingItem");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((BookingItemPaymentFieldIndex)fieldIndex)
			{
				case BookingItemPaymentFieldIndex.BookingItemID:
					DesetupSyncBookingItem(true, false);
					_alreadyFetchedBookingItem = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedBookingItem = (_bookingItem != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BookingItem":
					toReturn.Add(Relations.BookingItemEntityUsingBookingItemID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_bookingItem", (!this.MarkedForDeletion?_bookingItem:null));
			info.AddValue("_bookingItemReturnsNewIfNotFound", _bookingItemReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBookingItem", _alwaysFetchBookingItem);
			info.AddValue("_alreadyFetchedBookingItem", _alreadyFetchedBookingItem);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BookingItem":
					_alreadyFetchedBookingItem = true;
					this.BookingItem = (BookingItemEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BookingItem":
					SetupSyncBookingItem(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BookingItem":
					DesetupSyncBookingItem(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_bookingItem!=null)
			{
				toReturn.Add(_bookingItem);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bookingItemPaymentID">PK value for BookingItemPayment which data should be fetched into this BookingItemPayment object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bookingItemPaymentID)
		{
			return FetchUsingPK(bookingItemPaymentID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bookingItemPaymentID">PK value for BookingItemPayment which data should be fetched into this BookingItemPayment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bookingItemPaymentID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(bookingItemPaymentID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bookingItemPaymentID">PK value for BookingItemPayment which data should be fetched into this BookingItemPayment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bookingItemPaymentID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(bookingItemPaymentID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bookingItemPaymentID">PK value for BookingItemPayment which data should be fetched into this BookingItemPayment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bookingItemPaymentID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(bookingItemPaymentID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.BookingItemPaymentID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new BookingItemPaymentRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'BookingItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BookingItemEntity' which is related to this entity.</returns>
		public BookingItemEntity GetSingleBookingItem()
		{
			return GetSingleBookingItem(false);
		}

		/// <summary> Retrieves the related entity of type 'BookingItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BookingItemEntity' which is related to this entity.</returns>
		public virtual BookingItemEntity GetSingleBookingItem(bool forceFetch)
		{
			if( ( !_alreadyFetchedBookingItem || forceFetch || _alwaysFetchBookingItem) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BookingItemEntityUsingBookingItemID);
				BookingItemEntity newEntity = new BookingItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BookingItemID);
				}
				if(fetchResult)
				{
					newEntity = (BookingItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_bookingItemReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BookingItem = newEntity;
				_alreadyFetchedBookingItem = fetchResult;
			}
			return _bookingItem;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BookingItem", _bookingItem);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="bookingItemPaymentID">PK value for BookingItemPayment which data should be fetched into this BookingItemPayment object</param>
		/// <param name="validator">The validator object for this BookingItemPaymentEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 bookingItemPaymentID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(bookingItemPaymentID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_bookingItemReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingItemID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingItemPaymentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingItemPaymentType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataRowCreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExecutionTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VatAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VatRate", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _bookingItem</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBookingItem(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _bookingItem, new PropertyChangedEventHandler( OnBookingItemPropertyChanged ), "BookingItem", VarioSL.Entities.RelationClasses.StaticBookingItemPaymentRelations.BookingItemEntityUsingBookingItemIDStatic, true, signalRelatedEntity, "BookingItemPayments", resetFKFields, new int[] { (int)BookingItemPaymentFieldIndex.BookingItemID } );		
			_bookingItem = null;
		}
		
		/// <summary> setups the sync logic for member _bookingItem</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBookingItem(IEntityCore relatedEntity)
		{
			if(_bookingItem!=relatedEntity)
			{		
				DesetupSyncBookingItem(true, true);
				_bookingItem = (BookingItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _bookingItem, new PropertyChangedEventHandler( OnBookingItemPropertyChanged ), "BookingItem", VarioSL.Entities.RelationClasses.StaticBookingItemPaymentRelations.BookingItemEntityUsingBookingItemIDStatic, true, ref _alreadyFetchedBookingItem, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBookingItemPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="bookingItemPaymentID">PK value for BookingItemPayment which data should be fetched into this BookingItemPayment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 bookingItemPaymentID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)BookingItemPaymentFieldIndex.BookingItemPaymentID].ForcedCurrentValueWrite(bookingItemPaymentID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateBookingItemPaymentDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new BookingItemPaymentEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static BookingItemPaymentRelations Relations
		{
			get	{ return new BookingItemPaymentRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BookingItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBookingItem
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BookingItemCollection(), (IEntityRelation)GetRelationsForField("BookingItem")[0], (int)VarioSL.Entities.EntityType.BookingItemPaymentEntity, (int)VarioSL.Entities.EntityType.BookingItemEntity, 0, null, null, null, "BookingItem", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Amount
		{
			get { return (System.Int64)GetValue((int)BookingItemPaymentFieldIndex.Amount, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.Amount, value, true); }
		}

		/// <summary> The BookingItemID property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."BOOKINGITEMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 BookingItemID
		{
			get { return (System.Int64)GetValue((int)BookingItemPaymentFieldIndex.BookingItemID, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.BookingItemID, value, true); }
		}

		/// <summary> The BookingItemPaymentID property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."BOOKINGITEMPAYMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 BookingItemPaymentID
		{
			get { return (System.Int64)GetValue((int)BookingItemPaymentFieldIndex.BookingItemPaymentID, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.BookingItemPaymentID, value, true); }
		}

		/// <summary> The BookingItemPaymentType property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."BOOKINGITEMPAYMENTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.BookingItemPaymentType BookingItemPaymentType
		{
			get { return (VarioSL.Entities.Enumerations.BookingItemPaymentType)GetValue((int)BookingItemPaymentFieldIndex.BookingItemPaymentType, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.BookingItemPaymentType, value, true); }
		}

		/// <summary> The DataRowCreationDate property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."DATAROWCREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataRowCreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BookingItemPaymentFieldIndex.DataRowCreationDate, false); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.DataRowCreationDate, value, true); }
		}

		/// <summary> The Description property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)BookingItemPaymentFieldIndex.Description, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.Description, value, true); }
		}

		/// <summary> The ExecutionTime property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."EXECUTIONTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ExecutionTime
		{
			get { return (System.DateTime)GetValue((int)BookingItemPaymentFieldIndex.ExecutionTime, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.ExecutionTime, value, true); }
		}

		/// <summary> The LastModified property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)BookingItemPaymentFieldIndex.LastModified, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)BookingItemPaymentFieldIndex.LastUser, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PaymentMethod property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."PAYMENTMETHOD"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.DevicePaymentMethod PaymentMethod
		{
			get { return (VarioSL.Entities.Enumerations.DevicePaymentMethod)GetValue((int)BookingItemPaymentFieldIndex.PaymentMethod, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.PaymentMethod, value, true); }
		}

		/// <summary> The PaymentReference property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."PAYMENTREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentReference
		{
			get { return (System.String)GetValue((int)BookingItemPaymentFieldIndex.PaymentReference, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.PaymentReference, value, true); }
		}

		/// <summary> The PaymentState property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."PAYMENTSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.PaymentState PaymentState
		{
			get { return (VarioSL.Entities.Enumerations.PaymentState)GetValue((int)BookingItemPaymentFieldIndex.PaymentState, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.PaymentState, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)BookingItemPaymentFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The VatAmount property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."VATAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 VatAmount
		{
			get { return (System.Int64)GetValue((int)BookingItemPaymentFieldIndex.VatAmount, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.VatAmount, value, true); }
		}

		/// <summary> The VatRate property of the Entity BookingItemPayment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEMPAYMENT"."VATRATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 VatRate
		{
			get { return (System.Int64)GetValue((int)BookingItemPaymentFieldIndex.VatRate, true); }
			set	{ SetValue((int)BookingItemPaymentFieldIndex.VatRate, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'BookingItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBookingItem()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BookingItemEntity BookingItem
		{
			get	{ return GetSingleBookingItem(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBookingItem(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BookingItemPayments", "BookingItem", _bookingItem, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BookingItem. When set to true, BookingItem is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BookingItem is accessed. You can always execute a forced fetch by calling GetSingleBookingItem(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBookingItem
		{
			get	{ return _alwaysFetchBookingItem; }
			set	{ _alwaysFetchBookingItem = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BookingItem already has been fetched. Setting this property to false when BookingItem has been fetched
		/// will set BookingItem to null as well. Setting this property to true while BookingItem hasn't been fetched disables lazy loading for BookingItem</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBookingItem
		{
			get { return _alreadyFetchedBookingItem;}
			set 
			{
				if(_alreadyFetchedBookingItem && !value)
				{
					this.BookingItem = null;
				}
				_alreadyFetchedBookingItem = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BookingItem is not found
		/// in the database. When set to true, BookingItem will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BookingItemReturnsNewIfNotFound
		{
			get	{ return _bookingItemReturnsNewIfNotFound; }
			set { _bookingItemReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.BookingItemPaymentEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
