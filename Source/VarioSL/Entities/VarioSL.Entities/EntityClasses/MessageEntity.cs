﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Message'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class MessageEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.JobCollection	_jobs;
		private bool	_alwaysFetchJobs, _alreadyFetchedJobs;
		private EmailEventEntity _emailEvent;
		private bool	_alwaysFetchEmailEvent, _alreadyFetchedEmailEvent, _emailEventReturnsNewIfNotFound;
		private RecipientGroupEntity _recipientGroup;
		private bool	_alwaysFetchRecipientGroup, _alreadyFetchedRecipientGroup, _recipientGroupReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name EmailEvent</summary>
			public static readonly string EmailEvent = "EmailEvent";
			/// <summary>Member name RecipientGroup</summary>
			public static readonly string RecipientGroup = "RecipientGroup";
			/// <summary>Member name Jobs</summary>
			public static readonly string Jobs = "Jobs";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MessageEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public MessageEntity() :base("MessageEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="messageID">PK value for Message which data should be fetched into this Message object</param>
		public MessageEntity(System.Int64 messageID):base("MessageEntity")
		{
			InitClassFetch(messageID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="messageID">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public MessageEntity(System.Int64 messageID, IPrefetchPath prefetchPathToUse):base("MessageEntity")
		{
			InitClassFetch(messageID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="messageID">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="validator">The custom validator object for this MessageEntity</param>
		public MessageEntity(System.Int64 messageID, IValidator validator):base("MessageEntity")
		{
			InitClassFetch(messageID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_jobs = (VarioSL.Entities.CollectionClasses.JobCollection)info.GetValue("_jobs", typeof(VarioSL.Entities.CollectionClasses.JobCollection));
			_alwaysFetchJobs = info.GetBoolean("_alwaysFetchJobs");
			_alreadyFetchedJobs = info.GetBoolean("_alreadyFetchedJobs");
			_emailEvent = (EmailEventEntity)info.GetValue("_emailEvent", typeof(EmailEventEntity));
			if(_emailEvent!=null)
			{
				_emailEvent.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_emailEventReturnsNewIfNotFound = info.GetBoolean("_emailEventReturnsNewIfNotFound");
			_alwaysFetchEmailEvent = info.GetBoolean("_alwaysFetchEmailEvent");
			_alreadyFetchedEmailEvent = info.GetBoolean("_alreadyFetchedEmailEvent");

			_recipientGroup = (RecipientGroupEntity)info.GetValue("_recipientGroup", typeof(RecipientGroupEntity));
			if(_recipientGroup!=null)
			{
				_recipientGroup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_recipientGroupReturnsNewIfNotFound = info.GetBoolean("_recipientGroupReturnsNewIfNotFound");
			_alwaysFetchRecipientGroup = info.GetBoolean("_alwaysFetchRecipientGroup");
			_alreadyFetchedRecipientGroup = info.GetBoolean("_alreadyFetchedRecipientGroup");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((MessageFieldIndex)fieldIndex)
			{
				case MessageFieldIndex.EmailEventID:
					DesetupSyncEmailEvent(true, false);
					_alreadyFetchedEmailEvent = false;
					break;
				case MessageFieldIndex.RecipientGroupID:
					DesetupSyncRecipientGroup(true, false);
					_alreadyFetchedRecipientGroup = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedJobs = (_jobs.Count > 0);
			_alreadyFetchedEmailEvent = (_emailEvent != null);
			_alreadyFetchedRecipientGroup = (_recipientGroup != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "EmailEvent":
					toReturn.Add(Relations.EmailEventEntityUsingEmailEventID);
					break;
				case "RecipientGroup":
					toReturn.Add(Relations.RecipientGroupEntityUsingRecipientGroupID);
					break;
				case "Jobs":
					toReturn.Add(Relations.JobEntityUsingMessageID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_jobs", (!this.MarkedForDeletion?_jobs:null));
			info.AddValue("_alwaysFetchJobs", _alwaysFetchJobs);
			info.AddValue("_alreadyFetchedJobs", _alreadyFetchedJobs);
			info.AddValue("_emailEvent", (!this.MarkedForDeletion?_emailEvent:null));
			info.AddValue("_emailEventReturnsNewIfNotFound", _emailEventReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEmailEvent", _alwaysFetchEmailEvent);
			info.AddValue("_alreadyFetchedEmailEvent", _alreadyFetchedEmailEvent);
			info.AddValue("_recipientGroup", (!this.MarkedForDeletion?_recipientGroup:null));
			info.AddValue("_recipientGroupReturnsNewIfNotFound", _recipientGroupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRecipientGroup", _alwaysFetchRecipientGroup);
			info.AddValue("_alreadyFetchedRecipientGroup", _alreadyFetchedRecipientGroup);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "EmailEvent":
					_alreadyFetchedEmailEvent = true;
					this.EmailEvent = (EmailEventEntity)entity;
					break;
				case "RecipientGroup":
					_alreadyFetchedRecipientGroup = true;
					this.RecipientGroup = (RecipientGroupEntity)entity;
					break;
				case "Jobs":
					_alreadyFetchedJobs = true;
					if(entity!=null)
					{
						this.Jobs.Add((JobEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "EmailEvent":
					SetupSyncEmailEvent(relatedEntity);
					break;
				case "RecipientGroup":
					SetupSyncRecipientGroup(relatedEntity);
					break;
				case "Jobs":
					_jobs.Add((JobEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "EmailEvent":
					DesetupSyncEmailEvent(false, true);
					break;
				case "RecipientGroup":
					DesetupSyncRecipientGroup(false, true);
					break;
				case "Jobs":
					this.PerformRelatedEntityRemoval(_jobs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_emailEvent!=null)
			{
				toReturn.Add(_emailEvent);
			}
			if(_recipientGroup!=null)
			{
				toReturn.Add(_recipientGroup);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_jobs);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageID">PK value for Message which data should be fetched into this Message object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 messageID)
		{
			return FetchUsingPK(messageID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageID">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 messageID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(messageID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageID">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 messageID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(messageID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageID">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 messageID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(messageID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.MessageID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MessageRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'JobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch)
		{
			return GetMultiJobs(forceFetch, _jobs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'JobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiJobs(forceFetch, _jobs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiJobs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedJobs || forceFetch || _alwaysFetchJobs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_jobs);
				_jobs.SuppressClearInGetMulti=!forceFetch;
				_jobs.EntityFactoryToUse = entityFactoryToUse;
				_jobs.GetMultiManyToOne(null, null, null, null, null, this, null, null, filter);
				_jobs.SuppressClearInGetMulti=false;
				_alreadyFetchedJobs = true;
			}
			return _jobs;
		}

		/// <summary> Sets the collection parameters for the collection for 'Jobs'. These settings will be taken into account
		/// when the property Jobs is requested or GetMultiJobs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersJobs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_jobs.SortClauses=sortClauses;
			_jobs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'EmailEventEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EmailEventEntity' which is related to this entity.</returns>
		public EmailEventEntity GetSingleEmailEvent()
		{
			return GetSingleEmailEvent(false);
		}

		/// <summary> Retrieves the related entity of type 'EmailEventEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EmailEventEntity' which is related to this entity.</returns>
		public virtual EmailEventEntity GetSingleEmailEvent(bool forceFetch)
		{
			if( ( !_alreadyFetchedEmailEvent || forceFetch || _alwaysFetchEmailEvent) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EmailEventEntityUsingEmailEventID);
				EmailEventEntity newEntity = new EmailEventEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EmailEventID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EmailEventEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_emailEventReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EmailEvent = newEntity;
				_alreadyFetchedEmailEvent = fetchResult;
			}
			return _emailEvent;
		}


		/// <summary> Retrieves the related entity of type 'RecipientGroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RecipientGroupEntity' which is related to this entity.</returns>
		public RecipientGroupEntity GetSingleRecipientGroup()
		{
			return GetSingleRecipientGroup(false);
		}

		/// <summary> Retrieves the related entity of type 'RecipientGroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RecipientGroupEntity' which is related to this entity.</returns>
		public virtual RecipientGroupEntity GetSingleRecipientGroup(bool forceFetch)
		{
			if( ( !_alreadyFetchedRecipientGroup || forceFetch || _alwaysFetchRecipientGroup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RecipientGroupEntityUsingRecipientGroupID);
				RecipientGroupEntity newEntity = new RecipientGroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RecipientGroupID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RecipientGroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_recipientGroupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RecipientGroup = newEntity;
				_alreadyFetchedRecipientGroup = fetchResult;
			}
			return _recipientGroup;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("EmailEvent", _emailEvent);
			toReturn.Add("RecipientGroup", _recipientGroup);
			toReturn.Add("Jobs", _jobs);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="messageID">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="validator">The validator object for this MessageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 messageID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(messageID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_jobs = new VarioSL.Entities.CollectionClasses.JobCollection();
			_jobs.SetContainingEntityInfo(this, "Message");
			_emailEventReturnsNewIfNotFound = false;
			_recipientGroupReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmailEventID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InternalData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageContent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Recipient", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _emailEvent</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEmailEvent(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _emailEvent, new PropertyChangedEventHandler( OnEmailEventPropertyChanged ), "EmailEvent", VarioSL.Entities.RelationClasses.StaticMessageRelations.EmailEventEntityUsingEmailEventIDStatic, true, signalRelatedEntity, "Messages", resetFKFields, new int[] { (int)MessageFieldIndex.EmailEventID } );		
			_emailEvent = null;
		}
		
		/// <summary> setups the sync logic for member _emailEvent</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEmailEvent(IEntityCore relatedEntity)
		{
			if(_emailEvent!=relatedEntity)
			{		
				DesetupSyncEmailEvent(true, true);
				_emailEvent = (EmailEventEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _emailEvent, new PropertyChangedEventHandler( OnEmailEventPropertyChanged ), "EmailEvent", VarioSL.Entities.RelationClasses.StaticMessageRelations.EmailEventEntityUsingEmailEventIDStatic, true, ref _alreadyFetchedEmailEvent, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEmailEventPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _recipientGroup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRecipientGroup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _recipientGroup, new PropertyChangedEventHandler( OnRecipientGroupPropertyChanged ), "RecipientGroup", VarioSL.Entities.RelationClasses.StaticMessageRelations.RecipientGroupEntityUsingRecipientGroupIDStatic, true, signalRelatedEntity, "Messages", resetFKFields, new int[] { (int)MessageFieldIndex.RecipientGroupID } );		
			_recipientGroup = null;
		}
		
		/// <summary> setups the sync logic for member _recipientGroup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRecipientGroup(IEntityCore relatedEntity)
		{
			if(_recipientGroup!=relatedEntity)
			{		
				DesetupSyncRecipientGroup(true, true);
				_recipientGroup = (RecipientGroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _recipientGroup, new PropertyChangedEventHandler( OnRecipientGroupPropertyChanged ), "RecipientGroup", VarioSL.Entities.RelationClasses.StaticMessageRelations.RecipientGroupEntityUsingRecipientGroupIDStatic, true, ref _alreadyFetchedRecipientGroup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRecipientGroupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="messageID">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 messageID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)MessageFieldIndex.MessageID].ForcedCurrentValueWrite(messageID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateMessageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new MessageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MessageRelations Relations
		{
			get	{ return new MessageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Job' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathJobs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.JobCollection(), (IEntityRelation)GetRelationsForField("Jobs")[0], (int)VarioSL.Entities.EntityType.MessageEntity, (int)VarioSL.Entities.EntityType.JobEntity, 0, null, null, null, "Jobs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailEvent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailEvent
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailEventCollection(), (IEntityRelation)GetRelationsForField("EmailEvent")[0], (int)VarioSL.Entities.EntityType.MessageEntity, (int)VarioSL.Entities.EntityType.EmailEventEntity, 0, null, null, null, "EmailEvent", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RecipientGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRecipientGroup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RecipientGroupCollection(), (IEntityRelation)GetRelationsForField("RecipientGroup")[0], (int)VarioSL.Entities.EntityType.MessageEntity, (int)VarioSL.Entities.EntityType.RecipientGroupEntity, 0, null, null, null, "RecipientGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The EmailEventID property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGE"."EMAILEVENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EmailEventID
		{
			get { return (Nullable<System.Int64>)GetValue((int)MessageFieldIndex.EmailEventID, false); }
			set	{ SetValue((int)MessageFieldIndex.EmailEventID, value, true); }
		}

		/// <summary> The InternalData property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGE"."INTERNALDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Blob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.Byte[] InternalData
		{
			get { return (System.Byte[])GetValue((int)MessageFieldIndex.InternalData, true); }
			set	{ SetValue((int)MessageFieldIndex.InternalData, value, true); }
		}

		/// <summary> The LastModified property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)MessageFieldIndex.LastModified, true); }
			set	{ SetValue((int)MessageFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)MessageFieldIndex.LastUser, true); }
			set	{ SetValue((int)MessageFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MessageContent property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGE"."MESSAGECONTENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Blob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] MessageContent
		{
			get { return (System.Byte[])GetValue((int)MessageFieldIndex.MessageContent, true); }
			set	{ SetValue((int)MessageFieldIndex.MessageContent, value, true); }
		}

		/// <summary> The MessageID property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGE"."MESSAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 MessageID
		{
			get { return (System.Int64)GetValue((int)MessageFieldIndex.MessageID, true); }
			set	{ SetValue((int)MessageFieldIndex.MessageID, value, true); }
		}

		/// <summary> The MessageType property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGE"."MESSAGETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.MessageType MessageType
		{
			get { return (VarioSL.Entities.Enumerations.MessageType)GetValue((int)MessageFieldIndex.MessageType, true); }
			set	{ SetValue((int)MessageFieldIndex.MessageType, value, true); }
		}

		/// <summary> The Recipient property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGE"."RECIPIENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Recipient
		{
			get { return (System.String)GetValue((int)MessageFieldIndex.Recipient, true); }
			set	{ SetValue((int)MessageFieldIndex.Recipient, value, true); }
		}

		/// <summary> The RecipientGroupID property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGE"."RECIPIENTGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RecipientGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)MessageFieldIndex.RecipientGroupID, false); }
			set	{ SetValue((int)MessageFieldIndex.RecipientGroupID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)MessageFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)MessageFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiJobs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.JobCollection Jobs
		{
			get	{ return GetMultiJobs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Jobs. When set to true, Jobs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Jobs is accessed. You can always execute/ a forced fetch by calling GetMultiJobs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchJobs
		{
			get	{ return _alwaysFetchJobs; }
			set	{ _alwaysFetchJobs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Jobs already has been fetched. Setting this property to false when Jobs has been fetched
		/// will clear the Jobs collection well. Setting this property to true while Jobs hasn't been fetched disables lazy loading for Jobs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedJobs
		{
			get { return _alreadyFetchedJobs;}
			set 
			{
				if(_alreadyFetchedJobs && !value && (_jobs != null))
				{
					_jobs.Clear();
				}
				_alreadyFetchedJobs = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'EmailEventEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEmailEvent()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual EmailEventEntity EmailEvent
		{
			get	{ return GetSingleEmailEvent(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEmailEvent(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Messages", "EmailEvent", _emailEvent, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EmailEvent. When set to true, EmailEvent is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailEvent is accessed. You can always execute a forced fetch by calling GetSingleEmailEvent(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailEvent
		{
			get	{ return _alwaysFetchEmailEvent; }
			set	{ _alwaysFetchEmailEvent = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailEvent already has been fetched. Setting this property to false when EmailEvent has been fetched
		/// will set EmailEvent to null as well. Setting this property to true while EmailEvent hasn't been fetched disables lazy loading for EmailEvent</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailEvent
		{
			get { return _alreadyFetchedEmailEvent;}
			set 
			{
				if(_alreadyFetchedEmailEvent && !value)
				{
					this.EmailEvent = null;
				}
				_alreadyFetchedEmailEvent = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EmailEvent is not found
		/// in the database. When set to true, EmailEvent will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool EmailEventReturnsNewIfNotFound
		{
			get	{ return _emailEventReturnsNewIfNotFound; }
			set { _emailEventReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RecipientGroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRecipientGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RecipientGroupEntity RecipientGroup
		{
			get	{ return GetSingleRecipientGroup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRecipientGroup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Messages", "RecipientGroup", _recipientGroup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RecipientGroup. When set to true, RecipientGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RecipientGroup is accessed. You can always execute a forced fetch by calling GetSingleRecipientGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRecipientGroup
		{
			get	{ return _alwaysFetchRecipientGroup; }
			set	{ _alwaysFetchRecipientGroup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RecipientGroup already has been fetched. Setting this property to false when RecipientGroup has been fetched
		/// will set RecipientGroup to null as well. Setting this property to true while RecipientGroup hasn't been fetched disables lazy loading for RecipientGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRecipientGroup
		{
			get { return _alreadyFetchedRecipientGroup;}
			set 
			{
				if(_alreadyFetchedRecipientGroup && !value)
				{
					this.RecipientGroup = null;
				}
				_alreadyFetchedRecipientGroup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RecipientGroup is not found
		/// in the database. When set to true, RecipientGroup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RecipientGroupReturnsNewIfNotFound
		{
			get	{ return _recipientGroupReturnsNewIfNotFound; }
			set { _recipientGroupReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.MessageEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
