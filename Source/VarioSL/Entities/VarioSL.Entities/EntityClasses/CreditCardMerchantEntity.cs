﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CreditCardMerchant'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CreditCardMerchantEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CreditCardTerminalCollection	_creditCardTerminals;
		private bool	_alwaysFetchCreditCardTerminals, _alreadyFetchedCreditCardTerminals;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CreditCardTerminals</summary>
			public static readonly string CreditCardTerminals = "CreditCardTerminals";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CreditCardMerchantEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CreditCardMerchantEntity() :base("CreditCardMerchantEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="creditCardMerchantID">PK value for CreditCardMerchant which data should be fetched into this CreditCardMerchant object</param>
		public CreditCardMerchantEntity(System.Int64 creditCardMerchantID):base("CreditCardMerchantEntity")
		{
			InitClassFetch(creditCardMerchantID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="creditCardMerchantID">PK value for CreditCardMerchant which data should be fetched into this CreditCardMerchant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CreditCardMerchantEntity(System.Int64 creditCardMerchantID, IPrefetchPath prefetchPathToUse):base("CreditCardMerchantEntity")
		{
			InitClassFetch(creditCardMerchantID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="creditCardMerchantID">PK value for CreditCardMerchant which data should be fetched into this CreditCardMerchant object</param>
		/// <param name="validator">The custom validator object for this CreditCardMerchantEntity</param>
		public CreditCardMerchantEntity(System.Int64 creditCardMerchantID, IValidator validator):base("CreditCardMerchantEntity")
		{
			InitClassFetch(creditCardMerchantID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CreditCardMerchantEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_creditCardTerminals = (VarioSL.Entities.CollectionClasses.CreditCardTerminalCollection)info.GetValue("_creditCardTerminals", typeof(VarioSL.Entities.CollectionClasses.CreditCardTerminalCollection));
			_alwaysFetchCreditCardTerminals = info.GetBoolean("_alwaysFetchCreditCardTerminals");
			_alreadyFetchedCreditCardTerminals = info.GetBoolean("_alreadyFetchedCreditCardTerminals");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCreditCardTerminals = (_creditCardTerminals.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CreditCardTerminals":
					toReturn.Add(Relations.CreditCardTerminalEntityUsingCreditCardMerchantID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_creditCardTerminals", (!this.MarkedForDeletion?_creditCardTerminals:null));
			info.AddValue("_alwaysFetchCreditCardTerminals", _alwaysFetchCreditCardTerminals);
			info.AddValue("_alreadyFetchedCreditCardTerminals", _alreadyFetchedCreditCardTerminals);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CreditCardTerminals":
					_alreadyFetchedCreditCardTerminals = true;
					if(entity!=null)
					{
						this.CreditCardTerminals.Add((CreditCardTerminalEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CreditCardTerminals":
					_creditCardTerminals.Add((CreditCardTerminalEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CreditCardTerminals":
					this.PerformRelatedEntityRemoval(_creditCardTerminals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_creditCardTerminals);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardMerchantID">PK value for CreditCardMerchant which data should be fetched into this CreditCardMerchant object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardMerchantID)
		{
			return FetchUsingPK(creditCardMerchantID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardMerchantID">PK value for CreditCardMerchant which data should be fetched into this CreditCardMerchant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardMerchantID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(creditCardMerchantID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardMerchantID">PK value for CreditCardMerchant which data should be fetched into this CreditCardMerchant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardMerchantID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(creditCardMerchantID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardMerchantID">PK value for CreditCardMerchant which data should be fetched into this CreditCardMerchant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardMerchantID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(creditCardMerchantID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CreditCardMerchantID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CreditCardMerchantRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CreditCardTerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CreditCardTerminalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CreditCardTerminalCollection GetMultiCreditCardTerminals(bool forceFetch)
		{
			return GetMultiCreditCardTerminals(forceFetch, _creditCardTerminals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CreditCardTerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CreditCardTerminalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CreditCardTerminalCollection GetMultiCreditCardTerminals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCreditCardTerminals(forceFetch, _creditCardTerminals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CreditCardTerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CreditCardTerminalCollection GetMultiCreditCardTerminals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCreditCardTerminals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CreditCardTerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CreditCardTerminalCollection GetMultiCreditCardTerminals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCreditCardTerminals || forceFetch || _alwaysFetchCreditCardTerminals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_creditCardTerminals);
				_creditCardTerminals.SuppressClearInGetMulti=!forceFetch;
				_creditCardTerminals.EntityFactoryToUse = entityFactoryToUse;
				_creditCardTerminals.GetMultiManyToOne(this, filter);
				_creditCardTerminals.SuppressClearInGetMulti=false;
				_alreadyFetchedCreditCardTerminals = true;
			}
			return _creditCardTerminals;
		}

		/// <summary> Sets the collection parameters for the collection for 'CreditCardTerminals'. These settings will be taken into account
		/// when the property CreditCardTerminals is requested or GetMultiCreditCardTerminals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCreditCardTerminals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_creditCardTerminals.SortClauses=sortClauses;
			_creditCardTerminals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CreditCardTerminals", _creditCardTerminals);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="creditCardMerchantID">PK value for CreditCardMerchant which data should be fetched into this CreditCardMerchant object</param>
		/// <param name="validator">The validator object for this CreditCardMerchantEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 creditCardMerchantID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(creditCardMerchantID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_creditCardTerminals = new VarioSL.Entities.CollectionClasses.CreditCardTerminalCollection();
			_creditCardTerminals.SetContainingEntityInfo(this, "CreditCardMerchant");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditCardMerchantID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalMerchantID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TokenType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="creditCardMerchantID">PK value for CreditCardMerchant which data should be fetched into this CreditCardMerchant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 creditCardMerchantID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CreditCardMerchantFieldIndex.CreditCardMerchantID].ForcedCurrentValueWrite(creditCardMerchantID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCreditCardMerchantDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CreditCardMerchantEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CreditCardMerchantRelations Relations
		{
			get	{ return new CreditCardMerchantRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CreditCardTerminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCreditCardTerminals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CreditCardTerminalCollection(), (IEntityRelation)GetRelationsForField("CreditCardTerminals")[0], (int)VarioSL.Entities.EntityType.CreditCardMerchantEntity, (int)VarioSL.Entities.EntityType.CreditCardTerminalEntity, 0, null, null, null, "CreditCardTerminals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CreditCardMerchantID property of the Entity CreditCardMerchant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDMERCHANT"."CREDITCARDMERCHANTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CreditCardMerchantID
		{
			get { return (System.Int64)GetValue((int)CreditCardMerchantFieldIndex.CreditCardMerchantID, true); }
			set	{ SetValue((int)CreditCardMerchantFieldIndex.CreditCardMerchantID, value, true); }
		}

		/// <summary> The ExternalMerchantID property of the Entity CreditCardMerchant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDMERCHANT"."EXTERNALMERCHANTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 128<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ExternalMerchantID
		{
			get { return (System.String)GetValue((int)CreditCardMerchantFieldIndex.ExternalMerchantID, true); }
			set	{ SetValue((int)CreditCardMerchantFieldIndex.ExternalMerchantID, value, true); }
		}

		/// <summary> The LastModified property of the Entity CreditCardMerchant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDMERCHANT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CreditCardMerchantFieldIndex.LastModified, true); }
			set	{ SetValue((int)CreditCardMerchantFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CreditCardMerchant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDMERCHANT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CreditCardMerchantFieldIndex.LastUser, true); }
			set	{ SetValue((int)CreditCardMerchantFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity CreditCardMerchant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDMERCHANT"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 128<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CreditCardMerchantFieldIndex.Name, true); }
			set	{ SetValue((int)CreditCardMerchantFieldIndex.Name, value, true); }
		}

		/// <summary> The TokenType property of the Entity CreditCardMerchant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDMERCHANT"."TOKENTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TokenType
		{
			get { return (System.String)GetValue((int)CreditCardMerchantFieldIndex.TokenType, true); }
			set	{ SetValue((int)CreditCardMerchantFieldIndex.TokenType, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CreditCardMerchant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDMERCHANT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CreditCardMerchantFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CreditCardMerchantFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CreditCardTerminalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCreditCardTerminals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CreditCardTerminalCollection CreditCardTerminals
		{
			get	{ return GetMultiCreditCardTerminals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CreditCardTerminals. When set to true, CreditCardTerminals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CreditCardTerminals is accessed. You can always execute/ a forced fetch by calling GetMultiCreditCardTerminals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCreditCardTerminals
		{
			get	{ return _alwaysFetchCreditCardTerminals; }
			set	{ _alwaysFetchCreditCardTerminals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CreditCardTerminals already has been fetched. Setting this property to false when CreditCardTerminals has been fetched
		/// will clear the CreditCardTerminals collection well. Setting this property to true while CreditCardTerminals hasn't been fetched disables lazy loading for CreditCardTerminals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCreditCardTerminals
		{
			get { return _alreadyFetchedCreditCardTerminals;}
			set 
			{
				if(_alreadyFetchedCreditCardTerminals && !value && (_creditCardTerminals != null))
				{
					_creditCardTerminals.Clear();
				}
				_alreadyFetchedCreditCardTerminals = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CreditCardMerchantEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
