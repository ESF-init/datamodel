﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RecipientGroup'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RecipientGroupEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.MessageCollection	_messages;
		private bool	_alwaysFetchMessages, _alreadyFetchedMessages;
		private VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection	_recipientGroupMembers;
		private bool	_alwaysFetchRecipientGroupMembers, _alreadyFetchedRecipientGroupMembers;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Messages</summary>
			public static readonly string Messages = "Messages";
			/// <summary>Member name RecipientGroupMembers</summary>
			public static readonly string RecipientGroupMembers = "RecipientGroupMembers";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RecipientGroupEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RecipientGroupEntity() :base("RecipientGroupEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="recipientGroupID">PK value for RecipientGroup which data should be fetched into this RecipientGroup object</param>
		public RecipientGroupEntity(System.Int64 recipientGroupID):base("RecipientGroupEntity")
		{
			InitClassFetch(recipientGroupID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="recipientGroupID">PK value for RecipientGroup which data should be fetched into this RecipientGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RecipientGroupEntity(System.Int64 recipientGroupID, IPrefetchPath prefetchPathToUse):base("RecipientGroupEntity")
		{
			InitClassFetch(recipientGroupID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="recipientGroupID">PK value for RecipientGroup which data should be fetched into this RecipientGroup object</param>
		/// <param name="validator">The custom validator object for this RecipientGroupEntity</param>
		public RecipientGroupEntity(System.Int64 recipientGroupID, IValidator validator):base("RecipientGroupEntity")
		{
			InitClassFetch(recipientGroupID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RecipientGroupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_messages = (VarioSL.Entities.CollectionClasses.MessageCollection)info.GetValue("_messages", typeof(VarioSL.Entities.CollectionClasses.MessageCollection));
			_alwaysFetchMessages = info.GetBoolean("_alwaysFetchMessages");
			_alreadyFetchedMessages = info.GetBoolean("_alreadyFetchedMessages");

			_recipientGroupMembers = (VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection)info.GetValue("_recipientGroupMembers", typeof(VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection));
			_alwaysFetchRecipientGroupMembers = info.GetBoolean("_alwaysFetchRecipientGroupMembers");
			_alreadyFetchedRecipientGroupMembers = info.GetBoolean("_alreadyFetchedRecipientGroupMembers");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedMessages = (_messages.Count > 0);
			_alreadyFetchedRecipientGroupMembers = (_recipientGroupMembers.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Messages":
					toReturn.Add(Relations.MessageEntityUsingRecipientGroupID);
					break;
				case "RecipientGroupMembers":
					toReturn.Add(Relations.RecipientGroupMemberEntityUsingRecipientGroupID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_messages", (!this.MarkedForDeletion?_messages:null));
			info.AddValue("_alwaysFetchMessages", _alwaysFetchMessages);
			info.AddValue("_alreadyFetchedMessages", _alreadyFetchedMessages);
			info.AddValue("_recipientGroupMembers", (!this.MarkedForDeletion?_recipientGroupMembers:null));
			info.AddValue("_alwaysFetchRecipientGroupMembers", _alwaysFetchRecipientGroupMembers);
			info.AddValue("_alreadyFetchedRecipientGroupMembers", _alreadyFetchedRecipientGroupMembers);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Messages":
					_alreadyFetchedMessages = true;
					if(entity!=null)
					{
						this.Messages.Add((MessageEntity)entity);
					}
					break;
				case "RecipientGroupMembers":
					_alreadyFetchedRecipientGroupMembers = true;
					if(entity!=null)
					{
						this.RecipientGroupMembers.Add((RecipientGroupMemberEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Messages":
					_messages.Add((MessageEntity)relatedEntity);
					break;
				case "RecipientGroupMembers":
					_recipientGroupMembers.Add((RecipientGroupMemberEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Messages":
					this.PerformRelatedEntityRemoval(_messages, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RecipientGroupMembers":
					this.PerformRelatedEntityRemoval(_recipientGroupMembers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_messages);
			toReturn.Add(_recipientGroupMembers);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="recipientGroupID">PK value for RecipientGroup which data should be fetched into this RecipientGroup object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 recipientGroupID)
		{
			return FetchUsingPK(recipientGroupID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="recipientGroupID">PK value for RecipientGroup which data should be fetched into this RecipientGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 recipientGroupID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(recipientGroupID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="recipientGroupID">PK value for RecipientGroup which data should be fetched into this RecipientGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 recipientGroupID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(recipientGroupID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="recipientGroupID">PK value for RecipientGroup which data should be fetched into this RecipientGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 recipientGroupID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(recipientGroupID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RecipientGroupID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RecipientGroupRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.MessageCollection GetMultiMessages(bool forceFetch)
		{
			return GetMultiMessages(forceFetch, _messages.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.MessageCollection GetMultiMessages(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessages(forceFetch, _messages.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.MessageCollection GetMultiMessages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessages(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.MessageCollection GetMultiMessages(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessages || forceFetch || _alwaysFetchMessages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messages);
				_messages.SuppressClearInGetMulti=!forceFetch;
				_messages.EntityFactoryToUse = entityFactoryToUse;
				_messages.GetMultiManyToOne(null, this, filter);
				_messages.SuppressClearInGetMulti=false;
				_alreadyFetchedMessages = true;
			}
			return _messages;
		}

		/// <summary> Sets the collection parameters for the collection for 'Messages'. These settings will be taken into account
		/// when the property Messages is requested or GetMultiMessages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messages.SortClauses=sortClauses;
			_messages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RecipientGroupMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RecipientGroupMemberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection GetMultiRecipientGroupMembers(bool forceFetch)
		{
			return GetMultiRecipientGroupMembers(forceFetch, _recipientGroupMembers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RecipientGroupMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RecipientGroupMemberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection GetMultiRecipientGroupMembers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRecipientGroupMembers(forceFetch, _recipientGroupMembers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RecipientGroupMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection GetMultiRecipientGroupMembers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRecipientGroupMembers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RecipientGroupMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection GetMultiRecipientGroupMembers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRecipientGroupMembers || forceFetch || _alwaysFetchRecipientGroupMembers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_recipientGroupMembers);
				_recipientGroupMembers.SuppressClearInGetMulti=!forceFetch;
				_recipientGroupMembers.EntityFactoryToUse = entityFactoryToUse;
				_recipientGroupMembers.GetMultiManyToOne(null, this, filter);
				_recipientGroupMembers.SuppressClearInGetMulti=false;
				_alreadyFetchedRecipientGroupMembers = true;
			}
			return _recipientGroupMembers;
		}

		/// <summary> Sets the collection parameters for the collection for 'RecipientGroupMembers'. These settings will be taken into account
		/// when the property RecipientGroupMembers is requested or GetMultiRecipientGroupMembers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRecipientGroupMembers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_recipientGroupMembers.SortClauses=sortClauses;
			_recipientGroupMembers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Messages", _messages);
			toReturn.Add("RecipientGroupMembers", _recipientGroupMembers);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="recipientGroupID">PK value for RecipientGroup which data should be fetched into this RecipientGroup object</param>
		/// <param name="validator">The validator object for this RecipientGroupEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 recipientGroupID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(recipientGroupID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_messages = new VarioSL.Entities.CollectionClasses.MessageCollection();
			_messages.SetContainingEntityInfo(this, "RecipientGroup");

			_recipientGroupMembers = new VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection();
			_recipientGroupMembers.SetContainingEntityInfo(this, "RecipientGroup");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="recipientGroupID">PK value for RecipientGroup which data should be fetched into this RecipientGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 recipientGroupID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RecipientGroupFieldIndex.RecipientGroupID].ForcedCurrentValueWrite(recipientGroupID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRecipientGroupDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RecipientGroupEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RecipientGroupRelations Relations
		{
			get	{ return new RecipientGroupRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Message' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessages
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.MessageCollection(), (IEntityRelation)GetRelationsForField("Messages")[0], (int)VarioSL.Entities.EntityType.RecipientGroupEntity, (int)VarioSL.Entities.EntityType.MessageEntity, 0, null, null, null, "Messages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RecipientGroupMember' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRecipientGroupMembers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection(), (IEntityRelation)GetRelationsForField("RecipientGroupMembers")[0], (int)VarioSL.Entities.EntityType.RecipientGroupEntity, (int)VarioSL.Entities.EntityType.RecipientGroupMemberEntity, 0, null, null, null, "RecipientGroupMembers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity RecipientGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RECIPIENTGROUP"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)RecipientGroupFieldIndex.Description, true); }
			set	{ SetValue((int)RecipientGroupFieldIndex.Description, value, true); }
		}

		/// <summary> The LastModified property of the Entity RecipientGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RECIPIENTGROUP"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)RecipientGroupFieldIndex.LastModified, true); }
			set	{ SetValue((int)RecipientGroupFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity RecipientGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RECIPIENTGROUP"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)RecipientGroupFieldIndex.LastUser, true); }
			set	{ SetValue((int)RecipientGroupFieldIndex.LastUser, value, true); }
		}

		/// <summary> The RecipientGroupID property of the Entity RecipientGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RECIPIENTGROUP"."RECIPIENTGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 RecipientGroupID
		{
			get { return (System.Int64)GetValue((int)RecipientGroupFieldIndex.RecipientGroupID, true); }
			set	{ SetValue((int)RecipientGroupFieldIndex.RecipientGroupID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity RecipientGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RECIPIENTGROUP"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)RecipientGroupFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)RecipientGroupFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.MessageCollection Messages
		{
			get	{ return GetMultiMessages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Messages. When set to true, Messages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Messages is accessed. You can always execute/ a forced fetch by calling GetMultiMessages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessages
		{
			get	{ return _alwaysFetchMessages; }
			set	{ _alwaysFetchMessages = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Messages already has been fetched. Setting this property to false when Messages has been fetched
		/// will clear the Messages collection well. Setting this property to true while Messages hasn't been fetched disables lazy loading for Messages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessages
		{
			get { return _alreadyFetchedMessages;}
			set 
			{
				if(_alreadyFetchedMessages && !value && (_messages != null))
				{
					_messages.Clear();
				}
				_alreadyFetchedMessages = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RecipientGroupMemberEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRecipientGroupMembers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection RecipientGroupMembers
		{
			get	{ return GetMultiRecipientGroupMembers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RecipientGroupMembers. When set to true, RecipientGroupMembers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RecipientGroupMembers is accessed. You can always execute/ a forced fetch by calling GetMultiRecipientGroupMembers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRecipientGroupMembers
		{
			get	{ return _alwaysFetchRecipientGroupMembers; }
			set	{ _alwaysFetchRecipientGroupMembers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RecipientGroupMembers already has been fetched. Setting this property to false when RecipientGroupMembers has been fetched
		/// will clear the RecipientGroupMembers collection well. Setting this property to true while RecipientGroupMembers hasn't been fetched disables lazy loading for RecipientGroupMembers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRecipientGroupMembers
		{
			get { return _alreadyFetchedRecipientGroupMembers;}
			set 
			{
				if(_alreadyFetchedRecipientGroupMembers && !value && (_recipientGroupMembers != null))
				{
					_recipientGroupMembers.Clear();
				}
				_alreadyFetchedRecipientGroupMembers = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.RecipientGroupEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
