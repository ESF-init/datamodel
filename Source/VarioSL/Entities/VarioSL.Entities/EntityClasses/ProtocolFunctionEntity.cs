﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ProtocolFunction'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ProtocolFunctionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ProtocolCollection	_protocols;
		private bool	_alwaysFetchProtocols, _alreadyFetchedProtocols;
		private ProtocolFunctionGroupEntity _protocolFunctionGroup;
		private bool	_alwaysFetchProtocolFunctionGroup, _alreadyFetchedProtocolFunctionGroup, _protocolFunctionGroupReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ProtocolFunctionGroup</summary>
			public static readonly string ProtocolFunctionGroup = "ProtocolFunctionGroup";
			/// <summary>Member name Protocols</summary>
			public static readonly string Protocols = "Protocols";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ProtocolFunctionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ProtocolFunctionEntity() :base("ProtocolFunctionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="protocolFunctionID">PK value for ProtocolFunction which data should be fetched into this ProtocolFunction object</param>
		public ProtocolFunctionEntity(System.Int32 protocolFunctionID):base("ProtocolFunctionEntity")
		{
			InitClassFetch(protocolFunctionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="protocolFunctionID">PK value for ProtocolFunction which data should be fetched into this ProtocolFunction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProtocolFunctionEntity(System.Int32 protocolFunctionID, IPrefetchPath prefetchPathToUse):base("ProtocolFunctionEntity")
		{
			InitClassFetch(protocolFunctionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="protocolFunctionID">PK value for ProtocolFunction which data should be fetched into this ProtocolFunction object</param>
		/// <param name="validator">The custom validator object for this ProtocolFunctionEntity</param>
		public ProtocolFunctionEntity(System.Int32 protocolFunctionID, IValidator validator):base("ProtocolFunctionEntity")
		{
			InitClassFetch(protocolFunctionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProtocolFunctionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_protocols = (VarioSL.Entities.CollectionClasses.ProtocolCollection)info.GetValue("_protocols", typeof(VarioSL.Entities.CollectionClasses.ProtocolCollection));
			_alwaysFetchProtocols = info.GetBoolean("_alwaysFetchProtocols");
			_alreadyFetchedProtocols = info.GetBoolean("_alreadyFetchedProtocols");
			_protocolFunctionGroup = (ProtocolFunctionGroupEntity)info.GetValue("_protocolFunctionGroup", typeof(ProtocolFunctionGroupEntity));
			if(_protocolFunctionGroup!=null)
			{
				_protocolFunctionGroup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_protocolFunctionGroupReturnsNewIfNotFound = info.GetBoolean("_protocolFunctionGroupReturnsNewIfNotFound");
			_alwaysFetchProtocolFunctionGroup = info.GetBoolean("_alwaysFetchProtocolFunctionGroup");
			_alreadyFetchedProtocolFunctionGroup = info.GetBoolean("_alreadyFetchedProtocolFunctionGroup");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ProtocolFunctionFieldIndex)fieldIndex)
			{
				case ProtocolFunctionFieldIndex.GroupID:
					DesetupSyncProtocolFunctionGroup(true, false);
					_alreadyFetchedProtocolFunctionGroup = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedProtocols = (_protocols.Count > 0);
			_alreadyFetchedProtocolFunctionGroup = (_protocolFunctionGroup != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ProtocolFunctionGroup":
					toReturn.Add(Relations.ProtocolFunctionGroupEntityUsingGroupID);
					break;
				case "Protocols":
					toReturn.Add(Relations.ProtocolEntityUsingFunctionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_protocols", (!this.MarkedForDeletion?_protocols:null));
			info.AddValue("_alwaysFetchProtocols", _alwaysFetchProtocols);
			info.AddValue("_alreadyFetchedProtocols", _alreadyFetchedProtocols);
			info.AddValue("_protocolFunctionGroup", (!this.MarkedForDeletion?_protocolFunctionGroup:null));
			info.AddValue("_protocolFunctionGroupReturnsNewIfNotFound", _protocolFunctionGroupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProtocolFunctionGroup", _alwaysFetchProtocolFunctionGroup);
			info.AddValue("_alreadyFetchedProtocolFunctionGroup", _alreadyFetchedProtocolFunctionGroup);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ProtocolFunctionGroup":
					_alreadyFetchedProtocolFunctionGroup = true;
					this.ProtocolFunctionGroup = (ProtocolFunctionGroupEntity)entity;
					break;
				case "Protocols":
					_alreadyFetchedProtocols = true;
					if(entity!=null)
					{
						this.Protocols.Add((ProtocolEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ProtocolFunctionGroup":
					SetupSyncProtocolFunctionGroup(relatedEntity);
					break;
				case "Protocols":
					_protocols.Add((ProtocolEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ProtocolFunctionGroup":
					DesetupSyncProtocolFunctionGroup(false, true);
					break;
				case "Protocols":
					this.PerformRelatedEntityRemoval(_protocols, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_protocolFunctionGroup!=null)
			{
				toReturn.Add(_protocolFunctionGroup);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_protocols);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolFunctionID">PK value for ProtocolFunction which data should be fetched into this ProtocolFunction object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 protocolFunctionID)
		{
			return FetchUsingPK(protocolFunctionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolFunctionID">PK value for ProtocolFunction which data should be fetched into this ProtocolFunction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 protocolFunctionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(protocolFunctionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolFunctionID">PK value for ProtocolFunction which data should be fetched into this ProtocolFunction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 protocolFunctionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(protocolFunctionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolFunctionID">PK value for ProtocolFunction which data should be fetched into this ProtocolFunction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 protocolFunctionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(protocolFunctionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ProtocolFunctionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ProtocolFunctionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ProtocolEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProtocolEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProtocolCollection GetMultiProtocols(bool forceFetch)
		{
			return GetMultiProtocols(forceFetch, _protocols.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProtocolEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProtocolEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProtocolCollection GetMultiProtocols(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProtocols(forceFetch, _protocols.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProtocolEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProtocolCollection GetMultiProtocols(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProtocols(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProtocolEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProtocolCollection GetMultiProtocols(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProtocols || forceFetch || _alwaysFetchProtocols) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_protocols);
				_protocols.SuppressClearInGetMulti=!forceFetch;
				_protocols.EntityFactoryToUse = entityFactoryToUse;
				_protocols.GetMultiManyToOne(this, filter);
				_protocols.SuppressClearInGetMulti=false;
				_alreadyFetchedProtocols = true;
			}
			return _protocols;
		}

		/// <summary> Sets the collection parameters for the collection for 'Protocols'. These settings will be taken into account
		/// when the property Protocols is requested or GetMultiProtocols is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProtocols(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_protocols.SortClauses=sortClauses;
			_protocols.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ProtocolFunctionGroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProtocolFunctionGroupEntity' which is related to this entity.</returns>
		public ProtocolFunctionGroupEntity GetSingleProtocolFunctionGroup()
		{
			return GetSingleProtocolFunctionGroup(false);
		}

		/// <summary> Retrieves the related entity of type 'ProtocolFunctionGroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProtocolFunctionGroupEntity' which is related to this entity.</returns>
		public virtual ProtocolFunctionGroupEntity GetSingleProtocolFunctionGroup(bool forceFetch)
		{
			if( ( !_alreadyFetchedProtocolFunctionGroup || forceFetch || _alwaysFetchProtocolFunctionGroup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProtocolFunctionGroupEntityUsingGroupID);
				ProtocolFunctionGroupEntity newEntity = new ProtocolFunctionGroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GroupID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProtocolFunctionGroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_protocolFunctionGroupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProtocolFunctionGroup = newEntity;
				_alreadyFetchedProtocolFunctionGroup = fetchResult;
			}
			return _protocolFunctionGroup;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ProtocolFunctionGroup", _protocolFunctionGroup);
			toReturn.Add("Protocols", _protocols);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="protocolFunctionID">PK value for ProtocolFunction which data should be fetched into this ProtocolFunction object</param>
		/// <param name="validator">The validator object for this ProtocolFunctionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 protocolFunctionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(protocolFunctionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_protocols = new VarioSL.Entities.CollectionClasses.ProtocolCollection();
			_protocols.SetContainingEntityInfo(this, "ProtocolFunction");
			_protocolFunctionGroupReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Frequency", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FunctionName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LanguageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaximumAge", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinimumPriority", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProtocolFunctionID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _protocolFunctionGroup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProtocolFunctionGroup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _protocolFunctionGroup, new PropertyChangedEventHandler( OnProtocolFunctionGroupPropertyChanged ), "ProtocolFunctionGroup", VarioSL.Entities.RelationClasses.StaticProtocolFunctionRelations.ProtocolFunctionGroupEntityUsingGroupIDStatic, true, signalRelatedEntity, "ProtocolFunctions", resetFKFields, new int[] { (int)ProtocolFunctionFieldIndex.GroupID } );		
			_protocolFunctionGroup = null;
		}
		
		/// <summary> setups the sync logic for member _protocolFunctionGroup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProtocolFunctionGroup(IEntityCore relatedEntity)
		{
			if(_protocolFunctionGroup!=relatedEntity)
			{		
				DesetupSyncProtocolFunctionGroup(true, true);
				_protocolFunctionGroup = (ProtocolFunctionGroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _protocolFunctionGroup, new PropertyChangedEventHandler( OnProtocolFunctionGroupPropertyChanged ), "ProtocolFunctionGroup", VarioSL.Entities.RelationClasses.StaticProtocolFunctionRelations.ProtocolFunctionGroupEntityUsingGroupIDStatic, true, ref _alreadyFetchedProtocolFunctionGroup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProtocolFunctionGroupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="protocolFunctionID">PK value for ProtocolFunction which data should be fetched into this ProtocolFunction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 protocolFunctionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ProtocolFunctionFieldIndex.ProtocolFunctionID].ForcedCurrentValueWrite(protocolFunctionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateProtocolFunctionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ProtocolFunctionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ProtocolFunctionRelations Relations
		{
			get	{ return new ProtocolFunctionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Protocol' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProtocols
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProtocolCollection(), (IEntityRelation)GetRelationsForField("Protocols")[0], (int)VarioSL.Entities.EntityType.ProtocolFunctionEntity, (int)VarioSL.Entities.EntityType.ProtocolEntity, 0, null, null, null, "Protocols", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProtocolFunctionGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProtocolFunctionGroup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProtocolFunctionGroupCollection(), (IEntityRelation)GetRelationsForField("ProtocolFunctionGroup")[0], (int)VarioSL.Entities.EntityType.ProtocolFunctionEntity, (int)VarioSL.Entities.EntityType.ProtocolFunctionGroupEntity, 0, null, null, null, "ProtocolFunctionGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Frequency property of the Entity ProtocolFunction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_FUNCTION"."FCTFREQUENCY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Frequency
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProtocolFunctionFieldIndex.Frequency, false); }
			set	{ SetValue((int)ProtocolFunctionFieldIndex.Frequency, value, true); }
		}

		/// <summary> The FunctionName property of the Entity ProtocolFunction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_FUNCTION"."FCTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FunctionName
		{
			get { return (System.String)GetValue((int)ProtocolFunctionFieldIndex.FunctionName, true); }
			set	{ SetValue((int)ProtocolFunctionFieldIndex.FunctionName, value, true); }
		}

		/// <summary> The GroupID property of the Entity ProtocolFunction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_FUNCTION"."FCTGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> GroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProtocolFunctionFieldIndex.GroupID, false); }
			set	{ SetValue((int)ProtocolFunctionFieldIndex.GroupID, value, true); }
		}

		/// <summary> The LanguageID property of the Entity ProtocolFunction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_FUNCTION"."LANGUAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LanguageID
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProtocolFunctionFieldIndex.LanguageID, false); }
			set	{ SetValue((int)ProtocolFunctionFieldIndex.LanguageID, value, true); }
		}

		/// <summary> The MaximumAge property of the Entity ProtocolFunction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_FUNCTION"."MAXAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> MaximumAge
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProtocolFunctionFieldIndex.MaximumAge, false); }
			set	{ SetValue((int)ProtocolFunctionFieldIndex.MaximumAge, value, true); }
		}

		/// <summary> The MinimumPriority property of the Entity ProtocolFunction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_FUNCTION"."FCTMINPRIO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> MinimumPriority
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProtocolFunctionFieldIndex.MinimumPriority, false); }
			set	{ SetValue((int)ProtocolFunctionFieldIndex.MinimumPriority, value, true); }
		}

		/// <summary> The ProtocolFunctionID property of the Entity ProtocolFunction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_FUNCTION"."FCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 7, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 ProtocolFunctionID
		{
			get { return (System.Int32)GetValue((int)ProtocolFunctionFieldIndex.ProtocolFunctionID, true); }
			set	{ SetValue((int)ProtocolFunctionFieldIndex.ProtocolFunctionID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ProtocolEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProtocols()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProtocolCollection Protocols
		{
			get	{ return GetMultiProtocols(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Protocols. When set to true, Protocols is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Protocols is accessed. You can always execute/ a forced fetch by calling GetMultiProtocols(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProtocols
		{
			get	{ return _alwaysFetchProtocols; }
			set	{ _alwaysFetchProtocols = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Protocols already has been fetched. Setting this property to false when Protocols has been fetched
		/// will clear the Protocols collection well. Setting this property to true while Protocols hasn't been fetched disables lazy loading for Protocols</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProtocols
		{
			get { return _alreadyFetchedProtocols;}
			set 
			{
				if(_alreadyFetchedProtocols && !value && (_protocols != null))
				{
					_protocols.Clear();
				}
				_alreadyFetchedProtocols = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ProtocolFunctionGroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProtocolFunctionGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProtocolFunctionGroupEntity ProtocolFunctionGroup
		{
			get	{ return GetSingleProtocolFunctionGroup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProtocolFunctionGroup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProtocolFunctions", "ProtocolFunctionGroup", _protocolFunctionGroup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProtocolFunctionGroup. When set to true, ProtocolFunctionGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProtocolFunctionGroup is accessed. You can always execute a forced fetch by calling GetSingleProtocolFunctionGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProtocolFunctionGroup
		{
			get	{ return _alwaysFetchProtocolFunctionGroup; }
			set	{ _alwaysFetchProtocolFunctionGroup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProtocolFunctionGroup already has been fetched. Setting this property to false when ProtocolFunctionGroup has been fetched
		/// will set ProtocolFunctionGroup to null as well. Setting this property to true while ProtocolFunctionGroup hasn't been fetched disables lazy loading for ProtocolFunctionGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProtocolFunctionGroup
		{
			get { return _alreadyFetchedProtocolFunctionGroup;}
			set 
			{
				if(_alreadyFetchedProtocolFunctionGroup && !value)
				{
					this.ProtocolFunctionGroup = null;
				}
				_alreadyFetchedProtocolFunctionGroup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProtocolFunctionGroup is not found
		/// in the database. When set to true, ProtocolFunctionGroup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProtocolFunctionGroupReturnsNewIfNotFound
		{
			get	{ return _protocolFunctionGroupReturnsNewIfNotFound; }
			set { _protocolFunctionGroupReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ProtocolFunctionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
